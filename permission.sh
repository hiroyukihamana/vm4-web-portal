#!/bin/sh

dir=`pwd`
# set parmission
echo ${dir}

if [ ! -e ${dir}/htdocs/docs ];
then
	mkdir ${dir}/htdocs/docs
fi
if [ ! -e ${dir}/htdocs/files ];
then
	mkdir ${dir}/htdocs/files
fi
if [ ! -e ${dir}/tmp ];
then
	mkdir ${dir}/tmp
fi
if [ ! -e ${dir}/var ];
then
	mkdir ${dir}/var
fi
if [ ! -e ${dir}/bin/batch ];
then
	mkdir ${dir}/bin/batch
fi

chmod 777 ${dir}
chmod 777 ${dir}/htdocs/
chmod 777 -R ${dir}/htdocs/docs/
chmod 777 -R ${dir}/htdocs/files/
chmod 777 -R ${dir}/config/
chmod 777 -R ${dir}/webapp/smarty/cache/
chmod 777 -R ${dir}/webapp/smarty/compile/
chmod 777 -R ${dir}/logs/
chmod 777 -R ${dir}/tmp/
chmod 777 -R ${dir}/var/
chmod 777 -R ${dir}/bin/batch

