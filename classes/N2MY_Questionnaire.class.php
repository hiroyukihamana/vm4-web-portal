<?php

require_once("lib/EZLib/EZCore/EZLogger.class.php");
require_once("classes/mgm/dbi/questionnaire.dbi.php");
require_once("classes/mgm/dbi/question.dbi.php");
require_once("classes/mgm/dbi/question_branch.dbi.php");

class N2MY_Questionnaire {

    var $obj_Questionnaire = null;
    var $obj_Question = null;
    var $obj_QuestionBranch = null;
    var $dsn = null;

    function __construct($dsn) {
        $this->logger = EZLogger2::getInstance();
        $this->obj_Questionnaire     = new MgmQuestionnaireTable(N2MY_MDB_DSN);
        $this->obj_Question          = new MgmQuestionTable(N2MY_MDB_DSN);
        $this->obj_QuestionBranch    = new MgmQuestionBranchTable(N2MY_MDB_DSN);
        $this->dsn = $dsn;
    }

    function getQuestionnaireId($type) {
        if (!$type) {
            $this->logger->error("type not found");
           return false;
        }
        $where = "type = '".$type."'".
                 " AND is_active = 1 AND is_deleted = 0";
        $questionnaire = $this->obj_Questionnaire->getRow($where, "questionnaire_id");
        return $questionnaire["questionnaire_id"];
    }

    function getQuestionnaire($type, $lang = "en") {

        if (!$type) {
            $this->logger->error("type not found");
           return false;
        }
        $where = "type = '".$type."'".
                 " AND is_active = 1 AND is_deleted = 0";
        $questionnaire = $this->obj_Questionnaire->getRow($where);
        if (DB::isError($questionnaire)) {
            $this->logger->error($questionnaire->getUserInfo());
            return false;
        } elseif (!$questionnaire) {
            $this->logger->warn($type, "This Questionnaire type not exists!!");
            return false;
        }
        $where = "questionnaire_id = ".$questionnaire["questionnaire_id"];
        $questions = $this->obj_Question->getRowsAssoc($where, array("sort" => "asc"));
        if (DB::isError($questions)) {
            $this->logger->error($questions->getUserInfo());
            return false;
        } else {
            $config  = EZConfig::getInstance();
            $question_lang  = $config->getAll("QUESTION_LANG");
            if (!$question_lang[$lang]) {
              $text_lang = "lang2";
            } else {
                $text_lang = $question_lang[$lang];
            }
            foreach ($questions as $_key => $question) {
                //typeが5はcomment欄のみ
                $question_data[$_key] = array(
                       "question_id" => $question["question_id"],
                       "text"        => $question[$text_lang],
                       "type"        => $question["type"],
                       "sort"        => $question["sort"],
                );

                if ($question["type"] != "5"){
                    $where = "question_id = ".$question["question_id"];
                    $question_branchs = $this->obj_QuestionBranch->getRowsAssoc($where, array("sort" => "asc"));
                    if (DB::isError($question_branchs)) {
                        $this->logger->error($question_branchs->getUserInfo());
                        return false;
                    } else if ($question_branchs) {
                        $branch = array();
                        foreach ($question_branchs as $question_branch) {
                            $branch[] = array(
                                "question_branch_id" => $question_branch["question_branch_id"],
                                "text"               => $question_branch[$text_lang],
                                "sort"               => $question_branch["sort"],
                            );

                        }
                    }
                    $question_data[$_key]["branch"] = $branch;
                }
            }
        }
        return $question_data;
    }

    function add() {

    }

    function update() {

    }

    function delete() {

    }
}
