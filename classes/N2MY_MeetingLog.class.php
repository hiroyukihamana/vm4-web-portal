<?php
require_once("classes/core/Core_Meeting.class.php");
require_once("lib/EZLib/EZCore/EZLogger2.class.php");
//require_once("lib/EZLib/EZXML/EZXML.class.php");
require_once("lib/EZLib/EZUtil/EZUrl.class.php");
require_once("classes/core/dbi/Meeting.dbi.php");
require_once("classes/core/dbi/MeetingSequence.dbi.php");
require_once("classes/N2MY_Document.class.php");

class N2MY_MeetingLog {

    var $provider_id = "";
    var $provider_pw = "";
    var $logger = null;

    private $Core_Meeting;
    private $obj_Meeting;

    /**
     * コンストラクタ
     *
     * @param string $base_url COREのBaseURL
     * @param string $provider_id プロバイダID
     * @param string $provider_pw プロバイダパスワード
     */
    function __construct ($dsn) {
        $this->logger = & EZLogger2::getInstance();
        $this->dsn = $dsn;
        $this->Core_Meeting = new Core_Meeting( $this->dsn );
        $this->obj_Meeting = new DBI_Meeting( $this->dsn );
        $this->obj_MeetingSequence = new DBI_MeetingSequence( $this->dsn );
    }

    /**
     * 検索条件に一致するログ一覧を取得
     * @param array $param 検索条件の連想配列
     * @param boolean $del_flg 削除フラグ
     */
     function getList($param, $del_flg = false) {
        $param = array(
            "title"             => $param["title"],
            "participant"       => $param["participant"],
            "user_key"          => $param["user_key"],
            "room_key"          => $param["room_key"],
            "member_key"        => $param["member_key"],
            "meeting_tag"       => $param["meeting_tag"],
            "sort_key"          => isset($param["sort_key"]) ? $param["sort_key"] : "",
            "sort_type"         => isset($param["sort_type"]) ? $param["sort_type"] : "",
            "limit"             => $param["limit"],
            "offset"            => $param["offset"],
            "start_datetime"    => $param["start_datetime"],
            "end_datetime"      => $param["end_datetime"],
            "participant_tag"   => $param["participant_tag"],
        );

        return $del_flg ? $this->Core_Meeting->getMeetingInfoListAll( $param ) :
                                $this->Core_Meeting->getMeetingInfoList( $param );
     }

	 /**
	  * V-Cube ONE 専用
	  */
	 function getList_for_ONE($param, $del_flg = false){
        $param = array(
            "title"             => $param["title"],
            "participant"       => $param["participant"],
            "user_key"          => $param["user_key"],
            "room_key"          => $param["room_key"],
            "member_key"        => $param["member_key"],
            "meeting_tag"       => $param["meeting_tag"],
            "sort_key"          => isset($param["sort_key"]) ? $param["sort_key"] : "",
            "sort_type"         => isset($param["sort_type"]) ? $param["sort_type"] : "",
            "limit"             => $param["limit"],
            "offset"            => $param["offset"],
            "start_datetime"    => $param["start_datetime"],
            "end_datetime"      => $param["end_datetime"],
            "participant_tag"   => $param["participant_tag"],
        );

        return $del_flg ? $this->Core_Meeting->getMeetingInfoListAll( $param ) :
                                $this->Core_Meeting->getMeetingInfoList_for_ONE( $param );
	 }

     function getCount($param, $del_flg = false) {
        $param = array(
        	"user_key"          => $param["user_key"],
            "title"             => $param["title"],
            "participant"       => $param["participant"],
            "room_key"          => $param["room_key"],
            "member_key"        => $param["member_key"],
            "meeting_tag"       => $param["meeting_tag"],
            "sort"              => isset($param["sort"]) ? $param["sort"] : "",
            "sort_type"         => isset($param["sort_type"]) ? $param["sort_type"] : "",
            "limit"             => $param["limit"],
            "offset"            => $param["offset"],
            "start_datetime"    => $param["start_datetime"],
            "end_datetime"      => $param["end_datetime"],
            "participant_tag"   => $param["participant_tag"],
        );
        if ($del_flg) {
            $param["delete"] = 1;
        }
		$this->logger->info($param);
        return $this->Core_Meeting->getCount( $param );
     }

	 /**
	  * V-Cube ONE 専用
	  */
     function getCount_for_ONE($param, $del_flg = false) {
        $param = array(
            "title"             => $param["title"],
            "participant"       => $param["participant"],
            "user_key"          => $param["user_key"],
            "room_key"          => $param["room_key"],
            "member_key"        => $param["member_key"],
            "meeting_tag"       => $param["meeting_tag"],
            "sort"              => isset($param["sort"]) ? $param["sort"] : "",
            "sort_type"         => isset($param["sort_type"]) ? $param["sort_type"] : "",
            "limit"             => $param["limit"],
            "offset"            => $param["offset"],
            "start_datetime"    => $param["start_datetime"],
            "end_datetime"      => $param["end_datetime"],
            "participant_tag"   => $param["participant_tag"],
        );
        if ($del_flg) {
            $param["delete"] = 1;
        }
        return $this->Core_Meeting->getCount( $param );
     }

    /**
     * タイトル変更
     * core/htdocs/api/meeting/MeetingRename.php
     */
    function editTitle( $room_key, $meeting_key, $title )
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_key )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_key = $this->obj_Meeting->getOne( $where, "meeting_key" );
        if ($meeting_key) {
            $data = array( "meeting_name" => $title );
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    /**
     * パスワード設定
     */
    function setPassword($room_key, $meeting_key, $password, $owner = "")
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_key )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_key = $this->obj_Meeting->getOne($where, "meeting_key");

        if ( $meeting_key ) {
            $data = array(
                        "meeting_log_owner" => $owner,
                        "meeting_log_password" => $password
                    );
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    /**
     * パスワード解除
     */
    function unsetPassword($room_key, $meeting_key)
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_key )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_key = $this->obj_Meeting->getOne($where, "meeting_key");

        if ( $meeting_key ) {
            $data = array("meeting_log_password" => "");
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    //パスワード認証
    function checkPassword($room_key, $meeting_key, $password, $owner = "")
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_key )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_data = $this->obj_Meeting->getRow($where, "meeting_key, meeting_log_password");
        if (DB::isError($meeting_data) || empty($meeting_data) ) {
            $this->logger->error("meeting data not found : meeting_key:".$meeting_key);
            return false;
        }
        if ( $meeting_data["meeting_log_password"] == $password) {
            return true;
        } else {
            $this->logger->info("meeting password not match : meeting_key:".$meeting_key);
            return false;
        }
    }

    /**
     * 保護設定
     * core/htdocs/api/meeting_log/LogLock.php
     */
    function setProtect($room_key, $meeting_session_id)
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_session_id )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0".
                " AND is_locked = 0";
        $meeting_key = $this->obj_Meeting->getOne($where, "meeting_key");
        if ($meeting_key) {
            $data = array( "is_locked" => 1 );
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    /**
     * 保護解除
     */
    function unsetProtect($room_key, $meeting_session_id)
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_session_id )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0".
                " AND is_locked = 1";
        $meeting_key = $this->obj_Meeting->getOne($where, "meeting_key");
        if ($meeting_key) {
            $data = array( "is_locked" => 0 );
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    public function controlPublish($room_key, $meeting_session_id, $publish=0)
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_session_id )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0".
                " AND is_publish = '".addslashes( $publish )."'";
        $meeting_key = $this->obj_Meeting->getOne($where, "meeting_key");
        if ($meeting_key) {
            $is_publish = $publish ? 0 : 1;
            $data = array( "is_publish" => $is_publish );
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    /**
     * 会議の削除
     * api/meeting_log/LogDelete.php
     */
    function delete( $room_key, $meeting_session_id )
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_session_id )."'" .
//                " AND meeting_key = '".addslashes( $meeting_key )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0".
                " AND is_locked = 0";
        $meetingInfo = $this->obj_Meeting->getRow( $where );
        if ($meetingInfo) {
            $data = array(
                            "is_deleted"=> 1,
                            "is_active" => 0
                            );
            $where = sprintf( "meeting_key='%s'", $meetingInfo["meeting_key"] );
            $result = $this->obj_Meeting->update( $data, $where );
            // 映像の削除
            $this->deleteVideo( $meetingInfo["meeting_session_id"] );
            // 議事録の削除
            $this->deleteMinutes( $meetingInfo["meeting_session_id"] );
            // ファイルキャビネットの削除
            $this->Core_Meeting->deleteCabinet( $meetingInfo["meeting_key"] );
            // 動画生成データの削除
            $where = sprintf( "meeting_key='%s'", $meetingInfo["meeting_key"] );
            $sequenceList = $this->obj_MeetingSequence->getRowsAssoc( $where );
            foreach ($sequenceList as $sequence_info ) {
                if ($sequence_info["record_status"] == "complete" && $sequence_info["record_filepath"]) {
                    if (file_exists(N2MY_MOVIES_DIR.$sequence_info["record_filepath"])) {
                        $this->logger->info(N2MY_MOVIES_DIR.$sequence_info["record_filepath"], "delete movie");
                        unlink(N2MY_MOVIES_DIR.$sequence_info["record_filepath"]);
                    }
                }
            }
        }
    }

    /**
     * 映像の削除
     */
    public function deleteVideo( $meeting_session_id )
    {
        $meetingInfo = $this->obj_Meeting->getRow( sprintf( "meeting_session_id='%s'", $meeting_session_id  ) );
        if( ! $meetingInfo || $meetingInfo["is_locked"] ) return;

        $where = sprintf( "meeting_key='%s' AND has_recorded_video=1", $meetingInfo["meeting_key"] );
        $sequenceList = $this->obj_MeetingSequence->getRowsAssoc( $where );
        for( $i = 0; $i < count( $sequenceList ); $i++ ){
            $this->Core_Meeting->deleteVideo( $sequenceList[$i]["meeting_sequence_key"] );
        }
        // 会議容量
        $meeting_sequence_data = $this->obj_MeetingSequence->getStatus( $meetingInfo["meeting_key"] );
        // 資料容量
        if (($meeting_sequence_data['has_recorded_minutes'] == 0) && ($meeting_sequence_data['has_recorded_video'] == 0)) {
            // 通常
            $objDocument = new N2MY_Document($this->dsn);
            $where = "meeting_key = ". $meetingInfo["meeting_key"];
            $document_list = $objDocument->getList($where);
            $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$document_list);
            foreach($document_list as $document) {
                // 物理削除
                $objDocument->delete($document);
            }
            $document_size = 0;
        } else {
            $document_size = $this->Core_Meeting->getDocumentSize($meetingInfo["meeting_key"]);
        }
        // キャビネット容量
        $cabinet_size = 0;
        $this->logger->info(array(
            "meeting_size_used" => $meeting_sequence_data["meeting_size_used"],
            "document_size" => $document_size,
            "cabinet_size" => $cabinet_size
        ));
        $meetingData = array(
            "has_recorded_video" => $meeting_sequence_data['has_recorded_video'],
            "meeting_size_used" => $meeting_sequence_data["meeting_size_used"] + $document_size + $cabinet_size,
            "update_datetime" => date( "Y-m-d H:i:s" )
        );
        $this->logger->info($meetingData);
        $this->obj_Meeting->update( $meetingData, $where );
    }

    /**
     * 議事録の削除
     */
    public function deleteMinutes( $meeting_session_id )
    {
        $meetingInfo = $this->obj_Meeting->getRow( sprintf( "meeting_session_id='%s'", $meeting_session_id ) );
        if( ! $meetingInfo || $meetingInfo["is_locked"] ) {
            $this->logger->info($meeting_session_id,"meeting is locked");
            return;
        }
        $where = sprintf( "meeting_key='%s' AND has_recorded_minutes=1", $meetingInfo["meeting_key"] );
        $sequenceList = $this->obj_MeetingSequence->getRowsAssoc( $where );
        for( $i = 0; $i < count( $sequenceList ); $i++ ){
            $this->Core_Meeting->deleteMinute( $sequenceList[$i]["meeting_sequence_key"] );
        }
        // 会議容量
        $meeting_sequence_data = $this->obj_MeetingSequence->getStatus( $meetingInfo["meeting_key"] );
        // 資料容量
        if (($meeting_sequence_data['has_recorded_minutes'] == 0) && ($meeting_sequence_data['has_recorded_video'] == 0)) {
            // 通常
            $objDocument = new N2MY_Document($this->dsn);
            $where = "meeting_key = ". $meetingInfo["meeting_key"];
            $document_list = $objDocument->getList($where);
            $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$document_list);
            foreach($document_list as $document) {
                // 物理削除
                $objDocument->delete($document);
            }
            $document_size = 0;
        } else {
            $document_size = $this->Core_Meeting->getDocumentSize($meetingInfo["meeting_key"]);
        }
        // キャビネット容量
        $cabinet_size = 0;
        $this->logger->info(array(
            "meeting_size_used" => $meeting_sequence_data["meeting_size_used"],
            "document_size" => $document_size,
            "cabinet_size" => $cabinet_size
        ));
        $meetingData = array(
            "has_recorded_minutes"  => $meeting_sequence_data['has_recorded_minutes'],
            "meeting_size_used"     => $meeting_sequence_data["meeting_size_used"] + $document_size + $cabinet_size,
            "update_datetime"       => date( "Y-m-d H:i:s" )
        );
        $this->obj_Meeting->update( $meetingData, $where );
    }

    /**
     * 共有メモ削除
     * @param $meeting_key
     * @param $meeting_sequence_key
     * @param $shared_file_key
     */
    public function deleteSharedFile($meeting_key , $meeting_sequence_key , $shared_file_key){
        require_once("classes/dbi/shared_file.dbi.php");
        $objSharedFile = new sharedFileTable($this->dsn);
        $data = array(
                "status" => 0,
                "update_datetime" => date('Y-m-d H:i:s'),
        );
        $where = sprintf("shared_file_key='%s' AND meeting_key='%s' AND meeting_sequence_key='%s'",  $shared_file_key , $meeting_key , $meeting_sequence_key);
        $objSharedFile->update($data, $where);

        //ミーティングシーケンスのメモ数確認
        $where = sprintf("status = 1 AND meeting_sequence_key='%s'",  $meeting_sequence_key);
        $shard_file_count = $objSharedFile->numRows($where);
        // ミーティングシーケンスでメモの数が0だったらテーブル更新
        if($shard_file_count == 0){
            $data = array(
                    "has_shared_memo" => 0,
                    "update_datetime" => date('Y-m-d H:i:s'),
            );
            $where = sprintf("meeting_sequence_key='%s'",  $meeting_sequence_key);
            $this->obj_MeetingSequence->update($data, $where);
        }
        // ミーティングでのメモ数確認
        $where = sprintf("status = 1 AND meeting_key='%s'",  $meeting_key);
        $shard_file_count = $objSharedFile->numRows($where);
        if($shard_file_count == 0){
            $data = array(
                    "has_shared_memo" => 0,
                    "update_datetime" => date('Y-m-d H:i:s'),
            );
            $where = sprintf("meeting_key='%s'",  $meeting_key);
            $this->obj_Meeting->update($data, $where);
        }
    }

    /**
     * 指定した分サイズを引く
     * @param $meeting_sequence_key
     * @param int $size
     */
    public function shorten_meeting_size_used($meeting_sequence_key , $size){
        // サイズが数字じゃなかったらなにもしない
        if(!is_numeric($size)){
            return ;
        }

        $where = sprintf("meeting_sequence_key='%s'",  $meeting_sequence_key);
        $meeting_sequence_info = $this->obj_MeetingSequence->getRow($where);
        $new_size = $meeting_sequence_info["meeting_size_used"] - $size;
        // マイナス防止策
        if($new_size < 0){
            $new_size = 0;
        }
        $data = array(
                "meeting_size_used" => $new_size,
                "update_datetime" => date('Y-m-d H:i:s'),
                );
        $this->obj_MeetingSequence->update($data, $where);

    }

    /**
     * 会議の容量設定＆取得
     */
    public function getMeetingSize($meeting_session_id) {
        $meetingInfo = $this->obj_Meeting->getRow( sprintf( "meeting_session_id='%s'", $meeting_session_id ) );
        // 会議容量
        $meeting_sequence_data = $this->obj_MeetingSequence->getStatus( $meetingInfo["meeting_key"] );
        // 資料容量
        if (($meeting_sequence_data['has_recorded_minutes'] == 0) && ($meeting_sequence_data['has_recorded_video'] == 0)) {
            $document_size = 0;
        } else {
            $document_size = $this->Core_Meeting->getDocumentSize($meetingInfo["meeting_key"]);
        }
        // キャビネット容量
        $cabinet_size = 0;
        // 録画GW容量
        $record_gw_size = 0;
        $where = "meeting_key='".addslashes($meetingInfo["meeting_key"])."'";
        $sequenceList = $this->obj_MeetingSequence->getRowsAssoc( $where );
        foreach ($sequenceList as $key => $sequence_info) {
            if ($sequence_info["record_status"] == "complete") {
                if (file_exists(N2MY_MOVIES_DIR.$sequence_info["record_filepath"])) {
                    $record_gw_size += filesize(N2MY_MOVIES_DIR.$sequence_info["record_filepath"]);
                } else {
                    $this->logger->warn(N2MY_MOVIES_DIR.$sequence_info["record_filepath"], "file not exists");
                }
            }
        }
        $this->logger->info(array(
            "meeting_size_used" => $meeting_sequence_data["meeting_size_used"],
            "document_size"     => $document_size,
            "cabinet_size"      => $cabinet_size,
            "record_gw_size"    => $record_gw_size,
        ));

        $meeting_size = $meeting_sequence_data["meeting_size_used"] + $document_size + $cabinet_size + $record_gw_size;
        $meetingData = array(
            "meeting_size_used" => $meeting_size,
            "update_datetime" => date( "Y-m-d H:i:s" )
        );
        $where = "meeting_key = ". $meetingInfo["meeting_key"];
        $this->obj_Meeting->update( $meetingData, $where );
        return $meeting_size;
    }
}
