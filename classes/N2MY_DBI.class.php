<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once('lib/EZLib/EZDB/EZDB.class.php');

class N2MY_DB extends EZDB {

    function N2MY_DB($dsn, $table = null) {
        $this->init($dsn, $table);
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
