<?php 
class ConferenceRecordObject {
	private $methodName;
	private $confId;
	private $partId;
	private $partName;
	private $partType;
	private $state;
	private $info;
	function __construct(
			$methodName=null,
			$confId=null,
			$partId=null,
			$partName=null,
			$partType=null,
			$state=null,
			$info=null) {
		$this->methodName = $methodName;
		$this->confId = $confId;
		$this->partId = $partId;
		$this->partName = $partName;
		$this->partType = $partType;
		$this->state = $state;
		$this->info = $info;
	}
	
	public function getContents() {
		return $data = array(
				"methodName"=>$this->methodName,
				"confId"=>$this->confId,
				"partId"=>$this->partId,
				"partName"=>$this->partName,
				"partType"=>$this->partType,
				"state"=>$this->state,
				"info"=>$this->info,
				"createdate"=>date("Y-m-d H:i:s").substr((string)microtime(), 1, 8));
	}
}
