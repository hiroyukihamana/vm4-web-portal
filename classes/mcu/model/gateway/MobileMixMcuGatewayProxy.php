<?php
require_once(dirname(__FILE__)."/../../model/gateway/McuGatewayProxy.php"); 

class MobileMixMcuGatewayProxy extends McuGatewayProxy {
	public function __construct() {
		parent::__construct();
		
		$this->mcuServerModel = $this->getMcuServerMobileMixWebApi();
	}
}
