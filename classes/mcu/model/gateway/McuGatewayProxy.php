<?php
require_once("classes/mcu/model/Proxy.php");
require_once("classes/mgm/dbi/mcu_server.dbi.php");
require_once("classes/mgm/dbi/media_mixer.dbi.php");

require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/dbi/meeting.dbi.php");

/**
 * mcu server 情報のproxy
 * 
 * @author akihitowada
 *
 */
class McuGatewayProxy extends Proxy {
	private $settingRecord;
	private $mcuServer;
	private $mediaMixer;
	private $webApi;

	protected $mcuServerModel;
	
	function __construct() {
		parent::__construct();
		
		$this->mcuServer	= new McuServerTable($this->configProxy->getAuthDsn());
		$this->mediaMixer	= new MediaMixerTable($this->configProxy->getAuthDsn());
	}
	
	private function definedMCUController($mcuServerModel) {
		$this->mcuServerModel = $mcuServerModel;
		$wsdl = $this->getMCUControllerServer();
		define("WSDL_DOMAIN", $wsdl);
	}
	
	public function getSocketServer() {
		return $this->mcuServerModel["server_address"];
	}
	
	public function getSocketPort() {
		return $this->mcuServerModel["socket_port"];
	}
	
	public function getServerDomain() {
		return $this->mcuServerModel["server_address"];
	}
	
	public function getSipProxy() {
		return $this->mcuServerModel["server_address"];
	}
	
	public function getSipProxyPort() {
		return $this->mcuServerModel["sip_proxy_port"];
	}
	
	public function getMCUServer() {
		return $this->mcuServerModel["server_address"];
	}

	public function getRegularIP() {
		return $this->mcuServerModel["ip"];
	}
	
	public function getMCUControllerServer() {
		$return = null;
		if ($this->mcuServerModel)
			$return = sprintf("%s:%d", $this->mcuServerModel["server_address"], $this->mcuServerModel["controller_port"]);
		return $return;
	}
	
	public function getRegularDomain() {
		return $this->mcuServerModel["server_address"];
	}
	
	public function getRegularNumberDomain() {
		return $this->mcuServerModel["number_domain"];
	}
	
	public function getH323CalloutDomain() {
		return $this->mcuServerModel["h323_callout_domain"];
	}
	
	public function getH323GuestNumberDomain() {
		return $this->mcuServerModel["guest_h323_number_domain"];
	}
	
	public function guestH323Domain() {
		return $this->mcuServerModel["guest_h323_domain"];
	}
	
	public function guestSipDomain() {
		return $this->mcuServerModel["guest_domain"];
	}
	
	public function guestSipNumberDomain() {
		return $this->mcuServerModel["guest_number_domain"];
	}
	
	private function getWebApi($condition) {
		return $this->webApi = $this->configProxy->parser->getWebApiModel($condition);
	}
	
	private function getWebApiByTargetVersion($version) {
		return $this->webApi = $this->configProxy->parser->getWebApiModel(array("targetVersion"=>$version));
	}
	
	
	public function getMcuServerByServerKeyThrowable($key) {
		$mcuServer = $this->mcuServer->getRow(sprintf("server_key = '%s'", $key));
		if (!$mcuServer) {
			throw new Exception("no mcu server with server_key: ".$key);
		}
		else if (!$mcuServer["is_available"]) {
			throw new Exception("mcu server is not active. server_key: ".$key);
		}
		$webApi = $this->getWebApiByTargetVersion($mcuServer["target_web_api_version"]);
		if (!$mcuServer || !$webApi) {
			$this->logger->error("check mcu server status. ip:".$ip);
		}
		return array_merge($mcuServer, $webApi);
	}
	
	public function getMcuServerByServerKey($key) {
		$mcuServer = $this->mcuServer->getRow(sprintf("server_key = '%s'", $key));
		if (!$mcuServer) {
			$this->logger->error("no mcu server with server_key: ".$key);
		}
		$webApi = $this->getWebApiByTargetVersion($mcuServer["target_web_api_version"]);
		if (!$mcuServer || !$webApi) {
			$this->logger->error("check mcu server status. ip:".$ip);
		}
		return array_merge($mcuServer, $webApi);
	}
	
	/**
	 * 
	 * @param string $ip
	 */
	public function setMcuServerByServerIp($ip) {
		$mcuServer = $this->mcuServer->getRow(sprintf("ip = '%s'", $ip));
		if (!$mcuServer) {
			$this->logger->error("no mcu server with ip: ".$ip);
		}
		$webApi = $this->getWebApiByTargetVersion($mcuServer["target_web_api_version"]);
		if (!$mcuServer || !$webApi) {
			$this->logger->error("check mcu server status. ip:".$ip);
		}
		$mcuServerModel = array_merge($mcuServer, $webApi);
		$this->definedMCUController($mcuServerModel);
	}
	
	/**
	 * 
	 * @param string $ip
	 * @throws Exception
	 */
	public function setMcuServerByServerIpThrowable($ip) {
		$mcuServer = $this->mcuServer->getRow(sprintf("ip = '%s'", $ip));
		if (!$mcuServer) {
			throw new Exception("no mcu server with ip: ".$ip);
		}
		else if (!$mcuServer["is_available"]) {
			throw new Exception("mcu server is not active. ip: ".$ip);
		}
		$webApi = $this->getWebApiByTargetVersion($mcuServer["target_web_api_version"]);
		if (!$mcuServer || !$webApi) {
			$this->logger->error("check mcu server status. ip:".$ip);
		}
		$mcuServerModel = array_merge($mcuServer, $webApi);
		$this->definedMCUController($mcuServerModel);
	}
	
	public function getMcuServerModel() {
		return $this->mcuServerModel;
	}
	
	public function getMobileMixerRecordByMcuServerKey($key=null) {
		return $this->mediaMixer->getMobileMixerRecord($key);
	}
	
	public function getStbMobileMixerRecordByMcuServerKey($key=null) {
		return $this->mediaMixer->getStbMobileMixerRecord($key);
	}

	public function getVideoConferenceMixerRecordByMcuServerKey($key=null) {
		return $this->mediaMixer->getConferenceMixerRecord($key);
	}
	
	/**
	 * 
	 * @param string $roomKey
	 */
	public function setMcuServerModelByRoomKey($roomKey, $type=null) {
		$setting = new IvesSettingTable($this->configProxy->getDsn());
		$settingRecord = $setting->findByRoomKey($roomKey);
		if (count($settingRecord) > 0) {
			$mcuServerModel = $this->getMcuServerByServerKey($settingRecord[0]["mcu_server_key"]);
		}
		else {
			$mcuServerModel = $this->getLatestModel($type);
		}

		$this->definedMCUController($mcuServerModel);
	}

	/**
	 * 
	 * @param unknown $roomKey
	 */
	public function setMcuServerModelByRoomKeyThrowable($roomKey) {
		$setting = new IvesSettingTable($this->configProxy->getDsn());
		$settingRecord = $setting->findByRoomKey($roomKey);
		if (count($settingRecord) > 0) {
			$mcuServerModel = $this->getMcuServerByServerKey($settingRecord[0]["mcu_server_key"]);
		}
		else {
			$mcuServerModel = $this->getLatestModel();
		}
		if (!$mcuServerModel || !$mcuServerModel["is_available"])
			throw new Exception(__CLASS__.":".__FUNCTION__.":active mcu server record is not exist. roomKey: ".$roomKey);
		
		$this->definedMCUController($mcuServerModel);
	}
	
	/**
	 * <p></p>
	 * @param int $meetingKey
	 */
	public function setMcuServerModelByMeetingKey($meetingKey) {
		$meeting		= new MeetingTable($this->configProxy->getDsn());
        $meetingRecord	= $meeting->findActiveMeetingByMeetingKey($meetingKey);
        $type = null;
        if ($meetingRecord["use_stb_option"]) {
        	$type = "stb";
        }
        $this->setMcuServerModelByRoomKey($meetingRecord["room_key"], $type);
	}
	
	/**
	 * 
	 */
	public function setLatestModel($type=null) {
		$mcuServerModel = $this->getLatestModel($type);
		$this->definedMCUController($mcuServerModel);
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public function setLatestModelThrowable($type=null) {
		$webApi = $this->configProxy->parser->getLatestWebApiModel();
		$where = sprintf("target_web_api_version = '%s' AND use_mobile_mix = 1 AND is_available = 1", $webApi["targetVersion"]);
		if ($type == "stb") {
			$where .= " AND use_stb = 1";
		}
		$mcuServer = $this->mcuServer->getRow($where);
		if (!$mcuServer)
			throw new Exception(__CLASS__.":".__FUNCTION__.":active mcu server record is not exist");
		$this->definedMCUController(array_merge($mcuServer, $webApi));
	}
	
	private function getLatestModel($type=null) {
		$webApi = $this->configProxy->parser->getLatestWebApiModel();
		$where = sprintf("target_web_api_version = '%s' AND use_mobile_mix = 1 AND is_available = 1", $webApi["targetVersion"]);
		if ($type == "stb") {
			$where .= " AND use_stb = 1";
		}
		$mcuServer = $this->mcuServer->getRow($where);
		return array_merge($mcuServer, $webApi);
	}
	
	public function __get($key) {
		return $this->$key;
	}
}
