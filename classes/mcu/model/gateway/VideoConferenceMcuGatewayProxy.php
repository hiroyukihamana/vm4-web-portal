<?php
require_once(dirname(__FILE__)."/../../model/gateway/McuGatewayProxy.php"); 

class VideoConferenceMcuGatewayProxy extends McuGatewayProxy {
	function __construct($settingRecord) {
		parent::__construct();

		$this->mcuServerModel = $this->getMcuServerByServerKey($settingRecord["mcu_server_key"]);
		
		$this->definedMCUController();
	}
}
