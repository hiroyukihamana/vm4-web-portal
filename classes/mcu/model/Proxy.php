<?php
require_once("classes/mcu/config/McuConfigProxy.php");

abstract class Proxy {
	protected $config;
	protected $logger;
	protected $dsn;
	
	function __construct() {
		$this->configProxy 	= new McuConfigProxy();
		$this->dsn = $this->configProxy->getDsn();
		$this->logger = EZLogger2::getInstance();
	}
}
