<?php 

require_once("classes/mcu/config/McuConfigProxy.php");

class MCUCurl {
	private $logger = "";
	private $config;
	
	function __construct() {
		$this->configProxy 	= new McuConfigProxy();
		$this->logger = EZLogger2::getInstance();
	}
	
	public function post($url, $confId, $imagePath) {
		$param = array("image" => "@".$imagePath);
		$api = sprintf("http://%s:9090%s/%s",$url, $this->configProxy->get("document_sharing_upload_path"), $confId);
		
		$ch = curl_init($api);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		curl_exec($ch);
		$respons = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		curl_close($ch);
	}
}
