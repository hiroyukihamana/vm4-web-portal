<?php
require_once("classes/dbi/conference_record.dbi.php");
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/vo/ConferenceRecordObject.php");

define('CONFERENCE_LOGGER_TRACE',	0);
define('CONFERENCE_LOGGER_DEBUG',	1);
define('CONFERENCE_LOGGER_INFO',	2);
define('CONFERENCE_LOGGER_WARN',	3);
define('CONFERENCE_LOGGER_ERROR',	4);
define('CONFERENCE_LOGGER_FATAL',	5);

class ConferenceLogger {
	private static $instance = null;
	
	private static function _init() {
		if (self::$instance === null) {
			self::$instance = new _ConferenceLogger();
		}
		
		if (!defined('CONFERENCE_LOGGER_LVL'))
			define('CONFERENCE_LOGGER_LVL',	CONFERENCE_LOGGER_INFO);
	}
	
	public static function trace(
			$methodName=null,
			$confId=null,
			$partId=null,
			$partName=null,
			$partType=null,
			$state=null,
			$info=null) {
		self::_init();
		
		if (CONFERENCE_LOGGER_LVL > CONFERENCE_LOGGER_TRACE) return;

		$obj = new ConferenceRecordObject($methodName, $confId, $partId, $partName, $partType, $state, $info ? serialize($info) : null);
		self::$instance->insert($obj);
	}
	
	public static function debug(
			$methodName=null,
			$confId=null,
			$partId=null,
			$partName=null,
			$partType=null,
			$state=null,
			$info=null) {
		self::_init();
		
		if (CONFERENCE_LOGGER_LVL > CONFERENCE_LOGGER_DEBUG) return;

		$obj = new ConferenceRecordObject($methodName, $confId, $partId, $partName, $partType, $state, $info ? serialize($info) : null);
		self::$instance->insert($obj);
	}
	
	public static function info(
			$methodName=null,
			$confId=null,
			$partId=null,
			$partName=null,
			$partType=null,
			$state=null,
			$info=null) {
		self::_init();
		
		if (CONFERENCE_LOGGER_LVL > CONFERENCE_LOGGER_INFO) return;

		$obj = new ConferenceRecordObject($methodName, $confId, $partId, $partName, $partType, $state, $info ? serialize($info) : null);
		self::$instance->insert($obj);
		
	}
}

class _ConferenceLogger {
	private $configProxy;
	private $dsn;
	private $conferenceRecord;
	function __construct() {
		$this->configProxy 	= new McuConfigProxy();
		$this->dsn			= $this->configProxy->getDsn();
		$this->logger		= EZLogger2::getInstance();

		$this->conferenceRecord	= new ConferenceRecordTable($this->dsn);
	}
	
	function insert($obj) {
		$this->conferenceRecord->add($obj->getContents());
	}
}
