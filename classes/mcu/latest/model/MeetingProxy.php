<?php
require_once("classes/mcu/model/Proxy.php");

require_once("classes/core/Core_Meeting.class.php");
require_once("classes/core/dbi/DataCenter.dbi.php");
require_once('classes/core/dbi/MeetingOptions.dbi.php');
require_once('classes/core/dbi/MeetingSequence.dbi.php');
require_once "classes/core/dbi/MeetingUptime.dbi.php";
require_once("classes/core/dbi/Participant.dbi.php");
require_once('classes/core/dbi/SharingServer.dbi.php');

require_once("classes/dbi/datacenter_ignore.dbi.php");
require_once("classes/dbi/datacenter_priority.dbi.php");
require_once("classes/dbi/fms_deny.dbi.php");
require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/dbi/reservation.dbi.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/operation_log.dbi.php");
require_once('classes/mgm/dbi/FmsServer.dbi.php');

require_once("classes/N2MY_Meeting.class.php");

class MeetingProxy extends Proxy {
	private $meeting;
	private $participant;
	static $sessionLength = 20;
	
	private $activeMeetingRecord	= null;
	private $activeSpeaker	= false;
	private $meetingRecord	= null;
	private $roomRecord		= null;
	private $settingRecord	= null;
	private $activeParticipantRecordList = array();
	private $operationLog   = null;
	
	function __construct() {
		parent::__construct();
		$this->coreMeeting	= new Core_Meeting($this->dsn);
		$this->n2myMeeting	= new N2MY_Meeting($this->dsn);

		$this->dataCenter		= new DBI_DataCenter(N2MY_MDB_DSN);
		$this->datacenterIgnore	= new DatacenterIgnoreTable($this->dsn);
		$this->datacenterPriority= new DatacenterPriorityTable($this->dsn);
		$this->fmsDeny			= new FmsDenyTable($this->dsn);
		$this->fmsServer		= new DBI_FmsServer(N2MY_MDB_DSN);
		$this->meeting			= new MeetingTable($this->dsn);
		$this->meetingOptions 	= new DBI_MeetingOptions($this->dsn);
		$this->meetingSequence	= new DBI_MeetingSequence($this->dsn);
		$this->meetingUptime	= new DBI_MeetingUptime($this->dsn);
		$this->participant		= new DBI_Participant($this->dsn);
		$this->room				= new RoomTable($this->dsn);
		$this->reserve			= new ReservationTable($this->dsn);
		$this->setting			= new IvesSettingTable($this->dsn);
        $this->sharingServer	= new DBI_SharingServer($this->dsn);
        $this->user  			= new UserTable($this->dsn);
        $this->operationLog     = new OperationLogTable($this->dsn);
	}
	
	static private $instance = null;
	public static function getInstance() {
		if (!self::$instance)
			self::$instance = new self;
		return self::$instance;
	}
	
	/**
	 * @param room object $roomRecord
	 * @return meeting record.
	 */
	public function getMeetingByRoomRecord($roomRecord /* room dbi */) {
        $meeting = $this->n2myMeeting->getMeeting($roomRecord["room_key"], "polycom", $roomRecord["meeting_key"]);
        // アクティブ会議無し && 現在時刻に予約無し
        if( !$meeting || !$meeting["last_meeting_status"] && !$meeting['reservation_session'] ) return null;
        
        return $this->meetingRecord = $this->meeting->findByMeetingTicket($meeting["meeting_key"]);
	}
	
	/**
	 * <p></p>
	 * @param int $meetingKey
	 * @return meeting record.
	 */
	public function getMeetingByMeetingKey($meetingKey) {
        $this->meetingRecord	= $this->meeting->findActiveMeetingByMeetingKey($meetingKey);
        return $this->meetingRecord;
	}

	/**
	 * <p>
	 * meeting tableよりアクティブなレコードを取得後モデルで保持
	 * </p>
	 * @param int $meetingKey
	 * @throws Exception
	 */
	public function setActiveMeetingRecordByMeetingKeyThrowable($meetingKey) {
		if ($this->activeMeetingRecord) return;
		$meetingRecord	= $this->getMeetingByMeetingKey($meetingKey);
		if (!$meetingRecord)
			throw new Exception(__CLASS__."::".__FUNCTION__."::no active meeting record. meetingKey: ".$meetingKey);
		$this->activeMeetingRecord = $meetingRecord;
	}
	
	public function getParticipantByMeetingKey($meetingKey) {
		$list = $this->participant->getActiveParticipantListByMeetingKey($meetingKey);
		return $list;
	}
	
	/**
	 * 
	 * @param int $participantKey
	 * @throws Exception
	 * @return participant record.
	 */
	public function getActiveParticipantByParticipantKeyWithThrowableException($participantKey) {
		$record = $this->participant->getActiveParticipantByParticipantKey($participantKey);
		if (!$record)
			throw new Exception("no record of active participant. key:".$participantKey);
		
		return $record;
	} 
	
	public function createMeeting($roomRecord /* room dbi*/) {
		$meeting_key		= $roomRecord["room_key"]."_".md5(uniqid(rand(), true));
		$options = array(
				"user_key"			=> $roomRecord["user_key"],
				"country_id"		=> "jp",
				"start_time"		=> date("Y-m-d H:i:s")
		);
		$meeting_session_id = $this->n2myMeeting->createMeeting($roomRecord["room_key"], $meeting_key, $options);
		$this->room->last_meeting_update($roomRecord["room_key"], $meeting_key);
		
		$this->meetingRecord = $this->meeting->findByMeetingSessionId($meeting_session_id);
		return $this->meetingRecord;
	}
	
	public function createMeetingSequenceRecord() {
		if ($this->meetingRecord && $this->meetingRecord["meeting_key"] &&$this->meetingRecord["server_key"]) {
			$data = array(
									"meeting_key" => $this->meetingRecord["meeting_key"],
									"server_key"  => $this->meetingRecord["server_key"]
					);
			//add server seaquence
			$meeting_sequence_key = $this->meetingSequence->add($data);
		}
		else {
			$this->logger->error($this->meetingRecord);
			throw new Exception("fail to create meeting_sequence_record caused by no meeting record");
		}
	}
	
	public function resolveEnvironmentData($meetingRecord) {
        $userRecord = $this->user->getRow("user_key = '".addslashes($meetingRecord["user_key"])."'", "user_id,is_compulsion_pw,compulsion_pw,meeting_version");
		if (!$userRecord["meeting_version"]) {
			$version = "4.6.5.0";
		} else {
			$version = $this->coreMeeting->getVersion();
		}
		
		// resolve sharing server
		$sharing_server_key = $this->sharingServer->getKeyByPriority($meetingRecord['meeting_country'], $ssl);
		$server_key = $this->resolveFMSServer($meetingRecord);

		$server_data = array(
				"version"            => $version,
				"fms_path"           => "mtg".date("Y/m/d/"),
				"server_key"         => $server_key,
				"sharing_server_key" => $sharing_server_key,
		);
        // 強制パスワード設定
        if (!$meetingRecord["meeting_log_password"] && $userRecord["is_compulsion_pw"] == "1") {
            $this->logger2->info($userRecord, "パスワード強制設定");
            $server_data["meeting_log_password"] = $userRecord["compulsion_pw"];
        }
		$where = "meeting_key = ".addslashes($meetingRecord['meeting_key']);
		$this->meeting->update($server_data, $where);
		$this->meetingRecord = $this->getMeetingByMeetingKey($meetingRecord['meeting_key']);
		return $this->meetingRecord;
	}
	
	/**
	 * 
	 * @param unknown $meetingRecord
	 * @return boolean
	 * @since Core_Meeting::getMeetingDetailsからの移植
	 */
	private function resolveFMSServer($meetingRecord) {
		// SSLオプション
		$options = $this->meetingOptions->getList($meetingRecord["meeting_key"]);
		$ssl     = 0;
		$use_chinaline_status = 0;
		foreach($options as $option) {
			if ($option["option_name"] == "ssl") {
				$ssl = 1;
			}
			if ($option["option_name"] == "ChinaFastLine") {
				$use_chinaline_status = 1;
			}
			if ($option["option_name"] == "ChinaFastLine" && $option["option_name"] == "ChinaFastLine2") {
				$use_chinaline_status = 2;
			}
			if ($option["option_name"] == "GlobalLink") {
				$use_global_link = 1;
			}
		}
		//利用しない(利用停止中の)FMSを取得
		try {
			$_fms_deny_keys = $this->fmsDeny->findByUserKey($meetingRecord["user_key"], null, null, 0, "fms_server_key");
		} catch (Exception $e) {
			$this->logger2->error($e->getMessage());
			return false;
		}
		$fms_deny_keys = array();
		if ($_fms_deny_keys) {
			foreach ($_fms_deny_keys as $fms_key) {
				$fms_deny_keys[] = $fms_key["fms_server_key"];
			}
		}
		
		//除外DC
		$ignore_dc = $this->datacenterIgnore->get_datacenter_ignore($meetingRecord["user_key"]);
		if ($use_global_link) {
			$fms_country_priority = $this->configProxy->getWithGroupName("FMS_COUNTRY_PRIORITY",$meetingRecord["meeting_country"]);
			if (!$fms_country_priority) {
				$fms_country_priority = $this->configProxy->getWithGroupName("FMS_COUNTRY_PRIORITY","etc");
			}
			$datacenter_country_list = split(',', $fms_country_priority);
			$server_key = $this->fmsServer->getGlobalLinkFmsList($ssl, $fms_deny_keys, $datacenter_country_list ,$meetingRecord["need_fms_version"]);
		}
		else {
			//優先DC
			$priority_dc	=	$this->datacenterPriority->get_datacenter_priority($meetingRecord["user_key"],$meetingRecord["meeting_country"]);
			$priority_dc_all=	$this->datacenterPriority->get_datacenter_priority($meetingRecord["user_key"],"all");
			$priority_dc = array_merge($priority_dc, $priority_dc_all);
			
			//所在地以外のDCリスト取得
			$fms_country_priority = $this->configProxy->getWithGroupName("FMS_COUNTRY_PRIORITY",$meetingRecord["meeting_country"]);
			if (!$fms_country_priority) {
				$fms_country_priority = $this->configProxy->getWithGroupName("FMS_COUNTRY_PRIORITY","etc");
			}
			$datacenter_country_list = split(',', $fms_country_priority);
			$datacenter_list = array();
			foreach ( $datacenter_country_list as $datacenter_country) {
				if ($meetingRecord["meeting_country"] != $datacenter_country_list) {
					$country_priority_dc_list =
					$this->datacenterPriority->get_datacenter_priority($meetingRecord["user_key"],$datacenter_country);
				}
				if ($country_priority_dc_list) {
					if ($ignore_dc) {
						$ignore_dc = array_merge($ignore_dc, $country_priority_dc_list);
					}
				}
				$country_dc_list =	$this->dataCenter->getKeyByCountry($datacenter_country, $ignore_dc,$data["account_model"],$use_chinaline_status);
				if ($country_dc_list) {
					foreach( $country_dc_list as $country_dc ){
						$datacenter_list[] = $country_dc;
					}
				}
				$datacenter_list =	array_merge($country_priority_dc_list, $datacenter_list);
			}
			$datacenter_list = array_merge($priority_dc,$datacenter_list);
			$datacenter_list = array_map('serialize', $datacenter_list);
			$datacenter_list = array_unique($datacenter_list);
			$datacenter_list = array_map('unserialize', $datacenter_list);
			if ($datacenter_list) {
				$server_key = $this->fmsServer->getKeyByDataCenterList($ssl, $fms_deny_keys, $datacenter_list, $meetingRecord["need_fms_version"]);
			}
		}
		return $server_key;
	}
	
	public function getSessionId() {
		$sCharList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
		mt_srand();
		$sRes = "";
		for($i = 0; $i < self::$sessionLength; $i++)
			$sRes .= $sCharList[mt_rand(0, strlen($sCharList) - 1)];
			return $sRes;
	}
	
	
	public function addParticipant($data) {
		$result = $this->participant->addPolycomUser($data);
		return $result;
	}

	/**
	 *
	 * @param string $did
	 * @return setting object
	 */
	public function getSettingRecordByRoomKey($roomKey) {
		$result = $this->setting->findByRoomKey($roomKey);
		return $result[0];
	}

	/**
	 * 
	 * @param string $roomKey
	 */
	public function setSettingRecordByRoomKey($roomKey) {
		if ($this->settingRecord) return;
		$result = $this->getSettingRecordByRoomKey($roomKey);
		$this->settingRecord = $result;
		$this->activeSpeaker = $this->settingRecord["use_active_speaker"];
	}
	
	/**
	 * 
	 * @param string $did
	 * @return setting object
	 */
	public function getSettingRecordByDid($did) {
		$result = $this->setting->findByDid($did);
		
		// fix teleconference
		if ( !$result ) {
			require_once 'classes/dbi/video_conference.dbi.php';
			$dbiVideoConference = new VideoConferenceTable($this->dsn);
			$conference_info = $dbiVideoConference->getRow( sprintf( 'did="%s" AND is_active=1', $did ) );
			
			// check option
			require_once 'classes/dbi/ordered_service_option.dbi.php';
			$dbi_service_option = new OrderedServiceOptionTable( $this->dsn );
			if ( $dbi_service_option->enableOnRoom( 23, $conference_info['room_key'] ) ) {
				// convert to ives_setting
				$ives_setting = array();
				$ives_setting['mcu_server_key'] = $conference_info['mcu_server_key'];
				$ives_setting['media_mixer_key'] = $conference_info['media_mixer_key'];
				$ives_setting['user_key'] = $conference_info['user_key'];
				$ives_setting['room_key'] = $conference_info['room_key'];
				$ives_setting['ives_did'] = $did;
				$ives_setting['num_profiles'] = 1;
				$result = array($ives_setting);
			}
		}

		return $result;
	}
	
	/**
	 * @param vector<conference participant record>
	 * 
	 */
	public function clearParticipantByConferenceParticipantList($list) {
		if (0 == count($list))
			return;
		$keys = array();
		while (list($key, $value) = each($list)) {
			$keys[] = "participant_key = ".$value["participant_key"];
		}
		$data = array(
					"is_active"=>0,
					"update_datetime"=>date("Y-m-d H:i:s"));
		$where = implode(" OR ", $keys);
		$this->participant->update($data, $where);
	}
	
	/**
	 * 
	 * @param string $roomKey
	 * @return room record object.
	 */
	public function getRoomRecordByRoomKey($roomKey) {
		if (!$this->roomRecord)
			$this->roomRecord = $this->room->findByKey($roomKey);
		return $this->roomRecord;
	}
	
	/**
	 * 
	 * @param string $roomKey
	 * @throws Exception
	 */
	public function setRoomRecordByRoomKeyThrowable($roomKey) {
		if ($this->roomRecord) return;
		$roomRecord = $this->getRoomRecordByRoomKey($roomKey);
		if (!$roomRecord)
			throw new Exception(__CLASS__."::".__FUNCTION__."::irregal roomKey: ".$roomKey);
		$this->roomRecord = $roomRecord;
	}
	
    public function removeParticipantByParticipantKey($participantKey) {
        if(!$participantKey){
            $this->logger->warn(__FUNCTION__."#participantKey doesn't exist",__FILE__,__LINE__);
            return false;
        }
        $participantInfo = $this->participant->getActiveParticipantByParticipantKey($participantKey);
        $meetingInfo     = $this->getMeetingByMeetingKey($participantInfo['meeting_key']);
        $data = array(
            "is_active"       => 0,
            "update_datetime" => date("Y-m-d H:i:s"));
        $where = sprintf("participant_key=%d", $participantKey);
        $this->participant->update($data, $where);
        $operationLogData = array(
            'user_key'           => $participantInfo['user_key'],
            'remote_addr'        => '',
            'action_name'        => 'meeting_out',
            'operation_datetime' => date('Y-m-d H:i:s'),
            'info'               => serialize(array(
                "participant_name" => $participantInfo['participant_name'],
                "room_key"         => $meetingInfo['room_key'],
            )),
        );
        $this->operationLog->add($operationLogData);
	}
	
	public function stopMeeting($meetingKey) {
		$this->coreMeeting->stopMeeting($meetingKey);
		/**
		 * polycom onlyの会議ははmeeting_use_log上で利用時間を管理していないため
		 * meeting_uptimeからデータをかき集めてきてアップデートをかける
		*/
		$useList = $this->meetingUptime->getRowsAssoc(sprintf("meeting_key=%s", $meetingKey));
		$usetime = 0;
		foreach ($useList as $key => $uptime) {
			$usage = strtotime($uptime["meeting_uptime_stop"]) - strtotime($uptime["meeting_uptime_start"]);
			if ($usage > 0) {
				$usetime += ceil($usage / 60);
			}
		}
		$meeting_sequence_status = $this->meetingSequence->getStatus($meetingKey);
		$meeting_sequence_status["meeting_use_minute"]  = $usetime;
		$where = "meeting_key = ".$meetingKey;
		$this->meeting->update($meeting_sequence_status, $where);		
	}
	
	public function stopCount($meetingKey) {
		$this->meetingUptime->stop($meetingKey);
	}
	
	public function createDid() {
		function makeRandStr($length) {
			$str = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z"'));
			for ($i = 0; $i < $length; $i++) {
				$r_str .= $str[rand(0, count($str))];
			}
			return $r_str;
		}
        try{
            do {
                $did = makeRandStr($this->configProxy->get("didLength"));
                $res	= $this->setting->findByDid($did);
                if (PEAR::isError($res)) {
                    throw new Exception("select ives_setting did failed PEAR said : ".$did);
                }
                if ($res) {
                    continue;
                }
                break;
            }
            while (true);
        	return $did;
        }
        catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            throw $e;
        }
	}
	
	/**
	 * FlashGateway.startmeeting.
	 * @param int $meetingKey
	 */
	public function startMeeting($meetingKey) {
		$this->meetingUptime->start($meetingKey); // 利用期間テーブル更新
		$meetingUptimeRecord = $this->meetingUptime->getActiveInfo($meetingKey); // 実利用開始日時取得
		// 会議内容更新
		$meetingRecord = $this->getMeetingByMeetingKey($meetingKey);
		$data["use_flg"] = 1;
		if (!$meetingRecord["actual_start_datetime"]) {
			$data["actual_start_datetime"] = $meetingUptimeRecord["meeting_uptime_start"];
		}
		$data["update_datetime"] = date("Y-m-d H:i:s");
		$where = "meeting_key = ".$meetingKey;
		$this->meeting->update($data, $where);
	}
	
	public function updateConferenceClientUseCount($meetingKey) {
		$where = "meeting_key = '".$meetingKey."'".
				" AND is_active = 1";
		$data = array("use_count" => 1);
		$this->participant->update($data, $where);
	}
	
	public function getUsageTimeFromMeetingUpTime($meetingKey) {		 
		$useList = $this->meetingUptime->getRowsAssoc(sprintf("meeting_key=%s", $meetingKey));
		$usetime = 0;
		foreach ($useList as $key => $uptime) {
			$usage = strtotime($uptime["meeting_uptime_stop"]) - strtotime($uptime["meeting_uptime_start"]);
			if ($usage > 0) {
				$usetime += ceil($usage / 60);
			}
		}
		return $usetime;
	}
	
	public function updateListWithoutPolycom($meetingKey, $participantList) {
		$result = $this->participant->updateListWithoutPolycom($meetingKey, $participantList);
		return $result;		
	}
	
	public function getReservationRecordByMeetingKey($meetingKey) {
		$where	= "meeting_key = '".addslashes($meetingKey)."'".
				" AND reservation_status = 1";
		return $this->reserve->getRow($where, "*");
	}
	
	/**
	 * 
	 * @param unknown $meetingRecord
	 */
	public function setMeetingRecord($meetingRecord) {
		$this->meetingRecord = $meetingRecord;
	}
	
	/**
	 * 
	 * @param int $meetingKey
	 * @return boolean
	 */
	public function isSalesConferenceByMeetingKey($meetingKey) {
		$this->meetingRecord = $this->meeting->findActiveMeetingByMeetingKey($meetingKey);
		return ($this->meetingRecord && $this->meetingRecord["use_sales_option"]);
	}
	
	public function getMeetingSequenceActiveRecord($meetingKey) {
		$sequenceRecord = $this->meetingSequence->getActiveSequenceInfo($meetingKey);
		return $sequenceRecord;
	}
	
	public function getFMSServerDomain() {
		if ($this->meetingRecord) {
			$sequenceRecord = $this->getMeetingSequenceActiveRecord($this->meetingRecord["meeting_key"]);
			if ($sequenceRecord) {
				$record = $this->fmsServer->getServerRecordByServerKey($sequenceRecord["server_key"]);
				if ($record)
					return $record["server_address"];
				else
					throw new Exception("MeetingProxy::getFMSServerDomain::fail to resolve fms server by server_key: ".$sequenceRecord["server_key"]);
			}
			else
				throw new Exception("MeetingProxy::getFMSServerDomain::fail to resolve meeting_sequence record. meeting_key: ".$this->meetingRecord["meeting_key"]);
		}
		else 
			throw new Exception("MeetingProxy::getFMSServerDomain::fail to resolve fms server because of no meeting record.");
	}
	
	public function getFMSInstancePath() {
		$path = "";
		if ($this->meetingRecord) {
			$path  = $this->configProxy->getWithGroupName("CORE", "app_name")."/".$this->meetingRecord["fms_path"].$this->meetingRecord["meeting_session_id"];
		}
		return $path;
	}
	
	/**
	 * <p></p>
	 * @param int $userKey
	 * @throws Exception
	 * @return user record object.
	 */
	public function getActiveUserRecordByUserKeyWithThrowableException($userKey) {
		if (!$userKey)
			throw new Exception(__CLASS__."::".__FUNCTION__."::no user key.");
		$userRecord = $this->getUserRecordByUserKey($userKey);
		if (!$userRecord || $userRecord["user_status"] != 1)
			throw new Exception(__CLASS__."::".__FUNCTION__."::confirm user record. user_key: ".$userKey);
		return $userRecord;		
	}
	/**
	 * <p></p>
	 * @param int $userKey
	 * @throws Exception
	 * @return all activeParticipants
	 */
	public function getAllParticipantListsByUserKey($userKey) {
		$allParticipantsList = $this->participant->getAllActiveParticipantsByUserKey($userKey);
		return count($allParticipantsList);	
	}
	
	private function getUserRecordByUserKey($userKey) {
		$userRecord = $this->user->getRow("user_key = '".addslashes($userKey)."'");
		return $userRecord; 
	}
	
	public function getReservation($meeting_key){
		$meeting_info = $this->getMeetingByMeetingKey($meeting_key);
		require_once 'classes/dbi/reservation.dbi.php';
		$dbi_reservation = new ReservationTable( $this->dsn );
		return $dbi_reservation->getRow( sprintf( 'meeting_key="%s"', $meeting_info['meeting_ticket'] ) );
	}
	
	/**
	 * 
	 * @param object $key
	 * @return object;
	 */
	public function __get($key) {
		return $this->$key;
	}
}
