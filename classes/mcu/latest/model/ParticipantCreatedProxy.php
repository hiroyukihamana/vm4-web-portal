<?php
require_once("classes/mcu/model/Proxy.php");
require_once(dirname(__FILE__)."/../model/vo/ParticipantCreatedRequestObject.php");

class ParticipantCreatedProxy extends Proxy {
	private $did;
	private $dataObject;
	function __construct($request) {
		$this->dataObject = new ParticipantCreatedRequestObject($request);
	}
	
	public function setDid($did) {
		$this->did = $did;
	}
	
	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return $this->dataObject->$key;
		}
	}
}
