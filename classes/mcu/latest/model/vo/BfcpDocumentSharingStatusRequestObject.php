<?php 
class BfcpDocumentSharingStatusRequestObject {
	private $confId;
	private $partId;
	private $status;
	private $reason;

	function __construct($request) {
		$this->confId	= $request->confId;
		$this->partId	= $request->partId;
		$this->status	= $request->status;
		$this->reason	= $request->reason;
	}

	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return null;
		}
	}
}
