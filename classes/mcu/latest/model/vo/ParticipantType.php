<?php 
class ParticipantType {
	static $FLASH_VIEWER	= array(
			"type"=>"FLASH_VIEWER",	
			"partType"=>"SIP",
			"visible"=>false);
	static $SMALL_VIEWER	= array(
			"type"=>"SMALL_VIEWER",	
			"partType"=>"SIP",
			"visible"=>false);
	static $FLASH_PARTICIPANT	= array(
			"type"=>"FLASH_PARTICIPANT",	
			"partType"=>"web",
			"visible"=>true);
	static $CONFERENCE_PARTICIPANT	= array(
			"type"=>"CONFERENCE_PARTICIPANT",	
			"partType"=>"SIP",
			"visible"=>true);
}