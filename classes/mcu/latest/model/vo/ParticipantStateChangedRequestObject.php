<?php
class ParticipantStateChangedRequestObject {
	private $confId;
	private $data;
	private $partId;
	private $partState;
	
	function __construct($request) {
		$this->confId	= $request->confId;
		$this->partId	= $request->partId;
		$this->partState	=$request->state; 
		$this->data		= $request->param;
	}
	
	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return null;
		}
	}
}
