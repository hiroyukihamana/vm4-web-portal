<?php
require_once("classes/mcu/model/Proxy.php");

require_once(dirname(__FILE__)."/../../lib/MCUCurl.php");
require_once(dirname(__FILE__)."/../model/do/State.php");
require_once(dirname(__FILE__)."/../model/delegate/MCUSoapClient.php");
require_once(dirname(__FILE__)."/../model/delegate/MCUSoapGuestClient.php");
require_once(dirname(__FILE__)."/../params/AcceptDocSharingRequestParams.php");
require_once(dirname(__FILE__)."/../params/AddMosaicParticipantParams.php");
require_once(dirname(__FILE__)."/../params/AddSidebarParticipantParams.php");
require_once(dirname(__FILE__)."/../params/AcceptDocSharingRequestParams.php");
require_once(dirname(__FILE__)."/../params/CallParticipantParams.php");
require_once(dirname(__FILE__)."/../params/CallParticipantWithAuthParams.php");
require_once(dirname(__FILE__)."/../params/CallParticipant2Params.php");
require_once(dirname(__FILE__)."/../params/CreateTemplateGuestDidParams.php");
require_once(dirname(__FILE__)."/../params/DeleteTemplateGuestDidParams.php");
require_once(dirname(__FILE__)."/../params/DeleteTemplateGuestDidsParams.php");
require_once(dirname(__FILE__)."/../params/GetConferenceParams.php");
require_once(dirname(__FILE__)."/../params/GetConferenceAdHocTemplatePropertiesParams.php");
require_once(dirname(__FILE__)."/../params/GetConferenceReturnParams.php");
require_once(dirname(__FILE__)."/../params/GetParticipantsParams.php");
require_once(dirname(__FILE__)."/../params/GetDocSharingStatusRequestParams.php");
require_once(dirname(__FILE__)."/../params/RemoveConferenceParams.php");
require_once(dirname(__FILE__)."/../params/RemoveParticipantParams.php");
require_once(dirname(__FILE__)."/../params/RejectParticipantParams.php");
require_once(dirname(__FILE__)."/../params/SetVideoMuteParams.php");
require_once(dirname(__FILE__)."/../params/SetDocSharingMosaicParams.php");
require_once(dirname(__FILE__)."/../params/SetParticipantBackgroundParams.php");
require_once(dirname(__FILE__)."/../params/SetParticipantDisplayNameParams.php");
require_once(dirname(__FILE__)."/../params/StartRenderingInSlotParams.php");
require_once(dirname(__FILE__)."/../params/StopDocSharingParams.php");
require_once(dirname(__FILE__)."/../params/StopRenderingInSlotParams.php");
require_once(dirname(__FILE__)."/do/BfcpDocumentSharingStatus.php");

/**
 * mcu controller上のデータの操作（取得）
 * @author akihitowada
 *
 */
class MCUControllerProxy extends Proxy {
	private $curl;
	private $soap;
	private $soapGuest;
	function __construct() {
		parent::__construct();
		
		$this->curl = new MCUCurl();
		$this->soap = new MCUSoapClient();
		$this->soapGuest = new MCUSoapGuestClient();
	}
	
	static private $instance = null;
	public static function getInstance() {
		if (!self::$instance)
			self::$instance = new self;
		return self::$instance;
	}
	
	public function setWSDL($wsdl) {
		if ($this->soap) {
			$this->soap = null;
			$this->soapGuest = null;
		}
		$this->soap = new MCUSoapClient($wsdl);
		$this->soapGuest = new MCUSoapGuestClient($wsdl);
	}
	
	/**
	 * 
	 * @param unknown $confId
	 */
	public function getParticipatedVideoParticipantListByConfId($confId) {
		$list = array();
		$list = $this->removeViewParticipant($this->getParticipantListByConfId($confId));

		return $list;
	}
	
	/**
	 * 
	 * @param string $confId
	 * @return Array<Participant>
	 */
	public function getParticipatedHardWareParticipantListByConfId($confId) {
		$list = array();
		$list = $this->removeFlashParticipant($this->removeViewParticipant($this->getParticipantListByConfId($confId)));
		return $list;
	}
	
	public function getParticipatedHardWareParticipantListByConfIdWithoutSelfPartId($confId, $partId) {
		$list = array();
		$list = $this->removeFlashParticipant($this->removeViewParticipant($this->getParticipantListByConfId($confId)));
		$result = array();
		for ($i = 0; $i < count($list); $i++) {
			$client = $list[$i];
			if ($client->id != $partId) {
				$result[] = $client;
			}
		}
		return $result;
	}
	
	/**
	 * mcuから取得
	 * 一人だけの場合オブジェクトが帰ってくるので
	 * array形式にしている
	 * @param strin $confId
	 * @return array
	 */
	public function getParticipantListByConfId($confId) {
		$obj = new GetParticipantsParams(array("confId"=>$confId));
		$params = $obj->getParams();
		$result = $this->soap->getParticipants($params);
		$list = array();
		if (0 == strcasecmp(gettype($result), "object")) {
			array_push($list, $result);
		}
		else if (0 == strcasecmp(gettype($result), "array")) {
			$list = $result;
		}
		return $list;
	}
	
	/**
	 * view clientを削除
	 * @param array $list
	 * @return array
	 */
	private function removeViewParticipant($list) {
		// viewerの参加者は削除
		$result = array();
		for ($i = 0; $i < count($list); $i++) {
			$client = $list[$i];
			$name = explode("@", $client->name);
			if ($name[0] != $this->configProxy->get("defaultViewId") &&
					$name[0] != $this->configProxy->get("mcuToPgiDid")) {
// 			if ($name[0] != $this->configProxy->get("defaultViewId") &&
// 				$name[0] != $this->configProxy->get("smallViewId")) {
// 				array_splice($list, $i, 1);
				$result[] = $client;
			}
		}
		return $result;
// 		return $list;
	}

	/**
	 * flash & mobile clientを削除
	 * @param array $list
	 * @return array
	 */
	private function removeFlashParticipant($list) {
		// viewerの参加者は削除
		$result = array();
		for ($i = 0; $i < count($list); $i++) {
			$client = $list[$i];
			$name = explode("@", $client->name);
// 			if (
// 				// participated by temp//perm did.
// 				(($name[1] == $this->configProxy->get("videoParticipantDomain") ||
// 				$name[1] == $this->configProxy->get("videoParticipantGuestDomain")) &&
// 				$client->state == State::CONNECTED()) ||
				
// 				// callee video participant. 
// 				($name[1] != $this->configProxy->get("videoParticipantDomain") &&
// 				$name[1] != $this->configProxy->get("videoParticipantGuestDomain") &&
// 				$client->state == State::CONNECTED())
// 							) {
// 				$result[] = $client;
// 			}
			if ($client->type == "SIP" &&
				$client->state == State::CONNECTED()) {
				$result[] = $client;
			}
		}
		return $result;
	}
	
	public function removeParticipant($confId, $partId) {
		$obj = new RemoveParticipantParams(array(
				"confId"=>$confId,
				"partId"=>$partId
		));
		$this->soap->removeParticipant($obj->getParams());
	}
	
	/**
	 * <p></p>
	 * @param string $confId
	 * @param int $partId
	 */
	public function rejectParticipant($confId, $partId) {
		$obj = new RejectParticipantParams(array(
				"confId"=>$confId,
				"partId"=>$partId
		));
		$this->soap->rejectParticipant($obj->getParams());
	}
	
	public function createConferenceExt($params) {
		$result = $this->soap->createConferenceExt($params);
		return $result->id;
	}
	
	public function getConference($params) {
		$result = $this->soap->getConference($params);
		if ($result) {
			$obj = new GetConferenceReturnParams($result);
			return $obj->getParams();
		}
		return null;
	}
	
	public function setParticipantBackground($confId, $partId) {
		$obj = new SetParticipantBackgroundParams(array(
				"confId"=>$confId,
				"partId"=>$partId,
				"filename"=>$this->configProxy->get("backgroundImage")
		));
		$this->soap->setParticipantBackground($obj->getParams());
	}
	
	public function setCustomBackground($confId, $path) {
		$obj = new SetParticipantBackgroundParams(array(
				"confId"=>$confId,
				"partId"=>0,
				"filename"=>$path
		));
		$this->soap->setParticipantBackground($obj->getParams());
	}
	
	public function setTelephoneClientBackground($confId, $partId) {
		$obj = new SetParticipantBackgroundParams(array(
				"confId"=>$confId,
				"partId"=>$partId,
				"filename"=>$this->configProxy->get("telephoneClientImage")
		));
		$this->soap->setParticipantBackground($obj->getParams());
	}
	
	public function setVideoMute($confId, $partId, $flag) {
		$obj = new SetVideoMuteParams(array(
				"confId"=>$confId,
				"partId"=>$partId,
				"flag"=>$flag ? 1 : 0));
		$this->soap->setVideoMute($obj->getParams());
	}
	
	public function callParticipant($did, $dest) {
		$obj = new CallParticipantParams(array(
				"confId"=>$did,
				"dest"=>$dest,
				"mosaicId"=>0,
				"sidebarId"=>0));
		$this->soap->callParticipant($obj->getParams());
	}
	
	public function callSipParticipant($did, $dest, $callerId, $opensipsProxy) {
		$obj = new CallParticipantWithAuthParams(array(
				"confId"=>$did,
				"dest"=>$dest,
				"mosaicId"=>0,
				"sidebarId"=>0,
				"profileId"=>$this->configProxy->get("polycomProfileId"),
				"callerId"=> $callerId,
				"username"=>"",
				"password"=>"",
				"proxy"=>$opensipsProxy));
		$this->soap->callParticipantWithAuth($obj->getParams());
	}
// 
    public function callParticipant2($did, $dest, $callerId=null, $opensipsProxy=null) {
    	$result = split(";", $dest);
        $obj = new CallParticipant2Params(array(
				"confId"=>$did,
				"dest"=>"sip:".$result[0].":6060;pgilink=GM;". $result[2],
				"mosaicId"=>0,
				"sidebarId"=>0,
				"profileId"=>$this->configProxy->get("polycomProfileId"),
				"callerId"=>null,
				"username"=>null,
				"password"=>null,
				"proxy"=>$this->configProxy->get("opensipsProxy"),
        		"options"=>"media=A;audiocodecs=PCMU"));
		$this->soap->callParticipant2($obj->getParams());
	}
	
	public function cancelCalling($confId, $partId) {
		$this->removeParticipant($confId, $partId);
	}
	
	public function removeConferenceByConfId($confId) {
		$obj = new RemoveConferenceParams(array("confId"=>$confId));
		$this->soap->removeConference($obj->getParams());
	}
	
	public function startRenderingInSlotForVideoParticipant($confId, $appId, $slot) {
		$obj = new StartRenderingInSlotParams(
				array("confId"=>$confId,
						"mosaicId"=>0,
						"appId"=>$appId,
						"slot"=>$slot));
		$this->soap->startRenderingInSlot($obj->getParams());
	}


	/**
	 * <p>BFCPによるシェアリングが開始された際
	 * Flash上のハードウェア用シートに画面を表示する</p>
	 * @param string $confId
	 */
	public function startRenderingInSlot($confId) {
		$this->startRenderingInSlotForFlashParticipant($confId);
		
		$this->startRenderingInSlotForMobileParticipant($confId);
	}
	
	private function startRenderingInSlotForFlashParticipant($confId) {
		$obj = new StartRenderingInSlotParams(
				array("confId"=>$confId,
						"mosaicId"=>1,
						"appId"=>2,
						"slot"=>0));
		$this->soap->startRenderingInSlot($obj->getParams());
	}
	
	private function startRenderingInSlotForMobileParticipant($confId) {
		$obj = new StartRenderingInSlotParams(
				array("confId"=>$confId,
						"mosaicId"=>2,
						"appId"=>2,
						"slot"=>0));
		$this->soap->startRenderingInSlot($obj->getParams());
	}

	/**
	 * <p>
	 * BFCP開始すると、mosaic設定する必要がある
	 * </p>
	 *  @param string $confId
	 */
	
	public function setDocSharingInMosaict($confId) {
		$this->setDocSharingFlashMosaict($confId);
		//BFCPのmosaic設定はひとつだけ
	//	$this->setDocSharingMobileMosaict($confId);
	}
	
	private function setDocSharingFlashMosaict($confId) {
		$obj = new SetDocSharingMosaicParams(
				array("confId"=>$confId,
						"mosaicId"=>1));
		$this->soap->setDocSharingMosaic($obj->getParams());
	}
	
	private function setDocSharingMobileMosaict($confId) {
		$obj = new SetDocSharingMosaicParams(
				array("confId"=>$confId,
						"mosaicId"=>2));
		$this->soap->setDocSharingMosaic($obj->getParams());
	}	
		
	/**
	 * <p>
	 * BFCPによるシェアリングが終了時
	 * </p>
	 * @param string $confId
	 */
	public function stopRenderingInSlot($confId) {
		$this->stopRenderingInSlotForFlashParticipant($confId);
	
		$this->stopRenderingInSlotForMobileParticipant($confId);
	}
	
	private function stopRenderingInSlotForFlashParticipant($confId) {
		$obj = new StopRenderingInSlotParams(
				array("confId"=>$confId,
						"mosaicId"=>1,
						"appId"=>2));
		$this->soap->stopRenderingInSlot($obj->getParams());
	}
	
	private function stopRenderingInSlotForMobileParticipant($confId) {
		$obj = new StopRenderingInSlotParams(
				array("confId"=>$confId,
						"mosaicId"=>2,
						"appId"=>2));
		$this->soap->stopRenderingInSlot($obj->getParams());
	}
	
	public function stopRenderingInSlotForVideoParticipant($confId, $appId, $slotId) {
		$obj = new StopRenderingInSlotParams(
				array("confId"=>$confId,
						"mosaicId"=>0,
						"appId"=>$appId));
		$this->soap->stopRenderingInSlot($obj->getParams());
	}
	
	public function getBfcpStateFromMCU($confId) {
		$obj = new GetConferenceParams(array("confId"=>$confId));
		$result = $this->soap->getConference($obj->getParams());
		if ($result) {
			$obj = new GetConferenceReturnParams($result);
			$conferenceInfo = $obj->getParams();
		}
		
//		$this->logger->warn($conferenceInfo["docSharingState"]);
		return $conferenceInfo;
	}
	
	public function setDocument($url, $confId, $imagePath) {
		$obj = new GetConferenceParams(array("confId"=>$confId));
		$result = $this->soap->getConference($obj->getParams());
		if ($result) {
			$obj = new GetConferenceReturnParams($result);
			$conferenceInfo = $obj->getParams();
		}
	
		if ($url != $conferenceInfo["mixer"]->ip)
			$url = $conferenceInfo["mixer"]->ip;
		$this->curl->post($url, $confId, $imagePath);
	}
	
	public function createTemplateGuestDid($did, $callbackurl) {
		$obj = new CreateTemplateGuestDidParams(array(
				"templDid"=>$did,
				"didPrefix"=>$this->configProxy->get("guestPrefix"),
				"didLength"=>$this->configProxy->get("guestDidLength"),
				"callbackUrl"=>$callbackurl,
				"createOnConf"=>true));
		$result = $this->soapGuest->createTemplateGuestDid($obj->getParams());
		return $result;
	}
	
	public function deleteTemplateGuestDid($guestDid) {
		$obj = new DeleteTemplateGuestDidParams(array("guestDid"=>$guestDid));
		$this->soapGuest->deleteTemplateGuestDid($obj->getParams());
	}
	
	public function deleteTemplateGuestDids($did) {
		$obj = new DeleteTemplateGuestDidsParams(array("did"=>$did));
		$this->soapGuest->deleteTemplateGuestDids($obj->getParams());
	}
	
	public function existConferenceTemplateByDid($did) {
		$obj = new GetConferenceAdHocTemplatePropertiesParams(array("did"=>$did));
		$return = $this->soap->getConferenceAdHocTemplateProperties($obj->getParams());
		if (!$return) {
			throw new Exception(sprintf("did %s does not exists.", $did));
		}
	}
	
	/**
	 * 
	 * @param string $confId
	 * @param int $mosaicId
	 * @param int $partId
	 * @param string $displayText
	 */
	public function setParticipantDisplayName($confId, $partId, $displayText=null) {
		$obj = new SetParticipantDisplayNameParams(array(
				"confId"=>$confId,
				"mosaicId"=>VIDEO_FOR_CONFERENCE,
				"partId"=>$partId,
				"displayText"=>$displayText,
				"scriptCode"=>0)
		);
		$this->soap->setParticipantDisplayName($obj->getParams());
	}
	
	/**
	 *
	 * @param string $confId
	 * @param int $mosaicId
	 * @param int $partId
	 * @param string $displayText
	 */
	public function setHardwareParticipantDisplayNameforMosaic2($confId, $partId, $displayText=null) {
		$obj = new SetParticipantDisplayNameParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"partId"=>$partId,
				"displayText"=>$displayText,
				"scriptCode"=>0)
		);
		$this->soap->setParticipantDisplayName($obj->getParams());
	}

	private function addMosaic2forMobileParticipant($videoParticipantRecord) {
		$obj = new AddMosaicParticipantParams(array(
				"confId"=>$videoParticipantRecord["conference_id"],
				"mosaicId"=>VIDEO_FOR_MOBILE,
				"partId"=>$videoParticipantRecord["part_id"]));
		$this->soap->addMosaicParticipant($obj->getParams());
	}
	
	private function addMosaic3forMobileParticipant($videoParticipantRecord) {
		$obj = new AddMosaicParticipantParams(array(
				"confId"=>$videoParticipantRecord["conference_id"],
				"mosaicId"=>VIDEO_FOR_PGI,
				"partId"=>$videoParticipantRecord["part_id"]));
		$this->soap->addMosaicParticipant($obj->getParams());
	}
	
	private function addSidebar2forMobileParticipant($videoParticipantRecord) {
		$obj = new AddSidebarParticipantParams(array(
				"confId"=>$videoParticipantRecord["conference_id"],
				"sidebarId"=>AUDIO_FOR_MOBILE,
				"partId"=>$videoParticipantRecord["part_id"]));
		$this->soap->addSidebarParticipant($obj->getParams());
	}
	 
	private function addSidebar3forMobileParticipant($videoParticipantRecord) {
		$obj = new AddSidebarParticipantParams(array(
				"confId"=>$videoParticipantRecord["conference_id"],
				"sidebarId"=>AUDIO_FOR_PGI,
				"partId"=>$videoParticipantRecord["part_id"]));
		$this->soap->addSidebarParticipant($obj->getParams());
	}
	 
	private function setParticipantNametoMosaic2($videoParticipantRecord, $display_name=null) {
		$obj = new SetParticipantDisplayNameParams(array(
				"confId"=>$videoParticipantRecord["conference_id"],
				"mosaicId"=>VIDEO_FOR_MOBILE,
				"partId"=>$videoParticipantRecord["part_id"],
				"displayText"=>$display_name,
				"scriptCode"=>0));
		$this->soap->setParticipantDisplayName($obj->getParams());
	}
	
	private function addParticipantToStbMosaic($videoParticipantRecord) {
		$this->addMosaic2forMobileParticipant($videoParticipantRecord);
		$this->addSidebar2forMobileParticipant($videoParticipantRecord);
	}
	
	/*
	 * mobileの参加者名をmosaic2とmosaic3へ追加
	*/
	private function addMobileParticipantToMosaic2($videoParticipantRecord, $display_name, $useSalesOptionFlag) {
		$this->addMosaic2forMobileParticipant($videoParticipantRecord);
		
		//for new PGI -- it is should be disable
//		$this->addMosaic3forMobileParticipant($videoParticipantRecord);
	
		$this->addSidebar2forMobileParticipant($videoParticipantRecord);

		//for new PGI -- it is should be disable
//		$this->addSidebar3forMobileParticipant($videoParticipantRecord);
	
		if (!$useSalesOptionFlag)
			$this->setParticipantNametoMosaic2($videoParticipantRecord, $display_name);
	}
	
	/*
	 * flashの参加者名をmosaic2へ追加
	*/
	private function addFlashParticipantToMosaic2($videoParticipantRecord, $display_name, $useSalesOptionFlag) {
		$this->addMosaic2forMobileParticipant($videoParticipantRecord);
	
		$this->addSidebar2forMobileParticipant($videoParticipantRecord);
	
		if (!$useSalesOptionFlag)
			$this->setParticipantNametoMosaic2($videoParticipantRecord, $display_name);
	}
	
	/**
	 * mosaic2に表示名を追加する
	 */
	public function addParticipantToMosaic2($videoParticipantRecord, $display_name, $useSalesOptionFlag=null, $stbFlag=null) {
		$result = split("@", $videoParticipantRecord["participant_name"]);
		if ($videoParticipantRecord["participant_type"] == "WEB") {
			if ($stbFlag) {
				$this->addParticipantToStbMosaic($videoParticipantRecord);
				return;
			}	
			// view用のPariticipant　判別
			if ($result[0] == $this->configProxy->get("defaultViewId")) {
				return;
			}
			// mobile client
			else if ($result[0] == $this->configProxy->get("smallId")) {	// mobile?
				$this->addMobileParticipantToMosaic2($videoParticipantRecord, $display_name, $useSalesOptionFlag);
			}
			// pgi client
			else if ($result[0] == $this->configProxy->get("telephoneId")) {	// PGI?
				return;
			}
			// flash client
			else {
				$this->addFlashParticipantToMosaic2($videoParticipantRecord, $display_name, $useSalesOptionFlag);
			}
		}
	}
	
	
	/**
	 * 
	 * @param string $confId
	 * @param int $partId
	 */
	public function acceptDocumentSharing($confId, $partId) {
		$obj = new AcceptDocSharingRequestParams(array("confId"=>$confId, "partId"=>$partId));
		$return = $this->soap->acceptDocSharingRequest($obj->getParams());
	}
	
	/**
	 * 
	 * @param string $confId
	 * @param int $partId
	 */
	public function stopDocSharing($confId, $partId) {
		$obj = new StopDocSharingParams(array("confId"=>$confId, "partId"=>$partId));
		$return = $this->soap->stopDocSharing($obj->getParams());
	}
	
	/**
	 * 
	 * @param string $confId
	 * @return DocumentSharingStatus
	 */
	private function getDocSharingStatus($confId) {
		$obj = new GetDocSharingStatusRequestParams(array("confId"=>$confId, "partId"=>$partId));
		$return = $this->soap->getDocSharingStatus($obj->getParams());
		return $return;
	}
	
	/**
	 * 
	 * @param string $confId
	 * @return boolean
	 */
	public function isActiveDocSharingStatus($confId) {
		return BfcpDocumentSharingStatus::$ACTIVE == $this->getDocSharingStatus($confId);
	}

	public function __call($name, $args) {
		$params = $args[0];
		return $this->soap->$name($params);
	}
	
	public function createAdhocConferenceTemplate() {
		
	}
}
