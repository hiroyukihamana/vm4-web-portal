<?php

require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/McuModel.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");
require_once("classes/mgm/dbi/mcu_server.dbi.php");

require_once(dirname(__FILE__)."/controller/ConferenceController.php");
require_once(dirname(__FILE__)."/logic/composition/CompositionManager.php");
require_once(dirname(__FILE__)."/logic/composition/DocumentSharingCompositionManager.php");
require_once(dirname(__FILE__)."/logic/MosaicController.php");
require_once(dirname(__FILE__)."/logic/ParticipationController.php");


class VideoConferenceOptionClass extends ConferenceController {
	private $settingRecord;
	private $roomRecord;
	
	private $is_extend_teleconference;
	
	public function __construct($roomKey, $is_extend_teleconference = false) {
		parent::__construct();
		
		$this->is_extend_teleconference = $is_extend_teleconference;
		
		$this->roomRecord		= $this->meetingProxy->getRoomRecordByRoomKey($roomKey);
		$this->mcuGatewayProxy= new McuGatewayProxy();
		$this->settingRecord = $this->meetingProxy->getSettingRecordByRoomKey($roomKey);
		
		if( $is_extend_teleconference ){
			$this->mcuGatewayProxy->setLatestModel();
		}else{
			$this->mcuGatewayProxy->setMcuServerModelByRoomKey($roomKey);
		}
	}
	
	private function formedAddress($did, $domain) {
		$address = sprintf("%s@%s", $did, $domain);
		return $address;
	}
	
	public function getNumberAddress($did) {
		$domain = $this->mcuGatewayProxy->getRegularNumberDomain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}
	
	public function getRegularAddress($did) {
		$domain = $this->mcuGatewayProxy->getRegularDomain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}

	public function getH323TemporaryNumberAddress($did) {
		$domain = $this->mcuGatewayProxy->getH323GuestNumberDomain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}

	public function getH323TemporaryAddress($did) {
		$domain = $this->mcuGatewayProxy->guestH323Domain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}
	
	public function getSipTemporaryNumberAddress($did) {
		$domain = $this->mcuGatewayProxy->guestSipNumberDomain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}
	
	public function getSipTemporaryAddress($did) {
		$domain = $this->mcuGatewayProxy->guestSipDomain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}
	
	public function getVideoConferenceByMeetingKey ($meetingKey) {
		require_once("classes/dbi/video_conference.dbi.php");
		$videoConf		= new VideoConferenceTable($this->dsn);
		$videoWhere		= "meeting_key = '".addslashes($meetingKey)."'";
		$videoInfo	= $videoConf->getRow($videoWhere);
		return $videoInfo;
	}
	
	public function getIvesInfoByRoomKey($roomKey) {
		require_once("classes/dbi/ives_setting.dbi.php");
		$ivesSettingTable= new IvesSettingTable($this->dsn);
		$where = "room_key='".$roomKey."' and is_deleted = 0";
		$ivesInfo	= $ivesSettingTable->getRow($where);
		
		require_once("classes/dbi/ordered_service_option.dbi.php");
		$service_option  = new OrderedServiceOptionTable($this->dsn);
		$where = sprintf("room_key = '%s' AND ordered_service_option_status = 1", $roomKey);
		$option = $service_option->getRow($where);
		if ($option) {
			return $ivesInfo;
		}
		else {
			return null;
		}
	}
	
	public function getDidByRoomKey($roomKey) {
		$ives = $this->getIvesInfoByRoomKey($roomKey);
		return $ives["ives_did"];
	}
	
	public function cancelReservation($confId) {
		$conf = $this->mcuControllerProxy->getConference( array('confId' => $confId) );
		if($conf)
			$this->mcuControllerProxy->removeConferenceByConfId($confId);
	}
		
	/**
	 * 追加をonparticipantCreated側に委譲するため
	 * 削除予定
	 * stable1_3側のみ機能
	 * @param unknown $meetingKey
	 * @param unknown $participantKey
	 * @version 20130523
	 */
	public function addVideoConferenceParticipant($meetingKey, $participantKey) {}
	
	public function getConfig() {
		return McuConfig::getInstance();
	}
	
	public function getMcuGatewayByRoomKey($roomKey) {
		$info = array(
				"status"			=> true,
				"contract"			=> true,
				"sipServer"			=> $this->mcuGatewayProxy->getMCUControllerServer(),
				"sipServerDomain"	=> $this->mcuGatewayProxy->getServerDomain(),
				"sipProxyIp"		=> $this->mcuGatewayProxy->getSipProxy(),
				"sipProxyPort"		=> $this->mcuGatewayProxy->getSipProxyPort(),
				"socketServer"		=> $this->mcuGatewayProxy->getSocketServer(),
				"socketServerPort"	=> $this->mcuGatewayProxy->getSocketPort(),
				"defaultId"			=> $this->settingRecord['client_id'],
				"defaultPw"			=> $this->settingRecord['client_pw'],
				"defaultViewId"		=> $this->configProxy->get("defaultViewId"),
				"defaultViewPw"		=> $this->configProxy->get("defaultViewPw"),
				"smallId"			=> $this->configProxy->get("smallId"),
				"smallPw"			=> $this->configProxy->get("smallPw"),
				"smallViewId"		=> $this->configProxy->get("smallViewId"),
				"smallViewPw"		=> $this->configProxy->get("smallViewPw"),
				"telephoneId"		=> $this->configProxy->get("telephoneId"),
				"telephonePw"		=> $this->configProxy->get("telephonePw"),
				"did"				=> $this->settingRecord['ives_did']);
		$model = new McuModel($info);
		return $model->getContent();
	}
	
	public function getReservationGuestDidByParmanentDid($did) {
		$callbackurl = sprintf($this->configProxy->get("reservationGuestCallbackUrl"), $this->configProxy->get("soap_domain"));
		return $this->mcuControllerProxy->createTemplateGuestDid($did, $callbackurl);
	}
	
	public function getInvitationGuestDidByParmanentDid($did) {
		$callbackurl = sprintf($this->configProxy->get("invitationGuestCallbackUrl"), $this->configProxy->get("soap_domain"));
		return $this->mcuControllerProxy->createTemplateGuestDid($did, $callbackurl);
	}
	
	public function deleteTemplateGuestDid($guestDid) {
		$this->mcuControllerProxy->deleteTemplateGuestDid($guestDid);
	}
	
	public function getUsageTimeFromMeetingUpTime($meetingKey) {
		return $this->meetingProxy->getUsageTimeFromMeetingUpTime($meetingKey);
	}
	
	public function updateListWithoutPolycom($meetingKey, $participantList) {
		return $this->meetingProxy->updateListWithoutPolycom($meetingKey, $participantList);
	}
	
	public function getParticipatedList($meetingKey) {
		return $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByDid($this->settingRecord["ives_did"]);
	}
	
	public function removeConference($confId) {
		$list = $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByConfId($confId);
		for ($i = 0; $i < count($list); $i++) {
			$participant = $list[$i];
			$this->mcuControllerProxy->removeParticipant($confId, $participant["part_id"]);
		}
	}
	
	/**
	 * pgi コネクションを切断する
	 */
	public function removeTeleconferenceParticipant( $confId ) {
		$list = $this->mcuControllerProxy->getParticipantListByConfId( $confId );
		foreach( $list as $participant ) {
			$name = split( "@", $participant->name );
			if ( $this->configProxy->get( "mcuToPgiDid" ) == $name[0] ) {
				$this->mcuControllerProxy->removeParticipant( $confId, $participant->id );
			}
		}
	}
	
	public function callParticipant($meetingKey, $target, $protocol="sip", $name=null) {
		
		if( ! $this->is_extend_teleconference){
			$this->mcuControllerProxy->existConferenceTemplateByDid($this->settingRecord["ives_did"]);
		}
		
		if (0 != strcasecmp($protocol, "pgi")) {
			// 入力値チェック
			preg_match("/^".REG_IP."$/", $target, $ipresult);
			if (count($ipresult) <= 0) throw new Exception("invalid ip string: ".$target, -2);
			preg_match("/^".REG_CALLOUT_NAME."$/", $name, $nameresult);
			if (count($nameresult) <= 0) throw new Exception("invalid name string: ".$name, -2);
			
			//参加者チェック
			$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
			if ($conferenceRecord) {
				$list = $this->meetingProxy->getParticipantByMeetingKey($meetingKey);
				$participationController = new ParticipationController();
				if (!$participationController->checkParticipation($conferenceRecord["conference_id"], $list, $this->roomRecord["max_seat"], $this->settingRecord["num_profiles"])) {
					throw new Exception("invalid participated number: ".count($list), -1);
				}
			} else if ($this->settingRecord["num_profiles"] <= 0) {
				throw new Exception("check contract number. did:  ".$this->settingRecord["ives_did"], -1);
			}
		}
		
		$user_info = $this->meetingProxy->getActiveUserRecordByUserKeyWithThrowableException( $this->roomRecord['user_key'] );
		// port制御対応
		if ( $user_info['use_port_plan'] ) {
			// ユーザレベル ポート入室数制限
			$allActiveParticipantsNumber = $this->meetingProxy->getAllParticipantListsByUserKey($this->roomRecord["user_key"]);
			if ( $user_info['max_connect_participant'] <= $allActiveParticipantsNumber ) throw new Exception("invalid participated number", -1);
		
			// 予約レベル ポート入室数制限
			if( $user_info['entry_mode'] == '1' ){
				// entry_mode:1 の時は予約しないと入室できない
				$reservation_info = $this->meetingProxy->getReservation($meetingKey);
				$list = $this->meetingProxy->getParticipantByMeetingKey($meetingKey);
				if ( !$reservation_info || $reservation_info['max_port'] <= count($list) ) throw new Exception("invalid participated", -1);;
			}
		}
		
		$clientName = $this->conferenceProxy->getCalleeName($name);
		if (0 == strcasecmp($protocol, "sip")) {
			if (!$name) throw new Exception("rejection because of empty of name in case using sip protocol:  ", -2);
			$template = $this->configProxy->get("callout_sip_template");
			$dest = sprintf($template, $clientName, $target);
			
			$calloutProxy = $this->configProxy->get("callout_proxy");
			$opensipsProxy = sprintf($calloutProxy, $this->mcuGatewayProxy->getRegularIP());
			
// 			$displayName = $name ? $name : $this->configProxy->get("callout_sip_displayname");
			$callerIdTemplate = $this->configProxy->get("callout_callerId_template");
			$callerId = sprintf($callerIdTemplate, $name, $this->mcuGatewayProxy->getRegularDomain());
			$participantRecord = $this->conferenceProxy->addCallingParticipantRecord($this->settingRecord["ives_did"], $meetingKey, $clientName, $target);
			$this->mcuControllerProxy->callSipParticipant($this->settingRecord["ives_did"], $dest, $callerId, $opensipsProxy);
		} else if (0 == strcasecmp($protocol, "pgi")) {
			$callerIdTemplate = $this->configProxy->get("callout_pgi_name");
			$this->logger->debug(array($this->settingRecord["ives_did"],$meetingKey, $callerIdTemplate, $target));
			
			$did = $this->settingRecord['ives_did'];
			if($this->is_extend_teleconference){
				$meeting_info = $this->meetingProxy->getMeetingByMeetingKey($meetingKey);
				$did = $meeting_info['temporary_did'];
			}
			
			$this->mcuControllerProxy->callParticipant2($did, $target);	
		} else {
			$template = $this->configProxy->get("callout_h323_template");
			$dest = sprintf($template, $clientName, $target, $this->mcuGatewayProxy->getH323CalloutDomain());
			$participantRecord = $this->conferenceProxy->addCallingParticipantRecord($this->settingRecord["ives_did"], $meetingKey, $clientName, $target);
			$this->mcuControllerProxy->callParticipant($this->settingRecord["ives_did"], $dest);
		}
		return $participantRecord;
	}
	
	public function getCallingStatus($videoParticipantKey) {
		return $this->conferenceProxy->getCalleeParticipantStatusVideoParticipantKey($videoParticipantKey);
	}
	
	public function cancelCallingParticipant($meetingKey) {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			return;
		$callee = $this->conferenceProxy->getCallingParticipantRecord($conferenceRecord["conference_id"], $conferenceRecord["did"], $meetingKey);
		if ($callee) {
			$this->mcuControllerProxy->cancelCalling($conferenceRecord["conference_id"], $callee["part_id"]);
		}
	}
	
	public function startDocumentSharing($meetingKey) {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			return;

		/**
		 * BFCP利用中の場合は終了後、DocumentSharingを開始する
		 */
		if ($this->mcuControllerProxy->isActiveDocSharingStatus($conferenceRecord["conference_id"])) {
			$this->meetingProxy->setActiveMeetingRecordByMeetingKeyThrowable($meetingKey);
			$this->fmsController = new FMSNetConnectionController($this->meetingProxy->activeMeetingRecord);
			$this->fmsController->connect();
			$return = $this->fmsController->getDocumentSharingStatus();

			$bfcpActiveState = $this->mcuControllerProxy->getBfcpStateFromMCU($conferenceRecord["conference_id"]);
			if ($bfcpActiveState["docSharingState"] == "ACTIVE")
				 $return->partId = $bfcpActiveState["docSharingPartId"];

			$this->mcuControllerProxy->stopDocSharing($conferenceRecord["conference_id"], $return->partId);
		}
		
		// document_status 開始フラグ
		$this->conferenceProxy->setDocumentSharingStatus($conferenceRecord, 1);
		
		// composite
		try {
			$count = count($this->mcuControllerProxy->getParticipatedVideoParticipantListByConfId($conferenceRecord["conference_id"]));
			$composite = new DocumentSharingCompositionManager($conferenceRecord["conference_id"], $count, $this->settingRecord && $this->settingRecord["use_active_speaker"], $this->roomRecord["mobile_4x4_layout"]);
			$composite->run();
			
			// mosaic
			$mosaic = new MosaicController();
			$params = $mosaic->getDefaultMosaicSlotParamsToDeActivateVadSlot($conferenceRecord["conference_id"]);
			$this->mcuControllerProxy->setMosaicSlot($params);
			
			// start
			$this->mcuControllerProxy->startRenderingInSlotForVideoParticipant($conferenceRecord["conference_id"], 1, 0);
		} catch (Exception $e) {
			$this->conferenceProxy->setDocumentSharingStatus($conferenceRecord, 0);
			throw $e;
		}
	}
	
	public function stopDocumentSharing($meetingKey, $bfcpStatus) {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			return;

		// document_status 終了フラグ
		$this->conferenceProxy->setDocumentSharingStatus($conferenceRecord, 0);
		
		$count = count($this->mcuControllerProxy->getParticipatedVideoParticipantListByConfId($conferenceRecord["conference_id"]));
		$countHardwareParticipant = count($this->mcuControllerProxy->getParticipatedHardWareParticipantListByConfId($conferenceRecord["conference_id"]));
		
		//BFCPを開始する時、コールバックはhardwareのcompositionだけ変更
		if ($bfcpStatus) {		
	        $this->logger->warn($bfcpStatus);
			$composite = new CompositionManager($conferenceRecord["conference_id"], $count, $countHardwareParticipant, $this->settingRecord && $this->settingRecord["use_active_speaker"], $this->roomRecord["mobile_4x4_layout"], $bfcpStatus, $this->roomRecord["use_stb_option"]);
			$composite->runHardwareComposition();
		} else {
			$composite = new CompositionManager($conferenceRecord["conference_id"], $count, $countHardwareParticipant, $this->settingRecord && $this->settingRecord["use_active_speaker"], $this->roomRecord["mobile_4x4_layout"], $bfcpStatus, $this->roomRecord["use_stb_option"]);
			$composite->run();
		}

		// mosaic
		$mosaic = new MosaicController();
		if ($this->settingRecord &&
			$this->settingRecord["use_active_speaker"] &&
			$count >= 3) {
			$params = $mosaic->getDefaultMosaicSlotParamsToActivateVadSlot($conferenceRecord["conference_id"]);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}

		$this->mcuControllerProxy->stopRenderingInSlotForVideoParticipant($conferenceRecord["conference_id"], 1, 0);
	}
	
	public function documentSharing($meetingKey, $documentId=null, $page=null) {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			throw new Exception("no conference record: ".$meetingKey);
		
		$mcuRecord	= $this->mcuGatewayProxy->getMcuServerByServerKey($conferenceRecord["mcu_server_key"]);
		if (!$mcuRecord)
			throw new Exception("irregal mcu server status: key: ".$conferenceRecord["mcu_server_key"]);
		
		if ($documentId) {
			$imagePath = $this->documentProxy->getImagePath($documentId, $page);
			if (!file_exists($imagePath)) {
				$this->logger->info("[document sharing] incompatible file. id: ".$documentId);
				$imagePath = $this->documentProxy->getNoImagePath();
			}
		}
		else {
			$imagePath = $this->documentProxy->getNoImagePath();
		}
		if (!file_exists($imagePath)) {
			throw new Exception("file does not exist. path: ".$imagePath);
		}
		
		$this->mcuControllerProxy->setDocument($mcuRecord["server_address"], $conferenceRecord["conference_id"], $imagePath);
	}
	
	public function changeDataCenter($meetingRecord) {
		$fmsController= new FMSNetConnectionController($meetingRecord);
		$fmsController->connect();
		$fmsController->bootFMSInstance($this->settingRecord);

		$list = $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByMeetingKey($meetingRecord["meeting_key"]);
		$data = array("participantList"=>$list,"callingStatus"=>"");
		$fmsController->noticeParticipantListChanged($data);
	}
}
