<?php 
require_once(dirname(__FILE__)."/../../controller/mobile/MobileConferenceController.php");

class SalesMobileConferenceController extends MobileConferenceController {
	public function createConference($meetingKey, $did) {
		$logic = new MobileConferenceCreatedLogic();
		$this->mcuGatewayProxy->setMcuServerModelByMeetingKey($meetingKey);
		$mediaMixer = $logic->fixMediaMixerByMcuKey($this->mcuGatewayProxy->mcuServerModel["server_key"]);
	
		if ($confId = $logic->getConferenceByDID($did))
			$logic->removeConferenceByConfId($confId);
	
		$obj = new CreateConferenceExtParams(array(
				"name"		=> $this->configProxy->get("conference_name"),
				"did"		=> $did,
				"mixerId"	=> $mediaMixer["media_name"]."@".$mediaMixer["url"],
				"size"		=> $this->configProxy->get("size"),
				"compType"	=> 0,
				"vad"		=> true,
				"profileId"	=> $this->configProxy->get("profileId"),
				"audioCodecs"=> "",
				"videoCodecs"=> "",
				"textCodecs"	=> "",
				"autoAccept"	=> false,
				"callbackURL"	=> sprintf($this->configProxy->get("callback_conference"), $this->configProxy->get("soap_domain"))
		));
		$logic->createConferenceForMobile($obj->getParams());
	
		$logic->createConferenceRecord($meetingKey, $did, $mediaMixer["media_mixer_key"]);
	
		$params = $logic->getCreateFlashMosaic();
		$logic->createMosaic($params);
	
		$params = $logic->getCreateMobileMosaic();
		$logic->createMosaic($params);
	
		$params = $logic->getCreateSidebarParams();
		$logic->createSidebar($params);
	}
}
