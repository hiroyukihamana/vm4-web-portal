<?php
require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../../../logic/conference/ConferenceCreatedLogic.php");

class CreateConferenceClass {
    private $logger;
    private $logic;
    
    function __construct($model){
        $this->logic = new ConferenceCreatedLogic($model);
		$this->logger = EZLogger2::getInstance();
    }
    
    public function execute() {
    	
    	$this->logic->setSettingRecord();
    	if (!$this->logic->settingRecord)
    		return;
    	$this->logic->clearConferenceRecord();
    		
    	$this->logic->clearConferenceParticipantRecord();
    		
   		$this->logic->setRoomRecord();
    		
   		$this->logic->setMeetingRecord();
   		
   		$this->logic->setBackgroundImage();
   		
    	if (!$this->logic->meetingRecord) {
    		
    		$this->logic->createMeetingRecord();
    		
    		$this->logic->resolveEnvironmentData();

    		$this->logic->createMeetingSequenceRecord();

    		$this->logic->bootFMSInstance();
    	}
    	/**
    	 * 予約会議かつPolycomClientが最初に入室した場合
    	 */
    	else if ($this->logic->meetingRecord["is_reserved"] &&
    			!$this->logic->hasMeetingSequenceActiveRecord()) {
    		
    		$this->logic->resolveEnvironmentData();
    		
    		$this->logic->createMeetingSequenceRecord();

    		$this->logic->bootFMSInstance();
    	}
    	// global link
    	else if(!$this->logic->hasParticipantActiveRecord()) {
    		$this->logic->bootFMSInstance();
    	}
    	
   		$this->logic->createConferenceRecord();

    	$this->logic->createSidebar();
    	
    	$this->logic->createMosaic();
    	
    	$this->logic->setFlashMosaicSlot();
    }
}
