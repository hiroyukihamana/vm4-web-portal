<?php 
interface IDestroyedConferenceParticipant {
	function removeConferenceParticipantRecord();
	function setConferenceParticipantRecord();
	function setConferenceParticipantList();
}
