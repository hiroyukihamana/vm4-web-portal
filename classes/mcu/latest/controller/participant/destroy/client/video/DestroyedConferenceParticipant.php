<?php 
require_once(dirname(__FILE__)."/../../../../../controller/participant/destroy/client/ADestroyedParticipant.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/destroy/client/interface/conference/IDestroyedConference.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/destroy/client/interface/conference/IDestroyedConferenceParticipant.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/destroy/client/interface/meeting/IDestroyedMeeting.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/destroy/client/interface/meeting/IDestroyedParticipant.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/destroy/client/interface/meeting/IDestroyedSetting.php");

class DestroyedConferenceParticipant extends ADestroyedParticipant implements IDestroyedConference, IDestroyedConferenceParticipant, IDestroyedMeeting, IDestroyedParticipant, IDestroyedSetting {

	// IConference
	function removeConferenceRecord() {
	
	}
	function setConferenceRecord() {
		$this->logic->setConferenceRecord();
	}
	// IConference
	
	// IConferenceParticipant
	function removeConferenceParticipantRecord() {
		$this->logic->removeConferenceParticipantRecord();
	}
	
	function setConferenceParticipantRecord() {
		$this->logic->setConferenceParticipantRecord();
	}
	
	function setConferenceParticipantList() {
		$this->logic->setConferenceParticipantList();
	}
	// IConferenceParticipant
	
	// IMeeting
	function removeMeetingRecord() {
		
	}
	
	function setMeetingRecord() {
		$this->logic->setMeetingRecord();
	}
	// IMeeting

	// IParticipant
	function removeParticipantRecord() {
		$this->logic->removeParticipantRecord();
	}
	
	function setParticipantRecord() {
		$this->logic->setParticipantRecord();
	}
	// IParticipant
	
	// IDestroyedSetting
	function setSettingRecord() {
		$this->logic->setSettingRecordByDid();
	}
	// IDestroyedSetting
	
	public function execute() {
		$this->setConferenceParticipantList();
		
		$this->setConferenceParticipantRecord();
		
		$this->setConferenceRecord();
		
		$this->setSettingRecord();
		
		$this->setMeetingRecord();
		
		$this->logic->setParticipantRecordList();
		
		$this->logic->setRoomRecordByRoomKey();

		if ($this->logic->conferenceParticipantRecord) {	
			$this->removeParticipantRecord();
		
			$this->removeConferenceParticipantRecord();
		}
		
		$this->logic->noticeParticipantDestroyed();

		//再度取得 
		$this->setConferenceParticipantList();

		if ($this->logic->isActiveConferenceOnMCU()) {
			if (0 == $this->logic->getParticipantNumberOnMCU()) {
				$this->logic->destroyConference();
			}
			else {
				$this->logic->controlDeActivateVadSlot();
			}
		}
		
		$this->logic->setCompositionType();
		
// 		($this->hasMeetingParticipant())
// 		? $this->meetingWithFMSInsctance()
// 		: $this->meetingWithoutFMSInsctance();
	}
	
	private function hasMeetingParticipant() {
		if ($this->logic->conferenceParticipantRecordList) {
			foreach ($this->logic->conferenceParticipantRecordList as $key => $value) {
				if ($value["participant_type"] == "WEB")
					return true;
			}
		}

		if ($this->logic->participantRecordList) {
			foreach ($this->logic->participantRecordList as $key => $value) {
				if ($value["participant_protocol"] == "rtmp")
					return true;
			}
		}
		return false;
	}
	
	private function meetingWithFMSInsctance() {
		$this->logic->setCompositionType();
	}
	
	private function meetingWithoutFMSInsctance() {
		if (0 == count($this->logic->conferenceParticipantRecordList)) {
			$this->logic->stopMeeting();
			return;
		}
		if (1 == count($this->logic->conferenceParticipantRecordList)) {
			$this->logic->stopCount();
		}
		$this->logic->setCompositionType();
// 		$this->logic->setCompositionTypeForConferenceParticipant();
	}
}
