<?php

require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/vcube/flash/FlashParticipant.php");

class PgiClient extends FlashParticipant {

	public function setParticipantBackground() {
		$this->logic->setTelephoneClientBackground();
	}

	public function execute() {
		$this->setSettingRecord();
		
		$this->setConferenceRecord();
		if (!$this->logic->conferenceRecord)
			throw new Exception("no conference recrod. confId: ".$this->logic->requestProxy->confId);
		
		$this->setMeetingRecord();
		if (!$this->logic->meetingRecord) 
			throw new Exception("no meeting recrod. confId: ".$this->logic->requestProxy->confId);
		
		$this->logic->setRoomRecordByRoomKey();
		
		$this->checkBlocked();
		
		$this->acceptPgiParticipant();
		
		$this->createConferenceParticipantRecord();

		$this->logic->controlActivateVadSlot();
		
		$this->setCompositionType();
		
		$this->changeParticipantProfile();
//for new PGI
	//	$this->addMosaicParticipant();
		
	// for new PGI 
		$this->addMosaicPGIparticipant();
		
		$this->addSidebarParticipant();
		
		$this->setParticipantBackground();

		$this->logic->setVideoMute();
	}
}
