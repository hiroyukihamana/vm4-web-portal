<?php 
require_once(dirname(__FILE__)."/InvalidParticipant.php");

class UnknownParticipant extends InvalidParticipant {
	static $errorMessage = "unknown client type.: ";
}
