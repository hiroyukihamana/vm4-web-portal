<?php 
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/ACreatedParticipant.php");
require_once(dirname(__FILE__)."/../../../../../logic/participant/InvalidLogic.php");
require_once(dirname(__FILE__)."/../../../../../../lib/RejectedException.php");

class InvalidParticipant extends ACreatedParticipant {
	static $errorMessage = "";
	protected function setLogic($model) {
		$this->logic = new InvalidLogic($model);
	}
	
	public function execute() {
		$this->throwRejectedException();
	}

	protected function throwRejectedException() {
		throw new RejectedException(self::$errorMessage.$model->confId);
	}
	
	public function acceptParticipant() {}
}
