<?php 
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/ACreatedParticipant.php");
require_once(dirname(__FILE__)."/../../../../../logic/participant/InvalidParticipantLogic.php");
require_once(dirname(__FILE__)."/../../../../../../lib/RejectedException.php");

class InvalidCalleeParticipant extends ACreatedParticipant {
	public function __construct($model, $participant) {
		$this->initialize($model);
		
		$this->setCalleeParticipant($participant);
	}
	
	protected function setLogic($model) {
		$this->logic = new InvalidParticipantLogic($model);
	}
	
	private function setCalleeParticipant($participant) {
		$this->logic->setCalleeParticipant($participant);
	}
	
	public function execute() {
		$this->logic->setConferenceRecord();

		$this->logic->setMeetingRecord();
		
		$this->logic->voidCalleeParticipant();

		$this->logic->noticeCalleeStatus();
		
		$this->throwRejectedException();
	}
	
	private function throwRejectedException() {
		throw new RejectedException("conference is already destroyed. confId: ".$model->confId);
	}
	
	public function acceptParticipant() {}
}
