<?php 

require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/meeting/IMeeting.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/video/AConferenceParticipant.php");
require_once(dirname(__FILE__)."/../../../../../logic/participant/CallingParticipantCreatedLogic.php");


class CalledVideoParticipant extends AConferenceParticipant implements IMeeting {
	
	protected function initialize($model) {
		parent::initialize($model, false);
		
		try {
			$this->setISettingRecord();
			$this->setIRoomRecord();
		}
		catch (Exception $e) {
			$this->remove();
			throw $e;
		}
	}
	
	protected function setLogic($model) {
		$this->logic = new CallingParticipantCreatedLogic($model);
	}
	
	public function execute() {
		try {
			$this->setMeetingRecord();
	
			$this->checkBlocked();
				
			$this->setParticipantRecord();
	
			$this->checkParticipatedNumber();
				
			$this->setConferenceRecord();		
		}
		catch (Exception $e) {
			$this->remove();
			throw $e;
		}

		$this->logic->setCallerParticipant();

		$this->logic->updateCallerParticipant();
	}
	
	// IMeeting
	public function checkBlocked() {
		if (!$this->logic->meetingRecord) {
			throw new Exception("irregal meeting record.");
		}
		if ($this->logic->meetingRecord["is_blocked"]) {
			throw new Exception("meeting is blocked. meeting_key:".$this->logic->meetingRecord["meeting_key"]);
		}
	}
	
	public function createMeetingRecord() {}	

	public function setMeetingRecord() {
		$this->logic->setMeetingRecordByRoomRecord();
	}
}
