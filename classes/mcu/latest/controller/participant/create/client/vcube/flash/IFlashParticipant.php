<?php

interface IFlashParticipant {
	function changeParticipantProfile();
	function setCompositionType();
}
