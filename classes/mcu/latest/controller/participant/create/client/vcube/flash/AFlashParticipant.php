<?php
require_once(dirname(__FILE__)."/../../../../../../participant/create/client/ACreatedParticipant.php");
require_once(dirname(__FILE__)."/../../../../../../participant/create/client/interface/initialize/IIMeeting.php");
require_once(dirname(__FILE__)."/../../../../../../participant/create/client/interface/initialize/IIRoom.php");
require_once(dirname(__FILE__)."/../../../../../../participant/create/client/interface/conference/ICreateConference.php");

abstract class AFlashParticipant extends ACreatedParticipant implements IIMeeting, IIRoom {
	protected function initialize($model) {
		parent::initialize($model);

		$this->setIRoomRecord();
		
		$this->setIMeetingRecord();
	}
	
	public function setIRoomRecord() {
		$this->logic->setIRoomRecordByRoomKey();
	}
	
	// IMeeting
	public function checkBlocked() {
		if ($this->logic->meetingRecord["is_blocked"]) {
			throw new Exception("meeting is blocked.");
		}
	}
	
	public function setIMeetingRecord() {
		$this->logic->setIMeetingRecord();
	}
	// IMeeting
}
