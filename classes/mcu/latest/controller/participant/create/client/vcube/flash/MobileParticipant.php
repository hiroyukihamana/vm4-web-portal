<?php

require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/ACreatedParticipant.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/conference/ICreateConference.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/conference/ICreateConferenceParticipant.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/meeting/IMeeting.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/meeting/ICreateParticipant.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/meeting/IRoom.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/meeting/ISetting.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/vcube/flash/IFlashParticipant.php");

class MobileParticipant extends ACreatedParticipant implements ICreateConference, ICreateConferenceParticipant, IFlashParticipant, IMeeting, ICreateParticipant, IRoom, ISetting {
	// ICreateConference
	public function removeConferenceRecord() {
		$this->logic->removeConferenceRecord();
	}
	
	public function setConferenceRecord() {
		$this->logic->setConferenceRecord();
	}
	// ICreateConference
	
	//ICreateConferenceParticipant
	public function removeConferenceParticipantRecord() {
		$this->logic->removeConferenceParticipantRecord();
	}
	
	public function createConferenceParticipantRecord() {
		$this->logic->createConferenceParticipantRecord();
	}
	
	public function setConferenceParticipantRecord() {
		$this->logic->setConferenceParticipantRecord();
	}
	//ICreateConferenceParticipant

	// IMeeting
	public function checkBlocked() {
		if ($this->logic->meetingRecord["is_blocked"]) {
			throw new Exception("meeting is blocked.");
		}
	}
	public function createMeetingRecord() {}
	public function setMeetingRecord() {
		$this->logic->setMeetingRecord();
	}
	// IMeeting
	
	// ICreateParticipant
	public function createParticipantRecord() {}
	public function removeParticipantRecord() {}
	public function setParticipantRecord() {}
	// ICreateParticipant

	// IRoom
	public function setRoomRecord() {
		$this->logic->setRoomRecordByRoomKey();
	}
	// IRoom	
	
	// ISetting
	public function setSettingRecord() {
		$this->logic->setSettingRecordByDid();
	}
	// ISetting
	
	public function execute() {
		$this->setSettingRecord();

		$this->setConferenceRecord();

		if (!$this->logic->conferenceRecord)
			throw new Exception("no conference record. confId: ".$this->logic->requestProxy->confId);
		
		$this->logic->setMeetingRecordByMeetingKey();
		if (!$this->logic->meetingRecord)
			throw new Exception("no meeting record. meetingKey: ".$this->logic->conferenceRecord["meeting_key"]);
		
		$this->setRoomRecord();
		
		$this->checkBlocked();
		
		$this->acceptParticipant();
		
		$this->createConferenceParticipantRecord();
		
		$this->logic->controlActivateVadSlot();

		if ($this->logic->meetingRecord["use_stb_option"]) {
			$this->changeParticipantHD720Profile();
		} else {
			$this->changeParticipantProfile();
		}

		$this->setCompositionType();

//		$this->addMosaicParticipant();
		
//		$this->addSidebarParticipant();
		
		$this->setParticipantBackground();
		
		if( $this->logic->meetingRecord['use_sales_option'] ){
			$this->setMosaicParticipant();
		}
		
	}
	
	//IFlashParticipant
	public function setCompositionType() {
		$this->logic->setCompositionType();
	}

	public function changeParticipantHD720Profile() {
		$params = $this->logic->getChangeProfileParamsForStbParticipant();
		$this->logic->changeParticipantProfile($params);
	}
	
	public function changeParticipantProfile() {
		$params = $this->logic->getChangeProfileParamsForMobileParticipant();
		$this->logic->changeParticipantProfile($params);
	}
	//IFlashParticipant
	
	public function acceptParticipant() {
		$params = $this->logic->getAcceptMobileViewerParticipantParams();
		$this->logic->acceptParticipant($params);
	}
	
	public function addMosaicParticipant() {
		// for mobile
		$params = $this->logic->getAddMobileMosaicForMobileParticipantParams();
		$this->logic->addMosaicParticipant($params);
		
		$params = $this->logic->getAddPgiMosaicForMobileParticipantParams();
		$this->logic->addMosaicParticipant($params);
		
	}
	
	public function setMosaicParticipant() {
		$this->logic->setMobileMosaicForSales();
	}
	
	public function addSidebarParticipant() {
		$this->logic->addSidebarParticipantForMobile();
		
		$this->logic->addSidebarParticipantForPgi();
	}

	public function setParticipantBackground() {
		$this->logic->setParticipantBackground();
	}
}
	
