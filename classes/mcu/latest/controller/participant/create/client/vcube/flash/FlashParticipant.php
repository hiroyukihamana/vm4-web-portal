<?php

require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/ACreatedParticipant.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/conference/ICreateConference.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/conference/ICreateConferenceParticipant.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/meeting/IMeeting.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/meeting/ISetting.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/vcube/flash/IFlashParticipant.php");

class FlashParticipant extends ACreatedParticipant implements ICreateConference, ICreateConferenceParticipant, IFlashParticipant, IMeeting, ISetting {	

	// ICreateConference
	public function removeConferenceRecord() {
		$this->logic->removeConferenceRecord();
	}
	
	public function setConferenceRecord() {
		$this->logic->setConferenceRecord();
	}
	// ICreateConference

	//ICreateConferenceParticipant
	public function removeConferenceParticipantRecord() {
		$this->logic->removeConferenceParticipantRecord();
	}
	
	public function createConferenceParticipantRecord() {
		$this->logic->createConferenceParticipantRecord();
	}
	
	public function setConferenceParticipantRecord() {}
	//ICreateConferenceParticipant
	
	//IFlashParticipant
	public function setCompositionType() {
		$this->logic->setCompositionType();
	}
	
	public function changeParticipantHD720Profile() {
		$params = $this->logic->getChangeProfileParamsForStbParticipant();
		$this->logic->changeParticipantProfile($params);
	}

	public function changeParticipantProfile() {
		$params = $this->logic->getChangeProfileParamsForFlashParticipant();
		$this->logic->changeParticipantProfile($params);
	}
	//IFlashParticipant
	
	// IMeeting
	public function checkBlocked() {
		if ($this->logic->meetingRecord["is_blocked"]) {
			throw new Exception("meeting is blocked.");
		}
	}
	public function createMeetingRecord() {}
	
	public function setMeetingRecord() {
		$this->logic->setMeetingRecordByMeetingKey();
	}
	// IMeeting
	
	// ISetting
	function setSettingRecord() {
		$this->logic->setSettingRecordByDid();
	}
	// ISetting

	public function acceptParticipant() {
		$params = $this->logic->getAcceptParamsForFlashParticipant();
		$this->logic->acceptParticipant($params);
	}
	
	public function acceptTelephonyParticipant() {
		$params = $this->logic->getAcceptParamsForTelephonyParticipant();
		$this->logic->acceptParticipant($params);
	}
	
	public function addMosaicParticipant() {
		// for mobile
		$params = $this->logic->getAddFlashMosaicForMobileParticipantParams();
		$this->logic->addMosaicParticipant($params);

		// for PGI	
		$params = $this->logic->getAddPgiMosaicForMobileParticipantParams();
		$this->logic->addMosaicParticipant($params);
	}
	
	public function addMosaicPGIparticipant() {
			// for PGI	
		$params = $this->logic->getAddPgiMosaicForMobileParticipantParams();
		$this->logic->addMosaicParticipant($params);
	}
	
	public function setMosaicParticipant() {
		$this->logic->setFlashMosaicForSales();
	}
	
	public function addSidebarParticipant() {
		$this->logic->addSidebarParticipantForMobile();
		// form new PGI
		//$this->logic->addSidebarParticipantForPgi();
	}
	
	public function setParticipantBackground() {
		$this->logic->setParticipantBackground();
	}
	
	public function execute() {
		$this->setSettingRecord();
		
		$this->setConferenceRecord();
		if (!$this->logic->conferenceRecord)
			throw new Exception("no conference recrod. confId: ".$this->logic->requestProxy->confId);
		
		$this->setMeetingRecord();
		if (!$this->logic->meetingRecord) 
			throw new Exception("no meeting recrod. confId: ".$this->logic->requestProxy->confId);
		
		$this->logic->setRoomRecordByRoomKey();
		
		$this->checkBlocked();
		
		$this->acceptParticipant();
		
		$this->createConferenceParticipantRecord();

		$this->logic->controlActivateVadSlot();
		
		$this->setCompositionType();
	
		if ($this->logic->meetingRecord["use_stb_option"]) {
        	$this->changeParticipantHD720Profile();
		} else {
        	$this->changeParticipantProfile();
		}
		
		if( $this->logic->meetingRecord['use_sales_option'] ){
			$this->setMosaicParticipant();
		}
		
/*
		$this->addMosaicParticipant();

		if ($this->logic->meetingRecord["use_sales_option"]) {
			$this->setMosaicParticipant();
		}
		
		$this->addSidebarParticipant();
*/		
		$this->setParticipantBackground();
	}
}
