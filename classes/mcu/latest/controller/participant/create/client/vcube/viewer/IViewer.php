<?php

interface IViewer {
	function changeParticipantProfile();
	function removeMosaicParticipant();
}
