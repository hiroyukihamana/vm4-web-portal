<?php
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");

require_once(dirname(__FILE__)."/../../../controller/participant/create/client/exception/LateParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/exception/UnknownParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/pgi/PgiParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/telephonyViewer/TelephonyViewer.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/vcube/flash/FlashParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/vcube/flash/MobileParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/vcube/viewer/FlashViewer.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/vcube/viewer/MobileViewer.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/video/CalledVideoParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/video/InvalidCalleeParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/video/VideoParticipantFromTemporaryDid.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/video/VideoParticipant.php");

require_once(dirname(__FILE__)."/../../../model/do/State.php");
require_once(dirname(__FILE__)."/../../../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../../../model/ConferenceProxy.php");
require_once(dirname(__FILE__)."/../../../params/GetConferenceParams.php");

require_once(dirname(__FILE__)."/../../../../lib/RejectedException.php");

class ParticipantCreatedController {
	static function create($model) {
		$configProxy 	= new McuConfigProxy();
		$dsn = $configProxy->getDsn();
		$logger = EZLogger2::getInstance();

		$conferenceProxy	= ConferenceProxy::getInstance();

		/* conferenceの詳細情報を取得 */
		$mcuControllerProxy	= MCUControllerProxy::getInstance();
		$obj = new GetConferenceParams(array("confId"=>$model->confId));
		$conferenceInfo = $mcuControllerProxy->getConference($obj->getParams());
		if (!$conferenceInfo) {
			if ($participant = $conferenceProxy->existCalleeParticipantWithoutConferenceOnMCU($model->confId, $model->partName)) {
				return new InvalidCalleeParticipant($model, $participant);
			}
			else
				return new LateParticipant($model);
		}
		
		if (!$conferenceProxy->getActiveConferenceByDidWithWatching($conferenceInfo["did"], 3))
			throw new RejectedException("posibility that conference record couldn't created. confId: ".$model->confId);

		ConferenceLogger::info(__CLASS__.":".__FUNCTION__."_start", $model->confId, null, null, null, null, serialize($model));
		$model->setDid($conferenceInfo["did"]);
		$logger->info($model);
		
		
		$gatewayProxy = new McuGatewayProxy();
		$gatewayProxy->setMcuServerByServerIp($_SERVER["REMOTE_ADDR"]);

		$result = split("@", $model->partName);
		if ($model->partType == "SIP") {
			// hardware entered with temporary did
			if (($gatewayProxy->mcuServerModel["guest_domain"] && $gatewayProxy->mcuServerModel["guest_domain"] == $result[1])
					|| ($configProxy->get("videoParticipantGuestDomain") == $result[1])) {
				return new VideoParticipantFromTemporaryDid($model);
			}
			// hardware client
			else if ($gatewayProxy->mcuServerModel["server_address"] == $result[1]
					|| ($configProxy->get("videoParticipantDomain") == $result[1])) {
				return new VideoParticipant($model);
			}
			// PGI client
			else if ($configProxy->get("mcuToPgiDid") == $result[0]) {
				return new PgiParticipant($model);
			}
			// call out.
			else if ($conferenceProxy->isCalleeParticipant($model->confId, $model->did, $model->partName)) {
				return new CalledVideoParticipant($model);
			}
			else {
				return new UnknownParticipant($model);
			}
		}
		else if ($model->partType == "WEB") {
			// view用のPariticipant　判別
			if ($result[0] == $configProxy->get("defaultViewId")) {
				return new FlashViewer($model);
			}
			// mobile client
			else if ($result[0] == $configProxy->get("smallId")) {	// mobile?
				return new MobileParticipant($model);
			}
			// telephony Viewer用
			else if ($result[0] == $configProxy->get("telephoneId")) {	// mobile?
				return new TelephonyViewer($model);
			}
			// flash client
			else {

				return new FlashParticipant($model);
			}
		}
		else {
			return new UnknownParticipant($model);
		}
	}
}
