<?php
require_once(dirname(__FILE__)."/../../../../logic/participant/ParticipantStateChangeLogic.php");

require_once("classes/mcu/config/McuConfigProxy.php");
 
class FailedCalleeParticipant {

	protected $logger;
	protected $logic;
	
	public function __construct($logic) {
		$this->logger = EZLogger2::getInstance();
		$this->logic = $logic;
	}
	
	protected function reject() {}
	
	public function execute() {
		$this->logic->setConferenceRecordByConfId();
				
		$this->logic->setMeetingRecordByMeetingKey();	
		
		$this->logic->changeState();

		$this->logic->noticeCalleeStatus();
	}
}
