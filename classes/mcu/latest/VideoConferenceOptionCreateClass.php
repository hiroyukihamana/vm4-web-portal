<?php
require_once("classes/mcu/config/McuConfigProxy.php");
require_once(dirname(__FILE__)."/VideoConferenceOptionClass.php");
require_once("lib/EZLib/EZCore/EZLogger2.class.php");

class VideoConferenceOptionCreateClass {
	
	const OPTIONKEY_VIDEO = 24; // video_conference
	const OPTIONKEY_TELE = 23; // teleconference
	
	public static function getObjectByRoomKey($roomKey, $is_extend_teleconference = false) {
		if ( !$roomKey ) return null;
		
		static $instance = null;
		if ( !is_null($instance) ) return $instance;
		
		// check IGNORE_MENU
		$ignore = $is_extend_teleconference ? 'teleconference' : 'video_conference';
		if( EZConfig::getInstance()->get( 'IGNORE_MENU', $ignore, false) ) return null;
		
		// check room_option
		$proxy = new McuConfigProxy();
		require_once 'classes/dbi/ordered_service_option.dbi.php';
		$dbi_service_option  = new OrderedServiceOptionTable($proxy->getDsn());
		if($dbi_service_option->enableOnRoom(self::OPTIONKEY_VIDEO, $roomKey)){
			// video_confernce が付いていれば固定didを利用
			$instance = new VideoConferenceOptionClass( $roomKey, false );
		} else if($is_extend_teleconference && $dbi_service_option->enableOnRoom(self::OPTIONKEY_TELE, $roomKey)){
			// 固定did が無くても teleconference が付いていれば解決可能
			$instance = new VideoConferenceOptionClass($roomKey, true);
		} else {
			$instance = false;
		}
		
		return $instance;
		
	}
	
}
