<?php 

require_once("classes/mcu/config/McuConfigProxy.php");
require_once(dirname(__FILE__)."/../logic/MCUConferenceController.php");

class ParticipationController extends MCUConferenceController {	
	
	protected function formatParticipantList($participantList) {
		$sip  = array();
		$rtmp = array();
		$other = array();
		$audience = array();
		if ($participantList) {
			foreach ($participantList as $list) {
				$protocol = $list['participant_protocol'];
				if ("sip" == $protocol) {
					$sip[] = $list;
				} else if ("rtmp" == $protocol) {
					if (2 == $list["participant_type_key"]) {
						$audience[] = $list;
					}
					else if (1 == $list["participant_type_key"]) {
						$rtmp[] = $list;
					}
					else {
						$other[] = $list;
					}
				}
			}
		}
		$participantList = array("sip" => $sip, "rtmp" => $rtmp, "audience" => $audience);
		
		return $participantList;
	}
	/**
	 * 
	 * @param string $confId
	 * @param int $maxRoomSeat
	 * @param int $contractNumber
	 * @return boolean
	 */
	public function checkParticipation($confId, $participatedList, $maxRoomSeat, $contractNumber) {
		$list = $this->mcuControllerProxy->getParticipatedHardWareParticipantListByConfId($confId);
		
		$formatedParticipantList = $this->formatParticipantList($participatedList);
		$participantCount	= count($participantList);
		$sipCount			= count($list);
		$rtmpCount			= count($formatedParticipantList['rtmp']);
		$audience			= count($formatedParticipantList['audience']);
		//
		return (0 != $contractNumber && $maxRoomSeat > ($rtmpCount + $sipCount) && $sipCount < $contractNumber) ? true: false;
	}

	/**
	 * @since callout時にチェックをかけると自分自身が含まれているためpartIdによるremoveを行なった上で数値比較を行う
	 * @param string $confId
	 * @param array<participant record> $participatedList
	 * @param int $maxRoomSeat
	 * @param int $contractNumber
	 * @param int $partId
	 * @return multitype:multitype:unknown  |boolean
	 */
	public function checkParticipationWithoutSelfByPartId($confId, $participatedList, $maxRoomSeat, $contractNumber, $partId) {
		function formatParticipantList($participantList) {
			$sip  = array();
			$rtmp = array();
			$other = array();
			$audience = array();
			if ($participantList) {
				foreach ($participantList as $list) {
					$protocol = $list['participant_protocol'];
					if ("sip" == $protocol) {
						$sip[] = $list;
					} else if ("rtmp" == $protocol) {
						if (2 == $list["participant_type_key"]) {
							$audience[] = $list;
						}
						else if (1 == $list["participant_type_key"]) {
							$rtmp[] = $list;
						}
						else {
							$other[] = $list;
						}
					}
				}
			}
			$participantList = array("sip" => $sip, "rtmp" => $rtmp, "audience" => $audience);
	
			return $participantList;
		}
	
		$list = $this->mcuControllerProxy->getParticipatedHardWareParticipantListByConfIdWithoutSelfPartId($confId, $partId);
	
		$formatedParticipantList = formatParticipantList($participatedList);
		$participantCount	= count($participantList);
		$sipCount			= count($list);
		$rtmpCount			= count($formatedParticipantList['rtmp']);
		$audience			= count($formatedParticipantList['audience']);
		//
		return (0 != $contractNumber && $maxRoomSeat > ($rtmpCount + $sipCount) && $sipCount < $contractNumber) ? true: false;
	}
}
