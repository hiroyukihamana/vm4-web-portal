<?php 
require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../model/MeetingProxy.php");
require_once(dirname(__FILE__)."/../model/ConferenceProxy.php");
require_once(dirname(__FILE__)."/../params/SetCompositionTypeParams.php");

class MCUConferenceController {
	protected $conferenceProxy;
	protected $mcuControllerProxy;
	protected $meetingProxy;
	
	protected $configProxy;
	protected $logger;
	
	function __construct() {
		$this->configProxy 	= new McuConfigProxy();
		$this->logger = EZLogger2::getInstance();

		$this->conferenceProxy		= ConferenceProxy::getInstance();
		$this->mcuControllerProxy	= MCUControllerProxy::getInstance();
		$this->meetingProxy			= MeetingProxy::getInstance();
	}
}