<?php 

require_once(dirname(__FILE__)."/../../logic/MCUConferenceController.php");

class BFCPCompositionManager extends MCUConferenceController {
	protected $hdFlag;
	protected $size;
	protected $profileId;

	function __construct($confId, $hdFlag=flase) {
		parent::__construct();
		
		$this->confId = $confId;
		$this->hdFlag = $hdFlag;
	}

	public function run() {
		//polycom端末はdual displayため、layout変更する必要がなくなった。
		//$this->setCompositionTypeForConferenceParticipant();
	
		$this->setCompositionTypeForMobileParticipant();
	
		$this->setCompositionTypeForFlashParticipant();

	}
	private function getHd720pSettings() {
		if($this->hdFlag) {
			$this->size = $this->configProxy->get("hd_size");
			$this->profileId = $this->configProxy->get("polycomHD720ProfileId"); 
		}
		else {
			$this->size = $this->configProxy->get("size");
			$this->profileId = $this->configProxy->get("polycomProfileId"); 
		}
	}

	protected function setCompositionTypeForConferenceParticipant() {
		$this->getHd720pSettings();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_CONFERENCE,
				"compType"	=> ONE_X_ONE,
				"size"		=> $this->size,
				"profileId"	=> $this->profileId));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	protected function setCompositionTypeForMobileParticipant() {
		$this->getHd720pSettings();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_MOBILE,
				"compType"	=> ONE_X_ONE,
				"size"		=> $this->size,
				"profileId"	=> $this->profileId));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	protected function setCompositionTypeForFlashParticipant() {
		$this->getHd720pSettings();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_FLASH,
				"compType"	=> ONE_X_ONE,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("flashProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
}
