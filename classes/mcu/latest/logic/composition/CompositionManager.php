<?php
require_once(dirname(__FILE__)."/../../logic/MCUConferenceController.php");
require_once(dirname(__FILE__)."/../../params/SetCompositionTypeParams.php");

class CompositionManager extends MCUConferenceController {
	protected $totalCount;
	protected $countHardwareParticipant;
	protected $confId;
	protected $activeSpeaker;
	protected $mobile4x4;
	protected $bfcpStatus;
	protected $stbStatus;
	protected $hdFlag;
	
	public function __construct($confId, $totalCount, $countHardwareParticipant, $activeSpeaker=true, $mobile4x4=false, $bfcpStatus=false, $stbStatus=false, $hdFlag=false) {
		parent::__construct();
		
		$this->count = $totalCount;
		$this->countHardwareParticipant	= $countHardwareParticipant;
		$this->confId = $confId;
		$this->activeSpeaker = $activeSpeaker;
		$this->mobile4x4 = $mobile4x4;
		$this->bfcpStatus = $bfcpStatus;
		$this->stbStatus = $stbStatus;
		$this->hdFlag = $hdFlag;
	}
	
	public function run() {
		// polycom側　composition
		$this->setCompositionTypeForConferenceParticipant();

		// mobile側　composition
		if ($this->bfcpStatus) {		
        	$this->setBfcpCompositionTypeForMobileParticipant();
		} else if ($this->stbStatus) {
			$this->setCompositionTypeForStbMobileParticipant();
		} else if ($this->hdFlag) {
			$this->setCompositionTypeForHd720pMobileParticipant();
		} else {
			$this->setCompositionTypeForMobileParticipant();
		}
		
		// mobile側　composition
		if($this->stbStatus) {
			$this->setCompositionTypeForStbFlashParticipant();
		}
		else if($this->hdFlag) {
			$this->setCompositionTypeForHd720pFlashParticipant();
		} else {
			$this->setCompositionTypeForFlashParticipant();
		}
		
	}
	
	public function runHardwareComposition() {
		$this->setCompositionTypeForConferenceParticipant();
	}
	
	protected function setCompositionTypeForConferenceParticipant() {
		if ($this->stbStatus) {
			$size = $this->configProxy->get("hd_size");
			$profile = $this->configProxy->get("stbHDProfileId");
		} else if ($this->hdFlag) { //HD720p  オプション
			$size = $this->configProxy->get("hd_size");
			$profile = $this->configProxy->get("polycomHD720ProfileId");
		} 
		else {
			$size = $this->configProxy->get("size");
			$profile = $this->configProxy->get("polycomProfileId");
		}
		
		$compositionType = $this->getVideoParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_CONFERENCE,
				"compType"	=> $compositionType,
				"size"		=> $size,
				"profileId"	=> $profile));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	protected function setCompositionTypeForStbMobileParticipant() {
		$compositionType = $this->getMobileParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_MOBILE,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("hd_size"),
				"profileId"	=> $this->configProxy->get("stbHDProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	protected function setCompositionTypeForHd720pMobileParticipant() {
		$compositionType = $this->getMobileParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_MOBILE,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("hd_size"),
				"profileId"	=> $this->configProxy->get("polycomHD720ProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	protected function setCompositionTypeForMobileParticipant() {
		$compositionType = $this->getMobileParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_MOBILE,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("mobileProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	protected function setBfcpCompositionTypeForMobileParticipant() {
		$compositionType = $this->getMobileParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_MOBILE,
				"compType"	=> ONE_X_ONE,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("mobileProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	protected function setCompositionTypeForFlashParticipant() {
		$compositionType = $this->getFlashParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_FLASH,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("flashProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	protected function setCompositionTypeForStbFlashParticipant() {
		$compositionType = $this->getFlashParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_FLASH,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("hd_size"),
				"profileId"	=> $this->configProxy->get("stbflashProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	protected function setCompositionTypeForHd720pFlashParticipant() {
		$compositionType = $this->getFlashParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_FLASH,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("hd_size"),
				"profileId"	=> $this->configProxy->get("polycomHD720ProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}

	private function getFlashParticipantCompositionType() {
		if ($this->activeSpeaker) {
			$compositionType = ONE_X_ONE;
		}
		else {
			if (1 == $this->countHardwareParticipant) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->countHardwareParticipant && $this->countHardwareParticipant <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $this->countHardwareParticipant) {
				$compositionType = THREE_X_THREE;
			}
			else {
				// 				return false;
// 				throw new Exception("check participant number.");
				$compositionType = ONE_X_ONE;
			}
		}
		return $compositionType;
	}
	
	protected function getMobileParticipantCompositionType() {
		if ($this->mobile4x4) {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->count && $this->count <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $this->count && $this->count <= 9) {
				$compositionType = THREE_X_THREE;
			}
			else if ($this->count > 9) {
				$compositionType = FOUR_X_FOUR;
			}
		}
		else if ($this->stbStatus)
		{
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->count && $this->count <= 2) {
				$compositionType = ONE_PLUS_ONE;
			}
			else if (2 < $this->count && $this->count <= 6) {
				$compositionType = ONE_PLUS_FIVE;	
			} 
			else {
				$compositionType = ONE_PLUS_SEVEN;
			}
		}
		else {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else {
				$compositionType = TWO_X_TWO;
			}
		}
		return $compositionType;
	}
	
	public function getVideoParticipantCompositionType() {
		if ($this->activeSpeaker) {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (2 == $this->count) {
				$compositionType = ONE_PLUS_ONE;
			}
			else if (3 <= $this->count && $this->count <= 6) {
				$compositionType = ONE_PLUS_FIVE;
			}
			else {
				$compositionType = ONE_PLUS_SEVEN;
			}
		}
		else {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->count && $this->count <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $this->count) {
				$compositionType = THREE_X_THREE;
			}
		}
		return $compositionType;
	}
}
