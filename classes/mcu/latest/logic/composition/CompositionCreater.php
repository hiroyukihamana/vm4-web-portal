<?php
require_once("classes/mcu/config/McuConfigProxy.php"); 

require_once(dirname(__FILE__)."/../../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../../model/MeetingProxy.php");
require_once(dirname(__FILE__)."/../../model/ConferenceProxy.php");

require_once(dirname(__FILE__)."/../../logic/composition/BFCPCompositionManager.php");
require_once(dirname(__FILE__)."/../../logic/composition/CompositionManager.php");
require_once(dirname(__FILE__)."/../../logic/composition/DocumentSharingCompositionManager.php");
require_once(dirname(__FILE__)."/../../logic/composition/MobileMixCompositionManager.php");
require_once(dirname(__FILE__)."/../../logic/composition/SalesMobileMixCompositionManager.php");
require_once(dirname(__FILE__)."/../../logic/FMSNetConnectionController.php");

class CompositionCreater {
	public static function createByConfId($confId) {
		// proxy
		$configProxy 	= new McuConfigProxy();
		$logger = EZLogger2::getInstance();
		
		$conferenceProxy		= ConferenceProxy::getInstance();
		$mcuControllerProxy		= MCUControllerProxy::getInstance();
		$meetingProxy			= MeetingProxy::getInstance();
		
		// records
		// conference record.
		$conferenceProxy->setActiveConferenceByConfIdThrowable($confId);
			
		// meeting record.
		$meetingProxy->setActiveMeetingRecordByMeetingKeyThrowable($conferenceProxy->activeConferenceRecord["meeting_key"]);
		
		$fmsController = new FMSNetConnectionController($meetingProxy->activeMeetingRecord);
		$fmsController->connect();
		$bfcpStatus = $fmsController->getBFCPStatus();
		
		//activeConferenceのdocSharingStateを取得
		$bfcpActiveState = $mcuControllerProxy->getBfcpStateFromMCU($confId);
		if ($bfcpActiveState["docSharingState"] == "ACTIVE") $bfcpStatus = true;
		
		// room record
		$meetingProxy->setRoomRecordByRoomKeyThrowable($meetingProxy->activeMeetingRecord["room_key"]);
		
		// ives_setting record
		$meetingProxy->setSettingRecordByRoomKey($meetingProxy->activeMeetingRecord["room_key"]);
		try {
			$totalCount = count($mcuControllerProxy->getParticipatedVideoParticipantListByConfId($confId));
			$countHardwareParticipant = count($mcuControllerProxy->getParticipatedHardWareParticipantListByConfId($confId));
			if ($meetingProxy->activeMeetingRecord && $meetingProxy->isSalesConferenceByMeetingKey($meetingProxy->activeMeetingRecord["meeting_key"])) {
				$obj = new SalesMobileMixCompositionManager(
											$confId, 
											$totalCount, 
											$countHardwareParticipant, 
											$meetingProxy->activeSpeaker, 
											$meetingProxy->roomRecord["mobile_4x4_layout"]
											);
			}
			else if ($conferenceProxy->activeConferenceRecord && !$conferenceProxy->activeConferenceRecord["document_sharing_status"]) {
				$obj = new CompositionManager($confId,
											$totalCount,
											$countHardwareParticipant,
											$meetingProxy->activeSpeaker, 
											$meetingProxy->roomRecord["mobile_4x4_layout"], 
											$bfcpStatus, 
											$meetingProxy->roomRecord["use_stb_option"],
											$meetingProxy->settingRecord["mcu_hd_flg"]
											);
			}
			// 		else if ($fmsController->getBFCPStatus()) {
			// 		}
			else {
				$obj = new DocumentSharingCompositionManager(
											$confId, 
											$totalCount, 
											$countHardwareParticipant, 
											$meetingProxy->activeSpeaker, 
											$roomRecord["mobile_4x4_layout"], 
											$bfcpStatus,
											$meetingProxy->roomRecord["use_stb_option"],
											$meetingProxy->settingRecord["mcu_hd_flg"]
											);
			}
			return $obj;
		} catch (Exception $e) {
			return;
		}	
		
	}
	
	public static function createBFCPComposition($confId) {
		$configProxy 	= new McuConfigProxy();
		$conferenceProxy		= ConferenceProxy::getInstance();
		$mcuControllerProxy		= MCUControllerProxy::getInstance();
		$meetingProxy			= MeetingProxy::getInstance();
		
		// records
		// conference record.
		$conferenceProxy->setActiveConferenceByConfIdThrowable($confId);
		
		// meeting record.
		$meetingProxy->setActiveMeetingRecordByMeetingKeyThrowable($conferenceProxy->activeConferenceRecord["meeting_key"]);

		// ives_setting record
		$meetingProxy->setSettingRecordByRoomKey($meetingProxy->activeMeetingRecord["room_key"]);

		return new BFCPCompositionManager($confId, $meetingProxy->settingRecord["mcu_hd_flg"]);
	}
}
