<?php
require_once(dirname(__FILE__)."/../../logic/composition/CompositionManager.php");
 
class MobileMixCompositionManager extends CompositionManager {
	public function run() {
		$this->setCompositionTypeForMobileParticipant();
	}
}
