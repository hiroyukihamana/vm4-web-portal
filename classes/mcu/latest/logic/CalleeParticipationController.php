<?php 
class CalleeParticipationController extends ParticipationController {
	/**
	 *
	 * @param string $confId
	 * @param int $maxRoomSeat
	 * @param int $contractNumber
	 * @return boolean
	 */
	public function checkParticipation($confId, $participatedList, $maxRoomSeat, $contractNumber) {
		/*
		function formatParticipantList($participantList) {
			$sip  = array();
			$rtmp = array();
			$other = array();
			$audience = array();
			if ($participantList) {
				foreach ($participantList as $list) {
					$protocol = $list['participant_protocol'];
					if ("sip" == $protocol) {
						$sip[] = $list;
					} else if ("rtmp" == $protocol) {
						if (2 == $list["participant_type_key"]) {
							$audience[] = $list;
						}
						else if (1 == $list["participant_type_key"]) {
							$rtmp[] = $list;
						}
						else {
							$other[] = $list;
						}
					}
				}
			}
			$participantList = array("sip" => $sip, "rtmp" => $rtmp, "audience" => $audience);
	
			return $participantList;
		}
		*/
	
		$list = $this->mcuControllerProxy->getParticipatedHardWareParticipantListByConfId($confId);
	
		$formatedParticipantList = $this->formatParticipantList($participatedList);
		$participantCount	= count($participantList);
		$sipCount			= count($list) - 1;
		$rtmpCount			= count($formatedParticipantList['rtmp']);
		$audience			= count($formatedParticipantList['audience']);
		//
		return (0 != $contractNumber && $maxRoomSeat > ($rtmpCount + $sipCount) && $sipCount < $contractNumber) ? true: false;
	}
}