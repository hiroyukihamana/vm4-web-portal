<?php

require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");

require_once("classes/mcu/model/McuModel.php");

require_once(dirname(__FILE__)."/../model/ConferenceProxy.php");
require_once(dirname(__FILE__)."/../model/FMSProxy.php");
require_once(dirname(__FILE__)."/../model/MeetingProxy.php");
 
class FMSNetConnectionController {
	private $configProxy;
	private $meetingProxy;
	private $fmsProxy;
	public function __construct($meetingRecord) {
		$this->configProxy	= new McuConfigProxy();
		$this->logger = EZLogger2::getInstance();
		
		$this->conferenceProxy		= ConferenceProxy::getInstance();
		$this->meetingProxy			= MeetingProxy::getInstance();
		$this->meetingProxy->setMeetingRecord($meetingRecord);
		
		$this->fmsProxy = new FMSProxy();
		$this->fmsProxy->setHost($this->meetingProxy->getFMSServerDomain());
		$this->fmsProxy->setAppName($this->meetingProxy->getFMSInstancePath());
	}
	
	public function connect() {
		$this->fmsProxy->connect();
	}
	
	public function bootFMSInstance($settingRecord=null) {
		$this->mcuGatewayProxy = new McuGatewayProxy();
		$settingRecord
		? $this->mcuGatewayProxy->setMcuServerModelByRoomKey($settingRecord["room_key"])
		: $this->mcuGatewayProxy->setLatestModel();
		$info = array(
				"status"			=> true,
				"contract"			=> true,
				"sipServer"			=> $this->mcuGatewayProxy->getMCUControllerServer(),
				"sipServerDomain"	=> $this->mcuGatewayProxy->getServerDomain(),
				"sipProxyIp"		=> $this->mcuGatewayProxy->getSipProxy(),
				"sipProxyPort"		=> $this->mcuGatewayProxy->getSipProxyPort(),
				"socketServer"		=> $this->mcuGatewayProxy->getSocketServer(),
				"socketServerPort"	=> $this->mcuGatewayProxy->getSocketPort(),
				"defaultId"			=> ($settingRecord)?$settingRecord['client_id']:$this->configProxy->get("defaultId"),
				"defaultPw"			=> ($settingRecord)?$settingRecord['client_pw']:$this->configProxy->get("defaultPw"),
				"defaultViewId"		=> $this->configProxy->get("defaultViewId"),
				"defaultViewPw"		=> $this->configProxy->get("defaultViewPw"),
				"smallId"			=> $this->configProxy->get("smallId"),
				"smallPw"			=> $this->configProxy->get("smallPw"),
				"telephoneId"		=> $this->configProxy->get("telephoneId"),
				"telephonePw"		=> $this->configProxy->get("telephonePw"),
				"did"				=> $settingRecord['ives_did']);
		$model = new McuModel($info);
		
		$data = array("type"=>"onConfrenceCreated","data"=>$model->getContent());
		$this->fmsProxy->bootFMSInstance(json_encode($data));
	}
	
	public function noticeParticipantListChanged($params) {
		$data = array("type"=>"onParticipantNumberChange","data"=>$params);
		
		$this->fmsProxy->noticeParticipantListChanged(json_encode($data));
	}
	
	public function noticeBFCPStatus($flag = true, $partId = null) { 
		$result = $this->fmsProxy->noticeBFCPStatus($flag, $partId);
	}
	
	public function getDocumentSharingStatus() {
		$result = $this->fmsProxy->getDocumentSharingStatus();
		return $result;
	}

	public function getBfcpStatus() {
		$result = $this->fmsProxy->getBfcpStatus();
		return $result;
	}
}
