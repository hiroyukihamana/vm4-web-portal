<?php 

require_once(dirname(__FILE__)."/../../logic/participant/ParticipantCreatedLogic.php");
require_once(dirname(__FILE__)."/../../model/do/State.php");

class ParticipantStateChangeLogic extends ParticipantCreatedLogic {
	public $conferenceParticipant;
	public function __construct($model) {
		parent::__construct($model);
	}

	// override
	public function checkParticipatedNumber() {
		if (!$this->roomRecord) {
			throw new Exception("no room record.");
		}
		if (!$this->settingRecord) {
			throw new Exception("no setting record.");
		}
	
		$participationController = new ParticipationController();
		return $participationController->checkParticipationWithoutSelfByPartId($this->requestProxy->confId, $this->participantRecordList, $this->roomRecord["max_seat"], $this->settingRecord["num_profiles"], $this->requestProxy->partId);
	}
	
	public function setConferenceActiveRecordByConfId() {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByConfId($this->requestProxy->confId);
		if (!$conferenceRecord)
			throw new Exception("ParticipantStateChangeLogic::no conference. did: ".$this->requestProxy->confId);
		else if (count($conferenceRecord) != 1)
			throw new Exception("ParticipantStateChangeLogic::check active conference. did: ".$this->requestProxy->confId);
		else
			$this->conferenceRecord = $conferenceRecord[0];
	}
	
	public function setConferenceRecordByConfId() {
		$conferenceRecord = $this->conferenceProxy->getConferenceByConfId($this->requestProxy->confId);
		if (!$conferenceRecord)
			throw new Exception("ParticipantStateChangeLogic::no conference. did: ".$this->requestProxy->confId);
		else
			$this->conferenceRecord = $conferenceRecord;
	}
	
	public function setMeetingRecordByMeetingKey() {
		$this->meetingRecord = $this->meetingProxy->getMeetingByMeetingKey($this->conferenceRecord["meeting_key"]);
		if (!$this->meetingRecord)
			throw new Exception("ParticipantStateChangeLogic::no meeting record. meetingKey:".$this->conferenceRecord["meeting_key"]);
	}
	
	public function setRoomRecord() {
		$this->roomRecord = $this->meetingProxy->getRoomRecordByRoomKey($this->meetingRecord["room_key"]);
		if (!$this->roomRecord)
			throw new Exception("ParticipantStateChangeLogic::no room record. roomKey:".$this->meetingRecord["room_key"]);
	}
	
	public function setSettingRecord() {
		$settingRecord = $this->meetingProxy->getSettingRecordByDid($this->conferenceRecord["did"]);
		
		if (1 == count($settingRecord))
			$this->settingRecord = $settingRecord[0];
		else if (count($settingRecord) > 1)
			throw new Exception("check issued did: ".$this->requestProxy->did);
	}
	
	public function setParticipantRecordList() {
		if (!$this->meetingRecord)
			throw new Exception("irregal meeting record. meeting_key:".$this->meetingRecord["meeting_key"]);
		$this->participantRecordList = $this->meetingProxy->getParticipantByMeetingKey($this->meetingRecord["meeting_key"]);
	}
	
	public function setConferenceParticipantRecord() {
		$this->conferenceParticipant = $this->conferenceProxy->getConferenceParticipantByConfIdPartId($this->requestProxy->confId, $this->requestProxy->partId);
		if (!$this->conferenceParticipant)
			throw new Exception("ParticipantStateChangeLogic::unknown client. confId: ".$this->requestProxy->confId.", partId: ".$this->requestProxy->partId);
	}
	
	
	public function changeState() {

		$this->conferenceProxy->updateState($this->requestProxy);
	}
	
	public function removeParticipant() {
		$this->mcuControllerProxy->removeParticipant($this->requestProxy->confId, $this->requestProxy->partId);
	}
	
	// override
	public function createParticipantRecord() {
		$data = array(
				"participant_session_id"=> $this->meetingProxy->getSessionId(),
				"meeting_key"			=> $this->meetingRecord["meeting_key"],
				"participant_name"		=> ($this->conferenceParticipant["participant_name"] == $this->configProxy->get("callout_name")) ? $this->conferenceParticipant["participant_ip"] : $this->conferenceParticipant["participant_name"],
				"participant_protocol"	=> "sip",
				"update_datetime"		=> date("Y-m-d H:i:s"),
				"is_active"				=> 1,
				"uptime_start"			=> date("Y-m-d H:i:s"),
				"uptime_end"			=> null,
				"is_narrowband"			=> 0,
				"use_count"				=> 0,
				"participant_locale"	=> 'ja',
				"create_datetime"		=> date('Y-m-d H:i:s'),
				"ives_part_id"			=> $this->requestProxy->partId
		);
		$result = $this->meetingProxy->addParticipant($data);
		if ($result)
			$this->participantRecord = $result;
		else
			throw new Exception("Failed in creating participant record.");
	}
	
	public function activateVideoParticipantRecord() {
		$data = array(
				"participant_key"=>$this->participantRecord["participant_key"],
				"is_active"=>1,
				"updatetime"=>date("Y-m-d H:i:s"));
		$this->conferenceProxy->updateCallingParticipant($data, $this->conferenceParticipant["video_participant_key"]);		
	}
	
	/**
	 * <p>callout により呼びだされたClientによる接続完了判別</p>
	 * @return boolean
	 */
	public function isConnectionCompleteCallerParticipant() {
		return (!$this->conferenceParticipant["participant_key"] && 
			!$this->conferenceParticipant["is_active"] &&
			$this->requestProxy->partState == State::CONNECTED());
	}
	
	/**
	 * <p>Calloutにより呼びだされたClientからの接続失敗判別</p>
	 * @return boolean
	 */
	public function isConnectionFailedCallerParticipant() {
		return
		// h323
		($this->conferenceParticipant["participant_name"] == $this->configProxy->get("callout_name") &&
		!$this->conferenceParticipant["is_active"] &&
		($this->requestProxy->partState == State::ERROR() ||
		$this->requestProxy->partState == State::TIMEOUT() ||
		$this->requestProxy->partState == State::DISCONNECTED())) ||
		// sip
// 		(0 == $this->conferenceParticipant["participant_key"] &&
		(!$this->conferenceParticipant["is_active"] &&
		$this->requestProxy->partState == State::DECLINED() ||
		$this->requestProxy->partState == State::BUSY() ||
		$this->requestProxy->partState == State::ERROR());
	}
}
