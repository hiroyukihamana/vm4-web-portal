<?php 

require_once(dirname(__FILE__)."/../../logic/participant/ALogic.php");

class InvalidParticipantLogic extends ALogic {
	public function setConferenceRecord() {
		$this->conferenceProxy->getConferenceByConfIdWithThrowableException($this->requestProxy->confId);
	}
	
	public function setMeetingRecord() {
		$this->meetingRecord = $this->meetingProxy->getMeetingByMeetingKey($this->conferenceProxy->conferenceRecord["meeting_key"]);
	}
	
	public function setCalleeParticipant($participant) {
		$this->conferenceParticipant = $participant;
	}
	
	/**
	 * <p></p>
	 */
	public function voidCalleeParticipant() {
		$this->conferenceProxy->voidCalleeParticipant($this->conferenceParticipant);
	}
}
