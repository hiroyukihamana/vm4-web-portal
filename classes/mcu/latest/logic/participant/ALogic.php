<?php
require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../../logic/composition/CompositionCreater.php");
require_once(dirname(__FILE__)."/../../logic/MosaicController.php");

require_once(dirname(__FILE__)."/../../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../../model/MeetingProxy.php");
require_once(dirname(__FILE__)."/../../model/ConferenceProxy.php");

abstract class ALogic {
	protected $soap;
	public $requestProxy;
	public $logger;
	
	protected $meetingRecord;
	protected $participantRecord;
	protected $participantRecordList;
	protected $roomRecord;
	protected $settingRecord;
	protected $conferenceRecord;
	protected $conferenceParticipantRecord;
	protected $conferenceParticipantRecordList;
	protected $userRecord;
	
	//proxy
	protected $mcuControllerProxy;
	protected $meetingProxy;
	protected $conferenceProxy;

// 	protected $mosaicManager;
	protected $mosaicController;
	
	public function __construct($model) {

		$this->configProxy 	= new McuConfigProxy();
		$this->dsn = $this->configProxy->getDsn();
		$this->logger = EZLogger2::getInstance();
		
		$this->requestProxy	= $model;
		
		// proxy
		$this->mcuControllerProxy	= MCUControllerProxy::getInstance();
		$this->meetingProxy			= MeetingProxy::getInstance();
		$this->conferenceProxy		= ConferenceProxy::getInstance();

		//
		$this->mosaicController = new MosaicController();
	}
	
	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return null;
		}
	}
	
	public function getParticipatedCount() {
		return count($this->mcuControllerProxy->getParticipatedVideoParticipantListByConfId($this->requestProxy->confId));
	}

	// activate vad
	private function controlActivateVadSlotForDefaultMosaic() {
		if ($this->settingRecord &&
			$this->settingRecord["use_active_speaker"] &&
			$this->getParticipatedCount() >= 3 &&
			($this->conferenceRecord && !$this->conferenceRecord["document_sharing_status"])) {
			
			$params = $this->mosaicController->getDefaultMosaicSlotParamsToActivateVadSlot($this->requestProxy->confId);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}
	}
	
	private function controlActivateVadSlotForMobile() {
		if (($this->roomRecord["mobile_4x4_layout"] &&
			$this->getParticipatedCount() == 17) ||
			(!$this->roomRecord["mobile_4x4_layout"] &&
			$this->getParticipatedCount() == 10)) {
			
			$params = $this->mosaicController->getMobileMosaicSlotParamsToActivateVadSlot($this->requestProxy->confId);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}
	}
	
	private function controlActivateVadSlotForSTB() {
		if ($this->getParticipatedCount() > 2){
			$params = $this->mosaicController->getMobileMosaicSlotParamsToActivateVadSlot($this->requestProxy->confId);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}
	}
	
	
	public function controlActivateVadSlot() {
		$this->controlActivateVadSlotForDefaultMosaic();

		if ($this->meetingRecord["use_stb_option"]) {
			$this->controlActivateVadSlotForSTB();
		} else {
			$this->controlActivateVadSlotForMobile();
		}
	}
	// activate vad
	
	// deactivate vad
	private function controlDeActivateVadSlotForDefaultMosaic() {
		if ($this->settingRecord["use_active_speaker"] && $this->getParticipatedCount() == 2) {
			$params = $this->mosaicController->getDefaultMosaicSlotParamsToDeActivateVadSlot($this->requestProxy->confId);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}
	}
	
	private function controlDeActivateVadSlotForMobile() {
		if (($this->roomRecord["mobile_4x4_layout"] &&
			$this->getParticipatedCount() == 16) ||
			(!$this->roomRecord["mobile_4x4_layout"] &&
			$this->getParticipatedCount() == 9)) {

			$params = $this->mosaicController->getMobileMosaicSlotParamsToDeActivateVadSlot($this->requestProxy->confId);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}
	}

	private function controlDeActivateVadSlotForSTB() {
		if ($this->getParticipatedCount() <= 2 ){
			$params = $this->mosaicController->getMobileMosaicSlotParamsToDeActivateVadSlot($this->requestProxy->confId);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}
	}
	
	public function controlDeActivateVadSlot() {
		$this->controlDeActivateVadSlotForDefaultMosaic();
		if ($this->meetingRecord["use_stb_option"]) {
			$this->controlDeActivateVadSlotForSTB();
		}
		else {
			$this->controlDeActivateVadSlotForMobile();
		}
	}
	
	/**
	 * 
	 */
	public function setCompositionType() {
		$obj = CompositionCreater::createByConfId($this->requestProxy->confId);
		if ($obj)
			$obj->run();
	}

	public function getParticipantNumberOnMCU() {
		return count($this->mcuControllerProxy->getParticipantListByConfId($this->requestProxy->confId));
	}
	
	public function isActiveConferenceOnMCU() {
		$obj = new GetConferenceParams(array("confId" => $this->requestProxy->confId));
		$return = $this->mcuControllerProxy->getConference($obj->getParams());
		return $return;
	}
	
	public function destroyConference() {
		$this->mcuControllerProxy->removeConferenceByConfId($this->requestProxy->confId);
	}


	// IRoom
	public function setRoomRecordByRoomKey() {
		$this->roomRecord = $this->meetingProxy->getRoomRecordByRoomKey($this->meetingRecord["room_key"]);
	}
	// IRoom
	
	public function noticeParticipantListChanged() {
		if (!$this->meetingRecord)
			throw new Exception(__CLASS__."::".__FUNCTION__."::no meeting record");
		
		require_once(dirname(__FILE__)."/../../logic/FMSNetConnectionController.php");

		$fmsController = new FMSNetConnectionController($this->meetingRecord);
		$fmsController->connect();
		
		$list = $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByMeetingKey($this->meetingRecord["meeting_key"]);
		$data = array("participantList"=>$list,"callingStatus"=>"");
		$fmsController->noticeParticipantListChanged($data);
	}

	/**
	 * <p>call out で呼ばれたClientのstatus
	 * 及びSipClientの配列をFMSへ通知</p>
	 */
	public function noticeCalleeStatus() {
		require_once(dirname(__FILE__)."/../../logic/FMSNetConnectionController.php");
	
		$fmsController = new FMSNetConnectionController($this->meetingRecord);
		$fmsController->connect();
		$status = $this->conferenceProxy->getCalleeParticipantStatusVideoParticipantKey($this->conferenceParticipant["video_participant_key"]);
		$list = $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByConfId($this->requestProxy->confId);
		$data = array("participantList"=>$list,"callingStatus"=>$status);
		$fmsController->noticeParticipantListChanged($data);
	}
}
