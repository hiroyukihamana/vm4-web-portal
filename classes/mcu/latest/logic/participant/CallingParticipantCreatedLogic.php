<?php 
require_once(dirname(__FILE__)."/../../logic/participant/ParticipantCreatedLogic.php");
require_once(dirname(__FILE__)."/../../logic/CalleeParticipationController.php");

class CallingParticipantCreatedLogic extends ParticipantCreatedLogic {
	public $callingParticipant;
	public function setCallerParticipant() {
		$this->callingParticipant = $this->conferenceProxy->getCallingParticipant($this->meetingRecord["meeting_key"], $this->settingRecord["ives_did"]);
		if (!$this->callingParticipant)
			throw new Exception("uninvited guest. meetingKey: ".$this->meetingRecord["meeting_key"].", did: ".$this->settingRecord["ives_did"]);
	}
	
	public function updateCallerParticipant() {
		$data = array(
				"video_conference_key"=>$this->conferenceRecord["video_conference_key"],
				"media_mixer_key"=>$this->conferenceRecord["media_mixer_key"],
				"conference_id"=>$this->requestProxy->confId,
				"part_id"=>$this->requestProxy->partId,
				"updatetime"=>date("Y-m-d H:i:s"));
		$this->conferenceProxy->updateCallingParticipant($data, $this->callingParticipant["video_participant_key"]);
		list($partName) = explode("-", $this->requestProxy->partName);
		$operationLogData = array(
			'user_key'           => $this->meetingRecord['user_key'],
			'remote_addr'        => '',
			'action_name'        => 'meeting_in',
			'operation_datetime' => date('Y-m-d H:i:s'),
			'info'               => serialize(array(
				"participant_name" => $partName,
				"room_key"         => $this->meetingRecord['room_key'],
			)),
		);
		$this->meetingProxy->operationLog->add($operationLogData);
	}
	
	// IParticipant
	public function checkParticipatedNumber() {
		if (!$this->roomRecord) {
			throw new Exception("no room record.");
		}
		if (!$this->settingRecord) {
			throw new Exception("no setting record.");
		}
// 		if (!$this->participantRecordList)
// 			return true;
		
		$participationController = new CalleeParticipationController();
		return $participationController->checkParticipation($this->requestProxy->confId, $this->participantRecordList, $this->roomRecord["max_seat"], $this->settingRecord["num_profiles"]);
	}
}
