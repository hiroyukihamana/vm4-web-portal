<?php

require_once(dirname(__FILE__)."/../../logic/participant/ALogic.php");
 
class ParticipantDestroyedLogic extends ALogic {
	public function __construct($model) {
		parent::__construct($model);
	}
	

	// IConference
	function removeConferenceRecord() {
	
	}
	
	function setConferenceRecord() {
		$conferenceRecord = $this->conferenceProxy->getConferenceByConfId($this->requestProxy->confId);
		if (!$conferenceRecord) {
			$this->logger->warn("irregal conference record. confId:".$this->requestProxy->confId);
		}
		$this->conferenceRecord = $conferenceRecord;
	}
	// IConference
	
	// IConferenceParticipant
	function removeConferenceParticipantRecord() {
		$this->conferenceProxy->removeConferenceParticipantRecord($this->requestProxy->confId, $this->requestProxy->partId);
	}
	
	function setConferenceParticipantRecord() {
		$conferenceParticipantRecord	= $this->conferenceProxy->getConferenceParticipant($this->requestProxy->confId, $this->requestProxy->partId);
		$this->conferenceParticipantRecord	= $conferenceParticipantRecord[0];
		
	}
	
	function setConferenceParticipantList() {
		$this->conferenceParticipantRecordList	= $this->conferenceProxy->getActiveConferenceParticipantListByConfId($this->requestProxy->confId);
	}
	// IConferenceParticipant


	// IMeeting
	function removeMeetingRecord() {
	
	}
	
	function setMeetingRecord() {
		$this->meetingRecord = $this->meetingProxy->getMeetingByMeetingKey($this->conferenceRecord["meeting_key"]);
	}
	// IMeeting
	
	// IParticipant
	function removeParticipantRecord() {
		$this->meetingProxy->removeParticipantByParticipantKey($this->conferenceParticipantRecord["participant_key"]);
	}
	
	function setParticipantRecord() {
		
	}
	// IParticipant
	
	function setParticipantRecordList() {
		$this->participantRecordList = $this->meetingProxy->getParticipantByMeetingKey($this->meetingRecord["meeting_key"]);
	}
	
	// ISetting
	public function setSettingRecordByDid() {
		$settingRecord = $this->meetingProxy->getSettingRecordByDid($this->conferenceRecord["did"]);
	
		if (1 == count($settingRecord))
			$this->settingRecord = $settingRecord[0];
		else if (count($settingRecord) > 1)
			throw new Exception("check issued did: ".$this->requestProxy->did);
	}
	// ISetting
	
	function stopMeeting() {
		$this->meetingProxy->stopMeeting($this->meetingRecord["meeting_key"]);
	}
	
	function stopCount() {
		$this->meetingProxy->stopCount($this->meetingRecord["meeting_key"]);
	}
	
	private function stopRenderingSlot() {
		$this->mcuControllerProxy->stopRenderingInSlotForVideoParticipant($this->requestProxy->confId, 1, 0);
	}
	
	public function stopDocumentSharing() {
		// document_status 終了フラグ
		$this->conferenceProxy->setDocumentSharingStatus($this->conferenceRecord, 0);
		$this->conferenceRecord["document_sharing_status"] = 0;
		
		$this->stopRenderingSlot();
	}
	
	public function noticeParticipantDestroyed() {
		$this->noticeParticipantListChanged();
	}
}
