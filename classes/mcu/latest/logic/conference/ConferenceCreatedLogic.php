<?php 

require_once(dirname(__FILE__)."/../../logic/conference/AbstractLogic.php");
require_once(dirname(__FILE__)."/../../logic/FMSNetConnectionController.php");

require_once(dirname(__FILE__)."/../../model/FMSProxy.php");
require_once(dirname(__FILE__)."/../../params/CreateMosaicParams.php");
require_once(dirname(__FILE__)."/../../params/CreateSidebarParams.php");
require_once(dirname(__FILE__)."/../../params/SetMosaicSlotParams.php");

class ConferenceCreatedLogic extends AbstractLogic{
	public $requestProxy;
	
	// record
	public $conferenceRecord;
	public $conferenceParticipantRecordList;
	public $meetingRecord;
	public $roomRecord;
	public $settingRecord;
	
	private $fmsProxy;
	
	function __construct($model) {
		parent::__construct();
	
		$this->requestProxy			= $model;
		$this->fmsProxy	= new FMSProxy();
	}
	
	public function setSettingRecord() {
		$settingRecord = $this->meetingProxy->getSettingRecordByDid($this->requestProxy->did);
		if (count($settingRecord) > 1)
			throw new Exception("irregal record status. did:".$this->requestProxy->did);
		if (1 == count($settingRecord))
			$this->settingRecord = $settingRecord[0];
	}
	
	public function setRoomRecord() {
		$roomRecord = $this->meetingProxy->getRoomRecordByRoomKey($this->settingRecord["room_key"]);
		if ($roomRecord)
			$this->roomRecord = $roomRecord;
		else
			throw new Exception("irregal room record. room_key: ".$this->settingRecord["room_key"]);
	}
	
	public function createMeetingRecord() {
		$meetingRecord = $this->meetingProxy->createMeeting($this->roomRecord);
		if ($meetingRecord)
			$this->meetingRecord = $meetingRecord;
		else
			throw new Exception("Failed in the generation of the meeting. roomkey: ".$this->roomRecord["room_key"]);
	}
	
	public function createMeetingSequenceRecord() {
		$this->meetingProxy->createMeetingSequenceRecord();
	}
	
	public function resolveEnvironmentData() {
		$this->meetingRecord = $this->meetingProxy->resolveEnvironmentData($this->meetingRecord);
	}
	
	public function setMeetingRecord() {
		$meetingRecord = $this->meetingProxy->getMeetingByRoomRecord($this->roomRecord);
		if ($meetingRecord)
			$this->meetingRecord = $meetingRecord;
// 		else
// 			throw new Exception("no active meeting record.  room_key: ".$this->roomRecord["room_Key"]);
	}
	
	public function createConferenceRecord() {
		$this->mediaController = new FixMediaController();
		$mediaServer = $this->mediaController->getConferenceMediaMixerByAvailableParticipantCount($this->settingRecord["mcu_server_key"]);
		$this->conferenceRecord = $this->conferenceProxy->createConferenceRecord($this->meetingRecord, $this->settingRecord["mcu_server_key"], $mediaServer["media_mixer_key"], $this->requestProxy->confId, $this->requestProxy->did);
	}
	
	public function setConferenceRecord() {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByDid($this->requestProxy->did);
		if (1 == count($conferenceRecord))
			$this->conferenceRecord = $conferenceRecord[0];
	}
	
	public function clearConferenceRecord() {
		$list = $this->conferenceProxy->getActiveConferenceByDid($this->requestProxy->did);
		if (count($list) > 0) {
			$this->logger->warn($list);
			$this->conferenceProxy->clearConferenceRecordByDid($this->requestProxy->did);
		}
	}
	
	public function clearConferenceParticipantRecord() {
		$list = $this->conferenceProxy->getActiveConferenceParticipantByDid($this->requestProxy->did);
		if (count($list)) {
			$this->logger->warn($list);
			
// 			$this->meetingProxy->clearParticipantByConferenceParticipantList($list);
			
			$this->conferenceProxy->clearConferenceParticipantRecordByDid($this->requestProxy->did);			
		}
	}

	private function getMosaicSize() {
		if ($this->settingRecord["mcu_hd_flg"]) {
			$mosaicSize = 6;
		} else {
			$mosaicSize = 2;
		}
		return $mosaicSize;
	}

	private function getCreateFlashMosaic() {
		$size =  $this->getMosaicSize();
		$obj = new CreateMosaicParams(array(
				"confId"   => $this->requestProxy->confId,
				"compType" => 0,
				"size"     => $size));
		return $obj->getParams();
	}
	
	private function getCreateMobileMosaic() {
		$size =  $this->getMosaicSize();
		$obj = new CreateMosaicParams(array(
				"confId"   => $this->requestProxy->confId,
				"compType" => 0,
				"size"     => $size));
		return $obj->getParams();
	}
	
	private function getCreatePgiMosaic() {
		$obj = new CreateMosaicParams(array(
				"confId"   => $this->requestProxy->confId,
				"compType" => 0,
				"size"     => 0));
		return $obj->getParams();
	}
	
	public function createMosaic() {
		$this->createFlashMosaic();
		$this->createMobileMosaic();
		$this->createPgiMosaic();
	}
	
	public function createFlashMosaic() {
		$params = $this->getCreateFlashMosaic();
		$this->mcuControllerProxy->createMosaic($params);
	}
	
	public function createMobileMosaic() {
		$params = $this->getCreateMobileMosaic();
		$this->mcuControllerProxy->createMosaic($params);
	}
	
	public function createPgiMosaic() {
		$params = $this->getCreatePgiMosaic();
		$this->mcuControllerProxy->createMosaic($params);
	}
	
// 	public function setMosaicSlot() {
// 		$this->setConferenceMosaicSlot();
// 		$this->setFlashMosaicSlot();
// 		$this->setMobileMosaicSlot();
// 	}
	
	private function getActiveSpeakerStatus() {
		return ($this->settingRecord && $this->settingRecord["use_active_speaker"]);
	}
	
	private function setConferenceMosaicSlot() {
		$params = $this->mosaicController->getInitConferenceMosaicSlotParams($this->requestProxy->confId, $this->getActiveSpeakerStatus());
		$this->mcuControllerProxy->setMosaicSlot($params);
	}
	
	public function setFlashMosaicSlot() {
		$params = $this->mosaicController->getFlashMosaicSlotParams($this->requestProxy->confId, $this->getActiveSpeakerStatus());
		$this->mcuControllerProxy->setMosaicSlot($params);
	}
	
	private function setMobileMosaicSlot() {
		$params = $this->mosaicController->getMobileMosaicSlotParams($this->requestProxy->confId, $this->getActiveSpeakerStatus());
		$this->mcuControllerProxy->setMosaicSlot($params);
	}
	
	public function getCreateSidebarParams() {
		$obj = new CreateSidebarParams(array("confId"   => $this->requestProxy->confId));
		return $obj->getParams();
	}
	
	public function createSidebar() {
		$this->createFlashSidebar();
		$this->createMobileSidebar();
		$this->createPgiSidebar();
	}
	
	private function createFlashSidebar() {
		$params = $this->getCreateSidebarParams();
		$this->mcuControllerProxy->createSidebar($params);
	}
	
	private function createMobileSidebar() {
		$params = $this->getCreateSidebarParams();
		$this->mcuControllerProxy->createSidebar($params);
	}
	
	private function createPgiSidebar() {
		$params = $this->getCreateSidebarParams();
		$this->mcuControllerProxy->createSidebar($params);
	}
	
	public function bootFMSInstance() {
		if ($this->meetingRecord) {
			$fmsController = new FMSNetConnectionController($this->meetingRecord);
			$fmsController->connect();
			
			$fmsController->bootFMSInstance($this->settingRecord);
		}
	}
	
	public function hasMeetingSequenceActiveRecord() {
		if ($this->meetingRecord) {
			$record = $this->meetingProxy->getMeetingSequenceActiveRecord($this->meetingRecord["meeting_key"]);
			return ($record);
		}
		throw new Exception(__CLASS__."::".__FUNCTION__."::no meeting record.");
	}
	
	public function hasParticipantActiveRecord() {
		if ($this->meetingRecord) {
			$list = $this->meetingProxy->getParticipantByMeetingKey($this->meetingRecord["meeting_key"]);
			return (count($list) > 0);
		}
		throw new Exception(__CLASS__."::".__FUNCTION__."::no meeting record.");
	}
	
	/**
	 * <p>custom_domain利用時,polycom clientの画面上に表示するバックグラウンド画像に指定がある場合変更通知を投げる</p>
	 */
	public function setBackgroundImage() {
		$userRecord = $this->meetingProxy->getActiveUserRecordByUserKeyWithThrowableException($this->roomRecord["user_key"]);
		if ($userRecord["service_name"]) {
			$filename = "backgroundImage_".$userRecord["service_name"];
			$filepath = $this->configProxy->get($filename);
			if ($filepath) $this->mcuControllerProxy->setCustomBackground($this->requestProxy->confId, $filepath);
		}
	}
	
}
