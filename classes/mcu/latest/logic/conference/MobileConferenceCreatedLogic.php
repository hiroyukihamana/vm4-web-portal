<?php 
require_once(dirname(__FILE__)."/../../logic/conference/AbstractLogic.php");

require_once(dirname(__FILE__)."/../../params/CreateMosaicParams.php");
require_once(dirname(__FILE__)."/../../params/CreateSidebarParams.php");
require_once(dirname(__FILE__)."/../../params/GetConferenceParams.php");
require_once(dirname(__FILE__)."/../../params/GetConferenceByDIDParams.php");
require_once(dirname(__FILE__)."/../../params/SetMosaicSlotParams.php");

class MobileConferenceCreatedLogic extends AbstractLogic {
	private $confId;
	
	function __construct() {
		parent::__construct();
		
		$this->mcuControllerProxy->setWSDL($this->mcuGatewayProxy->getMCUControllerServer());
	}
	
	public function getConferenceByDID($did) {
		$obj = new GetConferenceByDIDParams(array("did"=>$did));
		$return = $this->mcuControllerProxy->getConferenceByDID($obj->getParams());
		return ($return) ? $return->id : "";
	}
	
	public function createConferenceForMobile($params) {
		$this->confId = $this->mcuControllerProxy->createConferenceExt($params);
	}
	
	public function getConferenceRecordByMeetingKey($meetingKey) {
		$record = $this->conferenceProxy->getConferenceByMeetingKey($meetingKey);
		return $record;
	}
	
	public function removeConferenceByConfId($confId) {
		$obj = new GetConferenceParams(array("confId"=>$confId));
		$return = $this->mcuControllerProxy->getConference($obj->getParams());
		if ($return)
			$this->mcuControllerProxy->removeConferenceByConfId($confId);
	}
	
	public function removeConferenceRecord($confId) {
		$this->conferenceProxy->clearConferenceRecordByConfId($confId);
	}
	
	public function createConferenceRecord($meetingKey, $did, $mediaMixerKey) {
		$this->meetingProxy->setActiveMeetingRecordByMeetingKeyThrowable($meetingKey);
		$this->mcuGatewayProxy->setMcuServerModelByMeetingKey($meetingKey);
		if ($this->confId) {
			$this->conferenceProxy->addConferenceForMobile($this->meetingProxy->activeMeetingRecord, $this->mcuGatewayProxy->mcuServerModel["server_key"], $mediaMixerKey, $this->confId, $did);
		} else {
			$this->logger->error(array("Mobile conference create error. Please check mediaserver : ".$mediaMixerKey, $this->meetingProxy->activeMeetingRecord, $this->mcuGatewayProxy->mcuServerModel["server_key"], $mediaMixerKey, $this->confId, $did),"Mobile createConferenceRecord failed ");
			throw new Exception ("Mobile createConferenceRecord failed ");
		}
	}
	
	public function getCreateFlashMosaic() {
		$obj = new CreateMosaicParams(array(
				"confId"   => $this->confId,
				"compType" => 0,
				"size"     => $this->configProxy->get("size")));
		return $obj->getParams();
	}
	
	
	public function getCreateStbFlashMosaic() {
		$obj = new CreateMosaicParams(array(
				"confId"   => $this->confId,
				"compType" => 0,
				"size"     => $this->configProxy->get("hd_size")));
		return $obj->getParams();
	}
	
	public function getCreateStbMobileMosaic() {
		$obj = new CreateMosaicParams(array(
				"confId"   => $this->confId,
				"compType" => 0,
				"size"     => $this->configProxy->get("hd_size")));
		return $obj->getParams();
	}

	public function getCreateMobileMosaic() {
		$obj = new CreateMosaicParams(array(
				"confId"   => $this->confId,
				"compType" => 0,
				"size"     => $this->configProxy->get("size")));
		return $obj->getParams();
	}

	public function getCreatePgiMosaic() {
		$obj = new CreateMosaicParams(array(
				"confId"   => $this->confId,
				"compType" => 0,
				"size"     => $this->configProxy->get("size")));
		return $obj->getParams();
	}

	public function createMosaic($params) {
		$this->mcuControllerProxy->createMosaic($params);
	}
	
	public function setConferenceMosaicSlot() {
		$this->mcuControllerProxy->setMosaicSlot($this->getSetMosaicSlotParams(0));
	}
	
	public function setFlashMosaicSlot() {
		$this->mcuControllerProxy->setMosaicSlot($this->getSetMosaicSlotParams(1));
	}
	
	public function setMobileMosaicSlot() {
		$this->mcuControllerProxy->setMosaicSlot($this->getSetMosaicSlotParams(2));
	}

	private function getSetMosaicSlotParams($mosaicId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$this->confId,
				"mosaicId"=>$mosaicId,
				"num"=>0,
				"partId"=>0));
		return $obj->getParams();
	}

	public function getCreateSidebarParams() {
		$obj = new CreateSidebarParams(array("confId"   => $this->confId));
		return $obj->getParams();
	}
	
	public function createSidebar() {
		$this->createFlashSidebar();
		$this->createMobileSidebar();
		$this->createPgiSidebar();
	}
	
	public function createforStbSidebar() {
		$this->createFlashSidebar();
		$this->createMobileSidebar();
	}

	private function createFlashSidebar() {
		$params = $this->getCreateSidebarParams();
		$this->mcuControllerProxy->createSidebar($params);
	}
	
	private function createMobileSidebar() {
		$params = $this->getCreateSidebarParams();
		$this->mcuControllerProxy->createSidebar($params);
	}

	private function createPgiSidebar() {
		$params = $this->getCreateSidebarParams();
		$this->mcuControllerProxy->createSidebar($params);
	}

	public function fixMediaMixerByMcuKey($mcuKey) {
		return $this->mediaController->getMobileMediaMixerByAvailableParticipantCount($mcuKey);
	}
	
	public function fixStbMediaMixerByMcuKey($mcuKey) {
		return $this->mediaController->getStbMobileMediaMixerByAvailableParticipantCount($mcuKey);
	}
}
