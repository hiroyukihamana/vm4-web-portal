<?php 
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class CreateTemplateGuestDidParams extends WSDLParams {
	protected $templDid;
	protected $didPrefix;
	protected $didLength;
	protected $callbackUrl;
	protected $createOnConf;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}
}
