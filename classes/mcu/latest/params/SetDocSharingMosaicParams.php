<?php
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class SetDocSharingMosaicParams extends WSDLParams {
	protected $confId;
	protected $mosaicId;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}
}
