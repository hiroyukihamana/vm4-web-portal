<?php
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class AcceptParticipantParams extends WSDLParams {
	protected $confId;
	protected $partId;
	protected $mosaicId;
	protected $sidebarId;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}
}
