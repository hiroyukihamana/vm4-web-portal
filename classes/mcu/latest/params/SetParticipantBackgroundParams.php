<?php 
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class SetParticipantBackgroundParams extends WSDLParams {
	protected $confId;
	protected $partId;
	protected $filename;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}	
}
