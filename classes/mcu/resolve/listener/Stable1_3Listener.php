<?php
require_once(dirname(__FILE__)."/../../../mcu/resolve/listener/CallType.php");

//require_once("McuTypes.php");
require_once('classes/polycom/conference/CreateConferenceClass.php');
require_once('classes/polycom/conference/DestroyedConferenceClass.php');
require_once("classes/polycom/create/CreatedController.php");
require_once("classes/polycom/destroy/DestroyedController.php");
require_once("classes/polycom/Polycom.class.php");
require_once("classes/mgm/dbi/mcu_server.dbi.php");

require_once("classes/mcu/config/McuConfigProxy.php");
require_once('classes/mcu/latest/model/ConferenceCreatedRequestProxy.php');
require_once("classes/mcu/latest/model/ConferenceDestroyedRequestProxy.php");
require_once("classes/mcu/latest/model/ConferenceEndProxy.php");
require_once("classes/mcu/latest/model/ConferenceProxy.php");
require_once("classes/mcu/latest/model/ParticipantCreatedProxy.php");
require_once("classes/mcu/latest/model/ParticipantDestroyedProxy.php");
require_once("classes/mcu/latest/model/ParticipantStateChangedProxy.php");

class Stable1_3Listener implements CallType {
	function __construct() {
		$this->logger = EZLogger2::getInstance();
		$this->config = EZConfig::getInstance(N2MY_CONFIG_FILE);
		
		define("N2MY_MDB_DSN", $this->config->get("GLOBAL", "auth_dsn"));
		
		$this->obj_McuServer = new McuServerTable(N2MY_MDB_DSN);
		$mcu_server = $this->obj_McuServer->getRow(sprintf("ip = '%s'", $_SERVER["REMOTE_ADDR"]));
		$wsdl = sprintf("%s:%d", $mcu_server["server_address"], $mcu_server["controller_port"]);
		define("WSDL_DOMAIN", $wsdl);
	}
	
	function conferenceCreated($request) {
		// version 2?
		$model = new ConferenceCreatedRequestProxy($request);
		ConferenceLogger::info(__FUNCTION__."_start", $model->confId, null, null, null, null, serialize($model));
		
		$createClass = new CreateConferenceClass($request);
		$createClass->execute();
		
		ConferenceLogger::info(__FUNCTION__."_end", $model->confId, null, null, null, null, serialize($model));
	}
	
	function conferenceDestroyed($request) {
		try {
			$model = new ConferenceDestroyedRequestProxy($request);
			ConferenceLogger::info(__FUNCTION__."_start", $model->confId, null, null, null, null, serialize($model));
		
			$destroyedClass = new DestroyedConferenceClass($request);
			$destroyedClass->execute();
			
			ConferenceLogger::info(__FUNCTION__."_end", $model->confId, null, null, null, null, serialize($model));
		}
		catch (Exception $e) {
			$this->logger->error($e);
		}
	}
	
	
	function participantCreated($request) {
		try {
			if (!$request->confId || !$request->part) {
				throw new Exception("request info irregal.");
			}
			$model = new ParticipantCreatedProxy($request);
			ConferenceLogger::info(__FUNCTION__."_start", $model->confId, $model->partId, $model->partName, $model->partType, $model->partState, serialize($model));
		
			//$part->name	// register あり: register.sipserver(visi...)、なし: vcube,  guest
			$controller = CreatedController::create($request);
			$this->logger->info("controller: ".get_class($controller));
			$controller->execute();

			ConferenceLogger::info(__FUNCTION__."_end", $model->confId, $model->partId, $model->partName, $model->partType, $model->partState, serialize($model));
		}
		catch (Exception $e) {
			$this->exitWithError('create ives participant of meeting failed : '.$e->getMessage());
		}
	}
	
	function participantDestroyed($request) {
		try {
			$model = new ParticipantDestroyedProxy($request);
			ConferenceLogger::info(__FUNCTION__."_start", $model->confId, $model->partId, null, null, null, serialize($model));
			
			$controller = DestroyedController::create($request);
			$this->logger->info("controller: ".get_class($controller));
			$controller->execute();

			ConferenceLogger::info(__FUNCTION__."_end", $model->confId, $model->partId, null, null, null, serialize($model));
		}
		catch (Exception $e) {
			$this->exitWithError('destroyed participant of meeting failed : '.$e->getMessage());
		}
	}
	
	function participantStateChanged($request) {
		$this->logger->info($request);
		$model = new ParticipantStateChangedProxy($request);
		ConferenceLogger::info(__FUNCTION__."_start", $model->confId, $model->partId, null, null, $model->partState, serialize($model));
		
		if ($request->state == "TIMEOUT") {
			$polycom = new PolycomClass(PCore::getDsn());
			if ($request->confId && $request->partId) {
				$return = $polycom->getClientInfo($request->confId, $request->partId);
				$return["request"] = $request;
			}
			$polycom->alert($return);
			$this->logger->warn($return);
			
		}
		ConferenceLogger::info(__FUNCTION__."_end", $model->confId, $model->partId, null, null, $model->partState, serialize($model));
	}
	
	function conferenceEnded($request) {
		// TODO 使用用途が不明の為、保留
		$this->logger->info($request);
	}
	
	function conferenceInite($request) {}


	function onDocSharingStatus($request) {}
	
	function onRecordingStatus($request) {}
	
	function exitWithError($msg) {
		$this->logger->error($msg);
		print 0;
		exit;
	}
}
