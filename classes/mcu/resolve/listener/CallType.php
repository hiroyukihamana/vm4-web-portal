<?php 

interface CallType {
	function conferenceCreated($request);
	function conferenceDestroyed($request);
	
	function participantCreated($request);
	function participantDestroyed($request);
	function participantStateChanged($request);
	function conferenceEnded($request);
	function conferenceInite($request);
	
	function onDocSharingStatus($request);
	function onRecordingStatus($request);
// 	function exitWithError($msg);
}