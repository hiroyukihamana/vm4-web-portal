<?php
require_once(dirname(__FILE__)."/../../../mcu/lib/MCUSoapException.php"); 
require_once(dirname(__FILE__)."/../../../mcu/resolve/listener/CallType.php");

//require_once('McuTypes.php');

require_once("classes/mcu/config/McuConfigProxy.php");
require_once('classes/mcu/stable_api_2_0/controller/conference/create/CreateConferenceClass.php');
require_once('classes/mcu/stable_api_2_0/controller/conference/destroy/DestroyedConferenceController.php');
require_once('classes/mcu/stable_api_2_0/controller/conference/end/EndConferenceController.php');

require_once("classes/mcu/stable_api_2_0/controller/participant/change/ChangeController.php");
require_once("classes/mcu/stable_api_2_0/controller/participant/create/CreatedController.php");
require_once("classes/mcu/stable_api_2_0/controller/participant/destroy/DestroyedController.php");

require_once('classes/mcu/stable_api_2_0/model/ConferenceCreatedRequestProxy.php');
require_once("classes/mcu/stable_api_2_0/model/ConferenceDestroyedRequestProxy.php");
require_once("classes/mcu/stable_api_2_0/model/ConferenceProxy.php");

require_once("classes/mcu/stable_api_2_0/model/ConferenceEndProxy.php");
require_once("classes/mcu/stable_api_2_0/model/ConferenceProxy.php");
require_once("classes/mcu/stable_api_2_0/model/MCUControllerProxy.php");
require_once("classes/mcu/stable_api_2_0/model/ParticipantCreatedProxy.php");
require_once("classes/mcu/stable_api_2_0/model/ParticipantDestroyedProxy.php");
require_once("classes/mcu/stable_api_2_0/model/ParticipantStateChangedProxy.php");

class Stable_2_0_Listener implements CallType {
	function __construct() {
		$this->logger = EZLogger2::getInstance();
		$this->proxy = ConferenceProxy::getInstance();

		$this->configProxy 	= new McuConfigProxy();
		
		$mcuServer = new McuServerTable($this->configProxy->getAuthDsn());
		$mcuServerRecord = $mcuServer->getRow(sprintf("ip = '%s'", $_SERVER["REMOTE_ADDR"]));
		$wsdl = sprintf("%s:%d", $mcuServerRecord["server_address"], $mcuServerRecord["controller_port"]);
		define("WSDL_DOMAIN", $wsdl);
		
		$this->mcuControllerProxy	= new MCUControllerProxy();
	}
	
	function conferenceCreated($request) {

		$this->logger->info($request);
		$model = new ConferenceCreatedRequestProxy($request);
		
		ConferenceLogger::info(__FUNCTION__."_start", $model->confId, null, null, null, null, serialize($model));
		try {
// 			$createClass = new CreateConferenceClass($model);
// 			$createClass->execute();
// 			ConferenceLogger::info(__FUNCTION__."_end", $model->confId, null, null, null, null, serialize($model));
		}
		catch (Exception $e) {
			$this->exitWithError($e->getMessage());
		}
		catch (MCUSoapException $e) {
			$this->exitWithSoapError($e);
		}
	}
	
	function conferenceDestroyed($request) {

		$this->logger->info($request);
		$model = new ConferenceDestroyedRequestProxy($request);
		
		ConferenceLogger::info(__FUNCTION__."_start", $model->confId, null, null, null, null, serialize($model));
		try {
// 			$destroyedClass = new DestroyedConferenceController($model);
// 			$destroyedClass->execute();
// 			ConferenceLogger::info(__FUNCTION__."_end", $model->confId, null, null, null, null, serialize($model));
		}
		catch (Exception $e) {
			$this->exitWithError($e->getMessage());
		}
		catch (MCUSoapException $e) {
			$this->exitWithSoapError($e);
		}
	}
	
	
	function participantCreated($request) {

		$this->logger->info($request);
		$model = new ParticipantCreatedProxy($request);
		ConferenceLogger::info(__FUNCTION__."_start", $model->confId, $model->partId, $model->partName, $model->partType, $model->partState, serialize($model));
		try {
			if (!$request->confId || !$request->part) {
				throw new Exception("request info irregal.");
			}
			$controller = CreatedController::create($model);
			$this->logger->info("controller: ".get_class($controller));
			$controller->execute();
			ConferenceLogger::info(__FUNCTION__."_end", $model->confId, $model->partId, $model->partName, $model->partType, $model->partState, serialize($model));
		}
		catch(RejectedException $e) {
			$this->rejectParticipant($model);
			$this->exitWithSoapError($e);
		}
		catch (MCUSoapException $e) {
			$this->removeParticipant($model);
			$this->exitWithSoapError($e);
		}
		catch (Exception $e) {
			$this->removeParticipant($model);
			$this->exitWithError($e->getMessage());
		}
	}
	
	function participantDestroyed($request) {
		$this->logger->info($request);
		$model = new ParticipantDestroyedProxy($request);
		
		ConferenceLogger::info(__FUNCTION__."_start", $model->confId, $model->partId, null, null, null, serialize($model));
		try {
			$controller = DestroyedController::create($model);
			if ($controller) {
				$this->logger->info("controller: ".get_class($controller));
				$controller->execute();
				ConferenceLogger::info(__FUNCTION__."_end", $model->confId, $model->partId, null, null, null, serialize($model));
			}
		}
		catch (Exception $e) {
			$this->exitWithError($e->getMessage());
		}
		catch (MCUSoapException $e) {
			$this->exitWithSoapError($e);
		}
	}
	
	function participantStateChanged($request) {

		$this->logger->info($request);
		$model = new ParticipantStateChangedProxy($request);
		
		ConferenceLogger::info(__FUNCTION__."_start", $model->confId, $model->partId, null, null, $model->partState, serialize($model));
		try {
    		if ($controller = ChangeController::create($model)) {
	    		$this->logger->info("controller: ".get_class($controller));
	    		$controller->execute();
				ConferenceLogger::info(__FUNCTION__."_end", $model->confId, $model->partId, null, null, $model->partState, serialize($model));
    		}
		}
		catch (SoapFault $e) {
			$this->removeParticipant($model);
			$this->exitWithError($e);
		}
		catch (Exception $e) {
			$this->removeParticipant($model);
			$this->exitWithError($e->getMessage());
		}
		catch (MCUSoapException $e) {
			$this->removeParticipant($model);
			$this->exitWithSoapError($e);
		}
	}
	
	function conferenceEnded($request) {
		$this->logger->info($request);
		$model = new ConferenceDestroyedRequestProxy($request);
		
		ConferenceLogger::info(__FUNCTION__."_start", $model->confId, null, null, null, null, serialize($model));
		try {
			$destroyedClass = new DestroyedConferenceController($model);
			$destroyedClass->execute();
			ConferenceLogger::info(__FUNCTION__."_end", $model->confId, null, null, null, null, serialize($model));
		}
		catch (Exception $e) {
			$this->exitWithError($e->getMessage());
		}
		catch (MCUSoapException $e) {
			$this->exitWithSoapError($e);
		}
	}
	
	function conferenceInite($request) {
		$this->logger->info($request);
		$model = new ConferenceCreatedRequestProxy($request);
		
		ConferenceLogger::info(__FUNCTION__."_start", $model->confId, null, null, null, null, serialize($model));
		try {
			$createClass = new CreateConferenceClass($model);
			$createClass->execute();
			ConferenceLogger::info(__FUNCTION__."_end", $model->confId, null, null, null, null, serialize($model));
		}
		catch (Exception $e) {
			$this->exitWithError($e->getMessage());
		}
		catch (MCUSoapException $e) {
			$this->exitWithSoapError($e);
		}
	}
	
	function onDocSharingStatus($request) {
		try {
			$this->logger->info($request);
			ConferenceLogger::info(__FUNCTION__."_start", $model->confId, null, null, null, null, serialize($model));
		}
		catch (Exception $e) {
			$this->exitWithError($e->getMessage());
		}
		catch (MCUSoapException $e) {
			$this->exitWithSoapError($e);
		}
	}
	
	function onRecordingStatus($request) {
		try {
			$this->logger->info($request);
			ConferenceLogger::info(__FUNCTION__."_start", $model->confId, null, null, null, null, serialize($model));
		}
		catch (Exception $e) {
			$this->exitWithError($e->getMessage());
		}
		catch (MCUSoapException $e) {
			$this->exitWithSoapError($e);
		}
	}
	
	private function removeParticipant($model) {
		$this->mcuControllerProxy->removeParticipant($model->confId, $model->partId);
	}
	
	private function rejectParticipant($model) {
		$this->mcuControllerProxy->rejectParticipant($model->confId, $model->partId);
	}
	
	private function exitWithError($msg) {
		ConferenceLogger::info(__FUNCTION__, null, null, null, null, null, serialize($msg));
		$this->logger->warn($msg);
		print 0;
		exit;
	}
	
	private function exitWithSoapError($msg) {
		$this->logger->warn($msg);
		ConferenceLogger::info(__FUNCTION__, null, null, null, null, null, serialize($msg));
		print 0;
		exit;
	}
}
	