<?php

require_once("classes/mgm/dbi/mcu_server.dbi.php");
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");

class Resolver {
  public static function resolveByServerIP() {
    $logger = EZLogger2::getInstance();
    $proxy = new McuConfigProxy();
    $mcuProxy = new McuGatewayProxy();
    //$logger->info($mcuServer);
    try {
      $mcuProxy->setMcuServerByServerIpThrowable($_SERVER["REMOTE_ADDR"]);
      
      $classpath = $mcuProxy->mcuServerModel["listenerClassPath"];
      require_once($classpath);
      
      $classname = $mcuProxy->mcuServerModel["listenerClassName"];
      return new $classname();
    }
    catch (Exception $e) {
      $logger->warn($e->getMessage());
      return null;
    }
  }
  
  public static function getConferenceOptionClassByRoomKey( $roomKey, $is_extend_teleconference = false ) {
    $logger = EZLogger2::getInstance();
    $proxy = new McuConfigProxy();
    $dsn = $proxy->getDsn();
    
    require_once("classes/dbi/ives_setting.dbi.php");
    $ivesSettingTable= new IvesSettingTable($dsn);
    $where = "room_key='".$roomKey."' and is_deleted = 0";
    $ivesInfo	= $ivesSettingTable->getRow($where);
    if ( !$ivesInfo ){
    	if( !$is_extend_teleconference ) return null;
    	
    	// テレビ会議連携が付いていなくても、電話連携が付いていれば解決させる
    	require_once 'classes/dbi/ordered_service_option.dbi.php';
    	$dbi_service_option = new OrderedServiceOptionTable( $dsn );
    	if ( !$dbi_service_option->enableOnRoom( 23, $roomKey ) ) return null;
    }

    try {
      $mcuProxy = new McuGatewayProxy();
      
      if( !$ivesInfo && $is_extend_teleconference ) $mcuProxy->setLatestModelThrowable();
      else $mcuProxy->setMcuServerModelByRoomKeyThrowable($roomKey);
      
      $classpath = dirname(__FILE__)."/../../../".$mcuProxy->mcuServerModel["createClassPath"];
      require_once($classpath);
      /**
       * php 5.3以上で上記が可能
       */
      return VideoConferenceOptionCreateClass::getObjectByRoomKey($roomKey, $is_extend_teleconference);
    }
    catch (Exception $e) {
      $logger->warn($e->getMessage());
      return null;
    }
  }
  
  public static function getMobileController($roomKey) {
    $logger = EZLogger2::getInstance();
    $proxy = new McuConfigProxy();
    $dsn = $proxy->getDsn();
    
    require_once("classes/dbi/ives_setting.dbi.php");
    $ivesSettingTable= new IvesSettingTable($dsn);
    $where = "room_key='".$roomKey."' and is_deleted = 0";
    $ivesInfo	= $ivesSettingTable->getRow($where);

    require_once("classes/dbi/room.dbi.php");
    $room			= new RoomTable($dsn);
    $roomRecord = $room->findByKey($roomKey);
    
    $class = null;
    try {
      $mcuProxy = new McuGatewayProxy();
      if ($roomRecord["use_sales_option"]) {
        $mcuProxy->setLatestModelThrowable();
        $classpath = dirname(__FILE__)."/../../../".$mcuProxy->mcuServerModel["salesMobileControllerPath"];
        require_once($classpath);
        $classname = $mcuProxy->mcuServerModel["salesMobileControllerClassName"];
        $class = new $classname();
      } else if ($roomRecord["use_stb_option"]) {
        $mcuProxy->setLatestModelThrowable("stb");
        $classpath = dirname(__FILE__)."/../../../".$mcuProxy->mcuServerModel["stbMobileControllerPath"];
        require_once($classpath);
        $classname = $mcuProxy->mcuServerModel["stbMobileControllerClassName"];
        $class = new $classname();
      } else if ($ivesInfo) {
        $mcuProxy->setMcuServerModelByRoomKeyThrowable($roomKey);
        $classpath = dirname(__FILE__)."/../../../".$mcuProxy->mcuServerModel["mobileControllerPath"];
        require_once($classpath);
        $classname = $mcuProxy->mcuServerModel["mobileControllerClassName"];
        $class = new $classname();
      } else if ($roomRecord["mobile_mix"]) { // include teleconf
        $mcuProxy->setLatestModelThrowable();
        $classpath = dirname(__FILE__)."/../../../".$mcuProxy->mcuServerModel["mobileControllerPath"];
        require_once($classpath);
        $classname = $mcuProxy->mcuServerModel["mobileControllerClassName"];
        $class = new $classname();
      }
    } catch (Exception $e) {
      $logger->warn($e->getMessage());
    }
    return $class;
  }
  
  public static function getController($roomKey) {
    $controller = null;
    if ($controller = self::getConferenceOptionClassByRoomKey($roomKey))
      return $controller;
    if ($controller = self::getMobileController($roomKey))
      return $controller;
    return $controller;
  }
}
