<?php 
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class CreateMosaicParams extends WSDLParams {
	protected $confId;
	protected $compType;
	protected $size;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}
}
