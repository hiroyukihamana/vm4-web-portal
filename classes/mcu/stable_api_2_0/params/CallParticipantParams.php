<?php 
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class CallParticipantParams extends WSDLParams {
	protected $confId;
	protected $dest;
	protected $mosaicId;
	protected $sidebarId;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}	
}
