<?php 
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class GetConferenceReturnParams extends WSDLParams {
	protected $timestamp;
	protected $name;
	protected $did;
	protected $numActParticipants;
	protected $vad;
	protected $autoAccept;
	protected $isAdHoc;
	protected $mosaics = array();
	protected $sidebars	= array();
	protected $properties	= array();
	protected $duration;
	protected $compType;
	protected $numSlots;
	protected $size;
	protected $id;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}	
}
