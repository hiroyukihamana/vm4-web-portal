<?php 
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class CallParticipantWithAuthParams extends WSDLParams {
	protected $confId;
	protected $dest;
	protected $mosaicId;
	protected $sidebarId;
	protected $profileId;
	protected $callerId;
	protected $username;
	protected $password;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}	
}
