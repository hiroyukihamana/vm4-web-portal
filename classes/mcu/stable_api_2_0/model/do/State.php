<?php 

class State {
	static $CONNECTED		= array("key"=>1,"value"=>"CONNECTED");
	static $CREATED			= array("key"=>2,"value"=>"CREATED");
	static $DISCONNECTED	= array("key"=>3,"value"=>"DISCONNECTED");
	static $DESTROYED		= array("key"=>4,"value"=>"DESTROYED");
	static $WAITING_ACCEPT	= array("key"=>5,"value"=>"WAITING_ACCEPT");
	static $CONNECTING		= array("key"=>6,"value"=>"CONNECTING");
	static $TIMEOUT			= array("key"=>7,"value"=>"TIMEOUT");
	static $ERROR			= array("key"=>8,"value"=>"ERROR");
	static $BUSY			= array("key"=>9,"value"=>"BUSY");
	static $NOTFOUND		= array("key"=>10,"value"=>"NOTFOUND");
	static $DECLINED		= array("key"=>11,"value"=>"DECLINED");
	
	static $CALLING			= array("key"=>90,"value"=>"CALLING");
	static $INCOMPLETE		= array("key"=>91,"value"=>"INCOMPLETE");
	static function getState($status) {
		$state = self::$$status;
		return $state["key"];
	}
	
	/**
	 * 5.3 以降で__callStaticに置き換え
	 * @return unknown
	 */
	static function CONNECTED() {return self::$CONNECTED["value"];}
	static function CREATED() {return self::$CREATED["value"];}
	static function DISCONNECTED() {return self::$DISCONNECTED["value"];}
	static function DESTROYED() {return self::$DESTROYED["value"];}
	static function WAITING_ACCEPT() {return self::$WAITING_ACCEPT["value"];}
	static function CONNECTING() {return self::$CONNECTING["value"];}
	static function TIMEOUT() {return self::$TIMEOUT["value"];}
	static function ERROR() {return self::$ERROR["value"];}
	static function BUSY() {return self::$BUSY["value"];}
	static function NOTFOUND() {return self::$NOTFOUND["value"];}
	static function DECLINED() {return self::$DECLINED["value"];}
	
	static function CALLING() {return self::$CALLING["value"];}
	static function INCOMPLETE() {return self::$INCOMPLETE["value"];}
}
