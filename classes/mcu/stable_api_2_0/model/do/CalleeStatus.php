<?php
require_once(dirname(__FILE__)."/../../model/do/State.php");
 
class CalleeStatus {
	public static $CALLING	= "calling";
	public static $FAILED	= "failed";
	public static $INVALID	= "invalid";
	public static $SUCCESS	= "success";
	public static $ERROR	= "error";
	
	static function getCalleeStatus($obj) {
		if (!$obj)
			return self::$ERROR;
		
		switch ($obj["participant_state"]) {
			case State::getState("CONNECTED");
				return self::$SUCCESS;
				
			case State::getState("CALLING");
			case State::getState("CREATED");
			case State::getState("CONNECTING");
				return self::$CALLING;
					
			case State::getState("DISCONNECTED");
				return self::$INVALID;
				
			case State::getState("TIMEOUT");
			case State::getState("ERROR");
			case State::getState("BUSY");
			case State::getState("NOTFOUND");
			case State::getState("DECLINED");
				return self::$FAILED;

			case State::getState("DESTROYED");
			case State::getState("INCOMPLETE");
			case State::getState("WAITING_ACCEPT");
			default:
				return self::$ERROR;
		}
	}
}
