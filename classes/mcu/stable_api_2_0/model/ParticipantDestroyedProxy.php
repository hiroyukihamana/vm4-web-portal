<?php 

require_once("classes/mcu/model/Proxy.php");
require_once(dirname(__FILE__)."/../model/vo/ParticipantDestroyedRequestObject.php");

class ParticipantDestroyedProxy extends Proxy {
	private $dataObject;
	function __construct($request) {
		$this->dataObject = new ParticipantDestroyedRequestObject($request);
	}

	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return $this->dataObject->$key;
		}
	}
}
