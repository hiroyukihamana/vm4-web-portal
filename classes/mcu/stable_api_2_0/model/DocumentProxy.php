<?php
require_once("classes/core/dbi/Document.dbi.php");
require_once("classes/mcu/model/Proxy.php"); 

class DocumentProxy extends Proxy {
	private $document;
	function __construct() {
		parent::__construct();
		
		$this->document = new DBI_Document($this->dsn);
	}
	
	private function getDocumentByDocumentId($documentId) {
		$where = sprintf("document_id='%s'", $documentId);
		$record = $this->document->getRow($where);
		return $record;
	}
	
	public function getImagePath($documentId, $page=1) {
		$documentRecord = $this->getDocumentByDocumentId($documentId);
		if (!$documentRecord) return null;
		
		$path = sprintf("%s%s%s/%s_%d.jpg", N2MY_DOC_DIR, $documentRecord["document_path"], $documentRecord["document_id"], $documentRecord["document_id"], $page);
		return $path;
	}
	
	public function getNoImagePath() {
		$path = sprintf("%s%s", N2MY_DOCUMENT_ROOT, $this->configProxy->get("document_sharing_no_image_path"));
		return $path;
	}
}
