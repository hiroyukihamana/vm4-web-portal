<?php
require_once("classes/mcu/model/Proxy.php");

require_once("classes/dbi/conference_record.dbi.php");
require_once("classes/dbi/video_conference.dbi.php");
require_once("classes/dbi/video_conference_participant.dbi.php");

require_once(dirname(__FILE__)."/../model/do/State.php");
require_once(dirname(__FILE__)."/../model/do/CalleeStatus.php");

/**
 * conference proxy.
 * @author akihitowada
 *
 */

class ConferenceProxy extends Proxy {
	private $callback;
	private $confence;
	private $confenceParticipant;
	
	private $activeConferenceRecord = null;
	private $conferenceRecord = null;
	
	function __construct() {
		parent::__construct();
		
		$this->callback			= new ConferenceRecordTable($this->dsn);
		$this->confence	= new VideoConferenceTable($this->dsn);
		$this->confenceParticipant	= new VideoConferenceParticipantTable($this->dsn);
	}
	
	static private $instance = null;
	public static function getInstance() {
		if (!self::$instance)
			self::$instance = new self;
		return self::$instance;
	}
	
	/**
	 * 
	 * @param unknown $meetingRecord
	 * @param unknown $mcuServerKey
	 * @param unknown $confId
	 * @param unknown $did
	 * @return unknown
	 */
	public function createConferenceRecord($meetingRecord, $mcuServerKey, $mediaMixerKey, $confId, $did) {
		if (!$meetingRecord) {
			$this->logger->warn(debug_backtrace());
			throw new Exception("meeting record is null");
		}
			
		$data = array(
				"mcu_server_key"=> $mcuServerKey,
				"media_mixer_key"=>$mediaMixerKey,
				"user_key"		=> $meetingRecord["user_key"],
				"meeting_key"	=> $meetingRecord["meeting_key"],
				"room_key"		=> $meetingRecord["room_key"],
				"conference_id"	=> $confId,
				"did"			=> $did,
				"is_active"		=> 1,
				"createtime"	=> date("Y-m-d H:i:s")
				);
		$this->confence->add($data);
		$record = $this->confence->getActiveVideoConferenceByDid($did);
		return $record;
	}
	
	public function addConferenceForMobile($meetingRecord, $mcuServerKey, $mediaMixerKey, $confId, $did) {
		$data = array(
				"mcu_server_key"=> $mcuServerKey,
				"media_mixer_key"=>$mediaMixerKey,
				"user_key"		=> $meetingRecord["user_key"],
				"meeting_key"	=> $meetingRecord["meeting_key"],
				"room_key"		=> $meetingRecord["room_key"],
				"conference_id"	=> $confId,
				"did"			=> $did,
				"is_active"		=> 1,
				"createtime"	=> date("Y-m-d H:i:s")
				);
		$this->confence->add($data);
	}

	/**
	 * did による有効なVideoConferenceを取得
	 * @param string did
	 * @return object
	 */
	public function getActiveConferenceByDid($did) {
		if (!$this->activeConferenceRecord) {
			$where = sprintf("did='".$did."' AND is_active=1");
			$sort = array("video_conference_key"=>"desc");
			$this->activeConferenceRecord = $this->confence->getRowsAssoc($where, $sort);
		}
		return $this->activeConferenceRecord;
	}
	
	/**
	 * didより有効なvideoconferenceを全て無効化
	 * @param string did
	 * @return void
	 */
	public function clearConferenceRecordByDid($did) {
		$list = $this->getActiveConferenceByDid($did);
		if (count($list) >= 2) {
			$this->logger->warn("irregal conference record. did:".$did);
		}
		$data = array("is_active"=>0,"updatetime"=>date("Y-m-d H:i:s"));
		$where = "did='".$did."' AND is_active = 1";
		$this->confence->update($data, $where);
	}
	
	/**
	 * CallingStatusのレコードを未完了に更新
	 * @param unknown $did
	 */
	public function clearCallingParticipantRecordByDid($did) {
		$where = sprintf("did='%s' AND participant_state=%d", $did, State::getState("CALLING"));
		$list = array();
		$list = $this->confenceParticipant->getRowsAssoc($where);
		if (count($list) > 0) {
			$data = array(
				"participant_state"=>State::getState("INCOMPLETE"),
				"is_active"=>0,
				"updatetime"=>date("Y-m-d H:i:s"));
			foreach ($list as $key => $obj) {
				$where = "video_participant_key=".$obj["video_participant_key"];
				$this->confenceParticipant->update($data, $where);
			}
		}
	}
	
	/**
	 * 最新のレコードのみを残して他を全て無効状態に更新する
	 * @param string did
	 */
	public function clearOtherThanTheLatest($did) {
		$list = $this->getActiveConferenceByDid($did);
		if (count($list) <= 1) {
			return;
		}
		$key = $list[0]["video_conference_key"];
		$data = array("is_active"=>0,"updatetime"=>date("Y-m-d H:i:s"));
		$where = "did='".did."' AND is_active = 1 AND video_conference_key <>".$key;
		$this->confence->update($data, $where);
	}
	
	/**
	 * meeting_key による有効なVideoConferenceを取得
	 * @param int $meetingKey
	 * @return object
	 */
	public function getActiveConferenceByMeetingKey($meetingKey) {
		if (!$this->activeConferenceRecord) {
			$where = sprintf("meeting_key=".$meetingKey." and is_active=1");
			$obj = $this->confence->getRow($where);
			$this->activeConferenceRecord = $this->confence->getRow($where);
		}
		return $this->activeConferenceRecord;
	}
	
	/**
	 * video conference participantの追加
	 * @param unknown $data
	 */
	public function createConferenceParticipantRecord($data) {
		return $this->confenceParticipant->add($data);
	}
	
	/**
	 * 
	 * @param unknown $did
	 * @param unknown $meetingKey
	 * @param unknown $name
	 */
	public function addCallingParticipantRecord($did, $meetingKey, $name, $ip) {
		$data = array(
				"did"					=> $did,
				"meeting_key"			=> $meetingKey,
				"participant_name"		=> $name,
				"participant_ip"		=> $ip,
				"participant_type"		=> "SIP",
				"participant_state"		=> State::getState("CALLING"),
				"is_active"				=> 0,
				"createtime"=>date('Y-m-d H:i:s'));
		$this->createConferenceParticipantRecord($data);
		$where = sprintf("meeting_key=".$meetingKey.
				" AND did = '".$did."' AND is_active=0 AND participant_type='SIP' AND participant_state=".State::getState("CALLING"));
		$sort = array("video_participant_key"=>"desc");
		$obj = $this->confenceParticipant->getRow($where, "", $sort);
		return $obj["video_participant_key"];
	}
	
	public function getCallingParticipantRecord($confId, $did, $meetingKey) {
		$where = sprintf("participant_key = 0 AND part_id <> 0 AND is_active=0 AND conference_id='".$confId."'".
				" AND did='".$did.
				"' AND meeting_key = ".$meetingKey.
				" AND participant_state =".State::getState("CONNECTING"));
		$sort = array("video_participant_key"=>"desc");
		$obj = $this->confenceParticipant->getRow($where, "", $sort);
		return $obj;
	}
	
	public function getCalleeParticipantStatusVideoParticipantKey($participantKey) {
		$where = sprintf("video_participant_key = %d", $participantKey);
		$sort = array("video_participant_key"=>"desc");
		$obj = $this->confenceParticipant->getRow($where, "", $sort);
		return CalleeStatus::getCalleeStatus($obj);
	}
	
	public function getCalleeName($name) {
		$name = ($name != null)
		? $name
		: $this->configProxy->get("callout_name");
		return $name;
	}
	
	private function removeHyphenFromPartName($partName) {
		$arr = explode("-", $partName);
		preg_match("/^(.*)-".REG_IP."$/", $partName, $matches);
		return (count($matches) > 0) ? sprintf("%s%s.%s.%s.%s", $matches[1],$matches[2],$matches[3],$matches[4],$matches[5]) : $partName; 
	}
	
	private static $waitFlg = false;
	private function isExistConference($confId) {
		$conferenceRecord = $this->getConferenceByConfId($confId);
		if ($conferenceRecord)
			return $conferenceRecord;
		else if (!self::$waitFlg) {
			self::$waitFlg = true;
			sleep(1);
			$this->isExistConference($confId);
		}
		return null;
	}
	
	public function isCalleeParticipant($confId, $did, $partName) {
		$name = $this->removeHyphenFromPartName($partName);
		if (!$conferenceRecord = $this->isExistConference($confId))
			return;
		$where = "participant_key = 0 AND part_id = 0 AND is_active=0 AND conference_id=''".
				" AND did='".$did."'".
				" AND meeting_key = ".$conferenceRecord["meeting_key"].
				" AND ((participant_name='".$name."' OR participant_name = '".$this->configProxy->get("callout_name")."')".	// sip
				" OR CONCAT(participant_name, participant_ip) like '".$name."' )".	// h323
				" AND participant_state =".State::getState("CALLING");
		$sort = array("video_participant_key"=>"desc");
		$obj = $this->confenceParticipant->getRow($where, "", $sort);
		return $obj ? true : false;
	}

	/**
	 * 
	 * @param unknown $confId
	 * @param unknown $partName
	 * @return boolean
	 */
	public function existCalleeParticipant($confId, $partName) {
		$name = $this->removeHyphenFromPartName($partName);
		$conferenceRecord = $this->getConferenceByConfId($confId);
		if (!$conferenceRecord)
			return false;
		$where = "participant_key = 0 AND part_id = 0 AND is_active=0 AND conference_id=''".
				" AND meeting_key = ".$conferenceRecord["meeting_key"].
				" AND ((participant_name='".$name."' OR participant_name = '".$this->configProxy->get("callout_name")."')".	// sip
				" OR CONCAT(participant_name, participant_ip) like '".$name."' )".	// h323
				" AND participant_state =".State::getState("CALLING");
		$sort = array("video_participant_key"=>"desc");
		$obj = $this->confenceParticipant->getRow($where, "", $sort);
		return $obj;
	}
	
	public function voidCalleeParticipant($participant) {
		$data = array("updatetime"=>date("Y-m-d H:i:s"),
				"participant_state"=>State::getState("ERROR"));
		$where = sprintf("video_participant_key=%d", $participant["video_participant_key"]);
		$this->confenceParticipant->update($data, $where);
	}
	
	/**
	 * 
	 * @param unknown $model
	 */
	public function updateState($model /*ParticipantStateChangedProxy*/) {
		$data = array("updatetime"=>date("Y-m-d H:i:s"));
		if ($model->partState!=State::DESTROYED())
			$data["participant_state"] = State::getState($model->partState);
		$where = "conference_id='".$model->confId."' AND part_id = ".$model->partId;
		$this->confenceParticipant->update($data, $where);
	}
	
	public function getCallingParticipant($meetingKey, $did) {
		$result = $this->confenceParticipant->getCallingParticipantByMeetingKeyDid($meetingKey, $did);
		return $result;
	}
	
	public function updateCallingParticipant($data, $participantKey) {
		$where = sprintf("video_participant_key=%d", $participantKey);
		$this->confenceParticipant->update($data, $where);
	}
	
	public function getActiveConferenceParticipantByConfIdPartId($confId, $partId) {
		if (!$this->activeConferenceRecord) {
			$where = sprintf("part_id='".$partId."' AND conference_id='".$confId."' AND is_active=1");
			$this->activeConferenceRecord = $this->confenceParticipant->getRow($where);
		}
		return $this->activeConferenceRecord;
	}
	
	/**
	 * 非アクティブのユーザーでも取得
	 * @param unknown $confId
	 * @param unknown $partId
	 * @return unknown
	 */
	public function getConferenceParticipantByConfIdPartId($confId, $partId) {
		$where = sprintf("part_id='".$partId."' AND conference_id='".$confId."'");
		$result = $this->confenceParticipant->getRow($where);
		return $result;
	}
	
	public function getActiveConferenceParticipantByParticipantKey($partKey) {
		$where = sprintf("participant_key='".$partKey."' AND is_active=1");
		$result = $this->confenceParticipant->getRow($where);
		return $result;
	}

	public function getActiveConferenceParticipantBySessionId($sessionId) {
		$where = sprintf("participant_session_id='".$sessionId."' AND is_active=1");
		$result = $this->confenceParticipant->getRow($where);
		return $result;
	}
	
	public function getActiveConferenceParticipantListByDid($did) {
		$result = array();
		$where = sprintf("did='".$did."' and is_active=1");
		$result = $this->confenceParticipant->getRowsAssoc($where);
		return $result;
	}
	
	public function getActiveConferenceSipTypeParticipantListByDid($did) {
		$result = array();
		$where = sprintf("did='".$did."' AND participant_type='SIP' AND is_active=1");
		$result = $this->confenceParticipant->getRowsAssoc($where);
		return $result;
	}
	
	public function getActiveConferenceSipTypeParticipantListByMeetingKey($meetingKey) {
		$result = array();
		$where = sprintf("meeting_key='".$meetingKey."' AND participant_type='SIP' AND is_active=1");
		$result = $this->confenceParticipant->getRowsAssoc($where);
		return $result;
	}
	
	public function getActiveConferenceSipTypeParticipantListByConfId($confId) {
		$result = array();
		$where = sprintf("conference_id='".$confId."' AND participant_type='SIP' AND is_active=1");
		$result = $this->confenceParticipant->getRowsAssoc($where);
		return $result;
	}
	
	public function getActiveConferenceParticipantListByConfId($confId) {
		$result = array();
		$where = sprintf("conference_id='".$confId."' and is_active=1");
		$result = $this->confenceParticipant->getRowsAssoc($where);
		return $result;
	}
	
	public function clearConferenceParticipantRecordByConfId($confId) {
		$list = $this->getActiveConferenceParticipantListByConfId($confId);
		foreach ($list as $key => $value) {
			// log
		}
		$data = array(
					"is_active"=>0,
					"updatetime"=>date("Y-m-d H:i:s"));
		
		$where = sprintf("conference_id='".$confId."' and is_active=1");
		$this->confenceParticipant->update($data, $where);
	}
	
	public function clearConferenceParticipantRecordByDid($did) {
		$data = array(
					"is_active"=>0,
					"updatetime"=>date("Y-m-d H:i:s"));
		$where = sprintf("did='".$did."' and is_active=1");
		$this->confenceParticipant->update($data, $where);
	}
	
	public function getConferenceByConfId($confId) {
		$where = sprintf("conference_id='".$confId."'");
		$obj = $this->confence->getRow($where);
		
		return $obj;
	}
	
	public function getConferenceByMeetingKey($meetingKey) {
		$where = sprintf("meeting_key='".$meetingKey."'");
		$obj = $this->confence->getRowsAssoc($where);
		
		return $obj;
	}
	
	/**
	 * confId による有効なVideoConferenceを取得
	 * @param string $confId
	 * @return object
	 */
	public function getActiveConferenceByConfId($confId) {
		$where = sprintf("conference_id='".$confId."' AND is_active=1");
		$sort = array("video_conference_key"=>"desc");
		$obj = $this->confence->getRowsAssoc($where, $sort);
		
		return $obj;
	}

	/**
	 * confIdより有効なvideoconferenceを全て無効化
	 * @param string confId
	 * @return void
	 */
	public function clearConferenceRecordByConfId($confId) {
		$list = $this->getActiveConferenceByConfId($confId);
		if (count($list) >= 2) {
			$this->logger->warn("irregal conference record.".implode(", ", $list));
		}
		$data = array("is_active"=>0,"updatetime"=>date("Y-m-d H:i:s"));
		$where = "conference_id='".$confId."' AND is_active = 1";
		$this->confence->update($data, $where);
	}
	
	public function updateParticipantKeyBySessionId($participantKey, $sessionId) {
		$where = sprintf("participant_session_id='".$sessionId."'");
		$result = $this->confenceParticipant->getRowsAssoc($where);
		if (!$result)
			return;
// 			throw new Exception("no video participant record. session_id: ".$sessionId);
		if (count($result) > 1)
			throw new Exception("Not unique video participant record. session_id: ".$sessionId);
		
		$data = array("participant_key"=>$participantKey, "updatetime"=>date("Y-m-d H:i:s"));
		$this->confenceParticipant->update($data, $where);
	}
	
	public function insertConferenceRecord(/*ConferenceRecordObject*/ $obj) {
		$this->callback->add($obj->getContents());
	}
	
	public function getConferenceParticipant($confId, $partId) {
		$result = array();
		$where = sprintf("conference_id='".$confId."' and part_id='".$partId."'");
		$result = $this->confenceParticipant->getRowsAssoc($where);
		return $result;
	}
	
	public function getActiveConferenceParticipantByDid($did) {
		$result = array();
		$where = sprintf("did='".$did."' AND is_active=1");
		$result = $this->confenceParticipant->getRowsAssoc($where);
		return $result;
	}
	
	public function removeConferenceParticipantRecord($confId, $partId) {
		$where = sprintf("conference_id='".$confId."' and part_id='".$partId."'");
		$data = array("is_active"=>0,"updatetime"=>date("Y-m-d H:i:s"));
		$this->confenceParticipant->update($data, $where);
	}
	
	public function getActiveParticipantCountByMixerKey($mediaMixerKey) {
		return $this->confenceParticipant->getActiveParticipantCountByMixerKey($mediaMixerKey);
	}
	
	public function setDocumentSharingStatus($conferenceRecord, $status) {
		$data = array("document_sharing_status"=>$status);
		$where = sprintf("video_conference_key=%d", $conferenceRecord["video_conference_key"]);
		$this->confence->update($data, $where);
	}
	
	/**
	 * <p>
	 * 引数のDIDを元にConferenceRecordを取得する
	 * Recordが存在しない場合$timeの回数のみ取得を繰り返す
	 * </p>
	 * @param string $did
	 * @param int $times
	 * @return boolean
	 */
	public function getActiveConferenceByDidWithWatching($did, $times) {
		$count = 0;
		do {
			if ($record = $this->getActiveConferenceByDid($did)) return true;
			sleep(1);
			$count++;
		}
		while ($times > $count );
		return false;
	}
	
	/**
	 * 
	 * <p>終了しているカンファレンスを含めconference table内より検索する</p>
	 * @param string $confId
	 * @throws Exception
	 * @return conference record object
	 */
	public function getConferenceByConfIdWithThrowableException($confId) {
		$this->conferenceRecord = $this->getConferenceByConfId($confId);
		if (!$this->conferenceRecord)
			throw new Exception("doesnt exist conference record. id: ".$confId);
		return $this->conferenceRecord;
	}
	
	/**
	 * 
	 * @param object $key
	 * @return object;
	 */
	public function __get($key) {
		return $this->$key;
	}
}
