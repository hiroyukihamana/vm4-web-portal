<?php
require_once("classes/mcu/model/Proxy.php");

require_once(dirname(__FILE__)."/../../lib/MCUCurl.php");
require_once(dirname(__FILE__)."/../model/do/State.php");
require_once(dirname(__FILE__)."/../model/delegate/MCUSoapClient.php");
require_once(dirname(__FILE__)."/../model/delegate/MCUSoapGuestClient.php");
require_once(dirname(__FILE__)."/../params/CallParticipantParams.php");
require_once(dirname(__FILE__)."/../params/CallParticipantWithAuthParams.php");
require_once(dirname(__FILE__)."/../params/CreateTemplateGuestDidParams.php");
require_once(dirname(__FILE__)."/../params/DeleteTemplateGuestDidParams.php");
require_once(dirname(__FILE__)."/../params/DeleteTemplateGuestDidsParams.php");
require_once(dirname(__FILE__)."/../params/GetConferenceAdHocTemplatePropertiesParams.php");
require_once(dirname(__FILE__)."/../params/GetConferenceReturnParams.php");
require_once(dirname(__FILE__)."/../params/GetParticipantsParams.php");
require_once(dirname(__FILE__)."/../params/RemoveConferenceParams.php");
require_once(dirname(__FILE__)."/../params/RemoveParticipantParams.php");
require_once(dirname(__FILE__)."/../params/RejectParticipantParams.php");
require_once(dirname(__FILE__)."/../params/SetVideoMuteParams.php");
require_once(dirname(__FILE__)."/../params/SetParticipantBackgroundParams.php");
require_once(dirname(__FILE__)."/../params/StartRenderingInSlotParams.php");
require_once(dirname(__FILE__)."/../params/StopRenderingInSlotParams.php");

/**
 * mcu controller上のデータの操作（取得）
 * @author akihitowada
 *
 */
class MCUControllerProxy extends Proxy {
	private $curl;
	private $soap;
	private $soapGuest;
	function __construct() {
		parent::__construct();
		
		$this->curl = new MCUCurl();
		$this->soap = new MCUSoapClient();
		$this->soapGuest = new MCUSoapGuestClient();
	}
	
	public function setWSDL($wsdl) {
		if ($this->soap) {
			$this->soap = null;
			$this->soapGuest = null;
		}
		$this->soap = new MCUSoapClient($wsdl);
		$this->soapGuest = new MCUSoapGuestClient($wsdl);
	}
	
	/**
	 * 
	 * @param unknown $confId
	 */
	public function getParticipatedVideoParticipantListByConfId($confId) {
		$list = array();
		$list = $this->removeViewParticipant($this->getParticipantListByConfId($confId));

		return $list;
	}
	
	/**
	 * 
	 * @param string $confId
	 * @return Array<Participant>
	 */
	public function getParticipatedHardWareParticipantListByConfId($confId) {
		$list = array();
		$list = $this->removeFlashParticipant($this->removeViewParticipant($this->getParticipantListByConfId($confId)));
		return $list;
	}
	
	public function getParticipatedHardWareParticipantListByConfIdWithoutSelfPartId($confId, $partId) {
		$list = array();
		$list = $this->removeFlashParticipant($this->removeViewParticipant($this->getParticipantListByConfId($confId)));
		$result = array();
		for ($i = 0; $i < count($list); $i++) {
			$client = $list[$i];
			if ($client->id != $partId) {
				$result[] = $client;
			}
		}
		return $result;
	}
	
	/**
	 * mcuから取得
	 * 一人だけの場合オブジェクトが帰ってくるので
	 * array形式にしている
	 * @param strin $confId
	 * @return array
	 */
	public function getParticipantListByConfId($confId) {
		$obj = new GetParticipantsParams(array("confId"=>$confId));
		$params = $obj->getParams();
		$result = $this->soap->getParticipants($params);
		$list = array();
		if (0 == strcasecmp(gettype($result), "object")) {
			array_push($list, $result);
		}
		else if (0 == strcasecmp(gettype($result), "array")) {
			$list = $result;
		}
		return $list;
	}
	
	/**
	 * view clientを削除
	 * @param array $list
	 * @return array
	 */
	private function removeViewParticipant($list) {
		// viewerの参加者は削除
		$result = array();
		for ($i = 0; $i < count($list); $i++) {
			$client = $list[$i];
			$name = explode("@", $client->name);
			if ($name[0] != $this->configProxy->get("defaultViewId")) {
// 			if ($name[0] != $this->configProxy->get("defaultViewId") &&
// 				$name[0] != $this->configProxy->get("smallViewId")) {
// 				array_splice($list, $i, 1);
				$result[] = $client;
			}
		}
		return $result;
// 		return $list;
	}

	/**
	 * flash & mobile clientを削除
	 * @param array $list
	 * @return array
	 */
	private function removeFlashParticipant($list) {
		// viewerの参加者は削除
		$result = array();
		for ($i = 0; $i < count($list); $i++) {
			$client = $list[$i];
			$name = explode("@", $client->name);
// 			if (
// 				// participated by temp//perm did.
// 				(($name[1] == $this->configProxy->get("videoParticipantDomain") ||
// 				$name[1] == $this->configProxy->get("videoParticipantGuestDomain")) &&
// 				$client->state == State::CONNECTED()) ||
				
// 				// callee video participant. 
// 				($name[1] != $this->configProxy->get("videoParticipantDomain") &&
// 				$name[1] != $this->configProxy->get("videoParticipantGuestDomain") &&
// 				$client->state == State::CONNECTED())
// 							) {
// 				$result[] = $client;
// 			}
			if ($client->type == "SIP" &&
				$client->state == State::CONNECTED()) {
				$result[] = $client;
			}
		}
		return $result;
	}
	
	public function removeParticipant($confId, $partId) {
		$obj = new RemoveParticipantParams(array(
				"confId"=>$confId,
				"partId"=>$partId
		));
		$this->soap->removeParticipant($obj->getParams());
	}
	
	/**
	 * <p></p>
	 * @param string $confId
	 * @param int $partId
	 */
	public function rejectParticipant($confId, $partId) {
		$obj = new RejectParticipantParams(array(
				"confId"=>$confId,
				"partId"=>$partId
		));
		$this->soap->rejectParticipant($obj->getParams());
	}
	
	public function createConferenceExt($params) {
		$result = $this->soap->createConferenceExt($params);
		return $result->id;
	}
	
	public function getConference($params) {
		$result = $this->soap->getConference($params);
		if ($result) {
			$obj = new GetConferenceReturnParams($result);
			return $obj->getParams();
		}
		return null;
	}
	
	public function setParticipantBackground($confId, $partId) {
		$obj = new SetParticipantBackgroundParams(array(
				"confId"=>$confId,
				"partId"=>$partId,
				"filename"=>$this->configProxy->get("backgroundImage")
		));
		$this->soap->setParticipantBackground($obj->getParams());
	}
	
	public function setCustomBackground($confId, $path) {
		$obj = new SetParticipantBackgroundParams(array(
				"confId"=>$confId,
				"partId"=>0,
				"filename"=>$path
		));
		$this->soap->setParticipantBackground($obj->getParams());
	}
	
	public function setTelephoneClientBackground($confId, $partId) {
		$obj = new SetParticipantBackgroundParams(array(
				"confId"=>$confId,
				"partId"=>$partId,
				"filename"=>$this->configProxy->get("telephoneClientImage")
		));
		$this->soap->setParticipantBackground($obj->getParams());
	}
	
	public function setVideoMute($confId, $partId, $flag) {
		$obj = new SetVideoMuteParams(array(
				"confId"=>$confId,
				"partId"=>$partId,
				"flag"=>$flag ? 1 : 0));
		$this->soap->setVideoMute($obj->getParams());
	}
	
	public function callParticipant($did, $dest) {
		$obj = new CallParticipantParams(array(
				"confId"=>$did,
				"dest"=>$dest,
				"mosaicId"=>0,
				"sidebarId"=>0));
		$this->soap->callParticipant($obj->getParams());
	}
	
	public function callSipParticipant($did, $dest, $callerId) {
		$obj = new CallParticipantWithAuthParams(array(
				"confId"=>$did,
				"dest"=>$dest,
				"mosaicId"=>0,
				"sidebarId"=>0,
				"profileId"=>$this->configProxy->get("polycomProfileId"),
				"callerId"=>$callerId,
				"username"=>"",
				"password"=>""));
		$this->soap->callParticipantWithAuth($obj->getParams());
	}
	
	public function cancelCalling($confId, $partId) {
		$this->removeParticipant($confId, $partId);
	}
	
	public function removeConferenceByConfId($confId) {
		$obj = new RemoveConferenceParams(array("confId"=>$confId));
		$this->soap->removeConference($obj->getParams());
	}
	
	public function startRenderingInSlotForVideoParticipant($confId, $appId, $slot) {
		$obj = new StartRenderingInSlotParams(
				array("confId"=>$confId,
						"mosaicId"=>0,
						"appId"=>$appId,
						"slot"=>$slot));
		$this->soap->startRenderingInSlot($obj->getParams());
	}
	
	public function stopRenderingInSlotForVideoParticipant($confId, $appId, $slotId) {
		$obj = new StopRenderingInSlotParams(
				array("confId"=>$confId,
						"mosaicId"=>0,
						"appId"=>$appId));
		$this->soap->stopRenderingInSlot($obj->getParams());
	}
	
	public function setDocument($url, $confId, $imagePath) {
		$this->curl->post($url, $confId, $imagePath);
	}
	
	public function createTemplateGuestDid($did, $callbackurl) {
		$obj = new CreateTemplateGuestDidParams(array(
				"templDid"=>$did,
				"didPrefix"=>$this->configProxy->get("guestPrefix"),
				"didLength"=>$this->configProxy->get("guestDidLength"),
				"callbackUrl"=>$callbackurl,
				"createOnConf"=>true));
		$result = $this->soapGuest->createTemplateGuestDid($obj->getParams());
		return $result;
	}
	
	public function deleteTemplateGuestDid($guestDid) {
		$obj = new DeleteTemplateGuestDidParams(array("guestDid"=>$guestDid));
		$this->soapGuest->deleteTemplateGuestDid($obj->getParams());
	}
	
	public function deleteTemplateGuestDids($did) {
		$obj = new DeleteTemplateGuestDidsParams(array("did"=>$did));
		$this->soapGuest->deleteTemplateGuestDids($obj->getParams());
	}
	
	public function existConferenceTemplateByDid($did) {
		$obj = new GetConferenceAdHocTemplatePropertiesParams(array("did"=>$did));
		$return = $this->soap->getConferenceAdHocTemplateProperties($obj->getParams());
		if (!$return) {
			throw new Exception(sprintf("did %s does not exists.", $did));
		}
	}

	public function __call($name, $args) {
		$params = $args[0];
		return $this->soap->$name($params);
	}
}
