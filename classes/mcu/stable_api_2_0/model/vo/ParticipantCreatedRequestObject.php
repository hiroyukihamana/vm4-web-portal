<?php

class ParticipantCreatedRequestObject {
	private $did;
	private $confId;
	private $partAudioMuted;
	private $partAudioSupported;
	private $partId;
	private $partName;
	private $partProfileUid;
	private $partProfileName;
	private $partProfileVideoBitrate;
	private $partProfileVideoBitrateStrict;
	private $partProfileVideoFPS;
	private $partProfileVideoSize;
	private $partProfileVideoQmin;
	private $partProfileVideoQmax;
	private $partProfileVntraPeriod;
	private $partState;
	private $partTextMuted;
	private $partTextSupported;
	private $partType;
	private $partVideoMuted;
	private $partVideoSupported;
	private $partMosaicId;
	private $partSidebarId;
	private $partSessionId;
	
	public function __construct($request) {
		$this->confId				= $request->confId;
		$this->partAudioMuted		= $request->part->audioMuted;
		$this->partAudioSupported	= $request->part->audioSupported;
		$this->partId				= $request->part->id;
		$this->partName				= $request->part->name;
		$this->partProfileUid		= $request->part->profile->uid;
		$this->partProfileName		= $request->part->profile->name;
		$this->partProfileVideoBitrate	= $request->part->profile->videoBitrate;
		$this->partProfileVideoBitrateStrict	= $request->part->profile->videoBitrateStrict;
		$this->partProfileVideoFPS		= $request->part->profile->videoFPS;
		$this->partProfileVideoSize		= $request->part->profile->videoSize;
		$this->partProfileVideoQmin		= $request->part->profile->videoQmin;
		$this->partProfileVideoQmax		= $request->part->profile->videoQmax;
		$this->partProfileVntraPeriod	= $request->part->profile->intraPeriod;
		$this->partState			= $request->part->state;
		$this->partTextMuted		= $request->part->textMuted;
		$this->partTextSupported	= $request->part->textSupported;
		$this->partType				= $request->part->type;
		$this->partVideoMuted		= $request->part->videoMuted;
		$this->partVideoSupported	= $request->part->videoSupported;
		$this->partMosaicId			= $request->part->mosaicId;
		$this->partSidebarId		= $request->part->sidebarId;
		$this->partSessionId		= $request->part->sessionId;
	}

	public function setDid($did) {
		$this->did = $did;
	}
	
	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return null;
		}
	}
}