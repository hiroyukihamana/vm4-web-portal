<?php
class ConferenceCreatedRequestObject {
	private $confId;
	private $compType;
	private $did;
	private $id;
	private $name;
	private $numActParticipants;
	private $numSlots;
	private $size;
	private $slots;
	private $timestamp;
	
	function __construct($request) {
		$this->confId	= $request->confId;
		$this->compType	= $request->conf->confId;
		$this->did	= $request->conf->did;
		$this->id	= $request->conf->id;
		$this->name	= $request->conf->name;
		$this->numActParticipants	= $request->conf->numActParticipants;
		$this->numSlots	= $request->conf->numSlots;
		$this->size		= $request->conf->size;
		$this->slots	= $request->conf->slots;
		$this->timestamp	= $request->conf->timestamp;
	}
	
	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return null;
		}
	}
}
