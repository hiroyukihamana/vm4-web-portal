<?php
require_once(dirname(__FILE__)."/../../../lib/rtmp/RtmpClient.class.php");

require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../../../lib/MCUSoapException.php");
require_once(dirname(__FILE__)."/../../model/ConferenceProxy.php");

class FMSNetConnectionDelegate {
	private $rtmpClient;
	function __construct() {
		$this->rtmpClient	= new RTMPClient();

		$this->configProxy 	= new McuConfigProxy();
		
		$this->proxy = ConferenceProxy::getInstance();
		$this->logger = EZLogger2::getInstance();
	}
	
	public function connect($host, $appname, $params = null) {
		try {
			ConferenceLogger::info(__CLASS__.":".__FUNCTION__."_start", null, null, null, null, null, array($host, $appname, $params));
			$this->rtmpClient->connect($host, $appname, 1935, $params);
			ConferenceLogger::info(__CLASS__.":".__FUNCTION__."_end", null, null, null, null, null, array($host, $appname, $params));
		}
		catch (Exception $e) {
			$obj = new ConferenceRecordObject(__CLASS__.":".__FUNCTION__, null, null, null, null, null, $e->getMessage());
		}
	}
	
	public function __call($name, $args) {
		try {
			ConferenceLogger::info($name, null, null, null, null, null, $args);
			$return = $this->rtmpClient->$name($args[0]);
			ConferenceLogger::info($name, null, null, null, null, null, $return);
		}
		catch (Exception $e) {
			ConferenceLogger::info($name, null, null, null, null, null, $e->getMessage());
		}
	}
}
