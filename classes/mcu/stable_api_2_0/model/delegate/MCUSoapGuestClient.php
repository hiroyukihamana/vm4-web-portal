<?php
require_once(dirname(__FILE__)."/MCUSoapClient.php");
 
class MCUSoapGuestClient extends MCUSoapClient {
	private $guestClient;
	function __construct($wsdl = null){
		parent::__construct($wsdl);

		if ($wsdl) {
			define("WSDL_DOMAIN", $wsdl);
				
			$this->guestClient = new SoapClient(sprintf($this->configProxy->get("guestWsdl"), WSDL_DOMAIN));
		}
		else if (defined("WSDL_DOMAIN")) {
			$this->guestClient = new SoapClient(sprintf($this->configProxy->get("guestWsdl"), WSDL_DOMAIN));
		}
	}
	
	public function __call($name, $args) {
		try {
			$params = $args[0];
$this->logger->info($params);
$this->logger->info($name);
			$result = $this->guestClient->$name($params);
			return $result->return;
		}
		catch (SoapFault $fault) {
$this->logger->info($fault);
			$obj = new ConferenceRecordObject($name, null, null, null, null, null, $fault.faultcode);
			$this->proxy->insertConferenceRecord($obj);
			throw new Exception($name . ": params: ". implode(", ", $params).": code: " .$fault.faultcode);
		}
	}
}
