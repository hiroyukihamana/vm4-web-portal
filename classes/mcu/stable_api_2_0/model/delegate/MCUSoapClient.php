<?php

require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../../../lib/MCUSoapException.php");
require_once(dirname(__FILE__)."/../../model/ConferenceProxy.php");

class MCUSoapClient {

	protected $logger = "";
	private $client;
	private $config;
	
	function __construct($wsdl = null){
		$this->configProxy 	= new McuConfigProxy();
		
		$this->proxy = ConferenceProxy::getInstance();
		$this->logger = EZLogger2::getInstance();
		
		if ($wsdl) {
			define("WSDL_DOMAIN", $wsdl);
			
			$this->client	= new SoapClient(sprintf($this->configProxy->get("create_client"), WSDL_DOMAIN));
		}
		else if (defined("WSDL_DOMAIN")) {
			$this->client	= new SoapClient(sprintf($this->configProxy->get("create_client"), WSDL_DOMAIN));
		}
	}
	
	public function __call($name, $args) {
		try {
			$params = $args[0];
$this->logger->info($params);
$this->logger->info($name);
			$result = $this->client->$name($params);
$this->logger->info($result);
			return $result->return;
		}
		catch (SoapFault $fault) {
			ConferenceLogger::info($name, null, null, null, null, null, $fault.faultcode);
			throw new MCUSoapException($name . ": params: ". implode(", ", $params).": code: " .$fault.faultcode);
		}
	}
}
