<?php 
require_once("classes/mcu/model/Proxy.php");

require_once(dirname(__FILE__)."/../model/delegate/FMSNetConnectionDelegate.php");

class FMSProxy extends Proxy {
	private $ncDelegate;
	private $host;
	private $appName;
	function __construct($host = null, $appName = null) {
		parent::__construct();
		
		$this->ncDelegate = new FMSNetConnectionDelegate();
		if ($host) $this->setHost($host);
		if ($appName) $this->setAppName($appName);
	}
	
	public function connect() {
		$this->ncDelegate->connect($this->host, $this->appName, array(40960));
	}
	
	public function setHost($host) {
		$this->host = $host;
	}
	
	public function setAppName($appName) {
		$this->appName = $appName;
	}
	
	public function bootFMSInstance($params) {
		try {
			$this->ncDelegate->noticeFromWeb($params);
		}
		catch (Exception $e) {}
	}
	
	public function noticeParticipantListChanged($params) {
		try {
			$this->ncDelegate->noticeFromWeb($params);
		}
		catch (Exception $e) {}
	}
}
