<?php
require_once("classes/mcu/config/McuConfigProxy.php");
require_once(dirname(__FILE__)."/VideoConferenceOptionClass.php");
require_once("lib/EZLib/EZCore/EZLogger2.class.php");

class VideoConferenceOptionCreateClass {
	private static $egnore_option_name = "video_conference";
	private static $className = "VideoConferenceOptionClass";
	private static $optionKey = 24;
	
	private static $instance = null;
	public static function getObjectByRoomKey($roomKey) {
		if (!$roomKey) {
			return null;
		}
		else if (self::$instance) {
			return self::$instance;
		}
		if (self::getOption($roomKey))
			self::$instance = new self::$className($roomKey);
		 
		return self::$instance;
	}
	
	private static function getOption($roomKey) {
		$hasOption = self::checkOption($roomKey);
		$enable = self::checkEnableOption();
		return $hasOption && $enable;
	}
	
	/**
	 * @return boolean
	 */
	private static function checkOption($roomKey) {
		require_once("classes/dbi/ordered_service_option.dbi.php");
		/*
		 * dbi内で呼ばれてるLoggerが先にインスタンスを生成されている体で作られてるので仕方なく
		 */
		$config = self::getConfig();
		$logger2 = EZLogger2::getInstance($config);
		$proxy = new McuConfigProxy();
		$service_option  = new OrderedServiceOptionTable($proxy->getDsn());
		$hasOption = $service_option->enableOnRoom(self::$optionKey, $roomKey);
		return $hasOption;
	}
	
	/**
	 * @return boolean
	 */
	private function checkEnableOption() {
		$enable = !self::getConfig()->get("IGNORE_MENU", self::$egnore_option_name);
		return $enable;
	}
	
	/**
	 * 
	 */
	private static function getConfig() {
		return McuConfig::getInstance();
	}
}
