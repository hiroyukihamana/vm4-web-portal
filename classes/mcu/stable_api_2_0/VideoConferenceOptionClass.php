<?php

require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/McuModel.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");
require_once("classes/mgm/dbi/mcu_server.dbi.php");

require_once(dirname(__FILE__)."/controller/ConferenceController.php");
require_once(dirname(__FILE__)."/logic/composition/CompositionManager.php");
require_once(dirname(__FILE__)."/logic/composition/DocumentSharingCompositionManager.php");
require_once(dirname(__FILE__)."/logic/MosaicController.php");
require_once(dirname(__FILE__)."/logic/ParticipationController.php");


class VideoConferenceOptionClass extends ConferenceController {
	private $settingRecord;
	private $roomRecord;
	
	public function __construct($roomKey) {
		parent::__construct();
		
		$this->roomRecord		= $this->meetingProxy->getRoomRecordByRoomKey($roomKey);
		$this->settingRecord	= $this->meetingProxy->getSettingRecordByRoomKey($roomKey);
		$this->mcuGatewayProxy= new McuGatewayProxy();
		$this->mcuGatewayProxy->setMcuServerModelByRoomKey($roomKey);
	}
	
	private function formedAddress($did, $domain) {
		$address = sprintf("%s@%s", $did, $domain);
		return $address;
	}
	
	public function getNumberAddress($did) {
		$domain = $this->mcuGatewayProxy->getRegularNumberDomain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}
	
	public function getRegularAddress($did) {
		$domain = $this->mcuGatewayProxy->getRegularDomain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}

	public function getH323TemporaryNumberAddress($did) {
		$domain = $this->mcuGatewayProxy->getH323GuestNumberDomain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}

	public function getH323TemporaryAddress($did) {
		$domain = $this->mcuGatewayProxy->guestH323Domain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}
	
	public function getSipTemporaryNumberAddress($did) {
		$domain = $this->mcuGatewayProxy->guestSipNumberDomain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}
	
	public function getSipTemporaryAddress($did) {
		$domain = $this->mcuGatewayProxy->guestSipDomain();
		if ($domain)
			$address = $this->formedAddress($did, $domain);
		return $address;
	}
	
	public function getVideoConferenceByMeetingKey ($meetingKey) {
		require_once("classes/dbi/video_conference.dbi.php");
		$videoConf		= new VideoConferenceTable($this->dsn);
		$videoWhere		= "meeting_key = '".addslashes($meetingKey)."'";
		$videoInfo	= $videoConf->getRow($videoWhere);
		return $videoInfo;
	}
	
	public function getIvesInfoByRoomKey($roomKey) {
		require_once("classes/dbi/ives_setting.dbi.php");
		$ivesSettingTable= new IvesSettingTable($this->dsn);
		$where = "room_key='".$roomKey."' and is_deleted = 0";
		$ivesInfo	= $ivesSettingTable->getRow($where);
		
		require_once("classes/dbi/ordered_service_option.dbi.php");
		$service_option  = new OrderedServiceOptionTable($this->dsn);
		$where = sprintf("room_key = '%s' AND ordered_service_option_status = 1", $roomKey);
		$option = $service_option->getRow($where);
		if ($option) {
			return $ivesInfo;
		}
		else {
			return null;
		}
	}
	
	public function getDidByRoomKey($roomKey) {
		$ives = $this->getIvesInfoByRoomKey($roomKey);
		return $ives["ives_did"];
	}
	
	public function cancelReservation($confId) {
		$this->mcuControllerProxy->removeConferenceByConfId($confId);
	}
		
	/**
	 * 追加をonparticipantCreated側に委譲するため
	 * 削除予定
	 * stable1_3側のみ機能
	 * @param unknown $meetingKey
	 * @param unknown $participantKey
	 * @version 20130523
	 */
	public function addVideoConferenceParticipant($meetingKey, $participantKey) {}
	
	public function getConfig() {
		return McuConfig::getInstance();
	}
	
	public function getMcuGatewayByRoomKey($roomKey) {
		$info = array(
				"status"			=> true,
				"contract"			=> true,
				"sipServer"			=> $this->mcuGatewayProxy->getMCUControllerServer(),
				"sipServerDomain"	=> $this->mcuGatewayProxy->getServerDomain(),
				"sipProxyIp"		=> $this->mcuGatewayProxy->getSipProxy(),
				"sipProxyPort"		=> $this->mcuGatewayProxy->getSipProxyPort(),
				"socketServer"		=> $this->mcuGatewayProxy->getSocketServer(),
				"socketServerPort"	=> $this->mcuGatewayProxy->getSocketPort(),
				"defaultId"			=> $this->settingRecord['client_id'],
				"defaultPw"			=> $this->settingRecord['client_pw'],
				"defaultViewId"		=> $this->configProxy->get("defaultViewId"),
				"defaultViewPw"		=> $this->configProxy->get("defaultViewPw"),
				"smallId"			=> $this->configProxy->get("smallId"),
				"smallPw"			=> $this->configProxy->get("smallPw"),
				"smallViewId"		=> $this->configProxy->get("smallViewId"),
				"smallViewPw"		=> $this->configProxy->get("smallViewPw"),
				"telephoneId"		=> $this->configProxy->get("telephoneId"),
				"telephonePw"		=> $this->configProxy->get("telephonePw"),
				"did"				=> $this->settingRecord['ives_did']);
		$model = new McuModel($info);
		return $model->getContent();
	}
	
	public function getReservationGuestDidByParmanentDid($did) {
		$callbackurl = sprintf($this->configProxy->get("reservationGuestCallbackUrl"), $this->configProxy->get("soap_domain"));
		return $this->mcuControllerProxy->createTemplateGuestDid($did, $callbackurl);
	}
	
	public function getInvitationGuestDidByParmanentDid($did) {
		$callbackurl = sprintf($this->configProxy->get("invitationGuestCallbackUrl"), $this->configProxy->get("soap_domain"));
		return $this->mcuControllerProxy->createTemplateGuestDid($did, $callbackurl);
	}
	
	public function deleteTemplateGuestDid($guestDid) {
		$this->mcuControllerProxy->deleteTemplateGuestDid($guestDid);
	}
	
	public function getUsageTimeFromMeetingUpTime($meetingKey) {
		return $this->meetingProxy->getUsageTimeFromMeetingUpTime($meetingKey);
	}
	
	public function updateListWithoutPolycom($meetingKey, $participantList) {
		return $this->meetingProxy->updateListWithoutPolycom($meetingKey, $participantList);
	}
	
	public function getParticipatedList($meetingKey) {
		return $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByDid($this->settingRecord["ives_did"]);
	}
	
	public function removeConference($confId) {
		$list = $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByConfId($confId);
		for ($i = 0; $i < count($list); $i++) {
			$participant = $list[$i];
			$this->mcuControllerProxy->removeParticipant($confId, $participant["part_id"]);
		}
	}
	
	public function callParticipant($meetingKey, $target, $protocol="sip", $name=null) {
		$this->mcuControllerProxy->existConferenceTemplateByDid($this->settingRecord["ives_did"]);
		
		// 入力値チェック
		preg_match("/^".REG_IP."$/", $target, $ipresult);
		if (count($ipresult) <= 0) throw new Exception("invalid ip string: ".$target, -2);
		preg_match("/^".REG_CALLOUT_NAME."$/", $name, $nameresult);
		if (count($nameresult) <= 0) throw new Exception("invalid name string: ".$name, -2);
		
		/**
		 * 参加者チェック
		 */
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if ($conferenceRecord) {
			$list = $this->meetingProxy->getParticipantByMeetingKey($meetingKey);
			$participationController = new ParticipationController();
			if (!$participationController->checkParticipation($conferenceRecord["conference_id"], $list, $this->roomRecord["max_seat"], $this->settingRecord["num_profiles"])) {
				throw new Exception("invalid participated number: ".count($list), -1);
			}
		}
		else if ($this->settingRecord["num_profiles"] <= 0) {
			throw new Exception("check contract number. did:  ".$this->settingRecord["ives_did"], -1);
		}
		
		$name = $this->conferenceProxy->getCalleeName($name);
		if (0 == strcasecmp($protocol, "sip")) {
			$prefix = $this->configProxy->get("callout_prefix");
			$dest = $prefix.$name."@".$target;
		}
		else {
			$template = $this->configProxy->get("callout_h323_template");
// 			$dest = sprintf($template, $name, $target, $this->mcuGatewayProxy->getRegularDomain());
			$dest = sprintf($template, $name, $target, $this->mcuGatewayProxy->getH323CalloutDomain());
		}
		$participantRecord = $this->conferenceProxy->addCallingParticipantRecord($this->settingRecord["ives_did"], $meetingKey, $name, $target);
		$this->mcuControllerProxy->callParticipant($this->settingRecord["ives_did"], $dest);
		return $participantRecord;
	}
	
	public function getCallingStatus($videoParticipantKey) {
		return $this->conferenceProxy->getCalleeParticipantStatusVideoParticipantKey($videoParticipantKey);
	}
	
	public function cancelCallingParticipant($meetingKey) {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			return;
		$callee = $this->conferenceProxy->getCallingParticipantRecord($conferenceRecord["conference_id"], $conferenceRecord["did"], $meetingKey);
		if ($callee) {
			$this->mcuControllerProxy->cancelCalling($conferenceRecord["conference_id"], $callee["part_id"]);
		}
	}
	
	public function startDocumentSharing($meetingKey) {
		return;
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			return;

		// document_status 開始フラグ
		$this->conferenceProxy->setDocumentSharingStatus($conferenceRecord, 1);
		
		// composite
		try {
			$count = count($this->mcuControllerProxy->getParticipatedVideoParticipantListByConfId($conferenceRecord["conference_id"]));
			$composite = new DocumentSharingCompositionManager($conferenceRecord["conference_id"], $count, $this->settingRecord && $this->settingRecord["use_active_speaker"], $this->roomRecord["mobile_4x4_layout"]);
			$composite->run();
			
			// mosaic
			$mosaic = new MosaicController();
			$params = $mosaic->getDefaultMosaicSlotParamsToDeActivateVadSlot($conferenceRecord["conference_id"]);
			$this->mcuControllerProxy->setMosaicSlot($params);
			
			// start
			$this->mcuControllerProxy->startRenderingInSlotForVideoParticipant($conferenceRecord["conference_id"], 1, 0);
		} catch (Exception $e) {
			$this->conferenceProxy->setDocumentSharingStatus($conferenceRecord, 0);
			throw $e;
		}
	}
	
	public function stopDocumentSharing($meetingKey) {
		return;
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			return;

		// document_status 終了フラグ
		$this->conferenceProxy->setDocumentSharingStatus($conferenceRecord, 0);
		
		$count = count($this->mcuControllerProxy->getParticipatedVideoParticipantListByConfId($conferenceRecord["conference_id"]));
		$countHardwareParticipant = count($this->mcuControllerProxy->getParticipatedHardWareParticipantListByConfId($conferenceRecord["conference_id"]));
		
		$composite = new CompositionManager($conferenceRecord["conference_id"], $count, $countHardwareParticipant, $this->settingRecord && $this->settingRecord["use_active_speaker"], $this->roomRecord["mobile_4x4_layout"]);
		$composite->run();


		// mosaic
		$mosaic = new MosaicController();
		if ($this->settingRecord &&
			$this->settingRecord["use_active_speaker"] &&
			$count >= 3) {
			$params = $mosaic->getDefaultMosaicSlotParamsToActivateVadSlot($conferenceRecord["conference_id"]);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}

		$this->mcuControllerProxy->stopRenderingInSlotForVideoParticipant($conferenceRecord["conference_id"], 1, 0);
	}
	
	public function documentSharing($meetingKey, $documentId=null, $page=null) {
		return;
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			throw new Exception("no conference record: ".$meetingKey);
		
		$mcuRecord	= $this->mcuGatewayProxy->getMcuServerByServerKey($conferenceRecord["mcu_server_key"]);
		if (!$mcuRecord)
			throw new Exception("irregal mcu server status: key: ".$conferenceRecord["mcu_server_key"]);
		
		if ($documentId) {
			$imagePath = $this->documentProxy->getImagePath($documentId, $page);
			if (!file_exists($imagePath)) {
				$this->logger->info("[document sharing] incompatible file. id: ".$documentId);
				$imagePath = $this->documentProxy->getNoImagePath();
			}
		}
		else {
			$imagePath = $this->documentProxy->getNoImagePath();
		}
		if (!file_exists($imagePath)) {
			throw new Exception("file does not exist. path: ".$imagePath);
		}
		
		$this->mcuControllerProxy->setDocument($mcuRecord["server_address"], $conferenceRecord["conference_id"], $imagePath);
	}
	
	public function changeDataCenter($meetingRecord) {
		$fmsController= new FMSNetConnectionController($meetingRecord);
		$fmsController->connect();
		$fmsController->bootFMSInstance($this->settingRecord);

		$list = $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByMeetingKey($meetingRecord["meeting_key"]);
		$data = array("participantList"=>$list,"callingStatus"=>"");
		$fmsController->noticeParticipantListChanged($data);
	}
}
