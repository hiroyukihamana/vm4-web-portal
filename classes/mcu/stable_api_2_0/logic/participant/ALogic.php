<?php
require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../../logic/composition/CompositionManager.php");
require_once(dirname(__FILE__)."/../../logic/composition/DocumentSharingCompositionManager.php");
require_once(dirname(__FILE__)."/../../logic/composition/MobileMixCompositionManager.php");
require_once(dirname(__FILE__)."/../../logic/composition/SalesMobileMixCompositionManager.php");
require_once(dirname(__FILE__)."/../../logic/MosaicController.php");

require_once(dirname(__FILE__)."/../../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../../model/MeetingProxy.php");
require_once(dirname(__FILE__)."/../../model/ConferenceProxy.php");

abstract class ALogic {
	protected $soap;
	public $requestProxy;
	public $logger;
	
	protected $meetingRecord;
	protected $participantRecord;
	protected $participantRecordList;
	protected $roomRecord;
	protected $settingRecord;
	protected $conferenceRecord;
	protected $conferenceParticipantRecord;
	protected $conferenceParticipantRecordList;
	
	//proxy
	protected $mcuControllerProxy;
	protected $meetingProxy;
	protected $conferenceProxy;

// 	protected $mosaicManager;
	protected $mosaicController;
	
	public function __construct($model) {

		$this->configProxy 	= new McuConfigProxy();
		$this->dsn = $this->configProxy->getDsn();
		$this->logger = EZLogger2::getInstance();
		
		$this->requestProxy	= $model;
		
		// proxy
		$this->mcuControllerProxy	= new MCUControllerProxy();
		$this->meetingProxy			= new MeetingProxy();
		$this->conferenceProxy		= ConferenceProxy::getInstance();

		//
		$this->mosaicController = new MosaicController();
	}
	
	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return null;
		}
	}
	
	public function getParticipatedCount() {
		return count($this->mcuControllerProxy->getParticipatedVideoParticipantListByConfId($this->requestProxy->confId));
	}

	// activate vad
	private function controlActivateVadSlotForDefaultMosaic() {
		if ($this->settingRecord &&
			$this->settingRecord["use_active_speaker"] &&
			$this->getParticipatedCount() >= 3 &&
			($this->conferenceRecord && !$this->conferenceRecord["document_sharing_status"])) {
			
			$params = $this->mosaicController->getDefaultMosaicSlotParamsToActivateVadSlot($this->requestProxy->confId);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}
	}
	
	private function controlActivateVadSlotForMobile() {
		if (($this->roomRecord["mobile_4x4_layout"] &&
			$this->getParticipatedCount() == 17) ||
			(!$this->roomRecord["mobile_4x4_layout"] &&
			$this->getParticipatedCount() == 10)) {
			
			$params = $this->mosaicController->getMobileMosaicSlotParamsToActivateVadSlot($this->requestProxy->confId);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}
	}
	
	public function controlActivateVadSlot() {
		$this->controlActivateVadSlotForDefaultMosaic();
		
		$this->controlActivateVadSlotForMobile();
	}
	// activate vad
	
	// deactivate vad
	private function controlDeActivateVadSlotForDefaultMosaic() {
		if ($this->settingRecord["use_active_speaker"] && $this->getParticipatedCount() == 2) {
			$params = $this->mosaicController->getDefaultMosaicSlotParamsToDeActivateVadSlot($this->requestProxy->confId);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}
	}
	
	private function controlDeActivateVadSlotForMobile() {
		if (($this->roomRecord["mobile_4x4_layout"] &&
			$this->getParticipatedCount() == 16) ||
			(!$this->roomRecord["mobile_4x4_layout"] &&
			$this->getParticipatedCount() == 9)) {

			$params = $this->mosaicController->getMobileMosaicSlotParamsToDeActivateVadSlot($this->requestProxy->confId);
			$this->mcuControllerProxy->setMosaicSlot($params);
		}
	}
	
	public function controlDeActivateVadSlot() {
		$this->controlDeActivateVadSlotForDefaultMosaic();
		
		$this->controlDeActivateVadSlotForMobile();
	}
	// deactivate vad
	
	/**
	 * 
	 */
	public function setCompositionType() {
		$totalCount = count($this->mcuControllerProxy->getParticipatedVideoParticipantListByConfId($this->requestProxy->confId));
		$countHardwareParticipant = count($this->mcuControllerProxy->getParticipatedHardWareParticipantListByConfId($this->requestProxy->confId));
		
		if ($this->meetingRecord && $this->meetingProxy->isSalesConferenceByMeetingKey($this->meetingRecord["meeting_key"])) {
			$obj = new SalesMobileMixCompositionManager($this->requestProxy->confId, $totalCount, $countHardwareParticipant, $this->settingRecord && $this->settingRecord["use_active_speaker"], $this->roomRecord["mobile_4x4_layout"]);
		}
/*
		else if (0 == $countHardwareParticipant) {
			$obj = new MobileMixCompositionManager($this->requestProxy->confId, $totalCount, $countHardwareParticipant, $this->settingRecord && $this->settingRecord["use_active_speaker"], $this->roomRecord["mobile_4x4_layout"]);
		}
*/
		else if ($this->conferenceRecord && !$this->conferenceRecord["document_sharing_status"]) {
			$obj = new CompositionManager($this->requestProxy->confId, $totalCount, $countHardwareParticipant, $this->settingRecord && $this->settingRecord["use_active_speaker"], $this->roomRecord["mobile_4x4_layout"]);
		}
		else {
			$obj = new DocumentSharingCompositionManager($this->requestProxy->confId, $totalCount, $countHardwareParticipant, $this->settingRecord && $this->settingRecord["use_active_speaker"], $this->roomRecord["mobile_4x4_layout"]);
		}

		$obj->run();
	}

	public function getParticipantNumberOnMCU() {
		return count($this->mcuControllerProxy->getParticipantListByConfId($this->requestProxy->confId));
	}
	
	public function isActiveConferenceOnMCU() {
		$obj = new GetConferenceParams(array("confId" => $this->requestProxy->confId));
		$return = $this->mcuControllerProxy->getConference($obj->getParams());
		return $return;
	}
	
	public function destroyConference() {
		$this->mcuControllerProxy->removeConferenceByConfId($this->requestProxy->confId);
	}


	// IRoom
	public function setRoomRecordByRoomKey() {
		$this->roomRecord = $this->meetingProxy->getRoomRecordByRoomKey($this->meetingRecord["room_key"]);
	}
	// IRoom
	
	public function noticeParticipantListChanged() {
		if (!$this->meetingRecord)
			throw new Exception(__CLASS__."::".__FUNCTION__."::no meeting record");
		
		require_once(dirname(__FILE__)."/../../logic/FMSNetConnectionController.php");

		$fmsController = new FMSNetConnectionController($this->meetingRecord);
		$fmsController->connect();
// 		$fmsController->bootFMSInstance();
		
		$list = $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByMeetingKey($this->meetingRecord["meeting_key"]);
		$data = array("participantList"=>$list,"callingStatus"=>"");
		$fmsController->noticeParticipantListChanged($data);
	}
}
