<?php 
require_once(dirname(__FILE__)."/../../logic/participant/ALogic.php");
require_once(dirname(__FILE__)."/../../logic/ParticipationController.php");

require_once(dirname(__FILE__)."/../../params/AddMosaicParticipantParams.php");
require_once(dirname(__FILE__)."/../../params/AddSidebarParticipantParams.php");
require_once(dirname(__FILE__)."/../../params/AcceptParticipantParams.php");
require_once(dirname(__FILE__)."/../../params/ChangeParticipantProfileParams.php");
require_once(dirname(__FILE__)."/../../params/CreateMosaicParams.php");
require_once(dirname(__FILE__)."/../../params/GetConferenceParams.php");
require_once(dirname(__FILE__)."/../../params/GetParticipantsParams.php");
require_once(dirname(__FILE__)."/../../params/RemoveMosaicViewerParticipantParams.php");
require_once(dirname(__FILE__)."/../../params/SetCompositionTypeParams.php");

class ParticipantCreatedLogic extends ALogic {
	public function __construct($model) {
		parent::__construct($model);
	}
	
	// IIConference
	public function setIConferenceRecord() {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByDid($this->requestProxy->did);
		if (!$conferenceRecord)
			throw new Exception("no conference. did: ".$this->requestProxy->did);
		else if (count($conferenceRecord) != 1)
			throw new Exception("check active conference. did: ".$this->requestProxy->did);
		else
			$this->conferenceRecord = $conferenceRecord[0];
	}
	// IIConference

	// IISetting
	public function setISettingRecord() {
		$settingRecord = $this->meetingProxy->getSettingRecordByDid($this->requestProxy->did);
		if (!$settingRecord)
			throw new Exception("check contract. did: ".$this->requestProxy->did);
		else if (count($settingRecord) != 1)
			throw new Exception("check issued did: ".$this->requestProxy->did);
		
		$this->settingRecord = $settingRecord[0];
		
		if ($this->settingRecord["num_profiles"] <= 0)
			throw new Exception("check 'num_profiles' did: ".$this->requestProxy->did.", num_profiles: ".$this->settingRecord["num_profiles"]);
	}
	// IISetting
	
	// IIMeeting
	public function setIMeetingRecord() {
		$this->meetingRecord = $this->meetingProxy->getMeetingByRoomRecord($this->roomRecord);
		if (!$this->meetingRecord)
			throw new Exception("check meeting info. roomkey: ".$this->roomRecord["room_key"]);
	}
	// IIMeeting
	
	public function setIReservationRecord() {}
	
	// IIRoom
	public function setIRoomRecordByRoomKey() {
		$this->roomRecord = $this->meetingProxy->getRoomRecordByRoomKey($this->settingRecord["room_key"]);
		if (!$this->roomRecord)
			throw new Exception("check room info. room_key: ".$this->settingRecord["room_key"]);
	}
	// IIRoom

	// IMeeting
	public function createMeetingRecord() {}
	
	public function setMeetingRecordByRoomRecord() {
		if (!$this->meetingRecord) {
			$this->meetingRecord = $this->meetingProxy->getMeetingByRoomRecord($this->roomRecord);
			if (!$this->meetingRecord) {
				sleep(1);
			}
			$this->meetingRecord = $this->meetingProxy->getMeetingByRoomRecord($this->roomRecord);
		}
	}
	
	// 
	public function setMeetingRecordByMeetingKey() {
		if (!$this->meetingRecord)
			$this->meetingRecord = $this->meetingProxy->getMeetingByMeetingKey($this->conferenceRecord["meeting_key"]);
	}
	// IMeeting
	
	// IParticipant
	public function checkParticipatedNumber() {
		if (!$this->roomRecord) {
			throw new Exception("no room record.");
		}
		if (!$this->settingRecord) {
			throw new Exception("no setting record.");
		}
// 		if (!$this->participantRecordList)
// 			return true;
		
		$participationController = new ParticipationController();
		return $participationController->checkParticipation($this->requestProxy->confId, $this->participantRecordList, $this->roomRecord["max_seat"], $this->settingRecord["num_profiles"]);
	}
	
	public function removeParticipantRecord() {
	}
	
	public function createParticipantRecord() {
		$data = array(
				"participant_session_id"=> $this->meetingProxy->getSessionid(),
				"meeting_key"			=> $this->meetingRecord["meeting_key"],
				"participant_name"		=> $this->requestProxy->partName,
// 				"participant_port"		=> $this->requestProxy->port,
				"participant_protocol"	=> "sip",
				"update_datetime"		=> date("Y-m-d H:i:s"),
				"is_active"				=> 1,
				"uptime_start"			=> date("Y-m-d H:i:s"),
				"uptime_end"			=> null,
				"is_narrowband"			=> 0,
				"use_count"				=> 0,
				"participant_locale"	=> 'ja',
				"create_datetime"		=> date('Y-m-d H:i:s'),
				"ives_part_id"			=> $this->requestProxy->partId
		);
		$result = $this->meetingProxy->addParticipant($data);
		if ($result)
			$this->participantRecord = $result;
		else
			throw new Exception("Failed in creating participant record.");
	}
	
	public function setParticipantRecord() {
		if (!$this->meetingRecord)
			throw new Exception("irregal meeting record. did:".$this->settingRecord["ives_did"]);
		$this->participantRecordList = $this->meetingProxy->getParticipantByMeetingKey($this->meetingRecord["meeting_key"]);
	}
	// IParticipant
	
	//ISetting
	public function setSettingRecordByDid() {
		$settingRecord = $this->meetingProxy->getSettingRecordByDid($this->requestProxy->did);
	
		if (1 == count($settingRecord))
			$this->settingRecord = $settingRecord[0];
		else if (count($settingRecord) > 1)
			throw new Exception("check issued did: ".$this->requestProxy->did);
	}
	//ISetting
	
	// ICreateConference
	public function removeConferenceRecord () {
		if ($this->settingRecord)
			$this->conferenceProxy->clearConferenceRecordByDid($this->settingRecord["ives_did"]);
	}
	
	public function createConferenceRecord() {}
	
	public function setConferenceRecord() {
		if (!$this->conferenceRecord) {
			$conferenceRecord = $this->conferenceProxy->getActiveConferenceByDid($this->requestProxy->did);
			if (count($conferenceRecord) > 1) {
				$this->logger->warn("irregal conference record. did: ".$this->requestProxy->did);

				$this->conferenceProxy->clearOtherThanTheLatest($this->requestProxy->did);
			}
			else
				$this->conferenceRecord = $conferenceRecord[0];
		}
	}
	// ICreateConference
	
	// IConferenceParticipant
	public function removeConferenceParticipantRecord() {
		$this->conferenceProxy->clearConferenceParticipantRecordByConfId($this->requestProxy->confId);
	}
	
	public function createConferenceParticipantRecord() {
		if (!$this->conferenceRecord)
			throw new Exception("no conference record. did: ".$this->requestProxy->did);
		$data = array(
				"video_conference_key"	=> $this->conferenceRecord["video_conference_key"],
				"media_mixer_key"		=> $this->conferenceRecord["media_mixer_key"],
				"conference_id"			=> $this->conferenceRecord["conference_id"],
				"did"					=> $this->conferenceRecord["did"],
				"participant_session_id"=> $this->requestProxy->partSessionId,
				"meeting_key"			=> $this->conferenceRecord["meeting_key"],
				"part_id"				=> $this->requestProxy->partId,
				"participant_name"		=> $this->requestProxy->partName,
				"participant_type"		=> $this->requestProxy->partType,
				"is_active"				=> 1,
				"createtime"=>date('Y-m-d H:i:s'));
		if ($this->participantRecord&&$this->participantRecord["participant_key"])
			$data["participant_key"]	= $this->participantRecord["participant_key"];
		$this->conferenceProxy->createConferenceParticipantRecord($data);
	}
	
	public function setConferenceParticipantRecord() {
		$this->conferenceParticipantRecordList = $this->conferenceProxy->getActiveConferenceParticipantListByConfId($this->requestProxy->confId);
	}
	// IConferenceParticipant
	
	/**
	 * 
	 * @return object
	 * @version 20130425
	 * @since 3.0
	 */
	public function getAcceptVideoParticipantParams() {
		$obj = new AcceptParticipantParams(array(
				"confId"	=> $this->requestProxy->confId,
				"partId"	=> $this->requestProxy->partId,
				"mosaicId"	=> VIDEO_FOR_CONFERENCE,
				"sidebarId"	=> 0));
		return $obj->getParams();
	}
	
	public function getAcceptParamsForFlashParticipant() {
		$obj = new AcceptParticipantParams(array(
				"confId"	=> $this->requestProxy->confId,
				"partId"	=> $this->requestProxy->partId,
				"mosaicId"	=> VIDEO_FOR_FLASH,
				"sidebarId"	=> 1));
		return $obj->getParams();
	}
	
	public function getAcceptMobileViewerParticipantParams() {
		$obj = new AcceptParticipantParams(array(
			"confId"	=> $this->requestProxy->confId,
			"partId"	=> $this->requestProxy->partId,
			"mosaicId"	=> VIDEO_FOR_MOBILE,
			"sidebarId"	=> 0));
		return $obj->getParams();
	}
	
	public function acceptParticipant($params) {
		$this->mcuControllerProxy->acceptParticipant($params);
	}
	
	public function getRejectParams() {
		$params = array(
				"confId"	=> $this->requestProxy->confId,
				"partId"	=> $this->requestProxy->partId
		);
		return $params;
	}

	/**
	 * participant削除する際のパラメーター
	 * @return multitype:removeparam object
	 */
	public function getRemoveMosaicViewerParticipantParams() {
		$obj = new RemoveMosaicViewerParticipantParams(array(
				"confId"	=> $this->requestProxy->confId,
				"mosaicId"	=> VIDEO_FOR_CONFERENCE,
				"partId"	=> $this->requestProxy->partId
		));
		return $obj->getParams();
	}
	
	public function getChangeVideoParticipantProfileParams() {
		$obj = new ChangeParticipantProfileParams(array(
		            "confId"	=> $this->requestProxy->confId,
		            "partId"	=> $this->requestProxy->partId,
		            "profileId"	=> $this->configProxy->get("polycomProfileId")));
		return $obj->getParams();
	}
	
	public function getChangeProfileParamsForFlashParticipant() {
		$obj = new ChangeParticipantProfileParams(array(
		            "confId"	=> $this->requestProxy->confId,
		            "partId"	=> $this->requestProxy->partId,
		            "profileId"	=> $this->configProxy->get("flashProfileId")));
        return $obj->getParams();
	}
	
	public function getChangeProfileParamsForMobileParticipant() {
		$obj = new ChangeParticipantProfileParams(array(
		            "confId"	=> $this->requestProxy->confId,
		            "partId"	=> $this->requestProxy->partId,
		            "profileId"	=> $this->configProxy->get("mobileProfileId")));
        return $obj->getParams();
	}
	
	public function changeParticipantProfile($params) {
		$this->mcuControllerProxy->changeParticipantProfile($params);
	}
	
	public function getCreateMosaicParams() {
		$obj = new CreateMosaicParams(array(
					"confId"   => $this->requestProxy->confId,
					"compType" => 0,
					"size"     => 1));
		return $obj->getParams();
	}
	
	public function getAddMosaicForFlashParticipantParams() {
		$obj = new AddMosaicParticipantParams(array(
					"confId"=>$this->requestProxy->confId,
					"mosaicId"=>VIDEO_FOR_FLASH,
// 					"num"=>0,
					"partId"=>$this->requestProxy->partId));
		return $obj->getParams();
	}
	
	public function getAddMosaicForMobileParticipantParams() {
		$obj = new AddMosaicParticipantParams(array(
					"confId"=>$this->requestProxy->confId,
					"mosaicId"=>VIDEO_FOR_MOBILE,
// 					"num"=>0,
					"partId"=>$this->requestProxy->partId));
		return $obj->getParams();
	}
	
	public function getAddMobileMosaicForMobileParticipantParams() {
		$obj = new AddMosaicParticipantParams(array(
				"confId"=>$this->requestProxy->confId,
				"mosaicId"=>VIDEO_FOR_MOBILE,
// 				"num"=>0,
				"partId"=>$this->requestProxy->partId));
		return $obj->getParams();
	}
	
	public function getAddFlashMosaicForMobileParticipantParams() {
		$obj = new AddMosaicParticipantParams(array(
				"confId"=>$this->requestProxy->confId,
				"mosaicId"=>VIDEO_FOR_MOBILE,
// 				"num"=>1,
				"partId"=>$this->requestProxy->partId));
		return $obj->getParams();
	}
	
	public function addMosaicParticipant($params) {
		$this->mcuControllerProxy->addMosaicParticipant($params);
	}
	
	private function getAddSidebarForFlashParticipantParams() {
		$obj = new AddSidebarParticipantParams(array(
		            "confId"	=> $this->requestProxy->confId,
		            "partId"	=> $this->requestProxy->partId,
					"sidebarId"	=> AUDIO_FOR_FLASH));
		return $obj->getParams();
	}
	
	private function getAddSidebarForMobileParticipantParams() {
		$obj = new AddSidebarParticipantParams(array(
		            "confId"	=> $this->requestProxy->confId,
		            "partId"	=> $this->requestProxy->partId,
					"sidebarId"	=> AUDIO_FOR_MOBILE));
		return $obj->getParams();
	}
	
	private function addSidebarParticipantForFlash() {
		$params = $this->getAddSidebarForFlashParticipantParams();
		$this->mcuControllerProxy->addSidebarParticipant($params);
	}
	
	public function addSidebarParticipantForMobile() {
		$params = $this->getAddSidebarForMobileParticipantParams();
		$this->mcuControllerProxy->addSidebarParticipant($params);
	}
	
	public function addSidebarParticipant() {
		$this->addSidebarParticipantForFlash();
		
		$this->addSidebarParticipantForMobile();
	}
	
	public function checkReservationPassword() {
		$reservationRecord = $this->meetingProxy->getReservationRecordByMeetingKey($this->meetingRecord["meeting_ticket"]);
		if ($reservationRecord["reservation_pw"] && 1 == $reservationRecord["reservation_pw_type"])
			throw new Exception("reuquired password.");
	}

	public function rejectParticipant() {
		$this->mcuControllerProxy->rejectParticipant($this->requestProxy->confId, $this->requestProxy->partId);
	}
	
	public function removeParticipant() {
		$this->mcuControllerProxy->removeParticipant($this->requestProxy->confId, $this->requestProxy->partId);
	}
	
	public function removeMosaicParticipant($params) {
		$this->mcuControllerProxy->removeMosaicParticipant($params);
	}
	
	/**
	 * polycom２りによる会議開始かどうかの判別
	 * @return boolean
	 */
	public function isStartConferenceByVideoConferenceParticipant() {
		$list = $this->conferenceProxy->getActiveConferenceParticipantListByDid($this->requestProxy->did);
		if (count($list) != 2)
			return false;
		foreach ($list as $key => $value) {
			if (0 != strcasecmp($value["participant_type"], "SIP"))
				return false;
		}
		return true;
	}
	
	public function startConferenceWithoutFlashClient() {
		$this->meetingProxy->startMeeting($this->conferenceRecord["meeting_key"]);
		
		$this->meetingProxy->updateConferenceClientUseCount($this->conferenceRecord["meeting_key"]);
	}
	
	public function setParticipantBackground() {
		$this->mcuControllerProxy->setParticipantBackground($this->requestProxy->confId, $this->requestProxy->partId);
	}
	
	/**
	 * 
	 */
	public function setTelephoneClientBackground() {
		$this->mcuControllerProxy->setTelephoneClientBackground($this->requestProxy->confId, $this->requestProxy->partId);
	}
	
	/**
	 * 
	 */
	public function setVideoMute() {
		$this->mcuControllerProxy->setVideoMute($this->requestProxy->confId, $this->requestProxy->partId, true);
	}
	
	public function setFlashMosaicForSales() {
		$params = $this->mosaicController->getSalesConferenceFlashMosaicSlotParams($this->requestProxy->confId, $this->requestProxy->partId);
		$this->mcuControllerProxy->setMosaicSlot($params);
	}
	
	public function setMobileMosaicForSales() {
		$params = $this->mosaicController->getSalesConferenceMosaicMosaicSlotParams($this->requestProxy->confId, $this->requestProxy->partId);
		$this->mcuControllerProxy->setMosaicSlot($params);
	}
	
	public function noticeParticipantCreated() {
		$this->noticeParticipantListChanged();
	}
}
