<?php
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");

require_once(dirname(__FILE__)."/../logic/MCUConferenceController.php");
require_once(dirname(__FILE__)."/../model/ConferenceProxy.php");

class FixMediaController extends MCUConferenceController {
	private $mcuGatewayProxy;
	private $conferenceProxy;
	function __construct() {
		parent::__construct();
		
		$this->mcuGatewayProxy	= new McuGatewayProxy();
		$this->conferenceProxy	= ConferenceProxy::getInstance();
	}
	
	public function getMobileMediaMixerByAvailableParticipantCount($mcuKey) {
		$mixer = $this->mcuGatewayProxy->getMobileMixerRecordByMcuServerKey($mcuKey);
		if (1 == count($mixer))
			return $mixer[0];
		else if (1 <= count($mixer)) {
			return $this->getMixer($mixer);
		}
		else
			throw new Exception(__FUNCTION__.": check mcu server key: ".$mcuKey);
		return $this->getMixer($mixer);
	}
	
	public function getConferenceMediaMixerByAvailableParticipantCount($mcuKey) {
		$mixer = $this->mcuGatewayProxy->getVideoConferenceMixerRecordByMcuServerKey($mcuKey);
		if (1 == count($mixer))
			return $mixer[0];
		else if (1 <= count($mixer)) {
			return $this->getMixer($mixer);
		}
		else
			throw new Exception(__FUNCTION__.": check mcu server key: ".$mcuKey);
	}
	
	private function getMixer($mixer) {
		$minimumServer = array();
		foreach ($mixer as $key => $value) {
			$value["count"] = $this->conferenceProxy->getActiveParticipantCountByMixerKey($value["media_mixer_key"]);
			if ($minimumServer)
				$minimumServer = ($minimumServer["count"] < $value["count"])
				? $minimumServer
				: $value;
			else
				$minimumServer= $value;
		}
		return $minimumServer;
	}
}
