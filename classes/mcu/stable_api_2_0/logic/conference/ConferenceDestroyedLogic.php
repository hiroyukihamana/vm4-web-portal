<?php
require_once("classes/mcu/config/McuConfigProxy.php");
 
require_once(dirname(__FILE__)."/../../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../../model/MeetingProxy.php");
require_once(dirname(__FILE__)."/../../model/ConferenceProxy.php");
require_once(dirname(__FILE__)."/../../params/GetConferenceParams.php");

class ConferenceDestroyedLogic {
	private $logger;
	private $requestProxy;
	
	// record
	public $conferenceRecord;
	public $conferenceParticipantRecordList;
	public $settingRecord;
	
	// irregal
	public $irregal = false;
	
	function __construct($model) {
		$this->config	= new McuConfigProxy();
		$this->logger = EZLogger2::getInstance();

		$this->mcuControllerProxy	= new MCUControllerProxy();
		$this->meetingProxy			= new MeetingProxy();
		$this->requestProxy			= $model;
		$this->videoConferenceProxy	= ConferenceProxy::getInstance();
	}

	// IConference
	public function destroyConference() {
		$this->mcuControllerProxy->removeConferenceByConfId($this->requestProxy->confId);
	}
	
	public function setConferenceRecord() {
		$conferenceRecord = $this->videoConferenceProxy->getConferenceByConfId($this->requestProxy->confId);
		if (!$conferenceRecord) {
			throw new Exception("no conference record. confId: ".$this->requestProxy->confId);
		}
// 		else if (count($conferenceRecord) > 1) {
// 			$this->logger->warn("irregal conference record. confId:".$this->requestProxy->confId);
// 			$this->irregal = true;
// 		}
		$this->conferenceRecord = $conferenceRecord;
	}
	
	public function removeConferenceRecord() {
		$this->videoConferenceProxy->clearConferenceRecordByDid($this->conferenceRecord["did"]);
	}
    // IConference

	// IConferenceParticipant
	public function clearConferenceParticipantRecordByDid() {
		$this->videoConferenceProxy->clearConferenceParticipantRecordByDid($this->conferenceRecord["did"]);
	}
	
	public function clearCallingParticipantRecord() {
		$this->videoConferenceProxy->clearCallingParticipantRecordByDid($this->conferenceRecord["did"]);
	}
	
	public function setConferenceParticipantListByDid() {
		$this->conferenceParticipantRecordList = $this->videoConferenceProxy->getActiveConferenceParticipantListByDid($this->conferenceRecord["did"]);
	}
	// IConferenceParticipant
	
	//IParticipant
	public function clearParticipantRecord() {
		$list = $this->videoConferenceProxy->getActiveConferenceSipTypeParticipantListByDid($this->conferenceRecord["did"]);
		$this->meetingProxy->clearParticipantByConferenceParticipantList($list);
	}
	//IParticipant
	
	public function stopRenderingInSlot() {
		$this->mcuControllerProxy->stopRenderingInSlotForVideoParticipant($this->requestProxy->confId, 1, 0);		
	}
}
