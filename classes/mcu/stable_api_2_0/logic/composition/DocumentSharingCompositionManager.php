<?php 
require_once(dirname(__FILE__)."/../../logic/composition/CompositionManager.php");

class DocumentSharingCompositionManager extends CompositionManager {
	public function run() {
		$this->setCompositionTypeForConferenceParticipant();
		
// 		$this->setCompositionTypeForMobileParticipant();
	}
	
	protected function setCompositionTypeForConferenceParticipant() {
		$compositionType = $this->getVideoParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_CONFERENCE,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("polycomProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	public function getVideoParticipantCompositionType() {
		return ONE_X_ONE;
	}
}
