<?php
require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../../logic/MCUConferenceController.php");
require_once(dirname(__FILE__)."/../../params/SetCompositionTypeParams.php");

class CompositionManager extends MCUConferenceController {
	protected $totalCount;
	protected $countHardwareParticipant;
	protected $confId;
	protected $activeSpeaker;
	protected $mobile4x4;
	
	public function __construct($confId, $totalCount, $countHardwareParticipant, $activeSpeaker=true, $mobile4x4=false) {
		parent::__construct();
		
		$this->count = $totalCount;
		$this->countHardwareParticipant	= $countHardwareParticipant;
		$this->confId = $confId;
		$this->activeSpeaker = $activeSpeaker;
		$this->mobile4x4 = $mobile4x4;
	}
	
	public function run() {
		
		$this->setCompositionTypeForConferenceParticipant();
		
		$this->setCompositionTypeForMobileParticipant();
		
		$this->setCompositionTypeForFlashParticipant();
	}
	
	protected function setCompositionTypeForConferenceParticipant() {
		$compositionType = $this->getVideoParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_CONFERENCE,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("polycomProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	protected function setCompositionTypeForMobileParticipant() {
		$compositionType = $this->getMobileParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_MOBILE,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("mobileProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	protected function setCompositionTypeForFlashParticipant() {
		$compositionType = $this->getFlashParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_FLASH,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("flashProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	
	private function getFlashParticipantCompositionType() {
		if ($this->activeSpeaker) {
			$compositionType = ONE_X_ONE;
		}
		else {
			if (1 == $this->countHardwareParticipant) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->countHardwareParticipant && $this->countHardwareParticipant <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $this->countHardwareParticipant) {
				$compositionType = THREE_X_THREE;
			}
			else {
				// 				return false;
// 				throw new Exception("check participant number.");
				$compositionType = ONE_X_ONE;
			}
		}
		return $compositionType;
	}
	
	protected function getMobileParticipantCompositionType() {
		if ($this->mobile4x4) {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->count && $this->count <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $this->count && $this->count <= 9) {
				$compositionType = THREE_X_THREE;
			}
			else if ($this->count > 9) {
				$compositionType = FOUR_X_FOUR;
			}
		}
		else {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else {
				$compositionType = TWO_X_TWO;
			}
		}
		return $compositionType;
	}
	
	public function getVideoParticipantCompositionType() {
		if ($this->activeSpeaker) {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (2 == $this->count) {
				$compositionType = ONE_PLUS_ONE;
			}
			else if (3 <= $this->count && $this->count <= 6) {
				$compositionType = ONE_PLUS_FIVE;
			}
			else {
				$compositionType = ONE_PLUS_SEVEN;
			}
		}
		else {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->count && $this->count <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $this->count) {
				$compositionType = THREE_X_THREE;
			}
		}
		return $compositionType;
	}
}
