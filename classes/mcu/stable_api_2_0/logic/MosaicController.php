<?php 
require_once(dirname(__FILE__)."/../logic/MCUConferenceController.php");
require_once(dirname(__FILE__)."/../params/SetMosaicSlotParams.php");

class MosaicController extends MCUConferenceController {	
	public function getInitConferenceMosaicSlotParams($confId, $activeSpeaker=true) {
		return $this->getSetMosaicSlotParams($confId, 0, $activeSpeaker);
	}
	
	public function getFlashMosaicSlotParams($confId, $activeSpeaker=true) {
		return $this->getSetMosaicSlotParams($confId, 1, $activeSpeaker);
	}
	
	public function getMobileMosaicSlotParams($confId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"num"=>0,
				"partId"=>0));
		return $obj->getParams();
	}
	
	private function getSetMosaicSlotParams($confId, $mosaicId, $activeSpeaker = true) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>$mosaicId,
				"num"=>0,
				"partId"=>((0 != $mosaicId) && $activeSpeaker) ? -2 : 0));
		return $obj->getParams();
	}
	
	public function getDefaultMosaicSlotParamsToActivateVadSlot($confId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>0,
				"num"=>0,
				"partId"=>-2));
		return $obj->getParams();
	}

	public function getDefaultMosaicSlotParamsToDeActivateVadSlot($confId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>0,
				"num"=>0,
				"partId"=>0));
		return $obj->getParams();
	}
	
	public function getMobileMosaicSlotParamsToActivateVadSlot($confId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"num"=>0,
				"partId"=>-2));
		return $obj->getParams();
	}
	
	public function getMobileMosaicSlotParamsToDeActivateVadSlot($confId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"num"=>0,
				"partId"=>0));
		return $obj->getParams();
	}
	
	public function getSalesConferenceFlashMosaicSlotParams($confId, $partId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"num"=>0,
				"partId"=>$partId));
		return $obj->getParams();
	}
	
	public function getSalesConferenceMosaicMosaicSlotParams($confId, $partId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"num"=>1,
				"partId"=>$partId));
		return $obj->getParams();
	}
}
