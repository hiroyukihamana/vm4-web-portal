<?php 
require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../params/SetCompositionTypeParams.php");

class MCUConferenceController {
	protected $mcuControllerProxy;
	protected $configProxy;
	protected $logger;
	
	function __construct() {
		$this->configProxy 	= new McuConfigProxy();
		$this->mcuControllerProxy	= new MCUControllerProxy();
		$this->logger = EZLogger2::getInstance();
	}
}