<?php

require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/ACreatedParticipant.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/conference/ICreateConference.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/interface/meeting/IMeeting.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/create/client/vcube/viewer/IViewer.php");

class FlashViewer extends ACreatedParticipant implements ICreateConference, IMeeting, IViewer {
	public function execute() {
		$this->setConferenceRecord();
		if (!$this->logic->conferenceRecord)
			throw new Exception("no conference recrod. confId: ".$this->logic->requestProxy->confId);
		
		$this->setMeetingRecord();
		if (!$this->logic->meetingRecord) 
			throw new Exception("no meeting recrod. confId: ".$this->logic->requestProxy->confId);
		
		$this->checkBlocked();
		
		$this->acceptParticipant();
		
		$this->changeParticipantProfile();
			
		$this->removeMosaicParticipant();
	}
	
	// ICreateConference
	public function removeConferenceRecord() {
		$this->logic->removeConferenceRecord();
	}
	
	public function setConferenceRecord() {
		$this->logic->setConferenceRecord();
	}
	// ICreateConference
	
	// IMeeting
	public function checkBlocked() {
		if ($this->logic->meetingRecord["is_blocked"]) {
			throw new Exception("meeting is blocked.");
		}
	}
	public function createMeetingRecord() {}
	
	public function setMeetingRecord() {
		$this->logic->setMeetingRecordByMeetingKey();
		
	}
	// IMeeting
	
	// IRoom
	public function setRoomRecord() {
	}
	// IRoom
	
	// ISetting
	public function setSettingRecord() {
		$this->logic->setSettingRecordByDid();
	}
	// ISetting

	public function changeParticipantProfile() {
		$params = $this->logic->getChangeProfileParamsForFlashParticipant();
		$this->logic->changeParticipantProfile($params);
	}

	public function acceptParticipant() {
		$params = $this->logic->getAcceptParamsForFlashParticipant();
		$this->logic->acceptParticipant($params);
	}

	public function removeMosaicParticipant() {
		$params = $this->logic->getRemoveMosaicViewerParticipantParams();
		$this->logic->removeMosaicParticipant($params);
	}
}
	