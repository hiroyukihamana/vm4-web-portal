<?php

require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/video/AConferenceParticipant.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/initialize/IIMeeting.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/initialize/IIRoom.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/initialize/IISetting.php");

class VideoParticipantFromTemporaryDid extends AConferenceParticipant implements IIMeeting {
	public function __construct($model) {
		parent::__construct($model);
	}
	
	protected function initialize($model) {
		parent::initialize($model);
		
		$this->setIMeetingRecord();
	}
	// IIMeeting
	public function checkBlocked() {}
	
	public function setIMeetingRecord() {$this->logic->setIMeetingRecord();}
	// IIMeeting
	
	public function execute() {
		$this->setConferenceRecord();
		if (!$this->logic->conferenceRecord)
			throw new Exception("no conference record.");
		
		try {	
			$this->setParticipantRecord();
		
			$this->checkParticipatedNumber();
				
			$this->setConferenceRecord();
				
			$this->acceptParticipant();
		}
		catch (Exception $e) {
			$this->reject();
			throw $e;
		}
		
		$this->createParticipantRecord();
		
		$this->createConferenceParticipantRecord();

		$this->logic->noticeParticipantCreated();
		
		$this->logic->controlActivateVadSlot();
		
		$this->changeParticipantProfile();
		
		$this->setCompositionType();
			
		$this->addMosaicParticipant();
			
		$this->addSidebarParticipant();
	}
}
