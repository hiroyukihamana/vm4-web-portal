<?php
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/ACreatedParticipant.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/initialize/IIMeeting.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/initialize/IIRoom.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/initialize/IISetting.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/conference/ICreateConference.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/conference/ICreateConferenceParticipant.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/meeting/ICreateParticipant.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/video/IVideoParticipant.php");

abstract class AConferenceParticipant extends ACreatedParticipant implements IIRoom, IISetting, ICreateConference, ICreateConferenceParticipant, ICreateParticipant, IVideoParticipant {
	protected function initialize($model, $bypass = true) {
		parent::initialize($model);
		if (!$bypass) return;
		
		try {
			$this->setISettingRecord();
			$this->setIRoomRecord();
		}
		catch (Exception $e) {
			$this->reject();
			throw $e;
		}
	}
	
	// initialize method list.
	public function setIRoomRecord() {$this->logic->setIRoomRecordByRoomKey();}
	public function setISettingRecord() {$this->logic->setISettingRecord();}

	// ICreateConference
	public function removeConferenceRecord() {
		$this->logic->removeConferenceRecord();
	}
	
	public function setConferenceRecord() {
		$this->logic->setConferenceRecord();
	}
	
	public function createConferenceRecord() {}
	
	public function setConferenceParticipantRecord() {
		$this->logic->setConferenceParticipantRecord();
	}
	// ICreateConference
	
	// ICreateConferenceParticipant
	public function removeConferenceParticipantRecord() {
		$this->logic->removeConferenceParticipantRecord();
	}
	
	public function createConferenceParticipantRecord() {
		$this->logic->createConferenceParticipantRecord();
		/**
		 * polycom２理
		 */
		if ($this->logic->isStartConferenceByVideoConferenceParticipant()) {
			$this->logic->startConferenceWithoutFlashClient();
		}
	}
	// IConference
	
	// ICreateParticipant
	public function checkParticipatedNumber() {
		if (!$this->logic->checkParticipatedNumber())
			throw new Exception("check contract number. did:".$this->logic->requestProxy->did);
	}
	
	public function removeParticipantRecord() {
		$this->logic->removeParticipantRecord();
	}
	
	public function createParticipantRecord() {
		$this->logic->createParticipantRecord();
	}
	
	public function setParticipantRecord() {
		$this->logic->setParticipantRecord();
	}
	// ICreateParticipant
	
	public function setCompositionType() {
		$this->logic->setCompositionType();
	}
	
	public function changeParticipantProfile() {
		$params = $this->logic->getChangeVideoParticipantProfileParams();
		$this->logic->changeParticipantProfile($params);
	}
	
	public function acceptParticipant() {
		$params = $this->logic->getAcceptVideoParticipantParams();
		$this->logic->acceptParticipant($params);
	}
	
	public function addMosaicParticipant() {
		$params = $this->logic->getAddMosaicForFlashParticipantParams();
		$this->logic->addMosaicParticipant($params);

		$params = $this->logic->getAddMosaicForMobileParticipantParams();
		$this->logic->addMosaicParticipant($params);
	}
	
	public function addSidebarParticipant() {
		$this->logic->addSidebarParticipant();
	}
}
