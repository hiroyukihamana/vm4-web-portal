<?php

require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/interface/meeting/IMeeting.php");
require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/video/AConferenceParticipant.php");

class VideoParticipant extends AConferenceParticipant implements IMeeting {
	// 
	public function execute() {
		try {
			$this->setMeetingRecord();
			if ($this->logic->meetingRecord["is_reserved"]) {
				$this->logic->checkReservationPassword();
			}
	
			$this->checkBlocked();
			
			$this->setParticipantRecord();
				
			$this->checkParticipatedNumber();
			
			$this->setConferenceRecord();
			
			$this->acceptParticipant();
		}
		catch (Exception $e) {
			$this->reject();
			throw $e;
		}
		
		$this->createParticipantRecord();
		
		$this->createConferenceParticipantRecord();
		
		$this->logic->noticeParticipantCreated();

		$this->logic->controlActivateVadSlot();
		
		$this->changeParticipantProfile();

		$this->setCompositionType();
		
		$this->addMosaicParticipant();
		
		$this->addSidebarParticipant();
	}
	
	// IMeeting
	public function checkBlocked() {
		if (!$this->logic->meetingRecord) {
			throw new Exception("irregal meeting record.");
		}
		if ($this->logic->meetingRecord["is_blocked"]) {
			throw new Exception("meeting is blocked. meeting_key:".$this->logic->meetingRecord["meeting_key"]);
		}
	}
	
	public function createMeetingRecord() {
	}
	
	public function setMeetingRecord() {
		$this->logic->setMeetingRecordByRoomRecord();
	}
	// IMeeting
}
