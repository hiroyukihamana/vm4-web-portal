<?php 

require_once(dirname(__FILE__)."/../../../../../controller/participant/create/client/vcube/flash/IFlashParticipant.php");

interface IVideoParticipant extends IFlashParticipant {
	function addMosaicParticipant();
	function addSidebarParticipant();	
}
