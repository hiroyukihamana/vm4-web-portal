<?php

interface ICreateConferenceParticipant {
	function createConferenceParticipantRecord();
	function removeConferenceParticipantRecord();	// 実装の必要の有無について要検討
	function setConferenceParticipantRecord();
}