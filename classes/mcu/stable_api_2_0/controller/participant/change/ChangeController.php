<?php

require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");

require_once(dirname(__FILE__)."/../../../controller/participant/change/client/CalledParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/change/client/ChangeStateParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/change/client/FailedCalleeParticipant.php");

require_once(dirname(__FILE__)."/../../../model/do/State.php");
require_once(dirname(__FILE__)."/../../../logic/participant/ParticipantStateChangeLogic.php");

class ChangeController {
	static function create($model) {
		$configProxy 	= new McuConfigProxy();
		$dsn = $configProxy->getDsn();
		$logger = EZLogger2::getInstance();

		try {
			$logic = new ParticipantStateChangeLogic($model);
			$logic->setConferenceParticipantRecord();
		}
		catch(Exception $e) {
			$logger->info($e->getMessage());
			return null;
		}
		
		if (!$logic->conferenceParticipant["participant_key"] &&
			!$logic->conferenceParticipant["is_active"] &&
			$model->partState == State::CONNECTED()) {
			return new CalledParticipant($logic);
		}
		if ($logic->conferenceParticipant["participant_name"] == $configProxy->get("callout_name") &&
			!$logic->conferenceParticipant["is_active"] &&
			($model->partState == State::ERROR() ||
			$model->partState == State::TIMEOUT() ||
			$model->partState == State::DISCONNECTED())) {
			ConferenceLogger::info(__CLASS__."::".__FUNCTION__, $model->confId, $model->partId, null, null, null, $model);
			ConferenceLogger::info(__CLASS__."::".__FUNCTION__, $model->confId, $model->partId, null, null, null, $logic->conferenceParticipant);
			return new FailedCalleeParticipant($logic);
		}
		else
			return new ChangeStateParticipant($logic);
	}
}
