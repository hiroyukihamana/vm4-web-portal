<?php
require_once(dirname(__FILE__)."/../../../../logic/participant/ParticipantStateChangeLogic.php");

require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");

class ChangeStateParticipant {
	protected $logger;
	protected $logic;

	public function __construct($logic) {
		$this->logger = EZLogger2::getInstance();

		$this->logic = $logic;

		try {
// 			$this->logic->setConferenceParticipantRecord();
		}
		catch (Exception $e) {
			$this->logger->warn($e->getMessage());
			$this->reject();
		}
	}
		
	protected function reject() {
		$this->logic->removeParticipant();
	}

	protected function changeState() {
		$this->logic->changeState();
	}
	
	public function execute() {
		$this->changeState();
	}
}
