<?php 
require_once(dirname(__FILE__)."/../../../../controller/participant/change/client/ChangeStateParticipant.php");

class CalledParticipant extends ChangeStateParticipant {
	public function execute() {
		$this->logic->changeState();
		
		try {
			$this->logic->setConferenceActiveRecordByConfId();
			
			$this->logic->setMeetingRecordByMeetingKey();

			$this->checkBlocked();

			$this->logic->setRoomRecord();

			$this->logic->setSettingRecord();
			
			$this->logic->setParticipantRecordList();
			
			$this->checkParticipatedNumber();

			$this->logic->createParticipantRecord();
			
			$this->logic->activateVideoParticipantRecord();
			
			$this->logic->noticeCalleeStatus();
		}
		catch (Exception $e) {
			$this->reject();
			throw $e;
		}
			
		if ($this->logic->settingRecord["use_active_speaker"] && $this->logic->getParticipatedCount() == 3) {
			$this->logic->controlActivateVadSlot();
		}
		
		$this->changeParticipantProfile();
			
		$this->logic->setCompositionType();
		
		$this->addMosaicParticipant();
		
		$this->addSidebarParticipant();
	}
	
	public function checkBlocked() {
		if (!$this->logic->meetingRecord) {
			throw new Exception("irregal meeting record.");
		}
		if ($this->logic->meetingRecord["is_blocked"]) {
			throw new Exception("meeting is blocked. meeting_key:".$this->logic->meetingRecord["meeting_key"]);
		}
	}

	public function checkParticipatedNumber() {
		if (!$this->logic->checkParticipatedNumber())
			throw new Exception("check contract number. confId:".$this->logic->requestProxy->confId);
	}
	
	public function changeParticipantProfile() {
		$params = $this->logic->getChangeVideoParticipantProfileParams();
		$this->logic->changeParticipantProfile($params);
	}
	
	public function addMosaicParticipant() {
		$params = $this->logic->getAddMosaicForFlashParticipantParams();
		$this->logic->addMosaicParticipant($params);
		
		$params = $this->logic->getAddMosaicForMobileParticipantParams();
		$this->logic->addMosaicParticipant($params);
	}
	
	public function addSidebarParticipant() {
		$this->logic->addSidebarParticipant();
	}
}
