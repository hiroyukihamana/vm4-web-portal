<?php 
require_once(dirname(__FILE__)."/../../../../../../controller/participant/destroy/client/ADestroyedParticipant.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/destroy/client/interface/conference/IDestroyedConference.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/destroy/client/interface/conference/IDestroyedConferenceParticipant.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/destroy/client/interface/meeting/IDestroyedMeeting.php");
require_once(dirname(__FILE__)."/../../../../../../controller/participant/destroy/client/interface/meeting/IDestroyedParticipant.php");

class DestroyedFlashParticipant extends ADestroyedParticipant implements IDestroyedConference, IDestroyedConferenceParticipant, IDestroyedMeeting, IDestroyedParticipant {

	// IConference
	function removeConferenceRecord() {
	
	}
	function setConferenceRecord() {
		$this->logic->setConferenceRecord();
	}
	// IConference
	
	// IConferenceParticipant
	function removeConferenceParticipantRecord() {
		$this->logic->removeConferenceParticipantRecord();
	}
	
	function setConferenceParticipantRecord() {
		$this->logic->setConferenceParticipantRecord();
	}
	
	function setConferenceParticipantList() {
		$this->logic->setConferenceParticipantList();
	}
	// IConferenceParticipant
	
	// IMeeting
	function removeMeetingRecord() {
	
	}
	
	function setMeetingRecord() {
		$this->logic->setMeetingRecord();
	}
	// IMeeting
	
	// IParticipant
	function removeParticipantRecord() {
		$this->logic->removeParticipantRecord();
	}
	
	function setParticipantRecord() {
		$this->logic->setParticipantRecord();
	}
	// IParticipant
	
	// IDestroyedSetting
	function setSettingRecord() {
		$this->logic->setSettingRecordByDid();
	}
	// IDestroyedSetting
	
	public function execute() {
		$this->setConferenceParticipantList();
	
		$this->setConferenceParticipantRecord();
	
		$this->setConferenceRecord();
	
		$this->setSettingRecord();
	
		$this->setMeetingRecord();
		
		$this->logic->setRoomRecordByRoomKey();
	
		if ($this->logic->conferenceParticipantRecord) {
			$this->removeConferenceParticipantRecord();
		}
	
		//再度取得
		$this->setConferenceParticipantList();
		
		// Conference参加者が０の場合Destroyして終了
		if (!$this->logic->isActiveConferenceOnMCU())
			return;
		
		if (0 == $this->logic->getParticipantNumberOnMCU()) {
			$this->logic->destroyConference();
		}
		else {
			$this->logic->controlDeActivateVadSlot();
		}
		
		($this->hasMeetingParticipant($this->logic->conferenceParticipantRecordList))
		? $this->meetingWithFMSInsctance()
		: $this->meetingWithoutFMSInsctance();
	}
	
	private function hasMeetingParticipant($list) {
		foreach ($list as $key => $value) {
			if ($value["participant_type"] == "WEB")
				return true;
		}
		return false;
	}
	
	private function meetingWithFMSInsctance() {
		$this->logic->setCompositionType();
	}
	
	private function meetingWithoutFMSInsctance() {
// 		if (0 == count($this->logic->conferenceParticipantRecordList)) {
// 			$this->logic->stopMeeting();
// 		}
// 		else if (1 == count($this->logic->conferenceParticipantRecordList)) {
// 			$this->logic->stopCount();
// 		}
// 		else {
		$this->logic->stopDocumentSharing();
		
		$this->logic->setCompositionType();
// 		}
	}
}
