<?php 

require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../../../../logic/participant/ParticipantDestroyedLogic.php");

abstract class ADestroyedParticipant {
	protected $logger;
	protected $logic;
	
	abstract public function execute();
	
	public function __construct($model) {
		$this->logger = EZLogger2::getInstance();
		$this->initialize($model);
	}
	
	protected function initialize($model) {
		$this->setLogic($model);
	}
	
	protected function setLogic($model) {
		$this->logic = new ParticipantDestroyedLogic($model);
	}
}
