<?php 

interface IDestroyedParticipant {
	function removeParticipantRecord();
	function setParticipantRecord();
}