<?php 
interface IDestroyedConference {
	function removeConferenceRecord();
	function setConferenceRecord();
}
