<?php 
require_once(dirname(__FILE__)."/../model/ConferenceProxy.php");
require_once(dirname(__FILE__)."/../model/DocumentProxy.php");
require_once(dirname(__FILE__)."/../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../model/MeetingProxy.php");

require_once(dirname(__FILE__)."/../logic/FMSNetConnectionController.php");

class ConferenceController {
	protected $config;
	protected $logger;
	protected $dsn;
	
	// proxy
	protected $conferenceProxy;
	protected $mcuControllerProxy;
	protected $mcuGatewayProxy;
	protected $meetingProxy;
	protected $documentProxy;

	public function __construct() {
		$this->configProxy 	= new McuConfigProxy();
		$this->dsn = $this->configProxy->getDsn();
		$this->logger = EZLogger2::getInstance();
		
		$this->conferenceProxy		= ConferenceProxy::getInstance();
		$this->documentProxy		= new DocumentProxy();
		$this->mcuControllerProxy	= new MCUControllerProxy();
		$this->meetingProxy			= new MeetingProxy();
	}
	
	public function getActiveVideoConferenceByMeetingKey ($meetingKey) {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		return $conferenceRecord;
	}
	
	/**
	 * onparticipantcreate時に渡されるsessionIdと付きあわせて
	 * participant recordと videoparticipantrecordをマッチングさせる
	 * @param int $participantKey
	 * @param string $sessionId
	 */
	public function addParticipantKey2VideoParticipantRecord($participantKey, $sessionId) {
		$this->conferenceProxy->updateParticipantKeyBySessionId($participantKey, $sessionId);
	}
	
	public function setVideoMute($meetingKey, $participantKey, $flag) {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			return;
		$videoParticipantRecord = $this->conferenceProxy->getActiveConferenceParticipantByParticipantKey($participantKey);
		if (!$videoParticipantRecord)
			return;
		$this->mcuControllerProxy->setVideoMute($conferenceRecord["conference_id"], $videoParticipantRecord["part_id"], $flag);
	}
	
	public function setMuteToTelephoneClient($meetingKey, $sessionId) {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			return;
		$videoParticipantRecord = $this->conferenceProxy->getActiveConferenceParticipantBySessionId($sessionId);
		if (!$videoParticipantRecord)
			return;
		
		$this->mcuControllerProxy->setTelephoneClientBackground($conferenceRecord["conference_id"], $videoParticipantRecord["part_id"]);
		
		$this->mcuControllerProxy->setVideoMute($conferenceRecord["conference_id"], $videoParticipantRecord["part_id"], true);
	}
	
	public function render($conferenceRecord, $documentRecord) {
		$this->mcuControllerProxy->startRenderingInSlotForVideoParticipant($conferenceRecord["conference_id"], 1, 1);
	}
	
	public function forceExitPolycomClients($meetingKey) {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		$list = $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByMeetingKey($meetingKey);
		if (count($list) <= 0 || !$conferenceRecord)
			return;
		foreach ($list as $value) {
			$this->mcuControllerProxy->removeParticipant($conferenceRecord["conference_id"], $value["part_id"]);
		}
	}
}
