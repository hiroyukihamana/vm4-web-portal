<?php 

interface IConferenceParticipant {
	function clearConferenceParticipantRecord();
	function setConferenceParticipantList();
}