<?php 

interface IConference {
	function destroyConference();
	function removeConferenceRecord();
	function setConferenceRecord();
}
