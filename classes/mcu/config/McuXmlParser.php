<?php
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/config/McuServerModel.php");
require_once("classes/mcu/config/WebApiModel.php");

function f ($key, $value, $list) {
	$result = array();
	foreach ($list as $api) {
		if ($api->$key == $value) {
			$result[] = $api;
		}
	}
	return $result;
};

class McuXmlParser {
	private $xml;
	private $mcuServerList = array();
	private $webApiList = array();
	function __construct() {
		$this->logger = EZLogger2::getInstance();
		
		$this->xml = simplexml_load_file(N2MY_MCU_SERVER_XML);
		foreach ($this->xml->{'web-api-list'}->web as $web) {
			$this->webApiList[] = new WebApiModel($web);
		}
	}
	
	public function getWebApiModel($condition) {
		$list = $this->webApiList;
		foreach ($condition as $key => $value) {
			$list = call_user_func_array("f", array($key, $value, $list));
		}
		if (1 == count($list)) {
			return $list[0]->getContents();
		}
		else {
			$this->logger->info($list);
		}
	}
	
	
	public function getLatestWebApiModel() {
		$list = $this->webApiList;
		$returnModel = $list[0];
		for ($i = 0; $i < count($list);$i++) {
			if ($returnModel->aspVersion < $list[$i]->aspVersion)
				$returnModel = $list[$i];
		}
		return ($returnModel) ? $returnModel->getContents() : null;
	}
	
	public function getServerModel($domain) {
		foreach ($this->mcuServerList as $key => $value) {
			if ($domain == $value->controllerRegularDomain)
				return $value;
		}
		return null;
	}
	
	public function getListenerClassPathByServerDomain($domain) {
		$server = $this->getServerModel($domain);
		if ($server)
			return $server->listenerClassPath;
		return null;
	}
	
	public function getListenerClassNameByServerDomain($domain) {
		$server = $this->getServerModel($domain);
		if ($server)
			return $server->listenerClassName;
		return null;
	}
	
	public function getCreateClassPathByServerDomain($domain) {
		$server = $this->getServerModel($domain);
		if ($server)
			return $server->createClassPath;
		return null;
	}
	
	public function getCreateClassNameByServerDomain($domain) {
		$server = $this->getServerModel($domain);
		if ($server)
			return $server->createClassName;
		return null;
	}
	
	public function getMobileMixSupport($domain) {
		$server = $this->getServerModel($domain);
		if ($server)
			return $server->mobileMixSupport;
		return null;
	}
	
	public function getMobileControllerPath($domain) {
		$server = $this->getServerModel($domain);
		if ($server)
			return $server->mobileControllerPath;
		return null;
	}
	
	public function getMobileControllerClassName($domain) {
		$server = $this->getServerModel($domain);
		if ($server)
			return $server->mobileControllerClassName;
		return null;
	}
}
