<?php 

class WebApiModel {
	private $aspVersion;
	private $targetVersion;
	private $listenerClassPath;
	private $listenerClassName;
	private $createClassPath;
	private $createClassName;
	private $mobileMixSupport;
	private $mobileControllerPath;
	private $mobileControllerClassName;
	private $salesMobileControllerPath;
	private $salesMobileControllerClassName;
	private $stbMobileControllerPath;
	private $stbMobileControllerClassName;
	private $releaseDate;
	private $lastUpdate;
	function __construct($xml) {
		$this->aspVersion				= (string)$xml->aspVersion;
		$this->targetVersion			= (string)$xml->targetVersion;
		$this->listenerClassPath		= (string)$xml->listenerClassPath;
		$this->listenerClassName		= (string)$xml->listenerClassName;
		$this->createClassPath			= (string)$xml->createClassPath;
		$this->createClassName			= (string)$xml->createClassName;
		$this->mobileMixSupport			= (string)$xml->{'mobile-mix'}['support'];
		$this->mobileControllerPath		= (string)$xml->{'mobile-mix'}->mobileControllerPath;
		$this->mobileControllerClassName= (string)$xml->{'mobile-mix'}->mobileControllerClassName;
		$this->salesMobileControllerPath		= (string)$xml->{'mobile-mix'}->salesMobileControllerPath;
		$this->salesMobileControllerClassName= (string)$xml->{'mobile-mix'}->salesMobileControllerClassName;
		$this->stbMobileControllerPath		= (string)$xml->{'mobile-mix'}->stbMobileControllerPath;
		$this->stbMobileControllerClassName= (string)$xml->{'mobile-mix'}->stbMobileControllerClassName;
		$this->callOutSupport			= (string)$xml->callout['support'];
		$this->releaseDate				= (string)$xml->releaseDate;
		$this->lastUpdate				= (string)$xml->lastUpdate;
	}
	
	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		return null;
	}
	
	public function getContents() {
		return $data = array(
				"aspVersion"		=> $this->aspVersion,
				"targetVersion"		=> $this->targetVersion,
				"listenerClassPath"	=> $this->listenerClassPath,
				"listenerClassName"	=> $this->listenerClassName,
				"createClassPath"	=> $this->createClassPath,
				"createClassName"	=> $this->createClassName,
				"mobileMixSupport"	=> $this->mobileMixSupport,
				"mobileControllerPath"=>$this->mobileControllerPath,
				"mobileControllerClassName"=>$this->mobileControllerClassName,
				"salesMobileControllerPath"=>$this->salesMobileControllerPath,
				"salesMobileControllerClassName"=>$this->salesMobileControllerClassName,
				"stbMobileControllerPath"=>$this->stbMobileControllerPath,
				"stbMobileControllerClassName"=>$this->stbMobileControllerClassName,
				"releaseDate"		=> $this->releaseDate,
				"lastUpdate"		=> $this->lastUpdate
				);
	}	
}
