<?php
require_once("lib/EZLib/EZCore/EZConfig.class.php");

class McuConfig {
	private static $singleton;
	private $ezconfig;
	var $_config = array();
	
	function __construct($config_file) {
		$this->ezconfig = EZConfig::getInstance(N2MY_CONFIG_FILE);
		
		//check config_file
		if (!file_exists($config_file)) {
			trigger_error("#config_file not found -> $config_file", E_USER_ERROR);
		}
		if (!is_readable($config_file)) {
			trigger_error("#config_file not readable -> $config_file", E_USER_ERROR);
		}
		//parse ini_file
		$config = parse_ini_file($config_file, true);
	
		//set config
		if ($config && $config["MCU"]) {
			foreach ($config["MCU"] as $key => $value) {
				if (!$this->ezconfig->get("MCU", $key))
					$this->ezconfig->set("MCU", $key, $value);
			}
		} else {
			trigger_error("#unable to parse_config_file -> $config_file", E_USER_ERROR);
		}
	}
	
	public static function getInstance() {
		if (McuConfig::$singleton) {
			return McuConfig::$singleton;
		}
		McuConfig::$singleton = new McuConfig(N2MY_VC_CONFIG_FILE);
		return McuConfig::$singleton;
	}
	
	public function get($group, $key, $default = null)
	{
		return $this->ezconfig->get($group, $key, $default);
	}
	
	public function getAll($group, $default = null)
	{
		return $this->ezconfig->getAll($group, $default);
	}
	
	public function getGroups($default = null)
	{
		return $this->ezconfig->getGroups($default);
	}
	
	public function getGroupKeys($group, $default = null)
	{
		return $this->ezconfig->getGroupKeys($group, $default);
	}
	
	public function set($group, $key, $value, $create = true)
	{
		$this->ezconfig->set($group, $key, $value, $create);
	}
	
	public function setAll($group, $pair, $create = true)
	{
		$this->ezconfig->setAll($group, $pair, $create);
	}
}
