<?php 

define("VIDEO_FOR_CONFERENCE", 0);
define("VIDEO_FOR_FLASH", 1);
define("VIDEO_FOR_MOBILE", 2);
define("VIDEO_FOR_PGI", 3);

define("AUDIO_FOR_CONFERENCE", 0);
define("AUDIO_FOR_FLASH", 1);
define("AUDIO_FOR_MOBILE", 2);
define("AUDIO_FOR_PGI", 3);

define("ONE_X_ONE",		0);
define("TWO_X_TWO",		1);
define("THREE_X_THREE",	2);
define("THREE_PLUS_FOUR",3);
define("ONE_PLUS_SEVEN",4);
define("ONE_PLUS_FIVE", 5);
define("ONE_PLUS_ONE",	6);
define("PIP1",			7);
define("PIP3",			8);
define("FOUR_X_FOUR",	9);


define("REG_IP",	"(\d|[01]?\d\d|2[0-4]\d|25[0-5])\.(\d|[01]?\d\d|2[0-4]\d|25[0-5])\.(\d|[01]?\d\d|2[0-4]\d|25[0-5])\.(\d|[01]?\d\d|2[0-4]\d|25[0-5])");
define("REG_CALLOUT_NAME",	"[0-9a-zA-Z]*");
 