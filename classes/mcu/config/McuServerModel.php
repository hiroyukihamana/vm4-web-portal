<?php 

class McuServerModel {
	private $version;
	private $releasedate;
	private $controllerPort;
	private $controllerRegularDomain;
	private $controllerRegularNumberDomain;
	private $controllerGuestDomain;
	private $controllerGuestNumberDomain;
	private $controllerGuestH323Domain;
	private $controllerGuestH323NumberDomain;
	private $sipDomain;
	private $sipPort;
	private $socketDomain;
	private $socketPort;
	
	function __construct($xml) {
		$this->version	= (string)$xml->version;
		$this->releasedate	= (string)$xml->release->date;
		$this->controllerPort	= (string)$xml->controller->port;
		$this->controllerRegularDomain	= (string)$xml->controller->domain->regular->value;
		$this->controllerRegularNumberDomain	= (string)$xml->controller->domain->regular->simple;
		$this->controllerGuestDomain	= (string)$xml->controller->domain->guest->value;
		$this->controllerGuestNumberDomain	= (string)$xml->controller->domain->guest->simple;
		$this->controllerGuestH323Domain	= (string)$xml->controller->domain->guestH323->value;
		$this->controllerGuestH323NumberDomain	= (string)$xml->controller->domain->guestH323->simple;
		$this->sipDomain	= (string)$xml->sip->domain;
		$this->sipPort	= (string)$xml->sip->port;
		$this->socketDomain	= (string)$xml->socket->domain;
		$this->socketPort	= (string)$xml->socket->port;
	}

	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		return null;
	}
}
