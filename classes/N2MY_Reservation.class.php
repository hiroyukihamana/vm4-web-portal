<?php
require_once ("classes/N2MY_DBI.class.php");
require_once ("classes/dbi/user.dbi.php");
require_once ("classes/dbi/member.dbi.php");
require_once ("classes/dbi/room.dbi.php");
require_once ("classes/dbi/reservation.dbi.php");
require_once ("classes/dbi/reservation_user.dbi.php");
require_once ("classes/N2MY_Document.class.php");
require_once ("classes/mgm/MGM_Auth.class.php");
require_once ("lib/EZLib/EZUtil/EZDate.class.php");
require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
require_once ("lib/EZLib/EZHTML/EZTemplate.class.php");
require_once ("classes/dbi/address_book.dbi.php");
require_once ("classes/N2MY_Meeting.class.php");

class N2MY_Reservation {

    private $logger = "";
    private $dsn    = null;
    public  $obj_Reservation = null;
    public  $obj_ReservationUser = null;

    var $room_key         = "";
    var $db_host          = "";
    var $total_reservation_number = "";
    var $serverTimeZone   = N2MY_SERVER_TIMEZONE;
    var $obj_Document     = null;
    var $obj_N2MYDocument = null;
    var $obj_MGMAuthClass = null;
    var $session          = null;

    /**
     * コンストラクタ
     *
     * $db_hostは資料変換時にどうしてもDB名が必要だったためで、他ではまったく使わないので、後でなんとしても消す。
     */
    function __construct($dsn, $db_host = null) {
        $this->logger =& EZlogger2::getInstance();
        $this->dsn = $dsn;
        $this->db_host = $db_host;
        $this->obj_Reservation     = new ReservationTable($dsn);
        $this->obj_ReservationUser = new ReservationUserTable($dsn);
        $this->obj_N2MYDocument    = new N2MY_Document($dsn);
        $this->obj_MGMAuthClass    = new MGM_AuthClass(N2MY_MDB_DSN);
    }

    /**
     * 予約情報取得
     * start_time
     * end_time
     * reservation_name
     * sort_key
     * sort_type
     * limit
     * offset
     */
    function getList($room_key = null, $options = array(), $user_timezone = null,$checkPortOverArray = null) {
        // 検索オプション
        if (!$where = $this->_getWhere($room_key, $options)) {
            return array();
        }

        $limit  = isset($options["limit"])  ? $options["limit"]  : null;
        $offset = isset($options["offset"]) ? $options["offset"] : null;
        if (isset($options["sort_key"])) {
            $sort = array($options["sort_key"] => $options["sort_type"]);
        } else {
            $sort = array();
        }
        $reservations = $this->obj_Reservation->getList($where, $sort, $limit, $offset);
        if (DB::isError($reservations)) {
            $this->logger->error($reservations->getUserInfo());
            return false;
        }

        $reservation_list = array();
        // 一覧表示
        foreach ($reservations as $reservation_info){
            $reservation_info["port_over"] = "0";

            if (strtotime($reservation_info['reservation_starttime']) < strtotime("now")
             && strtotime($reservation_info['reservation_endtime'])   > strtotime("now") ) {
                $status = "now"; // 開催中
            } elseif (strtotime($reservation_info['reservation_endtime']) < strtotime("now")) {
                $status = "end"; // 終了
            } else {
                $status = "wait";// 開催待
            }
            $reservation_info["status"] = $status;
            if ($user_timezone !== "") {
                $reservation_info["reservation_starttime"] = EZDate::getLocateTime($reservation_info["reservation_starttime"],
                                                                                   $user_timezone, N2MY_SERVER_TIMEZONE);
                $reservation_info["reservation_endtime"]   = EZDate::getLocateTime($reservation_info["reservation_endtime"],
                                                                                   $user_timezone, N2MY_SERVER_TIMEZONE);
            }
            // 契約人数を超えている会議をチェック
            if ($checkPortOverArray){
                foreach ($checkPortOverArray as $port_over_info){
                    if ($reservation_info['reservation_starttime'] <= $port_over_info
                    &&  $reservation_info['reservation_endtime']   > $port_over_info ) {
                        $reservation_info["port_over"] = "1";
                        break;
                    }
                }
            }
            $reservation_list[] = $reservation_info;
        }
        return $reservation_list;
    }

  /**
   * V-Cube ONE 専用
   */
    function getList_for_ONE($room_key = null, $options = array(), $user_timezone = null) {
        // 検索オプション
        if (!$where = $this->_getWhere_for_ONE($room_key, $options)) {
            return array();
        }

        $limit  = isset($options["limit"])  ? $options["limit"]  : null;
        $offset = isset($options["offset"]) ? $options["offset"] : null;
        if (isset($options["sort_key"])) {
            $sort = array($options["sort_key"] => $options["sort_type"]);
        } else {
            $sort = array();
        }
        $reservations = $this->obj_Reservation->getList($where, $sort, $limit, $offset);
        if (DB::isError($reservations)) {
            $this->logger->error($reservations->getUserInfo());
            return false;
        }

        $reservation_list = array();
        // 一覧表示
        foreach ($reservations as $reservation_info){
            if (strtotime($reservation_info['reservation_starttime']) < strtotime("now")
             && strtotime($reservation_info['reservation_endtime'])   > strtotime("now") ) {
                $status = "now"; // 開催中
            } elseif (strtotime($reservation_info['reservation_endtime']) < strtotime("now")) {
                $status = "end"; // 終了
            } else {
                $status = "wait";// 開催待
            }
            $reservation_info["status"] = $status;
            if ($user_timezone !== "") {
                $reservation_info["reservation_starttime"] = EZDate::getLocateTime($reservation_info["reservation_starttime"],
                                                                                   $user_timezone, N2MY_SERVER_TIMEZONE);
                $reservation_info["reservation_endtime"]   = EZDate::getLocateTime($reservation_info["reservation_endtime"],
                                                                                   $user_timezone, N2MY_SERVER_TIMEZONE);
            }
            $reservation_list[] = $reservation_info;
        }
        return $reservation_list;
    }

    /**
     * 参加したすべての会議の情報取得
     */
    public function getParticipantedMeetingList( $member_info, $options, $user_timezone = null, $room_info = null)
    {
        if ($room_info) {
            foreach ($room_info as $room) {
                $room_keys[] = $room["room_info"]["room_key"];
            }
            $room_keys = " ('". implode("','", $room_keys) ."')";
            $this->logger->debug($room_keys);

        } else {
            $room_keys = " ('". $member_info["room_key"] ."')";
        }
        $sql = sprintf(
        "SELECT
            reservation. * , ru.r_member_key
        FROM
            reservation
        LEFT JOIN
            ( SELECT
                DISTINCT ( reservation_key) AS reservation_key, r_member_key
            FROM
                reservation_user
            WHERE
                r_member_key =%s
            )ru
        ON
            reservation.reservation_key = ru.reservation_key
        WHERE
            ( r_member_key= %d OR
              room_key IN %s )
                ", $member_info["member_key"], $member_info["member_key"], $room_keys );
        $sql .= " AND reservation_status = 1";

        $start_limit = ($options["start_time"]) ? $options["start_time"] : "0000-00-00 00:00:00";
        $end_limit   = ($options["end_time"])   ? $options["end_time"]   : "9999-12-31 23:59:59";

        $sql .= " AND (" .
                "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
        $sql .= "ORDER BY reservation_starttime";

        $rs = $this->obj_Reservation->_conn->query( $sql );
        if (DB::isError( $rs ) ) {
            $this->logger->error( $rs->getUserInfo() );
            return false;
        }

        $reservation_list = array();
        // 一覧表示
        while( $reservation_info = $rs->fetchRow( DB_FETCHMODE_ASSOC ) ){
            //他のメンバーが作成した部屋
            if( $reservation_info["room_key"] != $member_info["room_key"] ){
                $status = "end";
            } elseif (strtotime($reservation_info['reservation_starttime']) < strtotime("now")
                   && strtotime($reservation_info['reservation_endtime'])   > strtotime("now") ) {
                $status = "now"; // 開催中
                // 終了
            } elseif (strtotime($reservation_info['reservation_endtime']) < strtotime("now")) {
                $status = "end";// 開催待ち
            } else {
                $status = "wait";
            }
            $reservation_info["status"] = $status;
            if ($user_timezone !== "") {
                $reservation_info["reservation_starttime"] = EZDate::getLocateTime($reservation_info["reservation_starttime"], $user_timezone, N2MY_SERVER_TIMEZONE);
                $reservation_info["reservation_endtime"] = EZDate::getLocateTime($reservation_info["reservation_endtime"], $user_timezone, N2MY_SERVER_TIMEZONE);
            }
            $reservation_list[] = $reservation_info;
        }

        return $reservation_list;
    }

    /**
     * 招待された会議の情報取得
     */
    public function getInvitedMeetingList( $member_info, $options, $user_timezone = null)
    {
        $sql = sprintf(
        "SELECT
            reservation. * , ru.r_member_key
        FROM
            reservation
        LEFT JOIN
            ( SELECT
                DISTINCT ( reservation_key) AS reservation_key, r_member_key
            FROM
                reservation_user
            WHERE
                r_member_key =%s AND r_user_status = 1
            )ru
        ON
            reservation.reservation_key = ru.reservation_key
        WHERE
            ( r_member_key= %d )
                ", $member_info["member_key"], $member_info["member_key"]);
        $sql .= " AND reservation_status = 1";

        $start_limit = ($options["start_time"]) ? $options["start_time"] : "0000-00-00 00:00:00";
        $end_limit   = ($options["end_time"])   ? $options["end_time"]   : "9999-12-31 23:59:59";

        $sql .= " AND (" .
                "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
        $sql .= "ORDER BY reservation_starttime";

        $rs = $this->obj_Reservation->_conn->query( $sql );
        if (DB::isError( $rs ) ) {
            $this->logger->error( $rs->getUserInfo() );
            return false;
        }

        $reservation_list = array();
        // 一覧表示
        while( $reservation_info = $rs->fetchRow( DB_FETCHMODE_ASSOC ) ){
            //他のメンバーが作成した部屋
            if (strtotime($reservation_info['reservation_starttime']) < strtotime("now")
                   && strtotime($reservation_info['reservation_endtime'])   > strtotime("now") ) {
                $status = "now"; // 開催中
                // 終了
            } elseif (strtotime($reservation_info['reservation_endtime']) < strtotime("now")) {
                $status = "end";// 開催待ち
            } else {
                $status = "wait";
            }
            $reservation_info["status"] = $status;
            if ($user_timezone !== "") {
                $reservation_info["reservation_starttime"] = EZDate::getLocateTime($reservation_info["reservation_starttime"], $user_timezone, N2MY_SERVER_TIMEZONE);
                $reservation_info["reservation_endtime"] = EZDate::getLocateTime($reservation_info["reservation_endtime"], $user_timezone, N2MY_SERVER_TIMEZONE);
            }
            $reservation_list[] = $reservation_info;
        }

        return $reservation_list;
    }

    /**
     * 指定した条件の件数を取得
     */
    public function getCount($room_key, $options = array()) {
        if (!$where = $this->_getWhere($room_key, $options)) {
            return 0;
        }
        $count = $this->obj_Reservation->numRows($where);
        return $count;
    }

  public function getCount_for_ONE($room_key, $options = array()){
        if (!$where = $this->_getWhere_for_ONE($room_key, $options)) {
            return 0;
        }
        $count = $this->obj_Reservation->numRows($where);
        return $count;
  }

    //
    public function getParticipantedMeetingCount( $member_info, $options = array() )
    {
        $sql = sprintf(
        "SELECT
            count( * ) as count
        FROM
            reservation
        LEFT JOIN
            ( SELECT
                DISTINCT ( reservation_key) AS reservation_key, r_member_key
            FROM
                reservation_user
            WHERE
                r_member_key =%s
            )ru
        ON
            reservation.reservation_key = ru.reservation_key
        WHERE
            ( r_member_key= %d OR
              room_key ='%s' )
                ", $member_info["member_key"], $member_info["member_key"], $member_info["room_key"] );
        $sql .= " AND reservation_status = 1";

        $start_limit = ($options["start_time"]) ? $options["start_time"] : "0000-00-00 00:00:00";
        $end_limit   = ($options["end_time"])   ? $options["end_time"]   : "9999-12-31 23:59:59";

        $sql .= " AND (" .
                "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
        $sql .= "ORDER BY reservation_starttime";

        $rs   = $this->obj_Reservation->_conn->query( $sql );
        $reservation_info = $rs->fetchRow( DB_FETCHMODE_ASSOC );

        return $reservation_info["count"];
    }

    /**
     * 条件文取得
     */
    private function _getWhere($room_key = null, $options = array()) {
        if (empty($options["user_key"]) && !$room_key) {
            $this->logger->error(array($room_key, $options), "Parameter error!!");
            return "";
        }

        // 基本条件
        $cond = array();
        $cond[] = "reservation_status = 1";
        if (isset($options["user_key"])) {
            $cond[] = "user_key = '".addslashes($options["user_key"])."'";
        }
        if (isset($options["start_time"]) || isset($options["end_time"])) {
            $start_limit = $options["start_time"] ? mysql_real_escape_string($options["start_time"]) : "0000-00-00 00:00:00";
            $end_limit   = $options["end_time"] ? mysql_real_escape_string($options["end_time"]) : "9999-12-31 23:59:59";
            $cond[] = "(" .
                "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
        }
        if (isset($options["updatetime"])) {
            $updatetime = $options["updatetime"];
            $cond[] = "( reservation_registtime >= '$updatetime')" .
                " OR ( reservation_updatetime >= '$updatetime' )";

        }
        if (isset($options["status"])) {
            switch($options["status"]) {
            case 'wait':
                $cond[] = "reservation_starttime > '".date('Y-m-d H:i:s')."'";
                break;
            case 'now':
                $cond[] = "'".date('Y-m-d H:i:s')."' BETWEEN reservation_starttime AND reservation_endtime";
                break;
            case 'end':
                $cond[] = "reservation_starttime < '".date('Y-m-d H:i:s')."'";
                break;
            case 'valid':
                $start_limit = date('Y-m-d H:i:s');
                $end_limit   = "9999-12-31 23:59:59";
                $cond[] = "(" .
                    "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                    " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                    " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                    " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
                break;
            }
        }
        if (isset($options["reservation_name"])) {
            $reservation_name = $options["reservation_name"];
            $cond[] = "reservation_name LIKE '%".addslashes($reservation_name)."%'";
        }
        $where = join(" AND ", $cond);
        // ユーザ全件
        if (!$room_key && !$options["member_key"]) {
          $session   = EZSession::getInstance();
          if($session->get("service_mode") == "sales") {
            $where .= " AND room_key IN (SELECT room.room_key FROM room WHERE room.use_sales_option=1 AND room.user_key ='".$options["user_key"]."')";
          } elseif ($session->get("service_mode") == "meeting") {
            $where .= " AND room_key IN (SELECT room.room_key FROM room WHERE room.use_sales_option!=1 AND room.user_key ='".$options["user_key"]."')";
          }
            // 指定部屋のみ
        } elseif ($room_key && empty($options["member_key"])) {
            $where .= " AND room_key = '".addslashes($room_key)."'";
            // 招待された予約のみ
        } elseif (!$room_key && $options["member_key"]) {
            $where .= " AND reservation_key IN (SELECT reservation_user.reservation_key ".
                       "FROM reservation_user ".
                       "WHERE reservation_user.r_member_key = '".addslashes($options["member_key"])."' ".
                       "GROUP BY reservation_user.reservation_key)";
        } elseif ($room_key && $options["member_key"]) {
            $where .= " AND room_key = '".addslashes($room_key)."'" .
                      " OR (".$where." AND reservation_key IN (SELECT reservation_user.reservation_key ".
                       "FROM reservation_user WHERE reservation_user.r_member_key = '".addslashes($options["member_key"])."' ".
                       "GROUP BY reservation_user.reservation_key))";
        }

        $this->logger->debug($where);
        return $where;
    }

  /**
   * V-Cube ONE 専用
   */
    private function _getWhere_for_ONE($room_key = null, $options = array()) {
        if (empty($options["user_key"]) && !$room_key) {
            $this->logger->error(array($room_key, $options), "Parameter error!!");
            return "";
        }

        // 基本条件
        $cond = array();
        $cond[] = "reservation_status = 1";
        if (isset($options["user_key"])) {
            $cond[] = "user_key = '".addslashes($options["user_key"])."'";
        }
        if (isset($options["start_time"]) || isset($options["end_time"])) {
            $start_limit = $options["start_time"] ? mysql_real_escape_string($options["start_time"]) : "0000-00-00 00:00:00";
            $end_limit   = $options["end_time"] ? mysql_real_escape_string($options["end_time"]) : "9999-12-31 23:59:59";
            $cond[] = "(" .
                "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
        }
        if (isset($options["updatetime"])) {
            $updatetime = $options["updatetime"];
            $cond[] = "( reservation_registtime >= '$updatetime')" .
                " OR ( reservation_updatetime >= '$updatetime' )";

        }
        if (isset($options["status"])) {
            switch($options["status"]) {
            case 'wait':
                $cond[] = "reservation_starttime > '".date('Y-m-d H:i:s')."'";
                break;
            case 'now':
                $cond[] = "'".date('Y-m-d H:i:s')."' BETWEEN reservation_starttime AND reservation_endtime";
                break;
            case 'end':
                $cond[] = "reservation_starttime < '".date('Y-m-d H:i:s')."'";
                break;
            case 'valid':
                $start_limit = date('Y-m-d H:i:s');
                $end_limit   = "9999-12-31 23:59:59";
                $cond[] = "(" .
                    "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                    " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                    " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                    " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
                break;
            }
        }
        if (isset($options["reservation_name"])) {
            $reservation_name = $options["reservation_name"];
            $cond[] = "reservation_name LIKE '%".addslashes($reservation_name)."%'";
        }
        $where = join(" AND ", $cond);
        // ユーザ全件

        if($room_key){
          if($options['member_key']){
              $where .= " AND room_key = '".addslashes($room_key)."'" .
                      " OR (".$where." AND reservation_key IN (SELECT reservation_user.reservation_key ".
                          "FROM reservation_user WHERE reservation_user.r_member_key = '".addslashes($options["member_key"])."' ".
                          "GROUP BY reservation_user.reservation_key))";
          }else{
              $where .= " AND room_key = '".addslashes($room_key)."'";
          }
        }
        $session   = EZSession::getInstance();
        if($session->get("service_mode") == "sales") {
          $where .= " AND meeting_key IN (SELECT meeting_ticket FROM meeting WHERE use_sales_option=1 AND user_key ='".$options["user_key"]."')";
        } elseif ($session->get("service_mode") == "meeting") {
            $where .= " AND meeting_key IN (SELECT meeting_ticket FROM meeting WHERE use_sales_option!=1 AND user_key ='".$options["user_key"]."')";
        }
        $this->logger->debug($where);
        return $where;
    }

    /**
     * 予約追加
     */
    public function add($reservation_data) {
        require_once ("classes/N2MY_Account.class.php");
        $obj_N2MY_Account    = new N2MY_Account($this->dsn);
        $obj_N2MY_Meeting    = new N2MY_Meeting($this->dsn);

        $reservation_info    = $reservation_data["info"];
        $room_key            = $reservation_info["room_key"]; // MeetingKey発行
        $room_info           = $obj_N2MY_Account->getRoomInfo($room_key); // 会議情報取得
        $meeting_key         = $obj_N2MY_Meeting->createMeetingKey($room_key); // 会議キー生成
        $reservation_session = md5(uniqid(rand(), true));

        // 時差変換
        $starttime = EZDate::getLocateTime($reservation_info['reservation_starttime'], N2MY_SERVER_TIMEZONE,
                                           $reservation_info["reservation_place"]);
        $reservation_info['reservation_starttime'] = date("Y-m-d H:i:s", $starttime);
        $endtime   = EZDate::getLocateTime($reservation_info['reservation_endtime'], N2MY_SERVER_TIMEZONE,
                                           $reservation_info["reservation_place"]);
        $reservation_info['reservation_endtime'] = date("Y-m-d H:i:s", $endtime);

        // 制限を利用しない場合は
        if ($reservation_data["limited_function_flg"] == 0) {
            $reservation_data["limited_function"] = "";
        }
        // 予約会議登録
        $data = array(
             "user_key"                       => $reservation_info["user_key"],
             "member_key"                     => $reservation_info["member_key"],
             "room_key"                       => $room_key,
             "meeting_key"                    => $meeting_key,
             "reservation_name"               => $reservation_info["reservation_name"],
             "reservation_place"              => $reservation_info["reservation_place"],
             "reservation_starttime"          => $reservation_info["reservation_starttime"],
             "reservation_endtime"            => $reservation_info["reservation_endtime"],
             "reservation_session"            => $reservation_session,
             "sender_name"                    => $reservation_info["sender"],
             "sender_email"                   => $reservation_info["sender_mail"],
             "sender_lang"                    => $reservation_info["sender_lang"],
             "mail_type"                      => $reservation_info["mail_type"],
             "max_port"                       => $reservation_data["is_manager"]  ? "3" : $reservation_info["max_port"] ,
             "reservation_url"                => $reservation_info["reservation_url"],
             "reservation_custom"             => $reservation_info["reservation_custom"],
             "reservation_info"               => $reservation_info["mail_body"],
             "reservation_status"             => "1",
             "reservation_registtime"         => date("Y-m-d H:i:s"),
             "reservation_updatetime"         => date("Y-m-d H:i:s"),
             "is_multicamera"                 => $reservation_info["is_multicamera"]       ? $reservation_info["is_multicamera"] :"0",
             "is_limited_function"            => $reservation_info["is_limited_function"]  ? $reservation_info["is_limited_function"] :"0",
             "is_telephone"                   => $reservation_info["is_telephone"]         ? $reservation_info["is_telephone"]    : "0",
             "is_h323"                        => $reservation_info["is_h323"]              ? $reservation_info["is_h323"]         : "0",
             "is_mobile_phone"                => $reservation_info["is_mobile_phone"]      ? $reservation_info["is_mobile_phone"] : "0",
             "is_high_quality"                => $reservation_info["is_high_quality"]      ? $reservation_info["is_high_quality"] : "0",
             "is_desktop_share"               => $reservation_info["is_desktop_share"]     ? $reservation_info["is_desktop_share"]: "0",
             "is_meeting_ssl"                 => $reservation_info["is_meeting_ssl"]       ? $reservation_info["is_meeting_ssl"]  : "0",
             "is_invite_flg"                  => $reservation_info["is_invite_flg"]        ? $reservation_info["is_invite_flg"]   : "0",
             "is_rec_flg"                     => $reservation_info["is_rec_flg"]           ? $reservation_info["is_rec_flg"]      : "0",
             "is_convert_wb_to_pdf"           => $reservation_info["is_convert_wb_to_pdf"] ? $reservation_info["is_convert_wb_to_pdf"] : "0",
             "is_cabinet_flg"                 => $reservation_info["is_cabinet_flg"]       ? $reservation_info["is_cabinet_flg"]  : "0",
             "is_wb_no_flg"                   => $reservation_info["is_wb_no_flg"]         ? $reservation_info["is_wb_no_flg"]    : "0",
             "is_reminder_flg"                => $reservation_info["is_reminder_flg"]      ? $reservation_info["is_reminder_flg"] : "0",
             "is_customer_desktop_share_flg"  => $reservation_info["is_customer_desktop_share_flg"]  ? $reservation_info["is_customer_desktop_share_flg"] : "0",
             "is_auto_rec_flg"                => $reservation_info["is_auto_rec_flg"]      ? $reservation_info["is_auto_rec_flg"] : "0",
             "is_customer_camera_flg"         => $reservation_info["is_customer_camera_flg"]  ? $reservation_info["is_customer_camera_flg"] : "0",
             "is_customer_camera_display"     => $reservation_info["is_customer_camera_display"]  ? $reservation_info["is_customer_camera_display"] : "0",
             "is_wb_allow_flg"                => $reservation_info["is_wb_allow_flg"]      ? $reservation_info["is_wb_allow_flg"] : "0",
             "is_chat_allow_flg"              => $reservation_info["is_chat_allow_flg"]    ? $reservation_info["is_chat_allow_flg"] : "0",
             "is_print_allow_flg"             => $reservation_info["is_print_allow_flg"]    ? $reservation_info["is_print_allow_flg"] : "0",
             "is_authority_flg"               => $reservation_info["is_authority_flg"]    ? $reservation_info["is_authority_flg"] : "0",
             "is_organizer_flg"               => $reservation_info["is_organizer_flg"]    ? $reservation_info["is_organizer_flg"] : "0",
             "is_manager"                     => $reservation_data["is_manager"] ? $reservation_data["is_manager"] : "0",
             "organizer_name"                 => $reservation_data["organizer"]["name"]    ? $reservation_data["organizer"]["name"] : "",
             "organizer_email"                => $reservation_data["organizer"]["email"]    ? $reservation_data["organizer"]["email"] : "",
         );
        // パスワード変更
        if ($reservation_data["pw_flg"]) {
            if ($reservation_info["security_pw_flg"] == 1 && !$reservation_info["reservation_pw"]) {
                $session   = EZSession::getInstance();
                $user_info = $session->get("user_info");
                $data["reservation_pw"] = $user_info["is_compulsion_pw"];
            } else {
                $data["reservation_pw"] = $reservation_info["reservation_pw"];
            }
            $data["reservation_pw_type"] = $reservation_info["reservation_pw_type"] ? $reservation_info["reservation_pw_type"] : 1;
        }
        $this->logger->debug($data);
        $reservation_key = $this->obj_Reservation->add($data);
        if (DB::isError($reservation_key)) {
            $this->logger->error($reservation_key->getUserInfo());
            return false;
        }

        // ポータルプッシュ
        if ( EZConfig::getInstance()->get( 'VCUBE_PORTAL', 'is_pool_reservation' )
        	&& EZSession::getInstance()->get('service_mode') == 'meeting'
        	&& $vid_info = EZSession::getInstance()->get( 'vid_info' ) ) {
        	require_once 'classes/dbi/reservation_push.dbi.php';
        	$dbi_reservation_push = new ReservationPush( $this->dsn );
        	$dbi_reservation_push->add( ReservationPush::MODE_ADD, $reservation_key, $vid_info['vcube_id'], $vid_info['auth_token'], $vid_info['contract_id'] );
        }

        // オプション
        $options = array(
            "user_key"         => $reservation_info["user_key"],
            "country_id"       => $reservation_info["country_id"],
            "meeting_name"     => $reservation_info["reservation_name"],
            "start_time"       => $reservation_info["reservation_starttime"],
            "end_time"         => $reservation_info["reservation_endtime"],
            "password"         => $data["reservation_pw"],
            "tc_type"          => $reservation_info["tc_type"],
            "tc_teleconf_note" => $reservation_info["tc_teleconf_note"],
            "use_pgi_dialin"      => $reservation_info["use_pgi_dialin"],
            "use_pgi_dialin_free" => $reservation_info["use_pgi_dialin_free"],
            "use_pgi_dialin_lo_call" => $reservation_info["use_pgi_dialin_lo_call"],
            "use_pgi_dialout"     => $reservation_info["use_pgi_dialout"],
            "ives_setting_key"	=> $reservation_info["ives_setting_key"],
//            "ives_did"			=> $reservation_info["ives_did"],
//            "ives_conference_id"=> $reservation_info["ives_conference_id"]
            );
        if (!$obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options)) {
            $this->logger->error(array($room_key, $meeting_key, $options));
            return false;
        }

        $data["reservation_key"] = $reservation_key;
        // 資料のアップロード
        if (isset($reservation_data["documents"])) {
            foreach($reservation_data["documents"] as $document) {
                $this->addDocument($meeting_key, $document["tmp_name"], $document["name"], $document["format"], $document["version"],$document["sort"]);
            }
        }
        // ストレージアップ
        if(isset($reservation_data["storage_documents"])){
            foreach($reservation_data["storage_documents"] as $storage) {
                $session   = EZSession::getInstance();
                $user_info = $session->get("user_info");
                $this->_addStorageFile($storage , $room_key , $meeting_key , $user_info["user_id"]);
            }
        }
        // クリップとの紐付け
        $user_table = new UserTable($this->dsn);
        $user       = $user_table->find($reservation_info['user_key']);
        $this->logger->debug('clips  line '.__FILE__.__LINE__);
        if (!$this->addClipRelations($meeting_key, $reservation_data["clips"], $reservation_info['user_key'])){
            return false;
        }
        // 管理サーバーへ登録
        $user_id = $this->_get_user_id($reservation_info["room_key"]);
        $this->obj_MGMAuthClass->addRelationData($user_id, $reservation_session, "reservation");

        $reservation_info["intra_fms"] = $room_info["options"]["intra_fms"];


        // 参加者登録
        if ($reservation_data["guest_flg"] || $reservation_data["organizer"] || $reservation_data["authority"]) {
            $send_mail = !isset( $reservation_data['send_mail'] ) || $reservation_data['send_mail'];

	        require_once("classes/dbi/meeting.dbi.php");
	        $objMeeting = new MeetingTable($this->dsn);
	        $pin_cd     = $objMeeting->getOne("meeting_ticket = '".addslashes($meeting_key)."'", "pin_cd");
	        $reservation_info["pin_cd"] = $pin_cd;
	        $reservation_info["organizer"] = $reservation_data["organizer"];
	        if($reservation_info["is_authority_flg"]){
	            $reservation_info["authority"] = $reservation_data["authority"];
	        }
	        $data["guests"] = $this->updateParticipantList($reservation_key, $reservation_info,
	                                                       $reservation_data, "create", $send_mail);
	        $data["pin_cd"] = $pin_cd;
        }

        return $data;
    }

    /**
     * 資料ID生成
     */
    function createID() {
        $document_id = sha1(uniqid(rand(), true));
        return $document_id;
    }

    /**
     * cpコマンド
     * @param string $old_dir
     * @param string $new_dir
     */
    private function _cp($old_path, $new_path){
        if(file_exists($old_path)){
            if(is_dir($old_path)){
                $cmd = sprintf("cp -r %s %s", $old_path, $new_path);
            }else{
                $cmd = sprintf("cp %s %s", $old_path, $new_path);
            }
            // ※コマンド関数を使用するので使う環境で注意が必要
            $cmd_res = exec($cmd);
            //$this->logger->info($old_path);
            //$this->logger->info($new_path);
        }
    }

    /*
     * ストレージデータから事前アップへコピー
     */
    function _addStorageFile($file_info , $room_key , $meeting_ticket , $user_id){
        $document_id = $this->createID();
        $path = $user_id."/".$room_key."/".$meeting_ticket."/";
        require_once("classes/core/dbi/Document.dbi.php");
        $obj_Document = new DBI_Document($this->dsn);
        require_once('classes/core/dbi/Meeting.dbi.php');
        $obj_Meeting  = new DBI_Meeting($this->dsn);
        $where        = "meeting_ticket = '".addslashes($meeting_ticket)."'";
        $meeting_info = $obj_Meeting->getRow($where, "meeting_key,room_key");
        // ファイルのコピー DBコピー
        if($file_info["category"] == "image" || $file_info["category"] == "document"){
            // documentテーブルにデータ追加
            $where = "document_id = '".addslashes($file_info["document_id"])."'";
            $doucument_info = $obj_Document->getRow($where);
            unset($doucument_info["document_key"]);
            $doucument_info["document_id"] = $document_id;
            $doucument_info["meeting_key"] = $meeting_info["meeting_key"];
            $doucument_info["document_path"] = $path;
            $doucument_info["document_mode"] = "pre";
            $doucument_info["document_name"] = $file_info["name"];
            $doucument_info["is_storage"] = "1";
            $doucument_info["document_transmitted"] = "1";
            $doucument_info["participant_id"] = $participant_id;
            $doucument_info["create_datetime"] = date("Y-m-d H:i:s");
            $doucument_info["update_datetime"] = date("Y-m-d H:i:s");

            $where = $where = "meeting_key = '".addslashes($meeting_info["meeting_key"])."'";
            $doucument_info["document_sort"] = $obj_Document->getOne($where, "MAX(document_sort)", array("document_sort" => "desc")) + 1;

            $obj_Document->add($doucument_info);

            // 実ファイルのコピー
            $old_dir = N2MY_DOC_DIR.$file_info["file_path"].$file_info["document_id"];
            $new_dir = N2MY_DOC_DIR.$path.$document_id;

            if(file_exists($old_dir)){
                if(!file_exists(N2MY_DOC_DIR.$path)){
                    mkdir(N2MY_DOC_DIR.$path,0777,true);
                    chmod(N2MY_DOC_DIR.$path, 0777);
                }
                // コピー
                $this->_cp($old_dir, $new_dir);
                // ファイルを一つ一つリネームする
                $files = scandir($new_dir);
                // 「.」と「..」は除去
                unset($files[array_search('.',$files)],$files[array_search('..',$files)]);
                $count = 1;
                foreach($files as $file){
                    // ID部分だけ変更する
                    $new_file_name = preg_replace('/([a-zA-Z0-9]*)(_)([0-9]*)(.[a-zA-Z]*)/',$document_id.'$2$3$4',$file);
                    $target_file_path = $new_dir . "/" .$file;
                    $new_file_path = $new_dir . "/" .$new_file_name;
                    rename($target_file_path, $new_file_path);
                    $count++;
                    //$this->logger->info($new_file_path);
                }
            }

        }

        return true;
    }

    function getMessage($lang, $group, $id = "", $custom = "") {
        static $message;
        $config = EZConfig::getInstance();

        if(!isset($message[$lang])) {
            $config_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
            $message[$lang] = parse_ini_file($config_file, true);
            if(!empty($custom)) {
                $custom_config_file = N2MY_APP_DIR . "config/custom/$custom/" . $lang . "/message.ini";

                if(file_exists($custom_config_file)) {
                    $custom_msg_list = parse_ini_file($custom_config_file, true);

                    foreach($custom_msg_list as $grp => $vals) {
                        if(isset($message[$lang][$grp]) && is_array($message[$lang][$grp])) {
                            $message[$lang][$grp] = array_merge($message[$lang][$grp], $vals);
                        }
                        else {
                            $message[$lang][$grp] = $vals;
                        }
                    }
                }
            }
        }
        if (!$id) {
            return $message[$lang][$group];
        }

        return $message[$lang][$group][$id];
    }

    public function dropParticipantList( $guests )
    {
        if (!$guests) {
            return array();
        }

        $participant_list = array();

        foreach ($guests as $guest) {
            $data = array("r_user_status"     => 0);
            $this->logger->debug($data);
            $where = sprintf( "r_user_key='%s'", $guest["r_user_key"] );
            $ret = $this->obj_ReservationUser->update( $data, $where );
            if (DB::isError($ret)) {
                $this->logger->error($ret->getUserInfo());
            } else {
                $participant_list[] = $guest;
            }
        }

        return $participant_list;
    }

    /**
     * S&S 更新処理
     * S&Sでの監視者入室＋ポート制御＋最大参加者が2人予約の場合
     * max_portを2→3に変更
     */
    public function addManagerPort($meeting_key)
    {
        $where           = "meeting_key = '".addslashes($meeting_key)."'";
                
        $data = array(
                        "reservation_updatetime" => date("Y-m-d H:i:s"),
                        "is_manager"             => "2",
                        "max_port"               => "3",
        );
        
        $this->obj_Reservation->update($data, $where);
    }
    
    
    /**
     * 更新処理
     */
    public function update($reservation_data)
    {
        $reservation_info = $reservation_data["info"];
        // 時差変換
        $starttime = EZDate::getLocateTime($reservation_info['reservation_starttime'], N2MY_SERVER_TIMEZONE,
                                           $reservation_info["reservation_place"]);
        $reservation_info['reservation_starttime'] = date("Y-m-d H:i:s", $starttime);
        $endtime = EZDate::getLocateTime($reservation_info['reservation_endtime'], N2MY_SERVER_TIMEZONE,
                                         $reservation_info["reservation_place"]);
        $reservation_info['reservation_endtime'] = date("Y-m-d H:i:s", $endtime);
        
        // 監視者途中入室の処理
        $reservation_info_meeting = $this->obj_Reservation->getRow("reservation_key = '".$reservation_info["reservation_key"]."'");
        if(!$reservation_data["is_manager"] && $reservation_info_meeting["is_manager"] == 2){
            $reservation_data["is_manager"] = 2 ;
        }        
        // 予約更新
        $reservation_key = $reservation_info["reservation_key"];
        $room_key        = $reservation_info["room_key"];
        $where           = "reservation_session = '".addslashes($reservation_info["reservation_session"])."'";
        $data = array(
             "room_key"               => $room_key,
             "reservation_name"       => $reservation_info["reservation_name"],
             "reservation_place"      => $reservation_info["reservation_place"],
             "reservation_starttime"  => $reservation_info["reservation_starttime"],
             "reservation_endtime"    => $reservation_info["reservation_endtime"],
             "reservation_pw"         => $reservation_info["reservation_pw"],
             "reservation_pw_type"    => $reservation_info["reservation_pw_type"] ? $reservation_info["reservation_pw_type"] : 1,
             "sender_name"            => $reservation_info["sender"],
             "sender_email"           => $reservation_info["sender_mail"],
             "sender_lang"            => $reservation_info["sender_lang"],
             "mail_type"              => $reservation_info["mail_type"],
             "max_port"               => $reservation_data["is_manager"]  ? "3" : $reservation_info["max_port"] ,
             "reservation_info"       => $reservation_info["mail_body"],
             "reservation_updatetime" => date("Y-m-d H:i:s"),
             "reservation_updatetime" => date("Y-m-d H:i:s"),
             "is_multicamera"         => $reservation_info["is_multicamera"]     ? $reservation_info["is_multicamera"]  :"0",
             "is_limited_function"    => $reservation_info["is_limited_function"]? $reservation_info["is_limited_function"] :"0",
             "is_telephone"           => $reservation_info["is_telephone"]       ? $reservation_info["is_telephone"]    :"0",
             "is_h323"                => $reservation_info["is_h323"]            ? $reservation_info["is_h323"]         :"0",
             "is_mobile_phone"        => $reservation_info["is_mobile_phone"]    ? $reservation_info["is_mobile_phone"] :"0",
             "is_high_quality"        => $reservation_info["is_high_quality"]    ? $reservation_info["is_high_quality"] :"0",
             "is_desktop_share"       => $reservation_info["is_desktop_share"]   ? $reservation_info["is_desktop_share"]:"0",
             "is_meeting_ssl"         => $reservation_info["is_meeting_ssl"]     ? $reservation_info["is_meeting_ssl"]  :"0",
             "is_invite_flg"          => $reservation_info["is_invite_flg"]      ? $reservation_info["is_invite_flg"]   :"0",
             "is_rec_flg"             => $reservation_info["is_rec_flg"]         ? $reservation_info["is_rec_flg"]      :"0",
             "is_convert_wb_to_pdf"   => $reservation_info["is_convert_wb_to_pdf"] ? $reservation_info["is_convert_wb_to_pdf"] :"0",
             "is_cabinet_flg"         => $reservation_info["is_cabinet_flg"]     ? $reservation_info["is_cabinet_flg"]  :"0",
             "is_wb_no_flg"           => $reservation_info["is_wb_no_flg"]       ? $reservation_info["is_wb_no_flg"]    :"0",
             "is_reminder_flg"        => $reservation_info["is_reminder_flg"]    ? $reservation_info["is_reminder_flg"] :"0",
             "is_customer_desktop_share_flg" => $reservation_info["is_customer_desktop_share_flg"]    ? $reservation_info["is_customer_desktop_share_flg"] :"0",
             "is_auto_rec_flg"        => $reservation_info["is_auto_rec_flg"]    ? $reservation_info["is_auto_rec_flg"] :"0",
             "is_customer_camera_flg" => $reservation_info["is_customer_camera_flg"]    ? $reservation_info["is_customer_camera_flg"] :"0",
             "is_customer_camera_display" => $reservation_info["is_customer_camera_display"]    ? $reservation_info["is_customer_camera_display"] :"0",
             "is_wb_allow_flg"        => $reservation_info["is_wb_allow_flg"]    ? $reservation_info["is_wb_allow_flg"] :"0",
             "is_chat_allow_flg"      => $reservation_info["is_chat_allow_flg"]    ? $reservation_info["is_chat_allow_flg"] :"0",
             "is_print_allow_flg"     => $reservation_info["is_print_allow_flg"]    ? $reservation_info["is_print_allow_flg"] :"0",
             "is_authority_flg"       => $reservation_info["is_authority_flg"] ? $reservation_info["is_authority_flg"] : "0",
             "is_organizer_flg"       => $reservation_info["is_organizer_flg"],
             "is_manager"             => $reservation_data["is_manager"] ? $reservation_data["is_manager"] : "0",
             "organizer_name"         => $reservation_data["organizer"]["name"]    ? $reservation_data["organizer"]["name"] : "",
             "organizer_email"        => $reservation_data["organizer"]["email"]    ? $reservation_data["organizer"]["email"] : "",
                );
        
        // 時間変更があったらリマインダーメールフラグを未送信に変更
        if($reservation_data["start_time_modify"] == 1){
            $data["is_reminder_send_flg"] = 0;
        }
        if(!$reservation_data["pw_flg"]){
            $data["reservation_pw_type"] = 0;
        }

                
        $this->obj_Reservation->update($data, $where);

        // ポータルプッシュ
        if ( EZConfig::getInstance()->get( 'VCUBE_PORTAL', 'is_pool_reservation' )
        	&& EZSession::getInstance()->get('service_mode') == 'meeting'
        	&& $vid_info = EZSession::getInstance()->get( 'vid_info' ) ) {
        	require_once 'classes/dbi/reservation_push.dbi.php';
        	$dbi_reservation_push = new ReservationPush( $this->dsn );
        	$dbi_reservation_push->add( ReservationPush::MODE_UPDATE, $reservation_key, $vid_info['vcube_id'], $vid_info['auth_token'], $vid_info['contract_id'] );
        }

        // オプション
        $options = array(
            "user_key"            => $reservation_info["user_key"],
            "country_id"          => $reservation_info["country_id"],
            "meeting_name"        => $reservation_info["reservation_name"],
            "start_time"          => $reservation_info["reservation_starttime"],
            "end_time"            => $reservation_info["reservation_endtime"],
            "password"            => $data["reservation_pw"],
            "tc_type"             => $reservation_info["tc_type"],
            "tc_teleconf_note"    => $reservation_info["tc_teleconf_note"],
            "use_pgi_dialin"      => $reservation_info["use_pgi_dialin"],
            "use_pgi_dialin_free" => $reservation_info["use_pgi_dialin_free"],
            "use_pgi_dialin_lo_call" => $reservation_info["use_pgi_dialin_lo_call"],
            "use_pgi_dialout"     => $reservation_info["use_pgi_dialout"],
            "reservation_modify"  => "1",
            );
        $obj_N2MY_Meeting = new N2MY_Meeting($this->dsn);
        $this->logger->debug(array($reservation_info["room_key"], $reservation_info["meeting_key"], $options));
        if (!$obj_N2MY_Meeting->createMeeting($reservation_info["room_key"], $reservation_info["meeting_key"], $options)) {
            return false;
        }

        // 資料のアップロード
        if ($reservation_data["documents"]) {
            $user_id = $this->_get_user_id($reservation_info["room_key"]);
            foreach ($reservation_data["documents"] as $document) {
                if (isset($document["document_id"])) {
                    $this->updateDocument($document["document_id"], $document); // 更新
                } else {
                    $this->addDocument($reservation_info["meeting_key"], $document["tmp_name"],
                                       $document["name"], $document["format"], $document["version"], $document["sort"]);// 新規追加
                }
            }
        }
        // ストレージアップ
        if(isset($reservation_data["storage_documents"])){
          foreach($reservation_data["storage_documents"] as $storage) {
            $session   = EZSession::getInstance();
            $user_info = $session->get("user_info");
            $this->_addStorageFile($storage , $room_key , $reservation_info["meeting_key"] , $user_info["user_id"]);
          }
        }
        // 資料の削除
        if (isset($reservation_data["delete_documents"])) {
            foreach ($reservation_data["delete_documents"] as $document) {
                $this->deleteDocument($document);
            }
        }
        // クリップとの紐付け
        $user_table = new UserTable($this->dsn);
        $user       = $user_table->find($reservation_info['user_key']);
        $this->logger->debug('clips  line '.__FILE__.__LINE__);
        if (!$this->restoreClipRelations($reservation_info["meeting_key"], $reservation_data["clips"],
                                         $reservation_info['user_key'])) {
            return false;
        }
        // 参加者の更新
        if ( $reservation_data["guest_flg"]  || $reservation_data["organizer"]  || $reservation_data["authority"]) {
	        $obj_N2MY_Account = new N2MY_Account($this->dsn);
	        $room_info = $obj_N2MY_Account->getRoomInfo($reservation_data["info"]["room_key"]);
	        // intra用フラグ追加
	        $reservation_info["intra_fms"] = $room_info["options"]["intra_fms"];
	        if (isset($reservation_data["send_mail"]) && $reservation_data["send_mail"] == false) {
	            $send_mail = 0;
	        } else {
	            $send_mail = $reservation_data["mail_send_type"];
	        }
	        require_once("classes/dbi/meeting.dbi.php");
	        $objMeeting = new MeetingTable($this->dsn);
	        $pin_cd     = $objMeeting->getOne("meeting_ticket = '".addslashes($reservation_info["meeting_key"])."'", "pin_cd");
	        $reservation_info["pin_cd"] = $pin_cd;
	        $reservation_info["organizer"] = $reservation_data["organizer"];
	        if($reservation_info["is_authority_flg"]){
	            $reservation_info["authority"] = $reservation_data["authority"];
	        }
	        $data["guests"] = $this->updateParticipantList($reservation_key, $reservation_info,
	                                                       $reservation_data, "modify", $send_mail);
        }

        return $data;
    }

    /**
     * 招待者追加
     */
    public function updateParticipantList($reservation_key, $info, $reservation_data, $type, $send_mail = 2) {

        $config   = EZConfig::getInstance();
        // ユーザ取得
        $session   = EZSession::getInstance();
        $user_info = $session->get("user_info");
        $user_key  = $user_info["user_key"];

        $mail_info  = array('reservation_key' => $reservation_key,
                            'info'            => $info,
                            'type'            => $type);

        $customer = $reservation_data["guest"];
        $staff = $reservation_data["staff"];
        $guests = $reservation_data["guests"];
        $manager = $reservation_data["manager"];
        // 議長を招待者として追加
        if($reservation_data["info"]["is_authority_flg"] == 1){
            $guests[] = $reservation_data["authority"];
        }
        // 主催者も招待者として追加
        if($reservation_data["info"]["is_organizer_flg"] == 1 && $info["sender_mail"] != $reservation_data["organizer"]["email"]){
            $guests[] = $reservation_data["organizer"];
        }
        if($customer != null && $staff != null) {
          $mail_info["info"]["custom"] = "sales";
          $customer["reservation_key"] = $reservation_key;
          $staff["reservation_key"] = $reservation_key;
          $manager["reservation_key"] = $reservation_key;
          $_r_users = $this->getParticipantList($reservation_key);
          if(!empty($_r_users)) {
              $_staff = $r_user_info[$staff["r_session_key"]];
              $_customer = $r_user_info[$customer["r_session_key"]];
              //更新
              $updated_customer = $this->participant_mail($customer, $info, "modify");
              $add_list[]    = $updated_customer;
              $updated_staff = $this->participant_mail($staff, $info, "modify");
              $add_list[]    = $updated_staff;
              
              if($reservation_data["is_manager"]){
                  $manager["is_manager"] = $reservation_data["is_manager"];
                  if($manager["r_user_session"]){
                      // 監視者情報を変更
                      $updated_manager = $this->participant_mail($manager, $info, "modify");
                  }else{
                      // 監視者情報を追加
                      $manager["r_user_session"] = $this->getInviteId();
                      if($reservation_data["is_manager"] != "2"){
                          $updated_manager = $this->participant_mail($manager, $info, "create");
                       }else{
                          $updated_manager = $this->participant_mail($manager, $info, "modify");
                       }
                  }
                  $add_list[]    = $updated_manager;
              }else{
                  if($manager["r_user_session"]){
                      // 監視者情報を削除（reservation_user Table)
                      $updated_guest = $this->participant_mail($manager, $info, "deletelist");
                  }
              }
              if ($send_mail > 0 && $updated_customer['email'] && $updated_staff['email']) {
                $mail_info['guest']['modify'][] = $updated_customer;
                $mail_info['guest']['modify'][] = $updated_staff;
                $mail_info['guest']['modify'][] = $updated_manager;
              }
          } else {
            //追加
            $customer["r_user_session"] = $this->getInviteId();
            $updated_customer = $this->participant_mail($customer, $info, "create");
            $add_list[]    = $updated_customer;
            $staff["r_user_session"] = $this->getInviteId();
            $updated_staff = $this->participant_mail($staff, $info, "create");
            $add_list[]    = $updated_staff;
            if($reservation_data["is_manager"]){
                $manager["r_user_session"] = $this->getInviteId();
                $manager["is_manager"] = 1;
                $updated_manager = $this->participant_mail($manager, $info, "create");
                $add_list[]    = $updated_manager;
            }
            if ($send_mail > 0 && $updated_staff['email'] && $updated_customer['email']) {
              $mail_info['guest']['create'][] = $updated_staff;
              $mail_info['guest']['create'][] = $updated_customer;
              $mail_info['guest']['create'][] = $updated_manager;
            }
          }

        } else {
          // 登録済みの招待者一覧取得
          $_guests = $this->getParticipantList($reservation_key);
          foreach ($guests as $key => $guest) {
            $guest["reservation_key"] = $reservation_key;
            // 追加
            if (!$guest["r_user_session"]) {
              $guest["r_user_session"] = $this->getInviteId();

              $updated_guest = $this->participant_mail($guest, $info, "create");
              $add_list[]    = $updated_guest;
              if ($send_mail > 0 && $updated_guest['email']) {
                $mail_info['guest']['create'][] = $updated_guest;
              }
              // 更新
            } elseif (isset($_guests[$guest["r_user_session"]])) {
              $updated_guest = $this->participant_mail($guest, $info, "modify");// 更新
              $add_list[]    = $updated_guest;
              if ($send_mail == 2 && $updated_guest['email']) {
                $mail_info['guest']['modify'][] = $updated_guest;
              }
              elseif($send_mail == 1 && $updated_guest['email'] && $guest["is_changed"])
              {
                $mail_info['guest']['modify'][] = $updated_guest;
              }
              unset($_guests[$guest["r_user_session"]]); // 削除対象からはずす
              // 主催者
              if($guest["r_organizer_flg"]){
                  $info["organizer"]["r_user_session"] = $guest["r_user_session"];
               }
            }
          }

          // 削除ユーザ
          foreach ($_guests as $key => $delete_guests) {
            $delete_guests["reservation_key"] = $reservation_key;
            $updated_guest = $this->participant_mail($delete_guests, $info, "deletelist");
            $delete_list[] = $updated_guest;
            if ($send_mail > 0 && $updated_guest['email']) {
              $mail_info['guest']['deletelist'][] = $updated_guest;
            }
          }
        }

        $noreplay_address = $config->get("N2MY", "noreply_address")."@".$config->get("N2MY", "mail_wb_host");

        if ($send_mail > 0 && $info["sender_mail"] && ($info["sender_mail"] != $noreplay_address)) {
            $lang = $session->get("lang", _EZSESSION_NAMESPACE, "ja");
            $mail_info['owner'] = array("is_send" => true,
                                        "lang"    => $lang);
        }else{
            $mail_info['owner'] = array("is_send" => false);
        }

        if ($send_mail > 0 && !empty($mail_info['guest'])) {
          $this->logger->debug($mail_info,"mail_info");
            $this->sendMailWithBackground($mail_info);
        }

        return $add_list;
    }

    public function getInviteId() {
        return substr(md5(uniqid(rand(), 1)), 0, 24);
    }

    private function getTelephoneNo($lang) {
        static $telephone_data;
        // 設定
        if ($telephone_data[$lang]) {
            return $telephone_data[$lang];
        }
        $config  = EZConfig::getInstance();
        $tel_no  = $config->getAll("TELEPHONE");
        $lang_cd = EZLanguage::getLangCd($lang);
        $location_list = $this->getMessage($lang_cd, "TELEPHONE");
        $telephone_data[$lang] = array();
        foreach ($location_list as $key => $location) {
            $telephone_data[$lang][$key]["name"] = $location;
            $telephone_data[$lang][$key]["list"] = split(",", $tel_no[$key]);
        }
        return $telephone_data[$lang];
    }

    /**
     * 招待者の処理
     */
    function participant_mail($guest, $info, $type, $send_mail = false , $user_info = null) {
        // ユーザ取得
        $session   = EZSession::getInstance();
        if($user_info == null){
            $user_info = $session->get("user_info");
        }
        $user_id   = $user_info["user_id"];
        // 登録
        $data = array(
            "reservation_key"   => $guest["reservation_key"],
            "r_user_name"       => $guest["name"],
            "r_user_email"      => $guest["email"],
            "r_user_place"      => $guest["timezone"],
            "r_user_lang"       => $guest["lang"],
            "r_user_audience"   => $guest["type"],
            "r_user_session"    => $guest["r_user_session"],
            "r_organizer_flg"    => $guest["r_organizer_flg"],
            "r_user_authority"    => $guest["r_user_authority"]?$guest["r_user_authority"]:0,
            "r_manager_flg"       => $guest["is_manager"] ? 1 : 0 ,
            "r_user_updatetime" => date("Y-m-d H:i:s"),
            );
        // メンバーアカウントに相当するかチェック
        // 登録種別
        switch ($type) {
            // 追加
            case "create":
                // メンバー指定
                if ($guest["member_key"]) {
                    $data["r_member_key"] = $guest["member_key"];
                // メンバー課金
                } elseif ($user_info['account_model'] == "member") {
                    $objMember = new MemberTable($this->dsn);
                    $where = "user_key = ".$user_info["user_key"].
                             " AND member_status = 0".
                             " AND member_name = '".addslashes($guest["name"])."'".
                             " AND member_email = '".addslashes($guest["email"])."'";
                    if ($member_info = $objMember->getRow($where)) {
                        $data["r_member_key"] = $member_info["member_key"];
                    }
                }
                $data["r_user_status"]     = 1;
                $data["r_user_registtime"] = date("Y-m-d H:i:s");
                $ret = $this->obj_ReservationUser->add($data);

                // 管理サーバーへ登録
                $this->obj_MGMAuthClass->addRelationData($user_id, $guest["r_user_session"], "invite");
                // アドレス帳に追加
                $this->logger->debug($guest);
                if ($info["auto_addressbook_flg"]) {
                    $this->addAddressBook($guest);
                }                
                break;
            // 更新
            case "modify" :
                $where = "r_user_session = '".$guest["r_user_session"]."'";
                $ret   = $this->obj_ReservationUser->update($data, $where);
                if (DB::isError($ret)) {
                    $this->logger->error($ret->getMessage());
                }
                break;
            // 削除
            case "deletelist":
                $data["r_user_status"] = 0;
                $where = "r_user_session = '".$guest["r_user_session"]."'";
                $ret   = $this->obj_ReservationUser->update($data, $where);
                if (DB::isError($ret)) {
                    $this->logger->error($ret->getMessage());
                }
                $this->obj_MGMAuthClass->deleteRelationData($guest["r_user_session"], "invite");
                break;
            case "reminder":
                break;
        }
        // 時差調整
        if ($guest["timezone"] == "100") {
            $time_zone = $info["reservation_place"];
        } else {
            $time_zone = $guest["timezone"];
        }
        $country_key = $session->get("country_key", _EZSESSION_NAMESPACE);

        $guest["timezone"]    = $time_zone;
        if($guest["country_key"] == ""){
            $guest["country_key"] = $country_key;
        }
        $guest["starttime"]   = date("Y-m-d H:i:s", EZDate::getLocateTime($info["reservation_starttime"],
                                                                          $time_zone, N2MY_SERVER_TIMEZONE));
        $guest["endtime"]     = date("Y-m-d H:i:s", EZDate::getLocateTime($info["reservation_endtime"],
                                                                          $time_zone, N2MY_SERVER_TIMEZONE));
        // メール送信
        if ($send_mail == true && $guest["email"] != "") {
            $this->sendMailToParticipant($guest, $info, $type, $user_info["account_model"]);
        }
        return $guest;
    }
    //{{{ sendMailToParticipant
    public function sendMailToParticipant($guest, $info, $type, $account_model)
    {
        $config   = EZConfig::getInstance(N2MY_CONFIG_FILE);
        $template = new EZTemplate($config->getAll('SMARTY_DIR'));

        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang_cd  = EZLanguage::getLangCd($guest["lang"]);

        $meeting = $this->findMeetingByReservationKey($guest['reservation_key']);
        if (!$meeting) {
            $this->logger->error('find meeting failed');
            return false;
        }

        $teleconf_info   = null;
        $service_option  = new OrderedServiceOptionTable($this->dsn);
        $enable_teleconf = $service_option->enableOnRoom(23, $meeting["room_key"]); // 23 = teleconf

        if ($info["base_url"]) {
            $base_url = $info["base_url"];
        } else {
            $base_url = N2MY_BASE_URL;
        }
        //カスタマイズでセールスを使う場合に、カスタマイズ用のテンプレートを使えるように
        if($info["custom"] == "sales")
        {
            $info["custom"] = $config->get('SMARTY_DIR','custom')?$config->get('SMARTY_DIR','custom'):$info["custom"];
        }

        if (!$config->get("IGNORE_MENU", "teleconference") && $enable_teleconf) {
            require_once("classes/pgi/PGiPhoneNumber.class.php");
            if ($meeting['use_pgi_dialin'] || $meeting['use_pgi_dialin_free'] || $meeting['use_pgi_dialin_lo_call'])  {
                $main_phone_number = unserialize($meeting['pgi_phone_numbers']);
                $main_phone_number = PGiPhoneNumber::toMultilangLocationName($main_phone_number, $lang_cd);
                if (!$meeting['use_pgi_dialin']){
                    $main_phone_number = PGiPhoneNumber::extractFreeMain($main_phone_number);
                } else {
                    $main_phone_number = PGiPhoneNumber::extractLocalMain($main_phone_number);
                }
            }
            $teleconf_info = array('tc_type'             => $meeting['tc_type'],
                                   'pgi_service_name'    => $meeting['pgi_service_name'],
                                   'pgi_conference_id'   => $meeting['pgi_conference_id'],
                                   'pgi_m_pass_code'     => $meeting['pgi_m_pass_code'],
                                   'pgi_p_pass_code'     => $meeting['pgi_p_pass_code'],
                                   'pgi_l_pass_code'     => $meeting['pgi_l_pass_code'],
                                   'main_phone_number'   => $main_phone_number,
                                   'phone_number_url'    => $base_url .'p/'.$meeting['meeting_session_id'],
                                   'tc_teleconf_note'    => $meeting['tc_teleconf_note'],
                                   "use_pgi_dialin"      => $meeting["use_pgi_dialin"],
                                   "use_pgi_dialin_free" => $meeting["use_pgi_dialin_free"],
                                   "use_pgi_dialin_lo_call" => $meeting["use_pgi_dialin_lo_call"],
                                   "use_pgi_dialout"     => $meeting["use_pgi_dialout"]);
        }

        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting["room_key"])) && $meeting["temporary_did"]) {
            $guestSipNumberAddress = $videoConferenceClass->getSipTemporaryNumberAddress($meeting["temporary_did"]);
            $temporarySipAddress = $videoConferenceClass->getSipTemporaryAddress($meeting["temporary_did"]);
            $guestH323NumberAddress = $videoConferenceClass->getH323TemporaryNumberAddress($meeting["temporary_did"]);
            $temporaryH323Address = $videoConferenceClass->getH323TemporaryAddress($meeting["temporary_did"]);
            if ($guestSipNumberAddress)
                $template->assign("temporarySipNumberAddress", $guestSipNumberAddress);
            if ($temporarySipAddress)
                $template->assign("temporarySipAddress", $temporarySipAddress);
            if ($guestH323NumberAddress)
                $template->assign("temporaryH323NumberAddress", $guestH323NumberAddress);
            if ($temporaryH323Address)
                $template->assign("temporaryH323Address", $temporaryH323Address);
        }
        $mail     = new EZSmtp(null, $lang_cd, "UTF-8");
        $subject_id = $this->getSubjectIdByType($type);
        $subject  = $this->getMessage($lang_cd, "DEFINE", $subject_id, $info["custom"]);
        $subject .= " [" . $info['reservation_name'] . "]";


        $mail->setSubject($subject);

        // 送信元サーバのFROM及び、送信先
        $mail->setFrom(RESERVATION_FROM, $info["sender"]);
        $mail->setReturnPath($info["sender_mail"]);
        $mail->setTo($guest["email"], $guest["name"]);
        $template->assign("base_url"     , $base_url);
        $template->assign("info"         , $info);
        $template->assign("guest"        , $guest);
        $template->assign("teleconf_info", $teleconf_info);

        // 電話連携
        $telephone_data = $this->getTelephoneNo($guest["lang"], $type);
        $template->assign("telephone_data", $telephone_data);
        if ($account_model == "free") {
            //VCUBE IDから代理店IDを取得
            $wsdl = $config->get('VCUBEID','wsdl');
            $serverName = $_SERVER["SERVER_NAME"];
            if($serverName == $config->get('VCUBEID','meeting_server_name')){
              //meetingFree
              $consumerKey = $config->get('VCUBEID','meeting_free_consumer_key');
            }elseif($serverName == $config->get('VCUBEID','paperless_server_name')){
              //paperLess
              $consumerKey = $config->get('VCUBEID','paperless_free_consumer_key');
            }
            try {
                require_once("classes/dbi/member_room_relation.dbi.php");
                $objRoomRelation = new MemberRoomRelationTable($this->dsn);
                $where = "room_key = '".$meeting["room_key"]."'";
                $relation_info = $objRoomRelation->getRow($where);
                require_once("classes/dbi/member.dbi.php");
                $objMember = new MemberTable($this->dsn);
                $where = "member_key = ".$relation_info["member_key"];
                $member_info = $objMember->getRow($where, "member_id");
                if ($member_info) {
                    $soap = new SoapClient($wsdl,array('trace' => 1));
                    $response = $soap->getAgencyId($consumerKey, $member_info["member_id"]);
                    $agency_id = $response["agencyId"];
                }
            } catch (Exception $e) {
                $this->logger2->warn($e->getMessage());
            }
            $custom = 'custom/vcubeid/';
            if ($agency_id) {
                $custom_template_file = $custom.$lang_cd."/mail/".$agency_id."/reservation/user/reservation_" . $type . ".t.txt";
            } else {
                $custom_template_file = $custom.$lang_cd."/mail/0/reservation/user/reservation_" . $type . ".t.txt";
            }
            $this->logger->debug($template->template_dir.$custom_template_file);
            if ($custom_template_file && file_exists($template->template_dir.$custom_template_file)) {
                $template_file = $custom_template_file;
            } else {
                $template_file = $lang_cd."/mail/reservation/user/reservation_" . $type . ".t.txt";
            }
        } else if ($info["custom"]) {
            $custom = 'custom/'.$info["custom"].'/';
            $custom_multi_lang_template_file = $custom."common/multi_lang/reservation/user/reservation_".$type.".t.txt";
            if(file_exists($template->template_dir.$custom_multi_lang_template_file)){
                $template_file = $custom_multi_lang_template_file;
            }else{
                $custom_template_file = $custom.$lang_cd."/mail/reservation/user/reservation_" . $type . ".t.txt";
                if (file_exists($template->template_dir.$custom_template_file)) {
                    $template_file = $custom_template_file;
                } else {
                    $template_file = "common/mail_template/meeting/reservation/user/reservation_" . $type . ".t.txt";
                }
            }
        } else {
            $template_file = "common/mail_template/meeting/reservation/user/reservation_" . $type . ".t.txt";
        }
        $obj_User = new UserTable($this->dsn);
        $where = "user_key ='".$info["user_key"]."'";
        $user_info = $obj_User->getRow($where);
        $frame["lang"] = $lang_cd;
        $frame["user_info"] = $user_info;
        $template->assign("__frame",$frame);
        //html化
        if($info["mail_type"] == "html") {
            $mail->setTextBody($template->fetch($template_file));
            if($info["custom"] == "sales") {
                $custom = 'custom/'.$info["custom"].'/';
                $template_file = $custom."common/mail/reservation/user/reservation_".$type.".t.html";
            } else {
                $template_file = "common/mail/reservation/user/reservation_".$type.".t.html";
            }
            $mail->setHtmlBody($template->fetch($template_file));
            $this->logger->debug(array($info["sender_mail"], $guest["email"], $subject, $body, $template_file));
            $mail->setBody($body);
            $mail->sendHtml();
        } else {

            $body          = $template->fetch($template_file);
            $this->logger->debug(array($info["sender_mail"], $guest["email"], $subject, $body, $template_file));
            $mail->setBody($body);
            $mail->send();
        }
    }
   //}}}
    //{{{ getSubjectIdByType
    private function getSubjectIdByType($type){
        switch ($type) {
        case 'create':
            return "RESERVATION_MAIL_SUBJECT";
        break;
        case 'modify':
            return "RESERVATION_MAIL_MODIFY_SUBJECT";
            break;
        case 'deletelist':
            return "RESERVATION_MAIL_DELLIST_SUBJECT";
            break;
        case 'reminder':
            return "RESERVATION_MAIL_REMINDER_SUBJECT";
            break;
        default:
            return '';
            break;
        }
    }
    //}}}
    //{{{ sendMailToOwner
    public function sendMailToOwner($info, $type, $lang, $add_list)
    {
        // 送信者にメールを送る
        $subject_id = $this->getMailSubjectIdByType($type);
        $config     = EZConfig::getInstance();

        $teleconf_info = null;

        $meeting = $this->findMeetingByReservationKey($info['reservation_key']);
        if (!$meeting) {
            $this->logger->error('failed to find meeting');
        }
        $service_option  = new OrderedServiceOptionTable($this->dsn);
        $enable_teleconf = $service_option->enableOnRoom(23, $meeting["room_key"]); // 23 = teleconf

        if ($info["base_url"]) {
            $base_url = $info["base_url"];
        } else {
            $base_url = N2MY_BASE_URL;
        }
        //カスタマイズでセールスを使う場合に、カスタマイズ用のテンプレートを使えるように
        $this->logger->debug($info);
        if($info["custom"] == "sales")
        {
            $info["custom"] = $config->get('SMARTY_DIR','custom')?$config->get('SMARTY_DIR','custom'):$info["custom"];
        }
        $this->logger->debug($info);

        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang_cd = EZLanguage::getLangCd($lang);
        if (!$config->get("IGNORE_MENU", "teleconference") && $enable_teleconf) {
            require_once("classes/pgi/PGiPhoneNumber.class.php");

            if ($meeting['use_pgi_dialin'] || $meeting['use_pgi_dialin_free'] || $meeting['use_pgi_dialin_lo_call'])  {
                $main_phone_number = unserialize($meeting['pgi_phone_numbers']);
                $main_phone_number = PGiPhoneNumber::toMultilangLocationName($main_phone_number, $lang_cd);
                if (!$meeting['use_pgi_dialin']){
                    $main_phone_number = PGiPhoneNumber::extractFreeMain($main_phone_number);
                } else {
                    $main_phone_number = PGiPhoneNumber::extractLocalMain($main_phone_number);
                }
            }

            $teleconf_info = array(
                'tc_type'           => $meeting['tc_type'],
                'pgi_service_name'  => $meeting['pgi_service_name'],
                'pgi_conference_id' => $meeting['pgi_conference_id'],
                'pgi_m_pass_code'   => $meeting['pgi_m_pass_code'],
                'pgi_p_pass_code'   => $meeting['pgi_p_pass_code'],
                'pgi_l_pass_code'   => $meeting['pgi_l_pass_code'],
                'main_phone_number' => $main_phone_number,
                'phone_number_url'  => $base_url.'p/'.$meeting['meeting_session_id'],
                'tc_teleconf_note'  => $meeting['tc_teleconf_note'],
                "use_pgi_dialin"    => $meeting["use_pgi_dialin"],
                "use_pgi_dialin_free" => $meeting["use_pgi_dialin_free"],
                "use_pgi_dialin_lo_call" => $meeting["use_pgi_dialin_lo_call"],
                "use_pgi_dialout"   => $meeting["use_pgi_dialout"],
            );
        }

        $template   = new EZTemplate($config->getAll('SMARTY_DIR'));

        $subject = $this->getMessage($lang_cd, "DEFINE", $subject_id, $info["custom"]);
        $subject = $subject . " [" . $info['reservation_name'] . "]";

        $mail = new EZSmtp(null, $lang_cd, "UTF-8");
        $mail->setSubject($subject);
        $mail->setFrom(RESERVATION_FROM);
        $mail->setReturnPath(NOREPLY_ADDRESS);
        $mail->setTo($info["sender_mail"]);

        // 時差変換
        $starttime = EZDate::getLocateTime($info['reservation_starttime'], $info["reservation_place"], N2MY_SERVER_TIMEZONE);
        $info['reservation_starttime'] = date("Y-m-d H:i:s", $starttime);
        $endtime = EZDate::getLocateTime($info['reservation_endtime'], $info["reservation_place"], N2MY_SERVER_TIMEZONE);
        $info['reservation_endtime']   = date("Y-m-d H:i:s", $endtime);

        $template->assign("base_url"     , $base_url);
        $template->assign("info"    , $info);
        $template->assign("guests"  , $add_list);
        $template->assign("teleconf_info", $teleconf_info);

        // 電話連携
        $telephone_data = $this->getTelephoneNo($lang);
        $template->assign("telephone_data", $telephone_data);

        // polycom
        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting["room_key"])) && $meeting["temporary_did"]) {
            $guestSipNumberAddress = $videoConferenceClass->getSipTemporaryNumberAddress($meeting["temporary_did"]);
            $temporarySipAddress = $videoConferenceClass->getSipTemporaryAddress($meeting["temporary_did"]);
            $guestH323NumberAddress = $videoConferenceClass->getH323TemporaryNumberAddress($meeting["temporary_did"]);
            $temporaryH323Address = $videoConferenceClass->getH323TemporaryAddress($meeting["temporary_did"]);

            if ($guestSipNumberAddress)
                $template->assign("temporarySipNumberAddress", $guestSipNumberAddress);
            if ($temporarySipAddress)
                $template->assign("temporarySipAddress", $temporarySipAddress);
            if ($guestH323NumberAddress)
                $template->assign("temporaryH323NumberAddress", $guestH323NumberAddress);
            if ($temporaryH323Address)
                $template->assign("temporaryH323Address", $temporaryH323Address);
        }

        if ($info["custom"]) {
            $custom = 'custom/'.$info["custom"].'/';
            $custom_multi_lang_template_file = $custom."common/multi_lang/reservation_".$type.".t.txt";
            if(file_exists($template->template_dir.$custom_multi_lang_template_file)){
                $template_file = $custom_multi_lang_template_file;
            }else{
                $custom_template_file = $custom.$lang_cd."/mail/reservation_".$type.".t.txt";
                if (file_exists($template->template_dir.$custom_template_file)) {
                    $template_file = $custom_template_file;
                } else {
                    $template_file = "common/mail_template/meeting/reservation_" . $type . ".t.txt";
                }
            }
        } else {
            $template_file  = "common/mail_template/meeting/reservation_" . $type . ".t.txt";
        }
        $obj_User = new UserTable($this->dsn);
        $where = "user_key ='".$info["user_key"]."'";
        $user_info = $obj_User->getRow($where);
        $frame["lang"] = $lang_cd;
        $frame["user_info"] = $user_info;
        $template->assign("__frame",$frame);
        //html化
        if($info["mail_type"] == "html") {
            $mail->setTextBody($template->fetch($template_file));
            if($info["custom"] == "sales") {
                $custom = 'custom/'.$info["custom"].'/';
                $template_file = $custom."common/mail/reservation_".$type.".t.html";
            } else {
                $template_file = "common/mail/reservation_".$type.".t.html";
            }
            $body = $template->fetch($template_file);
            $mail->setHtmlBody($template->fetch($template_file));
            $this->logger->debug(array($info["sender_mail"], $guest["email"], $subject, $body, $template_file));
            $mail->setBody($body);
            $mail->sendHtml();
        } else {
            $body           = $template->fetch($template_file);
            $this->logger->debug(array(RESERVATION_FROM, $info["sender_mail"], $subject, $body));
            $mail->setBody($body);
            $mail->send();
        }
    }
    //}}}
    /**
     * 自動アドレス帳登録
     */
    function addAddressBook($address_info) {
        $this->logger->debug($address_info);
        // ユーザ取得
        $session        = EZSession::getInstance();
        $user_info      = $session->get("user_info");
        $member_info    = $session->get("member_info");
        $objMember      = new MemberTable($this->dsn);
        $objAddressBook = new AddressBookTable($this->dsn);
        // メンバー一覧に登録されていない
        $where = "user_key = ".$user_info["user_key"].
                 " AND member_status = 0".
                 " AND member_email = '".addslashes($address_info["email"])."'";
        if ($objMember->getRow($where) != 0) {
            return false;
        }

        $data = array(
            "user_key"  => $user_info["user_key"],
            "name"      => $address_info["name"],
            "name_kana" => "",
            "email"     => $address_info["email"],
            "timezone"  => $address_info["timezone"],
            "lang"      => $address_info["lang"],
        );
        // ユーザー
        if (!$member_info) {
            // 共通のアドレス帳に追加されていない
            $where = "user_key = " .$user_info["user_key"].
                     " AND member_key IS NULL".
                     " AND is_deleted = 0".
                     " AND email = '".addslashes($address_info["email"])."'";
            $this->logger->debug(array($where, $data));
            if ($objAddressBook->getRow($where) == 0) {
                $this->logger->debug($objAddressBook->_conn->last_query);
                $objAddressBook->add($data);
            }
            return;
        }

        $data["member_key"] = $member_info["member_key"];
        // 個人、共通のアドレス帳に追加されていない
        $where = "user_key = " .$user_info["user_key"].
                 " AND is_deleted = 0".
                 " AND email = '".addslashes($address_info["email"])."'";
        if ($objAddressBook->getRow($where) == 0) {
            $this->logger->debug(array($where, $data));
            $objAddressBook->add($data);
        }
    }

    /**
     * 複数招待者アドレス帳自動保存
     * @param $guests
     * @param $user_key
     * @param $member_key
     */
    function addAddressBookGuests($guests , $user_key, $member_key = null){
        $objAddressBook = new AddressBookTable($this->dsn);
        foreach($guests as $guest){
            if(!$objAddressBook->recordCheck($guest["email"], $user_key, $member_key)){
                //既に登録されているアドレスだった場合更新にする
                $address_data = $objAddressBook->addressDeleteCheck($guest["email"], $user_key, $member_key);
                if(!$address_data){
                    $data = array(
                            "user_key"       => $user_key,
                            "member_key"     => $member_key,
                            "name"           => $guest["name"],
                            "name_kana"      => "",
                            "email"          => $guest["email"],
                            "lang"           => $guest["lang"],
                            "timezone"       => $guest["timezone"],
                            );
                    $objAddressBook->add($data);
                }else{
                    $data = array(
                            "is_deleted"       => 0,
                            "name"           => $guest["name"],
                            "name_kana"      => "",
                            "lang"           => $guest["lang"],
                            "timezone"       => $guest["timezone"],
                    );
                    $where = "address_book_key = " . $address_data["address_book_key"];
                    $objAddressBook->update($data, $where);
                }
            }
        }
    }

    /**
     * 詳細情報を取得
     */
    public function getDetail($reservation_session) {
        // 会議予約情報取得
        $where = "reservation_session = '".mysql_real_escape_string($reservation_session)."'" .
                 " AND reservation_status = 1";
        $reservation_info = $this->obj_Reservation->getRow($where);
        if (DB::isError($reservation_info)) {
            $this->logger->error($reservation_info->getUserInfo());
            return false;
        } elseif (!$reservation_info) {
            $this->logger->warn($reservation_session, "Reservation not exists!!");
            return false;
        }

        $room_key   = $reservation_info["room_key"];
        $user_id    = $this->_get_user_id($room_key);
        $guests     = $this->getParticipantList($reservation_info["reservation_key"]);// 参加者情報取得

        $_documents = $this->getDocumentList($reservation_info["meeting_key"]); // 事前アップロードファイル取得
        $documents  = array();

        foreach ($_documents as $document) {
            $document_id = $document["document_id"];
            $documents[$document_id] = array(
                "document_id"     => $document_id,
                "name"            => $document["document_name"],
                "type"            => $document["document_extension"],
                "category"        => $document["document_category"],
                "status"          => $document["document_status"],
                "document_index"  => $document["document_index"],
                "sort"            => $document["document_sort"],
                "format"          => $document["document_format"],
                "version"         => $document["document_version"],
                "is_storage"      => $document["is_storage"],
                "create_datetime" => $document["create_datetime"],
                "update_datetime" => $document["update_datetime"],
            );
        }
        //セキュリティ設定取得
        $session   = EZSession::getInstance();
        $user_info = $session->get("user_info");
        $reservation_info["security_pw_flg"] = $user_info["is_compulsion_pw"];
        if (strtotime($reservation_info['reservation_starttime']) < strtotime("now")
            && strtotime($reservation_info['reservation_endtime'])> strtotime("now") ) {
                $status = "now"; // 開催中
        } elseif (strtotime($reservation_info['reservation_endtime']) < strtotime("now")) {
            $status = "end"; // 終了
        } else {
            $status = "wait";// 開催待ち
        }
        $reservation_info["status"] = $status;
        // 時差変換
        $starttime = EZDate::getLocateTime($reservation_info["reservation_starttime"], $reservation_info["reservation_place"],
                                           N2MY_SERVER_TIMEZONE);
        $reservation_info["reservation_starttime"] = date("Y-m-d H:i:s", $starttime);
        $endtime = EZDate::getLocateTime($reservation_info["reservation_endtime"], $reservation_info["reservation_place"],
                                         N2MY_SERVER_TIMEZONE);
        $reservation_info["reservation_endtime"] = date("Y-m-d H:i:s", $endtime);
        // マージ
        $reservation_info["sender"]      = $reservation_info["sender_name"];
        $reservation_info["sender_mail"] = $reservation_info["sender_email"];
        $reservation_info["mail_body"]   = $reservation_info["reservation_info"];

        require_once("classes/dbi/meeting.dbi.php");
        $objMeeting = new MeetingTable($this->dsn);
        $where            = "meeting_ticket = '".addslashes($reservation_info["meeting_key"])."'";
        $meetingInfo      = $objMeeting->getRow($where);
        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"])) && $meetingInfo["temporary_did"]) {
            $reservation_info["h323_temporary_number_address"] = $videoConferenceClass->getH323TemporaryNumberAddress($meetingInfo["temporary_did"]);
            $reservation_info["h323_temporary_address"] = $videoConferenceClass->getH323TemporaryAddress($meetingInfo["temporary_did"]);
            $reservation_info["sip_temporary_number_address"] = $videoConferenceClass->getSipTemporaryNumberAddress($meetingInfo["temporary_did"]);
            $reservation_info["sip_temporary_address"] = $videoConferenceClass->getSipTemporaryAddress($meetingInfo["temporary_did"]);
        }
        $reservation = array("info"      => $reservation_info,
                             "guest_flg" => 1,
                             "documents_dsp" => $documents,
                             "documents" => $documents);
        require_once ("classes/dbi/member.dbi.php");
        //$this->logger->info($room_key);
        $objMember = new MemberTable( $this->dsn );
        $member_info = $objMember->getDetailByRoomKey($room_key);
        //$this->logger->info($member_info);
        if($meetingInfo["use_sales_option"] == 1){
            foreach($guests as $guest) {
                if($guest["member_key"]) {
                    $reservation["staff"] = $guest;
                } elseif ($guest["r_manager_flg"]){
                   $reservation["manager"] = $guest;
                   $reservation["is_manager"] = "1";
                } else {
                   $reservation["guest"] = $guest;
                }
            }
        } else {
            // 主催者抜き出し
            if($reservation_info["is_organizer_flg"] || $reservation_info["is_authority_flg"]){
                foreach($guests as $key => $guest) {
                    if($guest["r_organizer_flg"]){
                        $reservation["organizer"] = $guest;
                        unset($guests[$key]);
                    }
                    if($guest["r_user_authority"]){
                        $reservation["authority"] = $guest;
                        unset($guests[$key]);
                    }
                }
                if(!$reservation["organizer"]){
                    $reservation["organizer"]["name"] = $reservation_info["organizer_name"];
                    $reservation["organizer"]["email"] = $reservation_info["organizer_email"];
                    $reservation["organizer"]["timezone"] = $reservation_info["reservation_place"];
                }
            }
            $reservation["guests"] = $guests;
        }
        $this->logger->debug($reservation);
        return $reservation;
    }

    /*
     * 主催者情報取得
     */
    function getReservationOrganizer($reservation_key){
        $where = "r_organizer_flg = 1 AND reservation_key = " . $reservation_key;
        $organaizer = $this->obj_ReservationUser->getRow($where);
        return $organaizer;
    }

    function getParticipantList($reservation_key) {
        // 参加者情報取得
        $_guests = $this->obj_ReservationUser->getUserList($reservation_key);
        $guests  = array();
        foreach ($_guests as $guest) {
            $guests[$guest["r_user_session"]] = array(
                "member_key"     => $guest["r_member_key"],
                "name"           => $guest["r_user_name"],
                "email"          => $guest["r_user_email"],
                "timezone"       => $guest["r_user_place"],
                "lang"           => $guest["r_user_lang"],
                "type"           => $guest["r_user_audience"],
                "r_user_session" => $guest["r_user_session"],
                "r_organizer_flg"  => $guest["r_organizer_flg"],
                "r_manager_flg"    => $guest["r_manager_flg"],
                "r_user_authority" => $guest["r_user_authority"],
            );
        }
        return $guests;
    }

    private function _get_user_id($room_key) {
        // user_key 取得
        $obj_Room = new RoomTable($this->dsn);
        $where    = "room_key = '".addslashes($room_key)."'";
        $user_key = $obj_Room->getOne($where, "user_key");
        // user_id 取得
        $obj_User = new UserTable($this->dsn);
        $where    = "user_key = ".$user_key;
        $user_id  = $obj_User->getOne($where, "user_id");

        return $user_id;
    }

    function cancel( $reservation_session ) {
        $where = "reservation_session = '".$reservation_session."'";
        $reservation_info = $this->obj_Reservation->getRow($where);

        // ポータルプッシュ
        if ( EZConfig::getInstance()->get( 'VCUBE_PORTAL', 'is_pool_reservation' )
        	&& EZSession::getInstance()->get('service_mode') == 'meeting'
        	&& $vid_info = EZSession::getInstance()->get( 'vid_info' ) ) {
        	require_once 'classes/dbi/reservation_push.dbi.php';
        	$dbi_reservation_push = new ReservationPush( $this->dsn );
        	$dbi_reservation_push->add( ReservationPush::MODE_DELETE, $reservation_info['reservation_key'], $vid_info['vcube_id'], $vid_info['auth_token'], $vid_info['contract_id'] );
        }

        $this->obj_Reservation->cancel( $reservation_session );
        $this->obj_ReservationUser->cancel( $reservation_info["reservation_key"] );
        require_once ( "classes/core/Core_Meeting.class.php" );
        $obj_CoreMeeting = new Core_Meeting( $this->dsn );

        //開始後
        if (strtotime($reservation_info["reservation_starttime"]) < time()) {
            $param = array("room_key"       => $reservation_info["room_key"],
                           "meeting_ticket" => $reservation_info["meeting_key"]);
            //$core_session_key = $obj_CoreMeeting->checkMeetingStatus( $param );
            $objMeeting       = new DBI_Meeting( $this->dsn );
            $meeting_ticket   = $reservation_info["meeting_key"];
            $where            = "meeting_ticket = '".addslashes($meeting_ticket)."'";
            $meetingInfo      = $objMeeting->getRow($where);
            if (!$meetingInfo["meeting_key"]) {
                $this->logger->warn($meeting_key->getUserInfo());
                return false;
            }
            // DNSキーを取得
            $server_list = parse_ini_file( N2MY_APP_DIR."config/server_list.ini", true);
            $this->logger->debug($server_list);
            foreach ($server_list["SERVER_LIST"] as $key => $dns) {
                if ($dns == $this->dsn) {
                    $server_dsn_key = $key;
                    break;
                }
            }
            $url = N2MY_LOCAL_URL."services/api.php" .
                    "?action_stop_meeting=" .
                    "&serverDsnKey=" .$server_dsn_key.
                    "&meeting_ticket=".$meeting_ticket;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            $str = curl_exec($ch);
            curl_close($ch);

            /* @see AMF::stopMeeting
            // polycom option.
            require_once("classes/mcu/resolve/Resolver.php");
            try {
                if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"])) && $meetingInfo["temporary_did"]) {
                    if ($videoInfo = $videoConferenceClass->getActiveVideoConferenceByMeetingKey($meetingInfo["meeting_key"])) {
                        $videoConferenceClass->cancelReservation($videoInfo["conference_id"]);
                    }
                }
            } catch ( Exception $e ) {
            	$this->logger->error($e->getMessage(), 'Exception::cancelReservation');
            }
            */
        }
        else {
            $user_table = new UserTable($this->dsn);
            $user       = $user_table->find($reservation_info['user_key']);
            if (!$user) {
                $this->logger->warn('user not found'.print_r($user , true));
                return false;
            }
            if (!$this->deleteClipRelations($reservation_info['meeting_key'])) {
                $this->logger->error('deleteClipRelations meeting_key:'.$reservation_info['meeting_key']);
                return false;
            }

            $param = array(
                "room_key"          => $reservation_info["room_key"],
                "meeting_ticket"    => $reservation_info["meeting_key"]
                );
            $obj_CoreMeeting->cancelReservation( $param );
        }
    }

    /**
     * 招待者を取得(削除用)
     * @param int $reservation_key
     * @return multitype:配列 招待者リストを中止用に変換
     */
    function getGuestsList($reservation_key) {
      // DBから招待者者取得
        $guests_list = $this->obj_ReservationUser->getUserSimpleList($reservation_key);
        // DBデータを削除用に変換
        $new_guest_list = array();
        foreach ($guests_list as $guest){
            $val = array(
                "r_user_session"=>	$guest["r_user_session"],
                "name"			=>	$guest["r_user_name"],
                "email"			=>	$guest["r_user_email"],
                "timezone"		=>	$guest["r_user_place"],
                "lang"			=>	$guest["r_user_lang"],
                "type"			=> NULL,
            );
            array_push($new_guest_list , $val);
        }
        return $new_guest_list;
    }

    /**
     * 招待者取得
     */
    function getUserData($reservation_key) {

    }

    /**
     * 招待者削除
     */
    function deleteUser() {

    }

    /**
     * パスワード追加
     */
    function setPassword() {

    }

    /**
     * パスワード削除
     */
    function unsetPassword() {

    }
    /**
     * 事前アップロード資料を取得
     */
    function getDocumentList($meeting_key) {
        // ファイル一覧取得
        require_once('classes/core/dbi/Meeting.dbi.php');
        $obj_Meeting = new DBI_Meeting($this->dsn);
        $where       = "meeting_ticket = '".addslashes($meeting_key)."'";
        $meeting_key = $obj_Meeting->getOne($where, "meeting_key");
        $rows        = array();
        if (!$meeting_key) {
            return array();
        }

        $where = "meeting_key = ".$meeting_key.
                 " AND document_mode = 'pre'";
        $rows = $this->obj_N2MYDocument->getList($where);
        return $rows;
    }

    function getDocumentData($meeting_key, $document_id) {
        // ファイル一覧取得
        require_once('classes/core/dbi/Meeting.dbi.php');
        $obj_Meeting = new DBI_Meeting($this->dsn);
        $where       = "meeting_ticket = '".addslashes($meeting_key)."'";
        $meeting_key = $obj_Meeting->getOne($where, "meeting_key");
        // 変換に成功したファイルを取得
        $where = "meeting_key = ".$meeting_key.
                 " AND document_id = '".addslashes($document_id)."'";
                 " AND document_status = ".DOCUMENT_STATUS_SUCCESS;
        $rows = $this->obj_N2MYDocument->getList($where);
        if ($rows) {
            return $rows[0];
        }

        return false;
    }

    /**
     * 資料変換処理
     */
    function addDocument($meeting_ticket, $filepath, $name, $format, $version, $sort) {
        static $document_path;
        if (!isset($document_path[$meeting_ticket])) {
            require_once('classes/core/dbi/Meeting.dbi.php');
            $obj_Meeting  = new DBI_Meeting($this->dsn);
            $where        = "meeting_ticket = '".addslashes($meeting_ticket)."'";
            $meeting_info = $obj_Meeting->getRow($where, "meeting_key,room_key");
            $room_key     = $meeting_info["room_key"];
            $user_id      = $this->_get_user_id($room_key);

            $document_path[$meeting_ticket]["path"]        = $user_id."/".$room_key."/".$meeting_ticket;
            $document_path[$meeting_ticket]["meeting_key"] = $meeting_info["meeting_key"];
        }
        // 部屋一覧取得
        $url = N2MY_LOCAL_URL."/api/document/index.php";
        $post_data = array(
            "action_accept" => "",
            "db_host"       => $this->db_host,
            "name"          => $name,
            "format"        => $format,
            "version"       => $version,
            "document_path" => $document_path[$meeting_ticket]["path"],
            "file"          => "@".$filepath,
            "meeting_key"   => $document_path[$meeting_ticket]["meeting_key"],
            "sort"          => $sort,
            );
        $ch = curl_init();
        $option = array(
            CURLOPT_URL            => $url,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $post_data,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT        => 5,
            );
        $this->logger->debug($option);
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        return $ret;
    }

    /**
     * 資料更新
     */
    function updateDocument($document_id, $document) {
        $this->obj_N2MYDocument->update($document_id, $document);
    }

    /**
     * 資料削除
     */
    function deleteDocument($document) {
        $this->obj_N2MYDocument->delete($document);
    }

    /**
     * 予約入力チェック
     */
    function check($room_key, $reservation_data) {
        // 重複日時チェック
        $info      = $reservation_data["info"];
        $session   = EZSession::getInstance();
        $user_info = $session->get("user_info");

        $starttime = EZDate::getLocateTime($info['reservation_starttime'], N2MY_SERVER_TIMEZONE, $info["reservation_place"]);
        $info['reservation_starttime'] = date("Y-m-d H:i:s", $starttime);

        $endtime = EZDate::getLocateTime($info['reservation_endtime'], N2MY_SERVER_TIMEZONE, $info["reservation_place"]);
        $info['reservation_endtime']   = date("Y-m-d H:i:s", $endtime);

        $message = $this->obj_Reservation->check($room_key, $info);
        // メールアドレスチェック
        if ($reservation_data["guests"] && $reservation_data["guest_flg"]) {
            $message .= $this->obj_ReservationUser->check($reservation_data["guests"]);
        }
        // 最大参加者数
        if( $user_info['use_port_plan'] && $user_info['entry_mode'] ){

            // マネージャ途中入室の場合もしくは、マネージャ予約に含めた場合、3人で計算をする
            $reservation_info_meeting = $this->obj_Reservation->getRow("reservation_key = '".$info["reservation_key"]."'");
            if($reservation_info_meeting["is_manager"] == 2 || ($info["is_manager"]  && $session->get("service_mode") == "sales")){
                // S&S 監視者参加の場合、ポート数は3
                $info['max_port'] = 3;
            }

        	// 残ポートチェック
        	$check_maxport = $this->checkMaxUsePort($starttime, $endtime, $info["reservation_session"]);
	        if( !is_numeric($info['max_port']) || $info['max_port'] < 2 || 50 < $info['max_port']
	        	|| ($user_info["max_connect_participant"] - $check_maxport) < $info["max_port"] ){
	        	$message .= '<li>'.RESERVATION_ERROR_OVERPORT .'</li>';
	        }
        }
        return $message;
    }

    //旧reservation.class check
    public function checkReservationInfo( $r_user_session )
    {
        $sql = "SELECT * FROM reservation r" .
               ", reservation_user ru" .
               " WHERE r_user_session = '$r_user_session'" .
               " AND r.reservation_key = ru.reservation_key";
        $db = EZDB::connect($this->dsn);
        $rs = $db->query($sql);
        if (DB::isError($rs)) {
            $this->logger->error($reservation_info->getUserInfo());
            return false;
        }

        return $rs->fetchRow(DB_FETCHMODE_ASSOC);
    }

    /**
     * タイムゾーン変換
     * Unixタイムスタンプ形式で返す
     * @param unixtime $datetime
     * @param integer $convert_tz
     * @param integer $original_tz デフォルトサーバ時間
     */

    public function convTimeZone($datetime, $convert_tz, $original_tz = null) {
        if (!is_numeric($datetime)) {
            $datetime = strtotime($datetime);
        }
        if ($original_tz === null) {
            $original_tz = $this->serverTimeZone;
        }
        $ret = $datetime - ($original_tz - $convert_tz) * 3600;
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                             "original_start" => $convert_tz,
                             "original_end"   => $original_tz,
                             "server_start"   => date('Y-m-d H:i:s',$datetime),
                             "server_end"     => date('Y-m-d H:i:s',$ret),
                            ));
        return $ret;
    }
    //{{{ getMailSubjectIdByType
    private function getMailSubjectIdByType($type) {
        switch ($type) {
        case "create":
            return "RESERVATION_OWNER_SUBJECT";
            break;
        case "modify":
            return "RESERVATION_OWNER_MODIFY_SUBJECT";
            break;
        case "delete" :
            return "RESERVATION_OWNER_DELETE_SUBJECT";
            break;
        case "addlist":
            return "RESERVATION_OWNER_ADDLIST_SUBJECT";
            break;
        case "deletelist":
            return "RESERVATION_OWNER_DELLIST_SUBJECT";
            break;
        case "modify_pw" :
            return "RESERVATION_OWNER_MODIFY_PW_SUBJECT";
            break;
        case "reminder" :
            return "RESERVATION_OWNER_REMINDER_SUBJECT";
            break;

        default:
            return "";
            break;
        }
    }
    //}}}
    //{{{ senderMail
    public function sendMailWithBackground($mail_info)
    {

        $session        = EZSession::getInstance();
        $user_info      = $session->get("user_info");

        if ($user_info["login_url"]) {
            $mail_info['info']['base_url'] = $user_info["login_url"];
            $mail_info['info']['custom'] = $mail_info['info']['custom']?$mail_info['info']['custom']:$user_info["custom"];
        } else {
            $mail_info['info']['base_url'] = N2MY_BASE_URL;
        }
        $mail_info['account_model'] = $user_info["account_model"];
        $mail_info['info']['guest_url_format'] = $user_info["guest_url_format"];
        $url   = N2MY_LOCAL_URL."services/reservation/mail.php";
        $ch    = curl_init();
        $posts = array('action_send_mail' => '',
                       'mail_info'        => serialize($mail_info));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $posts);
        $str = curl_exec($ch);
        curl_close($ch);
    }
    //}}}
    //{{
    private function findMeetingByReservationKey($reservation_key)
    {
        $where       = "reservation_key = '".mysql_real_escape_string($reservation_key)."'";
        $reservation = $this->obj_Reservation->getRow($where);
        if (PEAR::isError($reservation)) {
            $this->logger->error('get reservation faild PEAR said : '.$reservation->getUserInfo());
            return false;
        }
        if (!$reservation) {
            $this->logger->warn('reservation not found key:'.$reservation_key);
            return false;
        }
        require_once('classes/core/dbi/Meeting.dbi.php');
        $meeting_table = new DBI_Meeting($this->dsn);
        $meeting       = $meeting_table->getRow("meeting_ticket = '".
                                                addslashes($reservation['meeting_key'])."'");
        if (PEAR::isError($meeting)) {
            $this->logger->error('get meeting faild PEAR said : '.$meeting->getUserInfo());
            return false;
        }

        if (!$meeting) {
            $this->logger->warn('meeting not found key:'.$meeting);
            return false;
        }

        return $meeting;
    }
    //
    function addClipRelations($meeting_ticket, $clips, $user_key)
    {
        if (count($clips) <= 0) {
            return true;
        }
        require_once ("classes/N2MY_Clip.class.php");
        require_once ("classes/dbi/clip.dbi.php");
        $meeting_clip_table = new MeetingClipTable($this->dsn);
        $clip_table         = new ClipTable($this->dsn);
        $meeting_table      = new MeetingTable($this->dsn);
        try {
            $_meeting_key  = $meeting_table->findMeetingKeyByMeetingTicket($meeting_ticket);
//            $clips         = $clip_table->activeUsersClipKeysIn($clips, $user_key);
            $meeting_clip_table->addClipsToMeeting($clips, $_meeting_key);
        } catch(Exception $e) {
            $this->logger->error('failed clip relation / '.$e->getMessage().', meeting_key:'.
                                 $meeting_key.', clip_keys:',print_r($clips,true));
            return false;
        }

        return true;
    }
    private function restoreClipRelations($meeting_ticket, $clips, $user_key)
    {
        require_once ("classes/N2MY_Clip.class.php");
        require_once ("classes/dbi/clip.dbi.php");
        $n2my_clip     = new N2MY_Clip($this->dsn);
        $meeting_table = new MeetingTable($this->dsn);
        $clip_table    = new ClipTable($this->dsn);
        try {
            $meeting_key = $meeting_table->findMeetingKeyByMeetingTicket($meeting_ticket);
//            $clips       = $clip_table->activeUsersClipKeysIn($clips, $user_key);
            $n2my_clip->restoreRelations($meeting_key , $clips);
        } catch (Exception $e) {
            $this->logger->error('restoreClipRelations failed '.$e->getMessage().' meeting_ticket:'.$meeting_ticket.'
                                 clip_keys:', print_r($clips,true));
            return false;
        }
        return true;
    }
    private function deleteClipRelations($meeting_ticket)
    {
        require_once ("classes/N2MY_Clip.class.php");
        $meeting_table      = new MeetingTable($this->dsn);
        $meeting_clip_table = new MeetingClipTable($this->dsn);
        try {
            $_meeting_key = $meeting_table->findMeetingKeyByMeetingTicket($meeting_ticket);
            $meeting_clip_table->deleteByMeetingKey($_meeting_key);
        } catch (Exception $e) {
            $this->logger->error(__FUNCTION__." ".__LINE__." ".$e->getMessage());
            return false;
        }
        return true;
    }

  public function getReservationByTicket($ticket) {
    $where ="meeting_key = '".mysql_real_escape_string($ticket)."'".
        " AND reservation_status = 1";
    return $this->obj_Reservation->getRow($where);
  }

  public function getReminderList(){
      // 会議開始時間が1時間以上2時間以内前の会議取得
      $hour1 = date('Y-m-d H:i:s' , strtotime("+ 1 hour"));
      $hour2 = date('Y-m-d H:i:s' , strtotime("+ 2 hour"));
      $where = "reservation_status = 1 AND is_reminder_flg = 1 AND is_reminder_send_flg != 1 AND reservation_starttime > '".$hour1 . "' AND reservation_starttime <= '".$hour2."'";
      //return $now;
      // 予約一覧取得
      $reservation_list = $this->obj_Reservation->getList($where);
      $reservation_count = count($reservation_list);
      $user_db = new UserTable($this->dsn);
      require_once("classes/dbi/meeting.dbi.php");
      $objMeeting = new MeetingTable($this->dsn);
      for($i = 0; $i < $reservation_count; $i++){
          // 招待者取得
          $reservation_list[$i]["guests"] = $this->getParticipantList($reservation_list[$i]["reservation_key"]);
          //議長情報
          if($reservation_list[$i]["is_authority_flg"]) {
              foreach ($reservation_list[$i]["guests"] as $guest) {
                  if($guest["r_user_authority"]) {
                      $reservation_list[$i]["authority"] = $guest;
                  }
              }
          }
          // ユーザー情報取得
          $reservation_list[$i]["user_info"] = $user_db->getRow("user_key = " . $reservation_list[$i]["user_key"]);
          // 会議情報取得
          $meeting_info = $objMeeting->getRow("meeting_ticket = '".addslashes($reservation_list[$i]["meeting_key"])."'");
          $reservation_list[$i]["pin_cd"] = $meeting_info["pin_cd"];
          $reservation_list[$i]["meeting_country"] = $meeting_info["meeting_country"];
          //部屋のオプション確認
          require_once("classes/N2MY_Account.class.php");
          $obj_N2MY_Account = new N2MY_Account($this->dsn);
          $room_option      = $obj_N2MY_Account->getRoomOptionList($reservation_list[$i]["room_key"]);
          $reservation_list[$i]["room_option"] = $room_option;
      }
      return $reservation_list;
  }
  /**
   * 指定期間中に使用しているポート数（max_port）を返す
   * @param $start_time 探索開始日時（UNIXタイムスタンプ）
   * @param $end_time 探索終了日時（UNIXタイムスタンプ）
   * @param $reservation_session=null （予約変更の場合）変更対象の予約。探索対象から除外される。
   */
    function checkMaxUsePort($start_time , $end_time , $reservation_session = null){

        $session   = EZSession::getInstance();
        $user_info = $session->get("user_info");
        $user_key  = $user_info["user_key"];
        $ganttChartsArray = $this->ganttCharts($start_time ,$end_time , $user_key , $reservation_session);
        $max_use_port = $this->checkUsePort($end_time ,$ganttChartsArray);

        return $max_use_port;
    }

  /**
   * 契約上限を超えている時間帯を返す
   */
    function checkPortOver($max_port, $ganttChartsArray){
        $overArray = array();
        $active = 0;
        $count_port = 0;

        // 時間内のポート数調査
        foreach( $ganttChartsArray as $time => $port ){
            $active += $port;
            if($max_port < $active){
                $overArray[$count_port] = $time;
                $count_port++ ;
            }
        }
        return $overArray;
    }


  /**
   * 指定された時間帯の予約ポート数を返す
   */
    function checkUsePort($end_time, $ganttChartsArray){
        $max_active = 0;
        $active = 0;

        // 時間内のポート数調査
        foreach( $ganttChartsArray as $time => $port ){
            if($end_time <= $time){
                break;
            }

            $active += $port;
            if($max_active < $active){
                $max_active = $active;
            }
        }

        if(!$max_active){
            // 指定された時間内に新規の会議がない場合
            $use_port = $active;
        }else{
            $use_port = $max_active;
        }
        return $use_port;
    }

    function ganttCharts($search_time=null , $end_time=null ,$user_key, $reservation_session=null){
        $timeline = array();
        $my_search_time =date("Y-m-d H:i:s" , $search_time);
        $my_search_end_time =date("Y-m-d H:i:s" , $end_time);

        $where = " reservation_status = 1
        AND user_key = '". $user_key . "'
        AND reservation_endtime > '". $my_search_time ."'";

        if($reservation_session)
            $where .= " AND reservation_session <> '".$reservation_session."'";

        $reservation = $this->obj_Reservation->getList($where);

        foreach ( $reservation as $reservation_info ){
                $start = strtotime ( $reservation_info['reservation_starttime'] );
                $end   = strtotime ( $reservation_info['reservation_endtime'] );
                $max_port = $reservation_info['max_port'];
                $timeline[ $start ] += $max_port;
                $timeline[ $end ]   -= $max_port;
        }
        ksort($timeline);
        return $timeline;
    }
}