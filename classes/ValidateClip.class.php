<?php

class ValidateClip
{
    private $errors   = array();
    private $MAX_TITLE_LENGTH = 30;
    private $MAX_DESC_LENGTH  = 200;

    //{{ 
    //public function run($file_path, $title)
    public function forCreate($params , $file_path)
    {
        if (!$file_path) {
            $this->addError(CLIP_ERROR_FILE);
        }
        $config = EZConfig::getInstance();
        if ($config->get('VIDEO','max_filesize') < filesize($file_path)) {
            $this->addError(CLIP_ERROR_FILE_SIZE);
        }

        $this->forEdit($params);
    }
    public function forEdit($params)
    {
        $config = EZConfig::getInstance();
        if (!$params['title']) {
            $this->addError(CLIP_ERROR_TITLE);
        } else {
            if ($this->MAX_TITLE_LENGTH < mb_strlen($params['title'])) {
                $this->addError(CLIP_ERROR_TITLE_LENGTH);
            }
        }
        if ($params['description'] && ($this->MAX_DESC_LENGTH < mb_strlen($params['description']))) {
            $this->addError(CLIP_ERROR_DESC_LENGTH);
        }
    }
    public function addError($error_no)
    {
        $this->errors[] = $error_no;
    }
    public function hasError()
    {
        return 0 < count($this->errors);
    }
    public function getErrors()
    {
        return $this->errors;
    }
}
