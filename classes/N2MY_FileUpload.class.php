<?php

require_once("classes/dbi/meeting.dbi.php");
require_once("lib/EZLib/EZCore/EZLogger2.class.php");

class N2MY_FileUpload
{
    function __construct()
    {
        $this->serverList = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true );
        list( $key, $dsn ) = each( $this->serverList["SERVER_LIST"] );
        $this->dsn = $dsn;
        $this->logger =& EZlogger2::getInstance();
    }

    public function makeDir( $meeting_key )
    {
        $dir = $this->getDir( $meeting_key );
        $fullDir = $this->getFullPath( $dir );
        if( false !== $fullDir && ! is_dir( $fullDir ) ){
            EZFile::mkdir_r( $fullDir, 0777 );
            chmod( $fullDir , 0777 );
        }
        return $fullDir;
    }

    public function getDir( $meeting_key )
    {
        $objMeeting = new MeetingTable( $this->dsn );
        $meetingInfo = $objMeeting->getRow( sprintf( "meeting_key='%s'", $meeting_key ) );
        $meeting_id = $objMeeting->getMeetingID($meeting_key);
        $dateInfo = date_parse( $meetingInfo["create_datetime"] );
        //$this->logger->info( array(sprintf( "meeting_key='%s'", $meeting_key ), $meeting_id, $meetingInfo, $dateInfo));
        if( ! $dateInfo["error_count"] ){
            return sprintf( "%04d%02d/%02d/%s/", $dateInfo["year"], $dateInfo["month"], $dateInfo["day"], $meeting_id );
        } else {
            $this->logger->info( $meeting_key );
            return false;
        }
    }

    public function getInfo($cabinet_id) {
        require_once("classes/dbi/file_cabinet.dbi.php");
        $objFileCabinet = new FileCabinetTable($this->get_dsn());
        return $objFileCabinet->getRow("tmp_name = '.$cabinet_id.'");
    }

    public function getFullPath($dir, $name = null)
    {
        if ($name) {
            return sprintf( "%s%s%s", N2MY_FILES_DIR, $dir, $name );
        } else {
            return sprintf( "%s%s", N2MY_FILES_DIR, $dir );
        }
    }

    public function moveFile( $destFile, $targetFile )
    {
        if( is_uploaded_file( $targetFile ) && move_uploaded_file( $targetFile, $destFile ) ){
            return true;
        } else {
            $this->logger->info( "move: error" );
            return false;
        }
    }

    public function checkFile( $destDir, $fileName )
    {
        $destFile = sprintf( "%s%s", $destDir, $fileName );

        if( file_exists( $destFile ) ){
            return true;
        } else {
            $this->logger->warn( $destFile );
            return false;
        }
    }

    public function deleteDir( $target_dir )
    {
        $this->logger->info( $target_dir );
        if( file_exists( $target_dir ) ) {
            if (is_file($target_dir)) {
                unlink($target_dir);
            } elseif (is_dir($target_dir)) {
                EZFile::rmtree($target_dir);
            }
        }
    }
}
