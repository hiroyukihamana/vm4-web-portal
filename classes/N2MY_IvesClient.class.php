<?php
// require_once("classes/mgm/dbi/mcu_server.dbi.php");
require_once("lib/EZLib/EZCore/EZLogger2.class.php");
require_once('lib/EZLib/EZCore/EZConfig.class.php');
require_once("classes/mcu/config/McuConfigProxy.php");

class N2MY_IvesClient
{
    var $logger2 = "";
    var $client  = null;
    var $guestClient	= null;
    var $config  = null;
    private $_dsn;
    function N2MY_IvesClient($domain=null){
        $this->logger2 = & EZLogger2::getInstance();
        $this->config	= EZConfig::getInstance(N2MY_CONFIG_FILE);
        $this->configProxy 	= new McuConfigProxy();
        if ($domain)
        	$this->setClient($domain);
        else if (defined("WSDL_DOMAIN"))
        	$this->setClient(WSDL_DOMAIN);
    }
    
    public function setClient($domain) {
    	$this->client	= new SoapClient(sprintf($this->configProxy->get("create_client"), $domain));
    	$this->guestClient = new SoapClient(sprintf($this->configProxy->get("guestWsdl"), $domain));
    }
    
    public function setWsdlDomain($wsdlDomain) {
    	$this->client	= null;
    	$this->guestClient = null;

    	$this->setClient($wsdlDomain);
    }
    
    public function createGuestDid($confId, $callbackurl) {
    	$param = array(
				"confId"=>$confId,
				"didPrefix"=>$this->configProxy->get("guestPrefix"),
				"didLength"=>$this->configProxy->get("guestDidLength"),
				"callbackUrl"=>$callbackurl
				);
		try {
    		return $this->guestClient->createGuestDid($param);
		}
		catch (SoapFault $fault) {
			throw new Exception("createGuestDid: ".$fault);
		}
    }
    
    public function createTemplateGuestDid($did, $callbackurl) {
    	$param = array(
    			"templDid"=>$did,
    			"didPrefix"=>$this->configProxy->get("guestPrefix"),
    			"didLength"=>$this->configProxy->get("guestDidLength"),
    			"callbackUrl"=>$callbackurl,
    			"createOnConf"=>true
    	);
    	try {
    		return $this->guestClient->createTemplateGuestDid($param);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("createTemplateGuestDid: ".$fault);
    	}
    }
    
    public function deleteTemplateGuestDid($guestDid) {
    	try {
    		$params = array("guestDid"=>$guestDid);
    		$this->guestClient->deleteTemplateGuestDid($params);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("deleteTemplateGuestDid: ".$fault);
    	}
    }

    public function reRegistMngrListener() {
    }
    
    //}}}
    public function callCreateConferenceExt($did) {
        try {
            $params =  array(
                "name"        => $this->configProxy->get("conference_name"),
                "did"         => $did,
                "mixerId"     => $this->configProxy->get("mixerId"),
                "size"        => $this->configProxy->get("size"),
                "compType"    => 0,
                "profileId"   => $this->configProxy->get("profileId"),
                "audioCodecs" => "",
                "videoCodecs" => "",
                "textCodecs"  => "",
                "autoAccept"  => false,
                "callbackURL" => sprintf($this->configProxy->get("callback_conference"), $this->configProxy->get("soap_domain"))
            );
            $conf = $this->client->createConferenceExt($params);
        	return $conf;
        } catch (SoapFault $fault) {
            throw new Exception("SoapFault:callCreateConferenceExt - ".$fault);
        }
    }

    public function callCreateConferenceTemplateExt($params) {
    	try {
    		$conf = $this->client->addConferenceAdHocTemplate($params);
    	} catch (SoapFault $fault) {
    		throw new Exception($fault.", SoapFault:callCreateConferenceTemplateExt. did: ".$did);
    	}
    	return $conf;
    }
    
    public function calRemoveConferenceTemplateExt($did) {
    	try {
    		$params = array(
    				"did"=>$did);
    		$conf = $this->client->removeConferenceAdHocTemplate($params);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:calRemoveConferenceTemplateExt - ".$fault);
    	}
    	return $conf;
    }
    
    //}}}
    public function callRemoveConference($confId) {
        try {
            $params =  array(
                "confId"  => $confId
            );
            $this->client->removeConference($params);

        } catch (SoapFault $fault) {
            throw new Exception("SoapFault:callRemoveConference - ".$fault);
        }
    }

    //}}}
    public function callCreateMosaic($params) {
        try {
            $mosaic = $this->client->createMosaic($params);

        } catch (SoapFault $fault) {
            throw new Exception("SoapFault:callCreateMosaic - ".$fault);
        }

        return $mosaic;
    }

    public function callGetConference($confId) {
        try {
            $params = array("confId" => $confId);
            $this->logger2->info($params);
            $conf = $this->client->getConference($params);

        } catch (SoapFault $fault) {
            throw new Exception("SoapFault:callGetConference - ".$fault);
        }

        return $conf;
    }

    public function callGetConferenceByDid($params) {
        try {
            $this->logger2->info($params);
            $return = $this->client->getConferenceByDID($params);
        }
        catch (SoapFault $fault) {
            throw new Exception("SoapFault:callGetConferenceByDid - ".$fault);
        }
        return $return;
    }

    public function createDid($ivesSettingTable)
    {
        try{
            do {
                $did = rand(1000000, 9999999);
                $res   = $ivesSettingTable->findByDid($did);
                if (PEAR::isError($res)) {
                    throw new Exception("select ives_setting did failed PEAR said : ".$did);
                }
                if ($res) {
                    continue;
                }
                break;
            } while ($loop = true);

        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            throw new Exception("SoapFault:createDid - ".$fault);
        }

        return $did;
    }
    
    public function setFlashComposition($flashCompositionParams) {
    	try {
    		$this->client->setCompositionType($flashCompositionParams);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:setFlashComposition - ".$fault);
    	}
    }
    
    public function setPolycomComposition($polycomCompositionParams) {
    	try {
    		$this->client->setCompositionType($polycomCompositionParams);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:setPolycomComposition - ".$fault);
    	}
    }
    
    public function acceptParticipant($acceptParams) {
    	try {
    		$this->client->acceptParticipant($acceptParams);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:acceptParticipant - ".$fault);
    	}
    }
    
    public function addMosaicParticipant($mosaicParams) {
    	try {
    		$this->client->addMosaicParticipant($mosaicParams);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:addMosaicParticipant - ".$fault);
    	}
    }
    
    public function rejectParticipant($rejectParams) {
    	$this->logger2->info($rejectParams);
    	try {
        	$this->client->rejectParticipant($rejectParams);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:rejectParticipant - ".$fault);
    	}
    }
    
    public function removeMosaicParticipant($params) {
    	$this->logger2->info($params);
    	try {
        	$this->client->removeMosaicParticipant($params);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:removeMosaicParticipant - ".$fault);
    	}
    }
    
    public function removeParticipant($removeParam) {
    	$this->logger2->info($removeParam);
    	try {
        	$this->client->removeParticipant($removeParam);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:removeParticipant - ".$fault);
    	}
    }
    
    public function changeProfile($profileParams) {
    	try {
        	$this->client->changeParticipantProfile($profileParams);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:changeProfile - ".$fault);
    	}
    }
    
    public function getConferences() {
    	try {
    		return $this->client->getConferences();
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:getConferences - ".$fault);
    	}
    }
    
    public function setMosaicOverlayImage($params) {
    	try {
    		return $this->client->setMosaicOverlayImage($params);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:setMosaicOverlayImage - ".$fault);
    	}
    }
    
    public function getParticipants($params) {
    	try {
    		return $this->client->getParticipants($params);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:getParticipants - ".$fault);
    	}
    }
    
    public function createSidebar($confId) {
    	$params = array("confId"=>$confId);
    	try {
    		return $this->client->createSidebar($params);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:createSidebar - ".$fault);
    	}
    }
    
    public function addSidebarParticipant($params) {
    	try {
    		return $this->client->addSidebarParticipant($params);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:addSidebarParticipant - ".$fault);
    	}
    }
    
    public function setMosaicSlot($params) {
    	try {
    		return $this->client->setMosaicSlot($params);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:setMosaicSlot - ".$fault);
    	}
    }
    
    public function callParticipant($params) {
    	try {
    		return $this->client->callParticipant($params);
    	}
    	catch (SoapFault $fault) {
    		throw new Exception("SoapFault:callParticipant - ".$fault);
    	}
    }
}
