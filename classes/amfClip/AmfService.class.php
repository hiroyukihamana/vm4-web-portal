<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

class AmfService
{
    /**
     * コンストラクタ
     *
     * @access public
     * @param
     * @return
     */
    function AmfService()
    {
        //send header
        header("Content-Type: text/html; charset=UTF-8");
    }

    /**
     * デストラクタ
     *
     * @access public
     * @param
     * @return
     */
    function _AmfService()
    {
    }

    /**
     * コールされメソッドを登録
     *
     * @access public
     * @param  string  $method 実行するクラスメソッド名
     * @param  array   $args   実行するクラスメソッド引数
     * @return
     */
    function _addCalledMethod($method, $args)
    {
        $this->methodTable[$method] = array(
                                            'description' => "",
                                            'access'      => "remote",
                                            'arguments'   => $args
                                           );
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
