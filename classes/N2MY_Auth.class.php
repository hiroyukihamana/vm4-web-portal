<?php
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/user.dbi.php");

class N2MY_Auth{

    private $logger = "";
    private $user = "";
    private $member = "";

    function N2MY_Auth($dsn){
        $this->logger =& EZLogger::getInstance();
        $this->user = new UserTable($dsn);
        $this->member = new MemberTable($dsn);
    }

    /**
     * ログイン処理
     *
     * @param string $login_id ログインID
     * @param string $login_pw ログインパスワード
     * @param boolean $pw_encode パスワードの暗号化（MD5,sha1ハッシュ）
     * @param api_key $api_key
     * @return mixed 成功時:ユーザorメンバー情報、エラー時:false
     */
    function check($login_id, $login_pw, $pw_encode = "", $api_key = "", $login_type = ""){
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__, array($login_id, $login_pw));
        // 未入力チェック
        if(!$login_id || !$login_pw){
            $this->logger->warn(__FUNCTION__."#not exists user_id or password", __FILE__, __LINE__, array(
                "login_id" => $login_id,
                "login_pw" => $login_pw
                ));
            return false;
        }
        // ユーザアカウントでチェック
        $user_info = $this->user->checkLogin($login_id, $login_pw, $pw_encode, $api_key, $login_type);
        if (DB::isError($user_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
            return false;
        } elseif ($user_info) {
            // ユーザログイン
            return array(
                "user_info" => $user_info,
                "member_info" => null,
                );
        } elseif ("centre_admin_login" == $login_type) {
            //centre管理者ページからはメンバーログイン不可
            return false;
        } else {
            // メンバー取得
            $member_info = $this->member->checkLogin($login_id, $login_pw, $pw_encode, $api_key);
            if (DB::isError($member_info)) {
                $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $member_info->getUserInfo());
                return false;
            }
            // ユーザの有効期限確認
            $user_where = " user_key = '".addslashes($member_info["user_key"])."'" .
                    " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                    " AND user_delete_status < 2";
            $user_info = $this->user->getRow($user_where);
            if (DB::isError($user_info)) {
                $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
                return false;
            } else {
                if ($member_info && $user_info) {
                    // メンバーログイン
                    $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array($user_info, $member_info));
                    return array(
                        "user_info" => $user_info,
                        "member_info" => $member_info
                        );
                }
            }
        }
        // アカウントが存在しない
        return false;
    }

    /**
     * メンバー課金招待 メールからのログイン処理
     *
     * @param string $login_id ログインID
     * @param string $login_pw ログインパスワード
     * @param string $user_id ユーザーID
     * @param boolean $pw_encode パスワードの暗号化（MD5,sha1ハッシュ）
     * @param api_key $api_key
     * @return mixed 成功時:メンバー情報、エラー時:false
     */
    function checkMember($login_id, $login_pw, $user_key, $pw_encode = "", $api_key = "", $external_member_invitation_flg = ""){
      $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__, array($login_id, $login_pw));
      // 未入力チェック
      if(!$login_id || !$login_pw || !$user_key){
        $this->logger->warn(__FUNCTION__."#not exists user_id or password or user_id", __FILE__, __LINE__, array(
            "login_id" => $login_id,
            "login_pw" => $login_pw,
            "user_id"  => $user_key,
        ));
        return false;
      }
      // メンバー取得
      $member_info = $this->member->checkMemberLogin($login_id, $login_pw, $user_key, $pw_encode, $api_key,$external_member_invitation_flg);
      if (DB::isError($member_info)) {
        $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $member_info->getUserInfo());
        return false;
      }
      if($member_info){
        return $member_info;
      }
      return false;

    }

    /**
     * VCUBE IDログイン処理
     *
     */
    function checkVcubeId($login_id){
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__, array($login_id));
        // 未入力チェック
        if(!$login_id){
            $this->logger->warn(__FUNCTION__."#not exists user_id or password", __FILE__, __LINE__, array(
                "login_id" => $login_id,
                ));
            return false;
        }
        // ユーザアカウントでチェック
        // メンバー取得
        $where = " member_id = '".addslashes($login_id)."'".
            " AND member_status = 0";
        $member_info = $this->member->getRow($where);
        if (DB::isError($member_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $member_info->getUserInfo());
            return false;
        }
        // ユーザの有効期限確認
        $user_where = " user_key = '".addslashes($member_info["user_key"])."'" .
                " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                " AND user_delete_status < 2";
                
        $user_info = $this->user->getRow($user_where);
        if (DB::isError($user_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
            return false;
        } else {
            if ($member_info && $user_info) {
                // メンバーログイン
                // $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array($user_info, $member_info));
                return array(
                    "user_info" => $user_info,
                    "member_info" => $member_info
                    );
            }
        }
        // アカウントが存在しない
        return false;
    }

    /**
     * メンバー課金利用の際メンバーキーのみで情報を取得
     */
    public function getMemberInfo( $member_key )
    {
        $where = sprintf( "member_key=%s AND member_status = 0;", $member_key );
        $member_info = $this->member->getRow( $where );
        // メンバー取得
        if (DB::isError($member_info) || !$member_info ) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $member_info->getUserInfo());
            return false;
        }
        // ユーザの有効期限確認
        $user_where = " user_key = '".addslashes($member_info["user_key"])."'" .
                " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                " AND user_delete_status < 2";
        $user_info = $this->user->getRow( $user_where );
        if (DB::isError($user_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
            return false;
        } else {
            // メンバーログイン
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array($user_info["user_id"], $member_info["member_id"]));
            return array(
                "user_info" => $user_info,
                "member_info" => $member_info
                );
        }
    }

    /**
     * ログイン処理
     * 普通のユーザーとメンバーとONE全て含める
     * Phase 1 : API経由(4.9.10.4)
     * Phase 2 : WEBにも適用(4.10.0.0)
     * @param string $login_id ログインID
     * @param string $login_pw ログインパスワード
     * @param boolean $pw_encode パスワードの暗号化（MD5,sha1ハッシュ）
     * 
     */
     public function checkLogin($login_id, $login_pw, $pw_encode = "", $api_key = "", $login_type = ""){
        // 未入力チェック
        if(!$login_id || !$login_pw){
            $this->logger->warn(__FUNCTION__."#not exists user_id or password", __FILE__, __LINE__, array(
                "login_id" => $login_id,
                "login_pw" => $login_pw)
            );
            return false;
        }
        // ユーザアカウントでチェック
        if($login_type == "centre_admin_login"){
            $this->logger->warn(__FUNCTION__."#centre admin login not allowed", __FILE__, __LINE__, $login_type);
            return false;
        }
        // ユーザログイン
        $user_info = $this -> user -> checkLogin($login_id, $login_pw, $pw_encode, $api_key, $login_type);
        if (DB::isError($user_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
            return false;
        } elseif ($user_info) {
            return array(
                "user_info"   => $user_info,
                "member_info" => null,
            );
        }
        // V-CUBE ID ログイン
        $token_info = $this -> checkToken($login_id, $login_pw, $pw_encode);
        if($token_info['result'] === true){
            $this->logger->info('###TOKEN INFO###', __FILE__, __LINE__, $token_info);
            //複数vcubeIdが存在すれば、いつばん最初追加されたvcubeIdが戻ります
            $member_id = $token_info['contracts'][0]['member_id'];
            $vid_info  = $this -> checkVcubeId($member_id);
            if($vid_info){
            	$contract = $token_info['contracts'][0];
            	$member_info = &$vid_info['member_info'];
            	$member_info['is_vid_login'] = true;
            	$member_info['role'] = $contract['role'];
            	$member_info['is_portal_header'] = $contract['is_portal_header'];
            	$member_info['contract_id'] = $contract['contract_id'];
            	$vid_info['vIdAuthToken'] = $token_info['vIdAuthToken'];
            	return $vid_info;
            }
        }
        // メンバー取得
        $member_info = $this -> member -> checkLogin($login_id, $login_pw, $pw_encode, $api_key);
        if (DB::isError($member_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $member_info->getUserInfo());
            return false;
        }elseif($member_info){
            // ユーザの有効期限確認
            $where = sprintf("user_key = %s AND user_starttime <= '%s' AND user_delete_status <2", 
                     mysql_real_escape_string($member_info['user_key']), date('Y-m-d H:i:s'));
            $user_info = $this -> user -> getRow($where);
            if (DB::isError($user_info)) {
                $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
                return false;
            } else {
                if ($member_info && $user_info) {
                    // メンバーログイン
                    return array(
                        "user_info"   => $user_info,
                        "member_info" => $member_info
                    );
                }
            }
        }
        // アカウントが存在しない
        return false;
     }

    /**
     * V-cubeIdログインする時
     * token認証処理
     * @param string $login_id　ログインid
     * @param string $login_pw ログインパスワード
     * @param boolean $pw_encode パスワードの暗号化（MD5,sha1ハッシュ）
     * 
     */
    public function checkToken($login_id, $login_pw, $pw_encode=''){
        if(!$login_id || !$login_pw){
            $this->logger->warn(__FUNCTION__."#not exists user_id or password", __FILE__, __LINE__, array(
                "login_id" => $login_id,
                "login_pw" => $login_pw)
            );
            return $res['result'] = false;
        }
        require_once('lib/EZLib/EZCore/EZConfig.class.php');
        $configUtil = EZConfig::getInstance(N2MY_CONFIG_FILE);
        switch ($_SERVER["SERVER_NAME"]) {
            case $configUtil -> get('VCUBE_ONE','meeting_server_name'):
                $wsseId      = $configUtil->get('VCUBE_ONE','vcubeid_mtg_wsseId');
                $wssePw      = $configUtil->get('VCUBE_ONE','vcubeid_mtg_wssePw');
                $consumerKey = $configUtil->get('VCUBE_ONE','one_meeting_consumer_key');
                break;
            case $configUtil -> get('VCUBE_ONE','document_server_name'):
                $wsseId      = $configUtil->get('VCUBE_ONE','vcubeid_doc_wsseId');
                $wssePw      = $configUtil->get('VCUBE_ONE','vcubeid_doc_wssePw');
                $consumerKey = $configUtil->get('VCUBE_ONE','one_document_consumer_key');
                break;
            case $configUtil -> get('VCUBE_ONE','sales_server_name'):
                $wsseId      = $configUtil->get('VCUBE_ONE','vcubeid_sls_wsseId');
                $wssePw      = $configUtil->get('VCUBE_ONE','vcubeid_sls_wssePw');
                $consumerKey = $configUtil->get('VCUBE_ONE','one_sales_consumer_key');
                break;
            default:
                $wsseId      = $configUtil->get('VCUBE_ONE','vcubeid_mtg_wsseId');
                $wssePw      = $configUtil->get('VCUBE_ONE','vcubeid_mtg_wssePw');
                $consumerKey = $configUtil->get('VCUBE_ONE','one_meeting_consumer_key');
                break;
        }
        $domain = $configUtil -> get('VCUBE_ONE','vcubeid_domain');
        $isSSL  = $configUtil -> get('VCUBE_ONE','vcube_isSSL')  ? $configUtil -> get('VCUBE_ONE','isSSL')  : false;
        $isSha1 = $configUtil -> get('VCUBE_ONE','vcube_isSha1') ? $configUtil -> get('VCUBE_ONE','isSha1') : false;
        if (!$domain || !$wsseId || !$wssePw || !$consumerKey) {
            $this->logger->error("parametter error");
            $res["result"]  = false;
            $res["backUrl"] = $_REQUEST["backUrl"]? $_REQUEST["backUrl"]: "";
            return $res;
        }
        require_once ('classes/vcubeid/vcubeIdCore.class.php');
        $vcubeid = new vcubeIdCore($domain, $wsseId, $wssePw, $isSSL, $isSha1);
        if(!$pw_encode){
            $password = sha1($login_pw);
        }elseif($pw_encode == 'sha1'){
            $password = $login_pw;
        }else{
            $res["result"]  = false;
            $res["backUrl"] = $_REQUEST["backUrl"]? $_REQUEST["backUrl"]: "";
            return $res;
        }
        $token   = $vcubeid -> getToken($consumerKey, $login_id, $password);
        if(!$token['result']){
            $res["result"]  = false;
            $res["backUrl"] = $_REQUEST["backUrl"]? $_REQUEST["backUrl"]: "";
            return $res;
        }
        $this->logger->info('###TOKEN INFO###', __FILE__, __LINE__, $token);
        $response = $vcubeid -> existToken($consumerKey, $token['vIdAuthToken']);
        if($response["result"] === true){
            $res["result"]       = true;
            $res["vcubeId"]      = $response["vcubeId"];
        }else{
            $res["result"]       = false;
            $res["backUrl"]      = $_REQUEST["backUrl"] ? $_REQUEST["backUrl"] : "";
            $res["vIdAuthToken"] = $token["vIdAuthToken"];
            return $res;
        }
        // ステータス確認
        $response_id = $vcubeid -> ssologin($consumerKey, $token['vIdAuthToken'], $res["vcubeId"]);
        if($response_id['result'] !== true){
            $res = array(
                'result'  => false,
                'vcubeId' => $response['vcubeId'],
            );
        }else{
            $contracts = $response_id['contracts']['contract'];
            if(!array_key_exists(0, $contracts)){
                $tmp = $contracts;
                unset($contracts);
                $contracts[] = $tmp;
            }
            $_contracts = array();
            foreach ($contracts as $contract){
                $xml = (array)simplexml_load_string($contract['settintXml']);
                $xml_role = (array)$xml['role'];
                $_contracts[] = array(
                    'user_id'      => $contract['meeting']['userId'],
                    'member_id'    => $contract['meeting']['memberId'],
                    'company_name' => $contract['name'],
                    'role'         => $xml_role['permission'],
                	'is_portal_header' => ( $contract['one']['header'] === "true" ),
                	'contract_id' => $contract['id'],
                );
            }
            $res = array(
                'result'    => $response_id['result'],
                'contracts' => $_contracts,
            );
        }
        $res["backUrl"]       = $_REQUEST["backUrl"]? $_REQUEST["backUrl"]: "";
        $res["vIdAuthToken"]  = $token['vIdAuthToken'];
        return $res;
    }
}
