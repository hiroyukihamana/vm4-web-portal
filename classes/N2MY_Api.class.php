<?php

#ini_set('session.use_cookies', '0');

require_once("config/config.inc.php");
require_once("lib/EZLib/EZCore/EZFrameApi.class.php");
require_once("lib/EZLib/EZSession/EZSession.class.php");
require_once("lib/EZLib/EZHTML/EZValidator.class.php");
require_once ("lib/EZLib/EZUtil/EZDate.class.php");

define('N2MY_API_GET_LIST_LIMIT', '10');
define("API_ROOM_ID",            "room_id");
define("API_MEETING_ID",         "meeting_id");
define("API_RESERVATION_ID",     "reservation_id");
define("API_GUEST_ID",           "guest_id");
define("API_PARTICIPANT_ID",     "participant_id");
define("API_ADDRESS_ID",         "address_id");
define("API_TIME_ZONE",          "timezone");
define("API_MEETINGLOG_VIDEO",   "is_recorded_video");
define("API_MEETINGLOG_MINUTES", "is_recorded_minutes");

define("API_RESERVATION_START", "reservation_start_date");
define("API_RESERVATION_END",   "reservation_end_date");

define("API_PARTICIPANT_START", "participant_start_date");
define("API_PARTICIPANT_END",   "participant_end_date");

class N2MY_Api extends EZFrameApi {

    var $logger = "";
    var $session =  "";
    var $output_type = "";
    private $output_type_list = array("xml", "php", "json", "yaml");

    function _init() {
        // DB接続先
        $this->_auth_dsn = $this->config->get("GLOBAL", "auth_dsn");
        define("N2MY_MDB_DSN", $this->_auth_dsn);
        $this->_get_passage_time();
        // 言語設定
        $this->_lang = $this->get_language();
//        $this->getApiInterface();
    }

    // one用認証チェック
    function checkAuthorizationOne(){
        if (!defined('N2MY_SESS_NAME')) {
            define('N2MY_SESS_NAME', N2MY_SESSION);
        }
        $this->output_type = $this->request->get("output_type");
        $session_id = isset($_REQUEST[N2MY_SESS_NAME]) ? $_REQUEST[N2MY_SESS_NAME] : null;
        if ($session_id) {
            // セッションの開始
            session_id($session_id);
            $this->session = EZSession::getInstance();
            // 有効なアカウント情報かをチェック
            if(!$login = $this->session->get("one_login")) {
                return $output = $this->display_error(1, "Login failed / Invalid auth token");
            }
            // 設定ファイル取り込み
            $lang = $this->session->get("lang");
            require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
            $lang_cd = EZLanguage::getLangCd("ja");
            $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang_cd."/message.ini";
            if (file_exists($lang_conf_file)) {
                $this->_message = parse_ini_file($lang_conf_file, true);
            } else {
                $this->logger2->warn($lang_conf_file, "Message file not found");
            }
            // 無効なセッションが指定された場合はseesion_destroy
            // 認証時のドメイン／IPと異なっていた場合はエラー
        } else {
            // 認証されていないエラーを返す
                return $this->display_error(1, "Login failed / Invalid auth token", $session_id);
            }

    }

    // ユーザー認証チェック
    function checkAuthorization() {
        if (!defined('N2MY_SESS_NAME')) {
            define('N2MY_SESS_NAME', N2MY_SESSION);
        }
        if (!defined("N2MY_SERVER_TIMEZONE")) {
            date_default_timezone_set($this->config->get('GLOBAL', 'time_zone_name', date_default_timezone_get()));
            $_tz = (int)(substr( date( 'O' ), 0, 3));
            // サーバータイムゾーン設定
            define("N2MY_SERVER_TIMEZONE", $_tz);
        }
        $this->output_type = $this->request->get("output_type");
        $session_id = isset($_REQUEST[N2MY_SESS_NAME]) ? $_REQUEST[N2MY_SESS_NAME] : null;
        if ($session_id) {
            // セッションの開始
            session_id($session_id);
            $this->session = EZSession::getInstance();
            // 有効なアカウント情報かをチェック
            if(!$login = $this->session->get("login")) {
                return $output = $this->display_error(1, "Login failed / Invalid auth token");
            }
            // 設定ファイル取り込み
            $lang = $this->session->get("lang");
            require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
            $lang_cd = EZLanguage::getLangCd("ja");
            $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang_cd."/message.ini";
            if (file_exists($lang_conf_file)) {
                $this->_message = parse_ini_file($lang_conf_file, true);
            } else {
                $this->logger2->warn($lang_conf_file, "Message file not found");
            }
            // 無効なセッションが指定された場合はseesion_destroy
            // 認証時のドメイン／IPと異なっていた場合はエラー
        } else {
            // 認証されていないエラーを返す
            return $this->display_error(1, "Login failed / Invalid auth token", $session_id);
        }
    }
    // お客様認証チェック
    function checkAuthorizationCustomer() {
        if (!defined('N2MY_SESS_NAME')) {
            define('N2MY_SESS_NAME', N2MY_SESSION);
        }
    if (!defined("N2MY_SERVER_TIMEZONE")) {
            date_default_timezone_set($this->config->get('GLOBAL', 'time_zone_name', date_default_timezone_get()));
            $_tz = (int)(substr( date( 'O' ), 0, 3));
            // サーバータイムゾーン設定
            define("N2MY_SERVER_TIMEZONE", $_tz);
        }
      $this->output_type = $this->request->get("output_type");
      $session_id = isset($_REQUEST[N2MY_SESS_NAME]) ? $_REQUEST[N2MY_SESS_NAME] : null;
        $this->logger2->info($session_id);
      if ($session_id) {
        // セッションの開始
        session_id($session_id);
        $this->session = EZSession::getInstance();
        // 有効なアカウント情報かをチェック
        if(!$login = $this->session->get("login_customer")) {
          return $output = $this->display_error(1, "Login failed / Invalid auth token");
        }
        // 設定ファイル取り込み
        $lang = $this->session->get("lang");
        //require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        //$lang_cd = EZLanguage::getLangCd("ja");
        $lang_conf_file = N2MY_APP_DIR."config/".$lang."/message.ini";
        if (file_exists($lang_conf_file)) {
          $this->_message = parse_ini_file($lang_conf_file, true);
        } else {
          $this->logger2->warn($lang_conf_file, "Message file not found");
        }
        // 無効なセッションが指定された場合はseesion_destroy
        // 認証時のドメイン／IPと異なっていた場合はエラー
      } else {
        // 認証されていないエラーを返す
        return $this->display_error(1, "Login failed / Invalid auth token", $session_id);
      }
    }
    // ユーザー認証チェック
    function checkAuthorizationMeeting($session_id) {
        if (!defined('N2MY_SESS_NAME')) {
            define('N2MY_SESS_NAME', N2MY_SESSION);
        }
        if (!defined("N2MY_SERVER_TIMEZONE")) {
            date_default_timezone_set($this->config->get('GLOBAL', 'time_zone_name', date_default_timezone_get()));
            $_tz = (int)(substr( date( 'O' ), 0, 3));
            // サーバータイムゾーン設定
            define("N2MY_SERVER_TIMEZONE", $_tz);
        }
        $this->output_type = $this->request->get("output_type");
        $session_id = isset($session_id) ? $session_id : null;
        if ($session_id) {
            // セッションの開始
            session_id($session_id);
            $this->session = EZSession::getInstance();
            // 有効なアカウント情報かをチェック
            if(!$login = $this->session->get("login")) {
                return $output = $this->display_error(1, "Login failed / Invalid auth token");
            }
            // 設定ファイル取り込み
            $lang = $this->session->get("lang");
            require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
            $lang_cd = EZLanguage::getLangCd("ja");
            $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang_cd."/message.ini";
            if (file_exists($lang_conf_file)) {
                $this->_message = parse_ini_file($lang_conf_file, true);
            } else {
                $this->logger2->warn($lang_conf_file, "Message file not found");
            }
            // 無効なセッションが指定された場合はseesion_destroy
            // 認証時のドメイン／IPと異なっていた場合はエラー
        } else {
            // 認証されていないエラーを返す
            return $this->display_error(1, "Login failed / Invalid auth token", $session_id);
        }
    }

    function checkAuthorizationStatus() {
        if (!defined('N2MY_SESS_NAME')) {
            define('N2MY_SESS_NAME', N2MY_SESSION);
        }
        if (!defined("N2MY_SERVER_TIMEZONE")) {
            date_default_timezone_set($this->config->get('GLOBAL', 'time_zone_name', date_default_timezone_get()));
            $_tz = (int)(substr( date( 'O' ), 0, 3));
            // サーバータイムゾーン設定
            define("N2MY_SERVER_TIMEZONE", $_tz);
        }
        $this->output_type = $this->request->get("output_type");
        $session_id = isset($_REQUEST[N2MY_SESS_NAME]) ? $_REQUEST[N2MY_SESS_NAME] : null;
        if ($session_id) {
            // セッションの開始
            session_id($session_id);
            $this->session = EZSession::getInstance();
            // 有効なアカウント情報かをチェック
            if($login = $this->session->get("login")) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    // ユーザー認証チェック
    function checkAuthorizationAdmin() {
        if (!defined('N2MY_SESS_NAME')) {
            define('N2MY_SESS_NAME', N2MY_SESSION);
        }
        $this->output_type = $this->request->get("output_type");
        $session_id = isset($_REQUEST[N2MY_SESS_NAME]) ? $_REQUEST[N2MY_SESS_NAME] : null;
        if ($session_id) {
            // セッションの開始
            session_id($session_id);
            $this->session = EZSession::getInstance();
            // 有効なアカウント情報かをチェック
            if (!$login = $this->session->get("admin_login")) {
                return $this->display_error(1, "You do not have an admin authorization", $session_id);
            }
            // 無効なセッションが指定された場合はseesion_destroy
            // 認証時のドメイン／IPと異なっていた場合はエラー
            // 設定ファイル取り込み
            $lang = $this->session->get("lang");
            require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
            $lang_cd = EZLanguage::getLangCd("ja");
            $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang_cd."/message.ini";
            if (file_exists($lang_conf_file)) {
              $this->_message = parse_ini_file($lang_conf_file, true);
            } else {
              $this->logger2->warn($lang_conf_file, "Message file not found");
            }
        } else {
            // 認証されていないエラーを返す
            return $this->display_error(1, "Login failed / Invalid auth token", $session_id);
        }
    }

    function getApiVersion() {
        $group_name = $_SERVER["SCRIPT_NAME"];
        $group_split = split("/", $group_name);
        $_version_key = array_search("api", $group_split);
        if ($_version_key === false) {
            $this->logger2->error($_SERVER["SCRIPT_NAME"], "バージョンが取得できませんでした。");
            $interface = array();
            return false;
        }
        $version = $group_split[$_version_key+1];
        // バージョンのバリデーションをチェックしようと思ったが、顧客ごとのAPI対応もありえるので保留
//        if (preg_match('v\d', $version)) {
//            return false;
//        } else {
            return $version;
//        }
    }

    /**
     *
     */
    function getApiInterface() {
        $this->logger2->info(array($this->_get_passage_time()));
        static $interface = null;
        if (isset($interface)) {
            return $interface;
        }
        $method_name = $this->getActionName();
        $group_name = $_SERVER["SCRIPT_NAME"];
//        $group = "/api/kiyomizu/api/v1/reservation/index.php";
// ドキュメントルートのパスからバージョン情報を取得しないとうまくいかないかも
        $group_split = split("/", $group_name);
        $_version_key = array_search("api", $group_split);
        if ($_version_key === false) {
            $this->logger2->error($_SERVER["SCRIPT_NAME"], "バージョンが取得できませんでした。");
            $interface = array();
            return false;
        }
        $version = $group_split[$_version_key+1];
        require_once "lib/EZLib/EZXML/EZXML.class.php";
        $xml = new EZXML();
        if (!$data = file_get_contents(N2MY_APP_DIR."setup/api/".$version."/api_info.xml")) {
            $this->logger2->error(N2MY_APP_DIR."setup/api/".$version."/api_info.xml", "API情報が取得できません");
            $interface = array();
            return false;
        }
        $response_info = $xml->openXML($data);
        foreach($response_info["api_list"]["group"] as $key => $group) {
            if ("/".$group["_attr"]["path"] == dirname($group_name)."/") {
                foreach($group["method"] as $_key => $methods) {
                    if ($methods["_attr"]["method"] == $method_name) {
                        // サンプルレスポンスだけ取り除く
                        $interface["input"] = $methods;
                        break 2;
                    }
                }
            }
        }
        require_once "lib/EZLib/EZXML/EZXML.class.php";
        $xml = new EZXML();
        if (!$data = file_get_contents(N2MY_APP_DIR."setup/api/".$version."/response_info.xml")) {
            $this->logger2->error(N2MY_APP_DIR."setup/api/".$version."/response_info.xml", "API情報が取得できません");
            $interface = array();
            return false;
        }
        $response_info = $xml->openXML($data);
        foreach($response_info["api_list"]["group"] as $key => $group) {
            if ("/".$group["_attr"]["path"] == dirname($group_name)."/") {
                foreach($group["method"] as $_key => $methods) {
                    if ($methods["_attr"]["method"] == $method_name) {
                        // サンプルレスポンスだけ取り除く
                        $interface["output"] = $methods;
                        break 2;
                    }
                }
            }
        }
        $this->logger2->info(array($this->_get_passage_time(), $version, $group_name, $method_name));
        return $interface;
    }

    /**
     * 契約した部屋以外のroom_keyを参照させない
     */
    function get_room_key($default = null) {
        $room_key = $this->request->get(API_ROOM_ID, $default);
        // 指定がない場合もOKとする
        if ($room_key == "") {
            return "";
        }
        $room_list = $this->session->get("room_info");
        foreach($room_list as $_key => $room_info) {
            if ($_key == $room_key) {
                return $room_key;
            }
        }
        $this->logger2->warn(array($room_list, $room_key));
        $this->display("error.t.html");
        exit;
    }

    function get_valid_room_list() {
        require_once("classes/N2MY_Account.class.php");
        $server_info    = $this->session->get("server_info");
        $user_info      = $this->session->get("user_info");
        $member_info    = $this->session->get("member_info");
        $objN2MYAccount = new N2MY_Account($server_info["dsn"]);
        if ( $member_info && ($user_info["account_model"] == "member")) {
            $rooms = $objN2MYAccount->getOwnRoom( $member_info["member_key"], $user_info["user_key"]);
            if (count($rooms) > 1) {
                $user_info["member_multi_room_flg"] = 1;
            } else {
                $user_info["member_multi_room_flg"] = 0;
            }
            $this->session->set("user_info", $user_info);
        } else {
            $rooms = $objN2MYAccount->getRoomList( $user_info["user_key"] );
        }
        return $rooms;
    }

    /**
     * メッセージ取得
     *
     * @param string $group グループ
     * @param string $param パラメタ
     * @param mixed $default デフォルト値
     */
    function get_message($group, $param = null, $default = "")
    {
        if ($param === null) {
            if(isset($this->_message[$group]) &&
                $this->_message[$group] !== ''){
              return $this->_message[$group];
            } else{
              return $default;
            }
        } else {
            if(isset($this->_message[$group][$param]) &&
                $this->_message[$group][$param] !== ''){
              return $this->_message[$group][$param];
            } else{
              return $default;
            }
        }
    }

    /**
     * 地域コード一覧取得
     */
    function get_country_list() {
        $_country_list = $this->get_message("COUNTRY");
        $country_allow = $this->config->get("N2MY", "country_allow");
        $country_deny = $this->config->get("N2MY", "country_deny");
        foreach($_country_list as $country_key => $country_value) {
            list($datacenter_key, $country_name) = split(":",$country_value);
            if ($country_allow) {
                $allow_list = split(",", $country_allow);
                if (in_array($country_key, $allow_list)) {
                    $country_list[$country_key] = array(
                        "country_key" => $datacenter_key,
                        "country_name"   => $country_name,
                    );
                }
            } elseif ($country_deny) {
                $deny_list = split(",", $country_deny);
                if (!in_array($country_key, $deny_list)) {
                    $country_list[$country_key] = array(
                        "country_key" => $datacenter_key,
                        "country_name"   => $country_name,
                    );
                }
            } else {
                $country_list[$country_key] = array(
                    "country_key" => $datacenter_key,
                    "country_name"   => $country_name,
                );
            }
        }
        return $country_list;
    }

    function _get_country_key ($country_key) {
        //MaxMindでIPから所在地を検索
        $remote_addr =  $_SERVER["REMOTE_ADDR"];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$remote_addr);
        $maxmind_url = $this->config->get("MAXMIND", "url");
        $maxmind_key = $this->config->get("MAXMIND", "key");
        $result_country_key = "";
        if ($maxmind_url && $maxmind_key && $country_key == "auto") {
            require_once ("lib/maxmind/geoip.php");
            $obj_MaxMind = new MaxMaindApp();
            $result_country_key = $obj_MaxMind->getCountryCord($maxmind_url, $maxmind_key, $remote_addr);
            return $result_country_key;
        } else {
           return false;
        }
    }

    /**
     * ブラウザの言語設定から優先度が高い識別IDを返す
     *
     * @return string 言語識別ID（jp:日本[デフォルト]、en:英語）
     * @todo EZLibのユーティリティ関係のクラスに持って行きたい
     */
    function get_language($select_lang = null) {
        // キーはすべて小文字
        $_language_pages = array (
            "af" => "af",
            "ar" => "ar",
            "bg" => "bg",
            "ca" => "ca",
            "cs" => "cs",
            "cy" => "cy",
            "da" => "da",
            "de" => "de",
            "el" => "el",
            "en" => "en_US",
            "es" => "es",
            "et" => "et",
            "fa" => "fa",
            "fi" => "fi",
            "fr" => "fr_FR",
            "ga" => "ga",
            "gl" => "gl",
            "he" => "he",
            "hi" => "hi",
            "hr" => "hr",
            "hu" => "hu",
            "in" => "in_ID",
            "is" => "is",
            "it" => "it",
            "ja" => "ja_JP",
            "ko" => "ko_KR",
            "lt" => "lt",
            "lv" => "lv",
            "mk" => "mk",
            "ms" => "ms",
            "mt" => "mt",
            "nl" => "nl",
            "no" => "no",
            "pl" => "pl",
            "pt" => "pt",
            "ro" => "ro",
            "ru" => "ru",
            "sk" => "sk",
            "sl" => "sl",
            "sq" => "sq",
            "sr" => "sr",
            "sv" => "sv",
            "sw" => "sw",
            "th" => "th_TH",
            "tl" => "tl",
            "tr" => "tr",
            "uk" => "uk",
            "vi" => "vi",
            "yi" => "yi",
            "zh" => "zh_CN",
            "zh-cn" => "zh_CN",
            "zh-tw" => "zh_TW"
        );
        // 設定ファイルで有効な言語のみ
        $lang_allow = $this->config->get("N2MY", "lang_allow");
        $lang_deny  = $this->config->get("N2MY", "lang_deny");
        foreach($_language_pages as $lang_key => $lang_value) {
            if ($lang_allow) {
                $allow_list = split(",", $lang_allow);
                if (in_array($lang_key, $allow_list)) {
                    $language_pages[$lang_key] = $lang_value;
                }
            } elseif ($lang_deny) {
                $deny_list = split(",", $lang_deny);
                if (!in_array($lang_key, $deny_list)) {
                    $language_pages[$lang_key] = $lang_value;
                }
            } else {
                $language_pages[$lang_key] = $lang_value;
            }
        }
        // セッションから取得
        if ($this->session) {
            $lang = $this->session->get("lang");
        } else if ($select_lang) {
            $lang = $select_lang;
        }
        if ($lang) {
            if (array_key_exists($lang, $language_pages)) {
                return $language_pages[$lang];
            }
        }
        // 取得できなかった場合のデフォルト
        $language_default = $this->config->get("N2MY", "default_lang", "en");
        $http_accept_language = $_SERVER["HTTP_ACCEPT_LANGUAGE"];
        if (!$http_accept_language) {
            return $language_pages[$language_default];
        }
        // 小文字に変換／スペース削除
        $http_accept_language = strtolower($http_accept_language);
        $accept_language = str_replace(" ", "", $http_accept_language);
        $languages = explode(",", $accept_language);
        // 優先度でチェック
        foreach ($languages as $language) {
            list($lang) = explode(";", $language);
            // ２バイト
            if (array_key_exists($lang, $language_pages)) {
                return $language_pages[$lang];
            } else {
                if (strpos($lang, "-") !== false) {
                    $lang = substr($lang, 0, strpos($lang, "-"));
                }
                if (array_key_exists($lang, $language_pages)) {
                    return $language_pages[$lang];
                }
            }
        }
        // なかったら
        return $language_pages[$language_default];
    }

    function getSummerTime(){
        //現状はロサンゼルスのサマータイムフラグで確認
        date_default_timezone_set('America/Los_Angeles');
        $sum = date("I");
        date_default_timezone_set($this->config->get('GLOBAL', 'time_zone_name', date_default_timezone_get()));
        return $sum;
    }

    function default_view() {
        return $this->display_error(2, "Method not found");
    }

    /**
     * DSN取得
     */
    function get_auth_dsn() {
        return $this->_auth_dsn;
    }

    /**
     * DSN取得
     */
    function get_dsn() {
        if ($this->_dsn) {
            return $this->_dsn;
        }
        if( $serverdata = $this->session->get("server_info") ){
            $this->_dsn = $serverdata["dsn"];
        } else {
//            $this->logger2->error("DSN_KEY取得エラー");
            $this->_dsn = $this->get_dsn_value($this->get_dsn_key());
        }
        return $this->_dsn;
    }

    /**
     * DSN取得
     */
    function get_dsn_key() {
        if( $serverdata = $this->session->get("server_info") ){
            return $serverdata["host_name"];
        } else {
            return N2MY_DEFAULT_DB;
            /*
            $this->template->assign('message', SESSION_ERROR);
            $this->display('user/session_error.t.html');
            return N2MY_DEFAULT_DB;
            */
        }
    }

    /**
     * DSNキーから直接指定
     */
    function get_dsn_value($server_dsn_key) {
        static $dsn;
        if ($dsn) {
            return $dsn;
        }
        $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        if (isset($server_list["SERVER_LIST"][$server_dsn_key])) {
            $dsn = $server_list["SERVER_LIST"][$server_dsn_key];
        } else {
            $this->logger2->error($server_dsn_key, "DSN_KEY取得エラー");
            $dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
        }
        return $dsn;
    }

    /**
     * core側のURLを取得
     *
     * @param string $path 実行先のパス
     * @param array $query パラメタ
     * @return string core側のurl（指定がなければベースURl）
     */
    function get_core_url($path = "", $query = array(), $ssl = false) {
        $query_str = "";
        foreach ($query as $_key => $_value) {
            $query_str .= "&" . $_key . "=" . urlencode($_value);
        }
        if ($query_str) {
            $query_str = "?" . substr($query_str, 1);
        }
        $url = N2MY_BASE_URL.$path.$query_str;
        return $url;
    }

    /**
     * XMLに展開
     */
    function api_execute($url) {
        $contents = $this->wget($url);
        if ($contents == "") {
            return array();
        } else {
            require_once("lib/EZLib/EZXML/EZXML.class.php");
            $xml = new EZXML();
            $data = $xml->openXML($contents,"");
        }
        return $data;
    }

    /**
     * データ取得
     */
    function wget($url) {
        $_url = parse_url($url);
        if ($_url["scheme"] == "https") {
            $url = "http://";
            if ($_url["user"]) {
                $url .= $_url["user"].":";
            }
            if ($_url["pass"]) {
                $url .= $_url["pass"]."@";
            }
            $url .= $_url["host"].$_url["path"];
            if ($_url["query"]) {
                $url .= "?".$_url["query"];
            }
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$url);
        }
        $contents = "";
        $fp = @fopen($url, "r");
        if ($fp == false) {
            $this->logger->error(__FUNCTION__."#not found error", __FILE__, __LINE__, $url);
            return "";
        } else {
            while (!feof($fp)) {
                $contents .= fread($fp, 1024);
            }
            fclose($fp);
        }
        return $contents;
    }

    function display_error($error_cd, $error_msg = "", $error_info = array(), $_output_type = "") {
        if (!$error_msg) {
            // 言語別のエラーコード表から出力
        }
        // ログ
        $this->logger2->warn(array(
            'method'        => $_SERVER["SCRIPT_NAME"].'::'.$this->getActionName(),
            'request'       => $this->request->getAll(),
            'error_cd'      => $error_cd,
            'error_msg'     => $error_msg,
            'error_info'    => $error_info,
            ));
        if ($this->getApiVersion() == 'v1') {
            $data = array(
                "status" => 0,
                "error_msg" => $error_msg,
                "error_info" => array(
                    "error_cd"  => $error_cd,
                    "data"      => $error_info,
                    "method"    => $_SERVER["SCRIPT_NAME"].'::'.$this->getActionName(),
                    "request"   => $this->request->getAll()
                ),
            );
        } else {
            $data = array(
                "status" => 0,
                "error_info" => array(
                    "error_cd"  => $error_cd,
                    "error_msg" => $error_msg,
                    "details"   => $error_info["errors"],
                    "method"    => $_SERVER["SCRIPT_NAME"].'::'.$this->getActionName(),
                    "request"   => $this->request->getAll()
                ),
            );
        }
        $output = $this->serialize($data, $_output_type);
        print $output;
        exit;
    }

    /**
     * タイプに併せて出力
     *
     * @param string $output_type
     * @param string $data
     */
    function output($data = array(), $_output_type = "") {
        // エンティティ処理
        //$this->_tksort($data);
        $data = array(
            "status" => 1,
            "data" => $data,
        );
        $output = $this->serialize($data, $_output_type);
        print $output;
        return true;
    }

    /**
     * ver 4.10.0.0 Oneエスケープ処理
     * '&' (アンパサンド) は '&amp;'
     * '<' (小なり) は '&lt;'
     * '>' (大なり) は '&gt;'
     * のみエスケープ処理させる
     * @param string $data
     * @param string $_output_type
     * @return boolean
     */
    function one_output($data = array(), $_output_type = "") {
        // エンティティ処理
        //$this->_tksort($data);
        $data = array(
                "status" => 1,
                "data" => $data,
        );
        $output = $this->serialize($data, $_output_type);
        //既存方式だとシングルクオート、ダブルクオートもエスケープされるのでもとに戻す
        $output_ex = (str_replace("&apos;" , "'",$output));
        $output_ex = (str_replace("&quot;" , '"',$output_ex));
        // パーセント(%)もエスケープ
        $output_ex = (str_replace('%' , '&#37;',$output_ex));
        //print str_replace("&apos;" , "'",$output);
        print $output_ex;
        return true;
    }

    private function _tksort(&$array) {
      ksort($array);
      foreach(array_keys($array) as $k)
        {
        if(gettype($array[$k])=="array")
            {
                 $this->_tksort($array[$k]);
            }
        }
    }

    /**
     * ver 4.10.0.0 Oneエスケープ処理
     * SimpleXMLElementクラスを使用するとエスケープしたくない文字がエスケープされるのでそれを回避させる
     * return 文字列のxmlをsimplexml_load_stringにして返す
     * @param Oneからのリクエストxml $string_xml
     */
    function one_get_xml($xml_string){
        //simplexml_load_string使用すると「&apos;」がシングルクオートに「&quot;」ダブルクオートにエスケープ、置換しておく
        $xml_string_ex = (str_replace("&apos;" , "&amp;apos;",$xml_string));
        $xml_string_ex = (str_replace("&quot;" , '&amp;quot;',$xml_string_ex));
        return simplexml_load_string($xml_string_ex);
    }

    /**
     * タイプにあわせてシリアライズした結果を返す
     *
     * @param string $output_type
     * @param string $data
     * @return string シリアライズされた結果
     */
    function serialize($data, $_output_type = "") {

        if ($this->request->get("output_type")) {
            $this->output_type = $this->request->get("output_type");
        } else if ($_output_type) {
            $this->output_type = $_output_type;
        } else if (get_class($this->session) == 'EZSession') {
            $this->output_type = $this->session->get("output_type");
        }
        if ($this->output_type == "php") {
            $output = serialize($data);
        } else if ($this->output_type == "json") {
            $output = json_encode($data);
        } else if($this->output_type == "yaml") {
            require_once("lib/spyc/spyc.php");
            $output = Spyc::YAMLDump($data,4,60);
        } else {
            require_once("lib/pear/XML/Serializer.php");
            $serializer = new XML_Serializer();
            $serializer->setOption('mode','simplexml');
            $serializer->setOption('encoding','UTF-8');
            $serializer->setOption('addDecl','ture');
            $serializer->setOption('rootName','result');
            $serializer->serialize($data);
            $output = $serializer->getSerializedData();
            header("Content-Type: text/xml; charset=UTF-8");
        }
        $this->logger2->debug(array($this->_get_passage_time()));
        return $output;
    }


    /**
     * タイムゾーン一覧取得
     */
    function get_timezone_list() {
        static $timezone_list;
        if (!$timezone_list) {
            $_timezone_list = $this->get_message("TIME_ZONE");
            $timezone_allow = $this->config->get("N2MY", "timezone_allow");
            $timezone_deny = $this->config->get("N2MY", "timezone_deny");
            foreach($_timezone_list as $timezone_key => $timezone_value) {
                $timezone_key = (string)$timezone_key;
                if ($timezone_allow) {
                    $allow_list = split(",", $timezone_allow);
                    if (in_array($timezone_key, $allow_list)) {
                        $timezone_list[$timezone_key] = array(
                            "key" => (string)$timezone_key,
                            "value" => $timezone_value);
                    }
                } elseif ($timezone_deny) {
                    $deny_list = split(",", $timezone_deny);
                    if (!in_array($timezone_key, $deny_list)) {
                        $timezone_list[$timezone_key] = array(
                            "key" => (string)$timezone_key,
                            "value" => $timezone_value);
                    }
                } else {
                    $timezone_list[$timezone_key] = array(
                        "key" => (string)$timezone_key,
                        "value" => $timezone_value);
                }
            }
        }
        return $timezone_list;
    }

    function get_language_list() {
        $_lang_list = $this->get_message("LANGUAGE");
        $lang_allow = $this->config->get("N2MY", "lang_allow");
        $lang_deny = $this->config->get("N2MY", "lang_deny");
        foreach($_lang_list as $lang_key => $lang_value) {
            $lang_name = $lang_value;
            if ($lang_allow) {
                $allow_list = split(",", $lang_allow);
                if (in_array($lang_key, $allow_list)) {
                    $lang_list[$lang_key] = $lang_name;
                }
            } elseif ($lang_deny) {
                $deny_list = split(",", $lang_deny);
                if (!in_array($lang_key, $deny_list)) {
                    $lang_list[$lang_key] = $lang_name;
                }
            } else {
                $lang_list[$lang_key] = $lang_name;
            }
        }
        return $lang_list;
    }

    function get_output_type_list() {
        return $this->output_type_list;
    }

    /**
     * テンプレートディレクトリを言語別に指定
     */
    function _set_template_dir($lang = null) {
        static $base_template_dir;
        // 言語指定あり
        if ($lang) {
            $this->_lang = $lang;
        } else {
            $this->_lang = $this->session->get("lang", _EZSESSION_NAMESPACE);
            if (!$this->_lang) {
                $this->_lang = $this->get_language();
            }
        }
        // テンプレートの元ディレクトリ
        if (!$base_template_dir) {
            $base_template_dir = $this->template->template_dir;
        }
    }

    function display($template_file, $common = false, $content_type = "html") {
      $this->logger->trace(__FUNCTION__ . "#start", __FILE__, __LINE__);
      //        $template_path = $this->template->template_dir."/".$template_file;
      $template_file = sprintf( "%s/%s", $common ? "common" : $this->_lang, $template_file );
      $template_path = sprintf( "%s%s", $this->template->template_dir, $template_file );
      if (!file_exists($template_path)) {
        $this->logger->error(__FUNCTION__."#template file not found", __FILE__, __LINE__, $template_path);
        $this->_set_template_dir("ja");
        $template_file = sprintf( "%s/error.t.html", $this->_lang );
      } else {
        $this->logger->debug(__FUNCTION__ . "#template_file", __FILE__, __LINE__, $template_path);
      }
      $char_set = $this->config->get("GLOBAL", "html_output_char", "UTF-8");
      $this->template->assign("char_set", $char_set);
      // ログインタイプでヘッダフッタ切り替え
      $frame["user_info"] = $this->session->get("user_info");
      $frame["member_info"] = $this->session->get("member_info");
      $frame["google_analytics_cd"] = $this->config->get("N2MY", "google_analytics_cd");
      $frame["login_type"] = $this->session->get("login_type");
      // テンプレートの共通埋め込み
      $frame["lang"] = $this->_lang;
      $frame["lang_list"] = $this->get_language_list();
      $frame["lang_dir"] = $this->_lang.'/';
      // サービス提供情報
      $frame["service_info"] = $this->get_message("SERVICE_INFO");

      //非表示設定
      $ignore_menu = $this->config->getAll("IGNORE_MENU");
      if( $frame["login_type"] == "invite" || ( $frame["member_info"] && ($frame["user_info"]['account_model'] == "member" || $frame["user_info"]['account_model'] == "centre" ))){
        $ignore_menu["login_as_admin"] = 1;
      }

      //ecoは日本語の提供のみ
      if( $this->_lang != "ja" ) $ignore_menu["eco_meter"] = 1;

      // アカウントモデルによる上書き
      if( $frame["user_info"]["account_model"] == "member" || $frame["user_info"]["account_model"] == "centre" ){
        $ignore_menu["enter_as_audience"] = 1;
        $ignore_menu["tool_presence_app"] = 1;
        $ignore_menu["audience_manual"] = 1;
        $ignore_menu["admin_log_fee"] = 1;
        $ignore_menu["advertise"] = 1;

        //メンバーでのログイン
        if( $frame["member_info"] ){
          $ignore_menu["display_room_list"] = 1;
          $ignore_menu["room_select"] = 1;
        }
        //ユーザーでのログイン
        else {
          $ignore_menu["enter_meeting"] = 1;
        }
      }

      $frame["ignore_menu"] = $ignore_menu;
      $frame["n2my"] = $this->config->getAll("N2MY");
      $frame["message"] = $this->_message["DEFINE"];
      // スクリプト名
      $frame["base_url"] = N2MY_BASE_URL;
      $frame["base_dir"] = "/";
      $frame["self"] = $_SERVER["SCRIPT_NAME"];
      $frame["debug"] = $this->config->get("N2MY", "debug");
      // 地域
      $frame["country_key"] = $this->session->get("country_key", _EZSESSION_NAMESPACE, "ja");
      $frame["country_list"] = $this->get_country_list();
      // 実行時間
      $frame["passage_time"] = $this->_get_passage_time();
      // サーバの時間
      $frame["server_time"] = time();
      // サーバの時間
      $EZDate = new EZDate();
      $server_time_zone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE);
      $frame["server_time_zone"] = $server_time_zone;
      $time_zone = $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE);
      $frame["time_zone"] = $time_zone;
      $frame["time_zone_list"] = $this->get_timezone_list();
      $user_time = $EZDate->getLocateTime(time(), $time_zone, $server_time_zone);
      $frame["user_time"] = $user_time;
      // 部屋一覧
      $frame["room_list"] = $this->session->get("room_info");
      $this->template->assign("__frame",$frame);
      // エコメーター
      $this->template->assign("eco_report",   isset($_COOKIE["eco_report"]) ? $_COOKIE["eco_report"] : "");
      $this->template->assign("user_station", isset($_COOKIE["personal_station"]) ? $_COOKIE["personal_station"] : "" );
      switch ($content_type) {
        case "xml":
          header("Content-Type: text/xml; charset=UTF-8;");
          break;
        default:
          header("Content-Type: text/html; charset=".$char_set.";");
          break;
      }
      $this->template->display($template_file);
      $passage_time = $this->_get_passage_time();
      $alert_time = $this->config->get("N2MY", "alert_time", 0);
      if (($passage_time > $alert_time) && ($alert_time > 0)) {
        $this->logger->warn(__FUNCTION__."#passage_time", __FILE__, __LINE__, array(
            "url" => $_SERVER["SCRIPT_URI"],
            "host" => $_SERVER["REMOTE_ADDR"],
            "passage_time" => $passage_time)
        );
      } else {
        $this->logger->trace(__FUNCTION__."#passage_time", __FILE__, __LINE__, $passage_time);
      }
      return true;
    }

    /**
     * HTMLのテンプレート出力
     */
    function display_encrypt($template_file, $encrypt = false) {
        $lang = $this->get_language();
        $this->logger->trace(__FUNCTION__ . "#start", __FILE__, __LINE__);
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$_COOKIE);
        $this->_set_template_dir();
//        $template_path = $this->template->template_dir."/".$template_file;
        // 多言語かテンプレートのみ読み込み
        $custom = $this->config->get('SMARTY_DIR','custom');
        if ($this->session->get("service_mode") == "sales" && !$custom) {
            $custom = 'custom/sales/common/';
        } else if ($custom == "sales" && $this->session->get("service_mode") != "sales" && $this->session->get("login")) {
          $custom = "common";
        } else if ($custom == "") {
            $custom = 'common';
        } else  {
            $custom = 'custom/'.$custom.'/common/';
        }

        $this->template->assign("custom_dir", $custom);
        $this->template->assign("template_dir", $this->template->template_dir);
        if (file_exists($this->template->template_dir.$custom.'/'.$template_file)) {
            $template_file = $this->template->template_dir.$custom.'/'.$template_file;
        } else {
            $template_file = sprintf( "%s/%s", "common", $template_file );
            $template_path = sprintf( "%s%s", $this->template->template_dir, $template_file );
            if (!file_exists($template_path)) {
                $this->logger->error(__FUNCTION__."#template file not found", __FILE__, __LINE__, $template_path);
                $this->_set_template_dir($this->_lang);
                $template_file = "common/error.t.html";
            } else {
                $this->logger->debug(__FUNCTION__ . "#template_file", __FILE__, __LINE__, $template_path);
            }
        }
        $char_set = $this->config->get("GLOBAL", "html_output_char", "UTF-8");
        $this->template->assign("char_set", $char_set);

        //モバイル判別
        if (strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod") || strpos($_SERVER['HTTP_USER_AGENT'], "iPad;")) {
            $frame["view_mobile_flg"] = 1;
        }

        //言語KEY
        $lang_key = substr($this->_lang,0,2);
        if($lang_key == "zh")
          $lang_key = str_replace("_", "-", strtolower($this->_lang));
        // ログインタイプでヘッダフッタ切り替え
        $frame["user_info"]             = $this->session->get("user_info");
        $frame["session_id"]            = session_id();
        $frame["member_info"]           = $this->session->get("member_info");
        $frame["google_analytics_cd"]   = $this->config->get("N2MY", "google_analytics_cd");
        $frame["login_type"]            = $this->session->get("login_type");
        $frame["lang"]                  = $lang;
        $frame["lang_key"]              = $lang_key;
        $frame["lang_list"]             = $this->get_language_list();
        $frame["common_lang_list"]      = $this->get_language_list(true);
        $frame["lang_dir"]              = $lang.'/';
        $frame["service_mode"]          = $this->session->get("service_mode")?$this->session->get("service_mode"): null;
        $frame["use_mode_change"]       = $this->session->get("use_mode_change")?$this->session->get("use_mode_change"): 0;
        // サービス提供情報
        if (isset($custom) && !empty($this->_service_info)) {
            $frame["service_info"]          = $this->get_service_info("SERVICE_INFO");
        } else {
            $frame["service_info"]          = $this->get_message("SERVICE_INFO");
        }
        $this->logger->trace("service_info",__FILE__,__LINE__,$frame["service_info"]);

        //非表示設定
        $ignore_menu = $this->config->getAll("IGNORE_MENU");
        if( $frame["login_type"] == "invite" ||
            $frame["login_type"] == "invitedGuest" ||
          $frame["login_type"] == "presence" ||
            ($frame["member_info"] &&
                ($frame["user_info"]['account_model'] == "member" ||
                 $frame["user_info"]['account_model'] == "centre" ||
                 $frame["user_info"]['account_model'] == "free")
                )
            ) {
            $ignore_menu["login_as_admin"] = 1;
        }
        // アカウントモデルによる上書き
        if( $frame["user_info"]["account_model"] == "member" || $frame["user_info"]["account_model"] == "centre" || $frame["user_info"]["account_model"] == "free" ){
            if ($frame["user_info"]["account_model"] == "free") {
                $ignore_menu["enter_as_audience"]   = 1;
            }
            $ignore_menu["tool_presence_app"]   = 1;
            $ignore_menu["audience_manual"]     = 1;
            $ignore_menu["advertise"]           = 1;
            $ignore_menu["display_address"]     = 1;
            // メンバーでのログイン
            if( $frame["member_info"] && !$frame["user_info"]["member_multi_room_flg"]){
                $ignore_menu["display_room_list"] = 1;
                $ignore_menu["room_select"] = 1;
            }
            // ユーザーでのログイン
            else if (!$frame["member_info"]) {
                $ignore_menu["enter_meeting"] = 1;
            }
            if ($frame["user_info"]["account_model"] != "centre" ) {
                $ignore_menu["admin_log_plan"]       = 1;
                $ignore_menu["admin_log_fee"]        = 1;
            }
        } else if (!$frame["member_info"] && $frame["user_info"]["use_sales"] && $frame["service_mode"] == "sales") {
            //セールスモードユーザーでログインの際は入室ボタン非表示
            $ignore_menu["enter_meeting"] = 1;
        }
        if($frame["user_info"]["max_storage_size"] == 0){
            $ignore_menu["use_storage"] = 1;
        }


        $frame["ignore_menu"] = $ignore_menu;
        $frame["n2my"] = $this->config->getAll("N2MY");
        $EZDate = new EZDate();
        date_default_timezone_set($this->config->get('GLOBAL', 'time_zone_name', date_default_timezone_get()));
        $server_time_zone = (int)(substr( date( 'O' ), 0, 3));
        $time_zone = $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE);
        $frame["message"]           = $this->_message["DEFINE"];
        $frame["base_url"]          = N2MY_BASE_URL;
        $frame["base_dir"]          = "/";
        $frame["self"]              = $_SERVER["SCRIPT_NAME"];
        $frame["debug"]             = $this->config->get("N2MY", "debug");
        $frame["country_key"]       = $this->session->get("country_key", _EZSESSION_NAMESPACE, "auto");
        $frame["selected_country_key"]       = $this->session->get("selected_country_key", _EZSESSION_NAMESPACE, "auto");
        $frame["country_list"]      = $this->get_country_list();
        $frame["passage_time"]      = $this->_get_passage_time();
        $frame["server_time"]       = time();
        $frame["server_time_zone"]  = $server_time_zone;
        $frame["time_zone"]         = $time_zone;
        $frame["time_zone_list"]    = $this->get_timezone_list();
        $frame["user_time"]         = $EZDate->getLocateTime(time(), $time_zone, $server_time_zone);
        $frame["center_admin_login"] = $this->session->get("center_admin_login");
        $frame["vcubeid_login"]     = $this->session->get("vcubeid_login");
        if($frame["user_info"]["meeting_version"] != null){
            $frame["version"] = $this->_getMeetingVersion();
        }else{
            $frame["version"] = '4.6.5.0';
        }
        $frame["meeting_version"] = $this->_getMeetingVersion();
        $this->template->assign("__frame",$frame);
        // テンプレートの拡張子でContent-Type判定
        switch (substr($template_file, strrpos($template_file, '.') + 1)) {
            case "xml":
                if (!$encrypt) {
                    header("Content-Type: text/xml; charset=UTF-8;");
                }
                break;
            default:
                header("Expires: Fri, 01 Jan 1990 00:00:00 GMT");
                header("Content-Type: text/html; charset=".$char_set.";");
                header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0;");
                header("Pragma: no-cache");
                break;
        }
        $this->template->assign("login_type" , $this->session->get('login_type'));
        $this->template->display($template_file, null, null, $encrypt);
        return true;
    }

    /**
     * ユーザごとのワークエリアを作成して、パスを返す
     */
    function get_work_dir()
    {
        require_once("lib/EZLib/EZUtil/EZFile.class.php");
        $user_info = $this->session->get("user_info");
        $client_dir = N2MY_APP_DIR."/var/".$_SERVER["REMOTE_ADDR"]."/".$user_info["user_id"];
        if (!is_dir($client_dir)) {
            if (!EZFile::mkdir_r($client_dir, 0777)) {
                $this->logger2->error($client_dir, "#error_mkdir");
            } else {
                chmod($client_dir, 0777);
            }
        }
        return $client_dir;
    }

    /**
     * ログイン時、ログアウト時などにワークエリア内のファイルを削除
     */
    function clear_dir()
    {

    }

    /**
     * タイプに併せて配列変換し出力
     *
     * @param string $output_type
     * @param string $data
     */
    function opendata($data, $_output_type = "") {
        if ($_output_type) {
            $this->output_type = $_output_type;
        } else if ($this->request->get("output_type")) {
            $this->output_type = $this->request->get("output_type");
        } else  if (get_class($this->session) == 'EZSession') {
            $this->output_type = $this->session->get("output_type");
        }
        if ($this->output_type == "php") {
            $output = unserialize($data);
            $this->logger->info("unserial",__FILE__,__LINE__,$output);
            return $output;
        } else if ($this->output_type == "json") {
            $output = json_decode($data, true);
            return $output;
        } else {
            require_once("lib/pear/XML/Unserializer.php");
            $options = array(
              XML_UNSERIALIZER_OPTION_ATTRIBUTES_PARSE => 'parseAttributes'
            );
            $unserializer = new XML_Unserializer($options);
            $unserializer->unserialize($data);
            $result["result"] = $unserializer->getUnserializedData();
            $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, $result);
            return $result;
        }
    }

    function error_check($data, $rules) {
        $check_obj = new EZValidator($data);
        foreach($rules as $field => $rule) {
            $check_obj->check($field, $rule);
        }
        return $check_obj;
    }

    function get_error_info($err_obj) {
        static $err_cd_list;
        $err_fields = $err_obj->error_fields();
        if (!$err_fields) {
            return array();
        } else {
            $err_list = array();
            // v1の互換性保持のため
            if ($this->getApiVersion() == 'v1') {
                foreach($err_fields as $field) {
                    $type = $err_obj->get_error_type($field);
                    $err_rule = $err_obj->get_error_rule($field, $type);
                    if (($type == "allow") || $type == "deny") {
                        $err_rule = join(", ", $err_rule);
                    }
                    $err_list["errors"][] = array(
                        "field"   => $field,
                        "err_cd"  => $type,
                        "err_msg" => sprintf($this->get_message("ERROR", $type), $err_rule),
                        );
                }
            // v1以降のインタフェース
            } else {
                if (!$err_cd_list) {
                    // エラーメッセージのマッピング用
                    $api_erro_cd_file = N2MY_APP_DIR."config/lang/api/message.yaml";
                    require_once("lib/spyc/spyc.php");
                    $err_cd_list = Spyc::YAMLLoad(file_get_contents($api_erro_cd_file));
                    // エラーメッセージ用
                    $api_erro_msg_file = N2MY_APP_DIR."config/lang/en_US/message.ini";
                    $err_msg_list = parse_ini_file($api_erro_msg_file, true);
                }
                foreach($err_fields as $field) {
                    $type = $err_obj->get_error_type($field);
                    $err_rule = $err_obj->get_error_rule($field, $type);
                    if (($type == "allow") || $type == "deny") {
                        $err_rule = join(", ", $err_rule);
                    }
                    //
                    if (in_array($type, $err_cd_list['common'])) {
                        $err_cd = array_search($type, $err_cd_list['common']);
                        $err_msg = $err_msg_list['ERROR'][$type];
                    } else {
                        $err_cd = $type;
                        $type = $err_cd_list['appli'][$type];
                        $err_msg = $err_msg_list['DEFINE'][$type];
                    }
                    $err_list["errors"][] = array(
                        "field"   => $field,
                        "err_cd"  => $err_cd,
                        "err_msg" => sprintf($err_msg, $err_rule),
                        );
                }
            }
            return $err_list;
        }
    }

    function ticket_to_session($ticket) {
        if ($ticket) {
            require_once "classes/core/dbi/Meeting.dbi.php";
            $meeting = new DBI_Meeting($this->get_dsn());
            $where = "meeting_ticket = '".addslashes($ticket)."'";
            $meeting_session_id = $meeting->getOne($where, "meeting_session_id");
            return $meeting_session_id;
        }
    }

    function session_to_ticket($session_id) {
        if ($session_id) {
            require_once "classes/core/dbi/Meeting.dbi.php";
            $meeting = new DBI_Meeting($this->get_dsn());
            $where = "meeting_session_id = '".addslashes($session_id)."'";
            $meeting_ticket = $meeting->getOne($where, "meeting_ticket");
            return $meeting_ticket;
        }
    }

    /**
     *
     */
    function _get_member_id($user_key, $member_key = null) {
        static $member_list = array();
        if (!$user_key || !$member_key) {
            return null;
        }
        if (!$member_list[$user_key]) {
            require_once("classes/dbi/member.dbi.php");
            $objMember = new MemberTable($this->get_dsn());
            $where = "user_key = ".addslashes($user_key).
                 " AND member_status = 0";
            $rs = $objMember->select($where, null, null, null, 'member_key, member_id');
            if (DB::isError($rs)) {
                $this->logger2->error($rs->getUserInfo());
                $member_list[$user_key] = null;
                return null;
            }
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                $member_list[$user_key][$row['member_key']] = $row['member_id'];
            }
        }
        if (isset($member_list[$user_key][$member_key])) {
            return $member_list[$user_key][$member_key];
        } else {
            return null;
        }
    }
    /**
     * 操作ログ
  */
    function add_operation_log($action_name, $info = null) {
      if ($this->checkIgnoreRemoteAddress($_SERVER["REMOTE_ADDR"])) {
          $this->logger->debug('ignore remote addr : '.$_SERVER["REMOTE_ADDR"]);
          return false;
      }
      $user_info = $this->session->get('user_info');
      $member_info = $this->session->get('member_info');
      if (!$user_info["user_key"]) {
        $this->logger->warn(array($action_name, $info), 'Who are you?');
        return false;
      }
      //操作ログ追加
      require_once ("classes/dbi/operation_log.dbi.php");
      $objOperationLog = new OperationLogTable($this->get_dsn());
      $data = array(
          "user_key"           => $user_info["user_key"],
          "member_key"         => $member_info["member_key"],
          "member_id"          => $member_info["member_id"],
          "session_id"         => session_id(),
          "action_name"        => $action_name,
          "remote_addr"        => $_SERVER["REMOTE_ADDR"],
          "info"               => serialize($info),
          "operation_datetime" => date( "Y-m-d H:i:s" ),
      );
      //$this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$data);
      $objOperationLog->add($data);
      return true;
    }



    function add_user_agent() {
        require_once("classes/mgm/dbi/user_agent.dbi.php");
        $user_agent = new MgmUserAgentTable(N2MY_MDB_DSN);
        $request     = $this->request->getAll();
        if (!$server_info = $this->session->get("server_info")) {
            $this->logger2->warn("no server info");
            return false;
        }
        require_once "lib/EZLib/EZUtil/EZUserAgent.class.php";
        $objUserAgent = EZUserAgent::getInstance();
        $browser_info = $objUserAgent->getBrowser();
        if (!$user_info = $this->session->get("user_info")) {
            require_once("classes/dbi/meeting.dbi.php");
            $server_info = $this->session->get("server_info");
            $dsn = $server_info["dsn"];
            $objMeeting = new MeetingTable($dsn);
            $where = "meeting_key = '".$this->session->get("meeting_key")."'";
            $user_key = $objMeeting->getOne($where, "user_key");
            if ($user_key) {
                require_once("classes/dbi/user.dbi.php");
                $objUser = new UserTable($dsn);
                $where = "user_key = '".$user_key."'";
                $user_info = $objUser->getRow($where);
            }
        }
        $login_type = $this->session->get("login_type");
        $user_agent_info = array(
            "db_key"            => $server_info["host_name"] ,
            "user_id"           => $user_info["user_id"],
            "invoice_flg"       => $user_info["invoice_flg"],
            "meeting_key"       => $this->session->get("meeting_key"),
            "participant_key"   => $this->session->get("participant_key"),
            "appli_type"        => $this->session->get("meeting_type"),
            "appli_mode"        => ($user_info["user_status"] == 2) ? "trial" : "general",
            "appli_role"        => ($login_type) ? $login_type : "default",
            "appli_version"     => $request["appli_version"],
            "user_agent"        => $_SERVER["HTTP_USER_AGENT"],
            "os"                => $browser_info["platform"],
            "browser"           => $browser_info["parent"],
            "flash"             => $request["flash"],
            "protcol"           => $request["protocol"],
            "port"              => is_numeric($request["port"]) ? $request["port"] : "",
            "ip"                => $_SERVER["REMOTE_ADDR"],
            "host"              => gethostbyaddr($_SERVER["REMOTE_ADDR"]),
            "monitor_size"      => $request["monitor_size"],
            "monitor_color"     => $request["monitor_color"],
            "camera"            => $request["camera"],
            "mic"               => $request["mic"],
            "speed"             => is_numeric($request["speed"]) ? $request["speed"] : ""
        );
        $error = $user_agent->check($user_agent_info);
        if (EZValidator::isError($error)) {
            $this->logger2->warn(array($user_agent_info,$error));
        } else {
            $ret = $user_agent->add($user_agent_info);
        }
    }

    function getMeetingVersion() {
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }

    function slimXMLArray($data) {
        $result = "";
        if(array_key_exists ("_data" ,$data) && $data["_data"] != null){
            $result = $data["_data"];
        } else {
            foreach($data as $key => $value)
            {
                if($key == "0" && !array_key_exists ("1" ,$data)){
                    $result = $this->slimXMLArray($value);
                } else {
                    if($value)
                        $result[$key] = $this->slimXMLArray($value);
                    else if(strpos($key,'_attr') !== false && strpos($key,'_data') !== false)
                        $result[$key] = "";
                }
            }
        }
        return $result;
    }

    /*
     * 4.9.10.6 ADDED
     * @param ipAddress
     * @return country_key By MAXMIND
     */
    function detectCountryByIPAddress($ipAddress){
        if(!$ipAddress){
            return false;
        }
        $limit_time = time() + 365 * 24 * 3600;
        //MaxMindでIPから所在地を検索
        $maxmind_url = $this->config->get("MAXMIND", "url");
        $maxmind_key = $this->config->get("MAXMIND", "key");
        if (!$maxmind_url || !$maxmind_key) {
            return false;
        } else {
            require_once ("lib/maxmind/geoip.php");
            $obj_MaxMind = new MaxMaindApp();
            $result_country_key = $obj_MaxMind->getCountryCord($maxmind_url, $maxmind_key, $ipAddress);
            if(!$result_country_key){
                return false;
            }
            return $result_country_key;
        }
    }

    private function _getMeetingVersion() {
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }

    private function checkIgnoreRemoteAddress($remote_addr=null)
    {
        require_once "lib/pear/Net/IPv4.php";
        $ipv4 = new Net_IPv4();
        $ignoreIpList = $this->config->get("N2MY", "ignore_remote_addr");
        $ipList = explode(",", $ignoreIpList);
        $this->logger2->debug($remote_addr);
        if (count($ipList) < 1) {
            return true;
        } else if($remote_addr == null) {
            $this->logger2->error(array($remote_addr));
            return false;
        } else {
            foreach($ipList as $ip) {
                // IP
                if ($ipv4->validateIP($ip)) {
                    if ($ip == $remote_addr) return true;
                }
            }
        }
        return false;
    }

    function getRoomListInWhiteList($member_key){
        $user_info = $this->session->get('user_info');
        require_once ("classes/ONE_Account.class.php");
        $obj_ONE_Account = new ONE_Account($this->get_dsn());
        $room_info = $obj_ONE_Account->get_ONE_whitelist_room_list($user_info['user_key'], $member_key);
        if($room_info){
            return $room_info;
        }
        return false;
    }

    function sendReport($mail_from, $mail_to, $mail_subject, $mail_body) {
        require_once("lib/EZLib/EZMail/EZSmtp.class.php");
        // 入力チェック
        if (!$mail_from || !$mail_to || !$mail_subject || !$mail_body) {
            return false;
        }
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang = EZLanguage::getLangCd("ja");
        $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
        $obj_SendMail->setFrom($mail_from);
        $obj_SendMail->setReturnPath($mail_from);
        $obj_SendMail->setTo($mail_to);
        $obj_SendMail->setSubject($mail_subject);
        $obj_SendMail->setBody($mail_body);
        // メール送信
        $result = $obj_SendMail->send();
        $this->logger2->info(array($mail_from, $mail_to, $mail_subject, $mail_body));
    }

    /**
     * account_planがONEかチェック
     * ONE=true、ONE以外=false
     * @see classes/AppFrame.class.php::check_one_account
     */
    protected function check_one_account() {
        $user_info = $this->session->get("user_info");
        $account_plan = $user_info["account_plan"];
        require_once ("classes/ONE_Account.class.php");
        $obj_ONE_Account = new ONE_Account($this->get_dsn());
        return $obj_ONE_Account->is_one_account($account_plan);
    }
}
