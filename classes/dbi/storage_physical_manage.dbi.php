<?php
require_once("classes/N2MY_DBI.class.php");

/**
 * ストレージファイルの物理削除を管理
 */
class StoragePhysicalManageTable extends N2MY_DB {
	
	const TABLE = 'storage_physical_manage';
	
	const UNTREATED = 0;
	const SUCCESS = 1;
	const ERROR = -1;
	
	public function __construct( $dsn ) {
		parent::init( $dsn, self::TABLE );
	}
	
	/**
	 * 未処理リストを取得
	 */
	public function getUntreatedStorageFileList( $phsical_delete_span_day ) {
		$before = date( 'Y-m-d H:i:s', strtotime( sprintf( '%s -%s day', date( 'Y-m-d' ), $phsical_delete_span_day ) ) );
		$where[] = sprintf( 'status=%d', self::UNTREATED );
		$where[] = 'is_pending=0';
		$where[] = sprintf( 'create_datetime<"%s"', $before );
		return parent::getRowsAssoc( sprintf( '(%s)', join( ')AND(', $where ) ) );
	}
	
	/**
	 * 物理削除対象リストを登録
	 * @param array $storage_file_keys
	 * @param int $member_key
	 */
	public function add_storage_file_keys( $storage_file_keys, $member_key ) {
		if ( !is_array( $storage_file_keys ) || empty( $storage_file_keys ) || !is_numeric( $member_key ) ) return false;
		
		$columns = array('storage_file_key', 'member_key', 'status', 'create_datetime');
		$values = array();
		$date = date( 'Y-m-d H:i:s' );
		foreach ($storage_file_keys as $storage_file_key){
			$values[] = join( ',', array (
					$storage_file_key,
					$member_key,
					self::UNTREATED,
					sprintf( '"%s"', $date )
			) );
		}
		$sql = sprintf( 'INSERT INTO `%s` (`%s`) VALUES (%s);', self::TABLE, join( '`,`', $columns ), join( '),(', $values ) );
		if ( PEAR::isError( $res = $this->_conn->query( $sql ) ) ) {
			$this->logger2->error( $res->getUserInfo() );
			return false;
		}
		return true;
	}
	
	public function setSuccess( $storage_physical_manage_key ) {
		return $this->setStatus( $storage_physical_manage_key, self::SUCCESS );
	}

	public function setError( $storage_physical_manage_key ) {
		$this->logger2->error( array (
				'storage_physical_manage_key' => $storage_physical_manage_key
		), 'please check storage_physical' );
		return $this->setStatus( $storage_physical_manage_key, self::ERROR );
	}
	
	// TODO implement operation from admin_tool
	public function setPend( $storage_physical_manage_key ) {
		return $this->setStatus( $storage_physical_manage_key, self::PEND );
	}
	
	private function setStatus( $storage_physical_manage_key, $status ) {
		return $this->update(array(
				'status' => $status,
		), sprintf( 'storage_physical_manage_key=%d', $storage_physical_manage_key ) );
	}
	
	public function update( $data, $where ) {
		$data['update_datetime'] = date('Y-m-d H:i:s');
		return parent::update( $data, $where );
	}
	
}