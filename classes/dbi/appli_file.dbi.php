<?php
require_once("classes/N2MY_DBI.class.php");

# アプリケーションファイルディレクトリ
define('APPLICATION_FILES_DIR',   'htdocs/shared/appli/box/package/');
# アプリケーションファイルURL
define('APPLICATION_FILES_URL',   'shared/appli/box/package/');
# Installerファイル名
define('INSTALLER_FILE_NAME',     'RebootCenterSTB.exe');
# Binaryファイル名
define('BINARY_FILE_NAME',        'app.zip');

class AppliFileTable extends N2MY_DB {

    var $table = 'appli_file';
    protected $primary_key = "appli_file_key";

    function __construct($dsn) {
        $this->init($dsn, $this->table);
    }

    function add($data) {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $res = parent::add($data);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true));
        }
        //get answersheet_key
        return $res;
    }
    
    function update($data, $where) {
    	$data["update_datetime"] = date("Y-m-d H:i:s");
    	$res = parent::update($data, $where);
    	if (PEAR::isError($res)) {
    		throw new Exception("db errror where:$where , data:".print_r($data,true));
    	}
    	//get answersheet_key
    	return $res;
    }
}
