<?php
require_once("classes/N2MY_DBI.class.php");

class MeetingMemberUseLogTable extends N2MY_DB
{

    var $table = "meeting_member_use_log";
    protected $primary_key = "log_no";

    function __construct( $dsn ) {
        $this->init( $dsn, "meeting_member_use_log" );
    }
}
