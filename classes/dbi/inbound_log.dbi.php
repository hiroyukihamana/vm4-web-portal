<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:kiyomizu                                                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once("classes/N2MY_DBI.class.php");

class InboundLogTable extends N2MY_DB {

    protected $primary_key = "inbound_log_key";

    function InboundLogTable($dsn) {
        $this->init($dsn, "inbound_log");
    }

    function add($data) {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
