<?php
require_once("classes/N2MY_DBI.class.php");

/**
 * Created on 2006/02/15
 * 
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class BillingDetailInfoTable extends N2MY_DB {

    var $table = 'billing_detail_info';
    protected $primary_key = "billing_detail_info_key";

    
    /**
     * DB接続
     */
	function BillingDetailInfoTable($dsn) 
	{
        $this->init($dsn, $this->table);
        $this->logger = EZLogger::getInstance();
	}
	
}
