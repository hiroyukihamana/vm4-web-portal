<?php
require_once("classes/N2MY_DBI.class.php");

class EntrymodeOptionTable extends N2MY_DB
{

    const MODE_ADD = 1;
    const MODE_UPDATE = 2;
    
    var $table = 'entrymode_option';
    protected $primary_key = "entrymode_option_key";

    function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }

    function update( $mode, $data, $where) {
        // validate
        switch ( $mode ) {
            case self::MODE_UPDATE :
                break;
            default :
                $this->logger2->warn( $function_mode, '$function_mode is not allow' );
                return false;
                break;
        }
        return parent::update( $data , $where );
    }

    function add( $mode, $data ) {
        // validate
        switch ( $mode ) {
            case self:: MODE_ADD:
                break;
            default :
                $this->logger2->warn( $function_mode, '$function_mode is not allow' );
                return false;
                break;
        }
            
        $data["create_datetime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }

    public function getThreshold( $user_key ) {
        $where = "user_key = ". $user_key;
        $entry_mode_data = $this->getRow($where);
        return $entry_mode_data["threshold"];
    }
    
    public function port_plan_freetype_setting_modify($user_key, $orderKey , $sort, $threshold_cnt) {

        // オプション情報の内容チェック
        if($orderKey == "TIME" || $orderKey == "PARTICIPANT"){
            //nop
        }else{
            $orderKey = "TIME";
        }

        if($sort == "ASC" || $sort == "DESC"){
            //nop
        }else{
            $sort = "DESC";
        }
        
        $threshold = substr($threshold_cnt, 0, 1);
        if (preg_match("/^[0-5]/", $threshold)) {
            //nop
        }else{
            $threshold = '0';
        }
        
        $data  = array(
                        "user_key"          => $user_key,
                        "threshold"         => $threshold,
                        "target"            => $orderKey,
                        "sort"              => $sort,
                        "entrymode_option_updatetime"   => date("Y-m-d H:i:s"),
        );
        $where = "user_key = ". $user_key;
        $entry_mode_data = $this->getRow($where);

        if($entry_mode_data){
            $this->update(self:: MODE_UPDATE,$data , $where);
        }else{
            $this->add(self:: MODE_ADD,$data);  
            }
    }
}
