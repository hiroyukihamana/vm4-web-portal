<?php
require_once("classes/N2MY_DBI.class.php");

class TmpFiletTable extends N2MY_DB
{

    var $table = 'tmp_file';
    protected $primary_key = "tmp_file_key";

    function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }

    function add( $data ) {
        $data["create_datetime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }

    public function update($data, $where)
    {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }
}

