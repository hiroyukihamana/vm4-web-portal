<?php
require_once("classes/N2MY_DBI.class.php");

class AddressBookTable extends N2MY_DB {

    var $table = 'address_book';
    protected $primary_key = "address_book_key";
    var $rules = array(
        "name" => array(
            "required" => true,
            "maxlen" => 50,
            ),
        "name_kana" => array(
            "maxlen" => 50
            ),
        "group_name" => array(
            "maxlen" => 50
            ),
        "email" => array(
            "required" => true,
            "email" => true,
            "maxlen" => 50,
            ),
        "timezone" => array(
            "numeric" => true,
            ),
        "lang" => array(
            "required" => true,
            "minlen" => 2,
            "maxlen" => 10,
            ),
    );

    var $max_address_cnt = 2000;

    /**
     * DB接続
     */
    function AddressBookTable($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }

    function add($data) {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $data["update_datetime"] = date("Y-m-d H:i:s");
        $this->logger2->debug($data);
        return parent::add($data);
    }

    function update($data, $where) {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        $this->logger2->debug($data);
        return parent::update($data, $where);
    }

    function getActiveCount($user_key, $member_key = "") {
        $where = "user_key = ".$user_key;
        if ($member_key) {
            $where .= " AND member_key = ".$member_key;
        } else {
        	$where .= " AND member_key IS NULL";
        }
        $where .= " AND is_deleted = 0";
        return $this->numRows($where);
    }

    function check($data, $rules = null) {
        $check_obj = parent::check($data, $rules);
        $count = $this->getActiveCount($data["user_key"], $data["member_key"]);
        $this->logger2->debug($count);
        if ($count >= $this->max_address_cnt) {
            $check_obj->set_error("_form", "limit_count", $this->max_address_cnt);
        }
        return $check_obj;
    }

    /*
     * メールアドレスが登録されているか確認する
     */
    function recordCheck($mail_address , $user_key, $member_key = null){
        $where = "email = '" . addslashes($mail_address) . "' AND user_key = " . $user_key . " AND is_deleted = 0 ";
        if($member_key){
            $where = $where . " AND member_key = " . $member_key;
        }else{
            $where = $where . " AND member_key IS NULL";
        }
        return $this->numRows($where);
    }


    function addressDeleteCheck($mail_address , $user_key, $member_key = null){
        $where = "email = '" . addslashes($mail_address) . "' AND user_key = " . $user_key . " AND is_deleted = 1 ";
        if($member_key){
            $where = $where . " AND member_key = " . $member_key;
        }else{
            $where = $where . " AND member_key IS NULL";
        }
        return $this->getRow($where);
    }

}
