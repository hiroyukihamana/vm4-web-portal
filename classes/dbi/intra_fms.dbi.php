<?php
require_once("classes/N2MY_DBI.class.php");

class DBI_IntraFmsServer extends N2MY_DB
{

    var $table = "user_fms_server";
    protected $primary_key = "fms_key";

    function __construct( $dsn ) {
        $this->init( $dsn, "user_fms_server" );
        $this->config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
    }
}
