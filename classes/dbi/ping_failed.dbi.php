<?php
require_once("classes/N2MY_DBI.class.php");

class PingFailedTable extends N2MY_DB
{

    var $table = 'ping_failed';
    protected $primary_key = "ping_failed_id";

    function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }
}

