<?php
require_once("classes/N2MY_DBI.class.php");

class FmsDenyTable extends N2MY_DB {

    var $table = "fms_deny";
    var $logger = null;
    protected $primary_key = null;

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }
    public function add($data)
    {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $res = parent::add($data);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true));
        }
    }

    public function update($data, $where)
    {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        $res = parent::update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true));
        }
    }

    // findByUserKey
    public function findByUserKey($user_key, $sort = array(), $limit = null, $offset = 0, $columns = "*", $and_where = null)
    {
        if (!$user_key) {
            throw new Exception("user_key is empty");
        }
        $where  = sprintf("user_key ='%s' AND status = 1 ", mysql_real_escape_string($user_key));
        if ($and_where) {
            $where = $where .' AND '.$and_where;
        }

        $db_res = $this->select($where, $sort, $limit, $offset, $columns);
        if (!$db_res || PEAR::isError($db_res)) {
            throw new Exception("db errror where:".$db_res->getQuery());
        }

        $res = array();
        while ($row = $db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }
        return $res;
    }
}
