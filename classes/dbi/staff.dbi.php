<?php
require_once("classes/N2MY_DBI.class.php");

/*
 * Created on 2006/02/15
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class StaffTable extends N2MY_DB {

    var $table = 'staff';
    protected $primary_key = "staff_key";

    /**
     * DB接続
     */
    function StaffTable($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }

}
