<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:kiyomizu                                                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once('EZDB/EZDB.class.php');
require_once('EZHTML/EZValidator.class.php');

class Ask extends EZDB {

    var $table = "ask";
    var $logger = null;
    var $rules = array(
        //"ask_category"          => array("required" => false),
        "ask_contents"          => array("required" => true),
        "ask_company_name"      => array("required" => true),
        //"ask_company_division"  => array("required" => false),
        //"ask_name_kana"         => array("required" => false),
        "ask_name"              => array("required" => true),
        "ask_email"             => array("required" => true,
                                      "email"    => true)
        //"ask_telephone"         => array("required" => false,
        //                              "tel_ja_easy"    => false),
        //"ask_fax"                 => array("required" => false,
        //                              "tel_ja_easy"    => false)
    );

    var $error_rules = array( "ask_contents", "required", "内容が入力されていません");


    function Ask($dsn) {
        parent::EZDB($dsn);
        $this->logger = EZLogger::getInstance();
    }

    /**
     * 入力チェック
     *
     * @author kiyomizu
     * @param array $data フィールド名を連想配列形式で渡す
     * @return object EZValidatorのインスタンス
     *
     * EZValidator::isError()などでエラーの存在チェックを行ってください。
     */
    function check($data) {
        $check_obj = new EZValidator($data);
        foreach($this->rules as $field => $rules) {
            foreach($rules as $rule => $_val) {
                $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, array($field, '%aaa%', $rule, $_val));
                $check_obj->addRule($field, '%aaa%', $rule, $_val);
            }
        }
        return $check_obj;
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
