<?php
require_once("classes/N2MY_DBI.class.php");

class UserRoomSettingTable extends N2MY_DB {

    var $table = 'user_room_setting';
    var $logger = null;
    protected $primary_key = "user_room_setting_key";

    function __construct($dsn) {
        $this->init($dsn, $this->table);
        $this->logger = EZLogger::getInstance();
    }
}


?>
