<?php
require_once("classes/N2MY_DBI.class.php");

class SharedFileTable extends N2MY_DB {

    var $table = 'shared_file';
    protected $primary_key = "shared_file_key";

    function SharedFileTable($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }

    function getSequenceFileSize($meeting_sequence_key){
        $where = "status = 1 AND meeting_sequence_key = " . $meeting_sequence_key;
        $result = $this->getOne($where , "SUM( file_size ) AS size");
        if(!$result){
            $result = 0;
        }
        return $result;
    }

    function getMeetingFileSize($meeting_key){
        $where = "status = 1 AND meeting_key = " . $meeting_key;
        $result = $this->getOne($where , "SUM( file_size ) AS size");
        if(!$result){
            $result = 0;
        }
        return $result;
    }
}
