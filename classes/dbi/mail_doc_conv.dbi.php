<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:kiyomizu                                                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once("classes/N2MY_DBI.class.php");

class MailDocConvet extends N2MY_DB {

    var $user_key = null;
    var $rules = array(
            "name" => array(
                "required" => true,
                ),
            "mail_address" => array(
                "required" => true,
                "regex" => '/^([*+!.&#$|\'\\%\/0-9a-zA-Z^_`{}=?~:-]+)?@((?:[-a-zA-Z0-9]+\.)+[a-zA-Z]{2,6})$/',
                ),
        );
    var $primary_key = "uniq_key";

    function MailDocConvet($dsn, $user_key = null) {
        $this->init($dsn, "mail_doc_conv");
        if ($user_key) {
            $this->user_key = $user_key;
        }
    }

    function getList($where = null, $sort_key = null, $sort_type = null, $limit = null, $offset = 0, $columns = "*") {
        if ($this->user_key) {
            $where .= ($where) ? " AND" : "";
            $where .= " user_key = ". $this->user_key.
                " AND is_active = 1";
        }
        if ($sort_key) {
            $sort = array($sort_key => $sort_type);
        } else {
            $sort = null;
        }
        return $this->getRowsAssoc($where, $sort, $limit, $offset, $columns);
    }

    function add($data) {
        $data["user_key"] = $this->user_key;
        $data["is_active"] = 1;
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $data["update_datetime"] = date("Y-m-d H:i:s");
        $ret = parent::add($data);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return $ret;
    }

    function update($data, $where) {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        if ($this->user_key) {
            $where .= ($where) ? " AND" : "";
            $where .= " user_key = ". $this->user_key;
        }
        $ret = parent::update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            print $ret->getUserInfo();
        }
    }

    function delete($where) {
        $data["is_active"] = 0;
        $data["update_datetime"] = date("Y-m-d H:i:s");
        if ($this->user_key) {
            $where .= ($where) ? " AND" : "";
            $where .= " user_key = ". $this->user_key;
        }
        $ret = parent::update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            print $ret->getUserInfo();
        }
    }

}


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
