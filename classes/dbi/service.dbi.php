<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:kiyomizu                                                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once("classes/N2MY_DBI.class.php");

class ServiceTable extends N2MY_DB {

    function ServiceTable($dsn) {
        $this->init($dsn, "service");
    }

//    function getDetail ($service_key)
//    {
//        $where = "service_key = ".$service_key;
//        $rs = $this->select($where, null, null, 1, null);
//        if (DB::isError($rs)) {
//            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
//            return $rs;
//        }
//        $row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
//        return $row;
//    }



    function add($data) {
        $data["service_registtime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    /**
     * one_id_host_planのみのservice_keyを返す
     * @return
     */
    function getIdHostPlanKeys(){
        $where = "service_status = 1 AND is_one_id_host = 1";
        return $this->getCol($where,"service_key");
    }

}


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
