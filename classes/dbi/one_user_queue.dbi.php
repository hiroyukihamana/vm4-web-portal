<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class OneUserQueue extends N2MY_DB {

    var $table = "one_user_queue";
    var $logger = null;
    protected $primary_key = null;

    const MODE_UPDATE = 0;
    const MODE_DELETE = 2;

    function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }

    public function addQuser($user_key,$mode = null)
    {
        if($mode === null) {
            $mode = 2;
        }
        $data = array(
                     "user_key"  => mysql_real_escape_string($user_key),
                     "mode"  => $mode,
                     "error_count" => 0,
                     "pend"        => 0,
                     );
        $res = $this->add($data);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function updateQmode($user_key,$mode = null)
    {
        if($mode===null) {
            return array(false,"not set mode");
        }
        $where = ("user_key = '" . mysql_real_escape_string($user_key) . "' AND pend = 0" );
        $data = array(
                    "mode" => $mode,
                     );
        $res = $this->update($data,$where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function updateQmodeFromQueueKey($one_user_queue_key,$mode = null)
    {
        if($mode===null) {
            return array(false,"not set mode");
        }
        $where = ("one_user_queue_key = '" . mysql_real_escape_string($one_user_queue_key) . "'");
        $data = array(
                    "mode" => $mode,
                     );
        $res = $this->update($data,$where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    
    }

    public function getQuser($user_key,$pend = false) {
        $where = ("user_key = '" . mysql_real_escape_string($user_key) . "'");
        if($pend) {
            $where .= "AND pend > 0" ;
        } else {
            $where .= "AND pend = 0" ;
        }
        $res = $this->get($where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function getQuserFromQueueKey($one_user_queue_key) {
        $where = ("one_user_queue_key = '" . mysql_real_escape_string($one_user_queue_key) . "'");
        $res = $this->get($where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function getQnum($num,$mode=null,$pend=null) {
        if($mode===null) {
            $mode = -1;
        }

        if($pend===true) {
            $res = $this->getNum($num,"all",$mode);
        } else {
            $res = $this->getNum($num,null,$mode);
        }
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function getQall($mode=null,$pend = null) {
        if($mode===null) {
            $mode = -1;
        }
        if($pend===true) {
            $res = $this->getNum(0,"all",$mode);
        } else {
            $res = $this->getNum(0,null,$mode);
        }
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function deleteQuser($user_key) {
        $where = ("user_key = '" . mysql_real_escape_string($user_key) . "'");
        $where .= "AND pend = 0";
        $res = $this->delete($where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function deleteQuserFromQueueKey($one_user_queue_key) {
        $where = ("one_user_queue_key = '" . mysql_real_escape_string($one_user_queue_key) . "'");
        $res = $this->delete($where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function deleteQall($mode) {
        $res = $this->deleteAll($mode);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function updateQerr($one_user_queue_key) {
        $where = ("one_user_queue_key = '" . mysql_real_escape_string($one_user_queue_key) . "'");
        $res = $this->get($where);
        $counter = $res["error_count"] + 1;
        $data = array(
                     "error_count" => $counter,
                     );
        $res = $this->update($data,$where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function updateQpend($one_user_queue_key) {
        $where = ("one_user_queue_key = '" . mysql_real_escape_string($one_user_queue_key) . "'");
        $data = array(
                     "pend" => 1,
                     );
        $res = $this->update($data,$where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function getQcount($mode=null) {
        $where = "pend < 2";
        if($mode){
            $where .= "AND mode=" . $mode;
        }
        $res = $this->numRows($where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    function add($data) {
        $data["create_detatime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    function update($data, $where) {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }

    function get($where) {
        return $this->getRow($where,"one_user_queue_key,user_key,mode,error_count,pend");
    }

    function getNum($num,$kind,$mode) {
        $sort  = array("one_user_queue_key" => "ASC");
        if( $kind=="all" ) {
            $where = "pend < 2";
            $limit = $num;
        } else {
            $where = "pend = 0";
            $limit = $num;
        }
        
        if($mode >= 0 ) {
            $where .= " AND mode=" . $mode;
        }
        $data  = $this->getRowsAssoc($where,$sort,$limit,null);
        return $data;
    }

    function delete($where) {
        return parent::remove($where);
    }

    function deleteAll($mode) {
        $where = "pend < 2";
        if($mode >= 0 ) {
            $where .= " AND mode=" . $mode;
        }
        return parent::remove($where);
    }

}
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>