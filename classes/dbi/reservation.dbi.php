<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");
require_once("lib/EZLib/EZUtil/EZDate.class.php");

class ReservationTable extends N2MY_DB {

    var $serverTimeZone = N2MY_SERVER_TIMEZONE;
    var $time_zone = N2MY_SERVER_TIMEZONE;
    protected $primary_key = "reservation_key";

    function ReservationTable($dsn) {
        $this->init($dsn, "reservation");
    }

    /**
     * 会議予約のキャンセル
     *
     * @param string $reservation_session 会議予約ID
     */
    function cancel($reservation_session) {
        $data = array(
            "reservation_status" => 0,
            "reservation_updatetime" => date("Y-m-d H:i:s")
            );
        $where = "reservation_session = '".mysql_real_escape_string($reservation_session)."'";
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $where);
        $this->update($data, $where);
        return $this->getRow( $where );
    }

    /**
     * 会議予約のパスワード確認
     *
     * @param string $reservation_session 会議予約ID
     * @param string $password パスワード
     * @return boolean true:成功、false:失敗
     */
    function checkPassword($reservation_session, $password) {
        $where = "reservation_session = '".mysql_real_escape_string($reservation_session)."'".
            " AND reservation_status = 1";
        $rs = $this->select($where);
        if ($rs) {
            $row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
            // パスワードが設定されていないか、入力したパスワードと一致する場合
            if (($row["reservation_pw"] === "") || ($row["reservation_pw"] === $password)) {
                return true;
            }
        }
        return false;
    }

    /**
     * パスワードの設定有無
     *
     * @param string $reservation_session 会議予約ID
     * @return string 0:パスワードなし、1:パスワードあり、2:会議なし
     */
    function hasPassword($reservation_session) {
        $where = "reservation_session = '".mysql_real_escape_string($reservation_session)."'".
            " AND reservation_status = 1";
        $this->logger->debug($where);
        $rs = $this->select($where, null, null, null, "reservation_pw");
        if ($rs) {
            $row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
            $this->logger->debug(__FUNCTION__, __FILE__, __LINE__,$row);
            if ($row["reservation_pw"] == "") {
                return "0";
            } else {
                return "1";
            }
        } else {
            return "2";
        }
    }

    /**
     * 部屋が指定時間に予約中かどうか
     *
     * @param integer $datetime UNIXタイムスタンプ （デフォルト現在日時）
     * @return mixed 予約がない場合はfalseを、予約がある場合は連想配列で予約内容を返す。
     */
    function hasReservation($room_key, $datetime = null) {
        if ($datetime == null) {
            $datetime = time();
        }
        $where = "room_key = '".mysql_real_escape_string($room_key)."'".
                " AND reservation_starttime <= '".date("Y-m-d H:i:s",$datetime)."'".
                " AND reservation_endtime > '".date("Y-m-d H:i:s",$datetime)."'".
                " AND reservation_status = 1";
        $rs = $this->select($where);
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $rs);
            return $rs;
        } elseif ($rs) {
            $row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
            return $row;
        } else {
            return false;
        }
    }

    /**
     * 指定期間内の一覧取得
     *
     * @param string $room_key 部屋ID
     * @param datetime $starttime 開始日時
     * @param datetime $endtime 終了日時
     * @param string $reservation_session 予約ID
     */
    function getListBetween($room_key, $starttime, $endtime, $reservation_key = null)
    {
        // クエリー
        $where = " room_key = '$room_key'" .
                " AND reservation_status = 1" .
                " AND reservation_endtime != '$starttime'" .
                " AND reservation_starttime != '$endtime'" .
                " AND (" .
                "( reservation_starttime between '$starttime' AND '$endtime' )" .
                " OR ( reservation_endtime between '$starttime' AND '$endtime' )" .
                " OR ( '$starttime' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '$endtime' BETWEEN reservation_starttime AND reservation_endtime ) )";
        // 更新対象は無視
        if ($reservation_key) {
            $where .= " AND reservation_key != " . $reservation_key;
        }
        $rs = $this->select($where, array("reservation_starttime" => "asc"));
        $ret = array();
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
        } else {
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                $ret[] = $row;
            }
        }
        return $ret;
    }

    function getList($where = "", $sort = array(), $limit = null, $offset = 0, $columns = "*") {
        $ezdate = new EZDate();
        return $this->getRowsAssoc($where, $sort, $limit, $offset, $columns);
    }

    /**
     * 入力チェック
     */
    function check($room_key, $data) {
        $message = "";
        if ($data['reservation_name'] === "") {
            $message .= '<li>'.RESERVATION_ERROR_NAME . '</li>';
        }
        if (!$data['reservation_starttime']) {
            $message .= '<li>'.RESERVATION_ERROR_STARTTIME . '</li>';
        }
        if (!$data['reservation_endtime']) {
            $message .= '<li>'.RESERVATION_ERROR_ENDTIME . '</li>';
        }
        $starttime = $data['reservation_starttime'];
        $start_unix_time = strtotime($starttime);

        $endtime = $data['reservation_endtime'];
        $end_unix_time = strtotime($endtime);

        if ($start_unix_time >= $end_unix_time) {
            $message .= '<li>'.RESERVATION_ERROR_INVALIDTIME . '</li>';
        }
        // 重複一覧
        $where = "room_key = '".addslashes($room_key)."'" .
                " AND reservation_status = 1" .
                " AND reservation_endtime != '".$starttime."'" .
                " AND reservation_starttime != '".$endtime."'" .
                " AND (" .
                "( reservation_starttime between '".$starttime."' AND '".$endtime."' )" .
                " OR ( reservation_endtime between '".$starttime."' AND '".$endtime."' )" .
                " OR ( '".$starttime."' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '".$endtime."' BETWEEN reservation_starttime AND reservation_endtime ) )";
        if (isset($data["reservation_key"]) && $data["reservation_key"]) {
            $where .= " AND reservation_key != " . $data["reservation_key"];
        }
        $duplicate_list = $this->getList($where, array("reservation_starttime" => "asc"));
        if ($duplicate_list) {
            $message .= '<li>'.RESERVATION_ERROR_TIME;
            foreach ($duplicate_list as $reservation) {
                $message .= "<br/>&nbsp;&nbsp;&nbsp;[ " .
                    htmlspecialchars($reservation['reservation_name']) . " ] " .
                    date("Y/m/d H:i", EZDate::getLocateTime($reservation['reservation_starttime'],N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE)) . " - " .
                    date("Y/m/d H:i", EZDate::getLocateTime($reservation['reservation_endtime'],N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE));
            }
            $message .= '</li>';
        }
        return $message;
    }

    public function deleteReservationByRoomkey($room_key){
        $where = "room_key = '".mysql_real_escape_string($room_key)."'".
                " AND reservation_endtime > '".date("Y-m-d H:i:s")."'".
                " AND reservation_status = 1";

        $data = array(
                "reservation_status" => 0,
                "reservation_updatetime" => date("Y-m-d H:i:s")
        );
        $this->update($data, $where);
    }
    
    /**
     * MTGVFOUR-827
     * ポート制御モードのマネージャ入室時、S&Sの予約2人部屋であれば、3人に変更
     * @param string $reservation_session 会議予約ID
     * @max_port_count 増やす数
     */
    public function portCountModify( $reservation_session , $max_port_count) {
        $data = array(
            "max_port" => $max_port_count,
            "is_manager" => "2",
            "reservation_updatetime" => date("Y-m-d H:i:s")
            );
        $where = "reservation_session = '".mysql_real_escape_string($reservation_session)."'";
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $where);
        $this->update($data, $where);
    }
}


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
