<?php
require_once("classes/N2MY_DBI.class.php");

class AnswerTable extends N2MY_DB {

    var $table = 'answer';
    protected $primary_key = "answer_key";

    function __construct($dsn) {
        $this->init($dsn, $this->table);
    }

    function add($data) {
        $data["created_datetime"] = date("Y-m-d H:i:s");
        $res = parent::add($data);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true));
        }
        //get answersheet_key
        $where = sprintf( "answersheet_id='%s' AND question_id='%s'", $data['answersheet_id'], $data['question_id']);
        $sort = array( "answer_id" => "desc" );
        $result = $this->getRow( $where, "answer_id", $sort );
        return $result["answer_id"];
    }
}
