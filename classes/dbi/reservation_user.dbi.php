<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:kiyomizu                                                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once("classes/N2MY_DBI.class.php");
require_once("classes/mgm/MGM_Auth.class.php");

class ReservationUserTable extends N2MY_DB {

    protected $primary_key = "r_user_key";

    function ReservationUserTable($dsn) {
        $this->init($dsn, "reservation_user");
        $this->obj_MGMAuthClass = new MGM_AuthClass(N2MY_MDB_DSN);
    }

    /**
     * 招待者リストを取得
     * @param  $reservation_key
     * @param  $manager_flg
     */
    function getUserList($reservation_key , $manager_flg=null) {
        $where = "reservation_key = ".$reservation_key.
            " AND r_user_status = 1";
        if($manager_flg){
            $where .= " AND r_manager_flg = 1";
        }
        $list = $this->getRowsAssoc($where, null, null, null, null, "r_user_session");
        if (DB::isError($list)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$list->getUserInfo());
            return false;
        }
        return $list;
    }
    
    
    /**
     * 招待者リスト単純なリストで取得
     * @param int $reservation_key
     * @return multitype:配列 招待者リスト
     */
    function getUserSimpleList($reservation_key) {
    	$where = "reservation_key = ".$reservation_key.
    	" AND r_user_status = 1";
    	$list = $this->getRowsAssoc($where, null, null, null, null, null);
    	if (DB::isError($list)) {
    		$this->logger->error(__FUNCTION__,__FILE__,__LINE__,$list->getUserInfo());
    		return false;
    	}
    	return $list;
    }

    public function check($guests) {
        $message = "";
        foreach ($guests as $guest) {
            if ($guest["email"]) {
                if (!EZValidator::valid_email($guest["email"])) {
                    $message .= '<li>'.RESERVATION_ERROR_MAIL . "(" . htmlspecialchars($guest["email"]) . ")</li>";
                }
            }
        }
        return $message;
    }
    
    /**
     * 監視者不参加により、対象のr_user_statusを落とす
     * @param int $reservation_key
     */
    public function managerCancel( $reservation_key ) {
        $guests = $this->getUserList($reservation_key);
        foreach($guests as $session_id => $guest) {
            $this->obj_MGMAuthClass->deleteRelationData($session_id, "invite");
        }
        $data = array(
                        "r_user_status" => 0,
                        "r_user_updatetime" => date("Y-m-d H:i:s")
        );
        $where = "reservation_key = '".$reservation_key."' AND r_manager_flg = 1";
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $where);
        $this->update($data, $where);
        return ;
    }
    
    public function cancel( $reservation_key ) {
        $guests = $this->getUserList($reservation_key);
        foreach($guests as $session_id => $guest) {
            $this->obj_MGMAuthClass->deleteRelationData($session_id, "invite");
        }
        $data = array(
            "r_user_status" => 0,
            "r_user_updatetime" => date("Y-m-d H:i:s")
            );
        $where = "reservation_key = '".$reservation_key."'";
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $where);
        $this->update($data, $where);
        return $this->getOne( $where, "reservation_key" );
    }
}


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
