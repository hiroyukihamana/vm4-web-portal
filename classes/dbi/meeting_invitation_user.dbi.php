<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

class MeetingInvitationUserTable extends N2MY_DB {

    var $table = "meeting_invitation_user";
    var $logger = null;
    protected $primary_key = "meeting_invitation_user_key";

    function __construct( $dsn ) {
        $this->init($dsn, $this->table);
    }

    function add($data) {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    function update($data, $where) {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }

}
