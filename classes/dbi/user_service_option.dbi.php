<?php
require_once("classes/N2MY_DBI.class.php");

class UserServiceOptionTable extends N2MY_DB
{

    var $table = 'user_service_option';
    protected $primary_key = "user_service_option_key";

    function __construct( $dsn ) {
        $this->init($dsn, $this->table );
    }
}

