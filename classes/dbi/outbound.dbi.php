<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");
require_once ("lib/EZLib/EZXML/EZXML.class.php");

class OutboundTable extends N2MY_DB {

    var $table = 'outbound';
    protected $primary_key = "outbound_key";

    function OutboundTable($dsn) {
        $this->init($dsn, $this->table);
    }
}