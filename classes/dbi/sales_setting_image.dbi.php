<?php
require_once ("classes/N2MY_DBI.class.php");
require_once ("classes/dbi/user.dbi.php");
require_once('lib/EZLib/EZCore/EZConfig.class.php');

define("SALES_SETTING_BROWSER_TITLE_LENGTH", 64);
class SalesSettingImageTable extends N2MY_DB {

    var $table = 'sales_setting_image';
    var $userTable = null;
    var $config =  null;

    public function SalesSettingImageTable($dsn) {
        $this -> logger = &EZLogger::getInstance();
        $this -> init($dsn, $this -> table);
        $this -> userTable = new UserTable($dsn);
        $this->config = EZConfig::getInstance(N2MY_CONFIG_FILE);
    }

    /**
     * $targetにより変更するロゴが変わる
     * header   = メインページ・ヘッダー
     * logo     = 入室前設定画面のロゴ
     * seatLogo = カスタマー映像枠のロゴ
     * @param $userKey string
     * @param $target string
     */
    public function getImageInfo($userKey, $target) {
        if($this->config->get("IGNORE_MENU" , "sales_custom_logo" , false )){
            return false;
        }
        $userInfo = $this->userTable->getRow( sprintf('user_status=1 AND user_key=%d', $userKey) );
        if (!$userInfo) {
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__, $userKey);
            return false;
        }
        $image = $this->getRow( sprintf( "user_key = %d AND type = '%s' AND status = 1", $userKey, mysql_real_escape_string($target) ) );
        if (!$image) {
            return false;
        }
        $imageBasePath = 'salesSetting/' . $userInfo['user_id'] . '/' . $target . '/' . $image['image_id'] . '.' . $image['extension'];
        $imagePath = N2MY_APP_DIR . $imageBasePath;
        list($width, $height, $type) = getimagesize($imagePath);
        $imageInfo = array(
            'width'    => $width,
            'height'   => $height,
            'type'     => $type,
            'image_id' => $image['image_id']);
        return $imageInfo;
    }
}
