<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

/*
 * ビジネスロジックを含まない純粋なClipテーブルのDBI
 * */

class PGiRateTable extends N2MY_DB {

    var $table = "pgi_rate";
    var $logger = null;
    protected $primary_key = 'pgi_rate_key';

    public function __construct( $dsn ) 
    {
        $this->init($dsn, $this->table );
    }
    public function findByPGiSettingKey($pgi_setting_key)
    {
        return $this->findByPGiSettingKeys(array($pgi_setting_key));
    }
    public function findByPGiSettingKeys($pgi_setting_keys)
    {
        if (!is_array($pgi_setting_keys)) {
            throw new Exception("pgi_setting: is not array ");
        }
        if (count($pgi_setting_keys) <= 0){
            return array();
        }
        foreach ($pgi_setting_keys as $k => $v) {
            $pgi_setting_keys[$k]  = mysql_real_escape_string($v);
        }
        $where  = sprintf("pgi_setting_key in ('%s')", implode($pgi_setting_keys,"','"));

        $db_res = $this->select($where);
        if (!$db_res || PEAR::isError($db_res)) {
            throw new Exception(__FILE__.' '.__LINE__." where : $where");
        }
        $res = array();
        while ($row = $db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }
        return $res;

    }
    public function add($data)
    {
        $data["registtime"] = date("Y-m-d H:i:s");
        $pgi_rate_key = parent::add($data);
        if (PEAR::isError($pgi_rate_key)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true)." mysql said".mysql_error());
        }

        return $pgi_rate_key;
    }
    public function update($data, $where)
    {
        $data["updatetime"] = date("Y-m-d H:i:s");
        $res = parent::update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true).' mysql_error:'.mysql_error());
        }
    }
}
