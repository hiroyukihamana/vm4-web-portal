<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:oneda                                                        |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once("classes/N2MY_DBI.class.php");

class FaqTable extends N2MY_DB {

    function FaqTable($dsn, $lang = "ja") {
        $this->rules = array(

            "faq_key"     => array(
                "required" => true),
            "faq_category"     => array(
                "required" => true),
            "faq_subcategory"     => array(
                "required" => true),
            "faq_question"     => array(
                "required" => true),
            "faq_answer"    => array(
                "required"  => true),
            "faq_status" => array(
                "required" => true,)
        );
        // 言語
        if ($lang == 'en') {
            $this->lang = 1;
        } else {
            $this->lang = 0;
        }
        $this->init($dsn, "faq");
    }
    /**
     * データ取得
     */
    function getList( $model = null ){
        $type = ( $model == "member" ) ? "( faq_type = 1 OR faq_type = 3 )" : "( faq_type = 1 OR faq_type = 2)";
        $sql = "SELECT * FROM faq" .
            " WHERE faq_status = '1'" .
            " AND faq_lang = '".addslashes($this->lang)."'" .
            " AND ". $type .
            " ORDER BY faq_category asc,faq_subcategory asc,faq_order asc";
        $ret = $this->_conn->query($sql);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $ret->getUserInfo());
            return $ret;
        }
        return $ret;
    }

    /**
     * 有効なデータ取得
     */
    function getAllList(){
        $sql = "SELECT * FROM faq" .
            " WHERE faq_status > '0'" .
            " AND faq_lang = '".addslashes($this->lang)."'" .
            " ORDER BY faq_category,faq_subcategory,faq_order";
        $ret = $this->_conn->query($sql);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $sql);
            return $ret;
        }
        return $ret;
    }

    /**
     * 追加／更新
     */
    function complete($news_info, $id = ''){
        if ($news_info) {
            if ($id) {
                $data = array(
                    "faq_category"      => $news_info["cat_num"],
                    "faq_subcategory"   => $news_info["sub_num"],
                    "faq_question"      => $news_info["q"],
                    "faq_answer"        => $news_info["a"],
                    "faq_type"        => $news_info["faq_type"],
                );
                $where = "faq_key = ".$id;
                $ret = $this->update($data, $where);
            } else {
                $query = "SELECT MAX(faq_order) FROM ".$this->table.
                    " WHERE faq_category = ".$news_info["cat_num"].
                    " AND faq_subcategory = ".$news_info["sub_num"].
                    " AND faq_lang = ".$this->lang;
                $order = $this->_conn->getOne($query);
                $order++;
                $data = array(
                    "faq_category"      => $news_info["cat_num"],
                    "faq_subcategory"   => $news_info["sub_num"],
                    "faq_order"         => $order,
                    "faq_question"      => $news_info["q"],
                    "faq_answer"        => $news_info["a"],
                    "faq_status"        => 1,
                    "faq_lang"          => $this->lang,
                    "faq_type"        => $news_info["faq_type"]
                );
                $ret = $this->add($data);
            }
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $this->_conn->last_query);
                return $ret;
            }
            return $ret;
        }
    }

    function setStatus($status, $id) {
        if ($id) {
            $data = array("faq_status" => $status);
            $where = "faq_key = ".$id;
            $ret = $this->update($data, $where);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $this->_conn->last_query);
                return $ret;
            }
            return $ret;
        }
    }

    /**
     * sort
     */
    function sort($id, $sort_type) {
        if ($id) {
            $sql = "SELECT faq_category" .
                    ", faq_subcategory" .
                    ", faq_order" .
                    " FROM ".$this->table.
                    " WHERE faq_key = ".addslashes($id);
            $row = $this->_conn->getRow($sql, DB_FETCHMODE_ASSOC);
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, array($sql,$row));
            // 入れ替える対象を取得
            if ($sort_type == "up") {
                $sql = "SELECT * FROM " .$this->table.
                        " WHERE faq_category = ".$row["faq_category"].
                        " AND faq_subcategory = ".$row["faq_subcategory"].
                        " AND faq_order <= ".$row["faq_order"].
                        " AND faq_lang = ".$this->lang.
                        " AND faq_status != 0".
                        " ORDER BY faq_order DESC" .
                        " LIMIT 0, 2";
            } else {
                $sql = "SELECT * FROM " .$this->table.
                        " WHERE faq_category = ".$row["faq_category"].
                        " AND faq_subcategory = ".$row["faq_subcategory"].
                        " AND faq_order >= ".$row["faq_order"].
                        " AND faq_lang = ".$this->lang.
                        " AND faq_status != 0".
                        " ORDER BY faq_order ASC" .
                        " LIMIT 0, 2";
            }
            $rs = $this->_conn->query($sql);
            if (DB::isError($rs)) {
                $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $this->_conn->last_query);
                return false;
            } else {
                if ($rs->numRows() !== 2) {
                    $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $this->_conn->last_query);
                    return false;
                } else {
                    // 変更元
                    $before = $rs->fetchRow(DB_FETCHMODE_ASSOC);
                    // 変更後
                    $after = $rs->fetchRow(DB_FETCHMODE_ASSOC);
                    // 入れ替え１
                    $data = array("faq_order" => $after["faq_order"]);
                    $where = "faq_key = ".$before["faq_key"];
                    $ret = $this->update($data, $where);
                    // 入れ替え２
                    $data = array("faq_order" => $before["faq_order"]);
                    $where = "faq_key = ".$after["faq_key"];
                    $ret = $this->update($data, $where);
                    return true;
                }
            }
        }
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
