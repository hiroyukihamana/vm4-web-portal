<?php

require_once("classes/N2MY_DBI.class.php");

class MeetingSuggestTable extends N2MY_DB {

    var $table = "meeting_suggest_log";
    var $logger = null;
    var $dsn;
    protected $primary_key = "suggest_key";

    public function __construct( $dsn )
    {
        $this->dsn = $dsn;
        $this->init($dsn, $this->table);
    }
    public function getSuggestByMeetingKey($meeting_key) {
        $suggest_data = $this->getRowsAssoc(sprintf( "meeting_key='%s'", $meeting_key));
        require_once("classes/core/dbi/Participant.dbi.php");
        $objParticipant = new DBI_Participant($this->dsn);
        foreach ($suggest_data as $key => $value) {
            $where = sprintf( "participant_key='%s'", $value["participant_key"]);
            $participant_info = $objParticipant->getRow($where);
            $this->logger2->info($participant_info);
            //$suggest_data[""];
        }
    }
}