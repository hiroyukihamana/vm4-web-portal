<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class RoomPlanTable extends N2MY_DB {

    var $table = 'room_plan';
    protected $primary_key = "room_plan_key";

    function RoomPlanTable($dsn) {
        $this->init($dsn, $this->table);
    }

    function getDetail($room_key) {
        $now_date = getdate();
        $now_date['mon']++;
        if ($now_date['mon'] > 10) {
            $now_date['mon'] = 1;
            $now_date['year']++;
        }
        if ($now_date['mon'] < 10) {
            $now_date['mon'] = (string) '0' . $now_date['mon'];
        }
        $nextmonth = $now_date['year'] . "-" . $now_date['mon'] . "-00 00:00:00";

        $where = "room_key = '".$room_key."'" .
                " AND room_plan_status = 1".
                " AND room_plan_starttime < '$nextmonth'";
        $row = $this->getRow($where);
        if (DB::isError($row)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$row->getUserInfo());
            return $row;
        }
        return $row;
    }

    public function deleteRoomPlanByRoomkey($room_key){
        $plan_where = sprintf("room_key = '%s' AND room_plan_status = 1",mysql_real_escape_string($room_key));
        $plan_data = array (
                "room_plan_status" => 0,
                "room_plan_updatetime" => date("Y-m-d H:i:s"),
        );
        $this->update($plan_data, $plan_where);
    }


}


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
