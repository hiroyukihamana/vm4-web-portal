<?php

require_once("classes/N2MY_DBI.class.php");

class MeetingFunctionCountLogTable extends N2MY_DB {

    var $table = "meeting_function_count_log";
    var $dsn;
    protected $primary_key = "meeting_function_count_key";

    public function __construct( $dsn )
    {
        $this->dsn = $dsn;
        $this->init($dsn, $this->table);
    }
}