<?php
require_once("classes/N2MY_DBI.class.php");

/**
 * Created on 2006/02/15
 * 
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class BillingTotalInfoTable extends N2MY_DB {

    var $table = 'billing_total_info';
    var $logger = null;
    protected $primary_key = "billing_total_info_key";
    
    /**
     * DB接続
     */
	function BillingTotalInfoTable($dsn) 
	{
        $this->init($dsn, $this->table);
        $this->logger = EZLogger::getInstance();
	}

}
