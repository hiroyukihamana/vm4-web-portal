<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

class SalesRoomQueue extends N2MY_DB {

    var $table = "sales_room_queue";
    var $logger = null;
    protected $primary_key = null;

    function __construct( $dsn ) {
        $this->init($dsn, $this->table );
    }

    public function addQmember($member_key) {
        $data = array(
                     "member_key"  => mysql_real_escape_string($member_key),
                     "error_count" => 0,
                     "pend"        => 0,
                     );
        $res = $this->add($data);
        return $res;
    }

    public function getQmember($member_key) {
        $where = ("member_key = '" . mysql_real_escape_string($member_key) . "'");
        $res = $this->get($where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }
    public function getQnum($num) {
        $res = $this->getNum($num,null);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function getQall() {
        $res = $this->getNum(0,"all");
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function deleteQmember($member_key) {
        $where = ("member_key = '" . mysql_real_escape_string($member_key) . "'");
        $res = $this->delete($where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function deleteQall() {
        $res = $this->deleteAll();
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function updateQerr($member_key) {
        $where = ("member_key = '" . mysql_real_escape_string($member_key) . "'");
        $res = $this->get($where);
        $counter = $res["error_count"] + 1;
        $data = array(
                     "error_count" => $counter,
                     );
        $res = $this->update($data,$where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function updateQpend($member_key) {
        $where = ("member_key = '" . mysql_real_escape_string($member_key) . "'");
        $data = array(
                     "pend" => 1,
                     );
        $res = $this->update($data,$where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    public function getQcount() {
        $where = "pend < 2";
        $res = $this->numRows($where);
        if( DB::isError($res) ) {
            return array(false,$res->message);
        }
        return $res;
    }

    function add($data) {
        $data["create_detatime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    function update($data, $where) {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }

    function get($where) {
        return $this->getRow($where,"member_key,error_count,pend");
    }

    function getNum($num,$kind) {
        $sort  = array("sales_room_queue_key" => "ASC");
        if($kind=="all") {
            $where = "pend < 2";
            $data  = $this->getRowsAssoc($where,$sort,null,null);
        } else {
            $where = "pend = 0";
            $limit = $num;
            $data  = $this->getRowsAssoc($where,$sort,$limit,null);
        }
        return $data;
    }

    function delete($where) {
        return parent::remove($where);
    }

    function deleteAll() {
        $where = "pend < 2";
        return parent::remove($where);
    }

}
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
