<?php
require_once("classes/N2MY_DBI.class.php");

class AnswersheetTable extends N2MY_DB {

    var $table = 'answersheet';
    protected $primary_key = "answersheet_key";

    function __construct($dsn) {
        $this->init($dsn, $this->table);
    }

    function add($data) {
        $data["created_datetime"] = date("Y-m-d H:i:s");
        $res = parent::add($data);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true));
        }
        //get answersheet_key
        $where = sprintf( "participant_key='%s' ", $data['participant_key'] );
        $sort = array( "answersheet_id" => "desc" );
        $result = $this->getRow( $where, "answersheet_id", $sort );
        return $result["answersheet_id"];
    }
}
