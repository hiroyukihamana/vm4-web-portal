<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

class MeetingTable extends N2MY_DB {

    var $table = "meeting";
    var $logger = null;
    protected $primary_key = null;

    function __construct( $dsn ) {
        $this->init($dsn, $this->table );
    }

    /**
     * ミーティングの内部キーをバージョンによってハッシュ化させたものを利用する
     */
    function getMeetingID($meeting_id, $field = '') {
        // 文字種によって、参照フィールドを切り替える
        if ((strlen($meeting_id) < 20) && is_numeric($meeting_id)) {
            $where = "meeting_key = '".addslashes($meeting_id)."'";
        } else {
            $where = "meeting_session_id = '".addslashes($meeting_id)."'";
        }
        // 会議キーとバージョン取得
        $row = $this->getRow($where, 'meeting_key,meeting_session_id,version');
        if (DB::isError($row)) {
            $this->logger2->error($row->getUserInfo());
            return false;
        } else {
            // フィールド指定あり
            if ($field) {
                return $row['meeting_key'];
            } else {
                // 旧バージョン
                if (strnatcasecmp($row['version'], '4.5.5.0') == -1) {
                    return $row['meeting_key'];
                // 4.5.5.0 以降
                } else {
                    return $row['meeting_session_id'];
                }
            }
        }
    }

    function add($data) {
        $data["meeting_registtime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    function update($data, $where) {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }

    /**
     * 最後に利用したmeeting_keyを保存する
     * roomテーブルとmeetingテーブルのリレーション関係が1対1になっている
     * どちらを利用しているか不明なため、一応両方更新させておく
     *
     * 将来的にはmeetingテーブルはroomに対して複数あるはず
     * fcsサーバが0で固定の理由もいまいちわからない
     */
    function last_meeting_update($room_key, $meeting_key) {
        $where = "room_key = '".addslashes($room_key)."'";
        $count = $this->numRows($where);
        // DBエラー
        if (DB::isError($count)) {
            $this->logger->error("DB Error!!", __FILE__, __LINE__, $count);
            return $count;
        } else {
            // 存在無し
            if ($count == 0) {
                // 追加
                $data = array(
                    "meeting_key" => $meeting_key,
                    "room_key" => $room_key,
                    "fcs_key" => 0
                    );
                $ret = $this->add($data);
            } else {
                // 更新
                $data = array(
                    "meeting_key" => $meeting_key,
                    "fcs_key" => 0
                    );
                $ret = $this->update($data, $where);
            }
            if (DB::isError($ret)) {
                $this->logger->error("DB Error!!", __FILE__, __LINE__, $ret);
                return $ret;
            }
        }
        return true;
    }
// }}}
    /**
     * MeetingKey生成
     *
     * @param string room_key 会議室ID
     * @return string 会議ID
     */
// {{{ -- get_meeting_key()
    function create_meeting_key($room_key) {
        return $room_key."_".md5(uniqid(rand(), true));
    }
// }}}
    function findActiveMeetingByMeetingKey($meeting_key)
    {
        $where = "meeting_key = '".addslashes($meeting_key)."'".
				 " AND is_active = 1";
        $res   = $this->getRow($where);
        return @$res;
    }
    function findMeetingByMeetingKey($meeting_key , $columns = null)
    {
        $where = "meeting_key = '".addslashes($meeting_key)."'";
        $res   = $this->getRow($where , $columns);
        return @$res;
    }
    function findMeetingKeyByMeetingTicket($meeting_ticket)
    {
        $where = "meeting_ticket = '".addslashes($meeting_ticket)."'";
        $res   = $this->getRow($where, "meeting_key", array(), 1);
        return @$res['meeting_key'];
    }
    function findByMeetingTicket($meeting_ticket)
    {
        $where = "meeting_ticket = '".addslashes($meeting_ticket)."'";
        $res   = $this->getRow($where, "*", array(), 1);
        return @$res;
    }
    function findByMeetingSessionId($meeting_session_id)
    {
        $where = "meeting_session_id = '".addslashes($meeting_session_id)."'";
        $res   = $this->getRow($where, "*", array(), 1);
        return @$res;
    }
    public function findByConfId($conf_id)
    {
        if (!$conf_id) {
            throw new Exception("conf_id: $conf_id is empty ");
        }

        $where  = "ives_conference_id = '".mysql_real_escape_string($conf_id)."'"
                 ." AND is_active = 1 AND is_deleted = 0";

        $db_res = $this->select($where , array('update_datetime' => 'desc'));

        if ($db_res === false || PEAR::isError($db_res)) {
            throw new Exception(__FILE__.' '.__LINE__." where : $where");
        }

        if (!$db_res) {
            return array();
        }
        $res = array();
        while ($row  =$db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }

        return array_shift($res);
    }
    public function findByDid($did)
    {
        if (!$did) {
            throw new Exception("ives_did: $did is empty ");
        }

        $where  = "ives_did = '".mysql_real_escape_string($did)."'"
                 ." AND is_active = 1 AND is_deleted = 0";

        $db_res = $this->select($where , array('update_datetime' => 'desc'));

        if ($db_res === false || PEAR::isError($db_res)) {
            throw new Exception(__FILE__.' '.__LINE__." where : $where");
        }

        if (!$db_res) {
            return array();
        }
        $res = array();
        while ($row  =$db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }

        return array_shift($res);
    }
    public function getDownloadInfoByMeetingKeyAndUserKey($meeting_key,$user_key,$needAllInfo)
    {
        $where = "meeting_key = ".addslashes($meeting_key)." AND user_key = ".addslashes($user_key)."";
        if ($needAllInfo) {
            $projection = "*";
        }else{
             $projection = "fms_path as fms_path,meeting_session_id as meeting_session_id,server_key as server_key ";
        }
        $res   = $this->getRow($where, $projection, array(), 1);
        return @$res;
    }

    function deleteActiveMeetingByRoomkey($room_key){
        $where = sprintf("is_active = 1 AND room_key = '%s'",mysql_real_escape_string($room_key));
        $data = array("is_active" => 0);

        $this->update($data, $where);

    }
    function rowNowMeetingInfo($meeting_key){
        $where = "is_active = 1 AND meeting_ticket = '".$meeting_key."'";
        return $this->getRow($where);
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
