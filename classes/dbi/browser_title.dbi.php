<?php
require_once ("classes/N2MY_DBI.class.php");
require_once('lib/EZLib/EZCore/EZConfig.class.php');

class BrowserTitleTable extends N2MY_DB {

    var $table = 'browser_title';
    var $config =  null;

    public function BrowserTitleTable($dsn) {
        $this -> init($dsn, $this -> table);
        $this->config = EZConfig::getInstance(N2MY_CONFIG_FILE);
    }

    public function setTitle($userKey, $title) {
        if(!$userKey){
            $this -> logger -> warn(__FUNCTION__, __FILE__, __LINE__, $userKey);
            return false;
        }
        $condition = 'user_key = ' . $userKey;
        $userTitle = $this -> getRow($condition);
        if (!empty($title)) {
            if (!$userTitle) {
                $this -> add(array(
                    'user_key' => $userKey,
                    'status' => 1,
                    'title' => $title,
                    'create_datetime' => date('Y-m-d H:i:s'))
                );
            } else {
                $this -> update(array(
                    'title' => $title,
                    'status' => 1,
                    'update_datetime' => date('Y-m-d H:i:s')), $condition);
            }
        } else {
            if ($userTitle) {
                $this -> update(array(
                    'status' => 0,
                    'update_datetime' => date('Y-m-d H:i:s')), $condition);
            }
        }
        return $this -> getRow($condition);
    }

    public function getTitle($user_key){
        if($this->config->get("IGNORE_MENU" , "sales_custom_logo" , false )){
            return false;
        }
        return $this->getOne('status = 1 AND user_key = ' . $user_key, 'title');
    }

}
