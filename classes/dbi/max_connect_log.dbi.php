<?php
require_once("classes/N2MY_DBI.class.php");

class MaxConnectLog extends N2MY_DB
{

    var $table = 'max_connect_log';
    protected $primary_key = "max_connect_log_key";

    function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }

    function add( $data ) {
        $data["create_datetime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }
}

