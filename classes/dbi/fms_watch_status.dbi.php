<?php
require_once("classes/N2MY_DBI.class.php");

class FmsWatchTable extends N2MY_DB {

    var $table = 'fms_watch_status';
    protected $primary_key = "watch_status_key";

    function FmsWatchTable($dsn)
    {
        $this->logger2 =& EZLogger2::getInstance();
        $this->init($dsn, $this->table);
    }
    
    public function add($data){
    	$this->_log($data);
    	return parent::add($data);
    }
    
    public function update($data, $where){
    	$this->_log($data, $where);
    	return parent::update($data, $where);
    }
    
    // [MTGVFOUR-1151] 会議記録が作成中のまま完了にならない
    // 調査用ログ取り拡張
    private function _log($data, $where = null){
    	$bt = debug_backtrace();
    	foreach ($bt as $i => $trace){
    		$func = $bt[$i + 1]['function'];
    		if ( $func == '_execAction' ) break;
    		$node = &$backtrace[$i];
    		$node['func'] = $func;
    		$node['file'] = $trace['file'];
    		//$node['line'] = $trace['line'];
    		//$node['args'] = $trace["args"];
    	}
    	
    	$info['time'] = json_encode( microtime( true ) );
    	$info['data'] = json_encode( $data );
    	$info['where'] = json_encode( $where );
    	$info['request'] = json_encode( $_REQUEST );
    	//$info['session'] = json_encode( $_SESSION );
    	$info['backtrace'] = json_encode( $backtrace );
    	//$info['server'] = json_encode( $_SERVER );
    	$this->logger2->info( $info, __CLASS__ );
    }
}
