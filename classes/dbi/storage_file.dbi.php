<?php

require_once("classes/N2MY_DBI.class.php");

class StorageTable extends N2MY_DB {

    var $table = "storage_file";
    var $logger = null;
    protected $primary_key = "storage_id";

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }

    function WhiteboardStorageTable( $dsn )
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }

    function get_wb_meetingKey( $meeting_key )
    {
        $wb_meetigKey = array();
        $where = sprintf( "meeting_key='%s'", $meeting_key);
        $wb_mKey = $this->getRowsAssoc( $where );
        foreach( $wb_mKey as $m_key ){
            $wb_meetigKey[] = $m_key["meeting_key"];
        }
        return $wb_meetigKey;
    }

    public function add( $data )
    {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    public function update( $data, $where )
    {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }

    function getList($where = "", $sort = array(), $limit = null, $offset = 0, $columns = "*") {
        return $this->getRowsAssoc($where, $sort, $limit, $offset, $columns);
    }

}