<?php

require_once("classes/N2MY_DBI.class.php");

class PwResetTable extends N2MY_DB {

    var $table = "pw_reset";
    protected $primary_key = "pw_reset_id";

    function __construct($dsn) {
        $this->init( $dsn, $this->table );
        $this->logger = EZLogger::getInstance();
    }

    public function add($data) {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        return parent::add( $data );
    }

    public function getAuthorizeInfo($authorize_key) {
        $where = sprintf("authorize_key='%s'", $authorize_key);
        $authorize_info_list = $this->getRow($where);
        return $authorize_info_list;
    }

    public function getIgnoreInfo($ignore_key) {
        $where = sprintf("ignore_key='%s'", $ignore_key);
        $ignore_info_list = $this->getRow($where);
        return $ignore_info_list;
    }

}

