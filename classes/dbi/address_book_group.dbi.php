<?php
require_once("classes/N2MY_DBI.class.php");

class AddressBookGroupTable extends N2MY_DB {

    var $table = 'address_book_group';
    protected $primary_key = "address_book_group_key";
    var $rules = array(
        "group_name" => array(
            "required" => true,
            "maxlen" => 255,
            ),
    );


    /**
     * DB接続
     */
    function AddressBookGroupTable($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }

    function add($data) {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $data["update_datetime"] = date("Y-m-d H:i:s");
        $this->logger2->debug($data);
        return parent::add($data);
    }

    function update($data, $where) {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        $this->logger2->debug($data);
        return parent::update($data, $where);
    }

}
