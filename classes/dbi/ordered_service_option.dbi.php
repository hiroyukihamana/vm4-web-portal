<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:kiyomizu                                                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once("classes/N2MY_DBI.class.php");

class OrderedServiceOptionTable extends N2MY_DB {

    var $table = 'ordered_service_option';
    protected $primary_key = "ordered_service_option_key";

    function OrderedServiceOptionTable($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }


    function getServiceInfo($room_key)
    {
        $where = "room_key = '".addslashes($room_key)."'" .
            " AND ordered_service_option_status = '1'";
        $list = $this->getCol($where, "service_option_key");
        if (DB::isError($list)) {
            $this->logger->error($list->getMessage());
        } else {
            return $list;
        }
    }
    function enableOnRoom($service_option_key, $room_key)
    {
        if (!$service_option_key || !$room_key) {
            throw new Exception("parameter required");
        }

        $where = "room_key = '".mysql_real_escape_string($room_key)."'" .
                 " AND ordered_service_option_status = '1'".
                 " AND service_option_key = '".mysql_real_escape_string($service_option_key)."'";

        $list = $this->getRow($where, $columns = "*", $sort = array(), $limit = 1);
        if (DB::isError($list)) {
            $this->logger->error($list->getMessage());
            throw new Exception('enableInRoom is failed');
        }
        return $list ? true : false;
    }

    public function deleteRoomOptionByRoomkey($room_key){
        $option_where =sprintf("room_key='%s' AND ordered_service_option_status = 1",mysql_real_escape_string($room_key));
        $option_data = array (
                "ordered_service_option_status" => 0,
                "ordered_service_option_deletetime" => date("Y-m-d H:i:s"),
        );
        $this->update( $option_data, $option_where );
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
