<?php
require_once("classes/N2MY_DBI.class.php");

class MemberRoomRelationTable extends N2MY_DB
{

    var $table = 'member_room_relation';
    protected $primary_key = "member_room_relation_id";

    function __construct( $dsn ) {
        $this->init($dsn, $this->table );
    }

    function getRoomRelation($member_key) {
        $where = "member_key = ".$member_key;
        $rs = $this->getRow($where);
        return $rs;
    }
}

