<?php
require_once("classes/N2MY_DBI.class.php");

class OnetimeUrlTable extends N2MY_DB
{

    var $table = 'onetime_url';
    protected $primary_key = "onetime_url_key";

    function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }

    function add( $data ) {
        $data["create_datetime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }
}

