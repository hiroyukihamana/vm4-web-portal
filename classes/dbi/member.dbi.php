<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class MemberTable extends N2MY_DB {

    var $table = 'member';
    protected $primary_key = "member_key";
    var $dsn = "";

    function __construct($dsn) {
        $this->init( $dsn, $this->table );
        $this->logger = EZLogger::getInstance();
        $this->dsn = $dsn;
    }

    /**
     * ログイン認証を行い、存在すればユーザ情報を返す
     */
    function checkLogin($member_id, $member_pw, $enc_type = null, $api_key = null) {
        $where = " member_id = '".addslashes($member_id)."'".
//            " AND member_pass = '".addslashes($member_pw)."'".
            " AND member_status = 0";
        $member_info = $this->getRow($where);
        if (DB::isError($member_info)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$member_info->getUserInfo());
            return false;
        }
        if ($member_info) {
            require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
            $member_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_info["member_pass"]);
            if ($api_key) {
                require_once("classes/mgm/dbi/api_auth.dbi.php");
                $objApiAuth = new ApiAuthTable(N2MY_MDB_DSN);
                $where = "api_key = '".$api_key."'" .
                        " AND status = 1";
                $secret = $objApiAuth->getOne($where, 'secret');
                if (DB::isError($secret)) {
                    $this->logger2->error($secret->getUserInfo());
                    return false;
                } else {
                    if ($secret) {
                        $db_password = hash('sha256', ($member_id.hash('sha256', $member_password).$secret));
                    } else {
                        $this->logger2->warn('不正なAPI_KEYが指定されました。');
                        return false;
                    }
                }
            } else {
                switch ($enc_type) {
                    case "md5":
                        $db_password = md5($member_password);
                        break;
                    case "sha1":
                        $db_password = sha1($member_password);
                        break;
                    default :
                        $db_password = $member_password;
                        break;
                }
            }
            if (($member_info["member_id"] != $member_id) || ($db_password != $member_pw)) {
                $this->logger->warn(__FUNCTION__."#ID、PW入力エラー", __FILE__, __LINE__, array(
                    $member_id,
                    $member_info["member_id"],
                    $member_pw,
                    $db_password,
                ));
                return null;
            } else {
                return $member_info;
            }
        } else {
            return null;
        }
    }

    /**
     * メンバー課金 招待メールからのログイン認証
     * @param string $login_id ログインID
     * @param string $login_pw ログインパスワード
     * @param string $user_key ユーザーkey
     * @param boolean $pw_encode パスワードの暗号化（MD5,sha1ハッシュ）
     * @param api_key $api_key
     * @return mixed 成功時:メンバー情報、エラー時:null
     */
    function checkMemberLogin($login_id, $login_pw, $user_key, $enc_type, $api_key, $external_member_invitation_flg){
      $where = " member_id = '".addslashes($login_id)."'".
          " AND member_status = 0";
      //メンバー課金で外部IDを招待する場合は、メンバーのみで認証
      if (!$external_member_invitation_flg) {
          $where .= " AND user_key = ".addslashes($user_key);
      }
      $member_info = $this->getRow($where);
      if (DB::isError($member_info)) {
        $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$member_info->getUserInfo());
        return false;
      }
      if ($member_info) {
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $member_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_info["member_pass"]);
        if ($api_key) {
          require_once("classes/mgm/dbi/api_auth.dbi.php");
          $objApiAuth = new ApiAuthTable(N2MY_MDB_DSN);
          $where = "api_key = '".$api_key."'" .
              " AND status = 1";
          $secret = $objApiAuth->getOne($where, 'secret');
          if (DB::isError($secret)) {
            $this->logger2->error($secret->getUserInfo());
            return null;
          } else {
            if ($secret) {
              $db_password = hash('sha256', ($login_id.hash('sha256', $member_password).$secret));
            } else {
              $this->logger2->warn('不正なAPI_KEYが指定されました。');
              return null;
            }
          }
        } else {
          switch ($enc_type) {
            case "md5":
              $db_password = md5($member_password);
              break;
            case "sha1":
              $db_password = sha1($member_password);
              break;
            default :
              $db_password = $member_password;
              break;
          }
        }
        if (($member_info["member_id"] != $login_id) || ($db_password != $login_pw)) {
          $this->logger->warn(__FUNCTION__."#ID、PW入力エラー", __FILE__, __LINE__, array(
              $login_id,
              $member_info["member_id"],
              $login_pw,
              $db_password,
          ));
          return null;
        } else {
          return $member_info;
        }
      } else {
        return null;
      }
    }

    /**
     *
     */
    function getDetail($member_key) {
        $where = "member_key = ".$member_key;
        $rs = $this->select($where, null, null, 1, null);
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
            return $rs;
        }
        $member_row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
        return $member_row;
    }
    
    function getDetailByMemerID($member_id) {
    	$where = "member_id = '".mysql_real_escape_string($member_id)."'";
    	$rs = $this->select($where, null, null, 1, null);
    	if (DB::isError($rs)) {
    		$this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
    		return $rs;
    	}
    	$member_row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
    	return $member_row;
    }
    
    function getDetailByRoomKey($room_key) {
    	$where = "room_key = '".$room_key."'";
    	require_once("classes/dbi/member_room_relation.dbi.php");
    	$objMemberRoom = new MemberRoomRelationTable($this->dsn);
    	$member_key = $objMemberRoom->getOne($where, "member_key");
    	$where = "member_key = '".$member_key."'";
    	$rs = $this->select($where, null, null, 1, null);
    	if (DB::isError($rs)) {
    		$this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
    		return $rs;
    	}
    	$member_row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
    	return $member_row;
    }

    /**
     *
     */
    function getMemberList ($user_key) {
        $where = "user_key = ".$user_key.
            " AND member_status >= 0";
        $rs = $this->select($where);
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
            return $rs;
        }
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * その時点で有効なメンバー一覧を取得
     * 条件：
     * 　請求年月月初より前に開始されている　かつ
     * 　（ステータスが有効　もしくは
     * 　　更新日が請求年月月初以降）
     * のもの
     * （削除日が必要だと思う）
     *
     * 追記：請求管理で必要が無くなったので、他で使ってなければ削除します。
     *
     */
    function getEffectiveMemberCountList($billing_date = null)
    {
        $where = " `create_datetime` < '". $billing_date ."' " .
                 " AND (`member_status` = 1 OR " .
                 "      `update_datetime` >= '". $billing_date ."') ".
                 " AND `member_type` != 'terminal'";
        $sort = array(
                    "user_key" => "ASC",
                    "member_key" => "ASC",
                );
        $coulumns = " `user_key`, `member_key` ";
        $rows = $this->getRowsAssoc($where, $sort, null, null, $coulumns, null);
        foreach ($rows as $_key => $_val) {
            $_rows[$_val['user_key']] += 1;
        }
        return $_rows;
    }

    public function add( $data )
    {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    public function update( $data, $where )
    {
        $where = str_replace( '!', '\!', $where);
        $where = str_replace( '&', '\&', $where);
        $where = str_replace( '?', '\?', $where); 
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }

    public function changePasswd($member_key, $new_password) {
        if (!$member_key) {
            return false;
        }
        //ユーザーパスワードをチェック
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $new_password)) {
            return false;
        } elseif (!preg_match('/[[:alpha:]]+/',$new_password) || preg_match('/^[[:alpha:]]+$/',$new_password)) {
            return false;
        }
        require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
        $data = array("member_pass" => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $new_password));
        $where = "member_key = ".$member_key;
        return $this->update($data, $where);
    }

    public function default_check($data) {
        $rules = array(
            "member_name" => array(
                "required" => true,
                "maxlen" => 50,
                ),
            "member_name_kana" => array(
                "maxlen" => 50,
                ),
            "member_id" => array(
                "required" => true,
                "minlen" => 3,
                "maxlen" => 255,
                "regex" => array(
                    '/^[[:alnum:]@+._-]{3,255}$/',
                    ),
                ),
            "member_pass" => array(
                "required" => true,
                "minlen" => 8,
                "maxlen" => 128,
                "regex" => array(
                    '/[[:alpha:]]+/',
                    ),
                ),
            "member_email" => array(
//                "required" => true,
                "email" => true,
                ),
            "member_status" => array(
                "required" => true,
                ),
            "timezone" => array(
                "numeric" => true,
                ),
            "lang" => array(
                "alphabet" => true,
                "minlen" => 2,
                "maxlen" => 3,
                ),
        );
        $error_obj = $this->check($data, $rules);
        //ユーザーパスワードをチェック
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $data['member_pass'])) {
            $error_obj->set_error("member_pass", "password",$data['member_pass']);
        } elseif (!preg_match('/[[:alpha:]]+/',$data['member_pass']) || preg_match('/^[[:alpha:]]+$/',$data['member_pass'])) {
            $error_obj->set_error("member_pass", "password",$data['member_pass']);
        }
        return $error_obj;
    }

    function get_outbound_max_sort($user_key) {

        //outbound_numberキーからソート番号取得
        $where = " user_key = '".addslashes($user_key)."'".
                 " AND (outbound_id != '' OR outbound_id != NULL)";
        $member_count = $this->numRows($where);
        $this->logger->info("max_sort",__FILE__,__LINE__,$member_count);
        if($member_count > 0){
            $query = "SELECT max(outbound_number_id) FROM member WHERE user_key = ". $user_key;
            $ret = $this->_conn->getOne($query);
            $this->logger->info("max_sort",__FILE__,__LINE__,$ret);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $query);
                return $ret;
            }
            return $ret;
        }else{
            return 0;
        }
    }
    
    public function ss_watcher_update( $data, $user_key, $member_key )
    {
        // one アカウントのメンバー変更時
        if (!$user_key || !$member_key) {
            return false;
        }
        if($data == 0 || $data == 1){
            $data = array("use_ss_watcher" => $data);
            $where = "user_key='".$user_key. "' AND member_key='".$member_key."'" ;
            return $this->update($data, $where);
        }
    }
}

/*
 * Local variables: 
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
