<?php
require_once 'classes/N2MY_DBI.class.php';

class ReservationPush extends N2MY_DB {
	
	var $table = 'reservation_push';
	protected $primary_key = 'reservation_push_key';
	
	const MODE_ADD = 1;
	const MODE_UPDATE = 2;
	const MODE_DELETE = 3;
	
	public function __construct( $dsn ) {
		$this->init( $dsn, $this->table );
	}
	
	public function add( $mode, $reservation_key, $vcube_id, $auth_tokem, $contract_id ) {
		// validate
		switch ( $mode ) {
			case self::MODE_ADD :
			case self::MODE_UPDATE :
			case self::MODE_DELETE :
				break;
			default :
				$this->logger2->warn( $function_mode, '$function_mode is not allow' );
				return false;
				break;
		}
		
		return parent::add( array(
				'reservation_key' => $reservation_key,
				'vcube_id' => $vcube_id,
				'auth_token' => $auth_tokem,
				'contract_id' => $contract_id,
				'function' => $mode,
				'error_count' => 0,
				'is_pend' => 0,
				'update_datetime' => date( 'Y-m-d H:i:s' ),
				'create_datetime' => date( 'Y-m-d H:i:s' ) 
		) );
	}
	
	public function getRowsAssoc( $where = null ){
		return parent::getRowsAssoc( $where, array('reservation_push_key' => 'ASC') );
	}
	
	public function getList( $params = array() ) {
		
		// fix error_count, is_pend
		$reservation_list = array();
		foreach( $this->getRowsAssoc() as $row ) {
			$struct = &$reservation_list[$row['reservation_key']];
			$struct['rows'][] = $row;
			// error_countの最大値、pendフラグが立っているか、を記憶する
			$struct['max_error_count'] = max( $struct['max_error_count'], $row['error_count'] );
			$struct['is_pend'] = $struct['is_pend'] || $row['is_pend'];
		}
		unset( $struct );
		foreach( $reservation_list as $reservation_key => $struct ) {
			$max_error_count = $struct['max_error_count'];
			$is_pend = $struct['is_pend'];
			foreach( $struct['rows'] as $row ) {
				// error_count、pendフラグの差異を丸める
				if ( $row['error_count'] != $max_error_count || $row['is_pend'] != $is_pend ) {
					parent::update( array(
							'error_count' => $max_error_count,
							'is_pend' => $is_pend,
							'update_datetime' => date( 'Y-m-d H:i:s' ) 
					), sprintf( 'reservation_key=%d', $reservation_key ) );
					break;
				}
			}
		}
		
		$where = false;
		switch ( true ) {
			case !$params : // pend以外
				$where = 'is_pend=0';
				break;
			case isset( $params['ALL'] ) : // すべて
				$where = 1;
				break;
			case $params['push_key'] : // 指定レコード
				$where = sprintf( 'reservation_push_key=%d', $params['push_key'] );
				break;
			case $params['reservation_key'] : // 指定レコード
				$where = sprintf('reservation_key=%d', $params['reservation_key']);
				break;
			default : // フィルタ
				$where = array();
				$now = date( 'Y-m-d H:i:s' );
				foreach( $params as $error_count => $minute ) {
					$datetime = date( 'Y-m-d H:i:s', strtotime( sprintf( '%s -%d minute', $now, $minute ) ) );
					$where[] = sprintf( 'error_count=%d AND update_datetime < "%s"', $error_count, $datetime );
				}
				$where = sprintf( '(%s)', join( ')OR(', $where ) );
				$where = sprintf( 'is_pend=0 AND (%s)', $where );
				break;
		}
		$this->logger2->debug( $where );
		return $this->getRowsAssoc( $where );
	}
	
	public function remove( $reservation_push_key ) {
		return parent::remove( sprintf( 'reservation_push_key=%d', $reservation_push_key ) );
	}
	
	public function checkErrorCount( $max_push_error_count ) {
		
		$list = parent::getRowsAssoc( sprintf( 'is_pend=0' ) );
		$now = date( 'Y-m-d H:i:s' );
		
		$pend_list = array();
		foreach( $list as &$data ) {
			$data['update_datetime'] = $now;
			$data['error_count']++;
			
			// 規定値に達したクエリはペンド
			if($max_push_error_count <= $data['error_count']){
				$data['is_pend'] = 1;
				$pend_list[] = $data;
			}
			
			parent::update( $data, sprintf( 'reservation_push_key=%d', $data['reservation_push_key'] ) );
		}
		
		// 新たにペンドされたデータのみ返す
		return $pend_list;
	}

}
