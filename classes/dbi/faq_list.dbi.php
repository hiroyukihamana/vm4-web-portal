<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");
class FaqListTable extends N2MY_DB {

    var $lang = "";

    function __construct($dsn, $lang = "ja_JP") {
        $this->rules = array(

            "faq_key"     => array(
                "required" => true),
            "faq_category"     => array(
                "required" => true),
            "faq_subcategory"     => array(
                "required" => true),
            "faq_question"     => array(
                "required" => true),
            "faq_answer"    => array(
                "required"  => true),
            "faq_status" => array(
                "required" => true,)
        );
        // 言語
        $this->lang = $lang;
        $this->primary_key = "faq_key";
        $this->init($dsn, "faq_list");
    }
    /**
     * データ取得
     */
    function getList( $model = null ){
        $type = ( $model == "member" ) ? "( faq_type = 1 OR faq_type = 3 )" : "( faq_type = 1 OR faq_type = 2)";
        $sql = "SELECT * FROM ".$this->table .", faq_data" .
            " WHERE faq_list.faq_key = faq_data.faq_key" .
            " AND faq_data.lang = '".$this->lang."'" .
            " AND faq_status = '1'" .
            " AND faq_lang = '0'" .
            " AND ". $type .
            " ORDER BY faq_category asc,faq_subcategory asc,faq_order asc";
        $ret = $this->_conn->query($sql);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $ret->getUserInfo());
            return $ret;
        }
        return $ret;
    }

    /**
     * 有効なデータ取得
     */
    function getAllList(){
        $where = " faq_status > 0" .
            " AND faq_lang = '0'";
        $sort = array(
            "faq_category"      => "asc",
            "faq_subcategory"   => "asc",
            "faq_order"         => "asc"
            );
        $rows = $this->getRowsAssoc($where, $sort);
        if (DB::isError($rows)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $rows->getUserInfo());
            return array();
        }
        return $rows;
    }

    /**
     * 追加／更新
     */
    function complete($news_info, $id = ''){
        if ($news_info) {
            if ($id) {
                $data = array(
                    "faq_category"      => $news_info["category"],
                    "faq_subcategory"   => $news_info["subcategory"],
                    "faq_question"      => $news_info["question"],
                    "faq_answer"        => $news_info["answer"],
                    "faq_type"          => $news_info["type"],
                );
                $where = "faq_key = ".$id;
                $ret = $this->update($data, $where);
            } else {
                $query = "SELECT MAX(faq_order) FROM ".$this->table.
                    " WHERE faq_category = ".$news_info["category"].
                    " AND faq_subcategory = ".$news_info["subcategory"].
                    " AND faq_lang = '0'";
                $this->logger2->info($query);
                $order = $this->_conn->getOne($query);
                $order++;
                $data = array(
                    "faq_category"      => $news_info["category"],
                    "faq_subcategory"   => $news_info["subcategory"],
                    "faq_order"         => $order,
                    "faq_question"      => $news_info["question"],
                    "faq_answer"        => $news_info["answer"],
                    "faq_status"        => 1,
                    "faq_lang"          => '0',
                    "faq_type"          => $news_info["type"]
                );
                $this->logger2->info($data);
                $ret = $this->add($data);
            }
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $this->_conn->last_query);
                return $ret;
            }
            return $ret;
        }
    }

    function setStatus($status, $id) {
        if ($id) {
            $data = array("faq_status" => $status);
            $where = "faq_key = ".$id;
            $ret = $this->update($data, $where);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $this->_conn->last_query);
                return $ret;
            }
            return $ret;
        }
    }

    /**
     * sort
     */
    function sort($id, $sort_type) {
        if ($id) {
            $sql = "SELECT faq_category" .
                    ", faq_subcategory" .
                    ", faq_order" .
                    " FROM ".$this->table.
                    " WHERE faq_key = ".addslashes($id);
            $row = $this->_conn->getRow($sql, DB_FETCHMODE_ASSOC);
            // 入れ替える対象を取得
            if ($sort_type == "up") {
                $sql = "SELECT * FROM " .$this->table.
                        " WHERE faq_category = ".$row["faq_category"].
                        " AND faq_subcategory = ".$row["faq_subcategory"].
                        " AND faq_order <= ".$row["faq_order"].
                        " AND faq_lang = '0'".
                        " AND faq_status != 0".
                        " ORDER BY faq_order DESC" .
                        " LIMIT 0, 2";
            } else {
                $sql = "SELECT * FROM " .$this->table.
                        " WHERE faq_category = ".$row["faq_category"].
                        " AND faq_subcategory = ".$row["faq_subcategory"].
                        " AND faq_order >= ".$row["faq_order"].
                        " AND faq_lang = '0'".
                        " AND faq_status != 0".
                        " ORDER BY faq_order ASC" .
                        " LIMIT 0, 2";
            }
            $rs = $this->_conn->query($sql);
            if (DB::isError($rs)) {
                $this->logger2->error($rs->getUserInfo());
                return false;
            } else {
                if ($rs->numRows() !== 2) {
                    $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $this->_conn->last_query);
                    return false;
                } else {
                    // 変更元
                    $before = $rs->fetchRow(DB_FETCHMODE_ASSOC);
                    // 変更後
                    $after = $rs->fetchRow(DB_FETCHMODE_ASSOC);
                    // 入れ替え１
                    $data = array("faq_order" => $after["faq_order"]);
                    $where = "faq_key = ".$before["faq_key"];
                    $ret = $this->update($data, $where);
                    // 入れ替え２
                    $data = array("faq_order" => $before["faq_order"]);
                    $where = "faq_key = ".$after["faq_key"];
                    $ret = $this->update($data, $where);
                    return true;
                }
            }
        }
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
