<?php

require_once("lib/EZLib/EZCore/EZLogger2.class.php" );
require_once("classes/mgm/dbi/meeting_date_log.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");

class N2MY_Eco
{
    var $lineColor    = array( "R" => 255, "G" => 255, "B" => 255 );
    var $gridColor    = array( "R" => 220, "G" => 220, "B" => 220 );
    var $areaColor    = array( "R" => 252, "G" => 252, "B" => 252 );
    var $scaleColor    = array( "R" => 3, "G" => 0, "B" => 0 );
    var $plotColor    = array( "R" => 255, "G" => 255, "B" => 255 );
    var $unitColor    = array( "R" => 50, "G" => 50, "B" => 50 );
    var $data = array();
    var $unit;
    var $max;
    var $division;
    var $dsn = "";

    public function __construct( $dsn, $mdb_dsn )
    {
        $this->dsn = $dsn;
        $this->logger = & EZLogger2::getInstance();
        $this->objMeeting = new MeetingTable($dsn);
        $this->objUser = new UserTable($dsn);
        $this->objMeetingDateLog = new MeetingDateLogTable($mdb_dsn);
        $this->objParticipant = new DBI_Participant($dsn);
    }

    /*
     * グラフ線の色指定
     */
    public function setLineColor( $R = 188, $G = 244, $B = 46 )
    {
        $this->lineColor = array( "R"=> $R, "G" => $G, "B" => $B );
    }

    public function setGridColor( $R = 220, $G = 220, $B = 220 )
    {
        $this->gridColor = array( "R"=> $R, "G" => $G, "B" => $B );
    }

    public function setAreaColor( $R = 252, $G = 252, $B = 252 )
    {
        $this->areaColor = array( "R"=> $R, "G" => $G, "B" => $B );
    }

    public function setScaleColor( $R = 3, $G = 0, $B = 0 )
    {
        $this->scaleColor = array( "R"=> $R, "G" => $G, "B" => $B );
    }

    public function setPlotColor(  $R = 188, $G = 244, $B = 46 )
    {
        $this->plotColor = array( "R"=> $R, "G" => $G, "B" => $B );
    }

    public function setUnitColor(  $R = 156, $G = 156, $B = 156 )
    {
        $this->unitColor = array( "R"=> $R, "G" => $G, "B" => $B );
    }

    /*
     * データセット
     */
    public function setData( $x, $y = array() )
    {
        $data = array();
        for( $i = 0; $i < count( $x ); $i++ ){
            $this->data[] = array( $x[$i] => null != $y[$i] ? $y[$i] : 0 );
        }
    }

    /**
     * 単位設定
     */
    public function setUnit( $unit )
    {
        $this->unit = $unit;
    }

    public function setYValue( $max, $division = 0 )
    {
        $this->max = $max;
        $this->division = $division;
    }

    public function flushImage()
    {
        include( "classes/chart/Chart.class.php" );
        include( "classes/chart/Chart.data.class.php" );

         // Dataset definition
        $obj_ChartData = new pData();

        $obj_ChartData->AddPoint( $this->data );
        $obj_ChartData->AddSerie();
        $obj_ChartData->SetSerieName("Sample data","Serie1");

        // Initialise the graph
        $obj_ChartClass = new pChart( 245, 130 );

        //y座標のmin, max
//        $max = 0;
//        foreach( $data as $key => $value){
//            list( $horizontalData, $verticalData ) = each( $value );
//            $max = max( $max, $verticalData );
//        }
//        $place = 1;
//        for( $i = 0; $i < strlen( $max ) - 1; $i++ ){
//            $place .= 0;
//        }
//        $max += $place;
//        //y座標の数
//        $division = substr( $max, 0, 1 );
//        if( $division == 1 )
//            $max += $max / 10;

        $obj_ChartClass->setYValue( $this->max, 0 );
        $obj_ChartClass->setFontProperties( "font/hiragino.otf", 8 );
        $obj_ChartClass->setGraphArea( 40, 10, 230, 107 );

        //areaColor
        $obj_ChartClass->drawGraphArea( $this->areaColor );

        //scaleColor
        $obj_ChartClass->drawScale(
                                $obj_ChartData->GetData(),
                                $obj_ChartData->GetDataDescription(),
                                $this->division,
                                $this->scaleColor,
                                TRUE,
                                0, 2 );

        //gridColor
        $obj_ChartClass->drawGrid( 4, FALSE, $this->gridColor );

        // Draw the line graph
        $obj_ChartClass->drawLineGraph(
                                $obj_ChartData->GetData(),
                                $obj_ChartData->GetDataDescription(),
                                $this->lineColor );

//        $obj_ChartClass->drawPlotGraph(
//                                $obj_ChartData->GetData(),
//                                $obj_ChartData->GetDataDescription(),
//                                3,
//                                1,
//                                $this->plotColor );

        if( $this->unit ){
            $obj_ChartClass->setFontProperties( "font/hiragino.otf", 8 );
            $obj_ChartClass->setUnit( 0, 138, $this->unit , $this->unitColor );
        }

        // Finish the graph
        $gif = $obj_ChartClass->Render();

        header("Content-type:image/gif");
        imagegif( $gif );
        exit;
    }

    public function getMeetingLog( $meeting_ticket )
    {
        $sql = sprintf( "
                SELECT
                    *
                FROM
                    meeting
                WHERE
                    meeting_ticket = '%s'
                    ", mysql_real_escape_string($meeting_ticket) );
        $rs = $this->objMeeting->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getMessage());
            return false;
        } else {
            $info["meeting"] = $rs->fetchRow(DB_FETCHMODE_ASSOC);
        }
        if (!$info["meeting"]) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,"not meeting info");
            return false;
        }
        $sql = sprintf( "
                SELECT
                    participant_session_id, participant_station
                FROM
                    participant
                WHERE
                    meeting_key = '%s'
                ORDER BY
                    participant_key ASC
                    ", $info["meeting"]["meeting_key"] );
        $rs = $this->objMeeting->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getMessage());
            return false;
        } else {
            $data = array();
            while( $reslt = $rs->fetchRow(DB_FETCHMODE_ASSOC) ){
                $data[$reslt["participant_session_id"]] = $reslt;
            }
            $count = 0;
            foreach( $data as $key => $value ){
                if( ! $value["participant_station"] ) $count++;
            }
            $info["meeting"]["no_select"] = $count;
        }
        return $info;
    }

    public function getAllEcoLog( $room_key, $targetdate )
    {
        $date = substr( $targetdate, 0, 7 );
        $log_info = array();
        //今月のデータ
        $sql = "
                SELECT
                    SUM( eco_move ) as eco_move,
                    SUM( eco_time ) as eco_time,
                    SUM( eco_fare ) as eco_fare,
                    SUM( eco_co2 ) as eco_co2,
                    DATE_FORMAT( log_date, '%Y-%m')
                FROM
                    meeting_date_log
                WHERE
                    room_key = '" . mysql_real_escape_string($room_key) . "' AND
                    DATE_FORMAT( log_date, '%Y-%m') = '" . $date ."'
                GROUP BY
                    DATE_FORMAT( log_date, '%Y-%m');";
        $rs = $this->objMeetingDateLog->_conn->query( $sql );
        if ( DB::isError( $rs ) ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getMessage());
            return false;
        } else {
            $row = $rs->fetchRow( DB_FETCHMODE_ASSOC );
            $log_info["monthly"]["eco_move"] = $row["eco_move"];
            $log_info["monthly"]["eco_time"] = $row["eco_time"];
            $log_info["monthly"]["eco_fare"] = $row["eco_fare"];
            $log_info["monthly"]["eco_co2"] = $row["eco_co2"] < 0 ? 0 : $row["eco_co2"];
        }

        //先月のデータ
        /*
        $_date = date_parse($targetdate);
        $lastmonth = date("Y-m", mktime(0, 0, 0, $_date["month"] - 1, $_date["day"],$_date["year"]));
        $sql = "
                SELECT
                    SUM( eco_co2 ) as eco_co2
                FROM
                    meeting_date_log
                WHERE
                    room_key = '" . $room_key . "' AND
                    DATE_FORMAT( log_date, '%Y-%m') = '" . $lastmonth ."'
                GROUP BY
                    DATE_FORMAT( log_date, '%Y-%m');";
        $rs = $this->objMeetingDateLog->_conn->query( $sql );
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getMessage());
            return false;
        } else {
            $row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
            $last_co2 = $row["eco_co2"] < 0 ? 0 : $row["eco_co2"];
            $log_info["lastMonth"]["eco_co2"] = $log_info["monthly"]["eco_co2"] - $last_co2;
        }
        */
        //日付成形
        $nowdate = date("Y-m");        //当月より１年間に変更    [MTG_MEMBERS:4648] ECO月毎ページの変更作業
        $dateArray = date_parse( $nowdate );
        $startdate = mktime( 0, 0, 0, $dateArray["month"] + 1, 1, $dateArray["year"] -1 );
        $enddate = mktime( 0, 0, 0, $dateArray["month"], 1, $dateArray["year"] );

        //すべてのデータ
        $sql = "
                SELECT
                    DATE_FORMAT( log_date, '%Y-%m' ) as date,
                    SUM( eco_move ) as eco_move,
                    SUM( eco_time ) as eco_time,
                    SUM( eco_fare ) as eco_fare,
                    SUM( eco_co2 ) as eco_co2
                FROM
                    meeting_date_log
                WHERE
                    room_key = '" . mysql_real_escape_string($room_key) . "' AND
                    DATE_FORMAT( log_date, '%Y-%m' ) >= ".date("Y-m", $startdate)."
                GROUP BY
                    DATE_FORMAT( log_date, '%Y-%m' )
                ORDER BY
                    log_date";
        $rs = $this->objMeetingDateLog->_conn->query($sql);
        if ( DB::isError( $rs ) ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getMessage());
            return false;
        } else {
            $tmp = array();
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC ) ){
                $tmp[$row["date"]] = $row;
            }
        }

        //画像用データ処理
        $log_info["imagedata"]["startdate"] = date( "Y-m", $startdate );
        $start = $startdate;
        $lastmonth = strtotime("-1 month", $enddate);
        while( $start <= $enddate ){
            $eco_move = $tmp[date( "Y-m", $start )]["eco_move"] ? $tmp[date( "Y-m", $start )]["eco_move"] : 0;
            $eco_time = $tmp[date( "Y-m", $start )]["eco_time"] ? $tmp[date( "Y-m", $start )]["eco_time"] : 0;
            $eco_fare = $tmp[date( "Y-m", $start )]["eco_fare"] ? $tmp[date( "Y-m", $start )]["eco_fare"] : 0;
            $eco_co2 = $tmp[date( "Y-m", $start )]["eco_co2"] >= 0 ? $tmp[date( "Y-m", $start )]["eco_co2"] : 0;
            $log_info["imagedata"]["horizontal"][] = date( "n", $start );
            $log_info["all"]["eco_move"] += $log_info["imagedata"]["eco_move"][] = $eco_move;
            $log_info["all"]["eco_time"] += $log_info["imagedata"]["eco_time"][] = $eco_time;
            $log_info["all"]["eco_fare"] += $log_info["imagedata"]["eco_fare"][] = $eco_fare;
            $log_info["all"]["eco_co2"] += $log_info["imagedata"]["eco_co2"][] = $eco_co2;
            $start = strtotime("+1 month", $start);
        }
        $log_info["imagedata"]["enddate"] = date( "Y-m", $enddate );
        $sql = "
                SELECT
                    u.user_starttime
                FROM
                    (
                    SELECT
                        user_key
                    FROM
                        room
                    WHERE
                        room_key = '" . mysql_real_escape_string($room_key) . "'
                    ) r
                LEFT JOIN
                    user u
                ON
                    r.user_key = u.user_key
                LIMIT 0, 1";

        $rs = $this->objUser->_conn->query($sql);
        $row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
        $date = date_parse($row["user_starttime"]);
        $serviceStarttime = mktime(0, 0, 0, $date["month"], $date["day"],$date["year"]);
        $min = ($startdate < $serviceStarttime) ? $serviceStarttime : $startdate;

        /**
         * 利用期間
         * 利用開始日が１年未満の場合はサービス開始日より計算する
         */

        $log_info["all"]["period"] =  (strtotime(date("Y-m-d")) - $min) / 86400 + 1;
        /*
        $log_info["all"]["period"] = (date("Y", $enddate) > date("Y", $min)) ?
                                        13 + date("n", $enddate) - date("n", $min):
                                        date("n", $enddate) - date("n", $min) + 1;
        */
        //開始月
        /*
        $sql = "
                SELECT
                    DATE_FORMAT( MIN( log_date ), '%Y-%m' ) as min
                FROM
                    meeting_date_log
                WHERE
                    room_key = '" . $room_key . "';";
        $rs = $this->objMeetingDateLog->_conn->query($sql);
        if ( DB::isError( $rs ) ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getMessage());
            return false;
        } else {
            $row = $rs->fetchRow(DB_FETCHMODE_ASSOC );
            $log_info["all"]["min"] = date("Y-m", $min);
        }
        */
        $log_info["all"]["min"] = date("Y-m", $min);
        return $log_info;
    }
    
    public function getAllUserEcoLog( $user_id, $targetdate )
    {
    	$date = substr( $targetdate, 0, 7 );
    	$log_info = array();
    	//今月のデータ
    	$sql = "
                SELECT
                    SUM( eco_move ) as eco_move,
                    SUM( eco_time ) as eco_time,
                    SUM( eco_fare ) as eco_fare,
                    SUM( eco_co2 ) as eco_co2,
                    DATE_FORMAT( log_date, '%Y-%m')
                FROM
                    meeting_date_log
                WHERE
                    user_id = '" . mysql_real_escape_string($user_id) . "' AND
                    DATE_FORMAT( log_date, '%Y-%m') = '" . $date ."'
                GROUP BY
                    DATE_FORMAT( log_date, '%Y-%m');";
    	$rs = $this->objMeetingDateLog->_conn->query( $sql );
    	if ( DB::isError( $rs ) ) {
    		$this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getMessage());
    		return false;
    	} else {
    		$row = $rs->fetchRow( DB_FETCHMODE_ASSOC );
    		$log_info["monthly"]["eco_move"] = $row["eco_move"];
    		$log_info["monthly"]["eco_time"] = $row["eco_time"];
    		$log_info["monthly"]["eco_fare"] = $row["eco_fare"];
    		$log_info["monthly"]["eco_co2"] = $row["eco_co2"] < 0 ? 0 : $row["eco_co2"];
    	}
    
    	//日付成形
    	$nowdate = date("Y-m");        //当月より１年間に変更    [MTG_MEMBERS:4648] ECO月毎ページの変更作業
    	$dateArray = date_parse( $nowdate );
    	$startdate = mktime( 0, 0, 0, $dateArray["month"] + 1, 1, $dateArray["year"] -1 );
    	$enddate = mktime( 0, 0, 0, $dateArray["month"], 1, $dateArray["year"] );
    
    	//すべてのデータ
    	$sql = "
                SELECT
                    DATE_FORMAT( log_date, '%Y-%m' ) as date,
                    SUM( eco_move ) as eco_move,
                    SUM( eco_time ) as eco_time,
                    SUM( eco_fare ) as eco_fare,
                    SUM( eco_co2 ) as eco_co2
                FROM
                    meeting_date_log
                WHERE
                    user_id = '" . mysql_real_escape_string($user_id) . "' AND
                    DATE_FORMAT( log_date, '%Y-%m' ) >= ".date("Y-m", $startdate)."
                GROUP BY
                    DATE_FORMAT( log_date, '%Y-%m' )
                ORDER BY
                    log_date";
    	$rs = $this->objMeetingDateLog->_conn->query($sql);
    	if ( DB::isError( $rs ) ) {
    		$this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getMessage());
    		return false;
    	} else {
    		$tmp = array();
    		while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC ) ){
    			$tmp[$row["date"]] = $row;
    		}
    	}
    
    	//画像用データ処理
    	$log_info["imagedata"]["startdate"] = date( "Y-m", $startdate );
    	$start = $startdate;
    	$lastmonth = strtotime("-1 month", $enddate);
    	while( $start <= $enddate ){
    		$eco_move = $tmp[date( "Y-m", $start )]["eco_move"] ? $tmp[date( "Y-m", $start )]["eco_move"] : 0;
    		$eco_time = $tmp[date( "Y-m", $start )]["eco_time"] ? $tmp[date( "Y-m", $start )]["eco_time"] : 0;
    		$eco_fare = $tmp[date( "Y-m", $start )]["eco_fare"] ? $tmp[date( "Y-m", $start )]["eco_fare"] : 0;
    		$eco_co2 = $tmp[date( "Y-m", $start )]["eco_co2"] >= 0 ? $tmp[date( "Y-m", $start )]["eco_co2"] : 0;
    		$log_info["imagedata"]["horizontal"][] = date( "n", $start );
    		$log_info["all"]["eco_move"] += $log_info["imagedata"]["eco_move"][] = $eco_move;
    		$log_info["all"]["eco_time"] += $log_info["imagedata"]["eco_time"][] = $eco_time;
    		$log_info["all"]["eco_fare"] += $log_info["imagedata"]["eco_fare"][] = $eco_fare;
    		$log_info["all"]["eco_co2"] += $log_info["imagedata"]["eco_co2"][] = $eco_co2;
    		$start = strtotime("+1 month", $start);
    	}
    	$log_info["imagedata"]["enddate"] = date( "Y-m", $enddate );
    	$sql = "
                SELECT
                    user_starttime
                FROM
                    user
    			WHERE
    			    user_id ='" . mysql_real_escape_string($user_id) . "'
    			LIMIT 0, 1";
    
    	$rs = $this->objUser->_conn->query($sql);
    	$row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
    	$date = date_parse($row["user_starttime"]);
    	$serviceStarttime = mktime(0, 0, 0, $date["month"], $date["day"],$date["year"]);
    	$min = ($startdate < $serviceStarttime) ? $serviceStarttime : $startdate;
    
    	/**
    	 * 利用期間
    	 * 利用開始日が１年未満の場合はサービス開始日より計算する
    	 */
    
    	$log_info["all"]["period"] =  (strtotime(date("Y-m-d")) - $min) / 86400 + 1;
    	$log_info["all"]["min"] = date("Y-m", $min);
    	return $log_info;
    }

    public function getYearlyLog( $user_id, $thisMonth, $lastYear )
    {
        $log_info = array();
        //今月のデータ
        $sql = "
                SELECT
                    SUM( eco_move ) as eco_move,
                    SUM( eco_time ) as eco_time,
                    SUM( eco_fare ) as eco_fare,
                    SUM( eco_co2 ) as eco_co2
                FROM
                    meeting_date_log
                WHERE
                    user_id = '" . mysql_real_escape_string($user_id) . "' AND
                    DATE_FORMAT( log_date, '%Y-%m') <= '" . $thisMonth ."' AND
                    DATE_FORMAT( log_date, '%Y-%m') >= '" . $lastYear ."'";
        $rs = $this->objMeetingDateLog->_conn->query( $sql );
        if ( DB::isError( $rs ) ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getMessage());
            return false;
        } else {
            $info = $rs->fetchRow(DB_FETCHMODE_ASSOC );
            $co2 = $info["eco_co2"] < 0 ? 0 : $info["eco_co2"];
            $info["eco_co2"] = $this->get_co2_info($co2);
            $info["eco_time"] = $this->makeTime($info["eco_time"]);
            return $info;
        }
    }

    public static function get_co2_info( $co2 )
    {
        $s = array('g', 'Kg', 't', 'kt', 'Mt', 'Gt');
        if ($co2 == 0) {
            $e = 0;
            $result["value"] = "0";
            $result["unit"] = $s[0];
        } else {
            if ($co2 > 0) {
                $e = floor(log($co2)/log(1000));
                $result["value"] = sprintf('%.2f ', ($co2/pow(1000, floor($e))));
                $result["unit"] = $s[$e];
            } else {
                // マイナス
                $co2 = $co2 * -1;
                $e = floor(log($co2)/log(1000));
                $result["value"] = sprintf('-%.2f ', ($co2/pow(1000, floor($e))));
                $result["unit"] = $s[$e];
            }
        }
        if( $co2 >= 1000000 ){
            $result["denominator"] = 1000000;
        } else {
            $result["denominator"] = 1000;
        }
        return $result;
    }

    public static function makeTime( $time )
    {
        $return = array();
        $return["hour"] = floor( $time / 60 );
        $return["minute"] = fmod( $time, 60 );
        return $return;
    }

    /**
     * createReport
     *
     * 参加者情報からECO情報を自動生成を行う。
     * 利用用途は、まず参加者の拠点情報を更新してから、当該メソッドを実行してください。
     * ECO情報の更新には時間がかかる場合がありますので、バックエンドで処理を行います。
     *
     * @param integer $meeting_key  会議キー
     * @param string  $end_point    目的地の指定（無い場合は最短ルートから自動算出※非常に重いので注意！）
     * @param boolean $mail         参加者にメールを送信するか（false:送信しない、true:送信する）
     * @return boolean true:正常、false:失敗
     * @
     */
    public function createReport($meeting_key, $end_pint = "", $mail = false, $background = true) {
        $server_list = parse_ini_file( N2MY_APP_DIR."config/server_list.ini", true);
        foreach($server_list["SERVER_LIST"] as $key => $dns) {
            if ($dns == $this->dsn) {
                $server_dsn_key = $key;
                break;
            }
        }
        // ECOメータ集計
        $ch = curl_init();
        $url = N2MY_LOCAL_URL."api/eco.php";
        $post_data = array(
            "server_key"  => $server_dsn_key,
            "meeting_key" => $meeting_key,
            "end_point"   => $end_pint,
            "mail"        => $mail,
        );
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $post_data,
        );
        // 明示的に接続をきって、バックグラウンドで実行
        if ($background) {
            $option[CURLOPT_CONNECTTIMEOUT] = 1;
            $option[CURLOPT_TIMEOUT]        = 1;
        // 無制限というわけにも行かないので
        } else {
            $option[CURLOPT_CONNECTTIMEOUT] = 60;
            $option[CURLOPT_TIMEOUT]        = 60;
        }
        $this->logger->debug($option);
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        return true;
    }

    public function getMonthlyLogList($user_key, $tMonth)
    {
        $info = array();
        $info["fareTotal"] = 0;
        $info["minuteTotal"] = 0;
        $info["moveTotal"] = 0;
        $info["co2Total"] = 0;
        $info["numNoStationName"] = 0;
        $info["numStationName"] =0;

        //meeting
        $field = "meeting_key, meeting_name, actual_start_datetime, eco_info, eco_move, eco_time, eco_fare, eco_co2";
        $where = "user_key = '" . $user_key . "' AND
                    is_active = 0 AND
                    use_flg = 1 AND
                    is_deleted = 0 AND
                    meeting_use_minute > 0 AND
                    eco_co2 > 0 AND
                    DATE_FORMAT( actual_start_datetime, '%Y-%m') = '" . $tMonth ."'";
        $sort = array("actual_start_datetime" => "asc");
        $meetingList = $this->objMeeting->getRowsAssoc($where, $sort, null, null, $field);
        if ( DB::isError( $meetingList ) ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$meetingList->getMessage());
            return false;
        }
        $info["numMeeting"] = count($meetingList);
        for ($i = 0; $i < count($meetingList); $i++) {
            $meetingInfo = array(
                                "meeting_name"=>$meetingList[$i]["meeting_name"],
                                "actual_start_datetime"=>$meetingList[$i]["actual_start_datetime"]
                                );
            $ecoInfo = unserialize($meetingList[$i]["eco_info"]);
            $sql =sprintf(
                    "SELECT
                        p1.participant_name, p1.participant_station
                    FROM
                        participant p1
                    INNER JOIN
                    (
                        SELECT
                            MAX(participant_key) as participant_key, participant_session_id
                        FROM
                            participant
                        WHERE
                            meeting_key='%s' AND participant_name IS NOT NULL
                            GROUP BY participant_session_id
                    ) p2
                    ON
                        p1.participant_key = p2.participant_key
                    ", $meetingList[$i]["meeting_key"]);

            $rs = $this->objParticipant->_conn->query( $sql );
            if ( DB::isError( $rs ) ) {
                $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getMessage());
                continue;
            }
            $participantList = array();
            while($participant = $rs->fetchRow(DB_FETCHMODE_ASSOC )){
                //未入力
                if (!$participant["participant_station"]){
                    $info["numNoStationName"] += 1;
                    $participant["participant_station"] = "-";
                    $participant["fare"] = "-";
                    $participant["minute"] = "-";
                    $participant["move"] = "-";
                    $participant["co2"] = "-";
                }
                //開催地
                else if ($participant["participant_station"] == $ecoInfo["end"]) {
                    $participant["venue"] = 1;
                    $participant["fare"] = 0;
                    $participant["minute"] = 0;
                    $participant["move"] = 0;
                    $participant["co2"] = 0;
                    $info["numStationName"] += 1;
                }
                // from
                else {
                    $participant["fare"] = $ecoInfo["start"][$participant["participant_station"]]["summary"]["fare"]["fareunit"]["unit"] * 2;
                    $participant["minute"] = $ecoInfo["start"][$participant["participant_station"]]["summary"]["move"]["minute"] * 2;
                    $participant["move"] = $ecoInfo["start"][$participant["participant_station"]]["summary"]["move"]["move"] * 2;
//                    $participant["co2"] = sprintf('%.1f ', ($ecoInfo["start"][$participant["participant_station"]]["summary"]["move"]["co2"] * 2 / 1000));
                    $participant["co2"] = $ecoInfo["start"][$participant["participant_station"]]["summary"]["move"]["co2"] * 2;

                    $info["fareTotal"] += $participant["fare"];
                    $info["minuteTotal"] += $participant["minute"];
                    $info["moveTotal"] += $participant["move"];
                    $info["co2Total"] += $participant["co2"];

                    $info["numStationName"] += 1;
                }
                $meetingInfo["participantList"][] = $participant;
            }
            $info["meetingList"][] = $meetingInfo;
        }
        return $info;
    }

    public function getYearlyLogList($user_key, $tYear, $tMonth)
    {
        $info = array(
                        "fareTotal"=>0,
                        "minuteTotal"=>0,
                        "moveTotal"=>0,
                        "co2Total"=>0,
                        "numStationName"=>0,
                        "numNoStationName"=>0,
                        "numMeeting"=>0
                        );
        for ($i = 1; $i <= $tMonth; $i++) {
            $target = sprintf("%04d-%02d", $tYear, $i);
            $list = $this->getMonthlyLogList($user_key, $target);
            $list["date"] = $target;
            $info["list"][] = $list;

            $info["fareTotal"]            += $list["fareTotal"];
            $info["minuteTotal"]          += $list["minuteTotal"];
            $info["moveTotal"]            += $list["moveTotal"];
            $info["co2Total"]             += $list["co2Total"];
            $info["numStationName"]       += $list["numStationName"];
            $info["numNoStationName"]     += $list["numNoStationName"];
            $info["numMeeting"]           += $list["numMeeting"];
        }
        return $info;
    }
}
