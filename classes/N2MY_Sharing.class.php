<?php

require_once("classes/dbi/meeting.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("lib/EZLib/EZCore/EZLogger2.class.php");

class N2MY_Sharing
{

    var $obj_Document = null;
    var $obj_Process = null;
    var $logger = null;
    var $dsn = null;

    public $fromAddress = NOREPLY_ADDRESS;
    public $additionalMessage = null;
    public $fromUserName = null;

    function __construct($dsn) {
        $this->logger = EZLogger2::getInstance();
        $this->dsn = $dsn;
    }


    /*
     * メモファイル作成
     */
    function addMemo($path , $data){
        $fp = fopen($path,'wb');
        if($fp){
            if(flock($fp,LOCK_EX)){
                if(fwrite($fp,$data) === FALSE){
                    return false;
                }
                flock($fp,LOCK_UN);
            }
            else {
                return false;
            }
            fclose($fp);
            return true;
        }
        return false;
    }

    /**
     * ファイル名にプレフィックスを付けたファイル名を取得
     * @param $dir ディレクトリ
     * @param $filename ファイル名
     * @param $extension 拡張子名
     * @return プレフィックスが付与されたファイルパス
     */
    function addPrefixFileName($dir , $filename , $extension){
        $name_lengh = mb_strlen($filename);
        $ex_lengh = mb_strlen($extension);

        $no_ex_file_name = substr($filename,0,$name_lengh-$ex_lengh);
        $i = 1;
        while(1){
            $new_file_name = $no_ex_file_name."_".$i.$extension;
            if(!file_exists( $dir.$new_file_name)){
                break;
            }
            $i++;
        }
        return $new_file_name;
    }

    /*
     * ディレクトリ作成
     */
    public function makeDir( $meeting_key )
    {
        $dir = $this->getDir( $meeting_key );
        $fullDir = $this->getFullPath( $dir );
        if( false !== $fullDir && ! is_dir( $fullDir ) ){
            EZFile::mkdir_r( $fullDir, 0777 );
            chmod( $fullDir , 0777 );
        }
        return $fullDir;
    }

    /*
     * 一時ファイル用ディレクトリ作成
     */
    public function makeDirTmp($meeting_key){
        $dir = $this->getDir( $meeting_key );
        $fullDir = $this->getFullPath( $dir."tmp/" );
        if( false !== $fullDir && ! is_dir( $fullDir ) ){
            EZFile::mkdir_r( $fullDir, 0777 );
            chmod(  $this->getFullPath($dir) , 0777 );
            chmod( $fullDir , 0777 );
        }
        return $fullDir;
    }

    /*
     * ディレクトリ取得
     */
    function getDir($meeting_key){
        $objMeeting = new MeetingTable( $this->dsn );
        $meetingInfo = $objMeeting->getRow( sprintf( "meeting_key='%s'", $meeting_key ) );
        $meeting_id = $objMeeting->getMeetingID($meeting_key);
        $objUser = new UserTable($this->dsn);
        $user_info = $objUser->getRow(sprintf( "user_key='%s'", $meetingInfo["user_key"] ));
        $dateInfo = date_parse( $meetingInfo["create_datetime"] );
        //$this->logger->info( array(sprintf( "meeting_key='%s'", $meeting_key ), $meeting_id, $meetingInfo, $dateInfo));
        if( ! $dateInfo["error_count"] ){
            return sprintf( "%s/%s/%d/", $user_info["user_id"], $meetingInfo["room_key"], $meeting_key );
        } else {
            $this->logger->info( $meeting_key );
            return false;
        }
    }

    /*
     * フルパス取得
     */
    public function getFullPath($dir, $name = null)
    {
        if ($name) {
            return sprintf( "%s%s%s", N2MY_SHARE_DIR, $dir, $name );
        } else {
            return sprintf( "%s%s", N2MY_SHARE_DIR, $dir );
        }
    }

    /**
     * メモデータをテキストで保存する
     * @param $meeting_key ミーティングキー
     * @param $data 保存するデータ
     * @param $file_name ファイル名
     * @param $extension 拡張子
     * @param $tmp_flag 一時ファイルフラグ
     * @return 作成したファイルのフルパス
     */
    public function makeMemoDate($meeting_key , $data , $file_name , $extension = ".txt", $tmp_flag = false){
        // ディレクトリ取得
        $dir = $this->getDir($meeting_key);

        //
        if($tmp_flag){
            $dir = $dir."tmp/";
            // 一時ファイル用ディレクトリ作成
            if(!is_dir($this->getFullPath($dir))){
                $this->makeDirTmp($meeting_key);
            }
        }else{
            // ディレクトリ作成
            if(!is_dir($this->getFullPath($dir))){
                $this->makeDir($meeting_key);
            }
        }

        // ファイル名を作成
        //$filename =  $objSharing->addPrefixFileName($objSharing->getFullPath($dir), $meeting_sequence_key.".txt", ".txt");
        // プレフィックスを付ける
        $file_name =  $this->addPrefixFileName($this->getFullPath($dir), $file_name, $extension);
        // メモファイル作成
        $full_path = $this->getFullPath($dir,$file_name);
        // 改行コードを\r\nに統一
        $data = str_replace("\r\n","\n",$data);
        $data = str_replace("\r","\n",$data);
        $data = str_replace("\n","\r\n",$data);
        $memo_result = $this->addMemo($full_path, $data);

        if(!$memo_result){
            return false;
        }

        chmod($full_path , 0777);

        $return_data = array(
                "full_path" => $full_path,
                "file_name"  => $file_name
                );

        return $return_data;
    }

    /*
     * メモデータを送信
     */
    public function sendMemoData($meeting_key, $mailAddress, $filePath , $fileName, $lang="ja" , $time_zone = 9){
        require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
        require_once ("lib/EZLib/EZHTML/EZTemplate.class.php");
        $config = EZConfig::getInstance();

        require_once("classes/dbi/meeting.dbi.php");
        $objMeetingTable = new MeetingTable($this->dsn);

        $meeting_info = $objMeetingTable->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", mysql_real_escape_string($meeting_key )));

        // テンプレート
        $template = new EZTemplate($config->getAll('SMARTY_DIR'));
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang_cd = EZLanguage::getLangCd($lang);
        $frame["lang"] = $lang_cd;
        $template->assign("__frame",$frame);
        $mail    = new EZSmtp(null, $lang_cd, "UTF-8");
        $subject = $this->getMessage($lang_cd, "DEFINE", "SHARED_MEMO_SUBJECT");
        $template->assign("base_url", N2MY_BASE_URL);

        $mail->setSubject($subject);
        $mail->setFrom(RESERVATION_FROM);
        $mail->setReturnPath($this->fromAddress);
        if (preg_match("/,/", $mailAddress)) {
            $mail->setBcc($mailAddress);
        } else {
            $mail->setTo($mailAddress);
        }

        $time = (EZDate::getLocateTime((strtotime( $meeting_info["meeting_start_datetime"])), $time_zone, N2MY_SERVER_TIMEZONE));

        $template->assign("meeting_name",  $meeting_info["meeting_name"]);
        $template->assign("is_reserved",  $meeting_info["is_reserved"]);
        $template->assign("meeting_start_datetime",      date("Y-m-d H:i" , $time));
        $template->assign("meeting_stop_datetime",      $meeting_info["meeting_stop_datetime"]);
        $template_file = "common/mail_template/meeting/sharedmemo.t.txt";
        $custom = $config->get('SMARTY_DIR','custom');
        $session        = EZSession::getInstance();
        $user_info      = $session->get("user_info");
        if(!$custom){
            $custom = $user_info["service_name"];
        }
        if($custom) {
            $_template_file = "custom/". $custom . "/common/multi_lang/sharedmemo.t.txt";
            if(file_exists($template->template_dir . $_template_file)) {
                $template_file = $_template_file;
            }else{
                $_template_file = "custom/" . $custom . "/" .$lang_cd ."/mail/sharedmemo.t.txt";
                if (file_exists($template->template_dir . $_template_file)) {
                    $template_file = $_template_file;
                }else{
                    $template_file = "common/mail_template/meeting/sharedmemo.t.txt";
                }
            }
        }

        $body          = $template->fetch($template_file);
        $mail->setBody($body);
        $mail->setFilePath($filePath);
        $mail->setFileName($fileName);
        $mail->sendAttachment();
        return (string) 1;

    }
    /*
     * DL用URLの作成
     */
    public function makeUrl($server_dsn_key, $meeting_key , $shared_file_key){
        $url = N2MY_BASE_URL."services/amf/flash_api.php?action_dlSharedMemo=&server_dsn_key=".urlencode($server_dsn_key)."&meeting_key=".$meeting_key;
//        require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
//        $palam = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $file_name);
        $url = $url . "&shared_file_key=".$shared_file_key;
        return $url;
    }

    /*
     * 共有メモの名前を取得
     */
    public function getMemoFileName($meeting_key){
        $dir = $this->getDir($meeting_key);
        $full_path = $this->getFullPath($dir);
        if(!is_dir($full_path)){
            return false;
        }
        $drc = dir($full_path);
        $file_list = array();
        while($filename = $drc->read()){
            if(!is_dir($full_path.$filename) && ($filename != ".." && $filename != ".")){
                $file_list[] = $filename;
            }
        }
        return $file_list;
    }

    private function getMessage($lang, $group, $id = "") {
        static $message;
        if (!$message[$lang]) {
            $config_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
            $message[$lang] = parse_ini_file($config_file, true);
        }
        if (!$id) {
            return $message[$lang][$group];
        } else {
            return $message[$lang][$group][$id];
        }
    }


}
