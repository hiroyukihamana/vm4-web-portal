<?php
require_once( "classes/mgm/dbi/server.dbi.php" );
require_once( "classes/mgm/dbi/user.dbi.php" );
require_once ("classes/mgm/dbi/relation.dbi.php");

class MGM_AuthClass
{
    public $obj_User;
    public $obj_Server;

    function __construct( $dsn )
    {
        $this->logger =  EZLogger::getInstance();
        $this->logger2 = EZLogger2::getInstance();
        $this->dsn = $dsn;
        $this->obj_MgmRelationTable = new MgmRelationTable($this->dsn);
    }

    public function getUserInfoById( $user_id )
    {
        $obj_AuthUser = new MgmUserTable( $this->dsn );

        $user_info = $obj_AuthUser->getRow( sprintf( "user_id='%s'", addslashes($user_id )));
        if(DB::isError($user_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__,$user_info->getUserInfo() );
            return false;
        } else {
            return $user_info;
        }
    }

    public function getUserInfoByKey( $user_key )
    {
        $obj_AuthUser = new MgmUserTable( $this->dsn );

        $user_info = $obj_AuthUser->getRow( sprintf( "user_key='%s'", addslashes($user_key)) );
        if(DB::isError($user_info)) {
            $this->logger2->error($user_info->getUserInfo());
            return false;
        } else {
            return $user_info;
        }
    }

    public function getServerInfo( $server_key )
    {
        $obj_AuthServer = new MgmServerTable( $this->dsn );
        $server_info = $obj_AuthServer->get_server_info( $server_key );
        if(DB::isError($server_info)) {
            $this->logger2->warn($server_info->getUserInfo());
            return false;
        } else {
            return $server_info;
        }
    }

    public function getRelationInfo( $relation_key )
    {
        $obj_MgmAccessInfo = new MgmRelationTable( $this->dsn );
        $ret = $obj_MgmAccessInfo->getRow( sprintf( "relation_key='%s'", addslashes($relation_key )));
        if (DB::isError( $ret ) ) {
            $this->logger->error( $ret->getUserInfo() );
        } else {
            return $ret;
        }
    }

    /**
     * データを登録する
     *
     * @param
     * @return
     */
    function addRelationData($user_key, $relation_key, $type) {
        if ($user_key && $relation_key && $type) {
            //すでにrelation_keyが登録されているか確認
            $where = "relation_key ='".$relation_key."'".
                " AND user_key = '".$user_key."'".
                " AND relation_type = '".$type."'";
            $cnt = $this->obj_MgmRelationTable->numRows($where);
            if ($cnt == 0) {
                $data = array(
                    "relation_key"  => $relation_key,
                    "user_key"      => $user_key,
                    "relation_type" => $type,
                    "status"        => 1,
                );
                $add_data = $this->obj_MgmRelationTable->add($data);
                if (DB::isError($add_data)) {
                    $this->logger2->error($add_data->getUserInfo());
                } else {
                    return true;
                }
            } else {
                $this->logger2->warn(array($user_key, $relation_key, $type));
            }
        }
        return false;
    }

    /**
     * dsnを取得する
     *
     * @param
     * @return
     */
    function getRelationDsn($relation_key, $type) {
        if ($relation_key && $type) {
            // ユーザ取得
            $where = "relation_key = '". addslashes($relation_key)."'" .
                " AND relation_type = '".addslashes($type)."'";
//                " AND status = 1";
            $user_id = $this->obj_MgmRelationTable->getOne($where, "user_key");
            if ( DB::isError( $user_id ) ) {
                $this->logger->error( $user_id->getUserInfo() );
                return $user_id;
            }
            if (!$user_id) {
                $this->logger2->warn(array($relation_key, $type), "リレーションキーが存在しないか、無効");
                return false;
            }
            // ユーザ取得
            $obj_AuthUser = new MgmUserTable($this->dsn);
            $where = "user_id = '".mysql_real_escape_string($user_id)."'";
            $server_key = $obj_AuthUser->getOne($where, "server_key");
            if (DB::isError( $server_key )) {
                $this->logger->error( $server_key->getUserInfo() );
                return $server_key;
            }
            
            if ( !$server_key ){
                $this->logger2->warn(array($relation_key, $type), "リレーションキーが存在しないか、無効");
                return false;
            }
            
            // DSN取得
            $obj_AuthServer = new MgmServerTable($this->dsn);
            $where = "server_key = ".$server_key;
            $dsn_info = $obj_AuthServer->getRow($where);
            return $dsn_info;
        }
        return false;
    }

    /**
     * データを削除する
     *
     * @param
     * @return
     */
    function deleteRelationData($relation_key, $type = null) {
        if ($relation_key) {
            $where = " relation_key = '". addslashes($relation_key)."'";
            if (!$type) {
                $this->logger2->error($type);
            } else {
                $where .= " AND relation_type = '".$type."'";
            }
            $this->logger2->info($where);
            $data = array("status" => "0");
            $ret = $this->obj_MgmRelationTable->update($data, $where);
            if (DB::isError($ret)) {
                $this->logger2->info($ret->getMessage());
            }
        }
    }

    function getMeetingID($meeting_id) {
        static $meeting_info;
        if (isset($meeting_info[$meeting_id])) {
            return $meeting_info[$meeting_id];
        }
        // 変換不要
        if ((strlen($meeting_id) < 20) && is_numeric($meeting_id)) {
            $meeting_info[$meeting_id] = $meeting_id;
        // ハッシュしたキーから meeting_key
        } else {
            if (!$dsn_info = $this->getRelationDsn($meeting_id, "meeting")) {
                $this->logger2->warn('error');
                return false;
            }
            require_once("classes/core/dbi/Meeting.dbi.php");
            $obj_Meeting = new DBI_Meeting($dsn_info["dsn"]);
            $meeting_key = $obj_Meeting->getMeetingID($meeting_id, 'meeting_key');
            if ($meeting_key) {
                $meeting_info[$meeting_id] = $meeting_key;
            } else {
                $meeting_info[$meeting_id] = false;
            }
        }
        return $meeting_info[$meeting_id];
    }
}
