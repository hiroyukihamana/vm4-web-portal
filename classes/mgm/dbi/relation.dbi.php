<?php
require_once("classes/N2MY_DBI.class.php");

class MgmRelationTable extends N2MY_DB {

    function __construct( $dsn ) {
        $this->init( $dsn, "relation" );
    }

    public function add( $data )
    {
        $data["create_datetime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }

    public function deleteRelationByRoomkey($room_key){
        $relation_where = "relation_key = '".addslashes($room_key)."'";
        $relation_data = array(
                "status" => "0",
        );
        $this->update($relation_data, $relation_where);
    }
}
