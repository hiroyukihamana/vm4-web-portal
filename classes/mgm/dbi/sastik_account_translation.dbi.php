<?php
require_once("classes/N2MY_DBI.class.php");

class SastikAccountTranslationTable extends N2MY_DB {
	public $table = 'sastik_account_translation';
	protected $primary_key = 'account_translation_key';

	public function __construct($dsn) {
        	$this->init($dsn, $this->table);
	}

	public function add($data) {
	        $data["create_datetime"] = date("Y-m-d H:i:s");
	        $this->logger2->debug($data);
	        return parent::add($data);
	}

	public function update($data, $where) {
	        $data["update_datetime"] = date("Y-m-d H:i:s");
	        $this->logger2->debug(array($data, $where));
	        return parent::update($data, $where);
	}
}

?>

