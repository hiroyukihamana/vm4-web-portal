<?php
/**
 * Created on 2006/09/19
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

require_once("classes/N2MY_DBI.class.php");
require_once("lib/EZLib/EZXML/EZXML.class.php");
require_once("lib/EZLib/EZUtil/EZUrl.class.php");

class MeetingDateLogTable extends N2MY_DB {

    var $table = "meeting_date_log";
    var $logger = null;
    var $provider_id = "";
    var $provider_pw = "";
    var $base_url = "";

    var $rules = array(
        "log_no"       		=> array("required" => true),
        "log_date"         	=> array("required" => true),
        "meeting_cnt"     	=> array("required" => true),
        "use_time"    		=> array("required" => true),
        "participant_cnt"   => array("required" => true),
    );


    function MeetingDateLogTable($dsn)
    {
        $this->init($dsn, $this->table);
    }

    /**
     * 期間を指定して、月間集計を行う
     */
    function month_summary($target_month, $room_keys = array())
    {
        $start_date = "";
        $end_date = "";
        $sql = "SELECT " .
               " '" . $target_month . "' as log_month" .
               ", `user_id`" .
               ", `room_key`" .
               ", sum(`use_time`) as extends" .
               ", sum(`account_time_mobile_normal`) as mobile_extends_normal" .
               ", sum(`account_time_mobile_special`) as mobile_extends_special" .
               ", sum(`account_time_hispec`) as hispec_extends" .
               ", sum(`account_time_audience`) as audience_extends" .
               ", sum(`account_time_h323`) as h323_extends" .
               ", sum(`account_time_h323ins`) as h323ins_extends" .
               ", sum(`meeting_cnt`) as meeting_cnt" .
               ", sum(`participant_cnt`) as participant_cnt" .
               " FROM " . $this->table .
               " WHERE `log_date` LIKE '" . addslashes($target_month) . "%' ";
        if ($room_keys) { // roomの指定があれば対象のroomの分だけの月間集計を実施する。
            $sql .= " AND `room_key` in ('" . implode("','", $room_keys) . "') ";
        }
        $sql .= " GROUP BY `user_id`, `room_key` " .
                " ORDER BY `user_id`, `room_key` ";

//        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$sql);
        $rs = $this->_conn->query($sql);
        if(DB::isError($rs)) {
            $this->logger->info(__FUNCTION__." # Error Is ... ",__FILE__,__LINE__, $rs->getMessage());
            return $rs;
        }
        $ret = array();
        while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
//            $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$row);
            $ret[$row['user_id']][$row['room_key']] = $row;
            //H323ins対応（H323とH323insは合算する）金額は、同じ。
            $ret[$row['user_id']][$row['room_key']]['h323_extends'] += $ret[$row['user_id']][$row['room_key']]['h323ins_extends'];
        }
//        $this->logger->info(__FUNCTION__." # Result ... ",__FILE__,__LINE__, $ret);
        return $ret;
    }


    /**
     * core APIの結果取得
     *
     * @param string $path 実行先のパス
     * @param array $param パラメタ
     * @return string core側のurl（指定がなければベースURl）
     */
    function _get_data($path = "", $param = array()) {
        $query_str = "";
        foreach ($param as $_key => $_value) {
            $query_str .= "&" . $_key . "=" . urlencode($_value);
        }
        if ($query_str) {
            $query_str = "?" . substr($query_str, 1);
        }
        $url = $this->base_url."/".$path.$query_str;
        // 実行
        $ezurl = new EZUrl();
        $contents = $ezurl->wget($url);
        return $contents;
    }


}
?>
