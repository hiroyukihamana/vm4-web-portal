<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

class CustomDomainServiceTable extends N2MY_DB {

    function CustomDomainServiceTable($dsn) {
        $this->init($dsn, "custom_domain_service");
    }
}