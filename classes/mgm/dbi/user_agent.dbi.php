<?php
require_once("classes/N2MY_DBI.class.php");

class MgmUserAgentTable extends N2MY_DB {

    var $rules = array(
        "db_key"            => array("required" => true),
        "meeting_key"       => array("required" => true, "integer" => true),
        "participant_key"   => array("integer" => true),
        "port"              => array("integer" => true),
        "monitor_color"     => array("integer" => true),
    );

    function __construct( $dsn ) {
        $this->init( $dsn, "user_agent" );
    }

    public function add( $data )
    {
        $data["create_datetime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }

    public function update( $data, $where )
    {
        $data["update_datetime"] = date( "Y-m-d H:i:s" );
        parent::update( $data, $where );
    }
}