<?php
require_once("classes/N2MY_DBI.class.php");

class MgmQuestionTable extends N2MY_DB {

    function __construct( $dsn ) {
        $this->init( $dsn, "question" );
    }

    public function add( $data )
    {
        $data["create_datetime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }

    public function update( $data, $where )
    {
        $data["update_datetime"] = date( "Y-m-d H:i:s" );
        parent::update( $data, $where );
    }

    public function delete( $data, $where )
    {
        $data["delete_datetime"] = date( "Y-m-d H:i:s" );
        parent::update( $data, $where );
    }
}