<?php
require_once("classes/N2MY_DBI.class.php");

class McuServerTable extends N2MY_DB {

    var $table = 'mcu_server';
    protected $primary_key = "";

    function __construct($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }
    
    public function getActiveServerRecords($server_key = null) {
        $where = "is_available = 1";
        
        if($server_key !== null) {
            $_sk = $server_key * 1;
            if(is_int($_sk)) {
                $where .= " AND server_key = " . addslashes($_sk);
            }
            else {
                return array();
            }
        }
        
        $rows = $this->getRowsAssoc($where);
        
        if(DB::isError($rows)) {
            return array();
        }
        else {
            return $rows;
        }
    }
    
    public function getActiveServerAddress($server_key) {
        if($server_key === null) {
            return '';
        }
        $server_record = $this->getActiveServerRecords($server_key);
        
        if(count($server_record) == 0) {
            return '';
        }
        else {
            return $server_record[0]["server_address"] . ":" . $server_record[0]["controller_port"];
        }
    }
}
