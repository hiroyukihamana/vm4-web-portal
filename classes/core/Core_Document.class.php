<?php

class Core_Document
{
    var $logger = "";
    private $dsn = null;

    public function __construct( $dsn )
    {
        $this->logger = & EZLogger::getInstance();
        $this->dsn = $dsn;
    }

    /**
     * core: /htdocs/api/document/list2.php
     */
    public function getDocumentList( $document_path )
    {
        /*
        $documents = array();
        if ($document_path) {
            // 一覧取得
            $sql = "SELECT convert_file2.no" .
                    ", convert_file2.document_path" .
                    ", convert_file2.document_id" .
                    ", convert_file2.name" .
                    ", convert_file2.mime_type" .
                    ", convert_file2.size" .
                    ", convert_file2.extension" .
                    ", convert_file2.status" .
                    ", convert_file2.create_datetime" .
                    ", convert_file2.update_datetime" .
                    ", convert_file2.sort" .
                    ", convert_file2.proc_type" .
                    ", document.document_key" .
                    ", document.document_index" .
                    " FROM convert_file2" .
                    " LEFT JOIN document" .
                    " ON document.document_id = convert_file2.document_id" .
                    " WHERE convert_file2.document_path = '" .addslashes($document_path). "'".
                    " AND convert_file2.status in ('done', 'pending', '0','error')".
                    " ORDER BY convert_file2.sort";
            $ret = $this->_db->query($sql);
        }
        */
    }

    private function get_mime_type($extension)
    {
        switch ($extension) {
        case "gif" :
            $ret = "image/gif";
            break;
        case "jpg" :
            $ret = "image/jpeg";
            break;
        case "png" :
            $ret = "image/png";
            break;
        case "pdf" :
            $ret = "application/pdf";
            break;
        case "xls" :
            $ret = "application/vnd.ms-excel";
            break;
        case "xls" :
            $ret = "application/vnd.ms-excel";
            break;
        case "doc" :
            $ret = "application/msword";
            break;
        case "ppt" :
            $ret = "application/vnd.ms-powerpoint";
            break;
        default :
            $ret = "";
            break;
        }
        return $ret;
    }
}
