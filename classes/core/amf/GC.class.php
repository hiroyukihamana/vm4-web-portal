<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// | 株式会社ブイキューブ                                                 |
// | N2MY_CORE API                                                        |
// |                                                                      |
// | GC（ガベージコレクション）クラス                                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | OS     Red Hat Enterprise Linux AS release 3 (Taroon)                |
// | Apache version Apache/1.3.34 (Unix)                                  |
// | MySQL  version 4.0.22                                                |
// | PHP    version 4.3.11 (cli)                                          |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 V-cube, Inc.                                      |
// +----------------------------------------------------------------------+
// | Authors: Masayuki IIno <iino@vcube.co.jp>                            |
// |        :                                                             |
// +----------------------------------------------------------------------+
//
// $Id$

require_once('lib/EZLib/EZCore/EZConfig.class.php');
require_once('lib/EZLib/EZCore/EZDebugger.class.php');
require_once('webapp/classes/DataSync.class.php');
require_once('webapp/classes/dbi/Document.dbi.php');
require_once('webapp/classes/dbi/Knocker.dbi.php');
require_once('webapp/classes/dbi/Meeting.dbi.php');
require_once('webapp/classes/dbi/MeetingOptions.dbi.php');
require_once('webapp/classes/dbi/MeetingUptime.dbi.php');
require_once('webapp/classes/dbi/Participant.dbi.php');
require_once('webapp/classes/dbi/Provider.dbi.php');

// ------------------------------------------------------------------------

/**
 * 【GC】DBデータ削除クラス
 *
 * <pre>
 * データベースのレコード削除系処理クラス
 * </pre>
 *
 * @access public
 * @author Masayuki IIno <iino@vcube.co.jp>
 */
class GC
{
// ------------------------------------------------------------------------

// {{{ -- vars

    /**
     * コンフィグオブジェクト
     *
     * @access public
     * @var    object EZConfig
     */
    var $_config = null;

    /**
     * Debuggerオブジェクト
     *
     * @access private
     * @var    object EZDebugger
     */
    var $_debugger = null;

// }}}

// ------------------------------------------------------------------------

// {{{ -- GC()

    /**
     * コンストラクタ
     *
     * @access public
     * @param  
     * @return 
     */
    function GC()
    {
        //create object [ EZConfig ]
        $this->_config   =& EZConfig::getInstance();

        //get debugger_config
        $debugger_config = $this->_config->getAll('DEBUGGER');

        //create object [ EZDebugger ]
        $this->_debugger =& new EZDebugger();
        $this->_debugger->init($debugger_config);
    }

// }}}
// {{{ -- _GC()

    /**
     * デストラクタ
     *
     * @access public
     * @param  
     * @return 
     */
    function _GC()
    {
    }

// }}}

// ------------------------------------------------------------------------

// {{{ -- removeAll()

    /**
     * 特定の会議に関連するデータをすべて物理削除する
     *
     * <pre>
     * ==================================================
     * 起動条件：
     * ==================================================
     *
     * 1. 会議を削除する（管理画面から起動される）
     *
     * ==================================================
     * </pre>
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return boolean
     */
    function removeAll($meeting_key)
    {
        //debug
        $this->_debugger->setMsg('meeting_key => ', $meeting_key);

        //---------------------------------------------------------------------
        //create object [ Document ]
        $obj_d   =& new Document();

        //create object [ Knocker ]
        $obj_k   =& new Knocker();

        //create object [ Meeting ]
        $obj_m   =& new Meeting();

        //create object [ MeetingOptions ]
        $obj_mo  =& new MeetingOptions();

        //create object [ MeetingUptime ]
        $obj_mu  =& new MeetingUptime();

        //create object [ Participant ]
        $obj_u   =& new Participant();

        //---------------------------------------------------------------------
        //check parameter
        $meeting = $obj_m->getDetail($meeting_key);

        $result  = false;
        if ($meeting) {
            //delete record from meeting table
            $result = $obj_m->remove($meeting_key);

            //delete record from meeting_options table
            if ($result) {
                $result = $obj_mo->remove($meeting_key);
            }

            //delete record from meeting_uptime table
            if ($result) {
                $result = $obj_mu->remove($meeting_key);
            }

            //delete record from participant table
            if ($result) {
                $result = $obj_u->remove( $meeting_key);
            }

            //delete record from knocker table
            if ($result) {
                $result = $obj_k->remove( $meeting_key);
            }

            //delete record from document table
            if ($result) {
                $result = $obj_d->remove( $meeting_key);

                if ($result) {
                    //set directory path
                    $document_dir = $this->_config->get('CONFIG_DOCUMENT', 'document_dir');
                    $target_dir   = sprintf("%s/%s", $document_dir, sha1($meeting_key));
                    $result       = $obj_d->removeDocuments($target_dir);
                }

                if ($result) {
                    $result = $obj_m->accessFMS_Minutes($meeting_key);
                }

                if ($result) {
                    $result = $obj_m->accessFMS_Video(  $meeting_key);
                }
            }
        }

        //---------------------------------------------------------------------
        $this->_debugger->setMsg('result => ', $result);
        $this->_debugger->flushMsg(__FUNCTION__, __FILE__, __LINE__);

        //return __FUNCTION__;
        return $result;
    }

// }}}
// {{{ -- removeDocuments()

    /**
     * 特定の会議に関連する資料データをすべて物理削除する
     *
     * <pre>
     * ==================================================
     * 起動条件：
     * ==================================================
     *
     * 1. 議事録データと録画データが両方削除された
     *
     * ==================================================
     * </pre>
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return boolean
     */
    function removeDocuments($meeting_key)
    {
        //debug
        $this->_debugger->setMsg('meeting_key => ', $meeting_key);

        //---------------------------------------------------------------------
        //create object [ Document ]
        $obj_d   =& new Document();

        //create object [ Meeting ]
        $obj_m   =& new Meeting();

        //---------------------------------------------------------------------
        //initialize result
        $result  = false;

        //check parameter
        $meeting = $obj_m->getDetail($meeting_key);

        if ($meeting) {
            $result  = true;

            if ($meeting['has_recorded_minutes'] == 0) {
                if ($meeting['has_recorded_video'] == 0) {
                    //set directory path
                    $document_dir = $this->_config->get('CONFIG_DOCUMENT', 'document_dir');
                    $target_dir   = sprintf("%s/%s", $document_dir, sha1($meeting_key));
                    $result       = $obj_d->removeDocuments($target_dir);
                }
            }
        }

        //---------------------------------------------------------------------
        //sync data w/ mirrors
        $sync_hosts = $this->_config->getAll('CONFIG_SYNC');

        if ($sync_hosts) {
            $mirror_dir = $this->_config->get('CONFIG_DOCUMENT', 'document_dir_mirror');

            foreach($sync_hosts as $idx => $mirror_host) {
                if ($mirror_host) {
                    if ($mirror_host != $_SERVER["SERVER_ADDR"]) {
                        $obj_sync =& new DataSync();
                        $result   =  $obj_sync->call_sync($mirror_dir, $mirror_dir, $mirror_host);
                    }
                }
            }
        }

        //---------------------------------------------------------------------
        $this->_debugger->setMsg('result => ', $result);
        $this->_debugger->flushMsg(__FUNCTION__, __FILE__, __LINE__);

        //return __FUNCTION__;
        return $result;
    }

// }}}
// {{{ -- removeWithReservation()

    /**
     * 特定の会議に関連するデータをすべて物理削除する
     *
     * <pre>
     * ==================================================
     * 起動条件：
     * ==================================================
     *
     * 1. 会議予約をキャンセル
     *
     * ==================================================
     * </pre>
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return boolean
     */
    function removeWithReservation($meeting_key)
    {
        //debug
        $this->_debugger->setMsg('meeting_key => ', $meeting_key);

        //---------------------------------------------------------------------
        //create object [ Meeting ]
        $obj_m   =& new Meeting();

        //create object [ MeetingOptions ]
        $obj_mo  =& new MeetingOptions();

        //create object [ MeetingUptime ]
        $obj_mu  =& new MeetingUptime();

        //---------------------------------------------------------------------
        //check parameter
        $meeting = $obj_m->getDetail($meeting_key);

        $result  = false;
        if ($meeting) {
            //delete record from meeting table
            $result = $obj_m->remove($meeting_key);

            //delete record from meeting_options table
            if ($result) {
                $result = $obj_mo->remove($meeting_key);
            }

            //delete record from meeting_uptime table
            if ($result) {
                $result = $obj_mu->remove($meeting_key);
            }
        }

        //---------------------------------------------------------------------
        $this->_debugger->setMsg('result => ', $result);
        $this->_debugger->flushMsg(__FUNCTION__, __FILE__, __LINE__);

        //return __FUNCTION__;
        return $result;
    }

// }}}
// {{{ -- removeWithoutReservation()

    /**
     * 特定の会議に関連するデータをすべて物理削除する
     *
     * <pre>
     * ==================================================
     * 起動条件：
     * ==================================================
     *
     * 1. 予約なしで会議を開始する
     * 2. 参加者が 1以上増えずに会議を終了したする
     *
     * ==================================================
     * </pre>
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return boolean
     */
    function removeWithoutReservation($meeting_key)
    {
        //debug
        $this->_debugger->setMsg('meeting_key => ', $meeting_key);

        //---------------------------------------------------------------------
        //create object [ Knocker ]
        $obj_k   =& new Knocker();

        //create object [ Meeting ]
        $obj_m   =& new Meeting();

        //create object [ MeetingOptions ]
        $obj_mo  =& new MeetingOptions();

        //create object [ MeetingUptime ]
        $obj_mu  =& new MeetingUptime();

        //create object [ Participant ]
        $obj_u   =& new Participant();

        //---------------------------------------------------------------------
        //check parameter
        $meeting = $obj_m->getDetail($meeting_key);

        $result  = false;
        if ($meeting) {
            //delete record from meeting table
            $result = $obj_m->remove($meeting_key);

            //delete record from meeting_options table
            if ($result) {
                $result = $obj_mo->remove($meeting_key);
            }

            //delete record from meeting_uptime table
            if ($result) {
                $result = $obj_mu->remove($meeting_key);
            }

            //delete record from participant table
            if ($result) {
                $result = $obj_u->remove( $meeting_key);
            }

            //delete record from knocker table
            if ($result) {
                $result = $obj_k->remove( $meeting_key);
            }
        }

        //---------------------------------------------------------------------
        $this->_debugger->setMsg('result => ', $result);
        $this->_debugger->flushMsg(__FUNCTION__, __FILE__, __LINE__);

        //return __FUNCTION__;
        return $result;
    }

// }}}

// ------------------------------------------------------------------------
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
