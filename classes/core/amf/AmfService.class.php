<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// | 株式会社ブイキューブ                                                 |
// | N2MY_CORE API                                                        |
// |                                                                      |
// |【画面ＩＤ】                                                          |
// |【画面名称】                                                          |
// |                                                                      |
// +----------------------------------------------------------------------+
// | OS     Red Hat Enterprise Linux AS release 3 (Taroon)                |
// | Apache version Apache/1.3.34 (Unix)                                  |
// | MySQL  version 4.0.22                                                |
// | PHP    version 4.3.11 (cli)                                          |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 V-cube, Inc.                                      |
// +----------------------------------------------------------------------+
// | Authors: shitax        <watanabe@vcube.co.jp>                        |
// |        : Masayuki IIno <iino@vcube.co.jp>                            |
// |        :                                                             |
// +----------------------------------------------------------------------+
//
// $Id$

/**
 * 【AmfService】AMFフロントエンドクラス
 *
 * @access public
 * @author shitax <watanabe@vcube.co.jp>
 */
class AmfService
{
// ------------------------------------------------------------------------

// {{{ -- vars
// }}}

// ------------------------------------------------------------------------

// {{{ -- AmfService()

    /**
     * コンストラクタ
     *
     * @access public
     * @param  
     * @return 
     */
    function AmfService()
    {
        //send header
        header("Content-Type: text/html; charset=UTF-8");
    }

// }}}
// {{{ -- _AmfService()

    /**
     * デストラクタ
     *
     * @access public
     * @param  
     * @return 
     */
    function _AmfService()
    {
    }

// }}}

// ------------------------------------------------------------------------

// {{{ -- _addCalledMethod()

    /**
     * コールされメソッドを登録
     *
     * @access public
     * @param  string  $method 実行するクラスメソッド名
     * @param  array   $args   実行するクラスメソッド引数
     * @return 
     */
    function _addCalledMethod($method, $args)
    {
        $this->methodTable[$method] = array(
                                            'description' => "",
                                            'access'      => "remote",
                                            'arguments'   => $args
                                           );
    }

// }}}

// ------------------------------------------------------------------------
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
