<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("config/config.inc.php");
require_once("classes/amf/AmfService.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("lib/EZLib/EZCore/EZLogger.class.php");
require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/resolve/Resolver.php");
require_once("classes/dbi/quick_vote_log.dbi.php");

/**
 * 【FlashGateway】
 *
 * <pre>
 * Flash と連携するためのゲートウェイクラス
 * </pre>
 *
 * @access public
 * @author V-Cube
 */
class FlashGateway extends AmfService
{
    var $config = null;
    var $dsn = null;
    var $logger = null;

    function FlashGateway() {
        $this->config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        if (!defined("N2MY_MDB_DSN")) {
            define("N2MY_MDB_DSN", $this->config["GLOBAL"]["auth_dsn"]);
        }
        $this->logger = EZLogger::getInstance($this->config["LOGGER"]);
        $this->logger2 = EZLogger2::getInstance($this->config["LOGGER"]);
        //debug
        $this->_addCalledMethod("debug", array());
        //meeting
        $this->_addCalledMethod("getMeetingKey"              , array("meeting_session_id"));
        $this->_addCalledMethod("alterDataCenter"            , array("server_dsn_key", "meeting_key", "datacenter_key"));
        $this->_addCalledMethod("rebootDataCenter"           , array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("changeDataCenter"           , array("server_dsn_key", "meeting_key", "meeting_sequence_key", "has_recorded_minutes", "has_recorded_video", "eventlog", "moveFlg", "file_list", "rec_time"));
        $this->_addCalledMethod("getMeetingInfo"             , array("room_key"));
        $this->_addCalledMethod("createConferenceData"           , array("meeting_key"));
        $this->_addCalledMethod("alterMeetingName"           , array("server_dsn_key", "meeting_key", "meeting_name"));
        $this->_addCalledMethod("authMeetingLog"             , array("server_dsn_key", "meeting_key", "meeting_log_password"));
        $this->_addCalledMethod("deleteRecordedMinutes"      , array("server_dsn_key", "meeting_key", "meeting_sequence_key"));
        $this->_addCalledMethod("deleteRecordedVideo"        , array("server_dsn_key", "meeting_key", "meeting_sequence_key"));
        $this->_addCalledMethod("extendMeeting"              , array("server_dsn_key", "meeting_key", "extra_time"));
        $this->_addCalledMethod("getDataCenterList"          , array("server_dsn_key"));
        $this->_addCalledMethod("getIsExtended"              , array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("getExtendableTime"          , array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("getStatus"                  , array("server_dsn_key", "meeting_key", "types", "meeting_sequence_key", "calling_participant_key"));
        $this->_addCalledMethod("getRemainingRecordableSize" , array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("getRemainingTime"           , array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("getGlobalLinkInfo"          , array("meeting_key"));
        $this->_addCalledMethod("lockMeeting"                , array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("sendReport"                 , array("mail_from", "mail_to", "mail_subject", "mail_body"));
        $this->_addCalledMethod("setLogPassword"             , array("server_dsn_key", "meeting_key", "meeting_log_owner", "meeting_log_password"));
        $this->_addCalledMethod("startMeeting"               , array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("stopMeeting"                , array("server_dsn_key", "meeting_key", "meeting_sequence_key", "has_recorded_minutes", "has_recorded_video", "eventlog", "moveFlg", "file_list", "rec_time"));
        $this->_addCalledMethod("postPingFailedData"         , array("server_dsn_key", "meeting_key", "meeting_sequence_key", "info"));
        $this->_addCalledMethod("unlockMeeting"              , array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("unsetLogPassword"           , array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("postSharedMemoData"         , array("server_dsn_key", "meeting_key", "meeting_sequence_key","data"));
        $this->_addCalledMethod("setPersonalWhiteboard"      , array("server_dsn_key", "meeting_key", "meeting_sequence_key","personalwhiteboards"));
        $this->_addCalledMethod("setQuickVoteResult"         , array("server_dsn_key", "meeting_key", "meeting_squence_key","fcs_quick_vote_id" , "vote_results"));
        $this->_addCalledMethod("postSuggestData"            , array("server_dsn_key", "meeting_key", "meeting_sequence_key","data"));
        $this->_addCalledMethod("postActionCount"            , array("server_dsn_key", "meeting_key", "meeting_sequence_key","data"));

        //participant
        $this->_addCalledMethod("updateParticipant"          , array("server_dsn_key", "meeting_key", "participant", "action"));
        $this->_addCalledMethod("updateParticipantList"      , array("server_dsn_key", "meeting_key", "participant_list"));
        $this->_addCalledMethod("updateParticipantUptimeList", array("server_dsn_key", "participant_keys", "current_date"));

        $this->_addCalledMethod("getServerList", array("server_dsn_key"));
        $this->_addCalledMethod("setServerAvailability", array("server_dsn_key", "server_key", "status"));
        $this->_addCalledMethod("setServerAvailability2", array("server_dsn_key", "server_address", "status"));

        // voice echo canceler
        $this->_addCalledMethod("startEchoCancel", array("server_dsn_key", "meeting_key", "participant_id", "machine_id"));

        $this->_addCalledMethod("createTeleConf", array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("getTeleConf", array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("setTeleConf", array("server_dsn_key", "meeting_key", "type", "etc_info"));
        $this->_addCalledMethod("setTeleConfPinCd", array("server_dsn_key", "meeting_key", "pin_cd_count"));

        $this->_addCalledMethod("getPolycomData", array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("forceExitPolycomClients", array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("createVideoConferenceForMobile", array("server_dsn_key", "meeting_key", "did"));
        $this->_addCalledMethod("removeVideoConferenceForMobile", array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("setVCParticipantSessionid", array("server_dsn_key", "meeting_key", "participant_key", "session_id"));
        $this->_addCalledMethod("setVideoMute", array("server_dsn_key", "meeting_key", "participant_key", "mute"));
        $this->_addCalledMethod("callParticipant", array("server_dsn_key", "meeting_key", "target", "protocol", "name"));
        $this->_addCalledMethod("cascadePgi", array("server_dsn_key", "meeting_key", "target"));
        $this->_addCalledMethod("cancelCallingParticipant", array("server_dsn_key", "meeting_key"));
        $this->_addCalledMethod("documentSharing", array("server_dsn_key", "meeting_key", "status", "bfcpStatus"));
        $this->_addCalledMethod("changeDocumentSharing", array("server_dsn_key", "meeting_key", "documentId", "page"));
        $this->_addCalledMethod("noticeTelephonyComming", array("server_dsn_key", "meeting_key", "session_id"));
        
        $this->_addCalledMethod("removeTeleconferenceParticipant", array("server_dsn_key", "meeting_key"));
    }

    /**
     * [ debug ]
     *
     * @access public
     * @param
     * @return
     */
    function debug() {
        //debug
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__);
        return __FUNCTION__;
    }

    /**
     *
     */
    function _getMeetingKey($meeting_key) {
        if ((strlen($meeting_key) < 20) && is_numeric($meeting_key)) {
            return $meeting_key;
        } else {
            $meeting_info = $this->getMeetingKey($meeting_key);
            $this->logger2->trace(array($meeting_key, $meeting_info["meeting_key"]), "Faild getMeetingKey");
            $meeting_key = $meeting_info["meeting_key"];
            return $meeting_key;
        }
    }
    
    /**
     * 電話コネクションの切断命令
     */
    function removeTeleconferenceParticipant($server_dsn_key, $meeting_key){
    	if( !$server_dsn_key || !$meeting_key ){
    		$this->logger2->warn(array( '$server_dsn_key' => $server_dsn_key, '$meeting_key' => $meeting_key ) );
    	}
    	$dsn = $this->get_dsn($server_dsn_key);
    	require_once("classes/core/dbi/Meeting.dbi.php");
    	 
    	$dbi_meeting = new MeetingTable($dsn);
    	$meeting_info = $dbi_meeting->getRow(sprintf('meeting_key=%d', $meeting_key));
    	
    	require_once("classes/mcu/resolve/Resolver.php");
    	try {
    		if ( $videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting_info["room_key"], true) ) {
    			if ( $videoInfo = $videoConferenceClass->getActiveVideoConferenceByMeetingKey($meeting_key) ) {
    				$videoConferenceClass->removeTeleconferenceParticipant($videoInfo['conference_id']);
    			}
    		}
    	} catch(Exception $e) {
    		$this->error($meeting_info, "FlashGateway::removeTeleconferenceParticipant, ".$e->getMessage());
    		return array('status' => 0);
    	}
    	
    	return array('status' => 1);
    }

    function getMeetingKey($meeting_id) {
        if ( !$meeting_id) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("meeting_id"    => $meeting_id,));
            return false;
        }
        $this->logger2->debug($meeting_id);
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass(N2MY_MDB_DSN);
        if ($meeting_key = $obj_MGMClass->getMeetingID($meeting_id)) {
            return array(
                    'status' => 1,
                    'meeting_key' => $meeting_key,
                );
        } else {
            $this->logger2->warn($meeting_id);
            return array(
                    'status' => 0,
                    'meeting_key' => '',
                );
        }
    }

    /**
     * 再起動
     *
     * @access public
     * @param  integer $meeting_key    会議プライマリーKEY
     * @return boolean
     */
    function rebootDataCenter($server_dsn_key, $meeting_key)
    {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "meeting_key"    => $meeting_key,));
            return false;
        }
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        require_once("classes/core/Core_Meeting.class.php");
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingOptions.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_Meeting         = new DBI_Meeting($dsn);
        $obj_MeetingSequence = new DBI_MeetingSequence($dsn);
        $obj_MeetingOptions  = new DBI_MeetingOptions($dsn);
        $obj_FmsServer       = new DBI_FmsServer(N2MY_MDB_DSN);
        $obj_CoreMeeting     = new Core_Meeting($dsn, N2MY_MDB_DSN );

        // オプション取得
        // SSLオプション
        $sslOption = $obj_MeetingOptions->getRow(sprintf("meeting_key ='%s' AND option_key=1", $meeting_key ));
        $is_ssl = $sslOption ? 1 : 0;

        // 会議情報取得
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key) );
        $meetingSequenceInfo = $obj_MeetingSequence->getActiveSequenceInfo($meeting_key);
        if (!$meeting_info || !$meetingSequenceInfo) {
            $this->logger2->warn(array($meeting_key,$meeting_info,$meetingSequenceInfo));
            return false;
        }

        //User情報取得
        require_once("classes/dbi/user.dbi.php");
        $obj_User = new UserTable( $dsn );
        $user_info = $obj_User->getRow( sprintf( "user_key='%s' AND user_status = 1 AND user_delete_status = 0",
                                                 $meeting_info["user_key"] ) );

        //現在のFMS情報
        $where = "server_key = ".$meeting_info["server_key"];
        $nowFmsServer = $obj_FmsServer->getRow($where);
        //GlobalLinkOption
        $use_global_link = 0;
        if ($nowFmsServer["datacenter_key"] == 2001) {
            $use_global_link = 1;
        }

        // FMSサーバ取得
        require_once("classes/dbi/fms_deny.dbi.php");
        try {
            $objFmsDeny = new FmsDenyTable( $dsn );
            $_fms_deny_keys = $objFmsDeny->findByUserKey($meeting_info["user_key"], null, null, 0, "fms_server_key");
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            return false;
        }
        //現在利用のFMSも対象からはずす
        $fms_deny_keys = array();
        $deny_fms = $obj_FmsServer->getRowsAssoc("server_address = '".$nowFmsServer["server_address"]."' AND is_available = 1");
        if ($deny_fms) {
            foreach ($deny_fms as $fms_server) {
                $fms_deny_keys[] = $fms_server["server_key"];
            }
        }
        if ($_fms_deny_keys) {
            foreach ($_fms_deny_keys as $fms_key) {
                $fms_deny_keys[] = $fms_key["fms_server_key"];
            }
        }
        //除外DC
        require_once("classes/dbi/datacenter_ignore.dbi.php");
        $objDatacenterIgnore = new DatacenterIgnoreTable($dsn);
        $ignore_dc = $objDatacenterIgnore->get_datacenter_ignore($meeting_info["user_key"]);
        $this->logger2->debug($ignore_dc, "ignore datacenter");

        //優先DC
        require_once("classes/dbi/datacenter_priority.dbi.php");
        $objDatacenterPriority = new DatacenterPriorityTable($dsn);
        require_once("classes/dbi/datacenter_priority.dbi.php");
        require_once("classes/core/dbi/DataCenter.dbi.php");
        $objDatacenterPriority = new DatacenterPriorityTable($dsn);
        $objDatacenter = new DBI_DataCenter(N2MY_MDB_DSN);
        $priority_dc =$objDatacenterPriority->get_datacenter_priority($meeting_info["user_key"],$meeting_info["meeting_country"]);
        $priority_dc_all =$objDatacenterPriority->get_datacenter_priority($meeting_info["user_key"],"all");
        $priority_dc = array_merge($priority_dc, $priority_dc_all);
        if ($use_global_link) {
            $datacenter_list[] = 2001;
        } else {
            //所在地以外のDCリスト取得
            $fms_country_priority = $this->config['FMS_COUNTRY_PRIORITY'][$meeting_info["meeting_country"]];
            if (!$fms_country_priority) {
                $fms_country_priority = $this->config['FMS_COUNTRY_PRIORITY']["etc"];
            }

            $datacenter_country_list = split(',', $fms_country_priority);
            $datacenter_list = array();
            foreach ( $datacenter_country_list as $datacenter_country) {
                if ($meeting_info["meeting_country"] != $datacenter_country_list) {
                    $country_priority_dc_list =
                    $objDatacenterPriority->get_datacenter_priority($meeting_info["user_key"],$datacenter_country);
                }
                if ($country_priority_dc_list) {
                    if ($ignore_dc) {
                        $ignore_dc = array_merge($ignore_dc, $country_priority_dc_list);
                    }
                }
                $country_dc_list =
                    $objDatacenter->getKeyByCountry($datacenter_country, $ignore_dc, $user_info["account_model"], 0, $use_global_link);
                if ($country_dc_list) {
                    foreach( $country_dc_list as $country_dc ){
                        $datacenter_list[] = $country_dc;
                    }
                }
                $datacenter_list =
                    array_merge($country_priority_dc_list, $datacenter_list);
            }
            $datacenter_list = array_merge($priority_dc,$datacenter_list);
            $datacenter_list = array_map('serialize', $datacenter_list);
            $datacenter_list = array_unique($datacenter_list);
            $datacenter_list = array_map('unserialize', $datacenter_list);
            $this->logger2->debug($datacenter_list, "datacenter_list");
        }
        if ($datacenter_list) {
            $server_key = $obj_FmsServer->getKeyByDataCenterList($is_ssl, $fms_deny_keys, $datacenter_list, $meeting_info["need_fms_version"], $use_global_link);
            $this->logger2->debug($meeting_key);
            if ($server_key) {
                 return $obj_CoreMeeting->changeFMS($meeting_key, $server_key);
            }
        }

    $this->logger2->error(array("intra_fms"       => $meeting_info["intra_fms"],
                                        "meeting_country" => $meeting_info['meeting_country'] ,
                                         $is_ssl),
                                        "選択可能なデータセンターが存在しませんでした。");
    return false;
    }

    /**
     * [ Meeting ] 利用するデータセンター（サーバー）を切り替える
     *
     * @access public
     * @param  integer $meeting_key    会議プライマリーKEY
     * @param  integer $datacenter_key データセンタープライマリーKEY
     * @return boolean
     */
    function alterDataCenter($server_dsn_key, $meeting_key, $datacenter_key)
    {
        if ( !$server_dsn_key || !$meeting_key || $datacenter_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "meeting_key"    => $meeting_key,
                                                                     "datacenter_key" => $datacenter_key));
            return false;
        }
        // ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        require_once("classes/core/Core_Meeting.class.php");
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingOptions.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        require_once("classes/core/dbi/DataCenter.dbi.php");
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_Meeting         = new DBI_Meeting( $dsn );
//        $obj_MeetingSequence = new DBI_MeetingSequence( $dsn );
        $obj_MeetingOptions  = new DBI_MeetingOptions( $dsn );
        $obj_Datacenter      = new DBI_DataCenter( N2MY_MDB_DSN );
        $obj_FmsServer       = new DBI_FmsServer( N2MY_MDB_DSN );
        $obj_CoreMeeting     = new Core_Meeting($dsn, N2MY_MDB_DSN );
        // 会議情報取得
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key) );
        // データセンタ情報取得
        $datacenter_info = $obj_Datacenter->getRow( sprintf( "datacenter_key='%s'", $datacenter_key ) );
        // オプション取得
        // SSLオプション
        $sslOption = $obj_MeetingOptions->getRow( sprintf( "meeting_key='%s' AND option_key=1", $meeting_key ) );
        $is_ssl = $sslOption ? 1 : 0;
        require_once("classes/dbi/fms_deny.dbi.php");
        try {
            $objFmsDeny = new FmsDenyTable( $dsn );
            $_fms_deny_keys = $objFmsDeny->findByUserKey($meeting_info["user_key"], null, null, 0, "fms_server_key");
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            return false;
        }
        $fms_deny_keys = array();
        if ($_fms_deny_keys) {
            foreach ($_fms_deny_keys as $fms_key) {
                $fms_deny_keys[] = $fms_key["fms_server_key"];
            }
        }
        // FMSサーバ取得
        $server_key = $obj_FmsServer->getKeyByDatacenterKey($datacenter_info["datacenter_key"], $datacenter_info["datacenter_country"], $is_ssl, false, $fms_deny_keys);
        return $obj_CoreMeeting->changeFMS($meeting_key, $server_key);
    }

    /**
     * データセンター切り替え（やったところまでで終了）
     *
     * ミーティングキーだけで自動的に切り替えたほうがよさそう
     */
    function changeDataCenter($server_dsn_key, $meeting_key, $meeting_sequence_key, $has_recorded_minutes, $has_recorded_video, $eventlog, $moveFlg=false, $file_list=false, $rec_time = array())
    {
        $this->logger2->debug(array($server_dsn_key, $meeting_sequence_key, $has_recorded_minutes, $has_recorded_video, $eventlog));
        if ( !$server_dsn_key || !$meeting_sequence_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "meeting_sequence_key"    => $meeting_sequence_key));
            return false;
        }
        require_once("classes/core/Core_Meeting.class.php");
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");

        $dsn = $this->get_dsn($server_dsn_key);
        $obj_CoreMeeting = new Core_Meeting($dsn, N2MY_MDB_DSN);
        // イベント追加
        $obj_Meeting         = new DBI_Meeting($dsn);
        $obj_MeetingSequence = new DBI_MeetingSequence($dsn);

        $where = "meeting_sequence_key = ".$meeting_sequence_key;
        $meeting_key = $obj_MeetingSequence->getOne($where, "meeting_key");
        $this->eventlog($dsn, $meeting_key, $eventlog);
        $result = $obj_CoreMeeting->changeDataCenter($meeting_sequence_key, $has_recorded_minutes, $has_recorded_video, $rec_time);

        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key) );

        //
        require_once("classes/mcu/resolve/Resolver.php");
        try {
          if (($controller = Resolver::getController($meeting_info["room_key"]))) {
            if ($videoInfo = $controller->getActiveVideoConferenceByMeetingKey($meeting_key)) {
              $controller->changeDataCenter($meeting_info);
            }
          }
        }
        catch(Exception $e) {
          $this->error("FlashGateway::changeDataCenter".$e->getMessage());
        }
        return $result;
    }

    function getMeetingInfo($room_key) {
        if ( !$room_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("room_key" => $room_key));
            return false;
        }
        require_once("classes/mgm/MGM_Auth.class.php");
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        //
        $obj_MGMAuthClass = new MGM_AuthClass(N2MY_MDB_DSN);
        $dsn_info = $obj_MGMAuthClass->getRelationDsn($room_key, "mfp");
        if (DB::isError($dsn_info)) {
            $this->logger2->error($dsn_info->getUserInfo());
        }
        $this->logger2->debug($dsn_info);
        $objMeeting = new DBI_Meeting($dsn_info["dsn"]);
        $sql = "SELECT meeting.server_key".
            ", meeting.meeting_key".
            " FROM meeting".
            ", participant".
            " WHERE meeting.room_key = '".addslashes($room_key)."'".
            " AND meeting.meeting_key = participant.meeting_key".
            " AND meeting.is_active = 1".
            " AND meeting.is_deleted = 0".
            " AND participant.is_active = 1";
        $rs = $objMeeting->_conn->query($sql);
        $ret["status"] = 0;
        if (DB::isError($rs)) {
            $ret["status"] = -1;
            $this->logger2->warn($rs->getUserInfo());
        } else {
            $meeting_sequence_key = 0;
            if ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                $this->logger2->debug($row);
                // FCS取得
                $obj_FMSServer = new DBI_FmsServer( N2MY_MDB_DSN );
                $where = "server_key = ".$row["server_key"];
                $server_address = $obj_FMSServer->getOne($where, "server_address");
                // meeting_sequence_key取得
                $where = "meeting_key = ".$row["meeting_key"];
                $objMeetingSequence = new DBI_MeetingSequence($dsn_info["dsn"]);
                $meeting_sequence_key = $objMeetingSequence->getOne($where, "meeting_sequence_key", array("meeting_sequence_key" => "desc"));
                $ret["fcs_address"] = $server_address;
                $ret["core_session_key"] = $row["meeting_key"];
                $ret["meeting_sequence_key"] = $meeting_sequence_key;
                $ret["fcs_application_name"] = $this->config["CORE"]["app_name"];
                $meeting_sequence_key = $row["meeting_sequence_key"];
            }
            $ret["status"] = 1;
        }
        $this->logger2->debug($ret);
        return $ret;
    }

    /**
     * Meeting情報取得
     */
    function createConferenceData($meeting_id) {
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "meeting_id"=>$meeting_id));
        $error_result = array('status' => false);
        if (!$meeting_id) {
            $this->logger2->error($meeting_id);
            return $error_result;
        }
        $meeting_key_info = $this->getMeetingKey($meeting_id);
        $meeting_key = $meeting_key_info["meeting_key"];

        $obj_MGMAuthClass = new MGM_AuthClass(N2MY_MDB_DSN);
        $dsn_info = $obj_MGMAuthClass->getRelationDsn($meeting_id, "meeting");
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        $obj_Meeting         = new DBI_Meeting($dsn_info["dsn"]);
        $obj_MeetingSequence = new DBI_MeetingSequence( $dsn_info["dsn"] );

        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        $obj_FmsServer       = new DBI_FmsServer(N2MY_MDB_DSN);
        $meeting_info = $obj_Meeting->getRow(sprintf("meeting_key='%s' AND  is_deleted = 0" ,
                                                     mysql_real_escape_string($meeting_key)));
        $this->logger2->debug($meeting_info);
        if (!$meeting_info){
            $this->logger2->error("meeting_info not found". $meeting_key);
            return $error_result;
        }
        $meeting_sequence_info    = $obj_MeetingSequence->getRow(sprintf( "meeting_key='%s'", $meeting_key ), null, array( "meeting_sequence_key" => "desc"));
        // オプション情報
        require_once('classes/core/dbi/Options.dbi.php');
        $obj_Options         = new DBI_Options( N2MY_MDB_DSN );
        require_once("classes/core/dbi/MeetingOptions.dbi.php");
        $obj_MeetingOptions  = new DBI_MeetingOptions($dsn_info["dsn"]);
        $option_list = $obj_Options->getList();
        $where = "meeting_key = ".$meeting_key;
        $meeting_option_list = $obj_MeetingOptions->getRowsAssoc($where);
        foreach($meeting_option_list as $key => $row) {
            $option_name = $option_list[$row["option_key"]]["option_name"];
            $meeting_option[$option_name] = $row["meeting_option_quantity"];
        }

        $where = "server_key = ".$meeting_info["server_key"];
        $fms_info = $obj_FmsServer->getRow($where);

        if ($fms_info["datacenter_key"] != "2001") {
            $global_link_info = array("master_server_name" => "",
                                      "master_protocol_port" => "",
                                      "server_instance_directory" => "",
                                      "server_token"         => null,
                                      "fms_server_list" => "");
        } else {
            // SSLオプション
            $sslOption = $obj_MeetingOptions->getRow(sprintf("meeting_key ='%s' AND option_key=1", $meeting_key ));
            $is_ssl = $sslOption ? 1 : 0;

            // FMSサーバ取得
            require_once("classes/dbi/fms_deny.dbi.php");
            try {
                $objFmsDeny = new FmsDenyTable( $dsn_info["dsn"] );
                $_fms_deny_keys = $objFmsDeny->findByUserKey($meeting_info["user_key"], null, null, 0, "fms_server_key");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $error_result;
            }
            $fms_deny_keys = array();
            if ($_fms_deny_keys) {
                foreach ($_fms_deny_keys as $fms_key) {
                    $fms_deny_keys[] = $fms_key["fms_server_key"];
                }
            }
            $sort = array(
                        "server_key"=>"ASC",
                        "maintenance_time"=>"ASC"
                        );
            $where = sprintf("datacenter_key = 2001 AND is_available = 1 AND is_ssl='%s'", $is_ssl);
            if ( $meeting_info["need_fms_version"] ) {
                $where .= " AND server_version >= '".mysql_real_escape_string($meeting_info["need_fms_version"])."'";
            }
            if ($fms_deny_keys) {
                $fms_deny_list_comma_separated = implode(",", $fms_deny_keys);
                $where .= " AND NOT server_key IN (". $fms_deny_list_comma_separated .")";
            }
            $fmsServerList = $obj_FmsServer->getRowsAssoc($where, $sort, null, 0, "server_address,local_address,maintenance_time,protocol_port");
            foreach ($fmsServerList as $server) {
                $front_fms["server_address"] = $server["local_address"] ? $server["local_address"] : $server["server_address"];
                $front_fms["protocol_port"] = $server["protocol_port"];
                $front_fms_list[] = $front_fms;
            }
            $global_link_info = array(
                                    "master_server_name" => $fms_info["local_address"],
                                    "master_protocol_port" => $fms_info["protocol_port"],
                                    "server_instance_directory" => $meeting_info["fms_path"],
                                    "server_token"         => null,
                                    "fms_server_list" => $front_fms_list);

        }
        require_once('classes/dbi/room.dbi.php');
        $obj_Room = new RoomTable( $dsn_info["dsn"] );
        $room_info = $obj_Room->getRow( sprintf( "room_key='%s' AND room_status = 1", $meeting_info["room_key"] ));

        require_once('classes/dbi/reservation.dbi.php');
        $objReservation = new ReservationTable($dsn_info["dsn"]);
        $reservationInfo = $objReservation->getRow(sprintf("meeting_key='%s'", $meeting_info["meeting_ticket"]), "reservation_key, reservation_pw, reservation_pw_type, is_authority_flg, is_rec_flg, is_auto_rec_flg");
        if ($reservationInfo["reservation_pw"] && $reservationInfo["reservation_pw_type"] == "1") {
            $reservation_pw_flg = "true";
        }
        //ポリコム連携
        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting_info["room_key"]))) {
          $ivesSettingInfo = $videoConferenceClass->getIvesInfoByRoomKey($meeting_info["room_key"]);
          if ($ivesSettingInfo && !$ivesSettingInfo["ignore_video_conference_address"] && !$reservation_pw_flg) {
            $video_conference_address = "(SIP,H323)".$videoConferenceClass->getRegularAddress($ivesSettingInfo["ives_did"]);
          } else {
            $this->logger2->warn($meeting_info, "set video_conference_address error");
          }
        }

        //sharing
        $shringSring = "";
        //ignore or intra利用時は利用不可能
        $shringSring .= $this->config["IGNORE_MENU"]["tool_sharing2_app"] || 1 == $meeting_info["intra_fms"] ? 0 : 1;
        $shringSring .= $this->config["IGNORE_MENU"]["tool_sharing3_app"] ? 0 : 1;

        if( $meeting_info["user_key"] ){
            require_once("classes/dbi/user.dbi.php");
            $obj_User = new UserTable( $dsn_info["dsn"] );
            $user_info = $obj_User->getRow( sprintf( "user_key='%s' AND user_status != 0 AND user_delete_status != 2",
                                                     $meeting_info["user_key"] ) );
        }

        // フリーのユーザーか確認
        $needUserLogin = "false";
        if ($user_info["account_model"] == "free") {
            $needUserLogin = "true";
        }

        //部屋の帯域
        $extension = array();
        $total_seat = $meeting_info["meeting_max_seat"] + $meeting_info["meeting_max_audience"];
        $bandwidth_level = ceil($total_seat / 10);
        $maxRoomBandwidth = $bandwidth_level * 2048;
        //帯域
        if ($maxRoomBandwidth < $room_info["max_room_bandwidth"]) {
            $extension["maxRoomBandwidth"] = $room_info["max_room_bandwidth"];
        } else {
            $extension["maxRoomBandwidth"] = $maxRoomBandwidth;
        }
        $extension["maxUserBandwidth"] = $room_info["max_user_bandwidth"] ? $room_info["max_user_bandwidth"] : 256;
        //高画質
        if ($meeting_option["hispec"] > 0) {
            $hispecBandWidth = $meeting_option["hispec"] * 2048;
            $extension["maxHighQualityRoomBandwidth"] = $extension["maxRoomBandwidth"] + $hispecBandWidth;
            if ($room_info["max_user_bandwidth"] > 512) {
                $extension["maxHighQualityUserBandwidth"] = $room_info["max_user_bandwidth"];
            } else {
                $extension["maxHighQualityUserBandwidth"] = 512;
            }
        } else {
            $extension["maxHighQualityRoomBandwidth"] = $room_info["max_room_bandwidth"] ? $room_info["max_room_bandwidth"] : 2048;
            $extension["maxHighQualityUserBandwidth"] = $room_info["max_user_bandwidth"] ? $room_info["max_user_bandwidth"] : 256;
        }
        if ($meeting_option["minimumBandwidth80"]) {
            $extension["minUserBandwidth"] = 80;
        } else {
            $extension["minUserBandwidth"] = $room_info["min_user_bandwidth"] ? $room_info["min_user_bandwidth"] : 30;
        }
        $mcu_profile_type = "";
        if ($user_info["meeting_version"] >= "4.7.0.0" && $this->config["N2MY"]["mcu_profile_type"]) {
            $mcu_profile_type = $this->config["N2MY"]["mcu_profile_type"];

        }
        //自動録画
        if($reservationInfo){
            if($reservationInfo["is_rec_flg"] == 1 && $reservationInfo["is_auto_rec_flg"] == 1)
                $room_info["default_auto_rec_use_flg"] = 1;
            else
                $room_info["default_auto_rec_use_flg"] = 0;
        }
        //PGi情報取得
        require_once "classes/dbi/pgi_setting.dbi.php";
        $pgiSettingTable = new PGiSettingTable($dsn_info["dsn"]);
        $pgi_setting = $pgiSettingTable->findEnablleAtYM($meeting_info['room_key'],
                                                         date('Y-m',strtotime($meeting_info['meeting_start_datetime'] )));
        if ($room_info["is_auto_transceiver"]) {
            if (!$room_info["transceiver_number"]) $room_info["transceiver_number"] = 11;
        } else {
            $room_info["transceiver_number"] = 100;
        }
        $active_speaker_mode_flg = false;
        if($meeting_info['use_sales_option'] == 0 && ( $room_info["active_speaker_mode_use_flg"] == 1 || $room_info["active_speaker_mode_only_flg"] ) ) {
            $active_speaker_mode_flg = true;
        }
        $room_config = array(
            "roomKey" => $meeting_info["room_key"],
            "meetingSequenceKey" => (int)$meeting_sequence_info["meeting_sequence_key"],
            "maxSeatLength" => (int)$meeting_info["meeting_max_seat"],
            "maxAudienceLength" => (int)$meeting_info["meeting_max_audience"],
            "maxStaffLength" => (int)$meeting_info["meeting_max_staff"] ? (int)$meeting_info["meeting_max_staff"] : 1,
            "maxCustomerLength" => (int)$meeting_info["meeting_max_customer"] ? (int)$meeting_info["meeting_max_customer"] : 1,
            "maxSSWatcherLength" => (int)$room_info["max_ss_watcher_seat"],
            "enableMaxGuest" => (int)$room_info["max_guest_seat_flg"],
            "maxGuestLength" => (int)$room_info["max_guest_seat"],
            "maxInvisibleWBLength" => (int)$meeting_info["meeting_max_whiteboard"],
            "translationKey" => "",
            "h323Length" => $meeting_option["h323"] ? $meeting_option["h323"] : 0,
            "enableSsl" => $meeting_option["ssl"] ? true : false,
            "dataCenter" => (int)$fms_info["datacenter_key"],
            "server" => $fms_info["server_address"],
            "enableRoomLock" => false,
            "enableMailUpload" => $this->config["N2MY"]["mail_wb_upload"] ? true : false,
            "enableMultipleCamera" => $meeting_option["multicamera"] ? true : false,
            "teleconfServiceName" => "",
            "enableVideoconf" => $meeting_option["video_conference"] ? true : false,
            "videoconfAddress" => !$reservation_pw_flg ? $video_conference_address : "",
            "pinCd" => $meeting_info["pin_cd"],
            "mailUploadAddress" => ($this->config["N2MY"]["mail_wb_upload"]) ? $meeting_info["room_key"]."@".$this->config["N2MY"]["mail_wb_host"] : "",
            "isSharingService" => $meeting_option["sharing"] ? true : false,
            "availableSharingVersion" => "0x0".$shringSring."00000",
            "hasReservationPassword" => $reservation_pw_flg ? true : false,
            "reservationPassword" => $reservation_pw_flg ? $reservationInfo["reservation_pw"] : "",
            "needUserLogin" => $needUserLogin ? true : false,
            "maxRoomBandwidth" => $extension["maxHighQualityRoomBandwidth"] ? (int)$extension["maxHighQualityRoomBandwidth"] : 2048,
            "maxHighQualityRoomBandwidth" => $extension["maxHighQualityRoomBandwidth"] ? (int)$extension["maxHighQualityRoomBandwidth"] : 4096,
            "maxUserBandwidth" => $extension["maxUserBandwidth"] ? (int)$extension["maxUserBandwidth"] : 256,
            "minUserBandwidth" => $extension["minUserBandwidth"] ? (int)$extension["minUserBandwidth"] : 30,
            "maxHighQualityUserBandwidth" => $extension["maxHighQualityUserBandwidth"] ? (int)$extension["maxHighQualityUserBandwidth"] : 512,
            "maxFramerate" => 15,
            "minFramerate" => 3,
            "framerate" => 15,
            "bandwidthRate" => 1,
            "narrowbandRate" => 0.5,
            "cpuReduceRate" => 0.5,
            "noticeRemainMinuteTime" => 10,
            "meetingStatus" => "available",
            "isTransceiverMode" => false,
            "toTransceiverModeCount" => $room_info["transceiver_number"] ? (int)$room_info["transceiver_number"] : 11,
            "maxTransceiverSpeakerCount" => 5,
            "transceiverModeBandwidth" => 20,
            "isHighQuality" => ($meeting_option["hispec"] || $meeting_option["h264"]) ? true : false,
            "enableUnrecordMinutes" => $user_info["is_minutes_delete"] ? true : false,
            "showWBPage" => $room_info["is_wb_no"] ? true : false,
            "defaultLayout" => $room_info["default_layout"],
            "useAGC" => $room_info["default_agc_use_flg"] ? true : false,
            "minFramerateForCpuReduction" => 1,
            "availableUploadVector" => $this->config["IGNORE_MENU"]["document_vector"] ? false : true,
            "isDraggableFinger" => true,
            "isVideoSharingService" => true,
            "maxSharedMemoSize" => (int)$room_info["max_shared_memo_size"],
            "enableTranscoder" => $this->config["N2MY"]["enable_h264_transcoder"]? true : false,
            "useH264" => (1 == $room_info["default_h264_use_flg"] && $meeting_option["h264"]) ? true : false,
            "useSTB" => $room_info["use_stb_option"] ? true : false,
            "h264Available" => $meeting_option["h264"] ? true : false,
            "isFiexedUseH264" => $room_info["default_h264_use_flg"] ? false : true,
            "isParticipantNotifier" => $room_info["is_participant_notifier"] ? true : false,
            "isPersonalWB" => $room_info["is_personal_wb"] ? true : false,
            "mcuProfileType" => $mcu_profile_type,
            "useAutoRec" => $room_info["default_auto_rec_use_flg"] ? true : false,
            "roomName" => $meeting_info["meeting_room_name"] ? $meeting_info["meeting_room_name"] : "-----",
            "sessionName" => $meeting_info["meeting_name"] ? $meeting_info["meeting_name"] : "-----",
            "reservedAuthority" => $reservationInfo["is_authority_flg"] ? true : false ,
            "isActiveSpeakerMode" => $active_speaker_mode_flg,
            "maxActiveSpeakerCount" => $room_info["active_speaker_mode_user_count"] ? $room_info["active_speaker_mode_user_count"] : 3,
            "maxSpeakerCount" => $room_info["active_speaker_mode_speaker_count"] ? $room_info["active_speaker_mode_speaker_count"] : 2,
        );
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $obj_OrderedOption = new OrderedServiceOptionTable($dsn_info["dsn"]);
        $where_option = "room_key = '".$room_info["room_key"]."'".
                        " AND service_option_key = 23".
                        " AND ordered_service_option_status = 1";
        $teleconf_option = $obj_OrderedOption->getRow($where_option);
        $room_config["useTeleconf"] =  false;
        if ($teleconf_option && $room_info["use_teleconf"]) {
            $room_config["useTeleconf"] = ($meeting_info["is_reserved"] && $meeting_info["tc_type"] == "voip") ? false : true;
        }
        require_once "classes/pgi/PGiSystem.class.php";
        $system  = PGiSystem::findBySystemKey($pgi_setting['system_key']);
        $this->logger2->debug($system);
        $debug_config = array("enable_chat_command" => $this->config["N2MY"]["fms_chat_commnad"] ? true : false,
                              "log_level" => $this->config["N2MY"]["fms_log_level"] ? $this->config["N2MY"]["fms_log_level"] : "info");
        $result = array("status" => true,
                        "meeting_key" => (int)$meeting_key,
                        "meeting_version" => $meeting_info["meeting_version"],
                        "server_dsn_key" => $dsn_info["host_name"],
                        "global_link_info" => $global_link_info,
                        "room_config" => $room_config,
                        "debug_config" => $debug_config,
                        "pgi_info" => array(
                                "web_id" => (string)$system->webID,
                                "web_pw" => (string)$system->webPW,
                                "admin_client_id" => $pgi_setting["admin_client_id"] ? $pgi_setting["admin_client_id"] : (string)$system->adminClientID,
                                "admin_client_pw" => $pgi_setting["admin_client_pw"] ? $pgi_setting["admin_client_pw"] : (string)$system->adminClientPW,
                                "pia_endpoint_host" => (string)$system->piaEndpointHost,
                                "pia_endpoint_path" => (string)$system->piaEndpointPath,
                                "pia_rest_endpoint_path" => (string)$system->piaRestEndpointPath,
                                "pia_endpoint_port" => (int)$system->piaEndpointPort,
                                "service_name" => (string)$system->serviceName,
                                "pia_proxy_host" => (string)$system->piaProxyHost,
                                "pia_proxy_port" => (int)$system->piaProxyPort,
                                ),
                        );
        return $result;
    }


    /**
     * [ Meeting ] 会議室名を変更する
     *
     * @access public
     * @param  integer $meeting_key  会議プライマリーKEY
     * @param  string  $meeting_name 会議名
     * @return boolean
     */
    function alterMeetingName($server_dsn_key, $meeting_key, $meeting_name) {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "meeting_key"    => $meeting_key,));
            return false;
        }// ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "server_dsn_key"=>$server_dsn_key,
                                "meeting_key"=>$meeting_key,
                                "meeting_name"=>$meeting_name));
        //
        if ( !$server_dsn_key || !$meeting_key || !$meeting_name ) {
            //debug
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                'meeting_key' => $meeting_key,
                'meeting_name' => $meeting_name,
                ));
            return false;
        }
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting = new DBI_Meeting( $dsn );
        $data = array(
            "meeting_name"    => $meeting_name,
            "update_datetime" => date("Y-m-d H:i:s"),
        );
        $where = "meeting_key = ".$meeting_key;
        $result = $obj_Meeting->update($data, $where);
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result->getUserInfo());
            return false;
        }
        $meeting_info = $obj_Meeting->getRow($where = "meeting_key = ".$meeting_key);
        if($meeting_info["is_reserved"]) {
        //予約情報にも更新する
          require_once ("classes/dbi/reservation.dbi.php");
          $obj_reservation = new ReservationTable( $dsn );
          $where = "meeting_key = '".$meeting_info["meeting_ticket"]."'";
          $data = array(
              "reservation_name"    => $meeting_name,
              "reservation_updatetime" => date("Y-m-d H:i:s"),
          );
          $result = $obj_reservation->update($data, $where);
          if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result->getUserInfo());
            return false;
          }
        }
        return true;
    }

    /**
     * [ Meeting ] 会議ログにパスワードを設定する
     *
     * @access public
     * @param  integer $meeting_key          会議プライマリーKEY
     * @param  string  $meeting_log_owner    会議ログオーナー名
     * @param  string  $meeting_log_password 会議ログパスワード
     * @return boolean
     */
    function setLogPassword($server_dsn_key, $meeting_key, $meeting_log_owner, $meeting_log_password) {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "meeting_key"    => $meeting_key,));
            return false;
        }
        // ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "server_dsn_key"=>$server_dsn_key,
                                "meeting_key"=>$meeting_key,
                                "meeting_log_owner"=>$meeting_log_owner,
                                "meeting_log_password"=>$meeting_log_password));
        if ( !$server_dsn_key || !$meeting_key || !$meeting_log_password) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                "meeting_log_password" => $meeting_log_password,
                ));
            return false;
        }
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting = new DBI_Meeting( $dsn );
        $where = "meeting_key = ".addslashes($meeting_key);
        $data = array(
            "meeting_log_owner" => $meeting_log_owner,
            "meeting_log_password" => $meeting_log_password,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $ret = $obj_Meeting->update($data, $where);
        if ($ret) {
            $this->logger->error(__FUNCTION__."#result",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * [ Meeting ] 会議ログ再生のためのパスワード認証を行う
     *
     * @access public
     * @param  integer $meeting_key          会議プライマリーKEY
     * @param  string  $meeting_log_password 会議ログパスワード
     * @return boolean
     */
    function authMeetingLog($server_dsn_key, $meeting_key, $meeting_log_password) {
        if ( !$server_dsn_key || !$meeting_key || !$meeting_log_password) {
            //debug
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                "meeting_log_password" => $meeting_log_password,
                ));
            return false;
        }
        // ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "server_dsn_key"=>$server_dsn_key,
                                "meeting_key"=>$meeting_key,
                                "meeting_log_password"=>$meeting_log_password));
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting = new DBI_Meeting( $dsn );
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key) );
        if ($meeting_info["meeting_log_password"] !== $meeting_log_password) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,$meeting_log_password);
            return false;
        }
        return true;
    }

    /**
     * [ Meeting ] 会議ログのパスワードを解除する
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return integer
     */
    function unsetLogPassword($server_dsn_key, $meeting_key) {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                ));
            return false;
        }
        // ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "server_dsn_key"=>$server_dsn_key,
                                "meeting_key"=>$meeting_key));
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting = new DBI_Meeting( $dsn );
        $where = "meeting_key = ".addslashes($meeting_key);
        $data = array(
            "meeting_log_password" => "",
            "update_datetime"      => date("Y-m-d H:i:s")
            );
        $ret = $obj_Meeting->update($data, $where);
        if ($ret) {
            $this->logger->error(__FUNCTION__."#result",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * [ Meeting ] 会議にロックを設定する
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return boolean true:成功 false:失敗
     */
    function lockMeeting($server_dsn_key, $meeting_key) {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                ));
            return false;
        }
        // ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "server_dsn_key"=>$server_dsn_key,
                                "meeting_key"=>$meeting_key));
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting = new DBI_Meeting( $dsn );
        if (!$obj_Meeting->blockUser($meeting_key)) {
            return false;
        }
        return true;
    }

    /**
     * [ Meeting ] 会議のロックを解除する
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return boolean true:成功 false:失敗
     */
    function unlockMeeting($server_dsn_key, $meeting_key) {
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "server_dsn_key"=>$server_dsn_key,
                                "meeting_key"=>$meeting_key));
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                ));
            return false;
        }
        // ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting = new DBI_Meeting( $dsn );
        if (!$obj_Meeting->unblockUser($meeting_key)) {
            return false;
        }
        return true;
    }

    /**
     * [ Meeting ] 会議の議事録データフラグをオフにする
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return boolean
     */
    function deleteRecordedMinutes($server_dsn_key, $meeting_key, $meeting_sequence_key = null) {
        if ( !$server_dsn_key || !ctype_alnum($meeting_key) || !is_numeric($meeting_sequence_key)) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                ));
            return false;
        }
        // ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "server_dsn_key"=>$server_dsn_key,
                                "meeting_key"=>$meeting_key,
                                "meeting_sequence_key"=>$meeting_sequence_key));
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/Core_Meeting.class.php");
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        require_once("classes/core/dbi/Document.dbi.php");
        require_once("classes/N2MY_Document.class.php");
        require_once("classes/N2MY_MeetingLog.class.php");

        $obj_Meeting         = new DBI_Meeting( $dsn );
        $obj_MeetingSequence = new DBI_MeetingSequence( $dsn );
        $obj_Document        = new DBI_Document( $dsn );
        $obj_CoreMeeting     = new Core_Meeting( $dsn, N2MY_MDB_DSN );
        $obj_meetingLog      = new N2MY_MeetingLog($dsn);

        $meeting_info = $obj_Meeting->getRow(sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key));
        if ($meeting_info["is_locked"] == "1") {
            $this->logger->error(__FUNCTION__."#ロックがかかっているため削除できない", __FILE__, __LINE__, array($meeting_info));
            return false;
        }
        if ($meeting_info) {
            // 指定シーケンスの会議データを削除
            if ( ! $obj_CoreMeeting->deleteMinute( $meeting_sequence_key ) ) {
                $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$meeting_sequence_key);
                return false;
            }
            $meeting_sequence_data = $obj_MeetingSequence->getStatus($meeting_key);
            // 会議が終了していればファイルを物理削除
            if (($meeting_sequence_data['has_recorded_minutes'] == 0) && ($meeting_sequence_data['has_recorded_video'] == 0) && ($meeting_sequence_data['has_shared_memo'] == 0)) {
                // 通常
                $objDocument = new N2MY_Document($dsn);
                $where = "meeting_key = ". $meeting_key;
                $document_list = $objDocument->getList($where);
                $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$document_list);
                foreach($document_list as $document) {
                    // 物理削除
                    $objDocument->delete($document);
                }
                // ファイルキャビネットの削除
                // 容量を空にする
                $meeting_sequence_data["meeting_size_used"] = 0;
            } else {
                // 資料の容量も計算
                $meeting_sequence_data["meeting_size_used"] += $obj_CoreMeeting->getDocumentSize($meeting_key);
            }
            $where = "meeting_key = ".$meeting_key;
            $obj_Meeting->update($meeting_sequence_data, $where);
            $obj_meetingLog -> getMeetingSize($meeting_info["meeting_session_id"]);
            // 操作ログ情報
            require_once "classes/dbi/operation_log.dbi.php";
            $objOperationLog    = new OperationLogTable($dsn);
            $opration_log = array(
                "user_key"              => $meeting_info["user_key"],
                "remote_addr"           => $_SERVER["REMOTE_ADDR"],
                "action_name"           => 'meetinglog_delete_minutes',
                "operation_datetime"    => date("Y-m-d H:i:s"),
                "info"                  => serialize(array(
                    "room_key"          => $meeting_info["room_key"],
                    "meeting_name"      => $meeting_info["meeting_name"],
                    ))
                );
            $objOperationLog->add($opration_log);
        }
        return true;
    }

    /**
     * [ Meeting ] 会議の録画データフラグをオフにする
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return boolean
     */
    function deleteRecordedVideo($server_dsn_key, $meeting_key, $meeting_sequence_key=null) {
        if ( !$server_dsn_key || !ctype_alnum($meeting_key) || !is_numeric($meeting_sequence_key)) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                ));
            return false;
        }
        // ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "server_dsn_key"=>$server_dsn_key,
                                "meeting_key"=>$meeting_key,
                                "meeting_sequence_key"=>$meeting_sequence_key));
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/Core_Meeting.class.php");
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        require_once("classes/core/dbi/Document.dbi.php");
        require_once("classes/N2MY_Document.class.php");
        require_once("classes/N2MY_MeetingLog.class.php");

        $obj_Meeting         = new DBI_Meeting( $dsn );
        $obj_MeetingSequence = new DBI_MeetingSequence( $dsn );
        $obj_Document        = new DBI_Document( $dsn );
        $obj_CoreMeeting     = new Core_Meeting( $dsn, N2MY_MDB_DSN );
        $obj_meetingLog      = new N2MY_MeetingLog($dsn);

        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key) );
        if ($meeting_info["is_locked"] == "1") {
            $this->logger->error(__FUNCTION__."#ロックがかかっているため削除できない", __FILE__, __LINE__, array($meeting_info));
            return false;
        }
        if ($meeting_info) {
            // 指定シーケンスの会議データを削除
            if ( ! $obj_CoreMeeting->deleteVideo( $meeting_sequence_key ) ) {
                $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$meeting_sequence_key);
                return false;
            }
            $meeting_sequence_data = $obj_MeetingSequence->getStatus($meeting_key);
            // 会議が終了していればファイルを物理削除
            if (($meeting_sequence_data['has_recorded_minutes'] == 0) && ($meeting_sequence_data['has_recorded_video'] == 0) && ($meeting_sequence_data['has_shared_memo'] == 0)) {
                // 通常
                $objDocument = new N2MY_Document($dsn);
                $where = "meeting_key = ". $meeting_key;
                $document_list = $objDocument->getList($where);
                $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$document_list);
                foreach($document_list as $document) {
                    // 物理削除
                    $objDocument->delete($document);
                }
                // ファイルキャビネットの削除
                // 容量を空にする
                $meeting_sequence_data["meeting_size_used"] = 0;
            } else {
                // 資料の容量も計算
                $meeting_sequence_data["meeting_size_used"] += $obj_CoreMeeting->getDocumentSize($meeting_key);
            }
            $where = "meeting_key = ".$meeting_key;
            $obj_Meeting->update($meeting_sequence_data, $where);
            $obj_meetingLog -> getMeetingSize($meeting_info["meeting_session_id"]);
        }
        // 操作ログ情報
        require_once "classes/dbi/operation_log.dbi.php";
        $objOperationLog    = new OperationLogTable($dsn);
        $opration_log = array(
            "user_key"              => $meeting_info["user_key"],
            "remote_addr"           => $_SERVER["REMOTE_ADDR"],
            "action_name"           => 'meetinglog_delete_video',
            "operation_datetime"    => date("Y-m-d H:i:s"),
            "info"                  => serialize(array(
                "room_key"          => $meeting_info["room_key"],
                "meeting_name"      => $meeting_info["meeting_name"],
                ))
            );
        $objOperationLog->add($opration_log);
        return true;
    }

    /**
     * [ Meeting ] 会議を延長する
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @param  time    $extra_time  延長時間（UNIXタイムスタンプ）
     * @return boolean
     */
    function extendMeeting($server_dsn_key, $meeting_key, $extra_time)
    {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                ));
            return false;
        }
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        //debug
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
            "server_dsn_key"=>$server_dsn_key,
            'meeting_key' => $meeting_key,
            'extra_time' => $extra_time,
            ));
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_Meeting   = new DBI_Meeting( $dsn );

        //check parameter
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key) );

        //initialize
        $extendable_time = null;

        //extend meeting
        $result  = false;
        if ($meeting_info) {
            if ($extra_time <= $meeting_info['meeting_max_extendable']) {
                //get extendable_time
                $extendable_time = $obj_Meeting->amf_getExtendableTime($meeting_key);

                //extend meeting_time
                if (is_null($extendable_time) || ($extendable_time >= $extra_time)) {
                    $result = $obj_Meeting->amf_extendMeeting($meeting_key, $extra_time);
                }
            }
        }

        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
            'extendable_time' => $extendable_time,
            'result' => $result,
            ));
        return $result;
    }

    /**
     * [ Meeting ] 会議がリアルタイムに延長して実施されているか問い合わせる
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return boolean
     */
    function getIsExtended($server_dsn_key, $meeting_key) {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                ));
            return false;
        }
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        //debug
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
            "server_dsn_key"=>$server_dsn_key,
            'meeting_key' => $meeting_key,
            ));
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_Meeting   = new DBI_Meeting( $dsn );

        //check parameter
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key) );

        //get extending status
        $result  = false;
        if ($meeting_info) {
            $result = $obj_Meeting->amf_getIsExtended($meeting_key);
        }

        //---------------------------------------------------------------------
        $this->logger->debug(__FUNCTION__."#result",__FILE__,__LINE__,$result);
        //return __FUNCTION__;
        return $result;
    }

    /**
     * [ Meeting ] 会議の延長可能時間を取得する
     *
     * <pre>
     * 後続の会議予約がなく無制限に延長が可能な場合は[ NULL ]を返す。
     * </pre>
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return time | null
     */
    function getExtendableTime($server_dsn_key, $meeting_key) {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                ));
            return false;
        }
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        //debug
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
            "server_dsn_key"=>$server_dsn_key,
            'meeting_key' => $meeting_key,
            ));
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_Meeting   = new DBI_Meeting( $dsn );

        //check parameter
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key) );

        //get extendable_time
        $result  = 0;
        if ($meeting_info) {
            $result = $obj_Meeting->amf_getExtendableTime($meeting_key);
        }
        $this->logger->debug(__FUNCTION__."#result",__FILE__,__LINE__,$result);
        //return __FUNCTION__;
        return $result;
    }

    /**
     * [ Meeting ] タイプ別で会議の情報を取得する
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return array
     */
    function getStatus( $server_dsn_key, $meeting_key, $types, $meeting_sequence_key = null, $calling_participant_key=null) {
        if (!$types || !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("types"          => $types,
                                                                     "server_dsn_key" => $server_dsn_key,
                                                                     "meeting_key"    => $meeting_key,));
            return false;
        }
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,array("types"          => $types,
                                                                 "server_dsn_key"       => $server_dsn_key,
                                                                  "meeting_key"          => $meeting_key,
                                                                  "meeting_sequence_key" => $meeting_sequence_key));
        $dsn = $this->get_dsn( $server_dsn_key );
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting         = new DBI_Meeting( $dsn );
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key) );
        $result = array();

        require_once("classes/dbi/user.dbi.php");
        $user_table = new UserTable( $dsn );
        $user = $user_table->find($meeting_info['user_key']);

        foreach ($types as $type) {
            switch($type) {
                case "meetingStatus" :
                    require_once("classes/core/dbi/MeetingSequence.dbi.php");
                    require_once("classes/mgm/dbi/FmsServer.dbi.php");

                    $obj_MeetingSequence = new DBI_MeetingSequence( $dsn );
                    $obj_FmsServer       = new DBI_FmsServer( N2MY_MDB_DSN );
                    // 会議シーケンスの利用開始時間
                    $where = "meeting_key = ".$meeting_key.
                             " AND meeting_sequence_key = ".$meeting_sequence_key.
                             " AND start_datetime IS NULL";
                    $data = array(
                        "start_datetime" => date('Y-m-d H:i:s'),
                        );
                    $obj_MeetingSequence->update($data, $where);
                    $remaining_time = null;
                    $type = 'invalid';

                    // 会議データが存在し
                    if ($meeting_info) {
                        // 利用中で
                        if ((!$meeting_info['is_deleted']) && ($meeting_info['is_active'])) {
                            // 現在の会議遷移KEYが一致しない場合
                            $where = "meeting_key = ".$meeting_key;
                            $now_meeting_sequence = $obj_MeetingSequence->getRow($where, "meeting_sequence_key, status",
                                                                                 array("meeting_sequence_key" => "desc"));
                            if ("0" == $now_meeting_sequence["status"]) {
                                $this->logger2->warn(array($server_dsn_key, $meeting_key, $meeting_sequence_key), "invalid");
                            } elseif ($now_meeting_sequence["meeting_sequence_key"] != $meeting_sequence_key) {
                                $this->logger2->warn(array($meeting_key, $now_meeting_sequence["meeting_sequence_key"],
                                                           $meeting_sequence_key), "FMSの自動切換え");
                                $type = "moved";
                            } else {
                                // FMSのサーバ状態
                                if ($meeting_info["intra_fms"]) {
                                    require_once("classes/dbi/user_fms_server.dbi.php");
                                    $objUserFmsServer = new UserFmsServerTable($dsn);
                                    $where            = "fms_key = ".$meeting_info["server_key"];
                                    $server_info      = $objUserFmsServer->getRow($where);
                                } else {
                                    $where       = "server_key = ".$meeting_info["server_key"];
                                    $server_info = $obj_FmsServer->getRow($where);
                                }
                                //サービスメンテナンス情報取得
                                if ($this->config["N2MY"]["maintenance_notification"]) {
                                    if (N2MY_MDB_DSN) {
                                        require_once('classes/core/dbi/Notification.dbi.php');
                                        $objNotification = new NotificationTable(N2MY_MDB_DSN);
                                        $where = "start_datetime <= '".date('Y-m-d H:i:s',strtotime("1 hour"))."'" .
                                                 " AND start_datetime != '0000-00-00 00:00:00'" .
                                                 " AND end_datetime >= '".date('Y-m-d H:i:s')."'" .
                                                 " AND status = 1";
                                        if (DB::isError($objNotification)) {
                                            $this->logger2->error($objNotification->getUserInfo());
                                        } else {
                                            $notification_data = $objNotification->getRow($where, "start_datetime", array("start_datetime" => "asc"));
                                        }
                                    }
                                }
                                // メンテナンス中
                                if ($server_info["is_available"] == "0") {
                                    $this->logger->warn(__FUNCTION__."#server maintenance mode",__FILE__,__LINE__,array(
                                        $meeting_info,
                                        $server_info,
                                        ));
                                    $type = "maintenance";
                                } else {
                                    // 残りの時間を取得
                                    if ($meeting_info['is_reserved']) {
                                      require_once("classes/dbi/reservation.dbi.php");
                                      $reserve = new ReservationTable($dsn);
                                      $where = "meeting_key = '".$meeting_info["meeting_ticket"]."'";
                                      $reservationInfo = $reserve->getRow($where);
                                      $remaining_time = strtotime($reservationInfo['reservation_endtime']) - time();
                                      if ($meeting_info["prohibit_extend_meeting_flg"]) {
                                        //１分前を返す
                                        $remaining_time = $remaining_time - 60;
                                      }
                                    } else {
                                        $next_meeting = $obj_Meeting->getNextMeeting($meeting_key);
                                        if ($next_meeting) {
                                            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$next_meeting);
                                            $remaining_time = strtotime($next_meeting['meeting_start_datetime']) - time();
                                            if ($meeting_info["prohibit_extend_meeting_flg"]) {
                                              //１分前を返す
                                              $remaining_time = $remaining_time - 60;
                                            }
                                        }
                                    }

                                    // ステータス
                                    /**
                                     * 再起動時間５分前になったらrebootステータスを返す 2009/06/01 wada
                                     */
                                    $reboottime = null;
                                    if ($server_info["maintenance_time"]) {
                                        $maintenancetime = split(":", $server_info["maintenance_time"]);
                                        $reboottime      = mktime($maintenancetime[0], $maintenancetime[1], $maintenancetime[2]);
                                        $now             = mktime(date("H"), date("i"),      date("s"));
                                        $now5            = mktime(date("H"), date("i") + 5,  date("s"));
                                        $now10           = mktime(date("H"), date("i") + 10, date("s"));
                                    }
                                    
                                    if ($reboottime && ($reboottime > $now && $reboottime <= $now10)) {
                                        // 会議時間内
                                        $remaining_time = $reboottime - $now5;
                                        $type = 'reboot';
                                    } else if ( $notification_data["start_datetime"] ) {
                                        $remaining_time = strtotime($notification_data["start_datetime"]) - time();
                                        $type = 'serviceMaintenanceMode';
                                        $this->logger2->debug(array($remaining_time,$type));
                                    } else if ($remaining_time === null || $remaining_time > 0) {
                                        // 会議時間内
                                        $type = 'available';
                                    } else if ($next_meeting["prohibit_extend_meeting"]) {
                                        // 延長禁止
                                        $type = 'reservationTimeOver';
                                    } else if ($meeting_info["prohibit_extend_meeting_flg"] || $user["entry_mode"] !== "0" ) {
                                      // ポート制御設定の時は強制的に会議延長禁止
                                        $type = 'meetingTimeOver';
                                    } else {
                                        // 延長
                                        $type = 'extend';
                                    }
                                }
                            }
                        }
                    } else {
                        $this->logger2->info(array($server_dsn_key, $meeting_key, $meeting_sequence_key), "invalid");
                    }

                    $result["meetingStatus"] = array("remain" => $remaining_time,
                                                      "type"   => $type);
                    break;
                case "document" :
                    require_once("classes/core/dbi/Document.dbi.php");
                    $objDocument = new DBI_Document($dsn);
                    $where = "meeting_key = '".$meeting_key."'" .
                            " AND document_transmitted = 1".
                            " AND (document_status = ".DOCUMENT_STATUS_SUCCESS." OR document_status < 0)".
                            " AND document_mode IN ('meeting', 'mail')";
                    $documentList = $objDocument->getRowsAssoc($where);
                    $returnDocumentInfo = array();
                    $doc_dir = $this->getDocDir();
                    $doc_count = count($documentList);
                    $document_url = defined("N2MY_DOCS_URL") ? N2MY_DOCS_URL : N2MY_BASE_URL;
                    for ($i = 0; $i < $doc_count; $i++) {
                        $info = array();
                        $info["documentKey"]	  = $documentList[$i]["document_key"];
                        $info["url"]      = $document_url.$doc_dir.$documentList[$i]["document_path"].$documentList[$i]["document_id"]."/".$documentList[$i]["document_id"];
                        $info["fileName"] = $documentList[$i]["document_name"];
                        $info["length"]   = $documentList[$i]["document_index"];
                        $info["clientId"] = $documentList[$i]["participant_id"];
                        $info["mode"]     = $documentList[$i]["document_mode"];
                        $info["status"]   = $documentList[$i]["document_status"];
                        $info["format"]   = $documentList[$i]["document_format"];
                        $returnDocumentInfo[] = $info;
                        $data = array("document_transmitted" => 2,
                                      "update_datetime"      => date("Y-m-d H:i:s"));
                        $objDocument->update($data, sprintf("document_key='%s'", $documentList[$i]["document_key"]));
                    }
                    if ($returnDocumentInfo) {
                      $this->logger2->info($returnDocumentInfo, "upload_document_list");
                    }
                    $result["document"] = $returnDocumentInfo;
                    break;
                case "clip" :
                    require_once("classes/dbi/user.dbi.php");
                    $user_table = new UserTable( $dsn );
                    try {
                        $user = $user_table->find($meeting_info['user_key']);
                        $room_key = null;
                        // セールスだった場合部屋キーも渡す
                        if($meeting_info['use_sales_option'] == 1){
                            $room_key = $meeting_info['room_key'];
                        }
                        $returnVideoInfo = $this->getClip($dsn, $meeting_key, $meeting_info['user_key'] , $room_key);
                    } catch (Exception $e) {
                        $this->logger->error(__FUNCTION__,__FILE__,__LINE__.$e->getMessage());
                    }
                    $result['videoSharingList'] = "";
                    if (isset($returnVideoInfo)) {
                        $result['clip'] = $returnVideoInfo;
                    }
                    break;

                    /**
                     * calling
                     * success
                     * failed <declined>
                     * invalid
                     */
                case "videoConference" :
                  $result["videoConference"] = array();
                    if (($controller = Resolver::getConferenceOptionClassByRoomKey($meeting_info["room_key"]))) {
                      if ($videoInfo = $controller->getActiveVideoConferenceByMeetingKey($meeting_key)) {
                        $result["videoConference"]["participantList"] = $controller->getParticipatedList($meeting_key);
                      }
                      if ($calling_participant_key) {
                        $result["videoConference"]["callingStatus"] = $controller->getCallingStatus($calling_participant_key);
                      }
                    }
                    break;

                case "mobileConference" :
                  if (($controller = Resolver::getMobileController($meeting_info["room_key"]))) {
                    $status = $controller->getMobileConferenceStatus($meeting_key);
                  } else {
                    $status = false;
                  }
                  $result["mobileConference"]	= $status;
                  break;
                case "teleConference" :
                  // use_teleconf
                  require_once "classes/dbi/room.dbi.php";
                  $roomTable = new RoomTable($this->get_dsn($server_dsn_key));
                  $room_info = $roomTable->getRow( sprintf( "room_key='%s' AND room_status = 1", $meeting_info["room_key"] ));
                  if( !$room_info["use_teleconf"] ) {
                    $result["teleConference"]["teleConferenceId"] = 0;
                    break;
                  }
                  // option_key
                  require_once "classes/dbi/ordered_service_option.dbi.php";
                  $OrdSrvOptTbl = new OrderedServiceOptionTable($this->get_dsn($server_dsn_key));
                  $opt_info     = $OrdSrvOptTbl->enableOnRoom(23,$meeting_info["room_key"]);
                  if( !$opt_info ) {
                    $result["teleConference"]["teleConferenceId"] = 0;
                    break;
                  }
                  // PGI setting
                  require_once "classes/dbi/pgi_setting.dbi.php";
                  $pgiSettingTable = new PGiSettingTable($this->get_dsn($server_dsn_key));
                  $setting         = $pgiSettingTable->findEnablleNow($meeting_info['room_key']);
                  if( !$setting ) {
                    $result["teleConference"]["teleConferenceId"] = 0;
                    break;
                  }

                  $result["teleConference"]["teleConferenceId"] = $meeting_info["pgi_conference_id"];
                  break;
                default :
                    return false;
                    break;
            }
        }
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array("types"                => $types,
                                                                  "meeting_key"          => $meeting_key,
                                                                  "meeting_sequence_key" => $meeting_sequence_key,
                                                                  "result"               => $result));
        return $result;
    }

    /**
     * [ Meeting ] 会議室ベースでの録画可能残量を取得する（byte）
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return integer
     */
    function getRemainingRecordableSize( $server_dsn_key, $meeting_key )
    {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"          => $meeting_key,
                ));
            return false;
        }
        // ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                  "meeting_key"    => $meeting_key));
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        $dsn          = $this->get_dsn($server_dsn_key);
        $obj_Meeting  = new DBI_Meeting( $dsn );
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", $meeting_key ) );

        if( $meeting_info["user_key"] ){
            require_once("classes/dbi/user.dbi.php");
            $obj_User = new UserTable( $dsn );
            $user_info = $obj_User->getRow( sprintf( "user_key='%s' AND user_status = 1 AND user_delete_status = 0",
                                                     $meeting_info["user_key"] ) );
        }

        $remaining_size = 0;
        if ($meeting_info) {
            $total_used_size = ( $user_info["account_model"] == "member" || $user_info["account_model"] == "centre" ) ?
                                $obj_Meeting->getMemberUsedSize( $meeting_info["user_key"] ) :
                                $obj_Meeting->getUsedSize( $meeting_info["room_key"] );
            $remaining_size = ($meeting_info["meeting_size_recordable"] - $total_used_size);
        }
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,array(
            "meeting_key"     => $meeting_key,
            "room_key"        => $meeting_info["room_key"],
            "meeting_size"    => $remaining_size,
            "total_used_size" => $total_used_size,
            ));
        return $remaining_size;
    }

    /**
     * [ Meeting ] 会議の残り時間を取得する
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return time
     */
    function getRemainingTime($server_dsn_key, $meeting_key) {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "meeting_key"    => $meeting_key,));
            return false;
        }
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "server_dsn_key"=>$server_dsn_key,
                                "meeting_key"=>$meeting_key));
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting  = new DBI_Meeting( $dsn );
        $next_meeting = $obj_Meeting->getNextMeeting($meeting_key);
        if ($next_meeting) {
            $remaining_time = strtotime($next_meeting['meeting_start_datetime']) - time();
        }
        return $remaining_time;
    }

    /**
     * [ Meeting ] 実際の会議の開始日時を更新
     *
     * @access public
     * @param  integer $meeting_key 会議プライマリーKEY
     * @return boolean
     */
    function startMeeting($server_dsn_key, $meeting_key) {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "meeting_key"    => $meeting_key,));
            return false;
        }
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        $this->logger2->debug($meeting_key);
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingUptime.dbi.php");

        $obj_Meeting       = new DBI_Meeting( $dsn );
        $obj_MeetingUptime = new DBI_MeetingUptime( $dsn );

        $obj_MeetingUptime->start($meeting_key); // 利用期間テーブル更新
        $meeting_uptime = $obj_MeetingUptime->getActiveInfo($meeting_key); // 実利用開始日時取得
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array($meeting_key, $meeting_uptime));
        // 会議内容更新
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", $meeting_key ) );
        if ($meeting_info) {
            $data["use_flg"] = 1;
            if (!$meeting_info["actual_start_datetime"]) {
                $data["actual_start_datetime"] = $meeting_uptime["meeting_uptime_start"];
            }
            $data["update_datetime"] = date("Y-m-d H:i:s");
            //start meeting
            if ($meeting_info["is_reserved"] == "1") {
                $data["meeting_start_datetime"] = $meeting_uptime["meeting_uptime_start"];
            }
            $where = "meeting_key = ".$meeting_key;
            $ret   = $obj_Meeting->update($data, $where);
        }
        return true;
    }

    /**
     * [ Meeting ] 会議を終了してログを記録する
     *
     * @access public
     * @param string $server_dsn_key DNSキー
     * @param integer $meeting_key 会議キー
     * @param integer $meeting_sequence_key シーンキー
     * @param boolean $has_recorded_minutes 議事録あり
     * @param boolean $has_recorded_video ビデオ録画あり
     * @return boolean
     */
    function stopMeeting($server_dsn_key, $meeting_key, $meeting_sequence_key, $has_recorded_minutes,
                         $has_recorded_video, $eventlog, $moveFlg=false, $file_list=false, $rec_time = array()) {
        if ( !$server_dsn_key || !$meeting_key || !$meeting_sequence_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "meeting_key"    => $meeting_key,));
            return false;
        }

        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        // 必要なクラス
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        require_once "classes/core/dbi/Participant.dbi.php";
        require_once("classes/core/Core_Meeting.class.php");
        // DNSキー
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_Meeting         = new DBI_Meeting( $dsn );
        $obj_MeetingSequence = new DBI_MeetingSequence( $dsn );
        $obj_Participant        = new DBI_Participant( $dsn );
        $obj_CoreMeeting     = new Core_Meeting( $dsn, N2MY_MDB_DSN );
        // FMS終了
        $where = "meeting_sequence_key = ".$meeting_sequence_key;
        $meeting_sequence_row = $obj_MeetingSequence->getRow($where, "has_recorded_minutes,has_recorded_video");

        $resParticipant = $obj_Participant->getList($meeting_key);
        $meetingInfo = $obj_Meeting->getRow(sprintf("meeting_key='%s'", $meeting_key));
        
        try {
        	if ( $videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"], true) ) {
        		if ($videoInfo = $videoConferenceClass->getActiveVideoConferenceByMeetingKey($meeting_key)) {
        			$videoConferenceClass->cancelReservation( $videoInfo["conference_id"] );
        		}
        	}
        } catch (Exception $e){
        	$this->logger2->warn($e->getMessage());
        }
        
        $data = array(
            "has_recorded_minutes"  => ($meeting_sequence_row["has_recorded_minutes"]) ? 1 : $has_recorded_minutes,
            "has_recorded_video"    => ($meeting_sequence_row["has_recorded_video"]) ? 1 : $has_recorded_video,
            "end_datetime"          => date('Y-m-d H:i:s'),
            "status"                => 0,
            "record_second"         => (array_sum($rec_time) / 1000));
        $this->logger2->info(array($meeting_key, $data), "meeting_sequence_stop");
        $result = $obj_MeetingSequence->update($data, $where);

        $this->eventlog($dsn, $meeting_key, $eventlog);// イベントログ保存
        // 会議終了をバックグラウンドで実行
        /**
         * intrafmsの場合
         * sequence, meetingそれぞれフラグをたてて終了
         */
        $url = N2MY_LOCAL_URL."services/api.php" .
                "?action_stop_meeting=" .
                "&serverDsnKey=" .$server_dsn_key.
                "&meeting_ticket=".$meetingInfo["meeting_ticket"];
        $this->logger2->debug($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $str = curl_exec($ch);
        curl_close($ch);
    return true;
    }

    /**
     * [ Meeting ] 共有メモ保存機能
     * @param $server_dsn_key
     * @param $meeting_key
     * @param $meeting_sequence_key
     * @param $data
     * @return boolean
     */
    function postSharedMemoData($server_dsn_key, $meeting_key, $meeting_sequence_key , $data){
        if($data == null){
            $this->logger2->warn($meeting_key.":保存するデータがありません。");
            return false;
        }
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        require_once("classes/dbi/shared_file.dbi.php");
        require_once("classes/dbi/user.dbi.php");
        require_once("classes/N2MY_Sharing.class.php");
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        $dsn = $this->get_dsn($server_dsn_key);
        $objMeeting             = new DBI_Meeting($dsn);
        $objMeetingSequence     = new DBI_MeetingSequence($dsn);
        $objSharing     = new N2MY_Sharing($dsn);
        $objSharedFile = new SharedFileTable($dsn);
        $objUser     = new UserTable($dsn);
        $meeting_info = $objMeeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", mysql_real_escape_string($meeting_key )));
        $meeting_sequence_info = $objMeetingSequence->getRow(sprintf( "meeting_sequence_key='%s' ", mysql_real_escape_string($meeting_sequence_key )));
        // ミーティングデータがないとエラー
        if (!$meeting_info){
            $this->logger2->warn($meeting_key.":ミーティングがありません。");
            return false;
        }

        $user_info = $objUser->getRow( sprintf( "user_key='%s'", $meeting_info["user_key"]));
        // 録画した時のみ更新する場合 録画データがなければ終了
        if($user_info["is_minutes_delete"] == 1 && $meeting_sequence_info["has_recorded_video"] != 1){
            return false;
        }

        $file_name = sprintf( "%s%s%s", "vcmemo_",$meeting_sequence_key, ".txt" );

        $file = $objSharing->makeMemoDate($meeting_key, $data , $file_name);
        if(!$file){
            $this->logger2->warn($meeting_key.":メモファイル作成に失敗しました");
            return false;
        }

        // データ更新
        $db_data = array(
                "has_shared_memo" => 1,
        );
        $where = "meeting_sequence_key = ".$meeting_sequence_key;
        // ミーティングシーケンスにフラグを立てる
        $result = $objMeetingSequence->update($db_data, $where);
        $where = "meeting_key = ".$meeting_key;
        // ミーティングにフラグを立てる
        $result = $objMeeting->update($db_data, $where);

        // シェアファイルテーブルに追加
        $db_data = array(
                "meeting_sequence_key" => $meeting_sequence_key,
                "meeting_key"          => $meeting_key,
                "file_name"            => $file["file_name"],
                "file_path" => $file["full_path"],
                "file_size" => filesize($file["full_path"]),
                "type" => "txt",
                "status" => 1,
                "create_datetime" => date('Y-m-d H:i:s'),
                "update_datetime" => date('Y-m-d H:i:s'),
        );
        $result = $objSharedFile -> add($db_data);
        $this->logger2->info($file["full_path"].":メモファイルを作成しました。");
        return true;

    }

    /**
     * [ Meeting ] 個人ノート保存機能
     * @param $server_dsn_key
     * @param $meeting_key
     * @param $meeting_sequence_key
     * @param $personalwhiteboards
     * @return boolean
     */
    function setPersonalWhiteboard($server_dsn_key , $meeting_key , $meeting_sequence_key , $personalwhiteboards ){
        $this->logger2->info($personalwhiteboards );
        if(personalwhiteboards  == null){
            $this->logger2->warn($meeting_key.":保存するデータがありません。");
            return false;
        }
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        require_once("classes/dbi/user.dbi.php");
        require_once ("classes/dbi/member.dbi.php");
        require_once ("classes/core/dbi/Participant.dbi.php");
        require_once ("classes/N2MY_Storage.class.php");
        // インスタンス作成
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        $dsn = $this->get_dsn($server_dsn_key);
        $objMeeting             = new DBI_Meeting($dsn);
        $objMeetingSequence     = new DBI_MeetingSequence($dsn);
        $objMember   = new MemberTable($dsn);
        $objUser     = new UserTable($dsn);
        $objParticipant   = new DBI_Participant($dsn);
        $objStorageTable = new N2MY_Storage($dsn , $server_dsn_key);
        $obj_FmsService = new DBI_FmsServer(N2MY_MDB_DSN);
        $meeting_info = $objMeeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", mysql_real_escape_string($meeting_key )));
        $meeting_sequence_info = $objMeetingSequence->getRow(sprintf( "meeting_sequence_key='%s' ", mysql_real_escape_string($meeting_sequence_key )));
        $fms_server_info = $obj_FmsService->getRow("server_key = ".$meeting_sequence_info["server_key"]);
        $count = 1;
        foreach($personalwhiteboards  as $personalwhite_data){
            $participant_info = $objParticipant->getRow("participant_key = " . $personalwhite_data["participant_key"]);
            $member_info = $objMember->getRow(sprintf("member_id = '%s'" , $participant_info["storage_login_id"]));
            //$this->logger2->info($member_info );
            // TODO 名前決める
            $file_name = "vcpw_".date("YmdHi")."_".sprintf("%03d", $count).".wbs";
            $count++;
            //$file_name = $personalwhite_data["participant_key"]."meeting-wb.wbs";
            if($member_info){
                $user_info = $objUser->getRow("user_key = " . $member_info["user_key"]);
                $path = $user_info["user_id"] . "/".$member_info["member_key"] . "/wb/".$meeting_sequence_key."/".$personalwhite_data["participant_key"]."/";
                $is_member = 1;
                $member_key = $member_info["member_key"];
            }else{
              $user_info = $objUser->getRow(sprintf("user_id = '%s'" , $participant_info["storage_login_id"]));
              if($participant_info["personal_wb_login_id"]){
                //member_key
                $member_info = $objMember->getRow(sprintf("member_id = '%s'" , $participant_info["personal_wb_login_id"]));
                $path = $user_info["user_id"] . "/".$member_info["member_key"] . "/wb/".$meeting_sequence_key."/".$personalwhite_data["participant_key"]."/";
                $is_member = 1;
                $member_key = $member_info["member_key"];
              } else {
                $path = $user_info["user_id"] . "/wb/".$meeting_sequence_key."/".$personalwhite_data["participant_key"]."/";
                $is_member = 0;
                $member_key = 0;
              }
            }
            if($user_info["max_storage_size"] <= 0){
                continue;
            }
            $local_path =   N2MY_DOC_DIR . $path . $file_name;
            // フォルダ作成
            if(!file_exists(N2MY_DOC_DIR.$path)){
                mkdir(N2MY_DOC_DIR.$path,0777,true);
                chmod(N2MY_DOC_DIR.$path, 0777);
            }
            //windowsサーバ対策
            $windows_path_list = array(
                    "c" => "C:" ,
                    "d" => "D:" ,
                    "e" => "E:"
                    );
            $cygwin_root = "/cygdrive/";
            foreach($windows_path_list as $key => $windows_path){
                if(stristr($personalwhite_data["file_path"] , $windows_path)){
                	if(ctype_upper(substr($personalwhite_data["file_path"], 0, 1))){
            			$personalwhite_data["file_path"] = str_replace($windows_path, $cygwin_root.$key, $personalwhite_data["file_path"]);
                	}else{
            			$personalwhite_data["file_path"] = str_replace(strtolower($windows_path), $cygwin_root.$key, $personalwhite_data["file_path"]);
                	}
                    $personalwhite_data["file_path"] = str_replace("\\", "/", $personalwhite_data["file_path"]);
                }

                if(stristr($local_path , $windows_path)){
                	if(ctype_upper(substr($local_path, 0, 1))){
	                    $local_path = str_replace($windows_path, $cygwin_root.$key, $local_path);
                	}else{
	                    $local_path = str_replace(strtolower($windows_path), $cygwin_root.$key, $local_path);
                	}
                    $local_path = str_replace("\\", "/", $local_path);
                }
            }

            // FMSとwebサーバが同じだった場合のコマンド修正
            if($_SERVER["SERVER_NAME"] == $fms_server_info["server_address"]){
                $cmd = sprintf("scp "  . '"%s" "%s"' , $personalwhite_data["file_path"],$local_path);
            }else{
                $cmd = sprintf("scp " .N2MY_SCP_USER . '@%s:"%s" "%s"' , $fms_server_info["server_address"],$personalwhite_data["file_path"],$local_path);
            }
            exec($cmd, $arr, $res);
            if($res !== 0){
                $err = array(
                        "res" => $res,
                        "arr" => $arr,
                        "cmd" => $cmd,
                        "login_id" => $participant_info["storage_login_id"]
                        );
                $this->logger2->error($err);
                continue;
            }else{
                chmod($local_path, 0777);
                $personal_white_id = $meeting_key.",".$meeting_sequence_key.",".$personalwhite_data["participant_key"];
                $objStorageTable->add($user_info["user_key"], $member_key, $path, $file_name, $personalwhite_data["file_size"], "bitmap", "personal_white", "wbs", "as2" , null , null , $personal_white_id,0 , $personalwhite_data["page_count"]);
            }
        }
        return true;
    }

    /**
     * TODO アンケート機能受け取り用関数 とりあえずログだけ
     * @param unknown_type $server_dsn_key
     * @param unknown_type $meeting_key
     * @param unknown_type $meeting_squence_key
     * @param unknown_type $fcs_quick_vote_id
     * @param unknown_type $ArrayData[$participant_key,$participant_name,$result]
     */
    function setQuickVoteResult($server_dsn_key,$meeting_key , $meeting_squence_key , $fcs_quick_vote_id, $vote_results){

        $this->logger2->info("server_dsn_key:".$server_dsn_key);
        $this->logger2->info("meeting_key:".$meeting_key);
        $this->logger2->info("meeting_squence_key:".$meeting_squence_key);
        $this->logger2->info("fcs_quick_vote_id:".$fcs_quick_vote_id);
        $this->logger2->info($vote_results);

        $obj_quickVoteLog = new QuickVoteLogTable($this->get_dsn($server_dsn_key));
        $number = $obj_quickVoteLog->countVoteNumber($meeting_key);
        if($number == 1){
            require_once("classes/core/dbi/Meeting.dbi.php");
            $obj_Meeting = new DBI_Meeting($this->get_dsn($server_dsn_key));
            $where = "meeting_key =".$meeting_key;
            $data["has_quick_vote"] = 1;
            $obj_Meeting->update($data, $where);
        }
        $add_data = array();
        $add_data["meeting_key"]          = $meeting_key;
        $add_data["meeting_sequence_key"] = $meeting_squence_key;
        $add_data["number"]               = $number;
        $add_data["fcs_quick_vote_id"]    = $fcs_quick_vote_id;
        $add_data["create_datetime"]      = date('Y-m-d H:i:s');
        foreach($vote_results as $vote_result) {
            if($vote_result["action"] == "add") {
                $add_data["participant_key"]    = $vote_result["participant_key"];
                $add_data["participant_name"]   = $vote_result["participant_name"];
                $add_data["result"] = $vote_result["result"];
                $this->logger2->info($add_data);
                $obj_quickVoteLog->add($add_data);
            } elseif ($vote_result["action"] == "change") {
                $change_data["result"] = $vote_result["result"];
                $where = "participant_key ='".$vote_result["participant_key"]."' AND number='".$add_data["number"]."'";
                $obj_quickVoteLog->update($change_data, $where);
            }
        }
        return true;
    }

    /**
     * TODO サジェスト機能受け取り用関数 とりあえずログだけ
     * @param unknown_type $server_dsn_key
     * @param unknown_type $meeting_key
     * @param unknown_type $meeting_sequence_key
     * @param unknown_type $data[$participant_key,$user_name,$level,$statusType,$timeStamp,$timezoneMinutes]
     */
    function postSuggestData($server_dsn_key, $meeting_key, $meeting_sequence_key, $data){
        $this->logger2->info("server_dsn_key:".$server_dsn_key);
        $this->logger2->info("meeting_key:".$meeting_key);
        $this->logger2->info("meeting_sequence_key:".$meeting_sequence_key);
        $this->logger2->info($data);

        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting = new DBI_Meeting($dsn);
        $meeting_info = $obj_Meeting->getRow(sprintf("meeting_key='%s' AND is_deleted = 0", $meeting_key));
        if( $meeting_info ) {
            require_once("classes/dbi/meeting_suggest_log.dbi.php");
            $obj_MeetingSuggestLog = new MeetingSuggestTable($dsn);
            foreach($data as $value) {
                $suggest_data = array();
                $suggest_data["meeting_key"] = $meeting_key;
                $suggest_data["meeting_sequence_key"] = $meeting_sequence_key;
                $suggest_data["user_key"] = $meeting_info["user_key"];
                $suggest_data["participant_key"] = $value["participantKey"];
                $suggest_data["user_name"] = $value["userName"];
                $suggest_data["level"] = $value["level"];
                $suggest_data["status_type"] = $value["statusType"];
                $date =  date("Y-m-d H:i:s", $value["timeStamp"]/1000);
                $this->logger2->info($date);
                $suggest_data["suggest_time"] = $date;
                $suggest_data["create_datetime"] = date("Y-m-d H:i:s");
                $this->logger2->info($suggest_data);
                $obj_MeetingSuggestLog->add($suggest_data);
                unset($suggest_data);
            }

            return true;
        }
    }

    /**
     * TODO 会議室内に機能利用数カウント
     * @param unknown_type $server_dsn_key
     * @param unknown_type $meeting_key
     * @param unknown_type $meeting_sequence_key
     * @param unknown_type $data[$action_type,$count]
     */
    function postActionCount($server_dsn_key, $meeting_key, $meeting_sequence_key, $data){
      $this->logger2->debug("server_dsn_key:".$server_dsn_key);
      $this->logger2->debug("meeting_key:".$meeting_key);
      $this->logger2->debug("meeting_sequence_key:".$meeting_sequence_key);
      $this->logger2->debug($data);

      $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting = new DBI_Meeting($dsn);
      $meeting_info = $obj_Meeting->getRow(sprintf("meeting_key='%s' AND use_flg=1 AND is_deleted = 0", $meeting_key));
      if( isset($meeting_info) &&  $meeting_sequence_key) {
        require_once("classes/dbi/meeting_function_count_log.dbi.php");
            $obj_MeetingFunctionCountLog = new MeetingFunctionCountLogTable($dsn);
            $action_count = array();
            $action_count["meeting_key"] = $meeting_key;
            $action_count["meeting_sequence_key"] = $meeting_sequence_key;
            $action_count["create_datetime"] = date("Y-m-d H:i:s");
            foreach($data as $value) {
                $type_name = $value["actionType"];
                $action_count[$type_name] = $value["cnt"];// $value[0];
            }
            $this->logger2->debug($action_count);
            $obj_MeetingFunctionCountLog->add($action_count);
            unset($action_count);
            return true;
        } else {
            return false;
        }
    }

   /**
     * [ Participant ] 入退室時の参加者情報を更新
     *
     * @access public
     * @param  string  $server_dsn_key  DSNキー
     * @param  integer $meeting_key     会議キー
     * @param  array   $participant     参加者情報
     * @return boolean
     */
    function updateParticipant($server_dsn_key, $meeting_key, $participant_info) {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger2->warn(array("server_dsn_key" => $server_dsn_key,
                                       "meeting_key"    => $meeting_key,));
            return false;
        }
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        $this->logger2->debug(array("server_dsn_key" => $server_dsn_key,
                                   "meeting_key"    => $meeting_key,
                                   "participant"    => $participant_info));
        $dsn = $this->get_dsn($server_dsn_key);
        // クラス
        require_once "classes/core/dbi/Meeting.dbi.php";
        require_once "classes/core/dbi/MeetingUptime.dbi.php";
        require_once "classes/core/dbi/Participant.dbi.php";
        require_once "classes/dbi/operation_log.dbi.php";

        // インスタンス生成
        $obj_Meeting       = new DBI_Meeting($dsn);
        $obj_Participant   = new DBI_Participant($dsn);
        $obj_MeetingUptime = new DBI_MeetingUptime($dsn);
        $objOperationLog   = new OperationLogTable($dsn);
        $meeting_info      = $obj_Meeting->getRow(sprintf("meeting_key='%s' AND is_deleted = 0", $meeting_key));
        $result  = false;
        if ($meeting_info) {
            $participant = $participant_info['user'];
            if ($participant_info['action'] == 'remove') {
                // FMS利用統計
                require_once("classes/core/dbi/MeetingSequence.dbi.php");
                require_once("classes/dbi/ping_failed.dbi.php");
                $objMeetingSequence     = new DBI_MeetingSequence($dsn);
                $objPingFailed          = new PingFailedTable($dsn);
                $user_key = $meeting_info["user_key"];
                if ($participant['sequence']) {
                    $where = "meeting_key = ".$meeting_key.
                        " AND meeting_sequence_key = ".$participant['sequence'];
                    if ($server_key = $objMeetingSequence->getOne($where, "server_key")) {
                        $data = array(
                            'type'          => 'remove',
                            'reload_type'   => $participant['reloadType'],
                            'fms_key'       => $server_key,
                            'user_key'      => $user_key,
                            'meeting_key'   => $meeting_key,
                            'sequence_key'  => $participant['sequence'],
                            'p_session'     => $participant['id'],
                            'p_login_time'  => date('Y-m-d H:i:s', $participant['enteredTime'] / 1000),
                            'p_time'        => date('Y-m-d H:i:s', $participant['unixTime'] / 1000),
                            'p_protocol'    => $participant['protocol'],
                            'p_port'        => $participant['port'],
                            'fms_bytes_in'            => $participant['statistics']['bytes_in'],
                            'fms_bytes_out'           => $participant['statistics']['bytes_out'],
                            'fms_msg_in'              => $participant['statistics']['msg_in'],
                            'fms_msg_out'             => $participant['statistics']['msg_out'],
                            'fms_msg_dropped'         => $participant['statistics']['msg_dropped'],
                            'fms_ping_rtt'            => $participant['statistics']['ping_rtt'],
                            'fms_audio_queue_msgs'    => $participant['statistics']['audio_queue_msgs'],
                            'fms_video_queue_msgs'    => $participant['statistics']['video_queue_msgs'],
                            'fms_so_queue_msgs'       => $participant['statistics']['so_queue_msgs'],
                            'fms_data_queue_msgs'     => $participant['statistics']['data_queue_msgs'],
                            'fms_dropped_audio_msgs'  => $participant['statistics']['dropped_audio_msgs'],
                            'fms_dropped_video_msgs'  => $participant['statistics']['dropped_video_msgs'],
                            'fms_audio_queue_bytes'   => $participant['statistics']['audio_queue_bytes'],
                            'fms_video_queue_bytes'   => $participant['statistics']['video_queue_bytes'],
                            'fms_so_queue_bytes'      => $participant['statistics']['so_queue_bytes'],
                            'fms_data_queue_bytes'    => $participant['statistics']['data_queue_bytes'],
                            'fms_dropped_audio_bytes' => $participant['statistics']['dropped_audio_bytes'],
                            'fms_dropped_video_bytes' => $participant['statistics']['dropped_video_bytes'],
                            'fms_bw_out'              => $participant['statistics']['bw_out'],
                            'fms_bw_in'               => $participant['statistics']['bw_in'],
                            'fms_client_id'           => $participant['statistics']['client_id'],
                            'create_datetime'   => date('Y-m-d H:i:s'),
                        );
                        $ret = $objPingFailed->add($data);
                        if (DB::isError($ret)) {
                            $this->logger2->error($ret->getUserInfo());
                            return false;
                        }
                    }
                }
            }
            // 終了した会議
            if ($meeting_info["is_active"] == "0") {
                // 全員退出
                $data = array(
                    "is_active"       => 0,
                    "update_datetime" => date("Y-m-d H:i:s")
                    );
                $ret = $obj_Participant->update($data, "meeting_key = ".$meeting_key." AND is_active = 1");
                $this->logger2->warn($meeting_key, "end meeting");
            } else {
                // 参加者情報
                $edit_particpant = array(
                    "participant_name"     => $participant['name'],
                    "participant_port"     => $participant['port'],
                    "participant_protocol" => $participant['protocol'],
                    "update_datetime"      => date("Y-m-d H:i:s"),
                    );
                // 操作ログ情報
                $opration_log = array(
                    "user_key"             => $meeting_info["user_key"],
                    "remote_addr"          => $participant["ip"],
                    "operation_datetime"   => date("Y-m-d H:i:s"),
                    "info"                 => serialize(array(
                        "participant_name" => $participant["name"],
                        "room_key"         => $meeting_info["room_key"],
                        ))
                    );
                // 入室
                $is_participant = true;
                $room_info = array();
                if ($participant_info['action'] == 'add') {
                    $opration_log["action_name"]   = "meeting_in";
                    $edit_particpant["uptime_end"] = null;
                    $edit_particpant["is_active"]  = 1;
                    // Check exsits participant_key or participant_session_id
                    $where = "meeting_key = ".$meeting_key.
                             " AND (participant_key = '".addslashes($participant['id']).
                             "' OR participant_session_id = '".addslashes($participant['id'])."')";
                    $current_data = $obj_Participant->getRow($where);
                    if (!$current_data) {
                        $is_participant = false;
                    } else {
                        if (!$current_data["uptime_start"]) {
                            $edit_particpant["uptime_start"] = date("Y-m-d H:i:s");
                        }
                    }
                    /*
                     * Core_meeting::getMeetingDetailsより処理を移行
                     * onParticipantcreateの移行
                     */
                    if ($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting_info["room_key"])) {
                      if ($videoInfo = $videoConferenceClass->getActiveVideoConferenceByMeetingKey($meeting_key)) {
                        $videoConferenceClass->addVideoConferenceParticipant($meeting_key, $participant['id']);
                      }
                    }
                    //部屋人数拡張用に部屋情報取得
                    require_once("classes/dbi/room.dbi.php");
                    $obj_Room = new RoomTable( $dsn );
                    $room_info = $obj_Room->getRow( sprintf( "room_key='%s' AND room_status = 1", $meeting_info["room_key"] ),
                                                    "room_key, max_seat, max_audience_seat, extend_max_seat, extend_max_audience_seat, extend_seat_flg" );
                // 退出
                } else if ($participant_info['action'] == 'remove') {
                    $opration_log["action_name"]   = "meeting_out";
                    $edit_particpant["uptime_end"] = date("Y-m-d H:i:s");
                    $edit_particpant["is_active"]  = 0;
                }
                // 存在すれば更新
                if ($is_participant) {
                    // 参加者情報更新
                    $where = "meeting_key = ".$meeting_key.
                        " AND (participant_key = '".addslashes($participant['id']).
                        "' OR participant_session_id = '".addslashes($participant['id'])."')";
                    $this->logger2->debug(array($edit_particpant, $where));
                    $obj_Participant->update($edit_particpant, $where);

                    //video conference participantの更新
                    if ($participant_info['action'] == 'remove') {
                      require_once("classes/dbi/video_conference.dbi.php");
                      $video		= new VideoConferenceTable($dsn);
                      $where		= "meeting_key = '".addslashes($meeting_key)."' AND is_active = 1";
                      $videoInfo	= $video->getRow($where);
                      if ($videoInfo) {
                        require_once("classes/dbi/video_conference_participant.dbi.php");
                        $videoConf		= new VideoConferenceParticipantTable($dsn);
                        $videoConfData = array("is_active"=>0,"updatetime"=>date("Y-m-d H:i:s"));
                        $where = "video_conference_key = ".$videoInfo["video_conference_key"]." AND participant_key = '".addslashes($participant['id'])."'";
                        $videoConf->update($videoConfData, $where);
                      }
                    }
                } else {
                    $edit_particpant['meeting_key']            = $meeting_key;
                    $edit_particpant['participant_mode_name']  = $participant['type'];
                    $edit_particpant['participant_type_name']  = $participant['type'];
                    $edit_particpant['participant_role_name']  = 'default';
                    $edit_particpant['participant_locale']     = 'ja';
                    $edit_particpant['is_narrowband']          = 0;
                    $edit_particpant['use_count']              = 0;
                    $edit_particpant['create_datetime']        = date('Y-m-d H:i:s');
                    $edit_particpant['participant_session_id'] = $participant['id'];
                    $obj_Participant->add($edit_particpant);
                }
                // メンバー情報
                $where = "participant_key = '".$participant["id"]."'";
                $member = $obj_Participant->getRow($where, "member_key");
                if ($member["member_key"]) {
                    require_once ("classes/dbi/member.dbi.php");
                    $objMember   = new MemberTable($dsn);
                    $where       = "member_key = ".$member["member_key"];
                    $member_info = $objMember->getRow($where, "member_key, member_id");
                    $opration_log["member_key"] = $member_info["member_key"];
                    $opration_log["member_id"]  = $member_info["member_id"];
                }
                // 操作ログ追加
                $this->logger2->debug($opration_log, 'opration_log');
                $objOperationLog->add($opration_log);
                //部屋人数拡張時の人数チェック
                if ($room_info["extend_seat_flg"]) {
                    require_once('classes/core/dbi/Participant.dbi.php');
                    $obj_Participant     = new DBI_Participant( $dsn );
                     require_once('classes/dbi/max_connect_log.dbi.php');
                     $obj_MaxConnectLog     = new MaxConnectLog( $dsn );
                    //通常人数カウント
                    $data = array(
                        "user_key"      => $meeting_info["user_key"],
                        "meeting_key"   => $meeting_info["meeting_key"],
                        "room_key"      => $meeting_info["room_key"],
                        );
                    if (($room_info["extend_max_seat"] > 0 || $room_info["extend_max_audience_seat"] > 0) && $room_info["extend_seat_flg"]) {
                        $nowNormalParticipantNumber = $obj_Participant->numRows('meeting_key = '.$meeting_info["meeting_key"].' AND is_active = 1 AND participant_type_key = 1');
                        $this->logger2->info(array($room_info["max_seat"],$nowNormalParticipantNumber));
                        if ($nowNormalParticipantNumber > $room_info["max_seat"]) {
                            $data["type"] = "extend_seat";
                            $data["connect_count"] = $nowNormalParticipantNumber;
                            $obj_MaxConnectLog->add($data);
                        }
                        //オーディエンス人数カウント
                        $nowAudienceParticipantNumber = $obj_Participant->numRows('meeting_key = '.$meeting_info["meeting_key"].' AND is_active = 1 AND participant_type_key = 2');
                        $this->logger2->info(array($room_info["max_audience_seat"],$nowAudienceParticipantNumber));
                        if ($nowAudienceParticipantNumber > $room_info["max_audience_seat"]) {
                            $data["type"] = "extend_audience_seat";
                            $data["connect_count"] = $nowAudienceParticipantNumber;
                            $obj_MaxConnectLog->add($data);
                        }
                    }

                }
                // 現在の参加人数
                $active_number = $obj_Participant->numRows('meeting_key = '.$meeting_key.' AND is_active = 1');
                if ($active_number >= 2) {
                    // 会議停止
                    $this->logger2->debug('start');
                    $obj_MeetingUptime->start($meeting_key);
                } else {
                    // 会議停止
                    $this->logger2->debug('stop');
                    $obj_MeetingUptime->stop($meeting_key);
                }
                $result = true;
            }
        } else {
            $this->logger2->warn($meeting_key, 'meeting not exists');
        }

        return $result;
    }


    /**
      * [ Participant ] 入退室時の参加者情報を一括で更新する
     *
     * @access public
     * @param  integer $meeting_key      会議プライマリーKEY
     * @param  array   $participant_list 参加者情報一覧（連想配列）
     * @return boolean
     */
    function updateParticipantList($server_dsn_key, $meeting_key, $participant_list, $action) {
        if ( !$server_dsn_key || !$meeting_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key" => $server_dsn_key,
                "meeting_key"    => $meeting_key,
                ));
            return false;
        }
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
            "server_dsn_key"   => $server_dsn_key,
            "meeting_key"      => $meeting_key,
            "participant_list" => $participant_list,
            "action"           => $action));
        $dsn = $this->get_dsn($server_dsn_key);
        // クラス
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingUptime.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        // インスタンス生成
        $obj_Meeting       = new DBI_Meeting( $dsn );
        $obj_Participant   = new DBI_Participant( $dsn );
        $obj_MeetingUptime = new DBI_MeetingUptime( $dsn );
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key) );
        //update participant list
        $result  = false;
        if ($meeting_info) {
            // 終了した会議は更新しない！
            if ($meeting_info["is_active"] == "0") {
                $this->logger->warn(__FUNCTION__."#end meeting",__FILE__,__LINE__,$meeting_key);
                $result = false;
            } else {
                // 利用者更新
                    if ($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting_info["room_key"])) {
                        if ($videoInfo = $videoConferenceClass->getActiveVideoConferenceByMeetingKey($meeting_key)) {
                            $result = $videoConferenceClass->updateListWithoutPolycom($meeting_key, $participant_list);
                        } else {
                            $result = $obj_Participant->updateList($meeting_key, $participant_list);
                        }
                    }
                    else {
                        $result = $obj_Participant->updateList($meeting_key, $participant_list);
                    }
            }
        $this->logger->debug(__FUNCTION__."#list",__FILE__,__LINE__, array(
            "meeting_key"      => $meeting_key,
            "participant_list" => $participant_list,
            "result"           => $result
            ));
        return $result;
        }
    }

    /**
     * [ Participant ] 会議室毎の参加者の利用時間を更新する
     *
     * @access public
     * @param  string   $server_dsn_key     DSNキー
     * @param  integer  $meeting_key        会議キー
     * @param  array    $particiapnt_number 参加人数
     * @param  datetime $current_date       現在時刻
     * @return boolean
     */
    function updateParticipantUptimeList($server_dsn_key, $meeting_key, $particiapnt_number, $current_date) {
        if (!$server_dsn_key || !$meeting_key) {
            $this->logger2->warn(array("meeting_key"        => $meeting_key,
                                       "particiapnt_number" => $particiapnt_number));
            return array('status' => 0,
                         'err_cd' => 2,  // パラメタエラー
            );
        }
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        $this->logger2->debug(array(
            "meeting_key"        => $meeting_key,
            "particiapnt_number" => $particiapnt_number,
            "current_date"       => $current_date
            ));
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingOptions.dbi.php");
        require_once("classes/core/dbi/MeetingUseLog.dbi.php");
        require_once("classes/core/dbi/Options.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        require_once("classes/dbi/member.dbi.php");
        require_once("classes/dbi/meeting_member_use_log.dbi.php");

        $obj_Options            = new DBI_Options();
        $obj_Meeting            = new DBI_Meeting($dsn);
        $obj_MeetingOptions     = new DBI_MeetingOptions($dsn);
        $obj_MeetingUseLog      = new DBI_MeetingUseLog($dsn);
        $obj_Participant        = new DBI_Participant($dsn);
        $objMember              = new MemberTable($dsn);
        $objMeetingMemberUseLog = new MeetingMemberUseLogTable($dsn);

        // 会議情報取得
        $meeting_info = $obj_Meeting->getRow(sprintf( "meeting_key = '%s' AND is_deleted = 0", $meeting_key));
        if (!$meeting_info) {
            $this->logger2->warn($meeting_key, '会議が存在しない');
            return array('status' => 0,
                         'err_cd' => 1,
            );
        }
        // 会議の参加者情報を取得
        $participant_list = $obj_Participant->getList($meeting_key);
        // $this->logger2->info($participant_list);
        // 人数の一致を確認
        if (count($participant_list) != $particiapnt_number) {
            $this->logger2->warn(array($meeting_key, $participant_list, $particiapnt_number), '件数が一致しない');
            return array('status' => 0,
                         'err_cd' => 3,);
        } else if ($particiapnt_number == 1) {
            // 1拠点のみの場合でも課金しない
            $this->logger2->warn(array($meeting_key, $participant_list, $particiapnt_number), '1拠点のみ');
            return array('status' => 0,
                         'err_cd' => 5,);
        }
        // 参加者の利用時間を更新
        $sql = "UPDATE participant SET ".
               " use_count = use_count + 1".
               ",uptime_end = '".$current_date."'".
               ",update_datetime = '".date("Y-m-d H:i:s")."'" .
               " WHERE meeting_key = ".$meeting_key.
               " AND is_active = 1";
        //$this->logger2->info($sql);
        $obj_Participant->_conn->query($sql);
        // 会議の利用時間を追加
        $data = array(
            "meeting_key"     => $meeting_info["meeting_key"],
            "room_key"        => $meeting_info["room_key"],
            "meeting_tag"     => $meeting_info["meeting_tag"],
            "create_datetime" => date("Y-m-d H:i:s"),
        );
        $use_logs['meeting_use_log'] = $data;
        $ret = $obj_MeetingUseLog->add($data);
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return array(
                'status' => 0,
                'err_cd' => 4,
            );
        }
        // 高画質オプション課金
        $where      = "option_name = 'hispec'";
        $hispec_key = $obj_Options->getOne($where, "option_key");
        $where      = "meeting_key = ".$meeting_info["meeting_key"].
                      " AND option_key = ".$hispec_key;
        if ($obj_MeetingOptions->numRows($where)) {
            $data = array("meeting_key"     => $meeting_info["meeting_key"],
                          "room_key"        => $meeting_info["room_key"],
                          "meeting_tag"     => $meeting_info["meeting_tag"],
                          "participant_id"  => "",
                          "type"            => "hispec",
                          "create_datetime" => date("Y-m-d H:i:s"),
            );
            $use_logs['highspec'] = $meeting_info["room_key"];
            $ret = $obj_MeetingUseLog->add_extension($data);
            if (DB::isError($ret)){
                $this->logger2->error($ret->getUserInfo());
                return array('status' => 0,
                             'err_cd' => 4,
                );
            }
        }
        foreach($participant_list as $participant) {
            // 従量課金の追加
            switch ($participant['participant_type_name']) {
                case "h323":
                case "h323ins":
                case "phone":
                case "compact":
                case "audience":
                    $data = array(
                        'meeting_key'     => $meeting_info['meeting_key'],
                        'room_key'        => $meeting_info['room_key'],
                        'meeting_tag'     => $meeting_info['meeting_tag'],
                        'participant_id'  => $participant['participant_session_id'],
                        'type'            => $participant['participant_type_name'],
                        'create_datetime' => date("Y-m-d H:i:s"),
                    );
                    $types[] = array(
                        'type'            => $participant['participant_type_name'],
                        'participant_id'  => $participant['participant_session_id'],
                        );
                    $ret = $obj_MeetingUseLog->add_extension($data);
                    if(DB::isError( $ret )){
                        $this->logger2->error($ret->getUserInfo());
                    }
                    break;
            }
            // メンバー課金利用時間の追加
            if ($participant['member_key']) {
                $useData = array(
                    "meeting_key"     => $meeting_key,
                    "create_datetime" => date( "Y-m-d H:i:s" ),
                    "user_key"        => $meeting_info['user_key'],
                    "member_key"      => $participant['member_key'],
                    "participant_key" => $participant['participant_key'],
                    );
                $members[] = array(
                    "member_key"      => $participant['member_key'],
                    "participant_key" => $participant['participant_key'],
                    );
                $ret = $objMeetingMemberUseLog->add( $useData );
                if(DB::isError($ret)){
                    $this->logger2->error($ret->getUserInfo());
                }
            }
        }
        if ($types)     $use_logs['types'] = $types;
        if ($members)   $use_logs['members'] = $members;
        $this->logger2->debug($use_logs, 'add use log');

        return array(
            'status' => 1,
            'err_cd' => 0,
        );
    }

    /**
     * メール送信機能
     *
     * @access public
     * @param  string  $mail_from    メール送信元アドレス
     * @param  string  $mail_to      メール送信先アドレス（カンマ区切りで複数指定可）
     * @param  string  $mail_subject メールサブジェクト
     * @param  string  $mail_body    メール本文
     * @return boolean
     */
    function sendReport($mail_from, $mail_to, $mail_subject, $mail_body) {
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array("mail_from"    => $mail_from,
                                                                 "mail_to"      => $mail_to,
                                                                 "mail_subject" => $mail_subject,
                                                                 "mail_body"    => $mail_body));
        // 入力チェック
        if (!$mail_from || !$mail_to || !$mail_subject || !$mail_body) {
            return false;
        }
        require_once("lib/EZLib/EZMail/EZSmtp.class.php");
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang = EZLanguage::getLangCd("ja");
        $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
        $obj_SendMail->setFrom(N2MY_MAIL_FROM);
        $obj_SendMail->setReturnPath($mail_from);
        $obj_SendMail->setTo($mail_to);
        $obj_SendMail->setSubject($mail_subject);
        $obj_SendMail->setBody($mail_body);
        // メール送信
        $result = $obj_SendMail->send();
        $this->logger->info(__FUNCTION__."#result",__FILE__,__LINE__,$result);
        return $result;
    }

    /**
     * FMSの一覧取得
     */
    function getServerList() {
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
        $where = "is_available = 1";
        $ret = $obj_FmsServer->getRowsAssoc($where);
        return $ret;
    }

    /**
     * FMSの有効、無効切り替え
     */
    function setServerAvailability($server_dsn_key, $server_key, $status) {
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                 "server_key"     => $server_key,
                                                                 "status"         => $status));
        if ( !$server_dsn_key || !$server_key) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "server_key"     => $server_key,));
            return false;
        }
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
        $where         = "server_key = ".addslashes($server_key);
        if ($obj_FmsServer->numRows($where) == 0) {
            return false;
        }
        $status = ($status) ? 1 : 0;
        $data   = array("is_available" => $status);
        $ret    = $obj_FmsServer->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * FMSの有効、無効切り替え
     */
    function setServerAvailability2($server_dsn_key, $server_address, $status) {
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                 "server_address" => $server_address,
                                                                 "status"         => $status));
        if ( !$server_dsn_key || !$server_address) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "server_address" => $server_address,));
            return false;
        }
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
        $where         = "server_address = '".addslashes($server_address)."'";
        if ($obj_FmsServer->numRows($where) == 0) {
            return false;
        }
        $status = ($status) ? 1 : 0;
        $data   = array("is_available" => $status);
        $ret    = $obj_FmsServer->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * データセンタ一覧
     */
    function getDataCenterList($server_dsn_key) {
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "server_dsn_key"=>$server_dsn_key));
        if ( !$server_dsn_key ) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array( "server_dsn_key"=>$server_dsn_key,));
            return false;
        }
        $dsn = $this->get_dsn($server_dsn_key);
        require_once("classes/core/dbi/DataCenter.dbi.php");
        $obj_Datacenter = new DBI_DataCenter( N2MY_MDB_DSN );
        $where = "";
        return $obj_Datacenter->getRowsAssoc($where);
    }

    /**
     * GlobalLink情報取得
     */
    function getGlobalLinkInfo($meeting_id) {
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                                "meeting_id"=>$meeting_id));
        $meeting_key = $this->_getMeetingKey($meeting_id); // getMeetingKey に失敗している場合の対応
        $error_result = array('status' => false);
        if (!$meeting_key) {
            $this->logger2->error($meeting_key);
            return $error_result;
        }
        $obj_MGMAuthClass = new MGM_AuthClass(N2MY_MDB_DSN);
        $dsn_info = $obj_MGMAuthClass->getRelationDsn($meeting_id, "meeting");
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting         = new DBI_Meeting($dsn_info["dsn"]);

        $obj_Meeting  = new DBI_Meeting($dsn_info["dsn"]);
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        $obj_FmsServer       = new DBI_FmsServer(N2MY_MDB_DSN);
        $meeting_info = $obj_Meeting->getRow(sprintf("meeting_key='%s' AND  is_deleted = 0" ,
                                                     mysql_real_escape_string($meeting_key)));
        if (!$meeting_info){
            $this->logger2->error("meeting_info not found". $meeting_key);
            return $error_result;
        }
        $where = "server_key = ".$meeting_info["server_key"];
        $fms_info = $obj_FmsServer->getRow($where);
        if ($fms_info["datacenter_key"] != "2001") {
            $result = array("status" => true,
                        "master_server_name" => "",
                        "master_protocol_port" => "",
                        "server_instance_directory" => "",
                        "server_token"         => null,
                        "fms_server_list" => "");
            return $result;
        }

        // SSLオプション
        require_once("classes/core/dbi/MeetingOptions.dbi.php");
        $obj_MeetingOptions  = new DBI_MeetingOptions($dsn_info["dsn"]);
        $sslOption = $obj_MeetingOptions->getRow(sprintf("meeting_key ='%s' AND option_key=1", $meeting_key ));
        $is_ssl = $sslOption ? 1 : 0;

        // FMSサーバ取得
        require_once("classes/dbi/fms_deny.dbi.php");
        try {
            $objFmsDeny = new FmsDenyTable( $dsn_info["dsn"] );
            $_fms_deny_keys = $objFmsDeny->findByUserKey($meeting_info["user_key"], null, null, 0, "fms_server_key");
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            return $error_result;
        }
        $fms_deny_keys = array();
        if ($_fms_deny_keys) {
            foreach ($_fms_deny_keys as $fms_key) {
                $fms_deny_keys[] = $fms_key["fms_server_key"];
            }
        }
        $sort = array(
                    "server_key"=>"ASC",
                    "maintenance_time"=>"ASC"
                    );
        $where = sprintf("datacenter_key = 2001 AND is_available = 1 AND is_ssl='%s'", $is_ssl);
        if ( $meeting_info["need_fms_version"] ) {
            $where .= " AND server_version >= '".mysql_real_escape_string($meeting_info["need_fms_version"])."'";
        }
        if ($fms_deny_keys) {
            $fms_deny_list_comma_separated = implode(",", $fms_deny_keys);
            $where .= " AND NOT server_key IN (". $fms_deny_list_comma_separated .")";
        }
        $fmsServerList = $obj_FmsServer->getRowsAssoc($where, $sort, null, 0, "server_address,local_address,maintenance_time,protocol_port");
        foreach ($fmsServerList as $server) {
            $front_fms["server_address"] = $server["local_address"] ? $server["local_address"] : $server["server_address"];
            $front_fms["protocol_port"] = $server["protocol_port"];
            $front_fms_list[] = $front_fms;
        }
        $result = array("status" => true,
                        "master_server_name" => $fms_info["local_address"],
                        "master_protocol_port" => $fms_info["protocol_port"],
                        "server_instance_directory" => $meeting_info["fms_path"],
                        "server_token"         => null,
                        "fms_server_list" => $front_fms_list);
        return $result;
    }

    public function startEchoCancel($server_dsn_key, $meeting_key, $participant_id, $machine_id)
    {
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        if (!$server_dsn_key|| !$meeting_key || !$participant_id || !$machine_id) {
            $this->logger2->error($server_dsn_key, $meeting_key, $participant_id, $machine_id);
            return false;
        }
        $data = array("meeting_key"    => $meeting_key,
                      "participant_id" => $participant_id,
                      "machine_id"     => $machine_id);
        require_once("classes/core/dbi/EchoCancelerLog.dbi.php");
        $dsn = $this->get_dsn($server_dsn_key);
        $echoCancelerLog = new EchoCancelerLog($dsn);
        $result = $echoCancelerLog->add($data);
        if (DB::isError($result)) {
            $this->logger2->error($result->getUserInfo());
            return false;
        }
        return true;
    }

    private function get_dsn( $server_dsn_key )
    {
        static $server_list;
        if (!$server_list) {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        }
        $dsn = $server_list["SERVER_LIST"][$server_dsn_key];
        if (!$dsn) {
            $this->logger2->warn($server_dsn_key, "DSN_KEY取得エラー");
            $dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
        }
        return $dsn;
    }

    private function eventlog($dsn, $meeting_key, $event_list) {
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        require_once("classes/core/dbi/EventLog.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        $event           = new DBI_EventLog($dsn);
        $obj_Participant = new DBI_Participant( $dsn );
        $participant_data = $obj_Participant->getList($meeting_key, false);
        foreach($participant_data as $key => $row) {
            $participant_list[$row["participant_session_id"]] = $row;
        }
        foreach($event_list as $key => $row) {
            $data = array(
                "meeting_key"            => $meeting_key,
                "participant_session_id" => $row["userId"],
                "participant_id"         => $participant_list[$row["userId"]]["participant_id"],
                "participant_name"       => $participant_list[$row["userId"]]["participant_name"],
                "participant_type"       => $row["type"],
                "event_id"               => $row["mode"],
                "data"                   => serialize($row["data"]),
                "create_datetime"        => date("Y-m-d H:i:s", ($row["unixTime"] / 1000)),
                "elapsed_time"           => ($row["relativeTime"] / 1000),
                );
            $ret = $event->add($data);
            if (DB::isError($ret)) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     */
    function postPingFailedData($server_dsn_key, $meeting_key, $meeting_sequence_key, $info) {
        $this->logger2->debug(array($server_dsn_key, $meeting_key, $meeting_sequence_key, $info));
        if ( !$server_dsn_key || !$meeting_key || !$meeting_sequence_key || !$info) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                     "meeting_key"    => $meeting_key,
                                                                     "meeting_sequence_key"    => $meeting_sequence_key,));
            return false;
        }
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        require_once("classes/dbi/ping_failed.dbi.php");
        $meeting_key = $this->_getMeetingKey($meeting_key); // getMeetingKey に失敗している場合の対応
        $dsn = $this->get_dsn($server_dsn_key);
        $objMeeting             = new DBI_Meeting($dsn);
        $objMeetingSequence     = new DBI_MeetingSequence($dsn);
        $objPingFailed          = new PingFailedTable($dsn);
        $where = "meeting_key = ".$meeting_key;
        if (!$user_key = $objMeeting->getOne($where, "user_key")) {
            return false;
        } else {
            $where = "meeting_key = ".$meeting_key.
                " AND meeting_sequence_key = ".$meeting_sequence_key;
            if (!$server_key = $objMeetingSequence->getOne($where, "server_key")) {
                return false;
            }
            foreach($info as $ping_failed_info) {
                $data = array(
                    'type'          => 'ping_failed',
                    'reload_type'   => $ping_failed_info['reloadType'],
                    'fms_key'       => $server_key,
                    'user_key'      => $user_key,
                    'meeting_key'   => $meeting_key,
                    'sequence_key'  => $meeting_sequence_key,
                    'p_session'     => $ping_failed_info['id'],
                    'p_login_time'  => date('Y-m-d H:i:s', $ping_failed_info['enteredTime'] / 1000),
                    'p_time'        => date('Y-m-d H:i:s', $ping_failed_info['time'] / 1000),
                    'p_protocol'    => $ping_failed_info['protocol'],
                    'p_port'        => $ping_failed_info['port'],
                    'p_number'      => $ping_failed_info['count'],
                    'p_count'       => $ping_failed_info['sequence'],
                    'fms_bytes_in'            => $ping_failed_info['statistics']['bytes_in'],
                    'fms_bytes_out'           => $ping_failed_info['statistics']['bytes_out'],
                    'fms_msg_in'              => $ping_failed_info['statistics']['msg_in'],
                    'fms_msg_out'             => $ping_failed_info['statistics']['msg_out'],
                    'fms_msg_dropped'         => $ping_failed_info['statistics']['msg_dropped'],
                    'fms_ping_rtt'            => $ping_failed_info['statistics']['ping_rtt'],
                    'fms_audio_queue_msgs'    => $ping_failed_info['statistics']['audio_queue_msgs'],
                    'fms_video_queue_msgs'    => $ping_failed_info['statistics']['video_queue_msgs'],
                    'fms_so_queue_msgs'       => $ping_failed_info['statistics']['so_queue_msgs'],
                    'fms_data_queue_msgs'     => $ping_failed_info['statistics']['data_queue_msgs'],
                    'fms_dropped_audio_msgs'  => $ping_failed_info['statistics']['dropped_audio_msgs'],
                    'fms_dropped_video_msgs'  => $ping_failed_info['statistics']['dropped_video_msgs'],
                    'fms_audio_queue_bytes'   => $ping_failed_info['statistics']['audio_queue_bytes'],
                    'fms_video_queue_bytes'   => $ping_failed_info['statistics']['video_queue_bytes'],
                    'fms_so_queue_bytes'      => $ping_failed_info['statistics']['so_queue_bytes'],
                    'fms_data_queue_bytes'    => $ping_failed_info['statistics']['data_queue_bytes'],
                    'fms_dropped_audio_bytes' => $ping_failed_info['statistics']['dropped_audio_bytes'],
                    'fms_dropped_video_bytes' => $ping_failed_info['statistics']['dropped_video_bytes'],
                    'fms_bw_out'              => $ping_failed_info['statistics']['bw_out'],
                    'fms_bw_in'               => $ping_failed_info['statistics']['bw_in'],
                    'fms_client_id'           => $ping_failed_info['statistics']['client_id'],
                    'create_datetime'         => date('Y-m-d H:i:s'),
                );
                $this->logger2->debug($data);
                $ret = $objPingFailed->add($data);
                if (DB::isError($ret)) {
                    $this->logger2->error($ret->getUserInfo());
                    return false;
                }
            }
        }
        return true;
    }

    //{{{ getTeleConf
    public function getTeleConf($server_dsn_key, $meeting_key)
    {
        $args_summary = " (args) dsn = $server_dsn_key , meeting_key= $meeting_key";
        $error_result = array('status' => false);

        $this->logger2->debug("getTeleConf is executed ".$args_summary);

        if (!$server_dsn_key || !$meeting_key) {
            $this->logger2->warn("parameter required". $args_summary);
            return $error_result;
        }

        $meeting_key = $this->_getMeetingKey($meeting_key);
        if (!$meeting_key){
            $this->logger2->warn("meeting key not found : ". $args_summary);
            return $error_result;
        }

        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting  = new DBI_Meeting($this->get_dsn($server_dsn_key));
        $meeting_info = $obj_Meeting->getRow(sprintf("meeting_key='%s' AND  is_deleted = 0" ,
                                                     mysql_real_escape_string($meeting_key)));
        if (!$meeting_info){
            $this->logger2->error("meeting_info not found". $args_summary);
            return $error_result;
        }

        require_once "classes/dbi/pgi_setting.dbi.php";
        require_once "classes/dbi/room.dbi.php";
        $pgiSettingTable = new PGiSettingTable($this->get_dsn($server_dsn_key));
        $roomTable       = new RoomTable($this->get_dsn($server_dsn_key));
        $setting         = $pgiSettingTable->findEnablleNow($meeting_info['room_key']);
        $room            = $roomTable->findByKey($meeting_info['room_key']);
        require_once("classes/pgi/config/PGiConfig.php");
        $pgiConfig = PGiConfig::getInstance();
        $pgi_status = true;

        require_once("classes/dbi/ordered_service_option.dbi.php");
        $obj_OrderedOption = new OrderedServiceOptionTable($this->get_dsn($server_dsn_key));
        $where_option = "room_key = '".$room["room_key"]."'".
                " AND service_option_key = 23".
                " AND ordered_service_option_status = 1";
        $teleconf_option = $obj_OrderedOption->getRow($where_option);
        $use_teleconf = false;
        if ($teleconf_option && $room["use_teleconf"]) {
            $use_teleconf = ($meeting_info["is_reserved"] && $meeting_info["tc_type"] == "voip") ? false : true;
        }

        $pgi_config = array('use_teleconf'    => $use_teleconf,
                            'use_dialin'      => $room['use_pgi_dialin']      && $meeting_info['use_pgi_dialin'],
                            'use_dialin_free' => $room['use_pgi_dialin_free'] && $meeting_info['use_pgi_dialin_free'],
                            'use_dialin_lo_call' => $room['use_pgi_dialin_lo_call'] && $meeting_info['use_pgi_dialin_lo_call'],
                            'use_dialout'     => $room['use_pgi_dialout']     && $meeting_info['use_pgi_dialout']);
        $pgi_info   = null;
        require_once "classes/pgi/PGiPhoneNumber.class.php";
        if ($meeting_info['pgi_api_status'] == 'complete') {

            $pgi_status = 'enable';
            if ($setting['pgi_setting_key'] == $meeting_info['pgi_setting_key']) {
                require_once('classes/pgi/PGiSystem.class.php');
                $pgi_phone_numbers = $this->mergeMultilangPhoneInfo(unserialize($meeting_info['pgi_phone_numbers']), $meeting_info);
                $dialout_regions = $this->getMultilangDialoutInfo();
                $pgi_info   = array('conference_id' => $meeting_info['pgi_conference_id'],
                                    'm_pass_code'   => wordwrap($meeting_info['pgi_m_pass_code'], 3, ' ', true),
                                    'p_pass_code'   => wordwrap($meeting_info['pgi_p_pass_code'], 3, ' ', true),
                                    'l_pass_code'   => wordwrap($meeting_info['pgi_l_pass_code'], 3, ' ', true),
                                    'phone_numbers' => $pgi_phone_numbers,
                                    'dialout_regions' => $dialout_regions);
                $pgi_system = PGiSystem::findBySystemKey($setting['system_key']);
                $pgi_config = array_merge($pgi_config, array("web_id" => (string)$pgi_system->webID,
                                                             "web_pw" => (string)$pgi_system->webPW,
                                                             "admin_client_id" => $setting["admin_client_id"] ? $setting["admin_client_id"] : (string)$pgi_system->adminClientID,
                                                             "admin_client_pw" => $setting["admin_client_pw"] ? $setting["admin_client_pw"] : (string)$pgi_system->adminClientPW,
                                                             "client_id" => $setting["client_id"] ? $setting["client_id"] : "",
                                                             "client_pw" => $setting["client_pw"] ? $setting["client_pw"] : "",
                                                             "pia_endpoint_host" => (string)$pgi_system->piaEndpointHost,
                                                             "pia_endpoint_path" => (string)$pgi_system->piaEndpointPath,
                                                             "pia_rest_endpoint_path" => (string)$pgi_system->piaRestEndpointPath,
                                                             "pia_endpoint_port" => (integer)$pgi_system->piaEndpointPort,
                                                             "service_name" => (string)$pgi_system->serviceName,
                                                             "pia_proxy_host" => (string)$pgi_system->piaProxyHost,
                                                             "pia_proxy_port" => (integer)$pgi_system->piaProxyPort,
                                                             //"pgi_sip_address" => $meeting_info["pgi_sip_address"] ? $meeting_info["pgi_sip_address"] : (string)$pgi_system->sipAddress,
                                                             "pgi_sip_address" => (string)$pgi_system->sipAddress,
                                                        ));
            } else {
                $obj_Meeting->update(array('pgi_api_status' => 'create'),
                                           "meeting_key=".mysql_real_escape_string($meeting_key));
                $this->createPGIWithBackground($meeting_info['meeting_ticket']);
            }
        } else {
            $pgi_status = ($meeting_info['pgi_api_status'] == 'error') ? 'disable' : 'loading';
        }

        $voip_info = array('tel_list' => array());
        $etc_info  = $meeting_info['tc_teleconf_note'];

        // conifg = array(
        if ($room['use_teleconf'] && $meeting_info['tc_type']) {
            $tc_type = $meeting_info['tc_type'];
        } else {
            $tc_type = 'voip';
        }
        return array('teleconf_type' => $tc_type,
                     'voip_info'     => $voip_info,
                     'pgi_info'      => $pgi_info,
                     'etc_info'      => $etc_info,
                     'config'        => $pgi_config,
                     'pgi_status'    => $pgi_status,
                     'status'        => true);
    }



    //{{{ createTeleConf
    public function createTeleConf($server_dsn_key, $meeting_key)
    {
        $error_result = array('status' => false);

        if (!$server_dsn_key || !$meeting_key) {
            $this->logger2->warn(array($server_dsn_key, $meeting_key), "parameter required");
            return $error_result;
        }

        $meeting_key = $this->_getMeetingKey($meeting_key);
        if (!$meeting_key){
            $this->logger2->warn(array($server_dsn_key, $meeting_key), "meeting key not found");
            return $error_result;
        }

        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting  = new DBI_Meeting($this->get_dsn($server_dsn_key));
        $meeting_info = $obj_Meeting->getRow(sprintf("meeting_key='%s' AND  is_deleted = 0" ,
                                                     mysql_real_escape_string($meeting_key)));
        if (!$meeting_info){
            $this->logger2->warn(array($server_dsn_key, $meeting_key), "meeting_info not found");
            return $error_result;
        }

        require_once "classes/dbi/pgi_setting.dbi.php";
        require_once "classes/dbi/room.dbi.php";
        $pgiSettingTable = new PGiSettingTable($this->get_dsn($server_dsn_key));
        $roomTable       = new RoomTable($this->get_dsn($server_dsn_key));
        $setting         = $pgiSettingTable->findEnablleNow($meeting_info['room_key']);
        $room            = $roomTable->findByKey($meeting_info['room_key']);
        require_once("classes/pgi/config/PGiConfig.php");
        $pgiConfig = PGiConfig::getInstance();
        $pgi_status = true;
        if ($meeting_info['pgi_api_status'] != 'complete' && $setting) {

            $obj_Meeting->update(array('pgi_api_status' => 'create'),
                                           "meeting_key=".mysql_real_escape_string($meeting_key));
            $this->createPGIWithBackground($meeting_info['meeting_ticket']);
        }
        $result = array('status' => true);
        return $result;
    }

    //}}}
    //{{{ setTeleConf
    public function setTeleConf($server_dsn_key, $meeting_key, $type, $etc_info)
    {
        require_once("classes/core/dbi/Meeting.dbi.php");

        $args_summary = " (args) server_dsn_key = $server_dsn_key , meeting_key = $meeting_key, ".
                        "type=$type, etc_info=$etc_info";

        $this->logger2->debug("setTeleConf is executed ".$args_summary);

        $error_result = array('status' => false);


        if (!$server_dsn_key || !$meeting_key) {
            $this->logger2->warn("parameter required". $args_summary);
            return $error_result;
        }

        if (!$this->validateTeleconf($type, $etc_info)) {
            $this->logger2->warn("validate error". $args_summary);
            return $error_result;
        }

        $meeting_key = $this->_getMeetingKey($meeting_key);
        if (!$meeting_key){
            $this->logger2->warn("meeting_key not found". $args_summary);
            return $error_result;
        }

        $where  = "meeting_key = '".$meeting_key."'";
        $data   = array("tc_type"         => $type,
                        "update_datetime" => date("Y-m-d H:i:s"));
        if ($type == 'etc') {
            $data['tc_teleconf_note'] = $etc_info;
        }

        $obj_Meeting = new DBI_Meeting( $this->get_dsn($server_dsn_key) );
        $res = $obj_Meeting->update($data, $where);
        if (DB::isError($res)) {
            $this->logger2->warn('update meeting faild : '.$res->getUserInfo());
            return $error_result;
        }

        return array('status' => true);
    }
    //}}}
    //{{{ validateTeleconf
    private function validateTeleconf($type, $etc_info)
    {
        if (!in_array($type, array('', 'voip', 'pgi', 'etc'))) {
            $this->logger2->warn("parameter required");
            return false;
        }

        if ($type == 'etc') {
            if (!$etc_info) {
                $this->logger2->warn("etc note is requied");
                return false;
            }
            if (strlen($etc_info) > 1000) {
                $this->logger2->warn("etc note length over");
                return false;
            }
        }

        return true;
    }

    //}}}
    //{{{ setTeleConfPinCd
    public function setTeleConfPinCd($server_dsn_key, $meeting_key, $pin_cd_count)
    {
        if ( !$server_dsn_key || !$meeting_key || !$pin_cd_count) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array(
                "server_dsn_key"=>$server_dsn_key,
                "meeting_key"   => $meeting_key,
                "pin_cd_count"  => $pin_cd_count,
                ));
            return false;
        }
        // ハッシュした会議キーから連番のキーを参照
        if (!$_meeting_key = $this->getMeetingKey($meeting_key)) return false;
        $meeting_key = $_meeting_key['meeting_key'];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array("server_dsn_key" => $server_dsn_key,
                                                                  "meeting_key"    => $meeting_key));
        require_once("classes/core/dbi/Meeting.dbi.php");
        $dsn          = $this->get_dsn($server_dsn_key);
        $obj_Meeting  = new DBI_Meeting( $dsn );
        $data = array("pgi_pincd_count" => $pin_cd_count);
        $res = $obj_Meeting->update( $data, sprintf( "meeting_key='%s'", $meeting_key ) );
        if (DB::isError($res)) {
            $this->logger2->warn('update meeting faild : '.$res->getUserInfo());
            return false;
        }
        return true;
    }
    //}}
    ///}}}
    public function getPolycomData($server_dsn_key, $meeting_key) {
        $args_summary = " (args) dsn = $server_dsn_key , meeting_key= $meeting_key";
        $error_result = array('status' => false);
        if (!$meeting_key) {
          $this->logger2->warn("parameter required". $args_summary);
          return $error_result;
        }
        $dsn = $this->get_dsn($server_dsn_key);

        $meeting_key = $this->_getMeetingKey($meeting_key);
        if (!$meeting_key){
            $this->logger2->warn("meeting key not found : ". $args_summary);
            return $error_result;
        }

        require_once("classes/core/dbi/Meeting.dbi.php");
        $objMeeting  = new DBI_Meeting($dsn);
        $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
        $meetingInfo = $objMeeting->getRow($where);
        // サービスオプション情報
        // $obj_N2MY_Account = new N2MY_Account($dsn);
        // $_room_info       = $obj_N2MY_Account->getRoomInfo($meetingInfo["room_key"]);
        // $room_info        = $_room_info["room_info"];
        // $options          = $_room_info["options"];

        if ( $videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"], false) ) {
          if ($settingInfo = $videoConferenceClass->getIvesInfoByRoomKey($meetingInfo["room_key"])) {
            $conferenceInfo =  $videoConferenceClass->getMcuGatewayByRoomKey($meetingInfo["room_key"]);
          }
        } else if ($controller = Resolver::getMobileController($meetingInfo["room_key"])) {
          $type = null;
          if ($meetingInfo["use_stb_option"]) {
             $type = "stb";
          }
          $conferenceInfo = $controller->createMcuGatewayForMobile($meetingInfo["room_key"], $type);
          
          // 電話連携のため保持
          if( !$meetingInfo['temporary_did'] ){
          	$objMeeting->update( array('temporary_did' => $conferenceInfo['did']), sprintf('meeting_key=%d', $meetingInfo['meeting_key']) );
          } else {
          	// temporary_did が 生成済みなら それを使う
          	$conferenceInfo['did'] = $meetingInfo['temporary_did'];
          }
        } else {
          $conferenceInfo = array(
              "status"			=> false,
              "contract"			=> false);
        }
        
        return $conferenceInfo;
    }

    public function forceExitPolycomClients($server_dsn_key, $meeting_key) {
        if (!$server_dsn_key || !$meeting_key) {
            $this->logger2->warn("parameter required");
            return array('status' => false);
        }

        $dsn = $this->get_dsn($server_dsn_key);

        require_once("classes/core/dbi/Meeting.dbi.php");
        $objMeeting  = new DBI_Meeting($dsn);
        $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
        $meetingInfo = $objMeeting->getRow($where);
        try {

          if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"]))) {
            if ($videoInfo = $videoConferenceClass->getActiveVideoConferenceByMeetingKey($meeting_key)) {
              $videoConferenceClass->forceExitPolycomClients($meeting_key);
            }
          }
            return true;
        }
        catch (SoapFault $fault) {
      $this->logger2->error($fault);
      return false;
        }
    }

    /**
     * polycom optionが付与されていない部屋にMobile端末が入室してきた際
     * SOAP::createConferenceExtを利用してカンファレンスを生成する
     *
     * @param int $server_dsn_key
     * @param int $meeting_key
     * @param string $did
     * @return bool
     */
    public function createVideoConferenceForMobile($server_dsn_key, $meeting_key, $did) {

      if (!$server_dsn_key || !$meeting_key || !$did) {
        $this->logger2->error(array("incorrect parameters: ", $server_dsn_key, $meeting_key, $did));
        return false;
      }

      $dsn = $this->get_dsn($server_dsn_key);
      try {
          require_once("classes/core/dbi/Meeting.dbi.php");
          $objMeeting  = new DBI_Meeting($dsn);
          $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
          $meetingInfo = $objMeeting->getRow($where);

        if ($controller = Resolver::getMobileController($meetingInfo["room_key"])) {
          $confId = $controller->createConference($meeting_key, $did);
        }
        return true;
      }
      catch (Exception $e) {
      	$this->logger->warn($e->getMessage());
        return false;
      }
    }

    public function removeVideoConferenceForMobile($server_dsn_key, $meeting_key) {
      if (!$server_dsn_key || !$meeting_key) {
        $this->logger2->warn(array($server_dsn_key, $meeting_key));
        return false;
      }

      $dsn = $this->get_dsn($server_dsn_key);
      try {
        require_once("classes/core/dbi/Meeting.dbi.php");
        $objMeeting  = new DBI_Meeting($dsn);
        $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
        $meetingInfo = $objMeeting->getRow($where);

        if ($controller = Resolver::getMobileController($meetingInfo["room_key"])) {
          $confId = $controller->removeConference($meeting_key);
        }
        return true;
      }
      catch (Exception $e) {
        return false;
      }
    }

    public function callParticipant($server_dsn_key, $meeting_key, $target, $protocol, $name=null) {
      if (!$server_dsn_key || !$meeting_key || !$target) {
        $this->logger2->warn(array($server_dsn_key, $meeting_key, $target));
        return false;
      }
      else {
        $this->logger2->info(array($server_dsn_key, $meeting_key, $target, $protocol, $name));
      }
      $dsn = $this->get_dsn($server_dsn_key);
      require_once("classes/core/dbi/Meeting.dbi.php");
      $objMeeting  = new DBI_Meeting($dsn);
      $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
      $meetingInfo = $objMeeting->getRow($where);
      if (!$meetingInfo)
        throw new Exception("meeting doesnt exist. meetingKey: ".$meeting_key);
      try {
        if ( $videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"], $protocol == 'pgi') ) {
          $participantId = $videoConferenceClass->callParticipant($meeting_key, $target, $protocol, $name);
          return array(
              "status"=>true,
              "participantKey"=>$participantId);
        } else {
          throw new Exception("conference does not exists. room_key: ".$meetingInfo["room_key"]);
        }
      }
      catch (Exception $e) {
        $this->logger2->warn($e->getMessage());
        return array(
              "status"=>false,
              "participantKey"=>$e->getCode());
      }
    }

   // new PGI from IVES
    public function cascadePgi($server_dsn_key, $meeting_key, $target ) {
    	$this->logger2->info( array($server_dsn_key, $meeting_key, $target), __FUNCTION__ );
    	require_once 'classes/dbi/video_conference.dbi.php';
    	$dbi_video_conference = new VideoConferenceTable( $this->get_dsn($server_dsn_key) );
    	$limit = 5;
    	do {
    		$video_conference_info = $dbi_video_conference->getRow( sprintf( 'meeting_key=%d AND is_active=1', $meeting_key ) );
    		if ( $video_conference_info ) break;
    		sleep( 1 );
    	} while ( --$limit );
    	
    	if ( !$video_conference_info ) {
    		$this->logger2->error( array($server_dsn_key, $meeting_key, $target), 'failed cascadePgi');
    		return false;
    	}
    	$this->logger2->debug( $video_conference_info, 'try cascadePgi' );
    	return $this->callParticipant($server_dsn_key, $meeting_key, $target, 'pgi');
    }

    public function cancelCallingParticipant($server_dsn_key, $meeting_key) {
      if (!$server_dsn_key || !$meeting_key) {
        $this->logger2->warn(array($server_dsn_key, $meeting_key));
        return false;
      }
      $dsn = $this->get_dsn($server_dsn_key);
      require_once("classes/core/dbi/Meeting.dbi.php");
      $objMeeting  = new DBI_Meeting($dsn);
      $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
      $meetingInfo = $objMeeting->getRow($where);
      if (!$meetingInfo)
        return false;
      try {
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"]))) {
          $videoConferenceClass->cancelCallingParticipant($meeting_key);
        }
        return true;
      }
      catch (Exception $e) {
        $this->logger2->warn($e->getMessage());
        return false;
      }
    }

    /**
     * fms <-> mcu socket接続完了後
     * videoconference.partIdとparticipantを結びつけるためのamf api
     * @param int $server_dsn_key
     * @param int $meeting_key
     * @param int $participant_key
     * @param string $session_id
     * @since mcu 1.4 -
     * @version 201304
     */
    public function setVCParticipantSessionid($server_dsn_key, $meeting_key, $participant_key, $session_id) {
    if (!$server_dsn_key || !$meeting_key || !$participant_key || !$session_id) {
    $this->logger2->warn(array($server_dsn_key, $meeting_key, $participant_key, $session_id));
    return false;
    }
    $dsn = $this->get_dsn($server_dsn_key);
    require_once("classes/core/dbi/Meeting.dbi.php");
    $objMeeting  = new DBI_Meeting($dsn);
    $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
    $meetingInfo = $objMeeting->getRow($where);
    $roomKey = $meetingInfo["room_key"];
      try {
          if (($controller = Resolver::getController($meetingInfo["room_key"]))) {
        if ($videoInfo = $controller->getActiveVideoConferenceByMeetingKey($meeting_key)) {
                $controller->addParticipantKey2VideoParticipantRecord($participant_key, $session_id, $roomKey, $meeting_key);
        }
      }
      return true;
    }
    catch (Exception $e) {
      $this->logger2->warn($e->getMessage());
      return false;
    }
    }

    public function setVideoMute($server_dsn_key, $meeting_key, $participant_key, $mute) {
      if (!$server_dsn_key || !$meeting_key || !$participant_key) {
        $this->logger2->warn(array($server_dsn_key, $meeting_key, $participant_key));
        return false;
      }
      $dsn = $this->get_dsn($server_dsn_key);
      require_once("classes/core/dbi/Meeting.dbi.php");
      $objMeeting  = new DBI_Meeting($dsn);
      $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
      $meetingInfo = $objMeeting->getRow($where);

      try {
        if (($controller = Resolver::getController($meetingInfo["room_key"]))) {
          if ($videoInfo = $controller->getActiveVideoConferenceByMeetingKey($meeting_key)) {
            $controller->setVideoMute($meeting_key, $participant_key, $mute);
          }
        }
        return true;
      }
      catch (Exception $e) {
        $this->logger2->warn($e->getMessage());
        return false;
      }
    }

    public function documentSharing($server_dsn_key, $meeting_key, $status, $bfcpStatus) {
      if (!$server_dsn_key || !$meeting_key) {
        $this->logger2->warn(array($server_dsn_key, $meeting_key));
        return false;
      }
      $dsn = $this->get_dsn($server_dsn_key);
      require_once("classes/core/dbi/Meeting.dbi.php");
      $objMeeting  = new DBI_Meeting($dsn);
      $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
      $meetingInfo = $objMeeting->getRow($where);

      try {
        if (($controller = Resolver::getController($meetingInfo["room_key"]))) {
          if ($videoInfo = $controller->getActiveVideoConferenceByMeetingKey($meeting_key)) {
            $status ? $controller->startDocumentSharing($meeting_key) : $controller->stopDocumentSharing($meeting_key, $bfcpStatus);
          }
        }
        return true;
      }
      catch (Exception $e) {
        $this->logger2->warn($e->getMessage());
        return false;
      }
    }

    public function changeDocumentSharing($server_dsn_key, $meeting_key, $documentId, $page) {
      $dsn = $this->get_dsn($server_dsn_key);
      require_once("classes/core/dbi/Meeting.dbi.php");
      $objMeeting  = new DBI_Meeting($dsn);
      $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
      $meetingInfo = $objMeeting->getRow($where);

      try {
        if (($controller = Resolver::getController($meetingInfo["room_key"]))) {
          if ($videoInfo = $controller->getActiveVideoConferenceByMeetingKey($meeting_key)) {
            $controller->documentSharing($meeting_key, $documentId, $page);
          }
        }
        return true;
      }
      catch (Exception $e) {
        $this->logger2->warn($e->getMessage());
        return false;
      }
    }

    public function noticeTelephonyComming($server_dsn_key, $meeting_key, $session_id) {
      return true;
      exit;
      if (!$server_dsn_key || !$meeting_key) {
        $this->logger2->warn(array($server_dsn_key, $meeting_key));
        return false;
      }
      $dsn = $this->get_dsn($server_dsn_key);
      require_once("classes/core/dbi/Meeting.dbi.php");
      $objMeeting  = new DBI_Meeting($dsn);
      $where       = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_deleted = 0";
      $meetingInfo = $objMeeting->getRow($where);

      try {
        if (($controller = Resolver::getController($meetingInfo["room_key"]))) {
          if ($videoInfo = $controller->getActiveVideoConferenceByMeetingKey($meeting_key)) {
            $controller->setMuteToTelephoneClient($meeting_key, $session_id);
          }
        }
        return true;
      }
      catch (Exception $e) {
        $this->logger2->warn($e->getMessage());
        return false;
      }
    }

    //}}
    ///}}}
    private function getClip($dsn, $meeting_key, $user_key ,$room_key = null)
    {
        $clips = $this->getMeetingClip($dsn, $meeting_key, $user_key);
        // 部屋に登録された動画を取得(セールス用)
        if($room_key != null){
            $room_clips = $this->getRoomClip($dsn, $room_key, $user_key);
            if(count($room_clips) >= 1){
                foreach($room_clips as $room_clip){
                    $clips[] = $room_clip;
                }
            }
        }
        if (count($clips) <= 0) {
            return array();
        }
        $res = array();
        require_once('classes/ClipFormat.class.php');
        foreach ($clips as $clip) {
            // Rでは下記の値を再生に使う
            $video_config = $this->config["VIDEO"];
            $_res = array('id'           => $clip['clip_id'],
                          'isConverting' => $clip['clip_status'] == 0,
                          'videoName'    => $clip['clip_key'],
                          'thumbnailURL' => ClipFormat::thumbnailUrl($clip['clip_key'], $clip['storage_no']),
                          'title'        => $clip['meeting_clip_name'],
                          'duration'     => (float)$clip['duration']);
            $servers = array();
            if ($video_config['use_limelight'] && $this->existFlvAtLimeLight($clip['clip_key'], $clip['storage_no'])) {
                $appInstance = ClipFormat::limeLightInctanceName($clip['storage_no']);
                $servers[]   = array('FQDN'        => $video_config['limelight_url'],
                                     'AppInstance' => $appInstance);
            }
            $servers[] = array('FQDN'        => $video_config['stream_fms_url'],
                               'AppInstance' => ClipFormat::streamFmsInctanceName($clip['storage_no']));

            $_res['primaryServerFQDN']        = $servers[0]['FQDN'];
            $_res['primaryServerAppInstance'] = $servers[0]['AppInstance'];
            if (!$video_config['use_limelight']) {
                $_res['primaryServerAppInstance'] = "/".$servers[0]['AppInstance'];
            }
            if (isset($servers[1])) {
                $_res['secondaryServerFQDN']        = $servers[1]['FQDN'];
                $_res['secondaryServerAppInstance'] = $servers[1]['AppInstance'];
            } else {
                $_res['secondaryServerFQDN']        = null;
                $_res['secondaryServerAppInstance'] = null;
            }
            $res[] = $_res;
        }

        return $res;
    }
    ///}}}
    ///}}}
    private function getMeetingClip($dsn, $meeting_key, $user_key)
    {
        require_once("classes/N2MY_Clip.class.php");
        $n2my_clip = new N2MY_CLip($dsn);
        return $n2my_clip->findClipByUsersMeetingKey($meeting_key, $user_key);
    }
    private function getRoomClip($dsn, $room_key, $user_key){
        require_once("classes/N2MY_Clip.class.php");
        $n2my_clip = new N2MY_CLip($dsn);
        return $n2my_clip->findClipByUsersRoomKey($room_key, $user_key);
    }
    ///}}}
    //}}}
    private function existFlvAtLimeLight($clip_key, $storage_no)
    {
        $head = get_headers(ClipFormat::limeLightFlvUrl($clip_key, $storage_no), true);
        list($p, $code) = explode(" ", $head[0]);
        return  $code == '200';
    }
    //{{{ createPGIWithBackground
    public function createPGIWithBackground($meeting_key)
    {
        $url = N2MY_LOCAL_URL."pgi/api_access.php?action_create_reservation".
                              "&meeting_key=$meeting_key";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $str = curl_exec($ch);
        curl_close($ch);
    }
    private function mergeMultilangPhoneInfo($phoneNumbers, $meeting)
    {
        $res = array();
        $pgi_phone_number_list = array();
        $phone_types       = array();
        if ($meeting['use_pgi_dialin_free']) {
            $phone_types[] = 'Free';
        }
        if ($meeting['use_pgi_dialin']) {
            $phone_types[] = 'Local';
            $phone_types[] = 'Toll';
        }
        if ($meeting['use_pgi_dialin_lo_call']) {
            $phone_types[] = 'Lo-call';
        }
        if ("ReadyConference" == $meeting["pgi_service_name"] || !$meeting["pgi_service_name"]) {
            $extract_conditions = array('Location' => 'Japan',
                                        'Types'    => $phone_types);
        } else if ("GlobalMeet" == $meeting["pgi_service_name"]) {
            $extract_conditions = array('Types'    => $phone_types);
        } else {
            $extract_conditions = array('Types'    => $phone_types);
        }
        $phoneNumbers = PGiPhoneNumber::extracts($phoneNumbers, $extract_conditions);
        foreach ($phoneNumbers as $k=>$v) {
            $l_config       = PGiPhoneNumber::findLocationConfigByID($v['LocationID']);
            $country = (string)$l_config->countryId ? (string)$l_config->countryId : "OTHER";
            $region = (string)$l_config->regionId ? (string)$l_config->regionId : "0";
            if (!$pgi_phone_number_list[$region]) {
                $r_config       = PGiPhoneNumber::findRegionConfigByRegion($region);
                $r_name = $r_config->name;
                $default_r_name = (string)$r_name->en_US;
                $pgi_phone_number_list[$region] =  array(
                        "RegionId" => (string)$r_config->id,
                        "RegionMultilang" => array(
                                                        'ja_JP' => (string)$r_name->ja_JP ? (string)$r_name->ja_JP : $default_r_name,
                                                        'en_US' => (string)$r_name->en_US ? (string)$r_name->en_US : $default_r_name,
                                                        'zh_CN' => (string)$r_name->zh_CN ? (string)$r_name->zh_CN : $default_r_name,
                                                        'zh_TW' => (string)$r_name->zh_TW ? (string)$r_name->zh_TW : $default_r_name,
                                                        'fr_FR' => (string)$r_name->fr_FR ? (string)$r_name->fr_FR : $default_r_name,
                                                        'th_TH' => (string)$r_name->th_TH ? (string)$r_name->th_TH : $default_r_name,
                                                        'in_ID' => (string)$r_name->in_ID ? (string)$r_name->in_ID : $default_r_name,
                                                        'ko_KR' => (string)$r_name->ko_KR ? (string)$r_name->ko_KR : $default_r_name,),
                );
            }
            if (!$pgi_phone_number_list[$region]["CountryList"][$country]) {
                $c_config       = PGiPhoneNumber::findCountryConfigById($country);
                $c_name = $c_config->name;
                $default_c_name = (string)$c_name->en_US;
                $pgi_phone_number_list[$region]["CountryList"][$country] = array (
                            "Id"            => $country,
                            "Code"            => (string)$c_config->code,
                            "CountryMultilang"=> array(
                                                        'ja_JP' => (string)$c_name->ja_JP ? (string)$c_name->ja_JP : $default_c_name,
                                                        'en_US' => (string)$c_name->en_US ? (string)$c_name->en_US : $default_c_name,
                                                        'zh_CN' => (string)$c_name->zh_CN ? (string)$c_name->zh_CN : $default_c_name,
                                                        'zh_TW' => (string)$c_name->zh_TW ? (string)$c_name->zh_TW : $default_c_name,
                                                        'fr_FR' => (string)$c_name->fr_FR ? (string)$c_name->fr_FR : $default_c_name,
                                                        'th_TH' => (string)$c_name->th_TH ? (string)$c_name->th_TH : $default_c_name,
                                                        'in_ID' => (string)$c_name->in_ID ? (string)$c_name->in_ID : $default_c_name,
                                                        'ko_KR' => (string)$c_name->ko_KR ? (string)$c_name->ko_KR : $default_c_name,),
                );
            }
            $l_name         = $l_config->name;
            $default_l_name = (string)$l_name->en_US;
            $v['LocationMultilang'] = array(
                                                        'ja_JP' => (string)$l_name->ja_JP ? (string)$l_name->ja_JP : $default_l_name,
                                                        'en_US' => (string)$l_name->en_US ? (string)$l_name->en_US : $default_l_name,
                                                        'zh_CN' => (string)$l_name->zh_CN ? (string)$l_name->zh_CN : $default_l_name,
                                                        'zh_TW' => (string)$l_name->zh_TW ? (string)$l_name->zh_TW : $default_l_name,
                                                        'fr_FR' => (string)$l_name->fr_FR ? (string)$l_name->fr_FR : $default_l_name,
                                                        'th_TH' => (string)$l_name->th_TH ? (string)$l_name->th_TH : $default_l_name,
                                                        'in_ID' => (string)$l_name->in_ID ? (string)$l_name->in_ID : $default_l_name,
                                                        'ko_KR' => (string)$l_name->ko_KR ? (string)$l_name->ko_KR : $default_l_name,);
            $t_config = PGiPhoneNumber::findTypeConfigByKey($v['Type']);
            $t_name   = $t_config->name;
            $default_t_name = (string)$t_name->en_US;
            $v['TypeMultilang'] = array(                'ja_JP' => (string)$t_name->ja_JP ? (string)$t_name->ja_JP : $default_t_name,
                                                        'en_US' => (string)$t_name->en_US ? (string)$t_name->en_US : $default_t_name,
                                                        'zh_CN' => (string)$t_name->zh_CN ? (string)$t_name->zh_CN : $default_t_name,
                                                        'zh_TW' => (string)$t_name->zh_TW ? (string)$t_name->zh_TW : $default_t_name,
                                                        'fr_FR' => (string)$t_name->fr_FR ? (string)$t_name->fr_FR : $default_t_name,
                                                        'th_TH' => (string)$t_name->th_TH ? (string)$t_name->th_TH : $default_t_name,
                                                        'in_ID' => (string)$t_name->in_ID ? (string)$t_name->in_ID : $default_t_name,
                                                        'ko_KR' => (string)$t_name->ko_KR ? (string)$t_name->ko_KR : $default_t_name,);
            $pgi_phone_number_list[$region]["CountryList"][$country]["DialIn"][] = $v;
        }
        $pgi_phone_number_list[8] = $pgi_phone_number_list[8]?$pgi_phone_number_list[8]:null;
        $this->logger2->debug($pgi_phone_number_list);
        return $pgi_phone_number_list;
    }

    private function getMultilangDialoutInfo() {
    	$pgi_out_dialout_list = array();
        $region_list = PGiPhoneNumber::getRegionConfig();
        foreach($region_list as $r_config) {
            $region = (string)$r_config->id;
            if (!$pgi_out_dialout_list[$region]) {
                $r_name = $r_config->name;
                $default_r_name = (string)$r_name->en_US;
                $pgi_out_dialout_list[$region] =  array(
                        "RegionId" => (string)$r_config->id,
                        "RegionMultilang" => array(
                                                        'ja_JP' => (string)$r_name->ja_JP ? (string)$r_name->ja_JP : $default_r_name,
                                                        'en_US' => (string)$r_name->en_US ? (string)$r_name->en_US : $default_r_name,
                                                        'zh_CN' => (string)$r_name->zh_CN ? (string)$r_name->zh_CN : $default_r_name,
                                                        'zh_TW' => (string)$r_name->zh_TW ? (string)$r_name->zh_TW : $default_r_name,
                                                        'fr_FR' => (string)$r_name->fr_FR ? (string)$r_name->fr_FR : $default_r_name,
                                                        'th_TH' => (string)$r_name->th_TH ? (string)$r_name->th_TH : $default_r_name,
                                                        'in_ID' => (string)$r_name->in_ID ? (string)$r_name->in_ID : $default_r_name,
                                                        'ko_KR' => (string)$r_name->ko_KR ? (string)$r_name->ko_KR : $default_r_name,),
                );
            }
        }
        $country_list = PGiPhoneNumber::getCountryConfig();
        foreach($country_list as $c_config) {
            $country = (string)$c_config->id;
            $region = (string)$c_config->regionId ? (string)$c_config->regionId : "0";
            if (!$pgi_out_dialout_list[$region]["CountryList"][$country]) {
                $c_name = $c_config->name;
                $default_c_name = (string)$c_name->en_US;
                $pgi_out_dialout_list[$region]["CountryList"][$country] = array (
                            "Id"            => $country,
                            "Code"            => (string)$c_config->code,
                            "CountryMultilang"=> array(
                                                        'ja_JP' => (string)$c_name->ja_JP ? (string)$c_name->ja_JP : $default_c_name,
                                                        'en_US' => (string)$c_name->en_US ? (string)$c_name->en_US : $default_c_name,
                                                        'zh_CN' => (string)$c_name->zh_CN ? (string)$c_name->zh_CN : $default_c_name,
                                                        'zh_TW' => (string)$c_name->zh_TW ? (string)$c_name->zh_TW : $default_c_name,
                                                        'fr_FR' => (string)$c_name->fr_FR ? (string)$c_name->fr_FR : $default_c_name,
                                                        'th_TH' => (string)$c_name->th_TH ? (string)$c_name->th_TH : $default_c_name,
                                                        'in_ID' => (string)$c_name->in_ID ? (string)$c_name->in_ID : $default_c_name,
                                                        'ko_KR' => (string)$c_name->ko_KR ? (string)$c_name->ko_KR : $default_c_name,),
                );
            }
        }
        $pgi_out_dialout_list[8] = $pgi_out_dialout_list[8]?$pgi_out_dialout_list[8]:null;
        $this->logger2->debug($pgi_out_dialout_list);
    	return $pgi_out_dialout_list;
    }

    private function getDocDir() {
        $default_doc_dir = 'docs';

        if(isset($this->config["CONTENT_DELIVERY"]["is_enabled"]) && $this->config["CONTENT_DELIVERY"]["is_enabled"]) {
            $doc_dir = isset($this->config["CONTENT_DELIVERY"]["doc_dir"]) ? $this->config["CONTENT_DELIVERY"]["doc_dir"] : '';
            if(strlen($doc_dir) == 0 || strstr($doc_dir, '..') !== false) {
                $doc_dir = $default_doc_dir;
            }
        }
        else {
            $doc_dir = $default_doc_dir;
        }

        if(!preg_match('/\/$/', $doc_dir)) {
            $doc_dir .= '/';
        }

        return $doc_dir;
    }

}
    
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
