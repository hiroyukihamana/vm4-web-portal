<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class DBI_MeetingUseLog extends N2MY_DB
{
    public $table = "meeting_use_log";
    public $logger = null;
    public $rules = array();
    protected $primary_key = "log_no";

    function __construct( $dsn )
    {
        $this->init( $dsn, $this->table );
    }

    function add( $data )
    {
        // 作成日時
        $_data = array(
            "room_key" => $data["room_key"],
            "meeting_key" => $data["meeting_key"],
            "meeting_tag" => $data["meeting_tag"],
            "create_datetime" => date("Y-m-d H:i:s")
            );
        parent::add($_data);
        return true;
    }

    function add_extension( $data )
    {
        // 作成日時
        $_data = array(
            "room_key" => $data["room_key"],
            "meeting_key" => $data["meeting_key"],
            "meeting_tag" => $data["meeting_tag"],
            "participant_id"  => $data["participant_id"],
            "type"            => $data["type"],
            "create_datetime" => date("Y-m-d H:i:s")
            );
        parent::add($_data);
        return true;
    }
}
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
