<?php
require_once( "classes/N2MY_DBI.class.php" );
require_once( "classes/core/dbi/Options.dbi.php" );

class DBI_MeetingOptions extends N2MY_DB
{
    public $table = "meeting_options";
    public $logger = null;
    public $rules = array();
    protected $primary_key = "meeting_options_key";

    function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
    }

    function add( $data )
    {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    function getList($meeting_key)
    {
        $where = "meeting_key = ".mysql_real_escape_string($meeting_key);
        $rows = $this->getRowsAssoc($where);
        $options = $this->_getOptions();
        foreach ($rows as $idx => $val) {
            $rows[$idx]['option_name'] = $options[$val["option_key"]]["option_name"];
        }
        return $rows;
    }

    function _getOptions() {
        $obj_Option = new DBI_Options();
        $rows = $obj_Option->getRowsAssoc("", null, null, null, "option_key,option_name", "option_key");
        return $rows;
    }
}
