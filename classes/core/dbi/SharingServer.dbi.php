<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class DBI_SharingServer extends N2MY_DB
{
    public $logger = null;
    public $rules = array();
    public $table = "sharing_server";
    public $obj_MGM_DB = null;
    protected $primary_key = "server_key";

    public function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
        $this->obj_MGM_DB = new N2MY_DB(N2MY_MDB_DSN);
    }

    public function getKeyByPriority($country, $is_ssl, $is_check = true)
    {
        static $num = 0;
        static $start_server_key;
        static $server_list = null;
        // パラメタチェック
        // サーバ一覧取得
        if ($server_list == null) {
            // 指定された地域のサーバ一覧
            $server_list = array();
            $sql  = "SELECT * FROM " .$this->table.
                    " WHERE server_country = '".addslashes($country)."'".
                    " ORDER BY server_count ASC,".
                    " server_priority DESC";
            $rs = $this->obj_MGM_DB->_conn->query($sql);
            if (DB::isError($rs)) {
                $this->logger2->error($rs->getUserInfo());
            } else {
                while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $server_list[] = $row;
                }
            }
            // 指定された地域以外のサーバ一覧
            $sql  = "SELECT * FROM " .$this->table.
                    " WHERE server_country != '".addslashes($country)."'".
                    " ORDER BY server_count ASC,".
                    " server_priority DESC";
            $rs = $this->obj_MGM_DB->_conn->query($sql);
            if (DB::isError($rs)) {
                $this->logger2->error($rs->getUserInfo());
            } else {
                while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $server_list[] = $row;
                }
            }
            if (count($server_list) == 0) {
                $this->logger2->error($sql);
                return false;
            }
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$server_list);
        }
        // 指定回数以上、または同一サーバキーに回ってきた場合は終了
        if ($num > 10) {
            $this->logger2->error($num);
            return false;
        }
        $server = $server_list[$num];
        $num++;
        if (!$start_server_key) {
            $start_server_key = $server['server_key'];
        } else {
            if ($start_server_key == $server['server_key']) {
                $this->logger2->error(array($server,$start_server_key), "The server that can be connected is not found.");
                return false;
            }
        }
        // 優先度変更
        $this->_counterIncrease($server['server_key']);
        return $server['server_key'];
    }

    private function _counterIncrease( $server_key )
    {
        $sql = sprintf( "UPDATE %s" .
                " set server_count = server_count + 1," .
                " update_datetime  = NOW()" .
                " WHERE server_key = %s", $this->table, $server_key );
        $ret = $this->obj_MGM_DB->_conn->query($sql);
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return false;
        }
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
