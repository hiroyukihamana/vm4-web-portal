<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class DBI_Meeting extends N2MY_DB
{
    public $table = "meeting";
    public $logger = null;
    public $rules = array();
    protected $primary_key = "meeting_key";

    function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
    }

    /**
     * ミーティングの内部キーをバージョンによってハッシュ化させたものを利用する
     */
    function getMeetingID($meeting_id, $field = '') {
        // 文字種によって、参照フィールドを切り替える
        if ((strlen($meeting_id) < 20) && is_numeric($meeting_id)) {
            $where = "meeting_key = '".addslashes($meeting_id)."'";
        } else {
            $where = "meeting_session_id = '".addslashes($meeting_id)."'";
        }
        // 会議キーとバージョン取得
        $row = $this->getRow($where, 'meeting_key,meeting_session_id,version');
        if (DB::isError($row)) {
            $this->logger2->error($row->getUserInfo());
            return false;
        } else {
            // フィールド指定あり
            if ($field) {
                return $row['meeting_key'];
            } else {
                // 旧バージョン
                if (strnatcasecmp($row['version'], '4.5.5.0') == -1) {
                    return $row['meeting_key'];
                // 4.5.5.0 以降
                } else {
                    return $row['meeting_session_id'];
                }
            }
        }
    }

    function update($data, $where) {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        $ret = parent::update($data, $where);
        return $ret;
    }

    public function getInfo( $meeting_key )
    {
        $where = sprintf( "meeting_key='%s' AND is_deleted=0", $meeting_key );
        return $this->getRowsAssoc( $where );
    }

    function add( $data )
    {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        parent::add($data);

        //get meeting_key
        $where = sprintf( "meeting_session_id='%s' AND is_deleted = 0 ", $data['meeting_session_id'] );
        $sort = array( "meeting_key" => "desc" );
        $result = $this->getRow( $where, "meeting_key", $sort );
        return $result["meeting_key"];
    }

    /**
     * 予約
     */
    function getNextMeeting($meeting_key) {
        $meeting_info = $this->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", $meeting_key ) );
        $where = "room_key = '".$meeting_info["room_key"]."'".
            " AND meeting_key <> ".$meeting_key.
            " AND meeting_start_datetime > '".$meeting_info["meeting_start_datetime"]."'".
            " AND meeting_stop_datetime  > '".date("Y-m-d H:i:s")."'".
            " AND is_active = 1".
            " AND is_deleted = 0";
        $sort = array("meeting_start_datetime" => "asc");
        $next_meeting_info = $this->getRow($where, "*", $sort, null, 0, "room_key");
        return $next_meeting_info;
    }

    function start() {
    }

    /**
     * 会議終了
     *
     * @param integer $meeting_key 会議キー
     * @param integer $has_recorded_minutes 録画フラグ
     * @param integer $has_recorded_video 議事録フラグ
     * @param integer $recorded_size FMS上のファイルサイズ
     * @param integer $use_time 利用時間
     *
     */
    function stop($meeting_key, $data) {
        $data["is_active"] = 0;
        $data["pin_cd"] = "";
        $where = "meeting_key = ".$meeting_key;
        $result = $this->update($data, $where);
        $this->logger2->debug($meeting_key);
        if (DB::isError($result) ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * 利用しているサイズ取得
     */
    function getUsedSize($room_key) {
        $where = "is_active = 0" .
            " AND is_deleted = 0".
            " AND meeting_use_minute > 0".
            " AND room_key = '".addslashes($room_key)."'";
        return $this->getOne($where, "SUM(meeting_size_used)");
    }

    /*
     * メンバー課金時メンバー全体での容量を取得する
     */
    public function getMemberUsedSize( $user_key ) {
        $where = "is_active = 0" .
            " AND is_deleted = 0".
            " AND user_key = '".addslashes($user_key)."'";
        return $this->getOne($where, "SUM(meeting_size_used)");
    }

    /**
     * 会議記録の保護
     */
    function lock($meeting_key) {
        $where = "meeting_key = ".addslashes($meeting_key);
        $data = array(
            "is_locked" => 1,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $ret = $this->update($data, $where);
        if ($ret) {
            $this->logger->error(__FUNCTION__."#result",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * 会議記録の保護解除
     */
    function unlock($meeting_key) {
        $where = "meeting_key = ".addslashes($meeting_key);
        $data = array(
            "is_locked" => 0,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $ret = $this->update($data, $where);
        if ($ret) {
            $this->logger->error(__FUNCTION__."#result",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * 会議入室制限
     */
    function blockUser($meeting_key) {
        $where = "meeting_key = ".addslashes($meeting_key);
        $data = array(
            "is_blocked" => 1,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $ret = $this->update($data, $where);
        if ($ret) {
            $this->logger->error(__FUNCTION__."#result",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * 会議入室制限解除
     */
    function unblockUser($meeting_key) {
        $where = "meeting_key = ".addslashes($meeting_key);
        $data = array(
            "is_blocked" => 0,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $ret = $this->update($data, $where);
        if ($ret) {
            $this->logger->error(__FUNCTION__."#result",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * 次の会議
     */
    function getNextKey($room_key, $meeting_key)
    {
        $meeting_info = "";
        $where = "room_key = '".addslashes($room_key)."'".
            " AND is_active = 1".
            " AND is_deleted = 0".
            " AND is_reserved = 1";
        if ($meeting_key) {
            $where .= " AND meeting_key <> '".addslashes($meeting_key)."'";
            $meeting_info = $this->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", $meeting_key ) );
        } else {
            $this->logger->debug("not meeting_key",__FILE__,__LINE__,$where);
        }
        if ($meeting_info) {
            $where .= " AND (" .
            " meeting_start_datetime > '".$meeting_info["meeting_start_datetime"]."'".
            " OR ('".date("Y-m-d H:i:s")."' BETWEEN meeting_start_datetime AND meeting_stop_datetime )" .
            " )";
        } else {
            $where .= " AND '".date("Y-m-d H:i:s")."' BETWEEN meeting_start_datetime AND meeting_stop_datetime";
        }
        $sort = array("meeting_start_datetime" => "asc");
        $next_meeting_info = $this->getRow($where, "meeting_key", $sort);
        return count( $next_meeting_info ) > 0 ? $next_meeting_info["meeting_key"] : false;
    }

    /**
     * 前の会議（いる？）
     */
    public function getPrevKey( $room_key, $meeting_key )
    {
        $meeting_info = $this->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", $meeting_key ) );
        $where = "room_key = '".addslashes($room_key)."'" .
            " AND meeting_key <> " .$meeting_key.
            " AND meeting_start_datetime < '".$meeting_info["meeting_start_datetime"]."'" .
            " AND is_active = 1" .
            " AND is_deleted = 0" .
            " AND is_reserved = 0";
        $sort = array( "meeting_stop_datetime" => "desc" );
        return $this->getOne( $where, "meeting_key", $sort, 1 );
    }

    public function getStatus()
    {
    }
}
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
