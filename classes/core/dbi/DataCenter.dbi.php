<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class DBI_DataCenter extends N2MY_DB
{
    public $logger = null;
    public $rules = array();
    public $table = "datacenter";
    protected $primary_key = "datacenter_key";

    function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
    }

    function getKeyByCountry($countery, $ignore_dc_list = null, $account_model = null, $use_chinaline_status = null, $use_global_link_status = null) {
        $where  = "datacenter_country = '".addslashes($countery)."'".
                  " AND status = 1";
        if ($use_chinaline_status == 1) {
            $where .= " AND datacenter_key < 1101";
        } else if ($use_chinaline_status == "2") {
            $where .= " AND datacenter_key < 1201";
        } else if ($use_global_link_status == "1") {
            $where .= " AND datacenter_key = 2001";
        } else {
            $where .= " AND datacenter_key < 1001";
        }
        if ($ignore_dc_list) {
            $ignore_dc_list_comma_separated = implode(",", $ignore_dc_list);
            $where .= " AND NOT datacenter_key IN (".
                $ignore_dc_list_comma_separated .")";
        }
        if ($account_model == "free") {
            $where .= " AND free_use_flg = 1";
        }
        $sort = array("sort" => "ASC");
        $coulumns = " `datacenter_key` ";
        $rows = $this->getRowsAssoc($where, $sort, null, null, $coulumns, null);
        $this->logger2->debug($where);
        if (DB::isError($rows)) {
            $this->logger->error($rows->getUserInfo());
            return false;
        } else {
            foreach ($rows as $row) {
                $datacenter_list[] = $row["datacenter_key"];
            }
        }
        $this->logger2->debug($datacenter_list);
        return $datacenter_list;
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
