<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class DBI_DocumentConvertProcess extends N2MY_DB
{
    var $table = "convert_process";

    function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
