<?php

require_once("classes/N2MY_DBI.class.php");

class DBI_Options extends N2MY_DB
{
    public $table = "options";
    public $logger = null;
    public $rules = array();
    protected $primary_key = "option_key";

    function __construct()
    {
        $this->init( N2MY_MDB_DSN, $this->table );
    }

    function getDetail( $where)
    {
        return $this->getRow( $where );
    }

    function getList() {
        $option_list = $this->getRowsAssoc("", array(), null, 0, "option_key, option_name", "option_key");
        return $option_list;
    }
}
