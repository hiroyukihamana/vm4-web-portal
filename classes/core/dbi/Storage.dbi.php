<?php

require_once("classes/N2MY_DBI.class.php");

define("STORAGE_STATUS_WAIT",    0);
define("STORAGE_STATUS_DO",      1);
define("STORAGE_STATUS_SUCCESS", 2);
define("STORAGE_STATUS_DELETE",  3);
define("STORAGE_STATUS_ERROR",  -1);
define("STORAGE_STATUS_PAGE_ERROR",  -3);

class DBI_StorageFile extends N2MY_DB
{
    public $table = "storage_file";
    public $logger = null;
    protected $primary_key = "storage_file_key";

    function __construct( $dsn )
    {
        $this->init( $dsn, $this->table );
    }

    public function add($data)
    {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    public function update($data, $where)
    {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }

    /**
     * ドキュメント情報一覧を取得する
     *
     * @access public
     * @return array
     */
    function getList($user_id)
    {
        //check parameters
        if (!$user_id) {
            $this->logger->warn(__FUNCTION__.'invalid argument',__FILE__,__LINE__,array(
                'user_id' => $user_id,
                ));
            return array();
        }

        //prepare data
        $d   = array();
        $d[] = $user_id;

        //create sql
        $sql  = " select";
        $sql .= "   storage_id      , ";
        $sql .= "   storage_key       , ";
        $sql .= "   member_key      , ";
        $sql .= "   storage_territory , ";
        $sql .= "   storage_conversion_status , ";
        $sql .= "   storage_file_category , ";
        $sql .= "   storage_file_extension, ";
        $sql .= "   storage_status   , ";
        $sql .= "   storage_index    , ";
        $sql .= "   storage_sort     , ";
        $sql .= "   create_datetime   , ";
        $sql .= "   update_datetime     ";
        $sql .= " from ";
        $sql .=     $this->_tbl;
        $sql .= " where";
        $sql .= "   user_id     = ? ";
        $sql .= " and ";
        $sql .= "   storage_status = 1 ";
        $sql .= " order by ";
        $sql .= "   storage_sort asc ";

        //prepare
        $sth = $this->_conn->prepare($sql);
        if (PEAR::isError($sth)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$sth);
            //return $sth;
            return array();
        }

        //run query
        $result =& $this->_conn->execute($sth, $d);
        if (PEAR::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result);
            //return $result;
            return array();
        }

        //fetchRow
        $ra = array();
        while ($r =& $result->fetchRow(DB_FETCHMODE_ASSOC)) {
            if (PEAR::isError($r)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$r);
                //return $r;
                return array();
            }

            $ra[] = $r;
        }

        return $ra;
    }

// }}}
// {{{ -- getStatus()

    /**
     * ドキュメントステータス情報を取得する
     *
     * @access public
     * @param  string  $storage_id ドキュメントID
     * @return integer
     */
    function getStatus($storage_id)
    {
        //check parameters
        if (!$storage_id) {
            $this->logger->warn(__FUNCTION__.'invalid argument',__FILE__,__LINE__,array(
                'storage_id' => $storage_id,
                ));
            return false;
        }

        //prepare data
        $d   = array();
        $d[] = $storage_id;

        //create sql
        $sql  = " select";
        $sql .= "   storage_status ";
        $sql .= " from ";
        $sql .=     $this->_tbl;
        $sql .= " where";
        $sql .= "   storage_id = ?";
        $sql .= " limit 1 ;";

        //prepare
        $sth = $this->_conn->prepare($sql);
        if (PEAR::isError($sth)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$sth);
            //return $sth;
            return false;
        }

        //run query
        $result =& $this->_conn->execute($sth, $d);
        if (PEAR::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result);
            //return $result;
            return false;
        }

        //fetchRow
        $r =& $result->fetchRow(DB_FETCHMODE_ASSOC);
        if (PEAR::isError($r)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$r);
            //return $r;
            return false;
        }

        return $r['storage_status'];
    }

//  }}}
//  {{{ -- removeDocuments()
    /**
     * 指定されたユーザーIDのドキュメントを格納ディレクトリごと削除する
     *
     * @access public
     * @param  string  $dir ディレクトリパス
     * @return boolean
     */
    function removeDocuments($dir)
    {
        $result = $this->_rmdir($dir);

        return $result;
    }

    /**
     * 使用サイズ取得
     * @param unknown_type $user_key
     * @return void|unknown
     */
    function get_total_size($user_key){
        $sql = "SELECT " .
                " SUM(file_size) as total_size " .
                " FROM storage_file ";
        $where = " WHERE user_key=".$user_key.
                 " AND (status != 3)";

        $sql = $sql.$where;

        $rs = $this->_conn->query( $sql );
        if (DB::isError($rs)) {
            //$this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
            return ;
        }
        $row    = $rs->fetchRow(DB_FETCHMODE_ASSOC);
        return $row["total_size"];

    }

    /**
     *
     */
    function getDetail($storage_id) {
        $where = "storage_id = ".$storage_id;
        $rs = $this->select($where, null, null, 1, null);
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
            return $rs;
        }
        $storage_row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
        return $storage_row;
    }
}

