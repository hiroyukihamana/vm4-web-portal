<?php
require_once("classes/N2MY_DBI.class.php");

class EchoCancelerLog extends N2MY_DB
{
    public $table = "echo_canceler_log";
    public $logger = null;
    public $rules = array();
//    protected $primary_key = "meeting_key";

    function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
    }
    
    public function add($data)
    {
        $data["start_datetime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }
}
