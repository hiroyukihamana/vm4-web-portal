<?php
require_once("classes/N2MY_DBI.class.php");

class NotificationTable extends N2MY_DB {

    var $table = 'notification';
    protected $primary_key = "id";
    var $rules = array(
    );

    function __construct($dsn) {
        $this->init( $dsn, $this->table );
    }

    /**
     * 指定時間のメンテナンス数を取得
     */
    function check($starttime, $endtime) {
        $where = "status = 1" .
                " AND end_datetime != '".$starttime."'" .
                " AND start_datetime != '".$endtime."'" .
                " AND (" .
                "( start_datetime between '".$starttime."' AND '".$endtime."' )" .
                " OR ( end_datetime between '".$starttime."' AND '".$endtime."' )" .
                " OR ( '".$starttime."' BETWEEN start_datetime AND end_datetime )" .
                " OR ( '".$endtime."' BETWEEN start_datetime AND end_datetime ) )";
        $count = $this->numRows($where);
        return $count;
    }
}
