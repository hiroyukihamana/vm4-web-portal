<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");
require_once("classes/mgm/dbi/FmsServer.dbi.php");

class DBI_MeetingSequence extends N2MY_DB
{
    public $logger = null;
    public $rules = array();
    public $table = "meeting_sequence";
    private $dsn;
    private $config;
    protected $primary_key = "meeting_sequence_key";

    public function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
        $this->dsn = $dsn;
        $this->config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
    }

    public function add( $data )
    {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $ret = parent::add( $data );
        return $ret;
    }

    public function update( $data, $where )
    {
        if (!$where) {
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__, $data);
            return false;
        }
        $data["update_datetime"] = date("Y-m-d H:i:s");
        $ret = parent::update($data, $where);
        return $ret;
    }

    public function change( $data )
    {
        $this->add( $data );
    }

    public function getActiveSequenceInfo($meetingKey)
    {
        $sort = array("meeting_sequence_key"=>"desc");
        $where = sprintf("meeting_key='%s' AND status=1", $meetingKey);
        $meetingSequenceList = $this->getRowsAssoc($where, $sort);

        if (DB::isError($meetingSequenceList)) {
            $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$meetingSequenceList);
            return false;
        }
        return $meetingSequenceList[0];
    }

    public function chagenSequenceData($meetingKey, $serverKey=null)
    {
        $data = array(
            "status"       => 0,
            "end_datetime" => date('Y-m-d H:i:s'),
        );
        $where = sprintf("meeting_key='%s' AND status=1", $meetingKey);
        $this->update($data, $where);

        $addData = array(
                    "meeting_key"	=> $meetingKey,
                    "status"		=> 1
                    );
        if ($serverKey) $addData["server_key"] = $serverKey;
        return $this->add($addData);
    }

    /**
     * 状態取得
     *
     */
    function getStatus($meeting_key) {
        $where = "meeting_key = ".$meeting_key;
        $rows = $this->getRowsAssoc($where, null, null, null, "meeting_size_used, has_recorded_minutes, has_recorded_video, has_shared_memo");
        if (DB::isError($rows)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rows->getUserInfo());
            return false;
        }
        $res = array(
            "has_recorded_minutes" => 0,
            "has_recorded_video"   => 0,
            "meeting_size_used"    => 0
            );
        foreach($rows as $row) {
            if($row["has_recorded_minutes"]) {
                $res["has_recorded_minutes"] = 1;
            }
            if($row["has_recorded_video"]) {
                $res["has_recorded_video"] = 1;
            }
            if($row["has_shared_memo"]){
                $res["has_shared_memo"] = 1;
            }
            $res["meeting_size_used"] += $row["meeting_size_used"];
        }
        return $res;
    }

    /**
     * 終了
     */
    function stop() {
    }

    /**
     * 議事録削除
     */
    function deleteRecordedMinutes($meeting_sequence_key) {
        $data = array(
            "meeting_sequence_key" => $meeting_sequence_key,
            "has_recorded_minutes" => 0
            );
        return $this->update($data);
    }

    /**
     * 録画記録削除
     */
    function deleteRecordedVideo($meeting_sequence_key) {
        $data = array(
            "meeting_sequence_key" => $meeting_sequence_key,
            "has_recorded_video"   => 0
            );
        return $this->update($data);
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
