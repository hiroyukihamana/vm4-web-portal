<?php
require_once ("config/config.inc.php");
require_once ("lib/EZLib/EZCore/EZFrame.class.php");
require_once ("lib/EZLib/EZUtil/EZDate.class.php");
require_once ("lib/EZLib/EZUtil/EZFile.class.php");
require_once ("lib/EZLib/EZSession/EZSession.class.php");
require_once ("lib/EZLib/EZHTML/EZTemplate.class.php");
require_once ("lib/pear/DB.php");
require_once ("lib/EZLib/EZXML/EZXML.class.php");

class AppFrame extends EZFrame {

    var $_lang = "ja";
    var $_core_url = "";
    var $provider_id = "";
    var $provider_pw = "";
    var $_dsn = "";
    // EZFrame
    var $logger = null;
    var $session = null;
    var $request = null;
    var $config = null;
    var $_message = array();
    var $_service_info = array();
    var $_app_dir = null;
    var $menu = null;
    var $user_auth = "";

    /**
     * N2MY全体で共通の初期処理
     * @todo 言語ファイルの取得はクラスに分けてどこからでも参照できたほうがよいかもしれない。
     */
    function _init() {
        $cookie_param = session_get_cookie_params();
        define("N2MY_COOKIE_PATH", "/");
        define("N2MY_COOKIE_SECURE", $cookie_param["secure"]);
        define("N2MY_COOKIE_HTTPONRY", $cookie_param["httponly"]);
        define("N2MY_COOKIE_LIFETIME", time() + 365 * 24 * 3600);
        // 空で良いか要検討
        define("N2MY_COOKIE_DOMAIN", "");
        $this->request->get("n2my_session");
        // Framework
        $this->logger    = EZLogger::getInstance();
        // Template
        $this->template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
        // Session
        if (!defined('N2MY_SESS_NAME')) {
            define('N2MY_SESS_NAME', N2MY_SESSION);
        }
        $session_id = $this->request->get(N2MY_SESS_NAME);
        if ($session_id) {
            $this->logger2->debug($session_id);
            // セッションの開始
            session_id($session_id);
        }
        $this->session   = EZSession::getInstance();
        // 言語設定
        $this->_lang = $this->get_language();
        /*
        $this->initMenu();
        $tree = $this->getMenu($this->menu, "ur31");
        $this->logger2->info($tree);
        */
        // サーバタイムゾーン
        if (!defined("N2MY_SERVER_TIMEZONE")) {
            date_default_timezone_set($this->config->get('GLOBAL', 'time_zone_name', date_default_timezone_get()));
            $_tz = (int)(substr( date( 'O' ), 0, 3));
            // サーバータイムゾーン設定
            define("N2MY_SERVER_TIMEZONE", $_tz);
        }
        // ユーザ指定タイムゾーン
        if (!defined("N2MY_USER_TIMEZONE")) {
            $time_zone = $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE);
            define("N2MY_USER_TIMEZONE", $time_zone);
        }
        // DB接続先
        $this->_auth_dsn = $this->config->get("GLOBAL", "auth_dsn");
        define("N2MY_MDB_DSN", $this->_auth_dsn);
        // 言語別のメッセージファイルを読み込み（DBに移行して、そこから自動生成する方法が楽かもしれない）
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$this->_lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        $custom = $this->config->get('SMARTY_DIR','custom');
        $this->logger2->debug($this->session->get("service_mode"));
        if ($this->session->get("service_mode") == "sales") {
            $custom = "sales";
        } elseif($custom == "sales" && $this->session->get("login")) {
            unset($custom);
        }
        if (isset($custom) && file_exists(N2MY_APP_DIR."config/custom/".$custom."/".$this->_lang."/message.ini")) {
            $custome_service_info_file = N2MY_APP_DIR."config/custom/".$custom."/".$this->_lang."/message.ini";
            $this->_service_info = parse_ini_file($custome_service_info_file, true);
        }
        // 既存のコード用に定数処理（最終的な移行時は削除）
        $define_keys = array_keys($this->_message["DEFINE"]);
        foreach ($define_keys as $_key => $key_name) {
            if (defined($key_name)) {
                $this->logger->warn(__FUNCTION__, __FILE__, __LINE__, $key_name);
            } else {
                $key_value = $this->get_message("DEFINE", $key_name, "");
                define($key_name, $key_value);
            }
        }
        // アプリケーションディレクトリ
        $this->_app_dir = N2MY_APP_DIR;
        //ユーザートラッキング改善によるログ収集情報の追加・変更
        if(empty($_COOKIE['UUID'])) {
            $uuid = sprintf('%s_%s', date('YmdHis'), sha1(uniqid(rand(), true)));
            setcookie('UUID', $uuid, 2147483647, '/');
        }
    }

    /**
     * ログイン認証チェック
     * デフォルトはエラーを表示する
     *
     * @param boolean $err_display エラー表示あり
     */
    function checkAuth($err_display = true) {
    	$url = substr($_SERVER['REQUEST_URI'],0,strrpos($_SERVER['REQUEST_URI'],"?"));
    	$is_ondemand_process = $url == "/api/flash/meetinglog.php" || $url == "/api/meeting_config/log_video/layout_as3.php";
        if (!$this->session->get('login')){
            if ($err_display) {
                $this->template->assign('message', SESSION_ERROR);
                $this->display('user/session_error.t.html');
                exit();
            } else {
                return false;
            }
        } else if (!$is_ondemand_process && ((($this->session->get("login_type") == "invite" || $this->session->get("customer") == "invite" || $this->session->get("login_type") == "invitedGuest" ) && ($this->user_auth  != "invite")) ||
                   ($this->session->get("login_type") == "onetime_url" && $this->user_auth  != "onetime_url"))) {
        	$this->logger2->info($_SERVER['REQUEST_URI']);
        	if ($err_display) {
                $this->template->assign('message', SESSION_ERROR);
                $this->display('user/session_error.t.html');
                exit();
            } else {
                return false;
            }
        } else if($this->session->get('ondemand') && !$is_ondemand_process && !$this->session->get("user_info")) {
        	if ($err_display) {
        		$this->template->assign('message', SESSION_ERROR);
        		$this->display('user/session_error.t.html');
        		exit();
        	} else {
        		return false;
        	}
        }
        return true;
    }

    /**
     * 管理者認証チェック
     * デフォルトはエラーを表示する
     *
     * @param boolean $err_display エラー商事あり
     */
    function checkAdminAuth($err_display = true) {
        $checkAuth = $this->checkAuth( $err_display );
        $admin_login = $this->session->get( 'admin_login' ) || $this->session->get( 'center_admin_login' );

        if ( $checkAuth && $admin_login ) {
            return true;
        }

        if( $err_display ){
            $this->template->assign( 'message', SESSION_ERROR );
            $this->display( 'admin/session_error.t.html' );
            exit();
        }
        return false;
    }

    /**
     * タイムゾーン一覧取得
     */
    function get_timezone_list() {
        static $timezone_list;
        if (!$timezone_list) {
            $_time = gettimeofday();

            $is_summertime = $this->getSummerTime();
            if ($_time['dsttime'] == 0) {
                // 通常時
                $_timezone_list = $this->get_message("TIME_ZONE");
            }else{
                // サマータイム補正
                $_timezone_list = $this->get_message("TIME_ZONE_SUMMER");
            }
            $timezone_allow = $this->config->get("N2MY", "timezone_allow");
            $timezone_deny = $this->config->get("N2MY", "timezone_deny");
            foreach($_timezone_list as $timezone_key => $timezone_value) {
                $timezone_key = (string)$timezone_key;
                if ($timezone_allow) {
                    $allow_list = split(",", $timezone_allow);
                    if (in_array($timezone_key, $allow_list)) {
                        $timezone_list[$timezone_key] = array(
                            "key" => (string)$timezone_key,
                            "value" => $timezone_value);
                    }
                } elseif ($timezone_deny) {
                    $deny_list = split(",", $timezone_deny);
                    if (!in_array($timezone_key, $deny_list)) {
                        $timezone_list[$timezone_key] = array(
                            "key" => (string)$timezone_key,
                            "value" => $timezone_value);
                    }
                } else {
                    $timezone_list[$timezone_key] = array(
                        "key" => (string)$timezone_key,
                        "value" => $timezone_value);
                }
            }
        }
        return $timezone_list;
    }

    function getSummerTime(){
        $defaultTimeZone = date("e");
        date_default_timezone_set('America/Los_Angeles');
        $sum = date("I");
        date_default_timezone_set($defaultTimeZone);
        return $sum;
    }

    /**
     * メッセージ取得
     *
     * @param string $group グループ
     * @param string $param パラメタ
     * @param mixed $default デフォルト値
     */
    function get_message($group, $param = null, $default = "")
    {
        if ($param === null) {
            if(isset($this->_message[$group]) &&
                $this->_message[$group] !== ''){
              return $this->_message[$group];
            } else{
              return $default;
            }
        } else {
            if(isset($this->_message[$group][$param]) &&
                $this->_message[$group][$param] !== ''){
              return $this->_message[$group][$param];
            } else{
              return $default;
            }
        }
    }

    function get_service_info($group, $param = null, $default = "")
    {
        if ($param === null) {
            if(isset($this->_service_info[$group]) &&
                $this->_service_info[$group] !== ''){
              return $this->_service_info[$group];
            } else{
              return $default;
            }
        } else {
            if(isset($this->_service_info[$group][$param]) &&
                $this->_service_info[$group][$param] !== ''){
              return $this->_service_info[$group][$param];
            } else{
              return $default;
            }
        }
    }

    /**
     * 地域コード一覧取得
     */
    function get_country_list() {
        $_country_list = $this->get_message("COUNTRY");
        $country_allow = $this->config->get("N2MY", "country_allow");
        $country_deny = $this->config->get("N2MY", "country_deny");
        foreach($_country_list as $country_key => $country_value) {
            list($datacenter_key, $country_name) = split(":",$country_value);
            if ($country_allow) {
                $allow_list = split(",", $country_allow);
                if (in_array($country_key, $allow_list)) {
                    $country_list[$country_key] = array(
                        "country_key" => $datacenter_key,
                        "country_name"   => $country_name,
                    );
                }
            } elseif ($country_deny) {
                $deny_list = split(",", $country_deny);
                if (!in_array($country_key, $deny_list)) {
                    $country_list[$country_key] = array(
                        "country_key" => $datacenter_key,
                        "country_name"   => $country_name,
                    );
                }
            } else {
                $country_list[$country_key] = array(
                    "country_key" => $datacenter_key,
                    "country_name"   => $country_name,
                );
            }
        }
        return $country_list;
    }

   function _get_country_key ($country_key) {
        //MaxMindでIPから所在地を検索
        $remote_addr =  $_SERVER["REMOTE_ADDR"];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$remote_addr);
        $maxmind_url = $this->config->get("MAXMIND", "url");
        $maxmind_key = $this->config->get("MAXMIND", "key");
        $result_country_key = "";
        if ($maxmind_url && $maxmind_key && $country_key == "auto") {
            require ("lib/maxmind/geoip.php");
            $obj_MaxMind = new MaxMaindApp();
            $result_country_key = $obj_MaxMind->getCountryCord($maxmind_url, $maxmind_key, $remote_addr);
            return $result_country_key;
        } else {
           return false;
        }
    }

    function get_language_list($common = false) {
        if ($common) {
            $_lang_list = $this->get_message("LOGIN_LANG");
        } else {
            $_lang_list = $this->get_message("LANGUAGE");
        }
        $lang_allow = $this->config->get("N2MY", "lang_allow");
        $lang_deny = $this->config->get("N2MY", "lang_deny");
        $user_info = $this->session->get("user_info");
        foreach($_lang_list as $lang_key => $lang_value) {
            $lang_name = $lang_value;
            if ($lang_allow) {
                $allow_list = split(",", $lang_allow);
                if (in_array($lang_key, $allow_list)) {
                    $lang_list[$lang_key] = $lang_name;
                }
            } elseif ($lang_deny) {
                $deny_list = split(",", $lang_deny);
                if (!in_array($lang_key, $deny_list)) {
                    $lang_list[$lang_key] = $lang_name;
                }
            } else {
                $lang_list[$lang_key] = $lang_name;
            }

            if($user_info["lang_allow"]){
                $user_lang_list = split("," , $user_info["lang_allow"]);
                if(!in_array($lang_key, $user_lang_list)){
                    unset($lang_list[$lang_key]);
                }
            }
        }
        return $lang_list;
    }

    /**
     * DSN取得
     */
    function get_dsn() {
        if ($this->_dsn) {
            return $this->_dsn;
        }
        if( $serverdata = $this->session->get("server_info") ){
            $this->_dsn = $serverdata["dsn"];
        } else {
//            $this->logger2->error("DSN_KEY取得エラー");
            $this->_dsn = $this->get_dsn_value($this->get_dsn_key());
        }
        return $this->_dsn;
    }

    /**
     * DSN取得
     */
    function get_dsn_key() {
        if( $serverdata = $this->session->get("server_info") ){
            return $serverdata["host_name"];
        } else {
            return N2MY_DEFAULT_DB;
            /*
            $this->template->assign('message', SESSION_ERROR);
            $this->display('user/session_error.t.html');
            return N2MY_DEFAULT_DB;
            */
        }
    }

    /**
     * DSN取得
     */
    function get_auth_dsn() {
        return $this->_auth_dsn;
    }

    /**
     * DSNキーから直接指定
     */
    function get_dsn_value($server_dsn_key) {
        static $dsn;
        if ($dsn) {
            return $dsn;
        }
        $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        if (isset($server_list["SERVER_LIST"][$server_dsn_key])) {
            $dsn = $server_list["SERVER_LIST"][$server_dsn_key];
        } else {
            $this->logger2->error($server_dsn_key, "DSN_KEY取得エラー");
            $dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
        }
        return $dsn;
    }

    /**
     *
     */
    function wget($url) {
        $_url = parse_url($url);
        if ($_url["scheme"] == "https") {
            $url = "http://";
            if ($_url["user"]) {
                $url .= $_url["user"].":";
            }
            if ($_url["pass"]) {
                $url .= $_url["pass"]."@";
            }
            $url .= $_url["host"].$_url["path"];
            if ($_url["query"]) {
                $url .= "?".$_url["query"];
            }
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$url);
        }
        $contents = "";
        $fp = @fopen($url, "r");
        if ($fp == false) {
            $this->logger->error(__FUNCTION__."#not found error", __FILE__, __LINE__, $url);
            return "";
        } else {
            while (!feof($fp)) {
                $contents .= fread($fp, 1024);
            }
            fclose($fp);
        }
        return $contents;
    }

    /**
     *_
     */
    function api_execute($url) {
        $contents = $this->wget($url);
        if ($contents == "") {
            return array();
        } else {
            $xml = new EZXML();
            $data = $xml->openXML($contents,"");
        }
        return $data;
    }

    /**
     * 会議室の状態を取得
     *
     * @return boolean true:会議中、false:空室
     */
    function api_room_status($meeting_key) {
        $query = array(
            "meeting_ticket" => $meeting_key,
            "provider_id" => $this->provider_id,
            "provider_password" => $this->provider_pw,
        );
        $url = $this->get_core_url("api/meeting/MeetingStatus.php", $query);
        $data = $this->api_execute($url);
        $status = $data["Meeting"]["status"][0]["_data"];
        return $status;
    }

    /**
     * ブラウザの言語設定から優先度が高い識別IDを返す
     *
     * @return string 言語識別ID（jp:日本[デフォルト]、en:英語）
     * @todo EZLibのユーティリティ関係のクラスに持って行きたい
     */
    function get_language() {
        // キーはすべて小文字
        $_language_pages = array (
            "af" => "af",
            "ar" => "ar",
            "bg" => "bg",
            "ca" => "ca",
            "cs" => "cs",
            "cy" => "cy",
            "da" => "da",
            "de" => "de",
            "el" => "el",
            "en" => "en_US",
            "es" => "es",
            "et" => "et",
            "fa" => "fa",
            "fi" => "fi",
            "fr" => "fr_FR",
            "ga" => "ga",
            "gl" => "gl",
            "he" => "he",
            "hi" => "hi",
            "hr" => "hr",
            "hu" => "hu",
            "in" => "in_ID",
            "is" => "is",
            "it" => "it",
            "ja" => "ja_JP",
            "ko" => "ko_KR",
            "lt" => "lt",
            "lv" => "lv",
            "mk" => "mk",
            "ms" => "ms",
            "mt" => "mt",
            "nl" => "nl",
            "no" => "no",
            "pl" => "pl",
            "pt" => "pt",
            "ro" => "ro",
            "ru" => "ru",
            "sk" => "sk",
            "sl" => "sl",
            "sq" => "sq",
            "sr" => "sr",
            "sv" => "sv",
            "sw" => "sw",
            "th" => "th_TH",
            "tl" => "tl",
            "tr" => "tr",
            "uk" => "uk",
            "vi" => "vi",
            "yi" => "yi",
            "zh" => "zh_CN",
            "zh-cn" => "zh_CN",
            "zh-tw" => "zh_TW"
        );
        // 設定ファイルで有効な言語のみ
        $lang_allow = $this->config->get("N2MY", "lang_allow");
        $lang_deny  = $this->config->get("N2MY", "lang_deny");
        foreach($_language_pages as $lang_key => $lang_value) {
            if ($lang_allow) {
                $allow_list = split(",", $lang_allow);
                if (in_array($lang_key, $allow_list)) {
                    $language_pages[$lang_key] = $lang_value;
                }
            } elseif ($lang_deny) {
                $deny_list = split(",", $lang_deny);
                if (!in_array($lang_key, $deny_list)) {
                    $language_pages[$lang_key] = $lang_value;
                }
            } else {
                $language_pages[$lang_key] = $lang_value;
            }
        }
        // セッション・クッキーから取得
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$this->session->get("lang"));
        if (!$lang = $this->session->get("lang")) {
            $lang = $this->request->getCookie("lang");
        }
        if ($lang) {
            if (array_key_exists($lang, $language_pages)) {
                return $language_pages[$lang];
            }
        }
        // 取得できなかった場合のデフォルト
        $language_default = ($this->config->get("N2MY", "default_lang")) ? $this->config->get("N2MY", "default_lang") : "en";
        $http_accept_language = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '';
        if (empty($http_accept_language)) {
            return $language_pages[$language_default];
        }
        // 小文字に変換／スペース削除
        $http_accept_language = strtolower($http_accept_language);
        $accept_language = str_replace(" ", "", $http_accept_language);
        $languages = explode(",", $accept_language);
        // 優先度でチェック
        foreach ($languages as $language) {
            list($lang) = explode(";", $language);
            // ２バイト
            if (array_key_exists($lang, $language_pages)) {
                return $language_pages[$lang];
            } else {
                if (strpos($lang, "-") !== false) {
                    $lang = substr($lang, 0, strpos($lang, "-"));
                }
                if (array_key_exists($lang, $language_pages)) {
                    return $language_pages[$lang];
                }
            }
        }
        // なかったら
        return $language_pages[$language_default];
    }

    /**
     * テンプレートディレクトリを言語別に指定
     */
    function _set_template_dir($lang = null) {
        static $base_template_dir;
        // 言語指定あり
        if ($lang) {
            $this->_lang = $lang;
        } else {
            if (!$this->_lang) {
                $this->_lang = $this->get_language();
            }
        }
        // テンプレートの元ディレクトリ
        if (!$base_template_dir) {
            $base_template_dir = $this->template->template_dir;
        }
//        $this->template->template_dir = $base_template_dir . $this->_lang;
//        $this->template->template_dir = $base_template_dir;
    }

    /**
     * HTMLのテンプレート出力
     */
    function display($template_file, $encrypt = false) {
        $this->logger->trace(__FUNCTION__ . "#start", __FILE__, __LINE__);
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$_COOKIE);
        $this->_set_template_dir();
//        $template_path = $this->template->template_dir."/".$template_file;
        // 多言語かテンプレートのみ読み込み
        $custom = $this->config->get('SMARTY_DIR','custom');
        if ($this->session->get("service_mode") == "sales" && !$custom) {
            $custom = 'custom/sales/common/';
        } else if ($custom == "sales" && $this->session->get("service_mode") != "sales" && $this->session->get("login")) {
        	$custom = "common";
        } else if ($custom == "") {
            $custom = 'common';
        } else  {
            $custom = 'custom/'.$custom.'/common/';
        }

        $this->template->assign("custom_dir", $custom);
        $this->template->assign("template_dir", $this->template->template_dir);
        if (file_exists($this->template->template_dir.$custom.'/'.$template_file)) {
            $template_file = $this->template->template_dir.$custom.'/'.$template_file;
        } else {
            $template_file = sprintf( "%s/%s", "common", $template_file );
            $template_path = sprintf( "%s%s", $this->template->template_dir, $template_file );
            if (!file_exists($template_path)) {
                $this->logger->error(__FUNCTION__."#template file not found", __FILE__, __LINE__, $template_path);
                $this->_set_template_dir($this->_lang);
                $template_file = "common/error.t.html";
            } else {
                $this->logger->debug(__FUNCTION__ . "#template_file", __FILE__, __LINE__, $template_path);
            }
        }
        $char_set = $this->config->get("GLOBAL", "html_output_char", "UTF-8");
        $this->template->assign("char_set", $char_set);

        //モバイル判別
        if (strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod") || strpos($_SERVER['HTTP_USER_AGENT'], "iPad;")) {
            $frame["view_mobile_flg"] = 1;
        }

        //ポータルはちまき判定
        $config_ptl = $this->config->getAll('VCUBE_PORTAL');
        if( $config_ptl['is_display_header'] && $this->session->get( 'vid_info' ) ){
			$portal_header = sprintf( 'http%s://%s/%s', $config_ptl['isSSL'] ? 's' : '', $config_ptl['domain'], $config_ptl['header_endpoint'] );
			$this->template->assign('portal_header', $portal_header);
        }

        //言語KEY
        $lang_key = substr($this->_lang,0,2);
        if($lang_key == "zh")
          $lang_key = str_replace("_", "-", strtolower($this->_lang));
        // ログインタイプでヘッダフッタ切り替え
        $frame["user_info"]             = $this->session->get("user_info");
        $frame["session_id"]            = session_id();
        $frame["member_info"]           = $this->session->get("member_info");
        $frame["google_analytics_cd"]   = $this->config->get("N2MY", "google_analytics_cd");
        $frame["login_type"]            = $this->session->get("login_type");
        $frame["lang"]                  = $this->_lang;
        $frame["lang_key"]              = $lang_key;
        $frame["lang_list"]             = $this->get_language_list();
        $frame["common_lang_list"]      = $this->get_language_list(true);
        $frame["lang_dir"]              = $this->_lang.'/';
        $frame["service_mode"]          = $this->session->get("service_mode")?$this->session->get("service_mode"): null;
        $frame["use_mode_change"]       = $this->session->get("use_mode_change")?$this->session->get("use_mode_change"): 0;
        $frame["is_member_login"]       = $this->session->get("member_info") ? 1 : 0 ;
        $frame["role"]                  = $this->session->get("role") ? $this->session->get("role") : 0 ;
        // サービス提供情報
        if (isset($custom) && !empty($this->_service_info)) {
            $frame["service_info"]          = $this->get_service_info("SERVICE_INFO");
        } else {
            $frame["service_info"]          = $this->get_message("SERVICE_INFO");
        }
        $this->logger->trace("service_info",__FILE__,__LINE__,$frame["service_info"]);

        //非表示設定
        $ignore_menu = $this->config->getAll("IGNORE_MENU");
        if( $frame["login_type"] == "invite" ||
            $frame["login_type"] == "invitedGuest" ||
          $frame["login_type"] == "presence" ||
            ($frame["member_info"] &&
                ($frame["user_info"]['account_model'] == "member" ||
                 $frame["user_info"]['account_model'] == "centre" ||
                 $frame["user_info"]['account_model'] == "free")
                )
            ) {
            $ignore_menu["login_as_admin"] = 1;
        }
        // アカウントモデルによる上書き
        if( $frame["user_info"]["account_model"] == "member" || $frame["user_info"]["account_model"] == "centre" || $frame["user_info"]["account_model"] == "free" ){
            if ($frame["user_info"]["account_model"] == "free") {
                $ignore_menu["enter_as_audience"]   = 1;
            }
            $ignore_menu["tool_presence_app"]   = 1;
            $ignore_menu["audience_manual"]     = 1;
            $ignore_menu["advertise"]           = 1;
            $ignore_menu["display_address"]     = 1;
            // メンバーでのログイン
            if( $frame["member_info"] && !$frame["user_info"]["member_multi_room_flg"]){
                $ignore_menu["display_room_list"] = 1;
                $ignore_menu["room_select"] = 1;
            }
            // ユーザーでのログイン
            else if (!$frame["member_info"]) {
                $ignore_menu["enter_meeting"] = 1;
            }
            if ($frame["user_info"]["account_model"] != "centre" ) {
                $ignore_menu["admin_log_plan"]       = 1;
                $ignore_menu["admin_log_fee"]        = 1;
            }
        } else if ((!$frame["member_info"] && ($frame["user_info"]["use_sales"] && $frame["service_mode"] == "sales")) || ($frame["user_info"]["use_stb"])) {
            //セールスモードユーザーでログインの際は入室ボタン非表示
            $ignore_menu["enter_meeting"] = 1;
        }
        if($frame["user_info"]["max_storage_size"] == 0){
            $ignore_menu["use_storage"] = 1;
        }


        $frame["ignore_menu"] = $ignore_menu;
        $frame["n2my"] = $this->config->getAll("N2MY");
        // ユーザー告知を表示
        if ($frame["n2my"]['maintenance_notification']) {
            if (N2MY_MDB_DSN) {
                require_once('classes/core/dbi/Notification.dbi.php');
                $objNotification = new NotificationTable(N2MY_MDB_DSN);
                $notification_visible_day = $frame["n2my"]["maintenance_notification_visible_day"] ? $frame["n2my"]["maintenance_notification_visible_day"] : 14;
                $where = "start_datetime <= '".date('Y-m-d H:i:s', time() + 3600 * 24 * $notification_visible_day)."'" .
                    " AND end_datetime >= '".date('Y-m-d H:i:s')."'" .
                    " AND status = 1";
                if (DB::isError($objNotification)) {
                    $this->logger2->error($objNotification->getUserInfo());
                } else {
                    $notification_data = $objNotification->getRow($where, null, array("start_datetime" => "asc"));
                    if ($notification_data) {
                        $frame['maintenance']['info']           = $notification_data['info'];
                        $frame['maintenance']['start_datetime'] = EZDate::getLocateTime($notification_data['start_datetime'], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE);
                        $frame['maintenance']['end_datetime']   = EZDate::getLocateTime($notification_data['end_datetime'], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE);
                        switch ($this->_lang) {
                            case 'ja_JP':
                                $url = $notification_data['url_ja'];
                                break;
                            case 'en_US':
                                $url = $notification_data['url_en'];
                                break;
                            case 'zh_CN':
                                $url = $notification_data['url_zh'];
                                break;
                        }
                        $frame['maintenance']['url'] = $url ? $url : $notification_data['url_'.$notification_data['default_lang']];
                        if ($frame['maintenance']['url']) {
                            $url_info = parse_url($frame['maintenance']['url']);
                            $frame['maintenance']['target'] = ($url_info['host'] == $_SERVER['HTTP_HOST']) ? "_self" : "_blank";
                        }
                    }
                }
            }
        }
        $EZDate = new EZDate();
        date_default_timezone_set($this->config->get('GLOBAL', 'time_zone_name', date_default_timezone_get()));
        $server_time_zone = (int)(substr( date( 'O' ), 0, 3));
        $time_zone = $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE);
        $frame["message"]           = $this->_message["DEFINE"];
        $frame["base_url"]          = N2MY_BASE_URL;
        $frame["base_dir"]          = "/";
        $frame["self"]              = $_SERVER["SCRIPT_NAME"];
        $frame["debug"]             = $this->config->get("N2MY", "debug");
        $frame["country_key"]       = $this->session->get("country_key", _EZSESSION_NAMESPACE, "auto");
        $frame["selected_country_key"]       = $this->session->get("selected_country_key", _EZSESSION_NAMESPACE, "auto");
        $frame["country_list"]      = $this->get_country_list();
        $frame["passage_time"]      = $this->_get_passage_time();
        $frame["server_time"]       = time();
        $frame["server_time_zone"]  = $server_time_zone;
        $frame["time_zone"]         = $time_zone;
        $frame["time_zone_list"]    = $this->get_timezone_list();
        $frame["user_time"]         = $EZDate->getLocateTime(time(), $time_zone, $server_time_zone);
        $frame["room_list"]         = $this->get_room_info();
        $frame["center_admin_login"] = $this->session->get("center_admin_login");
        $frame["vcubeid_login"]     = $this->session->get("vcubeid_login");
        $frame['isVersion4650'] = 0;
        if($frame["user_info"]["meeting_version"] != null){
            $frame["version"] = $this->_getMeetingVersion();
        }else{
            $frame["version"] = '4.6.5.0';
            $frame["isVersion4650"] = 1;
        }
        //MTGVFOUR-1164
        if($frame['user_info'] && ($frame['service_mode'] == 'sales') && $this->session->getAll()){
            require_once('classes/dbi/browser_title.dbi.php');
            $objBrowserTitle = new BrowserTitleTable($this->get_dsn());
            $browserTitle = $objBrowserTitle->getTitle( $frame['user_info']['user_key']);
            $frame['browser_title'] = htmlspecialchars($browserTitle);
            require_once('classes/dbi/sales_setting_image.dbi.php');
            $objSalesSettingImage = new SalesSettingImageTable($this->get_dsn());
            $customHeaderImage = $objSalesSettingImage->getImageInfo($frame['user_info']['user_key'], 'header');
            if($customHeaderImage){
                $customHeaderImagePath =  N2MY_BASE_URL . '/api/meeting_config/log_minutes/customLogo.php?image_id=' . $customHeaderImage['image_id'];
                $this->template->assign('customHeader', $customHeaderImagePath);
            }
        }
        $frame["meeting_version"] = $this->_getMeetingVersion();
        $this->template->assign("__frame",$frame);
        // エコメーター
        $this->template->assign("eco_report",   isset($_COOKIE["eco_report"]) ? $_COOKIE["eco_report"] : "");
        $this->template->assign("user_station", isset($_COOKIE["personal_station"]) ? $_COOKIE["personal_station"] : "" );
        // テンプレートの拡張子でContent-Type判定
        switch (substr($template_file, strrpos($template_file, '.') + 1)) {
            case "xml":
                if (!$encrypt) {
                    header("Content-Type: text/xml; charset=UTF-8;");
                }
                break;
            default:
                header("Expires: Fri, 01 Jan 1990 00:00:00 GMT");
                header("Content-Type: text/html; charset=".$char_set.";");
                header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0;");
                header("Pragma: no-cache");
                break;
        }
        $view_modal = 0;
        if($frame["user_info"]["promotion_flg"] == 1  && ($frame["user_info"]["meeting_version"] != null && $frame["user_info"]["meeting_version"] != "" ) && $this->_has_teleconferenc_use_room() ){
            $view_modal =  1;
        }
        $this->template->assign("view_modal" , $view_modal);
        $this->template->assign("login_type" , $this->session->get('login_type'));
        $is_one_account = $this->check_one_account();
        $this->template->assign("is_one_account", $is_one_account);
        $is_ss_watcher_account = $this->check_SSWatcher_account();
        if($this->session->get("user_info") && !$this->session->get("member_info")){
            $this->template->assign("is_user_account", true);
        }
        $this->template->assign("is_ss_watcher_account", $is_ss_watcher_account);
        $this->template->display($template_file, null, null, $encrypt);
        $this->template->assign("is_one_account", $this->check_one_account());
        return true;
    }

    function initMenu() {
        require_once("lib/pear/XML/Unserializer.php");
        $options = array(
            "parseAttributes" => true,
            "attributesArray" => "_attr",
            "returnResult" => true,
            );
        $us = new XML_Unserializer($options);
        $path = N2MY_APP_DIR."config/".$this->_lang."/menu.xml";
        $data = $us->unserialize($path, true);
        if (PEAR::isError($data)) {
            $this->logger2->info(array($path, $data->getMessage()));
        } else {
            $this->menu = $data;
        }
    }

    function getMenu($a, $id) {
        static $tree = array();
        foreach ($a as $key => $_a) {
            if ($key != "_attr") {
                //$this->logger2->info($key);
                $this->getMenu($_a, $id);
            } else {
                if ($_a["id"] == $id) {
//                    $this->logger2->info($tree);
                } else {
                    $tree[] = $_a;
//                    $this->logger2->info($_a);
                }
            }
            $tree = array();
        }
        return $tree;
    }

    /**
     * テンプレート変換したテキストのみ取得
     *
     * @param string $template_file テンプレートファイル
     */
    function fetch($template_file) {
        $this->logger->trace(__FUNCTION__ . "#start", __FILE__, __LINE__);
        $this->_set_template_dir();
        return $this->template->fetch($template_file);
    }

    /**
     * URIのスキーマ、ホスト、ポートを判定し
     * サイト内のリダイレクト先URLを返します。
     *
     * @access public
     * @param string $path URLのパス指定
     * @param array $query GETリクエストを連想配列形式で指定
     * @return string リダイレクト先のURI
     */
    function get_redirect_url($path, $query = array ()) {
        $this->logger->trace(__FUNCTION__ . "#start", __FILE__, __LINE__);
        $query_str = "";
        foreach ($query as $_key => $_value) {
            $query_str .= "&" . $_key . "=" . urlencode($_value);
        }
        if ($query_str) {
            $query_str = "?" . substr($query_str, 1);
        }
        // URL生成
        $url = N2MY_BASE_URL. $path . $query_str;
        $this->logger2->trace($url);
        return $url;
    }

    /**
     * ユーザごとのワークエリアを作成して、パスを返す
     */
    function get_work_dir()
    {
        $user_info = $this->session->get("user_info");
        $client_dir = $this->_app_dir."/var/".$_SERVER["REMOTE_ADDR"]."/".$user_info["user_id"];
        if (!is_dir($client_dir)) {
            if (!EZFile::mkdir_r($client_dir, 0777)) {
                $this->logger2->error($client_dir, "#error_mkdir");
            } else {
                chmod($client_dir, 0777);
            }
        }
        return $client_dir;
    }

    /**
     * ログイン時、ログアウト時などにワークエリア内のファイルを削除
     */
    function clear_dir()
    {

    }


    /**
     * フォームのダブルクリックや、リロードによる二重処理を回避するキーの発行
     * 利用時はテンプレート変数に __submit_key を作成し、受け取り側の処理で
     * check_submit_key()で発行したキーの有効性をチェックしてください。
     *
     * @param string $name_space
     */
    function set_submit_key($name_space = null) {
        if ($name_space == null) {
            $name_space = md5(__FILE__);
        }
        $submit_key = md5(microtime());
        $this->session->set("__SUBMIT_KEY", $submit_key, $name_space);
        $this->template->assign("__submit_key", $submit_key);
    }

    /**
     * フォームのダブルクリックや、リロードによる二重処理を回避するキーのチェック
     *
     * @param string $name_space
     * @return boolean true:正常、false:異常
     */
    function check_submit_key($name_space = null) {
        if ($name_space == null) {
            $name_space = md5(__FILE__);
        }
        $submit_key = $this->session->get('__SUBMIT_KEY', $name_space);
        $in = $this->request->get("__submit_key");
        // キー削除
        $this->session->remove('__SUBMIT_KEY', $name_space);
        if ($in === $submit_key) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 契約した部屋以外のroom_keyを参照させない
     */
    function get_room_key($default = null) {
        $room_key = $this->request->get("room_key", $default);
        // 指定がない場合もOKとする
        if ($room_key == "") {
            return "";
        }
        $room_list = $this->session->get("room_info");
        foreach($room_list as $_key => $room_info) {
            if ($_key == $room_key) {
                return $room_key;
            }
        }
        $this->logger2->warn(array($room_list, $room_key));
        $this->display("error.t.html");
        exit;
    }

	/**
	 * 会議室一覧取得<br />
	 * @param string $is_solve_ext_opt オプション情報の拡張@重負荷 (video_conference_number, h264_use_flg)
	 * @param string $is_get_all_user_room true: all not deleted room, false: own room or solved white_list (管理者ページ用)
	 * @return array
	 */
	function get_room_list($is_solve_ext_opt = false, $is_get_all_user_room = false) {
		$this->session->remove( 'room_info' );
		require_once ("classes/N2MY_Account.class.php");
		$obj_N2MY_Account = new N2MY_Account( $this->get_dsn() );
		$user_info = $this->session->get( "user_info" );
		$member_info = $this->session->get( "member_info" );
		$service_mode = $this->session->get( "service_mode" );

		$room_list = array ();
		if( $is_get_all_user_room || ! $member_info ){
			// 削除された部屋は除く 全会議
			$room_list = $obj_N2MY_Account->getRoomList( $user_info["user_key"], $service_mode );
		} else {
			switch ( true ) {
				case $this->check_one_account() && $service_mode == "meeting" :
					// One Meeting
					$room_list = $this->get_ONE_whitelist_room_list( $user_info["user_key"], $member_info["member_key"], false );
					break;
				case $user_info["account_model"] == "free" :
					// Freeの場合はサービスごとの部屋を取得
					$custom = $this->config->get( 'SMARTY_DIR', 'custom' );
					$service_name = $custom == "paperless" ? "Paperless_Free" : "Meeting_Free";
					$room_list = $obj_N2MY_Account->getFreeRoom( $member_info["member_key"], $user_info["user_key"], $service_name );
					break;
				case $user_info["account_model"] == "member"
						|| $user_info["account_model"] == "centre"
						|| $member_info["room_key"]
						|| $member_info["use_sales"] && $service_mode == "sales" :
					// member制, centre, sales
                    if($this->check_SSWatcher_account()){
                        // 監視権限を持つSTAFFは、レイアウトを変更して表示
                        $is_room_split = $obj_N2MY_Account->getSalesRoomCntCheck( $user_info["user_key"] );
                        if($is_room_split){
                            // 特定件数以上では部屋取得時タイムアウトとなる為、分割で表示
                            $this->template->assign("is_room_split", $is_room_split);
                            $this->session->set( 'is_room_split', $is_room_split );

                            if($_POST['search_room_name']){
                                // 検索ボタン押下時は値を取得
                                $search_room_name = $_POST['search_room_name'];                            
                                $this->session->set( 'search_room_name', $search_room_name );
                            }elseif(!$this->request->get("new")  && $this->session->get("search_room_name")){
                                // new がある場合は会議予約に遷移した状態となる。検索結果を保持したroom_infoでは紐付いた部屋情報がない場合があるので再取得
                                // sessionにデータがある場合は、人数取得ロジックの影響で２回目の本処理フローに入った場合となる。この場合はPOSTが使えないのでsessionから検索ワードがあるか判断を実施                                
                                $search_room_name = $this->session->get("search_room_name");                            
                            }
                            
                            if($search_room_name){
                                $this->template->assign("search_room_name", $search_room_name);
                                $room_list = $obj_N2MY_Account->getRoomList(  $user_info["user_key"], $service_mode , null , null , null , SPLIT_COUNT , null , $search_room_name);
                                break;
                            }
                        }else{
                            $room_list = $obj_N2MY_Account->getRoomList( $user_info["user_key"], $service_mode );
                            break;
                        }
                    }
                    $room_list = $obj_N2MY_Account->getOwnRoom( $member_info["member_key"], $user_info["user_key"], $service_mode );
                    $user_info["member_multi_room_flg"] = (1 < count( $room_list )) ? 1 : 0;
						$this->session->set( "user_info", $user_info );
                    break;
				default :
					// room制 MTG member login
					$room_list = $obj_N2MY_Account->getRoomList( $user_info["user_key"], $service_mode );
					break;
			}
		}

		if ( $is_solve_ext_opt ) {
			// オプション情報の拡張
			foreach ( $room_list as $room_key => $room_info ) {
				$room_list[$room_key]['options'] = $obj_N2MY_Account->getRoomOptionList( $room_key );
			}
		}

		$this->session->set( 'room_info', $room_list );
		return $room_list;
	}

    function get_room_info( $room_key=null ) {
        $room_list =  $this->session->get("room_info");
        if( ! $room_key ) {
            return $room_list;
        } else {
            foreach( $room_list as $key => $room_info ){
                if( $key == $room_key ) {
                    return $room_info;
                }
            }
            return;
        }
    }

    //V-CUBE One対応：会議室とメンバー紐付け機能追加（管理者＞会議室設定）
    function get_ONE_whitelist_room_list($user_key, $member_key, $delete_flag = false){
		require_once 'classes/ONE_Account.class.php';
		$obj_ONE_Account = new ONE_Account( $this->get_dsn() );
		return $obj_ONE_Account->get_ONE_whitelist_room_list($user_key, $member_key, $delete_flag);
	}

    function room_exist($room_key) {
        return array_key_exists( $room_key, $this->session->get( 'room_info', _EZSESSION_NAMESPACE, array () ) );
    }

    /**
     * @param $integer limit １ページの件数
     * @param $integer page カレントページ(指定が無い場合は１件目として処理する)
     * @param $total_count トータル件数
     *
     */
    public function setPager(&$limit, &$page, $total_count, $sort_key = "", $sort_type = "") {
        require_once("lib/pear/Pager.php");
        if (!$limit || !is_int((int)$limit) || $limit < 1) {
            $limit = 10;
        }
        if (!$page || !is_int((int)$page) || $page < 1) {
            $page = 1;
        }
        $offset = ($limit * ($page - 1));
        $pager_options = array(
            'mode' => 'Sliding',
            'perPage' => $limit,
            'currentPage' => $page,
            'urlVar' => "action_reservation&page",
            'delta' => 3,
            'totalItems' => $total_count,
            'importQuery' => false,
            'firstPagePost' => '',
            );
        $pager = & Pager::factory($pager_options);
        $links = $pager->getLinks();
        $pager_info = array(
            "links"   => $links,
            "limit"   => $limit,
            "range"   => $pager->range,
            "total"   => $total_count,
            "current" => $pager->getCurrentPageID(),
            "next"    => $pager->getNextPageID(),
            "prev"    => $pager->getPreviousPageID(),
            "last"    => $pager->numPages(),
            "start"   => $total_count ? ($offset + 1) : 0,
            "end"     => ($total_count < $offset) ? $offset + $limit :
                (($total_count < ($offset + $limit)) ? $total_count : ($offset + $limit)),
            "sort_key" => $sort_key,
            "sort_type" => $sort_type,
            );
        return $pager_info;
    }

    function sendReport($mail_from, $mail_to, $mail_subject, $mail_body) {
        require_once("lib/EZLib/EZMail/EZSmtp.class.php");
        // 入力チェック
        if (!$mail_from || !$mail_to || !$mail_subject || !$mail_body) {
            return false;
        }
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang = EZLanguage::getLangCd("ja");
        $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
        $obj_SendMail->setFrom($mail_from);
        $obj_SendMail->setReturnPath($mail_from);
        $obj_SendMail->setTo($mail_to);
        $obj_SendMail->setSubject($mail_subject);
        $obj_SendMail->setBody($mail_body);
        // メール送信
        $result = $obj_SendMail->send();
        $this->logger2->info(array($mail_from, $mail_to, $mail_subject, $mail_body));
    }

    function get_error_info($err_obj) {
        $err_fields = $err_obj->error_fields();
        if (!$err_fields) {
            return array();
        } else {
            $msg_format = $this->get_message("error");
            foreach($err_fields as $field) {
                $type = $err_obj->get_error_type($field);
                $err_rule = $err_obj->get_error_rule($field, $type);
                if (($type == "allow") || $type == "deny") {
                    $err_rule = join(", ", $err_rule);
                }
                $error_info[$field] = array(
                    "err_cd"  => $type,
                    "err_msg" => sprintf($msg_format, sprintf($this->get_message("ERROR", $type), $err_rule)),
                    );
            }
            return $error_info;
        }
    }

    private function _getMeetingVersion() {
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }

    /**
     * ユーザーに指定されている有効言語か確認する
     * @param string $lang
     * @return String
     */
    function user_lang_allow_confirm($lang){
        $user_info = $this->session->get("user_info");
        // 利用言語設定 指定した言語が利用できない場合は優先言語を指定する
        if($user_info["lang_allow"]){
            $user_lang_list = split("," , $user_info["lang_allow"]);
            if(!in_array($lang , $user_lang_list)){
                $lang = $user_lang_list[0];
            }
        }

        return $lang;

    }

    function get_output_encoding() {
        // 現在の言語設定で判断。
        $lang = $this->get_language();

        switch($lang) {
            case 'ja_JP':
                return 'SJIS';
            case 'en_US':
            case 'zh_CN':
            case 'zh_TW':
            case 'fr_FR':
            case 'in_ID':
            case 'th_TH':
                return 'UTF-8';
            default:
                return 'UTF-8';
        }
    }

    /**
     * 操作ログ
     */
    function add_operation_log($action_name, $info = null) {
        $user_info = $this->session->get('user_info');
        $member_info = $this->session->get('member_info');
        if (!$user_info["user_key"]) {
            $this->logger->warn(array($action_name, $info), 'Who are you?');
            return false;
        }
        //操作ログ追加
        require_once ("classes/dbi/operation_log.dbi.php");
        $objOperationLog = new OperationLogTable($this->get_dsn());
        $data = array(
                "user_key"           => $user_info["user_key"],
                "member_key"         => $member_info["member_key"],
                "member_id"          => $member_info["member_id"],
                "session_id"         => session_id(),
                "action_name"        => $action_name,
                "remote_addr"        => $_SERVER["REMOTE_ADDR"],
                "info"               => serialize($info),
                "operation_datetime" => date( "Y-m-d H:i:s" ),
        );
        //$this->logger2->info($data);
        $objOperationLog->add($data);
        return true;
    }

    function _has_teleconferenc_room(){
        $rooms = $this->session->get( 'room_info' );
        if ( ! $rooms ) $rooms = $this->get_room_list();
        foreach($rooms as $rom_info){
            $teleconference_option = $rom_info["options"]["teleconference"];
            if($teleconference_option){
                return 1;
            }
        }
        return 0;
    }

    // 電話会議連携を持っている確認する
    function _has_teleconferenc_use_room(){
        $rooms = $this->session->get( 'room_info' );
        if ( ! $rooms ) $rooms = $this->get_room_list();
        foreach($rooms as $rom_info){
            $use_teleconf = $rom_info["room_info"]["use_teleconf"];
            $teleconference_option = $rom_info["options"]["teleconference"];
            if($use_teleconf && $teleconference_option){
                return 1;
            }
        }
        return 0;
    }

    function getIgroreIpAddr() {
        $ignoreIpList = $this->config->get("N2MY", "ignore_remote_addr");
        if (!$ignoreIpList) {
            return false;
        }
        $ipList = explode(",", $ignoreIpList);
        return $ipList;
    }

    function getCustomDir(){
        $custom = $this->config->get('SMARTY_DIR','custom');
        if ($this->session->get("service_mode") == "sales" && !$custom) {
            $custom = 'custom/sales/common/';
        } else if ($custom == "sales" && $this->session->get("service_mode") != "sales" && $this->session->get("login")) {
            $custom = "common";
        } else if ($custom == "") {
            $custom = 'common';
        } else  {
            $custom = 'custom/'.$custom.'/common/';
        }
        return $custom;
    }

    /**
     * ポータル連携
     * @see htdocs/api/v1/user/index.php::setVidInfo
     */
    protected function setVidInfo( $vcube_one_member_id = null, $vIdAuthToken  = null, $contract_id = null, $lang = null, $time_zone = null ) {

        if ( !$vcube_one_member_id || !$vIdAuthToken || !$contract_id || !$lang || $time_zone === null){
            $this->session->remove( 'vid_info' );
            setcookie( 'cmsHeaderData', '', time() - 1, N2MY_COOKIE_PATH, N2MY_COOKIE_DOMAIN, false, false );
            return;
        }

        $config = $this->config->getAll('VCUBE_PORTAL');

        // 定義不足
        if ( !$config['domain'] ) return;

        // ポータル連携不要
        if ( !$config['is_display_header'] && !$config['is_pool_reservation'] ) return;

        $vid_info = array();
        $vid_info['vcube_id'] = $vcube_one_member_id;
        $vid_info['auth_token'] = $vIdAuthToken;
        $vid_info['contract_id'] = $contract_id;
        $vid_info['lang'] = $lang;
        $vid_info['time_zone'] = $time_zone;

        // はちまき描画
        $data = array($vcube_one_member_id, $vIdAuthToken, $contract_id, $lang, $time_zone);
        $cmsHeaderData = base64_encode( join(',', $data) );
        // 別ドメインのため secure, httponly は false
        setcookie( 'cmsHeaderData', $cmsHeaderData, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH, N2MY_COOKIE_DOMAIN, false, false );
        $vid_info['cmsHeaderData'] = $cmsHeaderData;

        $this->session->set('vid_info', $vid_info);
        $this->logger2->info($vid_info, 'set session vid_info');
    }

    /**
     * 乱数ID発行
     */
    protected function create_id() {
        $document_id = sha1(uniqid(rand(), true));
        return $document_id;
    }

    /**
     * account_planがONEかチェック
     * ONE=true、ONE以外=false
     * @see classes/N2MY_Api.class.php::check_one_account
     */
    protected function check_one_account() {
        $user_info = $this->session->get("user_info");
        $account_plan = $user_info["account_plan"];
        require_once ("classes/ONE_Account.class.php");
        $obj_ONE_Account = new ONE_Account($this->get_dsn());
        return $obj_ONE_Account->is_one_account($account_plan);
    }
    /**
     * S&S 時に、監視入室可能かチェック
     */
    private function check_SSWatcher_account() {
        if($this->session->get("service_mode") == "sales"){
            if($this->session->get("role") == "10"){
                // one account＋SS管理者
                return true;
            }else{
                $member_info = $this->session->get("member_info");
                if ($member_info["use_ss_watcher"]){
                    // 非one account＋use_ss_watcher権限
                    return true;
                }
            }
        }
        return false;
    }
}