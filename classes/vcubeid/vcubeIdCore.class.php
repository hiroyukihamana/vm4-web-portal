<?php
/**
 * V-CUBE ID Rest API Call
 */
 require_once('classes/vcubeid/wsseApi.class.php');

class vcubeIdCore extends wsseApi
{

  const API_PATH = "service/1.0/rest/";

  const FORMAT_XML   = "xml";
  const FORMAT_JSON  = "json";
  const FORMAT_ARRAY = "array";



  /**
   *
   */
  public function __construct($domain, $wsseId, $wssePw, $isSSL, $isPwSha1 = true)
  {
    parent::__construct();

    $this->setDomain($domain);
    $this->setUserId($wsseId);
    $this->setUserPw($wssePw);
    $this->setIsSSL($isSSL);
    $this->setIsPwSha1($isPwSha1);

  }

  public function ssologin($consumerKey, $vIdAuthToken, $vcubeId, $formatType="array"){

      $this->setEndPoint(self::API_PATH."contract");

      $params = array(
              "consumerKey"  => $consumerKey
              , "vcubeId" => $vcubeId
              , "vIdAuthToken" => $vIdAuthToken
      );

      $this->get()->run($params);
      return $this->returnFormat($formatType);
  }

  /**
   * VID AuthTokenの取得
   * @param string $consumerKey
   * @param string $vcuneId
   * @param string $vcubePw
   * @return string array
   */
  public function getToken($consumerKey, $vcubeId, $vcubePw, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."token");

    $params = array(
          "consumerKey" => $consumerKey
        , "vcubeId"     => $vcubeId
        , "vcubePw"     => $vcubePw
    );

    $this->post()->run($params);
    return $this->returnFormat($formatType);

  }

  /**
   * VID AuthTokenの存在チェック
   * @param string $consumerKey
   * @param string $vidAuthToken
   * @param array
   */
  public function existToken($consumerKey, $vIdAuthToken, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."exist/token");

    $params = array(
        "consumerKey"  => $consumerKey
      , "vIdAuthToken" => $vIdAuthToken
    );

    $this->get()->run($params);
    return $this->returnFormat($formatType);
  }

  /**
   * 特定のUserのAgencyIdを取得
   * @param string $vIdAuthToken
   * @param string $vcubeId
   * @return array
   */
  public function getUserAgency($vIdAuthToken, $vcubeId, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/agency");

    $params = array(
        "vIdAuthToken" => $vIdAuthToken
      , "vcubeId"      => urlencode($vcubeId)
    );

    $this->get()->run($params);
    return $this->returnFormat($formatType);
  }

  /**
   * ユーザーの認証無しに、強制的にAgencyを取得
   * @param string $consumerKey
   * @param string $secretAuthCode
   * @return array
   */
  public function getAgencyForced($consumerKey, $secretAuthCode, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."agency");

    $params = array(
          "consumerKey"    => $consumerKey
        , "secretAuthCode" => $secretAuthCode
    );

    $this->get()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザー情報の取得
   * @param string $vIdAuthToken
   * @param string $vcubeId
   * @return array
   */
  public function getUser($vIdAuthToken, $vcubeId, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user");

    $params = array(
          "vIdAuthToken"  => $vIdAuthToken
        , "vcubeId"       => urlencode($vcubeId)
    );

    $this->get()->run($params);
    return $this->returnFormat($formatType);
  }

  /**
   * ユーザー情報の強制取得
   * @param string $consumerKey
   * @param string $secretAuthCode
   * @param string $vcubeId
   * @return array
   */
  public function getUserForced($consumerKey, $secretAuthCode, $vcubeId, $formatType="array" )
  {
    $this->setEndPoint(self::API_PATH."user");

    $params = array(
          "consumerKey"    => $consumerKey
        , "secretAuthCode" => $secretAuthCode
        , "vcubeId"        => urlencode($vcubeId)
    );

    $this->get()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ContractIdの取得
   * @param unknown $vcubeId
   * @param unknown $consumerKey
   * @param unknown $vIdAuthToken
   * @return Ambigous <multitype:, mixed>
   */
  public function getContract($vcubeId, $consumerKey, $vIdAuthToken, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/contract");

    $params = array(
          "consumerKey"    => $consumerKey
        , "vcubeId"        => urlencode($vcubeId)
        , "vIdAuthToken" => $vIdAuthToken
    );

    $this->get()->run($params);
    return $this->returnFormat($formatType);
  }

  /**
   * ContractIdの取得
   * @param unknown $vcubeId
   * @param unknown $consumerKey
   * @param unknown $secretAuthCode
   * @return Ambigous <multitype:, mixed>
   */
  public function getContractForced($vcubeId, $consumerKey, $secretAuthCode, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/contract");

    $params = array(
          "consumerKey"    => $consumerKey
        , "vcubeId"        => urlencode($vcubeId)
        , "secretAuthCode" => $secretAuthCode
    );

    $this->get()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザー 本登録
   * @param string $consumerKey
   * @param number $agencyId
   * @param string $vcubeId
   * @param string $vcubePw
   * @param string $name
   * @param string $kana
   * @param string $email
   * @param string $lang
   */
  public function addRegistUser($consumerKey, $agencyId, $vcubeId, $vcubePw, $name, $kana, $email, $lang, $formatType="array")
  {
    $type = "regist";
    return $this->addUser($type, $consumerKey, $agencyId, $vcubeId, $vcubePw, $name, $kana, $email, $lang, $formatType);
  }

  /**
   * ユーザー仮登録
   * @param string $consumerKey
   * @param number $agencyId
   * @param string $vcubeId
   * @param string $vcubePw
   * @param string $name
   * @param string $kana
   * @param string $email
   * @param string $lang
   */
  public function addInterimUser($consumerKey, $agencyId, $vcubeId, $vcubePw, $name, $kana, $email, $lang, $formatType="array")
  {
    $type = "interim";
    return $this->addUser($type, $consumerKey, $agencyId, $vcubeId, $vcubePw, $name, $kana, $email, $lang);
  }

  /**
   * ユーザー登録
   * @param string $type
   * @param string $consumerKey
   * @param number $agencyId
   * @param string $vcubeId
   * @param string $vcubePw
   * @param string $name
   * @param string $kana
   * @param string $email
   * @param string $lang
   * @return array
   */
  private function addUser($type, $consumerKey, $agencyId, $vcubeId, $vcubePw, $name, $kana, $email, $lang, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user");

    $params = array(
          "type"        => $type
        , "consumerKey" => $consumerKey
        , "agencyId"    => $agencyId
        , "vcubeId"     => $vcubeId
        , "vcubePw"     => $vcubePw
        , "name"        => urlencode($name)
        , "kana"        => urlencode($kana)
        , "email"       => $email
        , "lang"        => $lang
    );

    $this->post()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザー情報更新
   * @param string $vIdAuthToken
   * @param string $vcubeId
   * @param string $name
   * @param string $kana
   * @param string $email
   * @param string $mobileEmail
   * @param string $lang
   * @return array
   */
  public function updateUser($vIdAuthToken, $vcubeId, $name, $kana, $email, $mobileEmail, $lang, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user");

    $params = array(
          "vIdAuthToken" => $vIdAuthToken
        , "vcubeId"      => urlencode($vcubeId)
        , "name"         => urlencode($name)
        , "kana"         => urlencode($kana)
        , "email"        => urlencode($email)
        , "mobileEmail"  => urlencode($mobileEmail)
        , "lang"         => $lang
    );

    $this->put()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザーパスワード更新
   * @param string $consumerKey
   * @param string $vcubeId
   * @param string $vcubePwNow
   * @param string $vcubePwNew
   * @return array
   */
  public function updateUserPassword($consumerKey, $vcubeId, $vcubePwNow, $vcubePwNew, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/password");

    $params = array(
            "consumerKey"  => $consumerKey
        , "vcubeId"      => urlencode($vcubeId)
        , "vcubePw"      => $vcubePwNow
        , "vcubePwNew"   => $vcubePwNew
    );

    $this->put()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * パスワード強制変更
   * @param string $consumerKey
   * @param string $vcubeId
   * @param string $secretAuthCode
   * @param string $vcubePwNew
   * @param number $contractId
   * @return array
   */
  public function updateUserPasswordForced($consumerKey, $vcubeId, $secretAuthCode, $vcubePwNew, $contractId = 0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/password");

    $params = array(
          "consumerKey"    => $consumerKey
        , "vcubeId"        => urlencode($vcubeId)
        , "secretAuthCode" => $secretAuthCode
        , "vcubePwNew"     => $vcubePwNew
        , "contractId"     => $contractId
    );

    $this->put()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザーが利用出来るConsumerを追加
   * @param string $consumerKey
   * @param string $vcubeId
   * @param string $vcubePw
   * @param string $role
   * @param number $contractId
   * @return array
   */
  public function addUserHasConsumer($consumerKey, $vcubeId, $vcubePw, $role, $contractId = 0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/consumer");

    $params = array(
          "consumerKey"  => $consumerKey
        , "vcubeId"      => $vcubeId
        , "vcubePw"      => $vcubePw
        , "role"         => $role
        , "contractId"   => $contractId
    );

    $this->post()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * コンシューマー一括登録
   * @param string $vcubeId
   * @param string $secretAuthCode
   * @param string $consumersXml
   * @param number $contractId
   * @return array
   */
  public function addUserHasConsumerForXml($vcubeId, $secretAuthCode, $consumersXml, $contractId = 0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/consumers");

    $params = array(
          "vcubeId"         => urlencode($vcubeId)
        , "secretAuthCode"  => $secretAuthCode
        , "consumersXml"    => $consumersXml
        , "contractId"      => $contractId
    );

    $this->post()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザーが利用できるConsumerを強制的に追加
   * @param string $consumerKey
   * @param string $vcubeId
   * @param string $secretAuthCode
   * @param string $role
   * @param number $contractId
   * @return array
   */
  public function addUserHasConsumerForced($consumerKey, $vcubeId, $secretAuthCode, $role, $contractId=0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/consumer");

    $params = array(
          "consumerKey"    => $consumerKey
        , "vcubeId"        => urlencode($vcubeId)
        , "secretAuthCode" => $secretAuthCode
        , "role"           => $role
        , "contractId"     => $contractId
    );

    $this->post()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザーへ、初期登録するConsumerを設定する
   * @param string $consumerKey
   * @param string $vcubeId
   * @param string $vcubePwNoSha1
   * @return array
   */
  public function addUserHasDefaultConsumer($consumerKey, $vcubeId, $vcubePwNoSha1, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/defaultconsumer");

    $params = array(
          "consumerKey"    => $consumerKey
        , "vcubeId"        => urlencode($vcubeId)
        , "secretAuthCode" => $secretAuthCode
        , "role"           => $role
        , "contractId"     => $contractId
    );

    $this->post()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * 特定のConsumerへ、複数のUserを追加する
   * @param string $consumerKey
   * @param string $secretAuthCode
   * @param string $usersXml (コメント内にサンプル有り)
   * @param string $contractId
   * @param string $agencyId
   * @param string $formatType
   * @return array
   *
     <!-- usersXml Sample --->
     <?xml version="1.0" encoding="UTF-8"?>
     <entry>
          <contract_id>1</contract_id>
       <consumer_key>consumerKey000001testdesuyo</consumer_key>
       <users>
         <user>
           <vcube_id>test@test.com</vcube_id>
           <name>テスト太郎</name>
           <password>test01Test</password>
           <authority>10</authority>
         </user>
         <user>〜</user>を件数分・・・
       </users>
     </entry>
   */
  public function addUsersXml($consumerKey, $secretAuthCode, $usersXml, $contractId, $agencyId, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."consumer/users");

    $params = array(
          "consumerKey"    => $consumerKey
        , "secretAuthCode" => $secretAuthCode
        , "usersXml"       => urlencode($usersXml)
        , "contractId"     => $contractId
        , "agencyId"       => $agencyId
    );

    $this->post()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * 特定のユーザーに、コンシューマー一括登録
   * @param string $vcubeId
   * @param string $secretAuthCode
   * @param string $consumersXml
   * @param int $contractId
   * @param string $formatType
   * @return Ambigous <SimpleXMLElement, string, multitype:, mixed>
   *
      //参考XML
     <?xml version="1.0" encoding="UTF-8"?>
    <entry>
      <contract_id>1</contract_id>
      <vcube_id>akio-watanabe@vcube.co.jp</vcube_id>
      <consumers>
        <consumer>
          <consumer_key>consumerKey0001test</consumer_key>
          <authority>00</authority>
        </consumer>
        <consumer>
          <consumer_key>consumerKey0002test</consumer_key>
          <authority>10</authority>
        </consumer>
      </consumers>
    </entry>
   */
  public function addConsumersXml($vcubeId, $secretAuthCode, $consumersXml, $contractId, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/Consumers");

    $params = array(
          "vcubeId"        => urlencode($vcubeId)
        , "secretAuthCode" => $secretAuthCode
        , "consumersXml"   => urlencode($consumersXml)
        , "contractId"     => $contractId
    );

    $this->post()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザーの利用出来るConsumerを外す
   * @param string $consumerKey
   * @param string $vcubeId
   * @param string $contractId
   * @return array
   */
  public function deleteUserHasConsumer($consumerKey, $vcubeId, $vIdAuthToken, $contractId = 0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/consumer");

    $params = array(
          "consumerKey"    => $consumerKey
        , "vcubeId"        => urlencode($vcubeId)
        , "vIdAuthToken"   => $vIdAuthToken
        , "contractId"     => $contractId
    );

    $this->delete()->run($params);

    return $this->returnFormat($formatType);
  }

  public function deleteUserHasConsumerForced($consumerKey, $vcubeId, $secretAuthCode, $contractId = 0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/consumer");

    $params = array(
          "consumerKey"    => $consumerKey
        , "vcubeId"        => urlencode($vcubeId)
        , "secretAuthCode"  => $secretAuthCode
        , "contractId"     => $contractId
    );

    $this->delete()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザー論理削除
   * @param string $consumerKey
   * @param string $vcubeId
   * @param number $contractId
   * @return array
   */
  public function deleteUserLogical($consumerKey, $vcubeId, $contractId = 0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user");

    $params = array(
          "type"            => "logical"
        , "consumerKey"     => $consumerKey
        , "secretAuthCode"  => $secretAuthCode
        , "vcubeId"         => urlencode($vcubeId)
        , "contractId"      => $contractId
    );

    $this->delete()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザーの物理削除
   * @param string $consumerKey
   * @param string $secretAuthCode
   * @param string $vcubeId
   * @param number $contractId
   * @return array
   */
  public function deleteUserHard($consumerKey, $secretAuthCode, $vcubeId, $contractId=0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user");

    $params = array(
          "type"            => "hard"
        , "consumerKey"     => $consumerKey
        , "secretAuthCode"  => $secretAuthCode
        , "vcubeId"         => urlencode($vcubeId)
        , "contractId"      => $contractId
    );

    $this->delete()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザーの存在確認
   * @param string $consumerKey
   * @param string $vcubeId
   * @return array
   */
  public function existUser($consumerKey, $vcubeId, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."exist/user");

    $params = array(
          "consumerKey"     => $consumerKey
        , "vcubeId"         => urlencode($vcubeId)
    );

    $this->get()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * メール送信
   * @param string $consumerKey
   * @param string $vcubeId
   * @param string $lang
   * @return array
   */
  public function sendMailConfirmation($consumerKey, $vcubeId, $lang="", $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."mail/confirmation");

    $params = array(
          "consumerKey"  => $consumerKey
        , "vcubeId"      => urlencode($vcubeId)
        , "lang"         => $lang
    );

    $this->post()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザーが利用出来るサービス一覧
   * @param string $consumerKey
   * @param string $vcubeId
   * @return array
   */
  public function existUserHasConsumer($consumerKey, $vcubeId, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/consumer");

    $params = array(
            "consumerKey"  => $consumerKey
        , "vcubeId"      => urlencode($vcubeId)
    );

    $this->get()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * サービスを利用出来るユーザー一覧の取得
   * @param string $consumerKey
   * @param string $contractId
   * @return array
   */
  public function getConsumerHasUser($consumerKey, $limit, $offset, $order, $contractId = 0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."consumer/user");

    $params = array(
          "consumerKey" => $consumerKey
        , "limit"      => $limit
        , "offset"     => $offset
        , "order"      => $order
        , "contractId" => $contractId
    );

    $this->get()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * サービスを利用可能なユーザー
   * @param unknown $consumerId
   * @param unknown $admin
   * @param number $contractId
   * @return Ambigous <multitype:, mixed>
   */
  public function getConsumerHasUserCount($consumerKey, $role, $contractId=0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."consumer/usercount");

    $params = array(
        "consumerKey" => $consumerKey
          , "role"       => $role
        , "contractId" => $contractId
    );

    $this->get()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * サービス情報の取得
   * @param string $consumerKey
   * @return array
   */
  public function getConsumer($consumerKey, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."consumer");

    $params = array(
        "consumerKey" => $consumerKey
    );

    $this->get()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * VIDと契約情報で、マッチするConsumerKeyを取得する
   * @param unknown $vcubeId
   * @param unknown $consumerKey
   * @return Ambigous <multitype:, mixed>
   */
  public function getUserHasConsumer($vcubeId, $contractId, $secretAuthCode, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."user/consumer");

    $params = array(
          "vcubeId"        => urlencode($vcubeId)
        , "contractId"    => $contractId
        , "secretAuthCode" => $secretAuthCode
    );

    $this->get()->run($params);

    return $this->returnFormat($formatType);
  }

  /**
   * ユーザーの権限
   * @param string $consumerKey
   * @param string $vcubeId
   * @param string $secretAuthCode
   * @param string $role
   * @param number $contractId
   */
  public function updateUserHasConsumer($consumerKey, $vcubeId, $secretAuthCode, $role, $contractId = 0, $formatType="array")
  {
    $this->setEndPoint(self::API_PATH."consumer");

    $params = array(
          "consumerKey"    => $consumerKey
        , "vcubeId"        => urlencode($vcubeId)
        , "secretAuthCode" => $secretAuthCode
        , "role"           => $role
        , "contractId"     => $contractId
    );

    $this->put()->run($params);

    return $this->returnFormat($formatType);
  }

}
