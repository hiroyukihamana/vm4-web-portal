<?php

require_once 'classes/vcubeid/baseApi.class.php';
require_once 'lib/EZLib/EZHTML/EZValidator.class.php';
require_once 'lib/EZLib/EZCore/EZConfig.class.php';
require_once 'lib/pear/XML/Serializer.php';
require_once 'classes/N2MY_Reservation.class.php';

class PortalCore extends baseApi {
	
	const API_PATH = "api/v1/";
	const REQUEST_ADD = 'meeting/reserve';
	// const REQUEST_UPDATE = self::REQUEST_ADD;
	const REQUEST_DELETE = 'meeting/reserve/delete';
	
	// @formatter:off
	private static $rules = array(
			'reservation_name' => array(
					'required' => true,
					'maxlen' => 64,
			),
			'reservation_pw' => array(
					'required' => true,
					'allow' => array( 0, 1 ),
			),
			'status' => array(
					'required' => true,
					'allow' => array( 'wait', 'now', 'end' ),
			),
			'user_id' => array(
					'required' => true,
			),
			'room_id' => array(
					'required' => true,
					'maxlen' => 255,
			),
			'reservation_id' => array(
					'required' => true,
					'maxlen' => 255,
			),
			'meeting_id' => array(
					'required' => true,
					'regex' => '/^[0-9a-fA-F]{32}$/', // MD5 hash
					// 'maxlen' => 255,
			),
			'reservation_start_date' => array(
					'required' => true,
					'integer' => true,
			),
			'reservation_end_date' => array(
					'required' => true,
					'integer' => true,
			) 
	);
	// @formatter:on
	
	// auth
	private $vid, $authToken, $contractId, $authorization;
	// @formatter:off
	public function setVid($value){ $this->vid = $value; }
	public function getVid(){ return $this->vid; }
	public function setAuthToken($value){ $this->authToken = $value; }
	public function getAuthToken(){ return $this->authToken; }
	public function setContractId($value){ $this->contractId = $value; }
	public function getContractId(){ return $this->contractId; }
	public function setAuthorization($value){ $this->authorization = $value; }
	public function getAuthorization(){ return $this->authorization; }
	// @formatter:on
	
	// log
	private $log_file;
	
	public function __construct( $vid = null, $authToken = null, $contractId = null ) {
		parent::__construct();
		
		$this->setVid( $vid );
		$this->setAuthToken( $authToken );
		$this->setContractId( $contractId );
		
		if ( $vid && $authToken && $contractId ) {
			$this->setAuthorization( $this->makeAuthorizaation() );
			$this->setHeader();
		}
		
		// setting API
		$config = EZConfig::getInstance()->getAll( 'VCUBE_PORTAL' );
		parent::setDomain( $config['domain'] );
		parent::setIsSSL( $config['isSSL'] );
		// parent::setParams( 'timeout', $config['push_timeout'] );
		parent::post();
		
		// setting Log
		$dir = EZConfig::getInstance()->get('LOGGER', 'log_dir');
		$prefix = $config['log_prefix'] ?: 'portal_push';
		$span = $config['log_span'] ?: 'Ym';
		$this->log_file = sprintf( '%s/%s_%s.log', $dir, $prefix, date( $span ) );
	}
	
	public function makeAuthorizaation( $vid = null, $authToken = null, $contractId = null ) {
		$vid = $vid ?: $this->getVid();
		$authToken = $authToken ?: $this->getAuthToken();
		$contractId = $contractId ?: $this->getContractId();
		return base64_encode( join( ':', array( $vid, $authToken, $contractId ) ) );
	}
	
	public function setHeader( $value = null ) {
		$value = $value ?: array(
				'vId' => $this->getVid(),
				'authToken' => $this->getAuthToken(),
				'contractId' => $this->getContractId(),
				'Authorization' => $this->getAuthorization() 
		);
		parent::setHeader( $value );
	}
	
	public function setEndPoint( $value ) {
		parent::setEndPoint( self::API_PATH . $value );
	}
	
	private function checkValidate( $resource, $rules = null ) {
		$rules = $rules ?: self::$rules;
		$validator = new EZValidator( $resource );
		foreach( $rules as $field => $rule ) {
			$validator->check( $field, $rule );
		}
		if ( !EZValidator::isError( $validator ) ) return true;
		else {
			$error = array();
			foreach ( $validator->error_fields() as $key ) {
				$type = $validator->get_error_type( $key );
				$rule = $validator->get_error_rule( $key, $type );
				$error[$key] = array(
						'value' => $resource[$key],
						'type' => $type,
						'rule' => $rule 
				);
			}
			$this->logger2->warn( $error );
		}
		return false;
	}
	
	private function data2xml( $reservation_data ) {
		// format as v1API
		$data = array(
				// 'status' => 1,
				'data' => array(
						// 'count' => 1,
						'reservations' => array(
								'reservation' => $reservation_data 
						) 
				) 
		);
		
		$serializer = new XML_Serializer();
		$serializer->setOption( 'mode', 'simplexml' );
		$serializer->setOption( 'encoding', 'UTF-8' );
		$serializer->setOption( 'addDecl', 'ture' );
		$serializer->setOption( 'rootName', 'result' );
		$serializer->serialize( $data );
		return $serializer->getSerializedData();
	}
	
	protected function run( $param ) {
		$date = date('Y-m-d H:i:s');
		
		parent::run( $param );
		
		$this->log( array(
				'date' => $date,
				'response_code' => parent::getResponseCode(),
				'response_time' => parent::getResponseTime(),
				'request_url' => parent::getUrl(),
				'request_body' => $param,
				'request_header' => parent::getHeader(),
				'response_body' => parent::getResponseBody(),
				'response_headers' => parent::getResponseHeaders(),
		) );
		
		// TODO switch maintenance time
		switch ( parent::getResponseCode() ) {
			case 200:
				return true;
				break;
			default:
				return false;
				break;
		}
	}
	
	public function addReservation( $reservation_data ) {
		if ( !$this->checkValidate( $reservation_data ) ) return false;
		
		$param = array(
				// 'output' => 'xml',
				'reserve' => $this->data2xml( $reservation_data ) 
		);
		
		$this->setEndPoint( self::REQUEST_ADD );
		return $this->run($param);
	}
	
	public function updateReservation( $reservation_data ) {
		// $this->setEndPoint( self::REQUEST_UPDATE );
		return $this->addReservation( $reservation_data );
	}
	
	public function deleteReservation( $meeting_id ) {
		if ( !$this->checkValidate( 
				array( 'meeting_id' => $meeting_id ), 
				array( 'meeting_id' => self::$rules['meeting_id'] ) 
		) ) return false;
		
		$param = array(
				'meetingId' => $meeting_id 
		);
		$this->setEndPoint( self::REQUEST_DELETE );
		return $this->run($param);
	}
	
	private function log( $info = array(), $msg = null ) {
		$fp = fopen( $this->log_file, 'a' );
		if ( $fp ) fwrite( $fp, json_encode( $info ) . PHP_EOL );
		fclose( $fp );
		$this->logger2->debug( $info, $msg );
	}
}
