<?php
require_once('classes/vcubeid/baseApi.class.php');

class wsseApi extends baseApi
{

  /**
   * X-WSSEのPasswordDigest生成時に、PWをSha1する場合は true。Sha1しない場合は、false
   * @param boolean $value
   */
  public function setIsPwSha1($value) { $this->isPwSha1 = $value; }


  public function __construct()
  {
    parent::__construct();
  }

  public function createXWSSEHeader($uid,$pw)
  {
    $nonce = sha1( uniqid(rand(),TRUE) );
    $created = date( 'Y-m-d\TH:i:s\Z', time());

    if ( $this->isPwSha1 ) {
      $pwDigest = sha1( $nonce.$created.sha1(trim($pw)));
    } else {
      $pwDigest = sha1( $nonce.$created.trim($pw));
    }

    $nonce_b64 = base64_encode($nonce);
    $pwDigest_b64 = base64_encode($pwDigest);

    $ret = 'UsernameToken Username="'.$uid.'", PasswordDigest="'.$pwDigest_b64.'", Nonce="'.$nonce_b64.'", Created="'.$created.'"';
    $this->setWsseHeader($ret);
    return $ret;
  }

  protected function run(array $param = array())
  {
    $this->setHeader(array("X-WSSE"=>$this->createXWSSEHeader($this->getUserId(), $this->getUserPw())));
    try{
      parent::run($param);
    }catch (apiException $e) {
      throw new wsseApiException($e->getMessage(), $e->getCode());
    }
  }

}

class wsseApiException extends Exception
{
  public function __construct($message = null, $code = null, $previous = null)
  {
    parent::__construct($message,$code,$previous);
  }
}
