<?php
require_once('lib/pear/HTTP/Request.php');

class baseApi
{
  protected $logger2;
  
  private $domain;
  public function getDomain() { return $this->domain;}
  public function setDomain($value) { $this->domain = $value;}

  private $isSSL;
  public function getIsSSL() { return $this->isSSL;}
  public function setIsSSL($value) { $this->isSSL = $value;}

  private $endPoint;
  public function getEndPoint() { return $this->endPoint;}
  public function setEndPoint($value) { $this->endPoint = $value;}

  private $nonce;
  public function getNonce() { return $this->nonce;}
  public function setNonce($value) { $this->nonce = $value;}

  private $wsseHeader;
  public function getWsseHeader() { return $this->wsseHeader;}
  public function setWsseHeader($value) { $this->wsseHeader = $value;}

  private $method;
  public function getMethod() { return $this->method;}
  public function setMethod($value) { $this->method = $value;}

  private $userId;
  public function getUserId() { return $this->userId;}
  public function setUserId($value) { $this->userId = $value;}

  private $userPw;
  public function getUserPw() { return $this->userPw;}
  public function setUserPw($value) { $this->userPw = $value;}

  private $config;
  public function getConfig() { return $this->config;}
  public function setConfig($value) { $this->config = $value;}

  private $isPwSha1;
  public function getIsPwSha1() { return $this->isPwSha1; }
  
  private $httpHeader = null;
  public function getHeader() { return $this->httpHeader; }
  public function setHeader($value) { $this->httpHeader = $value; }  
  
  private $exceptionInstance = null;
  public function getExceptionInstance() { return $this->exceptionInstance; }
  public function setExceptionInstance($value) { $this->exceptionInstance = $value; }

  private $params = array();
  public function getParams() { return $this->params; }
  public function setParams( $key, $value ) { $this->params[$key] = $value; }
  
  // keep response
  private $response_code, $response_headers, $response_body, $response_time;
  private function setResponseCode( $val ) { $this->response_code = $val; }
  private function setResponseHeaders( $val ) { $this->response_headers = $val; }
  private function setResponseBody( $val ) { $this->response_body = $val; }
  private function setResponseTime( $val ) { $this->response_time = $val; }
  public function getResponseCode() { return $this->response_code; }
  public function getResponseHeaders() { return $this->response_headers; }
  public function getResponseBody() { return $this->response_body; }
  public function getResponseTime() { return $this->response_time; }
  
  public function __construct()
  {
  	$this->logger2 = EZLogger2::getInstance();
  }

  public function getUrl()
  {
    return "http".($this->isSSL?"s":"")."://".str_replace("//", "/", $this->domain."/".$this->endPoint);
  }

  public function get(){ $this->setMethod("get"); return $this;}
  public function post(){ $this->setMethod("post"); return $this;}
  public function put(){ $this->setMethod("put"); return $this;}
  public function delete(){ $this->setMethod("delete"); return $this;}
  
  
  protected function run(array $param = array())
  {
    $client = new HTTP_Request(null, $this->getParams());
    $client->setURL($this->getUrl());
    if (is_array($this->httpHeader)) {
      foreach ($this->httpHeader as $key => $value) {
          $client->addHeader($key, $value);
      }
    }
    $method = strtoupper($this->getMethod());
    switch ($method) {
      case "PUT":
      case "POST":
        $client->setMethod(HTTP_REQUEST_METHOD_POST);
        $client->addRawPostData(http_build_query($param));
        break;
      case "GET":
      case "DELETE":
      default:
        $client->setMethod(HTTP_REQUEST_METHOD_GET);
        foreach ($param as $key=>$val) {
            $client->addQueryString($key, $val);
        }
      
    }
    $time = microtime( true );
    $res = $client->sendRequest();
    $time = microtime( true ) - $time;
    
    $this->setResponseTime( $time );
    $this->setResponseCode( $client->getResponseCode() );
    $this->setResponseHeaders( $client->getResponseHeader() );
    $this->setResponseBody($client->getResponseBody());
    
    if (PEAR::isError($res)) {
    	$this->logger2->warn( $client, 'dump HTTP_Request' );
    	$this->logger2->error( array(
    		'request_url' => $this->getUrl(),
    		'request_method' => $this->getMethod(),
    		'request_param' => $param,
    		'response_time' => $time,
    		'response_code' => $this->getResponseCode(),
    		'request_header' => $this->getResponseHeaders(),
    		'request_body' => $this->getResponseBody(),
    	), $res->toString() );
    	return false;
    }
  }

  public function toXML()
  {
    return simplexml_load_string($this->getResponseBody());
  }

  public function toJson()
  {
    return json_encode($this->toXML());
  }

  public function getRaw()
  {
    return $this->getResponseBody();
  }

  /**
   * 配列XMLDataを変換する
   * @param json $jsonData
   * @return array
   */
  protected function toResultArray($jsonData)
  {
    //TODO: attribute対応
    $tmp = json_decode($jsonData, true);
    array_walk_recursive($tmp, "formatter");
  
    return $tmp;
  }
  
  protected function returnFormat($type)
  {
    switch (strtolower($type)){
      case "xml":
        return $this->toXML();
        break;
      case "json":
        return $this->toJson();
        break;
      case "array":
        return $this->toResultArray($this->toJson());
        break;
      default:
        return $this->getRaw();
    }
  }
  
  
  function _setHTTPStatusCode()
  {
    $code['200'] = array('OK', true);
    $code['201'] = array('Created', true);
    $code['202'] = array('Accepted', true);
    $code['203'] = array('Non-Authoritative Information', true);
    $code['204'] = array('No Content', true);
    $code['205'] = array('Reset Content', true);
    $code['206'] = array('Partial Content', true);
    $code['400'] = array('Bad Request', false);
    $code['401'] = array('Unauthorized', false);
    $code['402'] = array('Payment Required', false);
    $code['403'] = array('Forbidden', false);
    $code['404'] = array('File Not Found', false);
    $code['405'] = array('Method Not Allowed', false);
    $code['406'] = array('Not Acceptable', false);
    $code['407'] = array('Proxy Authentication Required', false);
    $code['408'] = array('Request Time-out', false);
    $code['409'] = array('Conflict', false);
    $code['410'] = array('Gone', false);
    $code['411'] = array('Length Required', false);
    $code['412'] = array('Precondition Failed', false);
    $code['413'] = array('Request Entity Too Large', false);
    $code['414'] = array('Request-URI Too Large', false);
    $code['415'] = array('Unsupported Media Type', false);
    $code['416'] = array('Requested range not satisfiable', false);
    $code['417'] = array('Expectation Failed', false);
    $code['500'] = array('Internal Server Error', false);
    $code['501'] = array('Not Implemented', false);
    $code['502'] = array('Bad Gateway', false);
    $code['502'] = array('Service Unavailable', false);
    $code['504'] = array('Gateway Time-out', false);
    $code['505'] = array('HTTP Version not supported', false);

    $this->_codes = $code;

    return true;
  }
  
  function _getHTTPStatusCodes()
  {
      return $this->_codes;
  }
  
  function getStatusCode()
  {
      return $this->_status;
  }
  
}

function formatter(&$v,$k){
  switch ($k){
    case "result":
      $v = strtolower($v) == "true"?true:false;
      break;
    case "code":
    case "purgeTime":
    case "consumerId":
    case "contractId":
    case "accountStatusId":
      $v = intval($v);
      break;
    case "settingXml":
      $v = simplexml_load_string($v);
      break;
  }
}

class apiException extends Exception
{
  public function __construct($message = null, $code = null, $previous = null)
  {
    parent::__construct($message,$code,$previous);
  }
}
