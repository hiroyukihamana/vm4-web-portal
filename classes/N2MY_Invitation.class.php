<?php

class N2MY_Invitation
{
    private $dsn = null;

    public $fromAddress = RESERVATION_FROM;
    public $additionalMessage = null;
    public $fromUserName = null;
    public $returnToAddress = NOREPLY_ADDRESS;
    public $returnToName = null;
    function __construct($dsn)
    {
        $this->logger =& EZlogger2::getInstance();
        $this->dsn = $dsn;
    }

    public function setFromAddress($address = null)
    {
        if ($address != null)
            $this->fromAddress = $address;
    }

    public function setAdditionalMessage($message = null)
    {
        $this->additionalMessage = $message;
    }

    public function setFromUserName($userName = null)
    {
        $this->fromUserName = $userName;
    }

    public function setReturnToAddresss($address = null)
    {
        if ($address != null)
            $this->returnToAddress = $address;
    }

    public function setReturnToName($userName = null)
    {
        $this->returnToName = $userName;
    }

    function sendInvitationMail($meeting_key, $mailAddress, $lang="ja", $userType="normal", $reservationPassword=null, $contactName="")
    {
        require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
        require_once ("lib/EZLib/EZHTML/EZTemplate.class.php");
        $config = EZConfig::getInstance();

        require_once("classes/dbi/meeting.dbi.php");
        $objMeetingTable = new MeetingTable($this->dsn);
        $meetingInfo     = $objMeetingTable->getRow(sprintf("meeting_key='%s'", $meeting_key));
        if (!$meeting_key || !$mailAddress || !$meetingInfo) {
            $this->logger->warn(array($meeting_key, $mailAddress, $meetingInfo));
            return (string) 0;
        }
        // テンプレート
        $template = new EZTemplate($config->getAll('SMARTY_DIR'));
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang_cd = EZLanguage::getLangCd($lang);

        if (!$config->get("IGNORE_MENU", "teleconference")) {
            require_once("classes/pgi/PGiPhoneNumber.class.php");

            if ($meetingInfo['use_pgi_dialin'] || $meetingInfo['use_pgi_dialin_free'] || $meetingInfo['use_pgi_dialin_lo_call']) {
                $pgi_phone_numbers  = @unserialize($meetingInfo['pgi_phone_numbers']);
                $pgi_phone_numbers = PGiPhoneNumber::toMultilangLocationName($pgi_phone_numbers, $lang_cd);
                if (!$meetingInfo['use_pgi_dialin']){
                    $main_phone_number = PGiPhoneNumber::extractFreeMain($pgi_phone_numbers);
                } else {
                    $main_phone_number = PGiPhoneNumber::extractLocalMain($pgi_phone_numbers);
                }
            }

            $teleconf_info     = array('tc_type'             => $meetingInfo['tc_type'],
                                       'pgi_service_name'    => $meetingInfo['pgi_service_name'],
                                       'pgi_conference_id'   => $meetingInfo['pgi_conference_id'],
                                       'pgi_m_pass_code'     => $meetingInfo['pgi_m_pass_code'],
                                       'pgi_p_pass_code'     => $meetingInfo['pgi_p_pass_code'],
                                       'pgi_l_pass_code'     => $meetingInfo['pgi_l_pass_code'],
                                       'use_pgi_dialin'      => $meetingInfo['use_pgi_dialin'],
                                       'use_pgi_dialin_free' => $meetingInfo['use_pgi_dialin_free'],
                                       'use_pgi_dialin_lo_call' => $meetingInfo['use_pgi_dialin_lo_call'],
                                       'main_phone_number'   => @$main_phone_number,
                                       'phone_number_url'    => N2MY_BASE_URL.'p/'.$meetingInfo['meeting_session_id'],
                                       'tc_teleconf_note'    => $meetingInfo['tc_teleconf_note']);
        }
        require_once("classes/mcu/resolve/Resolver.php");
        if (!$reservationPassword && ($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"])) && $meetingInfo['temporary_did']) {
          $template->assign("temporarySipNumberAddress", $videoConferenceClass->getSipTemporaryNumberAddress($meetingInfo["temporary_did"]));
          $template->assign("temporarySipAddress", $videoConferenceClass->getSipTemporaryAddress($meetingInfo['temporary_did']));
          $template->assign("temporaryH323NumberAddress", $videoConferenceClass->getH323TemporaryNumberAddress($meetingInfo["temporary_did"]));
          $template->assign("temporaryH323Address", $videoConferenceClass->getH323TemporaryAddress($meetingInfo['temporary_did']));
        }
        $mail    = new EZSmtp(null, $lang_cd, "UTF-8");
        $subject = $this->getMessage($lang_cd, "INVITATION", "INVITATION_SUBJECT");

        $mail->setSubject($subject);
        $mail->setFrom($this->fromAddress , $this->fromUserName);
        $mail->setReturnPath($this->returnToAddress);
        if (preg_match("/,/", $mailAddress)) {
            $mail->setBcc($mailAddress);
        } else {
            $mail->setTo($mailAddress);
        }


        $template->assign("base_url", N2MY_BASE_URL);
        $invitation_url = $this->getInvitationMailUrl($meetingInfo["meeting_session_id"], $lang, $userType);
        $template->assign("reservationPassword", $reservationPassword);

        // room info
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->dsn);
        $room_info        = $obj_N2MY_Account->getRoomInfo($meetingInfo['room_key']);
        if ($room_info["room_info"]["use_sales_option"] && $room_info["room_info"]["outbound_id"] && !$contactName) {
            $invitation_url = N2MY_BASE_URL."outbound/".$room_info["room_info"]["outbound_id"];
        }
        $template->assign("invitation_url", $invitation_url);

        $template->assign("room_info", $room_info);
        $template->assign("meeting_info", $meetingInfo);
        $template->assign("contact_name", $contactName);
        $tel_no         = $config->getAll("TELEPHONE");
        $location_list  = $this->getMessage($lang_cd, "TELEPHONE");
        $telephone_data = array();
        foreach($location_list as $key => $location) {
            $telephone_data[$key]["name"] = $location;
            $telephone_data[$key]["list"] = split(",", $tel_no[$key]);
        }
        $template->assign("telephone_data", $telephone_data);
        //normal audience
        $template->assign("teleconf_info",  $teleconf_info);
        $template->assign("user_type",      $userType);

        $session        = EZSession::getInstance();
        $user_info      = $session->get("user_info");
        $info = array(
        	"guest_url_format" => $user_info['guest_url_format']
        );
        $template->assign("info", $info);
        if ($room_info["room_info"]["use_sales_option"]) {
            $custom = "sales";
        } else {
            $custom = $user_info["custom"];
        }
        if(!$custom){
            $custom = $user_info["service_name"];
        }

        if ($user_info["account_model"] == "free") {
            //VCUBE IDから代理店IDを取得
            $wsdl = $config->get('VCUBEID','wsdl');
            $serverName = $_SERVER["SERVER_NAME"];
            if($serverName == $config->get('VCUBEID','meeting_local_server_name')){
              //meetingFree
              $consumerKey = $config->get('VCUBEID','meeting_free_consumer_key');
            }elseif($serverName == $config->get('VCUBEID','paperless_local_server_name')){
              //paperLess
              $consumerKey = $config->get('VCUBEID','paperless_free_consumer_key');
            }
            try {
                require_once("classes/dbi/member_room_relation.dbi.php");
                $objRoomRelation = new MemberRoomRelationTable($this->dsn);
                $where = "room_key = '".$meetingInfo["room_key"]."'";
                $relation_info = $objRoomRelation->getRow($where);
                require_once("classes/dbi/member.dbi.php");
                $objMember = new MemberTable($this->dsn);
                $where = "member_key = ".$relation_info["member_key"];
                $member_info = $objMember->getRow($where, "member_id");
                if ($member_info) {
                    $soap = new SoapClient($wsdl,array('trace' => 1));
                    $response = $soap->getAgencyId($consumerKey, $member_info["member_id"]);
                    $agency_id = $response["agencyId"];
                }
            } catch (Exception $e) {
                $this->logger->warn($e->getMessage());
            }
            $custom = 'custom/vcubeid/';
            if ($agency_id) {
                $custom_template_file = $custom.$lang_cd."/mail/".$agency_id."/invitation.t.txt";
            } else {
                $custom_template_file = $custom.$lang_cd."/mail/0/invitation.t.txt";
            }
            if ($custom_template_file && file_exists($template->template_dir.$custom_template_file)) {
                $template_file = $custom_template_file;
            } else {
                $template_file = $template->template_dir.$lang_cd."/mail/invitation.t.txt";
            }
            $this->logger->info($template_file);
        } else if ($custom) {
            $custom = 'custom/'.$custom.'/';
            $custom_template_file = $custom . "/common/multi_lang/invitation.t.txt";
            if (file_exists($template->template_dir.$custom_template_file)){
                $template_file = $custom_template_file;
            }else{
                $custom_template_file = $custom. "/" . $lang_cd."/mail/invitation.t.txt";
                if (file_exists($template->template_dir.$custom_template_file)) {
                    $template_file = $custom_template_file;
                } else {
                    $template_file = "common/mail_template/meeting/invitation.t.txt";
                }
            }
        } else {
            $template_file = "common/mail_template/meeting/invitation.t.txt";
        }
        $frame["lang"] = $lang_cd;
        $template->assign("__frame",$frame);
        $body          = $template->fetch($template_file);

        $mail->setBody($body);
        $mail->send();
        $this->logger->info(RESERVATION_FROM);
        return (string) 1;
    }

    public function getShowList()
    {
    }

    private function getMessage($lang, $group, $id = "") {
        static $message;
        if (!$message[$lang]) {
            $config_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
            $message[$lang] = parse_ini_file($config_file, true);
        }
        if (!$id) {
            return $message[$lang][$group];
        } else {
            return $message[$lang][$group][$id];
        }
    }

    public function getInvitationMailUrl($meeting_session_id, $lang, $type = "normal")
    {
        require_once "classes/dbi/meeting_invitation.dbi.php";
        $objMeetingInvitationTable = new MeetingInvitationTable($this->dsn);
        $this->logger->info($type);
        if($type == "normal") {
            $column = "user_session_id";
        } elseif ($type == "authority") {
            $column = "authority_session_id";
        } else {
            $column = "audience_session_id";
        }
        $invitationData = $objMeetingInvitationTable->getRow(
                                                            sprintf("meeting_session_id='%s' AND status = 1", $meeting_session_id),
                                                            $column,
                                                            array("invitation_key"=>"desc"));
        $config      = EZConfig::getInstance();
        $account_dns = $config->get("GLOBAL", "auth_dsn");
        $session        = EZSession::getInstance();
        $user_info      = $session->get("user_info");
        require_once "classes/N2MY_DBI.class.php";
        $objCustomDomainServiceTable = new N2MY_DB($account_dns, "custom_domain_service");
        $customDomainServiceData     = $objCustomDomainServiceTable->getRow(sprintf("service_name='%s' AND status =1", $user_info["service_name"]));
        if ($customDomainServiceData["domain"]) {
            $url = sprintf("https://%s/g/%s/%s/%s", $customDomainServiceData["domain"], $lang, $meeting_session_id, $invitationData[$column]);
        } else {
            $url = sprintf("%sg/%s/%s/%s", N2MY_BASE_URL, $lang, $meeting_session_id, $invitationData[$column]);
        }
        return $url;
    }

    public function encryptUserStatus($meeting_session_id, $type = "normal")
    {
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $key = N2MY_ENCRYPT_KEY;
        $iv = substr($meeting_session_id, 0, 8);
        $encrypted_data = EZEncrypt::encrypt($key, $iv, $type);
        $encrypted_data = str_replace("/", "-", $encrypted_data);
        $encrypted_data = str_replace("+", "_", $encrypted_data);
        return urlencode($encrypted_data);
    }

    public function decryptUserStatus($object, $meeting_session_id)
    {
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $key = N2MY_ENCRYPT_KEY;
        $iv = substr($meeting_session_id, 0, 8);
        $object = urldecode($object);
        $object = str_replace("-", "/", $object);
        $object = str_replace(' ', '+', $object);
        $object = str_replace('_', '+', $object);
        return EZEncrypt::decrypt($key, $iv, $object);
    }
    //{{
}
