<?php

require_once(dirname(__FILE__)."/../../../lib/EZLib/EZCore/EZConfig.class.php");
require_once(dirname(__FILE__)."/../../../lib/EZLib/EZCore/EZLogger2.class.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/Polycom.class.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/conference/model/OnConferenceDestroyedRequestModel.php");

class DestroyedConferenceClass {
    var $logger	= "";
    var $config	= null;
    private $model;
    
    function __construct($request){
    	$this->dsn		= $this->getDsn();
        $this->logger = EZLogger2::getInstance();
        $this->config = EZConfig::getInstance(N2MY_CONFIG_FILE);
        define("N2MY_MDB_DSN", $this->config->get("GLOBAL", "auth_dsn"));
        
        $this->polycomClass = new PolycomClass($this->dsn);
        
        $this->model = new OnConferenceDestroyedRequestModel($request);
        $this->logger->info($this->model);
    }
    
    public function execute() {
    	// participant tableからpolycom clientを全て退出状態にする
    	$this->polycomClass->removeAllPolycomParticipantFromParticipantTableByConfId($this->model->confId);
    	
    	// 全ての参加者に退出フラグを立てる
    	$this->polycomClass->removeAllParticipantFromVideoConferenceParticipant($this->model->confId);
    	
    	// fmsへ人数の通知
    	$this->polycomClass->noticeRemoveConference($this->model->confId);
    	
    	// 会議終了処理
    	$this->polycomClass->destroyConference($this->model->confId);
    	
    	// Polycom会議周りのデータをクリア
    	$this->polycomClass->clearConferenceOption($this->model->confId);
    }
	
	private function getDsn() {
		$server_list = parse_ini_file(dirname(__FILE__)."/../../../config/server_list.ini");
		return $server_list[N2MY_DEFAULT_DB];
	}
}
