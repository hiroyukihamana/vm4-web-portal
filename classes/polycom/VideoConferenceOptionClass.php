<?php

require_once("config/config.inc.php");
require_once("lib/EZLib/EZCore/EZLogger2.class.php");
require_once("classes/mcu/config/McuConfigProxy.php");

require_once("classes/mcu/model/McuModel.php");
require_once("classes/polycom/Polycom.class.php");
require_once("classes/polycom/FMSController.php");

class VideoConferenceOptionClass {
	private $config;
	private $logger;
	private $dsn;
	private $mcuServerModel;
	private $mcuServerRecord;
	
	public function __construct($roomKey) {
		$this->proxy = new McuConfigProxy();
		$this->logger = EZLogger2::getInstance();
		$this->dsn = $this->proxy->getDsn();
		
		$this->polycom	= new PolycomClass($this->dsn);
		
		$this->mcuServerRecord = $this->polycom->defineMCUServerByRoomKey($roomKey);
		
		$this->ivesRecord = $this->getIvesInfoByRoomKey($roomKey);
		
		$this->fmsController = new FMSController();
	}
	
	private function getAddress($did, $domain) {
		$address = sprintf("%s@%s", $did, $domain);
		return $address;
	}
	
	public function getNumberAddress($did) {
		$address = null;
		if ($domain = $this->mcuServerRecord["number_domain"])
			$address = $this->getAddress($did, $domain);
		return $address;
	}
	
	public function getRegularAddress($did) {
		$address = null;
		if ($domain = $this->mcuServerRecord["server_address"])
			$address = $this->getAddress($did, $domain);
		return $address;
	}

	public function getH323TemporaryNumberAddress($did) {
		$address = null;
		if ($domain = $this->mcuServerRecord["guest_h323_number_domain"])
			$address = $this->getAddress($did, $domain);
		return $address;
	}

	public function getH323TemporaryAddress($did) {
		$address = null;
		if ($domain = $this->mcuServerRecord["guest_h323_domain"])
			$address = $this->getAddress($did, $domain);
		return $address;
	}
	
	public function getSipTemporaryNumberAddress($did) {
		$address = null;
		if ($domain = $this->mcuServerRecord["guest_number_domain"])
			$address = $this->getAddress($did, $domain);
		return $address;
	}
	
	public function getSipTemporaryAddress($did) {
		$address = null;
		if ($domain = $this->mcuServerRecord["guest_domain"])
			$address = $this->getAddress($did, $domain);
		return $address;
	}
	
	public function getVideoConferenceByMeetingKey ($meetingKey) {
		require_once("classes/dbi/video_conference.dbi.php");
		$videoConf		= new VideoConferenceTable($this->dsn);
		$videoWhere		= "meeting_key = '".addslashes($meetingKey)."'";
		$videoInfo	= $videoConf->getRow($videoWhere);
		return $videoInfo;
	}
	
	public function getActiveVideoConferenceByMeetingKey ($meetingKey) {
		require_once("classes/dbi/video_conference.dbi.php");
		$videoConf		= new VideoConferenceTable($this->dsn);
		$videoWhere		= "meeting_key = '".addslashes($meetingKey)."' AND is_active = 1";
		$videoInfo	= $videoConf->getRow($videoWhere);
		return $videoInfo;
	}
	
	public function getIvesInfoByRoomKey($roomKey) {
		require_once("classes/dbi/ives_setting.dbi.php");
		$ivesSettingTable= new IvesSettingTable($this->dsn);
		$where = "room_key='".$roomKey."' and is_deleted = 0";
		$ivesInfo	= $ivesSettingTable->getRow($where);
		
		require_once("classes/dbi/ordered_service_option.dbi.php");
		$service_option  = new OrderedServiceOptionTable($this->dsn);
		$where = sprintf("room_key = '%s' AND ordered_service_option_status = 1", $roomKey);
		$option = $service_option->getRow($where);
		if ($option) {
			return $ivesInfo;
		}
		else {
			return null;
		}
	}
	
	public function getDidByRoomKey($roomKey) {
		$ives = $this->getIvesInfoByRoomKey($roomKey);
		return $ives["ives_did"];
	}
	
	public function cancelReservation($confId) {
		$this->polycom->removeAllPolycomParticipant($confId);
	}

	private function getDsn() {
		$server_list = parse_ini_file(dirname(__FILE__)."/../../config/server_list.ini");
		return $server_list[N2MY_DEFAULT_DB];
	}
	
	public function getConfig() {
		return McuConfig::getInstance();
	}
	
	public function getMcuGatewayByRoomKey($roomKey) {
		$settingRecord = $this->getIvesInfoByRoomKey($roomKey);

		$info = array(
				"status"			=> true,
				"contract"			=> true,
				"sipServer"			=> sprintf("%s:%d", $this->mcuServerRecord["server_address"], $this->mcuServerRecord["controller_port"]),
				"sipServerDomain"	=> $this->mcuServerRecord["server_address"],
				"sipProxyIp"		=> $this->mcuServerRecord["server_address"],
				"sipProxyPort"		=> $this->mcuServerRecord["sip_proxy_port"],
				"socketServer"		=> $this->mcuServerRecord["server_address"],
				"socketServerPort"	=> $this->mcuServerRecord["socket_port"],
				"defaultId"			=> $settingRecord['client_id'],
				"defaultPw"			=> $settingRecord['client_pw'],
				"defaultViewId"		=> $this->proxy->get("defaultViewId"),
				"defaultViewPw"		=> $this->proxy->get("defaultViewPw"),
				"smallId"			=> $this->proxy->get("smallId"),
				"smallPw"			=> $this->proxy->get("smallPw"),
				"smallViewId"		=> $this->proxy->get("smallViewId"),
				"smallViewPw"		=> $this->proxy->get("smallViewPw"),
				"telephoneId"		=> $this->proxy->get("telephoneId"),
				"telephonePw"		=> $this->proxy->get("telephonePw"),
				"did"				=> $settingRecord['ives_did']);
		$model = new McuModel($info);
		return $model->getContent();
	}
	
	public function addVideoConferenceParticipant($meetingKey, $participantKey) {
		$this->polycom->addVideoConferenceParticipant($meetingKey, $participantKey);
	}
	
	public function getReservationGuestDidByParmanentDid($did) {
		return $this->polycom->getReservationGuestDidByParmanentDid($did);
	}
	
	public function getInvitationGuestDidByParmanentDid($did) {
		return $this->polycom->getInvitationGuestDidByParmanentDid($did);
	}
	
	public function deleteTemplateGuestDid($guestDid) {
// 		$did = $this->ivesRecord["ives_did"];
		$this->polycom->deleteTemplateGuestDid($guestDid);
	}
	
	public function getUsageTimeFromMeetingUpTime($meeting_key) {
		return $this->polycom->getUsageTimeFromMeetingUpTime($meeting_key);
	}
	
	public function addParticipantKey2VideoParticipantRecord() {}
	
	public function updateListWithoutPolycom($meeting_key, $participant_list) {
        $obj_Participant   = new DBI_Participant($this->dsn);
		$result = $obj_Participant->updateListWithoutPolycom($meeting_key, $participant_list);
		return $result;
	}
	
	public function getParticipatedList($meetingKey) {
		require_once("classes/dbi/video_conference.dbi.php");
		$video		= new VideoConferenceTable($this->dsn);
		$where		= "meeting_key = '".addslashes($meetingKey)."' AND is_active = 1";
		$videoInfo	= $video->getRow($where);
		$participant_sip_list = array();
		if ($videoInfo) {
			require_once("classes/dbi/video_conference_participant.dbi.php");
			$videoConf		= new VideoConferenceParticipantTable($this->dsn);
			$where = "conference_id = '".$videoInfo["conference_id"]."'" .
					" AND meeting_key = '".$meetingKey."'" .
					" AND part_id <> ''".
					" AND is_active = 1";
			$participant_sip_list = $videoConf->getRowsAssoc($where);
		}
		return $participant_sip_list;
	}
	
	public function setVideoMute($meeting_key, $participant_key, $flag) {}
	public function getCallingStatus($videoParticipantKey) {}
	public function cancelCallingParticipant($meetingKey) {}
	public function startDocumentSharing($meetingKey) {}
	public function stopDocumentSharing($meetingKey) {}
	public function documentSharing($meetingKey, $documentId, $page=null) {}
	
	public function removeConference($confId) {
		try {
			$this->polycom->removeConference($confId);			
		}
		catch(Exception $e) {
			$this->logger->warn($e->getMessage());
		}
	}
	
	public function changeDataCenter($meetingRecord) {

		$sequenceRecord = $this->polycom->getMeetingSequenceRecordByMeetingKey($meetingRecord["meeting_key"]);
		if (!$sequenceRecord)
			throw new Exception("bootFMSInstance::check sequence record. meeting_key: ".$meetingRecord["meeting_key"]);
		
		$fmsRecord = $this->polycom->getServerRecordByServerKey($sequenceRecord["server_key"]);
		if (!$fmsRecord)
			throw new Exception("bootFMSInstance::cannot resolve fms server. server_key: ".$sequenceRecord["server_key"]);
			
		$path  = $this->proxy->getWithGroupName("CORE", "app_name")."/".$meetingRecord["fms_path"].$meetingRecord["meeting_session_id"];
		$this->fmsController->connect($fmsRecord["server_address"], $path);
		
		$bootParams = $this->getMcuGatewayByRoomKey($meetingRecord["room_key"]);
		$this->fmsController->bootFMSInstance($bootParams);
		
		$participantList = $this->polycom->getPolycomParticipantListByMeetingKey($meetingRecord["meeting_key"]);
		$this->fmsController->noticeParticipantListChanged($participantList);
	}
	
	public function forceExitPolycomClients($meetingKey) {
		require_once("classes/dbi/video_conference.dbi.php");
		$video		= new VideoConferenceTable($this->dsn);
		$where		= "meeting_key = '".addslashes($meetingKey)."' AND is_active = 1";
		$videoInfo	= $video->getRow($where);
		if ($videoInfo)
			$this->removeConference($videoInfo["conference_id"]);
	}
}
