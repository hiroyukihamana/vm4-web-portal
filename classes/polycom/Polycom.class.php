<?php

require_once("classes/N2MY_IvesClient.class.php");
require_once("classes/N2MY_Meeting.class.php");

require_once("classes/core/Core_Meeting.class.php");
require_once("classes/core/dbi/DataCenter.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once('classes/core/dbi/MeetingOptions.dbi.php');
require_once("classes/core/dbi/MeetingSequence.dbi.php");
require_once('classes/core/dbi/SharingServer.dbi.php');

require_once("classes/dbi/datacenter_ignore.dbi.php");
require_once("classes/dbi/datacenter_priority.dbi.php");
require_once("classes/dbi/fms_deny.dbi.php");
require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/dbi/ordered_service_option.dbi.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/video_conference.dbi.php");
require_once("classes/dbi/video_conference_participant.dbi.php");
require_once("classes/dbi/user.dbi.php");

require_once("classes/mgm/dbi/mcu_server.dbi.php");

require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/McuModel.php");
require_once("classes/polycom/FMSController.php");

require_once("lib/EZLib/EZCore/EZLogger2.class.php");
require_once("lib/EZLib/EZCore/EZConfig.class.php");

define("VIDEO_FOR_FLASH", 1);
define("VIDEO_FOR_POLYCOM", 0);

define("ONE_X_ONE", 0);
define("TWO_X_TWO", 1);
define("THREE_X_THREE", 2);
define("THREE_PLUS_FOUR", 3);
define("ONE_PLUS_SEVEN", 4);
define("ONE_PLUS_FIVE", 5);
define("ONE_PLUS_ONE", 6);

class PolycomClass {
    private $logger	= "";
    private $config	= null;
    
    private $ivesClient;
    private $videoConfence;
    private $videoConfenceParticipant;
    
    function __construct($dsn){
    	$this->dsn = $dsn;
        $this->logger = EZLogger2::getInstance();
        $this->config = EZConfig::getInstance(N2MY_CONFIG_FILE);
        $this->configProxy 	= new McuConfigProxy();
        
        define("N2MY_MDB_DSN", $this->configProxy->getAuthDsn());
        
        $this->ivesClient				= new N2MY_IvesClient();
        $this->setting					= new IvesSettingTable($this->dsn);
        $this->videoConfence			= new VideoConferenceTable($this->dsn);
        $this->videoConfenceParticipant	= new VideoConferenceParticipantTable($this->dsn);
        $this->participant				= new DBI_Participant($this->dsn);
		$this->mcuServer				= new McuServerTable($this->config->get("GLOBAL", "auth_dsn"));

		$this->coreMeeting				= new Core_Meeting($this->dsn);
		$this->dataCenter		= new DBI_DataCenter(N2MY_MDB_DSN);
		$this->datacenterIgnore			= new DatacenterIgnoreTable($this->dsn);
		$this->datacenterPriority= new DatacenterPriorityTable($this->dsn);
		$this->fmsDeny					= new FmsDenyTable($this->dsn);
		$this->fmsServer				= new DBI_FmsServer(N2MY_MDB_DSN);
		$this->meeting					= new MeetingTable($this->dsn);
		$this->meetingOptions 			= new DBI_MeetingOptions($this->dsn);
		$this->meetingSequence			= new DBI_MeetingSequence($this->dsn);
        $this->sharingServer			= new DBI_SharingServer($this->dsn);
        $this->user  					= new UserTable($this->dsn);

        $this->fmsController = new FMSController();
    }
    
    // IVeSリクエストの送信先を変える
    public function setWsdlDomain($domain) {
        $this->ivesClient->setWsdlDomain($domain);
    }
    
    public function setClient($domain) {
        $this->ivesClient->setClient($domain);
    }
     
    public function updatePolycomClientUseCount($meeting_key) {
    	$obj_Participant = new DBI_Participant($this->dsn);
	    $where =	"meeting_key = '".$meeting_key."'".
				    " AND is_active = 1";
        $data = array("use_count" => 1);
        $obj_Participant->update($data, $where);
    }
    
    // polycom option 契約数check
    public function checkPolycomContractNumber($confId, $did) {
		$contractNumber = $this->getPolycomContractNumber($did);
		$videConferenceInfo = $this->getActiveVideoConferenceByConfId($confId);
		
		//　一人目の入室時
		if (!$videConferenceInfo) {
			return true;
		}
		// ２人目以降
		$objRoom	= new RoomTable($this->dsn);
		$roomInfo = $objRoom->findByKey($videConferenceInfo["room_key"]);
		
        // participant list
        $objParticipant	= new DBI_Participant($this->dsn);
		$where  =	"meeting_key = '".$videConferenceInfo["meeting_key"]."'" .
					" AND is_active = 1";
        $participantList	= $objParticipant->getRowsAssoc($where);
        $formatedParticipantList	= $this->formatParticipantList($participantList);
        $participantCount	= count($participantList);
        $sipCount			= count($formatedParticipantList['sip']);
        $rtmpCount			= count($formatedParticipantList['rtmp']);
        $audience			= count($formatedParticipantList['audience']);
        // 
        return ($roomInfo["max_seat"] > ($rtmpCount + $sipCount) && $sipCount < $contractNumber )? true: false;
    }
    
    // polycom option 契約数
    public function getPolycomContractNumber($did) {
    	$obj = $this->setting->getRow(sprintf("ives_did = '%s' AND is_deleted != 1", $did));
    	if (0 == $obj["num_profiles"])
    		throw new Exception("check polycom contract number of room: '%d'", $roomKey);
    	else
    		return $obj["num_profiles"];
    }
    
    public function addPolycomParticipant($confId, $partId, $participantPort, $participantName, $meetingKey, $did) {
    	function getRandomString($nLengthRequired = 8){
    		$sCharList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
    		mt_srand();
    		$sRes = "";
    		for($i = 0; $i < $nLengthRequired; $i++)
    			$sRes .= $sCharList[mt_rand(0, strlen($sCharList) - 1)];
    			return $sRes;
    	}
    	
        $objParticipant	= new DBI_Participant($this->dsn);
        $data = array(
            "participant_session_id"=> getRandomString(20),
            "meeting_key"			=> $meetingKey,
            "participant_name"		=> $participantName,
            "participant_port"		=> $participantPort,
            "participant_protocol"	=> "sip",
            "update_datetime"		=> date("Y-m-d H:i:s"),
            "is_active"				=> 1,
            "uptime_start"			=> date("Y-m-d H:i:s"),
            "uptime_end"			=> null,
            "is_narrowband"			=> 0,
            "use_count"				=> 0,
            "participant_locale"	=> 'ja',
            "create_datetime"		=> date('Y-m-d H:i:s'),
            "ives_part_id"			=> $partId
        );
        $participantRecord  = $objParticipant->addPolycomUser($data);
        
        //
        $this->addVideoConferenceParticipant($meetingKey, $participantRecord, $partId);
    }
    
    private function getActiveVideoConferenceByMeetingKey($meetingKey) {
    	$where		= "meeting_key = '".addslashes($meetingKey)."' AND is_active = 1";
    	$sort = array(
    			"video_conference_key" => "desc",
    			"createtime" => "desc"
    	);
    	$videoInfo	= $this->videoConfence->getRow($where, "*", $sort);
    	return $videoInfo;
    }
    
    private function getActiveVideoConferenceByConfId($confId) {
    	$where		= "conference_id = '".addslashes($confId)."' AND is_active = 1";
    	$sort = array(
    			"video_conference_key" => "desc",
    			"createtime" => "desc"
    	);
    	$videoInfo	= $this->videoConfence->getRow($where, "*", $sort);
    	return $videoInfo;
    }
    
    public function addVideoConferenceParticipant($meetingKey, $participantRecord, $partId=0) {
    	$videoInfo	= $this->getActiveVideoConferenceByMeetingKey($meetingKey);
    	$videoConfData = array(
    			"video_conference_key"=>$videoInfo["video_conference_key"],
    			"conference_id"=>$videoInfo["conference_id"],
    			"did"=>$videoInfo["did"],
    			"participant_key"=>$participantRecord["participant_key"],
    			"meeting_key"=>$videoInfo["meeting_key"],
    			"part_id"=>$partId,
    			"is_active"=>1,
    			"createtime"=>date('Y-m-d H:i:s'));
    	
    	$this->videoConfenceParticipant->add($videoConfData);
    }
    
    public function getRejectParams($confId, $partId) {
        $params = array(
            "confId"	=> $confId,
            "partId"	=> $partId
            );
    	return $params;
    }
    
    public function getRemoveParams($confId, $partId) {
        $params = array(
            "confId"	=> $confId,
            "partId"	=> $partId
            );
    	return $params;
    }
    
    public function callCreateSidebar($confId) {
    	$result = $this->ivesClient->createSidebar($confId);
    	if ($result->return)
    		return $result->return;
    	else
    		throw new Exception("callCreateSidebar fault.".$confId);
    }
    
    public function getMosaicParams($confId, $partId, $type) {
    	$params = array();
    	if ($type == "SIP") {
            $params = array(
                "confId"	=> $confId,
                "mosaicId"	=> VIDEO_FOR_FLASH,
                "partId"	=> $partId);
    	}
    	else if ($type == "WEB") {
            $params = array(
                "confId"	=> $confId,
                "mosaicId"	=> VIDEO_FOR_POLYCOM,
                "partId"	=> $partId);
    	}
    	return $params;
    }
    
    public function getAcceptParams($confId, $partId, $type) {
    	$params = array();
    	if ($type == "SIP") {
            $params = array(
                "confId"	=> $confId,
                "partId"	=> $partId,
                "mosaicId"	=> VIDEO_FOR_POLYCOM,
            	"sidebarId"	=> 0);
    	}
    	else if ($type == "WEB") {
            $params = array(
                "confId"	=> $confId,
                "partId"	=> $partId,
                "mosaicId"	=> VIDEO_FOR_FLASH,
            	"sidebarId"	=> 1);
    	}
    	return $params;
    }
    
    public function getSidbarParams($confId, $partId, $sidebarId) {
        $params = array(
            "confId"	=> $confId,
            "partId"	=> $partId,
            "sidebarId"	=> 1	//
            );
    	return $params;
    }

    private function formatParticipantList($participantList) {
        $sip  = array();
        $rtmp = array();
        $audience = array();
        foreach ($participantList as $list) {
            $protocol = $list['participant_protocol'];
            if ("sip" == $protocol) {
                $sip[] = $list;
            } else if ("rtmp" == $protocol) {
            	if (2 == $list["participant_type_key"]) {
            		$audience[] = $list;
            	}
            	else {
                	$rtmp[] = $list;            		
            	}
            }
        }
        $participantList = array("sip" => $sip, "rtmp" => $rtmp, "audience" => $audience);

        return $participantList;
    }
    
    public function getFlashParticipantListByMeetingKey($meetingKey) {
		$objParticipant = new DBI_Participant($this->dsn);
        $where = "meeting_key = '".$meetingKey."'".
                 " AND is_active = 1".
                 " AND participant_protocol != 'sip'";
		return $objParticipant->getRowsAssoc($where);
    }
    
    public function getFlashParticipantListFromVideoConferenceByConfId($confId) {
    	$where = "conference_id = '".$confId."'".
                 " AND is_active = 1".
    			" AND part_id = 0";
    	return $this->videoConfenceParticipant->getRow($where);
    }
    
    public function getAllPolycomParticipantListByMeetingKey($meetingKey) {
		$objParticipant = new DBI_Participant($this->dsn);
        $where = "meeting_key = '".$meetingKey."'".
                 " AND participant_protocol = 'sip'";
		return $objParticipant->getRowsAssoc($where);
    }
    
    public function getPolycomParticipantListByMeetingKey($meetingKey) {
		$where = "meeting_key = '".$meetingKey."'".
				" AND is_active = 1".
				" AND part_id <> 0";
		return $this->videoConfenceParticipant->getRowsAssoc($where);
    }
    public function getPolycomParticipantListByConfId($confId) {
    	$where = "conference_id = '".$confId."'".
    			" AND is_active = 1".
    			" AND part_id != ''";
    	return $this->videoConfenceParticipant->getRowsAssoc($where);
    }
    
    public function getParticipantListExceptPolycomByMeetingKey($meetingKey) {
        $where = "meeting_key = '".$meetingKey."'".
                 " AND is_active = 1".
                 " AND participant_protocol != 'sip'";
		return $this->participant->getRowsAssoc($where);
    }
    
    public function getMeetingUserList($meetingKey) {
    	$where = "meeting_key = '".$meetingKey."'".
    			" AND is_active = 1".
    			" AND participant_type_key <> 2".	// audience以外
    			" AND participant_protocol <> 'sip'";
    	return $this->participant->getRowsAssoc($where);
    }
	
	public function getTargetPolycomParticipant($confId, $partId) {
		$where = "conference_id = '".$confId."'".
                 " AND part_id = '".$partId."'".
                 " AND is_active = 1";
		return $this->videoConfenceParticipant->getRow($where);
	}
	
	public function removePolycomParticipant($confId, $partId) {
		$where = "conference_id = '".$confId."'".
				" AND part_id = '".$partId."'".
				" AND is_active = 1";
		$videoConfParticipant = $this->videoConfenceParticipant->getRow($where);
		if (!$videoConfParticipant) return;
		
		// participant 削除
		$objParticipant = new DBI_Participant($this->dsn);
        $where = "participant_key = '".$videoConfParticipant["participant_key"]."'";
        $data = array("is_active" => 0);
        $objParticipant->update($data, $where);
        
        // video conf participant 削除
        $where = "video_participant_key = ".$videoConfParticipant["video_participant_key"];
		$data = array(
        			"updatetime"=>date("Y-m-d H:i:s"),
        			"is_active" => 0
        		);
        $this->videoConfenceParticipant->update($data, $where);
	}
	
	public function removeAllPolycomParticipantFromParticipantTableByConfId($confId) {
		$where = "conference_id = '".$confId."'".
				" AND part_id <> 0".
				" AND is_active = 1";
		$list = $this->videoConfenceParticipant->getRowsAssoc($where);
		$arr = array();
		for ($i = 0; $i < count($list); $i++) {
			array_push($arr, "participant_key = '".$list[$i]['participant_key']."'");
		}
		if (count($arr) <= 0) return;
		
		$objParticipant = new DBI_Participant($this->dsn);
		$where = implode(" OR ", $arr);
		$data = array(
				"is_active" => 0,
				"update_datetime"=>date("Y-m-d H:i:s"));
		$objParticipant->update($data, $where);
	}
	
	public function removeAllParticipantFromVideoConferenceParticipant($confId) {
		$where = "conference_id = '".$confId."'".
				" AND is_active = 1";
		$data = array(
				"is_active" => 0,
				"updatetime"=>date("Y-m-d H:i:s"));
		$this->videoConfenceParticipant->update($data, $where);
	}
	
	public function removeFlashParticipantFromVideoConferenceParticipant($confId, $partId) {
		$where = "conference_id = '".$confId."'".
				" AND part_id = 0".
				" AND is_active = 1";
		$data = array(
				"is_active" => 0,
				"updatetime"=>date("Y-m-d H:i:s"));
		$this->videoConfenceParticipant->update($data, $where);
	}
	
	public function stopMeeting($meetingKey) {
		$objMeeting			= new MeetingTable($this->dsn);
        $objMeetingSequence	= new DBI_MeetingSequence($this->dsn);
        
        $where 	= "meeting_key = '".$meetingKey."'" .
                  " AND is_active = 1";
        $data	= array("is_active" => 0);
        $resMeeting = $objMeeting->update($data, $where);
        
        $where 	= "meeting_key = '".$meetingKey."'" .
                  " AND status = 1";
        $data = array("status" => 0);
        $resMeetingSequence = $objMeetingSequence->update($data, $where);
	}
	
	public function checkCompositionStatus() {
		return true;
	}
	
	public function setFlashComposition($confId, $did, $meetingKey) {
		$policomList = $this->getPolycomParticipantListByMeetingKey($meetingKey);
		$conference = $this->ivesClient->callGetConference($confId);
		
		if (0 < count($policomList)) {
			if ($conference->return) {
				$flashCompositionParams = $this->getFlashCompositionParams($confId, $did, $meetingKey);
				$this->ivesClient->setFlashComposition($flashCompositionParams);
			}
		}
	}
	
	private function getParticipantsFromController($confId) {
		$return = $this->ivesClient->getParticipants(array("confId"=>$confId))->return;
		$list = array();
		if (0 == strcasecmp(gettype($return), "object")) {
			array_push($list, $return);
		}
		else if (0 == strcasecmp(gettype($return), "array")) {
			$list = $return;
		}
		for ($i = 0; $i < count($list); $i++) {
			$client = $list[$i];
			$name = explode("@", $client->name);
			if ($name[0] == $this->configProxy->get("defaultViewId")) {
				array_splice($list, $i, 1);
			}
		}
		return $list;
	}
	
	private function getPolycomCompositionType($count, $activeSpeaker = false) {
		if ($activeSpeaker) {
			if (1 == $count) {
				$compositionType = ONE_X_ONE;
			}
			else if (2 == $count) {
				$compositionType = ONE_PLUS_ONE;
			}
			else if (3 <= $count && $count <= 6) {
				$compositionType = ONE_PLUS_FIVE;
			}
			else {
				$compositionType = ONE_PLUS_SEVEN;
			}
		}
		else {
			if (1 == $count) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $count && $count <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $count) {
				$compositionType = THREE_X_THREE;
			}
		}
		return $compositionType;
	}
	
	public function setPolycomComposition($confId, $did) {
		$conference = $this->ivesClient->callGetConference($confId);
		if ($conference->return) {
			$list = $this->getParticipantsFromController($confId);
			$count = count($list);
			if (0 < $count) {
				$activeSpeakerFlg = $this->getActiveSpeakerStatusByDid($did);
				$compositionType = $this->getPolycomCompositionType($count, $activeSpeakerFlg);
				$polycomCompositionParams = array(
						"confId"	=> $confId,
						"mosaicId"	=> VIDEO_FOR_POLYCOM,
						"compType"	=> $compositionType,
						"size"		=> $this->configProxy->get("size"),
						"profileId"	=> $this->configProxy->get("profileId"));
				$this->ivesClient->setPolycomComposition($polycomCompositionParams);	
			}
		}
	}
	
	private function sortByParticipantType($participantList) {
		$list = array();
		for ($i = 0; $i < count($participantList); $i++) {
			$participant = $participantList[$i];
			if ($participant["participant_protocol"] == "sip") {
				$list["polycom"][] = $participant;
			}
			else if (2 == $participant["participant_type_key"]) {
				$list["audience"][] = $participant;
			}
			else {
				$list["normal"][] = $participant;
			}
		}
		return $list;
	}
	
	public function getFlashCompositionParams($confId, $did, $meetingKey) {
		if (!$this->getActiveSpeakerStatusByDid($did)) {
			$sipListNumber = count($this->getPolycomParticipantListByMeetingKey($meetingKey));
			if (1 == $sipListNumber) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $sipListNumber && $sipListNumber <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $sipListNumber) {
				$compositionType = THREE_X_THREE;
			}
			else {
				return false;
			}
		}
		else {
			$compositionType = ONE_X_ONE;
		}
        return array(
	            "confId"	=> $confId,
	            "mosaicId"	=> VIDEO_FOR_FLASH,
	            "compType"	=> $compositionType,
	            "size"		=> $this->configProxy->get("size"),
	            "profileId"	=> $this->configProxy->get("profileId"));
	}
	
	public function getPolycomProfileParams($confId, $partId) {
        return array(
		            "confId"	=> $confId,
		            "partId"	=> $partId,
		            "profileId"	=> $this->configProxy->get("polycomProfileId"));
	}
	
	public function getFlashProfileParams($confId, $partId) {
        return	array(
		            "confId"	=> $confId,
		            "partId"	=> $partId,
		            "profileId"	=> $this->configProxy->get("flashProfileId"));
	}
	
	public function changeProfile($params) {
		$result		= $this->ivesClient->changeProfile($params);
	}
	
	public function getAllParticipantListByConfernceId($confId) {
		$where = "conference_id = '".$confId."'".
                 " AND is_active = 1";
		$list = $this->videoConfenceParticipant->getRowsAssoc($where);
		return $list;
	}
	
	public function getAllParticipantListByMeetingKey($meetingKey) {
        $objParticipant	= new DBI_Participant($this->dsn);
		$where  =	"meeting_key = '".$meetingKey."'" .
					" AND is_active = 1";
        return $objParticipant->getRowsAssoc($where);
	}
	
	public function getConferenceIdByRoomKey($roomKey) {
        $video	= new VideoConferenceTable($this->dsn);
        $where	= "room_key = '".addslashes($roomKey)."' AND is_active = 1";
        return	$video->getRow($where, "*", array("video_conference_key" => "desc"));
	}
	
	public function getInvitationGuestDidByParmanentDid($did) {
		$callbackurl = sprintf($this->configProxy->get("invitationGuestCallbackUrl"), $this->configProxy->get("soap_domain"));
		$result		= $this->ivesClient->createTemplateGuestDid($did, $callbackurl);
		if ($result->return) {
			return $result->return;
		}
		else {
			throw new Exception("fault create guest did.");
		}
	}

 	public function getInvitationGuestDidByConfId($confId) {
		$callbackurl = sprintf($this->configProxy->get("invitationGuestCallbackUrl"), $this->configProxy->get("soap_domain"));
 		$result		= $this->ivesClient->createGuestDid($confId, $callbackurl);
		if ($result->return) {
			return $result->return;
		}
		else {
			throw new Exception("fault create guest did.");
		}
	}
	public function getReservationGuestDidByParmanentDid($did) {
		$callbackurl = sprintf($this->configProxy->get("reservationGuestCallbackUrl"), $this->configProxy->get("soap_domain"));
		$result		= $this->ivesClient->createTemplateGuestDid($did, $callbackurl);
		if ($result->return) {
			return $result->return;
		}
		else {
			throw new Exception("fault create guest did.");
		}
	}
 	public function getReservationGuestDidByConfId($confId) {
		$callbackurl = sprintf($this->configProxy->get("reservationGuestCallbackUrl"), $this->configProxy->get("soap_domain"));
		$result		= $this->ivesClient->createTemplateGuestDid($confId, $callbackurl);
		if ($result->return) {
			return $result->return;
		}
		else {
			throw new Exception("fault create guest did.");
		}
	}
	
	/**
	 * 
	 **/
	public function getMeetingByConfId($confId) {
		require_once("classes/dbi/video_conference.dbi.php");
		require_once("classes/dbi/room.dbi.php");
        $video		= new VideoConferenceTable($this->dsn);
        $where		= "conference_id = '".addslashes($confId)."' AND is_active = 1";
        $videoInfo	= $video->getRow($where, "*", array("video_conference_key" => "desc"));
        if (!$videoInfo) return;
        
        // get meeting ticket
        $roomObj		= new RoomTable($this->dsn);
        $roomInfo		= $roomObj->findByKey($videoInfo["room_key"]);
        $objN2MYMeeting = new N2MY_Meeting($this->dsn);
        
        // get target by meeting_ticket.
        $meeting = $objN2MYMeeting->getMeeting($roomInfo["room_key"], "polycom", $roomInfo["meeting_key"]);
        if (!$meeting) {
        	return null;
        }
        else {
        	$objMeeting		= new MeetingTable($this->dsn);
        	return $objMeeting->findByMeetingTicket($meeting["meeting_key"]);
        }
	}
	
	public function getMeetingByDid($did) {
		require_once("classes/N2MY_Meeting.class.php");
		require_once("classes/dbi/room.dbi.php");
		
		/* polycom契約情報取得 */
		$where = "ives_did='".addslashes($did)."'";
		$ivesInfo	= $this->setting->getRow($where);
		
		/* 部屋情報取得 */
		$roomObj		= new RoomTable($this->dsn);
		$roomInfo		= $roomObj->findByKey($ivesInfo["room_key"]);
		
		// get target by meeting_ticket.
		$objN2MYMeeting = new N2MY_Meeting($this->dsn);
		$meeting = $objN2MYMeeting->getMeeting($roomInfo["room_key"], "polycom", $roomInfo["meeting_key"]);
		if (!$meeting) {
			return null;
		}
		else {
			$objMeeting		= new MeetingTable($this->dsn);
			return $objMeeting->findByMeetingTicket($meeting["meeting_key"]);
		}
	}
	
	public function addConfId2VideoConference($meetingKey, $confId) {
		require_once("classes/dbi/room.dbi.php");
		$objMeeting		= new MeetingTable($this->dsn);
		$where = "meeting_key='".addslashes($meetingKey)."'";
		$meetingInfo = $objMeeting->getRow($where);
		
		
		/* polycom契約情報取得 */
		$where = "room_key='".$meetingInfo["room_key"]."' AND is_deleted = 0";
		$ivesInfo	= $this->setting->getRow($where);
		
		$video	= new VideoConferenceTable($this->dsn);
		$data	= array("user_key"=>$meetingInfo["user_key"],
						"meeting_key"=>$meetingKey,
						"room_key"=>$meetingInfo["room_key"],
						"conference_id"=>$confId,
						"did"=>$ivesInfo["ives_did"],
						"is_active"=>1,
						"createtime"=>date("Y-m-d H:i:s"),
						"starttime"=>date("Y-m-d H:i:s"));
		$videoKey =  $video->add($data);
		return $video->getRow(sprintf("video_conference_key='%s'", $videoKey));
	}
	
	public function createVideoConferenceParticipant($meetingKey, $confId) {
		$video	= new VideoConferenceTable($this->dsn);
		$where = "meeting_key='".$meetingKey."' AND conference_id = '".$confId."' AND is_active = 1";
		$videoInfo = $video->getRow($where);
		
    	$obj_Participant = new DBI_Participant($this->dsn);
	    $where =	"meeting_key = '".addslashes($meetingKey)."'".
	    			" AND participant_protocol <> 'sip'".
				    " AND is_active = 1";
        $participantList = $obj_Participant->getRowsAssoc($where);
        
        $videoConfData = array(
        		"video_conference_key"=>$videoInfo["video_conference_key"],
        		"conference_id"=>$videoInfo["conference_id"],
        		"meeting_key"=>$videoInfo["meeting_key"],
        		"is_active"=>1,
        		"createtime"=>date('Y-m-d H:i:s'));
        
        for ($i = 0; $i < count($participantList); $i++) {
        	$participant = $participantList[$i];
//         	$videoConfData["participant_key"] = $participant["participant_key"];
        	$this->addVideoConferenceParticipant($videoInfo["meeting_key"], $participant["participant_key"]);
//         	$this->videoConfenceParticipant->add($videoConfData);
        }
	}
	
	public function destroyConference($confId) {
		$where = "conference_id='".$confId."'";
		$video	= new VideoConferenceTable($this->dsn);
		$videoInfo = $video->getRow($where);
		
		if (!$videoInfo) {
			$this->logger->warn("no conference_id on db: ".$confId);
			return;
		}
		
		/* */
		$where = "room_key='".$videoInfo["room_key"]."' AND is_active = 1";
		$videolist = $video->getRowsAssoc($where);
		
		if (1 == count($videolist)) {
			$data = array("is_active"=>0,"updatetime"=>date("Y-m-d H:i:s"));
			$where = "did='".$videoInfo["did"]."' AND is_active = 1";
			$video->update($data, $where);
			$this->logger->info("destroyConference: ".$confId);
		}
		else if (1 < count($videolist)) {
			$this->logger->warn($videolist);
			$where = "did='".$videoInfo["did"]."' AND is_active = 1";
			$data = array("is_active"=>0,"updatetime"=>date("Y-m-d H:i:s"));
			$video->update($data, $where);
		}
		else {
// 			$this->logger->info("all confernce created by .".$videoInfo["did"]." have already ended");
		}
		
		$return = $this->getConferenceByDid($videoInfo["did"]);
		if ($return->return->id) {
		 	$this->removeConference($return->return->id);
		}
	}
	
	public function getMeetingByMeetingKey($meetingKey) {
		$objMeeting	= new MeetingTable($this->dsn);
        $where = "meeting_key = '".addslashes($meetingKey)."'";
        $result= $objMeeting->getRow($where);
		if ($result)
			return $result;
		else
			throw new Exception("no target meeting.");
	}
	
	public function getMeetingByMeetingTicket($ticket) {
		$objMeeting	= new MeetingTable($this->dsn);
		$result		= $objMeeting->findByMeetingTicket($ticket);
		if ($result)
			return $result;
		else
			throw new Exception("no target meeting.");
	}
	
	public function createMeetingByPolycomClient($confId, $did) {
		$where = "ives_did = '".addslashes($did)."' and is_deleted = 0";
		$optionInfo = $this->setting->getRow($where);
		if (!$optionInfo) {
			$this->logger->error("no ives setitng option.");
			throw new Exception("fail to create meeting.");
		}
        
        $meeting_key		= $optionInfo["room_key"]."_".md5(uniqid(rand(), true));
        $obj_N2MY_Meeting	= new N2MY_Meeting($this->dsn);
        $options = array(
            "user_key"			=> $optionInfo["user_key"],
            "country_id"		=> "jp",
            "start_time"		=> date("Y-m-d H:i:s"),
            "ives_did"			=> $did,
            "ives_conference_id"=> $confId
            );
        if (!$obj_N2MY_Meeting->createMeeting($optionInfo["room_key"], $meeting_key, $options)) {
			throw new Exception("fail to create meeting.");
        }
        
        require_once("classes/dbi/room.dbi.php");
        $obj_Room	= new RoomTable($this->dsn);
		$obj_Room->last_meeting_update($optionInfo["room_key"], $meeting_key);
		$objMeeting	= new MeetingTable($this->dsn);
		$meetingInfo = $objMeeting->findByMeetingTicket($meeting_key);
		
		
		require_once("classes/dbi/video_conference.dbi.php");
		$video	= new VideoConferenceTable($this->dsn);
		$data	= array("user_key"=>$meetingInfo["user_key"],
						"meeting_key"=>$meetingInfo["meeting_key"],
						"room_key"=>$meetingInfo["room_key"],
						"conference_id"=>$confId,
						"did"=>$did,
						"is_active"=>1,
						"createtime"=>date("Y-m-d H:i:s"),
						"starttime"=>date("Y-m-d H:i:s"));
		$video->add($data);
		
		return $meetingInfo;
	}
	
	public function getReservationInfo($meetingTicket) {
        require_once("classes/dbi/reservation.dbi.php");
    	$reserve		= new ReservationTable($this->dsn);
        $where			= "meeting_key = '".addslashes($meetingTicket)."'".
        				" AND reservation_status = 1";
        return $reserve->getRow($where, "*");
	}
	
	public function getMeetingByTemporaryAddress($address) {
		$objMeeting		= new MeetingTable($this->dsn);
        $where			= "temporary_did = '".addslashes($address)."'".
        				" AND is_active = 1";
        $result = $objMeeting->getRow($where, "*");
        if ($result)
        	return $result;
        else
        	throw new Exception("no reservation data");
	}
	
	public function checkExistDidInConferences($did) {
		$list		= $this->ivesClient->getConferences();
		if ($list->return) {
			foreach($list->return as $key => $value) {
				if ($value->did == $did)
					return $value;
			}
		}
		return;
	}
	
	public function removeConference($confId) {
		$return		= $this->ivesClient->callRemoveConference($confId);
	}
	
	private function getConferenceByDid($did) {
		$params = array("did" => $did);
		$return = $this->ivesClient->callGetConferenceByDid($params);
		return $return;
	}
	
	public function callCreateMosaic($confId) {
		$params = array(
				"confId"   => $confId,
				"compType" => 0,
				"size"     => 1);
		$mosaic = $this->ivesClient->callCreateMosaic($params);
		if ($mosaic->return)
			return $mosaic->return;
		else
			throw new Exception("callCreateMosaic fault.".$confId);
	}
	
	private function getActiveSpeakerStatusByDid($did) {
		$where = sprintf("ives_did = '%s' AND is_deleted != 1", $did);
		$return = $this->setting->getRow($where);
		return ($return && $return["use_active_speaker"]);
	}
	
	public function setPolycomMosaicSlot($confId, $did) {
		$params = array("confId"=>$confId,
				"mosaicId"=>0,
				"num"=>0,
				"partId"=>$this->getActiveSpeakerStatusByDid($did)? -2: 0);
		$this->ivesClient->setMosaicSlot($params);
	}
	
	public function setFlashMosaicSlot($confId, $did) {
		$params = array("confId"=>$confId,
				"mosaicId"=>1,
				"num"=>0,
				"partId"=>$this->getActiveSpeakerStatusByDid($did)? -2: 0);
		$this->ivesClient->setMosaicSlot($params);
	}
	
	public function createConference($did) {
		$conf		= $this->ivesClient->callCreateConferenceExt($did);
		if ($conf->return) {
	        $params = array(
	                "confId"   => $conf->return->id,
	                "compType" => 0,
	                "size"     => 1);
			$mosaic = $this->ivesClient->callCreateMosaic($params);
			if ($mosaic->return)
				return $conf->return->id;
		}
		throw new Exception("create conference fault.".$did);
	}
	
	private function getAdhocTemplateParams($did, $vad = true, $mixer = null) {
		if (!$mixer) {
			$mixer = $this->configProxy->get("mixerId");
		}
			
		$params = array(
				"name"=>$this->configProxy->get("conference_name"),
				"did"=>$did,
				"mixerId"=>$mixer,
				"mixerId2"=>$this->configProxy->get("mixerId2"),
				"size"=>$this->configProxy->get("size"),
				"compType"=>0,
				"vad"=>$vad,
				"profileId"=>$this->configProxy->get("profileId"),
				"autoAccept"=>false,
				"audioCodecs"=>"",
				"videoCodecs"=>"",
				"textCodecs"=>"",
				"properties"=>"b2bapi.callbackUrl=".sprintf($this->configProxy->get("callback_conference"), $this->configProxy->get("soap_domain"))
		);
		return $params;
	}
	
	public function createAdhocConferenceTemplate($did, $vad, $mixer) {
		$params = $this->getAdhocTemplateParams($did, $vad, $mixer);
		$return = $this->ivesClient->callCreateConferenceTemplateExt($params);
		if (1 != $return->return)
			throw new Exception("create adhocConferenceTemplate fault.".$did);
	}
	
	public function removeAdhocConferenceTemplate($did) {
		$return = $this->ivesClient->calRemoveConferenceTemplateExt($did);
	}
	
	public function deleteTemplateGuestDid($did) {
		$this->ivesClient->deleteTemplateGuestDid($did);
	}
	
	public function createDid() {
        try{
            do {
                $did = $this->makeRandStr($this->configProxy->get("didLength"));
                $res	= $this->setting->findByDid($did);
                if (PEAR::isError($res)) {
                    throw new Exception("select ives_setting did failed PEAR said : ".$did);
                }
                if ($res) {
                    continue;
                }
                break;
            }
            while (true);
        	return $did;
        }
        catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            throw $e;
        }
	}
	
	private function makeRandStr($length) {
		$str = array_merge(range('a', 'z'), range('0', '9'));
		for ($i = 0; $i < $length; $i++) {
			$r_str .= $str[rand(0, count($str))];
		}
		return $r_str;
	} 
	
	public function recoveryDid() {
		require_once("classes/polycom/PolycomOptionClass.php");
		require_once("classes/dbi/video_conference_operation_log.dbi.php");
		
        $videoOperation		= new VideoConferenceOperationLogTable($this->dsn);
        $operationId		= $videoOperation->getThisOperationId();
		$where  =	" is_deleted = 0";
		$list = $this->setting->getRowsAssoc($where);
		foreach ($list as $key => $value) {
			$polycomOptionClass = new PolycomOptionClass($this->dsn);
			$polycomOptionClass->setOperationId($operationId);
			$polycomOptionClass->create($value["ives_did"], $value["room_key"], $value["user_key"]);
		}
	}
	
	public function removeAllPolycomParticipant($confId) {
		$list = $this->getParticipantsFromController($confId);
		for ($i = 0; $i < count($list); $i++) {
			$participant = $list[$i];
			
			// remove participant
			$removeParams = $this->getRemoveParams($confId, $participant->id);
			$this->ivesClient->removeParticipant($removeParams);
		}
	}
	
	public function getUsageTimeFromMeetingUpTime($meetingKey) {
		require_once('classes/core/dbi/Meeting.dbi.php');
		require_once "classes/core/dbi/MeetingUptime.dbi.php";
		$obj_MeetingUptime = new DBI_MeetingUptime($this->dsn);
    	$obj_Meeting        = new DBI_Meeting( $this->dsn );
    	
        $useList = $obj_MeetingUptime->getRowsAssoc(sprintf("meeting_key=%s", $meetingKey));
        $usetime = 0;
        foreach ($useList as $key => $uptime) {
        	$usage = strtotime($uptime["meeting_uptime_stop"]) - strtotime($uptime["meeting_uptime_start"]);
        	if ($usage > 0) {
        		$usetime += ceil($usage / 60);
        	}
        }
        return $usetime;
	}
	
	public function getConferenceByConfId($confId) {
		return $this->ivesClient->callGetConference($confId);
	}
	
	public function getActiveVideoConference($meetingKey, $confId) {
		$video	= new VideoConferenceTable($this->dsn);
		$where	= "meeting_key = '".addslashes($meetingKey)."' AND conference_id = '".addslashes($confId)."' AND is_active = 1";
		return	$video->getRow($where);
	}
	
	public function getConfIdByMeetingKey($meetingKey) {
		$video	= new VideoConferenceTable($this->dsn);
		$where = "meeting_key='".addslashes($meetingKey)."' AND is_active = 1";
		$video	= $video->getRow($where);
		return $video["conference_id"];
	}
	
	public function reRegistMngrListener() {
		$this->ivesClient->reRegistMngrListener();
	}
	
	public function callParticipant($confId, $dest) {
		$params = array(
            "confId"	=> $confId,
            "dest"	=> $dest
            );
		return $this->ivesClient->callParticipant($params);
	}
	
	public function getDidByConfId($confId) {
		$where = "conference_id='".addslashes($confId)."'";
		$video	= $this->videoConfence->getRow($where);
		return $video["did"];
	}
	
	// clear
	public function clearConferenceOption($confId) {
		$did = $this->getDidByConfId($confId);
		
		$this->clearAllVideoConferenceByDid($did);
		
		$this->clearAllVideoConferenceParticipant($did);
	}
	
	private function clearAllVideoConferenceByDid($did) {
		$where = sprintf("did='%s' AND is_active = 1", $did);
		$count = $this->videoConfence->numRows($where);
		if ($count > 0) {
			$data = array(
					"is_active"=>0,
					"updatetime"=>date("Y-m-d H:i:s")
			);
			$this->videoConfence->update($data, $where);
			
			$this->logger->warn("remain active video_conferences. did: ".$did);
		}
	}
	
	private function clearAllVideoConferenceParticipant($did) {
		$where = sprintf("did='%s' AND is_active = 1", $did);
		$count = $this->videoConfenceParticipant->numRows($where);
		if ($count > 0) {
			$data = array(
					"is_active"=>0,
					"updatetime"=>date("Y-m-d H:i:s")
			);
			$this->videoConfenceParticipant->update($data, $where);
				
			$this->logger->warn("remain active video conference participants. did: ".$did);
		}
	}
	
	public function getClientInfo($confId, $partId) {
		$videoParticipant = null; 
		$participant = null;
		$where = "conference_id = '".$confId."'".
    			" AND part_id = ".$partId;
    	$videoParticipant = $this->videoConfenceParticipant->getRow($where);
    	if ($videoParticipant) {
    		$where = "participant_key = ".$videoParticipant["participant_key"];
    		$participant = $this->participant->getRow($where);
    	}
    	return array("video participant" =>$videoParticipant, "participant"=>$participant);
	}
	
	public function alert($message) {
		require_once("lib/pear/Mail.php");
		$title = "Polycom Notification [TIMEOUT] ".$_SERVER["SERVER_NAME"]. " - ";
		$sendTo = array(N2MY_ERROR_TO, "hanawa@vcube.co.jp", "yuji-hanawa@vcube.co.jp", "aoshima@vcube.co.jp", "watanabe@vcube.co.jp");
		$mail_body .= "--\n".print_r($message, true);
		
		$header['From'] = N2MY_ERROR_FROM;
		$header['Subject'] = $title;
		$mail = Mail::factory("sendmail");
		$ret = $mail->send($sendTo, $header, $mail_body);
		if(PEAR::isError($ret)) {
			die("エラーメッセージ：".$ret->getMessage());
		}
	}
	
	public function reject($confId, $partId) {
		$params=$this->getRejectParams($confId, $partId);
		$this->ivesClient->rejectParticipant($params);
	}
	
	public function acceptParticipant($conferenceId, $participantId, $type) {
		$params = $this->getAcceptParams($conferenceId, $participantId, $type);
		$this->ivesClient->acceptParticipant($params);
	}
	
	public function addSidebarParticipant($confId, $partId, $sidebarId) {
		$this->ivesClient->addSidebarParticipant($this->getSidbarParams($confId, $partId, $sidebarId));
	}
	
	public function addMosaicParticipant($conferenceId, $participantId, $type) {
		$mosaicParams = $this->getMosaicParams($conferenceId, $participantId, $type);
		$this->ivesClient->addMosaicParticipant($mosaicParams);
	}
	
	public function removeMosaicParticipant($conferenceId, $participantId, $type) {
		$mosaicParams = $this->getMosaicParams($conferenceId, $participantId, $type);
		$this->ivesClient->removeMosaicParticipant($mosaicParams);
	}
	
	public function defineMCUServerByRoomKey($roomKey) {
		$settingRecord = $this->setting->getRow(sprintf("room_key='%s' AND is_deleted=0", $roomKey));
		$mcuServerRecord = $this->mcuServer->getRow(sprintf("server_key=%d", $settingRecord["mcu_server_key"]));
// 		$model = $this->configProxy->parser->getServerModel($mcuServerRecord["server_address"]);
		
		if ($mcuServerRecord) {
// 			$wsdl = sprintf("%s:%d", $model->controllerRegularDomain, $model->controllerPort);
			$wsdl = sprintf("%s:%d", $mcuServerRecord["server_address"], $mcuServerRecord["controller_port"]);

			$this->ivesClient->setClient($wsdl);
			define("WSDL_DOMAIN", $wsdl);
		}
		return $mcuServerRecord;
	}
	
	public function getMeetingSequenceRecordByMeetingKey($meetingKey) {
		$resMeetingSequence = $this->meetingSequence->getActiveSequenceInfo($meetingKey);
		return $resMeetingSequence;
	}
	
	public function createMeetingSequenceRecord($meetingRecord) {
		if ($meetingRecord && $meetingRecord["meeting_key"] && $meetingRecord["server_key"]) {
			$data = array(
									"meeting_key" => $this->meetingRecord["meeting_key"],
									"server_key"  => $this->meetingRecord["server_key"]
					);
			//add server seaquence
			$meeting_sequence_key = $this->meetingSequence->add($data);
		}
		else {
			$this->logger->error($this->meetingRecord);
			throw new Exception("fail to create meeting_sequence_record caused by no meeting record");
		}
	}
	
	public function resolveEnvironmentData($meetingRecord) {
        $userRecord = $this->user->getRow("user_key = '".addslashes($meetingRecord["user_key"])."'", "user_id,is_compulsion_pw,compulsion_pw,meeting_version");
		if (!$userRecord["meeting_version"]) {
			$version = "4.6.5.0";
		} else {
			$version = $this->coreMeeting->getVersion();
		}
		
		// resolve sharing server
		$sharing_server_key = $this->sharingServer->getKeyByPriority($meetingRecord['meeting_country'], $ssl);
		$server_key = $this->resolveFMSServer($meetingRecord);

		$server_data = array(
				"version"            => $version,
				"fms_path"           => "mtg".date("Y/m/d/"),
				"server_key"         => $server_key,
				"sharing_server_key" => $sharing_server_key,
		);
        // 強制パスワード設定
        if (!$meetingRecord["meeting_log_password"] && $userRecord["is_compulsion_pw"] == "1") {
            $this->logger2->info($userRecord, "パスワード強制設定");
            $server_data["meeting_log_password"] = $userRecord["compulsion_pw"];
        }
		$where = "meeting_key = ".addslashes($meetingRecord['meeting_key']);
		$this->meeting->update($server_data, $where);
		$this->meetingRecord = $this->getMeetingByMeetingKey($meetingRecord['meeting_key']);
		return $this->meetingRecord;
	}
	
	private function resolveFMSServer($meetingRecord) {
		// SSLオプション
		$options = $this->meetingOptions->getList($meetingRecord["meeting_key"]);
		$ssl     = 0;
		$use_chinaline_status = 0;
		foreach($options as $option) {
			if ($option["option_name"] == "ssl") {
				$ssl = 1;
			}
			if ($option["option_name"] == "ChinaFastLine") {
				$use_chinaline_status = 1;
			}
			if ($option["option_name"] == "ChinaFastLine" && $option["option_name"] == "ChinaFastLine2") {
				$use_chinaline_status = 2;
			}
			if ($option["option_name"] == "GlobalLink") {
				$use_global_link = 1;
			}
		}
		
		//利用しない(利用停止中の)FMSを取得
		try {
			$_fms_deny_keys = $this->fmsDeny->findByUserKey($meetingRecord["user_key"], null, null, 0, "fms_server_key");
		} catch (Exception $e) {
			$this->logger2->error($e->getMessage());
			return false;
		}
		$fms_deny_keys = array();
		if ($_fms_deny_keys) {
			foreach ($_fms_deny_keys as $fms_key) {
				$fms_deny_keys[] = $fms_key["fms_server_key"];
			}
		}
		
		//除外DC
		$ignore_dc = $this->datacenterIgnore->get_datacenter_ignore($meetingRecord["user_key"]);
		if ($use_global_link) {
			$fms_country_priority = $this->configProxy->getWithGroupName("FMS_COUNTRY_PRIORITY",$meetingRecord["meeting_country"]);
			if (!$fms_country_priority) {
				$fms_country_priority = $this->configProxy->getWithGroupName("FMS_COUNTRY_PRIORITY","etc");
			}
			$datacenter_country_list = split(',', $fms_country_priority);
			$server_key = $this->fmsServer->getGlobalLinkFmsList($ssl, $fms_deny_keys, $datacenter_country_list ,$meetingRecord["need_fms_version"]);
		}
		else {
			//優先DC
			$priority_dc	=	$this->datacenterPriority->get_datacenter_priority($meetingRecord["user_key"],$meetingRecord["meeting_country"]);
			$priority_dc_all=	$this->datacenterPriority->get_datacenter_priority($meetingRecord["user_key"],"all");
			$priority_dc = array_merge($priority_dc, $priority_dc_all);
			
			//所在地以外のDCリスト取得
			$fms_country_priority = $this->configProxy->getWithGroupName("FMS_COUNTRY_PRIORITY",$meetingRecord["meeting_country"]);
			if (!$fms_country_priority) {
				$fms_country_priority = $this->configProxy->getWithGroupName("FMS_COUNTRY_PRIORITY","etc");
			}
			$datacenter_country_list = split(',', $fms_country_priority);
			$datacenter_list = array();
			foreach ( $datacenter_country_list as $datacenter_country) {
				if ($meetingRecord["meeting_country"] != $datacenter_country_list) {
					$country_priority_dc_list =
					$this->datacenterPriority->get_datacenter_priority($meetingRecord["user_key"],$datacenter_country);
				}
				if ($country_priority_dc_list) {
					if ($ignore_dc) {
						$ignore_dc = array_merge($ignore_dc, $country_priority_dc_list);
					}
				}
				$country_dc_list =	$this->dataCenter->getKeyByCountry($datacenter_country, $ignore_dc,$data["account_model"],$use_chinaline_status);
				if ($country_dc_list) {
					foreach( $country_dc_list as $country_dc ){
						$datacenter_list[] = $country_dc;
					}
				}
				$datacenter_list =	array_merge($country_priority_dc_list, $datacenter_list);
			}
			$datacenter_list = array_merge($priority_dc,$datacenter_list);
			$datacenter_list = array_map('serialize', $datacenter_list);
			$datacenter_list = array_unique($datacenter_list);
			$datacenter_list = array_map('unserialize', $datacenter_list);
			if ($datacenter_list) {
				$server_key = $this->fmsServer->getKeyByDataCenterList($ssl, $fms_deny_keys, $datacenter_list, $meetingRecord["need_fms_version"]);
			}
		}
		return $server_key;
	}
	
	public function getServerRecordByServerKey($serverKey) {
		$record = $this->fmsServer->getServerRecordByServerKey($serverKey);
		return $record;
	}
	
	public function getMcuGatewayByRoomKey($roomKey) {
		require_once("classes/dbi/ives_setting.dbi.php");
		$ivesSettingTable= new IvesSettingTable($this->dsn);
		$where = "room_key='".$roomKey."' and is_deleted = 0";
		$settingRecord	= $ivesSettingTable->getRow($where);
		
		require_once("classes/dbi/ordered_service_option.dbi.php");
		$service_option  = new OrderedServiceOptionTable($this->dsn);
		$where = sprintf("room_key = '%s' AND ordered_service_option_status = 1", $roomKey);
		$option = $service_option->getRow($where);
		if (!$option)
			return null;

		$mcuServerRecord = $this->defineMCUServerByRoomKey($roomKey);
		
		$info = array(
				"status"			=> true,
				"contract"			=> true,
				"sipServer"			=> sprintf("%s:%d", $mcuServerRecord["server_address"], $mcuServerRecord["controller_port"]),
				"sipServerDomain"	=> $mcuServerRecord["server_address"],
				"sipProxyIp"		=> $mcuServerRecord["server_address"],
				"sipProxyPort"		=> $mcuServerRecord["sip_proxy_port"],
				"socketServer"		=> $mcuServerRecord["server_address"],
				"socketServerPort"	=> $mcuServerRecord["socket_port"],
				"defaultId"			=> $settingRecord['client_id'],
				"defaultPw"			=> $settingRecord['client_pw'],
				"defaultViewId"		=> $this->configProxy->get("defaultViewId"),
				"defaultViewPw"		=> $this->configProxy->get("defaultViewPw"),
				"smallId"			=> $this->configProxy->get("smallId"),
				"smallPw"			=> $this->configProxy->get("smallPw"),
				"smallViewId"		=> $this->configProxy->get("smallViewId"),
				"smallViewPw"		=> $this->configProxy->get("smallViewPw"),
				"telephoneId"		=> $this->configProxy->get("telephoneId"),
				"telephonePw"		=> $this->configProxy->get("telephonePw"),
				"did"				=> $settingRecord['ives_did']);
		$model = new McuModel($info);
		return $model->getContent();
	}
	
	public function getSipParticipantListByMeetingKey($meetingKey) {
		$result = array();
		$where = sprintf("meeting_key='".$meetingKey."' AND participant_type='SIP' AND is_active=1");
		$result = $this->videoConfenceParticipant->getRowsAssoc($where);
		return $result;
	}
	
	public function noticeRemoveConference($confId) {
		$meetingRecord = $this->getMeetingByConfId($confId);
		$sequenceRecord = $this->getMeetingSequenceRecordByMeetingKey($meetingRecord["meeting_key"]);
		if (!$meetingRecord)
			return;
		
		if (!$sequenceRecord) {
			$this->logger->warn($meetingRecord["meeting_key"]);
			return;
		}
	
		$fmsRecord = $this->getServerRecordByServerKey($sequenceRecord["server_key"]);
		if (!$fmsRecord)
			throw new Exception("bootFMSInstance::cannot resolve fms server. server_key: ".$sequenceRecord["server_key"]);
	
		$path  = $this->configProxy->getWithGroupName("CORE", "app_name")."/".$meetingRecord["fms_path"].$meetingRecord["meeting_session_id"];
		$this->fmsController->connect($fmsRecord["server_address"], $path);

		$this->fmsController->noticeParticipantListChanged();
	}
}
