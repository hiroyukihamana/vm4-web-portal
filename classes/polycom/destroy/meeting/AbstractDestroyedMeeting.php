<?php
require_once(dirname(__FILE__)."/../../../../lib/EZLib/EZCore/EZConfig.class.php");
require_once(dirname(__FILE__)."/../../../../lib/EZLib/EZCore/EZLogger2.class.php");
require_once (dirname(__FILE__)."/../../../../classes/polycom/Polycom.class.php");

require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/polycom/FMSController.php");

abstract class AbstractDestroyedMeeting {
	protected $meeting;
	public function __construct($meeting) {
		$this->config = EZConfig::getInstance(N2MY_CONFIG_FILE);
		$this->logger = EZLogger2::getInstance($this->config);
		$this->dsn		= $this->getDsn();
		
		$this->polycomClass = new PolycomClass($this->dsn);
		
		$this->meeting = $meeting;

		$this->configProxy = new McuConfigProxy();
		$this->fmsController = new FMSController();
	}
	
	private function getDsn() {
		$server_list = parse_ini_file(dirname(__FILE__)."/../../../../config/server_list.ini");
		return $server_list[N2MY_DEFAULT_DB];
	}
	
	abstract public function setComposition($confId, $did);
	abstract public function removePolycomParticipant($confId, $partId);
	abstract public function checkConferenceStatus($confId);
	
	public function destroyConference($confId) {
		$this->polycomClass->destroyConference($confId);
	}
	
	public function noticeParticipantListChanged() {
		if (!$this->meeting)
			throw new Exception(__FUNCTION__."does not exist meeting record.");
		
		if (!$this->fmsController->isConnected()) {
			$this->connect();
		}

		$participantList = $this->polycomClass->getPolycomParticipantListByMeetingKey($this->meeting["meeting_key"]);
		$this->fmsController->noticeParticipantListChanged($participantList);
	}
	
	protected function connect() {
		$sequenceRecord = $this->polycomClass->getMeetingSequenceRecordByMeetingKey($this->meeting["meeting_key"]);
		if (!$sequenceRecord)
			throw new Exception("bootFMSInstance::check sequence record. meeting_key: ".$this->meeting["meeting_key"]);
		
		$fmsRecord = $this->polycomClass->getServerRecordByServerKey($sequenceRecord["server_key"]);
		if (!$fmsRecord)
			throw new Exception("bootFMSInstance::cannot resolve fms server. server_key: ".$sequenceRecord["server_key"]);
		
		$path  = $this->configProxy->getWithGroupName("CORE", "app_name")."/".$this->meeting["fms_path"].$this->meeting["meeting_session_id"];
		$this->fmsController->connect($fmsRecord["server_address"], $path);
	}
}