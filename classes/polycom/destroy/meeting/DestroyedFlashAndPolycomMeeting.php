<?php
require_once("AbstractDestroyedMeeting.php");

class DestroyedFlashAndPolycomMeeting extends AbstractDestroyedMeeting {
	protected $conferenceId;
	public function __construct($meeting) {
		parent::__construct($meeting);
	}
	
	public function removePolycomParticipant($confId, $partId) {
		$this->polycomClass->removePolycomParticipant($confId, $partId);
	}
	
	public function setComposition($confId, $did) {

		$this->polycomClass->setPolycomComposition($confId, $did);
		
		$this->polycomClass->setFlashComposition($confId, $did, $this->meeting["meeting_key"]);
	}
	
	public function checkConferenceStatus($confId) {
		$participantList = $this->polycomClass->getAllParticipantListByMeetingKey($this->meeting["meeting_key"]);
		$polycomList = $this->polycomClass->getPolycomParticipantListByMeetingKey($this->meeting["meeting_key"]);
		if (0 >= count($participantList)) {
			$core = new Core_Meeting($this->dsn);
			$core->stopMeeting($this->meeting["meeting_key"]);
			
			// video_conference 終了
			$this->polycomClass->destroyConference($confId);
		}
		else if (0 >= count($polycomList)) {
			// 全ての参加者に退出フラグを立てる
			$this->polycomClass->removeAllParticipantFromVideoConferenceParticipant($confId);
			
			// video_conference 終了
			$this->polycomClass->destroyConference($confId);
		}
	}
}