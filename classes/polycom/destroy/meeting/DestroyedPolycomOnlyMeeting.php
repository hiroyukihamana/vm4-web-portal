<?php
require_once("AbstractDestroyedMeeting.php");
require_once('classes/core/dbi/Meeting.dbi.php');
require_once("classes/core/dbi/MeetingUptime.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");

class DestroyedPolycomOnlyMeeting extends AbstractDestroyedMeeting {
	protected $conferenceId;
	public function __construct($meeting) {
		parent::__construct($meeting);
	}
	
	public function removePolycomParticipant($confId, $partId) {
		$participantList = $this->polycomClass->getPolycomParticipantListByMeetingKey($this->meeting["meeting_key"]);
		if (2 == count($participantList)) {
			$obj_MeetingUptime = new DBI_MeetingUptime($this->dsn);
			$obj_MeetingUptime->stop($this->meeting["meeting_key"]);
		}
		// participant, video_conf_part から削除
		$this->polycomClass->removePolycomParticipant($confId, $partId);
	}
	
	public function setComposition($confId, $did) {
    	// polycom
		$this->polycomClass->setPolycomComposition($confId, $did);
	}
	
	public function checkConferenceStatus($confId) {
		$participantList = $this->polycomClass->getAllParticipantListByMeetingKey($this->meeting["meeting_key"]);
		if (0 >= count($participantList)) {
			$core = new Core_Meeting($this->dsn);
			$core->stopMeeting($this->meeting["meeting_key"]);
			/**
			 * polycom onlyの会議ははmeeting_use_log上で利用時間を管理していないため
			 * meeting_uptimeからデータをかき集めてきてアップデートをかける
			 */
        	$obj_Meeting        = new DBI_Meeting($this->dsn);
			$usetime = $this->polycomClass->getUsageTimeFromMeetingUpTime($this->meeting["meeting_key"]);
	        $meeting_sequence_status["meeting_use_minute"]  = $usetime;
	        $where = "meeting_key = ".$this->meeting["meeting_key"];
	        $obj_Meeting->update($meeting_sequence_status, $where);
	        
	        // video_conference 終了
	        $this->polycomClass->destroyConference($confId);
		}
	}
}