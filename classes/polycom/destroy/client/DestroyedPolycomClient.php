<?php
require_once(dirname(__FILE__)."/../../../../classes/polycom/destroy/client/DestroyedClient.php");
require_once(dirname(__FILE__)."/../../../../classes/polycom/IExecuter.php");

class DestroyedPolycomClient extends DestroyedClient implements IExecuter{
	public function __construct($request, $obj) {
		parent::__construct($request, $obj);
	}
	
	public function execute() {
		//
		try {
			// participant 削除処理
			$this->obj->removePolycomParticipant($this->conferenceId, $this->participantId);

			// 参加人数をFMSへ通知
			$this->obj->noticeParticipantListChanged();
			
			// レイアウト処理
			$this->obj->setComposition($this->conferenceId, $this->did);
			
			// 会議状態
			$this->obj->checkConferenceStatus($this->conferenceId);
		}
		catch (SoapFault $e) {
			$this->reject($e);
		}
		catch (Exception $e) {
			$this->reject($e);
		}
	}
}
