<?php
require_once(dirname(__FILE__)."/../../../../lib/EZLib/EZCore/EZConfig.class.php");
require_once(dirname(__FILE__)."/../../../../lib/EZLib/EZCore/EZLogger2.class.php");
require_once(dirname(__FILE__)."/../../../../classes/polycom/Polycom.class.php");

abstract class DestroyedClient {
	protected $dsn;
	protected $config;
	protected $logger;
	
	protected $conferenceId;
	protected $participantId;
	protected $did;
	
	protected $obj;
	
	protected $polycomClass;
	public function __construct($request, $obj) {
		$this->config = EZConfig::getInstance(N2MY_CONFIG_FILE);
		$this->logger = EZLogger2::getInstance($this->config);
		$this->dsn		= $this->getDsn();
		
		$this->conferenceId	= $request->confId;
		$this->participantId= $request->partId;
		
		$this->obj			= $obj;
		
		$this->polycomClass = new PolycomClass($this->dsn);
		$this->did	= $this->polycomClass->getDidByConfId($this->conferenceId);
	}
	
	private function getDsn() {
		$server_list = parse_ini_file(dirname(__FILE__)."/../../../../config/server_list.ini");
		return $server_list[N2MY_DEFAULT_DB];
	}
	
	protected function reject($message) {
        throw new Exception("polycom destroeyed with irregal situation.".$message);
	}
}