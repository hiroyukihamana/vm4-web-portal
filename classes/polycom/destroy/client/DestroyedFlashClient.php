<?php
require_once(dirname(__FILE__)."/../../../../classes/polycom/destroy/client/DestroyedClient.php");
require_once(dirname(__FILE__)."/../../../../classes/polycom/IExecuter.php");

class DestroyedFlashClient extends DestroyedClient implements IExecuter {
	public function __construct($request, $obj) {
		parent::__construct($request, $obj);
	}
	
	public function execute() {
		try {
		// レイアウト処理
			$this->obj->setComposition($this->conferenceId, $this->did);
		}
		catch (SoapFault $e) {
			$this->reject($e);
		}
		catch (Exception $e) {
			$this->reject($e);
		}
	}
}
