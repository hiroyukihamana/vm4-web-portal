<?php
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/lib/rtmp/RtmpClient.class.php");
 
class FMSController {
	private $fmsProxy;
	private $connected = false;
	function __construct() {
		$this->proxy = new McuConfigProxy();
		$this->logger = EZLogger2::getInstance();
		
		$this->fmsProxy = new FMSControllerProxy();
	}
	
	public function isConnected() {
		return $this->connected;
	}
	
	public function connect($host, $path) {

		$this->fmsProxy->connect($host, $path, array(40960));
		
		$this->connected = true;
	}
	
	public function bootFMSInstance($params) {
		$data = array("type"=>"onConfrenceCreated","data"=>$params);
		$this->fmsProxy->noticeFromWeb(json_encode($data));
	}
	
	public function noticeParticipantListChanged($list=array()) {
		$params = array("participantList"=>$list,"callingStatus"=>"");
		$data = array("type"=>"onParticipantNumberChange","data"=>$params);
		$this->fmsProxy->noticeFromWeb(json_encode($data));
	}
}


class FMSControllerProxy {
	function __construct() {
		$this->rtmpClient	= new RTMPClient();
		
		$this->proxy = new McuConfigProxy();
		$this->logger = EZLogger2::getInstance();
	}

	public function connect($host, $appname, $params = null) {
		$this->rtmpClient->connect($host, $appname, 1935, $params);
	}
	
	public function __call($name, $args) {
		$this->logger->info($name);
		$this->logger->info($args[0]);
		$this->rtmpClient->$name($args[0]);
	}
}