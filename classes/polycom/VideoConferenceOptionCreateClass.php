<?php
require_once(dirname(__FILE__)."/../../config/config.inc.php");
require_once(dirname(__FILE__)."/../../lib/EZLib/EZCore/EZConfig.class.php");
require_once(dirname(__FILE__)."/../../lib/EZLib/EZCore/EZLogger.class.php");
require_once(dirname(__FILE__)."/../../lib/EZLib/EZCore/EZLogger2.class.php");
require_once(dirname(__FILE__)."/../../classes/polycom/VideoConferenceOptionClass.php");

class VideoConferenceOptionCreateClass {
	private static $egnore_option_name = "video_conference";
	private static $className = "VideoConferenceOptionClass";
	private static $optionKey = 24;
	
	private static $instance = null;
	public static function getObjectByRoomKey($roomKey) {
		if (!$roomKey) {
			return null;
		}
		else if (self::$instance) {
			return self::$instance;
		}
		if (self::getOption($roomKey))
			self::$instance = new self::$className($roomKey);
		 
		return self::$instance;
	}
	
	private static function getOption($roomKey) {
		$hasOption = self::checkOption($roomKey);
		$enable = self::checkEnableOption();
		return $hasOption && $enable;
	}
	
	/**
	 * @return boolean
	 */
	private static function checkOption($roomKey) {
		require_once("classes/dbi/ordered_service_option.dbi.php");
		/*
		 * dbi内で呼ばれてるLoggerが先にインスタンスを生成されている体で作られてるので仕方なく
		 */
		$config = self::getConfig();
		$logger = EZLogger::getInstance($config);
		$logger2 = EZLogger2::getInstance($config);
		$service_option  = new OrderedServiceOptionTable(self::getDsn());
		$hasOption = $service_option->enableOnRoom(self::$optionKey, $roomKey);
		return $hasOption;
	}
	
	/**
	 * @return boolean
	 */
	private function checkEnableOption() {
		$enable = !self::getConfig()->get("IGNORE_MENU", self::$egnore_option_name);
		return $enable;
	}

	private static function getDsn() {
		$server_list = parse_ini_file(dirname(__FILE__)."/../../config/server_list.ini");
		return $server_list[N2MY_DEFAULT_DB];
	}
	
	/**
	 * 
	 */
	private static function getConfig() {
		return EZConfig::getInstance(dirname(__FILE__)."/../../config/config.ini");
	}
}
