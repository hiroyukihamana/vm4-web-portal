<?php
require_once(dirname(__FILE__)."/AbstractMeeting.php");

require_once("classes/mcu/config/McuConfigProxy.php");

class PolycomOnlyMeeting extends AbstractMeeting {
	public function __construct() {
		parent::__construct();
	}
	
	public function setMeetingByConfId($confId) {
		$this->meeting = $this->polycomClass->getMeetingByConfId($confId);
		if ($this->meeting) {
			if ($this->meeting["is_reserved"]) $this->setReservation();
			return true;
		}
		else
			return false;
	}
	
	public function createMeetingByPolycomClient($confId, $did) {
		$this->meeting = $this->polycomClass->createMeetingByPolycomClient($confId, $did);
	}
	
	public function bootFMSInstance() {
		$this->connect();
		
		$bootParams = $this->polycomClass->getMcuGatewayByRoomKey($this->meeting["room_key"]);
		$this->fmsController->bootFMSInstance($bootParams);
	}
	
	public function addPolycomParticipant($conferenceId, $did, $participantId, $participantPort, $name) {
		$this->polycomClass->addPolycomParticipant($conferenceId, $participantId, $participantPort, $name, $this->meeting["meeting_key"], $did);
		if (2 == count($participantList)) {
			/**
			 * FlashGateway.startMeetingから拝借
			 * 
			 */
	        require_once("classes/core/dbi/Meeting.dbi.php");
	        require_once("classes/core/dbi/MeetingUptime.dbi.php");
	        $obj_Meeting       = new DBI_Meeting($this->dsn);
	        $obj_MeetingUptime = new DBI_MeetingUptime($this->dsn);
	        
	        $obj_MeetingUptime->start($this->meeting["meeting_key"]); // 利用期間テーブル更新
	        $meeting_uptime = $obj_MeetingUptime->getActiveInfo($this->meeting["meeting_key"]); // 実利用開始日時取得
	        
	        // 会議内容更新
            $data["use_flg"] = 1;
            if (!$this->meeting["actual_start_datetime"]) {
                $data["actual_start_datetime"] = $meeting_uptime["meeting_uptime_start"];
            }
            
            $data["update_datetime"] = date("Y-m-d H:i:s");
            $where = "meeting_key = ".$this->meeting["meeting_key"];
            $ret   = $obj_Meeting->update($data, $where);
            
            /**
             * 予約会議を中止する際use_countが１以上の参加者が存在しないと
             * meetingのデータがクリアされてしまうので無理くり１を入れている
             * polycom client.use_countがカウントアップされるように変更される場合はこの処理を削除する必要がある
             */
            $this->polycomClass->updatePolycomClientUseCount($this->meeting["meeting_key"]);
		}
	}
	
	public function setComposition($confId, $did) {
		$this->polycomClass->setPolycomComposition($confId, $did);
	}
}
