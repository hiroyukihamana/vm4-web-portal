<?php
require_once(dirname(__FILE__)."/AbstractMeeting.php");
class FlashAndPolycomMeeting extends AbstractMeeting {
	public function __construct() {
		parent::__construct();
	}
	
	public function setMeetingByConfId($confId) {
		$this->meeting = $this->polycomClass->getMeetingByConfId($confId);
		if ($this->meeting) {
			if (!$this->meeting["is_blocked"]) {
				if ($this->meeting["is_reserved"]) {
					$this->setReservation(); 
				}
				return true;
			}
			else
				throw new Exception("blocked meeting.");
		}
		else {
			throw new Exception("check meeting status.");
		}
	}
	
	public function addPolycomParticipant($conferenceId, $did, $participantId, $participantPort, $name) {
		$this->polycomClass->addPolycomParticipant($conferenceId, $participantId, $participantPort, $name, $this->meeting["meeting_key"], $did);
	}
	
	public function setComposition($confId, $did) {
		// flash表示画面
    	$this->polycomClass->setFlashComposition($confId, $did, $this->meeting["meeting_key"]);

    	// polycom
    	$this->polycomClass->setPolycomComposition($confId, $did);
	}
}
