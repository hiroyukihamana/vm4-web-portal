<?php
require_once(dirname(__FILE__)."/../../../../lib/EZLib/EZCore/EZConfig.class.php");
require_once(dirname(__FILE__)."/../../../../lib/EZLib/EZCore/EZLogger2.class.php");
require_once (dirname(__FILE__)."/../../../../classes/polycom/Polycom.class.php");

require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/polycom/FMSController.php");

abstract class AbstractMeeting {
	protected $conferenceId;
	protected $did;
	protected $participantId;
	protected $meeting;
	protected $reservation;
	
	protected $fmsController;
	public function __construct() {
		$this->config = EZConfig::getInstance(N2MY_CONFIG_FILE);
		$this->logger = EZLogger2::getInstance($this->config);
		$this->dsn		= $this->getDsn();
		
		$this->polycomClass = new PolycomClass($this->dsn);
		$this->conferenceId = $conferenceId;

		$this->configProxy = new McuConfigProxy();
		$this->fmsController = new FMSController();
	}
	
	abstract public function setComposition($confId, $did);
	abstract public function setMeetingByConfId($confId);
	abstract public function addPolycomParticipant($conferenceId, $did, $participantId, $participantPort, $name);
	
	public function setReservation() {
		$this->reservation = $this->polycomClass->getReservationInfo($this->meeting["meeting_ticket"]);
		return $this->reservation ?true: false;
	}
	
	public function callCreateMosaic($confId) {
		$this->polycomClass->callCreateMosaic($confId);
	}
	
	public function checkPassword() {
		if ($this->reservation && ($this->reservation["reservation_pw"] && 1 == $this->reservation["reservation_pw_type"]))
			throw new Exception("meeting set password.");
	}
	
	public function checkIndispensablePassword() {
		if (!$this->reservation)
			throw new Exception("check reservation info.");
		else if ($this->reservation["reservation_pw"] && 1 == $this->reservation["reservation_pw_type"])
			throw new Exception("meeting set password.");
	}
	
	public function checkContract($confId, $did) {
        if (!$this->polycomClass->checkPolycomContractNumber($confId, $did)) {
            throw new Exception("contract status irregal.");
        }
	}
	
	public function getPolycomProfileParams($conferenceId, $participantId) {
        return $this->polycomClass->getPolycomProfileParams($conferenceId, $participantId);
	}
	
	public function getFlashProfileParams($conferenceId, $participantId) {
        return $this->polycomClass->getFlashProfileParams($conferenceId, $participantId);
	}
	
	public function changeProfile($profileParams) {
		$this->polycomClass->changeProfile($profileParams);
	}
	
	public function accept($conferenceId, $participantId, $type) {
        $this->polycomClass->acceptParticipant($conferenceId, $participantId, $type);
	}
	
	public function callCreateSidebar($confId) {
		$this->polycomClass->createSidebar($confId);
	}
	
	public function callAddSidebarParticipant($confId, $partId, $sidebarId) {
		$this->polycomClass->addSidebarParticipant($confId, $partId, $sidebarId);
	}
	
	public function mosaic($conferenceId, $participantId, $type) {
        $this->polycomClass->addMosaicParticipant($conferenceId, $participantId, $type);
	}
	
	public function removeMosaic($conferenceId, $participantId, $type) {
		$this->polycomClass->removeMosaicParticipant($conferenceId, $participantId, $type);
	}
	
	public function removePolycomParticipant($confId, $partId) {
		$this->polycomClass->removePolycomParticipant($confId, $partId);
	}
	
	private function getDsn() {
		$server_list = parse_ini_file(dirname(__FILE__)."/../../../../config/server_list.ini");
		return $server_list[N2MY_DEFAULT_DB];
	}
	
	public function noticeParticipantListChanged() {
		if (!$this->meeting)
			throw new Exception(__FUNCTION__."does not exist meeting record.");
		
		if (!$this->fmsController->isConnected()) {
			$this->connect();
		}

		$participantList = $this->polycomClass->getPolycomParticipantListByMeetingKey($this->meeting["meeting_key"]);
		$this->fmsController->noticeParticipantListChanged($participantList);
	}
	
	protected function connect() {
		$sequenceRecord = $this->polycomClass->getMeetingSequenceRecordByMeetingKey($this->meeting["meeting_key"]);
		if (!$sequenceRecord)
			throw new Exception("bootFMSInstance::check sequence record. meeting_key: ".$this->meeting["meeting_key"]);
		
		$fmsRecord = $this->polycomClass->getServerRecordByServerKey($sequenceRecord["server_key"]);
		if (!$fmsRecord)
			throw new Exception("bootFMSInstance::cannot resolve fms server. server_key: ".$sequenceRecord["server_key"]);
		
		$path  = $this->configProxy->getWithGroupName("CORE", "app_name")."/".$this->meeting["fms_path"].$this->meeting["meeting_session_id"];
		$this->fmsController->connect($fmsRecord["server_address"], $path);
	}
	
	public function hasMeetingSequence() {
		if ($this->meeting) {
			$record = $this->polycomClass->getMeetingSequenceRecordByMeetingKey($this->meeting["meeting_key"]);
			if ($record) return true;
		}
		return false;
	}
	
	public function setMeetingSequence() {
		if (!$this->meeting) throw new Exception("no meeting record.");
		$record = $this->polycomClass->createMeetingSequenceRecord($this->meeting);
		return $record;
	}
	
	public function resolveEnvironmentData() {
		if (!$this->meeting) throw new Exception("no meeting record."); 
		$this->meeting = $this->polycomClass->resolveEnvironmentData($this->meeting);
		return $this->meeting;
	}
}
