<?php

require_once(dirname(__FILE__)."/../../../lib/EZLib/EZCore/EZConfig.class.php");

require_once(dirname(__FILE__)."/../../../classes/polycom/create/client/CreatedPolycomClient.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/create/client/CreatedFlashClient.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/create/client/CreatedReservationPolycomClient.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/create/client/CreatedViewClient.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/create/meeting/PolycomOnlyMeeting.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/create/meeting/FlashAndPolycomMeeting.php");
require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../../../lib/EZLib/EZCore/EZConfig.class.php");
require_once(dirname(__FILE__)."/../../../lib/EZLib/EZCore/EZLogger2.class.php");

class CreatedController {
  static function create($request) {
    $config = EZConfig::getInstance(N2MY_CONFIG_FILE);
    $logger = EZLogger2::getInstance($config);
    $configProxy 	= new McuConfigProxy();

	$confId	= $request->confId;
	$part	= $request->part;
    $partId	= $part->id;

    $dsn			= CreatedController::getDsn();
    $polycomClass = new PolycomClass($dsn);
    $logger->info($request);
    /* conferenceの詳細情報を取得 */
    $conferenceInfo = $polycomClass->getConferenceByConfId($confId);
    $request->did = $conferenceInfo->return->did;

    /**
     * did より会議情報取得
     */
    // 既にVcubeMeetingが存在しているケース
    if ($meeting = $polycomClass->getMeetingByDid($conferenceInfo->return->did)) {
      /* meeting recordとconferenceデータを結びつける */
      if (!$polycomClass->getActiveVideoConference($meeting["meeting_key"], $confId)) {
        // video conf 作成
        $polycomClass->addConfId2VideoConference($meeting["meeting_key"], $confId);

        /**
         * 参加済みのFlashClientに対して
         * video conf participant作成
         */
        $polycomClass->createVideoConferenceParticipant($meeting["meeting_key"], $confId);
      }

    }
    $list = $polycomClass->getFlashParticipantListFromVideoConferenceByConfId($confId);
    if (count($list) > 0) {
      $obj = new FlashAndPolycomMeeting();
    }
    else {
      $obj = new PolycomOnlyMeeting();
    }

    $logger->info("obj type: ".get_class($obj));
    $result = split("@", $part->name);
    if ($part->type == "SIP") {
      if ($configProxy->get("guestDomain") == $result[1] ||
        "vcubeguest" == $result[1] ||
      	"g.mtg.vc" == $result[1] ||
      	"1.2192.jp" == $result[1] ||
      	"2.2192.jp" == $result[1] ||
      	"11.2192.jp" == $result[1]) {
        return new CreatedReservationPolycomClient($request, $obj);
      }
      else if ("vcube" == $result[1] ||
          "visioassistance.net" == $result[1] ||
          "mtg.vc" == $result[1] ||
      	  "0.2192.jp" == $result[1] ||
      	  "00.2192.jp" == $result[1] ||
      	  "media2.mtg.vc" == $result[1] ||
          $configProxy->get("opensips_newfrom_domain") == $result[1]) {
        return new CreatedPolycomClient($request, $obj);
      }
      else {
        throw new Exception("unknown client type.");
      }
    }
    else if ($part->type == "WEB") {
      // view用のPariticipant　判別
      if ($result[0] == $configProxy->get("defaultViewId") ) {
        return new CreatedViewClient($request, $obj);
      }
      else {
        return new CreatedFlashClient($request, $obj);
      }
    }
    else {
      throw new Exception("unknown client type.");
    }
  }

  private static function getDsn() {
    $server_list = parse_ini_file(dirname(__FILE__)."/../../../config/server_list.ini");
    return $server_list[N2MY_DEFAULT_DB];
  }
}
