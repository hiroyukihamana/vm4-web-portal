<?php
require_once(dirname(__FILE__)."/../../../../classes/polycom/create/client/CreatedClient.php");
require_once(dirname(__FILE__)."/../../../../classes/polycom/IExecuter.php");

class CreatedFlashClient extends CreatedClient implements IExecuter {
	public function __construct($request, $obj) {
		parent::__construct($request, $obj);
	}
	
	/**
	 * Flash clientの入室処理に関しては基本的にservices/index.phpで行なっているはずなので
	 * ここではレイアウトの調整とアクセプトのみ
	 */
	public function execute() {
		try {
			// set meeting info
			$this->obj->setMeetingByConfId($this->conferenceId);
			
			// layout
			$this->obj->setComposition($this->conferenceId, $this->did);
			
			// profile
			$this->obj->changeProfile($this->obj->getFlashProfileParams($this->conferenceId, $this->participantId));
			
			// 入室許可
			$this->obj->accept($this->conferenceId, $this->participantId, $this->type);
		}
		catch (Exception $e) {
			$this->reject($e);
		}
	}
}
