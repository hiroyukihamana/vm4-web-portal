<?php
require_once(dirname(__FILE__)."/../../../../lib/EZLib/EZCore/EZConfig.class.php");
require_once(dirname(__FILE__)."/../../../../lib/EZLib/EZCore/EZLogger2.class.php");
require_once (dirname(__FILE__)."/../../../../classes/polycom/Polycom.class.php");

abstract class CreatedClient {
	protected $dsn;
	protected $config;
	protected $logger;
	
	protected $conferenceId;
	protected $participantId;
	protected $port;
	protected $name;
	protected $type;
	protected $state;
	protected $sidebarId;
	protected $obj;
	
	protected $soapClient;
	protected $polycomClass;
	public function __construct($request, $obj) {
		$this->config	= EZConfig::getInstance(N2MY_CONFIG_FILE);
		$this->logger	= EZLogger2::getInstance($this->config);
		$this->dsn		= $this->getDsn();
		$this->obj		= $obj;
		
		$this->conferenceId	= $request->confId;
		$this->participantId= $request->part->id;
		$this->did			= $request->did;
		$this->port			= $request->part->port;
		$this->name			= $request->part->name;
		$this->type			= $request->part->type;
		$this->state		= $request->part->state;
		$this->sidebarId	= $request->part->sidebarId;
		
		$this->polycomClass	= new PolycomClass($this->dsn);
	}
	
	private function getDsn() {
		$server_list = parse_ini_file(dirname(__FILE__)."/../../../../config/server_list.ini");
		return $server_list[N2MY_DEFAULT_DB];
	}
	
	protected function reject($e) {
		// destroyと同じ処理を入れてみた
		$this->obj->removePolycomParticipant($this->conferenceId, $this->participantId);
		
		$this->obj->setComposition($this->conferenceId, $this->did);
		
// 		$this->obj->checkConferenceStatus($this->conferenceId);
		// destroyと同じ処理を入れてみた
		
//     	$rejectParams = $this->polycomClass->getRejectParams($this->conferenceId, $this->participantId);
//         $this->soapClient->rejectParticipant($rejectParams);
        
        $this->polycomClass->reject($this->conferenceId, $this->participantId);
        
        throw new Exception($e);
	}
}
