<?php
require_once(dirname(__FILE__)."/../../../../classes/polycom/IExecuter.php");
require_once(dirname(__FILE__)."/../../../../classes/polycom/create/client/CreatedClient.php");

class CreatedPolycomClient extends CreatedClient implements IExecuter{
	public function __construct($request, $obj) {
		parent::__construct($request, $obj);
	}
	
	public function execute() {
		try {
			// password確認
			if ($this->obj->setMeetingByConfId($this->conferenceId)) {
				$this->obj->checkPassword();
			}
			
			// 契約数確認
			$this->obj->checkContract($this->conferenceId, $this->did);
			
			// profile
			$this->obj->changeProfile($this->obj->getPolycomProfileParams($this->conferenceId, $this->participantId));
			
			// 入室許可
			$this->obj->accept($this->conferenceId, $this->participantId, $this->type);

			// set meeting info.
			/**
			 * 通常会議且つ一人目がPolycomのケース
			 */
			if (!$this->obj->setMeetingByConfId($this->conferenceId)) {
				$this->obj->createMeetingByPolycomClient($this->conferenceId, $this->did);

				$this->obj->resolveEnvironmentData();

				// create meeting_sequence record
				$this->obj->setMeetingSequence();
				
				// fms instance 起動
				$this->obj->bootFMSInstance($this->conferenceId, $this->did);
			}
			/**
			 * 予約会議且つ一人目Polycomのケース
			 **/
			else if (!$this->obj->hasMeetingSequence()) {

				$this->obj->resolveEnvironmentData();
				
				// create meeting_sequence record
				$this->obj->setMeetingSequence();
				
				// fms instance 起動
				$this->obj->bootFMSInstance($this->conferenceId, $this->did);				
			}
			
			// participant 追加
			$this->obj->addPolycomParticipant($this->conferenceId, $this->did, $this->participantId, $this->port, $this->name);

			// 参加人数をFMSへ通知
			$this->obj->noticeParticipantListChanged();
			
			// レイアウト調整
			$this->obj->setComposition($this->conferenceId, $this->did);
			
			// mosaic
			$this->obj->mosaic($this->conferenceId, $this->participantId, $this->type);

			// add sidebar.
			$this->obj->callAddSidebarParticipant($this->conferenceId, $this->participantId, $this->sidebarId);
		}
		catch (Exception $e) {
			$this->reject($e);
		}
	}
}
