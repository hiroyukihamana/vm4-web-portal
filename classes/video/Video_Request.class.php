<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * PHP versions 4 and 5
 *
 * @package     nicetomeetyou For Video API Accessor
 * @author      Yuki Asano <y-asano@vcube.co.jp>
 * @copyright   V-cube Inc.
 * @license     http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version     CVS: $Id:$
 */
//require_once('classes/video/pear/HTTP_Request/Request.php');
require_once 'lib/pear/HTTP/Request.php';

class Video_Request extends HTTP_Request
{

    //---- set Variable ----
    var $_param;
    var $_status;
    var $_xwsse;
    var $_tmpDir;
    var $_codes;
    var $req;

// ------  constructor -------------------------------------------------
    //{{{ Video_Request()
    /**
     * コンストラクタ
     *
     * @access  public
     * @since   3.1.0
     */
    function Video_Request($url=null ,$timeout=array())
    {
        parent::HTTP_Request($url, $timeout);
        $this->_setHTTPStatusCode();
    }
    //}}}

// ------ public ------------------------------------------------------
    //{{{ setAuth()
    /**
     * X-WSSEヘッダ生成
     *
     * @access  public
     * @param   string  $user
     * @param   string  $password
     * @return  array
     */
    function setAuth($user, $password)
    {
        $this->xwsse = $this->createXWSSEHeader($user, $password);
    }
    //}}}
    //{{{ getAuth()
    /**
     * X-WSSEヘッダ取得
     *
     * @access  public
     * @return  array
     */
    function getAuth()
    {
        return $this->xwsse;
    }
    //}}}
    //{{{ setParamters()
    /**
     * setParameters
     *
     * @access  private
     * @param   array    $data
     * @since   3.1.0
     */
    function setParameters($data)
    {
        $this->_param = $data;
    }
    //}}}
    //{{{ getParamters()
    /**
     * setParameters
     *
     * @access  private
     * @since   3.1.0
     */
    function getParameters()
    {
        return $this->_param;
    }
    //}}}
    //{{{ createXWSSEHeader()
    /**
     * createXWSSEHeader
     *
     * @access  public
     * @param   string $username
     * @param   string $password
     * @since   3.1.0
     */
    function createXWSSEHeader($username, $password) {

        $nonce    = sha1(uniqid(rand(), true));
//        $created  = date('c');
        $created = date('Y-m-d\TH:i:s\Z', time());
//        $pdigest  = sha1($nonce . $created . sha1($password));
        $pdigest  = sha1($nonce . $created . $password);

        // --- base64 encoding ---
//        $created  = base64_encode($created);
        $nonce    = base64_encode($nonce);
        $pdigest  = base64_encode($pdigest);

        $res["X-WSSE"] = 'UsernameToken Username='.$username.', PasswordDigest='.$pdigest.', Nonce='.$nonce.', Created='.$created;

        return $res;
    }
    //}}}
    //{{{ setFileFromRaw()
    /**
     * create and set File
     *
     * @access  private
     * @since   3.1.0
     */
    function setFileFromRaw($binary)
    {
        $tmpDir = $this->getTemporaryDir();
        // --- set temporary dir ---
        if (!$tmpDir) {
            $tmpDir = "/tmp";
        }

        if (($fp = fopen($tmpDir."/".$name, 'w')) === false ) {
            return false;
        }

        if (fwrite($fp, base64_decode($binary)) === false) {
            return false;
        }
        fclose($fp);

        $req->addFile("mov", "/tmp/".$data['id']);
    }
    //}}}
    //{{{ setTemporaryDir()
    /**
     * setParameters
     *
     * @access  private
     * @since   3.1.0
     */
    function setTemporaryDir($dir)
    {
        $this->_tmpDir = $dir;
    }
    //}}}
    //{{{ getTemporaryDir()
    /**
     * getTemporaryDir
     *
     * @access  private
     * @since   3.1.0
     */
    function getTemporaryDir()
    {
        return $this->_tmpDir;
    }
    //}}}
    //{{{ getFileText()
    /**
     * ファイルの中身を取得
     * 第２引数でbase64_encodeの有無を指定
     *
     * @access  public
     * @param   string  $file   ファイル名までのパス
     * @return  array
     */
    function getFileText($file, $encode)
    {

        if (!file_exists($file)) {
            return false;
        }

        if ($encode) {
            $data = base64_encode(file_get_contents($file));
        } else {
            $data = file_get_contents($file);
        }

        return $data;
    }
    //}}}
    //{{{ checkHTTPStatusCode()
    /**
     * HTTPステータスコードを取得
     *
     * @access  public
     * @return  boolean
     */
    function checkHTTPStatusCode()
    {
        $res   = parent::getResponseCode();
        if (!$res) {
            return false;
        }
        $codes = $this->_getHTTPStatusCodes();
        $bool  = @$codes[$res][1];
        return $bool;
    }
    //}}}
    //{{{ urlencodeAll()
    /**
     * @access  public
     * @since   3.1.0
     */
    function urlencodeAll($items=array())
    {
        foreach($items as $k => $v){
            $items[$k] = urlencode($v);
        }
        return $items;
    }
    //}}}
	//{{{ quote()
    /**
     * @access  public
     * @since   3.1.0
     */
    function quote($item)
    {
    	if(is_array($item) || is_null($item)){
    		return $item;
    	}
        
    	
    	
        return htmlspecialchars($item);
    }
    //}}}
	//{{{ quoteAll()
    /**
     * @access  public
     * @since   3.1.0
     */
    function quoteAll($items=array())
    {
    	foreach($items as $k => $v){
    		$items[$k] = $this->quote($v);
    	}
    	
    	return $items;
    }
    //}}}

    // --- PEAR wrapper ---
    //{{{ addHeaders()
    /**
     * addHeaders
     *
     * @access  private
     * @param   array    $data
     * @since   3.1.0
     */
    function addHeaders($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $this->addHeader($key, $value);
            }
        }
        return ;
    }
    //}}}
    //{{{ addQueryStrings()
    /**
     * addQueryStrings
     *
     * @access  private
     * @since   3.1.0
     */
    function addQueryStrings($param=array())
    {
        if ($param && is_array($param)) {
            foreach ($param as $k => $v ) {
                $this->addQueryString($k, $v);
            }
        }
    }
    //}}}
    //{{{ setMethod()
    /**
     * PEAR::setMethodのラッパ
     *
     * @access  public
     * @param   string  $method
     * @return  array
     */
    function setMethod($method="post")
    {
        parent::setMethod(strtoupper($method));
    }
    //}}}
    //{{{ setUrl()
    /**
     * PEAR::setUrlのラッパ
     *
     * @access  public
     * @param   string  $url
     * @return  array
     */
    function setUrl($url)
    {
        $url = preg_replace("|([^://])/{2,}|", '$1/', $url);
        parent::setUrl($url);
    }
    //}}}

    // --- renderer ---
    //{{{ assignXML()
    /**
     * assign data
     *
     * @access  private
     * @since   3.1.0
     */
    function assignXML($name, $value)
    {
        $this->_assign[$name] = $value;
    }
    //}}}
    //{{{ getAssigns()
    /**
     * assign data
     *
     * @access  private
     * @since   3.1.0
     */
    function getAssigns()
    {
        return $this->_assign;
    }
    //}}}
    //{{{ fetchXML()
    /**
     * create and set File
     *
     * @access  private
     * @param   string  $tmpl
     * @since   3.1.0
     */
    function fetchXML($tmpl)
    {
        $assigns = $this->getAssigns();

        extract($assigns, EXTR_SKIP);

        ob_start();

        require_once($tmpl);

        $resource = ob_get_contents();

        ob_end_clean();

        return $resource;
    }
    //}}}

// ------  private -----------------------------------------------------
    //{{{ _setHTTPStatusCode()
    /**
     * 使用する（将来的にそれが使用されると予想される）HTTPステータスコードを定義
     *
     * @access  private
     * @return  array
     */
    function _setHTTPStatusCode()
    {
        $code['200'] = array('OK', true);
        $code['201'] = array('Created', true);
        $code['202'] = array('Accepted', true);
        $code['203'] = array('Non-Authoritative Information', true);
        $code['204'] = array('No Content', true);
        $code['205'] = array('Reset Content', true);
        $code['206'] = array('Partial Content', true);
        $code['400'] = array('Bad Request', false);
        $code['401'] = array('Unauthorized', false);
        $code['402'] = array('Payment Required', false);
        $code['403'] = array('Forbidden', false);
        $code['404'] = array('File Not Found', false);
        $code['405'] = array('Method Not Allowed', false);
        $code['406'] = array('Not Acceptable', false);
        $code['407'] = array('Proxy Authentication Required', false);
        $code['408'] = array('Request Time-out', false);
        $code['409'] = array('Conflict', false);
        $code['410'] = array('Gone', false);
        $code['411'] = array('Length Required', false);
        $code['412'] = array('Precondition Failed', false);
        $code['413'] = array('Request Entity Too Large', false);
        $code['414'] = array('Request-URI Too Large', false);
        $code['415'] = array('Unsupported Media Type', false);
        $code['416'] = array('Requested range not satisfiable', false);
        $code['417'] = array('Expectation Failed', false);
        $code['500'] = array('Internal Server Error', false);
        $code['501'] = array('Not Implemented', false);
        $code['502'] = array('Bad Gateway', false);
        $code['502'] = array('Service Unavailable', false);
        $code['504'] = array('Gateway Time-out', false);
        $code['505'] = array('HTTP Version not supported', false);

        $this->_codes = $code;

        return true;
    }
    //}}}
    //{{{ _getHTTPStatusCodes()
    /**
     * HTTPステータスコードを取得
     *
     * @access  public
     * @return  array
     */
    function _getHTTPStatusCodes()
    {
        return $this->_codes;
    }
    //}}}

    // --- older ---
    //{{{ RequestWithData()
    /**
     * APIへリクエスト
     *
     * @access  public
     * @param   string $url
     * @param   string $data
     * @param   string $method
     * @since   3.1.0
     */
    function RequestWithData($url ,$header=array(), $data=array(), $binary=null, $method='post')
    {
        $req = $this->_getInstance($url ,array('timeout'=>120));
        $method = strtoupper($method);
        $req->setMethod($method);

        // --- add file ---
        $file = file_put_contents("/tmp/".$data['id'], base64_decode($binary));
        $req->addFile("mov", "/tmp/".$data['id']);

        // --- add headers ---
        $this->_addHeaders($header);

        if ($data && is_array($data)) {
            if ($method == 'GET') {
                foreach ($data as $key=>$val) {
                    $req->addQueryString($key, $val);
                }
            } else {
                foreach ($data as $key=>$val) {
                    $req->addPostData($key, $val);
                }
            }
        }

        if (!PEAR::isError($req->sendRequest())) {
            return $req->getResponseBody();
        }

        return true;
    }
    //}}}
    //{{{ RequestXML()
    /**
     * APIへリクエスト
     *
     * @access  public
     * @param   string  $url
     * @param   array   $header
     * @param   array   $data
     * @param   string  $method
     * @since   3.1.0
     */
    function RequestXML($url ,$header=array(), $data=array(), $method='post')
    {

        $req = $this->_getInstance($url, array('timeout'=>120));

        $method = strtoupper($method);
        $req->setMethod($method);

        $this->_addHeaders($header);

        if ($method == 'GET') {
            if (is_array($data)) {
                foreach ($data as $k => $v ) {
                    $req->addQueryString($k, $v);
                }
            }
        } else {
            $req->addRawPostData($data, true);
        }

        if (!PEAR::isError($req->sendRequest())) {
            return $req->getResponseBody();
        }

        return false;
    }
    //}}}
    //{{{ getStatusCode()
    /**
     * getStatusCode
     *
     * @access  public
     * @return  array
     */
    function getStatusCode()
    {
        return $this->_status;
    }
    //}}}



}
?>
