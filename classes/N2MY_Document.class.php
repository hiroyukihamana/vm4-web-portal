<?php

require_once("classes/core/dbi/Document.dbi.php");
require_once("classes/core/dbi/DocumentConvertProcess.dbi.php");
require_once("classes/core/dbi/ConvertFile.dbi.php");
require_once("lib/EZLib/EZUtil/EZImage.class.php");
require_once("lib/EZLib/EZCore/EZLogger.class.php");
require_once("lib/EZLib/EZUtil/EZString.class.php");
require_once("lib/EZLib/EZUtil/EZFile.class.php");
require_once("classes/core/dbi/Storage.dbi.php");
require_once("classes/dbi/storage_file.dbi.php");

class N2MY_Document {

    var $obj_Document = null;
    var $obj_Process = null;
    var $_objStorage = null;
    var $doc_dir = null;
    var $dsn = null;

    function __construct($dsn) {
        $this->logger = EZLogger2::getInstance();
        $this->obj_Process     = new DBI_DocumentConvertProcess(N2MY_MDB_DSN);
        $this->obj_ConvertFile = new DBI_ConvertFile(N2MY_MDB_DSN);
        $this->obj_Document    = new DBI_Document($dsn);
        $this->_objStorage     = new DBI_StorageFile($dsn);
        $this->dsn = $dsn;
    }

    /**
     * 資料追加
     *
     * @param string $document_id 資料ID
     * @param string $path 出力先のディレクトリ
     * @param string $file ファイルパス
     * @param string $meeting_key 会議キー
     * @param string $name = null 資料名
     * @param string $sort = null 表示順
     */
    function add($document_id, $path, $file, $mode = null, $name = null, $meeting_key = 0, $participant_id = null, $format = null, $version=null, $sort=null , $is_storage = 0 ) {
        $file_info = $this->file_info($file);
//        if ("image" == $file_info["type"] || $file_info["extension"] == "pdf") {
        if ("image" == $file_info["type"]) {
            $format = "bitmap";
        }
        $data = array(
            "meeting_key" => $meeting_key,
            "document_id" => $document_id,
            "participant_id" => $participant_id,
            "document_mode" => $mode,
            "document_path" => $path,
            "document_name" => $name,
            "document_size" => filesize($file),
            "document_category" => $file_info["type"],
            "document_extension" => $file_info["extension"],
            "document_format" => $format,
            "document_version" => $version,
            "is_storage" => $is_storage,
            "create_datetime" => date("Y-m-d H:i:s"),
            "update_datetime" => date("Y-m-d H:i:s")
        );
        $where = "document_path = '".$path."'";
        if (!isset($sort)) {
            $sort = $this->obj_Document->getOne($where, "document_sort", array("document_sort" => "desc")) + 1;
        }
        $data["document_sort"] = $sort;
        $this->logger->debug($data);
        $ret = $this->obj_Document->add($data);
        if (DB::isError($ret)) {
            $this->logger->warn($ret->getMessage());
            return false;
        }
        if (file_exists($file)) {
            return $ret;
        } else {
            $this->setStatus($document_id, DOCUMENT_STATUS_ERROR);
            return false;
        }
    }

    function update($document_id, $document) {
        $where = "document_id = '".$document_id."'";
        // 資料ステータス
        $data = array(
            "document_name"   => $document["name"],
            "document_sort"   => $document["sort"],
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $this->obj_Document->update($data, $where);
    }

    /**
     * ファイルは物理削除、DBは論理削除
     */
    function delete($_document) {
    	if($_document["is_storage"] == "1"){
            $this->setStatus($_document["document_id"], DOCUMENT_STATUS_DELETE,true);
    		// 変換後のファイルを削除
    		$document_id = $_document["document_id"];
    		$document = $this->getDetail($document_id);
    		$dir = N2MY_DOC_DIR.$document["document_path"].$document["document_id"];
    		require_once("lib/EZLib/EZUtil/EZFile.class.php");
    		$obj_File = new EZFile();
    		// $document["document_path"].$document["document_id"]がnullだとdoc全部消えるので念のためチェック
    		if(N2MY_DOC_DIR != $dir){
    		    $obj_File->rmtree($dir);
    		}
    	} else {
    		if($_document["document_id"]){
    	        $document_id = $_document["document_id"];
    		}else{
    		    $document_id = $_document;
    		}
    		$document = $this->getStatus($document_id);
    		$this->logger->info($document);
    		// 変換が始まりだしたら削除不可
    		//        if (isset($document["status"]) && $document["status"] != DOCUMENT_STATUS_DO) {
    		if (isset($document["status"])) {
    			$where = "document_id = '".addslashes($document_id)."'";
    			if ($document["status"] == DOCUMENT_STATUS_WAIT) {
    				// 変換前の場合
    				$this->_deleteOriginFile($document_id);
    			} else {
    				// 変換後のファイルを削除
    				$dir = N2MY_DOC_DIR.$document["document_path"].$document_id;
    				$this->logger->debug($dir);
    				require_once("lib/EZLib/EZUtil/EZFile.class.php");
    				$obj_File = new EZFile();
                    // $document["document_path"].$document["document_id"]がnullだとdoc全部消えるので念のためチェック
                    if(N2MY_DOC_DIR != $dir){
                        $obj_File->rmtree($dir);
                    }
    			}
    			$this->setStatus($document_id, DOCUMENT_STATUS_DELETE);
    			return true;
    		} else {
    			return false;
    		}
    	}
    }

    /**
     * 元ファイルの削除
     */
    function _deleteOriginFile($document_id) {
        $document = $this->getStatus($document_id);
        if (isset($document["status"]) && $document["status"] != DOCUMENT_STATUS_DO) {
            $file = N2MY_DOC_DIR.$document["document_path"].$document_id.".".$document["extension"];
            if (file_exists($file)) {
                $this->logger->debug($file);
                unlink($file);
            }
            return true;
        }
        return false;
    }

    /**
     * 資料変換用のキューにためる
     */
    function addQueue($db, $document_path, $document_id, $ext, $priority, $addition = null) {
        $data = array(
            "db_host"       => $db,
            "document_path" => $document_path,
            "document_id"   => $document_id,
            "document_format"   => $addition["format"],
            "document_version"   => $addition["version"],
            "extension"     => $ext,
            "status"        => DOCUMENT_STATUS_WAIT,
            "priority"      => $priority,
            "addition"      => serialize($addition),
            "create_datetime" => date("Y-m-d H:i:s"),
            "update_datetime" => date("Y-m-d H:i:s")
        );
        $this->obj_ConvertFile->add($data);
    }

    /**
     * キューにたまったファイルを抽出
     */
    function getQueue() {
        $where = "status = 0";
        $sort = array(
            "priority" => "asc",
            "create_datetime" => "asc",
        );
        $row = $this->obj_ConvertFile->getRow($where, "*", $sort);
        $this->logger->debug($row);
        if ($row) {
            $this->setStatus($row["document_id"], DOCUMENT_STATUS_DO);
        }
        return $row;
    }

    /**
     * 変換中のステータス取得
     */
    function getStatus($document_id) {
        $where = "document_id = '".$document_id."'";
        $row = $this->obj_ConvertFile->getRow($where);
        return $row;
    }

    /**
     * ステータスを更新
     */
    function setStatus($document_id, $status, $is_storage = false) {
        // キューのステータス
        $where = "document_id = '".$document_id."'";
        if($is_storage == false) {
        	$data = array(
        			"status"          => $status,
        			"update_datetime" => date("Y-m-d H:i:s")
        	);
        	$this->obj_ConvertFile->update($data, $where);
        }
        // 資料ステータス
        $data = array(
            "document_status" => $status,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $this->obj_Document->update($data, $where);
        // storage機能の情報も更新
        // 資料情報
        $document_info = $this->getDetail($document_id);
        if($document_info["document_mode"] == "storage"){
            $storage_data = array(
            "status" => $status,
            "update_datetime" => date("Y-m-d H:i:s")
            );
            $this->_objStorage->update($storage_data, $where);
        }
    }

    /**
     * 詳細情報取得
     */
    function getDetail($document_id) {
        $where = "document_id = '".addslashes($document_id)."'";
        $row = $this->obj_Document->getRow($where);
        return $row;
    }

    /**
     * 資料変換正常終了
     * 変換後のドキュメント情報を更新する。
     */
    function successful($document_id, $page_cnt) {
        require_once "classes/dbi/meeting.dbi.php";
        require_once "classes/dbi/room.dbi.php";
        $objMeeting = new MeetingTable($this->dsn);
        $objRoom = new RoomTable($this->dsn);
        // 資料情報
        $document_info = $this->getDetail($document_id);
        // 会議情報取得
        $room_key = $objMeeting->getOne("meeting_key = '".$document_info["meeting_key"]."'", "room_key");
        // 部屋の情報
        $whiteboard_page = $objRoom->getOne("room_key = '".$room_key."'", "whiteboard_page");
        // 指定あり
        if ($whiteboard_page) {
            // アップロード上限チェック
            if ($whiteboard_page < $page_cnt) {
                $this->logger->warn(array($document_id, $page_cnt, $whiteboard_page), "ページ上限オーバーにより変換しませんでした");
                $this->setStatus($document_id, DOCUMENT_STATUS_PAGE_ERROR);
                return false;
            }
        }
        $document = $this->getStatus($document_id);
        if ($document["status"] == DOCUMENT_STATUS_DELETE) {
            $this->logger->info($document_id, "削除済み");
            $dir = N2MY_DOC_DIR.$document_info["document_path"].$document_info["document_id"];
            require_once("lib/EZLib/EZUtil/EZFile.class.php");
            $obj_File = new EZFile();
            // $document["document_path"].$document["document_id"]がnullだとdoc全部消えるので念のためチェック
            if(N2MY_DOC_DIR != $dir && file_exists($dir)){
                $this->logger->info($dir);
                $obj_File->rmtree($dir);
            }
            return false;
        }
        $this->logger->debug(N2MY_DOC_DIR.$document["document_path"].$document_id);
        $base_dir = N2MY_DOC_DIR.$document["document_path"].$document_id;
        // オプション
        $addition = unserialize($document["addition"]);
        $filesize = 0;
        for($i = 1; $i <= $page_cnt; $i++) {
            $filesize += filesize($base_dir."/".$document_id."_".$i.".".$addition["ext"]);
        }
        // キューの状態を正常完了
        $this->setStatus($document_id, DOCUMENT_STATUS_SUCCESS);
        // ユーザDBの資料情報を更新
        $where = "document_id = '".$document_id."'";
        $data = array(
            "document_index"  => $page_cnt,
            "document_size"   => $filesize,
            "document_status" => DOCUMENT_STATUS_SUCCESS,
            "update_datetime" => date("Y-m-d H:i:s")
        );
        $this->obj_Document->update($data, $where);
        // storage機能の情報も更新
//        $this->logger->info($document_info["document_mode"]);
        if($document_info["document_mode"] == "storage"){
            $storage_data = array(
            "file_path" => $document_info["document_path"],
            "document_index"  => $page_cnt,
            "file_size"   => $filesize,
            "status" => DOCUMENT_STATUS_SUCCESS,
            "update_datetime" => date("Y-m-d H:i:s")
            );
            $this->_objStorage->update($storage_data, $where);
        }
        // 元ファイル削除
        $this->_deleteOriginFile($document_id);
        // コールバックURL
        if (isset($addition["success_url"])) {
            $ch = curl_init();
            $option = array(
                CURLOPT_URL => $addition["success_url"],
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 1,
                CURLOPT_TIMEOUT => 1,
                );
            curl_setopt_array($ch, $option);
            $this->logger->debug($option);
            $ret = curl_exec($ch);
        }
    }

    function error($document_id, $status) {
        $document = $this->getStatus($document_id);
        $base_dir = N2MY_DOC_DIR.$document["document_path"].$document_id;
        $addition = unserialize($document["addition"]);
        // キューのステータス
        $where = "document_id = '".$document_id."'";
        $data = array(
            "status"          => DOCUMENT_STATUS_ERROR,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $this->obj_ConvertFile->update($data, $where);
        // 資料ステータス
        if (!is_numeric($status)) {
            $status = DOCUMENT_STATUS_ERROR;
        }
        $data = array(
            "document_status" => $status,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $this->obj_Document->update($data, $where);
        // 資料情報
        $document_info = $this->getDetail($document_id);
        if($document_info["document_mode"] == "storage"){
            $storage_data = array(
                    "status" => $status,
                    "update_datetime" => date("Y-m-d H:i:s")
            );
            $this->_objStorage->update($storage_data, $where);
        }
        $document = $this->getStatus($document_id);
        // コールバックURL
        if (isset($addition["error_url"])) {
            $ch = curl_init();
            $option = array(
                CURLOPT_URL => $addition["error_url"],
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 1,
                CURLOPT_TIMEOUT => 1,
                );
            curl_setopt_array($ch, $option);
            $this->logger->info($option);
            $ret = curl_exec($ch);
        }
    }

    /**
     * 資料ID生成
     */
    function createID() {
        $document_id = sha1(uniqid(rand(), true));
        return $document_id;
    }

    /**
     * 一覧取得
     */
    function getList($where = "") {
        if ($where) {
            $where .= " AND document_status != ".DOCUMENT_STATUS_DELETE;
        }
        $rows = $this->obj_Document->getRowsAssoc($where, array("document_sort" => "asc"));
        return $rows;
    }

    /**
     * ソート
     */
    function sort() {
    }

    /**
     * 変換実行中か確認
     */
    function getActiveProcess() {
        $data = $this->obj_Process->getRow(1);
        return $data;
    }

    /**
     * 変換開始
     */
    function start($url) {
        $data = array(
            "status" => "1",
            "create_datetime" => date("Y-m-d H:i:s"),
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $this->obj_Process->add($data);
        // 変換開始
        if ($url) {
            $ch = curl_init();
            $option = array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 1,
                CURLOPT_TIMEOUT => 1,
                );
            curl_setopt_array($ch, $option);
            $this->logger->debug($option);
            $ret = curl_exec($ch);
        }
    }

    /**
     * 処理中
     */
    function keepAlive($no) {
        $where = "no = ".$no;
        $data = array(
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $this->obj_Process->update($data, $where);
    }

    function stop($no) {
        $where = "no = ".$no;
        $this->obj_Process->remove($where);
        return true;
    }

    /**
     * フォーマットチェック
     */
    function check($file) {
        if (!$this->file_info($file["name"])) {
            return false;
        }
        return true;
    }

    /**
     * 画像フォーマット
     */
    function convertImage($input_file, $output_file, $opt) {
        $obj_Image = new EZImage(N2MY_IMGMGC_DIR);
        if (!file_exists(dirname($output_file))) {
            EZFile::mkdir_r(dirname($output_file), 0777);
            chmod(dirname($output_file), 0777);
        }
        // 画像変換
        $ret = $obj_Image->convert($input_file, $output_file, $opt["x"], $opt["y"]);
        if ($ret) {
            $this->logger->debug($ret);
            // 単なるリサイズの場合は削除しない
            if ($input_file != $output_file) {
                unlink($input_file);
            }
            return count($ret);
        } else {
            return false;
        }
    }

    /**
     *
     */
    function file_info($file) {
        // 拡張子でチェック（無難？）
        $path_parts = pathinfo($file);
        $path_parts['extension'] = strtolower($path_parts['extension']);
        $config = EZConfig::getInstance();
        $image_ext = $config->getAll("DOCUMENT_IMAGES");
        // 画像
        foreach($image_ext as $type => $extensions) {
            $_extensions = split(",", $extensions);
            if (in_array($path_parts['extension'], $_extensions)) {
                $path_parts["type"] = "image";
                return $path_parts;
            }
        }
        // 資料
        $document_ext = $config->getAll("DOCUMENT_FILES");
        foreach($document_ext as $type => $extensions) {
            $_extensions = split(",", $extensions);
            if (in_array($path_parts['extension'], $_extensions)) {
                $path_parts["type"] = "document";
                return $path_parts;
            }
        }
        return false;
    }

    public function getPreviewFileInfo( $info, $index = 1)
    {
        $fileInfo = array();
        $this->logger->debug($info);
        switch( $info["document_status"] ){
            case "1":    //変換中？
                $fileInfo = $this->_getConvertingFileInfo( $info, $index );
                break;

            case "2":    //正常完了
                $fileInfo = $this->_getPreviewFileInfo( $info, $index );
                break;

            case "3":    //削除?
            default :    //失敗（-1）
                $fileInfo = $this->_getVoidImageInfo();
                break;
        }
        return $fileInfo;
    }

    public function getSupportFormat($room_key) {
        static $room_list;
        if (!$room_key) {
            return false;
        } elseif (isset($room_list[$room_key])) {
            return $room_list[$room_key];
        }
        // アプリで有効なフォーマット一覧
        $config = EZConfig::getInstance();
        $document_ext = $config->getAll('DOCUMENT_FILES');
        $image_ext    = $config->getAll('DOCUMENT_IMAGES');
        // 部屋情報取得
        require_once "classes/dbi/room.dbi.php";
        $objRoom = new RoomTable($this->dsn);
        $where = "room_key = '".addslashes($room_key)."'";
        $room_info = $objRoom->getRow($where);
        if ($room_info) {
            // 指定有り
            if ($room_info["whiteboard_filetype"]) {
                $_filetype = unserialize($room_info["whiteboard_filetype"]);
                $filetype       = array();
                $document_list  = array();
                $image_list     = array();
                // 資料
                if ($_filetype["document"]) {
                    foreach ($_filetype["document"] as $file) {
                        foreach($document_ext as $type => $extensions) {
                            $_extensions = split(",", $extensions);
                            // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                            if (in_array($file, $_extensions) || $file == $type) {
                                $document_list[$type] = $_extensions;
                                foreach($_extensions as $ext) {
                                    $filetype[]             = $ext;
                                    $document_ext_list[]    = $ext;
                                }
                            }
                        }
                    }
                }
                // 画像
                if ($_filetype["image"]) {
                    foreach ($_filetype["image"] as $file) {
                        foreach($image_ext as $type => $extensions) {
                            $_extensions = split(",", $extensions);
                            // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                            if (in_array($file, $_extensions) || $file == $type) {
                                $image_list[$type] = $_extensions;
                                foreach($_extensions as $ext) {
                                    $filetype[]         = $ext;
                                    $image_ext_list[]   = $ext;
                                }
                            }
                        }
                    }
                }
                if (($document_ext_list || $image_ext_list) && $document_ext["xdw"] && !array_search("xdw",$document_ext_list)) {
                    $filetype[]          = "xdw";
                    $document_ext_list[] = "xdw";
                }
                if (($document_ext_list || $image_ext_list) && $document_ext["xdw"] && !array_search("xbd",$document_ext_list)) {
                    $filetype[]          = "xbd";
                    $document_ext_list[] = "xbd";
                }
            // 指定なし（全て利用可能）
            } else {
                // 資料
                foreach($document_ext as $type => $extensions) {
                    $_extensions = split(",", $extensions);
                    $document_list[$type] = $_extensions;
                    foreach($_extensions as $ext) {
                        $filetype[]             = $ext;
                        $document_ext_list[]    = $ext;
                    }
                }
                // 画像
                foreach($image_ext as $type => $extensions) {
                    $_extensions = split(",", $extensions);
                    $image_list[$type] = $_extensions;
                    foreach($_extensions as $ext) {
                        $filetype[]         = $ext;
                        $image_ext_list[]   = $ext;
                    }
                }
                if (($document_ext_list || $image_ext_list) && $document_ext["xdw"] && !array_search("xdw",$document_ext_list)) {
                    $filetype[]          = "xdw";
                    $document_ext_list[] = "xdw";
                }
                if (($document_ext_list || $image_ext_list) && $document_ext["xdw"] && !array_search("xbd",$document_ext_list)) {
                    $filetype[]          = "xbd";
                    $document_ext_list[] = "xbd";
                }
            }
            $room_list[$room_key] = array(
                'support_ext_list'  => $filetype,
                'document_list'     => $document_list,
                'document_ext_list' => $document_ext_list,
                'image_list'        => $image_list,
                'image_ext_list'    => $image_ext_list,
            );
        // 部屋指定なし
        } else {
            $room_list[$room_key] = false;
        }
        return $room_list[$room_key];
    }

    function change_sort($id,$move){
        $mocument_info = $this->getDetail($id);
        $this->logger->debug($mocument_info,__FILE__,__FUNCTION__,__LINE__);
                
        if($move == "up" and $mocument_info["document_sort"] > 1) {
            $current_sort = $mocument_info["document_sort"];
            $move_sort = $current_sort - 1;
        } else if ($move == "down") {
        	$current_sort = $mocument_info["document_sort"];
        	$move_sort = $current_sort + 1;
        }
        $this->logger->debug($move_sort,__FILE__,__FUNCTION__,__LINE__);
        $update_data = array( "document_sort" => $current_sort );
        $where = "meeting_key = '".$mocument_info["meeting_key"]."' AND document_sort = '".$move_sort."'";
        $this->obj_Document->update($update_data,$where);
        $update_data = array( "document_sort" => $move_sort );
        $where = "document_id = '".$id."'";
        $this->obj_Document->update($update_data,$where);
        
        return 1;
    }
    
    private function _getConvertingFileInfo()
    {
        $targetImage = N2MY_DOCUMENT_ROOT."shared/images/lb/image-converting.jpg";
        $imageInfo = getimagesize($targetImage);
        $this->logger->debug($imageInfo);
        if( $imageInfo != false ){
            $return["fileName"] = "image-converting.jpg";
            $return["targetImage"] = $targetImage;
            $return["mime"] = $imageInfo["mime"];
            return $return;
        } else {
            return $this->_getVoidImageInfo();
        }
    }

    private function _getPreviewFileInfo( $info, $index )
    {
        $return = array();
        $return["fileName"] = sprintf( "%s_%d.jpg", $info["document_id"], $index );
        $targetImage = sprintf( "%s%s%s/%s", N2MY_DOC_DIR, $info["document_path"], $info["document_id"], $return["fileName"] );
        $imageInfo = getimagesize( $targetImage );

        if( $imageInfo && $imageInfo != false ){
            $return["targetImage"] = $targetImage;
            $return["mime"] = $imageInfo["mime"];
            return $return;
        } else {
            return $this->_getVoidImageInfo();
        }
    }

    private function _getVoidImageInfo()
    {
        $errorImage = N2MY_DOCUMENT_ROOT."shared/images/lb/image-error.jpg";
        $imageInfo = getimagesize($errorImage);
        $return["fileName"] = "image-error.jpg";
        $return["targetImage"] = $errorImage;
        $return["mime"] = $imageInfo["mime"];
        $this->logger->debug($return);
        return $return;
    }
}
