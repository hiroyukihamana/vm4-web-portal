<?php

require_once("lib/EZLib/EZCore/EZLogger.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/member_room_relation.dbi.php");
require_once('classes/core/Core_Meeting.class.php');

class N2MY_Inbound {

    var $dsn = null;

    function __construct($dsn) {
        $this->logger = EZLogger2::getInstance();
        $this->obj_User     = new UserTable($dsn);
        $this->obj_Member   = new MemberTable($dsn);
        $this->obj_Room     = new RoomTable($dsn);
        $this->obj_MemberRoomRelation     = new MemberRoomRelationTable($dsn);
        $this->dsn = $dsn;
    }

    function inboundRoomFlg($user_key, $inbound_id, $status, $sort_key, $sort_type, $limit = null, $offset = 0, $search = "") {
      $relate = "room.room_key = mr.room_key";
      $where = "user_key = ".$user_key.
               " AND use_sales_option = 1 AND room_status = 1";
      if ($inbound_id && $status) {
          $where .= " AND inbound_flg = 1".
                    " AND inbound_id = '".$inbound_id."'";
      } else {
          $where .= " AND inbound_flg = 0";
      }
      $relation_keys = $this->obj_Room->innerJoin("member_room_relation mr",$relate ,$where ,null ,null ,null ,"mr.member_key,mr.room_key");
      if ($relation_keys) {
          foreach ($relation_keys as $data) {
              $member_keys[] = $data["member_key"];
              $room_keys[$data["member_key"]] = $data["room_key"];
          }
          $where = "member_key IN (". implode(",", $member_keys) .")";
          if ($search) {
            $where .= " AND (member_name like '%".mysql_real_escape_string($search)."%' or member_id like '%".mysql_real_escape_string($search)."%')";
          }
          $member_list = $this->obj_Member->getRowsAssoc($where, array($sort_key => $sort_type),$limit,$offset);
          foreach ($member_list as $member) {
              $room_key = $room_keys[$member["member_key"]];
              $member["room_key"] = $room_key;
              $result[] = $member;
          }
          $this->logger->debug(array($where, $member_keys, $result));
          return $result;
      } else {
          return array();
      }
    }

    function inboundRoomFlgCount($user_key, $inbound_id, $status, $search = "") {
      $relate = "room.room_key = mr.room_key";
      $where = "user_key = ".$user_key.
               " AND use_sales_option = 1 AND room_status = 1";
      if ($inbound_id && $status) {
          $where .= " AND inbound_flg = 1".
                    " AND inbound_id = '".$inbound_id."'";
      } else {
          $where .= " AND inbound_flg = 0";
      }
      $relation_keys = $this->obj_Room->innerJoin("member_room_relation mr",$relate ,$where, null, null, null, "mr.member_key");
      if ($relation_keys) {
          foreach ($relation_keys as $data) {
              $member_keys[] = $data["member_key"];
          }

          $where = "member_key IN (". implode(",", $member_keys) .")";
          if ($search) {
            $where .= " AND (member_name like '%".mysql_real_escape_string($search)."%' or member_id like '%".mysql_real_escape_string($search)."%')";
          }
          $member_count = $this->obj_Member->numRows($where);
          $this->logger->debug(array($where, $member_keys, $member_count));
          return $member_count;
      } else {
          return 0;
      }
    }

    public function getInboundStatus($user_id, $inbound_id)
    {
        $obj_CoreMeeting = new Core_Meeting( $this->dsn );
        $room_list = $obj_CoreMeeting->getMeetingStatusInbound($user_id, $inbound_id);
        $this->logger->info($room_list);
        $list = array();
        if ($room_list) {
            foreach( $room_list as $key => $room){
                $participants = array();
                $room["staff"] = 0;
                $room["customer"] = 0;
                foreach($room["participants"] as $participant) {
                    switch ($participant["participant_type_name"]) {
                    case "staff":
                        $room["staff"]++;
                        if (1 == $participant["is_away"]) {
                            $room["is_away"] = 1;
                        }
                        break;
                    case "customer":
                        $room["customer"]++;
                        break;
                    }
                }
                //入室者がstaffが一人でis_awayだったらstatusを0にする。
                if (1 == $room["staff"] && 0 == $room["customer"] && 1 == $room["is_away"]) {
                    $room["status"] = "0";
                } else if (1 == $room["staff"] && 0 == $room["customer"]) {
                //入室者がstaff一人だったら、入室URLも渡す。
                    $invitation_id = sprintf("%s,%s", $room["room_key"], $room["meeting_session_id"]);
                    //$invitation_id = $this->encryptInviteId($invitation_id)
                    $room["meeting_id"] = $room["meeting_session_id"];
                    $room["invitation_url"] = sprintf( "%sinbound/%s", N2MY_BASE_URL, $room["meeting_session_id"]);
                }
                //予約確認
                if ($room["status"] == 1 && $room["is_reserved"] == 1) {
                    $room["status"] = "0";
                }
                $list[] = $room;
            }
            // エンティティ処理
            array_walk_recursive($list, create_function('&$val, $key', '$val = htmlspecialchars($val);'));
        } else {
            $list = "";
        }
        return $list;
    }

    public function encryptInviteId($data)
    {
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $key = N2MY_ENCRYPT_KEY;
        $iv = N2MY_ENCRYPT_IV;
        $object = EZEncrypt::encrypt($key, $iv, $data);
        $object = str_replace("/", "-", $object);
        $object = str_replace("+", "_", $object);
        return urlencode($object);
    }

    public function decryptInviteId($data)
    {
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $key = N2MY_ENCRYPT_KEY;
        $iv = N2MY_ENCRYPT_IV;
        $object = urldecode($data);
        $object = str_replace("-", "/", $object);
        $object = str_replace(' ', '+', $object);
        $object = str_replace('_', '+', $object);
        return EZEncrypt::decrypt($key, $iv, $object);
    }
}
