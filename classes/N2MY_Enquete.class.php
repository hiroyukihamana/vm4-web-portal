<?php
/**
 * 指定したIDでDBの保存先、アンケート項目を取得し
 * 入力チェック、保存、一覧取得等を可能にする。
 */
require_once 'lib/EZLib/EZCore/EZLogger2.class.php';
class N2MY_Enquete {

    var $id = "";
    var $dsn = null;
    var $define = null;
    var $logger = null;
    var $db = null;

    function __construct($dsn) {
        $this->logger = EZLogger2::getInstance();
        $this->dsn = $dsn;
    }

    function getList() {
        $dir = N2MY_APP_DIR.'setup/enquete/';
        $enqete_files = glob($dir."*.yaml");
        $list = array();
        foreach($enqete_files as $file) {
            $pathinfo = pathinfo($file);
            $list[] = $pathinfo['filename'];
        }
        return $list;
    }

    function init($id) {
        $this->id = $id;
        $enquete_file = N2MY_APP_DIR.'setup/enquete/'.$id.'.yaml';
        if ($enquete_file) {
            require_once("lib/spyc/spyc.php");
            $enquete_model = Spyc::YAMLLoad($enquete_file);
            $this->define = $enquete_model;
            require_once("lib/EZLib/EZDB/EZDB.class.php");
            $this->objEnquete = new EZDB;
            $this->objEnquete->init($this->dsn, $id);
        }
    }

    function getModel() {
        return $this->define["model"];
    }

    function check($data) {
        $check_obj = new EZValidator($data);
        foreach($this->define["model"] as $key => $field) {
            if ($field["validator"]) {
                $check_obj->check($key, $field["validator"]);
            }
        }
        return $check_obj;
    }

    function save($data) {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $this->logger->info($data);
        $ret = $this->objEnquete->add($data);
        if (DB::isError($ret)) {
            $this->logger->info($ret->getUserInfo());
            return false;
        }
        return true;
    }

    function getAnswer() {
        $rows = $this->objEnquete->getRowsAssoc();
        $model = $this->getModel();
        foreach($rows as $key => $row) {
            foreach($model as $field => $info) {
                if (isset($rows[$key][$field])) {
                    switch ($info['type']) {
                        // 単一選択
                        case 'set' :
                        case 'list' :   // みっすた。変なキー作るんじゃなかった
                            $rows[$key][$field] = $info['list'][$row[$field]];
                            break;
                        // 複数選択
                        case 'enum' :
                            $_list = split(',', $row[$field]);
                            $_val = "";
                            if ($_list) {
                                foreach($_list as $_key) {
                                    $_val .= $info['list'][$_key];
                                }
                            }
                            $rows[$key][$field] = $_val;
                            break;
                        case 'text' :
                            break;
                        default :
                            break;
                    }
                }
            }
        }
        return $rows;
    }

    function _displayValue() {
    }

    function getSummary() {
    }
}