<?php
require_once("classes/dbi/pgi_rate.dbi.php");
require_once("classes/dbi/pgi_setting.dbi.php");
require_once("lib/EZLib/EZCore/EZLogger.class.php");
require_once("lib/EZLib/EZUtil/EZString.class.php");
require_once("lib/EZLib/EZUtil/EZFile.class.php");

class N2MY_PGi_Rate
{
    var $dsn;
    var $config;
    //{{{ __construct
    public function __construct($dsn)
    {
        $this->logger            = EZLogger2::getInstance();
        $this->dsn               = $dsn;
        $ezConfig                = EZConfig::getInstance(N2MY_CONFIG_FILE);
        $this->config            = $ezConfig->getAll("PGi");
        $this->pgi_setting_table = new PGiSettingTable($this->dsn);
        $this->pgi_rate_table    = new PGiRateTable($this->dsn);
    }
    //{{{ getPGiRateByRoomKey
    public function findByRoomKey($room_key)
    {
        if (!$room_key) {
            throw new Exception('room_key required');
        }

        $res  = array(); 
        $pgi_setting       = $this->pgi_setting_table->findEnablleNow($room_key);
        if ($pgi_setting) {
            $pgi_rate       = $this->pgi_rate_table->findByPGiSettingKey($pgi_setting['pgi_setting_key']);
            foreach ($pgi_rate as $rate) {
                $res[$rate['rate_type']] = $rate['rate']; 
            }
            return $res;
        }

        require_once('classes/pgi/PGiSystem.class.php');
        foreach (PGiSystem::findDefault()->defaultRates as $k=>$rates) {
            foreach ($rates as $k => $rate) {
                $res[$k] = (string)$rate;
            }
        }
        return $res; 
    }
    //}}}
}
