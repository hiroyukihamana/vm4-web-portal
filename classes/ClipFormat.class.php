<?php
class ClipFormat
{
    //{{ formatClipThumbnailUrl
    static function thumbnailUrl($clip_key, $storage_no)
    {
        $config  = EZConfig::getInstance(N2MY_CONFIG_FILE)->getAll("VIDEO");
        return sprintf("%s/%s/%s/thumbnail/%s.jpg?%s", $config["virtualhost_url"],
                                                       $config["virtualhost_hush"],
                                                       $storage_no,
                                                       $clip_key,
                                                       uniqid());
    }
    //}}} 
    function streamFmsInctanceName($storage_no)
    {
        $config  = EZConfig::getInstance(N2MY_CONFIG_FILE)->getAll("VIDEO");
        return sprintf("%s/%s/%s/movie/",
                       $config["stream_fms_appli"],
                       $config["storage"],
                       $storage_no);
    }
    function limeLightInctanceName($storage_no)
    {
        $config  = EZConfig::getInstance(N2MY_CONFIG_FILE)->getAll("VIDEO");
        return sprintf("/%s/%s/%s/%s/movie/",
                       $config["limelight_appli"],
                       $config["limelight_mode"],
                       $config["storage"],
                       $storage_no);
    }
    function limeLightFlvUrl($clip_key, $storage_no)
    {
       $config = EZConfig::getInstance(N2MY_CONFIG_FILE)->getAll("VIDEO");
       return sprintf( "%s/%s/%s/%s/movie/%s.flv",
                       $config["limelight_http_url"],
                       $config["limelight_mode"],
                       $config["storage"],
                       $storage_no,
                       $clip_key);
   }
   function limeLightPlayUrl($clip_key, $storage_no) 
   {
        $config = EZConfig::getInstance(N2MY_CONFIG_FILE)->getAll("VIDEO");
        return  preg_replace('|(/{2,})|','/',sprintf("%s/%s/%s/movie/%s",
                    $config["limelight_mode"],                        
                    $config["storage"],
                    $storage_no,
                    $clip_key));
  }
}
