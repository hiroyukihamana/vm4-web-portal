<?php
require_once("lib/EZLib/EZXML/EZXML.class.php");
require_once("lib/EZLib/EZUtil/EZUrl.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/reservation.dbi.php");
require_once('classes/core/Core_Meeting.class.php');

class N2MY_Meeting {

    var $logger = "";
    private $dsn = null;
    private $obj_Room = null;
    private $obj_Reservation = null;

    /**
     * コンストラクタ
     */
    function N2MY_Meeting($dsn){
        $this->logger  = & EZLogger::getInstance();
        $this->logger2 = & EZLogger2::getInstance();
        $this->dsn     = $dsn;
        $this->obj_Room        = new RoomTable($dsn);
        $this->obj_Reservation = new ReservationTable($dsn);
    }

    /**
     * 現在利用可能な会議を取得する
     *
     * @param string $room_key 部屋ID
     * @return array 会議データ
     */
    function getNowMeeting($room_key, $meeting_ticket = null) {
        $room_info        = $this->obj_Room->getRow( sprintf( "room_key='%s'", $room_key ) );

        //Box対策
        if ($room_info['use_stb_option'] == 1) {
			$this->logger2->info(array($room_info["room_key"],$room_info["creating_meeting_flg"],$room_info["meeting_key"]), 'use_stb_room waiting');
        	$millisec = mt_rand( 1, 1000 ); // 1ms - 1000ms(1sec)
			usleep( $rand  * 1000 ); // micro sec
			$room_info = $this->obj_Room->getRow( sprintf( "room_key='%s'", $room_key ) );
			$this->logger2->info(array($room_info["room_key"],$room_info["creating_meeting_flg"],$room_info["meeting_key"], $rand), 'use_stb_room waiting done');
		}

        //同時入室対策の会議データ作成中の場合はsleep
        if ($room_info["creating_meeting_flg"] == 1) {
            sleep(1);
            $this->logger2->info(array($room_info["room_key"],$room_info["meeting_key"]), "waiting create meeting");
            //部屋情報の最新再取得
            $room_info        = $this->obj_Room->getRow( sprintf( "room_key='%s'", $room_key ) );
            $this->logger2->info(array($room_info["room_key"],$room_info["meeting_key"]), "waiting done");
        }
        $meeting_name     = "";
        $meeting_password = "";
        // 最後に実行されたMeetingKeyを取得
        $last_meeting_key    = $room_info["meeting_key"];
        $last_meeting_info   = $this->getMeetingStatus($room_key, $last_meeting_key);
        $last_meeting_status = $last_meeting_info["status"];
        $reservation_row     = $this->obj_Reservation->hasReservation($room_key);
        $this->logger->debug(__FUNCTION__."reservation_row", __FILE__, __LINE__, $reservation_row);

        // 最後に実行された会議が終了
        if ($last_meeting_status == "0") {
            // 現在時刻予約なし
            if (!$reservation_row) {
                // 発行直前に変更されていないか確認
                $rewrite_row = $this->obj_Room->getRow( sprintf( "room_key='%s'", $room_key ) );
                if ($last_meeting_key != $rewrite_row["meeting_key"]) {
                    $start_status = "[会議終了]-発行直前に変更された";
                    $meeting_key  = $rewrite_row["meeting_key"];
                } else {
                    $start_status = "[会議終了]-新規会議で上書き";
                    $meeting_key  = $this->createMeetingKey($room_key);
                    ////会議作成中にフラグ変更
                    $this->obj_Room->last_meeting_update($room_key, $meeting_key, 1);
                }
                $start_time = date("Y-m-d H:i:s");
                $end_time   = "";
            // 予約あり
            } else {
                $start_status        = "[会議終了]-予約あり会議";
                $meeting_key         = $reservation_row["meeting_key"];
                $this->obj_Room->last_meeting_update($room_key, $meeting_key);
                $meeting_password    = $reservation_row["reservation_pw"];
                $meeting_name        = $reservation_row["reservation_name"];
                $start_time          = $reservation_row["reservation_starttime"];
                $end_time            = $reservation_row["reservation_endtime"];
                $reservation_session = $reservation_row["reservation_session"];
            }
        // 最後に実行された会議が使用中
        } elseif ($last_meeting_status == "1") {
            if ($reservation_row["meeting_key"] == $last_meeting_key) {
                $start_status     = "[会議中]-予約会議";
                $meeting_password = $reservation_row["reservation_pw"];
                $meeting_name     = $reservation_row["reservation_name"];
                $start_time       = $reservation_row["reservation_starttime"];
                $end_time         = $reservation_row["reservation_endtime"];
                $reservation_session = $reservation_row["reservation_session"];
            } else if($meeting_ticket == $last_meeting_info["meeting_ticket"]) {
                $start_status = "[会議延長中]-招待された会議が延長中";
                $where = "meeting_key = '".$meeting_ticket."'";
                $now_reservation     = $this->obj_Reservation->getRow($where);
                $meeting_password = $now_reservation["reservation_pw"];
                $meeting_name     = $now_reservation["reservation_name"];
                $start_time       = $now_reservation["reservation_starttime"];
                $end_time         = $now_reservation["reservation_endtime"];
                $reservation_session = $now_reservation["reservation_session"];
                //招待を受けた会議と現在行われている会議が異なるため入室できない
            } else if($meeting_ticket &&
                    $meeting_ticket != $last_meeting_info["meeting_ticket"]) {
                    $start_status = "[会議終了]-招待された会議が既に終了";
                    $last_meeting_status = 0;
                //招待を受けた会議と現在行われている会議が異なるため入室できない
            } else {
                //延長会議が予約だった場合は予約情報取得
                $this->logger->debug("last_meeting",__FILE__,__LINE__,$last_meeting_info);
                if ($last_meeting_info["is_reserved"]) {
                    $now_reservation = $this->obj_Reservation->getRow("meeting_key = '".$last_meeting_info["meeting_ticket"]."'");
                    $meeting_password = $now_reservation["reservation_pw"];
                    $meeting_name     = $now_reservation["reservation_name"];
                    $start_time       = $now_reservation["reservation_starttime"];
                    $end_time         = $now_reservation["reservation_endtime"];
                    $reservation_session = $now_reservation["reservation_session"];
                }
                $this->logger->debug("meeting-info",__FILE__,__LINE__,$last_meeting_info);
                $start_status = "[会議中]-予約なし会議";
                $start_time = date("Y-m-d H:i:s");
                $end_time = "";
            }
            $this->logger->debug(__FUNCTION__."#開催中の会議を使用", __FILE__, __LINE__);
            $meeting_key = $last_meeting_key;
        }
        $ret = array(
            "meeting_key"         => $meeting_key,
            "meeting_name"        => $meeting_name,
            "meeting_password"    => $meeting_password,
            "start_time"          => $start_time,
            "end_time"            => $end_time,
            "reservation_session" => $reservation_session,
            "last_meeting_status" => $last_meeting_status
            );
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__,
            array(
                "start_status"        => $start_status,
                "last_meeting_key"    => $last_meeting_key,
                "last_meeting_status" => $last_meeting_status,
                "result" => $ret
                )
            );
        return $ret;
    }

    /**
     * one time meeting 開始準備
     * @param $user_key
     */
    function oneTimeMeeting($user_id){

        $meeting_name = "";
        $meeting_password = "";
        $start_time = "";
        $end_time = "";
        $reservation_session = "";
        $last_meeting_status = "";

        $meeting_key  = $user_id . "_" . md5(uniqid(rand(), true));

        $ret = array(
                "meeting_ticket"         => $meeting_key,
                "meeting_name"        => $meeting_name,
                "meeting_password"    => $meeting_password,
                "start_time"          => $start_time,
                "end_time"            => $end_time,
                "reservation_session" => $reservation_session,
                "last_meeting_status" => $last_meeting_status
        );
        return $ret;
    }

    /**
     * 会議情報取得
     */
    function getMeeting($room_key, $type, $meeting_key = null) {
        $meeting = null;
        switch ($type) {
            case "invitedGuest" :
                $meeting_ticket = $meeting_key;    //混乱を避けるため正しい変数名に差し替え
                $meeting        = $this->getNowMeeting($room_key, $meeting_ticket);
                    $this->logger->debug("now_meeting",__FILE__,__LINE__,$meeting);
                if (!$meeting["last_meeting_status"]) {
                    return "";
                }
                if (!$meeting["reservation_session"] || ($meeting["reservation_session"] && $meeting["last_meeting_status"])) {
                    return $meeting;
                }
            // 招待者
            case "invite" :
                // 現在の予約状況
                $nowReservation = $this->obj_Reservation->hasReservation($room_key);
                // 現在の予約と一致しない場合はエラー
                if ($nowReservation["meeting_key"] != $meeting_key) {
                    // 予約時間が過ぎていても利用中であれば入室
                    $meeting = $this->getNowMeeting($room_key, $meeting_ticket);
                    if (!$meeting["last_meeting_status"]) {
                        return false;
                    }
                    // 予約時間を過ぎて利用中
                    if (!$meeting["reservation_session"] || ($meeting["reservation_session"] && $meeting["last_meeting_status"])) {
                        return $meeting;
                    }
                    $this->logger->warn(__FUNCTION__."#現在の予約と一致しない場合はエラー",__FILE__,__LINE__,
                                        array($nowReservation, $meeting_key));
                    return false;
                }
                // 最後に実行されたMeetingKeyを取得
                $room_info           = $this->obj_Room->getRow( sprintf( "room_key='%s'", $room_key ) );
                $last_meeting_key    = $room_info["meeting_key"];
                $last_meeting_info   = $this->getMeetingStatus($room_key, $last_meeting_key);
                $last_meeting_status = $last_meeting_info["status"];
                $participant_count   = count($last_meeting_info["participants"]);
                // 他の利用者が制限時間を超えて会議を継続中
                if ($last_meeting_key != $meeting_key) {
                    // ほかの会議で使用中
                    if (($last_meeting_status == "1") && ($participant_count > 0)) {
                        $this->logger->warn(__FUNCTION__."#他の利用者が制限時間を超えて会議を継続中",
                            __FILE__,__LINE__,array($nowReservation,$meeting_key));
                        return false;
                    }
                    $this->obj_Room->last_meeting_update($room_key, $meeting_key);
                    $meeting = array(
                        "meeting_key"      => $meeting_key,
                        "meeting_name"     => $nowReservation["reservation_name"],
                        "meeting_password" => $nowReservation["reservation_pw"],
                        "start_time"       => $nowReservation["reservation_starttime"],
                        "end_time"         => $nowReservation["reservation_endtime"]
                    );
                }
                $meeting = $this->getNowMeeting($room_key);
                break;

            // 通常、コンパクト、テレプレゼンスなど
            default :
                // 現在利用可能な会議キーを取得
                $meeting = $this->getNowMeeting($room_key);
                if (!$meeting) {
                    $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,array($room_key, $type, $meeting_key));
                }

                break;
        }
        return $meeting;
    }

    /**
     * 会議ステータス取得
     */
    function getMeetingStatus($room_key, $meeting_key) {
        require_once( "classes/core/Core_Meeting.class.php" );
        $room             = array($room_key => $meeting_key);
        $obj_CORE_Meeting = new Core_Meeting( $this->dsn );
        $data             = $obj_CORE_Meeting->getMeetingStatus($room);
        $status           = $data[0];
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $status);

        return $status;
    }

    /**
     * 会議の作成
     *
     * @param string $room_key 部屋ID
     * @param array $option 会議オプション指定
     *      "country_id" 国コード
     *      "meeting_name" 会議名
     *      "user_key" ユーザキー
     *      "start_time" 開始時間
     *      "end_time" 終了時間
     * @return string 作成後のミーティングキー
     */
    function createMeeting($room_key, $meeting_key, $user_option = array())
    {
        require_once("classes/N2MY_Account.class.php");
        require_once("classes/dbi/user.dbi.php");
        require_once("config/config.inc.php");
        $config = EZConfig::getInstance();

        $obj_User  = new UserTable( $this->dsn );
        $user_info = $obj_User->getRow(sprintf("user_key='%s'", $user_option["user_key"]));

        // サービスオプション情報
        $obj_N2MY_Account = new N2MY_Account($this->dsn);
        if($room_key == null){
            // userに紐づく部屋設定とオプションを取得する
            $_room_info       = $obj_N2MY_Account->getUserRoomInfo($user_option["user_key"]);
            $room_key = $user_info["user_id"];
        }else{
            $_room_info       = $obj_N2MY_Account->getRoomInfo($room_key);
        }
        $room_info        = $_room_info["room_info"];
        $options          = $_room_info["options"];


        // 記録最大容量
        /*if ($user_info["account_model"] == 'member'){
            $meeting_rec_size = 0;
            $meeting_rec_size += $options["hdd_extention"] * 500 * 1024 * 1024;  // 500m
        } else {
            $meeting_rec_size = RECORD_SIZE * (1024 * 1024);          // デフォルト500MByte
            $meeting_rec_size += $options["hdd_extention"] * 1024 * 1024 * 1024;  // 1GBtype/契約
        }*/
        $meeting_rec_size = $user_info["max_storage_size"] * (1024 * 1024);

        // h.264とポリコムオプションついてる場合はFMSバージョンを登録
        if (($options["h264"] && $room_info["default_h264_use_flg"] == 1) || $options["video_conference"]) {
            $need_fms_version = "4.5.0.0";
        }

        $meeting_rec_size = $meeting_rec_size;

        // レイアウトタイプ
        if ($room_info["room_layout_type"]) {
            $layout_type = $room_info["room_layout_type"];
        } else {
            $layout_type = "1"; // デフォルト値
        }
        $country_id       = isset($user_option["country_id"])       ? $user_option["country_id"]   : "jp";
        $meeting_name     = isset($user_option["meeting_name"])     ? $user_option["meeting_name"] : "";
        $user_key         = isset($user_option["user_key"])         ? $user_option["user_key"]     : "";
        $starttime        = isset($user_option["start_time"])       ? $user_option["start_time"]   : date("Y-m-d H:i:s");
        $endtime          = isset($user_option["end_time"])         ? $user_option["end_time"]     : "";
        // CoreAPIの呼び出し
        $param = array(
            "room_key"                  => $room_key,
            "user_key"                  => $user_key,
            "layout_key"                => $layout_type,
            "meeting_ticket"            => $meeting_key,
            "meeting_room_name"         => $room_info["room_name"],
            "meeting_country"           => $country_id,
            "meeting_name"              => $meeting_name,
            "meeting_log_password"      => $user_option["password"],
            "meeting_tag"               => $room_key,
            "meeting_max_seat"          => ($room_info["extend_seat_flg"] && $room_info["extend_max_seat"] > 0) ? $room_info["extend_max_seat"] : $room_info["max_seat"],
            "meeting_max_audience"      => ($room_info["extend_seat_flg"] && $room_info["extend_max_audience_seat"] > 0) ? $room_info["extend_max_audience_seat"] : $room_info["max_audience_seat"],
            "meeting_max_whiteboard"    => $room_info["max_whiteboard_seat"],
            "meeting_size_recordable"   => $meeting_rec_size,
            "meeting_size_uploadable"   => 0,
            "meeting_start_datetime"    => $starttime,
            "meeting_stop_datetime"     => $endtime,
            "need_fms_version"          => $need_fms_version,
            "use_sales_option"          => $room_info["use_sales_option"],
            "use_stb_option"            => $room_info["use_stb_option"],
            "inbound_flg"               => $room_info["inbound_flg"],
            "inbound_id"                => $room_info["inbound_id"],
            "prohibit_extend_meeting_flg" => $room_info["prohibit_extend_meeting_flg"],
            "ssl"                       => $options["meeting_ssl"],
            "tls"                       => $options["meeting_tls"],
            "hispec"                    => $options["high_quality"],
            "mobile"                    => $options["mobile_phone_number"],
            "h323"                      => $options["h323_number"],
            "sharing"                   => $options["desktop_share"],
            "intra_fms"                 => $options["intra_fms"],
            "multicamera"               => $options["multicamera"],
            "telephone"                 => $options["telephone"],
            "smartphone"                => $options["smartphone"],
            "record_gw"                 => $options["record_gw"],
            "whiteboard_video"          => $options["whiteboard_video"],
            "video_conference"          => $options["video_conference"],
            "minimumBandwidth80"        => $options["minimumBandwidth80"],
            "h264"                      => $options["h264"],
            "ChinaFastLine"             => $options["ChinaFastLine"],
            "ChinaFastLine2"            => $options["ChinaFastLine2"],
            "GlobalLink"                => $options["GlobalLink"],
            "reservation_modify"        => $user_option["reservation_modify"],
            "is_one_time_meeting"       => $room_info["is_one_time_meeting"],
          );
        if (!$config->get("IGNORE_MENU", "teleconference") && @$options["teleconference"]) {
            $pgi_colmns = array("tc_type",
                                "tc_teleconf_note",
                                "use_pgi_dialin",
                                "use_pgi_dialin_free",
                                "use_pgi_dialin_lo_call",
                                "use_pgi_dialout");

            foreach ($pgi_colmns as $pgi_col) {
                if (isset($user_option[$pgi_col])) {
                    $param[$pgi_col] = $user_option[$pgi_col];
                }
            }
        }
        $obj_CORE_Meeting = new Core_Meeting( $this->dsn );
        $core_session_key = $obj_CORE_Meeting->checkMeetingStatus( $param );

        $this->logger2->debug('<<try create_pgi');
        if (!$config->get("IGNORE_MENU", "teleconference") && @$options["teleconference"] && $user_option["tc_type"]) {
            $this->logger2->debug('create_pgi');
            $this->createPGIWithBackground($meeting_key);
        }
        $this->logger2->debug('>>end create_pgi');

        return $core_session_key;
    }

    /**
     * 会議開始用のURLを作成(画面サイズを調整するページ)
     */
    function startMeeting($core_session, $type, $user_info) {
        $user_key            = isset($user_info["user_key"])  ? $user_info["user_key"] : "";
        $participant_id      = isset($user_info["id"])        ? $user_info["id"]       : "";
        $participant_name    = isset($user_info["name"])      ? $user_info["name"]     : "";
        $participant_country = isset($user_info["participant_country"]) ? $user_info["participant_country"]     : "jp";
        $lang                = isset($user_info["lang"])      ? $user_info["lang"]     : "ja";
        $skin_type           = isset($user_info["skin_type"]) ? $user_info["skin_type"] : "";
        $narrow              = isset($user_info["narrow"])    ? $user_info["narrow"]   : "0";
        $mode                = isset($user_info["mode"])      ? $user_info["mode"]     : "";
        $role                = isset($user_info["role"])      ? $user_info["role"]     : "";
        $mail_upload_address = $user_info["mail_upload_address"];
        $is_intra            = isset($user_info["is_intra"]) ? 1 : 0;

        // ミーティング開始
        $param = array(
            "user_key"             => $user_key,
            "meeting_session_id"   => $core_session,
            "member_key"           => $user_info["member_key"],
            "participant_id"       => $participant_id,
            "participant_name"     => $participant_name,
            "participant_email"    => $user_info["participant_email"],
            "participant_station"  => $user_info["participant_station"],
            "participant_locale"   => mysql_real_escape_string($lang),
            "participant_country"  => $participant_country,
            "participant_skin_type"=> $skin_type,
            "is_narrowband"        => $narrow,
            "participant_mode_name"=> $mode,      //trialか一般かの２択?
            "participant_type_name"=> mysql_real_escape_string($type),        //normal, audience, document, compact, telepresence
            "participant_role_name"=> $role,      //（デフォルト、オーディエンス、ゲスト、トライアル*）//default, invite
            "mail_upload_address"  => $mail_upload_address,
            "account_model"        => $user_info["account_model"],
            );

        if($user_info["participant_tag"]){
            $param["participant_tag"] = $user_info["participant_tag"];
        }

        $obj_CORE_Meeting = new Core_Meeting( $this->dsn );
        return $obj_CORE_Meeting->getMeetingDetails( $param );
    }

    /**
     * 会議キーを生成
     */
    public function createMeetingKey($room_key) {
        return $this->obj_Room->create_meeting_key($room_key);
    }
    //{{{ createPGIWithBackground
    public function createPGIWithBackground($meeting_key)
    {
        $url = N2MY_LOCAL_URL."pgi/api_access.php?action_create_reservation".
                              "&meeting_key=$meeting_key";
        $this->logger2->debug('create_pgi url '.$url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $str = curl_exec($ch);
        curl_close($ch);
    }

    public function getOneTimeMeetingRoomCount($user_key){
        $room_db = new RoomTable($this->dsn);
        $where = "is_one_time_meeting = 1 AND user_key='".addslashes($user_key)."'";
        return $room_db->numRows($where);
    }

    /**
     * ワンタイム部屋作成
     * @param int $user_key
     */
    public function createOneTimeMeetingRoom($user_key){
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->dsn);
        // userに紐づく部屋設定とオプションを取得する
        $_room_info       = $obj_N2MY_Account->getUserRoomInfo($user_key , 1);

        $user_room_info = $_room_info["room_info"];
        $user_option_info = $_room_info["options"];
        if(!$user_room_info){
            return false;
        }
        require_once("classes/dbi/user.dbi.php");

        $user_db = new UserTable($this->dsn);
        $where = "user_key='".addslashes($user_key)."'";
        $user_data = $user_db->getRow($where);
        $room_db = new RoomTable($this->dsn);
        $count = $room_db->numRows($where);

        $room_key = $user_data["user_id"]."-".($count + 1)."-".substr(md5(uniqid(time(), true)), 0,4);
        // いらないデータ削除
        unset($user_room_info["create_datetime"]);
        unset($user_room_info["update_datetime"]);
        unset($user_room_info["delete_datetime"]);
        unset($user_room_info["expire_datetime"]);
        unset($user_room_info["status"]);
        unset($user_room_info["user_room_setting_key"]);
        // 必要データ入れ
        $user_room_info["room_key"] = $room_key;
        $user_room_info["room_status"] = 1;
        $user_room_info["room_sort"] = ($count + 1);
        $user_room_info["room_registtime"] = date("Y-m-d H:i:s");
        $user_room_info["is_one_time_meeting"] = 1;
        // 会議室作成
        $room_db->add($user_room_info);
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option_db = new OrderedServiceOptionTable($this->dsn);
        // オプション付
        foreach($user_option_info as $option){
            // 電話会議連携、MCUオプションは付けない
            if($option["service_option_key"] == 23 || $option["service_option_key"] == 24){
                continue;
            }
            $option_data = array(
                    "room_key" => $room_key,
                    "service_option_key" => $option["service_option_key"],
                    "ordered_service_option_status" => 1,
                    "ordered_service_option_starttime" => date("Y-m-d H:i:s"),
                    );
            $ordered_service_option_db->add($option_data);
        }
       //リレーションはり
        require_once("classes/mgm/dbi/relation.dbi.php");
        $relation_db = new MgmRelationTable(N2MY_MDB_DSN);
        $relation_data = array(
                "relation_key" => $room_key,
                "relation_type" => "mfp",
                "user_key" => $user_data["user_id"],
                "status" => "1",
                "create_datetime" => date("Y-m-d H:i:s"),
        );
        $relation_db->add($relation_data);



        return $room_key;
    }
    //}}}
}
