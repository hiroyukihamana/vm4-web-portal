<?php
/*
 * Created on 2009/07/29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("lib/EZLib/EZCore/EZLogger2.class.php");
require_once('lib/EZLib/EZCore/EZConfig.class.php');
require_once("classes/mcu/config/McuConfigProxy.php");

class N2MY_IvesSipClient
{
    var $logger2 = "";
    var $client  = null;
    var $guestClient	= null;
    var $config  = null;
    private $configProxy = null;
    private $_dsn;

    function N2MY_IvesSipClient($dsn){
        $this->logger2 = & EZLogger2::getInstance();

        $this->config       = EZConfig::getInstance(N2MY_CONFIG_FILE);
        $this->configProxy  = new McuConfigProxy();
        $wsdl               = $this->configProxy->get("sipWsdl");
        $this->client       = new SoapClient($wsdl);
        $this->_dsn		    = $dsn;
    }

    public function createSubscriber($subscriber) {
        try {
            $result = $this->client->createSubscriber($subscriber);

        } catch (SoapFault $fault) {
            throw $fault;
        }

        return $result;
    }

    public function getSipInformation($uId) {
        try {
            $params = array("uid" => $uId);
            $result = $this->client->getSipInformation($params);

        } catch (SoapFault $fault) {
            throw $fault;
        }

        return $result;
    }

    public function deleteSubscriber($uId) {
        try {
            $params = array("uid" => $uId);
            $result = $this->client->deleteSubscriber($params);

        } catch (SoapFault $fault) {
            throw $fault;
        }

        return $result;
    }
}
