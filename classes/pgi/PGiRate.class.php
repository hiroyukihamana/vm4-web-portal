<?php
class PGiRate
{
    static function findConfigAll()
    {
        $rate_config = simplexml_load_file(N2MY_APP_DIR.'/config/pgi/rate.xml');
        if (!$rate_config) {
            throw('rate.xml file not found');
        }
        return $rate_config;
    }

    static function findGmConfigAll()
    {
        $rate_config = simplexml_load_file(N2MY_APP_DIR.'/config/pgi/rate_gm.xml');
        if (!$rate_config) {
            throw('rate.xml file not found');
        }
        return $rate_config;
    }
}
