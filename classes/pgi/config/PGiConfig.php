<?php
require_once("lib/EZLib/EZCore/EZConfig.class.php");

class PGiConfig {
	private static $singleton;
	private $ezconfig;
	var $_config = array();
	
	function __construct($config_file) {
		$this->ezconfig = EZConfig::getInstance(N2MY_CONFIG_FILE);
		
		//check config_file
		if (!file_exists($config_file)) {
			trigger_error("#config_file not found -> $config_file", E_USER_ERROR);
		}
		if (!is_readable($config_file)) {
			trigger_error("#config_file not readable -> $config_file", E_USER_ERROR);
		}
		//parse ini_file
		$config = parse_ini_file($config_file, true);
	
		//set config
		if ($config && $config["PGI"]) {
			foreach ($config["PGI"] as $key => $value) {
				if (!$this->ezconfig->get("PGI", $key))
					$this->ezconfig->set("PGI", $key, $value);
			}
		} else {
			trigger_error("#unable to parse_config_file -> $config_file", E_USER_ERROR);
		}
	}
	
	public static function getInstance() {
		if (PGiConfig::$singleton) {
			return PGiConfig::$singleton;
		}
		PGiConfig::$singleton = new PGiConfig(N2MY_PGI_CONFIG_FILE);
		return PGiConfig::$singleton;
	}
	
	public function get($group, $key, $default = null)
	{
		return $this->ezconfig->get($group, $key, $default);
	}
	
	public function getAll($group, $default = null)
	{
		return $this->ezconfig->getAll($group, $default);
	}
	
	public function getGroups($default = null)
	{
		return $this->ezconfig->getGroups($default);
	}
	
	public function getGroupKeys($group, $default = null)
	{
		return $this->ezconfig->getGroupKeys($group, $default);
	}
	
	public function set($group, $key, $value, $create = true)
	{
		$this->ezconfig->set($group, $key, $value, $create);
	}
	
	public function setAll($group, $pair, $create = true)
	{
		$this->ezconfig->setAll($group, $pair, $create);
	}
}
