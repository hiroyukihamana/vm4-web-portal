<?php

require_once("lib/EZLib/EZXML/EZXML.class.php");
require_once("lib/EZLib/EZUtil/EZUrl.class.php");
require_once("lib/EZLib/EZSession/EZSession.class.php");
require_once("config/config.inc.php");

require_once("classes/dbi/user.dbi.php");
require_once("classes/mgm/dbi/user.dbi.php");
require_once("classes/dbi/user_plan.dbi.php" );
require_once("classes/dbi/user_service_option.dbi.php" );
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/service.dbi.php");
require_once("classes/dbi/room_plan.dbi.php");
require_once("classes/dbi/ordered_service_option.dbi.php");
require_once("classes/mgm/dbi/service_option.dbi.php");
require_once("classes/mgm/dbi/relation.dbi.php");
require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/N2MY_DBI.class.php");           /* MTGVFOUR-2215 */
require_once("classes/dbi/sales_room_queue.dbi.php"); /* MTGVFOUR-2215 */
require_once ("classes/dbi/member.dbi.php");          /* MTGVFOUR-2215 */
require_once ("classes/dbi/one_user_queue.dbi.php");  /* MTGVFOUR-2335 */

class ONE_Account {

	var $logger = "";
	var $dsn = null;
	var $account_dsn = null;

	var $session =  "";
	private $obj_User = null;
	private $obj_MgmUser = null;
	private $obj_Room = null;
	private $obj_RoomPlan = null;
	private $obj_RoomSeat = null;
	private $obj_Service = null;
	private $obj_UserPlan = null;
	private $obj_UserServiceOption = null;
	private $obj_OrderedOption = null;

	// id_host_planの入室の人数
	private $id_host_plan_max_seat = 30;

	private $id_host_plan_str = "one_id_host";
	private $normal_plan_str = "one";

	/**
	 * コンストラクタ
	 */
	function __construct ($dsn) {
		$this->logger2 = & EZLogger2::getInstance();
		$this->session   = EZSession::getInstance();
		$this->dsn = $dsn;
		// DBI関連
		$this->obj_User          = new UserTable($this->dsn);
		$this->obj_MgmUser       = new MgmUserTable(N2MY_MDB_DSN);
		$this->obj_Room          = new RoomTable($this->dsn);
		$this->obj_RoomPlan      = new RoomPlanTable($this->dsn);
		$this->obj_Service       = new ServiceTable(N2MY_MDB_DSN);
		$this->obj_ServiceOption = new ServiceOptionTable(N2MY_MDB_DSN);
		$this->obj_UserPlan      = new UserPlanTable($this->dsn);
		$this->obj_UserServiceOption = new UserServiceOptionTable( $this->dsn );
		$this->obj_OrderedOption = new OrderedServiceOptionTable($this->dsn);
		$this->obj_SalesRoomQueue = new SalesRoomQueue($this->dsn); /* MTGVFOUR-2215 */
		$this->obj_member         = new MemberTable($this->dsn);    /* MTGVFOUR-2215 */
		$this->obj_OneUserQueue   = new OneUserQueue($this->dsn);   /* MTGVFOUR-2335 */
	}

    private function isIdHostPlan($plan_key){
        static $id_host_plan_keys = array();
        if(!$id_host_plan_keys){
            $id_host_plan_keys = $this->obj_Service->getIdHostPlanKeys();
        }
        return in_array($plan_key, $id_host_plan_keys);
    }

    /*
     * プランのタイプに違いがあるかのチェック
     * 4.10.1.0現在 Oneノーマルかid_host_planの2つ
     */
    private function planTypeCheck($now_plan_key , $new_plan_key){
        $now = $this->isIdHostPlan($now_plan_key);
        $new = $this->isIdHostPlan($new_plan_key);

        if($now == $new){
            return true;
        }
        return false;
    }

	function add_one_user($user_info, $n2my_session=null) {
        $result = array();
        //バリデーション
        $data = (array)$user_info["user"];
        $user_options = (array)$user_info["useroptions"];
        //$plan_service_key = $user_options["plan_service_key"];
        $check_data = $data;
        $check_data["plan_service_key"] = $user_options["plan_service_key"];
        $check_data["port"] = $user_options["port"];
        $check_data["room_count"] = $user_options["room_count"];
        $check_data["max_id"] = $user_options["max_id"];
        //$this->logger2->info($check_data);
        $err_obj = $this->user_data_validation($check_data,"add");
        if (EZValidator::isError($err_obj)) {
            $result["status"] = "panameter_error";
            $result["err_obj"] = $err_obj;
            return $result;
        }

        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $data["user_password"] = $data["user_password"]?$data["user_password"]:$this->create_id().rand(2,9);
        $data["user_admin_password"] = $data["user_admin_password"]?$data["user_admin_password"]:$this->create_id().rand(2,9);
        if($this->isIdHostPlan($user_options["plan_service_key"])){
            $data["account_plan"] = $this->id_host_plan_str;
        }else{
            $data["account_plan"] = $this->normal_plan_str;
        }
        //add to user table
        $user_add_data = array(
            "user_id" => $data["user_id"],
            "user_password" => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $data["user_password"]),
            "user_admin_password" => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $data["user_admin_password"]),
            "user_company_name" => $data["company"],
            "user_staff_lastname" => $data["contact_name"],
            "user_company_phone" => $data["contact_phone"],
            "user_staff_email" => $data["email"],
            "account_plan"     => $data["account_plan"],
            "country_key"    => $data["country_key"]?$data["country_key"]:"1",
            "user_status" => 1,
            "meeting_version" => "4.7.0.0",
            "invoice_flg" => $data["invoice"]!==null?$data["invoice"]:1,
            "max_room_count" => $user_options["room_count"]?$user_options["room_count"]:0,
            "max_member_count" => $user_options["max_id"]?$user_options["max_id"]:0,
            //"agency" => $data["agency"],
            "user_registtime" => date("Y-m-d H:i:s"),
            "user_expiredatetime" => $data["expiredatetime"]?$data["expiredatetime"]:"0000-00-00 00:00:00",
            "use_sales" => $data["use_sales"]?$data["use_sales"]:"1",
            "is_add_room" => 1,
            "is_one_time_meeting" => 1,
            "member_id_format_flg"=> 1,
        );
        $result = $this->obj_User->add($user_add_data);
        if (DB::isError($result)) {
            $this->logger->error($result->getUserInfo(),__FUNCTION__."#DB ERROR!",__FILE__,__LINE__);
            $result["status"] = "error";
            $result["massage"] = "add user failed";
            return $result;
        } else {
            $this->logger2->info($user_add_data, __FUNCTION__."#add user successful!",__FILE__,__LINE__);
        }
        //add to mgm user table
        $server_info = $this->session->get("server_info");
        $auth_data = array(
            "user_id"         => $data["user_id"],
            "server_key"      => $server_info["server_key"],
            "user_registtime" => date("Y-m-d H:i:s"),
        );
        $result = $this->obj_MgmUser->add( $auth_data );
        if (DB::isError($result)) {
            $this->logger->error($result->getUserInfo(),__FUNCTION__."#DB ERROR!",__FILE__,__LINE__);
            $result["status"]  = "error";
            $result["massage"] = "add auth user failed";
            return $result;
        } else {
            $this->logger2->info($auth_data, __FUNCTION__."#add auth successful!",__FILE__,__LINE__);
        }

        $where = "user_id='".addslashes($data["user_id"])."'";
        $user_info = $this->obj_User->getRow($where);
        $user_info["user_password"] = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_password"]);
        $user_info["user_admin_password"] = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_admin_password"]);
        $user_key = $user_info["user_key"];
        //set user plan
        $plan_service_info["service_key"] = $user_options["plan_service_key"];
        $plan_service_info["port"] = $user_options["port"];
        $this->set_user_plan($user_key, $plan_service_info);

        $result["status"] = "success";
        $result["user_info"] = $user_info;
        // id_host_planだったら初期の部屋数はリクエストの値
        if($this->isIdHostPlan($user_options["plan_service_key"])){
            $room_data["normal"] = $user_options["room_count"];
			$room_data["document"] = 0;
        }
        $this->add_room($user_key, $room_data);

        $this->callSetMcu($user_key, $n2my_session);
        return $result;
	}

	private function callSetMcu($user_key, $n2my_session){
	    //MCU対応
	    $nowPlanInfo = $this->obj_UserPlan->getRow("user_plan_status = 1 AND user_key = " . $user_key);
	    $planInfo = $this->obj_Service->getRow("service_key = " . $nowPlanInfo["service_key"]);
	    $url = N2MY_LOCAL_URL . "api/one/admin/user/?action_set_room_mcu_server";

	    $ch = curl_init();
	    $parameters = array(
	            "n2my_session"             => $n2my_session,
	            "user_key"                 => $user_key,
	            "ives_profile_id"          => $planInfo["use_trial_plan"] ? 321 : 323, //323タイプ：account
	            "ives_mcu_server_key"      => ONE_DEFAULT_MCU_SERVER,
	            "ives_use_hd_profile"      => 0,
	            "ives_use_active_speaker"  => 1,
	    );
	    $ch_options = array(
	            CURLOPT_URL            => $url,
	            CURLOPT_POST           => 1,
	            CURLOPT_POSTFIELDS     => $parameters,
	            CURLOPT_RETURNTRANSFER => 1,
	            CURLOPT_TIMEOUT        => 1,
	            CURLOPT_CONNECTTIMEOUT => 1,
	    );
	    curl_setopt_array($ch, $ch_options);
	    $ret = curl_exec($ch);
	    curl_close($ch);
	}

	function edit_one_user($user_info,$n2my_session=null) {
		$result = array();
		//バリデーション
		$data = (array)$user_info["user"];
		$user_options = (array)$user_info["useroptions"];
		//$plan_service_key = $user_options["plan_service_key"];
		$check_data = $data;
		$check_data["plan_service_key"] = $user_options["plan_service_key"];
		$check_data["port"] = $user_options["port"];
		$check_data["room_count"] = $user_options["room_count"];
		$check_data["max_id"] = $user_options["max_id"];
		//$this->logger2->info($check_data);
		$err_obj = $this->user_data_validation($check_data,"edit");
		if (EZValidator::isError($err_obj)) {
			$result["status"] = "panameter_error";
			$result["err_obj"] = $err_obj;
			return $result;
		}

		require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
		$user_add_data = array();
		if($data["user_password"]) {
			$user_add_data["user_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $data["user_password"]);
		}
		if($data["user_admin_password"]) {
			$user_add_data["user_admin_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $data["user_admin_password"]);
		}
		if($data["company"]) {
			$user_add_data["user_company_name"] = $data["company"];
		}
	    if($data["contact_name"]) {
			$user_add_data["user_staff_lastname"] = $data["contact_name"];
		}
		if($data["contact_phone"]) {
			$user_add_data["user_company_phone"] = $data["contact_phone"];
		}
		if($data["email"]) {
			$user_add_data["user_staff_email"] = $data["email"];
		}
		// id_host_planの処理
		if($user_options["room_count"]) {
			$user_add_data["max_room_count"] = $user_options["room_count"];
		}
		if($user_options["max_id"]) {
			$user_add_data["max_member_count"] = $user_options["max_id"];
		//MTGVFOUR-2433 id_host_plan でなけでば 0 に変更する
		} else {
			if(!$this->isIdHostPlan($user_options["plan_service_key"])) {
				$user_add_data["max_member_count"] = 0;
			}
		}
		if($user_options["port"]) {
		   $user_add_data["max_connect_participant"] =  $user_options["port"];
		}
		if($this->isIdHostPlan($user_options["plan_service_key"])){
            $user_add_data["account_plan"] = $this->id_host_plan_str;
        }else{
            $user_add_data["account_plan"] = $this->normal_plan_str;
        }


		if($user_add_data) {
			$user_where = "user_id='".addslashes($data["user_id"])."' AND user_delete_status = 0";
			$result = $this->obj_User->update($user_add_data,$user_where);
			if (DB::isError($result)) {
				$this->logger->error($result->getUserInfo(),__FUNCTION__."#DB ERROR!",__FILE__,__LINE__);
				$result["status"] = "error";
				$result["massage"] = "add user failed";
				return $result;
			} else {
				$this->logger2->info($user_add_data, __FUNCTION__."#update user successful!",__FILE__,__LINE__);
			}
		}
		$where = "user_id='".addslashes($data["user_id"])."'";
		$user_info = $this->obj_User->getRow($where);
		$user_key = $user_info["user_key"];

		$nowPlanWhere = sprintf( "user_key='%s' AND user_plan_status=1", $user_key );
		$nowPlanInfo = $this->obj_UserPlan->getRow( $nowPlanWhere );

		if($user_options["port"]) {
		    if($this->isIdHostPlan($user_options["plan_service_key"])){
		        $update_port = $this->id_host_plan_max_seat;
		    }else{
		        $update_port = $user_options["port"];
		    }
			$this->update_port($user_key, $update_port);
		}
		//one_user_queue用に先にプランチェック
		$plan_type_check = $this->planTypeCheck($nowPlanInfo["service_key"] , $user_options["plan_service_key"]);
		if($user_options["plan_service_key"]) {
			$this->update_user_plan($user_key, $user_options["plan_service_key"],$plan_type_check);
		}

        // id_host_plan から oneのnomal、oneのノーマルからid_host_planに切り替えの場合全部屋削除し、デフォルト部屋のみにする
		if(!$plan_type_check){
            // 部屋削除
            $this->deletemeetingRoom($user_key);
            // デフォルト部屋作成部屋
            if($this->isIdHostPlan($user_options["plan_service_key"])){
                $room_data["normal"] = $user_options["room_count"];
                $room_data["document"] = 0;
            }
            $this->add_room($user_key, $room_data);
            $this->callSetMcu($user_key, $n2my_session);
		}

		return $result;
	}

	function user_data_validation($user_data, $operation_type) {
		//validation
		//get one plan list
		$service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
		$one_plan_list = $service_db->getCol("use_one_plan = '1' AND service_status = '1'", "service_key");
		//$this->logger2->info($user_data);
		$err_obj = new EZValidator($user_data);

		// id_host_plan対応
		$room_count_rule = array (
			                "minval" => 0,
			                "numeric" => "true"
			        );
		if($this->isIdHostPlan($user_data["plan_service_key"])){
		    $room_count_rule = array_merge($room_count_rule , array("required" => "true"));
		}

		if($operation_type == "add") {
			$rules = array(
					"user_id" => array (
							"required" => "true",
							"regex" => "/^[[:alnum:]._-]{3,30}$/"
					),
					"user_password" => array (
							"regex" => "/^(?=.*[\d])(?=.*[a-zA-Z]).*$/",
							"minlen" => 8,
							"maxlen" => 16
					),
					"user_admin_password" => array (
							"regex" => "/^(?=.*[\d])(?=.*[a-zA-Z]).*$/",
							"minlen" => 8,
							"maxlen" => 16
					),
					"company" => array (
							"required" => "true",
							// "minlen" => 3,
							"maxlen" => 255
					),
					"contact_name" => array (
							"required" => "true",
							// "minlen" => 3,
							"maxlen" => 255
					),
					"contact_phone" => array (
							"required" => "true",
							"numeric" => "true",
							"maxlen" => 255
					),
					"email" => array (
							"email" => "true",
							"maxlen" => 255
					),
					"invoice" => array (
							"allow" => array (
									0,
									1
							)
					),
					"plan_service_key" => array (
							"required" => "true",
							"allow" => $one_plan_list
					),
					"port" => array (
							"minval" => 0,
							"numeric" => "true"
					),
			        "room_count" => $room_count_rule,
			        "max_id" => $room_count_rule,
			);
			foreach($rules as $field => $rule) {
				$err_obj->check($field, $rule);
			}
			$user_where = "user_id='".addslashes($user_data["user_id"])."' AND user_delete_status = 0";
			$mgm_user_where = "user_id='".addslashes($user_data["user_id"])."'";
			if($this->obj_User->numRows($user_where) > 0 || $this->obj_MgmUser->numRows($mgm_user_where) > 0)
				$err_obj->set_error("user_id", "exist", $user_data["user_id"]);
		}
		if($operation_type == "edit") {
			$user_where = "user_id='".addslashes($user_data["user_id"])."' AND user_delete_status = 0";
			$mgm_user_where = "user_id='".addslashes($user_data["user_id"])."'";
			if($this->obj_User->numRows($user_where) == 0 || $this->obj_MgmUser->numRows($mgm_user_where) == 0)
				$err_obj->set_error("user_id", "don't exist", $user_data["user_id"]);
			$rules = array(
					"user_password"			=> array(
							"regex" => "/^(?=.*[\d])(?=.*[a-zA-Z]).*$/",
							"minlen" => 8,
							"maxlen" => 16),
					"user_admin_password"	=> array(
							"regex" => "/^(?=.*[\d])(?=.*[a-zA-Z]).*$/",
							"minlen" => 8,
							"maxlen" => 16),
					"company"               => array(
							"maxlen" => 255),
					"contact_name"          => array(
							"maxlen" => 255),
					"contact_phone"         => array(
							"numeric" => "true",
							"maxlen" => 255),
					"email"                  => array(
							"email" => "true",
							"maxlen" => 255),
					"plan_service_key"       => array(
							"allow" => $one_plan_list),
					"port"                   => array(
							"minval" => 0,
							"numeric" => "true",),
			        "room_count" => $room_count_rule,
			        "max_id" => $room_count_rule,
			);
			foreach($rules as $field => $rule) {
				$err_obj->check($field, $rule);
			}
		}


		return $err_obj;
	}

	function update_port($user_key,$port) {

		$update_info["max_seat"] = $port;
		$update_info["max_whiteboard_seat"] = $port;
		$update_info["active_speaker_mode_only_flg"] = 1;
		if($port >= 50) {
			$update_info["max_seat"] = 50;
			$update_info["max_whiteboard_seat"] = 50;
		}
		if($port<=12) {
			$update_info["active_speaker_mode_only_flg"] = 0;
		}
		//set user_room_setting
		require_once("classes/dbi/user_room_setting.dbi.php");
		$obj_UserRoomSetting = new UserRoomSettingTable($this->dsn);
		$where = sprintf( "status = 1 AND user_key=%s", $user_key );
		$now_UserRoomSetting = $obj_UserRoomSetting->getRow($where);
		$where = sprintf( "user_room_setting_key='%s'", $now_UserRoomSetting["user_room_setting_key"]);
		$nowUserRoomSettingKey = $obj_UserRoomSetting->update($update_info, $where);
		$this->logger2->info($now_UserRoomSetting);
		//既存部屋の設定更新
		$where = sprintf( "use_sales_option = 0 AND room_status = 1 AND user_key=%s", $user_key);
		$room_list = $this->obj_Room->getRowsAssoc($where,array(),null,0,"room_key,max_seat,max_whiteboard_seat,use_sales_option");
		//$this->logger2->info($room_list);
		if($room_list) {
			foreach($room_list as $room_info) {
				//$this->logger2->info($room_info);
				if($room_info["use_sales_option"]) {
					continue;
				}
				if($room_info["max_seat"]  == $now_UserRoomSetting["max_seat"] && $room_info["max_whiteboard_seat"]  == $now_UserRoomSetting["max_whiteboard_seat"]) {
					$where_room = "room_key = '".$room_info["room_key"]."'";
					$this->obj_Room->update($update_info, $where_room);
				}
			}
		}

	}

	function set_user_plan($user_key, $user_plan_info) {
		//now plan stop
		$where = sprintf( "user_key='%s' AND user_plan_status=1", $user_key );
		$nowPlanInfo = $this->obj_UserPlan->getRow( $where );
		if($nowPlanInfo) {
			$userPlanData = array(
					"user_plan_status"        => 0,
					"user_plan_updatetime"    => date( "Y-m-d H:i:s"),
			);
			$where = sprintf( "user_plan_key='%s'", $nowPlanInfo["user_plan_key"] );
			$this->obj_UserPlan->update( $userPlanData, $where );
		}
		//add new plan
		$userPlanData = array(
				"user_key"              => $user_key,
				"service_key"           => $user_plan_info["service_key"],
				"user_plan_starttime"   => $user_plan_info["user_plan_starttime"]?$user_plan_info["user_plan_starttime"]:date( "Y-m-d 00:00:00"),
				"user_plan_endtime"     => $user_plan_info["user_plan_endtime"]?$user_plan_info["user_plan_endtime"] : "0000-00-00 00:00:00",
				"discount_rate"         => $user_plan_info["discount_rate"]?$user_plan_info["discount_rate"]:0,
				"contract_month_number" => $user_plan_info["contract_month_number"]?$user_plan_info["contract_month_number"]:0,
				"user_plan_registtime"  => date( "Y-m-d H:i:s"),
				"user_plan_status"      => 1
		);
// 		if($request["account_model"] == "sales") {
// 			$userPlanData["sales_plan_flg"] = 1;
// 		}
        //$this->logger2->info($userPlanData);
		$newUserPlanKey = $this->obj_UserPlan->add( $userPlanData );

		$where_user_service = sprintf( "service_key='%s'", $user_plan_info["service_key"] );
		$new_plan_info = $this->obj_Service->getRow($where_user_service);
		$new_plan_info["port"] = $user_plan_info["port"]?$user_plan_info["port"]:$new_plan_info["port"];
		//update user info

		$plan_max_seat = $new_plan_info["max_seat"];
		$plan_max_whiteboard_seat = $new_plan_info["max_whiteboard_seat"];

		$update_user_data = array(
		        "max_storage_size"		=> $new_plan_info["max_storage_size"],
				"lang_allow"			=> $new_plan_info["lang_allow"],
				"use_port_plan"			=> $new_plan_info["port"] > 0 ? 1:0,
				"max_connect_participant" => $new_plan_info["port"] > 0 ? $new_plan_info["port"]:0,
		);
		if($new_plan_info["use_trial_plan"]) {
			$update_user_data["invoice_flg"] = 0;
			$update_user_data["user_status"] = 2;
		}
		//$this->logger2->info($update_user_data,"update user info");
		$this->obj_User->update($update_user_data, sprintf( "user_key='%s'", $user_key));

		//set user_room_setting
		require_once("classes/dbi/user_room_setting.dbi.php");
		$obj_UserRoomSetting = new UserRoomSettingTable($this->dsn);
		$where = sprintf( "status = 1 AND user_key=%s", $user_key );
		if($now_UserRoomSetting = $obj_UserRoomSetting->getRow($where)) {
			$delete_UserRoomSetting = array(
					"status"        => 0,
					"delete_datetime"    => date( "Y-m-d H:i:s"),
			);
			$where = sprintf( "user_room_setting_key='%s'", $now_UserRoomSetting["user_room_setting_key"] );
			$this->obj_UserPlan->update( $delete_UserRoomSetting, $where );
		}

		if($new_plan_info["port"] >= 50) {
			$new_plan_info["max_seat"] = 50;
			$new_plan_info["max_whiteboard_seat"] = 50;
		} elseif($new_plan_info["port"] > 0) {
			$new_plan_info["max_seat"] = $new_plan_info["port"];
			$new_plan_info["max_whiteboard_seat"] = $new_plan_info["port"];
			if($new_plan_info["port"]<=12)
				$new_plan_info["active_speaker_mode_only_flg"] = 0;
		}

		//id_host_planだったら強制的に30人に変更
		if($this->isIdHostPlan($user_plan_info["service_key"])){
		    $new_plan_info["max_seat"] =$plan_max_seat;
		    $new_plan_info["max_whiteboard_seat"] = $plan_max_whiteboard_seat;
		    $new_plan_info["active_speaker_mode_only_flg"] = 1;
		}

		$add_UserRoomSetting = array(
				"max_seat"					=> $new_plan_info["max_seat"] ? $new_plan_info["max_seat"] : 10,
				"max_audience_seat" 		=> $new_plan_info["max_audience_seat"] ? $new_plan_info["max_audience_seat"] : 0,
				"max_whiteboard_seat" 		=> $new_plan_info["max_whiteboard_seat"] ? $new_plan_info["max_whiteboard_seat"] : 0,
				"max_guest_seat" 			=> $new_plan_info["max_guest_seat"] ? $new_plan_info["max_guest_seat"] : 0,
				"max_guest_seat_flg" 		=> $new_plan_info["max_guest_seat_flg"] ? 1 : 0,
				"extend_max_seat" 			=> $new_plan_info["extend_max_seat"] ? $new_plan_info["extend_max_seat"] : 0,
				"extend_max_audience_seat"	=> $new_plan_info["extend_max_audience_seat"] ? $new_plan_info["extend_max_audience_seat"] : 0,
				"extend_seat_flg"			=> $new_plan_info["extend_seat_flg"] ? 1 : 0,
				"user_key"					=> $user_key,
				"room_name"					=> "",
				"status"					=> 1,
				"max_room_bandwidth"		=> $new_plan_info["max_room_bandwidth"],
				"max_user_bandwidth"		=> $new_plan_info["max_user_bandwidth"],
				"min_user_bandwidth"		=> $new_plan_info["min_user_bandwidth"],
				"default_camera_size"		=> $new_plan_info["default_camera_size"],
				"disable_rec_flg"			=> $new_plan_info["disable_rec_flg"],
				"hd_flg"					=> $new_plan_info["hd_flg"],
				"whiteboard_page"			=> $new_plan_info["whiteboard_page"],
				"whiteboard_size"			=> $new_plan_info["whiteboard_size"],
				"cabinet_filetype" 			=> $new_plan_info["cabinet_filetype"],
				"active_speaker_mode_only_flg"   => $new_plan_info["active_speaker_mode_only_flg"]? 1 : 0,
				"active_speaker_mode_use_flg" 	 => $new_plan_info["active_speaker_flg"]? 1 : 0,
				"active_speaker_mode_user_count" => $new_plan_info["active_speaker_user_count"]? $new_plan_info["active_speaker_user_count"] : 0,
				"use_teleconf" 					=> $new_plan_info["teleconference"]? 1 : 0,
				"use_pgi_dialin" 				=> $new_plan_info["teleconference"]? 1 : 0,
				"use_pgi_dialin_free" 			=> $new_plan_info["teleconference"]? 1 : 0,
				"use_pgi_dialin_lo_call" 		=> $new_plan_info["teleconference"]? 1 : 0,
				"use_pgi_dialout" 				=> $new_plan_info["teleconference"]? 1 : 0,
				"meeting_limit_time" 		=> $new_plan_info["meeting_limit_time"],
				"transceiver_number"		=> 0,
				"use_extend_bandwidth"		=> NULL,
				"rtmp_protocol"				=> "",
				"create_datetime"			=> date( "Y-m-d H:i:s"),
		);
		//$this->logger2->info($add_UserRoomSetting,"user room setting");
		$nowUserRoomSettingKey = $obj_UserRoomSetting->add($add_UserRoomSetting);

		//既存部屋の設定更新
        $where = sprintf( "room_status = 1 AND user_key=%s", $user_key);
        $room_list = $this->obj_Room->getRowsAssoc($where,"","",0,"room_key");
		//$this->logger2->info($room_list);
		if($room_list) {
			$room_data = array (
					"max_seat"              		=> ($new_plan_info["max_seat"]) ? $new_plan_info["max_seat"] : 10,
					"max_audience_seat"     		=> ($new_plan_info["max_audience_seat"]) ? $new_plan_info["max_audience_seat"] : 0,
					"max_whiteboard_seat"   		=> ($new_plan_info["max_whiteboard_seat"]) ? $new_plan_info["max_whiteboard_seat"] : 0,
					"max_guest_seat"       			=> ($new_plan_info["max_guest_seat_flg"]) ? $new_plan_info["max_guest_seat"] : 0,
					"max_guest_seat_flg"       		=> ($new_plan_info["max_guest_seat_flg"]) ? 1 : 0,
					"extend_seat_flg"       		=> ($new_plan_info["extend_seat_flg"]) ? 1 : 0,
					"extend_max_seat"       		=> ($new_plan_info["extend_seat_flg"]) ? $new_plan_info["extend_max_seat"] : 0,
					"extend_max_audience_seat" 		=> ($new_plan_info["extend_seat_flg"]) ? $new_plan_info["extend_max_audience_seat"] : 0,
					"meeting_limit_time"    		=> $new_plan_info["meeting_limit_time"] ? $new_plan_info["meeting_limit_time"] : 0,
					"default_camera_size"   		=> ($new_plan_info["default_camera_size"]) ? $new_plan_info["default_camera_size"] : "",
					"hd_flg"                		=> $new_plan_info["hd_flg"] ? 1 : 0,
					"disable_rec_flg"       		=> $new_plan_info["disable_rec_flg"] ? 1 : 0,
					"max_room_bandwidth"			=> $new_plan_info["max_room_bandwidth"]? $new_plan_info["max_room_bandwidth"] : 2048,
					"max_user_bandwidth"			=> $new_plan_info["max_user_bandwidth"]? $new_plan_info["max_user_bandwidth"] : 256,
					"min_user_bandwidth"			=> $new_plan_info["min_user_bandwidth"]? $new_plan_info["min_user_bandwidth"] : 30,
					"whiteboard_page"				=> $new_plan_info["whiteboard_page"] ? $new_plan_info["whiteboard_page"] : 30,
					"whiteboard_size"				=> $new_plan_info["whiteboard_size"] ? $new_plan_info["whiteboard_size"] : 30,
					"cabinet_filetype" 				=> $new_plan_info["cabinet_filetype"]? $new_plan_info["cabinet_filetype"] : "",
					"active_speaker_mode_use_flg" 	=> $new_plan_info["active_speaker_flg"]? 1 : 0,
					"active_speaker_mode_user_count" => $new_plan_info["active_speaker_user_count"]? $new_plan_info["active_speaker_user_count"] : 0,
					"use_teleconf" 					=> $new_plan_info["teleconference"]? 1 : 0,
					"use_pgi_dialin" 				=> $new_plan_info["teleconference"]? 1 : 0,
					"use_pgi_dialin_free" 			=> $new_plan_info["teleconference"]? 1 : 0,
					"use_pgi_dialin_lo_call" 		=> $new_plan_info["teleconference"]? 1 : 0,
					"use_pgi_dialout" 				=> $new_plan_info["teleconference"]? 1 : 0,
			);
			foreach($room_list as $room_key) {
				$where_room = "room_key = '".$room_key["room_key"]."'";
				$this->obj_Room->update($room_data, $where_room);
			}
		}
		//change user option
		//登録されているオプションを全て破棄
		$option_reset = array(
				'user_service_option_status' => 0,
				'user_service_option_deletetime' => date('Y-m-d H:i:s')
		);
		$where = "user_key = '".addslashes($user_key)."'" .
				" AND user_service_option_status = 1";
		$this->obj_UserServiceOption->update($option_reset, $where);
		//部屋のオプションも全て破棄
		$add_options = array();
		if ($new_plan_info["meeting_ssl"] == 1) {
			$add_options[] = "2";
		}
		if ($new_plan_info["desktop_share"] == 1) {
			$add_options[] = "3";
		}
		if ($new_plan_info["high_quality"] == 1) {
			$add_options[] = "5";
		}
		if ($new_plan_info["mobile_phone"] > 0) {
			$add_options[] = "6";
		}
		if ($new_plan_info["h323_client"] > 0) {
			$add_options[] = "8";
		}
		//hdd_extentionは数値分登録
		if ($new_plan_info["hdd_extention"] > 0) {
			$add_options[] = "4";
		}
		// ホワイトボードプラン
		if ($new_plan_info["whiteboard"] == 1) {
			$add_options[] = "16";
		}
		// マルチカメラ
		if ($new_plan_info["multicamera"] == 1) {
			$add_options[] = "18";
		}
		// 電話連携
		if ($new_plan_info["telephone"] == 1) {
			$add_options[] = "19";
		}
		// 録画GW
		if ($new_plan_info["record_gw"] == 1) {
			$add_options[] = "20";
		}
		// スマートフォン
		if ($new_plan_info["smartphone"] == 1) {
			$add_options[] = "21";
		}
		// 資料共有映像再生許可
		if ($new_plan_info["whiteboard_video"] == 1) {
			$add_options[] = "22";
		}
		// PGi連携
		if ($new_plan_info["teleconference"] == 1) {
			$add_options[] = "23";
		}
		// 最低帯域アップ
		if ($new_plan_info["video_conference"] == 1) {
			$add_options[] = "24";
		}
		// 最低帯域アップ
		if ($new_plan_info["minimumBandwidth80"] == 1) {
			$add_options[] = "25";
		}
		// h264
		if ($new_plan_info["h264"] == 1) {
			$add_options[] = "26";
		}
		// global_link
		if ($new_plan_info["global_link"] == 1) {
			$add_options[] = "30";
		}
		foreach ($add_options as $option_key) {
			$data = array(
					"service_option_key"            => $option_key,
					"user_service_option_starttime" => $user_plan_info["user_plan_starttime"]?$user_plan_info["user_plan_starttime"]:date( "Y-m-d 00:00:00"),
			);
			$this->add_user_option($user_key, $data);
			//PGiオプション対応
			if ($data["service_option_key"] == "23") {
				if($new_plan_info["pgi_setting_key"])
					$this->set_use_pgi_plan($user_key,$new_plan_info["pgi_setting_key"]);
				else
					$this->logger2->error($new_plan_info,"PGiプランを設定してない。");
			}
		}

	}

	function update_user_plan ($user_key, $service_plan_key , $plan_type_check = null){
		$where = sprintf( "user_key='%s' AND user_plan_status=1", $user_key );
		$nowPlanInfo = $this->obj_UserPlan->getRow( $where );
		// プラン変更なしならなにもしない
		if($nowPlanInfo["service_key"] == $service_plan_key){
		    return;
		}
		$where_user_service = sprintf( "service_key='%s'", $nowPlanInfo["service_key"] );
		$now_plan_info = $this->obj_Service->getRow($where_user_service);
		if($nowPlanInfo) {
			$userPlanData = array(
					"user_plan_status"        => 0,
					"user_plan_updatetime"    => date( "Y-m-d H:i:s"),
			);
			$where = sprintf( "user_plan_key='%s'", $nowPlanInfo["user_plan_key"] );
			$this->obj_UserPlan->update( $userPlanData, $where );
		}
		//add new plan
		$userPlanData = array(
				"user_key"              => $user_key,
				"service_key"           => $service_plan_key,
				"user_plan_starttime"   => date( "Y-m-d 00:00:00"),
				"user_plan_endtime"     => "0000-00-00 00:00:00",
				"discount_rate"         => 0,
				"contract_month_number" => 0,
				"user_plan_registtime"  => date( "Y-m-d H:i:s"),
				"user_plan_status"      => 1
		);
		$newUserPlanKey = $this->obj_UserPlan->add( $userPlanData );

		$where_user_service = sprintf( "service_key='%s'", $service_plan_key );
		$new_plan_info = $this->obj_Service->getRow($where_user_service);
		//update user info

		$update_user_data = array(
				"max_storage_size"		=> $new_plan_info["max_storage_size"],
				"lang_allow"			=> $new_plan_info["lang_allow"],
		);
		if($new_plan_info["use_trial_plan"]) {
			$update_user_data["invoice_flg"] = 0;
			$update_user_data["user_status"] = 2;
		} else {
			$update_user_data["invoice_flg"] = 1;
			$update_user_data["user_status"] = 1;
		}
		//$this->logger2->info($update_user_data,"update user info");
		$this->obj_User->update($update_user_data, sprintf( "user_key='%s'", $user_key));

		//set user_room_setting
		require_once("classes/dbi/user_room_setting.dbi.php");
		$obj_UserRoomSetting = new UserRoomSettingTable($this->dsn);
		$where = sprintf( "status = 1 AND user_key=%s", $user_key );
		$now_UserRoomSetting = $obj_UserRoomSetting->getRow($where);

		$update_UserRoomSetting = array(
				"max_guest_seat" 			=> $new_plan_info["max_guest_seat"] ? $new_plan_info["max_guest_seat"] : 0,
				"max_guest_seat_flg" 		=> $new_plan_info["max_guest_seat_flg"] ? 1 : 0,
				"extend_max_seat" 			=> $new_plan_info["extend_max_seat"] ? $new_plan_info["extend_max_seat"] : 0,
				"extend_max_audience_seat"	=> $new_plan_info["extend_max_audience_seat"] ? $new_plan_info["extend_max_audience_seat"] : 0,
				"extend_seat_flg"			=> $new_plan_info["extend_seat_flg"] ? 1 : 0,
				"room_name"					=> "",
				"max_room_bandwidth"		=> $new_plan_info["max_room_bandwidth"],
				"max_user_bandwidth"		=> $new_plan_info["max_user_bandwidth"],
				"min_user_bandwidth"		=> $new_plan_info["min_user_bandwidth"],
				"default_camera_size"		=> $new_plan_info["default_camera_size"],
				"disable_rec_flg"			=> $new_plan_info["disable_rec_flg"],
				"hd_flg"					=> $new_plan_info["hd_flg"],
				"whiteboard_page"			=> $new_plan_info["whiteboard_page"],
				"whiteboard_size"			=> $new_plan_info["whiteboard_size"],
				"cabinet_filetype" 			=> $new_plan_info["cabinet_filetype"],
				"active_speaker_mode_use_flg" 	 => $new_plan_info["active_speaker_flg"]? 1 : 0,
				"active_speaker_mode_user_count" => $new_plan_info["active_speaker_user_count"]? $new_plan_info["active_speaker_user_count"] : 0,
				"use_teleconf" 					=> $new_plan_info["teleconference"]? 1 : 0,
				"use_pgi_dialin" 				=> $new_plan_info["teleconference"]? 1 : 0,
				"use_pgi_dialin_free" 			=> $new_plan_info["teleconference"]? 1 : 0,
				"use_pgi_dialin_lo_call" 		=> $new_plan_info["teleconference"]? 1 : 0,
				"use_pgi_dialout" 				=> $new_plan_info["teleconference"]? 1 : 0,
				"meeting_limit_time" 		=> $new_plan_info["meeting_limit_time"],
				"update_datetime"			=> date( "Y-m-d H:i:s"),
		);
		//$this->logger2->info($add_UserRoomSetting,"user room setting");
		$where = sprintf( "user_room_setting_key='%s'", $now_UserRoomSetting["user_room_setting_key"]);
		$nowUserRoomSettingKey = $obj_UserRoomSetting->update($update_UserRoomSetting, $where);

        // MTGVFOUR-2335 one_user_queue に キューをためる場合
        $config = EZConfig::getInstance();
        //キュー格納設定かつ、デフォルト部屋作成なしの場合
        if($config->get("N2MY","is_one_user_queue") == 1 && $plan_type_check===true) {
            $this->logger2->info("Update One User Plan Add Queue. User_Key=" . $user_key);
            $ret = $this->userAddQueue($user_key,0);
            return;
        }

		//既存部屋の設定更新
        $where = sprintf( "is_one_time_meeting = 0 AND use_sales_option = 0 AND room_status = 1 AND user_key=%s", $user_key );
		$room_list = $this->obj_Room->getRowsAssoc($where);
		//$this->logger2->info($room_list);
// 		if($room_list) {
// 			$room_data = array (
// 					"max_guest_seat"       			=> ($new_plan_info["max_guest_seat_flg"]) ? $new_plan_info["max_guest_seat"] : 0,
// 					"max_guest_seat_flg"       		=> ($new_plan_info["max_guest_seat_flg"]) ? 1 : 0,
// 					"extend_seat_flg"       		=> ($new_plan_info["extend_seat_flg"]) ? 1 : 0,
// 					"extend_max_seat"       		=> ($new_plan_info["extend_seat_flg"]) ? $new_plan_info["extend_max_seat"] : 0,
// 					"extend_max_audience_seat" 		=> ($new_plan_info["extend_seat_flg"]) ? $new_plan_info["extend_max_audience_seat"] : 0,
// 					"meeting_limit_time"    		=> $new_plan_info["meeting_limit_time"] ? $new_plan_info["meeting_limit_time"] : 0,
// 					"default_camera_size"   		=> ($new_plan_info["default_camera_size"]) ? $new_plan_info["default_camera_size"] : "",
// 					"hd_flg"                		=> $new_plan_info["hd_flg"] ? 1 : 0,
// 					"disable_rec_flg"       		=> $new_plan_info["disable_rec_flg"] ? 1 : 0,
// 					"max_room_bandwidth"			=> $new_plan_info["max_room_bandwidth"]? $new_plan_info["max_room_bandwidth"] : 2048,
// 					"max_user_bandwidth"			=> $new_plan_info["max_user_bandwidth"]? $new_plan_info["max_user_bandwidth"] : 256,
// 					"min_user_bandwidth"			=> $new_plan_info["min_user_bandwidth"]? $new_plan_info["min_user_bandwidth"] : 30,
// 					"whiteboard_page"				=> $new_plan_info["whiteboard_page"] ? $new_plan_info["whiteboard_page"] : 30,
// 					"whiteboard_size"				=> $new_plan_info["whiteboard_size"] ? $new_plan_info["whiteboard_size"] : 30,
// 					"cabinet_filetype" 				=> $new_plan_info["cabinet_filetype"]? $new_plan_info["cabinet_filetype"] : "",
// 					"active_speaker_mode_use_flg" 	=> $new_plan_info["active_speaker_flg"]? 1 : 0,
// 					"active_speaker_mode_user_count" => $new_plan_info["active_speaker_user_count"]? $new_plan_info["active_speaker_user_count"] : 0,
// 					"use_teleconf" 					=> $new_plan_info["teleconference"]? 1 : 0,
// 					"use_pgi_dialin" 				=> $new_plan_info["teleconference"]? 1 : 0,
// 					"use_pgi_dialin_free" 			=> $new_plan_info["teleconference"]? 1 : 0,
// 					"use_pgi_dialin_lo_call" 		=> $new_plan_info["teleconference"]? 1 : 0,
// 					"use_pgi_dialout" 				=> $new_plan_info["teleconference"]? 1 : 0,
// 			);
// 			foreach($room_list as $room_key) {
// 				$where_room = "room_key = '".$room_key["room_key"]."'";
// 				$this->obj_Room->update($room_data, $where_room);
// 			}
// 		}

// 		//登録されているオプションを全て破棄
// 		$option_reset = array(
// 				'user_service_option_status' => 0,
// 				'user_service_option_deletetime' => date('Y-m-d H:i:s')
// 		);
// 		$where = "user_key = '".addslashes($user_key)."'" .
// 				" AND user_service_option_status = 1";
// 		$this->obj_UserServiceOption->update($option_reset, $where);
// 		//PGiオプションも破棄

		$add_options = array();
		$delete_options = array();
		if ($new_plan_info["meeting_ssl"] - $now_plan_info["meeting_ssl"] == 1) {
			$add_options[] = "2";
		} else if ($new_plan_info["meeting_ssl"] - $now_plan_info["meeting_ssl"] == -1){
			$delete_options = "2";
		}
		if ($new_plan_info["desktop_share"] - $now_plan_info["desktop_share"] == 1) {
			$add_options[] = "3";
		} else if ($new_plan_info["desktop_share"] - $now_plan_info["desktop_share"] == -1) {
			$delete_options[] = "3";
		}
		if ($new_plan_info["high_quality"] - $now_plan_info["high_quality"] == 1) {
			$add_options[] = "5";
		} else if ($new_plan_info["high_quality"] - $now_plan_info["high_quality"] == -1) {
			$delete_options[] = "5";
		}
		if ($new_plan_info["mobile_phone"] > 0 && $now_plan_info["mobile_phone"] <= 0) {
			$add_options[] = "6";
		} else if ($new_plan_info["mobile_phone"] <= 0 && $now_plan_info["mobile_phone"] > 0) {
			$delete_options[] = "6";
		}
		if ($new_plan_info["h323_client"] > 0 && $now_plan_info["h323_client"] <= 0) {
			$add_options[] = "8";
		} else if ($new_plan_info["h323_client"] <= 0 && $now_plan_info["h323_client"] > 0) {
			$delete_options[] = "8";
		}
		//hdd_extentionは数値分登録
		if ($new_plan_info["hdd_extention"] > 0 && $now_plan_info["hdd_extention"] <= 0) {
			$add_options[] = "4";
		} else if ($new_plan_info["hdd_extention"] <= 0 && $now_plan_info["hdd_extention"] > 0) {
			$delete_options[] = "4";
		}
		// ホワイトボードプラン
		if ($new_plan_info["whiteboard"] - $now_plan_info["whiteboard"] == 1) {
			$add_options[] = "16";
		} else if ($new_plan_info["whiteboard"] - $now_plan_info["whiteboard"] == -1) {
			$delete_options[] = "16";
		}
		// マルチカメラ
		if ($new_plan_info["multicamera"] - $now_plan_info["multicamera"] == 1) {
			$add_options[] = "18";
		} else if ($new_plan_info["multicamera"] - $now_plan_info["multicamera"] == -1) {
			$delete_options[] = "18";
		}
		// 電話連携
		if ($new_plan_info["telephone"] - $now_plan_info["telephone"] == 1) {
			$add_options[] = "19";
		} else if ($new_plan_info["telephone"] - $now_plan_info["telephone"] == -1) {
			$delete_options[] = "19";
		}
		// 録画GW
		if ($new_plan_info["record_gw"] - $now_plan_info["record_gw"] == 1) {
			$add_options[] = "20";
		} else if ($new_plan_info["record_gw"] - $now_plan_info["record_gw"] == -1) {
			$delete_options[] = "20";
		}
		// スマートフォン
		if ($new_plan_info["smartphone"] - $now_plan_info["smartphone"] == 1) {
			$add_options[] = "21";
		} else if ($new_plan_info["smartphone"] - $now_plan_info["smartphone"] == -1) {
			$delete_options[] = "21";
		}
		// 資料共有映像再生許可
		if ($new_plan_info["whiteboard_video"] - $now_plan_info["whiteboard_video"] == 1) {
			$add_options[] = "22";
		} else if ($new_plan_info["whiteboard_video"] - $now_plan_info["whiteboard_video"] == -1) {
			$delete_options[] = "22";
		}
		// PGi連携
		if ($new_plan_info["teleconference"] - $now_plan_info["teleconference"] == 1) {
			$add_options[] = "23";
		} else if ($new_plan_info["teleconference"] - $now_plan_info["teleconference"] == -1) {
			$delete_options[] = "23";
		} else if($new_plan_info["pgi_setting_key"] != $now_plan_info["pgi_setting_key"]){
			$add_options[] = "23";
			$delete_options[] = "23";
		}
		// 最低帯域アップ
		if ($new_plan_info["video_conference"] - $now_plan_info["video_conference"] == 1) {
			$add_options[] = "24";
		} else if ($new_plan_info["video_conference"] - $now_plan_info["video_conference"] == -1) {
			$delete_options[] = "24";
		}
		// 最低帯域アップ
		if ($new_plan_info["minimumBandwidth80"] - $now_plan_info["minimumBandwidth80"] == 1) {
			$add_options[] = "25";
		} else if ($new_plan_info["minimumBandwidth80"] - $now_plan_info["minimumBandwidth80"] == -1) {
			$delete_options[] = "25";
		}
		// h264
		if ($new_plan_info["h264"] - $now_plan_info["h264"] == 1) {
			$add_options[] = "26";
		} else if ($new_plan_info["h264"] - $now_plan_info["h264"] == -1) {
			$delete_options[] = "26";
		}
		// global_link
		if ($new_plan_info["global_link"] - $now_plan_info["global_link"] == 1) {
			$add_options[] = "30";
		} else if ($new_plan_info["global_link"] - $now_plan_info["global_link"] == -1) {
			$delete_options[] = "30";
		}
		$this->logger2->info(array($option_reset, $where, $add_options, $delete_options));
		foreach ($delete_options as $option_key) {
			$option_reset = array(
					'user_service_option_status' => 0,
					'user_service_option_deletetime' => date('Y-m-d H:i:s')
			);
			$where = "user_key = '".addslashes($user_key)."'" .
					" AND service_option_key=".addslashes($option_key).
					" AND user_service_option_status = 1";
			$this->obj_UserServiceOption->update($option_reset, $where);
			if($room_list) {
				foreach ($room_list as $room_info) {
					if($room_info["use_sales_option"]) {
						continue;
					}
					$where = "room_key = '".addslashes($room_info["room_key"])."'" .
							" AND service_option_key=".addslashes($option_key).
							" AND ordered_service_option_status = 1";
					$option_reset = array(
							'ordered_service_option_status' => 0,
							'ordered_service_option_deletetime' => date('Y-m-d H:i:s')
					);
					$this->obj_OrderedOption->update($option_reset, $where);
				}
			}
			//PGiオプション削除
			if ($option_key == "23") {
				$this->delete_use_pgi_plan($user_key);
				if($room_list) {
					foreach ($room_list as $room_info) {
						if($room_info["use_sales_option"]) {
							continue;
						}
						$this->delete_default_room_pgi_setting($room_info["room_key"]);
						$update_room_teleconference = array (
								"use_teleconf"  		 => 0,
								"use_pgi_dialin" 		 => 0,
								"use_pgi_dialin_free" 	 => 0,
								"use_pgi_dialin_lo_call" => 0,
								"use_pgi_dialout" 		 => 0,
						);
						$where_room = "room_key = '".$room_info["room_key"]."'";
						$this->obj_Room->update($update_room_teleconference, $where_room);
					}
				}
			}
		}
		foreach ($add_options as $option_key) {
			$data = array(
					"service_option_key"            => $option_key,
					"user_service_option_starttime" => date( "Y-m-d 00:00:00"),
			);
			$user_service_option_key = $this->add_user_option($user_key, $data);
			//部屋プランも更新
			if($room_list) {
				foreach ($room_list as $room_info) {
					if($room_info["use_sales_option"]) {
						continue;
					}
					$data = array(
							"room_key"						    => $room_info["room_key"],
							"user_service_option_key"           => $user_service_option_key,
							"service_option_key"                => $option_key,
							"ordered_service_option_status"     => 1,
							"ordered_service_option_registtime" => date("Y-m-d H:i:s"),
							"ordered_service_option_starttime"  => date("Y-m-d H:i:s"),
					);
					$result = $this->obj_OrderedOption->add( $data );
				}
			}
			//PGiオプション対応
			if ($data["service_option_key"] == "23") {
				if($new_plan_info["pgi_setting_key"]) {
					$this->set_use_pgi_plan($user_key,$new_plan_info["pgi_setting_key"]);
					if($room_list) {
						foreach ($room_list as $room_info) {
							if($room_info["use_sales_option"]) {
								continue;
							}
							$this->set_default_room_pgi_setting($user_key, $room_info["room_key"]);
							$update_room_teleconference = array(
									"use_teleconf"  		 => 1,
									"use_pgi_dialin" 		 => 1,
									"use_pgi_dialin_free" 	 => 1,
									"use_pgi_dialin_lo_call" => 1,
									"use_pgi_dialout" 		 => 1,
							);
							$where_room = "room_key = '".$room_info["room_key"]."'";
							$this->obj_Room->update($update_room_teleconference, $where_room);
						}
					}
				}
				else
					$this->logger2->error($new_plan_info,"PGiプランを設定してない。");
			}

		}
		//トライアル対応
		if($new_plan_info["use_trial_plan"] != $now_plan_info["use_trial_plan"]) {
			foreach ($room_list as $room_info) {
				if($room_info["use_sales_option"]) {
					continue;
				}
				//delete old mcu
				$where = sprintf("room_key ='%s' AND ordered_service_option_status=1 AND service_option_key=16",$room_info["room_key"]);
				if(!$this->obj_OrderedOption->numRows($where)) {
					$mcu_data = array(
							//"ives_mcu_server_key" =>  ONE_DEFAULT_MCU_SERVER,
							"ives_profile_id" => $new_plan_info["use_trial_plan"]?321:323, //タイプ：demo
							//"ives_num_profiles" => $room_info["max_seat"]>20?20:$room_info["max_seat"],
							//"ives_use_active_speaker" => 1,
					);
					$this->logger2->info($mcu_data);
					$this->update_room_mcu_server($room_info["room_key"], $mcu_data);
				}
			}
		}
	}

	function set_use_pgi_plan($user_key, $pgi_key) {
		require_once('classes/pgi/PGiSystem.class.php');
		require_once("classes/pgi/PGiRate.class.php");
		$pgi_system = (array)PGiSystem::findBySystemKey($pgi_key);
		//$this->logger2->info($pgi_system);

		require_once("classes/dbi/user_pgi_setting.dbi.php");
		require_once("classes/dbi/pgi_rate.dbi.php");
		$pgiSettingTable = new UserPGiSettingTable($this->dsn);
		$pgiRateTable    = new N2MY_DB($this->dsn, "user_pgi_rate");
		$pgi_setting_data = array(
				'user_key'   => $user_key,
				'startdate'  => date("Y-m-01"),
				'system_key' => $pgi_system['key'],
				'company_id' => $pgi_system['defaultCompanyID'],
// 				'admin_client_id' => $pgi_system['adminClientID'],
// 				'admin_client_pw' => $pgi_system['adminClientPW'],
		);
		//$this->logger2->info($pgi_setting_data);
		try {
			$pgi_setting_key = $pgiSettingTable->add($pgi_setting_data);
		} catch (Exception $e) {
			return false;
		}
		if ($pgi_system['rates']) { // 設定にデフォルト料金が登録されているsystemはpgi_rateに料金を保存
			//$this->logger2->debug($validated['data']);
			$rate_config = (array)PGiRate::findGmConfigAll();
			$pgi_system_rates = (array)$pgi_system['rates'];
			foreach ($rate_config["ratePlan"] as $pgi_rate) {
				//$this->logger2->info($pgi_rate);
				$pgi_rate_data = array('user_pgi_setting_key' => $pgi_setting_key,
						'rate_type'       => (string)$pgi_rate->type,
						'rate'            => $pgi_system_rates[(string)$pgi_rate->type]);
				//$this->logger2->info($pgi_rate_data);
				try {
					$pgiRateTable->add($pgi_rate_data);
				} catch (Exception $e) {
					return false;
				}
			}
		}

	}

	function delete_use_pgi_plan($user_key) {
		require_once("classes/dbi/user_pgi_setting.dbi.php");
		require_once("classes/dbi/pgi_rate.dbi.php");
		$pgiSettingTable = new UserPGiSettingTable($this->dsn);
		$pgiRateTable    = new N2MY_DB($this->dsn, "user_pgi_rate");
		$where_pgiSetting = "is_deleted = 0 AND user_key=".$user_key;
		$now_pgiSetting = $pgiSettingTable->getRow($where_pgiSetting);

		$delete_pgi_setting_data = array(
				"is_deleted" => 1,
				"updatetime" => date("Y-m-d H:i:s"),
		);
		$pgiSettingTable->update($delete_pgi_setting_data, $where_pgiSetting);

		//Delete User PGi Rate
		$where_pgiRate = "is_deleted = 0 AND user_pgi_setting_key=".$now_pgiSetting["user_pgi_setting_key"];
		$delete_pgi_rate_data = array(
				"is_deleted" => 1,
				"updatetime" => date("Y-m-d H:i:s"),
		);
		$pgiRateTable->update($delete_pgi_rate_data, $where_pgiRate);
	}

	function add_user_option($user_key, $option_info) {
		$option_info["user_key"] = $user_key;
		if ( ! $option_info["user_service_option_starttime"] ) {
			$option_info["user_service_option_starttime"] = date("Y-m-d 00:00:00");
		}
		// sharing optionは一つだけ
		if( 3 == $option_info["service_option_key"] ){
			$where = sprintf( "service_option_key = 3 AND user_key=%s AND user_service_option_status=1", $user_key );
			if( $this->obj_UserServiceOption->numRows($where) > 0) {
				return false;
			}
		}
		$today = date("Y-m-d 23:59:59");
		$option_info["user_service_option_status"] = ( strtotime( $option_info["user_service_option_starttime"] ) >= strtotime( $today ) ) ? 2 : 1;
		$option_info["user_service_option_registtime"] = date("Y-m-d H:i:s");
		//$this->logger2->info($option_info);
		$result = $this->obj_UserServiceOption->add( $option_info );


		return $result;
		//部屋にもオプシンを紐つける
//		$where = sprintf( "room_status = 1 AND user_key=%s", $user_key);
//		$room_list = $this->obj_Room->getRowsAssoc($where,"","",0,"room_key");
		//$this->logger2->info($room_list);
// 		if($room_list) {
// 			$data = array(
// 					"user_service_option_key"           => $result,
// 					"service_option_key"                => $option_info["service_option_key"],
// 					"ordered_service_option_status"     => $option_info["user_service_option_status"],
// 					"ordered_service_option_registtime" => $option_info["user_service_option_registtime"],
// 					"ordered_service_option_starttime"  => $option_info["user_service_option_starttime"]
// 			);
// 			require_once("classes/dbi/pgi_setting.dbi.php");
// 			$pgi_setting_table = new PGiSettingTable($this->dsn);
// 			foreach($room_list as $room) {
// 				$data["room_key"] = $room["room_key"];
// 				$result = $objOrderedServiceOption->add( $data );
// 				if ($add_form["service_option_key"] == "23") {
// 					//PGiオプション対応
// 					// 				$pgi_settings  = $pgi_setting_table->findByRoomKey($room["room_key"]);
// 					// 				if (!$pgi_settings) {
// 					// 					$pgi_form = $this->_getDefaultPGI();
// 					// 					$this->action_pgi_setting($pgi_form,$room["room_key"],1);
// 					// 				}
// 					// 				$room_data = array("use_teleconf" => "1",
// 					// 						"use_pgi_dialin"      => "1",
// 					// 						"use_pgi_dialin_free" => "1",
// 					// 						"use_pgi_dialin_lo_call" => "1",
// 					// 						"use_pgi_dialout"     => "1");
// 					// 				$where_room = "room_key = '".addslashes($room["room_key"])."'";
// 					// 				$room_db = new N2MY_DB($this->get_dsn(), "room");
// 					// 				$room_data = $room_db->update($room_data, $where_room);
// 				}
// 			}
// 		}
	}

	function stop_user_option($user_key, $option_key) {

	}

	function add_room($user_key,$room_data) {

		$where = sprintf( "user_status >= 1 AND user_key=%s", $user_key );
		$user_info = $this->obj_User->getRow($where);
		require_once("classes/dbi/user_room_setting.dbi.php");
		$obj_UserRoomSetting = new UserRoomSettingTable($this->dsn);
		$where = sprintf( "status = 1 AND user_key=%s", $user_key );
		$now_UserRoomSetting = $obj_UserRoomSetting->getRow($where);
		$add_room_data = $now_UserRoomSetting;
		$room_name = $add_room_data["room_name"]?$add_room_data["room_name"]:"ROOM";
		$add_room_data["room_status"] = 1;
		$add_room_data["room_registtime"] = date("Y-m-d H:i:s");
		unset($add_room_data["user_room_setting_key"]);
		unset($add_room_data["create_datetime"]);
		unset($add_room_data["update_datetime"]);
		unset($add_room_data["delete_datetime"]);
		unset($add_room_data["expire_datetime"]);
		unset($add_room_data["status"]);
		unset($add_room_data["pgi_client_id"]);
		unset($add_room_data["pgi_client_pw"]);
		unset($add_room_data["is_device_check"]);

		if(!$room_data) {
			$room_data["normal"] = 3;
			$room_data["document"] = 1;
		}

		for ($i = 1 ; $i <= ($room_data["normal"] + $room_data["document"]) ;$i++) {
			$where_room = sprintf( "user_key=%s", $user_key);
			$_count = $this->obj_Room->numRows($where_room);
			$where_room = sprintf( "use_sales_option = 0 AND room_status = 1 AND user_key=%s", $user_key);
			$count = $this->obj_Room->numRows($where_room);
			$room_key = sprintf("%s-%s-%s",$user_info["user_id"],($_count + 1),substr(md5(uniqid(time(), true)), 0,4));
			$add_room_data["room_key"] = $room_key;
			$add_room_data["room_sort"] = $count+1;
			$add_room_data["room_name"] = $room_name.$add_room_data["room_sort"];

			if($i > $room_data["normal"]) {
				$add_room_data["room_name"] = "DOC".($add_room_data["room_sort"] - $room_data["normal"]);
				//$add_room_data["max_whiteboard_seat"] = $add_room_data["max_seat"];
				$add_room_data["active_speaker_mode_only_flg"] = 0;
				$add_room_data["active_speaker_mode_use_flg"] = 0;
				$add_room_data["disable_rec_flg"] = 1;
			}
			$ret = $this->obj_Room->add($add_room_data);
			if (DB::isError($ret)) {
				$this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
				return false;
			}

			$relation_db = new N2MY_DB(N2MY_MDB_DSN, "relation");
			$relation_data = array(
					"relation_key" => $room_key,
					"relation_type" => "mfp",
					"user_key" => $user_info["user_id"],
					"status" => "1",
					"create_datetime" => date("Y-m-d H:i:s"),
			);
			$ret = $relation_db->add($relation_data);
			if (DB::isError($ret)) {
				$this->logger2->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
				return false;
			}

			//add room option
			$where = sprintf( "user_key=%s AND user_service_option_status=1", $user_key );
			$optionList = $this->obj_UserServiceOption->getRowsAssoc( $where );
			//$this->logger2->info($optionList);
			if($optionList) {
				foreach ($optionList as $option) {
					$data = array(
						"room_key"						    => $room_key,
						"user_service_option_key"           => $option["user_service_option_key"],
						"service_option_key"                => $option["service_option_key"],
						"ordered_service_option_status"     => 1,
						"ordered_service_option_registtime" => date("Y-m-d H:i:s"),
						"ordered_service_option_starttime"  => date("Y-m-d H:i:s"),
					);
					$result = $this->obj_OrderedOption->add( $data );
					if ($option["service_option_key"] == "23") {
						$this->set_default_room_pgi_setting($user_key,$room_key);
					}
				}
			}
			if($i > $room_data["normal"]) {
				//ドキュメント部屋オプション
				$data = array(
					"room_key"						    => $room_key,
					"user_service_option_key"           => $option["user_service_option_key"],
					"service_option_key"                => 16,
					"ordered_service_option_status"     => 1,
					"ordered_service_option_registtime" => date("Y-m-d H:i:s"),
					"ordered_service_option_starttime"  => date("Y-m-d H:i:s"),
				);
				$result = $this->obj_OrderedOption->add( $data );
			}
		}
	}

    //V-CUBE One対応：会議室とメンバー紐付け機能追加（管理者＞会議室設定）
    function get_ONE_whitelist_room_list($user_key, $member_key, $delete_flag = false){
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->dsn);
        $rooms = $obj_N2MY_Account->getRoomList($user_key, $this->session->get("service_mode"));
        $obj_member_room_whitelist = new N2MY_DB($this->dsn, "member_room_whitelist");
        $where = "whitelist_status = 1 AND member_key = '" . mysql_real_escape_string($member_key) . "'";
        $whitelist_rooms = $obj_member_room_whitelist->getCol($where, "room_key");
        $room_list = array();
        foreach ($rooms as $room) {
            if($room["room_info"]["has_member_whitelist"] == 0 || in_array($room["room_info"]["room_key"], $whitelist_rooms) ){
                $room_list[$room["room_info"]["room_key"]] = $room;
            }
        }
        if($delete_flag){
            $obj_room = new N2MY_DB($this->dsn, "room");
            $deleted_room_list = $obj_room->getRowsAssoc("room_status = 0 AND use_sales_option != 1 AND user_key = " . $user_key, null, null, null, 'room_key,has_member_whitelist');
            if($deleted_room_list){
                foreach ($deleted_room_list as $deleted_room) {
                    if($deleted_room["has_member_whitelist"] == 0 || in_array($deleted_room["room_key"], $whitelist_rooms)){
                        $room_list[$deleted_room["room_key"]] = $obj_N2MY_Account->getRoomInfo($deleted_room["room_key"]);
                    }
                }
            }
        }

        return $room_list;
    }

    function get_reservation_list($options = array(), $user_timezone = null, $checkPortOverArray = null){
        if (empty($options["user_key"]) && empty($options["member_key"]) && !$room_key) {
            $this->logger->error(array($room_key, $options), "Parameter error!!");
            return false;
        }
        // 基本条件
        $cond = array();
        $cond[] = "reservation_status = 1";
        if (isset($options["user_key"])) {
            $cond[] = "user_key = '".mysql_real_escape_string($options["user_key"])."'";
        }
        if (isset($options["start_time"]) || isset($options["end_time"])) {
            $start_limit = $options["start_time"] ? mysql_real_escape_string($options["start_time"]) : "0000-00-00 00:00:00";
            $end_limit   = $options["end_time"] ? mysql_real_escape_string($options["end_time"]) : "9999-12-31 23:59:59";
            $cond[] = "(" .
                "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
        }
        if (isset($options["updatetime"])) {
            $updatetime = $options["updatetime"];
            $cond[] = "( reservation_registtime >= '$updatetime') OR ( reservation_updatetime >= '$updatetime' )";
        }
        if (isset($options["status"])) {
            switch($options["status"]) {
            case 'wait':
                $cond[] = "reservation_starttime > '".date('Y-m-d H:i:s')."'";
                break;
            case 'now':
                $cond[] = "'".date('Y-m-d H:i:s')."' BETWEEN reservation_starttime AND reservation_endtime";
                break;
            case 'end':
                $cond[] = "reservation_starttime < '".date('Y-m-d H:i:s')."'";
                break;
            case 'valid':
                $start_limit = date('Y-m-d H:i:s');
                $end_limit   = "9999-12-31 23:59:59";
                $cond[] = "(" .
                    "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                    " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                    " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                    " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
                break;
            }
        }
        if (isset($options["reservation_name"])) {
            $reservation_name = $options["reservation_name"];
            $cond[] = "reservation_name LIKE '%".addslashes($reservation_name)."%'";
        }
        $where = join(" AND ", $cond);
        // ユーザ全件
        if ($options["room_key"]) {
            $where .= " AND room_key = '".mysql_real_escape_string($options["room_key"])."'";
        }else{
            $session   = EZSession::getInstance();
            if($session->get("service_mode") == "sales") {
                $where .= " AND room_key IN (SELECT room.room_key FROM room WHERE room.use_sales_option=1 AND room.user_key ='".$options["user_key"]."')";
            } elseif ($session->get("service_mode") == "meeting") {
                require_once("classes/N2MY_Account.class.php");
                $obj_N2MY_Account = new N2MY_Account($this->dsn);
                $rooms = $obj_N2MY_Account->getRoomList($options["user_key"], "meeting");
                $obj_member_room_whitelist = new N2MY_DB($this->dsn, "member_room_whitelist");
                $whitelist_where = "whitelist_status = 1 AND member_key = '" . mysql_real_escape_string($options["member_key"]) . "'";
                $whitelist_rooms = $obj_member_room_whitelist->getCol($whitelist_where, "room_key");
                $room_list = array();
                foreach ($rooms as $room) {
                    if($room["room_info"]["has_member_whitelist"] == 0){
                        $room_list[] = $room["room_info"]["room_key"];
                    }else{
                        if(in_array($room["room_info"]["room_key"], $whitelist_rooms)){
                            $room_list[] = $room["room_info"]["room_key"];
                        }
                    }
                }
                if(!$room_list){
                    return array();
                }
                $where .= " AND room_key IN ('". join("','", $room_list) ."')";;
            }
        }
        $limit  = isset($options["limit"])  ? $options["limit"]  : null;
        $offset = isset($options["offset"]) ? $options["offset"] : null;
        if (isset($options["sort_key"])) {
            $sort = array($options["sort_key"] => $options["sort_type"]);
        } else {
            $sort = array();
        }
        require_once ("classes/dbi/reservation.dbi.php");
        $obj_Reservation     = new ReservationTable($this->dsn);
        $reservations = $obj_Reservation->getList($where, $sort, $limit, $offset);
        if (DB::isError($reservations)) {
            $this->logger->error($reservations->getUserInfo());
            return false;
        }
        $reservation_list = array();
        // 一覧表示
        foreach ($reservations as $reservation_info){
            if (strtotime($reservation_info['reservation_starttime']) < strtotime("now")
             && strtotime($reservation_info['reservation_endtime'])   > strtotime("now") ) {
                $status = "now"; // 開催中
            } elseif (strtotime($reservation_info['reservation_endtime']) < strtotime("now")) {
                $status = "end"; // 終了
            } else {
                $status = "wait";// 開催待
            }
            $reservation_info["status"] = $status;
            if ($user_timezone !== "") {
                $reservation_info["reservation_starttime"] = EZDate::getLocateTime($reservation_info["reservation_starttime"],
                                                                                   $user_timezone, N2MY_SERVER_TIMEZONE);
                $reservation_info["reservation_endtime"]   = EZDate::getLocateTime($reservation_info["reservation_endtime"],
                                                                                   $user_timezone, N2MY_SERVER_TIMEZONE);
            }
            // 契約人数を超えている会議をチェック
            if ($checkPortOverArray){
                foreach ($checkPortOverArray as $port_over_info){
                    if ($reservation_info['reservation_starttime'] <= $port_over_info
                    &&  $reservation_info['reservation_endtime']    > $port_over_info ) {
                        $reservation_info["port_over"] = "1";
                        break;
                    }
                }
            }
            $reservation_list[] = $reservation_info;
        }
        return $reservation_list;
    }

	function set_default_room_pgi_setting($user_key,$room_key) {

		require_once("classes/dbi/pgi_setting.dbi.php");
		require_once("classes/dbi/pgi_rate.dbi.php");
		$pgiSettingTable = new PGiSettingTable($this->dsn);
		$pgiRateTable    = new PGiRateTable($this->dsn);

		require_once("classes/dbi/user_pgi_setting.dbi.php");
		require_once("classes/dbi/pgi_rate.dbi.php");
		$userPGiSettingTable = new UserPGiSettingTable($this->dsn);
		$userPGiRateTable    = new N2MY_DB($this->dsn, "user_pgi_rate");

		$where_userPGiSetting = sprintf( "user_key=%s AND is_deleted = 0", $user_key );
		$userPGiSetting = $userPGiSettingTable->getRow($where_userPGiSetting);
		$pgi_setting_data = array('room_key'   => $room_key,
				'startdate'  => date("Y-m-01"),
				'system_key' => $userPGiSetting['system_key'],
				'company_id' => $userPGiSetting['company_id'],
				'client_id'  => $userPGiSetting['client_id'],
				'client_pw'  => $userPGiSetting['client_pw'],
				'admin_client_id' => $userPGiSetting['admin_client_id'],
				'admin_client_pw' => $userPGiSetting['admin_client_pw'],
				'registtime' => date("Y-m-d H:i:s"),
		);
		try {
			$pgi_setting_key = $pgiSettingTable->add($pgi_setting_data);
		} catch (Exception $e) {
			$this->logger2->error($pgi_setting_data,"Can not add PGi Setting for room");
			return false;
		}

		$where_userPGiRates = sprintf( "user_pgi_setting_key=%s AND is_deleted = 0", $userPGiSetting["user_pgi_setting_key"] );
		$userPGiRates= $userPGiRateTable->getRowsAssoc($where_userPGiRates);
		if($userPGiRates) {
			foreach($userPGiRates as $userPGiRate){
				$pgi_rate_data = array('pgi_setting_key' => $pgi_setting_key,
						'rate_type'       => $userPGiRate["rate_type"],
						'rate'            => $userPGiRate["rate"],
						'registtime' => date("Y-m-d H:i:s"),
				);
				try {
					$pgiRateTable->add($pgi_rate_data);
				} catch (Exception $e) {
					$this->logger2->error($pgi_rate_data,"Can not add PGi Rates for room");
			        return false;
				}
			}
		}

	}
	function delete_default_room_pgi_setting($room_key) {
		require_once("classes/dbi/pgi_setting.dbi.php");
		require_once("classes/dbi/pgi_rate.dbi.php");
		$pgiSettingTable = new PGiSettingTable($this->dsn);
		$pgiRateTable    = new PGiRateTable($this->dsn);
		$where_pgiSetting = "is_deleted = 0 AND room_key='".$room_key."'";
		$now_pgiSetting = $pgiSettingTable->getRow($where_pgiSetting);
	    if(!$now_pgiSetting) {
	    	$this->logger2->warn($where_pgiSetting,"no active plan for deleting");
	        return false;
	    }
		$delete_pgi_setting_data = array(
				"is_deleted" => 1,
				"updatetime" => date("Y-m-d H:i:s"),
		);
		$pgiSettingTable->update($delete_pgi_setting_data, $where_pgiSetting);

		//Delete User PGi Rate
		$where_pgiRate = "is_deleted = 0 AND pgi_setting_key=".$now_pgiSetting["pgi_setting_key"];
		$delete_pgi_rate_data = array(
				"is_deleted" => 1,
				"updatetime" => date("Y-m-d H:i:s"),
		);
		$pgiRateTable->update($delete_pgi_rate_data, $where_pgiRate);
	}


	function set_room_mcu_server($room_key, $mcu_data) {

		$ivesSettingTable = new IvesSettingTable($this->dsn);
		//$ives = $ivesSettingTable->findLatestByRoomKey($room_key);

		require_once("classes/polycom/Polycom.class.php");
		require_once("classes/dbi/video_conference.dbi.php");
		$mcu_db     = new McuServerTable(N2MY_MDB_DSN);

		$where   	= "room_key = '".addslashes($room_key)."'";
		$room		= $this->obj_Room->getRow($where);

		$mcu_host = $mcu_db->getActiveServerAddress($mcu_data["ives_mcu_server_key"]);
		if(!$mcu_host) {
			$msg = "No MCU server available where key is '" . $mcu_data["ives_mcu_server_key"] . "'.";
			$this->logger2->warn($msg);
			return false;
		}

		$accountInfo = $this->createSipAccountWithRetry($room, $mcu_data["ives_profile_id"]);
		if(!$accountInfo) {
			$msg = "Failed to create a SIP account.".$mcu_data["ives_profile_id"];
			$this->logger2->warn($msg);
			return false;
		}
		$polycom = new PolycomClass($this->dsn);
		$polycom->setWsdlDomain($mcu_host);
		$did = $polycom->createDid();
		$vad = ($mcu_data["ives_use_active_speaker"] == 1) ? true : false;
		$hdFlag = ($mcu_data["ives_use_hd_profile"] == 1) ? true : false;
		$this->createAdHocConference($room, $did, $vad, $mcu_host);

		$data = array(
				'room_key'           => $room_key,
				'mcu_server_key'     => $mcu_data["ives_mcu_server_key"],
				'user_key'           => $room["user_key"],
				'ives_did'           => $did,
				'sip_uid'            => $accountInfo["uid"],
				'client_id'          => $accountInfo["name"],
				'client_pw'          => $accountInfo["secret"],
				'profile_id'         => $mcu_data["ives_profile_id"],
				'num_profiles'       => $mcu_data["ives_num_profiles"],
				'use_active_speaker' => $mcu_data["ives_use_active_speaker"],
				'mcu_hd_flg'         => $mcu_data["ives_use_hd_profile"],
				'startdate'          => date("Y-m-d H:i:s"),
				'registtime'         => date("Y-m-d H:i:s")
		);
		$res = $ivesSettingTable->add($data);
		if (DB::isError($res)) {
			$this->logger2->error($data,"#DB ERROR!");
			return false;
		} else {
			return true;
		}
	}

	function update_room_mcu_server($room_key, $mcu_data) {
		//
		$ivesSettingTable = new IvesSettingTable($this->dsn);
		$ivesSetting = $ivesSettingTable->findLatestByRoomKey($room_key);

		require_once("classes/mcu/config/McuConfigProxy.php");
		$configProxy = new McuConfigProxy();
		$ignore_sip_flg = $configProxy->get("ignore_create_sip_account");
		//sip account 一旦削除
		if(!$ignore_sip_flg) {
			$result = $this->action_delete_sip_account($ivesSetting["sip_uid"]);
		}

		$room_info = $this->obj_Room->getRow("room_key = '".addslashes($room_key)."'");
		//sip account 再生成
		$accountInfo = $this->createSipAccountWithRetry($room_info, $mcu_data["ives_profile_id"]);
		if($accountInfo === false) {
			$this->action_room_detail('', '', '', 'Failed to create a SIP account.');
			exit;
		}

		$data = array(
					'sip_uid' => $accountInfo["uid"],
					'client_id' => $accountInfo["name"],
					'client_pw' => $accountInfo["secret"],
					'profile_id' => $mcu_data["ives_profile_id"],
				);
		$where = "ives_setting_key=".$ivesSetting["ives_setting_key"];
		$res = $ivesSettingTable->update($data, $where);
		if (DB::isError($res)) {
			$this->logger2->error($data,"#DB ERROR!");
			return false;
		} else {
			return true;
		}
	}

	function createSipAccountWithRetry($room_info, $video_conference_profileId) {
		require_once("classes/mcu/config/McuConfigProxy.php");
		$configProxy 	= new McuConfigProxy();
		//SIPアカウント作成
		$ignore_sip_flg = $configProxy->get("ignore_create_sip_account");
		if (!$ignore_sip_flg) {
			$sip_uid = $this->create_sip_account($room_info, $video_conference_profileId);
			if ($sip_uid) {
				$this->logger2->info($sip_uid);
				for( $count = 0; $count < 3; $count++ ) {
					$sip_info = $this->get_sip_info($sip_uid);
					if ($sip_info) {
						break;
					} else {
						sleep(1);
					}
				}
			}
			if (!$sip_uid || !$sip_info) {
				$this->logger->error(__FUNCTION__."#create sip account fault",__FILE__,__LINE__,array($sip_uid, $sip_info));
				return false;
			}
		}
		else {
			$sip_info =  array(
					"uid" => rand(10000, 99999),
					"name" => rand(1000000, 9999999),
					"secret" => $this->create_id().rand(2,9)
			);
		}
		$this->logger2->info($sip_info);
		return $sip_info;
	}

	function get_sip_info($uid) {
		try {
			require_once("classes/mcu/config/McuConfigProxy.php");
		    $configProxy 	= new McuConfigProxy();
			$sipTransferUrl = $configProxy->get("sipTransferUrl");
			if (!$sipTransferUrl) {
				$sipTransferUrl = N2MY_BASE_URL."/admin_tool/ives/sip_account.php";
			}
			$formData = array(
					"type"      => "get_sip_info",
					"uid"  => $uid,
			);
			$option = array(
					CURLOPT_URL             => $sipTransferUrl,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => $formData,
					CURLOPT_RETURNTRANSFER  => 1,
					CURLOPT_CONNECTTIMEOUT  => 650,
					CURLOPT_TIMEOUT         => 650
			);
			$ch = curl_init();
			curl_setopt_array($ch, $option);
			$result = curl_exec($ch);
			curl_close($ch);
			$this->logger2->info($result);
			return unserialize($result);
		} catch (SoapFault $e) {
			$this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
			return false;
		} catch (Exception $e) {
			$this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
			return false;
		}
	}

	function create_sip_account($room_info, $profileId = 281) {
		try {
			$dsn = $this->dsn;
			$room_key = $room_info["room_key"];
			$login_id = str_replace("-", "_", $room_key);

			require_once("classes/mcu/config/McuConfigProxy.php");
			$configProxy 	= new McuConfigProxy();
			$sipTransferUrl = $configProxy->get("sipTransferUrl");
			if (!$sipTransferUrl) {
				$sipTransferUrl = N2MY_BASE_URL."/admin_tool/ives/sip_account.php";
			}
			$formData = array(
					"type"      => "create_sip_account",
					"login_id"  => $login_id,
					"profileId" => $profileId,
			);
			$option = array(
					CURLOPT_URL             => $sipTransferUrl,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => $formData,
					CURLOPT_RETURNTRANSFER  => 1,
					CURLOPT_CONNECTTIMEOUT  => 650,
					CURLOPT_TIMEOUT         => 650
			);
			$ch = curl_init();
			curl_setopt_array($ch, $option);
			$result = curl_exec($ch);
			curl_close($ch);
			$this->logger2->info($result,"CREATE MCU: create_sip_account");
			return $result;
		} catch (SoapFault $e) {
			$this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
			return false;
		} catch (Exception $e) {
			$this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
			return false;
		}
	}

	function action_delete_sip_account($uid) {
        try {
            require_once("classes/mcu/config/McuConfigProxy.php");
			$configProxy 	= new McuConfigProxy();
			$sipTransferUrl = $configProxy->get("sipTransferUrl");
            if (!$sipTransferUrl) {
                $sipTransferUrl = N2MY_BASE_URL."/admin_tool/ives/sip_account.php";
            }
            $formData = array(
                "type"      => "delete_sip_account",
                "uid"  => $uid,
            );
            $option = array(
                CURLOPT_URL             => $sipTransferUrl,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $formData,
                CURLOPT_RETURNTRANSFER  => 1,
                CURLOPT_CONNECTTIMEOUT  => 650,
                CURLOPT_TIMEOUT         => 650
            );
            $ch = curl_init();
            curl_setopt_array($ch, $option);
            $result = curl_exec($ch);
            curl_close($ch);
            $this->logger->info($result);
            return true;
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__,array($uid,$result));
            return false;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__,array($uid,$result));
            return false;
        }
    }

	function createAdHocConference($room, $did, $vad, $mcu_host) {
		try {
			require_once("classes/polycom/Polycom.class.php");
			$polycom = new PolycomClass($this->dsn);
			$polycom->setWsdlDomain($mcu_host);
			$polycom->createAdhocConferenceTemplate($did, $vad, null);
			return true;
		}
		catch (Exception $e) {
			$this->logger2->error($e);
			return false;
		}
	}

    // ユーザーに紐ずくmeeting部屋削除
    private function deletemeetingRoom($user_key){
        //user_keyから部屋をmeeting部屋のみ取得しforeachで回す
        $room_list = $this->obj_Room->getMeetingRoomkeys($user_key);
        $this->logger2->info($room_list);
        foreach($room_list as $room_key){
            $this->deleteRoom($room_key["room_key"]);
        }

    }

    // 部屋削除
    public function deleteRoom($room_key){
        //room削除
        $this->obj_Room->deleteRoomByRoomkey($room_key);
        //relation削除
        $dbi_relation = new MgmRelationTable(N2MY_MDB_DSN);
        $dbi_relation->deleteRelationByRoomkey($room_key);
        //reservation削除
        require_once ("classes/dbi/reservation.dbi.php");
        $dbi_Reservation = new ReservationTable($this->dsn);
        $dbi_Reservation->deleteReservationByRoomkey($room_key);
        //meeting削除
        require_once ("classes/dbi/meeting.dbi.php");
        $dbi_meeting = new MeetingTable($this->dsn);
        $dbi_meeting->deleteActiveMeetingByRoomkey($room_key);
        //room_plan削除
        $this->obj_RoomPlan->deleteRoomPlanByRoomkey($room_key);
        //pgi_setting削除
        //pgi_rate削除
        $this->delete_default_room_pgi_setting($room_key);
        //ives_setting削除 ivesSettingはそのままにしておく
        $this->logger2->info('did ivesSetting delete room_key:'.$room_key);
        //ordered_service_option削除
        $this->obj_OrderedOption->deleteRoomOptionByRoomkey($room_key);
    }

	private function create_id() {
		list($a, $b) = explode(" ", microtime());
		$a = (int)($a * 100000);
		$a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
		$c = $a.$b;
		// 36
		//print (base_convert($c, 10, 36));
		// 62
		return $this->dec2any($c);
	}

	/**
	 * 数値から62進数のID発行
	 */
	private function dec2any( $num, $base=57, $index=false ) {
		if (! $base ) {
			$base = strlen( $index );
		} else if (! $index ) {
			$index = substr(
					'23456789ab' .
					'cdefghijkm' .
					'nopqrstuvw' .
					'xyzABCDEFG' .
					'HJKLMNPQRS' .
					'TUVWXYZ' ,0 ,$base );
		}
		$out = "";
		for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
			$a = floor( $num / pow( $base, $t ) );
			$out = $out . substr( $index, $a, 1 );
			$num = $num - ( $a * pow( $base, $t ) );
		}
		return $out;
	}

	public function is_one_account($account_plan = null) {
	    switch ($account_plan){
	        case "one":
	            return true;
	        case "one_id_host":
	            return true;
	        default:
	            return false;
	    }
	}
/*-----------------*
 *  MTGVFOUR-2215  *
 *-----------------*/
    /* 指定件数分のメンバキーをキューから取得しセールス部屋の追加を行う。*/
    public function numExecFromQueue($num = null) {
        if($num===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $res = $this->obj_SalesRoomQueue->getQnum($num);
        if(!$res) {
            return array(false,"no data");/* no data */
        }
        if($res[0]==false) {
            return $res;
        }
        $ret = $this->untilAddRoom($res);
        return $ret;
    }

    /* 全てのメンバキーをキューから取得しセールス部屋の追加を行う。*/
    public function allExecFromQueue() {
        $res = $this->obj_SalesRoomQueue->getQall();
        if(!$res) {
            return array(false,"no data");/* no data */
        }
        if($res[0]==false) {
            return $res;
        }
        $ret = $this->untilAddRoom($res);
        return $ret;
    }

    /* 指定のメンバキーをキューから取得しセールス部屋の追加を行う。*/
    public function selExecFromQueue($member_key = null) {
        if($member_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $res = $this->obj_SalesRoomQueue->getQmember($member_key);
        if(!$res) {
            return array(false,"no data");/* no data */
        }
        if($res[0]==false) {
            return $res;
        }
        $ret = $this->addSalesRoom($member_key);
        if($ret) {
            $ret = $this->obj_SalesRoomQueue->deleteQmember($member_key);
            if($ret[0]==false) {
                return $ret;
            }
        } else {
            $ret = $this->obj_SalesRoomQueue->updateQerr($member_key);
            if($ret[0]==false) {
                return $ret;
            }
            if($res["error_count"] > 5) { /* 5回ダメならpend設定 */
                $ret = $this->obj_SalesRoomQueue->updateQpend($member_key);
                if($ret[0]==false) {
                    return $ret;
                }
            }
            return array(false,"addSalesRoom fail");/* error */
        }
        return $ret;
    }

    /* 指定のメンバキーのキュー追加を行う。*/
    public function addQueue($member_key = null) {
        if($member_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $where = "member_key = '" . mysql_real_escape_string($member_key) . "'";
        $member_info = $this->obj_member->getRow($where,"user_key");
        if(!$member_info) {
            return array(false,"no member_info");/* no member */
        }
        $where = "user_key = '" . mysql_real_escape_string($member_info['user_key']) . "'";
        $user_info = $this->obj_User->getRow($where,"use_sales");
        if(!$user_info) {
            return array(false,"no user_info");/* no user */
        }
        if($user_info["use_sales"]!=1) {
            return array(false,"no sales");/* no user */
        }
        $res = $this->obj_SalesRoomQueue->getQmember($member_key);
        if($res) {
            return array(false,"member found");/* member found */
        }
        $res = $this->obj_SalesRoomQueue->addQmember($member_key);
        if($res[0]==false) {
            return $res;
        }
        return $res;
    }

    /* 指定のメンバキーのキューを保留状態にする。*/
    public function pendSetQueue($member_key = null) {
        if($member_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $res = $this->obj_SalesRoomQueue->updateQpend($member_key);
        if(!$res) {
            return array(false,"no data");/* no data */
        }
        if($res[0]==false) {
            return $res;
        }
        return true;
    }

    /* 指定のメンバキーのキューを削除する。*/
    public function selQueueCancel($member_key = null) {
        if($member_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $res = $this->obj_SalesRoomQueue->getQmember($member_key);
        if(!$res) {
            return array(false,"no member");/* no member */
        }
        $res = $this->obj_SalesRoomQueue->deleteQmember($member_key);
        if($res[0]==false) {
            return $res;
        }
        return true;
    }

    /* 全てのキューを削除する。*/
    public function allQueueCancel() {
        $res = $this->obj_SalesRoomQueue->getQcount();
        if($res==0) {
            return array(false,"no data");/* no data */
        }
        $res = $this->obj_SalesRoomQueue->deleteQall();
        return $res;
    }

    /* 指定のメンバキーのセールス部屋追加を行う。*/
    public function forceAddRoom($member_key = null) {
        if($member_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $where = "member_key = '" . mysql_real_escape_string($member_key) . "'";
        $member_info = $this->obj_member->getRow($where,"user_key");
        if(!$member_info) {
            return array(false,"no member");/* no member */
        }
        $where = "user_key = '" . mysql_real_escape_string($member_info['user_key']) . "'";
        $user_info = $this->obj_User->getRow($where,"use_sales");
        if(!$user_info) {
            return array(false,"no user");/* no user */
        }
        if($user_info["use_sales"]!=1) {
            return array(false,"no sales");/* no sales */
        }
        $res = $this->addSalesRoom($member_key);
        if(!$res){
            return array(false,"addSalesRoom fail");/* error */
        }
        return $res;
    }

    /* 指定件数分のキュー情報を取得する。*/
    public function listQueue($num = null) {
        if($num===null) {
            $res = $this->obj_SalesRoomQueue->getQall();
        } else {
            $res = $this->obj_SalesRoomQueue->getQnum($num);
        }
        if($res[0]==false) {
            return array(false,"no data");/* no data */
        }
        return $res;
    }

    /* 部屋追加 */
    private function untilAddRoom($res) {
        foreach($res as $res_line) {
            $ret = $this->addSalesRoom($res_line["member_key"]);
            if($ret) {
                $ret = $this->obj_SalesRoomQueue->deleteQmember($res_line["member_key"]);
            } else {
                $ret = $this->obj_SalesRoomQueue->updateQerr($res_line["member_key"]);
                if(!$ret) {
                    return false;
                }
                if($res_line["error_count"] > 5) { /* 5回ダメならpend設定 */
                    $ret = $this->obj_SalesRoomQueue->updateQpend($member_key);
                    if(!$ret) {
                        return false;
                    }
                }
                return array(false,"addSalesRoom fail");/* error */
            }
        }
        return $ret;
    }

    /**
     * ADD SALES ROOM
     */
    function addSalesRoom($member_key) {
$this->logger2->info('>>> addSalesRoom START ( member_key:'.$member_key.' )');
        $where = 'member_key = ' . $member_key;
        $member_info = $this -> obj_member -> getRow($where);
        $where = 'user_key = ' . $member_info['user_key'];
        $user_info = $this -> obj_User -> getRow($where);
        $count = $this -> obj_Room -> numRows($where);
        $room_key = $user_info['user_id'] . '-' . ($count + 1) . '-' . substr(md5(uniqid(time(), true)), 0, 4);
        $room_sort = $count + 1;
        //部屋登録
        $room_data = array(
            'room_key' => $room_key,
            'room_name' => $member_info['member_name'],
            'user_key' => $user_info['user_key'],
            'room_sort' => $room_sort,
            'room_status' => "1",
            'use_teleconf' => "0",
            'use_pgi_dialin' => "0",
            'use_pgi_dialin_free' => "0",
            'use_pgi_dialin_lo_call' => "0",
            'use_pgi_dialout' => "0",
            'use_sales_option' => "1",
            'max_ss_watcher_seat' => "1",
            'room_registtime' => date("Y-m-d H:i:s")
        );
        $ret = $this -> obj_Room -> add($room_data);
        if (DB::isError($ret)) {
            return false;
        }
$this->logger2->info('>>> add[ ROOM ] SUCCESSFULLY');
        require_once("classes/AppFrame.class.php");/* MTGVFOUR-2215 */
        $obj_AppFrame = new AppFrame;/* MTGVFOUR-2215 */
        $this -> account_dsn = $obj_AppFrame -> config -> get('GLOBAL', 'auth_dsn');/* MTGVFOUR-2215 */
        $obj_relation = new N2MY_DB($this -> account_dsn, "relation");
        $relation_data = array(
            'relation_key' => $room_key,
            'relation_type' => "mfp",
            'user_key' => $user_info["user_id"],
            'status' => "1",
            'create_datetime' => date("Y-m-d H:i:s")
        );
        $ret = $obj_relation -> add($relation_data);
        require_once ("classes/dbi/inbound.dbi.php");
        $obj_inboound = new InboundTable($this -> dsn);
        $inbound_count = $obj_inboound -> numRows($where);
        if ($inbound_count == 0 || !$inbound_count) {
            $count = 1;
            while ($count > 0) {
                // 念のため8ケタにそろえる
                $inbound_id = substr($this -> create_id(), 0, 8);
                $inbound_where = "inbound_id = '" . $inbound_id . "'";
                $count = $obj_inboound -> numRows($inbound_where);
            }
            // 登録
            $inbound_data = array(
                "inbound_id" => $inbound_id,
                "user_key" => $user_info['user_key'],
                "status" => 1,
                "create_datetime" => date("Y-m-d H:i:s"),
                "update_datetime" => date("Y-m-d H:i:s")
            );
            $ret = $obj_inboound -> add($inbound_data);
            if (DB::isError($ret)) {
                return false;
            }
$this->logger2->info('>>> add[ INBOUND ] SUCCESSFULLY');
            //relation追加
            $relation_data = array(
                "relation_key" => $inbound_id,
                "relation_type" => "inbound_id",
                "user_key" => $user_info["user_id"],
                "status" => 1,
                "create_datetime" => date("Y-m-d H:i:s")
            );
            $obj_relation -> add($relation_data);
        }
        require_once ("classes/dbi/outbound.dbi.php");
        $obj_outbound = new OutboundTable($this -> dsn);
        $outbound_info = $obj_outbound -> getRow($where);
        if (!$outbound_info) {
            $outbound_data = array(
                "user_key" => $user_info['user_key'],
                "status" => 1,
                "create_datetime" => date("Y-m-d H:i:s"),
                "update_datetime" => date("Y-m-d H:i:s")
            );
            $ret = $obj_outbound -> add($outbound_data);
            if (DB::isError($ret)) {
                return false;
            }
$this->logger2->info('>>> add[ OUTBOUND ] SUCCESSFULLY');
        }
        $count = 1;
        while ($count > 0) {
            $outbound_id = $this -> create_id();
            $where = "outbound_id = '" . addslashes($outbound_id) . "'";
            $count = $this -> obj_member -> numRows($where);
        }
        $outbound_where = "user_key = " . $user_info['user_key'];
        $outbound_info = $obj_outbound -> getRow($outbound_where);
        $outbound_key = sprintf("%03d", $outbound_info["outbound_key"]);
        $outbound_where = "outbound_id != '' AND user_key = " . $user_info['user_key'];
        $sales_member_count = $this -> obj_member -> numRows($outbound_where);
        $outbound_sort = $sales_member_count + 1;
        $outbound_sort = sprintf("%03d", $outbound_sort);
        $outbound_number_id = $outbound_key . $outbound_sort;
        $member_update_data = array(
            "outbound_id" => $outbound_id,
            "outbound_number_id" => $outbound_number_id,
            "use_sales" => 1
        );
        $where_member = "member_key = " . $member_key;
        $this -> obj_member -> update($member_update_data, $where_member);
        //部屋のアウトバウンドIDも更新
        $where_room = "room_key = '" . addslashes($room_key) . "'";
        $room_update_data = array(
            "outbound_id" => $outbound_id
        );
        $add_room = $this -> obj_Room -> update($room_update_data, $where_room);
        $relation_data = array(
            "relation_key" => $outbound_id,
            "relation_type" => "outbound",
            "user_key" => $member_info["member_id"],
            "status" => "1",
            "create_datetime" => date("Y-m-d H:i:s")
        );
        $obj_relation -> add($relation_data);
        $relation_data_number = array(
            "relation_key" => $outbound_number_id,
            "relation_type" => "outbound_number",
            "user_key" => $member_info["member_id"],
            "status" => "1",
            "create_datetime" => date("Y-m-d H:i:s")
        );
        $obj_relation -> add($relation_data_number);
        //部屋のリレーション追加
        require_once ("classes/dbi/member_room_relation.dbi.php");
        $obj_member_room_relation = new MemberRoomRelationTable($this -> dsn);
        $member_room_relation_data = array(
            "member_key" => $member_info["member_key"],
            "room_key" => $room_key,
            "create_datetime" => date("Y-m-d H:i:s")
        );
        $ret = $obj_member_room_relation -> add($member_room_relation_data);
        if (DB::isError($ret)) {
            return false;
        }

        $service_key = 118;
        $room_plan_data = array(
            'room_key' => $room_key,
            'room_plan_status' => 1,
            'service_key' => $service_key,
            'contract_month_number' => 0,
            'discount_rate' => 0,
            'room_plan_starttime' => date("Y-m-d 00:00:00"),
            'room_plan_endtime' => "0000-00-00 00:00:00",
            'room_plan_registtime' => date("Y-m-d H:i:s")
        );
        require_once ("classes/dbi/room_plan.dbi.php");
        $obj_RoomPlan = new RoomPlanTable($this -> dsn);
        $ret = $obj_RoomPlan -> add($room_plan_data);
        if (DB::isError($ret)) {
            return false;
        }
$this->logger2->info('>>> add[ ROOM_PLAN ] SUCCESSFULLY');
        require_once ("classes/dbi/service.dbi.php");
        $obj_service = new ServiceTable($this -> account_dsn);
        $where = 'service_key = ' . $service_key;
        $service_info = $obj_service -> getRow($where);
        require_once ("classes/dbi/ordered_service_option.dbi.php");
        $obj_ordered_service_option = new OrderedServiceOptionTable($this -> dsn);
        $where = sprintf("room_key = '%s' AND service_option_key = 7 AND ordered_service_option_status = 1", addslashes($room_key));
        $audience_count = $obj_ordered_service_option -> numRows($where);
        if ($audience_count > 0) {
            $max_audience = $audience_count . "0";
        } else {
            $max_audience = $service_info["max_audience_seat"];
        }
        $image_ext    = $obj_AppFrame->config->getAll('DOCUMENT_IMAGES');
        $document_ext = $obj_AppFrame->config->getAll('DOCUMENT_FILES');
        if(array_key_exists("xdw", $document_ext)){
            unset($document_ext["xdw"]);
        }
        $whiteboard_filetype["document"] = array_keys($document_ext);
        $whiteboard_filetype["image"]    = array_keys($image_ext);
        $where = "room_key = '" . mysql_real_escape_string($room_key) . "'";
        $room_seat = array(
            "max_seat" => 2,
            "max_audience_seat" => $max_audience,
            "max_whiteboard_seat" => $service_info["max_whiteboard_seat"],
            "max_guest_seat" => $service_info["max_guest_seat"],
            "max_guest_seat_flg" => $service_info["max_guest_seat_flg"],
            "extend_seat_flg" => $service_info["extend_seat_flg"],
            "extend_max_seat" => $service_info["extend_max_seat"],
            "extend_max_audience_seat" => $service_info["extend_max_audience_seat"],
            "meeting_limit_time" => $service_info["meeting_limit_time"],
            "ignore_staff_reenter_alert" => $service_info["ignore_staff_reenter_alert"],
            "default_camera_size" => $service_info["default_camera_size"],
            "hd_flg" => $service_info["hd_flg"],
            "disable_rec_flg" => $service_info["disable_rec_flg"],
            "active_speaker_mode_only_flg" => 0,
            "active_speaker_mode_use_flg" => 0,
            "max_room_bandwidth" => $service_info["max_room_bandwidth"],
            "max_user_bandwidth" => $service_info["max_user_bandwidth"],
            "min_user_bandwidth" => $service_info["min_user_bandwidth"],
            "whiteboard_page" => $service_info["whiteboard_page"] ? $service_info["whiteboard_page"] : 100,
            "whiteboard_size" => $service_info["whiteboard_size"] ? $service_info["whiteboard_size"] : 20,
            "cabinet_size" => $service_info["cabinet_size"] ? $service_info["cabinet_size"] : 20,
            "customer_sharing_button_hide_status" => 1,
            "default_agc_use_flg" => 1,
            "default_layout_16to9" => "normal",
            "whiteboard_filetype" => serialize($whiteboard_filetype),
            "room_updatetime" => date("Y-m-d H:i:s")
        );
        $ret = $this -> obj_Room -> update($room_seat, $where);
        if (DB::isError($ret)) {
            return false;
        }
        //PC画面共有と高画質オプション付与
        //プランのオプション情報取得
        $add_options = array();
        if ($service_info["meeting_ssl"] == 1) {
            $add_options[] = "2";
        }
        if ($service_info["desktop_share"] == 1) {
            $add_options[] = "3";
        }
        if ($service_info["high_quality"] == 1) {
            $add_options[] = "5";
        }
        if ($service_info["mobile_phone"] > 0) {
            $add_options[] = "6";
        }
        if ($service_info["h323_client"] > 0) {
            $add_options[] = "8";
        }
        //hdd_extentionは数値分登録
        if ($service_info["hdd_extention"] > 0) {
            $add_options[] = "4";
        }
        // ホワイトボードプラン
        if ($service_info["whiteboard"] == 1) {
            $add_options[] = "16";
        }
        // マルチカメラ
        if ($service_info["multicamera"] == 1) {
            $add_options[] = "18";
        }
        // 電話連携
        if ($service_info["telephone"] == 1) {
            $add_options[] = "19";
        }
        // 録画GW
        if ($service_info["record_gw"] == 1) {
            $add_options[] = "20";
        }
        // スマートフォン
        if ($service_info["smartphone"] == 1) {
            $add_options[] = "21";
        }
        // 資料共有映像再生許可
        if ($service_info["whiteboard_video"] == 1) {
            $add_options[] = "22";
        }
        // PGi連携
        // if ($service_info["teleconference"] == 1) {
            // $add_options[] = "23";
        // }
        // 最低帯域アップ
        if ($service_info["video_conference"] == 1) {
            $add_options[] = "24";
        }
        // 最低帯域アップ
        if ($service_info["minimumBandwidth80"] == 1) {
            $add_options[] = "25";
        }
        // h264
        if ($service_info["h264"] == 1) {
            $add_options[] = "26";
        }
        // global_link
        // ユーザーにGLオプションがあればGLをつける
        require_once ("classes/dbi/user_service_option.dbi.php");
        $obj_user_option = new UserServiceOptionTable($this->dsn);
        $user_option_info = $obj_user_option -> getRow("service_option_key = 30 AND user_service_option_status =1 AND user_key = " . $user_info['user_key']);

        if ($service_info["global_link"] == 1 || $user_option_info) {
            $add_options[] = "30";
        }


        //オプション登録
        if ($add_options) {
            foreach ($add_options as $option_key) {
                $where = "ordered_service_option_status = 1 AND room_key = '" . $room_key . "' AND service_option_key = " . $option_key;
                $count = $obj_ordered_service_option -> numRows($where);
                if ($count == 0 || "4" == $option_key || "6" == $option_key || "8" == $option_key) {
                    if ("4" == $option_key) {
                        for ($num = 1; $num <= $service_info["hdd_extention"]; $num++) {
                            $this -> add_room_options($room_key, $option_key, null);
                        }
                    } else if ("6" == $option_key) {
                        for ($num = 1; $num <= $service_info["mobile_phone"]; $num++) {
                            $this -> add_room_options($room_key, $option_key, null);
                        }
                    } else if ("8" == $option_key) {
                        for ($num = 1; $num <= $service_info["h323_client"]; $num++) {
                            $this -> add_room_options($room_key, $option_key, null);
                        }
                    } else {
                        $this -> add_room_options($room_key, $option_key, null);
                    }
                }
            }
        }
$this->logger2->info('>>> addSalesRoom SUCCESSFULLY END ( member_key:'.$member_key.' )');
        return true;
    }

    function add_room_options($room_key, $service_option_key, $option_start_time) {
        require_once ("classes/dbi/ordered_service_option.dbi.php");
        $obj_ordered_service_option = new OrderedServiceOptionTable($this -> dsn);
        if (!$option_start_time) {
            $option_start_time = date("Y-m-d H:i:s");
        }
        if ($service_option_key == 23) {// teleconference
            require_once ("classes/dbi/pgi_setting.dbi.php");
            $obj_pgi_setting = new PGiSettingTable($this -> dsn);
            $month_first_day = substr($option_start_time, 0, 7);
            $pgi_setting = $obj_pgi_setting -> findEnablleAtYM($room_key, $month_first_day);
            if (count($pgi_setting) <= 0) {
                exit ;
            }
        }
        $today = date("Y-m-d 23:59:59");
        if (strtotime($option_start_time) >= strtotime($today)) {
            $status = "2";
        } else {
            $status = "1";
        }
        $data = array(
            "room_key" => $room_key,
            "service_option_key" => $service_option_key,
            "ordered_service_option_status" => $status,
            "ordered_service_option_starttime" => $option_start_time,
            "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
            "ordered_service_option_deletetime" => "0000-00-00 00:00:00"
        );
        if ($service_option_key == 7) {
            $obj_RoomPlan = new N2MY_DB($this -> dsn, "room_plan");
            $where = "room_plan_status = 1 AND room_key = '" . mysql_real_escape_string($room_key) . "'";
            $room_plan = $obj_RoomPlan -> getRow($where);
            if (!$room_plan) {
                $where = "room_plan_status = 2 AND room_key = '" . mysql_real_escape_string($room_key) . "'";
                $room_plan_reserve = $obj_RoomPlan -> getRow($where);
                if ($room_plan_reserve) {
                    $obj_ordered_service_option -> add($data);
                }
            } else {
                $obj_ordered_service_option -> add($data);
                $where_count = "ordered_service_option_status = 1 AND room_key = '" . mysql_real_escape_string($room_key) . "' AND service_option_key = " . $service_option_key;
                $audience_count = $obj_ordered_service_option -> numRows($where_count);
                if ($audience_count > 0) {
                    $seat_data["max_seat"] = "9";
                    $audience_seat = $audience_count . "0";
                } else {
                    $seat_data["max_seat"] = "10";
                    $audience_seat = "0";
                }
                $seat_data["max_audience_seat"] = $audience_seat;
                $where_room = "room_key = '" . mysql_real_escape_string($room_key) . "'";
                $room_data = $this -> obj_Room -> update($seat_data, $where_room);
            }
        } else {
            $obj_ordered_service_option -> add($data);
            if ($service_option_key == 23) {// teleconference
                $teleconf_data = array(
                    'use_teleconf' => 0,
                    'use_pgi_dialin' => 0,
                    'use_pgi_dialin_free' => 0,
                    'use_pgi_dialin_lo_call' => 0,
                    'use_pgi_dialout' => 0
                );
                $where_room = "room_key = '" . mysql_real_escape_string($room_key) . "'";
                $this -> obj_Room -> update($teleconf_data, $where_room);
            }
        }
        // スマートフォン連携時に電話連携がなければ自動でセットする
        if ($service_option_key == 21) {
            $where = "room_key = '" . mysql_real_escape_string($room_key) . "' AND service_option_key = 19 AND ordered_service_option_status = 1";
            $where_count = $obj_ordered_service_option -> numRows($where);
            if ($where_count == 0) {
                $data2 = $data;
                $data2["service_option_key"] = 19;
                $obj_ordered_service_option -> add($data2);
            }
        }
        //テレビ会議連携の場合は人数を9に変更
        if ($service_option_key == 24) {
            require_once ("classes/dbi/ives_setting.dbi.php");
            $obj_ives_setting = new IvesSettingTable($this -> dsn);
            $res = $obj_ives_setting -> findLatestByRoomKey($room_key);
            if (PEAR::isError($res) || !isset($res["use_active_speaker"])) {
                exit ;
            }
            // ActiveSpeakerがオンの時は、人数変更はスキップ。
            if ($res["use_active_speaker"] != 1) {
                $seat_data = array(
                    "max_seat" => 9
                );
                $where = "room_key = '" . mysql_real_escape_string($room_key) . "'";
                $this -> obj_Room -> update($seat_data, $where);
            }
        }
    }

/*-----------------*
 *  MTGVFOUR-2215  *
 *-----------------*/


/*-----------------*
 *  MTGVFOUR-2335  *
 *-----------------*/
    /*
     * 指定件数分のユーザーキーをキューから取得し削除、更新処理を行う
     */
    public function userExecFromQueue($num = null, $mode = null, $limit = null ) {
        if($num===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $res = $this->obj_OneUserQueue->getQnum($num,$mode);
        if(!$res) {
            return array(true,"no data");/* no data */
        }
        if($res[0]==false) {
            return $res;
        }
        foreach($res as $res_line) {
            switch ( $res_line["mode"] ) {
                // Update Mode 
                case OneUserQueue::MODE_UPDATE:
                    $this->logger2->info("ONE_USER_QUEUE:UPDATE_USER : user_key[ " . $res_line["user_key"] . " ]");
                    $ret = $this->updateOneUserPlan($res_line["user_key"]);
                    break;
                // Delete Mode
                case OneUserQueue::MODE_DELETE:
                    $this->logger2->info("ONE_USER_QUEUE:DELETE_USER : user_key[ " . $res_line["user_key"] . " ]");
                    $ret = $this->deleteOneMenber($res_line["user_key"],$limit);
                    break;
                default:
                    break;
            }
            if($ret[0]===false){
                $ret = $this->obj_OneUserQueue->updateQerr($res_line["one_user_queue_key"]);
                if($ret[0]===false){
                    break;
                }
                //エラーカウントが保留条件を超え、且つ保留状態ではない場合は保留にする
                if($res_line["error_count"] > 5 && $res_line["pned"] == 0 ) {
                    $ret = $this->obj_OneUserQueue->updateQpend($res_line["one_user_queue_key"]);
                    if($ret[0]===false){
                        break;
                    }
                }
            }
        }
        return $ret;
    }


    /*
     * キューに登録された全件を対象に処理を行う
     */
    public function userAllExecFromQueue($force = null, $mode = null ,$limit = null) {
        //夜間処理チェック
        $time_check = $this->getMidnightRuntimeInfo();
        if($time_check["is_run"]===false) {
            return array(false,"This time is can not All Mode Execute");
        }
        $res = $this->obj_OneUserQueue->getQall($mode,$force);
        // 処理対象がない場合は正常終了させる
        if(!$res) {
            $res[0] = true;
            return $res;
        }
        //異常があった場合はエラー
        if($res[0]===false ) {
            return $res;
        }
        foreach($res as $res_line) {
            switch ( $res_line["mode"] ) {
                // Update Mode 
                case OneUserQueue::MODE_UPDATE:
                    $this->logger2->info("ONE_USER_QUEUE:UPDATE_USER : user_key[ " . $res_line["user_key"] . " ]");
                    $ret = $this->updateOneUserPlan($res_line["user_key"],$res_line["one_user_queue_key"]);
                    break;
                // Delete Mode
                case OneUserQueue::MODE_DELETE:
                    $this->logger2->info("ONE_USER_QUEUE:DELETE_USER : user_key[ " . $res_line["user_key"] . " ]");
                    $ret = $this->deleteOneMenber($res_line["user_key"],$limit,$res_line["one_user_queue_key"]);
                    break;
                default:
                    $ret = false;
                    break;
            }
            if($ret[0]===false){
                $ret = $this->obj_OneUserQueue->updateQerr($res_line["one_user_queue_key"]);
                if($ret[0]===false){
                    break;
                }
                //エラーカウントが保留条件を超え、且つ保留状態ではない場合は保留にする
                if($res_line["error_count"] > 5 && $res_line["pned"] == 0) {
                    $ret[0]= $this->obj_OneUserQueue->updateQpend($res_line["one_user_queue_key"]);
                    if($ret[0]===false){
                        break;
                    }
                }
            }
            //現在時間を更新し、有効時間を超えた場合は終了する
            $time_now  = strtotime(date("H:i:s"));
            if($time_check["stop_time"] < $time_now) {
                $ret[0] = true;
                break;
            }
        }
        return array($ret);

    }


    /*
     * 指定のキューキーがキューにあった場合処理を実行する
     * 保留状態でも実行する
     */
    public function userExecFromUserQueuekey($one_user_queue_key = null) {
        if($one_user_queue_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $res = $this->obj_OneUserQueue->getQuserFromQueueKey($one_user_queue_key);
        if(!$res) {
            return array(false,"no user_info");/* no user */
        }
        switch ($res["mode"]) {
            //update
            case OneUserQueue::MODE_UPDATE:
                $this->logger2->info("ONE_USER_QUEUE:UPDATE_USER : user_key[ " . $res["user_key"] . " ]");
                $ret = $this->updateOneUserPlan($res["user_key"],$one_user_queue_key);
                break;
            //delete
            case OneUserQueue::MODE_DELETE:
                $this->logger2->info("ONE_USER_QUEUE:DELETE_USER : user_key[ " . $res["user_key"] . " ]");
                $ret = $this->deleteOneMenber($res["user_key"],null,$one_user_queue_key);
                break;
            //other
            default:
                break;
        }
        if($ret[0]===false){
            $ret = $this->obj_OneUserQueue->updateQerr($one_user_queue_key);
            //エラーカウントが保留条件を超え、且つ保留状態ではない場合は保留にする
            if($res["error_count"] > 5 && $res["pned"] == 0 ) {
                $ret[0]= $this->obj_OneUserQueue->updateQpend($one_user_queue_key);
            }
        }
        return $ret;
    }


    /* 
     * 指定のユーザーキーのキュー追加を行う。
     */
    public function userAddQueue($user_key = null,$mode = 2) {
        if($user_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $where = "user_key = '" . mysql_real_escape_string($user_key) . "'";
        $user_info = $this->obj_User->getRow($where);
        $member_info = $this->obj_member->getRow($where,"user_key");
        if(!$user_info && !$member_info) {
            return array(false,"no user_info");/* no user */
        }
        $res = $this->obj_OneUserQueue->getQuser($user_key);
        //該当のユーザーが既に登録されていた場合
        if($res) {
            // Mode優先順位判定
            if( $mode <= $res["mode"] ) {
                return array(false,"user found");/* user found */
            // 更新すべきModeが来た場合
            } else {
                $res = $this->obj_OneUserQueue->updateQmode($user_key,$mode);
            }
        //該当のユーザーが未登録だった場合
        } else {
            $res = $this->obj_OneUserQueue->addQuser($user_key,$mode);
        }
        return array($res);
    }


    /* 
     * 指定のキューキーのModeを削除に変更する。
     */
    public function userQueueSetDeleteMode($one_user_queue_key = null) {
        if($one_user_queue_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $mode = 2;
        $res = $this->obj_OneUserQueue->getQuserFromQueueKey($one_user_queue_key);
        //該当のユーザーが既に登録されていた場合
        if($res) {
            // Mode優先順位判定
            if( $mode == $res["mode"] ) {
                return array(false,"already set delete mode");/* Already set */
            // 更新すべきModeが来た場合
            } else {
                $res = $this->obj_OneUserQueue->updateQmodeFromQueueKey($one_user_queue_key,$mode);
            }
        //該当のユーザーが未登録だった場合
        } else {
            return array(false,"no entry user");/* no entry user */
        }
        return $res;
    }


    /*
    * 指定件数分のユーザーキュー情報を取得する
    */
    public function userListQueue($num = null,$mode = null,$pend = null) {
        if($num===null) {
            $res = $this->obj_OneUserQueue->getQall($mode,$pend);
        } else {
            $res = $this->obj_OneUserQueue->getQnum($num,$mode,$pend);
        }
        if($res===false) {
            return array(false,"no data");/* no data */
        }
        return $res;
    }

    /*
     * 指定のキューキーのキューを保留状態にする
     */
    public function userPendSetQueue($one_user_queue_key = null) {
        if($one_user_queue_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $res = $this->obj_OneUserQueue->updateQpend($one_user_queue_key);
        if(!$res) {
            return array(false,"no data");/* no data */
        }
        if($res[0]===false) {
            return $res;
        }
        return true;
    }

    /*
    * 指定のユーザーキーのキューを削除する
    */
    public function userQueueCancel($one_user_queue_key = null) {
        if($one_user_queue_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        $res = $this->obj_OneUserQueue->getQuserFromQueueKey($one_user_queue_key);
        if(!$res) {
            return array(false,"no entry user");/* no entry user */
        }
        $this->obj_OneUserQueue->deleteQuserFromQueueKey($one_user_queue_key);
        return true;
    }

    /* 全てのキューを削除する。*/
    public function userAllQueueCancel($mode = null) {
        $res = $this->obj_OneUserQueue->getQcount();
        if($res==0) {
            return array(false,"no data");/* no data */
        }
        if($mode===null) {
            $mode = -1;
        }
        $res = $this->obj_OneUserQueue->deleteQall($mode);
        return $res;
    }

    /*
     * 指定のユーザーキーのメンバ削除処理を行う。
     * メンバーがいない場合はキューから対象キューを削除する
     */
    private function deleteOneMenber($user_key = null,$limit = null , $pend_queue_key = null) {
        if($user_key===null) {
            return array(false,"no parameter");/* no parameter */
        }
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->dsn);
        $where = "user_key = '" . mysql_real_escape_string($user_key) . "' AND member_status >= 0";
        $member_list = $this->obj_member->getRowsAssoc($where, null,$limit);
        foreach($member_list as $member) {
            $ret = $obj_N2MY_Account->deleteMember( $member['member_key'] );
            if(!$ret) {
                return array(false,"member delete Error");/* error */
            }
        }
        /* 残メンバーがいなければキューから削除を実施する */
        $member_count = $this->obj_member->numRows( $where );
        $this->logger2->info("Remaining user_key=[ " . $user_key . " ] member Count=[ " . $member_count . " ]");
        if(!$member_count){
            $this->logger2->info("No remaining member. Compleate delete one user queue.");
            if($pend_queue_key) {
                $this->obj_OneUserQueue->deleteQuserFromQueueKey($pend_queue_key);
            } else {
                $this->obj_OneUserQueue->deleteQuser($user_key);
            }
        }
        return true;
    }

    /*
     * 指定のユーザーキーのプラン更新を行う
     */ 
   private function updateOneUserPlan($user_key = null,$pend_queue_key = null) {
        if($user_key===null) {
            return array(false,"no parameter");/* no parameter */
        }

        // 更新すべきプランを取得
        $where = sprintf( "user_key='%s' AND user_plan_status=1", $user_key );
        $newPlanInfo = $this->obj_UserPlan->getRow( $where );
        $where_user_service = sprintf( "service_key='%s'", $newPlanInfo["service_key"] );
        $new_plan_info = $this->obj_Service->getRow($where_user_service);

        //更新キューが処理される前にIDホストプランに切り替えられた場合の対策
        if($new_plan_info["is_one_id_host"]) {
            $this->logger2->info("User_key[ " . $user_key . " ] is Changed ID_Host_Plan.\nDelete Queue.");
            //更新しないでキューを削除する
            if($pend_queue_key) {
                $this->obj_OneUserQueue->deleteQuserFromQueueKey($pend_queue_key);
            } else {
                $this->obj_OneUserQueue->deleteQuser($user_key);
            }
            return true;
        }

        //現在のプランを取得
        $where = sprintf( "user_key='%s' AND user_plan_status=0", $user_key );
        $sort = array("user_plan_key" => "DESC");
        $nowPlanInfo = $this->obj_UserPlan->getRow( $where,null,$sort );
        $where_user_service = sprintf( "service_key='%s'", $nowPlanInfo["service_key"] );
        $now_plan_info = $this->obj_Service->getRow($where_user_service);

        //既存部屋の設定更新
        $where = sprintf( "is_one_time_meeting = 0 AND use_sales_option = 0 AND room_status = 1 AND user_key=%s", $user_key );
        $room_list = $this->obj_Room->getRowsAssoc($where);

        $add_options = array();
        $delete_options = array();
        if ($new_plan_info["meeting_ssl"] - $now_plan_info["meeting_ssl"] == 1) {
            $add_options[] = "2";
        } else if ($new_plan_info["meeting_ssl"] - $now_plan_info["meeting_ssl"] == -1){
            $delete_options = "2";
        }
        if ($new_plan_info["desktop_share"] - $now_plan_info["desktop_share"] == 1) {
            $add_options[] = "3";
        } else if ($new_plan_info["desktop_share"] - $now_plan_info["desktop_share"] == -1) {
            $delete_options[] = "3";
        }
        if ($new_plan_info["high_quality"] - $now_plan_info["high_quality"] == 1) {
            $add_options[] = "5";
        } else if ($new_plan_info["high_quality"] - $now_plan_info["high_quality"] == -1) {
            $delete_options[] = "5";
        }
        if ($new_plan_info["mobile_phone"] > 0 && $now_plan_info["mobile_phone"] <= 0) {
            $add_options[] = "6";
        } else if ($new_plan_info["mobile_phone"] <= 0 && $now_plan_info["mobile_phone"] > 0) {
            $delete_options[] = "6";
        }
        if ($new_plan_info["h323_client"] > 0 && $now_plan_info["h323_client"] <= 0) {
            $add_options[] = "8";
        } else if ($new_plan_info["h323_client"] <= 0 && $now_plan_info["h323_client"] > 0) {
            $delete_options[] = "8";
        }
        //hdd_extentionは数値分登録
        if ($new_plan_info["hdd_extention"] > 0 && $now_plan_info["hdd_extention"] <= 0) {
            $add_options[] = "4";
        } else if ($new_plan_info["hdd_extention"] <= 0 && $now_plan_info["hdd_extention"] > 0) {
            $delete_options[] = "4";
        }
        // ホワイトボードプラン
        if ($new_plan_info["whiteboard"] - $now_plan_info["whiteboard"] == 1) {
            $add_options[] = "16";
        } else if ($new_plan_info["whiteboard"] - $now_plan_info["whiteboard"] == -1) {
            $delete_options[] = "16";
        }
        // マルチカメラ
        if ($new_plan_info["multicamera"] - $now_plan_info["multicamera"] == 1) {
            $add_options[] = "18";
        } else if ($new_plan_info["multicamera"] - $now_plan_info["multicamera"] == -1) {
            $delete_options[] = "18";
        }
        // 電話連携
        if ($new_plan_info["telephone"] - $now_plan_info["telephone"] == 1) {
            $add_options[] = "19";
        } else if ($new_plan_info["telephone"] - $now_plan_info["telephone"] == -1) {
            $delete_options[] = "19";
        }
        // 録画GW
        if ($new_plan_info["record_gw"] - $now_plan_info["record_gw"] == 1) {
            $add_options[] = "20";
        } else if ($new_plan_info["record_gw"] - $now_plan_info["record_gw"] == -1) {
            $delete_options[] = "20";
        }
        // スマートフォン
        if ($new_plan_info["smartphone"] - $now_plan_info["smartphone"] == 1) {
            $add_options[] = "21";
        } else if ($new_plan_info["smartphone"] - $now_plan_info["smartphone"] == -1) {
            $delete_options[] = "21";
        }
        // 資料共有映像再生許可
        if ($new_plan_info["whiteboard_video"] - $now_plan_info["whiteboard_video"] == 1) {
            $add_options[] = "22";
        } else if ($new_plan_info["whiteboard_video"] - $now_plan_info["whiteboard_video"] == -1) {
            $delete_options[] = "22";
        }
        // PGi連携
        if ($new_plan_info["teleconference"] - $now_plan_info["teleconference"] == 1) {
            $add_options[] = "23";
        } else if ($new_plan_info["teleconference"] - $now_plan_info["teleconference"] == -1) {
            $delete_options[] = "23";
        } else if($new_plan_info["pgi_setting_key"] != $now_plan_info["pgi_setting_key"]){
            $add_options[] = "23";
            $delete_options[] = "23";
        }
        // 最低帯域アップ
        if ($new_plan_info["video_conference"] - $now_plan_info["video_conference"] == 1) {
            $add_options[] = "24";
        } else if ($new_plan_info["video_conference"] - $now_plan_info["video_conference"] == -1) {
            $delete_options[] = "24";
        }
        // 最低帯域アップ
        if ($new_plan_info["minimumBandwidth80"] - $now_plan_info["minimumBandwidth80"] == 1) {
            $add_options[] = "25";
        } else if ($new_plan_info["minimumBandwidth80"] - $now_plan_info["minimumBandwidth80"] == -1) {
            $delete_options[] = "25";
        }
        // h264
        if ($new_plan_info["h264"] - $now_plan_info["h264"] == 1) {
            $add_options[] = "26";
        } else if ($new_plan_info["h264"] - $now_plan_info["h264"] == -1) {
            $delete_options[] = "26";
        }
        // global_link
        if ($new_plan_info["global_link"] - $now_plan_info["global_link"] == 1) {
            $add_options[] = "30";
        } else if ($new_plan_info["global_link"] - $now_plan_info["global_link"] == -1) {
            $delete_options[] = "30";
        }
        $this->logger2->info(array($option_reset, $where, $add_options, $delete_options));
        foreach ($delete_options as $option_key) {
            $option_reset = array(
                    'user_service_option_status' => 0,
                    'user_service_option_deletetime' => date('Y-m-d H:i:s')
            );
            $where = "user_key = '".addslashes($user_key)."'" .
                    " AND service_option_key=".addslashes($option_key).
                    " AND user_service_option_status = 1";
            $this->obj_UserServiceOption->update($option_reset, $where);
            if($room_list) {
                foreach ($room_list as $room_info) {
                    if($room_info["use_sales_option"]) {
                        continue;
                    }
                    $where = "room_key = '".addslashes($room_info["room_key"])."'" .
                            " AND service_option_key=".addslashes($option_key).
                            " AND ordered_service_option_status = 1";
                    $option_reset = array(
                            'ordered_service_option_status' => 0,
                            'ordered_service_option_deletetime' => date('Y-m-d H:i:s')
                    );
                    $this->obj_OrderedOption->update($option_reset, $where);
                }
            }
            //PGiオプション削除
            if ($option_key == "23") {
                $this->delete_use_pgi_plan($user_key);
                if($room_list) {
                    foreach ($room_list as $room_info) {
                        if($room_info["use_sales_option"]) {
                            continue;
                        }
                        $this->delete_default_room_pgi_setting($room_info["room_key"]);
                        $update_room_teleconference = array (
                                "use_teleconf"           => 0,
                                "use_pgi_dialin"          => 0,
                                "use_pgi_dialin_free"      => 0,
                                "use_pgi_dialin_lo_call" => 0,
                                "use_pgi_dialout"          => 0,
                        );
                        $where_room = "room_key = '".$room_info["room_key"]."'";
                        $this->obj_Room->update($update_room_teleconference, $where_room);
                    }
                }
            }
        }
        foreach ($add_options as $option_key) {
            $data = array(
                    "service_option_key"            => $option_key,
                    "user_service_option_starttime" => date( "Y-m-d 00:00:00"),
            );
            $user_service_option_key = $this->add_user_option($user_key, $data);
            //部屋プランも更新
            if($room_list) {
                foreach ($room_list as $room_info) {
                    if($room_info["use_sales_option"]) {
                        continue;
                    }
                    $data = array(
                            "room_key"                            => $room_info["room_key"],
                            "user_service_option_key"           => $user_service_option_key,
                            "service_option_key"                => $option_key,
                            "ordered_service_option_status"     => 1,
                            "ordered_service_option_registtime" => date("Y-m-d H:i:s"),
                            "ordered_service_option_starttime"  => date("Y-m-d H:i:s"),
                    );
                    $result = $this->obj_OrderedOption->add( $data );
                }
            }
            //PGiオプション対応
            if ($data["service_option_key"] == "23") {
                if($new_plan_info["pgi_setting_key"]) {
                    $this->set_use_pgi_plan($user_key,$new_plan_info["pgi_setting_key"]);
                    if($room_list) {
                        foreach ($room_list as $room_info) {
                            if($room_info["use_sales_option"]) {
                                continue;
                            }
                            $this->set_default_room_pgi_setting($user_key, $room_info["room_key"]);
                            $update_room_teleconference = array(
                                    "use_teleconf"           => 1,
                                    "use_pgi_dialin"          => 1,
                                    "use_pgi_dialin_free"      => 1,
                                    "use_pgi_dialin_lo_call" => 1,
                                    "use_pgi_dialout"          => 1,
                            );
                            $where_room = "room_key = '".$room_info["room_key"]."'";
                            $this->obj_Room->update($update_room_teleconference, $where_room);
                        }
                    }
                } else {
                    $this->logger2->error($new_plan_info,"PGiプランを設定してない。");
                }
            }

        }
        //トライアル対応
        if($new_plan_info["use_trial_plan"] != $now_plan_info["use_trial_plan"]) {
            foreach ($room_list as $room_info) {
                if($room_info["use_sales_option"]) {
                    continue;
                }
                //delete old mcu
                $where = sprintf("room_key ='%s' AND ordered_service_option_status=1 AND service_option_key=16",$room_info["room_key"]);
                if(!$this->obj_OrderedOption->numRows($where)) {
                    $mcu_data = array(
                            //"ives_mcu_server_key" =>  ONE_DEFAULT_MCU_SERVER,
                            "ives_profile_id" => $new_plan_info["use_trial_plan"]?321:323, //タイプ：demo
                            //"ives_num_profiles" => $room_info["max_seat"]>20?20:$room_info["max_seat"],
                            //"ives_use_active_speaker" => 1,
                    );
                    $this->logger2->info($mcu_data);
                    $this->update_room_mcu_server($room_info["room_key"], $mcu_data);
                }
            }
        }
        //処理終了したら キューから削除を実施する
        $this->logger2->info("Compleate update one user queue. user_key=[ " . $user_key . " ]");
        if($pend_queue_key) {
            $this->obj_OneUserQueue->deleteQuserFromQueueKey($pend_queue_key);
        } else {
            $this->obj_OneUserQueue->deleteQuser($user_key);
        }
        return true;
    }

    /*
     * 夜間実行時間情報取得
     * return array(実行可能可否,開始時間,終了時間,実行可能時間)
     */
    public function getMidnightRuntimeInfo() {
        $res = array();
        $config = EZConfig::getInstance();
        $str_time_from = $config->get("N2MY","one_user_queue_md_strt");
        $str_time_to = $config->get("N2MY","one_user_queue_md_stop");
        $time_from = strtotime($str_time_from);
        $time_to   = strtotime($str_time_to);
        $time_now  = strtotime(date("H:i:s"),time());

         //デフォルト指定は02:00 - 05:00
        if(!$time_from) {
            $time_from = strtotime("02:00");
        }
        if(!$time_to) {
            $time_to = strtotime("05:00");
        }
        //24時間単位で記述のため、開始時間の方が大きい可能性がある(日跨ぎ)
        if( $time_from > $time_to ) {
            $now_date = date("Y-m-d",time());
            $time_from = strtotime($now_date . " " .$str_time_from);
            $time_to = strtotime("+1 day",strtotime($now_date . " " .$str_time_to));
            $time_now  = strtotime(date("y-m-d H:i:s"),time());
        }
        //起動範囲内でない場合
        if($time_to < $time_now || $time_now < $time_from ){
            $res["is_run"] = false;
        } else {
            $res["is_run"] = true;
        }

        $res["start_time"] = $time_from;
        $res["stop_time"] = $time_to;
        $res["time_duration"] = $time_to - $time_from;
        return $res;
    }
/*-----------------*
 *  MTGVFOUR-2335  *
 *-----------------*/
}
