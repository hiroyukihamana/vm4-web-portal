<?php
require_once("classes/N2MY_DBI.class.php");

class DefineTable extends N2MY_DB {

    var $table = 'define';
    var $logger = null;
    protected $primary_key = "define_no";

    /**
     * DB接続
     */
    function __construct($dsn) {
        $this->init($dsn, $this->table);
        $this->logger = EZLogger::getInstance();
    }

    function add($data) {
        $where = "type = '".addslashes($data["type"])."'".
        " AND path = '".addslashes($data["path"])."'" .
        " AND define_id = '".addslashes($data["define_id"])."'" .
        " AND flg  = '2'".
        " AND name = '".addslashes($data["name"])."'";
        $cnt = $this->numRows($where);
        if ($cnt > 0) {
            // 既に存在する
            $this->logger2->info($where);
            return false;
        }
        return parent::add($data);
    }
}
