━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
感谢使用视频会议系统。
密码重置申请已受理。
请在确认以下内容后，办理允许或者拒绝的手续。

■申请者ID
{$member_id}

■申请者姓名
{$name}

■允许再次发放密码
<{$smarty.const.N2MY_BASE_URL}a/{$authorize_key}>

■拒绝再次发放密码
<{$smarty.const.N2MY_BASE_URL}k/{$ignore_key}>

※有效期限：邮件发送后24小时内


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■开发・运营:
V-cube株式会社 <http://www.vcube.com/>
〒153-0051 东京都目黑区上目黑2-1-1 中目黑GT Tower 20F
■客服中心
电话号码: 0570-002192（24小时365天电话支持）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

