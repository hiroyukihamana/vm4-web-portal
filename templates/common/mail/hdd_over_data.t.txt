
---------------------------------------
 {$smarty.const.N2MY_BASE_URL}
 {$smarty.now|date_format:"%Y-%m-%d"}
---------------------------------------


▼ [ 利用制限をオーバーしたクライアント ]

{if $over_list}
{foreach from=$over_list item=row}
▽-----------------------------------------------------
ユーザID           ：{$row.user_id}
ユーザ名           ：{$row.user_name|default:"(no name)"}
ルーム名           ：{$row.room_key} [ {$row.room_name} ]
利用率（利用合計/利用制限）  ：{$row.use_size_view} / {$row.max_size_view} ( {$row.use_percent} % )
{/foreach}
-------------------------------------------------------
{else}
該当無し
{/if}



▼ [ 80% 以上を超えたクライアント ]

{if $limit_list}
{foreach from=$limit_list item=row}
▽-----------------------------------------------------
ユーザID           ：{$row.user_id}
ユーザ名           ：{$row.user_name|default:"(no name)"}
ルーム名           ：{$row.room_key} [ {$row.room_name} ]
利用率（利用合計/利用制限）  ：{$row.use_size_view} / {$row.max_size_view} ( {$row.use_percent} % )
{/foreach}
-------------------------------------------------------
{else}
該当無し
{/if}
