----------------------------------------------
【ユーザページからのお問い合わせ】
----------------------------------------------

■ 問い合わせの種類
{if $ask_category == 0}サービスについて
{elseif $ask_category == 1}料金について
{elseif $ask_category == 2}会議予約の変更について
{elseif $ask_category == 3}会議予約のキャンセルについて
{elseif $ask_category == 4}その他 
{/if}

■ 問い合わせの内容
{$ask_contents} 

----------------------------------------------

■ 問い合わせ者情報
ログインID      : {$login_id} 
御社名           : {$ask_company_name} 
部署名           : {$ask_company_division} 
担当者名（カナ）: {$ask_name_kana} 
ご担当者名     : {$ask_name} 
E-mailアドレス : <{$ask_email}> 
電話番号       : {$ask_telephone} 
FAX番号       : {$ask_fax} 