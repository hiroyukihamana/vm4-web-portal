{t}9008,_('※このメールは送信専用のため、ご返信いただけません。'){/t} 

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{t}9009,_('V-CUBE ミーティング4からのお知らせ'){/t} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{t}9024,_('以下の会議の開催時間が近くなりましたので、お知らせいたします。'){/t} 
------------------------------
{$info.sender} {if $guest.r_user_authority != 1}
{t}8884,_('様から、下記のWeb会議への招待がありました。

定刻になりましたら、招待URLをクリックして頂ければ、Web会議へ参加することができます。'){/t} 
{else}
{t}8999,_('様から、下記のWeb会議へ議長としての招待がありました。

定刻になりましたら、招待URLをクリックして頂ければ、Web会議へ参加することができます。'){/t} 
{/if}

■{t}2348,_('会議名'){/t} 
{$info.reservation_name}

{if $__frame.user_info.meeting_version >= "4.7.0.0"}
■{t}8995,_('議長'){/t} 
{$info.authority.name}
{/if}

■{t}9020,_('開催日時'){/t} 
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■{t}8587,_('主催者'){/t} 
{$info.organizer_name} 

■{t}9034,_('招待URL'){/t}{if $guest.r_user_authority == 1}({t}9044,_('議長専用・モバイル端末ではご利用いただけません'){/t}){/if}{if $guest.type == 1}({t}9244,_('モバイル端末ではご利用いただけません'){/t}){/if} 
{if $info.guest_url_format ==1}
{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

■{if $info.reservation_pw && $info.reservation_pw_type == 2}{t}2127,_('会議記録'){/t}{/if} {t}2349,_('パスワード'){/t} 
{if $info.reservation_pw}{$info.reservation_pw}{else}{t}8048,_('なし'){/t}{/if} 

------------------------------
{t 1=$info.sender}9026,_('%1$s様からのメッセージ'){/t} 
------------------------------
{$info.mail_body} 
{t 1=$info.sender_mail}8885,_('※このメールはシステムから自動送信されています。この会議のご招待についてのお問い合わせは、%1$sにご連絡下さい。'){/t} 
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if} 
{if !$guest.type && $guest.r_user_authority != 1}
------------------------------
{t}9010,_('会議の詳細情報'){/t} 
------------------------------
{if $info.pin_cd}
■{t}9011,_('V-CUBE ミーティング4用 暗証番号'){/t} 
{$info.pin_cd}
{t}9429,_('※iOS 末からは、下記URLにアクセスすると「暗証番号で参加する」ペー が表示されます。'){/t}

<{$smarty.const.N2MY_SHORT_URL}>

{t}9430,_('暗証番号を入力するとアプリが起動し、Web会議への参加が可能です。'){/t}

{t}9428,_('※Android端末からは、V-CUBE ミーティング4モバイルのアプリを起動し、「暗証番号でログイン」から暗証番号を入力する 、Web会 への 加が可能です。'){/t}

{t}9431,_('※V-CUBE  ーティング4モバイルアプリの入手方法は、下部の 利用案内をご確認ください。'){/t}

{/if}{/if}
{if ($temporarySipNumberAddress||$temporarySipAddress||$temporaryH323NumberAddress||$temporaryH323Address) && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■{t}9012,_('テレビ会議システム専用ゲストアドレス'){/t} 
 ・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if} 
 ・H.323 
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if} 
{t}9013,_('※Polycom等のテレビ会議端末からV-CUBEミーティングに接続する場合はこちらのアドレスを入力して入室してください。'){/t}
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}
{if $teleconf_info.tc_type == 'pgi'} 

■{t}8837,_('電話会議での参加方法'){/t} 
{t}8882,_('電話回線の使用が選択されました。アクセスポイントに電話をかけてガイダンスに従い、パスコードを入力します。'){/t} 
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- {t}9022,_('アクセスポイント（ダイヤルイン）番号'){/t}: 
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location}) 
-- {t}9023,_('その他のアクセスポイント一覧はこちら'){/t}: 
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if} 
-- {t}9019,_('パスコード'){/t} 
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

------------------------------
{t}9027,_('ご利用案内'){/t} 
------------------------------
■{t}9028,_('ご利用方法'){/t} 
1. {t}8847,_('Webカメラとヘッドセットを、パソコンに接続します。'){/t} 
2. {t}8848,_('環境確認ページにアクセスしてください。
   ネットワークや接続機器の状況が簡易診断できます。'){/t} 
{if $info.guest_url_format ==1}
{$base_url}services/tools/checker/
{else}
<{$base_url}services/tools/checker/>
{/if}
3. {t}8849,_('上記の「招待URL」から会議室に入室します。'){/t} 

{t}8850,_('※会議室に入室した後、カメラやヘッドセットを接続した場合、
一度会議室から退室し、再入室をすれば映像・音声を認識します。'){/t} 
{t}8851,_('※Webカメラのご用意がない場合、音声のみのご参加も可能です。'){/t} 

■{t}9029,_('推奨動作環境'){/t} 
{t}8891,_('動作環境ページにてご確認ください。'){/t} 
{if $info.guest_url_format ==1}
http://www.nice2meet.us/ja/requirements/meeting.html
{else}
<http://www.nice2meet.us/ja/requirements/meeting.html>
{/if}

■{t}9030,_('V-CUBE ミーティング4 アプリケーションの入手方法'){/t} 
{t}9031,_('スマートフォンやタブレット端末（iPad/Android）からは、アプリをダウンロードいただくことでご利用いただけます。'){/t} 
{t}8892,_('iOS用アプリケーションはこちらから'){/t} 
{if $info.guest_url_format ==1}
http://itunes.apple.com/jp/app/id431070449?mt=
{else}
<http://itunes.apple.com/jp/app/id431070449?mt=>
{/if}
{t}8893,_('Android用アプリケーションはこちらから'){/t} 
{if $info.guest_url_format ==1}
https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4
{else}
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>
{/if}

------------------------------
{t}9025,_('V-CUBE ミーティング4とは？'){/t} 
------------------------------
{t}8883,_('Web会議システム「V-CUBEミーティング4」は、インターネット回線とパソコンさえあればカンタンに会議ができます。

相手の顔を見て話ができるだけでなく、1つの資料を全員で共有したり、会議を録画して議事録として利用することができます。'){/t} 

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■{t}9018,_('V-CUBEミーティング4'){/t} 
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■{t}8927,_('開発・運営：'){/t} 
{t}8928,_('株式会社ブイキューブ'){/t} {if $info.guest_url_format ==1}{t}9449,_('http://www.vcube.com/'){/t} {else}<{t}9449,_('http://www.vcube.com/'){/t}>{/if} 
{t}8071,_('〒153-0051 東京都目黒区上目黒2-1-1 中目黒GTタワー20F'){/t} 
■{t}8888,_('カスタマーサポートセンター'){/t}{t}8838,_('（日本語専用）'){/t} 
{t}8889,_('電話番号：'){/t} {t}9450,_('0570-002192'){/t}({t}8890,_('24時間365日電話受付'){/t}) 
E-mail: {if $info.guest_url_format ==1} {t}9451,_('vsupport@vcube.co.jp'){/t} {else} <{t}9451,_('vsupport@vcube.co.jp'){/t}>{/if} 
■{t}8888,_('カスタマーサポートセンター'){/t}{t}8839,_('(英語その他)'){/t} 
{t}8889,_('電話番号：'){/t} 
　{t}8841,_('マレーシア'){/t}　+60-3-2202-4120 
　{t}8842,_('シンガポール'){/t}　+65-6636-5862 
　{t}8843,_('中国'){/t}　 +86-400-618-2360 
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
{t}8844,_('対応時間：平日'){/t} 9:00-18:00({t}9488,_('各国の現地時間'){/t}) 
{t}9489,_('土日及び各国の祝日は休業となります。'){/t} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
