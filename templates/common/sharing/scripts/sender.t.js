{literal}
var gController = null;
var gSid = null;
var gUid = null;

/* JS to Flash
function connect( url:String, sid:String, type:Number, uid:String, name:String )
function controlRequest( uid:String )
function cancelControlRequest( uid:String )
function controlRequestAnswer( result:Boolean )
*/

/* Flash to JS
function rControlRequest( targetUid:String, targetName:String )
function rCancelControlRequest( targetUid:String, targetName:String )
function rControlRequestAnswer( result:Boolean )
function rControlRejected()
function rConnectSuccess()
function rConnectFailed()
function rConnectClosed()
function rConnectRejected()
*/

// 制御用FCSに接続する
function connect( url, sid, type, uid, name ) {

    gSid = sid;
    gUid = uid;
    gController.call( "callGateway", "connect", url, gSid, type, gUid, name );
}

// 操作権の要求を受けた
function rControlRequest( targetUid, targetName ) { displayAlert( unescape( targetName ) ); }
function rCancelControlRequest( targetUid, targetName ) { alert( "{/literal}{t}6795,_('操作要求をキャンセルされました'){/t}{literal}" ); }
function controlRequestAnswer( result ) { gController.call( "callGateway", "controlRequestAnswer", result ); }
function test() {alert("test");}

// FCS EventHandlers
function rConnectSuccess() { sharingStart( gSid, "{/literal}{$sharing_server|escape:html}{literal}", 80, 5 ); }
function rConnectFailed() { alert( "{/literal}{t}6799,_('接続に失敗しました'){/t}{literal}" ); }
function rConnectClosed() {}
function rConnectRejected() { alert( "{/literal}{t}6797,_('接続を拒否されました'){/t}{literal}" ); }

function createController() {

    // 挿入対象のDIVを用意
    var targetElement = document.createElement( "div" );
    targetElement.id = "N2MYSharing2ControlManagerContainer";
    document.body.appendChild( targetElement );

    var jsConnectionId = ( new Date() ).getTime();
    var flashProxy = new FlashProxy( jsConnectionId, "/sharing/jsgw/JavaScriptFlashGateway.swf" );
    var flashTag = new FlashTag( "/sharing/flash/N2MYSharing2ControlManager.swf", 50, 50 );
    flashTag.setId( "N2MYSharing2ControlManager" );
    flashTag.setFlashvars( "jsConnectionId=" + jsConnectionId );
    targetElement.innerHTML = flashTag.toString();
    targetElement.style.position = "absolute";
    targetElement.style.left = -9999;
    return flashProxy;
}

function sharingStart( sharingId, serverAddress, port, fps ) {

    var DEFAULT_WIDTH    = 800;
    var DEFAULT_HEIGHT    = 600;

//    var ocx = document.getElementById( "N2MYRS2" );
    var ocx = sharing2.getSharing2Object();

    // Sender Mode
    ocx.Mode = Constants.APP_MODE_SENDER;

    // from Flash
    ocx.Server                = String( serverAddress );
    ocx.Port                = Number( port );
    ocx.SharingID            = String( sharingId );
    ocx.CaptureInterval    = Constants.getIntervalTimeByFPS( Number( fps ) );
    ocx.CaptureMode            = 1;

    // Capture Option
    // Set by 8Bit
    // 00000000
    // |||||--|
    // +||||--|--------- With Layerd Window
    //  +|||--|--------- With Cursor
    //   ++|--|--------- Send Mode
    //     +--+--------- Compression Level

    ocx.Option =
        Constants.COMP_LV_NORMAL +
        Constants.SEND_MODE_CLIPPED_CHANGED * 16
        Constants.WITH_OUT_CURSOR * 64 +
        Constants.WITH_LAYERED_WINDOW * 128;


    var startResult = ocx.Start();
    if ( startResult != 0 )
        alert( "{/literal}{t}6798,_('シェアリングの開始に失敗しました'){/t}{literal}\nErrorCode : " + startResult );
}

var eventListener = null;
function setEventListener( listener ) { eventListener = listener; }

function sharingStop() {

    //var ocx = document.getElementById( "N2MYRS2" );
    var ocx = sharing2.getSharing2Object();
    ocx.Stop();
}

function setCaptureMode( captureMode ) {

    //var ocx = document.getElementById( "N2MYRS2" );
    var ocx = sharing2.getSharing2Object();
    ocx.CaptureMode = Number(captureMode);
}

function setCaptureInterval( fps ) {

    //var ocx = document.getElementById( "N2MYRS2" );
    var ocx = sharing2.getSharing2Object();
    ocx.CaptureCycle = Constants.getIntervalTimeByFPS( Number( fps ) );
}

// 操作要求
function displayAlert( userName ) {

    //var ocx = document.getElementById( "N2MYRS2" );
    var ocx = sharing2.getSharing2Object();
    ocx.DisplayAlert(
        "[" + userName + "]" + Constants.ALERT_REQUEST,
        Constants.ALERT_TITLE,
        Constants.ALERT_ANIMATION_TIME + (Constants.ALERT_ANIMATION_TIME * 4096)
    );
}

function getVersion() {
    return sharing2.getSharing2Object().GetVersion();
    //return document.getElementById( "N2MYRS2" ).GetVersion();
}
{/literal}