{literal}
var gController = null;
var gSid = null;
var gUid = null;

/* JS to Flash
function connect( url:String, sid:String, type:Number, uid:String, name:String )
function controlRequest( result:Boolean )
function cancelControlRequest( uid:String )
function controlRequestAnswer( uid:String )
*/

/* Flash to JS
function rControlRequest( targetUid:String, targetName:String )
function rCancelControlRequest( targetUid:String, targetName:String )
function rControlRequestAnswer( result:Boolean )
function rControlRejected()
function rConnectSuccess()
function rConnectFailed()
function rConnectClosed()
function rConnectRejected()
*/

// 制御用FCSに接続する
function connect( url, sid, type, uid, name ) {
    gSid = sid;
    gUid = uid;

    gController.call( "callGateway", "connect", url, gSid, type, gUid, name );
}

function controlRequest() { gController.call( "callGateway", "controlRequest", gUid ); }
function controlEnd() { gController.call( "callGateway", "controlEnd" ); }
function cancelControlRequest() { gController.call( "callGateway", "cancelControlRequest", gUid ); }

// 操作権
function rControlRequestAnswer( result ) {

    if ( result ) {

        setControlMode( Constants.CONTROL_ON );
        eventListener.onControlSuccess();
    }

    else
        eventListener.onControlFailed();
}

function rControlRejected() {

    eventListener.onControlRejected();
}

// FCS EventHandlers
function rConnectSuccess() {viewSharingStart( gSid, "{/literal}{$sharing_server|escape:html}{literal}", 80 );}
function rConnectFailed() { alert( "{/literal}{t}6799,_('接続に失敗しました'){/t}{literal}" ); }
function rConnectClosed() {}
function rConnectRejected() { alert( "{/literal}{t}6797,_('接続を拒否されました'){/t}{literal}" ); }


function rControlRequestReturn( result/*:Boolean*/ ) {

    if ( !result )
        eventListener.onControlRequestFailed();
}
function rControlEndReturn( result/*:Boolean*/ ) {}
function rCancelControlRequestReturn( result/*:Boolean*/ ) {}

function createController() {

    // 挿入対象のDIVを用意
    var targetElement = document.createElement( "div" );
    targetElement.id = "N2MYSharing2ControlManagerContainer";
    document.body.appendChild( targetElement );
    var jsConnectionId = ( new Date() ).getTime();
    var flashProxy = new FlashProxy( jsConnectionId, "/sharing/jsgw/JavaScriptFlashGateway.swf" );
    var flashTag = new FlashTag( "/sharing/flash/N2MYSharing2ControlManager.swf", 1, 1 );
    flashTag.setId( "N2MYSharing2ControlManager" );
    flashTag.setFlashvars( "jsConnectionId=" + jsConnectionId );
    targetElement.innerHTML = flashTag.toString();
    targetElement.style.position = "absolute";
    targetElement.style.visibility = "hidden";
    targetElement.style.left = -9999;

    return flashProxy;
}




var eventListener = null;
function setEventListener( listener ) { eventListener = listener; }

function viewSharingStart( sharingId, serverAddress, port ) {
    //var ocx = document.getElementById( "N2MYRS2" );
    var ocx = sharing2.getSharing2Object();

    // from Flash
    ocx.Server		= String( serverAddress );
    ocx.Port		= Number( port );
    ocx.SharingID	= String( sharingId );

    // Control Mode
    ocx.Control		= Constants.CONTROL_OFF;

    var startResult = ocx.Start();
    if ( startResult == 0 ) {
        eventListener.onSharingStart();
    } else {
        alert( "--- {/literal}{$__frame.service_info.share_two|escape:'html'}{literal} ---\n{/literal}{t}6798,_('シェアリングの開始に失敗しました'){/t}{literal}\nErrorCode : " + startResult );
    }
}

function viewSharingStop() {

    //var ocx = document.getElementById( "N2MYRS2" );
    var ocx = sharing2.getSharing2Object();
    ocx.Stop();
}

function setControlMode( mode ) {

    //var ocx = document.getElementById( "N2MYRS2" );
    var ocx = sharing2.getSharing2Object();
    ocx.Control = mode;
}

function getVersion() {
    return sharing2.getSharing2Object().GetVersion();
    //return document.getElementById( "N2MYRS2" ).GetVersion();
}
{/literal}