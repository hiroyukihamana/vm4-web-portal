━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Merci d'utiliser V-cube Meeting

Votre demande pour un nouveau mot de passe a été refusé par l'administrateur du compte. Veuillez le contacter pour plus d'information.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■Developpé et Géré par:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Service client:
Téléphone : 0570-002192（Open 24/7）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
