*Veuillez ne pas répondre à cet email.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} a annulé la réunion plannifiée.

■Titre
{$info.reservation_name}

■Date et heure
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■Organisateur
{$info.organizer.name} 

------------------------------
Message de {$info.sender} 
------------------------------
{$info.mail_body}

※Cette invitation a été envoyée automatiquement à partir du système. 
Veuillez envoyer vos demandes pour cette réunion à cette adresse e-mail. 
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
À propos de V-CUBE Meeting
------------------------------
V-CUBE Meeting est un système de webconférence hebergé. Il peut être utilisé instantanément à partir de presque n'importe quel dispositif internet. 

Les utilisateurs peuvent partager des vidéos, des documents, et ainsi accroître la productivité.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Developpé et Géré par:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
