*Veuillez ne pas répondre à cet email.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Merci d'avoir choisi notre service de webconférence. V-cube a envoyé une notification de modification à tous les participants.

■Sujet
{$info.reservation_name}

■Date et heure
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■Organisateur
{$info.organizer.name} 

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive{/if}Mot de passe
{if $info.reservation_pw}{$info.reservation_pw}{else}Pas de mot de passe{/if}

------------------------------
Autres options de la réunion 
------------------------------
{if $info.pin_cd}
■Code d'accès pour V-CUBE mobile 
{$info.pin_cd}
※Vous pouvez accéder à la réunion avec iPad / Android. Démarrez l'application V-CUBE Mobile sur iPad / Android, allez dans "Connexion avec le code d'accès" et entrez ce code d'accès. 
{/if}
{if ($temporarySipNumberAddress||$temporarySipAddress||$temporaryH323NumberAddress||$temporaryH323Address) && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■Adresse invité de la réunion pour le terminal de vidéoconférence 
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※Entrez cette adresse de réunion lorsque vous vous connectez à V-CUBE Meeting à partir d'un terminal de vidéoconférence tel que Polycom. 
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■Numéro et code de téléconférence
{if $teleconf_info.tc_type == 'pgi'}
Appelez le Numéro d'accès, écouter l'invite de commande et entrez le code d'accès.
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Numéro d'accès:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- Autres numéros d'accès:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- Code d'accès:
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}


■Participants à la réunion
L'invitation a été envoyé à tous les participants

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (Public){elseif $guest.type == 2} (Lancer le tableau blanc ){/if}
　{$guest.email}
　Invitation URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if} 
{/foreach}{/if}

- Message pour les participants
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Developpé et Géré par:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
