﻿※本郵件為發信專用郵件，無法回信。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
來自V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
歡迎使用網絡視訊會議系統。邀請您參加現在正在召開的以下會議。
您只要點擊「邀請URL」，就可以參加網絡視訊會議。

■邀請URL
{if $info.guest_url_format ==1}
{$invitation_url}
{else}
<{$invitation_url}>
{/if}
{if $reservationPassword !== ""}

■密碼
{if $reservationPassword}{$reservationPassword}{else}无{/if}{/if}

------------------------------
會議的詳細資訊
------------------------------
{if $meeting_info.pin_cd}
■V-CUBE Mobile使用的驗證碼
{$meeting_info.pin_cd}
※通過iPad/Android啟動V-CUBE Mobile的應用程式，
在「用驗證碼登入」中輸入驗證碼，請確認下方的使用介紹。

※V-CUBE Mobile應用程式的獲取方法，请确认下方的使用介绍。
{/if}
{if ($temporarySipNumberAddress||$temporarySipAddress||$temporaryH323NumberAddress||$temporaryH323Address) && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■會議系統專用受邀者地址
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※從Polycom等電視會議終端連接V-CUBE Meeting時，請使用該地址進入會議室。
{/if}
{if $room_info.options.telephone == "1" && $meeting_info.pin_cd}

■從電話號碼一覽
從電話號碼一覽・根據語音提示輸入以下驗證碼就可以參加網路視訊會議。
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■驗證碼
{$meeting_info.pin_cd}
{/if}
{if $teleconf_info.pgi_conference_id && (!$meeting_info.is_reserved && $teleconf_info.tc_type == 'voip') || ($meeting_info.is_reserved && $teleconf_info.tc_type != 'voip') }

■電話會議號碼和密碼
如果你沒有密碼，請稍等片刻，或按＃。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- 訪問點（撥入）號碼:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
--  其它訪問點一覽:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- 密碼
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}

------------------------------
使用介紹
------------------------------
■使用方法
1. 將網絡攝像頭和耳機連接到電腦。
2. 請訪問環境確認頁面。
  可以方便地診斷網絡或連接設備的狀況。
{if $info.guest_url_format ==1}
   {$base_url}services/tools/checker/
{else}
   <{$base_url}services/tools/checker/>
{/if}
3.從以上「邀請URL」進入會議室。

※進入會議室後，如果已連接攝像頭或耳機，從會議室退出，再次進入會議室，識別視頻・語音。
※如果沒有準備網絡攝像頭，可以只通過語音參加會議。

■請在運行環境頁面對推薦運行環境進行確認。
{if $info.guest_url_format ==1}
http://www.nice2meet.us/ja/requirements/meeting.html
{else}
<http://www.nice2meet.us/ja/requirements/meeting.html>
{/if}

■V-CUBE Mobile 應用程式的獲取方法 您可以從智能手機或平板電腦終端（iPad/Android）下載應用程式後使用。
・iOS用應用程式
{if $info.guest_url_format ==1}
http://itunes.apple.com/jp/app/id431070449?mt=
{else}
<http://itunes.apple.com/jp/app/id431070449?mt=>
{/if}
・Android用應用程式
{if $info.guest_url_format ==1}
https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4
{else}
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>
{/if}

------------------------------
什麼是V-CUBE Meeting？
------------------------------
網絡視訊會議系統「V-CUBE Meeting」只要有網絡和電腦就可以簡便地召開會議。
不僅可以看著對方的表情進行交流，還可以將資料進行全員共用，錄製會議，並作為會議記錄使用。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
{if $info.guest_url_format ==1}{$smarty.const.N2MY_BASE_URL}{else}<{$smarty.const.N2MY_BASE_URL}>{/if} 
■開發・運營:
V-cube株式會社 {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 東京都目黑區上目黑2-1-1 中目黑GT Tower 20F
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
