■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
         {$info.sender} 様からウェブテレビ会議ご招待のお知らせ
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
        ウェブテレビ会議システム "V-CUBE ミーティング" を利用した
                       会議への招待のお知らせです。
      　下記の日時になりましたら、URLをクリックすることにより
              ウェブテレビ会議に参加することができます。
{if $info.room_option.telephone == "1" && $info.pin_cd}
         固定電話・携帯電話からは、接続する国の電話番号を入力し
            ガイダンスに従って暗証番号を入力することにより
              ウェブテレビ会議に参加することができます。
{/if}
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ 会議名
 　　{$info.reservation_name}

　□ 開催日時
　 　{$guest.starttime}
　 　～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

　□ URL
　 　{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}

　□ {if $info.reservation_pw && $info.reservation_pw_type == 2}会議記録{/if}パスワード
　　 {if $info.reservation_pw}{$info.reservation_pw}{else}無し{/if}

{if $info.room_option.telephone == "1"}

　□ 電話番号一覧
{foreach from=$telephone_data item=location}
　　 {$location.name} :
{foreach from=$location.list item=tel_no}
　　   {$tel_no}
{/foreach}

{/foreach}
{/if}
{if $info.pin_cd}

　□ 暗証番号
　　 {$info.pin_cd}
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

　□ 音声通話タイプ
{if $teleconf_info.tc_type == 'pgi'}
　　電話会議
　　 ・ダイヤルイン番号
{foreach from=$teleconf_info.pgi_phone_numbers item=pgi_phone}
　　　　{$pgi_phone.Number}  ({$pgi_phone.Location})
{/foreach}
　　 ・参加者パスコード
{if $guest.type == 1}
　　　 {$teleconf_info.pgi_l_pass_code|wordwrap:3:" ":true} (オーディエンス(仮）)
{else}
　　　 {$teleconf_info.pgi_p_pass_code|wordwrap:3:" ":true} (招待：通常ユーザ（仮）)
{/if}
{elseif $teleconf_info.tc_type == 'voip'}
　　VoIP(ウェブ会議を利用する端末上で、音声通話を行う)
{elseif $teleconf_info.tc_type == 'etc'}
　　他社の電話会議サービスを使用する
　　　　{$teleconf_info.tc_teleconf_note}
{/if}
{/if}

　□ {$info.sender} 様からのメッセージ
　　 {$info.mail_body}

　□ 招待内容のお問い合わせ先
　　 {$info.sender_mail}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　　　　ご利用案内
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

　□ ご利用の前に
 　　１．Webカメラとヘッドセットをパソコンに接続してください。
　　　　※会議室へ入室後にカメラ及びヘッドセットを接続すると、
 　  　　音声・映像が取得できず、再入室が必要になります。
　　　　※Webカメラがなくても、音声のみでのご参加が可能です。
　 　２．以下の環境確認ページにアクセスしてください。
　　　　{$base_url}services/tools/checker/
　　　　ネットワーク環境、カメラ、マイクの設定が簡易診断できます。
     ３．時間になったら、上記の「URL」から会議室に入室してください。
{if $info.pin_cd}
　　　　固定電話・携帯電話からのご参加は、接続する国の電話番号を入力し、
　　　　ガイダンスに従って暗証番号を入力して入室してください。
{/if}

　□ 推奨動作環境
　　 以下動作環境ページにてご確認ください。
　   <http://www.nice2meet.us/ja/requirements/meeting.html>

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　  ウェブテレビ会議システム "V-CUBE ミーティング" とは？
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

　　"V-CUBE ミーティング" は、インターネット上の仮想会議室です。
　　指定したアドレスにアクセスするだけで、遠く離れた場所にいても
　　お互いの顔を見ながら映像と音声によるウェブテレビ会議が可能です。
　
　　また、共有ホワイトボード機能、会議の録画機能、デスクトップ共有機能、
　　プレゼンス機能等、会議を円滑に進めるための機能が満載です。

 ======================================================================

  　 ■ウェブテレビ会議システム「V-CUBE ミーティング」
　　 {$base_url}

　　 ■開発・運営：株式会社ブイキューブ
　　 http://www.vcube.co.jp/
 　　　
　　 ■カスタマーサポートセンター　0570-002192（24時間365日電話受付）

 ======================================================================
