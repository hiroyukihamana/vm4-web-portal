※このメールは送信専用のため、ご返信いただけません。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBEミーティングからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Web会議システムのご利用ありがとうございます。
下記の会議の招待メールを、参加者に送信しました。

■会議名
{$info.reservation_name} 

■開催日時
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■主催者
{$info.organizer.name} 

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会議記録{/if}パスワード
{if $info.reservation_pw}{$info.reservation_pw}{else}なし{/if} 

------------------------------
会議の詳細情報
------------------------------
{if $info.pin_cd}
■V-CUBEモバイル用 暗証番号
{$info.pin_cd} 
※iPad/AndroidからV-CUBEモバイルのアプリを起動し、
「暗証番号でログイン」から暗証番号を入力することでも、
Web会議への参加が可能です。
{/if}

■会議の参加者
下記の参加者に対し、予約会議の招待メールを送信しました。

{if $guests}{foreach from=$guests item=guest}
{if !$guest.r_organizer_flg}
- {$guest.name} 様{if $guest.type == 1} (オーディエンス){elseif $guest.type == 2} (資料ユーザ){/if} 
　{$guest.email} 
　招待URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if} 
{/if}
{/foreach}{/if}

- 参加者へのメッセージ
{$info.mail_body} 

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
(本件問合せ先)
ビジネスプラス・コンシェルジュデスク
TEL：0120-688-360（フリーダイヤル）
受付時間：平日9：30 ～17:30　(土・日・祝日および年末年始は休業)
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
