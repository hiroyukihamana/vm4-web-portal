━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBEE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
感谢使用视频会议系统。

密码重置申请已被拒绝。
详细情况请咨询管理员。


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■开发・运营:
V-cube株式会社 <http://www.vcube.com/>
〒153-0051 东京都目黑区上目黑2-1-1 中目黑GT Tower 20F
■客服中心
电话号码: 0570-07-0710(9 am to 5pm on Weekdays)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
