﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
来自V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
请您注意预定的会议即将开始
------------------------------
{$info.sender}向您发送了以下网络视频会议的邀请。

您可以在指定时间点击邀请URL，参加网络视频会议。

■会议名称
{$info.reservation_name}

■召开时间
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■邀请URL
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会议记录{/if}密码
{if $info.reservation_pw}{$info.reservation_pw}{else}无{/if}

------------------------------
来自{$info.sender}的消息
------------------------------
{$info.mail_body}

※此邮件已通过系统自动发送。
　关于此会议的邀请，请咨询以下地址。
　<{$info.sender_mail}>

------------------------------
会议的详细信息
------------------------------
{if $info.pin_cd}
■V-CUBE Mobile使用的验证码
{$info.pin_cd}
※通过iPad/Android启动V-CUBE Mobile的应用程序，
在「用验证码登陆」中输入验证码，也可以参加网络视频会议。

※V-CUBE Mobile应用程序的获取方法，请确认下方的使用介绍。
{/if}
{if $temporarySipAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■会议系统专用受邀者地址
・SIP
{$temporarySipNumberAddress}
{$temporarySipAddress}
 ・H.323
{$temporaryH323NumberAddress}
{$temporaryH323Address}
※从Polycom等电视会议终端连接V-CUBE Meeting时，请使用该地址进入会议室。
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■电话会议号码和密码
{if $teleconf_info.tc_type == 'pgi'}
如果你没有密码，请稍等片刻，或按＃。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- 访问点（拨入）号码:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
--  其它访问点一览:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- 密码
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

------------------------------
使用介绍
------------------------------
■使用方法
1. 将网络摄像头和耳机连接到电脑。
2. 请访问环境确认页面。
  可以方便地诊断网络或连接设备的状况。
   <{$base_url}services/tools/checker/>
3.从以上「邀请URL」进入会议室。

※进入会议室后，如果已连接摄像头或耳机，从会议室退出，再次进入会议室，识别视频・语音。
※如果没有准备网络摄像头，可以只通过语音参加会议。

■请在推荐运行环境运行环境页面进行确认。
<http://www.nice2meet.us/ja/requirements/meeting.html>

■V-CUBE Mobile 应用程序的获取方法 您可以从智能手机或平板电脑终端（iPad/Android）下载应用程序后使用。
・iOS用应用程序
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Android用应用程序
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>

------------------------------
什么是V-CUBE Meeting？
------------------------------
网络视频会议系统「V-CUBE Meeting」只要有网络和电脑就可以简便地召开会议。
不仅可以看着对方的表情进行交流，还可以将资料进行全员共享，录制会议，并作为会议记录使用。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■开发・运营:
V-cube株式会社 <http://www.vcube.com/>
〒153-0051 东京都目黑区上目黑2-1-1 中目黑GT Tower 20F
■客服中心
电话号码: 0570-07-0710(9 am to 5pm on Weekdays)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
