﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
来自V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender}发出邀请的以下网络视频会议中止，特此通知。

■会议名称{$info.reservation_name}

■预约的召开时间
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

------------------------------
来自{$info.sender}的消息
------------------------------
{$info.mail_body}

※此邮件已通过系统自动发送。
　关于此会议的咨询，请联系以下地址。
　<{$info.sender_mail}>

------------------------------
什么是V-CUBE Meeting？
------------------------------
网络视频会议系统「V-CUBE Meeting」只要有网络和电脑就可以简便地召开会议。
不仅可以看着对方的表情进行交流，还可以将资料进行全员共享，录制会议，并作为会议记录使用。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■开发・运营:
V-cube株式会社 <http://www.vcube.com/>
〒153-0051 东京都目黑区上目黑2-1-1 中目黑GT Tower 20F
■客服中心
电话号码: 0570-07-0710(9 am to 5pm on Weekdays)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
