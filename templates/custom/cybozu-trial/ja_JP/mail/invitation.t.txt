━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBEミーティングからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Web会議システムのご利用ありがとうございます。
現在開催している下記の会議に招待されました。
「招待URL」をクリックすると、Web会議に参加することができます。

■招待URL
<{$invitation_url}>
{if $reservationPassword !== ""}

■パスワード
{if $reservationPassword}{$reservationPassword}{else}なし{/if}{/if}

------------------------------
会議の詳細情報
------------------------------
{if $meeting_info.pin_cd}
■V-CUBEモバイル用 暗証番号
{$meeting_info.pin_cd}
※iPad/AndroidからV-CUBEモバイルのアプリを起動し、
「暗証番号でログイン」から暗証番号を入力することでも、
Web会議への参加が可能です。

※V-CUBEモバイルアプリの入手方法は、下部のご利用案内をご確認ください。
{/if}
{if $temporarySipAddress && !$reservationPassword}

■テレビ会議システム専用ゲストアドレス
・SIP
{$temporarySipNumberAddress}
{$temporarySipAddress}
 ・H.323
{$temporaryH323NumberAddress}
{$temporaryH323Address}
※Polycom等のテレビ会議端末からV-CUBEミーティングに接続する場合はこちらのアドレスを入力して入室してください。
{/if}
{if $room_info.options.telephone == "1" && $meeting_info.pin_cd}

■電話番号一覧
固定電話・携帯電話から接続する国の電話番号を入力し、
音声ガイダンスに従って下記の暗証番号を入力することで、
Web会議にご参加頂けます。
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■暗証番号
{$meeting_info.pin_cd}
{/if}
{if $teleconf_info.pgi_conference_id && (!$meeting_info.is_reserved && $teleconf_info.tc_type == 'voip') || ($meeting_info.is_reserved && $teleconf_info.tc_type != 'voip') }

■電話会議での参加方法
{if $teleconf_info.pgi_conference_id}
アクセスポイントに電話をかけてガイダンスに従い、パスコードを入力します。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- アクセスポイント（ダイヤルイン）番号:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- その他のアクセスポイント一覧:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- パスコード
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

------------------------------
ご利用案内
------------------------------
■ご利用方法
1. Webカメラとヘッドセットを、パソコンに接続します。
2. 環境確認ページにアクセスしてください。
   ネットワークや接続機器の状況が簡易診断できます。
   <{$base_url}services/tools/checker/>
3. 上記の「招待URL」から会議室に入室します。

※会議室に入室した後、カメラやヘッドセットを接続した場合、
　一度会議室から退室し、再入室をすれば映像・音声を認識します。
※Webカメラのご用意がない場合、音声のみのご参加も可能です。

■推奨動作環境
動作環境ページにてご確認ください。
<http://www.nice2meet.us/ja/requirements/meeting.html>

■V-CUBEモバイル　アプリケーションの入手方法
スマートフォンやタブレット端末（iPad/Android）からは、
アプリをダウンロードいただくことでご利用いただけます。
・iOS用アプリケーション
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Android用アプリケーション
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>

------------------------------
V-CUBEミーティングとは？
------------------------------
Web会議システム「V-CUBEミーティング」は、
インターネット回線とパソコンさえあればカンタンに会議ができます。

相手の顔を見て話ができるだけでなく、1つの資料を全員で共有したり、
会議を録画して議事録として利用することができます。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBEミーティング
<{$smarty.const.N2MY_BASE_URL}>
■開発・運営:
株式会社ブイキューブ <http://www.vcube.com/>
〒153-0051 東京都目黒区上目黒2-1-1 中目黒GTタワー20F
■カスタマーサポートセンター
電話番号: 0570-07-0710（平日午前9時～午後5時　電話受付）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
