━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari Pertemuan V-CUBE
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Terima kasih telah menggunakan service web konferensi kami.
V-CUBE telah menjadwalkan pertemuan anda kepada semua undangan email.

■Topik
{$info.reservation_name}

■Tanggal dan Waktu
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Arsip{/if}Kata sandi
{if $info.reservation_pw}{$info.reservation_pw}{else}Tidak ada Kata Sandi{/if}

------------------------------
Opsi pertemuan lainnya
------------------------------
{if $info.pin_cd}
■Passcode untuk V-CUBE Mobile
{$info.pin_cd}
※Anda dapat menghadiri pertemuan dengan iPad/Android. Jalankan aplikasi V-CUBE Mobile di iPad/Android, lanjutkan ke "Masuk dengan Passcode" dan masukan passcode ini.
{/if}
{if $temporarySipAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■Khusus alamat tamu untuk sistem pertemuan video
・SIP
{$temporarySipNumberAddress}
{$temporarySipAddress}
 ・H.323
{$temporaryH323NumberAddress}
{$temporaryH323Address}
※Masukkan alamat pertemuan berikut ketika anda terhubung dengan pertemuan V-CUBE dari system video konferensi seperti Polycom
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■Opsi konferensi
{if $teleconf_info.tc_type == 'pgi'}
Hubungi nomor Dial-In, dengarkan instruksi yang diberikan dan masukkan kode akses
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Nomor Dial-In:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- Nomor Dial-In yang lain:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- Kode Akses
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

■Informasi Penting
Undangan ini telah dikirimkan ke semua partisipan

{if $guests}{foreach from=$guests item=guest}
- {$guest.name}  {if $guest.type == 1} (Penonton){elseif $guest.type == 2} (Whiteboard){/if}
　{$guest.email}
　UndanganURL: <{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}>
{/foreach}{/if}

- Pesan untuk para partisipan
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■Pertemuan V-CUBE
<{$base_url}>
■Dikembangkan dan dikelola oleh:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
telepon: 0570-07-0710(9 am to 5pm on Weekdays)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
