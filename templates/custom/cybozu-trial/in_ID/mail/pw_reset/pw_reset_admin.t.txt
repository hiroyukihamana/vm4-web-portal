━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari Pertemuan V-CUBE
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Terima kasih telah menggunakan Pertemuan V-CUBE

Kami telah menerima permohonan perubahan kata sandi anda. 
Silahkan konfirmasi dan menerima atau menolah permohonan berikut.

■ID Pengguna
{$member_id}

■Nama Pengirim Permohonan
{$name}

■Klik tautan berikut untuk menerima permohonan ini
<{$smarty.const.N2MY_BASE_URL}a/{$authorize_key}>

■Klik tautan berikut untuk menolak permohonan ini
<{$smarty.const.N2MY_BASE_URL}k/{$ignore_key}>

※Permohonan diatas akan berakhir dalam 24 jam


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■Pertemuan V-CUBE
<{$smarty.const.N2MY_BASE_URL}>
■Dikembangkan dan dikelola oleh:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
telepon: 0570-002192（Buka 24/7）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
