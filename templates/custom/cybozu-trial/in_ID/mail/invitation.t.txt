━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari Pertemuan V-CUBE
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Terima kasih telah menggunakan servis web konferensi kami.
Anda telah diundang untuk menghadiri pertemuan yang sedang berlangsung.
Klik tautan undangan dibawah ini untuk menghadiri pertemuan.

■Tautan aman menuju ke ruangan pertemuan
<{$invitation_url}>
{if $reservationPassword !== ""}

■Kata sandi
{if $reservationPassword}{$reservationPassword}{else}なし{/if}{/if}

------------------------------
opsi pertemuan lainnya.
------------------------------
{if $meeting_info.pin_cd}
■Passcode untuk V-CUBE Mobile
{$meeting_info.pin_cd}
※Anda dapat menghadiri pertemuan dengan iPad/Android. Jalankan aplikasi V-CUBE Mobile di iPad/Android, lanjutkan ke "Masuk dengan Passcode" dan masukan passcode ini.

※Periksa cara penggunaan dibawah ini bagaimana mendapatkan aplikasi V-CUBE Mobile
{/if}
{if $temporarySipAddress && !$reservationPassword}

■Khusus alamat tamu untuk sistem pertemuan video
・SIP
{$temporarySipNumberAddress}
{$temporarySipAddress}
 ・H.323
{$temporaryH323NumberAddress}
{$temporaryH323Address}
※Masukkan alamat pertemuan berikut ketika anda terhubung dengan pertemuan V-CUBE dari system video konferensi seperti Polycom
{/if}
{if $teleconf_info.pgi_conference_id && (!$meeting_info.is_reserved && $teleconf_info.tc_type == 'voip') || ($meeting_info.is_reserved && $teleconf_info.tc_type != 'voip') }

■Opsi konferensi
{if $teleconf_info.pgi_conference_id}
Hubungi nomor Dial-In, dengarkan instruksi yang diberikan dan masukkan kode akses
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Nomor Dial-In:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- Nomor Dial-In yang lain:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- Kode Akses
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

------------------------------
Informasi Penting
------------------------------
■Bagaimana menggunakan Pertemuan V-CUBE
1. Hubungkan sebuah Webcam dan sebuah Headset ke PC anda
2. Klik dan jalankan alat pengecek dibawah
Itu akan membantu anda mengatur pengaturan perangkat dan konektifitas jaringan yg terbaik <{$base_url}services/tools/checker/>
3. Klik undangan tautan aman yang ada di atas email ini untuk masuk ke pertemuan

※Mohon tidak menyambungkan peralatan lainnya setelah anda men-klik undangan tautan aman, karena tidak akan bisa dipakai sampai antar mengulangi ruang pertemuannya.
※Jika anda tidak memiliki webcam, anda tetap dapat bergabung ke pertemuan dengan menggunakan headset USB.

■Persyaratan sistem
Silahkan mengacu ke halaman persyaratan sistem kami.
<http://www.nice2meet.us/ja/requirements/meeting.html>

■Dapatkan aplikasi V-CUBE Mobile
Dapatkan aplikasi untuk smartphone atau tablet (iPad/Android) dari tautan dibawah untuk menghadiri pertemuan.
・Aplikasi untuk  iOS
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Aplikasi untuk Android
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>

------------------------------
Tentang Pertemuan V-CUBE
------------------------------
Pertemuan V-CUBE adalah jaringan sistem video konferensi berbasis cloud. Ini dapat digunakan tanpa instalasi hampir dari semua peralatan internet.

Penguna dapat berbagi video, dokumen, arsip pertemuan dan meningkatkan produktifitas. Silahkan menggunakan sistem kami.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■Pertemuan V-CUBE
<{$smarty.const.N2MY_BASE_URL}>
■Dikembangkan dan dikelola oleh:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
telepon: 0570-07-0710(9 am to 5pm on Weekdays)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
