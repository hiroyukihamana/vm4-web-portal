━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari Pertermuan V-CUBE
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Terima kasih telah menggunakan Pertemuan V-CUBE

Permohonan anda telah kami terima, dan akan kami jawab secepatnya

------------------------------
Perincian permohonan
------------------------------
■Topik
{$subject}

■Perincian
{$body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■Pertemuan V-CUBE
<{$smarty.const.N2MY_BASE_URL}>
■Dikembangkan dan dikelola oleh:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
telepon: 0570-07-0710(9 am to 5pm on Weekdays)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
