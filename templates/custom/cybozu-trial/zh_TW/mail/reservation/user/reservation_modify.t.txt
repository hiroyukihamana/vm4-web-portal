﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
來自V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
由於來自{$info.sender}的網絡視訊會議的邀請內容發生變更，進行以下通知。

而且，您在以下時間點擊邀請URL，就可以參加網絡視訊會議。

■會議名稱
{$info.reservation_name}

■召開時間
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■邀請URL
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>

■{if $info.reservation_pw && $info.reservation_pw_type == 2}會議記錄{/if}密碼
{if $info.reservation_pw}{$info.reservation_pw}{else}無{/if}

------------------------------
來自{$info.sender}的消息
------------------------------
{$info.mail_body}

※此郵件已通過系統自動發送。
　關於此會議的邀請，請點擊以下地址。
　<{$info.sender_mail}>

------------------------------
會議的詳細資訊
------------------------------
{if $info.pin_cd}
■V-CUBE Mobile使用的驗證碼
{$info.pin_cd}
※通過iPad/Android啟動V-CUBE Mobile的應用程式，
在「用驗證碼登陸」中輸入驗證碼，也可以參加網絡視訊會議。

※V-CUBE Mobile應用程式的獲取方法，請確認下方的使用介紹。
{/if}
{if $temporarySipAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■會議系統專用受邀者地址
・SIP
{$temporarySipNumberAddress}
{$temporarySipAddress}
 ・H.323
{$temporaryH323NumberAddress}
{$temporaryH323Address}
※從Polycom等電視會議終端連接V-CUBE Meeting時，請使用該地址進入會議室。
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■電話會議號碼和密碼
{if $teleconf_info.tc_type == 'pgi'}
如果你沒有密碼，請稍等片刻，或按＃。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- 訪問點（撥入）號碼:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
--  其它訪問點一覽:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- 密碼
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

------------------------------
使用介紹
------------------------------
■使用方法
1. 將網絡攝像頭和耳機連接到電腦。
2. 請訪問環境確認頁面。
  可以方便地診斷網絡或連接設備的狀況。
   <{$base_url}services/tools/checker/>
3.從以上「邀請URL」進入會議室。

※進入會議室後，如果已連接攝像頭或耳機，從會議室退出，再次進入會議室，識別視頻・語音。
※如果沒有準備網絡攝像頭，可以只通過語音參加會議。

■請在運行環境頁面對推薦運行環境進行確認。
<http://www.nice2meet.us/ja/requirements/meeting.html>

■V-CUBE Mobile 應用程式的獲取方法 您可以從智慧手機或平板電腦終端（iPad/Android）下載應用程式後使用。
・iOS用應用程式
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Android用應用程式
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>

------------------------------
什麼是V-CUBE Meeting？
------------------------------
網絡視訊會議系統「V-CUBE Meeting」只要有網絡和電腦就可以簡便地召開會議。
不僅可以看著對方的表情進行交流，還可以將資料進行全員共用，錄製會議，並作為會議記錄使用。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■開發・運營:
V-cube株式會社 <http://www.vcube.com/>
〒153-0051 東京都目黑區上目黑2-1-1 中目黑GT Tower 20F
■客服中心
電話號碼: 0570-07-0710(9 am to 5pm on Weekdays)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
