﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

來自V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
歡迎使用網絡視訊會議系統。收到您的提問，請您稍等。

------------------------------
提問內容
------------------------------
{if $ask_category == 0}關於服務
{elseif $ask_category == 1}關於使用費
{elseif $ask_category == 2}關於會議預約的變更
{elseif $ask_category == 3}關於會議預約的取消
{elseif $ask_category == 4}其它{/if}

{$ask_contents}

------------------------------
註冊內容
------------------------------
■公司名稱 : {$ask_company_name}
■部門名稱 : {$ask_company_division}
■電話號碼 : {$ask_telephone}
■FAX號碼 : {$ask_fax}
■擔當名 : {$ask_name}
■E-mail address : <{$ask_email}>

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■開發・運營:
V-cube株式會社 <http://www.vcube.com/>
〒153-0051 東京都目黑區上目黑2-1-1 中目黑GT Tower 20F
■客服中心
電話號碼: 0570-07-0710(9 am to 5pm on Weekdays)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
