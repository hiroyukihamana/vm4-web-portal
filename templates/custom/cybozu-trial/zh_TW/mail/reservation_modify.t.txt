﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
來自V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
歡迎使用網絡視訊會議系統。
以下會議的變更內容已通過郵件發送給參與者。

■會議名稱
{$info.reservation_name}

■召開時間
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■{if $info.reservation_pw && $info.reservation_pw_type == 2}會議記錄{/if}密碼
{if $info.reservation_pw}{$info.reservation_pw}{else}無{/if}

------------------------------
會議的詳細資訊
------------------------------
{if $info.pin_cd}
■V-CUBE Mobile使用的驗證碼
{$info.pin_cd}
※通過iPad/Android啟動V-CUBE Mobile的應用程式，
在「用驗證碼登入」中輸入驗證碼，也可以參加網絡視訊會議。
{/if}
{if $temporarySipAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■會議系統專用受邀者地址
・SIP
{$temporarySipNumberAddress}
{$temporarySipAddress}
 ・H.323
{$temporaryH323NumberAddress}
{$temporaryH323Address}
※從Polycom等電視會議終端連接V-CUBE Meeting時，請使用該地址進入會議室。
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■電話會議號碼和密碼
{if $teleconf_info.tc_type == 'pgi'}
如果你沒有密碼，請稍等片刻，或按＃。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- 訪問點（撥入）號碼:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
--  其它訪問點一覽:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- 密碼
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

■會議的參加者已向以下參與者發送預約會議變更的通知郵件。

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (听众){elseif $guest.type == 2} (资料用户){/if}
　{$guest.email}
　邀请URL: <{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/foreach}{/if}

- 發給參加者的消息
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■開發・運營:
V-cube株式會社 <http://www.vcube.com/>
〒153-0051 東京都目黑區上目黑2-1-1 中目黑GT Tower 20F
■客服中心
電話號碼: 0570-07-0710(9 am to 5pm on Weekdays)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
