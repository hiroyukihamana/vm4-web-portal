*Veuillez ne pas répondre à cet email.

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　Notification V-CUBE Vente & Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
              　　　Merci d'avoir utilisé V-CUBE Vente & Support.
La réunion prévue va bientôt commencer.
Cliquer sur l'URL d'invitation ci-dessous pour y participer.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ Sujet
　　 {$info.reservation_name}

　□ Date et heure
　　 {$info.reservation_starttime}
　　 〜 {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

　□ Mot de passe
　　 {if $info.reservation_pw}{$info.reservation_pw}{else}Pas de mot de passe{/if}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　　　　Les participants
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
{if $guests}{foreach from=$guests item=guest}
　□ Nom: {$guest.name}  {if $guest.type == 1} (Audience){elseif $guest.type == 2} (Document User){/if}

　□ E-mail Address: {$guest.email}
　□ {if $guest.member_key == ""}Customer{else}Staff{/if}URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if}

　□ Date et heure: {$guest.starttime}
　　           〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})
-----------------------------------------------------------------------
{/foreach}{/if}

　□ Message
　　 {$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Vente & Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Développé et géré par :
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051 
■Centre de support client (japonais uniquement)
Tel: 0570-002192 (Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Centre de support client (Anglais & autres)
Tel:
　Tokyo　+81-3-4560-1287
　Malaysia　+60-3-7724-9693
　Singapore　+65-3158-2832
　China　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Heure de support : Jours de la semaine 9:00 - 18:00 GMT+8
Le support n'est pas disponible pendant les week-ends et jours fériés en Malaysie.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
