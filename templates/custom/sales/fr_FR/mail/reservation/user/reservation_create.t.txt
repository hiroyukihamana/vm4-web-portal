*Veuillez ne pas répondre à cet email.

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
     Notification d'invitation du service Vente & Support de V-CUBE
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
{$info.sender}vous a invité à rejoindre un meeting web en utilisant "V-CUBE Vente & Support"

Cliquez sur l'URL d'invitation ci-dessous pour commencer à participer au meeting.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ Sujet
 　　{$info.reservation_name}

　□ Date et heure
　 　{$guest.starttime}
　 　〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

　□ {if $guest.member_key == ""}Client{else}Personnel{/if}URL
{if $info.guest_url_format == 1}
　 　{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
　 　<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

　□ Mot de passe
　　 {if $info.reservation_pw}{$info.reservation_pw}{else}Pas de mot de passe{/if}

　□ Message de  {$info.sender}
　　 {$info.mail_body}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　  Informations importantes
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

　□ Comment utilisler V-CUBE Vente & Support
     1. Connecter une webcam et un casque à votre PC
     2. Cliquer et exécuter "Outil de vérification de l'environnement
     3. Cliquer sur le lien d'invitation sécurisé pour entrer dans le meeting
     * Ne branchez pas de matériel supplémentaire après avoir cliqué sur le lien d'invitation sécurisé. Il ne sera pas disponible avant de redémarrer la réunion.
     * Si vous n'avez pas de webcam, vous pouvez rejoindre la réunion avec un casque USB.

　□ Vérifiez l'environnement Check Tool
     C'est un outil de vérification d'environnement V-CUBE (version simplifiée)
     Il vérifiera automatiquement l'environnement réseau, la caméra et le micro
Accéder à l'URL ci-dessous, sélectionner tous les champs et cliquer sur "Début de la vérification"

  　 {if $info.guest_url_format ==1}{$base_url}services/tools/checker/{else}<{$base_url}services/tools/checker/>{/if}

　□ Système requis
　　 Veuillez vous référer à notre page d'exigences système
　   {if $info.guest_url_format ==1}http://www.nice2meet.us/ja/requirements/sales.html{else}<http://www.nice2meet.us/ja/requirements/sales.html>{/if}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　 À propos de V-CUBE Vente & Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

V-CUBE Vente & Support est un système de vidéo-conférence web sur le cloud. En accédant à l'adresse spécifiée, les utilisateurs peuvent avoir des communications face à face en utilisant vidéo et audio.
Il y a de nombreux outils de communication comme le tableau blanc, l'enregistrement, et les fonctionnalités de partage de bureau PC.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Vente & Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Développé et géré par :
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Centre de support client (japonais uniquement)
Tel: 0570-002192 (Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if}
■Centre de support client (Anglais & autres)
Tel:
　Tokyo　+81-3-4560-1287
　Malaysia　+60-3-7724-9693
　Singapore　+65-3158-2832
　China　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if}
Heure de support : Jours de la semaine 9:00 - 18:00 GMT+8
Le support n'est pas disponible pendant les week-ends et jours fériés en Malaysie.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
