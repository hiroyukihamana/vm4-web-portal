*This email is transmission only, please do not reply.

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
           Notice of Cancellation from V-CUBE Sales & Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
    {$info.sender} has cancelled a meeting using "V-CUBE Sales & Support".
    Following meeting has been cancelled.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　■ Title
　　 {$info.reservation_name}

　■ Date and Time
　　 {$guest.starttime}
　　 〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

　■ Message from {$info.sender}
　　 {$info.mail_body}


■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　  About V-CUBE Sales&Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

V-CUBE Sales & Support is a cloud-based video web conferencing system. 
By accessing specified address, users can have face to face communication using video and audio.
There are many tools for communication such as whiteboard, recording, 
and PC desktop sharing feature.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Sales & Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Developed and Managed by:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center (Japanese Only)
Tel: 0570-002192 (Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center (English & Others)
Tel:
　Tokyo　+81-3-4560-1287
　Malaysia　+60-3-7724-9693
　Singapore　+65-3158-2832
　China　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Support Time：Weekdays 9:00-18:00(GMT+8)
Support not available during weekends and public holidays in Malaysia
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
