*This email is transmission only, please do not reply.

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
       V-CUBE Sales & Support: Notice of Change Sent
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
               Thank you for using V-CUBE Sales & Support.
V-cube has sent change notice for following meeting.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ Topic
　　 {$info.reservation_name}

　□ Date and Time
　　 {$info.reservation_starttime}
　　 〜 {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

　□ Password
　　 {if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}


■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　　　　Participants
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
{if $guests}{foreach from=$guests item=guest}
　□ Name: {$guest.name}  {if $guest.type == 1} (Audience){elseif $guest.type == 2} (Document User){/if}

　□ E-mail Address: {$guest.email}
　□ {if $guest.member_key == ""}Customer{else}Staff{/if}URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if}

　□ Date and Time: {$guest.starttime}
　　           〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})
-----------------------------------------------------------------------
{/foreach}{/if}

　□ Message
　　 {$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Sales & Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Developed and Managed by:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center (Japanese Only)
Tel: 0570-002192 (Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center (English & Others)
Tel:
　Tokyo　+81-3-4560-1287
　Malaysia　+60-3-7724-9693
　Singapore　+65-3158-2832
　China　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Support Time：Weekdays 9:00-18:00(GMT+8)
Support not available during weekends and public holidays in Malaysia
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
