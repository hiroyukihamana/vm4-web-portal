{t}9008,_('※このメールは送信専用のため、ご返信いただけません。'){/t} 

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{t}8894,_('V-CUBEセールス＆サポートからのお知らせ'){/t} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{t}8903,_('いつもV-CUBEセールス＆サポートをご利用頂きありがとうございます。
下記の会議の開催中止を承りました。
また、参加者への中止メール送信も完了しましたのでお知らせします。'){/t} 

■{t}2348,_('会議名'){/t} 
{$info.reservation_name}

■{t}9020,_('開催日時'){/t} 
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■{if $info.reservation_pw && $info.reservation_pw_type == 2}{t}6600,_('会議記録'){/t}{/if} {t}2349,_('パスワード'){/t} 
{if $info.reservation_pw}{$info.reservation_pw}{else}{t}8048,_('なし'){/t}{/if} 

■{t}9014,_('会議の参加者'){/t} 
　{if $guests}{foreach from=$guests item=guest} 
  {if $guest}
  {t}8899,_('お名前'){/t}: {$guest.name} {t}8063,_('様'){/t}{if $guest.type == 1} ({t}2363,_('オーディエンス'){/t}){elseif $guest.type == 2} ({t}8901,_('資料ユーザ'){/t}){/if} 
　{t}2379,_('メールアドレス'){/t}: {$guest.email}
  {if $guest.is_manager}{t}9439,_('監視者'){/t}{elseif $guest.member_key == ""}{t}8900,_('お客様'){/t}{else}{t}8496,_('スタッフ'){/t}{/if}URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if} 
  {t}9020,_('開催日時'){/t}: {$guest.starttime}
　　           〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})
-----------------------------------------------------------------------
  {/if}
{/foreach}{/if} 
- {t}9017,_('参加者へのメッセージ'){/t} 
{$info.mail_body} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■{t}8896,_('V-CUBEセールス＆サポート'){/t} 
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■{t}8927,_('開発・運営：'){/t} 
{t}8928,_('株式会社ブイキューブ'){/t} {if $info.guest_url_format ==1}{t}9449,_('http://www.vcube.com/'){/t} {else}<{t}9449,_('http://www.vcube.com/'){/t}>{/if} 
{t}8071,_('〒153-0051 東京都目黒区上目黒2-1-1 中目黒GTタワー20F'){/t} 
■{t}8888,_('カスタマーサポートセンター'){/t}{t}8838,_('（日本語専用）'){/t} 
{t}8889,_('電話番号'){/t} {t}9450,_('0570-002192'){/t}({t}8890,_('24時間365日電話受付'){/t}) 
E-mail: {if $info.guest_url_format ==1} {t}9451,_('vsupport@vcube.co.jp'){/t} {else} <{t}9451,_('vsupport@vcube.co.jp'){/t}>{/if} 
■{t}8888,_('カスタマーサポートセンター'){/t}{t}8839,_('(英語その他)'){/t} 
{t}8889,_('電話番号'){/t} 
　{t}8841,_('マレーシア'){/t}　+60-3-2202-4120
　{t}8842,_('シンガポール'){/t}　+65-6636-5862
　{t}8843,_('中国'){/t}　 +86-400-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
{t}8844,_('対応時間：平日'){/t} 9:00-18:00({t}9488,_('各国の現地時間'){/t}) 
{t}9489,_('土日及び各国の祝日は休業となります。'){/t} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━