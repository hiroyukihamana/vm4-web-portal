Ini hanya email transmisi, mohon untuk tidak membalasnya

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
       Pemberitahuan Pengingat Undangan dari V-CUBE Sales & Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
      　  　  {$info.sender} telah mengundang Anda untuk bergabung dalam pertemuan web menggunakan "V-CUBE Sales & Support".
Jadwal pertemuan sudah hampir tiba.
Klik URL undangan berikut ini pada saat memulai untuk menghadiri pertemuan.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ Topik
 　　{$info.reservation_name}

　□ Tanggal dan Waktu
　 　{$guest.starttime}
　 　〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

　□ {if $guest.member_key == ""}Pelanggan{else}Pegawai{/if}URL
{if $info.guest_url_format == 1}
　 　{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
　 　<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

　□ Kata Kunci
　　 {if $info.reservation_pw}{$info.reservation_pw}{else}Tanpa Kata Kunci{/if}

　□ Pesan dari {$info.sender}
　　 {$info.mail_body}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　Informasi Penting
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

　□ Bagaimnan menggunakan V-CUBE Sales & Support
     1. Menghubungkan kamera web dan headset ke PC Anda.
     2. Klik dan jalankan "Environment Check Tool" di bawah.
     3. Klik link undangan di atas untuk masuk ke pertemuan.
    ※Harap tidak menyambungkan perangkat keras tambahan apa pun setelah Anda me
     ngklik link undangan tersebut. Cara ini tidak akan berfungsi sampai Anda mem    ulai lagi ruang pertemuan.
    ※Jika kamera web tidak tersedia, Anda masih dapat mengikuti pertemuan denga
     n menggunakan headset USB.

　□ Periksa Environment Check Tool
     Ini adalah V-CUBE's Environment Chcek tool (Versi Sederhana).
     Ini secara otomatis akan mencek pengaturan lingkungan jaringan, kamera, dan
     mikrofon.
    Akses URL berkut, pilih bidang yang diinginkan dan klik tombol "Check start".
  　 {if $info.guest_url_format ==1}{$base_url}services/tools/checker/{else}<{$base_url}services/tools/checker/>{/if}

　□ Ketentuan-ketentuan Sistem
     Lihat halaman ketentuan-ketentuan sistem kami.
　   {if $info.guest_url_format ==1}http://www.nice2meet.us/ja/requirements/sales.html{else}<http://www.nice2meet.us/ja/requirements/sales.html>{/if} 
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　  About V-CUBE Sales&Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

V-CUBE Sales & Support adalah sebuah sistem konferensi web video berbasis udara. Dengan mengakses alamat tertentu, para pengguna dapat melakukan komunikasi tatap muka menggunakan video dan audio.
There are many tools for communication such as whiteboard, recording, and PC desktop sharing feature.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Sales & Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Dikembangkan dan Dikelola oleh:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Pusat Layanan Pelanggan (Khusus di Jepang)
Tel: 0570-002192 (Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Pusat Layanan Pelanggan (Bahasa Inggris & Lainnya)
Tel:
　Tokyo　+81-3-4560-1287
　Malaysia　+60-3-7724-9693
　Singapore　+65-3158-2832
　China　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Waktu Layanan：Tiap hari kerja 9:00-18:00(GMT+8)
Layanan tidak tersedia selama hari kerja dan hari libur di Malaysia
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
