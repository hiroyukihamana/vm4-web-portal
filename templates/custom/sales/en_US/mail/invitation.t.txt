*This email is transmission only, please do not reply.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Sales & Support
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using web conferencing service.
You have been invited to the meeting in progress.
Click the invitation URL below to attend the meeting.

■Secured link to the Meeting Room
{if $info.guest_url_format ==1}
{$invitation_url}
{else}
<{$invitation_url}>
{/if}
{if $reservationPassword !== ""}

■Password
{if $reservationPassword}{$reservationPassword}{else}なし{/if}{/if} 

------------------------------
Other Meeting Options
------------------------------
{if $meeting_info.pin_cd && !$contact_name}
■Passcode for V-CUBE Mobile
{$meeting_info.pin_cd} 
※You can attend the meeting with iPad/Android.  Start the V-CUBE Mobile application on iPad/Android, go to “Login with Passcode” and enter this passcode.

※Check below to see how to get V-CUBE Mobile application.
{/if}
{if $temporarySipAddress && !$reservationPassword}

■Guest Meeting Address for Hardware Video Conference System
・SIP
{$temporarySipNumberAddress} 
{$temporarySipAddress} 
 ・H.323
{$temporaryH323NumberAddress}
{$temporaryH323Address}
※Enter this meeting address when you connect to V-CUBE Meeting from hardware type video conference system such as Polycom.
{/if}
{if $room_info.options.telephone == "1" && $meeting_info.pin_cd}

■Teleconferencing Number and Code
You can join the meeting by dialing the phone number below from landline or mobile phone.  Please listen to the command prompt and enter the access code below.
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■Access Code
{$meeting_info.pin_cd}
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■Conferencing Options
{if $teleconf_info.tc_type == 'pgi'}
- Teleconferencing
Use Teleconferencing
Call the Dial -in number, listen to the command prompt, and enter in the access code.
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Dial-in Number:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- Other Dial-in Number:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- Access Code
{if $user_type == 'audience' && !$room_info.options.whiteboard}
{$teleconf_info.pgi_l_pass_code|wordwrap:3:" ":true} (Audio Only)
{else}
    {if $teleconf_info.pgi_service_name == "GlobalMeet"}
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
    {else}
{$teleconf_info.pgi_p_pass_code|wordwrap:3:" ":true}
    {/if}
{/if}
{elseif $teleconf_info.tc_type == 'voip'}
- VoIP
You can participate with VoIP by connect a USB headset.
{elseif $teleconf_info.tc_type == 'etc'}
- Other Conferencing Service
{$teleconf_info.tc_teleconf_note}
{/if}
{/if}
------------------------------
Important Information
------------------------------
■Important Information
1. Connect a webcam and a headset to your PC.
2. Click and run the Checker tool below.
   It will help you configure the proper hardware and best network connectivity.
{if $info.guest_url_format ==1}
   {$base_url}services/tools/checker/
{else}
   <{$base_url}services/tools/checker/>
{/if}
3. Click the secured link invitation at the top of this email to enter the meeting.

※Please do not plug in any additional hardware after you click the secured link invitation.  It will not be available until you restart the meeting room.
※If you do not have a webcam, you can still join the meeting using a USB headset.

■System Requirements
Refer to our system requirements page.
{if $info.guest_url_format ==1}
http://www.nice2meet.us/ja/requirements/meeting.html
{else}
<http://www.nice2meet.us/ja/requirements/meeting.html>
{/if}
■Get V-CUBE Mobile Application
Get application for smart phones or tablets (iPad/Android) from the links below to attend the meeting.
・Application for iOS
{if $info.guest_url_format ==1}
http://itunes.apple.com/jp/app/id431070449?mt=
{else}
<http://itunes.apple.com/jp/app/id431070449?mt=>
{/if}
・Application for Android
{if $info.guest_url_format ==1}
https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4
{else}
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>
{/if}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　 About V-CUBE Sales&Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

V-CUBE Sales & Support is a cloud-based video web conferencing system. By accessing specified address, users can have face to face communication using video and audio.
There are many tools for communication such as whiteboard, recording, and PC desktop sharing feature.
======================================================================

■V-CUBE Sales & Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Developed and Managed by:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center (Japanese Only)
Tel: 0570-002192 (Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center (English & Others)
Tel:
　Tokyo　+81-3-4560-1287
　Malaysia　+60-3-7724-9693
　Singapore　+65-3158-2832
　China　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Support Time：Weekdays 9:00-18:00(GMT+8)
Support not available during weekends and public holidays in Malaysia
======================================================================
