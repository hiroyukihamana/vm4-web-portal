※本邮件为发信专用邮件，无法回信。

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
             来自V-CUBE Sales&Support预约变更通知
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
             非常感谢您使用V-CUBE Sales&Support。
             以下预约内容变更通知邮件已发送至参加者。
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ 会议名称
　　 {$info.reservation_name}

　□ 召开时间
　　 {$info.reservation_starttime}
　　 〜 {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

　□ 密码
　　 {if $info.reservation_pw}{$info.reservation_pw}{else}无{/if}


■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　　　　会议参加者
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
{if $guests}{foreach from=$guests item=guest}
　□ 参加者名: {$guest.name}

　□ E-mail: {$guest.email}
　□ {if $guest.member_key == ""}客戶{else}工作人员{/if}URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if}

　□ 召开时间: {$guest.starttime}
　　           〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})
-----------------------------------------------------------------------
{/foreach}{/if}

　□ 信息
　　 {$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Sales&Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■开发・运营:
V-cube株式会社 {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 东京都目黑区上目黑2-1-1 中目黑GT Tower 20F
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel: 
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
