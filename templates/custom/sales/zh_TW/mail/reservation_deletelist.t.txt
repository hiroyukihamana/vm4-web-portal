※本郵件為發信專用郵件，無法回信。

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
            來自V-CUBE Sales&Support參加者取消通知
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
              非常感謝您使用V-CUBE Sales&Support。
              以下預約參加者取消郵件已發送至參加者。
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ 會議名稱
　　 {$info.reservation_name}

　□ 召開時間
　　 {$info.reservation_starttime}
　　 〜 {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　 取消的工作人員
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
{if $guests}{foreach from=$guests item=guest}
　□ 參加者名: {$guest.name}

　□ E-mail: {$guest.email}
　□ 召開時間: {$guest.starttime}
　　           〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})
-----------------------------------------------------------------------
{/foreach}{/if}

　□ 消息
　　 {$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Sales&Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■開發・運營:
V-cube株式會社 {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 東京都目黑區上目黑2-1-1 中目黑GT Tower 20F
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel: 
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━