※本郵件為發信專用郵件，無法回信。

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
       　{$info.sender} 向您發送了以下視頻會議確認通知
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
      　 "V-CUBE Sales&Support"通知窗口的會議確認通知。
                預定的會議即將開始，請注意。
     在下面記載的日期內，點擊URL連接即可進入會議室參加視頻會議。
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ 會議名稱
　　 {$info.reservation_name}

　□ 召開時間
　　 {$guest.starttime}
　　 〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

　□ {if $guest.member_key == ""}顧客{else}工作人員{/if}URL
{if $info.guest_url_format == 1}
　 　{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
　 　<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

　□ 密碼
　　 {if $info.reservation_pw}{$info.reservation_pw}{else}無{/if}


　□ 來自{$info.sender}的消息
　　 {$info.mail_body}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　　　　使用介紹
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

　□ 開始會議之前的準備
　　 １．連接您的攝影機与耳機。
　　 ２．通過下一項中的訪問環境確認頁面進行確認。
　　 ３．召開時間中點擊URL進入會議室。
　　 ※進入會議室後，如果已連接攝像頭或耳機，從會議室退出，再次進入會議室，識別視頻・語音。
   ※如果沒有準備網絡攝像頭，可以只通過語音參加會議。

　□ 訪問環境確認頁面
　　 可以方便地診斷網絡或連接設備的狀況。
  　 {if $info.guest_url_format ==1}{$base_url}services/tools/checker/{else}<{$base_url}services/tools/checker/>{/if}

　□ 推薦運行環境
　　 ■請在推薦運行環境運行環境頁面進行確認。
　   {if $info.guest_url_format ==1}http://www.nice2meet.us/ja/requirements/sales.html{else}<http://www.nice2meet.us/ja/requirements/sales.html>{/if}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　  什麼是V-CUBE Sales&Support？
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

　　通過V-CUBE Sales&Support的地址登陸，遠距離也可以像面對面
  一樣通過畫面和聲音進行交流。
  同時，還有白板功能，錄像，遠程桌面等幫助交流的工具可以使用。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Sales&Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■開發・運營:
V-cube株式會社 {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 東京都目黑區上目黑2-1-1 中目黑GT Tower 20F
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel: 
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
