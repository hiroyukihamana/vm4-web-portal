กรุณาอย่าตอบกลับอีเมลนี้

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
           หมายเหตุการยกเลิกประชุมจาก V-CUBE Sales & Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
    {$info.sender} ได้ยกเลิกการประชุมนี้แล้วโดยการใช้ ʺV-CUBE Sales & Supportʺ การประชุมนี้ได้ถูกยกเลิกไปแล้ว
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　■ หัวข้อ
　　 {$info.reservation_name}

　■ วันที่และเวลา
　　 {$guest.starttime}
　　 〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

　■ ข้อความจาก {$info.sender}
　　 {$info.mail_body}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　  เกี่ยวกับ V-CUBE Sales&Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

V-CUBE Sales & Support เป็นระบบการประชุมผ่านเว็บแบบ Cloud-based ซึ่งด้วยการเข้าถึงที่อยู่ตามที่กำหนดเอาไว้นั้น ผู้ใช้งานจะสามารถสื่อการกันแบบ Face-to-Face โดยการใช้ภาพวิดีโอและเสียงได้ โดยจะมีเครื่องมือสำหรับการสื่อสารช่วยเหลืออีกหลายชนิด เช่น ไวท์บอร์ด ระบบบันทึก และคุณสมบัติการแชร์หน้าเดสก์ทอปของเครื่องพีซี

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Sales & Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■พัฒนาและบริหารจัดการโดย:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■ศูนย์ให้การสนับสนุนลูกค้า (เฉพาะลูกค้าญี่ปุ่น)
Tel: 0570-002192 (Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if}
■ศูนย์ให้การสนับสนุนลูกค้า (อังกฤษและอื่นๆ)
Tel:
　Tokyo　+81-3-4560-1287
　Malaysia　+60-3-7724-9693
　Singapore　+65-3158-2832
　China　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if}
เวลาในการให้บริการ：วันธรรมดา 9:00-18:00 (GMT+8)
ไม่ให้บริการให้ช่วงวันหยุดสุดสัปดาห์และวันหยุดสาธารณะในมาเลเซีย
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
