กรุณาอย่าตอบกลับอีเมลนี้

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　V-CUBE Sales & Support: คำเชิญถูกส่งออกไปแล้ว
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
             ขอบคุณที่ใช้ V-CUBE Sales & Support
V-CUBE ได้ส่งเมล์เชิญสำหรับการประชุมต่อไปนี้ออกไปแล้ว
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ หัวข้อ
　　 {$info.reservation_name}

　□ วันที่และเวลา
　　 {$info.reservation_starttime}
　　 〜 {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

　□ รหัสผ่าน
　　 {if $info.reservation_pw}{$info.reservation_pw}{else}ไม่มีรหัสผ่าน{/if}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　　ผู้เข้าร่วมกิจกรรม
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
{if $guests}{foreach from=$guests item=guest}
　□ ชื่อ: {$guest.name}  {if $guest.type == 1} (Audience){elseif $guest.type == 2} (Document User){/if}

　□ E-mail Address: {$guest.email}
　□ {if $guest.member_key == ""}Customer{else}Staff{/if}URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if}

　□ วันที่และเวลา: {$guest.starttime}
　　           〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})
-----------------------------------------------------------------------
{/foreach}{/if}

　□ ข่าวสาร
　　 {$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Sales & Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■พัฒนาและบริหารจัดการโดย:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■ศูนย์ให้การสนับสนุนลูกค้า (เฉพาะลูกค้าญี่ปุ่น)
Tel: 0570-002192 (Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if}
■ศูนย์ให้การสนับสนุนลูกค้า (อังกฤษและอื่นๆ)
Tel:
　Tokyo　+81-3-4560-1287
　Malaysia　+60-3-7724-9693
　Singapore　+65-3158-2832
　China　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if}
เวลาในการให้บริการ：วันธรรมดา 9:00-18:00 (GMT+8)
ไม่ให้บริการให้ช่วงวันหยุดสุดสัปดาห์และวันหยุดสาธารณะในมาเลเซีย
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
