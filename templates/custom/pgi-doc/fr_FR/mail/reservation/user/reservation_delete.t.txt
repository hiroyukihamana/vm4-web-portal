﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Document
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} has cancelled this scheduled meeting.

■Title
{$info.reservation_name}

■Date and Time
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

------------------------------
Message from {$info.sender}
------------------------------
{$info.mail_body}

※This invitation has been sent automatically from the system.
　Please send any inquiry for this meeting to this email address.
　<{$info.sender_mail}>

------------------------------
About V-CUBE Document
------------------------------
“V-CUBE Document” is a paperless conference system
which allows all participants to share documents 
on iPads and Android-based tablets 
and write on the whiteboard in the same way as “V-CUBE Meeting”.
Please enjoy.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■Video Conferencing: V-CUBE
https://pgi-doc.nice2meet.us/

■Teleconferecing：　PGi / Premiere Conferencing, Inc
http://www.pgi.com/jp

■Managing Provider: V-CUBE, Inc.

■Pgi / Client Support
clientsupport.jp@pgi.com
03-4560-9630（Weekdays 9:00-20:00）
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

