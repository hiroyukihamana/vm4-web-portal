﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Document
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using web conferencing service.
V-cube has sent a notice of change to your e-mail invitations.

■Topic:
{$info.reservation_name}

■Date and Time:
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive{/if}Password
{if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}

------------------------------
Other Meeting Options
------------------------------
{if $info.pin_cd}
■Passcode for V-CUBE Mobile
{$info.pin_cd}
※iOS Application
Access the following URL.
{$smarty.const.N2MY_SHORT_URL}

V-cube Meeting4 Mobile application automatically launch on entering the passcode, and the meeting will start.

※Android Application
Start V-cube Meeting4 Mobile application and enter passcode for the meeting to enter.
{/if}

■Meeting Participants:
The notice has been sent to these participants.

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (Audience){elseif $guest.type == 2} (Whiteboard){/if}
　{$guest.email}
　Invitation URL: <{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}>
{/foreach}{/if}

- Message for Participants
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE Document powered by V-cube, Inc.
