﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBEドキュメントからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender}様から、Web会議への招待内容に
変更がありましたので、下記の通りご案内いたします。

なお、下記の日時になりましたら、招待URLをクリックして頂ければ、
Web会議へ参加することができます。

■会議名
{$info.reservation_name}

■開催日時
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■招待URL
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}>

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会議記録{/if}パスワード
{if $info.reservation_pw}{$info.reservation_pw}{else}なし{/if}

------------------------------
{$info.sender} 様からのメッセージ
------------------------------
{$info.mail_body}

※このメールはシステムから自動送信されています。
　この会議のご招待についてのお問い合わせは、下記のアドレス宛にご連絡下さい。
　<{$info.sender_mail}>

------------------------------
会議の詳細情報
------------------------------
{if $info.pin_cd}
■V-CUBEモバイル用 暗証番号
{$info.pin_cd}
※iOS端末からは、下記URLにアクセスすると「暗証番号で参加する」ページが表示されます。
{$smarty.const.N2MY_SHORT_URL}
暗証番号を入力するとアプリが起動し、Web会議への参加が可能です。

※Android端末からは、V-CUBE ミーティング4モバイルのアプリを起動し、「暗証番号でログイン」から暗証番号を入力すると、Web会議への参加が可能です。
※V-CUBE ミーティング4モバイルアプリの入手方法は、下部のご利用案内をご確認ください。
{/if}

------------------------------
ご利用案内
------------------------------
■ご利用方法
1. インターネットに接続されたパソコンやスマートフォンや
タブレット端末（iPad/Android）を用意します。

2.開催時間になったら「招待URL」をクリックします。

■推奨動作環境
動作環境ページにてご確認ください。
<http://www.nice2meet.us/ja/requirements/meeting.html>

■V-CUBEモバイル　アプリケーションの入手方法
スマートフォンやタブレット端末（iPad/Android）からは、
アプリをダウンロードいただくことでご利用いただけます。
・iOS用アプリケーション
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Android用アプリケーション
<https://market.android.com/details?id=air.jp.co.vcube.mobile.DocShare2>

------------------------------
V-CUBEドキュメントとは？
------------------------------
Web会議システム「V-CUBEドキュメント」は、参加者全員で資料を共有し、
ホワイトボードに書き込みも可能なドキュメント（資料）共有システムです。
パソコンや、Android搭載端末やiPadでご利用いただけます。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
　■ウェブテレビ会議システム「V-CUBE ドキュメント」
 　https://pgi-doc.nice2meet.us/

　■提供・販売：　PGi/プレミア コンファレンシング株式会社
 　http://www.pgi.com/jp

　■開発・運営：株式会社ブイキューブ
 　http://www.vcube.co.jp/

　■PGi/クライアントサポート
　clientsupport.jp@pgi.com
　03-4560-9630（平日9:00-20:00）
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

