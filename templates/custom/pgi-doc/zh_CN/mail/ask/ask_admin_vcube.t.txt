﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Document
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using V-cube Document.

Your inquiry has been sent us, and we will reply as soon as possible.

------------------------------
Inquiry Details
------------------------------
■Topic:
{$subject}

■Details:
{$body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■Video Conferencing: V-CUBE
https://pgi-doc.nice2meet.us/

■Teleconferecing：　PGi / Premiere Conferencing, Inc
http://www.pgi.com/jp

■Managing Provider: V-CUBE, Inc.

■Pgi / Client Support
clientsupport.jp@pgi.com
03-4560-9630（Weekdays 9:00-20:00）
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

