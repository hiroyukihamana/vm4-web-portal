━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Merci d'avoir choisi notre service de webconférence. pigeOne! a envoyé une notification d'annulation pour votre réunion plannifié.

■Sujet
{$info.reservation_name}

■Date et heure
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■Organisateur
{$info.organizer.name} 

■Participants à la réunion
L'information a été envoyé à tous les participants

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (Public){elseif $guest.type == 2} (Lancer le tableau blanc ){/if}
　{$guest.email}
{/foreach}{/if}

- Message pour les participants
　{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developpé et Géré par:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Support produit:
Téléphone : 03-4577-8609（10 heures-17 heures du lundi au vendredi）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
