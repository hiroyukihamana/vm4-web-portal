━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Nous vous rappelons que la réunion plannifiée va bientôt commencer.
------------------------------
{$info.sender}vous a invité à rejoindre une webconférence "pigeOne!".

Cliquez sur l'URL d'invitation ci-dessous à l'heure de début pour assister à la réunion.

■Sujet
{$info.reservation_name}

■Date et heure
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■Organisateur
{$info.organizer.name} 

■Lien sécurisé à la salle de réunion
{if $info.guest_url_format ==1}
{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archives {/if}Mot de passe
{if $info.reservation_pw}{$info.reservation_pw}{else}Aucun mot de passe {/if}

------------------------------
Message de {$info.sender}
------------------------------
{$info.mail_body}

※Cette invitation a été envoyée automatiquement à partir du système.
Veuillez envoyer vos demandes pour cette réunion à cette adresse e-mail.
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
Information importante 
------------------------------
Comment utiliser pigeOne!
1. Branchez une webcam et un casque à votre PC. 
2. Cliquez et exécutez l'outil Vérificateur ci-dessous. 
Il vous aidera à configurer le matériel adéquat et de meilleure connectivité réseau. 
{if $info.guest_url_format ==1}
 {$base_url}services/tools/checker/
{else}
 <{$base_url}services/tools/checker/>
{/if}
3. Cliquez sur le créancier garanti 

■Configuration requise 
Reportez-vous à notre page Configuration requise. 
{if $info.guest_url_format ==1}
  http://www.crosshead.co.jp/service/pigeone/
{else}
  <http://www.crosshead.co.jp/service/pigeone/>
{/if}

------------------------------
À propos de pigeOne!
------------------------------
pigeOne! est un système de webconférence hebergé. Il peut être utilisé instantanément à partir de presque n'importe quel dispositif internet. 

Les utilisateurs peuvent partager des vidéos, des documents, et ainsi accroître la productivité.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developpé et Géré par:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Support produit:
Téléphone : 03-4577-8609（10 heures-17 heures du lundi au vendredi）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
