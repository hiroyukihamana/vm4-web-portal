━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using web conferencing service.
pigeOne! has scheduled your meeting to all e-mail invitations.

■Topic:
{$info.reservation_name}

■Date and Time:
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■Organizer
{$info.organizer.name} 

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive{/if}Password
{if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}


■Meeting Participants:
The invitation has been sent to these participants.

{if $guests}{foreach from=$guests item=guest}
- {$guest.name}  {if $guest.type == 1} (Audience){elseif $guest.type == 2} (Whiteboard){/if}
　{$guest.email}
　Invitation URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if} 
{/foreach}{/if}

- Message for Participants
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━