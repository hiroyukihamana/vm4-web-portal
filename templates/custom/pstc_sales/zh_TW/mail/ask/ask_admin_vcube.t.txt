﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
來自pigeOne!的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
歡迎使用網絡視訊會議系統。收到您提出的問題。收到您的提問，請您稍等。

------------------------------
提問內容
------------------------------
■提問種類
{$subject}

■提問詳情
{$body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developed and Managed by:
CROSS HEAD <http://www.crosshead.co.jp/>
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
