﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
來自pigeOne!的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
歡迎使用網絡視訊會議系統。

以下會議中止的郵件通知已發送給參與者。

■會議名稱
{$info.reservation_name}

■召開時間
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■主持人
{$info.organizer.name} 

■會議的參與者已向以下參與者發送預約中止的郵件。

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (听众){elseif $guest.type == 2} (资料用户){/if}
　{$guest.email}
{/foreach}{/if}

- 發給參與者的消息
　{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number:  03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
