﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
来自pigeOne!的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender}发出邀请的以下网络视频会议中止，特此通知。

■会议名称{$info.reservation_name}

■预约的召开时间
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■主持人
{$info.organizer.name} 

------------------------------
来自{$info.sender}的消息
------------------------------
{$info.mail_body}

※此邮件已通过系统自动发送。
　关于此会议的咨询，请联系以下地址。
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
什么是pigeOne!？
------------------------------
网络视频会议系统「pigeOne!」只要有网络和电脑就可以简便地召开会议。
不仅可以看着对方的表情进行交流，还可以将资料进行全员共享，录制会议，并作为会议记录使用。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number:  03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━