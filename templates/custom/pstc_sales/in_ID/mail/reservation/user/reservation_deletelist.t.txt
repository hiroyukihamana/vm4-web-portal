━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} telah membatalkan undangan anda untuk pertemuan yang telah terjadwal tersebut.

■Topik
{$info.reservation_name}

■Tanggal dan Waktu
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

------------------------------
Pesan dari {$info.sender}
------------------------------
{$info.mail_body}

※Undangan ini terkirim secara otomatis oleh sistem. 
Silahkan kirimkan pertanyaan tentang pertemuan ini ke alamat e-mail berikut ini
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
Tentang pigeOne!
------------------------------
pigeOne! adalah jaringan sistem video konferensi berbasis cloud. Ini dapat digunakan tanpa instalasi hampir dari semua peralatan internet.

Penguna dapat berbagi video, dokumen, arsip pertemuan dan meningkatkan produktifitas. Silahkan menggunakan sistem kami. 

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number:  03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
