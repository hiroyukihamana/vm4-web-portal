━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
リアルタイムコラボレーションからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Web会議システムのご利用ありがとうございます。

パスワードの再発行申請を拒否しました。
申請者にはメールにて通知されます。


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■リアルタイムコラボレーション
http://panasonic.biz/it/sol/rtc/index.html
■運営
パナソニック ソリューションテクノロジー株式会社
〒105-0013 東京都港区浜松町1-17-14 浜松町ビル
http://panasonic.co.jp/avc/pstc/

■お問い合わせ受付窓口
E-mail: {if $info.guest_url_format ==1} support@g-asp.com {else} <support@g-asp.com>{/if} 
