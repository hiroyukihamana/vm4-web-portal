━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
リアルタイムコラボレーションからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Web会議システムのご利用ありがとうございます。
下記の会議の変更内容を、メールで参加者に送信しました。

■会議名
{$info.reservation_name}

■開催日時
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■主催者
{$info.organizer.name} 

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会議記録{/if}パスワード
{if $info.reservation_pw}{$info.reservation_pw}{else}なし{/if}

------------------------------
会議の詳細情報
------------------------------
{if $info.pin_cd}
■モバイル用 暗証番号
{$info.pin_cd}
※iPad/Androidからモバイルのアプリを起動し、
「暗証番号でログイン」から暗証番号を入力することでも、
Web会議への参加が可能です。

※モバイルアプリの入手方法は、下部のご利用案内をご確認ください。
http://panasonic.biz/it/sol/rtc/download.html

■テレビ会議システム専用ゲストアドレス
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※Polycom等のテレビ会議端末からリアルタイムコラボレーションに接続する場合はこちらのアドレスを入力して入室してください。
{/if}
{if $info.room_option.telephone == "1" && $info.pin_cd}

■電話番号一覧
固定電話・携帯電話から接続する国の電話番号を入力し、
音声ガイダンスに従って下記の暗証番号を入力することで、
Web会議にご参加頂けます。
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■暗証番号
{$info.pin_cd}
{/if}

{if $teleconf_info && $teleconf_info.tc_type != ''}
■この会議の音声取得方法
{if $teleconf_info.tc_type == 'pgi'}
#NAME?
電話回線の使用が選択されました。
アクセスポイントに電話をかけてガイダンスに従い、パスコードを入力します。
PINコードをお持ちでない場合はそのままお待ちいただくか、#を押してください。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- アクセスポイント（ダイヤルイン）番号:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- その他のアクセスポイント一覧:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
#NAME?
    {if $teleconf_info.pgi_service_name == "GlobalMeet"}
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
    {else}
{$teleconf_info.pgi_p_pass_code|wordwrap:3:" ":true}
    {/if}
{elseif $teleconf_info.tc_type == 'voip'}
#NAME?
会議室に入室するPCに、ヘッドセット、マイク等を接続して音声を取得します
{elseif $teleconf_info.tc_type == 'etc'}
#NAME?
{$teleconf_info.tc_teleconf_note}
{/if}
{/if}


■会議の参加者
下記の参加者に対し、予約会議の変更をメールで通知しました。

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} 様{if $guest.type == 1} (オーディエンス){elseif $guest.type == 2} (資料ユーザ){/if}
　{$guest.email}
　招待URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if} 
{/foreach}{/if}

#NAME?
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■リアルタイムコラボレーション
http://panasonic.biz/it/sol/rtc/index.html
■運営
パナソニック ソリューションテクノロジー株式会社
〒105-0013 東京都港区浜松町1-17-14 浜松町ビル
http://panasonic.co.jp/avc/pstc/

■お問い合わせ受付窓口
E-mail: {if $info.guest_url_format ==1} support@g-asp.com {else} <support@g-asp.com>{/if} 
