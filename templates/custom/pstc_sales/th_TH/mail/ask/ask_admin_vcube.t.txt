━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ประกาศจาก pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ขอบคุณที่ใช้ pigeOne!

ข้อมูลของคุณที่สอบถามได้ถูกส่งมาให้เราแล้ว  
และเราจะตอบกลับโดยเร็วที่สุด

------------------------------
รายละเอียดที่สอบถาม
------------------------------
■หัวเรื่อง
{$subject}

■รายละเอียด
{$body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developed and Managed by:
CROSS HEAD <http://www.crosshead.co.jp/>
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
