﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
來自V-CUBE Box 的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
歡迎使用網絡視訊會議系統。
以下會議邀請郵件已發送至參與者。

■會議名稱
{$info.reservation_name}

■召開時間
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■主持人
{$info.organizer.name} 

■{if $info.reservation_pw && $info.reservation_pw_type == 2}會議記錄{/if}密碼
{if $info.reservation_pw}{$info.reservation_pw}{else}無{/if}

------------------------------
會議的詳細資訊
------------------------------
{if $info.pin_cd}
■V-CUBE Mobile使用的驗證碼
{$info.pin_cd}
{t}9429,_('※iOS 應用程式
請連結至以下網址：'){/t}

<{$smarty.const.N2MY_SHORT_URL}>
{t}9430,_('輸入驗證碼，點選「加入」後， V-cube 會議 4 行動應用程式會自動執行，開始視訊會議。'){/t}


{t}9428,_('※Android 應用程式
請開啟 V-cube 會議 4 行動應用程式，輸入驗證碼，加入視訊會議。'){/t}

{/if}
{if ($temporarySipNumberAddress||$temporarySipAddress||$temporaryH323NumberAddress||$temporaryH323Address) && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■會議系統專用受邀者地址
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※從Polycom等電視會議終端連接V-CUBE Box 時，請使用該地址進入會議室。
{/if}
{if $info.room_option.telephone == "1" && $info.pin_cd}

■從電話號碼一覽
在固定電話・手機中輸入連接國家的電話號碼，根據語音提示輸入以下驗證碼就可以參加網路視訊會議。
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■驗證碼
{$info.pin_cd}
{/if}

{if $teleconf_info && $teleconf_info.tc_type != ''}
■此會議的語音獲取方法
{if $teleconf_info.tc_type == 'pgi'}
- 選擇使用電話會議電話線。
向訪問點撥打電話，根據語音提示，輸入密碼。
如果你沒有密碼，請稍等片刻，或按＃。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- 訪問點（撥入）號碼:
{$teleconf_info.main_phone_number.Number|japan_tel} ({$teleconf_info.main_phone_number.Location})
--  其它訪問點一覽:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- 密碼
    {if $teleconf_info.pgi_service_name == "GlobalMeet"}
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
    {else}
{$teleconf_info.pgi_p_pass_code|wordwrap:3:" ":true}
    {/if}
{elseif $teleconf_info.tc_type == 'voip'}
-在進入VoIP會議室的PC上連接耳機、話筒等，獲取語音
{elseif $teleconf_info.tc_type == 'etc'}
- 使用其它公司的電話會議服務
{$teleconf_info.tc_teleconf_note}
{/if}
{/if}


■會議的參與者已向以下參與者發送預約會議的邀請郵件。

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (听众){elseif $guest.type == 2} (资料用户){/if}
　{$guest.email}
　邀请URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if} 
{/foreach}{/if}

- 發給參與者的消息
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Box 
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■開發・運營:
V-cube株式會社 {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 東京都目黑區上目黑2-1-1 中目黑GT Tower 20F
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
