━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
感謝使用視訊會議系統。
密碼重置申請已受理。
請在確認以下內容後，辦理允許或者拒絕的手續。

■申請者ID
{$member_id}

■申請者姓名
{$name}

■允許再次發放密碼
<{$smarty.const.N2MY_BASE_URL}a/{$authorize_key}>

■拒絕再次發放密碼
<{$smarty.const.N2MY_BASE_URL}k/{$ignore_key}>

※有效期限：郵件發送後24小時內


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■開發・運營:
V-cube株式會社 <http://www.vcube.com/>
〒153-0051 東京都目黑區上目黑2-1-1 中目黑GT Tower20F
■客服中心
電話: 0570-002192（24小時365天電話支援）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
