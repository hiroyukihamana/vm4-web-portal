━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari Pertemuan V-CUBE
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Terima kasih telah menggunakan servis web konferensi kami. 
Anda telah diundang untuk menghadiri pertemuan yang sedang berlangsung. 
Klik tautan undangan dibawah ini untuk menghadiri pertemuan.

■Tautan aman menuju ke ruangan pertemuan
{if $info.guest_url_format ==1}
{$invitation_url}
{else}
<{$invitation_url}>
{/if}
{if $reservationPassword !== ""}

■Kata sandi
{if $reservationPassword}{$reservationPassword}{else}なし{/if}{/if}

------------------------------
opsi pertemuan lainnya.
------------------------------
{if $meeting_info.pin_cd}
■Passcode untuk V-CUBE Mobile
{$meeting_info.pin_cd}
※Aplikasi iOS
Akses URL berikut ini.
{$smarty.const.N2MY_SHORT_URL}
Aplikasi V-cube Meeting4 Mobile otomatis diluncurkan setelah memasukkan kode sandi, dan rapat akan dimulai.

※Aplikasi Android
Buka aplikasi V-cube Meeting4 Mobile dan masukkan kode sandi untuk masuk ke dalam rapat.

※Silahkan mengacu kepada bagaimana untuk mendapatkan aplikasi V-CUBE mobile dibawah ini.
{/if}
{if ($temporarySipNumberAddress||$temporarySipAddress||$temporaryH323NumberAddress||$temporaryH323Address) && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■Khusus alamat tamu untuk sistem pertemuan video
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※Masukkan alamat pertemuan berikut ketika anda terhubung dengan pertemuan V-CUBE dari system video konferensi seperti Polycom
{/if}
{if $room_info.options.telephone == "1" && $meeting_info.pin_cd}

■Kode dan Nomor Konferensi Jarak Jauh
Anda dapat bergabung ke pertemuan dengan menghubungi nomor telefon dibawah melalui telepon rumah atau handphone. Silahkan dengarkan instruksi yang diberikan dan masukkan kode akses dibawah ini.
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■Kode Akses
{$meeting_info.pin_cd}
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■Opsi konferensi
{if $teleconf_info.tc_type == 'pgi'}
- Konferensi jarak jauh
penggunaan konferensi jarak jauh
Hubungi nomor Dial-In, dengarkan instruksi yang diberikan dan masukkan kode akses
Jika Anda tidak memiliki kode akses, silahkan tunggu sejenak atau tekan #.
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Nomor Dial-In:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- Nomor Dial-In yang lain:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- Kode Akses
{if $user_type == 'audience' && !$room_info.options.whiteboard}
{$teleconf_info.pgi_l_pass_code|wordwrap:3:" ":true} (Hanya suara)
{else}
    {if $teleconf_info.pgi_service_name == "GlobalMeet"}
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
    {else}
{$teleconf_info.pgi_p_pass_code|wordwrap:3:" ":true}
    {/if}
{/if}
{elseif $teleconf_info.tc_type == 'voip'}
- VoIP
Anda dapat berpartisipasi dengan VoIP dengan menghubungkan headset USB
{elseif $teleconf_info.tc_type == 'etc'}
- Servis Konferensi yang lain
{$teleconf_info.tc_teleconf_note}
{/if}
{/if}
------------------------------
Informasi Penting
------------------------------
■Bagaimana menggunakan Pertemuan V-CUBE
1. Hubungkan sebuah Webcam dan sebuah Headset ke PC anda 
2. Klik dan jalankan alat pengecek dibawah
Itu akan membantu anda mengatur pengaturan perangkat dan konektifitas jaringan yg terbaik <{$base_url}services/tools/checker/>
3. Klik undangan tautan aman yang ada di atas email ini untuk masuk ke pertemuan

※Mohon tidak menyambungkan peralatan lainnya setelah anda men-klik undangan tautan aman, karena tidak akan bisa dipakai sampai antar mengulangi ruang pertemuannya.
※Jika anda tidak memiliki webcam, anda tetap dapat bergabung ke pertemuan dengan menggunakan headset USB.

■Persyaratan sistem
Silahkan mengacu ke halaman persyaratan sistem kami.
{if $info.guest_url_format ==1}
http://www.nice2meet.us/ja/requirements/meeting.html
{else}
<http://www.nice2meet.us/ja/requirements/meeting.html>
{/if}

■Dapatkan aplikasi V-CUBE Mobile
Dapatkan aplikasi untuk smartphone atau tablet (iPad/Android) dari tautan dibawah untuk menghadiri pertemuan.
・Aplikasi untuk  iOS
{if $info.guest_url_format ==1}
http://itunes.apple.com/jp/app/id431070449?mt=
{else}
<http://itunes.apple.com/jp/app/id431070449?mt=>
{/if}
・Aplikasi untuk Android
{if $info.guest_url_format ==1}
https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4
{else}
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>
{/if}

------------------------------
Tentang Pertemuan V-CUBE
------------------------------
Pertemuan V-CUBE adalah jaringan sistem video konferensi berbasis cloud. Ini dapat digunakan tanpa instalasi hampir dari semua peralatan internet.

Penguna dapat berbagi video, dokumen, arsip pertemuan dan meningkatkan produktifitas. Silahkan menggunakan sistem kami. 

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■Pertemuan V-CUBE
{if $info.guest_url_format ==1}{$smarty.const.N2MY_BASE_URL}{else}<{$smarty.const.N2MY_BASE_URL}>{/if} 
■Dikembangkan dan dikelola oleh:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━