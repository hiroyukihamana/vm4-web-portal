━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur V-CUBE Box 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Merci d'avoir choisi notre service de webconférence. V-cube a invité par email tout les participants.

■Sujet
{$info.reservation_name}

■Date et heure
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■Organisateur
{$info.organizer.name} 

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive{/if}Mot de passe
{if $info.reservation_pw}{$info.reservation_pw}{else}Pas de mot de passe{/if}

------------------------------
Autres options de la réunion 
------------------------------
{if $info.pin_cd}
■Code d'accès pour V-CUBE mobile 
{$info.pin_cd}
{t}9429,_('※Application iOS
Accedez a l’URL suivante :'){/t}

<{$smarty.const.N2MY_SHORT_URL}>
{t}9430,_('L’application V-cube Meeting4 Mobile se lance automatiquement a la saisie du code d’acces, et la reunion commencera.'){/t}


{t}9428,_('※Application Android
Lancez l’application V-cube Meeting4 Mobile et entrez le code d’acces pour commencer la reunion.'){/t}

{/if}
{if ($temporarySipNumberAddress||$temporarySipAddress||$temporaryH323NumberAddress||$temporaryH323Address) && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■Adresse invité de la réunion pour le terminal de vidéoconférence 
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※Entrez cette adresse de réunion lorsque vous vous connectez à V-CUBE Box  à partir d'un terminal de vidéoconférence tel que Polycom. 
{/if}
{if $info.room_option.telephone == "1" && $info.pin_cd}

■Numéro et code de téléconférence
Vous pouvez participer à la réunion en composant le numéro de téléphone ci-dessous à partir du téléphone fixe ou mobile. Veuillez écouter l'invite de commande et entrez le code d'accès ci-dessous. 
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■Code d'accès 
{$info.pin_cd}
{/if}

{if $teleconf_info && $teleconf_info.tc_type != ''}
■Options de conférence 
{if $teleconf_info.tc_type == 'pgi'}
- Téléconférence 
Utiliser Téléconférence 
Appelez le Numéro d'accès, écouter l'invite de commande et entrez le code d'accès. 
Si vous n'avez pas le code d'accès, Veuillez maintenez pendant un moment ou appuyez sur #. 
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Numéro d'accès:
{$teleconf_info.main_phone_number.Number|japan_tel} ({$teleconf_info.main_phone_number.Location})
-- Autres numéros d'accès:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- Code d'accès 
    {if $teleconf_info.pgi_service_name == "GlobalMeet"}
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
    {else}
{$teleconf_info.pgi_p_pass_code|wordwrap:3:" ":true}
    {/if}
{elseif $teleconf_info.tc_type == 'voip'}
- VoIP
Vous pouvez participer en VoIP en branchant un casque USB. 
{elseif $teleconf_info.tc_type == 'etc'}
- Autre service de conférence 
{$teleconf_info.tc_teleconf_note}
{/if}
{/if}


■Participants à la réunion
L'invitation a été envoyé à tous les participants

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (Public){elseif $guest.type == 2} (Lancer le tableau blanc ){/if}
　{$guest.email}
　Invitation URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if} 
{/foreach}{/if}

- Message pour les participants
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Box 
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Developpé et Géré par:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if}

■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if}

Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
