━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur V-CUBE Box 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Merci d'utiliser notre service de conférence Web. 
Veuillez trouver ci-joint le bloc-notes partagé pour la conférence ci-dessous. 

■Titre
{$meeting_name}

■Date et heure
{$meeting_start_datetime} ～

------------------------------
À propos de V-CUBE Box 
------------------------------
V-CUBE Box  est un système de webconférence hebergé. Il peut être utilisé instantanément à partir de presque n'importe quel dispositif internet. 

Les utilisateurs peuvent partager des vidéos, des documents, et ainsi accroître la productivité.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Box 
<{$smarty.const.N2MY_BASE_URL}>
■Developpé et Géré par:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Service client:
Téléphone : 0570-002192（Open 24/7）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
