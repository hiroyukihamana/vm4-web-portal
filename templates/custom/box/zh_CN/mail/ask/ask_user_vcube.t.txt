﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
来自V-CUBE Box 的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
欢迎使用网络视频会议系统。收到您的咨询。我们的回答让您久等了。

------------------------------
咨询内容
------------------------------
{if $ask_category == 0}关于服务
{elseif $ask_category == 1}关于使用费
{elseif $ask_category == 2}关于会议预约的变更
{elseif $ask_category == 3}关于会议预约的取消
{elseif $ask_category == 4}其它{/if}

{$ask_contents}

------------------------------
注册内容
------------------------------
■公司名称 : {$ask_company_name}
■部门名称 : {$ask_company_division}
■电话号码 : {$ask_telephone}
■FAX号码 : {$ask_fax}
■担当名 : {$ask_name}
■E-mail address : <{$ask_email}>

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Box 
<{$smarty.const.N2MY_BASE_URL}>
■开发・运营:
V-cube株式会社 <http://www.vcube.com/>
〒153-0051 东京都目黑区上目黑2-1-1 中目黑GT Tower 20F
■客服中心
电话号码: 0570-002192（24小时365天电话支持）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
