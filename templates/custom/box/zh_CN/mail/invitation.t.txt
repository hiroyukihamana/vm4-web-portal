﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
来自V-CUBE Box 的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
欢迎使用网络视频会议系统。邀请您参加现在正在召开的以下会议。
您只要点击「邀请URL」，就可以参加网络视频会议。

■邀请URL
{if $info.guest_url_format ==1}
{$invitation_url}
{else}
<{$invitation_url}>
{/if}
{if $reservationPassword !== ""}

■密码
{if $reservationPassword}{$reservationPassword}{else}无{/if}{/if}

------------------------------
会议的详细信息
------------------------------
{if $meeting_info.pin_cd}
■V-CUBE Mobile使用的验证码
{$meeting_info.pin_cd}
※iOS 应用程序
访问以下 URL。
{$smarty.const.N2MY_SHORT_URL}
一旦密码输入后，V-cube Meeting4 移动应用程序就会自动启动，之后会议就会开始。

※Android 应用程序
打开 V-cube Meeting4 移动应用程序，之后输入密码进入会议。

※V-CUBE Mobile应用程序的获取方法请确认下方的使用介绍。
{/if}
{if ($temporarySipNumberAddress||$temporarySipAddress||$temporaryH323NumberAddress||$temporaryH323Address) && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■会议系统专用受邀者地址
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※从Polycom等电视会议终端连接V-CUBE Box 时，请使用该地址进入会议室。
{/if}
{if $room_info.options.telephone == "1" && $meeting_info.pin_cd}

■从电话号码一览
固定电话・手中输入连接国家的电话号码，根据语音提示输入以下验证码就可以参加网络视频会议。
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■验证码
{$meeting_info.pin_cd}
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■此会议的语音获取方法
{if $teleconf_info.tc_type == 'pgi'}
- 选择使用电话会议电话线。
向访问点拨打电话，根据语音提示，输入密码。
如果你没有密码，请稍等片刻，或按＃。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- 访问点（拨入）号码:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- 其它访问点一览:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- 密码
{if $user_type == 'audience' && !$room_info.options.whiteboard}
{$teleconf_info.pgi_l_pass_code|wordwrap:3:" ":true} (收听专用)
{else}
    {if $teleconf_info.pgi_service_name == "GlobalMeet"}
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
    {else}
{$teleconf_info.pgi_p_pass_code|wordwrap:3:" ":true}
    {/if}
{/if}
{elseif $teleconf_info.tc_type == 'voip'}
- 在进入VoIP会议室的PC上连接耳机、话筒等，获取语音
{elseif $teleconf_info.tc_type == 'etc'}
- 使用其它公司的电话会议服务
{$teleconf_info.tc_teleconf_note}
{/if}
{/if}
------------------------------
使用介绍
------------------------------
■使用方法
1. 将网络摄像头和耳机连接到电脑。
2. 请访问环境确认页面。
  可以方便地诊断网络或连接设备的状况。
  {if $info.guest_url_format ==1}
  {$base_url}services/tools/checker/
  {else}
  <{$base_url}services/tools/checker/>
  {/if}
3. 从以上「邀请URL」进入会议室。

※进入会议室后，如果已连接摄像头或耳机，从会议室退出，再次进入会议室，识别视频・语音。
※如果没有准备网络摄像头，可以只通过语音参加会议。

■请在推荐运行环境运行环境页面进行确认。
{if $info.guest_url_format ==1}
http://www.nice2meet.us/ja/requirements/meeting.html
{else}
<http://www.nice2meet.us/ja/requirements/meeting.html>
{/if}

■V-CUBE Mobile 应用程序的获取方法 您可以从智能手机或平板电脑终端（iPad/Android）下载应用程序后使用。
・iOS用应用程序
{if $info.guest_url_format ==1}
http://itunes.apple.com/jp/app/id431070449?mt=
{else}
<http://itunes.apple.com/jp/app/id431070449?mt=>
{/if}
・Android用应用程序
{if $info.guest_url_format ==1}
https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4
{else}
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>
{/if}

------------------------------
什么是V-CUBE Box ？
------------------------------
网络视频会议系统「V-CUBE Box 」只要有网络和电脑就可以简便地召开会议。
不仅可以看着对方的表情进行交流，还可以将资料进行全员共享，录制会议，并作为会议记录使用。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Box 
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■开发・运营:
V-cube株式会社 {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 东京都目黑区上目黑2-1-1 中目黑GT Tower 20F
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
