━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Box 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} has cancelled your invitation to this scheduled meeting.

■Topic:
{$info.reservation_name}

■Date and Time:
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

------------------------------
Message from{$info.sender}
------------------------------
{$info.mail_body}

※This invitation has been sent automatically from the system.
　Please send any inquiry for this meeting to this email address.
{if $info.guest_url_format ==1}　
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
About V-CUBE Box 
------------------------------
V-CUBE Box  is a cloud-based video web conferencing system.  It can be used instantly from almost any internet device.

Users can share videos, share documents, archive meetings, and increase productivity.  Please enjoy.


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Box 
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Developed and Managed by:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>
■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
