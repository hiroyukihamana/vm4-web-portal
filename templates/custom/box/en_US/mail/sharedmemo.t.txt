━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Box 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using our web conferencing service.
Attached please find the shared notepad from the conference below.

■Topic
{$meeting_name}

■Date and Time
{$meeting_start_datetime} ～

------------------------------
About V-CUBE Box 
------------------------------
V-CUBE Box  is a cloud-based video web conferencing system.  It can be used instantly from almost any internet device.

Users can share videos, share documents, archive meetings, and increase productivity.  Please enjoy.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Box 
<{$smarty.const.N2MY_BASE_URL}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
Tel: 0570-002192(Open 24/7)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
