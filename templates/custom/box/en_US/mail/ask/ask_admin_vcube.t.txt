﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Box 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using V-CUBE Box .

Your inquiry has been sent us, and we will reply as soon as possible.

------------------------------
Inquiry Details
------------------------------
■Topic:
{$subject}

■Details:
{$body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Box 
<{$smarty.const.N2MY_BASE_URL}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
Tel: 0570-002192(Open 24/7)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
