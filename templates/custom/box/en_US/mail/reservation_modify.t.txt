━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Box 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using web conferencing service.
V-cube has sent a notice of change to your e-mail invitations.

■Topic:
{$info.reservation_name}

■Date and Time:
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■Organizer
{$info.organizer.name} 

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive{/if}Password
{if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}

------------------------------
Other Meeting Options
------------------------------
{if $info.pin_cd}
■Passcode for V-CUBE Mobile
{$info.pin_cd}
{t}9429,_('※iOS Application
Access the following URL.'){/t}

<{$smarty.const.N2MY_SHORT_URL}>

{t}9430,_('V-cube Meeting4 Mobile application automatically launch on entering the passcode, and the meeting will start.'){/t}

{t}9428,_('※Android Application
Start V-cube Meeting4 Mobile application and enter passcode for the meeting to enter.'){/t}

{/if}
{if ($temporarySipNumberAddress||$temporarySipAddress||$temporaryH323NumberAddress||$temporaryH323Address) && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■Guest Meeting Address for Hardware Video Conference System
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※Enter this meeting address when you connect to V-CUBE Box  from hardware type video conference system such as Polycom.
{/if}
{if $room_info.options.telephone == "1" && $meeting_info.pin_cd}

■Teleconferencing Number and Code
You can join the meeting by dialing the phone number below from landline or mobile phone.  Please listen to the command prompt and enter the access code below.

{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■Access Code:
{$info.pin_cd}
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■ Conferencing Options:
{if $teleconf_info.tc_type == 'pgi'}
- Teleconferencing
Use Teleconferencing
Call the Dial -in number, listen to the command prompt, and enter in the access code.
If you do not have the access code, please hold on for a moment or press #.
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Dial-in Number:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- Other Dial-in Number:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- Access Code:
{if $guest.type == 1 && !$info.room_option.whiteboard}{$teleconf_info.pgi_l_pass_code|wordwrap:3:" ":true} (Audio Only)
{else}
    {if $teleconf_info.pgi_service_name == "GlobalMeet"}
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
    {else}
{$teleconf_info.pgi_p_pass_code|wordwrap:3:" ":true}
    {/if}
{/if}
{elseif $teleconf_info.tc_type == 'voip'}
- VoIP
You can participate with VoIP by connect a USB headset.
{elseif $teleconf_info.tc_type == 'etc'}
- Other Conferencing Service
{$teleconf_info.tc_teleconf_note}
{/if}
{/if}


■Meeting Participants:
The notice has been sent to these participants.

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (Audience){elseif $guest.type == 2} (Whiteboard){/if}
　{$guest.email}
　Invitation URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if} 
{/foreach}{/if}

- Message for Participants
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Box 
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Developed and Managed by:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if}

■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if}

Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
