﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Document
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} has cancelled this scheduled meeting.

■Title
{$info.reservation_name}

■Date and Time
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

------------------------------
Message from {$info.sender}
------------------------------
{$info.mail_body}

※This invitation has been sent automatically from the system.
　Please send any inquiry for this meeting to this email address.
　<{$info.sender_mail}>

------------------------------
About V-CUBE Document
------------------------------
“V-CUBE Document” is a paperless conference system
which allows all participants to share documents 
on iPads and Android-based tablets 
and write on the whiteboard in the same way as “V-CUBE Meeting”.
Please enjoy.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Document
<{$base_url}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.co.jp/>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
