﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Document
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} has changed the details for your scheduled meeting.

Click the invitation URL below at the starting time to attend the meeting.

■Topic:
{$info.reservation_name}

■Date and Time
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■Secured link to the Meeting Room:
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}>

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive {/if}Password
{if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}

------------------------------
Message from {$info.sender}
------------------------------
{$info.mail_body}

※This invitation has been sent automatically from the system.
　Please send any inquiry for this meeting to this email address.
　<{$info.sender_mail}>

------------------------------
Other Meeting Options
------------------------------
{if $info.pin_cd}
■Passcode for V-CUBE Mobile
{$info.pin_cd}
※You can attend the meeting with iPad/Android.  Start the V-CUBE Mobile application on iPad/Android, go to “Login with Passcode” and enter this passcode.

※Click this link to see how to get V-CUBE Mobile application.
 {/if}

------------------------------
Important Information
------------------------------
■How to use V-CUBE Document
1. Click the secured link invitation at the top of this email to enter the meeting.

■System Requirements
Refer to our system requirements page.
<http://www.nice2meet.us/ja/requirements/meeting.html>

■Get V-CUBE Mobile Application
Get application for smart phones or tablets (iPad/Android) from the links below to attend the meeting.
・Application for iOS
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Application for Android
<https://market.android.com/details?id=air.jp.co.vcube.mobile.DocShare2>

------------------------------
About V-CUBE Document
------------------------------
“V-CUBE Document” is a paperless conference system
which allows all participants to share documents
on iPads and Android-based tablets
and write on the whiteboard in the same way as “V-CUBE Meeting”.
Please enjoy.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Document
<{$base_url}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.co.jp/>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

