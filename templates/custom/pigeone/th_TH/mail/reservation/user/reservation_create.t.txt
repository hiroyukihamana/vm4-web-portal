━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ประกาศจาก pigeOne! 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender}เชิญคุณเข้าร่วมการประชุมผ่านเว็บโดยใช้ "pigeOne!"

คลิกที่ URL เชิญด้านล่างได้ตั้งแต่เวลาเริ่มต้นการประชุม

■หัวเรื่อง
{$info.reservation_name}

■วันที่ และเวลา
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■ผู้จัด
{$info.organizer.name} 

■ลิงค์ที่มีความปลอดภัยเชื่อมโยงไปยังห้องประชุม
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>

■{if $info.reservation_pw && $info.reservation_pw_type == 2}ที่เก็บเอกสาร{/if}รหัสผ่าน
{if $info.reservation_pw}{$info.reservation_pw}{else}ไม่ต้องใส่รหัสผ่าน{/if}

------------------------------
ข้อความจาก {$info.sender}
------------------------------
{$info.mail_body}

※คำเชิญนี้ได้ถูกส่งโดยอัตโนมัติจากระบบ 
กรุณาส่งคำถามสำหรับการประชุมครั้งนี้ไปยังที่อยู่อีเมล์นี้
　<{$info.sender_mail}>

------------------------------
ข้อมูลสำคัญ
------------------------------
■วิธีการใช้งาน pigeOne! 
1. เชื่อมต่อเว็บแคมและชุดหูฟังเข้ากับเครื่องพีซีของคุณ 
2. คลิกและเรียกใช้เครื่องมือตรวจสอบรายละเอียดด้านล่าง    
มันจะช่วยให้คุณกำหนดค่าฮาร์ดแวร์ที่เหมาะสมและการเชื่อมต่อเครือข่ายที่ดีที่สุด
{if $info.guest_url_format ==1}
 {$base_url}services/tools/checker/
{else}
 <{$base_url}services/tools/checker/>
{/if}
3. คลิกที่ลิงค์เชิญที่มีความปลอดภัยที่ด้านบนของอีเมล์นี้เพื่อเข้าสู่การประชุม 
※กรุณาอย่าเสียบในอุปกรณ์ใด ๆ เพิ่มเติมหลังจากที่คุณคลิกลิงค์เชิญที่มีความปลอดภัย มันจะไม่สามารถใช้ได้จนกว่าคุณจะเริ่มต้นห้องประชุมใหม่ 
※หากคุณไม่มีเว็บแคมคุณยังสามารถเข้าร่วมประชุมโดยใช้ชุดหูฟัง USB

■ความต้องการของระบบ 
อ้างถึงส่วนที่เราแจ้งความต้องการของระบบ
{if $info.guest_url_format ==1}
  http://www.crosshead.co.jp/service/pigeone/
{else}
  <http://www.crosshead.co.jp/service/pigeone/>
{/if}

------------------------------
เกี่ยวกับ pigeOne! 
------------------------------
pigeOne! เป็นระบบการประชุมทางเว็บวิดีโอบนเทคโนโลยีคลาวด์ 
มันสามารถใช้ได้ทันทีจากอุปกรณ์หลายประเภทที่เชื่อมต่ออินเทอร์เน็ต 

ผู้ใช้สามารถแบ่งปันวิดีโอ, เอกสาร, เก็บข้อมูลการประชุม และเพิ่มผลผลิต กรุณาเพลิดเพลิน

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number:  03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
