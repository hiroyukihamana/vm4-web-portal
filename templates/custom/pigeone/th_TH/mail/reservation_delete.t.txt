━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ประกาศจาก pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ขอขอบคุณที่ใช้บริการประชุมผ่านเว็บ 
pigeOne! ได้แจ้งยกเลิกกำหนดการประชุมของคุณ

■หัวเรื่อง
{$info.reservation_name}

■วัน และเวลา
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})


■ผู้จัด
{$info.organizer.name} 

■ผู้เข้าร่วมประชุม
ได้ส่งคำเชิญไปให้ผู้เข้าร่วมประชุมเหล่านี้

{if $guests}{foreach from=$guests item=guest}
- {$guest.name}  {if $guest.type == 1} (ผู้ฟัง){elseif $guest.type == 2} (ไวท์บอร์ด){/if}
　{$guest.email}
{/foreach}{/if}

- ข้อความสำหรับผู้เข้าร่วมประชุม
　{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number:  03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
