━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
pigeOne! 회의 공지
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
pigeOne! Meeting을 이용해 주셔서 감사합니다.

계정 관리자가 비밀번호 재발급 요청을 거부했습니다.
요청자에게 안내 메일을 전송하였습니다.  


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developed and Managed by:
CROSS HEAD <http://www.crosshead.co.jp/>
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
