━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Nous vous rappelons que la réunion plannifiée va bientôt commencer.
------------------------------
Merci d'avoir choisi notre service de webconférence. pigeOne! a invité par email tout les participants.

■Sujet
{$info.reservation_name}

■Date et heure
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■Organisateur
{$info.organizer.name} 

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive{/if}Mot de passe
{if $info.reservation_pw}{$info.reservation_pw}{else}Pas de mot de passe{/if}

■Participants à la réunion
L'invitation a été envoyé à tous les participants

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (Public){elseif $guest.type == 2} (Lancer le tableau blanc ){/if}
　{$guest.email}
　Invitation URL: {if $info.guest_url_format ==1}{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>{/if} 
{/foreach}{/if}

 - Message pour les participants
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developpé et Géré par:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Support produit:
Téléphone : 03-4577-8609（10 heures-17 heures du lundi au vendredi）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
