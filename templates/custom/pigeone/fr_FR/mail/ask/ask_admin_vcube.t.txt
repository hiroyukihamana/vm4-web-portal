━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Merci d'utiliser pigeOne!

Votre demande nous a été transmise, nous vous répondrons dans les plus brefs délais.

------------------------------
Détails de la demande
------------------------------
■Sujet
{$subject}

■Details
{$body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developpé et Géré par:
CROSS HEAD <http://www.crosshead.co.jp/>
■Support produit:
Téléphone : 03-4577-8609（10 heures-17 heures du lundi au vendredi）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
