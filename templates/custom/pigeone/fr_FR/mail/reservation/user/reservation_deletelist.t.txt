━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} a annulé votre invitation pour la réunion plannifiée.

■Titre
{$info.reservation_name}

■Date et heure
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

------------------------------
Message de {$info.sender} 
------------------------------
{$info.mail_body}

※Cette invitation a été envoyée automatiquement à partir du système. 
Veuillez envoyer vos demandes pour cette réunion à cette adresse e-mail. 
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
À propos de pigeOne!
------------------------------
pigeOne! est un système de webconférence hebergé. Il peut être utilisé instantanément à partir de presque n'importe quel dispositif internet. 

Les utilisateurs peuvent partager des vidéos, des documents, et ainsi accroître la productivité.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developpé et Géré par:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Support produit:
Téléphone : 03-4577-8609（10 heures-17 heures du lundi au vendredi）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━