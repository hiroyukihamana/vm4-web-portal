━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} has invited you to join a web meeting using "pigeOne!".

Click the invitation URL below at the starting time to attend the meeting.

■Topic:
{$info.reservation_name}

■Date and Time:
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■Organizer
{$info.organizer.name} 

■Secured link to the Meeting Room:
{if $info.guest_url_format ==1}
{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive {/if}Password
{if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}

------------------------------
Message from {$info.sender}
------------------------------
{$info.mail_body}

※This invitation has been sent automatically from the system.
　Please send any inquiry for this meeting to this email address.
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}


------------------------------
Important Information
------------------------------
■How to use pigeOne!
1. Connect a webcam and a headset to your PC.
2. Click and run the Checker tool below.
   It will help you configure the proper hardware and best network connectivity.
{if $info.guest_url_format ==1}
   {$base_url}services/tools/checker/
{else}
   <{$base_url}services/tools/checker/>
{/if}
3. Click the secured link invitation at the top of this email to enter the meeting.

※Please do not plug in any additional hardware after you click the secured link invitation.  It will not be available until you restart the meeting room.
※If you do not have a webcam, you can still join the meeting using a USB headset.

■System Requirements
Refer to our system requirements page.
{if $info.guest_url_format ==1}
  http://www.crosshead.co.jp/service/pigeone/
{else}
  <http://www.crosshead.co.jp/service/pigeone/>
{/if}


------------------------------
About pigeOne!
------------------------------
pigeOne! is a cloud-based video web conferencing system.  It can be used instantly from almost any internet device.

Users can share videos, share documents, archive meetings, and increase productivity.  Please enjoy.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
