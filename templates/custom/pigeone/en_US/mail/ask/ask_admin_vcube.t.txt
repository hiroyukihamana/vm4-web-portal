﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using pigeOne!.

Your inquiry has been sent us, and we will reply as soon as possible.

------------------------------
Inquiry Details
------------------------------
■Topic:
{$subject}

■Details:
{$body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developed and Managed by:
CROSS HEAD <http://www.crosshead.co.jp/>
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
