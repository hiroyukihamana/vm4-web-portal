━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using pigeOne!.

We have received your new password request.
Please confirm and authorize or deny the following request.
 
■User ID:
{$member_id}

■Request Sender Name:
{$name}

■Click this link to authorize this request:
<{$smarty.const.N2MY_BASE_URL}a/{$authorize_key}>

■Click this link to deny this request:
<{$smarty.const.N2MY_BASE_URL}k/{$ignore_key}>

※The above request will expire in 24 hours.


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developed and Managed by:
CROSS HEAD <http://www.crosshead.co.jp/>
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
