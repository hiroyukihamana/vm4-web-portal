━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using web conferencing service.
You have been invited to the meeting in progress.
Click the invitation URL below to attend the meeting.

■Secured link to the Meeting Room:
{if $info.guest_url_format ==1}
{$invitation_url}
{else}
<{$invitation_url}>
{/if}
{if $reservationPassword !== ""}

■Password:
{if $reservationPassword}{$reservationPassword}{else}No Password{/if}{/if}

------------------------------
Important Information
------------------------------
■How to use pigeOne!
1. Connect a webcam and a headset to your PC.
2. Click and run the Checker tool below.
   It will help you configure the proper hardware and best network connectivity.
   <{$base_url}services/tools/checker/>
3. Click the secured link invitation at the top of this email to enter the meeting.

※Please do not plug in any additional hardware after you click the secured link invitation.  It will not be available until you restart the meeting room.
※If you do not have a webcam, you can still join the meeting using a USB headset.

■System Requirements
Refer to our system requirements page.
{if $info.guest_url_format ==1}
  http://www.crosshead.co.jp/service/pigeone/
{else}
  <http://www.crosshead.co.jp/service/pigeone/>
{/if}


------------------------------
About pigeOne!
------------------------------
pigeOne! is a cloud-based video web conferencing system.  It can be used instantly from almost any internet device.

Users can share videos, share documents, archive meetings, and increase productivity.  Please enjoy.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━