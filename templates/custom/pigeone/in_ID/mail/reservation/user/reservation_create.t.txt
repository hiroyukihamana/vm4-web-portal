━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} telah mengirim kepada anda undangan untuk bergabung dalam pertemuan berbasis web dengan "pigeOne!"

klik tautan undangan dibawah pada saat waktu pertemuan di mulai untuk menghadiri pertemuan tersebut.

■Topik
{$info.reservation_name}

■Tanggal dan Waktu
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■Penyelenggara
{$info.organizer.name} 

■Tautan aman menuju ke ruangan pertemuan
{if $info.guest_url_format ==1}
{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Arsip{/if}Kata sandi
{if $info.reservation_pw}{$info.reservation_pw}{else}Tidak ada kata sandi{/if}

------------------------------
Pesan dari {$info.sender}
------------------------------
{$info.mail_body}

※Undangan ini terkirim secara otomatis dari sistem. Silahkan kirimkan semua pertanyaan untuk pertemuan ini ke alamat email ini.
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
Informasi Penting
------------------------------
■Bagaimana menggunakan pigeOne!
1. Hubungkan sebuah Webcam dan sebuah Headset ke PC anda 
2. Klik dan jalankan alat pengecek dibawah
Itu akan membantu anda mengatur pengaturan perangkat dan konektifitas jaringan yg terbaik <{$base_url}services/tools/checker/>
3. Klik undangan tautan aman yang ada di atas email ini untuk masuk ke pertemuan

※Mohon tidak menyambungkan peralatan lainnya setelah anda men-klik undangan tautan aman, karena tidak akan bisa dipakai sampai antar mengulangi ruang pertemuannya.
※Jika anda tidak memiliki webcam, anda tetap dapat bergabung ke pertemuan dengan menggunakan headset USB.

■Persyaratan sistem
Silahkan mengacu ke halaman persyaratan sistem kami.
{if $info.guest_url_format ==1}
  http://www.crosshead.co.jp/service/pigeone/
{else}
  <http://www.crosshead.co.jp/service/pigeone/>
{/if}

------------------------------
Tentang pigeOne!
------------------------------
pigeOne! adalah jaringan sistem video konferensi berbasis cloud. Ini dapat digunakan tanpa instalasi hampir dari semua peralatan internet.

Penguna dapat berbagi video, dokumen, arsip pertemuan dan meningkatkan produktifitas. Silahkan menggunakan sistem kami. 

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number:  03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━