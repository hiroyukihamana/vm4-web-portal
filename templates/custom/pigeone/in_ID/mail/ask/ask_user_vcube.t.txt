━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Terima kasih telah menggunakan pigeOne!

Permohonan anda telah kami terima, dan akan kami jawab secepatnya

------------------------------
Perincian Permohonan
------------------------------
{if $ask_category == 0}Layanan
{elseif $ask_category == 1}Biaya
{elseif $ask_category == 2}Perubahan jadwal pertemuan
{elseif $ask_category == 3}Pembatalan jadwal pertemuan
{elseif $ask_category == 4}Lain-lain{/if}

{$ask_contents}

------------------------------
Informasi
------------------------------
■Nama Perusahaan : {$ask_company_name}
■Judul : {$ask_company_division}
■Nomor Telefon : {$ask_telephone}
■Nomor Fax : {$ask_fax}
■Nama : {$ask_name}
■Alamat e-mail : <{$ask_email}>

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developed and Managed by:
CROSS HEAD <http://www.crosshead.co.jp/>
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
