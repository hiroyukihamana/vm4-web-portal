━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
pigeOne!的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
感謝使用視訊會議系統。
密碼重置申請已受理。
請在確認以下內容後，辦理允許或者拒絕的手續。

■申請者ID
{$member_id}

■申請者姓名
{$name}

■允許再次發放密碼
<{$smarty.const.N2MY_BASE_URL}a/{$authorize_key}>

■拒絕再次發放密碼
<{$smarty.const.N2MY_BASE_URL}k/{$ignore_key}>

※有效期限：郵件發送後24小時內


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developed and Managed by:
CROSS HEAD <http://www.crosshead.co.jp/>
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
