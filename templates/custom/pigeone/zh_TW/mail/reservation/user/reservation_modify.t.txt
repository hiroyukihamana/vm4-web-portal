﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
來自pigeOne!的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
由於來自{$info.sender}的網絡視訊會議的邀請內容發生變更，進行以下通知。

而且，您在以下時間點擊邀請URL，就可以參加網絡視訊會議。

■會議名稱
{$info.reservation_name}

■召開時間
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■主持人
{$info.organizer.name} 

■邀請URL
{if $info.guest_url_format ==1}
{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

■{if $info.reservation_pw && $info.reservation_pw_type == 2}會議記錄{/if}密碼
{if $info.reservation_pw}{$info.reservation_pw}{else}無{/if}

------------------------------
來自{$info.sender}的消息
------------------------------
{$info.mail_body}

※此郵件已通過系統自動發送。
　關於此會議的邀請，請點擊以下地址。
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
使用介紹
------------------------------
■使用方法
1. 將網絡攝像頭和耳機連接到電腦。
2. 請訪問環境確認頁面。
  可以方便地診斷網絡或連接設備的狀況。
{if $info.guest_url_format ==1}
 {$base_url}services/tools/checker/
{else}
 <{$base_url}services/tools/checker/>
{/if}
3.從以上「邀請URL」進入會議室。

※進入會議室後，如果已連接攝像頭或耳機，從會議室退出，再次進入會議室，識別視頻・語音。
※如果沒有準備網絡攝像頭，可以只通過語音參加會議。

■請在運行環境頁面對推薦運行環境進行確認。
{if $info.guest_url_format ==1}
  http://www.crosshead.co.jp/service/pigeone/
{else}
  <http://www.crosshead.co.jp/service/pigeone/>
{/if}

------------------------------
什麼是pigeOne!？
------------------------------
網絡視訊會議系統「pigeOne!」只要有網絡和電腦就可以簡便地召開會議。
不僅可以看著對方的表情進行交流，還可以將資料進行全員共用，錄製會議，並作為會議記錄使用。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number:  03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
