━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ｐｉｇｅＯｎｅ！からのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
以下の会議の開催時間が近くなりましたので、お知らせいたします。
------------------------------
{$info.sender}様から、下記のWeb会議への招待がありました。

定刻になりましたら、招待URLをクリックして頂ければ、
Web会議へ参加することができます。

■会議名
{$info.reservation_name} 

■開催日時
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■主催者
{$info.organizer_name} 

■招待URL
{if $info.guest_url_format ==1}
{$base_url}r/{$guest.r_user_session}&c={$guest.country_key} 
{else}
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会議記録{/if}パスワード
{if $info.reservation_pw}{$info.reservation_pw}{else}なし{/if}

------------------------------
{$info.sender} 様からのメッセージ
------------------------------
{$info.mail_body} 

※このメールはシステムから自動送信されています。
　この会議のご招待についてのお問い合わせは、下記のアドレス宛にご連絡下さい。
{if $info.guest_url_format ==1}
　{$info.sender_mail} 
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
ご利用案内
------------------------------
■ご利用方法
1. Webカメラとヘッドセットを、パソコンに接続します。
2. 環境確認ページにアクセスしてください。
   ネットワークや接続機器の状況が簡易診断できます。
{if $info.guest_url_format ==1}
   {$base_url}services/tools/checker/
{else}
   <{$base_url}services/tools/checker/>
{/if}
3. 上記の「招待URL」から会議室に入室します。

※会議室に入室した後、カメラやヘッドセットを接続した場合、
　一度会議室から退室し、再入室をすれば映像・音声を認識します。
※Webカメラのご用意がない場合、音声のみのご参加も可能です。

■推奨動作環境
動作環境ページにてご確認ください。
{if $info.guest_url_format ==1}
  http://www.crosshead.co.jp/service/pigeone/
{else}
  <http://www.crosshead.co.jp/service/pigeone/>
{/if}

------------------------------
ｐｉｇｅＯｎｅ！とは？
------------------------------
Web会議システム「ｐｉｇｅＯｎｅ！」は、
インターネット回線とパソコンさえあればカンタンに会議ができます。

相手の顔を見て話ができるだけでなく、1つの資料を全員で共有したり、
会議を録画して議事録として利用することができます。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■ｐｉｇｅＯｎｅ！
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■販売元: 
クロス・ヘッド株式会社 {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
〒107-0052 東京都港区赤坂２－１７－２２　赤坂ツインタワー本館１０F
■プロダクト・サポート
電話番号： 03-4577-8609（平日午前10時～午後5時　電話受付）
E-mail: {if $info.guest_url_format ==1} product-support@crosshead.co.jp {else} <product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
