━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ｐｉｇｅＯｎｅ！からのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Web会議システムのご利用ありがとうございます。

管理者よりパスワードの再発行申請が承認されました。
新しいパスワードは以下となります。


■新しいパスワード
{$member_pass}


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■ｐｉｇｅＯｎｅ！
<{$base_url}>
■販売元: 
クロス・ヘッド株式会社 <http://www.crosshead.co.jp/>
〒107-0052 東京都港区赤坂２－１７－２２　赤坂ツインタワー本館１０F
■プロダクト・サポート
電話番号： 03-4577-8609（平日午前10時～午後5時　電話受付）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
