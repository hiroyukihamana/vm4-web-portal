﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using pigeOne!.

Your inquiry has been sent us, and we will reply as soon as possible.

------------------------------
Inquiry Details
------------------------------
{if $ask_category == 0}Service
{elseif $ask_category == 1}Fee
{elseif $ask_category == 2}Changing scheduled meeting
{elseif $ask_category == 3}Cancelling scheduled meeting
 {elseif $ask_category == 4}Others{/if}

{$ask_contents}

------------------------------
Information
------------------------------
■Company Name : {$ask_company_name}
■Title : {$ask_company_division}
■Phone Number : {$ask_telephone}
■FAX Number : {$ask_fax}
■Name : {$ask_name}
■E-mail Address : <{$ask_email}>

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developed and Managed by:
CROSS HEAD <http://www.crosshead.co.jp/>
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
