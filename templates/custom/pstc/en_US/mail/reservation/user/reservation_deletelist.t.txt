━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} has cancelled your invitation to this scheduled meeting.

■Topic:
{$info.reservation_name}

■Date and Time:
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

------------------------------
Message from{$info.sender}
------------------------------
{$info.mail_body}

※This invitation has been sent automatically from the system.
　Please send any inquiry for this meeting to this email address.
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
About pigeOne!
------------------------------
pigeOne! is a cloud-based video web conferencing system.  It can be used instantly from almost any internet device.

Users can share videos, share documents, archive meetings, and increase productivity.  Please enjoy.


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
