━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
リアルタイムコラボレーションからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Web会議システムのご利用ありがとうございます。
下記の会議が中止されたことを、メールで参加者に連絡しました。

■会議名
{$info.reservation_name}

■開催日時
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■主催者
{$info.organizer_name} 

■会議の参加者
下記の参加者に対し、予約中止のメールを送信しました。

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} 様{if $guest.type == 1} (オーディエンス){elseif $guest.type == 2} (資料ユーザ){/if}
　{$guest.email}
{/foreach}{/if}

#NAME?
　{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■リアルタイムコラボレーション
http://panasonic.biz/it/sol/rtc/index.html
■運営
パナソニック ソリューションテクノロジー株式会社
〒105-0013 東京都港区浜松町1-17-14 浜松町ビル
http://panasonic.co.jp/avc/pstc/

■お問い合わせ受付窓口
E-mail: {if $info.guest_url_format ==1} support@g-asp.com {else} <support@g-asp.com>{/if} 
