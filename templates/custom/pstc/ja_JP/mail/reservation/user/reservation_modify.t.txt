━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
リアルタイムコラボレーションからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender}様から、Web会議への招待内容に
変更がありましたので、下記の通りご案内いたします。

なお、下記の日時になりましたら、招待URLをクリックして頂ければ、
Web会議へ参加することができます。

■会議名
{$info.reservation_name}

■開催日時
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■主催者
{$info.organizer.name} 

■招待URL
{if $info.guest_url_format ==1}
{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会議記録{/if}パスワード
{if $info.reservation_pw}{$info.reservation_pw}{else}なし{/if}

------------------------------
{$info.sender} 様からのメッセージ
------------------------------
{$info.mail_body}

※このメールはシステムから自動送信されています。
　この会議のご招待についてのお問い合わせは、下記のアドレス宛にご連絡下さい。
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
会議の詳細情報
------------------------------
{if $info.pin_cd}
■モバイル用 暗証番号
{$info.pin_cd}
※iPad/Androidからモバイルのアプリを起動し、
「暗証番号でログイン」から暗証番号を入力することでも、
Web会議への参加が可能です。

※モバイルアプリの入手方法は、下部のご利用案内をご確認ください。
http://panasonic.biz/it/sol/rtc/download.html

■テレビ会議システム専用ゲストアドレス
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※Polycom等のテレビ会議端末からリアルタイムコラボレーションに接続する場合はこちらのアドレスを入力して入室してください。
{/if}
{if $info.room_option.telephone == "1" && $info.pin_cd}

■電話番号一覧
固定電話・携帯電話から接続する国の電話番号を入力し、
音声ガイダンスに従って下記の暗証番号を入力することで、
Web会議にご参加頂けます。
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■暗証番号
{$info.pin_cd}
{/if}
{if $teleconf_info}

■この会議の音声取得方法
{if $teleconf_info.tc_type == 'pgi'}
#NAME?
電話回線の使用が選択されました。
アクセスポイントに電話をかけてガイダンスに従い、パスコードを入力します。
PINコードをお持ちでない場合はそのままお待ちいただくか、#を押してください。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- アクセスポイント（ダイヤルイン）番号:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- その他のアクセスポイント一覧:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
#NAME?
{if $guest.type == 1 && !$info.room_option.whiteboard}{$teleconf_info.pgi_l_pass_code|wordwrap:3:" ":true} (聞き取り専用)
{else}
    {if $teleconf_info.pgi_service_name == "GlobalMeet"}
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
    {else}
{$teleconf_info.pgi_p_pass_code|wordwrap:3:" ":true}
    {/if}
{/if}
{elseif $teleconf_info.tc_type == 'voip'}
- VoIP
会議室に入室するPCに、ヘッドセット、マイク等を接続して音声を取得します
{elseif $teleconf_info.tc_type == 'etc'}
- 他社の電話会議サービスを使用する
{$teleconf_info.tc_teleconf_note}
{/if}
{/if}

------------------------------
ご利用案内
------------------------------
■ご利用方法
1. Webカメラとヘッドセットを、パソコンに接続します。
2. 環境確認ページにアクセスしてください。
   ネットワークや接続機器の状況が簡易診断できます。
{if $info.guest_url_format ==1}
   {$base_url}services/tools/checker/
{else}
   <{$base_url}services/tools/checker/>
{/if}
3. 上記の「招待URL」から会議室に入室します。

※会議室に入室した後、カメラやヘッドセットを接続した場合、
　一度会議室から退室し、再入室をすれば映像・音声を認識します。
※Webカメラのご用意がない場合、音声のみのご参加も可能です。

■推奨動作環境
動作環境ページにてご確認ください。
http://panasonic.biz/it/sol/rtc/systemreqs.html

■モバイル　アプリケーションの入手方法
スマートフォンやタブレット端末（iPad/Android）からは、
アプリをダウンロードいただくことでご利用いただけます。
http://panasonic.biz/it/sol/rtc/download.html

------------------------------
リアルタイムコラボレーションとは？
------------------------------
Web会議システム「リアルタイムコラボレーション」は、
インターネット回線とパソコンさえあればカンタンに会議ができます。

相手の顔を見て話ができるだけでなく、1つの資料を全員で共有したり、
会議を録画して議事録として利用することができます。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■リアルタイムコラボレーション
http://panasonic.biz/it/sol/rtc/index.html
■運営
パナソニック ソリューションテクノロジー株式会社
〒105-0013 東京都港区浜松町1-17-14 浜松町ビル
http://panasonic.co.jp/avc/pstc/

■お問い合わせ受付窓口
E-mail: {if $info.guest_url_format ==1} support@g-asp.com {else} <support@g-asp.com>{/if} 
