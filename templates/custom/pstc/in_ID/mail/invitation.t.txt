━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Terima kasih telah menggunakan servis web konferensi kami. 
Anda telah diundang untuk menghadiri pertemuan yang sedang berlangsung. 
Klik tautan undangan dibawah ini untuk menghadiri pertemuan.

■Tautan aman menuju ke ruangan pertemuan
{if $info.guest_url_format ==1}
{$invitation_url}
{else}
<{$invitation_url}>
{/if}
{if $reservationPassword !== ""}

■Kata sandi
{if $reservationPassword}{$reservationPassword}{else}Tidak ada Kata Sandi{/if}{/if}

------------------------------
Informasi Penting
------------------------------
■Bagaimana menggunakan pigeOne!
1. Hubungkan sebuah Webcam dan sebuah Headset ke PC anda 
2. Klik dan jalankan alat pengecek dibawah
Itu akan membantu anda mengatur pengaturan perangkat dan konektifitas jaringan yg terbaik <{$base_url}services/tools/checker/>
3. Klik undangan tautan aman yang ada di atas email ini untuk masuk ke pertemuan

※Mohon tidak menyambungkan peralatan lainnya setelah anda men-klik undangan tautan aman, karena tidak akan bisa dipakai sampai antar mengulangi ruang pertemuannya.
※Jika anda tidak memiliki webcam, anda tetap dapat bergabung ke pertemuan dengan menggunakan headset USB.

■Persyaratan sistem
Silahkan mengacu ke halaman persyaratan sistem kami.
{if $info.guest_url_format ==1}
  http://www.crosshead.co.jp/service/pigeone/
{else}
  <http://www.crosshead.co.jp/service/pigeone/>
{/if}

------------------------------
Tentang pigeOne!
------------------------------
pigeOne! adalah jaringan sistem video konferensi berbasis cloud. Ini dapat digunakan tanpa instalasi hampir dari semua peralatan internet.

Penguna dapat berbagi video, dokumen, arsip pertemuan dan meningkatkan produktifitas. Silahkan menggunakan sistem kami. 

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number:  03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
