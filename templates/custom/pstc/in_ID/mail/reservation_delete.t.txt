━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Terima kasih telah menggunakan service web konferensi kami. 
pigeOne! telah mengirimkan pemberitahuan pembatalan dari pertemuan yang telah dijadwalkan.

■Topik
{$info.reservation_name}

■Tanggal dan Waktu
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})


■Penyelenggara
{$info.organizer.name} 

■Partisipan pertemuan
Pemberitahuan telah dikirimkan ke partisipan berikut.

{if $guests}{foreach from=$guests item=guest}
- {$guest.name}  {if $guest.type == 1} (Penonton){elseif $guest.type == 2} (Whiteboard){/if}
　{$guest.email}
{/foreach}{/if}

- Pesan untuk partisipan
　{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number:  03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
