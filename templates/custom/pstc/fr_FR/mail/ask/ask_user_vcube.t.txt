━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur pigeOne!
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Merci d'utiliser pigeOne!

Votre demande nous a été transmise, nous vous répondrons dans les plus brefs délais.

------------------------------
Détails de la demande
------------------------------
{if $ask_category == 0}Sujet
{elseif $ask_category == 1}Coût
{elseif $ask_category == 2}Modifier la réunion plannifié
{elseif $ask_category == 3}Annuler la réunion plannifié
{elseif $ask_category == 4}Autres{/if}

{$ask_contents}

------------------------------
Information
------------------------------
■Nom de l'entreprise : {$ask_company_name}
■Titre : {$ask_company_division}
■Numéro de téléphone : {$ask_telephone}
■Numéro de fax : {$ask_fax}
■Nom: {$ask_name}
■Adresse email : <{$ask_email}>

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developpé et Géré par:
CROSS HEAD <http://www.crosshead.co.jp/>
■Support produit:
Téléphone : 03-4577-8609（10 heures-17 heures du lundi au vendredi）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
