━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
来自pigeOne!的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
欢迎使用视频会议系统。
已向以下会议中发送了共享记事簿的内容。
请确认附件。

■会议名称
{$meeting_name}

■召开时间
{$meeting_start_datetime} ～

------------------------------
什么是pigeOne!？
------------------------------
网络视频会议系统「pigeOne!」只要有网络和电脑就可以简便地召开会议。
不仅可以看着对方的表情进行交流，还可以将资料进行全员共享，录制会议，并作为会议记录使用。


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
<{$base_url}>
■Developed and Managed by:
CROSS HEAD <http://www.crosshead.co.jp/>
■Product Support:
Phone Number: 03-4577-8609（10 am to 5pm on Weekdays）
E-mail: <product-support@crosshead.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
