﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
来自pigeOne!的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender}向您发送了以下网络视频会议的邀请。

您可以在指定时间点击邀请URL，参加网络视频会议。

■会议名称
{$info.reservation_name}

■召开时间
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■主持人
{$info.organizer.name} 

■邀请URL
{if $info.guest_url_format ==1}
{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会议记录{/if}密码
{if $info.reservation_pw}{$info.reservation_pw}{else}无{/if}

------------------------------
来自{$info.sender}的消息
------------------------------
{$info.mail_body}

※此邮件已通过系统自动发送。
　关于此会议的邀请，请咨询以下地址。
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
使用介绍
------------------------------
■使用方法
1. 将网络摄像头和耳机连接到电脑。
2. 请访问环境确认页面。
  可以方便地诊断网络或连接设备的状况。
{if $info.guest_url_format ==1}
 {$base_url}services/tools/checker/
{else}
 <{$base_url}services/tools/checker/>
{/if}
3.从以上「邀请URL」进入会议室。

※进入会议室后，如果已连接摄像头或耳机，从会议室退出，再次进入会议室，识别视频・语音。
※如果没有准备网络摄像头，可以只通过语音参加会议。

■请在推荐运行环境运行环境页面进行确认。
{if $info.guest_url_format ==1}
  http://www.crosshead.co.jp/service/pigeone/
{else}
  <http://www.crosshead.co.jp/service/pigeone/>
{/if}

------------------------------
什么是pigeOne!？
------------------------------
网络视频会议系统「pigeOne!」只要有网络和电脑就可以简便地召开会议。
不仅可以看着对方的表情进行交流，还可以将资料进行全员共享，录制会议，并作为会议记录使用。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■pigeOne!
{if $info.guest_url_format ==1}<{$base_url}>{else}{$base_url}{/if} 
■Developed and Managed by:
CROSS HEAD {if $info.guest_url_format ==1}http://www.crosshead.co.jp/ {else}<http://www.crosshead.co.jp/>{/if} 
■Product Support:
Phone Number:  03-4577-8609（10 am to 5pm on Weekdays）
E-mail: {if $info.guest_url_format ==1}product-support@crosshead.co.jp{else}<product-support@crosshead.co.jp>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━