※このメールは送信専用のため、ご返信いただけません。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
AXLBOXミーティング by V-CUBEからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender}様から、下記のWeb会議への
招待が取り消されましたのでご案内いたします。

■会議名
{$info.reservation_name}

■予約されていた開催日時
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

------------------------------
{$info.sender}様からのメッセージ
------------------------------
{$info.mail_body}

※このメールはシステムから自動送信されています。
　この会議についてのお問い合わせは、下記のアドレス宛にご連絡下さい。
{if $info.guest_url_format ==1}
 {$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}
------------------------------
AXLBOXミーティング by V-CUBEとは？
------------------------------
Web会議システム「AXLBOXミーティング by V-CUBE」は、
インターネット回線とパソコンさえあればカンタンに会議ができます。

相手の顔を見て話ができるだけでなく、1つの資料を全員で共有したり、
会議を録画して議事録として利用することができます。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■AXLBOXミーティング by V-CUBE
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■開発・運営: 
株式会社ブイキューブ {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 東京都目黒区上目黒2-1-1 中目黒GTタワー20F
■カスタマーサポートセンター
E-mail:  <cs-meeting@axlbox.com> 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━