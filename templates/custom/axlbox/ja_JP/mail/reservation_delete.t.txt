※このメールは送信専用のため、ご返信いただけません。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
AXLBOXミーティング by V-CUBEからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Web会議システムのご利用ありがとうございます。
下記の会議が中止されたことを、メールで参加者に連絡しました。

■会議名
{$info.reservation_name}

■開催日時
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■主催者
{$info.organizer_name} 

■会議の参加者
下記の参加者に対し、予約中止のメールを送信しました。

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} 様{if $guest.type == 1} (オーディエンス){elseif $guest.type == 2} (資料ユーザ){/if}
　{$guest.email}
{/foreach}{/if}

- 参加者へのメッセージ
　{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■AXLBOXミーティング by V-CUBE
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■開発・運営: 
株式会社ブイキューブ {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 東京都目黒区上目黒2-1-1 中目黒GTタワー20F
■カスタマーサポートセンター
E-mail:  <cs-meeting@axlbox.com> 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
