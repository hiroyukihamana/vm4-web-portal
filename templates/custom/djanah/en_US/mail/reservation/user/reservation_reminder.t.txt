Please kindly be notified that the scheduled meeting will start soon.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} has invited you to join a web meeting using "V-CUBE Meeting".

Click the invitation URL below at the starting time to attend the meeting.

■Topic:
{$info.reservation_name}

■Date and Time:
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■Secured link to the Meeting Room:
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive {/if}Password
{if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}

------------------------------
Message from {$info.sender}
------------------------------
{$info.mail_body}

※This invitation has been sent automatically from the system.
　Please send any inquiry for this meeting to this email address.
　<{$info.sender_mail}>

------------------------------
Other Meeting Options
------------------------------
{if $info.pin_cd}
■Passcode for V-CUBE Mobile
{$info.pin_cd}
※You can attend the meeting with iPad/Android.  Start the V-CUBE Mobile application on iPad/Android, go to “Login with Passcode” and enter this passcode.

※Check below to see how to get V-CUBE Mobile application.
 {/if}
 {if $temporarySipAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■Guest Meeting Address for Hardware Video Conference System
{$temporarySipAddress} (SIP)
{$temporaryH323Address} (H.323)
※Enter this meeting address when you connect to V-CUBE Meeting from hardware type video conference system such as Polycom.
{/if}
{if $info.room_option.telephone == "1" && $info.pin_cd}

■ Teleconferencing Number and Code
Dial your country access number* and enter the conference ID ({$meeting_info.pin_cd}) when asked :
    * France : +33 (0)977 190 900
    * Holland : +31 (0)85 401 2100
    * Canada : +1 (514) 907-3324
    * USA/Californie : +1 (714) 583-7200
    * USA/Maryland : +1 (443) 393-3444

■Access Code:
{$info.pin_cd}
{/if}

------------------------------
Important Information
------------------------------
■How to use V-CUBE Meeting
1. Connect a webcam and a headset to your PC.
2. Click and run the Checker tool below.
   It will help you configure the proper hardware and best network connectivity.
   <{$base_url}services/tools/checker/>
3. Click the secured link invitation at the top of this email to enter the meeting.

※Please do not plug in any additional hardware after you click the secured link invitation.  It will not be available until you restart the meeting room.
※If you do not have a webcam, you can still join the meeting using a USB headset.

■System Requirements
Refer to our system requirements page.
<http://www.nice2meet.us/ja/requirements/meeting.html>

■Get V-CUBE Mobile Application
Get application for smart phones or tablets (iPad/Android) from the links below to attend the meeting.
・Application for iOS
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Application for Android
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>

------------------------------
About V-CUBE Meeting
------------------------------
V-CUBE Meeting is a cloud-based video web conferencing system.  It can be used instantly from almost any internet device.

Users can share videos, share documents, archive meetings, and increase productivity.  Please enjoy.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
Europe : +33 (0)9 800 833 40
Canada / USA : +1 (514)907-7719
E-mail: <support@djanah.com>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
