━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} a annulé votre invitation pour la réunion plannifiée.

■Titre
{$info.reservation_name}

■Date et heure
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

------------------------------
Message de {$info.sender} 
------------------------------
{$info.mail_body}

※Cette invitation a été envoyée automatiquement à partir du système. 
Veuillez envoyer vos demandes pour cette réunion à cette adresse e-mail. 
　<{$info.sender_mail}>

------------------------------
À propos de V-CUBE Meeting
------------------------------
V-CUBE Meeting est un système de webconférence hebergé. Il peut être utilisé instantanément à partir de presque n'importe quel dispositif internet. 

Les utilisateurs peuvent partager des vidéos, des documents, et ainsi accroître la productivité.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■Developpé et Géré par:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Service client:
Europe : +33 (0)9 800 833 40
Canada / USA : +1 (514)907-7719
E-mail: <support@djanah.com>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
