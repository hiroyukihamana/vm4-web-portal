━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Merci d'utiliser notre service de conférence Web. 
Veuillez trouver ci-joint le bloc-notes partagé pour la conférence ci-dessous. 

■Titre
{$meeting_name}

■Date et heure
{$meeting_start_datetime} ～

------------------------------
À propos de V-CUBE Meeting
------------------------------
V-CUBE Meeting est un système de webconférence hebergé. Il peut être utilisé instantanément à partir de presque n'importe quel dispositif internet. 

Les utilisateurs peuvent partager des vidéos, des documents, et ainsi accroître la productivité.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■Developpé et Géré par:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Service client:
Europe : +33 (0)9 800 833 40
Canada / USA : +1 (514)907-7719
E-mail: <support@djanah.com>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
