Nous vous rappelons que la réunion plannifiée va bientôt commencer.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Information sur V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Merci d'avoir choisi notre service de webconférence. V-cube a invité par email tous les participants.

■Sujet
{$info.reservation_name}

■Date et heure
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive{/if}Mot de passe
{if $info.reservation_pw}{$info.reservation_pw}{else}Pas de mot de passe{/if}

------------------------------
Autres options de la réunion 
------------------------------
{if $info.pin_cd}
■Code d'accès pour V-CUBE mobile 
{$info.pin_cd}
※Vous pouvez accéder à la réunion avec iPad / Android. Démarrez l'application V-CUBE Mobile sur iPad / Android, allez dans "Connexion avec le code d'accès" et entrez ce code d'accès. 
{/if}
{if $temporarySipAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■Adresse invité de la réunion pour le terminal de vidéoconférence 
{$temporarySipAddress} (SIP)
{$temporaryH323Address} (H.323)
※Entrez cette adresse de réunion lorsque vous vous connectez à V-CUBE Meeting à partir d'un terminal de vidéoconférence tel que Polycom. 
{/if}
{if $info.room_option.telephone == "1" && $info.pin_cd}

■Numéro et code de téléconférence
Composer le numéro d'accès de votre pays et entrer l'identifiant de conférence ({$meeting_info.pin_cd}) lorsque demandé : 
    * France : +33 (0)977 190 900
    * Hollande : +31 (0)85 401 2100
    * Canada : +1 (514) 907-3324
    * USA/Californie : +1 (714) 583-7200
    * USA/Maryland : +1 (443) 393-3444

■Code d'accès 
{$info.pin_cd}
{/if}


■Participants à la réunion
L'invitation a été envoyé à tous les participants

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (Public){elseif $guest.type == 2} (Lancer le tableau blanc ){/if}
　{$guest.email}
　Invitation URL: <{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/foreach}{/if}

- Message pour les participants
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■Developpé et Géré par:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Service client:
Europe : +33 (0)9 800 833 40
Canada / USA : +1 (514)907-7719
E-mail: <support@djanah.com>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
