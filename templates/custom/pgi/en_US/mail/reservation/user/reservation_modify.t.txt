━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} has changed the details for your scheduled meeting.

Click the invitation URL below at the starting time to attend the meeting.

■Topic:
{$info.reservation_name}

■Date and Time
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■Secured link to the Meeting Room:
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}>

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive {/if}Password
{if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}

------------------------------
Message from {$info.sender}
------------------------------
{$info.mail_body}

※This invitation has been sent automatically from the system.
　Please send any inquiry for this meeting to this email address.
　<{$info.sender_mail}>

------------------------------
Other Meeting Options
------------------------------
{if $info.pin_cd}
■Passcode for V-CUBE Mobile
{$info.pin_cd}
※iOS Application
Access the following URL.
{$smarty.const.N2MY_SHORT_URL}

V-cube Meeting4 Mobile application automatically launch on entering the passcode, and the meeting will start.

※Android Application
Start V-cube Meeting4 Mobile application and enter passcode for the meeting to enter.

※Please refer to how to get V-cube Meeting4 Mobile application below.
 {/if}
 {if $temporaryAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■Guest Meeting Address for Hardware Video Conference System
{$temporaryAddress}
※Enter this meeting address when you connect to V-CUBE Meeting from hardware type video conference system such as Polycom.
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■Teleconferencing Number and Code
{if $teleconf_info.tc_type == 'pgi'}
Call the Dial -in number, listen to the command prompt, and enter in the access code.
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Dial-in Number:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- Other Dial-in Number:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- Access Code:
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

------------------------------
Important Information
------------------------------
■How to use V-CUBE Meeting
1. Connect a webcam and a headset to your PC.
2. Click and run the Checker tool below.
   It will help you configure the proper hardware and best network connectivity.
   <{$base_url}services/tools/checker/>
3. Click the secured link invitation at the top of this email to enter the meeting.

※Please do not plug in any additional hardware after you click the secured link invitation.  It will not be available until you restart the meeting room.
※If you do not have a webcam, you can still join the meeting using a USB headset.

■System Requirements
Refer to our system requirements page.
<http://www.nice2meet.us/ja/requirements/meeting.html>

■Get V-CUBE Mobile Application
Get application for smart phones or tablets (iPad/Android) from the links below to attend the meeting.
・Application for iOS
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Application for Android
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>

------------------------------
About V-CUBE Meeting
------------------------------
V-CUBE Meeting is a cloud-based video web conferencing system.  It can be used instantly from almost any internet device.

Users can share videos, share documents, archive meetings, and increase productivity.  Please enjoy.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■Teleconferecing：PGi / Premiere Conferencing, Inc
http://www.pgi.com/jp

■Managing Provider: V-CUBE, Inc.

■Pgi / Client Support
clientsupport.jp@pgi.com
03-4560-9630（Weekdays 9:00-20:00）
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

