■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　ウェブテレビ会議システム "V-CUBE ミーティング"
　　　　　　　　　　　お問い合わせを受け付けました。
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　この度は、お問い合わせ頂きましてありがとうございます。
お問い合わせいただいた件につきましては担当のものに転送いたしましたので、
　　　　　　　　　速やかにご回答させていただきます。
　　お急ぎのところ誠に申し訳ございませんが、よろしくお願いいたします。

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　　　お問い合わせ内容
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　{if $ask_category == 0}サービスについて
　{elseif $ask_category == 1}料金について
　{elseif $ask_category == 2}会議予約の変更について
　{elseif $ask_category == 3}会議予約のキャンセルについて
　{elseif $ask_category == 4}その他{/if}

　{$ask_contents}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　　　　ご登録内容
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　□ 御社名         ： {$ask_company_name}
　□ 部署名         ： {$ask_company_division}
　□ 電話番号       ： {$ask_telephone}
　□ FAX番号        ： {$ask_fax}
　□ ご担当者名     ： {$ask_name}
　□ E-mailアドレス ： {$ask_email}

 ======================================================================

　　　ウェブテレビ会議システム「V-CUBE ミーティング」
      {$base_url}
 　　　
　　　お問い合わせ先
      vsupport@vcube.co.jp

　　　開発・運営：株式会社ブイキューブ
      http://www.vcube.co.jp/
　　　
      TEL: 0570-00-2192  FAX: 03-5501-9676
　　　〒153-0051　東京都目黒区上目黒2-1-1 中目黒GTタワー20F

 ======================================================================
