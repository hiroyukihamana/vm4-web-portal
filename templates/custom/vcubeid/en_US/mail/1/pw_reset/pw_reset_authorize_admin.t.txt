━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using V-CUBE Meeting.

Your new password has been issued. 
We have sent an notification email to the request sender. 

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.co.jp/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
Toll Free Number: 0570-002192（Open 24/7）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
