﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using V-cube Meeting.

Your inquiry has been sent us, and we will reply as soon as possible.

------------------------------
Inquiry Details
------------------------------
■Topic:
{$subject}

■Details:
{$body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.co.jp/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
