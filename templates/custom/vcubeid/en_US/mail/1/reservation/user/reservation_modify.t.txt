━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} has changed the details for your scheduled meeting.

Click the invitation URL below at the starting time to attend the meeting.

■Topic:
{$info.reservation_name}

■Date and Time
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■Secured link to the Meeting Room:
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}>

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive {/if}Password
{if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}

------------------------------
Message from {$info.sender}
------------------------------
{$info.mail_body}

※This invitation has been sent automatically from the system.
　Please send any inquiry for this meeting to this email address.
　<{$info.sender_mail}>

------------------------------
Other Meeting Options
------------------------------
{if $info.pin_cd}
■Passcode for V-CUBE Mobile
{$info.pin_cd}
※You can attend the meeting with iPad/Android.  Start the V-CUBE Mobile application on iPad/Android, go to “Login with Passcode” and enter this passcode.

※Check below to see how to get V-CUBE Mobile application.
 {/if}
 {if $temporaryAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■Guest Meeting Address for Hardware Video Conference System
{$temporaryAddress}
※Enter this meeting address when you connect to V-CUBE Meeting from hardware type video conference system such as Polycom.
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■Teleconferencing Number and Code
{if $teleconf_info.tc_type == 'pgi'}
Call the Dial -in number, listen to the command prompt, and enter in the access code.
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Dial-in Number:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- Other Dial-in Number:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- Access Code:
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

------------------------------
Important Information
------------------------------
■How to use V-CUBE Meeting
1. Connect a webcam and a headset to your PC.
2. Click and run the Checker tool below.
   It will help you configure the proper hardware and best network connectivity.
   <{$base_url}services/tools/checker/>
3. Click the secured link invitation at the top of this email to enter the meeting.

※Please do not plug in any additional hardware after you click the secured link invitation.  It will not be available until you restart the meeting room.
※If you do not have a webcam, you can still join the meeting using a USB headset.

■System Requirements
Refer to our system requirements page.
<http://www.nice2meet.us/ja/requirements/meeting.html>

■Get V-CUBE Mobile Application
Get application for smart phones or tablets (iPad/Android) from the links below to attend the meeting.
・Application for iOS
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Application for Android
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>

------------------------------
About V-CUBE Meeting
------------------------------
V-CUBE Meeting is a cloud-based video web conferencing system.  It can be used instantly from almost any internet device.

Users can share videos, share documents, archive meetings, and increase productivity.  Please enjoy.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.co.jp/>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

