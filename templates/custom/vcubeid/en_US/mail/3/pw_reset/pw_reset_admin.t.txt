━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using V-cube Meeting.

We have received your new password request.
Please confirm and authorize or deny the following request.
 
■User ID:
{$member_id}

■Request Sender Name:
{$name}

■Click this link to authorize this request:
<{$smarty.const.N2MY_BASE_URL}a/{$authorize_key}>

■Click this link to deny this request:
<{$smarty.const.N2MY_BASE_URL}k/{$ignore_key}>

※The above request will expire in 24 hours.


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.co.jp/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
Toll Free Number: 0570-002192（Open 24/7）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
