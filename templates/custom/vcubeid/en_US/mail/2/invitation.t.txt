━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using web conferencing service.
You have been invited to the meeting in progress.
Click the invitation URL below to attend the meeting.

■Secured link to the Meeting Room:
<{$invitation_url}>
{if $reservationPassword !== ""}

■Password:
{if $reservationPassword}{$reservationPassword}{else}No Password{/if}{/if}

------------------------------
Other Meeting Options
------------------------------
{if $meeting_info.pin_cd}
■Passcode for V-CUBE Mobile
{$meeting_info.pin_cd}
※You can attend the meeting with iPad/Android.  Start the V-CUBE Mobile application on iPad/Android, go to “Login with Passcode” and enter this passcode.

※Check below to see how to get V-CUBE Mobile application.
{/if}
{if $temporaryAddress && !$reservationPassword}

■Guest Meeting Address for Hardware Video Conference System
{$temporaryAddress}
※Enter this meeting address when you connect to V-CUBE Meeting from hardware type video conference system such as Polycom.
{/if}
{if $teleconf_info.pgi_conference_id && (!$meeting_info.is_reserved && $teleconf_info.tc_type == 'voip') || ($meeting_info.is_reserved && $teleconf_info.tc_type != 'voip') }

■Conferencing Options:
{if $teleconf_info.pgi_conference_id}
- Teleconferencing
Use Teleconferencing
Call the Dial -in number, listen to the command prompt, and enter in the access code.
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Dial-in Number:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- Other Dial-in Number:
<{$teleconf_info.phone_number_url}>
{/if}
-- Access Code:
{if $guest.type == 1 && !$info.room_option.whiteboard}{$teleconf_info.pgi_l_pass_code|wordwrap:3:" ":true} (Audio Only)
{else}
    {if $teleconf_info.pgi_service_name == "GlobalMeet"}
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
    {else}
{$teleconf_info.pgi_p_pass_code|wordwrap:3:" ":true}
    {/if}
{/if}
{elseif $teleconf_info.tc_type == 'voip'}
- VoIP
You can participate with VoIP by connect a USB headset.
{elseif $teleconf_info.tc_type == 'etc'}
- Other Conferencing Service
{$teleconf_info.tc_teleconf_note}
{/if}
{/if}

------------------------------
Important Information
------------------------------
■How to use V-CUBE Meeting
1. Connect a webcam and a headset to your PC.
2. Click and run the Checker tool below.
   It will help you configure the proper hardware and best network connectivity.
   <{$base_url}services/tools/checker/>
3. Click the secured link invitation at the top of this email to enter the meeting.

※Please do not plug in any additional hardware after you click the secured link invitation.  It will not be available until you restart the meeting room.
※If you do not have a webcam, you can still join the meeting using a USB headset.

■System Requirements
Refer to our system requirements page.
<http://www.nice2meet.us/ja/requirements/meeting.html>

■Get V-CUBE Mobile Application
Get application for smart phones or tablets (iPad/Android) from the links below to attend the meeting.
・Application for iOS
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Application for Android
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>

------------------------------
About V-CUBE Meeting
------------------------------
V-CUBE Meeting is a cloud-based video web conferencing system.  It can be used instantly from almost any internet device.

Users can share videos, share documents, archive meetings, and increase productivity.  Please enjoy.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.co.jp/>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

