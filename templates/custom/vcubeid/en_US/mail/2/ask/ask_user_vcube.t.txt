﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using V-cube Meeting.

Your inquiry has been sent us, and we will reply as soon as possible.

------------------------------
Inquiry Details
------------------------------
{if $ask_category == 0}Service
{elseif $ask_category == 1}Fee
{elseif $ask_category == 2}Changing scheduled meeting
{elseif $ask_category == 3}Cancelling scheduled meeting
 {elseif $ask_category == 4}Others{/if}

{$ask_contents}

------------------------------
Information
------------------------------
■Company Name : {$ask_company_name}
■Title : {$ask_company_division}
■Phone Number : {$ask_telephone}
■FAX Number : {$ask_fax}
■Name : {$ask_name}
■E-mail Address : <{$ask_email}>

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$base_url}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.co.jp/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
