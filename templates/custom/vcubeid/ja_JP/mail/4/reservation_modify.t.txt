━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBEミーティングからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Web会議システムのご利用ありがとうございます。
下記の会議の変更内容を、メールで参加者に送信しました。

■会議名
{$info.reservation_name}

■開催日時
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会議記録{/if}パスワード
{if $info.reservation_pw}{$info.reservation_pw}{else}なし{/if}

------------------------------
会議の詳細情報
------------------------------
{if $info.pin_cd}
■V-CUBEモバイル用 暗証番号
{$info.pin_cd}
※iPad/AndroidからV-CUBEモバイルのアプリを起動し、
「暗証番号でログイン」から暗証番号を入力することでも、
Web会議への参加が可能です。
{/if}
{if $temporaryAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■テレビ会議システム専用ゲストアドレス
{$temporaryAddress}
※Polycom等のテレビ会議端末からV-CUBEミーティングに接続する場合はこちらのアドレスを入力して入室してください。
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■電話会議での参加方法
{if $teleconf_info.tc_type == 'pgi'}
アクセスポイントに電話をかけてガイダンスに従い、パスコードを入力します。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- アクセスポイント（ダイヤルイン）番号:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- その他のアクセスポイント一覧:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- パスコード
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

■会議の参加者
下記の参加者に対し、予約会議の変更をメールで通知しました。

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} 様{if $guest.type == 1} (オーディエンス){elseif $guest.type == 2} (資料ユーザ){/if}
　{$guest.email}
　招待URL: <{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}>
{/foreach}{/if}

- 参加者へのメッセージ
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE ミーティング powered by V-cube, Inc.
