━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBEミーティングからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender}様から、下記のWeb会議への招待がありました。

定刻になりましたら、招待URLをクリックして頂ければ、
Web会議へ参加することができます。

■会議名
{$info.reservation_name}

■開催日時
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■招待URL
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}>

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会議記録{/if}パスワード
{if $info.reservation_pw}{$info.reservation_pw}{else}なし{/if}

------------------------------
{$info.sender} 様からのメッセージ
------------------------------
{$info.mail_body}

※このメールはシステムから自動送信されています。
　この会議のご招待についてのお問い合わせは、下記のアドレス宛にご連絡下さい。
　<{$info.sender_mail}>

------------------------------
会議の詳細情報
------------------------------
{if $info.pin_cd}
■V-CUBEモバイル用 暗証番号
{$info.pin_cd}
※iPad/AndroidからV-CUBEモバイルのアプリを起動し、
「暗証番号でログイン」から暗証番号を入力することでも、
Web会議への参加が可能です。

※V-CUBEモバイルアプリの入手方法は、下部のご利用案内をご確認ください。
{/if}
{if $temporaryAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■テレビ会議システム専用ゲストアドレス
{$temporaryAddress}
※Polycom等のテレビ会議端末からV-CUBEミーティングに接続する場合はこちらのアドレスを入力して入室してください。
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■電話会議での参加方法
{if $teleconf_info.tc_type == 'pgi'}
アクセスポイントに電話をかけてガイダンスに従い、パスコードを入力します。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- アクセスポイント（ダイヤルイン）番号:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- その他のアクセスポイント一覧:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- パスコード
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

------------------------------
ご利用案内
------------------------------
■ご利用方法
1. Webカメラとヘッドセットを、パソコンに接続します。
2. 環境確認ページにアクセスしてください。
   ネットワークや接続機器の状況が簡易診断できます。
   <{$base_url}services/tools/checker/>
3. 上記の「招待URL」から会議室に入室します。

※会議室に入室した後、カメラやヘッドセットを接続した場合、
　一度会議室から退室し、再入室をすれば映像・音声を認識します。
※Webカメラのご用意がない場合、音声のみのご参加も可能です。

■推奨動作環境
動作環境ページにてご確認ください。
<http://www.nice2meet.us/ja/requirements/meeting.html>

■V-CUBEモバイル　アプリケーションの入手方法
スマートフォンやタブレット端末（iPad/Android）からは、
アプリをダウンロードいただくことでご利用いただけます。
・iOS用アプリケーション
<http://itunes.apple.com/jp/app/id431070449?mt=>
・Android用アプリケーション
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>

------------------------------
V-CUBEミーティングとは？
------------------------------
Web会議システム「V-CUBEミーティング」は、
インターネット回線とパソコンさえあればカンタンに会議ができます。

相手の顔を見て話ができるだけでなく、1つの資料を全員で共有したり、
会議を録画して議事録として利用することができます。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBEミーティング
<{$base_url}>
■開発・運営:
株式会社ブイキューブ <http://www.vcube.co.jp/>
■カスタマーサポートセンター　0570-07-0710
受付時間：平日 09:00～17:00
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
