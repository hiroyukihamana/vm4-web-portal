﻿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
来自V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
欢迎使用网络视频会议系统。
以下会议邀请邮件已发送至参加者。

■会议名称
{$info.reservation_name}

■召开时间
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会议记录{/if}密码
{if $info.reservation_pw}{$info.reservation_pw}{else}无{/if}

------------------------------
会议的详细信息
------------------------------
{if $info.pin_cd}
■V-CUBE Mobile使用的验证码
{$info.pin_cd}
※通过iPad/Android启动V-CUBE Mobile的应用程序，
在「用验证码登陆」中输入验证码，也可以参加网络视频会议。
{/if}
{if $temporaryAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■会议系统专用受邀者地址
{$temporaryAddress}
※从Polycom等电视会议终端连接V-CUBE Meeting时，请使用该地址进入会议室。
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■电话会议号码和密码
{if $teleconf_info.tc_type == 'pgi'}
如果你没有密码，请稍等片刻，或按＃。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- 访问点（拨入）号码:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
--  其它访问点一览:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- 密码
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

■会议的参加者已向以下参加者发送预约会议的邀请邮件。

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (听众){elseif $guest.type == 2} (资料用户){/if}
　{$guest.email}
　邀请URL: <{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}>
{/foreach}{/if}

- 发给参加者的消息
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE Meeting powered by V-cube, Inc.
