━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
感谢使用视频会议系统。

密码重置申请已被拒绝。
将以邮件形式通知申请者。


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■开发・运营:
V-cube株式会社 <http://www.vcube.co.jp/>
〒153-0051 东京都目黑区上目2-1-1 中目黑GT Tower20F
■客服中心
电话号码: 0570-002192（24小时365天电话支持）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
