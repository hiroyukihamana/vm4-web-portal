━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE 회의 공지
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE Meeting을 이용해 주셔서 감사합니다.

계정 관리자가 비밀번호 재발급 요청을 거부했습니다.
세부적인 사항은 계정 관리자에게 문의 바랍니다.


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
Tel: 0570-002192(Open 24/7)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
