━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE 회의 공지
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE Meeting을 이용해 주셔서 감사합니다.

신규 비밀번호가 발급되었습니다.
요청자에게 안내 메일을 전송하였습니다.  

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
Tel: 0570-002192(Open 24/7)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
