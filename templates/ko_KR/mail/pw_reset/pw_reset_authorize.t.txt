━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE 회의 공지
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE Meeting을 이용해 주셔서 감사합니다.

계정 관리자가 비밀번호 재발급 요청을 승인했습니다. 
다음과 같이 신규 비밀번호를 지급합니다.


■ 신규 비밀번호:
{$member_pass}


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
<{$smarty.const.N2MY_BASE_URL}>
■Developed and Managed by:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
Tel: 0570-002192(Open 24/7)
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
