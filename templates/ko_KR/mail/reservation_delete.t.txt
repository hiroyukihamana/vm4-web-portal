*이 메일은 발신 전용입니다.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE 회의 공지
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
저희 웹컨퍼런스 서비스를 이용하여 주셔서 감사합니다.
V-cube가 귀하의 회의 일정 취소 안내를 전달하였습니다.

■ 제목:
{$info.reservation_name}

■ 일시:
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■ 주최:
{$info.organizer_name} 

■ 회의 참가자:
다음 참가자에게 일정 취소 안내를 전달하였습니다.

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (Audience){elseif $guest.type == 2} (Whiteboard){/if}
　{$guest.email}
{/foreach}{/if}

- 참가자에게 전송된 메시지
　{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Developed and Managed by:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
