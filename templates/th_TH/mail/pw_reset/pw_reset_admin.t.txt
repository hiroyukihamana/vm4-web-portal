━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ประกาศจากห้องประชุม V-CUBE
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ขอบคุณที่ใช้ห้องประชุม V-CUBE

ข้อมูลของคุณที่สอบถามได้ถูกส่งมาให้เราแล้ว  
และเราจะตอบกลับโดยเร็วที่สุด

■ชื่อผู้ใช้
{$member_id}

■ชื่อผู้ส่งคำขอ
{$name}

■คลิกที่ลิงค์นี้เพื่ออนุมัติคำขอนี้
<{$smarty.const.N2MY_BASE_URL}a/{$authorize_key}>

■คลิกที่ลิงค์นี้เพื่อปฏิเสธคำขอนี้
<{$smarty.const.N2MY_BASE_URL}k/{$ignore_key}>

※คำขอดังกล่าวจะหมดอายุภายใน 24 ชั่วโมง


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■ห้องประชุม V-CUBE
<{$smarty.const.N2MY_BASE_URL}>
■พัฒนาและดูแลโดย
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■ศูนย์บริการลูกค้า:
โทรศัพท์: 0570-002192 (เปิด 24/7)
 E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
