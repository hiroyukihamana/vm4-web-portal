กรุณาอย่าตอบกลับอีเมลนี้

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ประกาศจากห้องประชุม V-CUBE
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ขอขอบคุณที่ใช้บริการประชุมผ่านเว็บ 
คุณได้รับเชิญเข้าร่วมการประชุมนี้
คลิกที่ URL เชิญด้านล่างเพื่อเข้าร่วมประชุม

■ลิงค์ที่มีความปลอดภัยเชื่อมโยงไปยังห้องประชุม
{if $info.guest_url_format ==1}
{$invitation_url}
{else}
<{$invitation_url}>
{/if}
{if $reservationPassword !== ""}

■รหัสผ่าน
{if $reservationPassword}{$reservationPassword}{else}なし{/if}{/if}

------------------------------
ตัวเลือกอื่นๆในการประชุม
------------------------------
{if $meeting_info.pin_cd}
■รหัสผ่านสำหรับ V-CUBE บนมือถือ
{$meeting_info.pin_cd}
※คุณสามารถเข้าร่วมประชุมด้วย iPad / Android โดยใช้ผ่านโปรแกรม V-CUBE บนมือถือใน iPad / Android ให้ไปที่ "Login with Passcode" และใส่รหัสผ่านนี้

※ตรวจสอบรายละเอียดด้านล่างเพื่อดูวิธีการรับโปรแกรม V-CUBE บนมือถือ
{/if}
{if ($temporarySipNumberAddress||$temporarySipAddress||$temporaryH323NumberAddress||$temporaryH323Address) && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■ที่อยู่สำหรับผู้เข้าร่วมประชุมผ่านระบบการประชุมทางวิดีโอ
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※ใส่ที่อยู่การประชุมครั้งนี้เมื่อคุณต้องการเชื่อมต่อเข้าห้องประชุม V-CUBE ผ่านระบบการประชุมทางวิดีโอ เช่น Polycom
{/if}
{if $room_info.options.telephone == "1" && $meeting_info.pin_cd}

■หมายเลขและรหัสสำหรับการประชุมทางไกล
คุณสามารถเข้าร่วมประชุมโดยกดหมายเลขโทรศัพท์ด้านล่างจากโทรศัพท์บ้านหรือมือถือ กรุณาฟังคำสั่งแจ้งและใส่รหัสเข้าถึงด้านล่างนี้
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■รหัสเข้าถึง
{$meeting_info.pin_cd}
{/if}
{if $teleconf_info.pgi_conference_id && (!$meeting_info.is_reserved && $teleconf_info.tc_type == 'voip') || ($meeting_info.is_reserved && $teleconf_info.tc_type != 'voip') }

■ตัวเลือกการประชุม
โทรหมายเลขเชื่อมต่อ, ฟังคำสั่ง, และป้อนรหัสในการเข้าถึง
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- หมายเลขเชื่อมต่อ:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- หมายเลขเชื่อมต่ออื่นๆ:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- รหัสเข้าถึง
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}

------------------------------
ข้อมูลสำคัญ
------------------------------
■วิธีการใช้งานห้องประชุม V-CUBE 
1. เชื่อมต่อเว็บแคมและชุดหูฟังเข้ากับเครื่องพีซีของคุณ 
2. คลิกและเรียกใช้เครื่องมือตรวจสอบรายละเอียดด้านล่าง    
มันจะช่วยให้คุณกำหนดค่าฮาร์ดแวร์ที่เหมาะสมและการเชื่อมต่อเครือข่ายที่ดีที่สุด
{if $info.guest_url_format ==1}
{$base_url}services/tools/checker/
{else}
<{$base_url}services/tools/checker/>
{/if}
3. คลิกที่ลิงค์เชิญที่มีความปลอดภัยที่ด้านบนของอีเมล์นี้เพื่อเข้าสู่การประชุม 

※กรุณาอย่าเสียบในอุปกรณ์ใด ๆ เพิ่มเติมหลังจากที่คุณคลิกลิงค์เชิญที่มีความปลอดภัย มันจะไม่สามารถใช้ได้จนกว่าคุณจะเริ่มต้นห้องประชุมใหม่ 
※หากคุณไม่มีเว็บแคมคุณยังสามารถเข้าร่วมประชุมโดยใช้ชุดหูฟัง USB

■ความต้องการของระบบ 
อ้างถึงส่วนที่เราแจ้งความต้องการของระบบ
{if $info.guest_url_format ==1}
http://www.nice2meet.us/ja/requirements/meeting.html
{else}
<http://www.nice2meet.us/ja/requirements/meeting.html>
{/if}

■รับโปรแกรม V-CUBE บนมือถือ 
รับโปรแกรมสำหรับสมาร์ทโฟนหรือแท็บเล็ต (iPad / Android) จากลิงก์ด้านล่างเพื่อเข้าร่วมประชุม
・โปรแกรมสำหรับ iOS 
{if $info.guest_url_format ==1}
http://itunes.apple.com/jp/app/id431070449?mt=
{else}
<http://itunes.apple.com/jp/app/id431070449?mt=>
{/if}
・โปรแกรมสำหรับ Android 
{if $info.guest_url_format ==1}
https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4
{else}
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>
{/if}

------------------------------
เกี่ยวกับห้องประชุม V-CUBE 
------------------------------
ห้องประชุม V-CUBE เป็นระบบการประชุมทางเว็บวิดีโอบนเทคโนโลยีคลาวด์ 
มันสามารถใช้ได้ทันทีจากอุปกรณ์หลายประเภทที่เชื่อมต่ออินเทอร์เน็ต 

ผู้ใช้สามารถแบ่งปันวิดีโอ, เอกสาร, เก็บข้อมูลการประชุม และเพิ่มผลผลิต กรุณาเพลิดเพลิน

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■ห้องประชุม V-CUBE
{if $info.guest_url_format ==1}{$smarty.const.N2MY_BASE_URL}{else}<{$smarty.const.N2MY_BASE_URL}>{/if} 
■พัฒนาและดูแลโดย
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
