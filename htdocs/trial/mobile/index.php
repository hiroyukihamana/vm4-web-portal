<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/mgm/dbi/user.dbi.php");
class N2MY_Trial_Mobile extends AppFrame
{

    public $obj_N2MY_Account;
    public $obj_User;
    public $obj_AuthUser;
    var $account_dsn = null;

    function init() {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->obj_AuthUser = new MgmUserTable( $this->account_dsn );
        $this->obj_User = new UserTable($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function default_view () {
        $this->action_trial_add();
    }

    function action_trial_add($message="") {

        $user_info = ( 1 == $this->request->get( "action_trial_add") ) ? "" : $this->session->get('user_info');
        // エラー表示
        if($message){
            $this->template->assign('message', $message);
        }
        // 入力データ再表示
        if($user_info){
            foreach($user_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        // 入力画面表示
        $this->display('trial/mobile/index.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    function action_trial_confirm() {

        $request = $this->request->getAll();
        $this->set_submit_key($this->_name_space);
        $this->session->set('user_info', $request);
        $rules = array(
            "trial_user_id" => array(
                "required" => true,
                "regex"       => "/^[[:alnum:]._-]{3,30}$/"
                ),
            "trial_password" => array(
                "required" => true,
                "regex"       => "/^[!-\[\]-~]{8,128}$/"
                ),
            "trial_password_retype" => array(
                "required" => true,
                ),
            "trial_name" => array(
                "required" => true,
                "maxlen" => 30,
                ),
            "trial_company_name" => array(
                "required" => true,
                "maxlen" => 30,
                ),
            "trial_email" => array(
                "required" => true,
                "email" => true,
                ),
            "trial_telephone" => array(
                "required" => true,
                "regex" => "/^0\d{1,5}-\d{0,4}-?\d{4}$/",
                ),
        );
        $check_obj = new EZValidator($request);
        foreach($rules as $field => $rules) {
            $check_obj->check($field, $rules);
        }
        //IDチェック
        $where = "user_id = '".mysql_real_escape_string($request["trial_user_id"])."'";
        $count = $this->obj_AuthUser->numRows($where);
        if (EZValidator::isError($check_obj)) {
            $err_msg = $this->_get_error_info($check_obj);
        }
        if (!preg_match('/^[[:alnum:]._-]{3,30}$/', $request['trial_user_id'])) {
            $err_msg["trial_user_id"]["err_msg"] =  USER_ERROR_ID_LENGTH;
        }
        if (is_numeric($count) && $count > 0) {
            $err_msg["trial_user_id"]["err_msg"] = MEMBER_ERROR_ID_EXIST;
        }
        if ($request["trial_password"] != $request["trial_password_retype"]) {
            $err_msg["trial_password_retype"]["err_msg"] = MEMBER_ERROR_PASS_DIFF;
        } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $request['trial_password'])) {
            $err_msg["trial_password"]["err_msg"] = USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$request['trial_password']) || preg_match('/^[[:alpha:]]+$/',$request['trial_password'])) {
            $err_msg["trial_password"]["err_msg"] = USER_ERROR_PASS_INVALID_02;
        }
        if ($err_msg){
            $this->logger->info("error",__FILE__,__LINE__,$err_msg);
            $this->action_trial_add($err_msg);
            exit;
        }
        foreach($request as $key => $value){
                $this->template->assign($key, $value);
            }
        $this->display('trial/mobile/confirm.t.html');
    }

    function action_trial_complete () {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            header( "Location: ?action_trial_add=1" );
            exit;
        }
        $request = $this->session->get('user_info');
        $user_data = array(
            "user_id"               => $request["trial_user_id"],
            "user_password"         => $request["trial_password"],
            "user_admin_password"   => $request["trial_user_id"],
            "account_model"         => "try_mobile",
            "user_company_name"     => $request["trial_company_name"],
            "user_company_phone"    => $request["trial_telephone"],
            "user_staff_department" => $request["trial_company_division"],
            "user_staff_firstname"  => $request["trial_name"],
            "user_staff_email"      => $request["trial_email"],
            "user_status"           => "0",
            "user_registtime" => date("Y-m-d H:i:s"),
        );
        $ret = $this->obj_User->add($user_data);
        if (DB::isError($ret)) {
            $this->logger->error($ret->getUserInfo());
            return false;
        }

        //アカウントテーブルに追加
        $user_acount_data = array(
            "user_id"         => $request["trial_user_id"],
            "server_key"      => "1",
            "user_id"         => $request["trial_user_id"],
            "user_registtime" => date("Y-m-d H:i:s"),
            );
        $ret = $this->obj_AuthUser->add($user_acount_data);
        if (DB::isError($ret)) {
            $this->logger->error($ret->getUserInfo());
            return false;
        }

        //regist_keyをrelationテーブルに追加
        $regist_id = sha1("trial_mobile".uniqid(rand(), true));
        require_once 'classes/mgm/MGM_Auth.class.php';
        $objMgmRelationTable = new MGM_AuthClass(N2MY_MDB_DSN);
        $this->logger2->info(array($request["trial_user_id"], $regist_id, "trial_mobile"));
        if (!$result = $objMgmRelationTable->addRelationData($request["trial_user_id"], $regist_id, "trial_mobile")) {
            return false;
        }

        // mail送信
        if ($request["trial_email"]) {
            $this->template->assign("trial_name", $request["trial_name"]);
            $this->template->assign("regist_key", $regist_id);
            $this->template->assign('base_url', N2MY_BASE_URL);
            require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
            $mail = new EZSmtp(null, $this->_lang, "UTF-8");
            $body = $this->fetch('ja_JP/mail/trial/mobile.t.txt');
            //$body = $this->fetch($this->_lang.'/mail/trial/mobile.t.txt');
            $mail->setSubject(TRIAL_MOBILE_SUBJECT_FOR_USER);
            $mail->setTo($request["trial_email"]);
            $mail->setFrom(USER_ASK_FROM);
            $mail->setReturnPath(USER_ASK_FROM);
            $mail->setBody($body);
            $mail->send();
        }
        $this->session->remove('user_info');
        $this->display('trial/mobile/complete.t.html');

    }

    function action_regist() {
        $regist_key = $this->request->get("id");
        if (!$regist_key) {
            $this->logger2->info($regist_key, "regist_key notfound");
            $this->display('trial/mobile/error.t.html');
            exit;
        }
        require_once 'classes/mgm/MGM_Auth.class.php';
        $objMgmRelationTable = new MGM_AuthClass(N2MY_MDB_DSN);
        if (!$server_info = $objMgmRelationTable->getRelationDsn($regist_key, "trial_mobile")) {
            $this->logger2->info($regist_key, "トライアルアカウント発行regist_keyエラー");
            $this->display('trial/mobile/error.t.html');
            exit;
        } else {
            $this->session->set("server_info", $server_info );
        }

        if (!$relation_info = $objMgmRelationTable->getRelationInfo($regist_key)) {
            $this->logger2->info($regist_key, "トライアルアカウント発行regist_keyエラー");
            $this->display('trial/mobile/error.t.html');
            exit;
        } else {
            $objUser = new UserTable($this->get_dsn());
            $user_where = " user_id = '".addslashes($relation_info["user_key"])."'" .
                    " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                    " AND account_model = 'try_mobile'".
                    " AND user_status = 0";
            $user_info = $objUser->getRow($user_where);
        }

        //部屋データ登録
        require_once( "classes/dbi/room.dbi.php" );
        require_once( "classes/dbi/room_plan.dbi.php" );
        $objRoomPlanTable = new RoomPlanTable( $this->get_dsn() );
        $service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
        $service_info = $service_db->getRow("service_key = '41' AND service_status = 1");
        $where = sprintf( "user_key=%d", $user_info["user_key"] );
        $obj_Room = new RoomTable( $this->get_dsn() );
        $count = $obj_Room->numRows($where);
        $room_key = $user_info["user_id"]."-".($count + 1)."-".substr( md5( uniqid( time(), true ) ), 0, 4 );
        $room_sort = $count + 1;
        //部屋登録
        $room_data = array (
            "room_key"              => $room_key,
            "max_seat"              => ($service_info["max_seat"]) ? $service_info["max_seat"] : 10,
            "max_audience_seat"     => ($service_info["max_audience_seat"]) ? $service_info["max_audience_seat"] : 0,
            "max_whiteboard_seat"   => ($service_info["max_whiteboard_seat"]) ? $service_info["max_whiteboard_seat"] : 0,
            "user_key"              => $user_info["user_key"],
            "room_name"             => $user_info["user_id"],
            "room_sort"             => $room_sort,
            "room_status"           => "1",
            "room_registtime"       => date("Y-m-d H:i:s"),
            );
        $this->logger2->info($room_data);
        $add_room = $obj_Room->add($room_data);
        if (DB::isError($add_room)) {
            $this->logger2->info( $add_room->getUserInfo());
        }
        $data = array(
                    "room_key"=>$room_key,
                    "service_key"=>$service_info["service_key"],
                    "discount_rate"=>0,
                    "room_plan_starttime"=> date("Y-m-d H:i:s"),
                    "room_plan_endtime"=> date("Y-m-d H:i:s", strtotime("+1 week")),
                    "room_plan_status"=>"1",
                    "room_plan_registtime"=>date("Y-m-d H:i:s"),
                    "room_plan_updatetime"=>date("Y-m-d H:i:s"),
                     );
        $addPlan = $objRoomPlanTable->add( $data );
        if (DB::isError( $addPlan ) ) {
            $this->logger2->info( $addPlan->getUserInfo() );
        }
/*
        // userのプランオプション(user_service_option)からmemberの部屋のオプションを追加する(ordered_service_option)
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable( $this->get_dsn() );

        $this->objMeetingDB = new N2MY_DB( $this->get_dsn(), "user_service_option" );
//            $condition = sprintf( "user_key='%s' AND user_service_option_starttime<='%s' AND user_service_option_status=1", $user_info["user_key"], date( "Y-m-d" ) );
        $condition = sprintf( "user_key='%s' AND service_option_key !=14", $user_info["user_key"] );
        $optionList = $this->objMeetingDB->getRowsAssoc( $condition );

        for( $i = 0; $i < count( $optionList ); $i++ ){
            $data = array(
                "room_key" => $room_key,
                "user_service_option_key" => $optionList[$i]["user_service_option_key"],
                "service_option_key" => $optionList[$i]["service_option_key"],
                "ordered_service_option_status" => $optionList[$i]["user_service_option_status"],
                "ordered_service_option_starttime" => $optionList[$i]["user_service_option_starttime"],
                "ordered_service_option_registtime" => $optionList[$i]["user_service_option_registtime"],
                "ordered_service_option_deletetime" => $optionList[$i]["user_service_option_deletetime"]
            );
            $ordered_service_option->add( $data );
        }
        */

        // regist_key破棄
        require_once 'classes/mgm/MGM_Auth.class.php';
        $objMgmRelationTable = new MGM_AuthClass(N2MY_MDB_DSN);
        $objMgmRelationTable->deleteRelationData($regist_key, "trial_mobile");
        return true;
        $this->display('trial/mobile/regist.t.html');
    }

    function _get_error_info($err_obj) {
        $err_fields = $err_obj->error_fields();
        if (!$err_fields) {
            return array();
        } else {
            $msg_format = $this->get_message("error");
            foreach($err_fields as $field) {
                $type = $err_obj->get_error_type($field);
                $err_rule = $err_obj->get_error_rule($field, $type);
                if (($type == "allow") || $type == "deny") {
                    $err_rule = join(", ", $err_rule);
                }
                $error_info[$field] = array(
                    "err_cd"  => $type,
                    "err_msg" => sprintf($this->get_message("ERROR", $type), $err_rule),
                    );
            }
            return $error_info;
        }
    }

}
$main = new N2MY_Trial_Mobile();
$main->execute();
