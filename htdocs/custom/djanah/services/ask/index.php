<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");
require_once('config/config.inc.php');

class AppAsk extends AppFrame
{

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        if($this->config->get('SMARTY_DIR','custom') != "djanah") {
            header("HTTP/1.1 404 Not Found");
            $this->display("user/404_error.t.html");
            exit;
        }
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->action_ask();
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * フォームの表示
     */
    function action_ask($message="") {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        // エラー表示
        if($message){
            $this->template->assign('message', $message);
        }

        // 入力データ再表示
        if($ask_info = $this->session->get('user_ask_info')){
            foreach($ask_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }

        // 入力画面表示
        $this->display('user/ask/index.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
    /**
     * 確認処理
     */
    function action_ask_confirm(){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $request = $this->request->getAll();
        $this->session->set('user_ask_info', $request);

        $message = "";
        if(!$request['ask_contents']){
            $message .= "<li>".USER_ASK_ERROR_CONTENTS ."</li>";
        }
        if(!$request['ask_name']){
            $message .= "<li>".USER_ASK_ERROR_NAME ."</li>";
        }
        if(!$request['ask_email']){
            $message .= "<li>".USER_ASK_ERROR_EMAIL ."</li>";
        }elseif( ! EZValidator::valid_email( $request['ask_email'] ) ){
            $message .= "<li>".USER_ASK_ERROR_INVALIDEMAIL ."</li>";
        }

        // 入力チェック
        if($message != ""){
            $this->action_ask($message);
        }else{
            // 入力データ表示
            foreach($request as $key => $value){
                $this->template->assign($key, $value);
            }
            // 確認画面表示
            $this->display('user/ask/confirm.t.html');
        }

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
    /**
     * 完了処理
     */
    function action_ask_complete(){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        if($ask_info = $this->session->get('user_ask_info')){
            foreach($ask_info as $key => $value){
                $this->template->assign($key, $value);
            }
            require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
            
            if($this->session->get("login")) {
                $user_info = $this->session->get('user_info');
                $this->template->assign('user_id', $user_info['user_id']);
            }
            else {
                $this->template->assign('user_id', '');
            }
            $this->template->assign('n2my_address', $_SERVER['HTTP_HOST']);
            //v-cube
            $mail = new EZSmtp(null, "", "UTF-8");
            $body = $this->fetch('common/mail/user_ask.m.txt');
            $mail->setTo($this->config->get("DJANAH_ASK", "user_ask_to", USER_ASK_TO));
            $mail->setFrom($this->config->get("DJANAH_ASK", "user_ask_from", USER_ASK_FROM));
            $mail->setReturnPath($this->config->get("DJANAH_ASK", "user_ask_from", USER_ASK_FROM));
            $mail->setSubject($this->get_message("DEFINE", "USER_ASK_SUBJECT"));
            $mail->setBody($body);
            $mail->send();
            // user
            $mail = new EZSmtp(null, $this->_lang, "UTF-8");
            $body = $this->fetch($this->_lang . '/mail/ask/ask_user_vcube.t.txt');
            $mail->setSubject($this->get_message("DEFINE", "ASK_SUBJECT_FOR_USER"));
            $mail->setTo($ask_info['ask_email']);
            $mail->setFrom($this->config->get("DJANAH_ASK", "user_ask_from", USER_ASK_FROM));
            $mail->setReturnPath($this->config->get("DJANAH_ASK", "user_ask_from", USER_ASK_FROM));
            $mail->setBody($body);
            $mail->send();
            $this->session->remove('user_ask_info');
        }
        // 完了画面表示
        $this->display('user/ask/complete.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    // override
    function fetch($template_file) {
        $custom = $this->config->get('SMARTY_DIR','custom');
        $template_filepath = ($custom == "" ? '' : "custom/$custom/") . $template_file;
        if(!file_exists($this->template->template_dir . $template_filepath)) {
            $template_filepath = $template_file;
        }
        return parent::fetch($template_filepath);
    }

    function get_message($group, $param = null, $default = "") {
        $custom = $this->config->get('SMARTY_DIR', 'custom');
        $custom_msg_file = N2MY_APP_DIR . "config/custom/$custom/" . $this->_lang . "/message.ini";
        $custom_msg_list = parse_ini_file($custom_msg_file, true);
        $message = $this->_message;
        foreach($custom_msg_list as $grp => $vals) {
            if(isset($message[$grp]) && is_array($message[$grp])) {
                $message[$grp] = array_merge($message[$grp], $vals);
            }
            else {
                $message[$grp] = $vals;
            }
        }
        if($param === null) {
            if(isset($message[$group]) &&
                $message[$group] !== ''){
                return $message[$group];
            } 
            else {
                return $default;
            }
        }
        else {
            if(isset($message[$group][$param]) &&
                $message[$group][$param] !== ''){
                return $message[$group][$param];
            }
            else {
                return $default;
            }
        }
    }
}

$main =& new AppAsk();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
