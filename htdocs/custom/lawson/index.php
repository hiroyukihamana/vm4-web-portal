<?php
require_once ('classes/AppFrame.class.php');
require_once ('classes/dbi/room.dbi.php');
require_once ('classes/N2MY_Account.class.php');
require_once ('lib/EZLib/EZCore/EZLogger2.class.php');

class AppLawsonMain extends AppFrame {

	var $logger2 = null;
	var $obj_room = null;
	var $base_url = null;
	var $all_users = null;
	var $group_rooms = null;
	var $ip_address = null;

	function init() {
		$this -> obj_room = new RoomTable($this -> get_dsn());
		$this -> base_url = N2MY_BASE_URL . "api/v1/user/";
		$this -> all_users = $this -> get_all_users();
		$this -> group_rooms = $this -> get_all_rooms();
		$this -> ip_address = array('china' => '116.246.14.1/24', 'thailand' => '61.47.81.1/24', 'indonesia' => '182.253.205.1/24', 'japan' => '210.225.204.1/24');
	}

	function default_view() {
		$country = $this -> check_ip_address();
		$_country;
		switch ($country) {
			case 'china' :
				$_country = '接続中のネットワーク　：　中国専用回線です';
				break;
			case 'thailand' :
				$_country = '接続中のネットワーク　：　タイ専用回線です';
				break;
			case 'indonesia' :
				$_country = '接続中のネットワーク　：　インドネシア専用回線です';
				break;
			case 'japan' :
				$_country = '接続中のネットワーク　：　日本経由VPNです';
				break;
			case 'others' :
				$_country = '接続中のネットワーク　：　専用回線に接続していません';
		}
		$this -> template -> assign("country", $_country);
		$this -> template -> assign("url", "/custom/lawson/index.php");

		$category = $this -> request -> get('category');
		if ($category == '99plus') {
			$this -> template -> assign("room_groups", $this -> group_rooms['99plus']);
			$this -> session -> set("category", "99plus");
		} else {
			$this -> template -> assign("room_groups", $this -> group_rooms['normal']);
			$this -> session -> set("category", "normal");
		}
		$template_file = N2MY_APP_DIR . "templates/custom/lawson/common/user/index.t.html";
		$this -> template -> display($template_file);
	}

	/**
	 * curl->post->api_login
	 */
	function do_login($user_key) {
		$user_info = $this -> all_users[$user_key];
		$parameters = array('id' => $user_info['id'], 'pw' => $user_info['pw'], 'lang' => 'ja', 'country' => 'jp', 'timezone' => '9', 'enc' => null, 'output_type' => 'xml', 'login_type' => null);
		$url = $this -> base_url . "index.php?action_login=&" . http_build_query($parameters);
		$ch = curl_init();
		$options = array(CURLOPT_URL => $url, CURLOPT_CUSTOMREQUEST => "GET", CURLOPT_RETURNTRANSFER => 1, CURLOPT_CONNECTTIMEOUT => 10, CURLOPT_TIMEOUT => 10);
		$this -> logger2 -> info($options);
		curl_setopt_array($ch, $options);
		$ret = simplexml_load_string(curl_exec($ch));
		curl_close($ch);
		if ($ret) {
			return (string)$ret -> data -> session;
		} else {
			return false;
		}
	}

	/**
	 * session check
	 */
	function check_session_exists($user_key) {
		$_session = $this -> session -> get($user_key);
		if (!$_session) {
			$_session = $this -> do_login($user_key);
			$this -> session -> set($user_key, $_session);
		}
		return $_session;
	}

	/**
	 * get country info from ip address
	 */
	function check_ip_address() {
		require_once ('lib/pear/Net/IPv4.php');
		$ipv4 = new Net_IPv4();
		foreach ($this->ip_address as $country => $ips) {
			if ($ipv4 -> ipInNetwork($_SERVER['REMOTE_ADDR'], $ips)) {
				return $country;
			}
		}
		return "others";
	}

	/**
	 * config/room_list.xmlからルーム情報取る
	 */
	function get_all_rooms() {
		$room_list = simplexml_load_file(N2MY_APP_DIR . 'config/custom/lawson/room_list.xml');
		// $this -> logger2 -> info($room_list);
		$rooms = array();
		foreach ($room_list as $category) {
			$category_id = (string)$category -> id;
			foreach ($category->group as $group) {
				$group_id = (string)$group -> group_id;
				$rooms[$category_id][$group_id]['group_id'] = $group_id;
				$rooms[$category_id][$group_id]['group_name'] = (string)$group -> group_name;
				$rooms[$category_id][$group_id]['group_room_max_seat'] = (string)$group -> group_room_max_seat;
				$group_rooms = array();
				$group_room_index = 0;
				foreach ($group->rooms->room as $room) {
					$group_rooms[$group_room_index]['room_id'] = (string)$room -> room_id;
					$group_rooms[$group_room_index]['room_name'] = (string)$room -> room_name;
					$group_room_index++;
				}
				$rooms[$category_id][$group_id]['rooms'] = $group_rooms;
				$rooms[$category_id][$group_id]['room_count'] = count($group_rooms);
			}
		}
		return $rooms;
	}

	/**
	 * config/user_list.xmlからユーザー情報取る
	 */
	function get_all_users() {
		$user_list = simplexml_load_file(N2MY_APP_DIR . 'config/custom/lawson/user_list.xml');
		$users = array();
		foreach ($user_list->user as $user) {
			$user_id = (string)$user -> user_id;
			$users[$user_id]['id'] = $user_id;
			$users[$user_id]['pw'] = (string)$user -> user_pw;
		}
		return $users;
	}

	/**
	 * ルームstatusを取る
	 */
	function action_get_room_status() {

		$group_id = $this -> request -> get('group_id');
		$category = $this -> session -> get("category");
		if ($category == '99plus') {
			$rooms = $this -> group_rooms['99plus'][$group_id];
		} else {
			$rooms = $this -> group_rooms['normal'][$group_id];
		}
		$url = $this -> base_url . 'index.php?action_get_room_status=&';
		$mh = curl_multi_init();
		$ch_list = array();
		foreach ($rooms['rooms'] as $_room) {
			$parameters = array();
			$n2my_session = $this -> check_session_exists($_room['room_id']);
			if ($n2my_session) {
				$room_info = $this -> obj_room -> getRowsAssoc(sprintf("room_name = '%s'", $_room['room_id']));
				$parameters['n2my_session'] = $n2my_session;
				$parameters['room_id'] = $room_info[0]['room_key'];
				$_url = $url . http_build_query($parameters);
				$ch_list[$_room['room_id']] = curl_init();
				$options = array(CURLOPT_URL => $_url, CURLOPT_CUSTOMREQUEST => "GET", CURLOPT_RETURNTRANSFER => 1, CURLOPT_CONNECTTIMEOUT => 10, CURLOPT_TIMEOUT => 10);
				curl_setopt_array($ch_list[$_room['room_id']], $options);
				curl_multi_add_handle($mh, $ch_list[$_room['room_id']]);
			}
		}
		$running = null;
		do {
			curl_multi_exec($mh, $running);
		} while ($running > 0);
		// Get content and remove handles.
		$rooms_status = array();
		$index = 0;
		foreach ($ch_list as $_ch_index => $_ch_val) {
			$ret = simplexml_load_string(curl_multi_getcontent($_ch_val));
			$rooms_status[$index]['status'] = (string)$ret -> data -> room_status -> status;
			$rooms_status[$index]['pcount'] = (string)$ret -> data -> room_status -> pcount;
			$rooms_status[$index]['room_name'] = $_ch_index;
			curl_multi_remove_handle($mh, $_ch_val);
			$index++;
		}
		curl_multi_close($mh);
		$this -> logger2 -> info($rooms_status);
		$this -> logger2 -> info($this -> session);
		echo json_encode($rooms_status);
	}

	/**
	 * 入室画面に遷移
	 */
	function action_enter_room() {
		$user_key = $this -> request -> get('user_key');
		$n2my_session = $this -> check_session_exists($user_key);
		if ($n2my_session) {
			$room_info = $this -> obj_room -> getRowsAssoc(sprintf("room_name = '%s'", $user_key));
			$parameters = array('n2my_session' => $n2my_session, 'room_id' => $room_info[0]['room_key'], 'flash_version' => 'as3');
			$url = $this -> base_url . "meeting/index.php?action_start=&" . http_build_query($parameters);
			$ch = curl_init();
			$options = array(CURLOPT_URL => $url, CURLOPT_CUSTOMREQUEST => "GET", CURLOPT_RETURNTRANSFER => 1, CURLOPT_CONNECTTIMEOUT => 10, CURLOPT_TIMEOUT => 10);
			curl_setopt_array($ch, $options);
			$this -> logger2 -> info($options);
			$ret = simplexml_load_string(curl_exec($ch));
			curl_close($ch);
			if ($ret) {
				$meeting_room_url = (string)$ret -> data -> url;
				echo $meeting_room_url;
				exit ;
			} else {
				echo "error";
				exit ;
			}
		} else {
			echo "error";
			exit ;
		}
	}

	/**
	 * 予約画面に遷移
	 */
	function action_enter_reservation_page() {
		$user_key = $this -> request -> get('user_key');
		$n2my_session = $this -> check_session_exists($user_key);
		if ($n2my_session) {
			$main_page_url = N2MY_BASE_URL . 'services/?n2my_session=' . $n2my_session;
			$this -> logger2 -> info($main_page_url);
			echo $main_page_url;
			exit ;
		} else {
			echo "error";
			exit ;
		}
	}

}

$main = &new AppLawsonMain();
$main -> execute();
?>