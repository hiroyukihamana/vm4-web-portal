<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:Hirohsi                                                      |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//

require_once("classes/AppFrame.class.php");
//require_once('classes/authorization.class.php');
require_once("classes/N2MY_Account.class.php");
require_once('config/config.inc.php');

class AppFaq extends AppFrame {

    var $logger = null;
    var $_lang_num = 0;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        if($this->config->get('SMARTY_DIR','custom') != "pigeone") {
            header("HTTP/1.1 404 Not Found");
            $this->display("user/404_error.t.html");
            exit;
        }
    }

    private function get_faq_file_path($type) {
        $path = $this->config->get("PIGEONE_FAQ", "faq_file_base_dir", '');
        
        if(strlen($path) > 0) {
            switch($type) {
                case 'question':
                    $path .= $this->get_language() . '/faq_question.csv';
                    break;
                    
                case 'answer':
                    $path .= $this->get_language() . '/faq_answer.csv';
                    break;
                    
                default:
                    $path = '';
            }
        }
        return (strlen($path) > 0) ? $path : false;
    }

    function action_answer(){
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__);
        $cat_num = $_REQUEST['cat'];

        $faq_list = array();

        // faqデータをcsvから所得
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
        // answerを取得
        $csv_ans = new EZCsv(true, "UTF-8", "UTF-8", "\t", true);
        $file = $this->get_faq_file_path('answer');
        // answerを配列にする
        $ans_list = array();
        if($csv_ans->open($file, "r") == true) {
            while($faq_ans = $csv_ans->getNext(true)){
            	$faq_ans['faq_answer'] = strip_tags($faq_ans['faq_answer'], '<a>');
                $ans_list[$faq_ans['faq_key']] = $faq_ans;
            }
		}

        // questionをを取得
        $csv_question = new EZCsv(true, "UTF-8", "UTF-8", "\t", true);
        $file = $this->get_faq_file_path('question');
        // faqリスト作成
        if($csv_question->open($file, "r") == true) {
            while($faq = $csv_question->getNext(true) ){
                $faq_category = $faq['faq_category'];
                $faq_subcategory = $faq['faq_subcategory'];
                $q_and_a = array($faq['faq_question'],$ans_list[$faq['faq_key']]['faq_answer'],$faq['faq_key']);
                if(!isset($faq_list[$faq_category][$faq_subcategory])){
                    $faq_list[$faq_category][$faq_subcategory] = array();
                    array_push($faq_list[$faq_category][$faq_subcategory], $q_and_a);
                }else{
                    array_push($faq_list[$faq_category][$faq_subcategory], $q_and_a);
                }
            }
        }

        $this->template->assign('cat_num', $cat_num);
        $this->template->assign('general', isset($faq_list[$cat_num][1]) ? $faq_list[$cat_num][1] : array());
        $this->template->assign('meeting', isset($faq_list[$cat_num][2]) ? $faq_list[$cat_num][2] : array());
        $this->template->assign('seminar', isset($faq_list[$cat_num][3]) ? $faq_list[$cat_num][3] : array());

        $this->display('user/faq/answer.t.html');
        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);

    }

    function default_view() {
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__);

        // faqデータをcsvから所得
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
        $csv = new EZCsv(true, "UTF-8", "UTF-8", "\t", true);
        $file = $this->get_faq_file_path('question');
        if($csv->open($file, "r") == true) {
            $header = $csv->getHeader();
            while($faq = $csv->getNext(true)){
                $faq_category = $faq['faq_category'];
                $faq_subcategory = $faq['faq_subcategory'];
                if (trim($faq['faq_question'])) {
                    $q_and_key = array($faq['faq_question'],$faq['faq_key'],$faq['faq_category']);
                        if(!isset($faq_list[$faq_category][$faq_subcategory])){
                        $faq_list[$faq_category][$faq_subcategory] = array();
                        array_push($faq_list[$faq_category][$faq_subcategory], $q_and_key);
                    } else {
                        array_push($faq_list[$faq_category][$faq_subcategory], $q_and_key);
                    }
                }
            }
        }

        $this->template->assign('service_general',  isset($faq_list[1][1]) ? $faq_list[1][1] : array());
        $this->template->assign('service_meeting',  isset($faq_list[1][2]) ? $faq_list[1][2] : array());
        $this->template->assign('service_seminar',  isset($faq_list[1][3]) ? $faq_list[1][3] : array());
        $this->template->assign('price_general',    isset($faq_list[2][1]) ? $faq_list[2][1] : array());
        $this->template->assign('price_meeting',    isset($faq_list[2][2]) ? $faq_list[2][2] : array());
        $this->template->assign('price_seminar',    isset($faq_list[2][3]) ? $faq_list[2][3] : array());
        $this->template->assign('env_general',      isset($faq_list[3][1]) ? $faq_list[3][1] : array());
        $this->template->assign('env_meeting',      isset($faq_list[3][2]) ? $faq_list[3][2] : array());
        $this->template->assign('env_seminar',      isset($faq_list[3][3]) ? $faq_list[3][3] : array());
        $this->template->assign('trouble_general',  isset($faq_list[4][1]) ? $faq_list[4][1] : array());
        $this->template->assign('trouble_meeting',  isset($faq_list[4][2]) ? $faq_list[4][2] : array());
        $this->template->assign('trouble_seminar',  isset($faq_list[4][3]) ? $faq_list[4][3] : array());
        $this->template->assign('mac_general',      isset($faq_list[5][1]) ? $faq_list[5][1] : array());
        $this->template->assign('mac_meeting',      isset($faq_list[5][2]) ? $faq_list[5][2] : array());
        $this->template->assign('mac_seminar',      isset($faq_list[5][3]) ? $faq_list[5][3] : array());

        $this->display('user/faq/index.t.html');
        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);
    }
}

$main =& new AppFaq();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
