<?php

require_once "classes/AppFrame.class.php";
require_once "lib/EZLib/EZXML/EZXML.class.php";
require_once "htdocs/admin_tool/dBug.php";

define("N2MY_CURRENT_API_VERSION", "v1");

class AppCustom_AEONET extends AppFrame {

    function init() {
        $this->user_id            = $this->request->get('user_id');
        $this->lang               = $this->request->get('lang');
        $this->room_key           = $this->request->get('room_key');
        $this->participant_name   = $this->request->get('user_name');
        $this->lesson_date        = $this->request->get('lesson_date');
        $this->personal_id        = $this->request->get('personal_id');
        $this->is_mobile          = $this->request->get('is_mobile');
    }

    function default_view() {
        $session = $this->login();
        $url = $this->meeting_start($session);
        header('Location: '.$url);
    }

    /**
     * ログイン ( 入力チェック )
     */
    function login() {
        // error
        $auth = array(
            'vuzer' => 'vCube8sD',
            'aeon'  => '3b7uygWSc7',
            'hao'   => '9oGuTZdcC4',
            );
        $p_len = strlen($this->personal_id);
        if (array_key_exists($this->user_id, $auth) === false) {
            $this->logger2->info();
            $this->error("01");
            return false;
        }elseif(!$this->room_key){
            $this->logger2->info();
            $this->error("01");
            return false;
        }elseif(!$this->participant_name){
            $this->logger2->info();
            $this->error("11");
            return false;
        }elseif(!$this->lesson_date){
            $this->logger2->info();
            $this->error("21");
            return false;
        }elseif($this->lesson_date != date("Ymd")){
            $this->logger2->info();
            $this->error("22");
            return false;
        }elseif(!$this->personal_id){
            $this->logger2->info();
            $this->error("31");
            return false;
        }elseif($p_len != 4 && $p_len != 5 && $p_len != 8 && $p_len != 9 && $p_len != 10 ){
            $this->logger2->info();
            $this->error("32");
            return false;
        }elseif($this->lang != "ja" && $this->lang != "en" && $this->lang != "zh"){
            $this->logger2->info();
            $this->error("41");
            return false;
        }
        $user_pw = $auth[$this->user_id];
        $param = array(
            "id"            => $this->user_id,
            "pw"            => $user_pw,
            "lang"          => $this->lang,
            "country"       => "jp",
            "timezone"      => "9",
            "enc"           => "",
            );
        $result = $this->method_exec('user/', 'login', $param);
        if (!$session = $result['data']['session']) {
            $this->logger2->info();
            $this->error("01");
            return false;
        }
        $param = array(
            'n2my_session'  => $session,
            );
        $result = $this->method_exec('user/', 'get_room_list', $param);
        $rooms = $result['data']['rooms']['room'];
        $room_exists = false;
        foreach($rooms as $room) {
            if ($room['room_info']['room_id'] == $this->room_key) {
                $room_exists = true;
                break;
            }
        }
        if (!$room_exists) {
            $this->logger2->info();
            $this->error("01");
            return false;
        }
        return $session;
    }

    /**
     *
     */
    function meeting_start($session) {
        $param = array(
            'n2my_session'  => $session,
            'room_id'       => $this->room_key,
            'type'          => 'normal',
            'name'          => $this->participant_name,
            'is_narrow'     => "",
            'flash_version' => 'as3',
            'screen_mode'   => '',
            );
        if($this->is_mobile){
            $result = $this->method_exec('user/meeting/', 'start_mobile', $param);
        }else{
            $result = $this->method_exec('user/meeting/', 'start', $param);
        }
        $url = $result['data']['url'];
        return $url;
    }

    function method_exec($path, $method, $params) {
        $params['action_'.$method] = "";
        $params['output_type'] = "php";

        $request_action = null;
        $url = N2MY_BASE_URL.'api/'.N2MY_CURRENT_API_VERSION.'/'.$path;
        $ch = curl_init();
//CURLOPT_SSL_VERIFYPEER => falesは試験環境用、必要に応じてコメント解除
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query($params, '', '&'),
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 20,
//            CURLOPT_SSL_VERIFYPEER => fales,
            );
        curl_setopt_array($ch, $option);
        $contents = curl_exec($ch);
        $result = null;
        if ($contents) {
            $result = unserialize($contents);
        }
        return $result;
    }

    function error($error_code) {
        $this->logger2->warn(array(
            'error_code'  => $error_code,
            'personal_id' => $this->personal_id,
            'request'     => $this->request->getAll()
        ));
        header("Location: https://www.aeonnetcampus.com/internet/student/ProgramError.asp?personal_id=".$this->personal_id."&error_code=".$error_code);
        exit;
    }
}

$main = new AppCustom_AEONET();
$main->execute();
?>
