<?php// 環境設定require_once("classes/AppFrame.class.php");// ユーザ情報require_once ('classes/mgm/MGM_Auth.class.php');class AppSummary extends AppFrame {    function init()    {    }    /**     * default view routine     *     */    function default_view()    {        $this->logger->info(__FUNCTION__." # Summary Info Operation Start ... ", __FILE__, __LINE__);        // 初期化        $user_logs = array();        // パラメタ取得        $target_day = $this->request->get("target_day");        $this->logger2->info(" # Summary start memory ". memory_get_usage());
        // パラメタ確認        if ($target_day){            static $user_columns;            // サーバリスト取得            $objMeetingDateLog = new N2MY_DB( N2MY_MDB_DSN, "meeting_date_log");            $server_list = parse_ini_file( N2MY_APP_DIR."config/server_list.ini", true);            require("classes/dbi/user.dbi.php");            // 全ユーザー情報取得            $users = array();            foreach($server_list["SERVER_LIST"] as $key => $dsn) {                $objUser = new UserTable( $dsn , "user");                if (!$user_columns) {                    $user_columns = $objUser->getTableInfo("data");                }                $rows = $objUser->getRowsAssoc("", array(), null, 0, $columns = "user_id,account_model,user_status,user_delete_status,invoice_flg", null);                //$rows = $objUser->getRowsAssoc();                foreach ($rows as $user_info) {                    $users[$user_info["user_id"]] = $user_info;                }            }            $this->logger2->info(" # Get user info memory ". memory_get_usage());            // 日別のユーザー毎の集計情報取得（会議室単位）            $sql = "SELECT user_id" .                ",room_key " .                ",use_time " .                ",use_time_mobile_normal " .                ",use_time_mobile_special " .                ",use_time_hispec " .                ",use_time_audience " .                ",use_time_h323 " .                ",use_time_h323ins " .                ",meeting_cnt " .                ",participant_cnt " .                " FROM meeting_date_log".                " WHERE log_date like '" . addslashes($target_day) . "' ";            $rs = $objMeetingDateLog->_conn->query($sql);            if (DB::isError($rs)) {                $this->logger2->error($rs->getUserInfo());            }            while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {                $row["user_info"]["user_id"] = $users[$row["user_id"]]["user_id"];                $row["user_info"]["account_model"] = $users[$row["user_id"]]["account_model"];                $row["user_info"]["room_key"] = $row["room_key"];                $row["user_info"]["user_status"] = $users[$row["user_id"]]["user_status"];                $row["user_info"]["user_delete_status"] = $users[$row["user_id"]]["user_delete_status"];                $row["user_info"]["invoice_flg"] = $users[$row["user_id"]]["invoice_flg"];                $user_logs[] = $row;            }        } else {            $user_logs = array();        }        $this->logger2->info(" # Get user logs memory ". memory_get_usage());        print serialize($user_logs);        $this->logger->info(__FUNCTION__." # Summary Info Operation End ... ", __FILE__, __LINE__);        //終了後に実行完了メール送信
        $mail_subject = "【ミーティング】summary.php処理完了".$target_day;
        $mail_body = "【ミーティング】summary.php処理が完了しました。" ."\n\n" .
                "■ Date: ".$target_day."\n".                "■ Memory usage: ".memory_get_usage()."\n".
                "■ URL: ".N2MY_BASE_URL.$_SERVER["REQUEST_URI"]."\n";
        if (defined("N2MY_CRON_FROM") && defined("N2MY_CRON_TO") && N2MY_CRON_FROM && N2MY_CRON_TO) {
            $this->sendReport(N2MY_CRON_FROM, N2MY_CRON_TO, $mail_subject, $mail_body);
        }    }    function sendReport($mail_from, $mail_to, $mail_subject, $mail_body) {
        require_once("lib/EZLib/EZMail/EZSmtp.class.php");
        // 入力チェック
        if (!$mail_from || !$mail_to || !$mail_subject || !$mail_body) {
            return false;
        }
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang = EZLanguage::getLangCd("ja");
        $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
        $obj_SendMail->setFrom($mail_from);
        $obj_SendMail->setReturnPath($mail_from);
        $obj_SendMail->setTo($mail_to);
        $obj_SendMail->setSubject($mail_subject);
        $obj_SendMail->setBody($mail_body);
        // メール送信
        $result = $obj_SendMail->send();
        $this->logger2->info(array($mail_from, $mail_to, $mail_subject, $mail_body));
    }}$main = new AppSummary();$main->execute();?>