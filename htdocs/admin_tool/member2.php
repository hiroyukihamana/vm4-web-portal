<?php

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/dbi/meeting_extension_use_log.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");

class AppMember extends AppFrame {

    var $_auth = null;
    var $_sess_page;
    var $account_dsn = null;

    function init() {
        $_COOKIE["lang"] = "en";
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
    }

    public function auth()
    {
    }

    public function default_view()
    {
        $this->action_top();
    }

    public function action_top()
    {
        $request = $this->request->getAll();

        $serviceStartYear = "2007";
        $thisYear = $request["year"] ? $request["year"] : date( "Y" );
        while( $serviceStartYear <= $thisYear ){
            $info["yearlist"][$serviceStartYear] = $serviceStartYear;
            $info["year"] = $serviceStartYear;
            $serviceStartYear += 1;
        }
        for( $i = 1; $i <= 12; $i++ ){
            $info["monthlist"][sprintf( "%02d", $i )] = sprintf( "%02d", $i );
        }
        $info["month"] = $request["month"] ? sprintf( "%02d", $request["month"] ) : date( "m" );

        $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);

        $info["userList"] = array();

        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            $objUser = new UserTable( $dsn );
            $objMember = new MemberTable( $dsn );
            $objMeeting = new MeetingTable( $dsn );
            $objParticipant = new DBI_Participant( $dsn );
            $objMeetingExtensionUseLog = new MeetingExtensionUseLogTable( $dsn );
            $objN2MYdb = new N2MY_DB( $dsn );
            $where = "account_model = 'member' AND " .
                     "user_status=1 AND " .
                     "( user_delete_status = 0 OR " .
                     "	( user_delete_status = 2 AND DATE_FORMAT( user_deletetime, '%Y-%m' ) >= '" . $info["year"]. "-" . $info["month"] . "' )" .
                     ")";
            $sort = array( "user_key" => "ASC");
            $userList = $objUser->getRowsAssoc( $where, $sort );
            for( $i = 0; $i < count( $userList ); $i++ ) {
                $where = sprintf( "user_key='%s'", $userList[$i]["user_key"] );
                $sort = array( "member_key" => "ASC");

                //メンバーリスト取得
                $memberList = $objMember->getRowsAssoc( $where, $sort );
                foreach( $memberList as $roomListKey => $memberInfo ){

                    //自分自身の利用時間
$sql = "
SELECT
    sum( use_count ) as count
FROM
    participant p


LEFT JOIN
    meeting m
ON
    p.meeting_key = m.meeting_key







WHERE
    p.member_key='" . $memberInfo["member_key"] . "' AND
    p.is_active = 0 AND
    DATE_FORMAT( p.uptime_end, '%Y-%m' ) = '" . $info["year"]. "-" . $info["month"] . "' AND
    m.use_flg = 1 AND
    m.is_active = 0



GROUP BY
    p.member_key
;";
                    $rs_self_count = $objN2MYdb->_conn->query( $sql );
                    $ret = $rs_self_count->fetchRow(DB_FETCHMODE_ASSOC);
                    $memberInfo["count"] = $ret['count'];

                    // 携帯, h323
                    $sql = "
SELECT
    count(*) as count, type
FROM
    meeting_extension_use_log
WHERE
    room_key='" . $memberInfo["room_key"] . "' AND
    DATE_FORMAT( create_datetime, '%Y-%m' ) = '" . $info["year"]. "-" . $info["month"] . "'
GROUP BY
    type
;";
                    $rs_extension = $objN2MYdb->_conn->query( $sql );
                    while( $ret = $rs_extension->fetchRow(DB_FETCHMODE_ASSOC) ){
                        $memberInfo[$ret["type"]] = $ret["count"];
                    }

                    //招待者
                    $sql = "
SELECT
    sum( p.use_count ) as guest_count
FROM
    participant p
LEFT JOIN
    meeting m
ON
    p.meeting_key = m.meeting_key
WHERE
    p.member_key is Null AND
    m.room_key='" . $memberInfo["room_key"] . "' AND
    p.is_active = 0 AND
    DATE_FORMAT( p.uptime_end, '%Y-%m' ) = '" . $info["year"]. "-" . $info["month"] . "'
GROUP BY
    room_key;";
                    $rs_other_count = $objN2MYdb->_conn->query( $sql );
                    $ret = $rs_other_count->fetchRow(DB_FETCHMODE_ASSOC);
                    $memberInfo["guest_count"] = $ret['guest_count'];

                    $userList[$i]["memberList"][] = $memberInfo;
                    $userList[$i]["dsn"] = $dsn;
                }
                $info["userList"][] = $userList[$i];
            }
        }
        $this->template->assign( "info", $info );

        $this->display( "admin_tool/member2.t.html" );
    }

/*
    public function action_show_detail()
    {
        $request = $this->request->getAll();
        //memberデータ
        $objMember = new MemberTable( $request["dsn"] );
        $objMeeting = new MeetingTable( $request["dsn"] );
        $objParticipant = new DBI_Participant( $request["dsn"] );
        $info["memberInfo"] = $objMember->getRow( sprintf( "member_id='%s'", $request["member_id"] ) );

        $sort = array( "meeting_key" => "ASC");
        $where = "room_key='" . $info["memberInfo"]["room_key"]. "' AND
use_flg = 1 AND
is_active = 0 AND
DATE_FORMAT( actual_end_datetime, '%Y-%m' ) = '" . $request["year"]. "-" . $request["month"] ."'";

        //部屋ーごとの終了済み会議を取得
        $meetingList = $objMeeting->getRowsAssoc( $where, $sort );
        foreach( $meetingList as $meetingListKey => $meetingInfo ){

            //参加者それぞれの利用時間とメンバーであればメンバーデータも取得
$sql = "
SELECT
    m.*, p.use_count, p.uptime_start, p.uptime_end
FROM
(
SELECT
    member_key, SUM( use_count ) as use_count, MIN( uptime_start ) as uptime_start, MAX( uptime_end ) as uptime_end
FROM
    participant
WHERE
    meeting_key='" . $meetingInfo["meeting_key"] . "'
GROUP BY
    member_key
) as p
LEFT JOIN
    member m
ON
    p.member_key = m.member_key";
            $ret = $objParticipant->_conn->query( $sql );
            if (DB::isError($ret)) {
                $this->logger2->error($ret->getUserInfo());
                exit;
            }

            while( $row = $ret->fetchRow( DB_FETCHMODE_ASSOC ) ) {
                $meetingInfo["memberList"][] = $row;
            }
            $info["memberInfo"]["meetingList"][] = $meetingInfo;
        }

        $this->template->assign( "info", $info );
        $this->display( "admin_tool/member_detail.t.html" );
    }
    */
    public function action_show_detail()
    {
        $request = $this->request->getAll();
        //memberデータ
        $objMember = new MemberTable( $request["dsn"] );
        $objMeeting = new MeetingTable( $request["dsn"] );
        $objParticipant = new DBI_Participant( $request["dsn"] );
        $info["memberInfo"] = $objMember->getRow( sprintf( "member_id='%s'", mysql_real_escape_string($request["member_id"]) ) );

$sql = "


SELECT
m.*, p.*
FROM



(
SELECT
    meeting_key, SUM( use_count ) as use_count
FROM
    participant
WHERE
    member_key= ". $info["memberInfo"]["member_key"] . "
GROUP BY
    meeting_key
) p
LEFT JOIN
    meeting m
ON
    p.meeting_key = m.meeting_key
WHERE


m.use_flg = 1 AND
m.is_active = 0 AND
DATE_FORMAT( m.actual_end_datetime, '%Y-%m' ) = '" . $request["year"]. "-" . $request["month"] ."'






";
            $ret = $objParticipant->_conn->query( $sql );
            if (DB::isError($ret)) {
                $this->logger2->error($ret->getUserInfo());
                exit;
            }

            while( $meetingInfo = $ret->fetchRow( DB_FETCHMODE_ASSOC ) ) {
                $where = sprintf( "meeting_key='%s'", $meetingInfo["meeting_key"] );
                $meetingInfo["participantList"] = $objParticipant->getRowsAssoc( $where, null, null, null, "DISTINCT( participant_name ) as participant_name" );
                $info["meetingList"][] = $meetingInfo;
                print_r($meetingInfo);echo"<br><br>";
            }
        $this->template->assign( "info", $info );
        $this->display( "admin_tool/member_detail2.t.html" );
    }
}


$main =& new AppMember();
$main->execute();