<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("Auth.php");
require_once("Pager.php");

class N2MYAppUpdate extends AppFrame {

    function init() {
        $this->_name_space = md5(__FILE__);
        $this->_sess_page = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
    }

    function default_view() {
        $this->render_top();
    }

    function render_top() {
        $version_list = array();
        $_account_file = glob(N2MY_APP_DIR."setup/patch/account/*");
        if ($_account_file) {
            foreach($_account_file as $file) {
                $version_list[] = basename($file, ".sql");
            }
        }
        $_data_file = glob(N2MY_APP_DIR."setup/patch/data/*");
        if ($_data_file) {
            foreach($_data_file as $file) {
                $version_list[] = basename($file, ".sql");
            }
        }
        $_data_file = glob(N2MY_APP_DIR."setup/patch/batch/*");
        if ($_data_file) {
            foreach($_data_file as $file) {
                $batch_list[] = basename($file, ".php");
            }
        }
        natsort($version_list);
        $version_list = array_unique($version_list);
        $this->template->assign("version_list", $version_list);
        $this->template->assign("batch_list", $batch_list);
        $this->display("admin_tool/update/index.t.html");
    }

    function action_db_confirm() {
        $this->set_submit_key();
        require_once("lib/EZLib/EZDB/EZDB_Util.class.php");
        $version = $this->request->get("version");
        // アカウントDBのアップデート
        $sql_file = N2MY_APP_DIR."setup/patch/account/".$version.".sql";
        if (file_exists($sql_file)) {
            $account_db_querys = file_get_contents($sql_file);
        }

        // データDBのアップデート(複数のDBに対して実行)
        $sql_file = N2MY_APP_DIR."setup/patch/data/".$version.".sql";
        if (file_exists($sql_file)) {
            $user_db_querys = file_get_contents($sql_file);
        }
        $this->template->assign("version", $version);
        $this->template->assign("account_db_querys", $account_db_querys);
        $this->template->assign("user_db_querys", $user_db_querys);
        $this->display("admin_tool/update/db_confirm.t.html");
    }

    /**
     *
     */
    function action_db_update() {
        if (!$this->check_submit_key()) {
            return $this->render_top();
        }
        require_once("lib/EZLib/EZDB/EZDB_Util.class.php");
        $version = $this->request->get("version");
        // アカウントDBのアップデート
        $sql_file = N2MY_APP_DIR."setup/patch/account/".$version.".sql";
        $error_msg_list = array();
        $ok_msg_list = array();
        if (file_exists($sql_file)) {
            $account_db = new N2MY_DB(N2MY_MDB_DSN);
            $querys = EZDB_Util::parse_query(file_get_contents($sql_file));
            $this->logger2->info($querys);
            foreach($querys as $query) {
                $ret = $account_db->_conn->query($query);
                if (DB::isError($ret)) {
                    $error_msg_list[] = $ret->getUserInfo();
                    $this->logger->info($ret->getUserInfo());
                } else {
                    $ok_msg_list[] = $query;
                }
            }
        }

        // データDBのアップデート(複数のDBに対して実行)
        $sql_file = N2MY_APP_DIR."setup/patch/data/".$version.".sql";
        if (file_exists($sql_file)) {
            $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
                $data_db = new N2MY_DB($dsn);
                $querys = EZDB_Util::parse_query(file_get_contents($sql_file));
                $this->logger2->info($querys);
                foreach($querys as $query) {
                    $ret = $data_db->_conn->query($query);
                    if (DB::isError($ret)) {
                        $error_msg_list[] = $ret->getUserInfo();
                        $this->logger->info($ret->getUserInfo());
                    } else {
                        $ok_msg_list[] = $query;
                    }
                }
            }
        }
        $this->template->assign("error_msg_list", $error_msg_list);
        $this->template->assign("ok_msg_list", $ok_msg_list);
        return $this->render_top();
    }

    function action_batch_confirm() {
        $this->set_submit_key();
        $version = $this->request->get("version");
        $batch_file = N2MY_APP_DIR."setup/patch/batch/".$version.".php";
        if (file_exists($batch_file)) {
            require_once $batch_file;
            $class_name = "N2MY_BATCH_".str_replace(".", "_", $version);
            if (class_exists($class_name)) {
                $batch_class = new $class_name($this);
                $batch_info = $batch_class->getInfo();
                $this->template->assign("version", $version);
                $this->template->assign("batch_info", $batch_info);
                $this->display("admin_tool/update/batch_confirm.t.html");
            } else {
                return $this->render_top();
            }
        } else {
            return $this->render_top();
        }
    }

    function action_batch() {
        if (!$this->check_submit_key()) {
            return $this->render_top();
        }
        $version = $this->request->get("version");
        $batch_file = N2MY_APP_DIR."setup/patch/batch/".$version.".php";
        if (file_exists($batch_file)) {
            require_once $batch_file;
            $class_name = "N2MY_BATCH_".str_replace(".", "_", $version);
            if (class_exists($class_name)) {
                $batch_class = new $class_name($this);
                $batch_class->execute();
            }
        }
        return $this->render_top();
    }

    /**
     * 所要時間
     */
    function action_meeting_4_3_1() {
        // 4.3.1の差分追加
        // タイムアウト無し
        set_time_limit(0);
        require_once('classes/core/dbi/Meeting.dbi.php');
        require_once('classes/core/dbi/Participant.dbi.php');
        $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            // DBサーバ一覧
            $obj_Meeting            = new DBI_Meeting( $dsn );
            $obj_Participant        = new DBI_Participant( $dsn );
            // 利用された会議（削除されたデータも対象）
            $where = "is_active = 0".
                " AND use_flg = 1";
            $meeting_keys = $obj_Meeting->getCol($where, "meeting_key");
            $this->logger2->info($meeting_keys);
            foreach($meeting_keys as $meeting_key) {
                // 参加者情報を索引として登録
                $where = "meeting_key = ".$meeting_key.
                    " AND use_count > 0".
                    " AND is_active = 0";
                $participant_list = $obj_Participant->getRowsAssoc($where, null, null, null, "member_key,participant_name");
                $member_keys = array();
                $participant_names = array();
                foreach($participant_list as $_key => $participant) {
                    if ($participant["member_key"]) {
                        $member_keys[] = $participant["member_key"];
                    }
                    $participant_names[] = $participant["participant_name"];
                }
                $member_keys = array_unique($member_keys);
                $participant_names = array_unique($participant_names);
                // 改行コードできって検索しやすくする
                $participant_info["member_keys"] = join($member_keys,"\n");
                $participant_info["participant_names"] = join($participant_names,"\n");
                $this->logger2->info(array($meeting_key, $participant_info));
                $where = "meeting_key = ".$meeting_key;
                $obj_Meeting->update($participant_info, $where);
            }
        }
        $this->render_top();
    }
}
$main = new N2MYAppUpdate();
$main->execute();
?>
