<?php

require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
require_once 'classes/AppFrame.class.php';
require_once 'classes/N2MY_DBI.class.php';

class MasterIDCSV extends AppFrame {
    private $name_space;
    private $rule = array();

    public function init() {
        $this->name_space = md5( __FILE__ );

        // 入力CSVルール.定義されていないカラムはパース時に無視する.
        $this->rule = array (
            'user_id' => array (
                'required' => true
                ),
            'one_master_id' => array (
                'required' => true
                ),
            'one_master_id_password' => array (
                'required' => true,
                'maxlen' => 64
                ),
        );
    }

    public function default_view($err_msg = null) {
        $this->session->removeAll( $this->name_space );
        $this->display( $err_msg );
    }

    public function display($err_msg = null) {
        if ( $err_msg ) {
            $this->template->assign( 'error_msg', $err_msg );
        }

        // admin_tool 権限チェック
        $authority = $_SESSION['_authsession']['data']['authority'];
        $this->template->assign('authority', $authority);

        parent::display( 'admin_tool/user/masterID_csv.t.html' );
        exit();
    }

    private function getCSV($name){
        if ( $_FILES[$name]['error'] ) {
            $this->display( 'CSVファイルを選択してください' );
        }

        $file = $_FILES[$name];
        $pathinfo = pathinfo( $file['name'] );
        if ( $pathinfo['extension'] != 'csv' ) {
            $this->display( 'CSVファイルを選択してください' );
        }

        // 入力CSVパース
        $filename = parent::get_work_dir() . '/' . tmpfile();
        move_uploaded_file( $file['tmp_name'], $filename );
        require_once 'lib/EZLib/EZUtil/EZCsv.class.php';
        $csv = new EZCsv( true, 'UTF-8', 'UTF-8' );
        $csv->open( $filename, 'r' );
        $rows = array ();
        while ( $row = $csv->getNext() ) {
            foreach ( $row as $key => &$val ) {
                // 全入力データtrim
                $val = trim( $val );
                // ルール定義されたカラム以外は無視
                // 空文字列も無視
                if ( ! array_key_exists( $key, $this->rule ) || $val === '' ){
                    unset( $row[$key] );
                }
            }
            $rows[] = $row;
        }
        $csv->close();
        return $rows;
    }

    public function action_modify_go() {
        // input csv
        $rows = $this->getCSV('import_file');
        if ( count( $rows ) == 0 ) {
            $this->display( 'CSVデータがありません' );
            return $rows;
        }

        $file = $_FILES['import_file'];
        $pathinfo = pathinfo( $file['name'] );
        $log_fname = N2MY_APP_DIR.'logs/'.$pathinfo['filename'].'_result.log';
        $log_fp = fopen($log_fname,'w');
        fwrite($log_fp,'<<< user_id : result >>>'.$sts.PHP_EOL);

        // main proc
        foreach ( $rows as &$row ) {
            $result = false;
            $result = $this->modifyUser( $row );
            $row['status'] = $result;
            $sts = 'NG';
            if($row['status']){$sts = 'OK';}
            fwrite($log_fp,$row['user_id'].':'.$sts.PHP_EOL); //log
        }
        unset( $row );
        fclose($log_fp);

        // 操作ログ
        $objOperationLog = new N2MY_DB( parent::get_auth_dsn(), 'operation_log' );
        $objOperationLog->add( array (
                'staff_key' => $_SESSION['_authsession']['data']['staff_key'],
                'action_name' => 'ONE Master ID import',
                'operation_datetime' => date( 'Y-m-d H:i:s' ),
                'table_name' => 'user',
                'keyword' => $_SESSION["_authsession"]["username"],
                'info' => serialize( $rows ),
                'create_datetime' => date( 'Y-m-d H:i:s' )
        ) );

        $this->session->set('result_info', $rows, $this->name_space);
        $this->display('import done.');
    }

    private function modifyUser($row) {
        $data = array();
        $data['user_id']                = $row['user_id'];
        $data['one_master_id']          = $row['one_master_id'];
        $data['one_master_id_password'] = $row['one_master_id_password'];
        $objUser = new N2MY_DB(parent::get_dsn(), "user");
        $where = "user_id = '".addslashes($row['user_id'])."'";
        $user = $objUser->getRow( $where );
        if ( ! $user['user_key'] ) {
            return false;
        }
        if ( (strcmp($user['account_plan'] , 'one'         ) == 0 ) ||
             (strcmp($user['account_plan'] , 'one_id_host' ) == 0 ) ) { // not MTG user
            return false;
        }

        // 難読化対応
        if ( ! is_null( $data['one_master_id_password'] ) ) {
            $data['one_master_id_password'] = EZEncrypt::encrypt( VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $data['one_master_id_password'] );
        }

        $where = sprintf( 'user_key=%s', $user['user_key'] );
        $db = $objUser->update( $data, $where );
        if ( DB::isError( $db ) ) {
            $this->logger2->info('user table update error!');
            return false;
        }
        return true; 
    }

    private function checkDB($db){
        if ( ! is_array( $db ) ) $db = array ( $db );

        foreach ( $db as $node ) {
            if ( DB::isError( $node ) || $node === false ){
                $this->logger2->error( $node->getUserInfo() );
                return false;
            }
        }
        return true;
    }
}

$main = & new MasterIDCSV();
$main->execute();
