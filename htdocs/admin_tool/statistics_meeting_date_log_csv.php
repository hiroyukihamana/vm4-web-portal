<?php

// 環境設定
require_once ('classes/AppFrame.class.php');
// 部屋情報
require_once ('classes/N2MY_DBI.class.php');

/* ***********************************************
 * 会議の情報を請求管理DBへ反映するための処理
 *
 * ***********************************************/
class AppStatisticsMeetingDateLogCSV extends AppFrame {

	var $_dsn = null;
	var $account_dsn = null;
	var $meeting_date_log_obj = null;

	function init() {
		// DBサーバ情報取得
		if (file_exists(sprintf("%sconfig/slave_list.ini", N2MY_APP_DIR))) {
			$server_list = parse_ini_file(sprintf("%sconfig/slave_list.ini", N2MY_APP_DIR), true);
			$this -> _dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
			$this -> account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
		} else {
			$server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR), true);
			$this -> _dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
			$this -> account_dsn = $this -> config -> get("GLOBAL", "auth_dsn");
		}
		$this -> meeting_date_log_obj = new N2MY_DB($this -> account_dsn, "meeting_date_log");

	}

	function default_view() {

		$this -> logger -> info(__FUNCTION__ . ' # GetMeetingDateLog Info Operation Start ... ', __FILE__, __LINE__);

		// 初期化
		$mtg_meeting_date_logs = array();

		// パラメータ取得
		$target_day = $this -> request -> get('target_day');
		if (!$target_day) {
			$target_day = date("Y-m-d", (time() - 3600 * 24));
		}
		$this -> logger2 -> info("target day is " . $target_day);
		//$target_day = date('Y-m-d', time() - (3600*24)) . ' 00:00:00'; // 常に前日
		// パラメータ検証 ( boolearn : true / false )
		$param_is_safe = $this -> _check_param($target_day);

		// パラメータ判定
		if ($param_is_safe) {
			$this -> mtg_meeting_date_logs = array();
			$_tmp_data = null;
			$_tmp_data = $this -> _get_meeting_date_log_routine($target_day);
			if (is_array($_tmp_data) === true) {
				$mtg_meeting_date_logs = array_merge($mtg_meeting_date_logs, $_tmp_data);
			}
		} else {
			$mtg_meeting_date_logs = array();
		}
		$this -> logger -> info(__FUNCTION__ . ' # MeetingDateLog Count ... ', __FILE__, __LINE__, count($mtg_meeting_date_logs));

		$log_folder = N2MY_DOCUMENT_ROOT . "admin_tool/statistics_data/meeting_date_log/";

		//TO CSV
		require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
		$csv = new EZCsv(true, 'SJIS', 'UTF-8');
		$csv_file = $log_folder . "meeting_date_log_" . date("Ymd", $this -> get_target_date($target_day)) . ".csv";
		$handle = fopen($csv_file, "w");
		$csv -> open($csv_file, "w");
		$header = $this -> get_csv_header();
		$csv -> setHeader($header);
		$csv -> write($header);
		if ($mtg_meeting_date_logs) {
			foreach ($mtg_meeting_date_logs as $meeting_date_log) {
				$csv -> write($meeting_date_log);
			}
		}
		$csv -> close();
		chmod($csv_file, 0777);
		fclose($handle);

		//Delete log a week ago
		$delete_log = $log_folder . "meeting_date_log_" . date("Ymd", ($this -> get_target_date($target_day) - 3600 * 24 * 7)) . ".csv";
		if (file_exists($delete_log)) {
			unlink($delete_log);
		}

		//Download CSV file
		// header('Content-Type: application/octet-stream;');
		// header('Content-Disposition: attachment; filename="meeting' . date("Ymd") . '.csv"');
		// $fp = fopen($csvfile, "r");
		// $contents = fread($fp, filesize($csvfile));
		// fclose($fp);
		// print $contents;

		$this -> logger -> info(__FUNCTION__ . ' # GetMeetingDateLog Info Operation End ... ', __FILE__, __LINE__);
	}

	/**
	 * メイン取得
	 *
	 */
	private function _get_meeting_date_log_routine($target_day) {
		$rows = array();
		$row = array();
		$where = "SELECT * FROM  meeting_date_log where log_date LIKE '" . addslashes($target_day) . "%'";
		$rs = $this -> meeting_date_log_obj -> _conn -> query($where);
		if (DB::isError($rs)) {
			$this -> logger -> info(__FUNCTION__ . " # DBError", __FILE__, __LINE__, $sql);
			return false;
		} else {
			while ($row = $rs -> fetchRow(DB_FETCHMODE_ASSOC)) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	/**
	 * パラメータ検証
	 *
	 */
	private function _check_param($_param = null) {
		$ret = false;
		//未設定チェック
		if (!$_param) {
			return $ret;
		}
		//文字数チェック
		if (strlen($_param) < 7) {
			return $ret;
		}
		//指定不可文字存在チェック
		if (preg_match("/[_%*?]/", $_param)) {
			return $ret;
		}
		//チェック OK!
		$ret = true;

		return $ret;
	}

	/**
	 * describe table in order to get table columns
	 */
	function get_csv_header() {
		$sql = "desc meeting_date_log";
		$rs = $this -> meeting_date_log_obj -> _conn -> query($sql);
		$rows = array();
		$row = array();
		if (DB::isError($rs)) {
			$this -> logger -> info(__FUNCTION__ . " # DBError", __FILE__, __LINE__, $sql);
			return false;
		} else {
			while ($row = $rs -> fetchRow(DB_FETCHMODE_ASSOC)) {
				$rows[$row['Field']] = $row['Field'];
			}
		}
		return $rows;
	}

	/**
	 * 【テーブル名】_【YYYYmmdd】.csv
	 */
	function get_target_date($date) {
		$year = substr($date, 0, 4);
		$month = substr($date, 5, 2);
		$day = substr($date, 8, 2);
		$timestamp = mktime(0, 0, 0, $month, $day, $year);
		return $timestamp;
	}

}

$main = new AppStatisticsMeetingDateLogCSV();
$main -> execute();
?>