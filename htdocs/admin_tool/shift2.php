<?php
require_once("classes/AppFrame.class.php");
require_once("classes/mgm/MGM_Auth.class.php");

require_once("lib/pear/DB.php");
require_once("classes/mgm/dbi/server.dbi.php");

class AppShift extends AppFrame
{
    var $dsn;
    var $mgm_conn;

    var $dbList;

    public function init() {
        $this->config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        if (!defined("N2MY_MDB_DSN")) {
            define("N2MY_MDB_DSN", $this->config["GLOBAL"]["auth_dsn"]);
        }
        $this->dbList = $this->session->get( "dbList" );
        $this->logger2->info( $this->dbList );
        if( ! $this->dbList["meeting"] || ! $this->dbList["mgm"] ) return false;

        $this->mgm_conn = DB::connect( N2MY_MDB_DSN );
        if (DB::isError( $this->mgm_conn ) ) {
            return "false";
        } else {
            // キャラセット指定
            $dsn_elem = parse_url($this->_auth_dsn);
            if (isset ($dsn_elem["query"])) {
                $query_opt = array ();
                parse_str($dsn_elem["query"], $query_opt);
                if (isset ($query_opt["charset"])) {
                    $this->mgm_conn->query("SET NAMES " . $query_opt["charset"]);
                }
            }
        }
        $objServer = new MgmServerTable( N2MY_MDB_DSN );
        $where = sprintf( "server_key='%s'", $this->session->get( "now_server_key" ) );
        $this->dsn = $objServer->getOne( $where, "dsn" );

        $this->conn = DB::connect( $this->dsn );

        if (DB::isError($this->conn)) {
            return "false";
        } else {
            // キャラセット指定
            $dsn_elem = parse_url($this->_auth_dsn);
            if (isset ($dsn_elem["query"])) {
                $query_opt = array ();
                parse_str($dsn_elem["query"], $query_opt);
                if (isset ($query_opt["charset"])) {
                    $this->conn->query("SET NAMES " . $query_opt["charset"]);
                }
            }
        }
    }

    public function action_datacenter()
    {
        $ret = $this->truncate( "datacenter", "mgm" );
//        $ret = $this->truncate( "datacenter", $this->dbList["meeting"] );

        $sql = sprintf( "INSERT IGNORE INTO %s.datacenter SELECT * FROM %s.datacenter;", $this->dbList["mgm"], $this->dbList["core"] );
        print $this->query( $sql, "mgm" );
    }

    public function action_document()
    {
        $ret = $this->truncate( "document", "meeting" );
        if( "sales" == $this->dbList["service"] ){
            $sql = sprintf("
SELECT
    d.document_key,
    d.meeting_key,
    d.document_id,
    c.document_path,
    c.name as document_name,
    c.size as document_size,
    d.document_category,
    d.document_extension,
    d.document_status,
    d.document_index,
    d.document_sort,
    d.create_datetime,
    d.update_datetime
FROM
    %s.document d
LEFT JOIN
    %s.convert_file2 c
ON
    d.document_id = c.document_id
WHERE
    c.status = 'done'", $this->dbList["core"], $this->dbList["core"] );
            $ret = $this->conn->query( $sql );
            require_once( "classes/core/dbi/Document.dbi.php" );
            $objDocument = new DBI_Document( $this->dsn );
            while( $row = $ret->fetchRow(DB_FETCHMODE_ASSOC ) ) {
                $data = array(
                            "document_key"    => $row["document_key"],
                            "document_id"   => $row["document_id"],
                            "document_category" => $row["document_category"],
                            "document_extension"=> $row["document_extension"],
                            "document_index"  => $row["document_index"],
                            "document_status" => 2,
                            "document_sort"   => $row["document_sort"],
                            "create_datetime" => $row["create_datetime"],
                            "update_datetime" => $row["update_datetime"]
                            );
                if( $row["document_path"] ){
                    $data["document_path"] = preg_replace( "#^[_a-zA-Z0-9]*\/#", "", $row["document_path"] ) . "/";
                }
                if( $row["meeting_key"] ){
                    $data["meeting_key"] = $row["meeting_key"];
                    $data["document_mode"] = "meeting";
                } else
                    $data["document_mode"] = "pre";
                if( $row["document_name"] )
                    $data["document_name"] = $row["document_name"];
                if( $row["document_size"] )
                    $data["document_size"] = $row["document_size"];
                $sth = $objDocument->add( $data );
                if ( PEAR::isError( $sth ) ) {
                    $this->logger2->error( $sth->getUserInfo() );
                    print "false";
                    exit;
                }
            }
            print "true";exit;
        } else {
        $sql = sprintf("
INSERT IGNORE INTO %s.document(
document_key,
meeting_key,
document_id,
document_category,
document_extension,
document_status,
document_index,
document_sort,
create_datetime,
update_datetime) SELECT * FROM %s.document;", $this->dbList["meeting"], $this->dbList["core"] );
            print $this->query( $sql, "meeting" );exit;
        }
//        print $this->query( $sql, "meeting" );
    }

    public function action_document2()
    {
        $delete = $this->truncate( "convert_file", "mgm");
        $array = parse_url($this->dsn);
        $db_host = str_replace( "/", "", $array["path"] );
                    $sql = sprintf("
SELECT
document_key,
document_id,
document_path,
document_extension,
document_status,
create_datetime,
update_datetime
FROM
    %s.document", $this->dbList["meeting"] );
        $ret = $this->conn->query( $sql );
        while( $row = $ret->fetchRow(DB_FETCHMODE_ASSOC ) ) {
            $data = array(
                            "no"        => $row["document_key"],
                            "db_host"     => $db_host,
                            "document_path"   => $row["document_path"],
                            "document_id"   => $row["document_id"],
                            "extension"     => $row["document_extension"],
                            "status"      => $row["document_status"],
                            "priority"      => 1,
                            "create_datetime" => $row["create_datetime"]
                            );
            if( $row["update_datetime"] )
                $data["update_datetime"] = $row["update_datetime"];
            $sth = $this->mgm_conn->autoExecute( "convert_file", $data,
                                DB_AUTOQUERY_INSERT);
            if (DB::isError( $sth ) ) {
                $this->logger2->info( $sth->getUserInfo() );
                return $sth->getUserInfo();exit;
            }
        }
        print "true";
    }

    public function action_meeting()
    {
        $ret = $this->truncate( "meeting", "meeting" );
                $sql = sprintf('
SELECT
    meeting.*, meeting_use_log.meeting_use_minute
FROM
    %s.meeting
LEFT JOIN
(
    SELECT count(*) as meeting_use_minute, meeting_key FROM %s.meeting_use_log group by meeting_key
) meeting_use_log
ON
    meeting.meeting_key = meeting_use_log.meeting_key', $this->dbList["core"], $this->dbList["core"] );
        $ret = $this->conn->query( $sql );

//        require_once( "classes/dbi/meeting.dbi.php" );
//        $obj_Meeting = new MeetingTable( $this->dsn );
        while( $row = $ret->fetchRow(DB_FETCHMODE_ASSOC ) ) {
            $sql2 = sprintf( "
SELECT MIN( meeting_uptime_start ) as meeting_start, MAX( meeting_uptime_stop) as meeting_stop FROM
%s.meeting_uptime WHERE meeting_key ='%s' GROUP BY meeting_key;", $this->dbList["core"], $row["meeting_key"] );
            $ret2 = $this->mgm_conn->query( $sql2 );
            $result = $ret2->fetchRow(DB_FETCHMODE_ASSOC);
            if ($result)
                 $row["use_flg"] = 1;
            $row["actual_start_datetime"] = $result["meeting_start"];
            $row["actual_end_datetime"] = $result["meeting_stop"];

            $sth = $this->conn->autoExecute( "meeting", $row,
                                DB_AUTOQUERY_INSERT);
            if ( PEAR::isError( $sth ) ) {
                $this->logger2->error( $sth->getUserInfo() );
                print "false";
                exit;
            }
        }
        print "true";
    }

    public function action_meeting1()
    {
        $ret = $this->truncate( "meeting", "meeting" );

        switch( $this->dbList["service"] ){
            case "jp":
            case "us":
        $core_table = $this->_get_columns("meeting", $this->dbList["core"] );
        unset( $core_table["provider_key"] );
        $this->logger2->info($core_table);
        $data_table = $this->_set_prefix($core_table, "m");
        $core_table[] = "user_key";
        $data_table[] = "r.user_key";

        $sql = 'INSERT INTO %s.meeting (';
        $sql .= implode(",", $core_table).' )';
        $sql .= 'SELECT ';
        $sql .= implode(",", $data_table);
        $sql .= ' FROM
    %s.meeting m
LEFT JOIN
    %s.room r
ON
    m.room_key = r.room_key';
        $sql = sprintf($sql, $this->dbList["meeting"], $this->dbList["core"], $this->dbList["web"] );
                break;

            case "sales":
                break;

            default:
                return false;
        }
        print $this->query( $sql, "meeting" );
    }

    public function action_meeting2()
    {
        require_once( "classes/dbi/meeting.dbi.php" );

        $sql = sprintf("
SELECT
meeting_key, use_flg, meeting_use_minute, meeting_size_used, has_recorded_minutes, has_recorded_video
FROM
%s.meeting
ORDER BY
meeting_key ASC;", $this->dbList["meeting"] );
        $ret = $this->conn->query( $sql );
        if (DB::isError( $ret ) ) {
            $this->logger2->error( $ret->getUserInfo() );
            return "false";
        }
        while( $row = $ret->fetchRow(DB_FETCHMODE_ASSOC ) ) {
            $meetingList[] = $row;
        }
        $date = date( "Y/m/d h:i:s" );
        for( $i=0; $i < count( $meetingList ); $i++ ){
            $data = array();
            // meeting_uptimeよりmeeting_start, meeting_stopの追加
            $sql = sprintf( "
SELECT MIN( meeting_uptime_start ) as meeting_start, MAX( meeting_uptime_stop ) as meeting_stop FROM
%s.meeting_uptime WHERE meeting_key ='%s' GROUP BY meeting_key;", $this->dbList["core"], $meetingList[$i]["meeting_key"] );
            $ret = $this->conn->query( $sql );
            $result = $ret->fetchRow(DB_FETCHMODE_ASSOC);
            $where = sprintf( "meeting_key=%s", $meetingList[$i]["meeting_key"] );
            $data["actual_start_datetime"] = $result["meeting_start"];
            $data["actual_end_datetime"] = $result["meeting_stop"];

            //meeting_size_usedの上書き
            if( 0 < $meetingList[$i]["meeting_size_used"] &&
                0 == $meetingList[$i]["has_recorded_minutes"] &&
                0 == $meetingList[$i]["has_recorded_video"] ){
                $data["meeting_size_used"] = 0;
                $sql4 = sprintf( "INSERT INTO %s.shift VALUES( '%s', '%s.meeting_size_used', '%s' )", $this->dbList["meeting"], $meetingList[$i]["meeting_key"], $this->dbList["meeting"], $date );
                $this->conn->query( $sql4 );
            }
            //meeting_use_minuteの上書き
            if( ! $meetingList[$i]["meeting_use_minute"] ){
                $sql2 = sprintf("
SELECT
    COUNT( meeting_key ) as num
FROM
    %s.meeting_use_log
WHERE
    meeting_key = '%s'
GROUP BY
    meeting_key;
", $this->dbList["core"], $meetingList[$i]["meeting_key"] );
                $ret2 = $this->conn->query( $sql2 );
                $count = $ret2->fetchRow( DB_FETCHMODE_ASSOC );
                if( $count["num"] > 0 ){
                    $data["meeting_use_minute"] = $count["num"];
                    $data["use_flg"] = 1;
                    $sql3 = sprintf( "INSERT INTO %s.shift VALUES( '%s', '%s.use_flg', '%s' )", $this->dbList["meeting"], $meetingList[$i]["meeting_key"], $this->dbList["meeting"], $date );
                    $this->conn->query( $sql3 );
                } else {
                    $data["meeting_use_minute"] = 0;
                }
            }
            $result = $this->conn->autoExecute( $this->dbList["meeting"] . ".meeting", $data,
                                DB_AUTOQUERY_UPDATE, $where);
        }
        print "true";
        /*
        $sql = sprintf( "






















" );
        print $this->query( $sql, "meeting" );
        */
    }

    public function action_meeting_extension_use_log()
    {
        $ret = $this->truncate( "meeting_extension_use_log", "meeting" );
        $sql = sprintf( "
INSERT IGNORE INTO %s.meeting_extension_use_log SELECT * FROM %s.meeting_extension_use_log;", $this->dbList["meeting"], $this->dbList["core"] );
        print $this->query( $sql, "meeting" );
    }

    public function action_meeting_options()
    {
        $ret = $this->truncate( "meeting_options", "meeting" );
        $sql = sprintf( "INSERT IGNORE INTO %s.meeting_options SELECT * FROM %s.meeting_options;", $this->dbList["meeting"], $this->dbList["core"] );
        print $this->query( $sql, "meeting" );
    }

    public function action_meeting_sequence()
    {
        $ret = $this->truncate( "meeting_sequence", "meeting" );
        $sql = sprintf( "INSERT IGNORE INTO %s.meeting_sequence (
  meeting_sequence_key,
  server_key,
  meeting_key,
         meeting_size_used,
        has_recorded_minutes,
        has_recorded_video,
  create_datetime,
  update_datetime
  )SELECT
  ms.meeting_sequence_key,
  ms.server_key,
  ms.meeting_key,
         mm.meeting_size_used,
        mm.has_recorded_minutes,
        mm.has_recorded_video,
  ms.create_datetime,
  ms.update_datetime
        FROM %s.meeting_sequence ms
        LEFT JOIN %s.meeting mm
        on ms.meeting_key = mm.meeting_key
        ;", $this->dbList["meeting"], $this->dbList["core"], $this->dbList["core"] );
        print $this->query( $sql, "meeting" );
    }

    public function action_meeting_uptime()
    {
        $ret = $this->truncate( "meeting_uptime", "meeting" );
        $sql = sprintf( "INSERT IGNORE INTO %s.meeting_uptime SELECT * FROM %s.meeting_uptime;", $this->dbList["meeting"], $this->dbList["core"] );
        print $this->query( $sql, "meeting" );
    }

    public function action_meeting_use_log()
    {
        $ret = $this->truncate( "meeting_use_log", "meeting" );
//        $sql = sprintf( "INSERT IGNORE INTO %s.meeting_use_log ( log_no, meeting_key, room_key, meeting_tag, create_datetime ) SELECT * FROM %s.meeting_use_log;", $this->dbList["meeting"], $this->dbList["core"] );
$sql = sprintf( "
INSERT INTO %s.meeting_use_log
(
    meeting_key,
    room_key,
    meeting_tag,
    create_datetime
)
SELECT
    meeting_key,
    room_key,
    meeting_tag,
    create_datetime
FROM %s.meeting_use_log;

", $this->dbList["meeting"], $this->dbList["core"] );
        if ( ! $this->query( $sql, "meeting" ) ) {
            print false;exit;
        }
        $sql = sprintf( "INSERT INTO %s.meeting_use_log
(
    meeting_key,
    room_key,
    meeting_tag,
    type,
    participant_id,
    create_datetime
)
SELECT
    meeting_key,
    room_key,
    meeting_tag,
    type,
    participant_id,
    create_datetime
FROM
    %s.meeting_extension_use_log;", $this->dbList["meeting"], $this->dbList["core"] );
        print $this->query( $sql, "meeting" );
    }

    public function action_participant()
    {
        $ret = $this->truncate( "participant", "meeting" );

        //旧環境にindex付与
        $sql = sprintf( "ALTER TABLE %s.participant_uptime ADD INDEX (participant_key);", $this->dbList["core"] );
        print $this->query( $sql, "mgm" );

        switch( $this->dbList["service"] )
        {
            case "jp":
            case "us":
                $core_table = $this->_get_columns("participant", $this->dbList["core"] );
                $data_table = $this->_set_prefix($core_table, "p");
                $core_table[] = "uptime_start";
                $core_table[] = "uptime_end";
                $data_table[] = "p.uptime_start";
                $data_table[] = "p.uptime_end";

                $sql = 'INSERT INTO %s.participant (';
                $sql .= implode(",", $core_table).' )';
                $sql .= 'SELECT ';
                $sql .= implode(",", $data_table);
                $sql .= ' FROM
            %s.participant p
            LEFT JOIN %s.participant_uptime pu
            on p.participant_key = pu.participant_key';
                $sql = sprintf($sql, $this->dbList["meeting"], $this->dbList["core"], $this->dbList["core"] );
                break;

            case "sales":
                break;

            default:
                return false;
        }
        print $this->query( $sql, "meeting" );
    }

    public function action_server()
    {
        $ret = $this->truncate( "fms_server", "mgm" );
        $sql = sprintf( "INSERT IGNORE INTO %s.fms_server SELECT * FROM %s.server;", $this->dbList["mgm"], $this->dbList["core"] );
        print $this->query( $sql, "mgm" );
    }

    public function action_sharing_server()
    {
        $ret = $this->truncate( "sharing_server", "mgm" );
        $sql = sprintf( "INSERT IGNORE INTO %s.sharing_server SELECT * FROM %s.sharing_server;", $this->dbList["mgm"], $this->dbList["core"] );
        print $this->query( $sql, "mgm" );
    }

    public function action_mail_doc_conv()
    {
        $ret = $this->truncate( "mail_doc_conv", "meeting" );
        $sql = sprintf( "INSERT IGNORE INTO %s.mail_doc_conv SELECT * FROM %s.mail_doc_conv;", $this->dbList["meeting"], $this->dbList["web"] );
        print $this->query( $sql, "meeting" );
    }

    public function action_meeting_date_log()
    {
        $ret = $this->truncate( "meeting_date_log", "mgm" );

        switch( $this->dbList["service"] )
        {
            case "jp":
            case "us":
                $core_table = $this->_get_columns("meeting_date_log", $this->dbList["web"] );

                $sql = 'INSERT INTO %s.meeting_date_log (';
                $sql .= implode(",", $core_table).' )';
                $sql .= 'SELECT ';
                $sql .= implode(",", $core_table);
                $sql .= ' FROM %s.meeting_date_log;';
                $sql = sprintf($sql, $this->dbList["mgm"], $this->dbList["web"] );
                break;

            case "sales":
                break;

            default:
                return false;
        }
        print $this->query( $sql, "mgm" );
    }

    public function action_member()
    {
        $ret = $this->truncate( "member", "meeting" );
        $sql = sprintf( "
INSERT IGNORE INTO %s.member( member_key, user_key, member_id, member_pass, member_email, member_name, member_status, member_group )
SELECT * FROM %s.member;", $this->dbList["meeting"], $this->dbList["web"] );

        if( "true" == $this->query( $sql, "meeting" ) ){
            $sql = sprintf( "UPDATE %s.member SET create_datetime='%s'", $this->dbList["meeting"], date("Y-m-d H:i:s") );
            print $this->query( $sql, "meeting" );
            exit;
        }
        print "false";
    }

    public function action_member_group()
    {
        $ret = $this->truncate( "member_group", "meeting" );
        $sql = sprintf( "INSERT IGNORE INTO %s.member_group SELECT * FROM %s.member_group;", $this->dbList["meeting"], $this->dbList["web"] );
        print $this->query( $sql, "meeting" );

/* registtime 確認
        if( "true" == $this->query( $sql, "meeting" ) ){
            $sql = sprintf( "UPDATE %s.member_group SET member_group_registtime='%s'", $this->dbList["meeting"], date("Y-m-d H:i:s") );
            print $this->query( $sql, "meeting" );
            exit;
        }
        print "false";
        */
    }

    public function action_ordered_service_option()
    {
        $ret = $this->truncate( "ordered_service_option", "meeting" );
        $sql = sprintf( "INSERT IGNORE INTO %s.ordered_service_option(
ordered_service_option_key,
room_key,
service_option_key,
ordered_service_option_status,
ordered_service_option_registtime,
ordered_service_option_deletetime
) SELECT * FROM %s.ordered_service_option;", $this->dbList["meeting"], $this->dbList["web"] );
        print $this->query( $sql, "meeting" );
    }

    public function action_reservation()
    {
        $ret = $this->truncate( "reservation", "meeting" );
        $sql = sprintf( "
SELECT * FROM %s.reservation r LEFT JOIN (
    SELECT room_key, user_key FROM %s.room
) room
ON  r.room_key = room.room_key;", $this->dbList["web"], $this->dbList["web"] );

        $ret = $this->conn->query( $sql );

        while( $row = $ret->fetchRow(DB_FETCHMODE_ASSOC ) ) {

            if($row['reservation_pw'] !== "") {
                 $pw_type = 1;
            } else {
                 $pw_type = 0;
    }
            $data = array(
                'reservation_key'           => $row['reservation_key'],
                 'user_key'                  => $row['user_key'],
                 'room_key'                  => $row['room_key'],
                 'meeting_key'               => $row['meeting_key'],
                 'reservation_name'          => $row['reservation_name'],
                 'reservation_place'         => $row['reservation_place'],
                 'reservation_starttime'     => $row['reservation_starttime'],
                 'reservation_endtime'       => $row['reservation_endtime'],
                 'reservation_pw'            => $row['reservation_pw'],
                 'reservation_pw_type'       => $pw_type,
                 'reservation_session'       => $row['reservation_session'],
                 'reservation_status'        => $row['reservation_status'],
                 'reservation_registtime'    => $row['reservation_registtime'],
                 'reservation_updatetime'    => $row['reservation_updatetime']
            );

            require_once( "classes/dbi/reservation.dbi.php" );
            $obj_Reservation = new ReservationTable( $this->dsn );
            $sth = $obj_Reservation->add( $data );

            if (PEAR::isError($sth)) {
                $this->logger2->error( $sth->getUserInfo() );
                print "false";
                exit;
            }
        }
        print "true";
    }

    public function action_reservation_user()
    {
        $ret = $this->truncate( "reservation_user", "meeting" );

        $core_table = $this->_get_columns("reservation_user", $this->dbList["web"] );
        //$data_table = $this->_set_prefix($core_table, "m");

        $sql = 'INSERT INTO %s.reservation_user (';
        $sql .= implode(",", $core_table).' )';
        $sql .= 'SELECT ';
        $sql .= implode(",", $core_table);
        $sql .= ' FROM %s.reservation_user;';
        $sql = sprintf($sql, $this->dbList["meeting"], $this->dbList["web"] );
        print $this->query( $sql, "meeting" );
    }

    public function action_room()
    {
        $ret = $this->truncate( "room", "meeting" );
        $sql = sprintf( "INSERT IGNORE INTO %s.room
SELECT
    r.room_key,
    IFNULL( rs.room_seat_max_seat, 10 ) as max_seat,
    IFNULL( rs.room_seat_max_audience_seat, 0 ) as max_audience_seat,
    r.user_key,
    r.room_name,
    r.room_layout_type,
    r.room_status,
    r.meeting_key,
    r.room_registtime,
    r.room_updatetime,
    r.room_deletetime
FROM
    %s.room r
LEFT JOIN
    %s.room_seat rs
ON
    r.room_key = rs.room_key;", $this->dbList["meeting"], $this->dbList["web"], $this->dbList["web"] );
        print $this->query( $sql, "meeting" );
    }

    public function action_room_plan()
    {
        $ret = $this->truncate( "room_plan", "meeting" );

        $core_table = $this->_get_columns("room_plan", $this->dbList["web"] );
        //$data_table = $this->_set_prefix($core_table, "m");

        $sql = 'INSERT INTO %s.room_plan (';
        $sql .= implode(",", $core_table).' )';
        $sql .= 'SELECT ';
        $sql .= implode(",", $core_table);
        $sql .= ' FROM %s.room_plan;';
        $sql = sprintf($sql, $this->dbList["meeting"], $this->dbList["web"] );
        print $this->query( $sql, "meeting" );
    }

    public function action_user()
    {
        $ret = $this->truncate( "user", "meeting" );

        switch( $this->dbList["service"] )
        {
            case "jp":
            case "us":
                $ret = $this->truncate( "user", "meeting" );

                $core_table = $this->_get_columns("user", $this->dbList["web"] );
                //$data_table = $this->_set_prefix($core_table, "m");
                unset( $core_table["reseller_key"] );

                $sql = 'INSERT INTO %s.user (';
                $sql .= implode(",", $core_table).' )';
                $sql .= 'SELECT ';
                $sql .= implode(",", $core_table);
                $sql .= ' FROM %s.user;';
                $sql = sprintf($sql, $this->dbList["meeting"], $this->dbList["web"] );

            case "sales":
                break;

            default:
                return false;
        }
        print $this->query( $sql, "meeting" );
    }

    public function action_news()
    {
        $ret = $this->truncate( "news", "mgm" );
        $sql = sprintf( "INSERT INTO %s.news SELECT * FROM %s.news;", $this->dbList["mgm"], $this->dbList["web"] );
        print $this->query( $sql, "mgm" );
    }

    public function action_relation()
    {
        $ret = $this->truncate( "relation", "mgm" );
        //invite
        $sql = sprintf("SELECT
    r.r_user_session, u.user_id
FROM
(
SELECT
    ru.*, r.user_key
FROM
(
SELECT
    ru.reservation_key, ru.r_user_session, r.room_key
FROM
    %s.reservation_user ru
LEFT JOIN
    %s.reservation r
ON
    ru.reservation_key = r.reservation_key
    ) ru
LEFT JOIN
    %s.room r
ON
    ru.room_key = r.room_key
) r
LEFT JOIN
    %s.user u
ON
    r.user_key = u.user_key
WHERE
    r.user_key is not null", $this->dbList["web"], $this->dbList["web"], $this->dbList["web"], $this->dbList["web"] );

        $ret = $this->mgm_conn->query( $sql );
        require_once( "classes/mgm/dbi/relation.dbi.php" );
        $obj_Relation = new MgmRelationTable( $this->_auth_dsn );
        while( $row = $ret->fetchRow(DB_FETCHMODE_ASSOC ) ) {
            $data = array(
                        'relation_key'  => $row['r_user_session'],
                        'user_key'    => $row['user_id'],
                        'status'    => 1,
                        'relation_type' => 'invite' );
            $sth = $obj_Relation->add( $data );
            if (PEAR::isError($sth)) {
                $this->logger2->error( $sth->getUserInfo() );
                print "false";
                exit;
            }
        }
        //mfp
        $sql = sprintf( "
SELECT
    r.room_key as relation_key, u.user_id as user_key
FROM
    %s.room r
LEFT JOIN
    %s.user u
ON
    r.user_key = u.user_key", $this->dbList["web"], $this->dbList["web"] );

        $ret = $this->conn->query( $sql );
        if ( PEAR::isError( $ret ) ) {
            $this->logger2->error( $ret->getUserInfo() );
            print "false";
            exit;
        }
        while( $row = $ret->fetchRow(DB_FETCHMODE_ASSOC ) ) {
            if( ! $row['relation_key'] || ! $row['user_key'] ) continue;
            $data = array(
                        'relation_key'  => $row['relation_key'],
                        'user_key'    => $row['user_key'],
                        'status'    => 1,
                        'relation_type' => 'mfp' );
            $sth = $obj_Relation->add( $data );
            if (PEAR::isError($sth)) {
                $this->logger2->error( $sth->getUserInfo() );
                print "false";
                exit;
            }
        }
        print "true";
    }

    public function action_mgm_user()
    {
        $ret = $this->truncate( "user", "mgm" );
        $sql = sprintf( "
INSERT INTO %s.user(
    user_id
) SELECT
    user_id
FROM
    %s.user", $this->dbList["mgm"], $this->dbList["web"] );

        if( "true" == $this->query( $sql, "mgm" ) ){
        $sql = sprintf( "
INSERT INTO %s.user(
user_id
) SELECT
    member_id
FROM
    %s.member", $this->dbList["mgm"], $this->dbList["web"] );

            if( "true" == $this->query( $sql, "mgm" ) ){
                $sql = sprintf( "UPDATE %s.user SET server_key='1', user_registtime='%s', user_updatetime='%s'", $this->dbList["mgm"], date("Y-m-d H:i:s"), date("Y-m-d H:i:s") );
                print $this->query( $sql, "mgm" );
                exit;
            }
        }
            print "false";
    }

    private function query( $sql, $dbName )
    {
        $ret = ( "mgm" == $dbName ) ? $this->mgm_conn->query( $sql ) : $this->conn->query( $sql );
        $this->logger2->info($sql);
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return "false";
        } else {
            return "true";
        }
    }

    private function _get_columns($table, $dbname ) {
        $check_sql = sprintf("SHOW COLUMNS FROM %s FROM %s;", $table, $dbname);
        $rs1 = $this->conn->query( $check_sql );
        while($row1 = $rs1->fetchRow(DB_FETCHMODE_ASSOC)) {
            $type = $row1["Type"];
            $len_start = strpos($type, "(");
            if ($len_start) {
                $len_end = strpos($type, ")") - 1;
                $row1["Type"] = substr($type, 0, $len_start);
                $row1["Size"] = substr($type, $len_start + 1, ($len_end - $len_start));
            }
            $table_info[$row1["Field"]] = $row1["Field"];
        }
        $this->logger2->info($table_info);
        return $table_info;
    }

    private function _set_prefix($table_data, $prefix ) {
        foreach ($table_data as $data ) {
            $set_data[] = $prefix.".".$data;
        }
        return $set_data;
    }

    private function truncate( $tableName, $dbName)
    {
//        $sql = sprintf( "TRUNCATE TABLE n2my_%s.%s", $dbName, $tableName );
        $sql = sprintf( "TRUNCATE TABLE %s.%s", $dbName == "mgm" ? $this->dbList["mgm"] : $this->dbList["meeting"], $tableName );
        return $this->query( $sql, $dbName );
    }
}

$main =& new AppShift();
$main->execute();