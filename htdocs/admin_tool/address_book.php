<?php

require_once("classes/AppFrame.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/mgm/dbi/user.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/address_book.dbi.php");
require_once("lib/EZLib/EZUtil/EZCsv.class.php");

class AppAddressBook extends AppFrame {

    var $_auth = null;
    var $_sess_page;
    var $account_dsn = null;
    var $objUser = null;
    var $objAddressBook = null;
    var $server_key = null;

    /**
     * 初期化
     */
    function init() {
        $_COOKIE["lang"] = "ja";
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->_sess_page = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->_name_space = md5(__FILE__);
        if ($this->get_dsn()) {
            $this->objUser = new UserTable($this->get_dsn());
            $this->objAddressBook = new AddressBookTable($this->get_dsn());
        }
    }

    /**
     * DSN取得
     */
    function get_dsn() {
        static $dsn;
        if ($dsn) {
            return $dsn;
        } else {
            if ($id = $this->request->get("user_id")) {
                $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
                if (!$user_info = $obj_MGMClass->getUserInfoById( $id ) ){
                    die("User ID is not correct.");
                    return false;
                }
                if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
                    die("No member information.");
                    return false;
                }
            }
        }
        $this->logger2->info($server_info);
        $dsn = $server_info["dsn"];
        return $dsn;
    }

    function action_download() {
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="address_book.csv"');
        print file_get_contents(N2MY_APP_DIR."templates/common/admin_tool/address_book/sample.csv");
    }

    /**
     * メンバー一括登録確認
     */
    public function action_import_confirm() {
        //
        $user_id = $this->request->get("user_id");
        if (!trim($user_id)) {
            die("Please input the User ID");
        }
        $where = "user_id = '".addslashes($user_id)."'";
        $user_info = $this->objUser->getRow($where);
        if (!$user_info) {
            die("User ID is not correct.");
        }
        // ファイルアップロード
        $request["upload_file"] = $_FILES["import_file"];
        $data = pathinfo($request["upload_file"]["name"]);
        if ($data['extension'] != "csv") {
            die("Please update the CSV File.");
        }
        $filename = $this->get_work_dir()."/".tmpfile();
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $this->session->set('address_book_info', $request);
        // メッセージ
        $msg_format = $this->get_message("error");
        $msg = $this->get_message("ERROR");
        $csv = new EZCsv(true, "UTF-8", "UTF-8");
        $csv->open($filename, "r");
        $rows = array();
        $err_flg = 0;
        while($row = $csv->getNext(true)) {
            $row["user_key"] = $user_info["user_key"];
            if (!$this->check($row)) {
                $err_fields = $this->_error_obj->error_fields();
                $error = array();
                foreach ($err_fields as $key) {
                    $type = $this->_error_obj->get_error_type($key);
                    $err_rule = $this->_error_obj->get_error_rule($key, $type);
                    if (($type == "allow") || $type == "deny") {
                        $err_rule = join(", ", $err_rule);
                    }
                    $_msg = sprintf($msg[$type], $err_rule);
                    $error[$key] = $_msg;
                    $err_flg = 1;
                }
                $row["error"] = $error;
            }
            $rows[] = $row;
        }
        $this->template->assign("err_flg", $err_flg);
        $this->template->assign("rows", $rows);
        $this->display('admin_tool/address_book/add_list.t.html');
    }

    /**
     * メンバー一括登録確認
     */
    function action_import() {
        // ユーザー情報
        $user_id = $this->request->get("user_id");
        if (!trim($user_id)) {
            die("Please input the user ID");
        }
        $where = "user_id = '".addslashes($user_id)."'";
        $user_info = $this->objUser->getRow($where);
        if (!$user_info) {
            die("User ID is not correct.");
        }
        // CSVパース
        $csv = new EZCsv(true, "UTF-8", "UTF-8");
        $address_book_info = $this->session->get('address_book_info');
        $csv->open($address_book_info["upload_file"]["tmp_name"], "r");
        $result = array(
            "ok" => 0,
            "ng" => 0,
            );
        while($row = $csv->getNext(true)) {
            $row["user_key"] = $user_info["user_key"];
            if ($this->check($row)) {
                $ret = $this->address_book_add($row);
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                } else {
                    $result["ok"]++;
                    $ok_address_book_list[] = $row;
                    $this->logger->info(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,$row);
                }
            } else {
                $this->logger->warn(__FUNCTION__."#input error",__FILE__,__LINE__,$this->_error_obj);
                $result["ng"]++;
                $ng_address_book_list[] = $row;
                $row["error"] = "error";
            }
        }
        // 結果表示
        $this->template->assign("result", $result);
        $this->display('admin_tool/address_book/done_list.t.html');
    }

    /**
     * メンバー追加
     */
    function address_book_add($address_book_info) {
        $ret = $this->objAddressBook->add($address_book_info);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        } else {
            $this->logger->info(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,$address_book_info);
            return true;
        }
    }

    /**
     * 入力確認
     * @param mixed $data 確認データ
     */
    function check($data) {
        // 厳密な入力チェック
        $rules = $this->objAddressBook->rules;
        $rules["lang"]["allow"] = array_keys($this->get_language_list());
        $rules["country"]["allow"] = array_keys($this->get_country_list());
        $this->_error_obj = $this->objAddressBook->check($data, $rules);
        if (EZValidator::isError($this->_error_obj)) {
            return false;
        } else {
            return true;
        }
    }

    public function default_view() {
         $this->action_top();
    }

    public function action_top() {
        $this->display( "admin_tool/address_book/add_list.t.html" );
    }
}


$main =& new AppAddressBook();
$main->execute();