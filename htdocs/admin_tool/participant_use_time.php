<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");

class AppParticipantUseCount extends AppFrame {

    function init() {
    }

    function default_view() {
        // すでにデータが存在する場合
        require_once("classes/mgm/dbi/meeting_date_log.dbi.php");
        $objDateLog = new MeetingDateLogTable(N2MY_MDB_DSN);
        $configIni = "../../config/config.ini";
        $serverListIni = sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR );
        $config = parse_ini_file( $configIni, true );
        $serverList = parse_ini_file( $serverListIni, true);
        print "<h3></h3>";
        print "<pre>";
        foreach( $serverList["SERVER_LIST"] as $key => $dsn ){
            $meeting_obj = new MeetingTable($dsn);
            $participant_obj = new DBI_Participant($dsn);
            $where = "actual_start_datetime > '2009-04-01'" .
                " AND meeting_use_minute > 0" .
                " AND use_flg = 1" .
                " AND is_active = 0";
            $meeting_list = $meeting_obj->getRowsAssoc($where, null, null, null, "meeting_key, room_key, user_key, eco_co2, eco_info, actual_end_datetime", "meeting_key");
            foreach ($meeting_list as $meeting_key => $meeting_info) {
                $eco_info = unserialize($meeting_info["eco_info"]);
                $co2 = isset($eco_info["total"]["co2"]) ? $eco_info["total"]["co2"] : 0;
                if (1) {
                    $where = "meeting_key = ".$meeting_key.
                        " AND is_active = 0" .
                        " AND uptime_end IS NOT NULL" .
                        " AND use_count = 0";
                    $participant_list = $participant_obj->getRowsAssoc($where, null, null, null, "participant_key, uptime_start, uptime_end", "participant_key");
                    $total_minute = 0;
                    // 0件なら更新しない
                    if ($participant_list) {
                        foreach ($participant_list as $participant_key => $participant_info) {
                            $second = strtotime($participant_info["uptime_end"]) - strtotime($participant_info["uptime_start"]);
                            $minute = ceil($second / 60);
                            $where = "participant_key = ".$participant_key;
                            $data = array("use_count" => $minute);
                            //print_r(array($where, $data));
                            $participant_obj->update($data, $where);
                            $total_minute += $minute;
                        }
                        // ECOメーターの再集計
                        $client_co2 = $total_minute * ECO_CLIENT_CO2;
                        $actual_co2 = ($co2 - $client_co2);
                        $eco_info["client_co2"] = $actual_co2;
                        $where = "meeting_key = ".$meeting_key;
                        $data = array(
                            "eco_co2" => $actual_co2,
                            "eco_info" => serialize($eco_info));
                        $meeting_obj->update($data, $where);
                        // 日ごとの集計データを見直し
                        $date = substr($meeting_info["actual_end_datetime"], 0, 10);
                        $sql = "SELECT DATE_FORMAT(actual_end_datetime, '%Y-%m-%d') as log_date" .
                            ", user.user_id as user_id" .
                            ", room_key" .
                            ", SUM(eco_co2)  AS eco_co2" .
                            " FROM meeting, user" .
                            " WHERE meeting.user_key = user.user_key" .
                            " AND room_key = '".$meeting_info["room_key"]."'".
                            " AND actual_end_datetime like '".addslashes($date)."%'" .
                            " AND use_flg = 1".
                            " GROUP BY DATE_FORMAT(actual_end_datetime, '%Y-%m-%d')" .
                            ",room_key";
                        $eco_data = $meeting_obj->_conn->getRow($sql, DB_FETCHMODE_ASSOC);
                        if ($eco_data) {
                            $user_id = $eco_data["user_id"];
                            $room_key = $eco_data["room_key"];
                            $where = "log_date = '".addslashes($date)."'" .
                                    " AND user_id = '".addslashes($user_id)."'" .
                                    " AND room_key = '".addslashes($room_key)."'";
                            print_r(array($meeting_key, $where, $total_minute, $co2, $actual_co2, $eco_data));
                            if ($objDateLog->numRows($where) > 0) {
                                // 更新
                                $ret = $objDateLog->update($eco_data, $where);
                            } else {
                                // 追加
                                $ret = $objDateLog->add($eco_data);
                            }
                        }
                    }
                }
            }
        }
    }
}

$main = new AppParticipantUseCount();
$main->execute();
