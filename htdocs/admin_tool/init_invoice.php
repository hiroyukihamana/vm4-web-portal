<?php
set_time_limit(3600);
// 環境設定
require_once("bin/set_env.php");
require_once("classes/AppFrame.class.php");

// 日計ログ
require_once("classes/mgm/dbi/meeting_date_log.dbi.php");

// ユーザー関連
require_once ("classes/mgm/MGM_Auth.class.php");
require_once("classes/dbi/staff.dbi.php");
require_once("classes/dbi/user.dbi.php");

// 部屋関連
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/room_plan.dbi.php");

// オプション関連
require_once("classes/dbi/service.dbi.php"); // 部屋の基本料金、初期費用取得
require_once("classes/mgm/dbi/service_option.dbi.php"); // オプションの基本料金、単価取得
require_once("classes/dbi/ordered_service_option.dbi.php"); // 部屋に付随するオプション取得

class AppInitInvoice extends AppFrame {

    private $_core_db_dsn = "";
    private $billing_mains_obj = null;
    private $billing_subs_obj = null;
    private $month_log_init = "";
    private $product = "";
    private $base_url = "";
    private $service_list = "";
    private $service_option_list = "";
    private $month_summary_info = "";

    // 請求処理・対象月取得
    private $target_year = "";
    private $target_month = "";
    private $target_year_month = "";

    private $user_list = "";
    private $room_list = "";
    private $plan_list = "";

    private $_tax_rate = 0;
    private $_sales_tax = 0;
    private $_total_charge = 0;

    private $more_than_standard_plan_keys = array();
    private $premium_plan_keys = array();
    private $hq_plan_key = null;

    private $init_billing_data = array();

    function init()
    {
        // DBサーバ情報取得
        $this->dsn = $this->_get_dsn();
        // サービス名設定
        $this->product = "meeting";
        // 請求合計レコード初期設定
        $this->_init_billing_mains = array(
                                   "billing_year"            => null,
                                   "billing_month"           => null,
                                   "invoice_type"            => null,
                                   "product_id"              => $this->product,
                                   "country_id"              => null,
                                   "client_id"               => null,
                                   "client_key"              => null,
                                   "client_name"             => null,
                                   "client_zip"              => null,
                                   "client_state"            => null,
                                   "client_city"             => null,
                                   "client_address"          => null,
                                   "client_tel"              => null,
                                   "client_fax"              => null,
                                   "client_staff_department" => null,
                                   "client_staff_name"       => null,
                                   "client_staff_mail"       => null,
                                   "paymethod"               => null,
                                   "payment_terms"           => null,
                                   "paymethod_info"          => null,
                                   "staff_id"                => null,
                                   "staff_section_id"        => null,
                                   "staff_info"              => null,
                                   "tax_rate"                => 0,
                                   "sales_tax"               => 0,
                                   "total_charge"            => 0,
                                   "pay_limit_date"          => 0000-00-00,
                                   "billing_status"          => 0,
                                   "billing_date"            => 0000-00-00,
                                   "remarks"                 => null,
                                   "serialize_info"          => null,
                                   "billing_sub_list"        => null,
                               );
        // 請求明細レコード初期設定
        $this->_init_billing_subs = array(
                                   "detail_group_number"     => null,
                                   "detail_group_id"         => null,
                                   "detail_group_name"       => null,
                                   "account_type_number"     => null,
                                   "account_type_id"         => null,
                                   "account_type_name"       => null,
                                   "charge"                  => 0,
                                   "amount"                  => 0,
                                   "unit"                    => null,
                                   "price"                   => 0,
                                   "remarks"                 => null,
                                   "serialize_info"          => null,
                               );
    }


    /**
     * default view routine
     * 単一処理か一括処理かを振り分ける
     * 
     */
    function default_view()
    {
        // 処理の振り分け
		$this->logger->info(__FUNCTION__." # Init Invoice Start ... ", __FILE__, __LINE__);
//        echo "[".__FUNCTION__."] # Start ... \r\n";

		// リクエスト取得（処理名）
		$action = ($_REQUEST['i1'] ?$_REQUEST['i1'] : null);

		// 処理の振り分け
        switch ($action) {
            case "get_one" :
                // 単一ユーザの初期費用請求書作成
                $user_id = ($_REQUEST['i2'] ?$_REQUEST['i2'] : null);
                $payment_terms = ($_REQUEST['i3'] ?$_REQUEST['i3'] : null);
                $invoice_info = $this->make_one_init_invoice_info($user_id, $payment_terms);
                break;
            case "account_list" :
                // 初期費用請求書発行全対象ユーザＩＤ取得
                $yaer = ($_REQUEST['i2'] ?$_REQUEST['i2'] : null);
                $month = ($_REQUEST['i3'] ?$_REQUEST['i3'] : null);
                $invoice_info = $this->get_all_init_account_info($yaer, $month);
                break;
            case "get_all" :
                // 全対象ユーザの初期費用請求書作成
                $invoice_info = $this->make_all_init_invoice_info($action);
                break;
            default :
                $this->param_error($action);
                break;
		}

    }


    /**
     * make_one_init_invoice_info view routine
     *
     */
    function make_one_init_invoice_info($user_id=null, $payment_terms=null)
    {
		$this->logger->info(__FUNCTION__." # Make One Init Invoice Start ... ", __FILE__, __LINE__);

        // 初期化
        $status = 1;
        $user_info = null;
        $this->service_list = array();
        $init_invoice_info = array();
        $init_invoice_info_zip = array();
        $ret_user_info = null;
        $user_key_wk = null;

		// 処理年月（請求）チェック・取得
	    $_year = date("Y",time()); // 請求年（当年）
	    $_month = date("m",time()); // 請求月（当月）
        $target_start_date = $_year . "-" . $_month . "-01 00:00:00";

        // ユーザ情報取得（ＤＳＮ含み）
        $ret_user_info = $this->get_user_info($user_id);
        //$this->logger->info(__FUNCTION__." # user_id ", __FILE__, __LINE__, $user_id);

        if ("1" == $ret_user_info['status']) {

            // 部屋サービスプラン一覧取得
            $this->service_list = $this->getServiceList();
//          $this->logger->info(__FUNCTION__." # V3List ... ", __FILE__, __LINE__, $this->service_list);

            // オプション一覧取得
            $this->service_option_list = $this->getOptionPriceList();
//          $this->logger->info(__FUNCTION__." # ServiceOptionList ... ", __FILE__, __LINE__, $this->service_option_list);

            $user_key_wk = $ret_user_info['user_info']['user_key'];
            $user_info[$user_key_wk] = $ret_user_info['user_info'];
            $user_info[$user_key_wk]['payment_terms'] = $payment_terms;
            $meeting_dsn = $ret_user_info['dsn'];

            // 初期費用請求対象プラン取得
            $init_plan_list = $this->get_init_plan($meeting_dsn, $user_info[$user_key_wk], $target_start_date);
            $this->logger->info(__FUNCTION__." # User ", __FILE__, __LINE__, $user_info[$user_key_wk]);
            //$this->logger->info(__FUNCTION__." # init plan list ", __FILE__, __LINE__, $init_plan_list);

            // データ編集
            if ($init_plan_list) {
                foreach ($init_plan_list as $key_year_month => $val_user_info) {
                    $billing_year_month = split("-", $key_year_month);
                    foreach ($val_user_info as $key_user => $val_plan_list) {
                        // 請求データ作成（１ページ目）
                        $init_invoice_info[] = $this->make_init_invoice_info($billing_year_month, $user_info[$key_user], $val_plan_list, "init");
                        if ("pre" == $payment_terms) {
                            // 請求データ作成（２ページ目：前払いの時のみ）
                            $init_invoice_info[] = $this->make_init_invoice_info($billing_year_month, $user_info[$key_user], $val_plan_list, "normal");
                        }
                    }
                }
            }
        } else {
            $status = 0;
        }

        // 初期費用請求データ明細情報圧縮
		foreach ($init_invoice_info as $_key => $_val) {
			$init_invoice_info_zip[$_key] = $_val;
			$init_invoice_info_zip[$_key]['billing_sub_list'] = gzcompress(serialize($_val['billing_sub_list']), 9);
		}

        $output = array(
            "status" => $status,
            "data" => $init_invoice_info_zip,
        );
        $this->logger->info(__FUNCTION__." # output ", __FILE__, __LINE__, $output);

        // 初期費用請求データ出力
        print_r(serialize($output));

		$this->logger->info(__FUNCTION__." # Make One Init Invoice End ... ", __FILE__, __LINE__);
    }


    /**
     * get_all_init_account_info view routine
     *
     */
    function get_all_init_account_info($user_id = null)
    {
/*
		$this->logger->info(__FUNCTION__." # Init Invoice Start ... ", __FILE__, __LINE__);
//        echo "[".__FUNCTION__."] # Start ... \r\n";

		// リクエスト取得（請求年月）
		$_year = ($_REQUEST['billing_year'] ?$_REQUEST['billing_year'] : null);
		$_month = ($_REQUEST['billing_month'] ?$_REQUEST['billing_month'] : null);

		// 処理年月（請求）チェック・取得
		if (!$_year) {
	        $_year = date("Y",time()); // 請求年（当年）
//			$this->logger->info(__FUNCTION__." # no param [year] ... ", __FILE__, __LINE__, $_year);
		} else {
//			$this->logger->info(__FUNCTION__." # check param [year] ... ", __FILE__, __LINE__, $_year);
		}
		if (!$_month) {	
	        $_month = date("m",time()); // 請求月（当月）
//			$this->logger->info(__FUNCTION__." # no param [month] ... ", __FILE__, __LINE__, $_month);
		} else {
//			$this->logger->info(__FUNCTION__." # check param [month] ... ", __FILE__, __LINE__, $_month);
		}

		$_target_datetime = mktime(0, 0, 0, $_month, 1, $_year);

		// 請求年月設定
        $this->billing_year_month = $_year . "-" . $_month;
        $this->billing_year = $_year; // 請求年
        $this->billing_month = $_month; // 請求月

		// 取込対象・売上年月設定
        $this->target_year_month = date("Y-m",strtotime("-1 month", $_target_datetime));
        $year_month_array = split("-", $this->target_year_month); // 取込対象・売上年月（請求月の前月）
        $this->target_year = $year_month_array[0]; // 取込対象・売上年（請求月の前月）
        $this->target_month = $year_month_array[1]; // 取込対象・売上対象月（請求月の前月）

        // 初期化
        $this->service_list = array();
        $this->service_option_list = array();

        // 部屋サービスプラン一覧取得
        $service_obj = new ServiceTable(N2MY_MDB_DSN);
        $_service_condition = " `service_charge` != 0 OR `service_additional_charge` != 0 ";
        $this->service_list = $service_obj->getRowsAssoc($_service_condition, null, null, null, null, "service_key");

ini_set("memory_limit", "128M");

        // 請求データ作成（メイン）処理（用意されているDBの分だけ集計処理を繰り返す）
        $this->init_billing_data = array();
        foreach ($this->dsn as $server_key => $server_dsn) {
            $this->_billing_data_routine($server_dsn);
        }
//		$this->logger->info(__FUNCTION__." # MemoryInfo ... ", __FILE__, __LINE__, memory_get_usage());
		$this->_clear_private_info();
//		$this->logger->info(__FUNCTION__." # MemoryInfo ... ", __FILE__, __LINE__, memory_get_usage());

//		$this->logger->info(__FUNCTION__." # init_billing_data ... ", __FILE__, __LINE__, $this->init_billing_data);
		foreach ($this->init_billing_data as $main_key => $main_val) {
			$sub_serialize_info[$main_key] = $main_val;
			$sub_serialize_info[$main_key]['billing_sub_list'] = gzcompress(serialize($main_val['billing_sub_list']), 9);
		}
		unset($this->init_billing_data);
//		$this->logger->info(__FUNCTION__." # MemoryInfo ... ", __FILE__, __LINE__, memory_get_usage());

		print_r(serialize($sub_serialize_info));

ini_restore("memory_limit");

		$this->logger->info(__FUNCTION__." # Monthly Operation End ... ", __FILE__, __LINE__);
*/
    }


    /**
     * make_all_init_invoice_info view routine
     *
     */
    function make_all_init_invoice_info($action=null)
    {
		$this->logger->info(__FUNCTION__." # Param 1 Error : [" . $action . "]", __FILE__, __LINE__);
		print_r("Param 1 Error !! ==> [" . $action . "]");
    }


    /**
     * param_error view routine
     *
     */
    function param_error($action=null)
    {
		$this->logger->info(__FUNCTION__." # Param 1 Error : [" . $action . "]", __FILE__, __LINE__);
		print_r("Param 1 Error !! ==> [" . $action . "]");
    }


    /**
     * 初期費用請求データ作成
     * 
     */
    private function get_init_plan($data_db_dsn=null, $user_info=null, $target_start_date=null)
    {
        $room_list_wk = null;
        $plan_list_wk = null;
        $option_list_wk = null;
        $init_plan_list = null;
        $option_start_date_wk = null;
        $option_start_yaer_month = null;
        //$this->logger->info(__FUNCTION__." # UserList ... ", __FILE__, __LINE__, $user_info);

		// 有効な初期費用請求対象部屋一覧取得
        $room_keys = array();
        $whereRoom = " `user_key` = " . addslashes( $user_info['user_key'] ) . " " .
                     //" AND `room_registtime` >= '". $target_start_date ."' " .
                     " AND `room_status` = 1 " .
                     " ";
        $coulumnsRoom = " `user_key`, `room_key`, `room_name`, `max_seat`, `max_audience_seat`, `room_registtime` ";
        $room_obj = new RoomTable($data_db_dsn);
        $room_list_wk = $room_obj->getRowsAssoc($whereRoom, null, null, null, $coulumnsRoom);
        //$this->logger->info(__FUNCTION__." # RoomList ... ", __FILE__, __LINE__, $room_list_wk);

		// 有効な初期費用請求対象プラン一覧取得
        if ($room_list_wk) {
            $room_keys = array();
            foreach ($room_list_wk as $key_room => $val_room) {
                $room_key = $val_room['room_key'];
                $wherePlan = " `room_key` = '" . addslashes( $room_key ) . "' " .
                		     " AND `room_plan_starttime` >= '". addslashes( $target_start_date ) . "' " .
                             " AND `room_plan_status` != 0 " .
                             " ";
                $coulumnsPlan = " `room_key`, `service_key`, `room_plan_yearly`, `room_plan_yearly_starttime`, `room_plan_starttime` ";
                $plan_obj = new RoomPlanTable($data_db_dsn);
                $plan_list_wk = $plan_obj->getRowsAssoc($wherePlan, null, null, null, $coulumnsPlan, "room_key");
                //$this->logger->info(__FUNCTION__." # PlanList ... ", __FILE__, __LINE__, $plan_list_wk);
                if ($plan_list_wk) {
                    $start_yaer_month_wk = split("-", $plan_list_wk[$room_key]['room_plan_starttime']);
                    $start_yaer_month = $start_yaer_month_wk[0] . "-" . $start_yaer_month_wk[1];
                    $init_plan_list[$start_yaer_month][$val_room['user_key']][$val_room['room_key']]['room_info'] = $val_room;
                    $init_plan_list[$start_yaer_month][$val_room['user_key']][$val_room['room_key']]['plan_info'] = $plan_list_wk[$room_key];

                    // 有効な初期費用請求対象部屋の付属オプション一覧取得
                    $whereOption = " `room_key` = '" . addslashes( $room_key ) . "' " .
                                   //" AND `room_plan_starttime` >= '". addslashes( $target_start_date ) . "' " .
                                   " AND `ordered_service_option_status` != 0 " .
                                   " ";
                    $coulumnsOption = " `room_key`, `service_option_key`, `ordered_service_option_starttime` ";
                    $option_obj = new OrderedServiceOptionTable($data_db_dsn);
                    $option_list_wk = $option_obj->getRowsAssoc($whereOption, null, null, null, $coulumnsOption);
                    //$this->logger->info(__FUNCTION__." # OptionList ... ", __FILE__, __LINE__, $option_list_wk);
                    if ($option_list_wk) {
                        foreach ($option_list_wk as $key => $val) {
                            $init_plan_list[$start_yaer_month][$val_room['user_key']][$val_room['room_key']]['option_info'][$val['service_option_key']]['service_option_key'] = $val['service_option_key'];
                            $init_plan_list[$start_yaer_month][$val_room['user_key']][$val_room['room_key']]['option_info'][$val['service_option_key']]['count'] += 1;
                			$option_start_date_wk = split("-", $val['ordered_service_option_starttime']);
                            $option_start_yaer_month = $option_start_date_wk[0] . "-" . $option_start_date_wk[1];
                			if ( $start_yaer_month == $option_start_yaer_month ) { 
				                // 初期費用カウント（初期費は付与した月の請求分にしか上乗せしない。）
                                $init_plan_list[$start_yaer_month][$val_room['user_key']][$val_room['room_key']]['option_info'][$val['service_option_key']]['init_count'] += 1;
                		    } else {
                                $init_plan_list[$start_yaer_month][$val_room['user_key']][$val_room['room_key']]['option_info'][$val['service_option_key']]['init_count'] += 0;
                			}
                        }
                    } else {
                        $init_plan_list[$start_yaer_month][$val_room['user_key']][$val_room['room_key']]['option_info'] = null;
                    }
                }
            }
        }
        return $init_plan_list;
    }


    /**
     * 請求データ作成
     * （dataDB毎に実施）
     *
     */
    private function make_init_invoice_info($billing_year_month=array(), $user_info=null, $init_plan_list=null, $invoice_type="init")
    {
        $param = array();
        $billing_mains = null;
        $billing_year = $billing_year_month[0];
        $billing_month = $billing_year_month[1];
        $start_date_wk = null;
        $remark_wk = null;

        // 初期費用請求合計情報初期化
        $billing_mains = $this->_init_billing_mains;

        // 初期費用請求明細情報編集
        foreach ($init_plan_list as $room_key => $room_val) {
            // 課金項目ごとの編集＆集計
            $billing_subs[$room_key] = $this->_edit_billing_subs($invoice_type, $room_val, $user_info, $billing_year_month);
            if ("init" == $invoice_type) {
                $start_date_wk = split(" ",$room_val['plan_info']['room_plan_starttime']);
                $remark_wk = $start_date_wk[0]; // 初期費用請求時は、備考に開始日を設定する
            }
        }

        // 初期費用請求合計情報編集＆集計
        $billing_mains["billing_year"]     = $billing_year;
        $billing_mains["billing_month"]    = $billing_month;
        $billing_mains["invoice_type"]     = $invoice_type;
        //$billing_mains["product_id"]       = "meeting"; // 上流で設定済み
        // クライアント情報
        $billing_mains["country_id"]       = $user_info['country_key'];
        $billing_mains["client_key"]       = $user_info['user_id'];
        // 支払い情報（請求管理システムで設定を行う）
        // 金額情報（請求管理システムで設定を行う）
        // その他情報
        $billing_mains["remarks"]          = $remark_wk;
        //$billing_mains["serialize_info"]   = "";

        $detail_group_number = 1; // 明細（小計）グループ番号
        foreach($billing_subs as $key_room => $val_room){ // 
            $account_type_number = 1; // 明細連番（明細の順番）
            foreach($val_room as $key => $val){
                // 請求明細情報の登録
				if ("_total_" == $val['account_type_id']) {
                    $val['account_type_number'] = 100;
				} else {
                    $val['account_type_number'] = $account_type_number;
                }
                if (!$val['detail_group_number']) {
                    $val['detail_group_number'] = $detail_group_number;
                }
                $account_type_number += 1;
                $billing_mains['billing_sub_list'][] = $val;
            }
            $detail_group_number += 1;
        }
        return $billing_mains;

	}


    /**
     * BillingDetailInfoDB登録情報編集
     */
    private function _edit_billing_subs($invoice_type="init", $init_plan_info = array(), $user_info = array(), $billing_year_month=null)
    {

		$_charge_time = 0;
        // 請求明細初期化用オブジェクト初期設定
        $_billing_subs = array();
        $_billing_detail_work = array();

        // 初期化
        $_billing_detail_work = $this->_init_billing_subs;
        $_billing_detail_work['detail_group_id'] = $init_plan_info['room_info']['room_key'];
        $_billing_detail_work['detail_group_name'] = $init_plan_info['room_info']['room_name'];

        // 明細レコード生成（roomタイトル情報）
        $_title_data = $this->_edit_title($_billing_detail_work, $init_plan_info, $user_info['user_id'], $invoice_type);
        $_billing_subs[] = $_title_data;

        if ("init" == $invoice_type) {
            // 明細レコード生成（プラン初期費用）
            $_billing_subs[] = $this->_edit_init_charge($_billing_detail_work, $init_plan_info, $user_info);
            if ("post" != $user_info['payment_terms']) {
                // 明細レコード生成（基本料金）
                $_billing_subs[] = $this->_edit_basic_charge($_billing_detail_work, $init_plan_info['plan_info'], $user_info, $billing_year_month, $invoice_type);
            }
        } else { // "normal"
            // 明細レコード生成（基本料金）
            $_billing_subs[] = $this->_edit_basic_charge($_billing_detail_work, $init_plan_info['plan_info'], $user_info, $billing_year_month);
            // 明細レコード生成（追加料金）
            $_billing_subs[] = $this->_edit_extend_charge($_billing_detail_work, $init_plan_info['plan_info'], $user_info['user_id'], $_charge_time);
            // 明細レコード生成（携帯テレビ電話月額基本料金）
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_work, $init_plan_info, "mobile");
            // 明細レコード生成（携帯テレビ電話追加料金（通常））
            $_billing_subs[] = $this->_edit_option_extend_charge($_billing_detail_work, $init_plan_info, $user_info['user_id'], "mobile", "normal");
            // 明細レコード生成（携帯テレビ電話追加料金（特殊））
            $_billing_subs[] = $this->_edit_option_extend_charge($_billing_detail_work, $init_plan_info, $user_info['user_id'], "mobile", "special");
            // 明細レコード生成（Ｈ.３２３通信月額基本料金）
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_work, $init_plan_info, "h323");
            // 明細レコード生成（Ｈ.３２３通信追加料金）
            $_billing_subs[] = $this->_edit_option_extend_charge($_billing_detail_work, $init_plan_info, $user_info['user_id'], "h323");
            // 明細レコード生成（SSL通信）
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_work, $init_plan_info, "ssl");
            // 明細レコード生成（高画質モード基本料金）
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_work, $init_plan_info, "hispec");
            // 明細レコード生成（高画質モード追加料金）
            $_billing_subs[] = $this->_edit_option_extend_charge($_billing_detail_work, $init_plan_info, $user_info['user_id'], "hispec");
            // 明細レコード生成（デスクトップシェアリング）
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_work, $init_plan_info, "sharing");
            // 明細レコード生成（追加録画容量）
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_work, $init_plan_info, "hdd");
            // 明細レコード生成（コンパクト）
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_work, $init_plan_info, "compact");
            // 明細レコード生成（オーディエンス）
	        $audience_info = null;
            if ((array_key_exists($init_plan_info['plan_info']['service_key'], $this->premium_plan_keys) === false) && // room_plan=プレミアムプラン以外　かつ
                ($init_plan_info['room_info']['max_audience_seat'] > 0)) { // オーディエンス座席数１以上
                if (array_key_exists($this->option_keys['audience'], $this->option_list[$init_plan_info['room_info']['room_key']]) === false) {
                    $audience_info = null;
                } else {
                    $audience_info = $this->option_list[$init_plan_info['room_info']['room_key']][$this->option_keys['audience']];
                }
                if ($audience_info) {
                    if ($audience_info['init_count'] >= 1) { 
                        // 明細レコード生成（オーディエンス初期費用）
                        $_billing_subs[] = $this->_edit_audience_first_charge($_billing_detail_work, $init_plan_info['room_info']);
                    }
                }
                // 明細レコード生成（オーディエンス月額基本料金）
                $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_work, $init_plan_info, "audience");
                // 明細レコード生成（オーディエンス追加料金）
                $_billing_subs[] = $this->_edit_option_extend_charge($_billing_detail_work, $init_plan_info, $user_info['user_id'], "audience");
            }
        }

        // 明細レコード生成（room合計情報）
        $_billing_subs[] = $this->_edit_total($_billing_detail_work, $_billing_subs);

        return $_billing_subs;

    }


    /**
     * 部屋タイトル情報の個別編集
     *
     */
    private function _edit_title($init=array(), $init_plan_info=array(), $user_id=null, $invoice_type="init")
    {
        $plan_info = $init_plan_info['plan_info'];
        $room_info = $init_plan_info['room_info'];
        $service_info = $this->service_list[$plan_info['service_key']];
        $init['account_type_id'] = "_title_";
        $init['price'] = 0; // 単価
        $init['amount'] = 0; // 数量
        $init['charge'] = 0; // 料金
        $init['remarks'] = null; // 備考

        if ("init" == $invoice_type) {
            //$_title_remarks = $room_info['room_name'] . "：ご利用プラン　".$service_info['service_name'];
            $_title_remarks = "ご利用プラン　".$service_info['service_name'];
        } else {
            // 通話編集
            if (60000 == $service_info['service_freetime']) {
                $_free_time_disp = "通話無制限";
            } else {
                $_free_time_disp = $service_info['service_freetime'] . "分";
            }
            $_title_remarks = "ご利用プラン：".$service_info['service_name']."\n".
                              "無料通話時間：".$_free_time_disp.
                              "　　　　課金対象利用時間: 0分";
        }
        $init['remarks'] = $_title_remarks;
        return $init;
    }


    /**
     * 部屋の初期費用の個別編集
     *
     */
    private function _edit_init_charge($init=array(), $init_plan_info=null, $user_info=array())
    {
        $ret_account_info = array();
        $init['account_type_id'] = "init";
        $plan_info = $init_plan_info['plan_info'];
        $room_info = $init_plan_info['room_info'];
        $service_info = $this->service_list[$plan_info['service_key']];
        $init['price'] = $service_info['service_initial_charge']; // 単価
        $init['amount'] = 1; // 数量
        $init['charge'] = $init['price'] * $init['amount']; // 料金

        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        $ret_account_info = $this->get_account_info($init, $room_info, $user_info);
        $init['account_type_name'] = $ret_account_info['name'];
        $init['remarks'] = $ret_account_info['data'];
        return $init;
    }


    /**
     * 部屋の基本料金の個別編集
     *
     */
    private function _edit_basic_charge($init=array(), $plan_info=null, $user_info=array(), $billing_year_month=null, $invoice_type="normal")
    {
        $price_wk = 0; // 単価
        $daily_payment = null;
        $serialize_info_wk = null;
        $ret_account_info = array();
        $init['account_type_id'] = "basic";
        $service_info = $this->service_list[$plan_info['service_key']];

        $init['price'] = $service_info['service_charge']; // 単価
        $init['amount'] = 1; // 数量
        $init['charge'] = $init['price'] * $init['amount']; // 料金

        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        $ret_account_info = $this->get_account_info($init, $plan_info, $user_info);
        $init['account_type_name'] = $ret_account_info['name'];
        $init['remarks'] = $ret_account_info['data'];

        $serialize_info_wk = array("start_date" => $plan_info['room_plan_starttime']);
        $init['serialize_info'] = serialize($serialize_info_wk);

        return $init;
    }


    /**
     * 部屋の追加料金の個別編集
     *
     */
    private function _edit_extend_charge($init=array(), $plan_info=array(), $user_id=null, $_charge_time = 0)
    {
        $init['account_type_id'] = "extends"; // 課金科目
        $service_info = $this->service_list[$plan_info['service_key']];
        $init['price'] = 0; // 単価
        $init['amount'] = 0;
        $init['charge'] = 0; // 料金
        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        $ret_account_info = $this->get_account_info($init, $plan_info);
        $init['account_type_name'] = $ret_account_info['name'];
		$init['unit'] = $ret_account_info['unit'];
        $init['remarks'] = $ret_account_info['data'];
        return $init;
    }


	/**
	 * オプション基本料金の共通編集
	 *
	 */
	private function _edit_option_basic_charge($init=array(), $init_plan_info=array(), $option_name=null)
	{
        $room_info = $init_plan_info['room_info'];
        $plan_info = $init_plan_info['plan_info'];
        $option_info = $init_plan_info['option_info'];
        $service_info = $this->service_list[$plan_info['service_key']];
		$init['account_type_id'] = $option_name."_basic";
//$this->logger->info(__FUNCTION__." # service_info ... ", __FILE__, __LINE__, $service_info);
        $service_option_info = $this->service_option_list[$this->option_keys[$option_name]];
        if ($option_name == "audience") {
            $init['price'] = $service_option_info['price']; // 単価
            $audiense_amount = ceil($room_info['max_audience_seat'] / 10);
            $init['amount'] = $audiense_amount; // 数量
            $init['charge'] = $init['price'] * $init['amount']; // 料金
        }else{
            if ($option_info) { // オプションを持っている部屋か？
                if (array_key_exists($this->option_keys[$option_name], $option_info) === false) { // 編集対象のオプションは存在するか？
                } else {
                    $service_option_info = $this->service_option_list[$this->option_keys[$option_name]];
                    $init['price'] = $service_option_info['price']; // 単価
                    $init['amount'] = $option_info[$this->option_keys[$option_name]]['count']; // 数量
                    // スタンダードより上位プランの値引き
                    switch ($option_name) {
                        case "mobile" :
                            if (array_key_exists($plan_info['service_key'], $this->more_than_standard_plan_keys) === false) {
                                $init['charge'] = $init['price'] * $init['amount']; // 料金
                            } else {
									$init['charge'] = $init['price'] * (($init['amount'] - 1) < 0 ? 0 : ($init['amount'] - 1)); // 料金
                            }
                            break;
                        case "sharing" :
                            if (array_key_exists($plan_info['service_key'], $this->more_than_standard_plan_keys) === false) {
                                $init['charge'] = $init['price'] * $init['amount']; // 料金
                            } else {
                                $init['charge'] = 0; // 料金
                            }
                            break;
                        case "hispec" :
                            if ($plan_info['service_key'] != $this->hq_plan_key) {
                                $init['charge'] = $init['price'] * $init['amount']; // 料金
                            } else {
                                $init['charge'] = 0; // 料金
                            }
                            break;
                        default :
                            $init['charge'] = $init['price'] * $init['amount']; // 料金
                            break;
                    }
                }
            }
        }
        $init['charge'] = 0; // 料金（初期費用請求時は、課金しない）

		// 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
		$ret_account_info = $this->get_account_info($init, $plan_info);
		$init['account_type_name'] = $ret_account_info['name'];
		$init['unit'] = $ret_account_info['unit'];
		$init['remarks'] = $ret_account_info['data'];
		return $init;
	}


	/**
	 * オプション追加料金の共通編集
	 *
	 */
	private function _edit_option_extend_charge($init=array(), $init_plan_info=array(), $user_id=null, $option_name=null, $extend_type=null)
	{
        $room_info = $init_plan_info['room_info'];
        $plan_info = $init_plan_info['plan_info'];
        $option_info = $init_plan_info['option_info'];
        $service_info = $this->service_list[$plan_info['service_key']];

		$option_extends_name = null;
		if ($extend_type) {
			$option_extends_name = $option_name . "_extends_" . $extend_type;
		} else {
			$option_extends_name = $option_name . "_extends";
		}
		$init['account_type_id'] = $option_extends_name;
        $service_option_info = $this->service_option_list[$this->option_keys[$option_name]];

		if ($option_name == "audience") {
			$init['price'] = $service_option_info['running_price']; // 単価
			$init['amount'] = 0;
			$init['charge'] = 0; // 料金
		}else{
			if ($option_info) {
				if (array_key_exists($this->option_keys[$option_name], $option_info) === false) {
				} else {
					//$this->logger->info(__FUNCTION__."user = ".$init['detail_group_id'], __FILE__, __LINE__, $service_option_info);
					if (($option_name == "mobile") && ($extend_type == "special")) {
						$price = 80; // 単価
					} else {
						$price = $service_option_info['running_price']; // 単価
					}
					$init['price'] = $price; // 単価
					$init['amount'] = 0;

					// 高画質プランの値引き
					$init['charge'] = 0; // 料金
				}
			}
		}
        $init['charge'] = 0; // 料金（初期費用請求時は、課金しない）
		// 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
		$ret_account_info = $this->get_account_info($init, $plan_info);
		$init['account_type_name'] = $ret_account_info['name'];
		$init['unit'] = $ret_account_info['unit'];
		$init['remarks'] = $ret_account_info['data'];
		return $init;
	}


    /**
     * オーディエンス初期費用の個別編集
     *
     */
    private function _edit_audience_first_charge($init=array(), $init_plan_info=array())
    {
        $room_info = $init_plan_info['room_info'];
        $option_info = $init_plan_info['option_info'];
		$option_name="audience";
        $init['account_type_id'] = "audience_first";
        if (array_key_exists($this->option_keys[$option_name], $option_info) !== true) {
		}else{
            $service_option_info = $this->service_option_list[$this->option_keys[$option_name]];
            $init['price'] = $service_option_info['init_price']; // 単価
            $init['amount'] = $option_info[$this->option_keys['audience']]['init_count']; // 数量
            $init['charge'] = $init['price'] * $init['amount']; // 料金
        }
        $init['charge'] = 0; // 料金（初期費用請求時は、課金しない）
        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        $ret_account_info = $this->get_account_info($init);
        $init['account_type_name'] = $ret_account_info['name'];
        $init['remarks'] = $ret_account_info['data'];
        return $init;
    }


    /**
     * 合計金額の個別編集
     *
     */
    private function _edit_total($init=array(), $detail_info=array())
    {
        $total_charge = 0;
        foreach ($detail_info as $key => $val) {
            $total_charge += $val['charge'];
        }
        $init['account_type_id'] = "_total_";
        $init['price'] = 0; // 単価
        $init['amount'] = 0; // 数量
        $init['charge'] = $total_charge; // 料金
        $this->_total_charge += $total_charge;

        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        $ret_account_info = $this->get_account_info($init);
        $init['account_type_name'] = $ret_account_info['name'];
        $init['remarks'] = $ret_account_info['data'];

        return $init;
    }


    /**
     * 請求科目摘要欄個別編集処理（ミーティング用）
     */
    private function get_account_info($val = array(), $plan_info=array(), $invoice_type="normal")
    {
        $ret = array();
        $_name = "";
        $_unit = "";
        $_data = "";
        switch ($val['account_type_id']) {
            case "init" :
                $_name = "初期費用";
                $_unit = null;
                $_data = null;
                break;
            case "basic" :
                $_name = "月額基本料";
                $_unit = null;
                if ($plan_info['room_plan_yearly'] == "1") {
                    $limit_year = date("Y", strtotime($plan_info['room_plan_yearly_starttime']));
                    $limit_month = date("m", strtotime($plan_info['room_plan_yearly_starttime']));
                    $yearly_limit = date("Y-m-d",strtotime("-1 day", mktime(0, 0, 0, $limit_month, 1, $limit_year + 1)));
                    $_data = "（ 年間契約　有効期限 : ". $yearly_limit . " ）";
                } else {
                    $_data = null;
                }
                break;
            case "extends" :
                $_name = "追加料金";
                $_unit = "分";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                break;
            case "mobile_basic" :
                $_name = "携帯テレビ電話月額基本料金";
                $_unit = "回線";
				if (array_key_exists($plan_info['service_key'], $this->more_than_standard_plan_keys) === false) {
	                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "回線 ）";
				} else { 
	                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "回線　内1回線　プランに付属 ）";
				}
                break;
            case "mobile_extends_normal" :
                $_name = "携帯テレビ電話追加料金 \n（平日8時から19時）";
                $_unit = "分";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                break;
            case "mobile_extends_special" :
                $_name = "携帯テレビ電話追加料金 \n（19時から翌朝8時及び土、日曜祝日）";
                $_unit = "分";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                break;
            case "h323_basic" :
                $_name = "Ｈ.３２３通信月額基本料金";
                $_unit = "回線";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "回線 ）";
                break;
            case "h323_extends" :
                $_name = "Ｈ.３２３通信追加料金";
                $_unit = "分";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                break;
            case "ssl_basic" :
                $_name = "ＳＳＬ通信";
                $_unit = null;
                $_data = null;
                break;
            case "hispec_basic" :
                $_name = "高画質モード基本料金";
                $_unit = null;
				if ($plan_info['service_key'] != $this->hq_plan_key) {
	                $_data = null;
				} else { 
	                $_data = "（ プラン付属 ）";
				}
                break;
            case "hispec_extends" :
                $_name = "高画質モード追加料金";
                $_unit = "分";
				if ($plan_info['service_key'] != $this->hq_plan_key) {
	                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
				} else { 
	                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）（ プラン付属 ）";
				}
                break;
            case "sharing_basic" :
                $_name = "デスクトップシェアリング";
                $_unit = null;
				if (array_key_exists($plan_info['service_key'], $this->more_than_standard_plan_keys) === false) {
	                $_data = null;
				} else { 
	                $_data = "（ プラン付属 ）";
				}
                break;
            case "hdd_basic" :
                $_name = "追加録画容量";
                $_unit = "ＧＢ";
                $_data = "（ 追加容量 ". number_format($val['amount']) . "ＧＢ ）";
                break;
            case "compact_basic" :
                $_name = "コンパクト";
                $_unit = null;
                $_data = null;
                break;
            case "audience_first" :
                $_name = "オーディエンス機能初期費用";
                $_unit = "地点";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "地点 ）";
                break;
            case "audience_basic" :
                $_name = "オーディエンス機能基本料金";
                $_unit = "地点";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "地点 ）";
                break;
            case "audience_extends" :
                $_name = "オーディエンス機能利用料金";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                break;
            case "messenger_basic" :
                $_name = "メッセンジャー利用料金";
                $_unit = "名";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "名 ）";
                break;
            case "_total_" :
                if ("init" == $invoice_type) {
                    $_name = "会議室　小計";
                } else {
                    $_name = "小計";
                }
                $_unit = null;
                $_data = null;
                break;
        }
        $ret = array(
                    "name" => $_name,
                    "unit" => $_unit,
                    "data" => $_data,
                );
        // $this->logger->info(__FUNCTION__, __FILE__, __LINE__, $ret);
        return $ret;
    }


    /**
     * get_user_info
     *
     */
    private function get_user_info($user_id=null)
    {
        // 初期化
        $ret = array();
        $dsn = null;
        $data = null;
        $status = 0;
        //ユーザ取得
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if ( ! $user_info = $obj_MGMClass->getUserInfoById( $user_id ) ){
            $this->logger->info(__FUNCTION__." # User Info Not Found ... ", __FILE__, __LINE__, "param_user_id = " . $user_id);
        } else {
            $this->logger->info(__FUNCTION__." # UserInfo ... ", __FILE__, __LINE__, $user_info);
        }
        //サーバー取得
        if ( ! $server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] ) ){
            $this->logger->info(__FUNCTION__." # User Server Not Found ... ", __FILE__, __LINE__, "server_key = " . $user_info["server_key"]);
        } else {
            $this->logger->info(__FUNCTION__." # UserServerInfo ... ", __FILE__, __LINE__, $server_info);
            $dsn = $server_info['dsn'];
            // 該当ユーザーデータ取得
            $user_obj = new UserTable($dsn);
            $user_where = " `user_id` = '" . mysql_real_escape_string($user_id) . "'" .
                          " AND `invoice_flg` = 1 "; // 請求対象のアカウントのみとする。
            if ( ! $user_detail = $user_obj->getRow( $user_where ) ){
                $this->logger->info(__FUNCTION__." # User Detail Not Found ... ", __FILE__, __LINE__, "user_key = " . $user_id);
            } else {
                $this->logger->info(__FUNCTION__." # UserDetail ... ", __FILE__, __LINE__, $user_detail);
                $data = $user_detail;
                $status = 1;
            }
        }
        $ret['status'] = $status;
        $ret['dsn'] = $dsn;
        $ret['user_info'] = $data;
        return $ret;
    }


    /**
     * プラン基本料金日割り単価算出（削除予定）
     * TODO:削除
     */
    private function _get_daily_payment($base_price=0, $base_date=null, $start_date=null)
    {
        $this->logger->info(__FUNCTION__." # daily payment ... ", __FILE__, __LINE__, "param => [". $base_price . "][". $base_date . "][". $start_date . "]");
        $ret = 0;
        $start_date_wk = split("-", $start_date);
        $start_day = $start_date_wk[2];
        $base_date_wk = split("-", $base_date);
        $base_end_day = date("d",strtotime("-1 day", mktime(0, 0, 0, $base_date_wk[1] + 1, $base_date_wk[2], $base_date_wk[0])));
        $use_day_count = ( $base_end_day - $start_day + 1 );
        $this->logger->info(__FUNCTION__." # use_day_count ... ", __FILE__, __LINE__, $use_day_count ." = ( ". $base_end_day . " - " . $start_day . " + 1 )");
        $ret = round(($base_price / $base_end_day) * $use_day_count);
        return $ret;
    }


    /**
     * DataDBのDSN一覧取得
     *
     */
    private function _get_dsn()
    {
        static $server_list;
        if (!$server_list) {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        }
        if (!$server_list) {
            return null;
        } elseif (!is_array($server_list)) {
            return null;
        } else {
            if (array_key_exists("SERVER_LIST", $server_list)) {
                return $server_list["SERVER_LIST"];
            } else {
                return null;
            }
        }
    }


    /**
     * プラン価格一覧表取得
     * 
     */
    function getServiceList() 
    {
        $ret = null;
        $service_obj = new ServiceTable(N2MY_MDB_DSN);
    	// 有効なサービス一覧取得
        $where = " `service_charge` != 0 OR `service_additional_charge` != 0 ";
        $ret = $service_obj->getRowsAssoc($where, null, null, null, null, "service_key");
        return $ret;
    }


    /**
     * オプション価格一覧表取得
     * 
     */
    function getOptionPriceList($country_key=null) 
    {
        $ret = null;
        $list = null;
        $service_option_obj = new ServiceOptionTable(N2MY_MDB_DSN);
    	// 有効なオプション一覧取得
        $where = null;
        if ($country_key) {
            $where = " country_key = '" . addslashes($country_key) . "'";
        }
        $list = $service_option_obj->getRowsAssoc($where);
        if(DB::isError($list)) {
            $this->logger->info(__FUNCTION__." # Error Is ... ",__FILE__,__LINE__, $list->getMessage());
            return $list;
        }
        $ret = array();
        foreach($list as $key => $val) {
            $ret[$val['service_option_key']]['name'] = $val['service_option_name'];
            $ret[$val['service_option_key']]['price'] = $val['service_option_price'];
            $ret[$val['service_option_key']]['init_price'] = $val['service_option_init_price'];
            $ret[$val['service_option_key']]['running_price'] = $val['service_option_running_price'];
        }
        return $ret;
    }


}

$main = new AppInitInvoice();
$main->execute();
?>