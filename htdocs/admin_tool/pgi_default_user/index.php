<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/N2MY_IvesClient.class.php");
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');

class AppPgiDefaultUser extends AppFrame {

    function init() {
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->_sess_page  = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->_name_space = md5(__FILE__);
    }

    /**
     *   登録検証失敗するとき、エラーcallback
     */
    public function error($error_msg)
    {
        if ($error_msg == "Not Authed") {
            session_destroy();
            header("Location: /admin_tool/index.php");
            return;
        }
    }

    public function auth()
    {
        if ($_SESSION["_authsession"]["data"]["status"] == 0 || $_SESSION["_authsession"]["data"]["authority"] != 2) {
            return "Not Authed";
        }
    }

    function default_view() {
        $this->display_form();
    }

    function display_form($message = null) {
        if($message){
           $this->template->assign("message", $message);
        }
        $this->display( "admin_tool/pgi_default_user/index.t.html" );
    }

    function action_compleate(){

        $upload_file = $_FILES["csv"];
        $room_use_teleconf = $this->request->get("use_teleconf");
        $add_teleconf_option = $this->request->get("add_teleconf_option");

        $file_name = $upload_file["tmp_name"];

        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open($file_name, "r");
        //取り込んだファイルのヘッダチェック
        $head_ex = "user_id,meeting_version,promotion_flg";
        $h_ar = implode(",", $csv->getHeader());
        if($head_ex !== $h_ar) {
            $message = "フォーマットエラー";
            return $this->display_form($message);
        }

        $user_db = new N2MY_DB($this->get_dsn(), "user");
        $room_db = new N2MY_DB($this->get_dsn(), "room");
        $room_data = array(
                "use_teleconf" => $room_use_teleconf,
                "use_pgi_dialin" => $room_use_teleconf,
                "use_pgi_dialin_free" => $room_use_teleconf,
                "use_pgi_dialout" => $room_use_teleconf
        );
        $user_data = array();
        $error_user_data = array();
        $add_teleconf_option_room_list = array();
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable( $this->get_dsn() );
        // CSVを読み込み
        while($row = $csv->getNext(true)) {
            // promotion_flgだけチェック
            if($row["promotion_flg"] === "1" || $row["promotion_flg"] === "0" ){

                $where = "user_id = '" .$row["user_id"]."'";
                $user_data = array(
                        "promotion_flg" => $row["promotion_flg"]
                        );
                $user_key = $user_db->getRow($where,"user_key");
                if(!$user_key){
                    // エラーリスト
                    $error_user_data[] = $row;
                }else{
                    $row["user_key"] = $user_key["user_key"];
                    // 成功リスト
                    $succses_data[] = $row;
                    $res = $user_db->update($user_data, $where);
                    $room_where = "user_key = " .$user_key["user_key"];
                    if($room_use_teleconf != 2){
                        $room_db->update($room_data, $room_where);
                    }
                    // 電話連携オプションを付ける
                    if($add_teleconf_option){
                        $room_where = "room_status = 1 AND user_key = " .$user_key["user_key"];
                        $user_room_list = $room_db->getRowsAssoc($room_where, array(), null, 0,"room_key , user_key , room_name");
                        foreach($user_room_list as $room_info){
                            $room_key = $room_info["room_key"];
                            $pgi_form = $this->_getDefaultPGI();
                            $has_teleconference  = $ordered_service_option->numRows("(ordered_service_option_status = 1 OR ordered_service_option_status = 2) AND service_option_key = 23 AND room_key = '" . $room_key . "'");
                            if(!$has_teleconference){
                                if($this->action_pgi_setting($pgi_form,$room_key,1)){
                                    $option_data = array(
                                            "room_key" => $room_key,
                                            "service_option_key" => 23,
                                            "ordered_service_option_status" => 1,
                                            "ordered_service_option_starttime" => date("Y-m-d 00:00:00"),
                                            "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
                                            "ordered_service_option_deletetime" => "0000-00-00 00:00:00"
                                    );
                                    $ordered_service_option->add($option_data);
                                }

                                $add_teleconf_option_room_list["add_option"][] = $room_info;

                            }else{
                                $add_teleconf_option_room_list["already_option"][] = $room_info;
                            }

                        }

                    }
                }
            }else{
                $error_user_data[] = $row;
            }
        }
        $csv->close();
        $this->template->assign("error_user_data", $error_user_data);
        $this->logger2->info($add_teleconf_option ,"add_teleconf_option");
        $this->logger2->info($add_teleconf_option_room_list);
        $this->logger2->info($_SESSION["_authsession"]["data"]["name"],"staff_name");
        $this->logger2->info($room_use_teleconf,"room_use_teleconf");
        $this->logger2->info($succses_data,"succses_data");
        $this->logger2->info($error_user_data , "error_user_data");
// var_dump($error_user_data);
        return $this->display_form("変更処理完了");
    }

    // 下記PGI周りの関数を移植

    private function _getDefaultPGI(){

        require_once "classes/pgi/PGiSystem.class.php";
        $system  = PGiSystem::findBySystemKey("VCUBE HYBRID PRODUCTION");
        $pgi_form["pgi_start_date"] =date("Y/m");
        $pgi_form["system_key"] = "VCUBE HYBRID PRODUCTION";
        $pgi_form["company_id_type"] = "default";
        $pgi_form["company_id"] = "282363";
        $pgi_form["dialin_local"] ="";
        $pgi_form["dialin_free"] ="";
        $pgi_form["dialin_mobile"] ="";
        $pgi_form["dialin_area1"] ="";
        $pgi_form["dialin_area2"] ="";
        $pgi_form["dialin_area3"] ="";
        $pgi_form["dialin_area4"] ="";
        $pgi_form["dialin_area5"] ="";
        $pgi_form["dialout"] ="";
        $pgi_form["dialout_mobile"]="";
        $rates =  (array)($system->rates);
        foreach($rates as $key => $rate){
            $pgi_form[$key] = $rate;
        }
//var_dump($pgi_form);
        return $pgi_form;
    }

    function action_pgi_setting($form_data = null , $room_key = null , $no_page_move = null) {

        require_once("classes/dbi/pgi_setting.dbi.php");
        require_once("classes/dbi/pgi_rate.dbi.php");
        $pgiSettingTable = new PGiSettingTable($this->get_dsn());
        $pgiRateTable    = new PGiRateTable($this->get_dsn());
        if($room_key == null){
            return 0;
        }
        if (!$room_key) {
            return 0;
        }

        if($form_data == null){
            $form_data = $this->request->get("add_form");
        }
        if (!$form_data) {
            return 0;
        }

        try {
            $pgiSettings = $pgiSettingTable->findByRoomKey($room_key);
        } catch (Exception $e) {
            die('error : '.__FILE__.__LINE__);
        }
        $validated = $this->validatePGiSetting($form_data, $room_key);
        if ($validated['message']) {
            print($validated['message']);
            return 0;
        }

        $validated['room_key'] = $room_key;

        $this->request->set("room_key", $room_key);


        $pgi_setting_data = array('room_key'   => $room_key,
                'startdate'  => $validated['data']['startdate'],
                'system_key' => $validated['data']['system_key'],
                'company_id' => $validated['data']['company_id'],
                'admin_client_id' => $validated['data']['admin_client_id'],
                'admin_client_pw' => $validated['data']['admin_client_pw'],);
        try {
            $pgi_setting_key = $pgiSettingTable->add($pgi_setting_data);
        } catch (Exception $e) {
            $this->exitWithError($e->getMessage());
        }

        require_once('classes/pgi/PGiSystem.class.php');
        $pgi_system = PGiSystem::findBySystemKey($pgi_setting_data['system_key']);

        if ($pgi_system->rates) { // 設定にデフォルト料金が登録されているsystemはpgi_rateに料金を保存
            require_once("classes/pgi/PGiRate.class.php");
            $this->logger2->debug($validated['data']);
            $rate_config = PGiRate::findGmConfigAll();
            foreach ($rate_config as $pgi_rate) {
                $pgi_rate_data = array('pgi_setting_key' => $pgi_setting_key,
                        'rate_type'       => (string)$pgi_rate->type,
                        'rate'            => $validated['data'][(string)$pgi_rate->type]);
                try {
                    $pgiRateTable->add($pgi_rate_data);
                } catch (Exception $e) {
                    $this->exitWithError($e->getMessage());
                }
            }
        }
        //操作ログ登録
        $operation_data = array ("staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => "pgi_setting, pgi_rate",
                "keyword"            => $room_key,
                "info"               => serialize($pgi_setting_data['system_key']),
                "operation_datetime" => date("Y-m-d H:i:s"),
        );

        $this->add_operation_log($operation_data);
        return 1;
    }

    private function validatePGiSetting($form, $room_key)
    {
        $res = array('data'    => $form,
                'message' => '');

        if (!$form["pgi_start_date"]) {
            $res['message'] .= "Please input the start time.<br />";
        } else {
            list($y, $m) = explode('/',$form["pgi_start_date"]);
            if (!checkdate($m, 1, $y)) {
                $res['message'] .= "Start Time is not correct.<br />";
            } else {
                $startdate = str_replace('/','-',$form["pgi_start_date"])."-01";
                // 過去分の設定はできません
                if ($startdate < date('Y-m').'-01') {
                    $res['message'].= "Can not be a past time.<br />";
                }
            }
        }
        $res['data']['startdate']  = $startdate;

        require_once('classes/pgi/PGiSystem.class.php');
        $pgiSystems    = PGiSystem::findConfigAll();
        $pgiSystemKeys = array();
        $selectedSystem         = null;
        foreach ($pgiSystems as $system) {
            if (@$form["system_key"] == (string)$system->key) {
                $selectedSystem = $system;
            }
        }
        if (!$form["system_key"]) {
            $res['message'] .= "Please input the system_key.<br />";
        } elseif (!$selectedSystem) {
            $res['message'] .= "The system_key is not correct.<br />";
        }

        if ($form["company_id_type"] == 'default') {
            $form["company_id"]        = $selectedSystem->defaultCompanyID;
            $res['data']['company_id'] = $selectedSystem->defaultCompanyID;
        }

        if (!$form["company_id"]) {
            $res['message'] .= "Please input the company_id.<br />";
        } else {
            if ($selectedSystem && $selectedSystem->rates) {
                require_once("classes/pgi/PGiRate.class.php");
                // validate rate's values
                $rate_config = PGiRate::findGmConfigAll();

                foreach ($rate_config as $rate){
                    $col  = (string)$rate->type;
                    $name = (string)$rate->description;
                    if (!$form[$col]) {
                        $res['message'] .= "Please input the ".$name.".<br />";
                    } elseif (is_int($form[$col])) {
                        $res['message'] .= $name." should be a number.<br />";
                    } elseif ($form[$col] <= 0) {
                        $res['message'] .= $name."should be larger than 0.<br />";
                    }
                }
            }
        }

        require_once("classes/dbi/pgi_setting.dbi.php");
        $pgi_setting_table = new PGiSettingTable($this->get_dsn());
        try {
            $pgi_settings  = $pgi_setting_table->findByRoomKey($room_key);
        } catch(Exception $e) {
            die('error'.$e->getMessage());
        }
        if (0 == count($pgi_settings)) {
            return $res;
        }

        foreach ($pgi_settings as $setting) {
            if ($startdate == $setting['startdate']) {
                $res['message'] .= $startdate."have already been set.(auto generate when meeting start）<br />";
                return $res;
            }
        }

        return $res;
    }

    private function exitWithError($msg)
    {
        $b = debug_backtrace();

        $from = sprintf("called %s+%s : %s",$b[0]['file'], $b[0]['line'], $b[1]['function']);
        $this->logger->warn($msg.$from);

        exit($msg.$from);
    }

}
$main = new AppPgiDefaultUser();
$main->execute();
