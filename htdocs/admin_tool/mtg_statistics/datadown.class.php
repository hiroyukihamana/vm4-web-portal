<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Maple - PHP Web Application Framework
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @package     Maple.examples
 * @author      TAKAHASHI Kunihiko <kunit@kunit.jp>
 * @copyright   2004-2006 The Maple Project
 * @license     http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version     CVS: $Id: Person.class.php,v 1.2 2006/02/11 19:18:22 kunit Exp $
 */

/**
 *
 * @package     Maple.examples
 * @author      TAKAHASHI Kunihiko <kunit@kunit.jp>
 * @copyright   2004-2006 The Maple Project
 * @license     http://www.php.net/license/3_0.txt  PHP License 3.0
 * @access      public
 * @since       3.1.0
 */

//require_once('Common.class.php');

//class Lib_Datadown extends Lib_Common
class Datadown
{

    //------------- Constructor ------------------------------------------------
    // {{{ -- Datadown()
    /**
     * コンストラクタ
     *
     * @access public
     * @param 
     * @return_     
     */
    function Datadown()
    {

    }

    // }}}
    //------------- public -----------------------------------------------------
    // {{{ -- csvdown()
    /**
     *　CSVをOUTPUTする  
     *
     * @access public
     * @param string $data  出力内容
     * @param string $filename ダウンロードさせるファイル名
     * @param string $separator セパレータ
     * @param string $eol 改行コード CR or LF or CRLF
     * @return_ string $encode 出力エンコード 種類はmb_convert_encodingに準ずる
     */
    function csvdown($data=null,$filename=null,$separator = ",",$eol = "CRLF",$encode = "UTF-8")
    {
        $err = null;

        //入力Check
        $err = is_null($err)?is_null($data)?true:null:$err;
        $err = is_null($err)?is_null($filename)?true:null:$err;


        //改行コード
        $eol = strtoupper($eol);
        switch ($eol){
            case "CR":
            $eol = "\r";
            break;
            case "LF":
            $eol = "\n";
            break;
            case "CRLF":
            $eol = "\r\n";
            break;
            default:
            $err = is_null($err)?true:$err;
        }

        $content_length = 0;

        //データ作成
        $alldata = null;
        foreach ($data as $rows){
            $rowdata = null;
            foreach ($rows as $row){
                $rowdata .= "\"$row\"" . $separator;
            }
            $rowdata = rtrim($rowdata,$separator) . $eol;
            $content_length += strlen($rowdata);
            $alldata .= $rowdata;
        }
        //$alldata =  mb_convert_encoding($alldata,$encode);
        mb_language("Japanese");
        //$alldata =  mb_convert_encoding($alldata,$encode,"auto");
        //ダウンロード用のHTTPヘッダ送信x
        header("Cache-Control: public");
        header("Pragma: public");
      
        header("Content-Disposition: inline; filename=\"".$filename."\"");
        header("Content-Length: ".$content_length);
        header("Content-Type: application/octet-stream-dummy");
      //header("Content-type: text/plain;");

        echo $alldata;

        exit;

    }
    // }}}



}
?>
