<?php
$start = time();
echo "start => ".time()."<br />";
require_once( "Statistics.class.php" );

$st = new Statistics(Statistics::DETAIL1);

$search = null;

if($_SERVER["REQUEST_METHOD"] == "GET"){
  $search = $_GET;
}

if(count($search) != 0){
  $search = $st->checkData($search);
}else{
  $rows = array();
}

if(@$search['csv'] == 1){
  $st->getCSV($search);
}

$headersCnf = $st->getHeadersConfig();

$result = $st->getDetail($search);
$fmsServers = $st->getFmsInfo();

if($result["status"] === false){
  echo $result["msg"];
  exit;
}


$total    = $result["count"];
$pager    = $result["pager"];
$rows     = $result["rows"];
$columns  = $result["columns"];



$query_string = urldecode($_SERVER["QUERY_STRING"]);



?>

<html>
  <head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    <link rel="stylesheet" href="./css/api.css" type="text/css">
    <link rel="stylesheet" href="./css/calendar.css" type="text/css">
    <script type="text/javascript" src="./js/mtg.js"></script>
    <script type="text/javascript" src="./js/prototype/prototype.js" charset="UTF-8"></script>
    <script type="text/javascript" src="./js/protocalendar.js" charset="UTF-8"></script>
    <script type="text/javascript" src="./js/lang_ja.js" charset="UTF-8"></script>
    
  </head>
  <body>
  
   <table class="data">
      <tr>
        <th>fms</th>
        <td>
          <?php
            foreach($fmsServers as $i => $fmsServer){
              $chk = "";
              if(is_array($search["fms"]) && in_array($fmsServer["server_key"], $search["fms"])){
                echo sprintf('%s<br />', $fmsServer["server_address"]);
              }elseif(!is_array($search["fms"])){
                echo sprintf('%s<br />', $fmsServer["server_address"]);
              }
            }
          ?>
        </td>
      </tr>
      <tr>
        <th>期間</th>
        <td>
            <?php echo @$search['from'] ? @$search['from']: date('Y-m-d', strtotime('-30 day',time())); ?>&nbsp;<?php echo @$search['from'] ? @$search['from_h']: 0; ?>：<?php echo @$search['from'] ? @$search['from_m']: 0; ?>
            &nbsp;～&nbsp;
            <?php echo @$search['to'] ? @$search['to']: date('Y-m-d', time()); ?>&nbsp;<?php echo @$search['to'] ? @$search['to_h']: 0; ?>：<?php echo @$search['to'] ? @$search['to_m']: 0; ?>
          </td>
      </tr>
      <tr>
        <th>ユーザーアカウント</th>
        <td>
          <?php echo @$search['user'] ?>&nbsp;
        </td>
      </tr>
      <tr>
        <th>再入室</th>
        <td>
          <?php echo (empty($search['reStart']) || @$search['reStart'] == "0" )? "すべて": ""; ?>
          <?php echo (@$search['reStart'] == "1")? "再入室のみ": ""; ?>
        </td>
      </tr>
      <tr>
        <th>そのた</th>
        <td>
          <?php echo $headersCnf[$search["header"]]["view"] ?><br />
          <?php echo $headersCnf[$search["rHeader"]]["view"] ?>
        </td>
      </tr>
      <tr>
        <th>CSV Download</th>
        <td>
          <?php printf("<a href='./csv.php?%s&csv=1' target='_blank'>Download</a>&nbsp;", $query_string); ?>
        </td>
      </tr>
      <?php if(in_array(@$search["header"], array(25,26)) && $search["pf"] != 1): ?>
      <tr>
        <th>対象ping_failed</th>
        <td>
          <?php printf("<a href='./detail.php?%s&pf=1' target='_blank'>表示</a>&nbsp;", $query_string); ?>
        </td>
      </tr>
      <?php endif; ?>
    </table?>
    
  </form>
  
  <br /><br />
  
  <table class="data">
  
  <br />
  all: <?php echo $total; ?>
  【
  <?php
    foreach($pager as $page){
     printf("<a href='./detail.php?%s&p=%s'>%s</a>&nbsp;", $query_string, $page, $page);
    }
  ?>
  】
  <table class="data">

    <tr>
    <?php
      foreach($columns as $colum){
           printf( "<th>%s</th>", $colum);
       }
    ?>
    
    </tr>

    <?php
      $total  = array();
      foreach( $rows as $row) {
      echo "<tr>";
        foreach($row as $column){
          printf( "<td>%s&nbsp;</td>", $column );
        }
      echo "</tr>";
      }
    ?>

  </table>
  <br /><br />
  
  </body>
</html>
<?php
  $end = time();
  echo "end => ", $end."<br />";
  
  $aaa  = $end - $start;
  
  echo "time => ".$aaa."<br />";
  
?>


