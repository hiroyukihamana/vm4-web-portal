<?php

//$headers = array( 
//  0,  //remove
//  1,  //再入室数
//  2,  //RTMP
//  3,  //RTMPS
//  4,  //RTMPT
//  5,  //RTMPE
//  6,  //RTMPTE
//  7,  //-1  (接続時間
//  8,  //1-5
//  9,  //5-15
//  10, //15-30
//  11, //30-60
//  12, //60-120
//  13  //120-
//  14, //ping_failed (回数
//  15, //fms_bytes_in_avg    (受信した合計バイト数平均
//  16, //fms_bytes_out_avg   (送信した合計バイト数平均
//  17, //fms_msg_in_avg      (受信RTMPメッセージ合計数平均
//  18, //fms_msg_out_avg     (送信RTMPメッセージ合計数平均
//  19, //fms_msg_dropped_avg (欠落RTMPメッセージ合計数平均
//  20, //fms_ping_rtt_avg    (pingメッセージに応答するまでの時間平均
//  21, //fms_dropped_audio_msgs_avg  (欠落オーディオメッセージ数平均
//  22, //fms_doropped_video_msgs_avg (欠落ビデオメッセージ数平均
//  23, //fms_dropped_audio_bytes_avg (欠落オーディオメッセージサイズ平均
//  24, //fms_doropped_video_bytes_avg(欠落ビデオメッセージサイズ平均
//  25, //ping_failed が発生しなかった participant の数
//  26, //ping_failed が発生した       participant の数
//  27, // 26/25


//再入室数
//  28, // 初回入室
//  29, // ページ再読み込み
//  30, // データセンタ切り替え
//  31, // プロトコル変更
//  32, // ping_failed


//  );


require_once( "DB.class.php");
require_once( "spyc/spyc.php" );

class Statistics
{
  const MATRIX1 = 1;
  const MATRIX2 = 2;
  
  const DETAIL1 = 3;
  
  private $matrixType;
  private $db;
  private $config;
  private $headersCnf;
  
  public function __construct( $matrixType = Statistics::MATRIX1 )
  {
    define( 'CONFIG_YAML', realpath(dirname(__FILE__))."/config.yml" );
    $this->config = Spyc::YAMLLoad( CONFIG_YAML );
    $this->headersCnf = $this->config["header"];
    $this->matrixType = $matrixType;
    
    $n2myDataConfig = $this->config["n2my_data"];
    $this->db = new DB($n2myDataConfig["host"], $n2myDataConfig["account"], $n2myDataConfig["passwd"], $n2myDataConfig["dbname"]);
    
  }
  
  public function getHeadersConfig()
  {
    return $this->headersCnf;
  }
  
  public function getHeaders()
  {
    $hearders = array();

    switch ( $this->matrixType ) {
      case Statistics::MATRIX1:
        $headers = array( 
            28,  //再入室数初回入室
            29,  //再入室数ページリロード
            30,  //再入室数データセンタ切り替え
            31,  //再入室数プロトコル変更
            32,  //再入室数pinfailed
            
            25, //ping_failed が発生しなかった participant の数
            26, //ping_failed が発生した       participant の数
            //27, // 26/25
            2,  //RTMP
            3,  //RTMPS
            4,  //RTMPT
            5,  //RTMPE
            6,  //RTMPTE
            0,  //入退室数
            14, //ping_failed
            15, //fms_bytes_in_avg
            16, //fms_bytes_out_avg
            17, //fms_msg_out_avg
            18, //fms_msg_dropped_avg
            19, //fms_ping_rtt_avg
            20, //fms_dropped_audio_msgs_avg
            21, //fms_dropped_audio_msgs_avg
            22, //fms_dropped_audio_bytes_avg
            23, //fms_doropped_video_bytes_avg
            24, //fms_doropped_video_bytes_avg

          );
          
        break;
      case Statistics::MATRIX2:
        $headers = array( 28,29,30,31,32, 25,26, 0, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24);
        
        break;
        
    }

    return $headers;
  }
  
  public function getRowHeaders()
  {
    $hearders = array();

    switch ( $this->matrixType ) {
      case Statistics::MATRIX1:
        $headers = array( 
            7,  //-1
            8,  //1-5
            9,  //5-15
            10, //15-30
            11, //30-60
            12, //60-120
            13  //120-
          );
        break;
      case Statistics::MATRIX2:
          //$headers = array( 入退室数,再入室数,RTMP,RTMPS,RTMPT,RTMPE,RTMPTE);
          $headers = array( 
              //0,  //入退質数
              //1,  //再入室数
              2,  //RTMP
              3,  //RTMPS
              4,  //RTMPT
              5,  //RTMPE
              6  //RTMPTE
          );
          break;
    }

    return $headers;
  }
  

  public function getDataArray($search = null)
  {
    switch( $this->matrixType ) {
      case Statistics::MATRIX1:
        //$rows = array_fill( 0, count( $this->getHeaders() ), "1" );
        $rows = $this->getPartisipant($search);
        break;
      case Statistics::MATRIX2:
        $rows = $this->getPartisipant($search);
        break;
    }

    return $rows;
  }
  

  public function getPartisipant($search)
  {
    
    
    $return = array();
    foreach($this->getRowHeaders() as $header_id){
    
      //header_id =  0 ～ 6
      $result = $this->db->getPartisipant($header_id, $search);
      while ( $row = mysql_fetch_assoc($result)) {
        $tmp = new MatrixRow( $this->matrixType );
        $tmp->enter  = $row["enter"];
        //$tmp->reStart = $row["reStart"];
        $tmp->rtmp   = $row["rtmp"];
        $tmp->rtmps   = $row["rtmps"];
        $tmp->rtmpt  = $row["rtmpt"];
        $tmp->rtmpe  = $row["rtmpe"];
        $tmp->rtmpte = $row["rtmpte"];
        
        $tmp->time1  = $row["time1"];
        $tmp->time2  = $row["time2"];
        $tmp->time3  = $row["time3"];
        $tmp->time4  = $row["time4"];
        $tmp->time5  = $row["time5"];
        $tmp->time6  = $row["time6"];
        $tmp->time7  = $row["time7"];
        
        //$tmp->url = "http://";
        $return[$header_id] = $tmp;
        
      }
      
      
      
      //header_id =  7 ～ 24
      $result = $this->db->getPingFailed($header_id, $search);
      
      while ( $row = mysql_fetch_assoc($result)) {
        $tmp = $return[$header_id];
        $tmp->normal_restart_cnt            = $row["normal_restart_cnt"];
        $tmp->reload_cnt                    = $row["reload_cnt"];
        $tmp->dc_restart_cnt                = $row["dc_restart_cnt"];
        $tmp->protocol_restart_cnt          = $row["protocol_restart_cnt"];
        $tmp->ping_failed_restart_cnt       = $row["ping_failed_restart_cnt"];
        $tmp->pfailed                       = $row["pfailed"];
        
        $tmp->fms_bytes_in_avg              = $row["fms_bytes_in_avg"];
        $tmp->fms_bytes_out_avg             = $row["fms_bytes_out_avg"];
        $tmp->fms_msg_in_avg                = $row["fms_msg_in_avg"];
        $tmp->fms_msg_out_avg               = $row["fms_msg_out_avg"];
        $tmp->fms_msg_drop_avg              = $row["fms_msg_dropped_avg"];
        $tmp->fms_ping_rtt_avg              = $row["fms_ping_rtt_avg"];
        
        $tmp->fms_dropped_audio_msgs_avg    = $row["fms_dropped_audio_msgs_avg"];
        $tmp->fms_doropped_video_msgs_avg   = $row["fms_doropped_video_msgs_avg"];
        $tmp->fms_dropped_audio_bytes_avg   = $row["fms_dropped_audio_bytes_avg"];
        $tmp->fms_doropped_video_bytes_avg  = $row["fms_doropped_video_bytes_avg"];
        $tmp->fms_msg_dropped_avg           = $row["fms_msg_dropped_avg"];
        
        //$tmp->url = "http://";
        $return[$header_id] = $tmp;
      }
      
      
      //header_id =  25, 26, 27
      $result = $this->db->getFailedCount($header_id, $search);
      
      while ( $row = mysql_fetch_assoc($result)) {
        $tmp = $return[$header_id];
        $tmp->no_failed_cnt           = $row["noFailedCnt"];
        $tmp->failed_cnt              = $row["FailedCnt"];
        $tmp->failed_frequency       = @round( ($row["FailedCnt"] / $row["noFailedCnt"] ), 2 );
        
        $return[$header_id] = $tmp;
      }
      
    }
    
    return $return;
    
  }
  
  public function getDetail($search = null)
  {
    $select_headers = array($search["header"], $search["rHeader"]);
    $tmp = $this->db->getDetail($select_headers, $search);
    
    if(in_array(@$search["header"], array(25,26)) && @$search["pf"] == 1){
      if($tmp["count"] > 10000){
        $result["status"] = false;
        $result["msg"]    = "ごめんなさい。対象のParticipantが多すぎるため表示できませんでした。<br />。 対象を絞ってから試してみてください。";
        return $result;
      }
      $tmp = $this->db->getPfDetail($select_headers, $search, $tmp);
    }
    $columns = array();
    while ($i < mysql_num_fields($tmp["result"])) {
         $meta = mysql_fetch_field($tmp["result"]);
         $columns[] =  $meta->name;
         $i++;
    }
    $rows = array();
    if(@$search["csv"] == 1){
      while ( $row = mysql_fetch_row($tmp["result"])) {
        $rows[] = $row;
      }
    }else{
      $p = $search["p"] ? (int)$search["p"]: 1;
      $min =  1 + (50*($p-1));
      $max = 50 + (50*($p-1));
      $cnt = 1;
      while ( $row = mysql_fetch_row($tmp["result"])) {
        if($cnt >= $min && $cnt <= $max){
          $rows[] = $row;
        }elseif($cnt >= $max){
          break;
        }
        
        $cnt++;
      }
    }
    
    $page = array();
    for($i=1; $i <= ceil($tmp["count"] / 50); $i++){
      $page[] = $i;
    }
    
    $result["status"] = ture;
    $result["columns"] = $columns;
    $result["rows"] = $rows;
    $result["count"] = $tmp["count"];
    $result["pager"] = $page;
    
    return $result;
  }
  
  public function getFmsInfo(){
    $n2myDataConfig = $this->config["n2my_mgm"];
    $mgmDb = new DB($n2myDataConfig["host"], $n2myDataConfig["account"], $n2myDataConfig["passwd"], $n2myDataConfig["dbname"]);
    
    $result = $mgmDb->getFmsServer();
    $rtn = null;
    $fmsServer = array();
    while($row = mysql_fetch_assoc($result)){
      $fmsServer["server_key"]     = $row["server_key"];
      $fmsServer["server_address"] = $row["server_address"];
      
      $rtn[] = $fmsServer;
    }
    
    return $rtn;
    
  }
  
  
  
  public function checkData($search = array()){
    
    $from = strtotime("Y-m-d", strtotime("-1 MONTH" , time())); 
    $to = strtotime("Y-m-d", time());
    
    if(!empty($search["from"]) && !empty($search["to"])){
      $from = $search["from"];
      $to = $search["to"];
      if(strtotime($search["to"]) > strtotime("+1 month", strtotime($search["from"]))){ //一ヶ月を超えてたら
        $to = date( "Y-m-d", strtotime("+1 month", strtotime($search["from"])) );
      }
      
      
    }elseif(!empty($search["from"]) && empty($search["to"])){   //fromだけ
      $from = $search["from"];
      $to = date( "Y-m-d", strtotime("+1 month", strtotime($search["from"])) );
      
    }elseif(empty($search["from"]) && !empty($search["to"])){   //toだけ
      $from = date( "Y-m-d", strtotime("-1 month", strtotime($search["to"])) );
      $to = $search["to"];
    }
    
    $search["from"] = $from;
    $search["to"]   = $to;
    return $search;
  }
  
  function getCsv($search){
    require_once( "datadown.class.php" );
    
    $result = $this->getDetail($search);
    $fmsServers = $this->getFmsInfo();

    $total    = $result["count"];
    $pager    = $result["pager"];
    $rows     = $result["rows"];
    $columns  = $result["columns"];
    
    $data = array();
    
    $select_fms_servers = "";
    foreach($fmsServers as $i => $fmsServer){
      if(is_array($search["fms"]) && in_array($fmsServer["server_key"], $search["fms"])){
        $select_fms_servers .= $fmsServer["server_address"]."\n";
      }elseif(!is_array($search["fms"])){
        $select_fms_servers .= $fmsServer["server_address"]."\n";
      }
    }
    
    $restart = "";
    $restart = (empty($search['reStart']) || @$search['reStart'] == "0" )? "すべて": "再入室のみ";
    $from = sprintf("%s %s:%s", $search["from"], $search["from_h"], $search["from_m"]);
    
    $to   = sprintf("%s %s:%s", $search["to"], $search["to_h"], $search["to_m"]);
    
    
    
    $data[] = array("fms", $select_fms_servers);
    $data[] = array("期間", $from."～".$to);
    $data[] = array("ユーザーアカウント", $search["user"]);
    $data[] = array("再入室", $restart);
    
    
    $data[] = array();
    $data[] = $columns;
    
    foreach($rows as $i => $row){
      $data[] = $row;
    }
    
    $dataDown = new Datadown();
    $dataDown->csvdown($data, date("YmdHis").".csv", ",", "CRLF","SJIS");
    
  }
  
  

}

class MatrixRow
{
//  public $inTime;
//  public $outTime;
//  public $url = "";

  public $enter;
  
  public $rtmp;
  public $rtmps;
  public $rtmpt;
  public $rtmpe;
  public $rtmpte;
  
  public $time1;  //-1
  public $time2;  //1-5
  public $time3;  //5-15
  public $time4;  //15-30
  public $time5;  //30-60
  public $time6;  //60-120
  public $time7;  //120-
  
  public $pfailed;    //ping_failed.typeがping_failedの物の数
  
  public $fms_bytes_in_avg;    //fms_bytes_in   の平均
  public $fms_bytes_out_avg;   //fms_bytes_out  の平均
  public $fms_msg_in_avg;      //fms_msg_in     の平均
  public $fms_msg_out_avg;     //fms_msg_out    の平均
  public $fms_msg_dropped_avg; //fms_msg_droppedの平均
  public $fms_ping_rtt;
  
  public $fms_dropped_audio_msgs_avg;   //fms_dropped_audio_msgs  の平均
  public $fms_doropped_video_msgs_avg;  //fms_doropped_video_msgs の平均
  public $fms_dropped_audio_bytes_avg;  //fms_dropped_audio_bytes の平均
  public $fms_doropped_video_bytes_avg; //fms_doropped_video_bytesの平均
  
  public $no_failed_cnt;
  public $failed_cnt;
  public $failed_frequency;
  
  public $normal_restart_cnt;
  public $reload_cnt;
  public $dc_restart_cnt;
  public $protocol_restart_cnt;
  public $ping_failed_restart_cnt;

}

class MatrixHeader
{


}
