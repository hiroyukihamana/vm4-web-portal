<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Questionnaire.class.php");
require_once("classes/mgm/dbi/user_agent.dbi.php");
require_once("classes/dbi/answersheet.dbi.php");
require_once("classes/dbi/answer.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/answer_branch.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once("classes/core/dbi/Meeting.dbi.php");

class Answersheet extends AppFrame {
    var $_dsn = null;
    var $account_dsn = null;

    function init() {
        // DBサーバ情報取得
        if (file_exists(sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ))) {
            $server_list = parse_ini_file( sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
        } else {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        }
    }

    function default_view() {
      $this->action_answer_log_csv();
    }


    function action_answer_log_csv() {

        //1週間後削除
        $dir = dir(N2MY_DOCUMENT_ROOT."admin_tool/enqueteLog");
        $path = $dir->path;
        while (false !== ($filename = $dir->read())) {
            if($filename != '.' && $filename != '..') {
                $timestamp = filectime($path."/".$filename);
                if(mktime() > $timestamp + 60*60*24*7) {
                    unlink($path."/".$filename);
                }
            }
        }

        //前日
        $date = date("Y-m-d",strtotime("-1 day"));

        //入室者取得
        $obj_UserAgent = new MgmUserAgentTable($this->account_dsn);
        $where_agent = "create_datetime >= '".$date." 00:00:00'".
                 " AND create_datetime <= '".$date." 23:59:59'";
        $user_count = $obj_UserAgent->numRows($where_agent);
        if (DB::isError($user_count)) {
            $this->logger2->error($user_count->getUserInfo());
            return false;
        }

        //項目
        $menu = array("満足度,相手の映像が表示されない,自分の映像が表示されない,映像の品質が悪い,自分の音声が取得できない,相手の音声が聞こえない,音が頻繁に途切れる,音声が遅延する,音声がエコーする,音声がハウリングする,その他ご意見・ご要望,名前,フリガナ,会社名,メールアドレス,電話番号,ユーザーID,部屋名,回答日時,入室日時,会議名,参加者名");

        //質問
        $obj_Questionnaire = new N2MY_Questionnaire($this->get_dsn());
        $question_list = $obj_Questionnaire->getQuestionnaire("meeting_end", $this->request->get("lang","ja"));

        //出力したい答え。
        $obj_Answer = new AnswerTable($this->_dsn);
        $sql = "select answersheet.answersheet_id,answer.question_id,answer_branch.question_branch_id,answer.comment from answer
                LEFT JOIN answersheet as answersheet ON answersheet.answersheet_id = answer.answersheet_id
                LEFT JOIN answer_branch as answer_branch ON answer_branch.answer_id = answer.answer_id
                where answersheet.created_datetime >= '".$date." 00:00:00'".
                "AND answersheet.created_datetime <= '".$date." 23:59:59'".
                "GROUP BY answer.answer_id";

        $answer_result = $obj_Answer->_conn->query( $sql );
        if (DB::isError($answer_result)) {
            $this->logger2->error($answer_result->getUserInfo());
            return false;
        }

        $results = array();

        while ( $answer_row = $answer_result->fetchRow( DB_FETCHMODE_ASSOC ) ){

            //改行・半角スペース等の削除
            //テキストを""で囲う。
            $comment_e = str_replace("\n", "", $answer_row["comment"]);
            $comment_e = str_replace("\r", "|", $comment_e);
            $answer_row["comment"] = "\"". "$comment_e". "\"";

            foreach ($question_list as  $_q) {
                //選択肢の文言取得
                if (is_array($_q["branch"])) {
                    foreach($_q["branch"] as $_qb) {
                        if ($answer_row["question_branch_id"] == $_qb["question_branch_id"]){
                            $answer_row["branch_lang"] = $_qb["text"];
                        }
                    }
                }
                if ($_q["question_id"] == 1) {
                    if($_q["question_id"] == $answer_row["question_id"]) {
                        $results[$answer_row["answersheet_id"]][$answer_row["question_branch_id"]] = $answer_row["branch_lang"];
                    }
                } elseif ($_q["question_id"] == 2 || $_q["question_id"] == 3) {
                    foreach ($_q["branch"] as $_q2_3) {
                        if ($answer_row["question_branch_id"] == $_q2_3["question_branch_id"]) {
                                $results[$answer_row["answersheet_id"]][$_q2_3["question_branch_id"]] = "T";
                        } elseif (!$results[$answer_row["answersheet_id"]][$_q2_3["question_branch_id"]]) {
                                $results[$answer_row["answersheet_id"]][$_q2_3["question_branch_id"]] = "F";
                        }
                    }

                } elseif ($_q["question_id"] == 4) {
                    foreach ($_q["branch"] as $_q4) {
                        if ($answer_row["question_branch_id"] == $_q4["question_branch_id"]) {
                                $results[$answer_row["answersheet_id"]][$_q4["question_branch_id"]] = "T";
                        } elseif (!$results[$answer_row["answersheet_id"]][$_q4["question_branch_id"]]) {
                                $results[$answer_row["answersheet_id"]][$_q4["question_branch_id"]] = "F";
                        }
                    }
                    if (isset($answer_row["comment"]) && $answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"]) {
                        $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = "";
                    }


                } elseif ($_q["question_id"] == 5) {
                    foreach ($_q["branch"] as $_q5) {
                        if ($answer_row["question_branch_id"] == $_q5["question_branch_id"]) {
                                $results[$answer_row["answersheet_id"]][$_q5["question_branch_id"]] = "T";
                        } elseif (!$results[$answer_row["answersheet_id"]][$_q5["question_branch_id"]]) {
                                $results[$answer_row["answersheet_id"]][$_q5["question_branch_id"]] = "F";
                        }
                    }
                    if (isset($answer_row["comment"]) && $answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"]) {
                        $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = "";
                    }

                } elseif ($_q["question_id"] == 6) {
                    if($answer_row["question_id"] == 6) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["branch_lang"];
                    }
                    if(!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "F";
                    }

                } elseif ($_q["question_id"] == 7) {
                    if($answer_row["question_id"] == 7) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["branch_lang"];
                    }

                    if(!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "F";
                    }

                } elseif($_q["question_id"] == 8) {
                    if($answer_row["question_id"] ==8) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["branch_lang"];
                    }

                    if(!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "F";
                    }

                } elseif($_q["question_id"] == 9) {
                    if (isset($answer_row["comment"]) && $answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"]) {
                        $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = "";
                    }

                } elseif($_q["question_id"] == 10) {
                    if($answer_row["question_id"] == 10) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["branch_lang"];
                    }
                    if(!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "F";
                    }

                } elseif($_q["question_id"] == 11) {
                    if ($answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 12) {
                    if ($answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 13) {
                    if ($answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 14) {
                    if ($answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 15) {
                    if ($answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }
                } elseif($_q["question_id"] == 16) {
                	if($_q["question_id"] == $answer_row["question_id"]) {
                		$results[$answer_row["answersheet_id"]][$answer_row["question_branch_id"]] = $answer_row["branch_lang"];
                	}
                } elseif($_q["question_id"] == 17 || $_q["question_id"] == 18) {
                	foreach ($_q["branch"] as $_q2_3) {
                		if ($answer_row["question_branch_id"] == $_q2_3["question_branch_id"]) {
                			$results[$answer_row["answersheet_id"]][$_q2_3["question_branch_id"]] = "T";
                		} elseif (!$results[$answer_row["answersheet_id"]][$_q2_3["question_branch_id"]]) {
                			$results[$answer_row["answersheet_id"]][$_q2_3["question_branch_id"]] = "F";
                		}
                	}                    
                } elseif($_q["question_id"] == 19) {
                	if (isset($answer_row["comment"]) && $answer_row["question_id"] == $_q["question_id"]) {
                		$results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = $answer_row["comment"];
                	} elseif (!$results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"]) {
                		$results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = "";
                	}
                } elseif($_q["question_id"] == 20) {
                	if ($answer_row["question_id"] == $_q["question_id"]) {
                		$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                	} elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                		$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                	}
                } elseif($_q["question_id"] == 21) {
                	if ($answer_row["question_id"] == $_q["question_id"]) {
                		$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                	} elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                		$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                	}
                } elseif($_q["question_id"] == 22) {
                	if ($answer_row["question_id"] == $_q["question_id"]) {
                		$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                	} elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                		$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                	}
                } elseif($_q["question_id"] == 23) {
                	if ($answer_row["question_id"] == $_q["question_id"]) {
                		$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                	} elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                		$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                	}
                } elseif($_q["question_id"] == 24) {
                	if ($answer_row["question_id"] == $_q["question_id"]) {
                		$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                	} elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                		$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                	}
                }
            }

        }


        //アンケート項目以外の必要項目
        $obj_AnswerSheet = new AnswerSheetTable($this->_dsn);
        $sql2 = "select answersheet.answersheet_id,user.user_id,meeting.meeting_room_name,answersheet.created_datetime,participant.uptime_start,meeting.meeting_name,participant.participant_name from answersheet
                LEFT JOIN participant as participant ON participant.participant_key = answersheet.participant_key
                LEFT JOIN meeting as meeting ON meeting.meeting_key = participant.meeting_key
                LEFT JOIN user as user ON user.user_key = meeting.user_key
                where answersheet.created_datetime >= '".$date." 00:00:00'".
                "AND answersheet.created_datetime <= '".$date." 23:59:59'";

        $other_result = $obj_AnswerSheet->_conn->query( $sql2 );

        while($other_row = $other_result->fetchRow( DB_FETCHMODE_ASSOC )) {
            foreach($results as $keys => $anses) {
                if ($other_row["answersheet_id"] == $keys) {
                    $results[$keys]["user_id"] = $other_row["user_id"];
                    $results[$keys]["room_name"] = $other_row["meeting_room_name"];
                    $results[$keys]["create"] = $other_row["created_datetime"];
                    $results[$keys]["uptime"] = $other_row["uptime_start"];
                    $results[$keys]["m_name"] = $other_row["meeting_name"];
                    $results[$keys]["p_name"] = $other_row["participant_name"];
                }
            }
        }


        //まとめ
        $csv_data = "";
        $i = 0;
        foreach ($results as $result) {
            $csv_data .= join(",", $result)."\n";
            $i++;
        }

        //回答数
        $answer_count = $i;

        ob_start();
        $this->template->assign('user_count',$user_count);
        $this->template->assign('answer_count',$answer_count);
        $this->template->assign('start_date',$date);
        $this->template->assign('end_date',$date);
        $this->template->assign('menu',$menu);
        $this->template->assign('csv_data',$csv_data);
        $this->template->display('common/admin_tool/enquete_list/answersheet.t.csv');
        $contents = ob_get_contents();
        $contents = mb_convert_encoding($contents, 'SJIS', 'UTF-8');
        ob_clean();

        //書き込み
        $csv_file = N2MY_DOCUMENT_ROOT."admin_tool/enqueteLog/".$date.".csv";
        $fp = fopen($csv_file, 'w');
        fwrite($fp, $contents);
        fclose($fp);
        chmod($csv_file, 0777);

        //保存
        $file = file_get_contents($csv_file);

    }

}
$main = new Answersheet();
$main->execute();
