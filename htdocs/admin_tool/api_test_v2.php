<?php
header("Content-type: text/html; charset=UTF-8;");
require_once("config/config.inc.php");
require_once("config/service_api_config.php");
require_once("classes/N2MY_Api.class.php");
require_once("lib/pear/XML/Unserializer.php");
require_once("htdocs/admin_tool/dBug.php");
if (!$user_id || !$user_pw) {
    die("検証用のファイルがありません");
}

class AppApiDebug extends N2MY_Api
{
    function default_view() {

        // ユーザー
        global $user_id;
        global $user_pw;
        global $user_apw;
        // メンバー(APIのユーザーは必ず結びついている必要あり)
        global $member_id;
        global $member_pw;

        global $mail_to;
        global $mail_from;
        global $sender;

        global $timezone;

        global $api_key;
        global $secret;

        $host = "http://".$_SERVER["SERVER_NAME"];
        $api_version = "v2";
        $api = new API_Checker($host, $api_version);
        $api->_get_passage_time();

        /**
         * アカウント管理関連
         */

        $debug_flg     = ($_REQUEST["debug"])     ? 'checked="checked" ' : "";
        $error_flg     = ($_REQUEST["error"])     ? 'checked="checked" ' : "";
        $reference_flg = ($_REQUEST["reference"]) ? 'checked="checked" ' : "";

        $meeting_flg     = ($_REQUEST["check"]["meeting"])      ? 'checked="checked" ' : "";
        $reservation_flg = ($_REQUEST["check"]["reservation"])  ? 'checked="checked" ' : "";
        $meetinglog_flg  = ($_REQUEST["check"]["meetinglog"])   ? 'checked="checked" ' : "";
        $addressbook_flg = ($_REQUEST["check"]["addressbook"])  ? 'checked="checked" ' : "";
        $eco_flg         = ($_REQUEST["check"]["eco"])          ? 'checked="checked" ' : "";
        $document_flg    = ($_REQUEST["check"]["document_flg"]) ? 'checked="checked" ' : "";

        $admin_meetinglog_flg = ($_REQUEST["check"]["admin_meetinglog_flg"]) ? 'checked="checked" ' : "";
        $admin_room_flg       = ($_REQUEST["check"]["admin_room_flg"]) ? 'checked="checked" ' : "";
        $admin_member_flg     = ($_REQUEST["check"]["admin_member_flg"]) ? 'checked="checked" ' : "";

        if ($admin_meetinglog_flg || $admin_room_flg || $admin_member_flg) {
            $admin_flg = 1;
        }


        $filename = basename(__FILE__);
        print <<<EOM
<style type="text/css">
        body {
          font-family:Verdana, Arial, Helvetica, sans-serif; color:#000000; font-size:12px;
        }
</style>
<body>
<h2>APIテストケース検証ツール</h2>
<p>実行結果をクリックすることで、リクエスト・レスポンスの内容を参照できます</p>
<form action="$filename" method="post">
<input type="checkbox" name="debug" id="debug" value="1" $debug_flg /><label for="debug">デバッグ</label>
<input type="checkbox" name="error" id="error" value="1" $error_flg /><label for="error">エラーハンドリング</label>
<input type="checkbox" name="reference" id="reference" value="1" $reference_flg /><label for="reference">リファレンス更新（更新時は検証対象を全てチェックしてください）</label>
<fieldset>
<legend>ユーザーAPI</legend>
<input type="checkbox" name="check[meeting]" id="check[meeting]" value="1" $meeting_flg /><label for="check[meeting]">会議関連</label>
<input type="checkbox" name="check[document_flg]" id="check[document_flg]" value="1" $document_flg /><label for="check[document_flg]">資料変換関連</label>
<input type="checkbox" name="check[reservation]" id="check[reservation]" value="1" $reservation_flg /><label for="check[reservation]">予約関連</label>
<input type="checkbox" name="check[meetinglog]" id="check[meetinglog]" value="1" $meetinglog_flg /><label for="check[meetinglog]">会議記録関連</label>
<input type="checkbox" name="check[addressbook]" id="check[addressbook]" value="1" $addressbook_flg /><label for="check[addressbook]">アドレス帳関連</label>
<input type="checkbox" name="check[eco]" id="check[eco]" value="1" $eco_flg /><label for="check[eco]">ECOメーター関連</label>
</fieldset>
<fieldset>
<legend>管理者API</legend>
<input type="checkbox" name="check[admin_meetinglog_flg]" id="check[admin_meetinglog_flg]" value="1" $admin_meetinglog_flg /><label for="check[admin_meetinglog_flg]">会議記録管理関連</label>
<input type="checkbox" name="check[admin_room_flg]" id="check[admin_room_flg]" value="1" $admin_room_flg /><label for="check[admin_room_flg]">部屋設定関連</label>
<input type="checkbox" name="check[admin_member_flg]" id="check[admin_member_flg]" value="1" $admin_member_flg /><label for="check[admin_member_flg]">メンバー管理関連</label>
</fieldset>


<input type="submit" name="action_execute" value="実行" />
</form>

EOM;

        /*
        $url = 'http://meeting-trunk.kiyomizu.mac/api/user/meetinglog/?action_get_detail=&N2MY_SESSION=4075e4fae24fb26c92757d70ace430a5&meeting_session_id=1d6fb55c61e77e5fe7c81dade9e35fab&output_type=php';
        print $url;
        print_r (unserialize(file_get_contents($url)));
        die();
        */

        if ($_REQUEST["action_execute"]) {

            print "<ol>";
            /*******************************************
             * ユーザー用API
             *******************************************/

            $api->add_log("*ユーザー用API\n\n");
            /**************************
             * ユーザ認証関連
             **************************/

            $api_path = "user/";
            $api->add_log("**ユーザ認証関連 $api_path \n\n");
            $api->add_group($api_path, "ユーザ認証関連", "ログインなどが行えます。");

            ///*

                // ログイン
                $method_name  = 'action_login';
                $method_title = 'ログイン';
                $method_info  = 'ログイン処理を行いセッションキーを返す。以降のAPIは全てセッションキーを渡すことで実行可能となる。';
                $param = array(
                    "api_id"  => $api_key,
                    "id"       => $user_id,
                    "pw"   => hash('sha256', ($user_id.hash('sha256', $user_pw).$secret)),
                    "lang" => "ja",
                    "country" => "jp",
                    "timezone" => "9",
                    "enc" => "",
                    "output_type" => "php"
                    );
                $param_property = array(
                    "id"  => array(
                        'name'        => 'ユーザーID',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'ユーザーログイン用のID',
                        'example'     => 'api_test',
                        ),
                    "pw"  => array(
                        'name'        => 'パスワード',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'ユーザーログイン用のパスワード',
                        'example'     => '3s4G34rGw1',
                        ),
                    );
                $_user_info = $api->send($api_path, $method_name, $param, null, $method_title, $method_info, $param_property);
                $token = $_user_info["data"]["session"];
                $user_info = $_user_info["data"]["user_info"];

                /*
                // ログイン
                $method_name  = 'action_login';
                $method_title = 'ログイン';
                $method_info  = 'ログイン処理を行いセッションキーを返す。以降のAPIは全てセッションキーを渡すことで実行可能となる。';
                $param = array(
                    "id"  => $user_id,
                    "pw"  => $user_pw,
                    "lang" => "ja",
                    "country" => "jp",
                    "timezone" => "9",
                    "enc" => "",
                    "output_type" => "php"
                    );
                $param_property = array(
                    "id"  => array(
                        'name'        => 'ユーザーID',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'ユーザーログイン用のID',
                        'example'     => 'api_test',
                        ),
                    "pw"  => array(
                        'name'        => 'パスワード',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'ユーザーログイン用のパスワード',
                        'example'     => '3s4G34rGw1',
                        ),
                    );
                $_user_info = $api->send($api_path, $method_name, $param, null, $method_title, $method_info, $param_property);
                $token = $_user_info["data"]["session"];
                $user_info = $_user_info["data"]["user_info"];
                */

                // ログインユーザーのパスワード変更
                $method_name  = 'action_change_password';
                $method_title = 'パスワード変更';
                $method_info  = 'ログインユーザーのパスワードの変更します。';
                $param = array(
                    "pw" => "pw999999",
            //        "passwd" => "pw999999",
                    );
                $param_property = array(
                    "pw"  => array(
            //        "passwd"  => array(
                        'name'        => 'パスワード',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '更新用の新ログインパスワード',
                        'example'     => 'pw999999',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                // ログインユーザーのパスワード変更
                $method_name  = 'action_change_password';
                $method_title = 'ログインユーザーのパスワードを元に戻す';
                $method_info  = '';
                $param = array(
                    "pw" => $user_pw,
            //        "passwd" => $user_pw,
                    );
                $param_property = array(
                    "pw"  => array(
            //        "passwd"  => array(
                        'name'        => 'パスワード',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '更新用の新ログインパスワード',
                        'example'     => 'pw999999',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                // ユーザー情報の更新（ユーザー設定変更）
                $method_name  = 'action_update';
                $method_title = 'ユーザー情報の更新';
                $method_info  = 'ログインユーザーの情報の更新します。';
                $param = array(
                    "timezone" => '4',
                    "lang" => 'en',
                    "country" => 'us',
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                // ログアウト
                $method_name  = 'action_logout';
                $method_title = 'ログアウト';
                $method_info  = 'セッションの破棄し、ログアウトします';
                $param = array(
                    );
                $param_property = array(
                    );
                $ret = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                // ログイン
                /*
                $method_name  = 'action_login';
                $method_title = 'ログイン（MD5パスワード暗号化）';
                $method_info  = '';
                $param = array(
                    "id"  => $user_id,
                    "pw"  => md5($user_pw),
                    "enc" => "md5",
                    "output_type" => "php"
                    );
                $param_property = array(
                    );
                $ret = $api->send($api_path, $method_name, $param, null, $method_title, $method_info, $param_property);

                // ログイン
                $method_name  = 'action_login';
                $method_title = 'ログイン（SHA1パスワード暗号化）';
                $method_info  = '';
                $param = array(
                    "id"  => $user_id,
                    "pw"  => sha1($user_pw),
                    "enc" => "sha1",
                    "output_type" => "php"
                    );
                $param_property = array(
                    );
                $ret = $api->send($api_path, $method_name, $param, null, $method_title, $method_info, $param_property);
                */

                // ログイン
                $method_name  = 'action_login';
                $method_title = 'メンバーログイン';
                $method_info  = '';
                $param = array(
                    "api_id"  => $api_key,
                    "id"  => $member_id,
                    "pw"   => hash('sha256', ($member_id . hash('sha256', $member_pw) . $secret)),
                    "output_type" => "php"
                    );
                $param_property = array(
                    );
                $ret = $api->send($api_path, $method_name, $param, null, $method_title, $method_info, $param_property);
                $token = $ret["data"]["session"];
                $member_name = $ret["data"]["member_info"]['member_name'];
                $member_name_kana = $ret["data"]["member_info"]['member_name_kana'];

                // ユーザー情報の更新
                $method_name  = 'action_update';
                $method_title = 'メンバー情報の更新';
                $method_info  = '';
                $param = array(
                    "email"     => $mail_from,
                    "name"      => $member_name,
                    "name_kana" => $member_name_kana,
                    "timezone"  => $timezone,
                    "lang"      => "ja",
                    "country"   => "jp",
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                // メンバー一覧取得
                $method_name  = 'action_get_member_list';
                $method_title = 'メンバー一覧取得';
                $method_info  = 'ログインユーザーに所属しているメンバー一覧を取得します。';
                $param = array(
                    );
                $param_property = array(
                    );
                $member_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//                new dBug($member_list);
                $member_id = $member_list['data']['members']['member'][0]['member_id'];

                // 部屋一覧取得
                $method_name  = 'action_get_room_list';
                $method_title = '部屋一覧取得';
                $method_info  = 'ログインユーザが利用可能な部屋の一覧を取得します。';
                $param = array(
                    );
                $param_property = array(
                    );
                $room_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                list($room_no, $room_info) = each($room_list["data"]["rooms"]["room"]);
                $room_key = $room_info["room_info"][API_ROOM_ID];

                // 部屋状態取得（会議開催中のデータを取得）
                $method_name  = 'action_get_room_info';
                $method_title = '部屋状態取得';
                $method_info  = '部屋の利用状況（会議中、参加人数、参加者名など）を取得します。';
                $param = array(
                    API_ROOM_ID => $room_key,
                    );
                $param_property = array(
                    );
                $room_status = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                $meeting_id = $room_status['data']['room_status']['meeting_id'];
            //*/

            /**************************
             * 会議関連
             **************************/

            if ($meeting_flg) {
                $api_path = "user/meeting/";
                $api->add_log("**会議関連 $api_path \n\n");
                $api->add_group($api_path, "会議関連", "会議作成、開始、ノッカー機能が使用できます");

            ///*
//                // 会議作成
//                $method_name  = 'action_create';
//                $method_title = '会議作成';
//                $method_info  = '会議を作成します。';
//                $param = array(
//                    API_ROOM_ID => $room_key,
//                    );
//                $param_property = array(
//                    );
//                $meeting_info = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//                $meeting_ticket = $meeting_info["data"][API_MEETING_ID];

                //
                $method_name  = 'action_start';
                $method_title = '会議開始';
                $method_info  = '指定した部屋の会議に参加するためのURLを発行します。' .
                        '会議キーの指定がない場合は、現在利用可能な会議に参加されます。注意：発酵時の会議が終了すると入室できません。';
                $param = array(
                    API_ROOM_ID    => $room_key,
                    "type"        => "normal",
                    "name"        => "",
                    "is_narrow"   => "",
            //        "skin_type"   => "",
            //        "station"     => "",
                    );
                $param_property = array(
                    "type"  => array(
                        'name'        => 'ユーザータイプ',
                        'type'        => 'type',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => 'normal',
                        'addition'    => 'normal: 通常 / audience: オーディエンス',
                        'example'     => 'normal',
                        ),
                    "name"  => array(
                        'name'        => '参加者名',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => 'メンバーログイン時はメンバー名',
                        'addition'    => '空白の場合には、会議開始時に参加者名の入力ダイアログボックスが表示される',
                        'example'     => '参加者A',
                        ),
                    "is_narrow"  => array(
                        'name'        => '低速会線モード',
                        'type'        => 'boolean',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '1: 低速回線モード',
                        'example'     => '1',
                        ),
            //        "skin_type"  => array(
            //            'name'        => 'スキンタイプ',
            //            'type'        => '',
            //            'restriction' => '',
            //            'required'    => '',
            //            'default'     => '',
            //            'addition'    => '',
            //            ),
            //        "station"  => array(
            //            'name'        => '駅名',
            //            'type'        => '',
            //            'restriction' => '',
            //            'required'    => '',
            //            'default'     => '',
            //            'addition'    => '',
            //            ),
                    );
                $meeting_start = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                print '<a target="_blank" href="'.$meeting_start["data"]["url"].'">会議開始</a><br />';

                //
                $method_name  = 'action_start';
                $method_title = '会議開始 (会議IDを指定して入室)';
                $method_info  = '指定した部屋の会議に参加するためのURLを発行します。' .
                        '会議キーの指定がない場合は、現在利用可能な会議に参加されます。注意：発酵時の会議が終了すると入室できません。';
                $param = array(
                    API_ROOM_ID    => $room_key,
                    API_MEETING_ID => $meeting_id,
                    "type"        => "audience",
                    "name"        => "",
                    "is_narrow"   => "",
            //        "skin_type"   => "",
            //        "station"     => "",
                    );
                $meeting_start = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                print '<a target="_blank" href="'.$meeting_start["data"]["url"].'">会議開始</a><br />';

                //
                $method_name  = 'action_knocker';
                $method_title = 'ノッカー';
                $method_info  = "会議中の参加者にメッセージを送信します。";
                $param = array(
                    API_ROOM_ID => $room_key,
                    "message" => date("Y-m-d H:i:s"),
                    );
                $param_property = array(
                    "message"  => array(
                        'name'        => 'ノッカーメッセージ',
                        'type'        => 'string(100)',
                        'restriction' => '1～100文字以内',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'ノッカーで会議室内に表示させる文字列',
                        'example'     => 'メッセージ',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

            if ($document_flg) {
                //
                $method_name  = 'action_wb_upload';
                $method_title = '資料追加';
                $method_info  = '会議中のホワイトボードに資料を張り込みます。<br />';
                $param = array(
                    API_ROOM_ID => $room_key,
                    "file" => "@".N2MY_APP_DIR."htdocs/shared/images/ecowhats_bg.gif",
                    );
                $param_property = array(
                    "file"  => array(
                        'name'        => '貼り込み資料情報',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'Multipart/Form-data形式でポスト',
                        'example'     => '(ファイル)',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
            }

                //
                $method_name  = 'action_stop';
                $method_title = '会議終了';
                $method_info  = '会議を終了します。';
                $param = array(
                    API_ROOM_ID => $room_key,
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                //
                $method_name  = 'action_start';
                $method_title = '会議開始';
                $method_info  = '';
                $param = array(
                    API_ROOM_ID => $room_key,
                    );
                $param_property = array(
                    );
                $meeting_status_url = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                $meeting_ticket = $meeting_status_url["data"][API_MEETING_ID];

                //
                $method_name  = 'action_stop';
                $method_title = '会議終了（会議を指定して終了）';
                $method_info  = '';
                $param = array(
                    API_ROOM_ID => $room_key,
                    API_MEETING_ID => $meeting_ticket,
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

            //*/
            }

            /**************************
             * 予約関連
             **************************/

            if ($reservation_flg) {

                $api_path = "user/reservation/";
                $api->add_log("**予約関連 $api_path \n\n");
                $api->add_group($api_path, "予約関連", "予約、招待、事前アップロードなど");

                //
                $method_name  = 'action_get_list';
                $method_title = '予約一覧';
                $method_info  = '予約済みの一覧を取得します。';
                $param = array(
                    API_ROOM_ID   => $room_key,
                    "start_limit" => strtotime('2009-01-01 00:00:00'), // date("Y-m-d H:i:s"),
                    "end_limit"   => strtotime(date("Y-m-d H:i:s", time() + 3600 * 24 * 365)), // date("Y-m-d H:i:s", time() + 3600 * 24 * 30),
                    "limit"       => 20,
                    "offset"      => 0,
                    "status"      => 'wait',
                    "sort_key"    => "reservation_starttime",
                    "sort_type"   => "asc",
                    );
                $param_property = array(
                    "start_limit"  => array(
                        'name'        => '開催時刻検索始点',
                        'type'        => 'date',
                        'restriction' => 'GMTタイムスタンプ / 日付形式（yyyy-mm-dd hh:mm:ss）',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '対象の会議',
                        'example'     => '1262332800',
                        ),
                    "end_limit"  => array(
                        'name'        => '会議終了時刻',
                        'type'        => 'date', // 型
                        'restriction' => 'GMTタイムスタンプ / 日付形式（yyyy-mm-dd hh:mm:ss）',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '対象の会議',
                        'example'     => '1262336400',
                        ),
                    );
                $result = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                $reservation_list = $result["data"]["reservations"];
                $reservation_session = $reservation_list["reservation"][0][API_RESERVATION_ID];

                //
                $method_name  = 'action_get_detail';
                $method_title = '予約内容詳細';
                $method_info  = '予約の詳細情報を取得します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
                    );
                if ($reservation_list["reservation"][0]["reservation_pw"]) {
                    $param["password"] = $user_apw;
                }
                $param_property = array(
                    );
                $reservation_info = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                //
                $method_name  = 'action_add';
                $method_title = '予約追加';
                $method_info  = '予約を追加します。';
                $reservaion_pw = "password";
                $new_reservaion_pw = "newpassword";
                $param = array(
                    API_ROOM_ID     => $room_key,
                    "name"         => date("Y-m-d H:i:s")."[新規]",
                    "timezone"     => $timezone,
                    "start"        => date("Y-m-d H:i:00", time() + (3600 * 24 * 30)),
                    "end"          => date("Y-m-d H:i:30", time() + (3600 * 24 * 30)),
                    "password"     => $reservaion_pw,
                    "send_mail"    => 0,
                    "sender_name"  => $sender,
                    "sender_email" => $mail_from,
                    "info"         => "メール本文",
                    "guests[0][name]"     => "kiyomizu",
                    "guests[0][email]"    => "kiyomizu@vcube.co.jp",
                    "guests[0][type]"     => "",
                    "guests[0][timezone]" => "100",
                    "guests[0][lang]"     => "ja",
                    );
                $param_property = array(
                    "name"  => array(
                        'name'        => '会議名',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '会議名',
                        ),
                    "timezone" => array( // ！！！パラメータ名に揺らぎあり！！！
                        'name'        => 'タイムゾーン',
                        'type'        => 'int(64)', // 型
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '9',
                        'addition'    => '-11 ～ 14',
                        'example'     => '9',
                        ),
                    "start"  => array(
                        'name'        => '会議開始時刻',
                        'type'        => 'date', // 型
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'GMTタイムスタンプ / 日付形式（yyyy-mm-dd hh:mm:ss）',
                        'example'     => '1262332800',
                        ),
                    "end"  => array(
                        'name'        => '会議終了時刻',
                        'type'        => 'date', // 型
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'GMTタイムスタンプ / 日付形式（yyyy-mm-dd hh:mm:ss）',
                        'example'     => '1262336400',
                        ),
                    "password"  => array( // ！！！パラメータ名に揺らぎあり！！！
                        'name'        => '会議パスワード',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => 'abc123',
                        ),
                    "send_mail"  => array(
                        'name'        => 'メール送信フラグ',
                        'type'        => 'boolean',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '1',
                        'addition'    => '0: 送信しない / 1: 送信する',
                        'example'     => 'sample@example.com',
                        ),
                    "sender_name"  => array(
                        'name'        => '差出人名',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '名前',
                        ),
                    "sender_email"  => array(
                        'name'        => '差出人メールアドレス',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'メールアドレス形式',
                        'example'     => 'sample@example.com',
                        ),
                    "info"  => array(
                        'name'        => '招待メール本文',
                        'type'        => 'string(32768)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '招待メール本文',
                        ),
                    "guests"  => array(
                        'name'        => '招待者一覧',
                        'type'        => '',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '連想配列形式で以下のように指定する。' .
            '        "guests[n][name]"     => "name",
                    "guests[n][email]"    => "sample@example.com",
                    "guests[n][type]"     => "",
                    "guests[n][timezone]" => "9",
                    "guests[n][lang]"     => "ja",
            ',
                        'example'     => '',
                        ),
                    );
                $reservation_info = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                $reservation_session = $reservation_info["data"][API_RESERVATION_ID];

                $method_name  = 'action_add';
                $method_title = '予約追加(メンバーID指定)';
                $method_info  = '予約を追加します。';
                $param = array(
                    API_ROOM_ID     => $room_key,
                    "name"         => date("Y-m-d H:i:s")."[新規]",
                    "timezone"     => $timezone,
                    "start"        => date("Y-m-d H:i:00", time()),
                    "end"          => date("Y-m-d H:i:00", time() + 60),
                    "password"     => $reservaion_pw,
                    "send_mail"    => 0,
                    "sender_name"  => $sender,
                    "sender_email" => $mail_from,
                    "info"         => "メール本文",
                    "guests[0][name]"     => "kiyomizu",
                    "guests[0][email]"    => "kiyomizu@vcube.co.jp",
                    "guests[0][type]"     => "",
                    "guests[0][timezone]" => "100",
                    "guests[0][lang]"     => "ja",
                    );
                $reservation_info = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                $reservation_session = $reservation_info["data"][API_RESERVATION_ID];

                $method_name  = 'action_get_detail';
                $method_title = '予約内容詳細';
                $method_info  = '予約の詳細情報を取得します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password" => $reservaion_pw,
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                $method_name  = 'action_update';
                $method_title = '予約変更';
                $method_info  = '予約内容を変更します。';
                $param = array(
                    API_RESERVATION_ID  => $reservation_session,
                    "name"              => date("Y-m-d H:i:s")."[更新]",
                    "timezone"          => $timezone,
                    "start"             => date("Y-m-d H:i:00", time()),
                    "end"               => date("Y-m-d H:i:00", time() + 60),
//                    "password"          => $reservaion_pw,
                    "change_password"   => 1,
                    "new_password"      => $new_reservaion_pw,
                    "send_mail"         => 0,
                    "sender_name"       => $sender,
                    "sender_email"      => $mail_from,
                    "info"              => "メール本文",
                    );
                $param_property = array(
                    "name"  => array(
                        'name'        => '会議名',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '平成12年第4四半期 予算会議',
                        ),
                    "timezone" => array( // ！！！パラメータ名に揺らぎあり！！！
                        'name'        => 'タイムゾーン',
                        'type'        => 'int(64)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '9',
                        'addition'    => '-11 ～ 14',
                        'example'     => '9',
                        ),
                    "start"  => array(
                        'name'        => '会議開始時刻',
                        'type'        => 'date',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'GMTタイムスタンプ / 日付形式（yyyy-mm-dd hh:mm:ss）',
                        'example'     => '1262332800',
                        ),
                    "end"  => array(
                        'name'        => '会議終了時刻',
                        'type'        => 'date',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'GMTタイムスタンプ / 日付形式（yyyy-mm-dd hh:mm:ss）',
                        'example'     => '1262336400',
                        ),
                    "password"  => array( // ！！！パラメータ名に揺らぎあり！！！
                        'name'        => '会議パスワード',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => 'abc1234',
                        ),
                    "send_mail"  => array(
                        'name'        => 'メール送信フラグ',
                        'type'        => 'boolean',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '1',
                        'addition'    => '0: 送信しない / 1: 送信する',
                        'example'     => '1',
                        ),
                    "sender_name"  => array(
                        'name'        => '差出人名',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '送信者',
                        ),
                    "sender_email"  => array(
                        'name'        => '差出人メールアドレス',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'メールアドレス形式',
                        'example'     => 'sample@examile.com',
                        ),
                    "info"  => array(
                        'name'        => '招待メール本文',
                        'type'        => 'string(32768)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '招待メールの内容',
                        'example'     => '',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_update';
                $method_title = '予約変更';
                $method_info  = '予約内容を変更します。';
                $param = array(
                    API_RESERVATION_ID  => $reservation_session,
                    "name"              => date("Y-m-d H:i:s")."[更新]",
                    "timezone"          => $timezone,
                    "start"             => date("Y-m-d H:i:00", time()),
                    "end"               => date("Y-m-d H:i:00", time() + 60),
//                    "password"          => $new_reservaion_pw,
                    "change_password"   => 1,
                    "new_password"      => $new_reservaion_pw,
                    "send_mail"         => 0,
                    "sender_name"       => $sender,
                    "sender_email"      => $mail_from,
                    "guests[0][name]"       => "kiyomizu1",
                    "guests[0][email]"      => "kiyomizu@vcube.co.jp",
                    "guests[0][type]"       => "",
                    "guests[0][timezone]"   => "100",
                    "guests[0][lang]"       => "ja",
                    "guests[1][name]"       => "kiyomizu2",
                    "guests[1][email]"      => "kiyomizu@vcube.co.jp",
                    "guests[1][type]"       => "",
                    "guests[1][timezone]"   => "100",
                    "guests[1][lang]"       => "ja",
                    "guests[2][member_id]"  => $member_id,
                    "guests[2][name]"       => "",
                    "guests[2][email]"      => "",
                    "guests[2][type]"       => "",
                    "guests[2][timezone]"   => "",
                    "guests[2][lang]"       => "",
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_update';
                $method_title = '予約変更';
                $method_info  = '予約内容を変更します。（パスワード無しに変更）';
                $param = array(
                    API_RESERVATION_ID  => $reservation_session,
                    "name"              => date("Y-m-d H:i:s")."[更新]",
                    "timezone"          => $timezone,
                    "start"             => date("Y-m-d H:i:00", time()),
                    "end"               => date("Y-m-d H:i:00", time() + 60),
//                    "password"          => $new_reservaion_pw,
                    "change_password"   => 1,
                    "new_password"      => "",
                    "send_mail"         => 0,
                    "sender_name"       => $sender,
                    "sender_email"      => $mail_from,
                    "guests[0][name]"       => "kiyomizu1",
                    "guests[0][email]"      => "kiyomizu@vcube.co.jp",
                    "guests[0][type]"       => "",
                    "guests[0][timezone]"   => "100",
                    "guests[0][lang]"       => "ja",
                    "guests[1][name]"       => "kiyomizu2",
                    "guests[1][email]"      => "kiyomizu@vcube.co.jp",
                    "guests[1][type]"       => "",
                    "guests[1][timezone]"   => "100",
                    "guests[1][lang]"       => "ja",
                    "guests[2][member_id]"  => $member_id,
                    "guests[2][name]"       => "",
                    "guests[2][email]"      => "",
                    "guests[2][type]"       => "",
                    "guests[2][timezone]"   => "",
                    "guests[2][lang]"       => "",
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_get_detail';
                $method_title = '予約内容詳細';
                $method_info  = '予約の詳細情報を取得します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password" => $user_apw,
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                //
                $method_name  = 'action_add_invite';
                $method_title = '招待者追加（通常）';
                $method_info  = '予約した会議の招待メールを送信します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    "name"                => date("Y-m-d H:i:s")."[招待者新規]",
                    "email"               => $mail_to,
                    "timezone"            => $timezone,
                    "lang"                => "ja",
                    "type"                => "",
                    "send_mail"           => 0,
                    );
                $param_property = array(
                    API_RESERVATION_ID  => array(
                        'name'        => '予約会議セッションキー',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '予約した会議のセッションキー',
                        'example'     => '',
                        ),
                    "name"  => array(
                        'name'        => '招待者名',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '',
                        ),
                    "email"  => array(
                        'name'        => '招待者メールアドレス',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'メールアドレス形式',
                        'example'     => '',
                        ),
                    "timezone" => array( // ！！！パラメータ名に揺らぎあり！！！
                        'name'        => '招待者タイムゾーン',
                        'type'        => 'string(16)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '9',
                        'addition'    => '-11 ～ 14',
                        'example'     => '9',
                        ),
                    "lang"  => array(
                        'name'        => '招待者言語設定',
                        'type'        => 'string(2)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => 'ja',
                        'addition'    => 'ja: 日本語 / en: 英語',
                        'example'     => 'ja',
                        ),
                    "type"  => array(
                        'name'        => '招待者種別',
                        'type'        => 'string(8)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '(null): 通常 / audience: オーディエンス',
                        'example'     => '',
                        ),
                    "send_mail"  => array(
                        'name'        => 'メール送信フラグ',
                        'type'        => 'boolean',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '1',
                        'addition'    => '0: 送信しない / 1: 送信する',
                        'example'     => '',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                //
                $method_name  = 'action_add_invite';
                $method_title = '招待者追加（メンバー登録）';
                $method_info  = '予約した会議の招待メールを送信します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    "name"                => date("Y-m-d H:i:s")."[招待者新規]",
                    "email"               => $mail_to,
                    "timezone"            => $timezone,
                    "lang"                => "ja",
                    "type"                => "",
                    "send_mail"           => 0,
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                //
                $method_name  = 'action_add_invite';
                $method_title = '招待者追加1（一括登録）';
                $method_info  = '';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    "send_mail"           => 0,
                    "name[1]"             => "招待者新規1",
                    "email[1]"            => $mail_to,
                    "timezone[1]"         => "4",
                    "lang[1]"             => "en",
                    "type[1]"             => "audience",
                    "name[2]"             => "招待者新規2",
                    "email[2]"            => $mail_to,
                    "timezone[2]"         => "",
                    "lang[2]"             => "",
                    "type[2]"             => "",
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

                $method_name  = 'action_get_invite';
                $method_title = '招待者一覧';
                $method_info  = '予約した会議に登録した招待者一覧を取得します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    );
                $param_property = array(
                    );
                $invite_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                list($invite_id, $invite_info) = each($invite_list["data"]["guests"]["guest"]);

                //
                $method_name  = 'action_delete_invite';
                $method_title = '招待者削除';
                $method_info  = '予約した会議の招待者を削除します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    API_GUEST_ID => $invite_info[API_GUEST_ID],
                    "lang" => "ja",
                    );
                $param_property = array(
                    "invite_id" => array(
                        'name'        => '招待ID',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '',
                        ),
                    "lang"  => array(
                        'name'        => '招待者言語設定',
                        'type'        => 'string(2)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => 'ja',
                        'addition'    => 'ja: 日本語 / en: 英語',
                        'example'     => 'ja',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                //
                $method_name  = 'action_get_invite';
                $method_title = '招待者一覧';
                $method_info  = '';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    );
                $invite_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            if ($document_flg) {
                //
                $method_name  = 'action_add_document';
                $method_title = '資料追加';
                $method_info  = '予約した会議に事前資料アップロードを行います。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    "file" => "@".N2MY_APP_DIR."htdocs/shared/images/ecowhats_bg.gif",
                    "name" => "画像 - ".date("Y-m-d H:i:s"),
                    );
                $param_property = array(
                    "file"  => array(
                        'name'        => '貼り込み資料情報',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'Multipart/Form-data形式でポスト',
                        'example'     => '(バイナリファイル)',
                        ),
                    "name"  => array(
                        'name'        => '貼り込み資料名',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '会議資料',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                //
                $method_name  = 'action_add_document';
                $method_title = '資料追加2(画像)';
                $method_info  = '予約した会議に事前資料アップロードを行います。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    "file" => "@".N2MY_APP_DIR."htdocs/shared/images/ecowhats_bg.gif",
                    "name" => "",
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_add_document';
                $method_title = '資料追加(ドキュメント)';
                $method_info  = '';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    "file" => "@".N2MY_APP_DIR."htdocs/lang/ja_JP/PDF/Manual_Audience.pdf",
                    "name" => "資料 - ".date("Y-m-d H:i:s"),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

                //
                $method_name  = 'action_get_document';
                $method_title = '資料一覧取得';
                $method_info  = '予約した会議の事前資料一覧を取得します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    );
                $param_property = array(
                    );
                $document_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                list($a, $document) = each($document_list["data"]["documents"]["document"]);

                //
                $method_name  = 'action_update_document';
                $method_title = '資料名変更';
                $method_info  = '予約した会議の資料名を変更します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    "document_id" => $document["document_id"],
                    "name" => "名前変更 - ".date("Y-m-d"),
                    );
                $param_property = array(
                    "document_id"  => array(
                        'name'        => '資料ID',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '事前アップロードした資料ID',
                        'example'     => '34317fbc98bdc7bcaf24e7561639a1e73383b05f',
                        ),
                    "name"  => array(
                        'name'        => '資料名',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '会議資料',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                //
                $method_name  = 'action_delete_document';
                $method_title = '資料削除';
                $method_info  = '予約した会議の資料を削除します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    "document_id" => $document["document_id"],
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                //
                $method_name  = 'action_get_document';
                $method_title = '資料一覧取得';
                $method_info  = '';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

                //
            //    $method_name  = 'action_sort';
            //    $method_title = '資料の表示順変更';
            //    $method_info  = '予約した会議の資料表示順を変更します。';
            //    $param = array(
            ////        );
            //    $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

                $method_name  = 'action_preview_document';
                $method_title = '資料の表示';
                $method_info  = '';

                $url = $host."/api/".$api_version."/".$api_path .
                        "?action_preview_document=" .
                        "&document_id=" .$document["document_id"].
                        "&page=1" .
                        "&".N2MY_SESSION."=".$token;
                print '<img src="'.$url.'" />';
                print $url;

            }

                //
                $method_name  = 'action_delete';
                $method_title = '予約削除';
                $method_info  = '予約を取り消します。';
                $param = array(
                    API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

            //*/
            }

            /**************************
             * 会議記録関連
             **************************/

            if ($meetinglog_flg) {
                $api_path = "user/meetinglog/";
                $api->add_log("**会議記録関連 $api_path \n\n");
                $api->add_group($api_path, "会議記録関連", "会議記録関連");

                ///*
                //$sv_start_time = '2009-07-23 00:00:00';
                $sv_start_time = strtotime('2009-01-01 00:00:00');
                //$sv_end_time = '2009-07-29 23:59:59';
                $sv_end_time = strtotime(date("Y-m-d H:i:s", time() + 3600 * 24 * 30));
                //
                $method_name  = 'action_get_list';
                $method_title = '会議記録一覧';
                $method_info  = '会議記録一覧を取得します。';
                $param = array(
                    API_ROOM_ID    => $room_key,
                    "meeting_name" => "",
                    "user_name"    => "",
                    "start_limit"  => $sv_start_time, //"",
                    "end_limit"    => $sv_end_time, //"",
                    "page"         => 1,
                    "limit"        => "5",
                    "sort_key"     => "actual_start_datetime",
                    "sort_type"    => "asc",
            //        $sess_id       => $session,
                    );
                $param_property = array(
                    "meeting_name"  => array(
                        'name'        => '会議名',
                        'type'        => 'string(64)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '会議名',
                        ),
                    "user_name"  => array(
                        'name'        => '参加者名',
                        'type'        => '',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '参加者名',
                        ),
                    "start_limit"  => array(
                        'name'        => '開始日時',
                        'type'        => '',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => 'GMTタイムスタンプ / 日付形式（yyyy-mm-dd hh:mm:ss）',
                        'example'     => '1262332800',
                        ),
                    "end_limit"  => array(
                        'name'        => '終了日時',
                        'type'        => '',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => 'GMTタイムスタンプ / 日付形式（yyyy-mm-dd hh:mm:ss）',
                        'example'     => '1262336400',
                        ),
                    "page"  => array(
                        'name'        => 'ページ番号',
                        'type'        => '',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '表示するページ番号',
                        'example'     => '1',
                        ),
                    "limit"  => array(
                        'name'        => '取得件数',
                        'type'        => 'int',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '10',
                        'addition'    => '',
                        'example'     => '5',
                        ),
                    "sort_key"  => array(
                        'name'        => 'ソート項目',
                        'type'        => 'string',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => 'actual_start_datetime:実際の会議開始時間',
                        'addition'    => '',
                        'example'     => 'actual_start_datetime',
                        ),
                    "sort_type"  => array(
                        'name'        => '昇順 / 降順',
                        'type'        => '',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => 'asc:昇順',
                        'addition'    => 'asc: 昇順 / desk: 降順',
                        'example'     => 'asc',
                        ),
                    );
                $meeting_logs = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                list($meeting_key, $meeting_log) = each($meeting_logs["data"]["meetinglogs"]["meetinglog"]);
                $meeting_session_id = $meeting_log[API_MEETING_ID];
                $meeting_sequence_id = $meeting_log['meeting_sequences']['meeting_sequence'][0]['meeting_sequence_key'];

                /*
                $method_name  = 'action_get_detail';
                $method_title = '会議記録詳細';
                $method_info  = '会議記録の詳細情報を取得します。';
                $param = array(
                    API_MEETING_ID => $meeting_session_id,
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                */
                //
                $method_name  = 'action_video_player';
                $method_title = '会議記録再生';
                $method_info  = '録画再生用のワンタイムURLを発行します。有効期限は5分間です。 ';
                $param = array(
                    API_MEETING_ID  => $meeting_session_id,
                    "sequence_id"   => $meeting_sequence_id,
                    "lang"          => "ja",
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_minute_player';
                $method_title = '議事録再生';
                $method_info  = '議事録再生用のワンタイムURLを発行します。有効期限は5分間です。 ';
                $param = array(
                    API_MEETING_ID  => $meeting_session_id,
                    "sequence_id"   => $meeting_sequence_id,
                    "lang"          => "ja",
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

            //*/
            }

            /**************************
             * アドレス帳
             **************************/

            if ($addressbook_flg) {

                $api_path = "user/addressbook/";
                $api->add_log("**アドレス帳関連 $api_path \n\n");
                $api->add_group($api_path, "アドレス帳関連", "アドレス帳の登録、更新、削除");

            ///*

                //
                $method_name  = 'action_add';
                $method_title = 'アドレス帳追加';
                $method_info  = 'アドレス帳に追加します。';
                $param = array(
                    "name"       => "name",
                    "name_kana"  => "namae",
                    "email"      => "sample@example.com",
                    "timezone"   => "9",
                    "lang"       => "ja",
                    );
                $param_property = array(
                    "name"  => array(
                        'name'        => '名前',
                        'type'        => 'string(200)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '会議セッションキー',
                        'example'     => '山田太郎',
                        ),
                    "name_kana"  => array(
                        'name'        => '名前（カナ）',
                        'type'        => 'string(200)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '会議セッションキー',
                        'example'     => 'ヤマダタロウ',
                        ),
                    "email"  => array(
                        'name'        => 'メールアドレス',
                        'type'        => 'string(200)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'メールアドレス形式',
                        'example'     => 'sample@example.com',
                        ),
                    "timezone" => array( // ！！！パラメータ名に揺らぎあり！！！
                        'name'        => 'タイムゾーン設定',
                        'type'        => 'string(16)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '9',
                        'addition'    => '-11 ～ 14',
                        'example'     => '9',
                        ),
                    "lang"  => array(
                        'name'        => '言語設定',
                        'type'        => 'string(2)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => 'ja',
                        'addition'    => 'ja: 日本語 / en: 英語',
                        'example'     => 'ja',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                //
                $method_name  = 'action_get_list';
                $method_title = 'アドレス帳一覧';
                $method_info  = 'アドレス帳一覧を取得します';
                $param = array(
                    "sort_key"    => "name",
                    "sort_type"   => "asc",
                    "page"        => 1,
                    "limit"       => 20,
                    "keyword"     => "",
                    );
                $param_property = array(
                    "keyword"  => array(
                        'name'        => '検索キーワード',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '山田',
                        ),
                    );
                $address_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                list($key, $address_info) = each($address_list["data"]["addresses"]["address"]);

                //
                $method_name  = 'action_update';
                $method_title = 'アドレス帳更新';
                $method_info  = 'アドレス帳の情報を更新します。';
                $param = array(
                    API_ADDRESS_ID   => $address_info[API_ADDRESS_ID],
                    "name"          => "name",
                    "name_kana"     => "namae",
                    "email"         => "sample@example.com",
                    "timezone"      => "9",
                    "lang"          => "ja",
                    );
                $param_property = array(
                    "name"  => array(
                        'name'        => '名前',
                        'type'        => 'string(200)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '会議セッションキー',
                        'example'     => '山田太郎',
                        ),
                    "name_kana"  => array(
                        'name'        => '名前（カナ）',
                        'type'        => 'string(200)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '会議セッションキー',
                        'example'     => 'ヤマダタロウ',
                        ),
                    "email"  => array(
                        'name'        => 'メールアドレス',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'メールアドレス形式',
                        'example'     => 'sample@example.com',
                        ),
                    "timezone" => array(
                        'name'        => 'タイムゾーン',
                        'type'        => 'string(16)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => '9',
                        'addition'    => '-11 ～ 14',
                        'example'     => '9',
                        ),
                    "lang"  => array(
                        'name'        => '言語設定',
                        'type'        => 'string(2)',
                        'restriction' => '',
                        'required'    => '',
                        'default'     => 'ja',
                        'addition'    => 'ja: 日本語 / en: 英語',
                        'example'     => 'ja',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                if ($error_flg) {
                    //
                    $method_name  = 'action_update';
                    $method_title = 'アドレス帳更新';
                    $method_info  = 'アドレスIDが存在しない';
                    $param = array(
                        API_ADDRESS_ID   => 99999999,
                        "name"          => "name",
                        "name_kana"     => "namae",
                        "email"         => "sample@example.com",
                        "timezone"      => "9",
                        "lang"          => "ja",
                        );
                    $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                }

                //
                $method_name  = 'action_delete';
                $method_title = 'アドレス帳削除';
                $method_info  = 'アドレス帳から削除します';
                $param = array(
                    API_ADDRESS_ID   => $address_info[API_ADDRESS_ID],
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                if ($error_flg) {
                    //
                    $method_name  = 'action_delete';
                    $method_title = 'アドレス帳削除';
                    $method_info  = 'アドレスIDが存在しない';
                    $param = array(
                        API_ADDRESS_ID   => 999999999,
                        );
                    $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                }

                //
            //    $method_name  = 'action_get_member';
            //    $method_title = 'メンバー一覧';
            //    $method_info  = '';
            //    $param = array(
            ////        );
            //    $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);


            //*/
            }

            /**************************
             * ECOメーター
             **************************/

            if ($eco_flg) {
                $api_path = "user/eco/";
                $api->add_log("**エコメーター関連 $api_path \n\n");
                $api->add_group($api_path, "エコメーター関連", "エコメータ関連");

            ///*

                //
                $method_name  = 'action_get_station_list';
                $method_title = '駅名検索';
                $method_info  = '駅名を検索します。';
                $param = array(
                    "station" => "あ",
                    );
                $param_property = array(
                    "station"  => array(
                        'name'        => '参加者最寄り駅',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '駅名',
                        'example'     => '東京',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

            /*
                //
                $method_name  = 'action_get_transit';
                $method_title = '経路情報';
                $method_info  = '指定した拠点（駅）の最短ルート情報を取得します。';
                $param = array(
                    "station_list" => "中目黒,広尾,目黒",
                    "end_place" => "箱根湯本",
                    "sort" => "time",
                    );
                $param_property = array(
                    "station_list"  => array(
                        'name'        => '参加者最寄り駅',
                        'type'        => 'string(65535)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '駅名',
                        'example'     => '',
                        ),
                    "end_place"  => array(
                        'name'        => '目的地',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '駅名（中目黒）',
                        'example'     => '',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

            //*/
            }

            /*******************************************
             * 管理者用API
             *******************************************/
            if ($admin_flg) {
                $api_path = "admin/";
                $api->add_log("*管理者用API\n\n");
                $api->add_group($api_path, "管理者API関連", "管理者API関連");

                $method_name  = 'action_login';
                $method_title = '管理者権限取得';
                $method_info  = '通常ユーザーから管理者権限を取得します。';
                $param = array(
                    "admin_pw" => $user_apw,
                    );
                $param_property = array(
                    "admin_pw"  => array(
                        'name'        => '管理者パスワード',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => 'ia4daiwx9',
                        'example'     => 'admin9pw',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_change_password';
                $method_title = '管理者パスワード変更';
                $method_info  = '管理者パスワードを変更します';
                $param = array(
                    "admin_pw" => "kiyomizu123",
                    );
                $param_property = array(
                    "admin_pw"  => array(
                        'name'        => '管理者パスワード',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '489w11ixm',
                        'example'     => 'admin9pw',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_change_password';
                $method_title = '管理者パスワード変更';
                $method_info  = '管理者パスワードを変更します';
                $param = array(
                    "admin_pw" => $user_apw,
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

            }

            /*******************************************
             * 管理者用 - 部屋操作関連
             *******************************************/
            if ($admin_room_flg) {
                $api_path = "admin/room/";
                $api->add_log("*管理者 - 部屋操作関連\n\n");
                $api->add_group($api_path, "部屋操作関連", "部屋操作関連");

                $method_name  = 'action_update';
                $method_title = '部屋の設定変更';
                $method_info  = '部屋の設定を変更します';
                $param = array(
                    API_ROOM_ID => $room_key,
                    "room_name" => "部屋名１",
                    "cabinet" => "1",
                    "mail_wb_upload" => "all",
                    );
                $param_property = array(
                    "room_name"  => array(
                        'name'        => '部屋名',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '部屋名１',
                        ),
                    "cabinet" => array(
                        'name'        => 'ファイルキャビネット設定',
                        'type'        => 'string(255)',
                        'restriction' => '0:使用不可 / 1:使用可',
                        'required'    => '必須',
                        'default'     => '1',
                        'addition'    => '',
                        'example'     => '1',
                        ),
                    "mail_wb_upload"  => array(
                        'name'        => 'メール資料貼り込み設定',
                        'type'        => 'string(255)',
                        'restriction' => 'all:全許可 / select:指定受信 / deny:全拒否',
                        'required'    => '必須',
                        'default'     => 'all',
                        'addition'    => '',
                        'example'     => 'all',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_get_wb_mail';
                $method_title = 'メール資料貼り込みの送信元アドレス一覧取得';
                $method_info  = 'メール資料貼り込みが有効になっている送信元アドレス一覧を取得します';
                $param = array(
                    );
                $param_property = array(
                    );
                $wb_mails = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                list($_wb_meil_key, $wb_mail_info) = each($wb_mails["data"]["wb_mail_list"]);
                $meeting_session_id = $wb_mail_info["wb_mail_id"];

                $method_name  = 'action_add_wb_mail';
                $method_title = 'メール資料貼り込みの送信元追加';
                $method_info  = 'メール資料貼り込みの送信元アドレスを追加します';
                $param = array(
                    "name" => "部屋名",
                    "email" => "kiyomizu@vcube.co.jp",
                    );
                $param_property = array(
                    "name"  => array(
                        'name'        => '差出人ラベル',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '会社全体',
                        'example'     => '会社全体',
                        ),
                    "email"  => array(
                        'name'        => '差出人アドレス',
                        'type'        => 'string(255)',
                        'restriction' => '指定受信用のアドレス。部分一致可能',
                        'required'    => '必須',
                        'default'     => '@vcube.co.jp',
                        'addition'    => '',
                        'example'     => '@vcube.co.jp',
                        ),
                    );
                $wb_mail_info = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                $wb_mail_id = $wb_mail_info["data"]["wb_mail_id"];

                $method_name  = 'action_update_wb_mail';
                $method_title = 'メール資料貼り込みの送信元更新';
                $method_info  = 'メール資料貼り込みの送信元アドレスを更新します';
                $param = array(
                    "wb_mail_id" => $wb_mail_id,
                    "name" => "部屋名変更",
                    "email" => "kiyomizu@vcube.co.jp"
                    );
                $param_property = array(
                    "wb_mail_id"  => array(
                        'name'        => 'メール資料貼り込みID',
                        'type'        => 'string(255)',
                        'restriction' => 'メール資料張り込みアドレスの一意なキー',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '2141',
                        ),
                    "name"  => array(
                        'name'        => '差出人ラベル',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '会社全体',
                        'example'     => '会社全体',
                        ),
                    "email"  => array(
                        'name'        => '管理者パスワード',
                        'type'        => 'string(255)',
                        'restriction' => '',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '@vcube.co.jp',
                        'example'     => '@vcube.co.jp',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_delete_wb_mail';
                $method_title = 'メール資料貼り込みの送信元削除';
                $method_info  = 'メール資料貼り込みの送信元アドレスを削除します';
                $param = array(
                    "wb_mail_id" => $wb_mail_id,
                    );
                $param_property = array(
                    "wb_mail_id"  => array(
                        'name'        => 'メール資料貼り込みID',
                        'type'        => 'string(255)',
                        'restriction' => 'メール資料張り込みアドレスの一意なキー',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '2141',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_sort_up';
                $method_title = '表示順を上げる';
                $method_info  = '部屋一覧の表示順を下げます';
                $param = array(
                    API_ROOM_ID => $room_key,
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_sort_down';
                $method_title = '表示順を下げる';
                $method_info  = '部屋一覧の表示順を下げます';
                $param = array(
                    API_ROOM_ID => $room_key,
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//*/
            }

            /*******************************************
             * 管理者用 - 会議記録関連
             *******************************************/
            if ($admin_member_flg) {
                $api_path = "admin/member/";
                $api->add_log("*管理者 - メンバー管理\n\n");
                $api->add_group($api_path, "管理者 - メンバー管理", "管理者 - メンバー管理");

                $method_name  = 'action_get_list';
                $method_title = '';
                $method_info  = '';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_add';
                $method_title = '';
                $method_info  = '';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_update';
                $method_title = '';
                $method_info  = '';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_delete';
                $method_title = '';
                $method_info  = '';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_get_group';
                $method_title = '';
                $method_info  = '';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_add_group';
                $method_title = '';
                $method_info  = '';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_update_group';
                $method_title = '';
                $method_info  = '';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_delete_group';
                $method_title = '';
                $method_info  = '';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
            }


            /*******************************************
             * 管理者用 - 会議記録管理関連
             *******************************************/
            if ($admin_meetinglog_flg) {
                $api_path = "user/meetinglog/";

                $method_name  = 'action_get_list';
                $param = array(
                    API_ROOM_ID    => $room_key,
                    "meeting_name" => "",
                    "user_name"    => "",
                    "start_limit"  => $sv_start_time, //"",
                    "end_limit"    => $sv_end_time, //"",
                    "page"         => 1,
                    "limit"        => 1,
                    "sort_key"     => "actual_start_datetime",
                    "sort_type"    => "asc",
                    );
                $meeting_logs = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                list($meeting_key, $meeting_log) = each($meeting_logs["data"]["meetinglogs"]["meetinglog"]);
                $meeting_session_id = $meeting_log[API_MEETING_ID];

                $api_path = "admin/meetinglog/";
                $api->add_log("*管理者 - 会議記録操作\n\n");
                $api->add_group($api_path, "管理者 - 会議記録操作", "管理者 - 会議記録操作");

                $method_name  = 'action_set_ondemand';
                $method_title = 'オンデマンド有効';
                $method_info  = 'オンデマンドを有効にします';
                $param = array(
                    API_MEETING_ID => $meeting_session_id
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);

                $method_name  = 'action_unset_ondemand';
                $method_title = 'オンデマンド無効';
                $method_info  = 'オンデマンドを無効にします';
                $param = array(
                    API_MEETING_ID => $meeting_session_id
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//**
                $method_name  = 'action_set_protect';
                $method_title = '保護有効';
                $method_info  = '保護設定を有効にします';
                $param = array(
                    API_MEETING_ID => $meeting_session_id
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//*/
///*
                $method_name  = 'action_unset_protect';
                $method_title = '保護無効';
                $method_info  = '保護設定を無効にします';
                $param = array(
                    API_MEETING_ID => $meeting_session_id
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//*/
                $method_name  = 'action_set_password';
                $method_title = 'パスワード設定';
                $method_info  = '会議記録にパスワードを設定します';
                $param = array(
                    API_MEETING_ID => $meeting_session_id,
                    "pw" => "kiyomizu"
                    );
                $param_property = array(
                    ""  => array(
                        'name'        => 'ラベル',
                        'type'        => 'string(255)',
                        'restriction' => '説明',
                        'required'    => '必須',
                        'default'     => '',
                        'addition'    => '',
                        'example'     => '',
                        ),
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//*/
///*
                $method_name  = 'action_unset_password';
                $method_title = 'パスワード解除';
                $method_info  = '会議記録のパスワードを解除します';
                $param = array(
                    API_MEETING_ID => $meeting_session_id
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//*/

/*
                $method_name  = 'action_download_cabinet';
                $method_title = '';
                $method_info  = '';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
*/

                $method_name  = 'action_get_eco_info';
                $method_title = 'ECOメーター情報取得';
                $method_info  = '会議記録のECOメーター情報取得';
                $param = array(
                    API_MEETING_ID => $meeting_session_id
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
/*
                $method_name  = 'action_set_eco_info';
                $method_title = '';
                $method_info  = '';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//*/
///*
                $method_name  = 'action_delete_video';
                $method_title = '映像を削除';
                $method_info  = '映像を削除します';
                $param = array(
                    API_MEETING_ID => $meeting_session_id
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//*/
///*
                $method_name  = 'action_delete_minutes';
                $method_title = '議事録を削除';
                $method_info  = '議事録を削除します';
                $param = array(
                    API_MEETING_ID => $meeting_session_id
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//*/
//*/
                $method_name  = 'action_delete';
                $method_title = '会議記録を削除';
                $method_info  = '会議記録を削除します';
                $param = array(
                    API_MEETING_ID => $meeting_session_id
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
//*/
            }

            /*******************************************
             * 管理者用API
             *******************************************/
            if ($admin_flg) {
                $api_path = "admin/";

                /*
                $method_name  = 'action_logout';
                $method_title = '管理者権限を破棄';
                $method_info  = '管理者権限を破棄します。';
                $param = array(
                    );
                $param_property = array(
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info, $param_property);
                */
            }

            if ($_REQUEST["reference"]) {
                $api->outputLog();
            }
        }
    }
}

$main = new AppApiDebug();
$main->execute();

class API_Checker {

    var $version = "";
    var $url = "";
    var $log = "";
    var $method_list = "";
    var $xml = "";
    // ミーティング共通パラメータ
    var $param_proparty = array(
        API_ROOM_ID  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ログイン中のユーザーが所有する部屋のキー',
            'example'     => 'api_test-1-6b4e',
            ),
        API_MEETING_ID  => array(
            'name'        => '会議キー',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '予約等、入室する会議が決まっている場合の会議キー',
            'example'     => '39e8c7366ca89150627993c09cad2430',
            ),
        API_RESERVATION_ID  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            'example'     => 'f327998e82dc9fa496889c79c80e5d1f',
            ),
        API_ADDRESS_ID  => array(
            'name'        => 'アドレス帳キー',
            'type'        => 'int(20)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            'example'     => '87',
            ),
        "lang" => array(
            'name'        => '言語選択',
            'type'        => 'string(2)',
            'restriction' => '',
            'required'    => '',
            'default'     => 'ja',
            'addition'    => 'ja: 日本語 / en: 英語',
            'example'     => 'ja',
            ),
        "country" => array(
            'name'        => '現在の所在地',
            'type'        => 'string(32)',
            'restriction' => '',
            'required'    => '',
            'default'     => 'jp',
            'addition'    => 'jp: 日本 / us: アメリカ（西） / us2: アメリカ（東） / cn: 中国 / sg: シンガポール / etc: その他 / jp2: --',
            'example'     => 'jp',
            ),
        "timezone" => array(
            'name'        => 'タイムゾーン',
            'type'        => 'string(16)',
            'restriction' => '',
            'required'    => '',
            'default'     => '9',
            'addition'    => '-11 ～ 14',
            'example'     => '9',
            ),
        "enc" => array(
            'name'        => 'API出力結果のエンコード',
            'type'        => 'string(64)', //･･･ここは適当
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'null: エンコードなし / md5: MD5で暗号化 / sha1: SHA1で暗号化',
            'example'     => '',
            ),
        "output_type" => array(
            'name'        => 'API出力形式',
            'type'        => 'string(64)', //･･･ここは適当
            'restriction' => '',
            'required'    => '',
            'default'     => 'xml',
            'addition'    => 'xml: XML形式 / json：JSON形式 / php: PHPシリアライズ / yaml: YAML',
            'example'     => 'xml',
            ),
        // 検索オプション共通パラメータ
        "limit" => array(
            'name'        => '検索データ取得行数',
            'type'        => 'int(10)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'レコード取得制限件数',
            'example'     => '20',
            ),
        "page" => array(
            'name'        => 'ページ番号',
            'type'        => 'int(10)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '表示するページの表示番号',
            'example'     => '2',
            ),
        "sort_key" => array(
            'name'        => '検索データソートキー',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'ソート対象の項目ID',
            'example'     => '',
            ),
        "sort_type" => array(
            'name'        => '検索データソート順',
            'type'        => 'string(4)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'asc: 昇順 / desc: 降順',
            'example'     => 'asc',
            ),
        );
    function __construct($host, $version) {
        $this->version = $version;
        $this->sess_id = N2MY_SESSION;
        $this->param_proparty[N2MY_SESSION] = array(
                'name'        => 'ミーティングセッションID',
                'type'        => 'string(255)',
                'restriction' => '',
                'required'    => '必須',
                'default'     => '',
                'addition'    => 'API認証用セッションID',
                'example'     => 'l6teg8lv9rh35iemee2cjoq7b0',
            );
        $this->method_list = array();
        $this->base_url = $host;
    }

    function send($path, $method, $_param, $token = "", $title = "", $info = "", $param_proparty = array(), $override = false) {
        $param[$method] = "";
        if ($token) {
            $param[$this->sess_id] = $token;
        }
        foreach($_param as $key => $val) {
            $param[$key] = $val;
        }
        $start = $this->_get_passage_time();
        $url = $this->base_url."/api/".$this->version."/".$path;
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $param,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 20,
            );
        curl_setopt_array($ch, $option);
        $contents = $ret = curl_exec($ch);
        $time = $this->_get_passage_time() - $start;
        $ret = unserialize($contents);

        $query_str = "";
        $param["output_type"] = "xml";
        foreach ($param as $_key => $_value) {
            $post_data[$_key] .= $_value;
            $query_str .= "&" . $_key . "=" . urlencode($_value);
        }
        if ($query_str) {
            $query_str = "?" . substr($query_str, 1);
        }
        print "<li>".$title."<br/><font color=993322>".$method."</font>";
        print ' [ <a href="/api/reference/index.php?action_method=&path='."api/".$this->version."/".$path.'&method='.$method.'" target="_blank">リファレンス</a> ] ( <a href="'.$url.$query_str.'" target="_blank">結果表示</a> ) ';
        if ($info) {
            print "<br/><font color='gray'>".nl2br($info)."</font>";
        }
        if (!$ret) {
            print "<br /><font color='red'>実行エラー</font>";
        } elseif ($ret["status"] != "1") {
            print "<br /><font color='red'>パラメタエラー</font>";
        }

        $parse_url = parse_url($url);
        $parse_url["query"] = $post_data;

        require_once("lib/pear/XML/Serializer.php");
        $serializer = new XML_Serializer();
        $serializer->setOption('mode','simplexml');
        $serializer->setOption('encoding','UTF-8');
        $serializer->setOption('addDecl','ture');
        $serializer->setOption('rootName','result');
        $serializer->serialize($ret);
        $output = $serializer->getSerializedData();

        /**
         * pukiwiki用のコードをログにはく
         */
        $target = "z".substr(md5($path.$method), 0, 7);
        if (!in_array($target, $this->method_list) || ($override == true)) {
            $this->method_list[] = $target;
            $this->log .= <<<EOM
*** $title  ( $method ) [#$target]
- 概要
 $info

- メソッド ( GET / POST )
 $method

- パラメタ
|引数|名称|型|制約|必須|デフォルト値|備考|例|h

EOM;
            $param_proparty = array_merge($param_proparty, $this->param_proparty);
            $param_proparty[$method] = array(
                'name'        => 'メソッド',
                'type'        => '-',
                'restriction' => '',
                'required'    => '必須',
                'default'     => '',
                'addition'    => '',
            );
            foreach($post_data as $key => $val) {
                $_name = $param_proparty[$key]['name'];
                $_type = $param_proparty[$key]['type'];
                $_restriction = $param_proparty[$key]['restriction'];
                $_require = $param_proparty[$key]['required'];
                $_default = $param_proparty[$key]['default'];
                $_addition = $param_proparty[$key]['addition'];
                $this->log .= "|$key|$_name|$_type|$_restriction|$_require|$_default|$_addition|$val|"."\n";
            }
            $this->log .= "\n";
            $this->log .= "- サンプルレスポンス"."\n";
            $this->log .= " ".str_replace("\n", "\n ", $output)."\n";
            $this->log .= "\n";
            $this->log .= "- エラーコード表"."\n";
            $error_list = array(
                    "1"    => "Login failed / Invalid auth token",
                    "2"    => "Method not found",
                    "100"  => "Parameter error",
                    "101"  => "Object not found",
                    "1000" => "DataBase error",
                );
            foreach($error_list as $key => $val) {
                $this->log .= ":$key|$val"."\n";
            }
            $this->log .= "----"."\n\n";
            $this->log .= "\n";

            $xml = '        <method name="'.$title.'" method="'.$method.'">'."\n";
            $xml .= '            <desc><![CDATA['.$info.']]></desc>'."\n";
            $xml .= '            <params>'."\n";
            foreach($post_data as $key => $val) {
                $_name = htmlspecialchars($param_proparty[$key]['name']);
                $_type = htmlspecialchars($param_proparty[$key]['type']);
                $_restriction = htmlspecialchars($param_proparty[$key]['restriction']);
                $_require = ($param_proparty[$key]['required']) ? 1 : 0;
                $_default = htmlspecialchars($param_proparty[$key]['default']);
                $_addition = htmlspecialchars($param_proparty[$key]['addition']);
                $_example = htmlspecialchars($param_proparty[$key]['example']);
                $xml .= '                <param' .
                        ' id="'.$key.'"' .
                        ' name="'.$_name.'"' .
                        ' type="'.$_type.'"' .
                        ' restriction="'.$_restriction.'"' .
                        ' required="'.$_require.'"' .
                        ' default="'.$_default.'"' .
                        ' addition="'.$_addition.'"' .
                        ' example="'.htmlspecialchars($_example).'" />'."\n";
            }
            $xml .= '            </params>'."\n";
            $xml .= '            <errors>'."\n";
            foreach($error_list as $key => $val) {
                $xml .= '                <error cd="'.$key.'" msg="'.$val.'" />'."\n";
            }
            $xml .= '            </errors>'."\n";
            $xml .= '        </method>'."\n";
            $this->xml[$path][$method][] = $xml;
            // レスポンス仕様
            $response_xml = '        <method name="'.$title.'" method="'.$method.'">'."\n";
            $response_xml .= '            <response>'."\n";
            $unserializer = new XML_Unserializer();
            $unserializer->unserialize($output);
            $result1 = $unserializer->getUnserializedData();
            $response_xml .= $this->parse($result1, 0);
            $response_xml .= '            </response>'."\n";
            $response_xml .= '            <example><![CDATA['.$output.']]></example>'."\n";
            $response_xml .= '        </method>'."\n";
            $this->response_xml[$path][$method][] = $response_xml;
        }
        $input = array(
            "method"    => $method,
            "session"   => $token,
            "parameter" => $_param,
            );
        $result = array("Input" => $input, "Output" => $ret, "処理時間" => $time. " / ". $this->_get_passage_time());
        if ($_REQUEST["debug"] or !$ret["status"]) {
            new dBug(array("result" => $result));
        } else {
            new dBug(array("result" => $result), "", true);
        }
        return $ret;
    }

    function parse($data, $indent = 0) {
        static $result;
        if (!$indent) {
            $result = "";
        }
        $space = "                ".str_repeat(" ", $indent * 4);
        $sw = 0;
        foreach ($data as $key => $val) {
            if (is_array($val)) {
                if (!is_numeric($key)) {
                    $result .= $space.'<'.$key .' name="" info="" example="">'."\n";
                    $indent++;
                }
                if ($sw == 0) {
                    $this->parse($val, $indent);
                }
                if (!is_numeric($key)) {
                    $result .= $space.'</'.$key .'>'."\n";
                } else {
                    $sw = 1;
                }
            } else {
                if (!is_numeric($key)) {
                    $result .= $space.'<'.$key .' name="" info="" example="'.htmlspecialchars($val).'" />'."\n";
                }
            }
        }
        return $result;
    }

    function add_log($msg) {
        $this->log .= $msg;
    }

    function add_group($id, $name, $desc) {
        $this->group[$id]["name"] = $name;
        $this->group[$id]["desc"] = $desc;
    }

    /**
     *
     */
    function outputLog() {
        // wiki用
        // file_put_contents("../../logs/service_api.log", $this->log);
        // メソッド、入力、エラー情報
        @mkdir(N2MY_APP_DIR."/logs/api/".$this->version, 0777, true);
        $xml = '<api_list>'."\n";
        foreach ($this->xml as $key => $group) {
            $xml .= '    <group name="'.$this->group[$key]["name"].'" path="'.$key.'">' ."\n";
            $xml .= '    <desc><![CDATA['.$this->group[$key]["desc"].']]></desc>'."\n";
            foreach ($group as $methods) {
                foreach ($methods as $method) {
                    $xml .= $method;
                }
            }
            $xml .= '    </group>'."\n";
        }
        $xml .= '</api_list>';
        //new dBug($xml, "xml", true);
        file_put_contents(N2MY_APP_DIR."/logs/api/".$this->version."/api_info.xml", $xml);

        // レスポンス情報
        $xml = '<api_list>'."\n";
        foreach ($this->response_xml as $key => $group) {
            $xml .= '    <group name="'.$this->group[$key]["name"].'" path="'.$key.'">' ."\n";
            foreach ($group as $methods) {
                foreach ($methods as $method) {
                    $xml .= $method;
                }
            }
            $xml .= '    </group>'."\n";
        }
        $xml .= '</api_list>';
        //new dBug($xml, "xml", true);
        file_put_contents(N2MY_APP_DIR."/logs/api/".$this->version."/response_info.xml", $xml);
        print "APIの仕様書をログファイルに出力しました。";
    }

    /**
     * 経過時間取得
     */
    function _get_passage_time($reset = false) {
        static $start_time;
        if (!isset($start_time) || $reset == true) {
            list($micro, $sec) = split(" ", microtime());
            $start_time = $micro + $sec - 0.00001;
        }
        list($micro, $sec) = split(" ", microtime());
        $passage_time = ($micro + $sec) - $start_time;
        return $passage_time;
    }
}
