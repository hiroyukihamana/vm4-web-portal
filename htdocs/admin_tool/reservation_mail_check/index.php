<?php
require_once ("classes/AppFrame.class.php");
require_once ("classes/N2MY_DBI.class.php");
require_once ("Auth.php");
require_once ("Pager.php");
require_once ("config/config.inc.php");
require_once ("classes/dbi/ives_setting.dbi.php");
require_once ("classes/N2MY_IvesClient.class.php");
require_once ("classes/mgm/dbi/mcu_server.dbi.php");
class mailFrame extends AppFrame {
	var $_auth = null;
	var $_sess_page;
	var $account_dsn = null;
	var $session = null;
	var $language_array = array ();
	var $data_dsn = null;
	function init() {
		ini_set ( "display_errors", 1 );
		$_COOKIE ["lang"] = "en";
		$this->session = EZSession::getInstance ();
		$this->session->set ( "lang", "en" );
		$this->account_dsn = $this->config->get ( "GLOBAL", "auth_dsn" );
		$language_base_url = N2MY_APP_DIR . "templates/";
		$unNeedFolder = array (
				"common",
				"custom",
				".",
				"..",
				".svn",
				"ja"
		);
		$language_folder = scandir ( $language_base_url );
		$this->language_array = array_diff ( $language_folder, $unNeedFolder );
	}
	function set_dsn($server_key = 1) {
		$account_db = new N2MY_DB ( $this->account_dsn, "db_server" );
		$server_list = $account_db->getRowsAssoc ( "server_status = 1" );
		if (count ( $server_list ) == 1) {
			$this->data_dsn = $server_list [0] ["dsn"];
		} elseif (count ( $server_list ) > 1) {
			foreach ( $server_list as $server ) {
				if ($server ['server_key'] == $server_key) {
					$this->data_dsn = $server ["dsn"];
					break;
				}
			}
		}
	}
	function get_dsn() {
		if (! isset ( $this->data_dsn )) {
			$this->set_dsn ();
		}
		return $this->data_dsn;
	}
	function auth() {
		$auth_detail = "*";
		$this->_auth = new Auth ( "DB", array (
				"dsn" => $this->account_dsn,
				"table" => "staff",
				"cryptType" => "sha1",
				"db_fields" => $auth_detail,
				"usernamecol" => "login_id",
				"passwordcol" => "login_password"
		) );

		if (! $this->_auth->getAuth ()) {

			$this->_auth->start ();
			$userWithoutAdminAuth = isset ( $_SESSION ["_authsession"] ["data"] ["authority"] ) && ($_SESSION ["_authsession"] ["data"] ["authority"] != 2);
			$userNotEnable = ($_SESSION ["_authsession"] ["data"] ["status"] == "0");
			if ($userNotEnable || $userWithoutAdminAuth) {
				$this->_auth->logout ();
				$this->request->set ( "password", "" );
				$this->_auth->start ();
			}
			if ($this->_auth->checkAuth ()) {

				// 操作ログ登録
				$operation_data = array (
						"staff_key" => $_SESSION ["_authsession"] ["data"] ["staff_key"],
						"action_name" => "login",
						"table_name" => "staff",
						"keyword" => $_SESSION ["_authsession"] ["username"],
						"info" => "login",
						"operation_datetime" => date ( "Y-m-d H:i:s" )
				);
				$this->add_operation_log ( $operation_data );
			} else {
				exit ();
			}
		} else {
		}
	}
	function add_operation_log($operation_data) {
		$this->logger->debug ( "operation_log", __FILE__, __LINE__, $operation_data );
		$operation_db = new N2MY_DB ( $this->account_dsn, "operation_log" );
		$operation_data ["create_datetime"] = date ( "Y-m-d H:i:s" );
		$add_data = $operation_db->add ( $operation_data );
		if (DB::isError ( $add_data )) {
			$this->logger2->error ( $add_data->getUserInfo () );
			return false;
		}
		return true;
	}
	function default_view() {
		return $this->action_top ();
	}
	function _display($template) {
		$staff_auth = $_SESSION ["_authsession"] ["data"] ["authority"];
		$this->template->assign ( "authority", $staff_auth );
		$this->display ( $template );
	}

	/*
	 * 「common mail temlate」を得る
	 */
	function action_get_meeting_template() {
		$language = $this->request->get ( 'language' );
		$category = $this->request->get ( 'category' );
		$type = $this->request->get ( 'type' );
		$multi_lang_flag = $this->request->get ( 'show_multi_lang_flag' );
		$mail_path = null;
		if ($this->request->get ( 'require_html' ) == 'true') {
			$config = EZConfig::getInstance ();
			$template = new EZTemplate ( $config->getAll ( 'SMARTY_DIR' ) );
			$frame ['lang'] = $language;
			$template->assign ( "__frame", $frame );
			$template->assign ( "base_url", N2MY_BASE_URL );
			if (substr ( strtolower ( $category ), 12, strlen ( $category ) ) == 'receiver') {
				$mail_path = N2MY_APP_DIR . "templates/common/mail/reservation/user/reservation_" . $type . ".t.html";
			} else if (substr ( strtolower ( $category ), 12, strlen ( $category ) ) == 'sender') {
				$mail_path = N2MY_APP_DIR . "templates/common/mail/reservation_" . $type . ".t.html";
			}
			$mail_content = $template->fetch ( $mail_path );
		} else {
			if (strtolower ( $category ) == 'meeting_use') {
				if ($multi_lang_flag == 'false') {
					$mail_path = N2MY_APP_DIR . "templates/" . $language . "/mail/" . $type . ".t.txt";
				} else {
					$mail_path = N2MY_APP_DIR . "templates/common/mail_template/meeting/" . $type . ".t.txt";
				}
			} else if (substr ( strtolower ( $category ), 12, strlen ( $category ) ) == 'receiver') {
				if ($multi_lang_flag == 'false') {
					$mail_path = N2MY_APP_DIR . "templates/" . $language . "/mail/reservation/user/reservation_" . $type . ".t.txt";
				} else {
					$mail_path = N2MY_APP_DIR . "templates/common/mail_template/meeting/reservation/user/reservation_" . $type . ".t.txt";
				}
			} else if (substr ( strtolower ( $category ), 12, strlen ( $category ) ) == 'sender') {
				if ($multi_lang_flag == 'false') {
					$mail_path = N2MY_APP_DIR . "templates/" . $language . "/mail/reservation_" . $type . ".t.txt";
				} else {
					$mail_path = N2MY_APP_DIR . "templates/common/mail_template/meeting/reservation_" . $type . ".t.txt";
				}
			} else {
				if ($multi_lang_flag == 'false') {
					$mail_path = N2MY_APP_DIR . "templates/" . $language . "/mail/" . $category . "/" . $type . ".t.txt";
				} else {
					$mail_path = N2MY_APP_DIR . "templates/common/mail_template/meeting/" . $category . "/" . $type . ".t.txt";
				}
			}
			$this->logger2->debug ( $mail_path );
			$mail_content = @file_get_contents ( $mail_path );
			if ($mail_content === false) {
				$mail_content = "template doesn't exist";
			}
		}
		echo htmlspecialchars ( $mail_content );
	}
	function action_get_sales_template() {
		$language = $this->request->get ( 'language' );
		$category = $this->request->get ( 'category' );
		$type = $this->request->get ( 'type' );
		$multi_lang_flag = $this->request->get ( 'show_multi_lang_flag' );
		$mail_path = null;
		if (substr ( strtolower ( $category ), 12, strlen ( $category ) ) == 'receiver') {
			if ($multi_lang_flag == 'false') {
				$mail_path = N2MY_APP_DIR . "templates/custom/sales/" . $language . "/mail/reservation/user/reservation_" . $type . ".t.txt";
			} else {
				$mail_path = N2MY_APP_DIR . "templates/custom/sales/common/multi_lang/reservation/user/reservation_" . $type . ".t.txt";
			}
		} else {
			if ($multi_lang_flag == 'false') {
				$mail_path = N2MY_APP_DIR . "templates/custom/sales/" . $language . "/mail/reservation_" . $type . ".t.txt";
			} else {
				$mail_path = N2MY_APP_DIR . "templates/custom/sales/common/multi_lang/reservation_" . $type . ".t.txt";
			}
		}
		if ($type == 'invitation'){
			if ($multi_lang_flag == 'false') {
				$mail_path = N2MY_APP_DIR . "templates/custom/sales/" . $language . "/mail/invitation.t.txt";
			} else {
				$mail_path = N2MY_APP_DIR . "templates/custom/sales/common/multi_lang/invitation.t.txt";
			}
		}
		$this->logger2->debug ( $mail_path );
		$mail_content = @file_get_contents ( $mail_path );
		if ($mail_content === false) {
			$mail_content = "template doesn't exist";
		}
		echo htmlspecialchars ( $mail_content );
	}

	/*
	 * 「custom mail temlate」を得る
	 */
	function action_get_custom_template() {
		$folder = $this->request->get ( 'folder' );
		$language = $this->request->get ( 'language' );
		$category = $this->request->get ( 'category' );
		$type = $this->request->get ( 'type' );
		$mail_path = null;
		if (strtolower ( $category ) == 'meeting_use') {
			$mail_path = N2MY_APP_DIR . "templates/custom/" . $folder . "/" . $language . "/mail/" . $type . ".t.txt";
		} else if (substr ( strtolower ( $category ), 12, strlen ( $category ) ) == 'receiver') {
			$mail_path = N2MY_APP_DIR . "templates/custom/" . $folder . "/" . $language . "/mail/reservation/user/reservation_" . $type . ".t.txt";
		} else if (substr ( strtolower ( $category ), 12, strlen ( $category ) ) == 'sender') {
			$mail_path = N2MY_APP_DIR . "templates/custom/" . $folder . "/" . $language . "/mail/reservation_" . $type . ".t.txt";
		} else {
			$mail_path = N2MY_APP_DIR . "templates/custom/" . $folder . "/" . $language . "/mail/" . $category . "/" . $type . ".t.txt";
		}
		$this->logger->debug ( "mail_path", __FILE__, __LINE__, $mail_path );
		$mail_content = @file_get_contents ( $mail_path );
		if ($mail_content === false) {
			$mail_content = "template doesn't exist";
		}
		echo $mail_content;
	}

	/*
	 * 「common reservation mail」を得る
	 */
	function action_get_meeting_reservation_mail() {
		$reservation_key = $this->request->get ( 'reservation_key' );
		$type = $this->request->get ( 'type' );
		$character = $this->request->get ( 'character' );
		$language = $this->request->get ( 'language' );
		$source = $this->request->get ( 'source' );
		$config = EZConfig::getInstance ();
		$template = new EZTemplate ( $config->getAll ( 'SMARTY_DIR' ) );
		$frame ['lang'] = $language;
		$template->assign ( "__frame", $frame );
		switch ($source) {
			case "html" :
				if ($character == 'receiver') {
					$template_file = N2MY_APP_DIR . "templates/common/mail/reservation/user/reservation_" . $type . ".t.html";
				} else {
					$template_file = N2MY_APP_DIR . "templates/common/mail/reservation_" . $type . ".t.html";
				}
				$result = $this->get_mail_content ( $reservation_key, $template_file, $template, $config, $language );
				echo $result;
				break;
			case "multi" :
				if ($character == 'receiver') {
					$template_file = N2MY_APP_DIR . "templates/common/mail_template/meeting/reservation/user/reservation_" . $type . ".t.txt";
				} else {
					$template_file = N2MY_APP_DIR . "templates/common/mail_template/meeting/reservation_" . $type . ".t.txt";
				}
				$result = $this->get_mail_content ( $reservation_key, $template_file, $template, $config, $language );
				echo htmlspecialchars ( $result );
				break;
			case "text" :
				if ($character == 'receiver') {
					$template_file = N2MY_APP_DIR . "templates/" . $language . "/mail/reservation/user/reservation_" . $type . ".t.txt";
				} else {
					$template_file = N2MY_APP_DIR . "templates/" . $language . "/mail/reservation_" . $type . ".t.txt";
				}
				$result = $this->get_mail_content ( $reservation_key, $template_file, $template, $config, $language );
				echo htmlspecialchars ( $result );
		}

		$this->logger2->info ( $template_file );
	}
	function action_get_sales_reservation_mail() {
		$reservation_key = $this->request->get ( 'reservation_key' );
		$type = $this->request->get ( 'type' );
		$character = $this->request->get ( 'character' );
		$language = $this->request->get ( 'language' );
		$config = EZConfig::getInstance ();
		$template = new EZTemplate ( $config->getAll ( 'SMARTY_DIR' ) );
		$frame ['lang'] = $language;
		$template->assign ( "__frame", $frame );
		if ($character == 'receiver') {
			$template_file = N2MY_APP_DIR . "templates/custom/sales/common/multi_lang/reservation/user/reservation_" . $type . ".t.txt";
		} else {
			if ($type == 'invitation') {
				$template_file = N2MY_APP_DIR . "templates/custom/sales/common/multi_lang/invitation.t.txt";
			} else {
				$template_file = N2MY_APP_DIR . "templates/custom/sales/common/multi_lang/reservation_" . $type . ".t.txt";
			}
		}
		$this->logger2->info ( $template_file );
		$result = $this->get_mail_content ( $reservation_key, $template_file, $template, $config, $language );
		echo htmlspecialchars ( $result );
	}
	/*
	 * 「custom reservation mail」を得る
	 */
	function action_get_custom_reservation_mail() {
		$reservation_key = $this->request->get ( 'reservation_key' );
		$type = $this->request->get ( 'type' );
		$character = $this->request->get ( 'character' );
		$language = $this->request->get ( 'language' );
		$folder = $this->request->get ( 'folder' );
		$config = EZConfig::getInstance ();
		$template = new EZTemplate ( $config->getAll ( 'SMARTY_DIR' ) );
		if ($character == 'receiver') {
			$template_file = N2MY_APP_DIR . "templates/custom/" . $folder . "/" . $language . "/mail/reservation/user/reservation_" . $type . ".t.txt";
		} else {
			$template_file = N2MY_APP_DIR . "templates/custom/" . $folder . "/" . $language . "/mail/reservation_" . $type . ".t.txt";
		}
		if (! is_file ( $template_file )) {
			echo "no match template file";
			return;
		}
		$result = $this->get_mail_content ( $reservation_key, $template_file, $template, $config, $language );
		echo $result;
	}

	/*
	 * 「in-meeting invitation and sharedmemo mail」を得る
	 */
	function action_get_invitation_mail() {
		$meeting_key = $this->request->get ( 'meeting_key' );
		$type = $this->request->get ( 'type' );
		$language = $this->request->get ( 'language' );
		require_once ("classes/dbi/meeting.dbi.php");
		$meeting_table = new MeetingTable ( $this->get_dsn () );
		$where = "meeting_key = " . $meeting_key;
		$meeting = $meeting_table->getRow ( $where );
		if (! $meeting) {
			echo "meeting doesn't exist";
			return;
		}
		$config = EZConfig::getInstance ();
		$template = new EZTemplate ( $config->getAll ( 'SMARTY_DIR' ) );
		if ($type == 'invitation') {
			$template_file = N2MY_APP_DIR . "templates/common/mail_template/meeting/invitation.t.txt";
		} else {
			$template_file = N2MY_APP_DIR . "templates/common/mail_template/meeting/sharedmemo.t.txt";
		}
		if (! is_file ( $template_file )) {
			echo "no match template file";
			return;
		}
		$base_url = N2MY_BASE_URL;
		$template->assign ( "base_url", $base_url );
		$meeting_info = array (
				'pin_cd' => $meeting ['pin_cd']
		);
		$info = array (
				'guest_url_format' => 1
		);
		if ($meeting ['is_reserved']) {
			require_once ("classes/dbi/reservation.dbi.php");
			$reservation_table = new ReservationTable ( $this->get_dsn () );
			$where = "meeting_key = '" . mysql_real_escape_string ( $meeting ['meeting_ticket'] ) . "'";
			$reservation_info = $reservation_table->getRow ( $where );
			if (isset ( $reservation_info ['reservation_pw'] )) {
				$info ['reservation_pw'] = $reservation_info ['reservation_pw'];
				$template->assign ( "reservationPassword", $reservation_info ['reservation_pw'] );
			}
		}
		$template->assign ( "meeting_info", $meeting_info );
		$template->assign ( "info", $info );

		require_once ("classes/dbi/ordered_service_option.dbi.php");
		$teleconf_info = null;
		$service_option = new OrderedServiceOptionTable ( $this->get_dsn () );
		$enable_teleconf = $service_option->enableOnRoom ( 23, $meeting ["room_key"] ); // 23 = teleconf

		if (! $config->get ( "IGNORE_MENU", "teleconference" ) && $enable_teleconf) {
			require_once ("classes/pgi/PGiPhoneNumber.class.php");
			if ($meeting ['use_pgi_dialin'] || $meeting ['use_pgi_dialin_free']) {
				$main_phone_number = unserialize ( $meeting ['pgi_phone_numbers'] );
				$main_phone_number = PGiPhoneNumber::toMultilangLocationName ( $main_phone_number, $language );
				if (! $meeting ['use_pgi_dialin']) {
					$main_phone_number = PGiPhoneNumber::extractFreeMain ( $main_phone_number );
				} else {
					$main_phone_number = PGiPhoneNumber::extractLocalMain ( $main_phone_number );
				}
			}
			$teleconf_info = array (
					'tc_type' => $meeting ['tc_type'],
					'pgi_service_name' => $meeting ['pgi_service_name'],
					'pgi_conference_id' => $meeting ['pgi_conference_id'],
					'pgi_m_pass_code' => $meeting ['pgi_m_pass_code'],
					'pgi_p_pass_code' => $meeting ['pgi_p_pass_code'],
					'pgi_l_pass_code' => $meeting ['pgi_l_pass_code'],
					'main_phone_number' => $main_phone_number,
					'phone_number_url' => $base_url . 'p/' . $meeting ['meeting_session_id'],
					'tc_teleconf_note' => $meeting ['tc_teleconf_note'],
					"use_pgi_dialin" => $meeting ["use_pgi_dialin"],
					"use_pgi_dialin_free" => $meeting ["use_pgi_dialin_free"],
					"use_pgi_dialout" => $meeting ["use_pgi_dialout"]
			);
		}
		$tel_no = $config->getAll ( "TELEPHONE" );
		$config_file = N2MY_APP_DIR . "config/lang/" . $language . "/message.ini";
		$message [$language] = parse_ini_file ( $config_file, true );
		$location_list = $message [$language] ["TELEPHONE"];
		$telephonedata [$language] = array ();
		foreach ( $location_list as $key => $location ) {
			$telephonedata [$language] [$key] ["name"] = $location;
			$telephonedata [$language] [$key] ["list"] = split ( ",", $tel_no [$key] );
		}
		$telephone_data = $telephonedata [$language];
		$template->assign ( "telephone_data", $telephone_data );
		$template->assign ( "teleconf_info", $teleconf_info );
		require_once ("classes/mcu/resolve/Resolver.php");
		if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey ( $meeting ["room_key"] )) && $meeting ["temporary_did"]) {
			$guestSipNumberAddress = $videoConferenceClass->getSipTemporaryNumberAddress ( $meeting ["temporary_did"] );
			$temporarySipAddress = $videoConferenceClass->getSipTemporaryAddress ( $meeting ["temporary_did"] );
			$guestH323NumberAddress = $videoConferenceClass->getH323TemporaryNumberAddress ( $meeting ["temporary_did"] );
			$temporaryH323Address = $videoConferenceClass->getH323TemporaryAddress ( $meeting ["temporary_did"] );
			if ($guestSipNumberAddress)
				$template->assign ( "temporarySipNumberAddress", $guestSipNumberAddress );
			if ($temporarySipAddress)
				$template->assign ( "temporarySipAddress", $temporarySipAddress );
			if ($guestH323NumberAddress)
				$template->assign ( "temporaryH323NumberAddress", $guestH323NumberAddress );
			if ($temporaryH323Address)
				$template->assign ( "temporaryH323Address", $temporaryH323Address );
		}

		require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
		$lang = EZLanguage::getLang ( $language );
		require_once "classes/dbi/meeting_invitation.dbi.php";
		$objMeetingInvitationTable = new MeetingInvitationTable ( $this->get_dsn () );
		$where = "meeting_key = " . $meeting_key;
		$invitationData = $objMeetingInvitationTable->getRow ( $where );
		require_once ("classes/dbi/user.dbi.php");
		$user_table = new UserTable ( $this->get_dsn () );
		$where = "user_key = " . $meeting ["user_key"];
		$user_info = $user_table->getRow ( $where );
		if ($user_info ["login_url"]) {
			$invitation_url = sprintf ( "%sg/%s/%s/%s", $user_info ["login_url"], $lang, $meeting ['meeting_session_id'], $invitationData ['user_session_id'] );
		} else {
			$invitation_url = sprintf ( "%sg/%s/%s/%s", N2MY_BASE_URL, $lang, $meeting ['meeting_session_id'], $invitationData ['user_session_id'] );
		}
		$template->assign ( "invitation_url", $invitation_url );
		$template->assign ( "meeting_name", $meeting ['meeting_name'] );
		$template->assign ( "meeting_start_datetime", $meeting ['meeting_start_datetime'] );
		$mail_body = $template->fetch ( $template_file );
		echo htmlspecialchars ( $mail_body );
	}

	function action_get_custom_invitation_mail() {
		$meeting_key = $this->request->get ( 'meeting_key' );
		$type = $this->request->get ( 'type' );
		$language = $this->request->get ( 'language' );
		$folder = $this->request->get ( 'folder' );
		require_once ("classes/dbi/meeting.dbi.php");
		$meeting_table = new MeetingTable ( $this->get_dsn () );
		$where = "meeting_key = " . $meeting_key;
		$meeting = $meeting_table->getRow ( $where );
		if (! $meeting) {
			echo "meeting doesn't exist";
			return;
		}
		$config = EZConfig::getInstance ();
		$template = new EZTemplate ( $config->getAll ( 'SMARTY_DIR' ) );
		if ($type == 'invitation') {
			$template_file = N2MY_APP_DIR . "templates/custom/" . $folder . "/" . $language . "/mail/invitation.t.txt";
		} else {
			$template_file = N2MY_APP_DIR . "templates/custom/" . $folder . "/" . $language . "/mail/invitation.t.txt";
		}
		if (! is_file ( $template_file )) {
			echo "no match template file";
			return;
		}
		$base_url = N2MY_BASE_URL;
		$template->assign ( "base_url", $base_url );
		$meeting_info = array (
				'pin_cd' => $meeting ['pin_cd']
		);
		$info = array (
				'guest_url_format' => 1
		);
		if ($meeting ['is_reserved']) {
			require_once ("classes/dbi/reservation.dbi.php");
			$reservation_table = new ReservationTable ( $this->get_dsn () );
			$where = "meeting_key = '" . mysql_real_escape_string ( $meeting ['meeting_ticket'] ) . "'";
			$reservation_info = $reservation_table->getRow ( $where );
			if (isset ( $reservation_info ['reservation_pw'] )) {
				$info ['reservation_pw'] = $reservation_info ['reservation_pw'];
				$template->assign ( "reservationPassword", $reservation_info ['reservation_pw'] );
			}
		}
		$template->assign ( "meeting_info", $meeting_info );
		$template->assign ( "info", $info );

		require_once ("classes/dbi/ordered_service_option.dbi.php");
		$teleconf_info = null;
		$service_option = new OrderedServiceOptionTable ( $this->get_dsn () );
		$enable_teleconf = $service_option->enableOnRoom ( 23, $meeting ["room_key"] ); // 23 = teleconf

		if (! $config->get ( "IGNORE_MENU", "teleconference" ) && $enable_teleconf) {
			require_once ("classes/pgi/PGiPhoneNumber.class.php");
			if ($meeting ['use_pgi_dialin'] || $meeting ['use_pgi_dialin_free']) {
				$main_phone_number = unserialize ( $meeting ['pgi_phone_numbers'] );
				$main_phone_number = PGiPhoneNumber::toMultilangLocationName ( $main_phone_number, $language );
				if (! $meeting ['use_pgi_dialin']) {
					$main_phone_number = PGiPhoneNumber::extractFreeMain ( $main_phone_number );
				} else {
					$main_phone_number = PGiPhoneNumber::extractLocalMain ( $main_phone_number );
				}
			}
			$teleconf_info = array (
					'tc_type' => $meeting ['tc_type'],
					'pgi_service_name' => $meeting ['pgi_service_name'],
					'pgi_conference_id' => $meeting ['pgi_conference_id'],
					'pgi_m_pass_code' => $meeting ['pgi_m_pass_code'],
					'pgi_p_pass_code' => $meeting ['pgi_p_pass_code'],
					'pgi_l_pass_code' => $meeting ['pgi_l_pass_code'],
					'main_phone_number' => $main_phone_number,
					'phone_number_url' => $base_url . 'p/' . $meeting ['meeting_session_id'],
					'tc_teleconf_note' => $meeting ['tc_teleconf_note'],
					"use_pgi_dialin" => $meeting ["use_pgi_dialin"],
					"use_pgi_dialin_free" => $meeting ["use_pgi_dialin_free"],
					"use_pgi_dialout" => $meeting ["use_pgi_dialout"]
			);
		}
		$tel_no = $config->getAll ( "TELEPHONE" );
		$config_file = N2MY_APP_DIR . "config/lang/" . $language . "/message.ini";
		$message [$language] = parse_ini_file ( $config_file, true );
		$location_list = $message [$language] ["TELEPHONE"];
		$telephonedata [$language] = array ();
		foreach ( $location_list as $key => $location ) {
			$telephonedata [$language] [$key] ["name"] = $location;
			$telephonedata [$language] [$key] ["list"] = split ( ",", $tel_no [$key] );
		}
		$telephone_data = $telephonedata [$language];
		$template->assign ( "telephone_data", $telephone_data );
		$template->assign ( "teleconf_info", $teleconf_info );
		require_once ("classes/mcu/resolve/Resolver.php");
		if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey ( $meeting ["room_key"] )) && $meeting ["temporary_did"]) {
			$guestSipNumberAddress = $videoConferenceClass->getSipTemporaryNumberAddress ( $meeting ["temporary_did"] );
			$temporarySipAddress = $videoConferenceClass->getSipTemporaryAddress ( $meeting ["temporary_did"] );
			$guestH323NumberAddress = $videoConferenceClass->getH323TemporaryNumberAddress ( $meeting ["temporary_did"] );
			$temporaryH323Address = $videoConferenceClass->getH323TemporaryAddress ( $meeting ["temporary_did"] );
			if ($guestSipNumberAddress)
				$template->assign ( "temporarySipNumberAddress", $guestSipNumberAddress );
			if ($temporarySipAddress)
				$template->assign ( "temporarySipAddress", $temporarySipAddress );
			if ($guestH323NumberAddress)
				$template->assign ( "temporaryH323NumberAddress", $guestH323NumberAddress );
			if ($temporaryH323Address)
				$template->assign ( "temporaryH323Address", $temporaryH323Address );
		}

		require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
		$lang = EZLanguage::getLang ( $language );
		require_once "classes/dbi/meeting_invitation.dbi.php";
		$objMeetingInvitationTable = new MeetingInvitationTable ( $this->get_dsn () );
		$where = "meeting_key = " . $meeting_key;
		$invitationData = $objMeetingInvitationTable->getRow ( $where );
		require_once ("classes/dbi/user.dbi.php");
		$user_table = new UserTable ( $this->get_dsn () );
		$where = "user_key = " . $meeting ["user_key"];
		$user_info = $user_table->getRow ( $where );
		if ($user_info ["login_url"]) {
			$invitation_url = sprintf ( "%sg/%s/%s/%s", $user_info ["login_url"], $lang, $meeting ['meeting_session_id'], $invitationData ['user_session_id'] );
		} else {
			$invitation_url = sprintf ( "%sg/%s/%s/%s", N2MY_BASE_URL, $lang, $meeting ['meeting_session_id'], $invitationData ['user_session_id'] );
		}
		$template->assign ( "invitation_url", $invitation_url );
		$template->assign ( "meeting_name", $meeting ['meeting_name'] );
		$template->assign ( "meeting_start_datetime", $meeting ['meeting_start_datetime'] );
		$mail_body = $template->fetch ( $template_file );
		echo htmlspecialchars ( $mail_body );
	}
	/*
	 * 「commom template」を得る
	 */
	function action_top() {
		$this->template->assign ( "language_list", $this->language_array );
		$this->template->assign ( "language_json", json_encode ( $this->language_array ) );
		$custom_url = N2MY_APP_DIR . "templates/custom/";
		$custom_folder = scandir ( $custom_url );
		$custom_category = array_diff ( $custom_folder, array (
				".",
				"..",
				".svn"
		) );
		$this->template->assign ( "folder_list", $custom_category );
		$multi_lang_url = N2MY_APP_DIR . "templates/common/mail_template/meeting/";
		$multi_lang_category = scandir ( $multi_lang_url );
		$multi_lang_category_list = array ();
		$filter_array = array (
				".",
				"..",
				"reservation",
				".svn"
		);
		foreach ( $multi_lang_category as $temp_cate ) {
			if (is_dir ( $multi_lang_url . $temp_cate )) {
				if (! in_array ( $temp_cate, $filter_array )) {
					$multi_lang_category_list [] = $temp_cate;
				}
			}
		}
		$multi_lang_type_list = array ();
		foreach ( $multi_lang_category_list as $temp_cate ) {
			$temp_type_list = scandir ( $multi_lang_url . $temp_cate );
			foreach ( $temp_type_list as $temp_type ) {
				if (is_file ( $multi_lang_url . $temp_cate . "/" . $temp_type ) && ! in_array ( $temp_type, $filter_array )) {
					$multi_lang_type_list [$temp_cate] [] = substr ( $temp_type, 0, strlen ( $temp_type ) - 6 );
				}
			}
		}
		$multi_lang_category_list [] = "reservation:sender";
		$multi_lang_category_list [] = "reservation:receiver";
		$multi_lang_type_list ["reservation:sender"] = array (
				"create",
				"modify",
				"delete",
				"reminder"
		);
		$multi_lang_type_list ["reservation:receiver"] = array (
				'create',
				'modify',
				'delete',
				'deletelist',
				'reminder'
		);
		$this->template->assign ( "category1_json", json_encode ( $multi_lang_category_list ) );
		$this->template->assign ( "type1_json", json_encode ( $multi_lang_type_list ) );
		$template = "admin_tool/reservation_mail/detail.t.html";
		$this->_display ( $template );
	}

	/*
	 * 「common reservation」webpage を準備する
	 */
	function action_prepare_mailpage_fragment1() {
		$language = $this->request->get ( 'language' );
		$mail_root_path = N2MY_APP_DIR . "templates/" . $language . "/mail/";
		$mail_files = scandir ( $mail_root_path );
		$folders = array ();
		$typeOfCategory = array ();
		$meeting_use_mails = array ();
		$mail_files_length = count ( $mail_files );
		foreach ( $mail_files as $mail_file_name ) {
			if (is_dir ( $mail_root_path . $mail_file_name )) {
				if (! in_array ( $mail_file_name, array (
						".",
						"..",
						"reservation",
						".svn"
				) )) {
					array_push ( $folders, $mail_file_name );
				}
			} else {
				if (substr ( $mail_file_name, 0, 11 ) != "reservation") {
					array_push ( $meeting_use_mails, substr ( $mail_file_name, 0, strlen ( $mail_file_name ) - 6 ) );
				}
			}
		}

		$template_category = $folders;
		array_push ( $template_category, 'meeting_use' );
		array_push ( $template_category, 'reservation:sender' );
		array_push ( $template_category, 'reservation:receiver' );
		foreach ( $folders as $folder_name ) {
			$files_in_folder = scandir ( $mail_root_path . $folder_name );
			$base = $mail_root_path . $folder_name . "/";
			$filter = create_function ( '$file_name', 'return is_file(\'' . $base . '\'.$file_name);' );
			$typeOfCategory [$folder_name] = array_filter ( $files_in_folder, $filter );
			array_walk ( $typeOfCategory [$folder_name], array (
					$this,
					'removeFileSuffix'
			) );
		}
		$typeOfCategory ['meeting_use'] = $meeting_use_mails;
		$typeOfCategory ['reservation:sender'] = array (
				"create",
				"modify",
				"delete",
				"reminder"
		);
		$typeOfCategory ['reservation:receiver'] = array (
				'create',
				'modify',
				'delete',
				'deletelist',
				'reminder'
		);
		echo json_encode ( $template_category ) . "|" . json_encode ( $typeOfCategory );
	}

	/*
	 * 「custom reservation」webpage を準備する
	 */
	function action_prepare_mailpage_fragment3() {
		$language = $this->request->get ( 'language' );
		$mail_root_path = N2MY_APP_DIR . "templates/custom/sales/" . $language . "/mail/";
		$mail_files = scandir ( $mail_root_path );
		$folders = array ();
		$typeOfCategory = array ();
		$mail_files_length = count ( $mail_files );
		foreach ( $mail_files as $mail_file_name ) {
			if (is_dir ( $mail_root_path . $mail_file_name )) {
				if (! in_array ( $mail_file_name, array (
						".",
						"..",
						"reservation",
						".svn"
				) )) {
					array_push ( $folders, $mail_file_name );
				}
			}
		}
		$template_category = $folders;
		array_push ( $template_category, 'reservation:sender' );
		array_push ( $template_category, 'reservation:receiver' );
		foreach ( $folders as $folder_name ) {
			$files_in_folder = scandir ( $mail_root_path . $folder_name );
			$base = $mail_root_path . $folder_name . "/";
			$filter = create_function ( '$file_name', 'return is_file(\'' . $base . '\'.$file_name);' );
			$typeOfCategory [$folder_name] = array_filter ( $files_in_folder, $filter );
			array_walk ( $typeOfCategory [$folder_name], array (
					$this,
					'removeFileSuffix'
			) );
		}
		$typeOfCategory ['reservation:sender'] = array (
				"create",
				"modify",
				"delete",
				"reminder",
				"invitation"
		);
		$typeOfCategory ['reservation:receiver'] = array (
				'create',
				'modify',
				'delete',
				'reminder'
		);
		echo json_encode ( $template_category ) . "|" . json_encode ( $typeOfCategory );
	}
	function action_prepare_mailpage_fragment5() {
		$custom_folder = N2MY_APP_DIR . "templates/custom/" . $this->request->get ( 'custom_folder' );
		$custom_language_folder = scandir ( $custom_folder );

		$custom_language_support = array_filter ( $this->language_array, create_function ( '$lang', "return is_dir('$custom_folder/'.\$lang);" ) );

		if (! $custom_language_support) {
			echo "no files";
			return;
		} else {
			echo json_encode ( $custom_language_support );
		}
		$custom_language_support_with_content = array_filter ( $custom_language_support, create_function ( '$lang', "return is_dir('$custom_folder/'.\$lang.'/mail');" ) );
		$language_info = array ();
		foreach ( $custom_language_support_with_content as $custom_language_folder_name ) {
			$mail_path_of_lang = $custom_folder . "/" . $custom_language_folder_name . "/mail";
			$meeting_use_array = array ();
			$category_use_array = array ();
			$category_folders = scandir ( $mail_path_of_lang );
			foreach ( $category_folders as $category_folder_name ) {
				if (is_dir ( $mail_path_of_lang . "/" . $category_folder_name )) {
					if (! in_array ( $category_folder_name, array (
							".",
							"..",
							"reservation",
							".svn"
					) )) {
						$category_use_array [$category_folder_name] = null;
					}
				} else {
					if (substr ( $category_folder_name, 0, 11 ) != "reservation") {
						array_push ( $meeting_use_array, substr ( $category_folder_name, 0, strlen ( $category_folder_name ) - 6 ) );
					} else {
						$category_use_array ['reservation:sender'] = true;
					}
				}
			}
			if (in_array ( "reservation", $category_folders )) {
				$category_use_array ['reservation:receiver'] = true;
			}
			if ($category_use_array) {
				$language_info [$custom_language_folder_name] = $category_use_array;
			}
			if ($meeting_use_array) {
				$language_info [$custom_language_folder_name] ['meeting_use'] = $meeting_use_array;
			}
		}
		echo "|";
		echo json_encode ( $language_info );
		foreach ( $language_info as $language => $category_info ) {
			foreach ( $category_info as $category => $sub_value ) {
				$category_path = $custom_folder . "/" . $language . "/mail/" . $category;
				if (is_dir ( $category_path )) {
					$types = scandir ( $category_path );
					$type_of_category = array ();
					$language_info [$language] [$category] = array_filter ( $types, create_function ( '$type', "return is_file('$category_path/'.\$type);" ) );
					array_walk ( $language_info [$language] [$category], array (
							$this,
							'removeFileSuffix'
					) );
				}
			}

			if ($language_info [$language] ['reservation:sender']) {
				$language_info [$language] ['reservation:sender'] = array (
						"create",
						"modify",
						"delete",
						"reminder"
				);
			}
			if ($language_info [$language] ['reservation:receiver']) {
				$language_info [$language] ['reservation:receiver'] = array (
						'create',
						'modify',
						'delete',
						'deletelist',
						'reminder'
				);
			}
		}
		echo "|";
		echo json_encode ( $language_info );
	}

	/*
	 * 招待メイルの内容を準備する。異なる種類は対応なテンプレートファイルがある
	 */
	function get_mail_content($reservation_key, $template_file, $template, $config, $language) {
		require_once ("classes/dbi/reservation.dbi.php");
		$reservation_table = new ReservationTable ( $this->get_dsn () );
		$where = "reservation_key = " . $reservation_key . " AND reservation_status = 1";
		$reservation_info = $reservation_table->getRow ( $where );
		$this->logger->debug ( $reservation_info, __FILE__, __LINE__ );
		if (! $reservation_info) {
			return "reservation key doesn't exist or status is disabled";
		}
		require_once ("classes/dbi/user.dbi.php");
		$user_table = new UserTable ( $this->get_dsn () );
		$where = "user_key = " . $reservation_info ["user_key"];
		$user_info = $user_table->getRow ( $where );
		$base_url = N2MY_BASE_URL;
		require_once ("classes/dbi/meeting.dbi.php");
		$meeting_table = new MeetingTable ( $this->get_dsn () );
		$where = "meeting_ticket = '" . $reservation_info ["meeting_key"] . "'";
		$meeting_info = $meeting_table->getRow ( $where );
		if (! $meeting_info) {
			return "meeting doesn't exist";
		}
		require_once ("classes/N2MY_Account.class.php");
		$account_table = new N2MY_Account ( $this->get_dsn () );
		$room_option = $account_table->getRoomOptionList ( $reservation_info ["room_key"] );
		require_once ("classes/dbi/reservation_user.dbi.php");
		$reservationuser_info = new ReservationUserTable ( $this->get_dsn () );
		$where = "reservation_key = " . $reservation_key . " AND r_organizer_flg=1";
		$organizer_info = $reservationuser_info->getRow ( $where );
		if ($organizer_info) {
			$organizer = array (
					'name' => $reservation_info ["organizer_name"],
					'email' => $reservation_info ["organizer_email"],
					'lang' => $organizer_info ["r_user_lang"],
					'timezone' => $organizer_info ["r_user_place"]
			);
		}
		$info = array (
				'country_id' => $meeting_info ["meeting_country"],
				'reservation_starttime' => $reservation_info ["reservation_starttime"],
				'reservation_endtime' => $reservation_info ["reservation_endtime"],
				'reservation_place' => $reservation_info ["reservation_place"],
				'user_key' => $reservation_info ["user_key"],
				'room_key' => $reservation_info ["room_key"],
				'sender' => $reservation_info ["sender_name"],
				'sender_mail' => $reservation_info ["sender_email"],
				'is_limited_function' => $reservation_info ["is_limited_function"],
				'is_multicamera' => $reservation_info ["is_multicamera"],
				'is_telephone' => $reservation_info ["is_telephone"],
				'is_h323' => $reservation_info ["is_h323"],
				'is_mobile_phone' => $reservation_info ["is_mobile_phone"],
				'is_high_quality' => $reservation_info ["is_high_quality"],
				'is_desktop_share' => $reservation_info ["is_desktop_share"],
				'is_meeting_ssl' => $reservation_info ["is_meeting_ssl"],
				'is_invite_flg' => $reservation_info ["is_invite_flg"],
				'is_cabinet_flg' => $reservation_info ["is_cabinet_flg"],
				'is_wb_no_flg' => $reservation_info ["is_wb_no_flg"],
				'is_rec_flg' => $reservation_info ["is_rec_flg"],
				'room_option' => $room_option,
				'startDate' => substr ( $reservation_info ["reservation_starttime"], 0, 10 ),
				'startHour' => substr ( $reservation_info ["reservation_starttime"], 11, 2 ),
				'startMin' => substr ( $reservation_info ["reservation_starttime"], 14, 2 ),
				'endDate' => substr ( $reservation_info ["reservation_endtime"], 0, 10 ),
				'endHour' => substr ( $reservation_info ["reservation_endtime"], 11, 2 ),
				'endMin' => substr ( $reservation_info ["reservation_endtime"], 14, 2 ),
				'reservation_name' => $reservation_info ["reservation_name"],
				'is_organizer_flg' => $reservation_info ["is_organizer_flg"],
				'reservation_pw_type' => $reservation_info ["reservation_pw_type"],
				'reservation_pw' => $reservation_info ["reservation_pw"],
				'is_convert_wb_to_pdf' => $reservation_info ["is_convert_wb_to_pdf"],
				'reservation_custom' => $reservation_info ["reservation_custom"],
				'sender_lang' => $reservation_info ["sender_lang"],
				'resevation_url' => $reservation_info ["reservation_url"],
				'member_key' => $reservation_info ["member_key"],
				'intra_fms' => $reservation_info ["intra_fms"],
				'pin_cd' => $meeting_info ["pin_cd"],
				'guest_url_format' => $user_info ["guest_url_format"],
				'base_url' => $base_url,
				'organizer' => $organizer,
				'organizer_name' => $reservation_info ["organizer_name"],
				'mail_body' => $reservation_info ["reservation_info"]
		);
		require_once ("classes/dbi/ordered_service_option.dbi.php");
		$teleconf_info = null;
		$service_option = new OrderedServiceOptionTable ( $this->get_dsn () );
		$enable_teleconf = $service_option->enableOnRoom ( 23, $meeting_info ["room_key"] ); // 23 = teleconf

		if (! $config->get ( "IGNORE_MENU", "teleconference" ) && $enable_teleconf) {
			require_once ("classes/pgi/PGiPhoneNumber.class.php");
			if ($meeting_info ['use_pgi_dialin'] || $meeting_info ['use_pgi_dialin_free']) {
				$main_phone_number = unserialize ( $meeting_info ['pgi_phone_numbers'] );
				$main_phone_number = PGiPhoneNumber::toMultilangLocationName ( $main_phone_number, $language );
				if (! $meeting_info ['use_pgi_dialin']) {
					$main_phone_number = PGiPhoneNumber::extractFreeMain ( $main_phone_number );
				} else {
					$main_phone_number = PGiPhoneNumber::extractLocalMain ( $main_phone_number );
				}
			}
			$teleconf_info = array (
					'tc_type' => $meeting_info ['tc_type'],
					'pgi_service_name' => $meeting_info ['pgi_service_name'],
					'pgi_conference_id' => $meeting_info ['pgi_conference_id'],
					'pgi_m_pass_code' => $meeting_info ['pgi_m_pass_code'],
					'pgi_p_pass_code' => $meeting_info ['pgi_p_pass_code'],
					'pgi_l_pass_code' => $meeting_info ['pgi_l_pass_code'],
					'main_phone_number' => $main_phone_number,
					'phone_number_url' => $base_url . 'p/' . $meeting_info ['meeting_session_id'],
					'tc_teleconf_note' => $meeting_info ['tc_teleconf_note'],
					"use_pgi_dialin" => $meeting_info ["use_pgi_dialin"],
					"use_pgi_dialin_free" => $meeting_info ["use_pgi_dialin_free"],
					"use_pgi_dialout" => $meeting_info ["use_pgi_dialout"]
			);
		}
		$tel_no = $config->getAll ( "TELEPHONE" );
		$config_file = N2MY_APP_DIR . "config/lang/" . $language . "/message.ini";
		$message [$language] = parse_ini_file ( $config_file, true );
		$location_list = $message [$language] ["TELEPHONE"];
		$telephonedata [$language] = array ();
		foreach ( $location_list as $key => $location ) {
			$telephonedata [$language] [$key] ["name"] = $location;
			$telephonedata [$language] [$key] ["list"] = split ( ",", $tel_no [$key] );
		}
		$telephone_data = $telephonedata [$language];
		$template->assign ( "telephone_data", $telephone_data );
		$template->assign ( "teleconf_info", $teleconf_info );
		require_once ("classes/mcu/resolve/Resolver.php");
		if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey ( $meeting_info ["room_key"] )) && $meeting_info ["temporary_did"]) {
			$guestSipNumberAddress = $videoConferenceClass->getSipTemporaryNumberAddress ( $meeting_info ["temporary_did"] );
			$temporarySipAddress = $videoConferenceClass->getSipTemporaryAddress ( $meeting_info ["temporary_did"] );
			$guestH323NumberAddress = $videoConferenceClass->getH323TemporaryNumberAddress ( $meeting_info ["temporary_did"] );
			$temporaryH323Address = $videoConferenceClass->getH323TemporaryAddress ( $meeting_info ["temporary_did"] );
			if ($guestSipNumberAddress)
				$template->assign ( "temporarySipNumberAddress", $guestSipNumberAddress );
			if ($temporarySipAddress)
				$template->assign ( "temporarySipAddress", $temporarySipAddress );
			if ($guestH323NumberAddress)
				$template->assign ( "temporaryH323NumberAddress", $guestH323NumberAddress );
			if ($temporaryH323Address)
				$template->assign ( "temporaryH323Address", $temporaryH323Address );
		}

		$template->assign ( "base_url", $base_url );
		$template->assign ( "reservation_key", $reservation_key );
		$template->assign ( "account_model", $user_info ["account_model"] );
		$template->assign ( "info", $info );
		require_once "classes/dbi/meeting_invitation.dbi.php";
		$objMeetingInvitationTable = new MeetingInvitationTable ( $this->get_dsn () );
		$where = "meeting_key = " . $meeting_info ['meeting_key'];
		$invitationData = $objMeetingInvitationTable->getRow ( $where );
		if ($user_info ["login_url"]) {
			$invitation_url = sprintf ( "%sg/%s/%s/%s", $user_info ["login_url"], $language, $meeting_info ['meeting_session_id'], $invitationData ['user_session_id'] );
		} else {
			$invitation_url = sprintf ( "%sg/%s/%s/%s", N2MY_BASE_URL, $language, $meeting_info ['meeting_session_id'], $invitationData ['user_session_id'] );
		}
		$template->assign ( "reservationPassword", $reservation_info ['reservation_pw'] );
		$template->assign ( "invitation_url", $invitation_url );
		$where = "reservation_key = " . $reservation_key . " AND r_organizer_flg!=1";
		$guest_info = $reservationuser_info->getRowsAssoc ( $where );
		$guests = array ();
		if (count ( $guest_info ) == 0) {
			return "This reservation doesn't send any mails";
		} else {
			$template->assign ( "guest_flg", 1 );
			foreach ( $guest_info as $_guest ) {
				$guests [$_guest ["r_user_session"]] = array (
						"r_user_session" => $_guest ["r_user_session"],
						"name" => $_guest ["r_user_name"],
						"email" => $_guest ["r_user_email"],
						"timezone" => $_guest ["r_user_place"]=='100'?$reservation_info['reservation_place']:$_guest['r_user_place'],
						"r_organizer_flg" => $_guest ["r_organizer_flg"],
						"lang" => $_guest ["r_user_lang"],
						"country_key" => $meeting_info ["meeting_country"],
						"reservation_key" => $_guest ["reservation_key"],
						"starttime" => $reservation_info ["reservation_starttime"],
						"endtime" => $reservation_info ["reservation_endtime"]
				);
				$template->assign ( "guest", $guests [$_guest ["r_user_session"]] );
			}
			$template->assign ( "guests", $guests );
			$mail_body = $template->fetch ( $template_file );
			return $mail_body;
		}
	}
	public function removeFileSuffix(&$file_name) {
		$file_name = substr ( $file_name, 0, stripos ( $file_name, '.' ) );
	}
}

$main = new mailFrame ();
$main->execute ();

?>