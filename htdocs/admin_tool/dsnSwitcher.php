<?php

$configIni = "../../config/config.ini";
$serverListIni = "../../config/server_list.ini";
$slaveListIni = "../../config/slave_list.ini";

$configFile = parse_ini_file( $configIni, true );
$mgmServer = $configFile["GLOBAL"]["auth_dsn"];

$meetingServerList = parse_ini_file( $serverListIni, true );
$slaveList = parse_ini_file( $slaveListIni, true );

//copy( $serverListIni, $serverListIni.".bak.".date( "YmdHis" ) );

$switchList = array();
$switchList["mgm"]["master"] = $mgmServer;
$switchList["mgm"]["slave"] = $slaveList["SLAVE_LIST"]["auth_dsn"];

foreach( $meetingServerList["SERVER_LIST"] as $key => $dsn ){
    $server["master"] = $dsn;
    $server["slave"] = $slaveList["SLAVE_LIST"][$key];
    $switchList["meeting"][$key] = $server;
}

if( $dsnKey = $_POST["dsnKey"] ){
    switch( $dsnKey ){
        //mgm
        case "auth_dsn":
            //config.ini書き換え
            copy( $configIni, $configIni.".bak.".date( "YmdHis" ) );
            $fp = fopen($configIni, "r");
            while( ! feof( $fp ) ){
                $fileline = fgets( $fp );
                if( preg_match( "#^auth_dsn#", $fileline) ){
                    $str .= sprintf( "auth_dsn = \"%s\"\n", $switchList["mgm"]["slave"] );
                } else {
                    $str .= $fileline;
                }
            }
            fclose($fp);
            file_put_contents( $configIni, $str );

            //slavelist.ini書き換え
            copy( $slaveListIni, $slaveListIni.".bak.".date( "YmdHis" ) );
            $fp = fopen( $slaveListIni, "r" );
            $str = "";
            while( ! feof( $fp ) ){
                $fileline = fgets( $fp );
                if( preg_match( "#^auth_dsn#", $fileline) ){
                    $str .= sprintf( "auth_dsn = \"%s\"\n", $switchList["mgm"]["master"] );
                } else {
                    $str .= $fileline;
                }
            }
            fclose($fp);
            file_put_contents( $slaveListIni, $str );
            header( "Location: ./dsnSwitcher.php" );
            exit;

        default:
            $masterInfo = array();
            //serverlist.ini書き換え
            copy( $serverListIni, $serverListIni.".bak.".date( "YmdHis" ) );
            copy( $slaveListIni, $slaveListIni.".bak.".date( "YmdHis" ) );
            $fp = fopen( $serverListIni, "r" );
            $str = "";
            while( ! feof( $fp ) ){
                $fileline = fgets( $fp );
                if( preg_match( sprintf( "#^%s#", $dsnKey ), $fileline ) ){
                    $masterInfo["hostName"] = $dsnKey;
                    $masterInfo["dsn"] = $switchList["meeting"][$dsnKey]["slave"];
                    $str .= sprintf( "%s = \"%s\"\n", $dsnKey, $switchList["meeting"][$dsnKey]["slave"] );
                } else {
                    $str .= $fileline;
                }
            }
            fclose($fp);
            file_put_contents( $serverListIni, $str );

            //slavelist.ini書き換え
            $fp = fopen( $slaveListIni, "r" );
            $str = "";
            while( ! feof( $fp ) ){
                $fileline = fgets( $fp );
                if( preg_match( sprintf( "#^%s#", $dsnKey ), $fileline ) ){
                    $str .= sprintf( "%s = \"%s\"\n", $dsnKey, $switchList["meeting"][$dsnKey]["master"] );
                } else {
                    $str .= $fileline;
                }
            }
            fclose($fp);
            file_put_contents( $slaveListIni, $str );

            //mgm::db_server udpate
            if( $_POST["db"] ){
                require_once ('../../lib/pear/DB.php');
                $conn = DB :: connect( $switchList["mgm"]["master"] );
                $sql = sprintf( "UPDATE db_server SET dsn='%s' WHERE host_name='%s'", $masterInfo["dsn"], $masterInfo["hostName"]);
                $result = $conn->query( $sql );
                if( DB :: isError( $result ) ) {
                    var_dump( $result );
                    exit;
                }
            }

            header( "Location: ./dsnSwitcher.php" );
            exit;
    }
}

?>
<html>
<head>
<style TYPE="text/css">
<!--
* {
    margin: 0;
    padding: 5px;
    font-size: 12px;
    font-family: "Hiragino Kaku Gothic Pro", "\30D2\30E9\30AE\30CE\20Pro\20W3", "\FF2D\FF33\20\FF30\30B4\30B7\30C3\30AF", "Lucida Grande", "Lucida Sans Unicode", "Trebuchet MS", Osaka, sans-serif;
    line-height: 1.5;
    color: #666;
}

body {
    padding: 5px;

}

table
{
    width: 100%;
    border: 1px none #666;
}

th
{
    width: 100px;
}

div {
    text-decoration: underline;
}

-->
</style>

<script type="text/javascript">
<!--
function switchDsn( key )
{
    if( window.confirm( 'switch?' ) ){
        var form = document.getElementById( 'f1' );
        //while( form.hasChildNodes() ){ form.removeChild( form.lastChild ); };
        var input = document.createElement( 'INPUT' );
        input.type = "hidden";
        input.name='dsnKey';
        input.value=key;
        form.method="POST";
        form.appendChild( input );
        form.submit();
    }
}

-->
</script>
</head>

<body>

<div>n2my_mgm</div>
<form id="f1">
<table>
<tr>
    <th>master</th>
    <td><?=$switchList["mgm"]["master"]?></td>
    <td></td>
</tr>
<tr>
    <th>slave</th>
    <td><?=$switchList["mgm"]["slave"]?></td>
    <td><input type="button" value="switch" onClick="switchDsn('auth_dsn');"></td>
</tr>
</table>

<div>n2my_meeting</div>

<table>

<?php
foreach( $switchList['meeting'] as $key => $serverList ){
?>

<tr>
    <th>master</td>
    <td><?=$serverList["master"]?></th>
    <td>db <input type="checkbox" name="db"></td>
</tr>
<tr>
    <th>slave</th>
    <td><?=$serverList["slave"]?></td>
    <td><input type="button" value="switch" onClick="switchDsn('<?=$key?>');"></td>
</tr>

<?}?>
</form>

</table>
</body>
</html>
