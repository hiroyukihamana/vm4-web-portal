<?php

//環境設定
require_once('classes/AppFrame.class.php');
//PGi利用通信レート情報
require_once('classes/dbi/pgi_rate.dbi.php');

/* ***********************************************
 * PGI利用通信レート情報を請求管理DBへ反映するための処理
 *
 * ***********************************************/
class AppStatisticsPgiRateInfo extends AppFrame {

    var $_dsn = null;
    var $account_dsn = null;

    function init()
    {
        // DBサーバ情報取得
        if (file_exists(sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ))) {
            $server_list = parse_ini_file( sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
        } else {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        }
        //$this->logger->info(__FUNCTION__.' # Array Of Dsn Lists! ', __FILE__, __LINE__, $this->_dsn);
    }


    /**
     * default view routine
     *
     */
    function default_view()
    {

        $this->logger->info(__FUNCTION__.' # GetMeetingPgi_Rates Info Operation Start ... ', __FILE__, __LINE__);

        // 初期化
        $mtg_pgi_rates = array();

        // パラメタ取得
        $target_day = $this->request->get('target_day');
        //$target_day = date('Y-m-d', time() - (3600*24)) . ' 00:00:00'; // 常に前日

        // パラメータ検証 ( boolearn : true / false )
        $param_is_safe = $this->_check_param($target_day);

        // パラメータ判定
        if ($param_is_safe){
            // MTG利用統計用PGI設定データ（差分データ）作成
            $this->mtg_pgi_rates = array();
            //foreach ($this->_dsn as $server_key => $server_dsn) {
                $_tmp_data = null;
                //$_tmp_data = $this->_get_pgi_rate_routine($server_dsn, $target_day);
                $_tmp_data = $this->_get_pgi_rate_routine($this->_dsn, $target_day);
                if (is_array($_tmp_data) === true) {
                    //$this->logger->info(__FUNCTION__.' # PgiRateInfo ... ', __FILE__, __LINE__, $_tmp_data);
                    //$this->logger->info(__FUNCTION__.' # PgiRateInfo ... ', __FILE__, __LINE__, "### Merge Pgi Rate Info!!");
                    $mtg_pgi_rates = array_merge($mtg_pgi_rates, $_tmp_data);
                    //$this->logger->info(__FUNCTION__.' # PgiRateInfo ... ', __FILE__, __LINE__, $mtg_pgi_rates);
                }
            //}
        } else {
            $mtg_pgi_rates = array();
        }
        $this->logger->info(__FUNCTION__.' # PgiRateInfo Count ... ', __FILE__, __LINE__, count($mtg_pgi_rates));
        //$this->logger->debug(__FUNCTION__.' # PgiRateInfo ... ', __FILE__, __LINE__, $mtg_pgi_rates);

        // 戻り値編集
        //print_r($mtg_pgi_rates);
        //include_once("dBug.php"); //TEST用
        //new dBug($mtg_pgi_rates); //TEST用
        print serialize($mtg_pgi_rates);

        $this->logger->info(__FUNCTION__.' # GetMeetingPgiRates Info Operation End ... ', __FILE__, __LINE__);
    }


    /**
     * メイン取得
     *
     */
    private function _get_pgi_rate_routine($data_db_dsn, $target_day)
    {
        $pgi_rate_obj = new PgiRateTable($data_db_dsn);
        $rows = null;
        $_rows = null;
        $where = ' `registtime` LIKE \'' . addslashes($target_day) . '%\' ' .
                 ' OR `updatetime` LIKE \'' . addslashes($target_day) . '%\' ';
        $coulumns = ' `pgi_rate_key`, `pgi_setting_key`, `rate`, `rate_type`, ' .
                    ' `is_deleted`, `registtime`, `updatetime` ';

        $rows = $pgi_rate_obj->getRowsAssoc($where, null, null, null, $coulumns);
        foreach ($rows as $key => $val) {
            $_rows[] = $val;
        }
        return $_rows;
    }


    /**
     * パラメータ検証
     *
     */
    private function _check_param($_param = null)
    {
        $ret = false;
        //未設定チェック
        if (!$_param) {
            return $ret;
        }
        //文字数チェック
        if (strlen($_param) < 7) {
            return $ret;
        }
        //指定不可文字存在チェック
        if (preg_match("/[_%*?]/", $_param)) {
            return $ret;
        }
        //チェック OK!
        $ret = true;

        return $ret;

    }


}

$main = new AppStatisticsPgiRateInfo();
$main->execute();
?>