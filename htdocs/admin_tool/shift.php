<?php
require_once("classes/AppFrame.class.php");
require_once("classes/mgm/MGM_Auth.class.php");

require_once("lib/pear/DB.php");
require_once("classes/mgm/dbi/server.dbi.php");

class AppShift extends AppFrame
{
    var $server_key = 1;
    var $dsn;
    var $mgm_conn;

    public function init() {
        $this->obj_MGMAuth = new MGM_AuthClass( $this->_auth_dsn );
        $server_info = $this->obj_MGMAuth->getServerInfo( $this->server_key );
        $this->dsn = $server_info["dsn"];
        $this->mgm_conn = DB::connect( $this->_auth_dsn );
        if (DB::isError( $this->mgm_conn ) ) {
            return "false";
        } else {
            // キャラセット指定
            $dsn_elem = parse_url($this->_auth_dsn);
            if (isset ($dsn_elem["query"])) {
                $query_opt = array ();
                parse_str($dsn_elem["query"], $query_opt);
                if (isset ($query_opt["charset"])) {
                    $this->mgm_conn->query("SET NAMES " . $query_opt["charset"]);
                }
            }
        }
        $this->conn = DB::connect( $this->dsn );
        if (DB::isError($this->conn)) {
            return "false";
        } else {
            // キャラセット指定
            $dsn_elem = parse_url($this->_auth_dsn);
            if (isset ($dsn_elem["query"])) {
                $query_opt = array ();
                parse_str($dsn_elem["query"], $query_opt);
                if (isset ($query_opt["charset"])) {
                    $this->conn->query("SET NAMES " . $query_opt["charset"]);
                }
            }
        }
    }

    public function default_view()
    {
        $this->render_top();
    }

    private function render_top()
    {
        $this->display( "admin_tool/shift.t.html" );
    }

    private function query( $sql, $dest )
    {
        $ret = ( 1 == $dest ) ? $this->mgm_conn->query( $sql ) : $this->conn->query( $sql );
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return "false";
        } else {
            return "true";
        }
    }

    public function action_truncate()
    {
        $request = $this->request->getAll();
        $sql = sprintf( "TRUNCATE TABLE %s.%s", 1 == $request["dest"] ? "n2my_mgm" : "n2my_meeting", $request["table_name"] );
        print $this->query( $sql, $request["dest"] );
    }

    public function action_truncate_member()
    {
        $sql = "TRUNCATE TABLE n2my_mgm.user;";
        $this->query( $sql, 1 );
        $sql = "TRUNCATE TABLE n2my_meeting.member;";
        $this->query( $sql, 2 );
    }

    public function action_truncate_member_group()
    {
        print $this->action_truncate();
    }

    public function action_truncate_user()
    {
        print $this->action_truncate();
    }

    public function action_truncate_service()
    {
        print $this->action_truncate();
    }

    public function action_truncate_meeting()
    {
        print $this->action_truncate();
    }

    public function action_truncate_layout()
    {
        print $this->action_truncate();
    }

    public function action_truncate_participant_mode()
    {
        print $this->action_truncate();
    }

    public function action_truncate_participant_role()
    {
        print $this->action_truncate();
    }

    public function action_truncate_server()
    {
        $sql = "TRUNCATE TABLE n2my_mgm.fms_server;";
        print $this->query( $sql, 1 );
    }

    public function action_truncate_datacenter()
    {
        print $this->action_truncate();
    }

    public function action_truncate_document()
    {
        print $this->action_truncate();
    }

    public function action_truncate_convert_file2()
    {
        $sql = "TRUNCATE TABLE n2my_mgm.convert_file;";
        print $this->query( $sql, 1 );
    }

    /**
     * 共通テーブル以降
     */
    public function action_insert()
    {
        $request = $this->request->getAll();
        $sql = sprintf( "INSERT INTO %s.%s SELECT * FROM %s.%s;",
                        1 == $request["dest"] ? "n2my_mgm" : "n2my_meeting", $request["table_name"], $request["from"], $request["table_name"] );
        print $this->query( $sql, $request["dest"] );
    }

    public function action_member()
    {
        $sql = "INSERT INTO n2my_meeting.member SELECT * FROM n2my_web.member;";
        if( "true" == $this->query( $sql, 2 ) ){
            require_once( "classes/dbi/member.dbi.php" );
            require_once( "classes/mgm/dbi/user.dbi.php" );
            $obj_Member = new MemberTable( $this->dsn );
            $sort = array( 'member_key' => 'assoc' );
            $member_list = $obj_Member->getRowsAssoc( null, $sort );
            if( count( $member_list ) > 0 ){
                $obj_User = new MgmUserTable( $this->_auth_dsn );
                foreach( $member_list as $key => $member ){
                    $data = array(
                                "user_id"			=> $member["member_id"],
                                "server_key"		=> $this->server_key );
                    $obj_User->add( $data );
                }
            }
            print "true";
        } else
            print "false";
    }

    public function action_member_group()
    {
        $sql = "SELECT * FROM n2my_web.member_group;";
        $ret = $this->mgm_conn->query( $sql );
        require_once( "classes/dbi/member_group.dbi.php" );
        $obj_MemberGroup = new MemberGroupTable( $this->dsn );

        while($data = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $obj_MemberGroup->add( $data );
        }
        print "true";
    }

    public function action_service()
    {
        $sql = "SELECT * FROM n2my_web.service;";
        $ret = $this->mgm_conn->query( $sql );
        while( $data = $ret->fetchRow(DB_FETCHMODE_ASSOC ) ) {
            $data["service_status"] = 1;
            $sth = $this->mgm_conn->autoExecute( "service", $data,
                                DB_AUTOQUERY_INSERT);
            if (PEAR::isError($sth)) {
                $this->logger2->error( $sth->getUserInfo() );
                print "false";
                exit;
            }
        }
        print "true";
    }

    public function action_user()
    {
        $sql = "INSERT IGNORE INTO n2my_meeting.user SELECT
user_key,
reseller_key,
country_key,
user_id,
user_password,
user_admin_password,
user_company_name,
user_company_postnumber,
user_company_address,
user_company_phone,
user_company_fax,
user_staff_firstname,
user_staff_lastname,
user_staff_email,
user_status,
user_delete_status,
user_starttime,
user_registtime,
user_updatetime,
user_deletetime,
addition
FROM n2my_web.user;";
        if( "true" == $this->query( $sql, 2 ) ){
            require_once( "classes/dbi/user.dbi.php" );
            $obj_User = new UserTable( $this->dsn );
            $sort = array( 'user_key' => 'assoc' );
            $user_list = $obj_User->getRowsAssoc( null, $sort );
            if( count( $user_list ) > 0 ){
                $obj_User = new MgmUserTable( $this->_auth_dsn );
                foreach( $user_list as $key => $user ){
                    $data = array(
                                "user_id"			=> $user["user_id"],
                                "server_key"		=> $this->server_key );
                    $sth = $obj_User->add( $data );
                    if (PEAR::isError($sth)) {
                        $this->logger2->error( $sth->getUserInfo() );
                        print "false";
                        exit;
                    }
                }
            }
            print "true";
        } else
            print "false";
    }

    public function action_relation()
    {
        $sql = "SELECT
    r.r_user_session, u.user_id
FROM
(
SELECT
    ru.*, r.user_key
FROM
(
SELECT
    ru.reservation_key, ru.r_user_session, r.room_key
FROM
    n2my_web.reservation_user ru
LEFT JOIN
    n2my_web.reservation r
ON
    ru.reservation_key = r.reservation_key
    ) ru
LEFT JOIN
    n2my_web.room r
ON
    ru.room_key = r.room_key
) r
LEFT JOIN
    n2my_web.user u
ON
    r.user_key = u.user_key
WHERE
    r.user_key is not null";

        $ret = $this->mgm_conn->query( $sql );
        require_once( "classes/mgm/dbi/relation.dbi.php" );
        $obj_Relation = new MgmRelationTable( $this->_auth_dsn );
        while( $row = $ret->fetchRow(DB_FETCHMODE_ASSOC ) ) {
            $data = array(
                        'relation_key'	=> $row['r_user_session'],
                        'user_key'		=> $row['user_id'],
                        'status'		=> 1,
                        'relation_type'	=> 'invite' );
            $sth = $obj_Relation->add( $data );
            if (PEAR::isError($sth)) {
                $this->logger2->error( $sth->getUserInfo() );
                print "false";
                exit;
            }
        }
        print "true";
    }

    /**
     * core
     */
    public function action_datacenter()
    {
        $sql = "INSERT IGNORE INTO n2my_mgm.datacenter SELECT * FROM n2my_core.datacenter;";
        print $this->query( $sql, 1 );
    }

    public function action_meeting()
    {
        $sql = '
SELECT
    count( * ) as meeting_use_minute,
m.meeting_key,
m.layout_key,
m.server_key,
m.sharing_server_key,
m.room_key,
m.meeting_ticket,
m.meeting_session_id,
m.meeting_country,
m.meeting_room_name,
m.meeting_name,
m.meeting_tag,
m.meeting_max_audience,
m.meeting_max_extendable,
m.meeting_max_seat,
m.meeting_size_recordable,
m.meeting_size_uploadable,
m.meeting_size_used,
m.meeting_log_owner,
m.meeting_log_password,
m.meeting_start_datetime,
m.meeting_stop_datetime,
m.meeting_extended_counter,
m.has_recorded_minutes,
m.has_recorded_video,
m.is_active,
m.is_extended,
m.is_locked,
m.is_blocked,
m.is_reserved,
m.version,
m.create_datetime,
m.update_datetime,
m.room_addition,
m.is_deleted,
m.use_flg,
m.actual_start_datetime,
m.actual_end_datetime,
m.meeting_use_minute
FROM
    n2my_core.meeting_use_log mu
LEFT JOIN
    n2my_core.meeting m
ON
    m.meeting_key = mu.meeting_key
GROUP BY
    mu.meeting_key;';
        $ret = $this->mgm_conn->query( $sql );
        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meeting = new MeetingTable( $this->dsn );
        while( $row = $ret->fetchRow(DB_FETCHMODE_ASSOC ) ) {
            $sql2 = sprintf( "SELECT MIN( meeting_uptime_start ) as meeting_start, MAX( meeting_uptime_stop) as meeting_stop FROM n2my_core.meeting_uptime WHERE meeting_key ='%s' GROUP BY meeting_key;", $row["meeting_key"] );
            $ret2 = $this->mgm_conn->query( $sql2 );
            $result = $ret2->fetchRow(DB_FETCHMODE_ASSOC);
            if ($result)
                 $row["use_flg"] = 1;
            $row["actual_start_datetime"] = $result["meeting_start"];
            $row["actual_end_datetime"] = $result["meeting_stop"];

            $sth = $this->conn->autoExecute( "meeting", $row,
                                DB_AUTOQUERY_INSERT);
            if ( PEAR::isError( $sth ) ) {
                $this->logger2->error( $sth->getUserInfo() );
                print "false";
                exit;
            }
        }
        print "true";
    }

    public function action_layout()
    {
        $sql = "INSERT INTO n2my_mgm.layout SELECT
layout_key,
layout_id,
layout_name,
create_datetime,
update_datetime
        FROM n2my_core.layout;";
        print $this->query( $sql, 1 );
    }

    public function action_participant_mode()
    {
        $sql = "INSERT INTO n2my_mgm.participant_mode SELECT
participant_mode_key,
participant_mode_name,
create_datetime,
update_datetime
        FROM n2my_core.participant_mode;";
        print $this->query( $sql, 1 );
    }

    public function action_participant_role()
    {
        $sql = "INSERT INTO n2my_mgm.participant_role SELECT
participant_role_key,
participant_role_name,
create_datetime,
update_datetime
        FROM n2my_core.participant_role;";
        print $this->query( $sql, 1 );
    }

    public function action_server()
    {
        $sql = "INSERT INTO n2my_mgm.fms_server (
server_key,
datacenter_key,
server_address,
server_port,
server_country,
server_count,
server_priority,
create_datetime,
update_datetime,
is_available,
error_count
)
SELECT * FROM n2my_core.server;";
        print $this->query( $sql, 1 );
    }

    public function action_document()
    {
        print "uc";
    }

    public function action_convert_file2()
    {
        print "uc";
    }

}

$main =& new AppShift();
$main->execute();