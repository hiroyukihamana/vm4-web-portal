<?php

// 環境設定
require_once('classes/AppFrame.class.php');
// 部屋情報
require_once('classes/dbi/room.dbi.php');

/* ***********************************************
 * 部屋の情報を請求管理DBへ反映するための処理
 *
 * ***********************************************/
class AppStatisticsRoomInfo extends AppFrame {

    var $_dsn = null;
    var $account_dsn = null;

    function init()
    {
        // DBサーバ情報取得
        if (file_exists(sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ))) {
            $server_list = parse_ini_file( sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
        } else {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        }
        //$this->logger->info(__FUNCTION__.' # Array Of Dsn Lists! ', __FILE__, __LINE__, $this->_dsn);
    }


    /**
     * default view routine
     *
     */
    function default_view()
    {

        $this->logger->info(__FUNCTION__.' # GetMeetingRooms Info Operation Start ... ', __FILE__, __LINE__);

        // 初期化
        $mtg_rooms = array();

        // パラメタ取得
        $target_day = $this->request->get('target_day');
        //$target_day = date('Y-m-d', time() - (3600*24)) . ' 00:00:00'; // 常に前日

        // パラメータ検証 ( boolearn : true / false )
        $param_is_safe = $this->_check_param($target_day);

        // パラメータ判定
        if ($param_is_safe){
            // MTG利用統計用部屋データ（差分データ）作成
            $this->mtg_rooms = array();
            //foreach ($this->_dsn as $server_key => $server_dsn) {
                $_tmp_data = null;
                //$_tmp_data = $this->_get_room_routine($server_dsn, $target_day);
                $_tmp_data = $this->_get_room_routine($this->_dsn, $target_day);
                if (is_array($_tmp_data) === true) {
                    //$this->logger->info(__FUNCTION__.' # RoomInfo ... ', __FILE__, __LINE__, $_tmp_data);
                    //$this->logger->info(__FUNCTION__.' # RoomInfo ... ', __FILE__, __LINE__, "### Merge Room Info!!");
                    $mtg_rooms = array_merge($mtg_rooms, $_tmp_data);
                    //$this->logger->info(__FUNCTION__.' # RoomInfo ... ', __FILE__, __LINE__, $mtg_rooms);
                }
            //}
        } else {
            //$mtg_rooms = array("param_error" => "T");
            $mtg_rooms = array();
        }
        $this->logger->info(__FUNCTION__.' # RoomInfo Count ... ', __FILE__, __LINE__, count($mtg_rooms));
        //$this->logger->debug(__FUNCTION__.' # RoomInfo ... ', __FILE__, __LINE__, $mtg_rooms);

        // 戻り値編集
        //print_r($mtg_rooms);
        print serialize($mtg_rooms);

        $this->logger->info(__FUNCTION__.' # GetMeetingRooms Info Operation End ... ', __FILE__, __LINE__);
    }


    /**
     * メイン取得
     *
     */
    private function _get_room_routine($data_db_dsn, $target_day)
    {
        $room_obj = new RoomTable($data_db_dsn);
        $rows = null;
        $_rows = null;

$sql = "
SELECT
 `r`.`room_key`, `r`.`max_seat`, `r`.`max_audience_seat`, `r`.`max_whiteboard_seat`,
 `r`.`user_key`, `r`.`room_name`, `r`.`room_layout_type`, `r`.`room_status`, `r`.`room_sort`,
 `r`.`cabinet`, `r`.`mfp`, `r`.`default_layout_4to3`, `r`.`default_layout_16to9`,
 `r`.`whiteboard_page`, `r`.`whiteboard_size`, `r`.`whiteboard_filetype`, `r`.`cabinet_size`,
 `r`.`cabinet_filetype`, `r`.`is_auto_transceiver`, `r`.`transceiver_number`,
 `r`.`is_auto_voice_priority`, `r`.`is_netspeed_check`, `r`.`is_wb_no`, `r`.`is_device_skip`,
 `r`.`rtmp_protocol`, `r`.`meeting_key`, `u`.`user_id`,
 `r`.`room_registtime`, `r`.`room_updatetime`, `r`.`room_deletetime`,
 `r`.`use_teleconf`, `r`.`use_pgi_dialin`, `r`.`use_pgi_dialin_free`,
 `r`.`use_pgi_dialout`, `r`.`rec_gw_convert_type`, `r`.`rec_gw_userpage_setting_flg`
 FROM `room` AS `r` LEFT JOIN  `user` AS  `u` ON (  `u`.`user_key` =  `r`.`user_key` )
 WHERE r.is_one_time_meeting = 0 AND (`r`.`room_registtime` LIKE '" . addslashes($target_day) . "%'
    OR `r`.`room_updatetime` LIKE '" . addslashes($target_day) . "%'
    OR `r`.`room_deletetime` LIKE '" . addslashes($target_day) . "%') ";

        $rs = $room_obj->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger->info(__FUNCTION__." # DBError", __FILE__, __LINE__, $sql);
        }else{
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                $_rows[] = $row;
            }
            //$this->logger->info(__FUNCTION__.' # Rooms Info ... ', __FILE__, __LINE__, $_rows);
        }
        return $_rows;
    }


    /**
     * パラメータ検証
     *
     */
    private function _check_param($_param = null)
    {
        $ret = false;
        //未設定チェック
        if (!$_param) {
            return $ret;
        }
        //文字数チェック
        if (strlen($_param) < 7) {
            return $ret;
        }
        //指定不可文字存在チェック
        if (preg_match("/[_%*?]/", $_param)) {
            return $ret;
        }
        //チェック OK!
        $ret = true;

        return $ret;

    }


}

$main = new AppStatisticsRoomInfo();
$main->execute();
?>