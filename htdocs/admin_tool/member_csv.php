<?php

require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
require_once 'classes/AppFrame.class.php';
require_once 'classes/mgm/dbi/user.dbi.php';
require_once 'classes/dbi/user.dbi.php';
require_once 'classes/dbi/member.dbi.php';
require_once 'classes/dbi/member_group.dbi.php';
require_once 'classes/dbi/room.dbi.php';
require_once 'classes/dbi/inbound.dbi.php';
require_once 'classes/dbi/outbound.dbi.php';
require_once 'classes/dbi/member_room_relation.dbi.php';
require_once 'classes/dbi/user_plan.dbi.php';
require_once 'classes/dbi/room_plan.dbi.php';
require_once 'classes/dbi/service.dbi.php';
require_once 'classes/dbi/ordered_service_option.dbi.php';
require_once 'classes/dbi/user_service_option.dbi.php';

define( 'FLAG_ADD', 'A' );
define( 'FLAG_MODIFY', 'M' );
define( 'FLAG_DELETE', 'D' );
define( 'FLAG_UNDEF', 'X' );

class MemberCSV extends AppFrame {

	// dbi mgm
	private $objMgmUser, $objService, $objServiceOption,$objRelation;
	// dbi data
	private $objUser, $objMember, $objMemberGroup;
	private $objRoom;
	private $objInbound, $objOutbound;
	private $objMemberRoomRelation;
	private $objRoomPlan;
	private $objUserPlan;
	private $objOrderServiceOption;
	private $objUserServiceOption;

	private $name_space;
	private $rule = array();

	public function init() {
		$this->name_space = md5( __FILE__ );

		$this->objMgmUser = new MgmUserTable( parent::get_auth_dsn() );
		$this->objService = new ServiceTable( parent::get_auth_dsn() );
		$this->objServiceOption = new N2MY_DB( parent::get_auth_dsn(), 'service_option' );
		$this->objRelation = new N2MY_DB( parent::get_auth_dsn(), 'relation' );
		if ( $dsn = $this->get_dsn() ) {
			$this->objUser = new UserTable( $dsn );
			$this->objMember = new MemberTable( $dsn );
			$this->objMemberGroup = new MemberGroupTable( $dsn );
			$this->objRoom = new RoomTable( $dsn );
			$this->objInbound = new InboundTable( $dsn );
			$this->objOutbound = new OutboundTable( $dsn );
			$this->objMemberRoomRelation = new MemberRoomRelationTable( $dsn );
			$this->objRoomPlan = new RoomPlanTable( $dsn );
			$this->objUserPlan = new UserPlanTable( $dsn );
			$this->objOrderServiceOption = new OrderedServiceOptionTable( $dsn );
			$this->objUserServiceOption = new UserServiceOptionTable( $dsn );
		}

		// 入力CSVルール.定義されていないカラムはパース時に無視する.
		$this->rule = array (
				'flag' => array (
						'required' => true,
						'allow' => array (
								FLAG_ADD,
								FLAG_DELETE,
								FLAG_UNDEF
						)
				),
				'member_id' => array (
						// 'minlen' => 3,
						// 'maxlen' => 200,
						'required' => true
				),
				'member_pass' => array (
						// 'minlen' => 8,
						'maxlen' => 128
				),
				'member_email' => array (
						'email' => true
				),
				'member_name' => array (
						'maxlen' => 50
				),
				'member_name_kana' => array (
						'maxlen' => 50
				),
				'member_group_name' => array (
						'maxlen' => 50
				),
				'timezone' => array(
						// 'regex' => '/^-?[0-9]+?$/'
						'allow' => array (
								-12,-11,-10,-9,-8,-7,-6,-5,-4.5,-4,-3,-3.5,-2,-1,
								0,1,2,3,4,5,5.5,6,6.5,7,8,9,9.5,10,10.5,11,12,13,14,
						)
				),
				'lang' => array (
						'allow' => array ( 'ja','en','zh-cn','zh-tw','fr','th','in','ko' )
				),
				'use_shared_storage' => array (
						'allow' => array ( 1,0 )
				)
		);
	}

	public function default_view($err_msg = null) {
		$this->session->removeAll( $this->name_space );
		$this->display( $err_msg );
	}

	public function display($err_msg = null) {
		if ( $err_msg ) {
			$this->template->assign( 'error_msg', $err_msg );
		}

		$file_contents = file_get_contents( N2MY_APP_DIR . 'templates/common/admin_tool/member/member.csv' );
		$this->template->assign( 'file_contents', $file_contents );

		$this->template->assign( 'user_info', $this->session->get( 'user_info', $this->name_space ) );
		$this->template->assign( 'csv_info', $this->session->get( 'csv_info', $this->name_space ) );
		$this->template->assign( 'export_info', $this->session->get( 'export_info', $this->name_space ) );
		$this->template->assign( 'result_info', $this->session->get( 'result_info', $this->name_space ) );

		// admin_tool 権限チェック
		$authority = $_SESSION['_authsession']['data']['authority'];
		$this->template->assign('authority', $authority);

		parent::display( 'admin_tool/member/member_csv.t.html' );
		exit();
	}

	public function action_download_csv() {
		$target = $this->request->get( 'target' );
		$contents = '';
		$filename = '';
		switch ( $target ) {
			case 'csv_template':
				$filename = 'csv_template';
				$contents = file_get_contents( N2MY_APP_DIR . 'templates/common/admin_tool/member/member.csv' );
				break;
			case 'export':
				$data = $this->session->get('export_info', $this->name_space);
				$content[] = join( ',', array_keys( $data[0] ) );
				foreach ( $data as $row ) {
					$content[] = join( ',', array_values( $row ) );
				}
				$contents = join("\n", $content);
				$filename = sprintf( 'export_%s_%s', $this->session->get( 'user_id', $this->name_space ), date( 'ymdHi' ) );
				break;
			case 'result':
				$data = $this->session->get('result_info', $this->name_space);
				$columns = array ('status', 'action', 'member_id', 'member_pass');
				$content[] = join( ',',  $columns);
				foreach ( $data as $row ) {
					$node = array();
					foreach ($columns as $col){
						$node[] = $row[$col];
					}
					$content[] = join(',', $node);
				}
				$contents = join("\n", $content);
				$filename = sprintf( 'import_result_%s_%s', $this->session->get( 'user_id', $this->name_space ), date( 'ymdHi' ) );
				break;
			default:
				$this->display();
				break;
		}
		header( 'Content-Type: application/octet-stream;' );
		header( sprintf('Content-Disposition: attachment; filename="%s.csv"', $filename) );
		print $contents;
	}

	public function action_select_user() {
		$this->session->remove( 'user_info', $this->name_space );
		$this->session->remove( 'csv_info', $this->name_space );

		$user_id = trim( $this->request->get( 'user_id' ) );
		if ( ! $user_id ) {
			$this->display( 'ユーザーIDが入力されていません' );
		}

		$where = sprintf( 'user_id = "%s"', mysql_real_escape_string($user_id) );
		$user_info = $this->objUser->getRow( $where );

		// 対応ユーザバリデーションチェック
		if ( ! $user_info ) {
			$this->display( 'user not found' );
		}
		require_once ("classes/ONE_Account.class.php");
		$obj_ONE_Account = new ONE_Account($this->get_dsn());
		if ( $obj_ONE_Account->is_one_account($user_info["account_plan"])) {
			$this->display( 'no suport for One' );
		}

		// ID制,center,terminal は 未対応
		if ( $user_info['account_model'] ) {
			$this->display( 'no suport for account_model' );
		}

		if ( $user_info['use_sales'] ) {
			if ( ! $user_info['max_member_count'] ) {
				$this->display( 'max_member_count is 0' );
			}
			// user plan 探査
			$where = sprintf( 'user_key = %d AND user_plan_status = 1', $user_info['user_key'] );
			$service_key = $this->objUserPlan->getOne( $where, 'service_key' );
			if ( ! $service_key ) {
				$this->display(' user plan is null ');
			}
			$service_info = $this->objService->getRow( sprintf( 'service_key = %d', $service_key ) );
			$user_info['service_info'] = $service_info;

			// user option 探査
			$where = sprintf('user_key=%d AND user_service_option_status=1', $user_info['user_key']);
			$sort = array ( 'service_option_key' => 'asc' );// レンダリング用調整
			$options = $this->objUserServiceOption->getRowsAssoc( $where, $sort );
			foreach ( $options as &$option_info ) {
				// 名前解決
				$where = sprintf('service_option_key=%d AND option_status=1 AND use_user_option=0', $option_info['service_option_key']);
				$option_info['service_option_name'] = $this->objServiceOption->getOne( $where, 'service_option_name' );
			}
			unset( $option_info );
			$user_info['options'] = $options;

			// calc active member count
			$where = sprintf( 'user_key=%d AND member_status = 0', $user_info['user_key'] );
			$user_info['member_count'] = $this->objMember->numRows( $where );

		}

		// calc active room count
		$where = sprintf( 'user_key=%d AND room_status=1', $user_info['user_key'] );
		$user_info['room_count'] = $this->objRoom->numRows( $where );

		$this->session->set('user_info', $user_info, $this->name_space);
		$this->logger2->debug($user_info);
		$this->display();
	}

	private function getCSV($name){
		if ( $_FILES[$name]['error'] ) {
			$this->display( 'CSVファイルをアップロードしてください' );
		}

		$file = $_FILES[$name];
		$pathinfo = pathinfo( $file['name'] );
		if ( $pathinfo['extension'] != 'csv' ) {
			$this->display( 'CSVファイルをアップロードしてください' );
		}

		// 入力CSVパース
		$filename = parent::get_work_dir() . '/' . tmpfile();
		move_uploaded_file( $file['tmp_name'], $filename );
		require_once 'lib/EZLib/EZUtil/EZCsv.class.php';
		$csv = new EZCsv( true, 'UTF-8', 'UTF-8' );
		$csv->open( $filename, 'r' );
		$rows = array ();
		while ( $row = $csv->getNext() ) {
			foreach ( $row as $key => &$val ) {
				// 全入力データtrim
				$val = trim( $val );
				// ルール定義されたカラム以外は無視
				// 空文字列も無視
				if ( ! array_key_exists( $key, $this->rule ) || $val === '' ){
					unset( $row[$key] );
				}
			}
			$rows[] = $row;
		}
		$csv->close();
		return $rows;
	}

	public function action_import_confirm() {
		$this->session->remove( 'csv_info', $this->name_space );

		$user_info = $this->session->get( 'user_info', $this->name_space );
		if ( ! $user_info ) {
			$this->display( 'user_info is null' );
		}

		$target_model = $this->request->get( 'target_model' );
		switch ( $target_model ) {
			case 'sales' :
				if ( ! $user_info['use_sales'] ) {
					$this->display( 'use_sales is not 1' );
				}
				break;
			case 'meeting' :
				break;
			case 'member' :
			case 'centre' :
			case 'terminal' :
			case 'free' :
				// 未対応
				$this->display( sprintf( 'type %s is 未実装', $target_model ) );
				break;
			default :
				$this->display( 'model (type) が 不正です' );
		}
		$this->template->assign( 'target_model', $target_model );
		$this->session->set( 'target_model', $target_model, $this->name_space );

		// 整合性チェックここから
		$rows = $this->getCSV('import_file');
		if ( count( $rows ) == 0 ) {
			$this->display( 'CSVデータがありません' );
		}

		// パース
		$optional = $this->request->get( 'optional' );
		$member_count = $user_info['member_count'];
		$count = array();
		$server_key = $this->objMgmUser->getOne( sprintf( 'user_id="%s"', mysql_real_escape_string($user_info['user_id']) ), 'server_key' );
		foreach ( $rows as &$row ) {
			$row['action'] = $row['flag'];
			if ( $row['flag'] === FLAG_UNDEF ) continue;

			// バリデーションチェック
			$objError = $row['objError'] = $this->objMember->check( $row, $this->rule );

			$row['user_key'] = $user_info['user_key'];
			$row['server_key'] = $server_key;

			// 既に登録されているか
			$mgm_count = $this->objMgmUser->numRows( sprintf( 'user_id = "%s"', mysql_real_escape_string($row['member_id']) ) );
			if ( 0 < $mgm_count ) {
				$where = sprintf( 'user_key=%d AND member_id="%s"', $user_info['user_key'], mysql_real_escape_string($row['member_id']) );
				$row['member_key'] = $this->objMember->getOne( $where, 'member_key' );
			}

			// フラグ制御
			switch ( $row['flag'] ) {
				case FLAG_ADD :
					if ( 0 < $mgm_count ) {
						// modify
						if ( $row['member_key'] ) {
							$row['flag'] = FLAG_MODIFY;
							$row['action'] = 'Modify';
							$count['modify'] ++;
							if ( $optional['password_lock'] ) {
								unset( $row['member_pass'] );
								$row['password_lock'] = true;
							}
						} else {
							$objError->set_error( 'member_id', null, 'member already registered' );
							continue 2;
						}
					} else {
						// add
						++ $member_count;
						$count['add'] ++;
						$row['auto_member_key'] = true;
						$row['action'] = 'Add';
					}
					break;
				case FLAG_DELETE :
					if ( $mgm_count < 1 || ! $row['member_key'] ) {
						$objError->set_error( 'member_id', null, 'member not registered' );
						continue 2;
					} else {
						$row['action'] = 'Delete';
						-- $member_count;
						$count['delete'] ++;
						// flag, member_id 以外の rule 定義された情報を削除
						foreach ( $row as $key => $val ) {
							if ( $key !== 'flag' && $key !== 'member_id' && array_key_exists( $key, $this->rule ) ){
								unset( $row[$key] );
							}
						}
					}
					continue 2;
			}

			// グループ指定
			if ( $row['member_group_name'] ) {
				$group_key = $this->getGroupKey( $user_info['user_key'], $row['member_group_name'], false );
				if ( $group_key ) {
					// 既存グループに所属
					$row['member_group'] = $group_key;
				} else {
					// 新規グループを作成
					$row['auto_group_key'] = true;
				}
			}

			$row['use_sales'] = $target_model == 'sales' ? 1 : 0;

			// パスワード制御
			if ( ! $row['password_lock'] && is_null( $row['member_pass'] ) ){
				if ( ! $optional['password_generate'] ) {
					$objError->set_error( 'member_pass', null, 'no data ( please check option )' );
				} else {
					$row['auto_password'] = true;
					$row['member_pass'] = $this->create_pw();
				}
			}

			// modify モードの時は 未定義補完しない
			if ( $row['flag'] === FLAG_MODIFY ) continue;
			// 未定義カラム制御
            if ($user_info['member_id_format_flg'] == 0) {
                if (!EZValidator::valid_vcube_id($row['member_id'])) {
                    $objError->set_error( 'member_id invalid' );
                } elseif (mb_strlen($row['member_id']) > 255) {
                    $objError->set_error( 'member_id invalid' );
                }
            }else{
                if (!preg_match('/^[[:alnum:]!#$%&\'*+\-\/=?^_`{|}~.@]{3,255}$/', $row['member_id'])) {
                    $objError->set_error( 'member_id invalid' );
                }
            }
			if ( is_null( $row['member_name'] ) ) $row['member_name'] = $row['member_id'];
			if ( is_null( $row['member_email'] ) ) {
				if ( $optional['email_use_member_id'] ) {
					if ( ! EZValidator::valid_email( $row['member_id'] ) ) {
						$objError->set_error( 'member_id', null, 'necessary email format (please uncheck "email" use "member_id" or "email" input into csv)' );
					} else {
						$row['member_email'] = $row['member_id'];
					}
				} else {
					$objError->set_error( 'member_email', null, 'cannot be null' );
				}
			}
			if ( is_null( $row['lang'] ) ) $row['lang'] = $optional['lang'];
			if ( is_null( $row['timezone'] ) ) $row['timezone'] = $optional['timezone'];
			if ( is_null( $row['use_shared_storage'] ) ) $row['use_shared_storage'] = $optional['use_shared_storage'];

			// $this->logger2->debug( $row, 'result row' );
		}
		unset( $row );// reference break

		// エラーチェック
		$is_error = false;
		$msg = parent::get_message( 'ERROR' );
		foreach ( $rows as &$row ) {
			$objError = $row['objError'];
			if ( EZValidator::isError( $objError ) ) {
				// $this->logger2->info($objError);
				$is_error = true;
				$error = array ();
				$error_fields = $objError->error_fields();
				foreach ( $error_fields as $key ) {
					// TODO type='allow' の拡張
					$type = $objError->get_error_type( $key );
					$rule = $objError->get_error_rule( $key, $type );
					$message = '';
					if ( is_array( $rule ) ) {
						$rule = join( ',', $rule );
					}
					if ( $msg[$type] ) {
						$message = sprintf( $msg[$type], $rule );
					} else {
						$message = $rule;
					}
					$error[$key] = $message;
				}
				$row['error'] = $error;
			}
			unset($row['objError']);
			$this->logger2->debug($row);
		}
		unset( $row );

		$this->template->assign( 'rows', $rows );
		$this->template->assign( 'count', $count );

		// 現在の登録件数チェック
		if ( $user_info['account_model'] == 'member' || $user_info['account_model'] == 'centre' || $user_info['use_sales'] == 1 ) {
			if ( $user_info['max_member_count'] < $member_count ) {
				$this->display( MEMBER_ERROR_OVER_COUNT );
			}
		}

		if ( $is_error ) {
			$this->display( 'CSVデータに誤りがあります' );
		}

		$this->logger2->debug($rows, 'debgu resut');
		$this->session->set( 'csv_info', $rows, $this->name_space);
		$this->display();
	}

	public function action_import_complete() {

		$user_info = $this->session->get( 'user_info', $this->name_space );
		$csv_info = $this->session->get( 'csv_info', $this->name_space );
		$target_model = $this->session->get( 'target_model', $this->name_space );
		$this->session->remove( 'user_info', $this->name_space );
		$this->session->remove( 'csv_info', $this->name_space );
		if ( ! $user_info || ! $csv_info ) {
			$this->display( 'session error' );
		}

		require_once 'classes/N2MY_Account.class.php';
		$obj_n2my_account = new N2MY_Account( $this->get_dsn() );

		$change_room_count = 0;
		foreach ( $csv_info as &$row ) {
			$result = false;
			switch ( $row['flag'] ) {
				case FLAG_ADD :
					$member_key = $this->addMember( $row );
					if ( ! $member_key ) break;
					$row['member_key'] = $member_key;
					$member_info = $this->objMember->getRow( sprintf( 'member_key= %d', $member_key ) );
					if ( $member_info['use_sales'] ) {
						$room_key = $this->addRoom( $user_info, $member_info );
						if ( ! $room_key ) break;
						++ $change_room_count;
						$row['room_key'] = $room_key;
					}
					$result = true;
					break;
				case FLAG_MODIFY :
					$result = $this->modifyMember( $row );
					break;
				case FLAG_DELETE :
					$obj_n2my_account->deleteMember( $row['member_key'] );
					$result = true;
					// fix max_storage_size 内包済み
					// --$change_room_count;
					break;
				default :
					break;
			}
			$row['status'] = $result;
		}
		unset( $row );

		// fix max_storage_size
		if ( $change_room_count != 0 ){ // 部屋の増減有り
			$max_storage_size = $user_info['max_storage_size'] + $change_room_count * $user_info['service_info']['add_storage_size'];
			if ( $max_storage_size < 0 ) $max_storage_size = 0;
			$this->objUser->update( array (
					'max_storage_size' => $max_storage_size
			), sprintf( 'user_key = %d', $user_info['user_key'] ) );
		}

		// 操作ログ
		$objOperationLog = new N2MY_DB( parent::get_auth_dsn(), 'operation_log' );
		$objOperationLog->add( array (
				'staff_key' => $_SESSION['_authsession']['data']['staff_key'],
				'action_name' => __FUNCTION__,
				'operation_datetime' => date( 'Y-m-d H:i:s' ),
				'table_name' => 'member',
				'keyword' => $user_info['user_id'],
				'info' => serialize( $csv_info ),
				'create_datetime' => date( 'Y-m-d H:i:s' )
		) );
		$this->logger2->info($csv_info, 'solved csv data');

		$this->session->set('result_info', $csv_info, $this->name_space);
		$this->display('import done. please check result status.');
	}

	public function action_export() {
		$user_id = trim( $this->request->get( 'user_id' ) );

		if ( ! $user_id ) {
			$this->display( 'please input user_id' );
		}

		$user_info = $this->objUser->getRow( sprintf( 'user_id = "%s"', addslashes( $user_id ) ) );
		if ( ! $user_info ) {
			$this->display( 'user_id is not found' );
		}

		$member_list = $this->objMember->getRowsAssoc( sprintf( 'user_key = %d AND member_status = 0', $user_info['user_key'] ) );
		if ( count( $member_list ) < 1 ) {
			$this->display( 'empty members' );
		}


		$group_list = $this->objMemberGroup->getRowsAssoc( sprintf( 'user_key = %d', $user_info['user_key'] ), null, null, null, 'member_group_name,member_group_key', 'member_group_key' );
		$display = $this->request->get( 'display' );
		$export_info = array ();
		foreach ( $member_list as $member_info ) {
			$row = array ();

			foreach ( $display as $key => $val) {
				if ( $display['is_flag'] ) $row['flag'] = $display['flag'];
				switch ( $key ) {
					case 'member_pass_encode' :
						$row['member_pass_encode'] = $member_info['member_pass'];
						break;
					case 'member_pass_decode' : // 復号化
						$row['member_pass'] = EZEncrypt::decrypt( VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_info['member_pass'] );
						break;
					case 'member_group_name' :
						$row['member_group_name'] = null;
						if ( $member_info['member_group'] ) {
							$row['member_group_name'] = $this->objMemberGroup->getOne( sprintf( 'member_group_key=%d', $member_info['member_group'] ), 'member_group_name' );
						}
						break;
					case 'room_key' :
						$row['room_key'] = $this->objMemberRoomRelation->getOne( sprintf( 'member_key="%s"', $member_info['member_key'] ), 'room_key' );
					case array_key_exists( $key, $member_info ) :
						$row[$key] = $member_info[$key];
						break;
				}
			}
			$export_info[] = $row;
		}

		$this->session->set( 'user_id', $user_id, $this->name_space );
		$this->session->set( 'export_info', $export_info, $this->name_space );
		$this->logger2->debug($export_info);
		$this->display();
	}

	private function getGroupKey($user_key, $group_name, $auto_create = false) {
		static $group_list;
		$group_name = trim( $group_name );

		if ( ! isset( $group_list ) ) {
			$where = sprintf( 'user_key=%d', $user_key );
			$group_list = $this->objMemberGroup->getRowsAssoc( $where, null, null, null, 'member_group_key,member_group_name', 'member_group_name' );
		}

		if ( isset( $group_list[$group_name]['member_group_key'] ) ) {
			return $group_list[$group_name]['member_group_key'];
		}

		if ( $auto_create && $group_name ) {
			$data = array(
					'user_key' => $user_key,
					'member_group_name' => $group_name
			);
			$data['member_group_key'] = $this->objMemberGroup->add( $data );
			$group_list[$group_name] = $data;
			return $this->getGroupKey( $user_key, $group_name, false );
		}

		return false;
	}

	private function addMember($row) {
		$data = array ();
		$data['member_id'] = $row['member_id'];
		$data['user_key'] = $row['user_key'];
		$data['member_pass'] = EZEncrypt::encrypt( VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $row['member_pass'] );
		$data['member_email'] = $row['member_email'];
		$data['member_name'] = $row['member_name'];
		$data['member_name_kana'] = $row['member_name_kana'];
		$data['member_status'] = 0;
		// $data['member_type'] = ''; //centre, terminal, free

		if ( $row['member_group_name'] ){
			if ( $row['auto_group_key'] ) {
				$data['member_group'] = $this->getGroupKey( $row['user_key'], $row['member_group_name'], true );
			} else {
				$data['member_group'] = $row['member_group'];
			}
		}

		$data['use_shared_storage'] = $row['use_shared_storage'];
		$data['create_datetime'] = date( 'Y-m-d H:i:s' );
		$data['timezone'] = $row['timezone'];
		$data['lang'] = $row['lang'];
		$data['use_sales'] = $row['use_sales'];

		$this->logger2->debug( $data, 'debug add data' );
		// TODO status check ?
		$this->objMgmUser->add( array (
					'user_id' => $row['member_id'],
					'server_key' => $row['server_key'],
					'user_registtime' => date( 'Y-m-d H:i:s' )
		) );
		$db = $this->objMember->add( $data );
		return $this->checkDB( $db ) ? $db : false;
	}

	private function addRoom($user_info, $member_info) {
		$db = array ();

		$count = $this->objRoom->numRows( sprintf( 'user_key=%d', $user_info['user_key'] ) );
		$room_key = sprintf( '%s-%d-%s', $user_info['user_id'], $count + 1, substr( md5( uniqid( time(), true ) ), 0, 4 ) );
		$service_info = $user_info['service_info'];

		// create room
		{
			// base data
			$room_info = array (
					'room_key' => $room_key,
					'room_name' => $member_info['member_name'],
					'user_key' => $user_info['user_key'],
					'room_sort' => $count + 1,
					'room_status' => 1,
					'use_sales_option' => $member_info['use_sales'],
					'room_registtime' => date( 'Y-m-d H:i:s' )
			);

			// whiteboard_filetype extension by config
			$image_ext = $this->config->getAll( 'DOCUMENT_IMAGES' );
			$document_ext = $this->config->getAll( 'DOCUMENT_FILES' );
			unset( $document_ext['xdw'] );
			$whiteboard_filetype = array (
					'document' => $document_ext,
					'image' => $image_ext
			);

			// fix plan
			$room_info += array (
					'max_seat' => $service_info['max_seat'],
					'max_audience_seat' => $service_info['max_whiteboard_seat'],
					'max_whiteboard_seat' => $service_info['max_whiteboard_seat'],
					'max_guest_seat' => $service_info['max_guest_seat_flg'] ? $service_info['max_guest_seat'] : 0,
					'max_guest_seat_flg' => $service_info['max_guest_seat_flg'] ? 1 : 0,
					'extend_seat_flg' => $service_info['extend_seat_flg'] ? 1 : 0,
					'extend_max_seat' => $service_info['extend_seat_flg'] ? $service_info['extend_max_seat'] : 0,
					'extend_max_audience_seat' => $service_info['extend_seat_flg'] ? $service_info['extend_max_audience_seat'] : 0,
					'meeting_limit_time' => $service_info['meeting_limit_time'],
					'ignore_staff_reenter_alert' => $service_info['ignore_staff_reenter_alert'] ? $service_info['ignore_staff_reenter_alert'] : 0,
					'default_camera_size' => $service_info['default_camera_size'],
					'hd_flg' => $service_info['hd_flg'],
					'disable_rec_flg' => $service_info['disable_rec_flg'],
					'active_speaker_mode_only_flg' => $service_info['active_speaker_mode_only_flg'],
					'active_speaker_mode_use_flg' => $service_info['active_speaker_mode_only_flg'],
					// 'use_stb_option' => $service_info['use_stb_plan'],
					'max_room_bandwidth' => $service_info['max_room_bandwidth'] ? $service_info['max_room_bandwidth'] : 0,
					'max_user_bandwidth' => $service_info['max_user_bandwidth'] ? $service_info['max_user_bandwidth'] : 0,
					'min_user_bandwidth' => $service_info['min_user_bandwidth'] ? $service_info['min_user_bandwidth'] : 0,
					'whiteboard_page' => $service_info['whiteboard_page'] ? $service_info['whiteboard_page'] : 100,
					'whiteboard_size' => $service_info['whiteboard_size'] ? $service_info['whiteboard_size'] : 20,
					'cabinet_size' => $service_info['cabinet_size'] ? $service_info['cabinet_size'] : 20,
					// 'customer_sharing_button_hide_status' => 1,
					'default_agc_use_flg' => 1,
					'default_layout_16to9' => 'normal',
					'whiteboard_filetype' => serialize( $whiteboard_filetype ),
					'room_updatetime' => date( 'Y-m-d H:i:s' )
			);
			$db[] = $this->objRoom->add( $room_info );
		}

		// fix relations
		{
			// mgm mfp
			$db[] = $this->objRelation->add( array (
					'relation_key' => $room_key,
					'relation_type' => 'mfp',
					'user_key' => $user_info['user_id'],
					'status' => 1,
					'create_datetime' => date( 'Y-m-d H:i:s' )
			) );

			// plan
			$room_plan_data = array (
					'room_key' => $room_key,
					'room_plan_status' => 1,
					'service_key' => $service_info['service_key'],
					'room_plan_starttime' => date( 'Y-m-d 00:00:00' ),
					'room_plan_endtime' => '0000-00-00 00:00:00',
					'room_plan_registtime' => date( 'Y-m-d H:i:s' )
			);
			$db[] = $this->objRoomPlan->add( $room_plan_data );

			// options
			// TODO teleconference, video_conference
			foreach ( $user_info['options'] as $option ) {
				$db[] = $this->objOrderServiceOption->add( array (
						'room_key' => $room_key,
						'user_service_option_key' => $option['user_service_option_key'],
						'service_option_key' => $option['service_option_key'],
						'ordered_service_option_status' => 1,
						'ordered_service_option_starttime' => $option['ordered_service_option_starttime'],
						'ordered_service_option_registtime' => date('Y-m-d H:i:s')
				) );
			}
		}

		if ( $member_info['use_sales'] ) {
			$db[] = $this->solveSalesRoom( $room_key, $user_info, $member_info );
		}


		return $this->checkDB( $db ) ? $room_key : false;
	}

	private function solveSalesRoom($room_key, $user_info, $member_info){
		$db = array();
		// data relation
		{
			$db[] = $this->objMemberRoomRelation->add( array (
					'member_key' => $member_info['member_key'],
					'room_key' => $room_key,
					'create_datetime' => date( 'Y-m-d H:i:s' )
			) );
		}

		// inbound
		$inbound = $this->objInbound->numRows(sprintf('user_key=%d', $user_info['user_key']));
		if( ! $inbound ){
			$count = 1;
			while ( 0 < $count ) {
				$inbound_id = substr( $this->create_pw(), 0, 8 );
				$count = $this->objInbound->numRows( sprintf( 'inbound_id="%s"', $inbound_id ) );
			}
			$db[] = $this->objInbound->add( array (
					'inbound_id' => $inbound_id,
					'user_key' => $user_info['user_key'],
					'status' => 1,
					'create_datetime' => date( 'Y-m-d H:i:s' )
			) );

			// mgm relation
			$db[] = $this->objRelation->add( array (
					'relation_key' => $inbound_id,
					'relation_type' => 'inbound_id',
					'user_key' => $user_info['user_id'],
					'status' => 1,
					'create_datetime' => date( 'Y-m-d H:i:s' )
			) );
		}

		// outbound
		{
			$count = $this->objOutbound->numRows( sprintf( 'user_key="%s"', $user_info['user_key'] ) );
			if ( ! $count ) {
				$db[] = $this->objOutbound->add( array (
						'user_key' => $user_info['user_key'],
						'status' => 1,
						'create_datetime' => date( 'Y-m-d H:i:s' )
				) );
			}

			$count = 1;
			while ( 0 < $count ) {
				$outbound_id = $this->create_pw();
				$count = $this->objMember->numRows( sprintf( 'outbound_id="%s"', $outbound_id ) );
			}
			$outbound_key = $this->objOutbound->getOne( sprintf( 'user_key=%d', $user_info['user_key'] ), 'outbound_key' );
			$sales_member_count = $this->objMember->numRows( sprintf( 'outbound_id != "" AND user_key = %d', $user_info['user_key'] ) );
			$outbound_number_id = sprintf( '%03d%03d', $outbound_key, $sales_member_count + 1 );

			// member update
			$db[] = $this->objMember->update( array (
					'outbound_id' => $outbound_id,
					'outbound_number_id' => $outbound_number_id,
					'use_sales' => 1
			), sprintf( 'member_key = %d', $member_info['member_key'] ) );

			// room update
			$db[] = $this->objRoom->update( array (
					'outbound_id' => $outbound_id
			), sprintf( 'room_key = "%s"', $room_key ) );

			// mgm relation
			$db[] = $this->objRelation->add( array (
					'relation_key' => $outbound_id,
					'relation_type' => 'outbound',
					'user_key' => $member_info['member_id'],
					'status' => 1,
					'create_datetime' => date( 'Y-m-d H:i:s' )
			) );
			$db[] = $this->objRelation->add( array (
					'relation_key' => $outbound_number_id,
					'relation_type' => 'outbound_number',
					'user_key' => $member_info['member_id'],
					'status' => 1,
					'create_datetime' => date( 'Y-m-d H:i:s' )
			) );
		}

		return $this->checkDB( $db );
	}

	private function modifyMember($row) {
		$member_key = $row['member_key'];
		$user_key = $row['user_key'];

		$data = array();
		$columns = array_keys( $this->rule );
		foreach ( $row as $key => $val ) {
			// 定義されたカラム以外は変更不可
			if ( in_array( $key, $columns ) ) {
				$data[$key] = $val;
			}
		}

		// 難読化対応
		if ( ! is_null( $data['member_pass'] ) ) {
			$data['member_pass'] = EZEncrypt::encrypt( VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $data['member_pass'] );
		}

		if ( $data['member_group_name'] ) {
			$data['member_group'] = $this->getGroupKey( $user_key, $row['member_group_name'], true );
		}

		// 制御用なので除外
		unset( $data['member_id'] );
		unset( $data['flag'] );
		unset( $data['member_group_name'] );

		$this->logger2->debug( $data, 'debug update data' );
		$db = $this->objMember->update( $data, sprintf( 'member_key=%s', $member_key ) );

		return $this->checkDB( $db );
	}

	private function checkDB($db){
		if ( ! is_array( $db ) ) $db = array ( $db );

		foreach ( $db as $node ) {
			if ( DB::isError( $node ) || $node === false ){
				$this->logger2->error( $node->getUserInfo() );
				return false;
			}
		}
		return true;
	}

	/**
	 *
	 * @see AppFrame::get_dsn()
	 */
	public function get_dsn() {
		static $dsn;
		if ( $dsn ) {
			return $dsn;
		} else {
			$id = $this->request->get( 'user_id' );
			if ( ! $id && $user_info = $this->session->get( 'user_info', $this->name_space ) ) {
				$id = $user_info['user_id'];
			}
			if ( $id ) {
				require_once 'classes/mgm/MGM_Auth.class.php';
				$obj_MGMClass = new MGM_AuthClass( parent::get_auth_dsn() );
				if ( ! $user_info = $obj_MGMClass->getUserInfoById( $id ) ) {
					$this->display( 'ユーザーIDが間違っています' );
					return false;
				}
				if ( ! $server_info = $obj_MGMClass->getServerInfo( $user_info['server_key'] ) ) {
					$this->display( 'サーバー情報が有りません' );
					return false;
				}
			}
		}
		$this->logger2->debug( $server_info );
		$dsn = $server_info['dsn'];
		return $dsn;
	}

	/**
	 * 乱数PW発行
	 */
	private function create_pw() {
		list ( $a, $b ) = explode( ' ', microtime() );
		$a = (int) ($a * 100000);
		$a = strrev( str_pad( $a, 5, '0', STR_PAD_LEFT ) );
		$c = $a . $b;
		// 36
		// print (base_convert($c, 10, 36));
		// 62
		$c .= rand( 2, 9 );
		return $this->dec2any( $c );
	}

	/**
	 * 数値から62進数のID発行
	 */
	private function dec2any($num, $base = 57, $index = false) {
		if ( ! $base ) {
			$base = strlen( $index );
		} else if ( ! $index ) {
			$index = substr( '23456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ', 0, $base );
		}
		$out = '';
		for( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t -- ) {
			$a = floor( $num / pow( $base, $t ) );
			$out = $out . substr( $index, $a, 1 );
			$num = $num - ($a * pow( $base, $t ));
		}
		return $out;
	}

}

$main = & new MemberCSV();
$main->execute();