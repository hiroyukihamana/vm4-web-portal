<?php
/*
 * Created on 2008/11/18
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

require_once("classes/AppFrame.class.php");
require_once('classes/mgm/dbi/FmsServer.dbi.php');

class AppFmsService extends AppFrame {

	function init() {
	}
	
	/**
	 * 状態変更
	 */
	function default_view() {
		$fms = $this->request->get("fms");
		$status = $this->request->get("status");
        $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
        if ($obj_FmsServer->setStatus($fms, $status) == true) {
        	print "OK";
        } else {
        	print "NG";
        }
	}
}

$main = new AppFmsService();
$main->execute();
?>
