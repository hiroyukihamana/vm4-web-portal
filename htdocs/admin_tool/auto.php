<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/N2MY_IvesClient.class.php");
require_once("classes/mcu/config/McuConfigProxy.php");

class AppUserCreate extends AppFrame {

    function init() {
        $this->_name_space = md5(__FILE__);
        $this->_sess_page = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->configProxy 	= new McuConfigProxy();
        define("WSDL_DOMAIN", "mcu02.nice2meet.us:8080");
    }

    function default_view() {
        $this->clear_session_data();
        $this->session->remove("option_data", $this->_name_space);
        $this->session->remove("user_option_data", $this->_name_space);
        $this->display_form();
    }

    function action_confirm() {
        $data = $this->request->getAll();
        $this->set_session_data($data);
        // 入力チェック
        $rules = array(
            "country_key" => array(
                "required" => true,
                ),
            "name" => array(
                "required" => true
                ),
            "prefix" => array(
                "required" => true,
                "alnum"    => true,
                ),
            "starttime" => array(
                "required" => true,
                ),
            "max_member_count" => array(
                "numeric" => true,
                ),
            "clip_share_storage_size" => array(
                "numeric" => true,
                ),
            "start" => array(
                "required" => true,
                "numeric"  => true,
                ),
            "end" => array(
                "required" => true,
                "numeric"  => true,
                "maxval"   => 1000,
                ),
          "meeting_version" => array(
            "required" => true
            ),
            "user_status" => array(
                "required" => true
                ),
            "invoice_flg" => array(
                "required" => true
                ),
            "payment_terms" => array(
                "required" => true
                ),
            "digit" => array(
                "required" => true,
                "numeric"  => true,
                "maxval"   => 10,
                ),
            "max_seat" => array(
                "numeric" => true,
                ),
            "max_audience_seat" => array(
                "numeric" => true,
                ),
            "max_whiteboard_seat" => array(
                "numeric" => true,
                ),
            "max_storage_size" => array(
                    "numeric" => true,
            ),
            "room_name" => array(
                "required" => true
                ),
            "service_key" => array(
                "required" => true
                ),
            "plan_button" => array(
                "required" => true
                ),
            );
        $this->check_obj = new EZValidator($data);
        foreach($rules as $field => $rules) {
            $this->check_obj->check($field, $rules);
        }
        $this->display_form();
    }

    function display_form() {
        $form_data = $this->get_session_data();
        $option_data = $this->session->get("option_data", $this->_name_space);
        $user_option_data = $this->session->get("user_option_data",  $this->_name_space);
        // プラン一覧
        $plan_db = new N2MY_DB($this->account_dsn, "service");
        $where = "service_status='1'";
        $plan_list = $plan_db->getRowsAssoc($where, null, null, null, "service_key,service_name", "service_key");
        $this->template->assign("plan_list", $plan_list);
        // エラーメッセージ
        $msg_format = $this->get_message("error");
        $msg = $this->get_message("ERROR");
        $list = array();
        $form_error = array();
        $err_flg = false;
        // 入力エラーチェック
        if (EZValidator::isError($this->check_obj)) {
            $err_fields = $this->check_obj->error_fields();
            $form_error = array();
            foreach ($err_fields as $key) {
                $type = $this->check_obj->get_error_type($key);
                $_msg = sprintf($msg[$type], $this->check_obj->get_error_rule($key, $type));
                $form_error[$key] = sprintf($msg_format, $_msg);
                $err_flg = 1;
            }
            if(!empty($form_error["plan_button"]))
              $form_error["plan_button"] = sprintf($msg_format, "Add button required");
            $this->logger2->info($form_error);
        } else {
            // 入力値再現
            $prefix = $form_data["prefix"];
            $start  = $form_data["start"];
            $end    = $form_data["end"];
            $user_pw_flg = $form_data["user_pw"];
            $user_admin_pw_flg = $form_data["user_admin_pw"];
            $start = (int)$start;
            $end = (int)$end + $start;
            $count = $end - $start;
            $len = strlen($count);
            if ($len < $form_data["digit"]) {
                $len = $form_data["digit"];
            }
            for ($i = $start; $i < $end; $i++) {
                // 1件でも重複があったら作成しない
                $id = $prefix. str_pad($i, $len, "0", STR_PAD_LEFT);
                $user_db = new N2MY_DB($this->account_dsn, "user");
                $where = "user_id='".mysql_real_escape_string($id)."'";
                $count = $user_db->numRows($where);
                $error = array();
                if (is_numeric($count) && $count > 0) {
                    $error["user_id"] = sprintf($msg_format, $msg["duplicate"]);
                    $err_flg = true;
                }
                if ($user_pw_flg == "1") {
                    $user_pw = $id;
                } else {
                    $user_pw = "(ランダム)";
                }
                if ($user_admin_pw_flg == "1") {
                    $user_admin_pw = $id;
                } else {
                    $user_admin_pw = "(ランダム)";
                }
                // 部屋名
                if ($form_data["room_name"] == 'user_id') {
                    $room_name = $id;
                } else {
                    $room_name = $form_data["name"];
                }
                $list[] = array(
                    "user_id" => $id,
                    "user_pw" => $user_pw,
                    "user_admin_pw" => $user_admin_pw,
                    "user_name" => $form_data["name"],
                    "room_name" => $room_name,
                    "user_status" => $form_data["user_status"],
                    "service_key" => $form_data["service_key"],
                    "error" => $error,
                );
            }
        }
        //代理店取得
        $agency_db = new N2MY_DB($this->account_dsn, "agency");
        $agency_list = $agency_db->getRowsAssoc( sprintf( "is_deleted='%s'", "0" ) );

        // オプション情報取得
        $service_option_db = new N2MY_DB($this->account_dsn, "service_option");
        $service_option    = $service_option_db->getRowsAssoc("use_user_option = 0");
        $user_option    = $service_option_db->getRowsAssoc("use_user_option = 1");
        $option_db         = new N2MY_DB($this->get_dsn(), "ordered_service_option");
        $option_columns    = $this->_get_relation_data($this->account_dsn, $option_db->getTableInfo("data"));
        $this->template->assign(array("service_option" => $service_option,
                                      "option_columns" => $option_columns,
                                      "user_option"    => $user_option
                ));
        $this->template->assign("agency_list", $agency_list);
        $this->template->assign("err_flg", $err_flg);
        $this->template->assign("error", $form_error);
        $this->template->assign("rows", $list);
        $this->template->assign("option_date_default", date("Y-m-d 00:00:00"));
        if (!empty($option_data["service_option"])) {
            $this->template->assign("service_option_data", $option_data["service_option"]);
        }
        if (!empty($user_option_data["user_option"])) {
            $this->template->assign("user_option_data", $user_option_data["user_option"]);
        }
        $this->display( "admin_tool/auto/index.t.html" );
    }


    /**
     * ルームプラン設定（プランに合わせてオプションも登録）
     */
    function action_room_plan() {
        $this->session->remove("option_data", $this->_name_space);
        $service_db = new N2MY_DB($this->account_dsn, "service");
        $form_data = $this->request->getAll();
        $this->set_session_data($form_data);
        $this->check_obj = new EZValidator($form_data["service_key"]);
        $this->check_obj->check("service_key", array("required" => true, "numeric" => true));
        if (EZValidator::isError($this->check_obj)) {
            $this->display_form();
            return;
        }
        $service_key = $form_data["service_key"];
        $service_info = $service_db->getRow("service_key = ".$service_key);
        $this->logger2->info($service_key);
        //プランのオプション情報取得
        $add_options = array();
        if ($service_info["meeting_ssl"] == 1) {
            $add_options[] = "2";
        }
        if ($service_info["desktop_share"] == 1) {
            $add_options[] = "3";
        }
        if ($service_info["high_quality"] == 1) {
            $add_options[] = "5";
        }
        if ($service_info["mobile_phone"] > 0) {
            $add_options[] = "6";
        }
        if ($service_info["h323_client"] > 0) {
            $add_options[] = "8";
        }
        //hdd_extentionは数値分登録
        if ($service_info["hdd_extention"] > 0) {
            $add_options[] = "4";
        }
        // ホワイトボードプラン
        if ($service_info["whiteboard"] == 1) {
            $add_options[] = "16";
        }
        // マルチカメラ
        if ($service_info["multicamera"] == 1) {
            $add_options[] = "18";
        }
        // 電話連携
        if ($service_info["telephone"] == 1) {
            $add_options[] = "19";
        }
        // 録画GW
        if ($service_info["record_gw"] == 1) {
            $add_options[] = "20";
        }
        // スマートフォン
        if ($service_info["smartphone"] == 1) {
            $add_options[] = "21";
        }
        // 資料共有映像再生許可
        if ($service_info["whiteboard_video"] == 1) {
            $add_options[] = "22";
        }
        // PGi連携
        if ($service_info["teleconference"] == 1) {
            $add_options[] = "23";
        }
        // 最低帯域アップ
        if ($service_info["video_conference"] == 1) {
            $add_options[] = "24";
        }
        // 最低帯域アップ
        if ($service_info["minimumBandwidth80"] == 1) {
            $add_options[] = "25";
        }
        // 最低帯域アップ
        if ($service_info["h264"] == 1) {
            $add_options[] = "26";
        }
        //オプション登録
        if (!empty($add_options)) {
            foreach ($add_options as $key => $value) {
                if ("4" == $add_options) {
                   for ($num = 1; $num <= $service_info["hdd_extention"]; $num++ ) {
                        $this->action_room_option($value);
                    }
                } else if ("6" == $add_options) {
                    for ($num = 1; $num <= $service_info["mobile_phone"]; $num++ ) {
                        $this->action_room_option($value);
                    }
                } else if ("8" == $add_options) {
                    for ($num = 1; $num <= $service_info["h323_client"]; $num++ ) {
                        $this->action_room_option($value);
                    }
                } else {
                    $this->action_room_option($value);
                }
            }
        }

        $this->template->assign("max_seat", $service_info["max_seat"]);
        $this->template->assign("max_audience_seat", $service_info["max_audience_seat"]);
        $this->template->assign("max_whiteboard_seat", $service_info["max_whiteboard_seat"]);
        $this->template->assign("service_key", $service_key);
        $this->template->assign("plan_button", "1");
        $this->display_form();
    }

    /* オプション追加
     */
    function action_room_option($add_options = false) {
          $session_option_data = $this->session->get("option_data", $this->_name_space);
          $form_data = $this->request->getAll();
          $service_key = $form_data["service_key"];
          $this->logger2->info($service_key);

          $this->check_obj = new EZValidator($service_key);
          $this->check_obj->check("service_key", array("required" => true, "numeric" => true));
          if (EZValidator::isError($this->check_obj)) {
              $this->display_form();
              return;
          }
          if ($form_data["service_option_key"] == 24) {// ivesconference
          $this->logger2->info($form_data["video_conference_profileId"]);
              if (!$form_data["video_conference_profileId"]) {
                  $this->template->assign("err_flg", 1);
                  $form_error = array("option", "<br /><span class='attention'>入力必須です</span>");
                  $this->template->assign("error", $form_error);
                  $this->display_form();
                  return;
              }
          }
          // オプション情報取得
          $service_option_db = new N2MY_DB($this->account_dsn, "service_option");
          $service_option    = $service_option_db->getRowsAssoc();
          $service_option_key = $form_data["service_option_key"];
          $option_date = date("Y-m-d 00:00:00");
          if ($add_options) {
              foreach ($service_option as $key => $value) {
                  if ($value["service_option_key"] == $add_options) {
                      $service_option_name = $value["service_option_name"];
                  }
              }
              if (empty($session_option_data["service_option"])) {
                  $service_option = array(array("service_option_key" => $add_options,
                                          "option_start_time" => $option_date,
                                          "service_option_name" => $service_option_name));
              } else {
                  $service_option = array_merge($session_option_data["service_option"],
                      array(array("service_option_key" => $add_options,
                            "option_start_time" => $option_date,
                            "service_option_name" => $service_option_name)));
              }
          } else {
              if (empty($form_data["option_start_time"])) {
                  $option_start_time = date("Y-m-d 00:00:00");
              } else {
                  $option_start_time = $form_data["option_start_time"];
              }
              foreach ($service_option as $key => $value) {
                  if ($value["service_option_key"] == $service_option_key) {
                    $service_option_name = $value["service_option_name"];
                  }
              }
              if (!empty($session_option_data)) {
                  $service_option = array_merge($session_option_data["service_option"],
                      array(array("service_option_key" => $service_option_key,
                                          "option_start_time" => $option_start_time,
                                          "service_option_name" => $service_option_name)));
              } else {
                  $service_option = array(array("service_option_key" => $service_option_key,
                                          "option_start_time" => $option_start_time,
                                          "service_option_name" => $service_option_name));
              }
          }
          // スマートフォン連携時に電話連携がなければセット
          if ($service_option_key == "21") {
              foreach ($session_option_data["service_option"] as $key => $value) {
                  if ($value["service_option_key"] == "19") {
                      $telephone_option = true;
                  }
              }
              if (!$telephone_option) {
                   $service_option = array_merge($service_option,
                       array(array("service_option_key" => "19",
                           "option_start_time" => $option_date,
                           "service_option_name" => "telephone")));
              }
          }
          $service_db = new N2MY_DB($this->account_dsn, "service");
          $service_info = $service_db->getRow("service_key = ".$service_key);
          $max_seat = $service_info["max_seat"];
          $max_whiteboard_seat =  $service_info["max_whiteboard_seat"];
          $audience_count = "0";
          foreach ($service_option as $key => $value) {
              if ($value["service_option_key"] == "7") {
                  $audience_count++;
              }
          }

          if ("0" < $audience_count) {
              if ($service_key == "45" && $service_key == "46" && $service_key == "47") {
                  $this->logger2->error("not add audience");
              } else {
                  $max_seat = "9";
                  $max_audience_seat = 10 * $audience_count;
              }
          } else {
              $max_audience_seat = $service_info["max_audience_seat"];
          }

          $this->template->assign("service_key", $service_key);
          $this->template->assign("max_seat", $max_seat);
          $this->template->assign("max_audience_seat", $max_audience_seat);
          $this->template->assign("max_whiteboard_seat", $service_info["max_whiteboard_seat"]);
          $this->template->assign("service_option_data", $service_option);

          $this->session->set("option_data",
                              array("service_option" => $service_option),
                              $this->_name_space);
          if (!$add_options) {
              $this->display_form();
          }
    }

    /*
     * ユーザーオプション追加
     */
    function action_user_option(){
        $service_option_db = new N2MY_DB($this->account_dsn, "service_option");
        $session_user_option_data = $this->session->get("user_option_data", $this->_name_space);
        $form_data = $this->request->getAll();
        $user_option_key = $form_data["user_option_key"];
        $user_option_start_time = $form_data["user_option_start_time"];
        $user_option    = $service_option_db->getRow("service_option_key = " .$user_option_key. " AND use_user_option = 1");
        $user_option_name = $user_option["service_option_name"];
        $this->logger2->info($user_option_key);
        if (!empty($session_user_option_data)) {
            $user_option = array_merge($session_user_option_data["user_option"],
                    array(array("service_option_key" => $user_option_key,
                            "option_start_time" => $user_option_start_time,
                            "service_option_name" => $user_option_name)));
        } else {
            $user_option = array(array("service_option_key" => $user_option_key,
                    "option_start_time" => $user_option_start_time,
                    "service_option_name" => $user_option_name));
        }

        $this->session->set("user_option_data", array("user_option" => $user_option), $this->_name_space);
        $this->display_form();
    }

    // ユーザーオプションクリア
    function action_user_option_clear(){
        $this->session->remove("user_option_data", $this->_name_space);
        $this->display_form();
    }


    function _get_relation_data($dsn, $columns) {
        foreach ($columns as $key => $value) {
            if (isset($value["item"]["relation"])) {
                $relation = $value["item"]["relation"];
                if ($relation["db_type"] == "account") {
                    $relation_db = new N2MY_DB($this->account_dsn, $relation["table"]);
                } else {
                    $relation_db = new N2MY_DB($dsn, $relation["table"]);
                }
                if ($relation["status_name"]) {
                    $where = $relation["status_name"]."='".$relation["status"]."'";
                    $relation_data = $relation_db->getRowsAssoc($where);
                } else {
                    $relation_data = $relation_db->getRowsAssoc();
                }
                if ($relation["default_key"] || $relation["default_value"]) {
                    $columns[$key]["item"]["relation_data"][$relation["default_key"]] = $relation["default_value"];
                }
                foreach ($relation_data as $_data) {
                    $columns[$key]["item"]["relation_data"][$_data[$relation["key"]]] = $_data[$relation["value"]];
                }
            }
        }
        return $columns;
    }

    function get_dsn() {
        if (isset($_SESSION[$this->_sess_page]["dsn"])) {
            return $_SESSION[$this->_sess_page]["dsn"];
        }
    }

    function set_session_data($form_data) {
        $this->session->set("data", $form_data, $this->_name_space);
    }

    function get_session_data() {
        return $this->session->get("data", $this->_name_space);
    }

    function clear_session_data() {
        $this->session->remove("data", $this->_name_space);
    }

    function getRule($columns) {
        $rules = array();
        foreach($columns as $colum => $colum_info) {
            if (($colum_info["flags"]["not_null"] == "1") && ($colum_info["flags"]["auto_increment"] == "")) {
                $rules[$colum]["required"] = true;
            }
        }
        return $rules;
    }

    function action_create() {
        // プラン一覧
        $plan_db = new N2MY_DB($this->account_dsn, "service");
        $where = "service_status='1'";
        $plan_list = $plan_db->getRowsAssoc($where, null, null, null, "service_key,service_name", "service_key");
        // エラーメッセージ
        $msg_format = $this->get_message("error");
        $msg = $this->get_message("ERROR");
        // ユーザDB
        $dsn = $this->get_dsn();
        $data_db = new N2MY_DB($dsn, "user");
        $form_data = $this->get_session_data();
        $country_key = $form_data["country_key"];
        $prefix = $form_data["prefix"];
        $start  = $form_data["start"];
        $end    = $form_data["end"];
        $user_pw_flg = $form_data["user_pw"];
        $user_admin_pw_flg = $form_data["user_admin_pw"];
        $start = (int)$start;
        $end = (int)$end + $start;
        $count = $end - $start;
        $len = strlen($count);
        $today = date("Y-m-d 23:59:59");
        if ($len < $form_data["digit"]) {
            $len = $form_data["digit"];
        }
        $service_key  = $form_data["service_key"];
        $create_list = $this->request->get("create_list");

        $user_db     = new N2MY_DB($this->get_dsn(), "user");
        $user_columns = $user_db->getTableInfo("data");
        $user_columns = $this->_get_relation_data($this->account_dsn, $user_columns);
        $this->logger->trace("new_columuns",__FILE__,__LINE__,$user_columns);
        // ストレージ容量算出
        if($form_data["max_storage_size"] === ""){
            // プランにストレージ容量を設定
            $service_db = new N2MY_DB($this->account_dsn, "service");
            $service_info = $service_db->getRow("service_key = ".$service_key);
            $form_data["max_storage_size"] = $service_info["add_storage_size"];
        }
        $session_user_option_data = $this->session->get("user_option_data", $this->_name_space);
        foreach($session_user_option_data["user_option"] as $user_option){
            $service_option_name = $user_option["service_option_name"];
            $user_service_status = ( strtotime( $user_option["option_start_time"] ) >= strtotime( $today ) ) ? 2 : 1;
            if(defined($service_option_name) && is_numeric(constant($service_option_name)) && $user_service_status == 1){
                $form_data["max_storage_size"] = $form_data["max_storage_size"] + constant($service_option_name);
            }
        }
        for ($i = $start; $i < $end; $i++) {
            $id = $prefix. str_pad($i, $len, "0", STR_PAD_LEFT);
            if (!in_array($id, $create_list)) {
                continue;
            }
            if ($user_pw_flg == "1") {
                $user_pw = $id;
            } else {
                $user_pw = $this->create_id().rand(2,9);
            }
            if ($user_pw_flg == "1") {
                $user_pw = $id;
            } else {
                $user_pw = $this->create_id().rand(2,9);
            }
            if ($user_admin_pw_flg == "1") {
                $user_admin_pw = $id;
            } else {
                $user_admin_pw = $this->create_id().rand(2,9);
            }
            if ($user_admin_pw_flg == "1") {
                $user_admin_pw = $id;
            } else {
                $user_admin_pw = $this->create_id().rand(2,9);
            }
            // ユーザー登録
            $user_data = array(
                "country_key"             => $country_key,
                "user_company_name"       => $form_data["name"],
                "user_id"                 => $id,
                "user_password"           => $user_pw,
                "user_admin_password"     => $user_admin_pw,
                "user_company_postnumber" => $form_data["postnumber"],
                "user_company_address"    => $form_data["address"],
                "user_company_phone"      => $form_data["phone"],
                "user_company_fax"        => $form_data["fax"],
                "user_staff_department"   => $form_data["department"],
                "user_staff_lastname"     => $form_data["lastname"],
                "user_staff_firstname"    => $form_data["firstname"],
                "user_staff_email"        => $form_data["email"],
                "meeting_version"         => $form_data["meeting_version"],
                "lang_allow"			  => $form_data["lang_allow"],
                "intra_fms"               => $form_data["intra_fms"],
                "user_status"             => $form_data["user_status"],
                "user_delete_status"      => $form_data["user_delete_status"],
                "account_model"           => $form_data["account_model"],
                "max_member_count"        => $form_data["max_member_count"],
                "invoice_flg"             => $form_data["invoice_flg"],
                "promotion_flg"           => $form_data["promotion_flg"],
                "payment_terms"           => $form_data["payment_terms"],
                "memo"                    => $form_data["memo"],
                "has_chat"                => $form_data["has_chat"],
                "is_cybozu_option"        => $form_data["is_cybozu_option"],
                "use_clip_share"          => $form_data["use_clip_share"],
                "clip_share_storage_size" => $form_data["clip_share_storage_size"],
                "service_name"            => $form_data["service_name"],
                "max_storage_size"        => $form_data["max_storage_size"],
                "user_starttime"          => $form_data["starttime"],
                "user_registtime"         => date("Y-m-d H:i:s"),
                "user_deletetime"         => $form_data["deletetime"],
            );
            $columns = $data_db->getTableInfo("data");
            // デフォルト値をセット
            foreach ($columns as $_key => $data) {
                if (!$user_data[$_key]) {
                    $user_data[$_key] = $data["default"];
                }
            }
            $rules = $this->getRule($columns);
            // 入力チェック
            $err_obj = $data_db->check($user_data, $rules);
            $count = 0;
            // 重複エラーチェック
            $user_db = new N2MY_DB($this->account_dsn, "user");
            $where = "user_id = '".htmlspecialchars($user_data["user_id"])."'";
            $count = $data_db->numRows($where);
            //userの場合はアカウントテーブルもチェック
            $where = "user_id='".mysql_real_escape_string($user_data["user_id"])."'";
            $count = $user_db->numRows($where);
            if (is_numeric($count) && $count > 0) {
                $err_obj->set_error("user_id", USER_ERROR_ID_DUPLICATE);
            }
            //ユーザーID
            if (!preg_match('/^[[:alnum:]._-]{3,30}$/', $user_data['user_id'])) {
                $err_obj->set_error("user_id", USER_ERROR_ID_LENGTH);
            }
            //ユーザーパスワードをチェック
            if (!preg_match('/^[!-\[\]-~]{8,128}$/', $user_data['user_password'])) {
                $err_obj->set_error("user_password", USER_ERROR_PASS_INVALID_01);
            } elseif (!preg_match('/[[:alpha:]]+/',$user_data['user_password'])
                     || preg_match('/^[[:alpha:]]+$/',$user_data['user_password'])) {
                $err_obj->set_error("user_password", USER_ERROR_PASS_INVALID_02);
            }
            //管理者パスワード
            if (!preg_match('/^[!-\[\]-~]{8,128}$/', $user_data['user_admin_password'])) {
                $err_obj->set_error("user_admin_password", USER_ERROR_PASS_INVALID_01);
            } elseif (!preg_match('/[[:alpha:]]+/',$user_data['user_admin_password'])
                     || preg_match('/^[[:alpha:]]+$/',$user_data['user_admin_password'])) {
                $err_obj->set_error("user_admin_password", USER_ERROR_PASS_INVALID_02);
            }
            // 入力エラーチェック
            if (EZValidator::isError($err_obj)) {
                $err_fields = $err_obj->error_fields();
                $error = array();
                foreach ($err_fields as $key) {
                    $type = $err_obj->get_error_type($key);
                    $error[$key] = sprintf("%s : %s", $key, $type);
                    $err_flg = 1;
                }
                $user_data["error"] = $error;
            } else {
                $agency = $form_data["agency"];
                unset($user_data["agency"]);
                $user_data["user_registtime"] = date("Y-m-d H:i:s");
                if ($form_data["group"]) {
                    $user_data["user_group"] = $prefix;
                }
                //パスワード難読化
                require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
                $user_data['user_password'] =  EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_data['user_password']);
                $user_data['user_admin_password'] =  EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_data['user_admin_password']);
                // ユーザー情報追加
                $this->logger2->debug($data_db);
                $data_db->add($user_data);

                //表示用に復号化
                $user_data['user_password'] =  EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_data['user_password']);
                $user_data['user_admin_password'] =  EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_data['user_admin_password']);
                // 認証DB追加
                $auth_data = array(
                    "user_id" => $user_data["user_id"],
                    "server_key" => $this->session->get("now_server_key"),
                    "user_registtime" => date("Y-m-d H:i:s"),
                    );
                $user_db->add($auth_data);
                // ユーザー情報
                $user_info = $data_db->getRow("user_id = '".mysql_real_escape_string($user_data["user_id"])."'");

                if ($agency) {
                    $agency_rel_db = new N2MY_DB($dsn, "agency_relation_user");
                    $data = array("user_key" => $user_info["user_key"],
                                  "agency_id" => $agency);
                    $ret = $agency_rel_db->add($data);
                    if (DB::isError($ret)) {
                        $this->logger2->error($ret->getUserInfo());
                        return false;
                    }
                }

                //部屋登録
                $where = "user_key='".addslashes($user_info["user_key"])."'";
                $room_db = new N2MY_DB($this->get_dsn(), "room");
                $count = $room_db->numRows($where);
                $room_key = $user_data["user_id"]."-".($count + 1)."-".substr(md5(uniqid(time(), true)), 0,4);
                // 部屋名
                if ($form_data["room_name"] == 'user_id') {
                    $room_name = $id;
                } else {
                    $room_name = $form_data["name"];
                }
                $room_data = array (
                    "room_key"            => $room_key,
                    "room_name"           => $room_name,
                    "user_key"            => $user_info["user_key"],
                    "room_status"         => "1",
                    "use_teleconf"        => "0",
                    "use_pgi_dialin"      => "0",
                    "use_pgi_dialin_free" => "0",
                    "use_pgi_dialin_lo_call" => "0",
                    "use_pgi_dialout"     => "0",
                    "room_registtime"     => date("Y-m-d H:i:s"),
                    "max_seat"            => $form_data["max_seat"],
                    "max_audience_seat"   => $form_data["max_audience_seat"],
                    "max_whiteboard_seat" => $form_data["max_whiteboard_seat"],
                    );
                $add_room = $room_db->add($room_data);
                if (DB::isError($add_room)) {
                    $this->logger2->error( $add_room->getUserInfo());
                }
                // リレーション登録
                $acc_user_db = new N2MY_DB($this->account_dsn, "user");
                $where = "user_id='".addslashes($user_data["user_id"])."'";
                $acc_user_data = $acc_user_db->getRow($where);
                $relation_db = new N2MY_DB($this->account_dsn, "relation");
                $relation_data = array(
                    "relation_key"    => $room_key,
                    "relation_type"   => "mfp",
                    "user_key"        => $acc_user_data["user_id"],
                    "status"          => "1",
                    "create_datetime" => date("Y-m-d H:i:s"),
                    );
                $relation_db->add($relation_data);
                //プラン登録
                $this->action_room_plan_add($room_key, $service_key, $form_data["starttime"]);
                //オプション登録
                $session_option_data = $this->session->get("option_data", $this->_name_space);
                $service_option = $session_option_data["service_option"];
                foreach ($service_option as $key => $value) {
                    $this->action_room_option_add(
                        $room_key, $value["service_option_key"], $value["option_start_time"]);
                }
            }
            // 出力データ整形
            $user_data["country_key"] = $this->user_data_format("country_key", $user_data, $user_columns);
            $user_data["user_delete_status"] = $this->user_data_format("user_delete_status", $user_data, $user_columns);
            $user_data["has_chat"] = $this->user_data_format("has_chat", $user_data, $user_columns);
            $user_data["is_cybozu_option"] = $this->user_data_format("is_cybozu_option", $user_data, $user_columns);
            $user_data["use_clip_share"] = $this->user_data_format("use_clip_share", $user_data, $user_columns);
            foreach ($user_columns as $key => $colum) {
                if ("service_name" == $key) {
                    $out_data = $user_data[$key];
                    if ($colum["item"]["relation_data"]) {
                        foreach ($user_data[$key] as $value) {
                            if ($colum["item"]["relation_data"][$value] == $user_data[$key]) {
                                $out_data = sprintf("%s( %s )", $out_data, $colum["item"]["relation_data"][$value]);
                                var_dump($out_data);
                            }
                            break;
                        }
                    }
                    $user_data["service_name"] = $out_data;
                }
                break;
            }
            $room_data["service_key"] = $service_key;
            $import_list[] = array(
                "user_data"   => $user_data,
                "room_data"   => $room_data,
                "option_data" => $service_option
                );
            // ユーザオプション追加
            $session_user_option_data = $this->session->get("user_option_data", $this->_name_space);
            require_once( "classes/dbi/user_service_option.dbi.php" );
            $objUserServiceOption = new UserServiceOptionTable( $this->get_dsn() );
            foreach($session_user_option_data["user_option"] as $user_option){
                $user_option_add_data["user_key"] = $user_info["user_key"];
                $user_option_add_data["service_option_key"] = $user_option["service_option_key"];
                $user_option_add_data["user_service_option_starttime"] = $user_option["option_start_time"];
                $user_option_add_data["user_service_option_registtime"] = date("Y-m-d H:i:s");
                $user_option_add_data["user_service_option_status"] = ( strtotime( $user_option["option_start_time"] ) >= strtotime( $today ) ) ? 2 : 1;
                $result = $objUserServiceOption->add( $user_option_add_data );
            }
        }
        if (isset($user_info)) {
            // グループアカウント課金のルール
            require_once 'classes/dbi/user_group.dbi.php';
            $objUserGroup = new UserGroupTable($dsn);
            $group_exists = $objUserGroup->numRows("group_id = '".addslashes($prefix)."'");
            if ($group_exists == 0) {
                $data = array(
                    'group_id'          => $prefix,
                    'group_name'        => $form_data["name"],
                    'status'            => 1,
                    'create_datetime'   => date('Y-m-d H:i:s'),
                    'update_datetime'   => date('Y-m-d H:i:s'),
                );
                $objUserGroup->add($data);
            }
            // メール送信
            $to = N2MY_ACCOUNT_TO;
            $from = N2MY_ACCOUNT_FROM;
            if ($to && $from) {
                $subject = "アカウント一括登録";
                $template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
                $template->assign("import_list", $import_list);
                $template->assign("plan_list", $plan_list);
                $template_file = "ja/mail/user_add_list.t.txt";
                $body = $template->fetch($template_file);
                $this->sendReport($from, $to, $subject, $body);
            }
        }
        // 登録結果を表示
        $this->template->assign("plan_list", $plan_list);
        $this->template->assign("rows", $import_list);
        $this->template->assign("user_columns", $user_columns);
        $this->template->assign("room_columns", $room_columns);
        $this->clear_session_data();
        $this->display( "admin_tool/auto/done_list.t.html" );
    }

     /**
     * 出力データ整形
     */
    function user_data_format($out_data, $user_data, $user_columns) {
        foreach ($user_columns as $key => $colum) {
            if ($out_data == $key) {
                $out_data = $user_data[$key];
                if ($colum["item"]["select"]) {
                    foreach ($colum["item"]["select"] as $select) {
                        if ($select["value"] == $user_data[$key]){
                            $out_data = sprintf("%s( %s )", $out_data, $select["data"]);
                        }
                    }
                }
                return $out_data;
            }
        }
    }

     /**
     * ルームプラン登録（プランに合わせて人数、オプションも登録）
     */
    function action_room_plan_add($room_key, $service_key, $start_time) {
        $today = date("Y-m-d 23:59:59");
        if (strtotime($start_time) <= strtotime($today)) {
            //プラン登録
            $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
            $room_plan = array (
                "room_key"             => $room_key,
                "service_key"          => $service_key,
                "room_plan_status"     => "1",
                "room_plan_starttime"  => $start_time,
                "room_plan_registtime" => date("Y-m-d H:i:s"),
                );
            $add_plan = $room_plan_db->add($room_plan);
            if (DB::isError($add_plan)) {
                $this->logger2->error( $add_plan->getUserInfo());
            }
        } else {
            //プラン登録
            $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
            $room_plan = array (
                "room_key"             => $room_key,
                "service_key"          => $service_key,
                "room_plan_status"     => "2",
                "room_plan_starttime"  => $start_time,
                "room_plan_registtime" => date("Y-m-d H:i:s"),
                );
            $add_plan = $room_plan_db->add($room_plan);
            if (DB::isError($add_plan)) {
                $this->logger2->info( $add_plan->getUserInfo());
            }
        }
        // プランに併せて部屋の情報更新
        $service_db = new N2MY_DB($this->account_dsn, "service");
        $service_info = $service_db->getRow("service_key = ".$service_key);
        $this->logger2->info($service_key);
        //会議リミット、帯域情報登録
        $room_db = new N2MY_DB($this->get_dsn(), "room");
        $where = "room_key = '".addslashes($room_key)."'";
        $room_data = array (
            "extend_max_seat"       => ($service_info["extend_seat_flg"]) ? $service_info["extend_max_seat"] : 0,
            "extend_max_audience_seat" => ($service_info["extend_seat_flg"]) ? $service_info["extend_max_audience_seat"] : 0,
            "max_room_bandwidth"    => ($service_info["max_room_bandwidth"]) ? $service_info["max_room_bandwidth"] : 2048,
            "max_user_bandwidth"    => ($service_info["max_user_bandwidth"]) ? $service_info["max_user_bandwidth"] : 256,
            "min_user_bandwidth"    => ($service_info["min_user_bandwidth"]) ? $service_info["min_user_bandwidth"] : 30,
            "meeting_limit_time"    => ($service_info["meeting_limit_time"]) ? $service_info["meeting_limit_time"] : 0,
            "ignore_staff_reenter_alert"    => ($service_info["ignore_staff_reenter_alert"]) ? $service_info["ignore_staff_reenter_alert"] : 0,
            "default_camera_size"   => ($service_info["default_camera_size"]) ? $service_info["default_camera_size"] : "",
            "hd_flg"                => ($service_info["hd_flg"]) ? $service_info["hd_flg"] : "0",
        );
        if ($service_info["service_name"] == "Paperless_Free" || $service_info["service_name"] == "Meeting_Free" || $service_info["service_name"] == "PGi_Paperless_Free") {
            $room_data["whiteboard_page"] = "10";
            $room_data["whiteboard_size"] = "2";
            $whiteboard_filetype = array(
                "document" => array("ppt","pdf"),
                "image"    => array("jpg"),
            );
            $room_data["whiteboard_filetype"] =serialize($whiteboard_filetype);
        } else {
            $room_data["whiteboard_page"] = $service_info["whiteboard_page"] ? $service_info["whiteboard_page"] : 100;
            $room_data["whiteboard_size"] = $service_info["whiteboard_size"] ? $service_info["whiteboard_size"] : 20;
            $room_data["cabinet_size"] = $service_info["cabinet_size"] ? $service_info["cabinet_size"] : 20;
            if ($service_info["document_filetype"]) {
                $whiteboard_filetype["document"] = explode(",", $service_info["document_filetype"]);
            }
            if ($service_info["image_filetype"]) {
                $whiteboard_filetype["image"] = explode(",", $service_info["image_filetype"]);
            }
            if ($service_info["cabinet_filetype"]) {
                $cabinet_filetype = explode(",", $service_info["cabinet_filetype"]);
            }
            $room_data["cabinet_filetype"] =serialize($cabinet_filetype);
        }
        if ($service_info["service_name"] == "VCUBE_Doctor_Standard") {
            $room_data["invited_limit_time"] = $service_info["invited_limit_time"] ? $service_info["invited_limit_time"] : 30;
        }
        $add_data = $room_db->update($room_data, $where);

        //自動停止日の設定があったら、expire_deteを更新する
        if ($service_info["service_expire_day"] > 0) {
            $where = "room_key = '".addslashes($room_key)."'";
            $room_info = $room_db->getRow($where, "user_key");
            if ($room_info["user_key"]) {
                $where = "user_key = ".addslashes($room_info["user_key"]);
                $expire_date = strtotime($start_time);
                $expire_date = date( "Y-m-d 00:00:00" , strtotime( "+".$service_info["service_expire_day"]." day" , $expire_date) );
                $data = array("user_expiredatetime" => $expire_date);
                $objUser = new N2MY_DB($this->get_dsn(), "user");
                $updateUser = $objUser->update($data, $where);
                if (DB::isError($updateUser)) {
                    $this->logger2->info( $updateUser->getUserInfo());
                }
            }
        }
        // pgi 設定
        if($service_info["teleconference"] == 1){
            $pgi_form = $this->_getDefaultPGI();
            $pgi_form["pgi_start_date"] =substr($start_time ,0 ,7);
            $this->action_pgi_setting($pgi_form,$room_key);
        }
    }

    //オプション登録
    function action_room_option_add($room_key, $service_option_key, $option_start_time, $video_conference_profileId) {
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
        $this->logger->trace("start_time",__FILE__,__LINE__,$option_start_time);
        if (!$option_start_time) {
            $option_start_time = date("Y-m-d H:i:s");
        }
        $today = date("Y-m-d 23:59:59");
        if (strtotime($option_start_time) >= strtotime($today))  {
            $status = "2";
        } else {
            $status = "1";
        }
        $data = array(
            "room_key"                          => $room_key,
            "service_option_key"                => $service_option_key,
            "ordered_service_option_status"     => $status,
            "ordered_service_option_starttime"  => $option_start_time,
            "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
            "ordered_service_option_deletetime" => "0000-00-00 00:00:00",
            );
        $ordered_service_option->add($data);
        if ($service_option_key == 23) {
            require_once("classes/dbi/pgi_setting.dbi.php");
            $pgi_setting_table = new PGiSettingTable($this->get_dsn());
            $pgi_settings  = $pgi_setting_table->findByRoomKey($room_key);
            if (!$pgi_settings) {
                $pgi_form = $this->_getDefaultPGI();
                $this->action_pgi_setting($pgi_form, $room_key ,1);
            }
            $room_data = array("use_teleconf"        => "0",
                               "use_pgi_dialin"      => "0",
                               "use_pgi_dialin_free" => "0",
                               "use_pgi_dialin_lo_call" => "0",
                               "use_pgi_dialout"     => "0");
            $where_room = "room_key = '".addslashes($room_key)."'";
            $room_db = new N2MY_DB($this->get_dsn(), "room");
            $room_data = $room_db->update($room_data, $where_room);
        }
        if ($service_option_key != 7) {
            if ($service_option_key == 23) {
                $room_db       = new N2MY_DB($this->get_dsn(), "room");
                $teleconf_data = array(
                				'use_teleconf'          => 0, 
                				'use_pgi_dialin'        => 0, 
                				'use_pgi_dialin_free'   => 0, 
                				'use_pgi_dialin_lo_call'=> 0, 
                				'use_pgi_dialout'       => 0);
                $room_db->update($teleconf_data , "room_key = '".addslashes($room_key)."'");
            }
        }
        //テレビ会議連携の場合は人数を9に変更
        if ($service_option_key == 24) {
            $this->action_create_conference($room_key, $video_conference_profileId);
            $seat_data = array("max_seat" => 9);
            $where_room = "room_key = '".addslashes($room_key)."'";
            $room_db = new N2MY_DB($this->get_dsn(), "room");
            $room_data = $room_db->update($seat_data, $where_room);
        }
    }

    /**
     * 乱数ID発行
     */
    function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this->dec2any($c);
    }

    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=57, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr(
                    '23456789ab' .
                    'cdefghijkm' .
                    'nopqrstuvw' .
                    'xyzABCDEFG' .
                    'HJKLMNPQRS' .
                    'TUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }

    function _getMeetingVersion() {
        static $version;
        if ($version) {
            return $version;
        }
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }

    private function action_create_conference($roomKey, $video_conference_profileId)
    {
        try {
            $dsn = $this->get_dsn();
            $ivesSettingTable = new IvesSettingTable($dsn);

            $ives = $ivesSettingTable->findLatestByRoomKey($roomKey);

            if (PEAR::isError($ives)) {
                throw new Exception("select ives_setting room_key failed PEAR said : ".$roomKey);
            }

            require_once("classes/polycom/Polycom.class.php");
            require_once("classes/dbi/video_conference.dbi.php");
            $polycom	= new PolycomClass($this->get_dsn());
            $room_db	= new RoomTable($this->get_dsn());

            $where   	= "room_key = '".addslashes($roomKey)."'";
            $room		= $room_db->getRow($where);

//            $ives = $res[0];
            // 新規追加
            if (!$ives || $ives["is_deleted"]) {
                //SIPアカウント作成
                $ignore_sip_flg = $this->configProxy->get("ignore_create_sip_account");
                if (!$ignore_sip_flg) {
                    for( $count = 0; $count < 3; $count++ ) {
                        $sip_uid = $this->action_create_sip_account($room, $count, $video_conference_profileId);
                        if ($sip_uid) {
                            $this->logger2->debug($sip_uid);
                            $sip_info = $this->action_get_sip_info($sip_uid);
                            if ($sip_info) {
                                break;
                            }
                        }
                    }
                    if (!$sip_uid || !$sip_info) {
                        $this->logger->warn(__FUNCTION__."#sip fault",__FILE__,__LINE__,array($sip_uid, $sip_info));
                        $this->action_room_detail('', 'SIP アカウント の作成に失敗しました');
                        exit;
                    }
                    $this->logger2->info(array($sip_info->name, $sip_info->secret, $sip_info));
                    $sip_info = array(
                        "uid" => $sip_info->uid,
                        "name" => $sip_info->name,
                        "secret" => $sip_info->secret,
                    );
                } else {
                    $sip_info = array(
                        "uid" => rand(10000, 99999),
                        "name" => rand(1000000, 9999999),
                        "secret" => $this->create_id().rand(2,9),
                    );
                }

                $did = $polycom->createDid();
                $data = array(
                    'room_key'           => $roomKey,
                    'user_key'           => $room["user_key"],
                    'ives_did'           => $did,
                    'sip_uid'            => $sip_info["uid"],
                    'client_id'          => $sip_info["name"],
                    'client_pw'          => $sip_info["secret"],
                    'startdate'          => date("Y-m-d H:i:s"),
                    'registtime'         => date("Y-m-d H:i:s")
                );
                $res = $ivesSettingTable->add($data);
                $this->creteVideoOption($room, $did);
            }
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
            $this->action_room_detail('', 'MCU Conference の作成に失敗しました');
            exit;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            $this->action_room_detail('', 'MCU Conference の作成に失敗しました');
            exit;
        }
    }


    private function creteVideoOption($room, $did) {
        require_once("classes/polycom/Polycom.class.php");
        require_once("classes/dbi/video_conference.dbi.php");
        $polycom	= new PolycomClass($this->get_dsn());
        $ivesClient = new N2MY_IvesClient($this->get_dsn());
        $video		= new VideoConferenceTable($this->get_dsn());
        $where		= "room_key = '".addslashes($room["room_key"])."'".
          " AND is_active = 1";
        $videoInfo	= $video->getRow($where);
        // remove conference
        if ($videoInfo) {
      $conf = $ivesClient->callGetConference($videoInfo["conference_id"]);
      if ($conf->return) {
        $polycom->removeConference($videoInfo["conference_id"]);
      }
        }
        else {
      $conference = $polycom->checkExistDidInConferences($did);
      if ($conference) {
        $polycom->removeConference($conference->id);
      }
        }
        // create conference
        $confId = $polycom->createConference($did);
        //
        $video->removeAllByRoomKey($room["room_key"]);
    $data = array(
            "user_key"		=> $room["user_key"],
            "room_key"		=> $room["room_key"],
      "conference_id"	=> $confId,
      "did"			=> $did,
      "is_active"		=> 1,
      "createtime"	=> date("Y-m-d H:i:s"));
    $video->add($data);
    }

    function action_create_sip_account($room_info, $count = 0, $video_conference_profileId = 281) {
        try {
            $dsn = $this->get_dsn();

            require_once("classes/N2MY_IvesSipClient.class.php");
            $ivesClient = new N2MY_IvesSipClient($dsn);

            if ($count > 0) {
                $room_key = $room_info["room_key"]."_".$count;
            } else {
                $room_key = $room_info["room_key"];
            }
            $login_id = str_replace("-", "_", $room_key);

            $subscriber = array();
            $subscriber["callerid"] = "vcube"; //vcube
            $subscriber["title"] = "vcube"; //vcube
            $subscriber["gender"] = "M";
            $subscriber["lastName"] = "vcube"; //vcube
            $subscriber["firstName"] = "vcube"; //vcube
            $subscriber["dateOfBirth"] = "1998-10-09"; //vcube
            $subscriber["fax"] = "0355019676"; //vcube
            $subscriber["homePhone"] = "0357683111"; //vcube
            $subscriber["mobile"] = "0357683111"; //vcube

            //-が利用できないので_に置き換え
            $subscriber["login"] = $login_id; //room_key
            $subscriber["mail"] = "vsupport@vcube.co.jp"; //vsupport?
            $subscriber["userPassword"] = $this->create_id().rand(2,9);//ランダム
            $subscriber["address_1"] = "Nakameguro GT Tower 20F"; //vcube
            $subscriber["address_2"] = "2-1-1 Kamimeguro, Meguro-ku"; //vcube
            $subscriber["zipCode"] = "1530051"; //vcube
            $subscriber["city"] = "Tokyo";
            $subscriber["country"] = "Japan";
            $subscriber["preferedLanguage"] = "English";
            $subscriber["acceptMailHtml"] = false;
            $subscriber["acceptMailInformation"] = false;
            $subscriber["acceptProfilePublic"] = false;

            $account = array();
            $account["state"] = "Actif";
            $account["creationDate"] = date("Y-m-d");
            $account["activityEndDate"] = "0000-00-00";
            $account["inactivityEndDate"] = "0000-00-00";
            $account["paymentDate"] = "0000-00-00";

            $createSubscriber = array();
            $createSubscriber["subscriber"] = $subscriber;
            $createSubscriber["account"] = $account;
            //以下のデータは固定
            $createSubscriber["profileId"] = $video_conference_profileId;
            $createSubscriber["roleId"] = 1;
            $createSubscriber["optionId"] = 127;
            $createSubscriber["customerId"] = 0;

            $result = $ivesClient->createSubscriber($createSubscriber);
            $this->logger2->info(array($result, $createSubscriber));
            return $result->uid;
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
            return false;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            return false;
        }
    }

    function action_get_sip_info($uid) {
        $dsn = $this->get_dsn();
        $this->logger2->info($uid);
        require_once("classes/N2MY_IvesSipClient.class.php");
        $ivesSipClient = new N2MY_IvesSipClient($dsn);
        try {
            $result = $ivesSipClient->getSipInformation($uid);
            $this->logger2->debug($result);
            return $result;
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__,$result);
            return false;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__,$result);
            return false;
        }
    }

    function action_delete_sip_account($uid) {
        $dsn = $this->get_dsn();
        require_once("classes/N2MY_IvesSipClient.class.php");
        $ivesSipClient = new N2MY_IvesSipClient($dsn);
        try {
            $result = $ivesSipClient->deleteSubscriber($uid);
            return true;
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__,array($uid,$result));
            return false;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__,array($uid,$result));
            return false;
        }
    }
    function action_pgi_setting($form_data , $room_key) {

        require_once("classes/dbi/pgi_setting.dbi.php");
        require_once("classes/dbi/pgi_rate.dbi.php");
        $pgiSettingTable = new PGiSettingTable($this->get_dsn());
        $pgiRateTable    = new PGiRateTable($this->get_dsn());

        if (!$room_key) {
            $this->logger2->error("_pgi_setting NO room_key");
            return ;
        }

       if (!$form_data) {
           $this->logger2->error("_pgi_setting NO PGI_RATE DATA");
            return ;
        }

        try {
            $pgiSettings = $pgiSettingTable->findByRoomKey($room_key);
        } catch (Exception $e) {
            die('error : '.__FILE__.__LINE__);
        }
        $validated = $this->validatePGiSetting($form_data, $room_key);
        if ($validated['message']) {
            $this->logger2->error($validated);
            return;
        }

        $validated['room_key'] = $room_key;

        $this->request->set("room_key", $room_key);


        $pgi_setting_data = array('room_key'   => $room_key,
                'startdate'  => $validated['data']['startdate'],
                'system_key' => $validated['data']['system_key'],
                'company_id' => $validated['data']['company_id'],);
        try {
            $pgi_setting_key = $pgiSettingTable->add($pgi_setting_data);
        } catch (Exception $e) {
            $this->exitWithError($e->getMessage());
        }

        require_once('classes/pgi/PGiSystem.class.php');
        $pgi_system = PGiSystem::findBySystemKey($pgi_setting_data['system_key']);

        if ($pgi_system->rates) { // 設定にデフォルト料金が登録されているsystemはpgi_rateに料金を保存
            require_once("classes/pgi/PGiRate.class.php");
            $this->logger2->debug($validated['data']);
            $rate_config = PGiRate::findGmConfigAll();
            foreach ($rate_config as $pgi_rate) {
                $pgi_rate_data = array('pgi_setting_key' => $pgi_setting_key,
                        'rate_type'       => (string)$pgi_rate->type,
                        'rate'            => $validated['data'][(string)$pgi_rate->type]);
                try {
                    $pgiRateTable->add($pgi_rate_data);
                } catch (Exception $e) {
                    $this->exitWithError($e->getMessage());
                }
            }
        }
        //操作ログ登録
        $operation_data = array ("staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => "pgi_setting, pgi_rate",
                "keyword"            => $room_key,
                "info"               => serialize($pgi_setting_data['system_key']),
                "operation_datetime" => date("Y-m-d H:i:s"),
        );

        $this->add_operation_log($operation_data);
    }

    private function exitWithError($msg)
    {
        $b = debug_backtrace();

        $from = sprintf("called %s+%s : %s",$b[0]['file'], $b[0]['line'], $b[1]['function']);
        $this->logger->warn($msg.$from);

        exit($msg.$from);
    }

    private function validatePGiSetting($form, $room_key)
    {
        $res = array('data'    => $form,
                'message' => '');

        if (!$form["pgi_start_date"]) {
            $res['message'] .= "Please input the start time.<br />";
        } else {
            list($y, $m) = explode('/',$form["pgi_start_date"]);
            if (!checkdate($m, 1, $y)) {
                $res['message'] .= "Start Time is not correct.<br />";
            } else {
                $startdate = str_replace('/','-',$form["pgi_start_date"])."-01";
                // 過去分の設定はできません
                if ($startdate < date('Y-m').'-01') {
                    $res['message'].= "Can not be a past time.<br />";
                }
            }
        }
        $res['data']['startdate']  = $startdate;

        require_once('classes/pgi/PGiSystem.class.php');
        $pgiSystems    = PGiSystem::findConfigAll();
        $pgiSystemKeys = array();
        $selectedSystem         = null;
        foreach ($pgiSystems as $system) {
            if (@$form["system_key"] == (string)$system->key) {
                $selectedSystem = $system;
            }
        }
        if (!$form["system_key"]) {
            $res['message'] .= "Please input the system_key.<br />";
        } elseif (!$selectedSystem) {
            $res['message'] .= "The system_key is not correct.<br />";
        }

        if ($form["company_id_type"] == 'default') {
            $form["company_id"]        = $selectedSystem->defaultCompanyID;
            $res['data']['company_id'] = $selectedSystem->defaultCompanyID;
        }

        if (!$form["company_id"]) {
            $res['message'] .= "Please input the company_id.<br />";
        } else {
            if ($selectedSystem && $selectedSystem->rates) {
                require_once("classes/pgi/PGiRate.class.php");
                // validate rate's values
                $rate_config = PGiRate::findGmConfigAll();

                foreach ($rate_config as $rate){
                    $col  = (string)$rate->type;
                    $name = (string)$rate->description;
                    if (!$form[$col]) {
                        $res['message'] .= "Please input the ".$name.".<br />";
                    } elseif (is_int($form[$col])) {
                        $res['message'] .= $name." should be a number.<br />";
                    } elseif ($form[$col] <= 0) {
                        $res['message'] .= $name."should be larger than 0.<br />";
                    }
                }
            }
        }

        require_once("classes/dbi/pgi_setting.dbi.php");
        $pgi_setting_table = new PGiSettingTable($this->get_dsn());
        try {
            $pgi_settings  = $pgi_setting_table->findByRoomKey($room_key);
        } catch(Exception $e) {
            die('error'.$e->getMessage());
        }
        if (0 == count($pgi_settings)) {
            return $res;
        }

        foreach ($pgi_settings as $setting) {
            if ($startdate == $setting['startdate']) {
                $res['message'] .= $startdate."have already been set.(auto generate when meeting start）<br />";
                return $res;
            }
        }

        return $res;
    }
    /*
     * 操作ログ登録
    */
    function add_operation_log($operation_data) {
        $this->logger->debug("operation_log",__FILE__,__LINE__,$operation_data);
        $operation_db = new N2MY_DB($this->account_dsn, "operation_log");
        $operation_data["create_datetime"] = date("Y-m-d H:i:s");
        $add_data = $operation_db->add($operation_data);
        if (DB::isError($add_data)) {
            $this->logger2->error($add_data->getUserInfo());
            return false;
        }
        return true;
    }

    private function _getDefaultPGI(){
        $pgi_form["pgi_start_date"] =date("Y/m");
        $pgi_form["system_key"] = "VCUBE HYBRID PRODUCTION";
        $pgi_form["company_id_type"] = "default";
        $pgi_form["company_id"] = "282363";
        $pgi_form["dialin_local"] ="";
        $pgi_form["dialin_free"] ="";
        $pgi_form["dialin_mobile"] ="";
        $pgi_form["dialin_area1"] ="";
        $pgi_form["dialin_area2"] ="";
        $pgi_form["dialin_area3"] ="";
        $pgi_form["dialin_area4"] ="";
        $pgi_form["dialin_area5"] ="";
        $pgi_form["dialout"] ="";
        $pgi_form["dialout_mobile"]="";
        $pgi_form["gm_dialin_local_tokyo"] = "15";
        $pgi_form["gm_dialin_local_other"] = "15";
        $pgi_form["gm_dialin_local_area1"] = "15";
        $pgi_form["gm_dialin_local_area2"] = "20";
        $pgi_form["gm_dialin_local_area3"] = "25";
        $pgi_form["gm_dialin_local_area4"] = "30";
        $pgi_form["gm_dialin_navidial"] = "25";
        $pgi_form["gm_dialin_free"] = "25";
        $pgi_form["gm_dialin_free_mobile"] = "40";
        $pgi_form["gm_dialin_free_area1"] = "20";
        $pgi_form["gm_dialin_free_area2"] = "20";
        $pgi_form["gm_dialin_free_area3"] = "30";
        $pgi_form["gm_dialin_free_area4"] = "60";
        $pgi_form["gm_dialout"] = "20";
        $pgi_form["gm_dialout_mobile"] = "50";
        $pgi_form["gm_dialout_area1"] = "20";
        $pgi_form["gm_dialout_area2"] = "25";
        $pgi_form["gm_dialout_area3"] = "40";
        $pgi_form["gm_dialout_area4"] = "70";
        $pgi_form["gm_dialout_area5"] = "1000";

        return $pgi_form;
    }
}
$main = new AppUserCreate();
$main->execute();
