<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("Auth.php");
require_once("Pager.php");

class AppFree extends AppFrame {

    var $_auth = null;
    var $_sess_page;
    var $account_dsn = null;

    function init() {
        $_COOKIE["lang"]   = "ja";
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->_sess_page  = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->_name_space = md5(__FILE__);
    }

    function set_dsn($dsn) {
        $_SESSION[$this->_sess_page]["dsn"] = $dsn;
    }

    function get_dsn() {
        if (isset($_SESSION[$this->_sess_page]["dsn"])) {
            return $_SESSION[$this->_sess_page]["dsn"];
        }
    }

    /**
     * 一覧取得
     */
    function action_list() {
        $reset = $this->request->get("reset");
        if ($reset == 1) {
            $this->session->remove("user_data");
            $this->session->remove("form_data");
        }
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $dsn = $this->get_dsn();

        $type = "data";
        if (!$type) {
            $type = $this->session->get("type");
        }
        if ($type == "account") {
            $dsn = $this->account_dsn;
        } else {
            $dsn = $this->get_dsn();
        }
        $table = "user";
        $this->session->set("type", $type);
        $this->session->set("table", $table);
        $sort_key = $this->request->get("sort_key");
        $sort_type = $this->request->get("sort_type");
        if (!$sort_type) {
            $sort_type = "asc";
        }
        $data_db = new N2MY_DB($dsn, $table);
        $this->logger->trace("dsn",__FILE__,__LINE__,$dsn);
        $action_name = __FUNCTION__;
        $columns = $data_db->getTableInfo($type);
        $columns = $this->_get_relation_data($dsn, $columns);
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $sort_key, $sort_type);
        $this->logger->debug("form_data",__FILE__,__LINE__,$form_data);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
            $this->template->assign("request",$request);
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where = $data_db->getWhere($columns, $request);
        if ($where) {
            $where .= " AND user_status = 3";
        } else {
            $where = "user_status = 3";
        }
        // データ取得
        $rows = $data_db->getRowsAssoc($where, array($form_data['sort_key'] => $form_data['sort_type']), $limit, $offset);
        $total_count = $data_db->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name,
            "after_action" => $this->request->get("after_action"))
            );
        $add_form = $this->session->get("temp", $action_name);
        $this->template->assign("add_form", array(
            "data" => $add_form,
            "ok_action" => $action_name,
            "ng_action" => $action_name
          ));
        $this->template->assign("columns",$columns);
        $this->template->assign("rows",$rows);
        $this->template->assign("table",$table);
        $this->session->remove("message");
        $template = "admin_tool/free/index.t.html";
        $this->_display($template);
    }

    /**
     * 一覧取得
     */
    function action_member_list() {
        $reset = $this->request->get("reset");
        if ($reset == 1) {
            $this->session->remove("user_data");
            $this->session->remove("form_data");
        }
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $dsn = $this->get_dsn();

        $type = "data";
        if (!$type) {
            $type = $this->session->get("type");
        }
        if ($type == "account") {
            $dsn = $this->account_dsn;
        } else {
            $dsn = $this->get_dsn();
        }
        $table = "member";
        $this->session->set("type", $type);
        $this->session->set("table", $table);
        $sort_key = $this->request->get("sort_key");
        $sort_type = $this->request->get("sort_type");
        if (!$sort_type) {
            $sort_type = "asc";
        }
        $data_db = new N2MY_DB($dsn, $table);
        $this->logger->trace("dsn",__FILE__,__LINE__,$dsn);
        $action_name = __FUNCTION__;
        $columns = $data_db->getTableInfo($type);
        $columns = $this->_get_relation_data($dsn, $columns);
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $sort_key, $sort_type);
        $this->logger->debug("form_data",__FILE__,__LINE__,$form_data);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
            $this->template->assign("request",$request);
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where = $data_db->getWhere($columns, $request);
        // データ取得
        $rows = $data_db->getRowsAssoc($where, array($form_data['sort_key'] => $form_data['sort_type']), $limit, $offset);
        $total_count = $data_db->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name,
            "after_action" => $this->request->get("after_action"))
            );
        $add_form = $this->session->get("temp", $action_name);
        $this->template->assign("add_form", array(
            "data" => $add_form,
            "ok_action" => $action_name,
            "ng_action" => $action_name
          ));
        $this->template->assign("columns",$columns);
        $this->template->assign("rows",$rows);
        $this->template->assign("table",$table);
        $this->session->remove("message");
        $template = "admin_tool/free/member_list.t.html";
        $this->_display($template);
    }

    function action_add_user() {
        $this->set_submit_key($this->_name_space);
        $message = $this->session->get("message");
        $this->template->assign("message",$message);
        $template = "admin_tool/free/add_user.t.html";
        $this->_display($template);
    }

    function action_add_user_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            header("Location: paperless.php?action_list");
            exit;
        }
        $this->session->remove("message");
        $this->session->remove("required");
        $type = "data";
        $dsn = $this->get_dsn();
        $table = "user";
        $data_db = new N2MY_DB($dsn, $table);
        $form_data = $this->request->get("add_form");
        $regist = $this->session->get("regist");
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $user_password = $this->create_id().rand(2,9);
        $user_admin_password = $this->create_id().rand(2,9);
        $form_data["user_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_password);
        $form_data["user_admin_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_admin_password);
        $message = $this->_user_validator($form_data, "add");
        if ($message) {
            $required["user"] = "1";
            $this->session->set("message",$message);
            $this->session->set("required",$required);
            header("Location: paperless.php?action_add_user#fragment1");
            exit;
        }

        $acount_user_db = new N2MY_DB($this->account_dsn, "user");
        $where = "user_id = '".mysql_real_escape_string($form_data["user_id"])."'";
        $acount_count = $acount_user_db->numRows($where);
        $count = $data_db->numRows($where);
        if ((is_numeric($acount_count) && $acount_count > 0) || (is_numeric($count) && $count > 0)) {
            $message["check"]["user_id"] = "This ID has been added.";
            $required["check"]["user_id"] = "1";
            $this->session->set("message",$message);
            $this->session->set("required",$required);
            header("Location: paperless.php?action_list#fragment2");
            exit;
        } else {
            if (!$form_data["user_starttime"]) {
                $form_data["user_starttime"] = date("Y-m-d H:i:s");
            }
            $form_data["user_registtime"] = date("Y-m-d H:i:s");
            $form_data["meeting_version"] = $this->_getMeetingVersion();
            $agency = $form_data["agency"];
            unset($form_data["agency"]);
            $form_data["country_key"] = "1";
            $form_data["account_model"] = "free";
            $form_data["user_status"] = 3;
            $form_data["is_minutes_delete"] = 1;
            $form_data["service_name"] = "paperless";
            $data_db->add($form_data);
            $user_data = array(
                "user_id" => $form_data["user_id"],
                "server_key" => $this->session->get("now_server_key"),
                "user_id" => $form_data["user_id"],
                "user_registtime" => date("Y-m-d H:i:s"),
                );
                $this->logger2->info($user_data);
            $acount_user_db->add($user_data);
            $user_info = $data_db->getRow("user_id = '".mysql_real_escape_string($form_data["user_id"])."'");

            $user_plan_db = new N2MY_DB( $this->get_dsn(), "user_plan" );
            $plan_data = array("user_key" => $user_info["user_key"],
                               "user_plan_registtime" => date( "Y-m-d H:i:s" ),
                               "user_plan_status" => 1,);
            $result = $user_plan_db->add( $plan_data );
            if( DB :: isError( $result ) ){
                $this->logger2->error( $result );
            }

            $after_action = $after_action."&user_key=".$user_info["user_key"];
            $this->logger->info("user_key",__FILE__,__LINE__,$user_info);
            $where_agency = "user_id = '".htmlspecialchars($form_data["user_id"])."'";

            $agency_rel_db = new N2MY_DB($dsn, "agency_relation_user");
            if ($agency) {
                $data = array("user_key" => $user_info["user_key"],
                              "agency_id" => $agency);
                $ret = $agency_rel_db->add($data);
                if (DB::isError($ret)) {
                    $this->logger2->error($ret->getUserInfo());
                    return false;
                }
            }
            $this->logger->trace("form_data",__FILE__,__LINE__,$form_data);
            //操作ログ登録
            $operation_data = array (
                "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => $table,
                "keyword"            => "add data",
                "info"               => serialize($form_data),
                "operation_datetime" => date("Y-m-d H:i:s"),
            );
            $this->add_operation_log($operation_data);

            $this->session->remove("valid_required");
            $this->session->remove("check");
            $this->session->remove("regist");
            $this->template->assign("user_company_name", $form_data["user_company_name"]);
            $this->template->assign("user_password", $user_admin_password);
            $this->template->assign("user_admin_password", $user_admin_password);
            $template = "admin_tool/free/add_user_done.t.html";
            $this->_display($template);
        }
    }
     /**
     * メンバー追加画面
     */
    function action_add_member($message = "") {
        $this->set_submit_key($this->_name_space);
        $user_key = $this->request->get("user_key");
        if (!$user_key) {
            $user_key = $this->session->get("user_key");
        }
        if ($message) {
            $this->template->assign("add_member_form", $this->session->get("add_member_form"));
            $this->template->assign("message",$message);
            $user_key = $this->session->get("user_key");
        }
        $this->session->set("user_key",$user_key);
        $sort_key = $this->request->get("sort_key");
        if (!$sort_key) {
            $sort_key = "member_key";
        }
        $sort_type = $this->request->get("sort_type");
        if(!$sort_type) {
            $sort_type = "asc";
        }
        //ユーザー情報
        $objUser = new N2MY_DB( $this->get_dsn(), "user" );
        $user_info = $objUser->getRow("user_key='".$user_key."'");

        //代理店取得
        $agency_db = new N2MY_DB($this->account_dsn, "agency");
        $agency_list = $agency_db->getRowsAssoc( sprintf( "is_deleted='%s'", "0" ) );

        //タイムゾーン
        $timezoneList = $this->get_timezone_list();

        $data_db = new N2MY_DB($this->get_dsn(), "member");
        $action_name = __FUNCTION__;
        $columns = $data_db->getTableInfo("data");
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $sort_key, $sort_type);
        $this->logger->trace("form_data",__FILE__,__LINE__,$form_data);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where = "user_key='".$user_key."'";
        $this->logger->trace("columns",__FILE__,__LINE__,$columns);
        // データ取得
        $rows = $data_db->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $total_count = $data_db->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name)
            );
        $add_form = $this->session->get("temp", $action_name);
        $this->template->assign("add_form", array(
            "data" => $add_form,
            "ok_action" => $action_name,
            "ng_action" => $action_name
          ));
        $this->template->assign("columns",$columns);
        $this->logger->trace("data",__FILE__,__LINE__,$rows);
        $this->template->assign("rows",$rows);
        $this->session->remove("message");

        $plan_db = new N2MY_DB($this->account_dsn, "service");
        $where = "service_status='1'";
        $service_list = $plan_db->getRowsAssoc($where);
        $this->logger->debug("service_list",__FILE__,__LINE__,$service_list);
        $this->template->assign("user_key",$user_key);
        $this->template->assign("user_info",$user_info);
        $this->template->assign("agency_list",$agency_list);
        $template = "admin_tool/free/add_member.t.html";
        $this->_display($template);
    }

     /**
     * メンバー追加
     */
    function action_add_member_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_add_member();
            exit;
        }
        $form_data = $this->request->get("add_form");
        $form_data['member_pass'] = $this->create_id().rand(2,9);
        $this->session->set("add_member_form", $form_data);
        //ユーザー情報
        $user_db = new N2MY_DB( $this->get_dsn(), "user" );
        $user_info = $user_db->getRow("user_key='".$form_data["user_key"]."'");

        $member_db = new N2MY_DB($this->get_dsn(), "member");
        $member_list = $member_db->getRowsAssoc( sprintf( "user_key ='%s' and member_status > -1", $form_data['user_key'] ) );

        $message = null;
        if(!$form_data['member_name']){
            $message .= '<li>'.MEMBER_ERROR_NAME . '</li>';
        } elseif (mb_strlen($form_data['member_name']) > 50) {
             $message .= '<li>'.MEMBER_ERROR_NAME_LENGTH . '</li>';
        }
        if (mb_strlen($form_data['member_name_kana']) > 50) {
             $message .= '<li>'.MEMBER_ERROR_NAME_KANA_LENGTH . '</li>';
        }
        $this->logger2->info($form_data['member_id']);
        if (!preg_match('/^[[:alnum:]@+._-]{3,255}$/', $form_data['member_id'])) {
            $message .= '<li>'.USER_ERROR_ID_LENGTH . '</li>';
        } elseif($this->_isExistMember($form_data['member_id'])){
            $message .= '<li>'.MEMBER_ERROR_ID_EXIST . '</li>';
        }
        // メンバー課金の場合のみ必須
        if($user_info["account_model"] == "member" && !$form_data['member_email']){
            $message .= '<li>'.MEMBER_ERROR_EMAIL . '</li>';
        }
        if ($form_data['member_email'] != "") {
            if(!EZValidator::valid_email($form_data['member_email'])){
                $message .= '<li>'.MEMBER_ERROR_EMAIL_INVALID . '</li>';
            }
        }
        if (!$form_data['member_pass'] && !$form_data['member_pass_check']) {
            $message .= '<li>'.MEMBER_ERROR_PASS . '</li>';
        //ユーザーパスワードをチェック
        } elseif (!preg_match('/^[!-\[\]-~@._=`]{8,128}$/', $form_data['member_pass'])) {
            $message .= '<li>'.USER_ERROR_PASS_INVALID_01. '</li>';
        } elseif (!preg_match('/[[:alpha:]]+/',$form_data['member_pass']) || preg_match('/^[[:alpha:]]+$/',$form_data['member_pass'])) {
            $message .= '<li>'.USER_ERROR_PASS_INVALID_02. '</li>';
        }
        if( $message ){
            $this->action_add_member($message);
            exit;
        }
        if (!$form_data["room_starttime"]) {
            $form_data["room_starttime"] = date("Y-m-d 00:00:00");
        }
        $form_data["member_type"] = "free";
        $room_db = new N2MY_DB($this->get_dsn(), "room");

        //ミーティング用とペーパレス用の部屋を作成
        $rooms[] = $this->_add_room("Meeting_Free", $user_info, $form_data);
        $rooms[] = $this->_add_room("Paperless_Free", $user_info, $form_data);

        //member追加
        $user_key = $user_info['user_key'];
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $temp = array(
            "user_key"         => $user_info['user_key'],
            "member_id"        => $form_data['member_id'],
            "member_pass"      => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $form_data['member_pass']),
            "member_email"     => $form_data['member_email'],
            "member_name"      => $form_data['member_name'],
            "member_name_kana" => $form_data['member_name_kana'],
            "member_status"    => (($form_data['member_status_key']) ? $form_data['member_status_key'] : 0),
            "member_type"      => (($form_data['member_type']) ? $form_data['member_type'] : ""),
            "member_group"     => (($form_data['member_group_key']) ? $form_data['member_group_key'] : 0),
            "timezone"         => $form_data['timezone'],
            "lang"             => $form_data['lang'],
            "create_datetime"  => date("Y-m-d H:i:s"),
            "update_datetime"  => date("Y-m-d H:i:s"),
        );
        $ret = $member_db->add( $temp );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$temp);
        }
        $member_info = $member_db->getRow("member_id = '".addslashes($form_data['member_id'])."'");
        $agency = $form_data["agency"];
        $agency_rel_db = new N2MY_DB($this->get_dsn(), "agency_relation_member");
        if ($agency) {
            $data = array("member_key" => $member_info["member_key"],
                          "agency_id" => $agency);
            $ret = $agency_rel_db->add($data);
            if (DB::isError($ret)) {
                $this->logger2->error($ret->getUserInfo());
                return false;
            }
        }

        //認証サーバーへ追加
        $obj_MgmUserTable = new N2MY_DB( $this->account_dsn, "user" );
        $auth_data = array(
            "user_id"     => $form_data['member_id'],
            "server_key"    => $this->session->get("now_server_key")
            );
        $result = $obj_MgmUserTable->add( $auth_data );
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$auth_data);
        }

        //部屋のリレーション追加
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        foreach ($rooms as $_key => $room) {
            if ($room) {
                $data = array(
                        "member_key"      => $member_info["member_key"],
                        "room_key"        => $room,
                        "create_datetime" => date("Y-m-d H:i:s")
                    );
                $ret = $objRoomRelation->add($data);
                if (DB::isError($ret)) {
                    $this->logger2->info($data, "set relation error");
                }
                //relation tableへ追加
                require_once( "classes/mgm/MGM_Auth.class.php" );
                $obj_MGMAuthClass = new MGM_AuthClass( $this->account_dsn );
                $obj_MGMAuthClass->addRelationData( $user_info["user_id"], $room, "mfp" );
            }
        }

        $this->logger->info(__FUNCTION__."#member_add",__FILE__,__LINE__,$temp);
        $this->session->remove('add_member_info');
        //操作ログ登録
        $operation_data = array (
                "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => "member",
                "keyword"            => $form_data["member_id"],
                "info"               => serialize($form_data),
                "operation_datetime" => date("Y-m-d H:i:s"),
            );
        $this->add_operation_log($operation_data);
        $this->template->assign("user_key", $user_info['user_key']);
        $this->template->assign("member_name", $form_data["member_name"]);
        $this->template->assign("member_pass", $form_data['member_pass']);
        $template = "admin_tool/free/add_member_done.t.html";
        $this->_display($template);
    }

    private function _add_room($service_name, $user_info, $form_data) {
        require_once( "classes/dbi/room_plan.dbi.php" );
        $objRoomPlanTable = new RoomPlanTable( $this->get_dsn() );
        $room_db = new N2MY_DB($this->get_dsn(), "room");
        $service_db = new N2MY_DB($this->account_dsn, "service");
        $service_info = $service_db->getRow("service_name = '".$service_name."'");
        $where = sprintf( "user_key=%d", $user_info["user_key"] );
        $count = $room_db->numRows($where);
        $room_key = $user_info["user_id"]."-".($count + 1)."-".substr( md5( uniqid( time(), true ) ), 0, 4 );
        $room_sort = $count + 1;
        //部屋登録
        $this->logger2->info($service_info);
        $room_data = array (
            "room_key"              => $room_key,
            "max_seat"              => ($service_info["max_seat"]) ? $service_info["max_seat"] : 0,
            "max_audience_seat"     => ($service_info["max_audience_seat"]) ? $service_info["max_audience_seat"] : 0,
            "max_whiteboard_seat"   => ($service_info["max_whiteboard_seat"]) ? $service_info["max_whiteboard_seat"] : 3,
            "meeting_limit_time"    => ($service_info["meeting_limit_time"]) ? $service_info["meeting_limit_time"] : 45,
            "default_camera_size"   => ($service_info["default_camera_size"]) ? $service_info["default_camera_size"] : "",
            "user_key"              => $user_info["user_key"],
            "room_name"             => $form_data['member_name'],
            "room_sort"             => $room_sort,
            "room_status"           => "1",
            "room_registtime"       => date("Y-m-d H:i:s"),
            );
        if ($form_data["service_name"] == "Paperless_Free") {
            $room_data["whiteboard_page"] = "10";
            $room_data["whiteboard_size"] = "2";
            $whiteboard_filetype = array(
                "document" => array("ppt","pdf"),
                "image"    => array("jpg"),
            );
            $room_data["whiteboard_filetype"] =serialize($whiteboard_filetype);
        }
        $this->logger2->info($room_data);
        $add_room = $room_db->add($room_data);
        if (DB::isError($add_room)) {
            $this->logger2->error( $add_room->getUserInfo());
            return false;
            exit;
        }
        $relation_db = new N2MY_DB($this->account_dsn, "relation");
        $relation_data = array(
            "relation_key" => $room_key,
            "relation_type" => "mfp",
            "user_key" => $user_info["user_id"],
            "status" => "1",
            "create_datetime" => date("Y-m-d H:i:s"),
            );
        $relation_db->add($relation_data);
        if (DB::isError( $service_info ) ) {
            $this->logger2->info( $addPlan->getUserInfo() );
            return false;
            exit;
        } else {
            $data = array(
                        "room_key"=>$room_key,
                        "service_key"=> $service_info["service_key"],
                        "discount_rate"=>"0",
                        "contract_month_number"=>"-",
                        "room_plan_yearly"=> "0",
                        "room_plan_yearly_starttime"=>$form_data["room_starttime"],
                        "room_plan_starttime"=>$form_data["room_starttime"],
                        "room_plan_endtime"=> "0000-00-00 00:00:00",
                        "room_plan_status"=>"1",
                        "room_plan_registtime"=>$form_data["room_starttime"],
                        "room_plan_updatetime"=>date("Y-m-d H:i:s")
                         );
            $addPlan = $objRoomPlanTable->add( $data );
            if (DB::isError( $addPlan ) ) {
                $this->logger2->info( $addPlan->getUserInfo() );
                return false;
                exit;
            }
            require_once("classes/dbi/ordered_service_option.dbi.php");
            $ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
            if ($service_name == "Paperless_Free") {

                $data = array(
                "room_key"                          => $room_key,
                "service_option_key"                => "16",
                "ordered_service_option_status"     => 1,
                "ordered_service_option_starttime"  => date("Y-m-d H:i:s"),
                "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
                "ordered_service_option_deletetime" => "0000-00-00 00:00:00",
                );
                $addOption = $ordered_service_option->add($data);
                if (DB::isError( $addOption ) ) {
                    $this->logger2->info( $addOption->getUserInfo() );
                    return false;
                    exit;
                }
            }
            return $room_key;
        }
    }

    /**
     * id からそのユーザーが存在するかチェック
     */
    private function _isExistMember($id){
        //member, user, 認証DBのuser のテーブルをチェック
        $user_db = new N2MY_DB($this->get_dsn(), "user");
        $member_db = new N2MY_DB($this->get_dsn(), "member");
        $id = mysql_real_escape_string($id);
        if ( $user_db->numRows( sprintf("user_id='%s'", $id ) ) > 0 ||
             $member_db->numRows( sprintf("member_id='%s'", $id ) )  > 0 ||
             $user_db->numRows( sprintf("user_id='%s'", $id ) )  > 0 ){
            return true;
        } else {
            return false;
        }
    }

     /**
     * ユーザーを停止
     */
    function action_delete_user() {
        $user_key = $this->request->get("value");
        $data_db = new N2MY_DB($this->get_dsn(), "user");
        $where = "user_key = ".addslashes($user_key);
        $data = array(
            "user_delete_status" => 2,
            "user_deletetime" => date("Y-m-d H:i:s"),
        );
        $data_db->update($data, $where);
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "user",
            "keyword"            => "user_key",
            "info"               => $user_key,
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return $this->action_list();
    }

    /*
     * 操作ログ登録
     */
    function add_operation_log($operation_data) {
        $this->logger->debug("operation_log",__FILE__,__LINE__,$operation_data);
        $operation_db = new N2MY_DB($this->account_dsn, "operation_log");
        $operation_data["create_datetime"] = date("Y-m-d H:i:s");
        $add_data = $operation_db->add($operation_data);
        if (DB::isError($add_data)) {
            $this->logger2->error($add_data->getUserInfo());
            return false;
        }
        return true;
    }

    function getRule($columns) {
        $rules = array();
        foreach($columns as $colum => $colum_info) {
            $this->logger->trace("not_null",__FILE__,__LINE__,$colum_info);
            if (($colum_info["flags"]["not_null"] == "1") && ($colum_info["flags"]["auto_increment"] == "")) {
                $rules[$colum]["required"] = true;
            }
        }
        $this->logger2->trace($rules);
        return $rules;
    }

    /**
     * デフォルトページ
     */
    function default_view(){
        return $this->render_main();
    }

    /**
     * ログアウト
     */
    function action_logout() {
        $url = $this->get_redirect_url("admin_tool/paperless.php");
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => "logout",
            "table_name"         => "staff",
            "keyword"            => $_SESSION["_authsession"]["username"],
            "info"               => "logout",
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        $this->_auth->logout();
        unset($_SESSION[$this->_sess_page]);
        header("Location: ".$url);
    }

//===================================================================
// 表示
//===================================================================
    function render_main() {
        // セッションにdsnを登録
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $server_info = "";
        $server_key = $this->request->get("server_key");
        $account_db = new N2MY_DB($this->account_dsn, "db_server");
        $db_server_list = $account_db->getRowsAssoc("server_status = 1");
        $this->template->assign("db_server_list", $db_server_list);
        if ($server_key) {
            $server_info = $obj_MGMClass->getServerInfo($server_key);
        } else {
            $server_key = $db_server_list[0]["server_key"];
            $server_info = $obj_MGMClass->getServerInfo($server_key);
        }
        $this->session->set("now_server_key", $server_key);
        if (isset($server_info["dsn"])) {
            $this->set_dsn($server_info["dsn"]);
        }
        $this->logger->trace("dsn",__FILE__,__LINE__,$this->get_dsn());
        $this->display("admin_tool/index.t.html");
    }

    function render_menu() {
        $server_key = $this->session->get("now_server_key");
        $this->logger->debug("server_key",__FILE__,__LINE__,$server_key);
        $account_db = new N2MY_DB($this->account_dsn, "db_server");
        $db_server_list = $account_db->getRowsAssoc("server_status = 1");
        $this->logger2->debug($db_server_list);
        $this->logger->trace("dsn",__FILE__,__LINE__,$this->get_dsn());
        $this->template->assign("select_server_key", $server_key);
        $this->template->assign("db_server_list", $db_server_list);
        $this->_display("admin_tool/menu.t.html");
    }

    /**
     * フォームの内容を再現
     */
    function set_form($form_name, $default_sort_key, $default_sort_type) {
        //
        // デフォルトのソート順指定
        $sort_key = $this->request->get("sort_key");
        $sort_type = $this->request->get("sort_type");
        $page = $this->request->get("page");
        $page_cnt = $this->request->get("page_cnt");
        $reset = $this->request->get("reset");
        // デフォルト
        if (!isset($_SESSION[$this->_sess_page][$form_name]) || $reset == 1 || $reset == 2) {
            $_SESSION[$this->_sess_page][$form_name]['sort_key']  = $default_sort_key;
            $_SESSION[$this->_sess_page][$form_name]['sort_type'] = $default_sort_type;
            $_SESSION[$this->_sess_page][$form_name]['page'] = 1;
            $_SESSION[$this->_sess_page][$form_name]['page_cnt'] = 20;
        }
        if ($reset == 1) {
        $this->logger->debug("session",__FILE__,__LINE__,$_SESSION);
            unset($_SESSION[$this->_sess_page][$form_name]["request"]);
        } else {
            // ソート
            if ($sort_key) {
                $_SESSION[$this->_sess_page][$form_name]['sort_key']  = $sort_key;
                $_SESSION[$this->_sess_page][$form_name]['sort_type'] = $sort_type;
            }
            // ページ
            if ($page) {
                $_SESSION[$this->_sess_page][$form_name]['page'] = $page;
            }
            // ページ
            if ($page_cnt) {
                $_SESSION[$this->_sess_page][$form_name]['page_cnt'] = $page_cnt;
            }
            $request = $this->request->get("form");
            if ($request) {
                $_SESSION[$this->_sess_page][$form_name]['page'] = 1;
                $_SESSION[$this->_sess_page][$form_name]["request"] = $request;
            }
        }
        return $_SESSION[$this->_sess_page][$form_name];
    }

    /**
     * クエリーを自動で作成
     */
    function _get_query($columns, $request, $and_or = "AND") {
        $where = "";
        $and_or = " ".$and_or." ";
        $condition = array();
        foreach($columns as $key => $item){
            $fields .= ",".$key;
            switch ($item["type"]) {
            // 数値
            case "integer":
                switch($item["search"]) {
                // 同一
                case "equal":
                    if ($request[$key]) {
                        $condition[] = $key." = ".addslashes($request[$key]);
                    }
                    break;
                // 範囲指定
                case "range":
                    if ($request[$key]["min"]) {
                        $condition[] = $key." >= ".addslashes($request[$key]["min"]);
                    }
                    if ($request[$key]["max"]) {
                        $condition[] = $key." <= ".addslashes($request[$key]["max"]);
                    }
                    break;
                }
                break;
            // 文字列
            case "string":
                if ($request[$key] !== "") {
                    switch($item["search"]) {
                    // 同一
                    case "equal":
                        $condition[] = $key." = '".addslashes($request[$key])."'";
                        break;
                    case "prefix":
                        $condition[] = $key." like '".addslashes($request[$key])."%'";
                        break;
                    case "suffix":
                        $condition[] = $key." like '%".addslashes($request[$key])."'";
                        break;
                    case "like":
                        $condition[] = $key." like '%".addslashes($request[$key])."%'";
                        break;
                    }
                }
                break;
            case "datetime":
                if ($request[$key]["min"]) {
                    $condition[] = $key." >= '".$request[$key]["min"]."'";
                }
                if ($request[$key]["max"]) {
                    $condition[] = $key." <= '".$request[$key]["max"]."'";
                }
                break;
            }
            if ($condition) {
                $where = implode($and_or, $condition);
            }
        }
        $fields = substr($fields, 1);
        $ret = array(
            "fields" => $fields,
            "where" => $where
        );
        return $ret;
    }

    function _get_relation_data($dsn, $columns) {
        foreach ($columns as $key => $value) {
            if (isset($value["item"]["relation"])) {
                $relation = $value["item"]["relation"];
                if ($relation["db_type"] == "account") {
                    $relation_db = new N2MY_DB($this->account_dsn, $relation["table"]);
                } else {
                    $relation_db = new N2MY_DB($dsn, $relation["table"]);
                }
                if ($relation["status_name"]) {
                    $where = $relation["status_name"]."='".$relation["status"]."'";
                    $relation_data = $relation_db->getRowsAssoc($where);
                } else {
                    $relation_data = $relation_db->getRowsAssoc();
                }
                if ($relation["default_key"] || $relation["default_value"]) {
                    $columns[$key]["item"]["relation_data"][$relation["default_key"]] = $relation["default_value"];
                }
                foreach ($relation_data as $_data) {
                    $columns[$key]["item"]["relation_data"][$_data[$relation["key"]]] = $_data[$relation["value"]];
                }
            }
        }
        return $columns;
    }

    function _user_validator($form_data, $type) {
        //ユーザーID
        if ($type == "add") {
            if (!preg_match('/^[[:alnum:]._-]{3,30}$/', $form_data['user_id'])) {
                $message["user_id"] .= USER_ERROR_ID_LENGTH;
            }
        }
        //ユーザーパスワードをチェック
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $form_data['user_password'])) {
            $message["user_password"] .= USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$form_data['user_password']) || preg_match('/^[[:alpha:]]+$/',$form_data['user_password'])) {
            $message["user_password"] .= USER_ERROR_PASS_INVALID_02;
        }
        //管理者パスワード
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $form_data['user_admin_password'])) {
            $message["user_admin_password"] .= USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$form_data['user_admin_password']) || preg_match('/^[[:alpha:]]+$/',$form_data['user_admin_password'])) {
            $message["user_admin_password"] .= USER_ERROR_PASS_INVALID_02;
        }
        return $message;
    }

    /**
     * 乱数ID発行
     */
    function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this->dec2any($c);
    }

    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=57, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr(
                    '23456789ab' .
                    'cdefghijkm' .
                    'nopqrstuvw' .
                    'xyzABCDEFG' .
                    'HJKLMNPQRS' .
                    'TUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }

    function _display ($template) {
        $staff_auth = $_SESSION["_authsession"]["data"]["authority"];
        $this->template->assign("authority", $staff_auth);
        $this->display($template);
    }

    function action_agency_list() {
        $agency_id = $this->request->get("agency_id");
        $table = $this->request->get("table");
        $this->logger->info($table);
        if ($agency_id) {
            $this->session->set("agency_id",$agency_id);
        } else {
            $agency_id = $this->session->get("table");;
        }
        if ($table) {
            $this->session->set("table",$table);
        } else {
            $table = $this->session->get("table");;
        }
        $sort_type = $this->request->get("sort_type");
        $action_name = __FUNCTION__;
        // 検索条件設定・取得
        $sort_type = $sort_type ? $sort_type : "desc";
        $form_data = $this->set_form($action_name, "user_key", $sort_type);
        $page    = $form_data['page'] ? $form_data['page']: "1";
        $limit   = $form_data['page_cnt'] ? $form_data['page_cnt'] : "20";
        $offset  = ($limit * ($page - 1));
        if ($table == "user") {
            $agencyRelationObj = new N2MY_DB($this->get_dsn(), "agency_relation_user");
            $sql = "select agency_relation_user.user_key, user.user_id, user.user_company_name, agency_relation_user.user_key" .
                " FROM agency_relation_user" .
                " LEFT JOIN user ON agency_relation_user.user_key = user.user_key" .
                " WHERE agency_relation_user.agency_id = ".addslashes($agency_id);
            $sql .= " GROUP BY agency_relation_user.user_key ".$sort_type;
        } elseif ($table == "member") {
            $agencyRelationObj = new N2MY_DB($this->get_dsn(), "agency_relation_member");
            $sql = "select agency_relation_member.member_key, member.member_id, member.member_name, agency_relation_member.member_key" .
                " FROM agency_relation_member" .
                " LEFT JOIN member ON agency_relation_member.member_key = member.member_key" .
                " WHERE agency_relation_member.agency_id = ".addslashes($agency_id);
            $sql .= " GROUP BY agency_relation_member.member_key ".$sort_type;
        }
        $total_count = $agencyRelationObj->numRows("agency_id = ".mysql_real_escape_string($agency_id));
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => "action_agency_list",
            "after_action" => $this->request->get("after_action"))
            );
        $ret = $agencyRelationObj->_conn->limitQuery($sql, $offset, $limit);
        $rows = array();
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
        } else {
            while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
                $rows[] = $row;
            }
        }
        $this->template->assign("rows", $rows);
        if ($table == "user") {
            $this->display("admin_tool/account/agency/user_list.t.html");
        } elseif ($table == "member") {
            $this->display("admin_tool/account/agency/member_list.t.html");
        }
    }

    function action_table_info_builder() {
        $table = $this->request->get("table");
        $type  = $this->request->get("type");
        if (!$table || !$type) {
            print 'error';
        } else {
            if ($type == "account") {
                $dsn = $this->account_dsn;
            } else {
                $type = 'data';
                $dsn = $this->get_dsn();
            }
            $table = new N2MY_DB($dsn, $table);
            if (DB::isError($table)) {
                print $table->getUserInfo();
            } else {
                if (!$xml = $table->tableInfoBuilder($type)) {
                    print 'build error';
                } else {
                    print $xml;
                }
            }
        }
    }

    private function exitWithError($msg)
    {
        $b = debug_backtrace();

        $from = sprintf("called %s+%s : %s",$b[0]['file'], $b[0]['line'], $b[1]['function']);
        $this->logger->warn($msg.$from);

        exit($msg.$from);
    }
    //{{{validatePGiSetting
    private function validatePGiSetting($form, $room_key)
    {
        $res = array('data'    => $form,
                     'message' => '');

        if (!$form["pgi_start_date"]) {
            $res['message'] .= "Please input the start time.<br />";
        } else {
            list($y, $m) = explode('/',$form["pgi_start_date"]);
            if (!checkdate($m, 1, $y)) {
                $res['message'] .= "Start Time is not correct.<br />";
            } else {
                $startdate = str_replace('/','-',$form["pgi_start_date"])."-01";
                // 過去分の設定はできません
                if ($startdate < date('Y-m').'-01') {
                    $res['message'].= "Can not be set as a past time.<br />";
                }
            }
        }
        $res['data']['startdate']  = $startdate;

        require_once('classes/pgi/PGiSystem.class.php');
        $pgiSystems    = PGiSystem::findConfigAll();
        $pgiSystemKeys = array();
        $selectedSystem         = null;
        foreach ($pgiSystems as $system) {
            if (@$form["system_key"] == (string)$system->key) {
                $selectedSystem = $system;
            }
        }

        if (!$form["system_key"]) {
            $res['message'] .= "system_keyを入力してください。<br />";
        } elseif (!$selectedSystem) {
            $res['message'] .= "system_keyが不正です<br />";
        }

        if ($form["company_id_type"] == 'default') {
            $form["company_id"]        = $selectedSystem->defaultCompanyID;
            $res['data']['company_id'] = $selectedSystem->defaultCompanyID;
        }

        if (!$form["company_id"]) {
            $res['message'] .= "Please input company_id<br />";
        } else {
            if ($selectedSystem && $selectedSystem->rates) {
                require_once("classes/pgi/PGiRate.class.php");
                // validate rate's values
                foreach (PGiRate::findConfigAll() as $rate){
                    $col  = (string)$rate->type;
                    $name = (string)$rate->description;
                    if (!$form[$col]) {
                        $res['message'] .= "Please input ".$name.".<br />";
                    } elseif (is_int($form[$col])) {
                        $res['message'] .= $name." should be number.<br />";
                    } elseif ($form[$col] <= 0) {
                        $res['message'] .= $name." should larger than 0.<br />";
                    }
                }
            }
        }

        require_once("classes/dbi/pgi_setting.dbi.php");
        $pgi_setting_table = new PGiSettingTable($this->get_dsn());
        try {
            $pgi_settings  = $pgi_setting_table->findByRoomKey($room_key);
        } catch(Exception $e) {
            die('error'.$e->getMessage());
        }
        if (0 == count($pgi_settings)) {
            return $res;
        }

        foreach ($pgi_settings as $setting) {
            if ($startdate == $setting['startdate']) {
                $res['message'] .= $startdate." setting have been saved.(auto generate when meeting start.）<br />";
                return $res;
            }
        }

        return $res;
    }
    function action_create_member_id() {
        $prefix = "VCH000";
        $member_list = array();
        for ($i = 1; $i <= 1000; $i++) {
            $member_list[] = array("id" => $prefix.sprintf("%04d",$i), "pass" => $this->create_id().rand(2,9));
        }
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            $csv = new EZCsv(true, "UTF-8", "UTF-8");
            $dir = $this->get_work_dir();
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");
            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="'.date("Ym").'.csv"');
            // ヘッダ２
            $header["id"] = "ID";
            $header["pass"] = "Password";
            $csv->setHeader($header);
            $csv->write($header);

           foreach($member_list as $row) {
                $csv->write($row);
            }
            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            while($str = fread($fp, 4096)) {
                print $str;
            }
            @fclose($fp);
            $this->logger2->info(memory_get_peak_usage());
            unlink($tmpfile);
            return true;
    }

    function _getMeetingVersion() {
        static $version;
        if ($version) {
            return $version;
        }
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }
}

$main = new AppFree();
$main->execute();