<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/dbi/user.dbi.php");

class AppHddNotice extends AppFrame {

    var $_core_db_dsn = "";

    function init() {
    }

    function auth() {
    }

    function default_view(){
        // 部屋一覧
        $user_obj = new UserTable($this->get_dsn());
        $where = "user_status = 1" .
        		" AND user_delete_status = 0";
        $user_list = $user_obj->getRowsAssoc($where, array("user_id" => "sorc"), null, null, "user_key, user_id, user_company_name, invoice_flg", "user_key");
        $account = new N2MY_Account($this->get_dsn());
        // 容量取得
        $core = new N2MY_DB($this->get_dsn(), "meeting");
        $sql = "SELECT room_key, sum(meeting_size_used) as hdd_use_size" .
                " FROM `meeting`" .
                " WHERE is_deleted = 0".
                " AND is_active = 0".
                " AND use_flg = 1".
                " GROUP BY room_key";
        $rs = $user_obj->_conn->query($sql);
        while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $room_key = $row["room_key"];
            $meeting_hdd[$room_key] = $row["hdd_use_size"];
        }
        $over_list = array();
        $limit_list = array();
        $s = array('byte', 'KB', 'MB', 'GB', 'TB');
        foreach($user_list as $user) {
        	$user_key = $user["user_key"];
	        $rooms = $account->getRoomList($user_key);
            foreach($rooms as $room_key => $room) {
                // 契約しているディスク容量
                $max_size = (1024 * 1024 * 500) + (1024 * 1024 * 1024 * (($room["options"]["hdd_extention"]) ? $room["options"]["hdd_extention"] : 0));
                // ユーザの使用容量
                $uze_size = ($meeting_hdd[$room_key]) ? $meeting_hdd[$room_key] : "0";
                $use_percent = number_format($uze_size / $max_size * 100);
                $_room_data["user_id"] = $user["user_id"];
                $_room_data["user_name"] = $user["user_company_name"];
                $_room_data["room_key"] = $room_key;
                $_room_data["room_name"] = $room["room_info"]["room_name"];
                $_room_data["max_size"] = number_format($max_size);
                $_room_data["uze_size"] = number_format($uze_size);
                $_room_data["use_percent"] = $use_percent;
                if ($uze_size > $max_size) {
	                $e = floor(log($max_size)/log(1024));
	                $_room_data["max_size_view"] = sprintf('%.2f '.$s[$e], ($max_size/pow(1024, floor($e))));
	                $e = floor(log($uze_size)/log(1024));
	                $_room_data["use_size_view"] = sprintf('%.2f '.$s[$e], ($uze_size/pow(1024, floor($e))));
                    $over_list[] = $_room_data;
                } elseif ($_room_data["use_percent"] > 80)  {
	                $e = floor(log($max_size)/log(1024));
	                $_room_data["max_size_view"] = sprintf('%.2f '.$s[$e], ($max_size/pow(1024, floor($e))));
	                $e = floor(log($uze_size)/log(1024));
	                $_room_data["use_size_view"] = sprintf('%.2f '.$s[$e], ($uze_size/pow(1024, floor($e))));
                    $limit_list[] = $_room_data;
                }
            }
        }
        $to = $this->config->get("NOTIFICATION", "to");
        $from = $this->config->get("NOTIFICATION", "from");
        $subject = $this->get_message("NOTIFICATION", "hdd_over_subject");
        $this->template->assign("over_list", $over_list);
        $this->template->assign("limit_list", $limit_list);
        $body = $this->fetch("common/mail/hdd_over_data.t.txt");
        print "<pre>";
        print $body;
        if ($this->request->get("mail")) {
	        $this->sendReport($from, $to, $subject, $body);
        }
    }
}

$main = new AppHddNotice();
$main->execute();
