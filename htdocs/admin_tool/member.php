<?php

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");

class AppMember extends AppFrame {

    var $_auth = null;
    var $_sess_page;
    var $account_dsn = null;

    function init() {
        $_COOKIE["lang"] = "en";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
    }

    public function auth()
    {
    }

    public function default_view()
    {
        $this->action_top();
    }

    public function action_top()
    {
        $request = $this->request->getAll();
        $serviceStartYear = "2007";
        $thisYear = $request["year"] ? $request["year"] : date( "Y" );
        while( $serviceStartYear <= $thisYear ){
            $info["yearlist"][$serviceStartYear] = $serviceStartYear;
            $info["year"] = $serviceStartYear;
            $serviceStartYear += 1;
        }
        for( $i = 1; $i <= 12; $i++ ){
            $info["monthlist"][sprintf( "%02d", $i )] = sprintf( "%02d", $i );
        }
        $info["month"] = $request["month"] ? sprintf( "%02d", $request["month"] ) : date( "m" );

        $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);

        $info["userList"] = array();

        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            $objUser = new UserTable( $dsn );
            $objMember = new MemberTable( $dsn );
            $objMeeting = new MeetingTable( $dsn );
            $objParticipant = new DBI_Participant( $dsn );
            $where = "account_model = 'member' AND " .
                     "user_status=1 AND " .
                     "( user_delete_status = 0 OR " .
                     "    ( user_delete_status = 2 AND DATE_FORMAT( user_deletetime, '%Y-%m' ) >= '" . $info["year"]. "-" . $info["month"] . "' )" .
                     ")";
            $sort = array( "user_key" => "ASC");
            $userList = $objUser->getRowsAssoc( $where, $sort );
            for( $i = 0; $i < count( $userList ); $i++ ){
                $where = sprintf( "user_key='%s'", $userList[$i]["user_key"] );
                $sort = array( "member_key" => "ASC");

                //部屋データ(メンバーデータ)取得
                $roomList = $objMember->getRowsAssoc( $where, $sort );
                foreach( $roomList as $roomListKey => $roomInfo ){
                    $sort = array( "meeting_key" => "ASC");
                    $where = "room_key='" . $roomInfo["room_key"]. "' AND
                              use_flg = 1 AND
                              is_active = 0 AND
                              DATE_FORMAT( actual_end_datetime, '%Y-%m' ) = '" . $info["year"]. "-" . $info["month"] ."'";

                    //部屋ーごとの終了済み会議を取得
                    $meetingList = $objMeeting->getRowsAssoc( $where, $sort );
                    foreach( $meetingList as $meetingListKey => $meetingInfo ){

                        //参加者それぞれの利用時間とメンバーであればメンバーデータも取得
$sql = "
SELECT
    m.*, p.use_count, p.uptime_start, p.uptime_end
FROM
(
SELECT
    member_key, SUM( use_count ) as use_count, MIN( uptime_start ) as uptime_start, MAX( uptime_end ) as uptime_end
FROM
    participant
WHERE
    meeting_key='" . $meetingInfo["meeting_key"] . "'
GROUP BY
    member_key
) as p
LEFT JOIN
    member m
ON
    p.member_key = m.member_key";
                        $ret = $objParticipant->_conn->query( $sql );
                        if (DB::isError($ret)) {
                            $this->logger2->error($ret->getUserInfo());
                            exit;
                        }

                        while( $row = $ret->fetchRow( DB_FETCHMODE_ASSOC ) ) {
                            $meetingInfo["memberList"][] = $row;
                        }
                        $roomInfo["meetingList"][] = $meetingInfo;
                    }
                    $userList[$i]["roomList"][] = $roomInfo;
                }
                $info["userList"][] = $userList[$i];
            }
        }
        $this->template->assign( "info", $info );

        $this->display( "admin_tool/member.t.html" );
    }
}


$main =& new AppMember();
$main->execute();