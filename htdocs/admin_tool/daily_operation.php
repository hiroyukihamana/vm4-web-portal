<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");

class AppDailyOperation extends AppFrame {

    function init() {
        // DBサーバ情報取得
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->dsn = $this->_get_dsn();
        define("WSDL_DOMAIN", "mcu02.nice2meet.us:8080");
    }

    function default_view() {
        $now_date = date("Y-m-d", time());
        foreach($this->dsn as $dsn) {
            //プラン変更
            $plan_obj = new N2MY_DB($dsn, "room_plan");
            $where = "room_plan_starttime <= '$now_date 23:59:59'".
                     " AND room_plan_status = 2";
            $waiting_start_data = $plan_obj->getRowsAssoc($where);
            $this->logger->trace("today",__FILE__,__LINE__,$waiting_start_data);
            $room_db = new N2MY_DB($dsn, "room");
            if ($waiting_start_data) {
                foreach($waiting_start_data as $wait_data) {
                    $where_plan = "room_key = '".addslashes($wait_data["room_key"])."'".
                                  " AND room_plan_status = 1";
                    $now_plan = $plan_obj->getRowsAssoc($where_plan);
                    $now_plan = end($now_plan);
                    $this->logger->debug("now_plan",__FILE__,__LINE__,$now_plan);
                    if ($now_plan) {
                        $data_old_plan = array(
                            "room_plan_status"     => "0",
                            "room_plan_updatetime" => date('Y-m-d H:i:s'),
                        );
                        $where_change_oldplan = "room_plan_key = ".$now_plan["room_plan_key"];
                        $plan_obj->update($data_old_plan, $where_change_oldplan);
                    }
                    //サービスダウングレードの際はオプションも削除
                    $this->logger->debug("now_plan",__FILE__,__LINE__,$now_plan["service_key"]);
                    /* 削除しないように変更（2013-09-24）
                    if ($now_plan["service_key"] == "44") {
                        $this->_room_option_delete($dsn, $wait_data["room_key"], "3");
                        $this->_room_option_delete($dsn, $wait_data["room_key"], "5");
                        $this->_room_option_delete($dsn, $wait_data["room_key"], "6");
                    } else if ($wait_data["service_key"] == "41" || $wait_data["service_key"] == "42") {
                        $this->_room_option_delete($dsn, $wait_data["room_key"], "3");
                        $this->_room_option_delete($dsn, $wait_data["room_key"], "6");
                    }
                    */
                    //資料共有プラン以外の場合は資料共有オプション削除
                    if ($wait_data["service_key"] != "102" && $wait_data["service_key"] != "94" && $wait_data["service_key"] != "56") {
                        $this->_room_option_delete($dsn , $wait_data["room_key"], "16", $now_date);
                    }
                    $data_new_plan = array(
                        "room_plan_status" => "1",
                        "room_plan_updatetime" => date('Y-m-d H:i:s'),
                    );
                    $where_change_newplan = "room_plan_key = ".$wait_data["room_plan_key"];
                    $plan_obj->update($data_new_plan, $where_change_newplan);

                    //シート、オプション登録
                    $room_db = new N2MY_DB($this->get_dsn(), "room");
                    $service_db = new N2MY_DB($this->account_dsn, "service");
                    $service_info = $service_db->getRow("service_key = ".$wait_data["service_key"]);
                    //オーディエンスオプション数で人数変更
                    require_once("classes/dbi/ordered_service_option.dbi.php");
                    $ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
                    if ($service_info["max_audience_seat"] == "0") {
                        $where = "room_key = '".addslashes($wait_data["room_key"])."'".
                                 " AND service_option_key = 7".
                                 " AND ordered_service_option_status = 1";
                        $audience_count = $ordered_service_option->numRows($where);
                        $this->logger->trace("audience",__FILE__,__LINE__,$audience_count);
                        if ($audience_count > 0 ) {
                            $max_audience = $audience_count."0";
                        } else {
                            $max_audience = $service_info["max_audience_seat"];
                        }
                    } else {
                        $max_audience = $service_info["max_audience_seat"];
                    }
                    $where = "room_key = '".addslashes($wait_data["room_key"])."'";
                    $room_seat = array (
                        "max_seat" => $service_info["max_seat"],
                        "max_audience_seat" => $max_audience,
                        "max_whiteboard_seat" => $service_info["max_whiteboard_seat"],
                        "meeting_limit_time" => $service_info["meeting_limit_time"],
                        "ignore_staff_reenter_alert"    => ($service_info["ignore_staff_reenter_alert"]) ? $service_info["ignore_staff_reenter_alert"] : 0 ,
                        "max_guest_seat"       => ($service_info["max_guest_seat_flg"]) ? $service_info["max_guest_seat"] : 0,
                        "max_guest_seat_flg"       => ($service_info["max_guest_seat_flg"]) ? 1 : 0,
                        "extend_seat_flg"       => ($service_info["extend_seat_flg"]) ? 1 : 0,
                        "extend_max_seat"  => ($service_info["extend_seat_flg"]) ? $service_info["extend_max_seat"] : 0,
                        "extend_max_audience_seat" => ($service_info["extend_seat_flg"]) ? $service_info["extend_max_audience_seat"] : 0,
                        "default_camera_size" => $service_info["default_camera_size"],
                        "hd_flg"              => $service_info["hd_flg"],
                        "active_speaker_mode_only_flg"       => $service_info["active_speaker_mode_only_flg"],
                        "active_speaker_mode_use_flg"        => $service_info["active_speaker_mode_only_flg"],
                        "use_stb_option"       => $service_info["use_stb_plan"],
                        "room_updatetime" => date("Y-m-d H:i:s"),
                        );
                    if ($service_info["disable_rec_flg"]){
                         $room_seat["disable_rec_flg"] = $service_info["disable_rec_flg"];
                    }
                    if ($service_info["use_stb_plan"]) {
                        $room_seat["default_h264_use_flg"] = 1;
                        $room_seat["active_speaker_mode_only_flg"] = 1;
                    }
                    if ($service_info["max_room_bandwidth"] != 0) {
                        $room_seat["max_room_bandwidth"] = $service_info["max_room_bandwidth"];
                    }
                    if ($service_info["max_user_bandwidth"] != 0) {
                        $room_seat["max_user_bandwidth"] = $service_info["max_user_bandwidth"];
                    }
                    if ($service_info["min_user_bandwidth"] != 0) {
                        $room_seat["min_user_bandwidth"] = $service_info["min_user_bandwidth"];
                    }
                    if ($service_info["service_name"] == "VCUBE_Doctor_Standard") {
                        $room_seat["invited_limit_time"] = $service_info["invited_limit_time"] ? $service_info["invited_limit_time"] : 30;
                    }
                    $add_seat = $room_db->update($room_seat, $where);
                    //プランのオプション情報取得
                    $add_options = array();
                    if ($service_info["meeting_ssl"] == 1) {
                        $add_options[] = "2";
                    }
                    if ($service_info["desktop_share"] == 1) {
                        $add_options[] = "3";
                    }
                    if ($service_info["high_quality"] == 1) {
                        $add_options[] = "5";
                    }
                    if ($service_info["mobile_phone"] > 0) {
                        $add_options[] = "6";
                    }
                    if ($service_info["h323_client"] > 0) {
                        $add_options[] = "8";
                    }
                    //hdd_extentionは数値分登録
                    if ($service_info["hdd_extention"] > 0) {
                        $add_options[] = "4";
                    }
                    // ホワイトボードプラン
                    if ($service_info["whiteboard"] == 1) {
                        $add_options[] = "16";
                    }
                    // マルチカメラ
                    if ($service_info["multicamera"] == 1) {
                        $add_options[] = "18";
                    }
                    // 電話連携
                    if ($service_info["telephone"] == 1) {
                        $add_options[] = "19";
                    }
                    // 録画GW
                    if ($service_info["record_gw"] == 1) {
                        $add_options[] = "20";
                    }
                    // スマートフォン
                    if ($service_info["smartphone"] == 1) {
                        $add_options[] = "21";
                    }
                    // 資料共有映像再生許可
                    if ($service_info["whiteboard_video"] == 1) {
                        $add_options[] = "22";
                    }
                    // PGi連携
                    if ($service_info["teleconference"] == 1) {
                        $add_options[] = "23";
                    }
                    // H.264
                    if ($service_info["h264"] == 1) {
                        $add_options[] = "26";
                    }
                    // global_link
                    if ($service_info["global_link"] == 1) {
                        $add_options[] = "30";
                    }
                    //オプション登録
                    if ($add_options) {
                        foreach ($add_options as $value) {
                            $where = "room_key = '".addslashes($wait_data["room_key"])."'".
                                     " AND service_option_key = ".$value.
                                     " AND ordered_service_option_status = 1";
                            $count = $ordered_service_option->numRows($where);
                            $this->logger2->debug($count);
                            if ($count == 0 || "4" == $value || "6" == $value || "8" == $value) {
                                if ("4" == $value) {
                                    $this->logger2->info($value);
                                    for ($num = 1; $num <= $service_info["hdd_extention"]; $num++ ) {
                                        $this->_room_option_add($dsn, $wait_data["room_key"], $value, null);
                                    }
                                } else if ("6" == $value) {
                                    $this->logger2->info($value);
                                    for ($num = 1; $num <= $service_info["mobile_phone"]; $num++ ) {
                                        $this->_room_option_add($dsn, $wait_data["room_key"], $value, null);
                                    }
                                } else if ("8" == $value) {
                                    $this->logger2->info($value);
                                    for ($num = 1; $num <= $service_info["h323_client"]; $num++ ) {
                                        $this->_room_option_add($dsn, $wait_data["room_key"], $value, null);
                                    }
                                } else {
                                    $this->_room_option_add($dsn, $wait_data["room_key"], $value, null);
                                }
                            }
                        }
                        if (DB::isError($add_seat)) {
                            $this->logger2->debug( $add_seat->getUserInfo());
                        }
                    }
                    //プレミアムプランへの変更時はオーディエンスオプションを停止
                    if ($service_info["max_audience"] != 0) {
                        $where = "room_key = '".addslashes($wait_data["room_key"])."'".
                                     " AND service_option_key = 7".
                                     " AND ordered_service_option_status = 1";
                        $data = array (
                            "ordered_service_option_status" => "0"
                        );
                        $oudience_option = $ordered_service_option->update($data, $where);
                    }
                    // ストレージ容量の再計算

                    $new_service_info = $service_info;
                    // ユーザー情報取得
                    $room_db =  new N2MY_DB($this->get_dsn(), "room");
                    $room_info = $room_db->getRow("room_key = '".addslashes($wait_data["room_key"])."'");
                    $user_key = $room_info["user_key"];
                    $where = "user_key='".addslashes($user_key)."'";
                    $user_db = new N2MY_DB($this->get_dsn(), "user");
                    $user_info = $user_db->getRow($where);
                    if($user_info["account_model"] == ""){
                        $storage_column_name = "add_storage_size";
                    }else if($user_info["account_model"] == "member"){
                        $storage_column_name = "member_storage_size";
                    }
                    if($now_plan){
                        $old_service_info = $service_db->getRow("service_key = ".$now_plan["service_key"]);
                        $new_storage_size = $user_info["max_storage_size"] + $new_service_info[$storage_column_name] - $old_service_info[$storage_column_name];
                    }
                    else{
                        $new_storage_size = $user_info["max_storage_size"] + $new_service_info[$storage_column_name];
                    }
                    if($user_info["max_storage_size"] != $new_storage_size){
                        if($new_storage_size < 0){
                            $new_storage_size = 0;
                        }

                        if($new_storage_size != $user_info["max_storage_size"]){
                            $user_data = array(
                                    "max_storage_size" => $new_storage_size
                            );
                            $user_db->update($user_data, $where);
                        }
                    }

                    //操作ログ登録
                    $operation_data = array (
                        "staff_key"          => "daily_operation",
                        "action_name"        => "daily_operation plan",
                        "table_name"         => "room_plan",
                        "keyword"            => "daily_operation plan",
                        "info"               => $wait_data["room_plan_key"],
                        "operation_datetime" => date("Y-m-d H:i:s"),
                    );
                    $this->add_operation_log($operation_data);
                }
            }
            //オプション登録
            $option_obj = new N2MY_DB($dsn, "ordered_service_option");
            $where_op = "ordered_service_option_starttime <= '$now_date 23:59:59'".
                     " AND ordered_service_option_status = 2";
            $waiting_start_option = $option_obj->getRowsAssoc($where_op);
            $this->logger->debug("change_option",__FILE__,__LINE__,$waiting_start_option);
            if ($waiting_start_option) {
                require_once("classes/dbi/ordered_service_option.dbi.php");
                $ordered_service_option = new OrderedServiceOptionTable($dsn);
                foreach ($waiting_start_option as $option_data) {
                    $this->logger->trace("option",__FILE__,__LINE__,$option_data);
                    $where = "ordered_service_option_key = ".$option_data["ordered_service_option_key"];
                    $data = array(
                        "ordered_service_option_status" => 1,
                        "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
                    );
                    $ordered_service_option->update($data, $where);

                    //オーディエンスオプションの際は、オーディエンスオプション数で人数変更
                    if ("7" == $option_data["service_option_key"]) {
                        require_once("classes/dbi/ordered_service_option.dbi.php");
                        $where = "room_key = '".addslashes($option_data["room_key"])."'".
                                 " AND service_option_key = 7".
                                 " AND ordered_service_option_status = 1";
                        $audience_count = $ordered_service_option->numRows($where);
                        $this->logger->trace("audience",__FILE__,__LINE__,$audience_count);
                        if ($audience_count > 0 ) {
                            $max_seat = "9";
                            $max_audience = $audience_count."0";
                        }
                        $where = "room_key = '".addslashes($option_data["room_key"])."'";
                        $room_seat = array (
                            "max_seat" => $max_seat,
                            "max_audience_seat" => $max_audience,
                            "room_updatetime" => date("Y-m-d H:i:s"),
                            );
                        $add_seat = $room_db->update($room_seat, $where);
                    }

                    //操作ログ登録
                    $operation_data = array (
                        "staff_key"          => "daily_operation",
                        "action_name"        => "daily_operation option",
                        "table_name"         => "ordered_service_option",
                        "keyword"            => $option_data["room_key"],
                        "info"               => serialize(array("room_key" => $option_data["room_key"], "ordered_service_option_key" => $option_data["ordered_service_option_key"])),
                        "operation_datetime" => date("Y-m-d H:i:s"),
                    );
                    $this->add_operation_log($operation_data);
                }
            }
            //ユーザー停止
            $this->_user_delete($dsn, $now_date);

            //部屋停止
            $this->_room_delete($dsn, $now_date);

            //部屋オプション停止
            $this->_room_option_delete($dsn, "", "", $now_date, true);

            /** member課金用に追加 **/
            // ユーザープラン
            $this->changeUserPlan( $dsn, $now_date );

            // ユーザーオプション
            $this->changeUserOption( $dsn, $now_date );
        }
        //終了後に実行完了メール送信
        $this->logger2->info("dairy operation sucsess");
        $mail_subject = "【ミーティング】アカウント日次処理完了".$now_date;
        $mail_body = "【ミーティング】アカウント日次処理が完了しました。" ."\n\n" .
                     "■ Date: ".$now_date."\n".
                     "■ SERVER_NAME: ".$_SERVER["SERVER_NAME"]."\n".
                     "■ REQUEST_URI: ".$_SERVER["REQUEST_URI"]."\n";
        if (defined("N2MY_CRON_FROM") && defined("N2MY_CRON_TO") && N2MY_CRON_FROM && N2MY_CRON_TO) {
            $this->sendReport(N2MY_CRON_FROM, N2MY_CRON_TO, $mail_subject, $mail_body);
        }
    }

    /**
     * ユーザープラン変更
     */
    private function changeUserPlan( $dsn, $now_date )
    {
        //ユーザープラン変更
        $objPlan = new N2MY_DB($dsn, "user_plan");
        $where = "user_plan_starttime <= '$now_date 23:59:59'".
                     " AND user_plan_status = 2";
        $planList = $objPlan->getRowsAssoc( $where );
        for( $i = 0; $i < count( $planList ); $i++ ){
            $planInfo = $planList[$i];
            $where = sprintf( "user_key='%s' AND user_plan_status = 1", $planInfo["user_key"] );

            /** 現在アクティブなプランを取得して削除フラグを挿入**/
            $now_plan = $objPlan->getRowsAssoc( $where );
            $finishPlanData = array(
                                "user_plan_status"        => 0,
                                "user_plan_updatetime"    => $now_date
                                );
            for( $ii = 0; $ii < count( $now_plan ); $ii++ ){
                $objPlan->update( $finishPlanData, sprintf( "user_plan_key='%s'", $now_plan[$ii]["user_plan_key"] ) );
            }
            //ユーザープランを有効にする
            $activePlanData = array(
                                "user_plan_status"        => 1,
                                "user_plan_updatetime"    => $now_date
                                );
            $objPlan->update( $activePlanData, sprintf( "user_plan_key='%s'", $planInfo["user_plan_key"] ) );

            $user_key = $planInfo["user_key"];
            // ユーザーの言語限定 サービス名更新
            $service_db      = new N2MY_DB($this->account_dsn, "service");
            $where_service_key = "service_key = ".$planInfo["service_key"];
            $new_plan_info          = $service_db->getRow($where_service_key);
            $objUser = new N2MY_DB( $this->get_dsn(), "user" );
            // ユーザー情報の更新
            $user_data = array(
                    "lang_allow" => $new_plan_info["lang_allow"],
                    "service_name" => $new_plan_info["user_service_name"],
                    "guest_url_format" => ($new_plan_info["guest_url_format"] == 1) ? 1 : 0,
            );
            if($new_plan_info["external_user_invitation_flg"] == 1){
                $user_data["external_user_invitation_flg"] = 1;
            }
            if($new_plan_info["external_member_invitation_flg"] == 1){
                $user_data["external_member_invitation_flg"] = 1;
            }
            $user_where = sprintf( "user_key=%s", $user_key );
            $objUser->update($user_data, $user_where);

        }
    }

    /**
     * ユーザーオプション
     */
    private function changeUserOption( $dsn, $now_date )
    {
        // オプション情報取得
        $service_option_db = new N2MY_DB($this->account_dsn, "service_option");
        $service_option    = $service_option_db->getRowsAssoc("option_status = 1",array(), null, 0, "*", $key = "service_option_key");

        $objUserServiceOption = new N2MY_DB( $dsn, "user_service_option" );
        $where = "user_service_option_starttime <= '$now_date 23:59:59'".
                     " AND user_service_option_status = 2";
        $serviceOptionList = $objUserServiceOption->getRowsAssoc( $where );
        for( $i = 0; $i < count( $serviceOptionList ); $i++ ){
            $serviceOptionInfo = $serviceOptionList[$i];
            //ユーザープランを有効にする
            $activeOptionData["user_service_option_status"] = 1;
            $objUserServiceOption->update( $activeOptionData, sprintf( "user_service_option_key='%s'", $serviceOptionInfo["user_service_option_key"] ) );
            // ストレージオプションならストレージ再計算
            $service_option_name = $service_option[$serviceOptionInfo["service_option_key"]]["service_option_name"];
            if(defined($service_option_name) && is_numeric(constant($service_option_name))){
                $this->_addStorageSize(constant($service_option_name) , $serviceOptionInfo["user_key"]);
            }
        }

        // ユーザープラン削除
        $delete_where = "user_service_option_expiredatetime != '0000-00-00 00:00:00' ".
                " AND user_service_option_expiredatetime <='$now_date 23:59:59'".
                " AND user_service_option_status = 1";
        $DeleteServiceOptionList = $objUserServiceOption->getRowsAssoc( $delete_where );
        for( $i = 0; $i < count( $DeleteServiceOptionList ); $i++ ){
            $serviceOptionInfo = $DeleteServiceOptionList[$i];
            //ユーザープランを有効にする
            $activeOptionData["user_service_option_status"] = 0;
            $activeOptionData["user_service_option_deletetime"] = date("Y-m-d H:i:s", time());
            $objUserServiceOption->update( $activeOptionData, sprintf( "user_service_option_key='%s'", $serviceOptionInfo["user_service_option_key"] ) );
            // ストレージオプションならストレージ再計算
            $service_option_name = $service_option[$serviceOptionInfo["service_option_key"]]["service_option_name"];
            if(defined($service_option_name) && is_numeric(constant($service_option_name))){
                $this->_addStorageSize(constant($service_option_name) , $serviceOptionInfo["user_key"] , 0);
            }
        }

    }

    // ストレージ容量計算
    private function _addStorageSize($add_storage_size , $user_key , $add_flag = 1){
        $objUser = new N2MY_DB($this->get_dsn(), "user");
        $where = "user_key = " . $user_key;
        $user_info = $objUser->getRow($where);
        if($add_flag){
            $new_storage_size = $user_info["max_storage_size"] + $add_storage_size;
        }else{
            $new_storage_size = $user_info["max_storage_size"] - $add_storage_size;
            if($new_storage_size < 0){
                $new_storage_size = 0;
            }
        }

        $data = array(
                "max_storage_size" => $new_storage_size
        );
        $objUser->update($data, $where);
    }

    //オプション登録
    function _room_option_add($dsn, $room_key, $service_option_key) {
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable($dsn);
        $data = array(
            "room_key" => $room_key,
            "service_option_key" => $service_option_key,
            "ordered_service_option_status" => 1,
            "ordered_service_option_starttime" => date("Y-m-d 00:00:00"),
            "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
            "ordered_service_option_deletetime" => "0000-00-00 00:00:00",
            );
        $ordered_service_option->add($data);
    }

     /**
     * ユーザー停止
     */
     private function _user_delete($dsn, $now_date) {
        require_once("classes/dbi/user.dbi.php");
        $user_obj   = new UserTable($dsn);
        $where_user = "user_expiredatetime <= '$now_date 00:00:00'".
                      " AND user_expiredatetime != '0000-00-00 00:00:00'".
                      " AND user_delete_status = 1";
        $waiting_end_users = $user_obj->getRowsAssoc($where_user);
        $this->logger->info("end_user",__FILE__,__LINE__,$waiting_end_users);
        foreach ($waiting_end_users as $user) {
            $user_key     = addslashes($user["user_key"]);
            $where_delete = "user_key = '".$user_key."'".
                 " AND user_delete_status = 1";
            $data_delete = array(
                "user_deletetime"    => date("Y-m-d H:i:s"),
                "user_updatetime"    => date("Y-m-d H:i:s"),
                "user_delete_status" => 2
            );
            $user_obj->update($data_delete, $where_delete);
        }
        return true;
    }

    /**
    * 部屋停止
    */
    private function _room_delete($dsn, $now_date) {
        require_once("classes/dbi/user.dbi.php");
        $user_obj   = new UserTable($dsn);
        require_once("classes/dbi/room.dbi.php");
        $room_obj   = new RoomTable($dsn);
        $where_room = "room_expiredatetime <= '$now_date 00:00:00'".
                      " AND room_expiredatetime != '0000-00-00 00:00:00'".
                      " AND room_status = 1";
        $waiting_end_rooms = $room_obj->getRowsAssoc($where_room);
        $this->logger->info("end_room",__FILE__,__LINE__,$waiting_end_rooms);
        require_once("classes/dbi/member_room_relation.dbi.php");
        $obj_MemberRoomRelation = new MemberRoomRelationTable($dsn);
        foreach ($waiting_end_rooms as $room) {
            $room_key     = addslashes($room["room_key"]);
            $where_delete = "room_key = '".$room_key."'".
                " AND room_status = 1";
            $data_delete = array(
                "room_deletetime" => date("Y-m-d H:i:s"),
                "room_updatetime" => date("Y-m-d H:i:s"),
                "room_status"     => "0"
            );
            $room_obj->update($data_delete, $where_delete);
            //メンバー部屋リレーション削除
            if($obj_MemberRoomRelation->numRows(sprintf("room_key='%s'", $room_key))) {
              $obj_MemberRoomRelation->remove(sprintf("room_key='%s'", $room_key));
            }

            $use_where = "user_key='".addslashes($room["user_key"])."'";
            $user_data = $user_obj->getRow($use_where);
            if($user_data["account_model"] == ""){
                //プランステータス変更
                $plan_db = new N2MY_DB($dsn, "room_plan");
                $plan_where = "room_key='".$room_key."'".
                        " AND room_plan_status = 1";

                //現在のプランを取得
                $now_plan = $plan_db->getRow($plan_where);
                if($now_plan){
                    $service_db = new N2MY_DB($this->account_dsn, "service");
                    $service_info = $service_db->getRow("service_key = ".$now_plan["service_key"]);
                    if($user_data["max_storage_size"] > $service_info["add_storage_size"]){
                        $update_data = array(
                                "max_storage_size" => $user_data["max_storage_size"] - $service_info["add_storage_size"],
                        );
                    }else{
                        $update_data = array(
                                "max_storage_size" => 0.
                        );
                    }
                    $user_obj->update($update_data , $use_where);
                }
            }

        }
    }

    /**
    * オプション停止
    */
    function _room_option_delete($dsn, $room_key, $service_option_key, $now_date, $expire = false) {
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable($dsn);
        if ($expire) {
            $where = "ordered_service_option_expiredatetime <= '$now_date 00:00:00'".
                     " AND ordered_service_option_expiredatetime != '0000-00-00 00:00:00'".
                     " AND ordered_service_option_status = 1";
            $waiting_end_service_options = $ordered_service_option->getRowsAssoc($where);
            $this->logger->info("end_service_option",__FILE__,__LINE__,$waiting_end_service_options);
            foreach ($waiting_end_service_options as $service_option) {
                $ordered_service_option_key = addslashes($service_option["ordered_service_option_key"]);
                $where_delete = "ordered_service_option_key = '".$ordered_service_option_key."'".
                                " AND ordered_service_option_status = 1";
                $data_delete = array(
                    "ordered_service_option_status"     => "0",
                    "ordered_service_option_deletetime" => date("Y-m-d H:i:s")
                );
                $ordered_service_option->update($data_delete, $where_delete);

                if($service_option["service_option_key"] == 24) {
                    $where = "room_key = '".addslashes($service_option["room_key"])."'".
                    " AND ordered_service_option_status != 0".
                    " AND service_option_key = 24";

                    // ビデオ会議の設定を削除
                    if($ordered_service_option->numRows($where) <= 0) {
                        $this->deleteConference($dsn, $service_option["room_key"]);
                    }
                }
            }
            return true;
        }
        $where = "room_key = '".addslashes($room_key)."'".
                 " AND service_option_key = ".addslashes($service_option_key).
                 " AND ordered_service_option_status = 1";
        $this->logger->trace("where",__FILE__,__LINE__,$where);
        $data = array(
            "ordered_service_option_status"     => 0,
            "ordered_service_option_deletetime" => date("Y-m-d 00:00:00"),
        );
        $ordered_service_option->update($data, $where);

        return true;
    }

    /**
     * DataDBのDSN一覧取得
     *
     */
    function _get_dsn()
    {
        static $server_list;
        if (!$server_list) {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        }
        return $server_list["SERVER_LIST"];
    }

    /*
     * 操作ログ登録
     */
    function add_operation_log($operation_data) {
        $operation_db = new N2MY_DB($this->account_dsn, "operation_log");
        $operation_data["create_datetime"] = date("Y-m-d H:i:s");
        $add_data = $operation_db->add($operation_data);
        $this->logger->debug("operation_log",__FILE__,__LINE__,$add_data);
        return true;
    }

    private function deleteConference($dsn, $room_key) {
        require_once("classes/dbi/ives_setting.dbi.php");
        $ivesSettingTable = new IvesSettingTable($this->get_dsn());
        $where = "room_key = '" . addslashes($room_key) . "' AND is_deleted = 0";
        $ives_setting = $ivesSettingTable->getRow($where);
        $this->logger2->info($ives_setting);
        if(DB::isError($ives_setting) || empty($ives_setting)) {
            $this->logger2->warn("No active Ives setting found for $room_key.");
        }
        else {
            if(!$ives_setting["use_active_speaker"]) {
                $this->resetMaxSeatAmount($room_key);
            }
            $this->removeIvesConference($room_key);
        }

        return true;
    }

    private function removeIvesConference($roomKey) {
        try {
            require_once("classes/dbi/ives_setting.dbi.php");
            require_once("classes/dbi/video_conference.dbi.php");
            require_once("classes/polycom/Polycom.class.php");
            require_once("classes/N2MY_IvesSipClient.class.php");
            $dsn = $this->get_dsn();
            //$ivesClient       = new N2MY_IvesClient($dsn);
            $ivesSettingTable = new IvesSettingTable($dsn);
            $video            = new VideoConferenceTable($dsn);
            $polycom          = new PolycomClass($dsn);


            $res = $ivesSettingTable->findByRoomKey($roomKey);
            if (PEAR::isError($res) || empty($res[0]["ives_did"])) {
                throw new Exception("select ives_setting room_key failed PEAR said : ".$roomKey);
            }

            $ivesSettingList = $res[0];
            $did          = $ivesSettingList["ives_did"];
            $confId       = $ivesSettingList["ives_conference_id"];
            $where = "room_key = '".$roomKey."'";
            $data  = array(
                "is_deleted" => 1
            );
            $res = $ivesSettingTable->update($data, $where);

            if (PEAR::isError($res)) {
                throw new Exception("ives setting update room failed : ".$roomKey);
            }

            // remove adhocConference template.
            $mcu_db = new McuServerTable($this->get_auth_dsn());
            $mcu_host = $mcu_db->getActiveServerAddress($ivesSettingList["mcu_server_key"]);
            $polycom->setWsdlDomain($mcu_host);
            $polycom->removeAdhocConferenceTemplate($did);

            // remove from video table.
            $video->removeAllByRoomKey($roomKey);

            //sip account 削除
            require_once("classes/mcu/config/McuConfigProxy.php");
            $configProxy 	= new McuConfigProxy();
            $ignore_sip_flg = $configProxy->get("ignore_create_sip_account");
            if (!$ignore_sip_flg) {
                // (201603)ここでエラーになるのでignore_create_sip_accountが解除された場合は要注意
                $ivesSipClient    = new N2MY_IvesSipClient($dsn);
                $ivesSipClient->deleteSubscriber($ivesSettingList["sip_uid"]);
            }
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
            exit;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            exit;
        }
    }

    /**
     * Reset the max number of seats based on the current plan where the room engages.
     */
    private function resetMaxSeatAmount($room_key) {
        $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
        $where = "room_key='".$room_key."'" . " AND room_plan_status = 1";
        //現在のプランを取得
        $now_plan = $room_plan_db->getRow($where);
        $objService = new N2MY_DB( $this->account_dsn, "service" );
        $where = sprintf( "service_key=%s", $now_plan["service_key"] );
        $now_plan_info = $objService->getRow( $where );
        $max_seat = $now_plan_info["max_seat"] ? $now_plan_info["max_seat"] : 10;
        $this->logger2->info($now_plan_info);
        $room_db = new N2MY_DB($this->get_dsn(), "room");
        $where_room = "room_key = '".addslashes($room_key)."'";
        $seat_data = array("max_seat" => $max_seat);
        $room_data = $room_db->update($seat_data, $where_room);
    }

    function sendReport($mail_from, $mail_to, $mail_subject, $mail_body) {
        require_once("lib/EZLib/EZMail/EZSmtp.class.php");
        // 入力チェック
        if (!$mail_from || !$mail_to || !$mail_subject || !$mail_body) {
            return false;
        }
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang = EZLanguage::getLangCd("ja");
        $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
        $obj_SendMail->setFrom($mail_from);
        $obj_SendMail->setReturnPath($mail_from);
        $obj_SendMail->setTo($mail_to);
        $obj_SendMail->setSubject($mail_subject);
        $obj_SendMail->setBody($mail_body);
        // メール送信
        $result = $obj_SendMail->send();
        $this->logger2->info(array($mail_from, $mail_to, $mail_subject, $mail_body));
    }
}
$main = new AppDailyOperation();
$main->execute();