<?php

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");

class AppSetUserKey extends AppFrame {

	function init() {
        $this->_name_space = md5(__FILE__);
        $this->_sess_page = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
	}

	function default_view() {
        $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        print "<pre>";
        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            $meeting_db = new N2MY_DB($dsn,"meeting");
            $room_db = new N2MY_DB($dsn,"room");
            $user_db = new N2MY_DB($dsn,"user");
            $where = "user_key = 0";
			$meeting_list = $meeting_db->getRowsAssoc($where, null, null, null, "user_key, meeting_key, room_key");
//			print_r($meeting_list);
			foreach($meeting_list as $meeting) {
				$where = "room_key = '".addslashes($meeting["room_key"])."'";
				$room_info = $room_db->getRow($where);
				print_r(array($meeting, $room_info["room_key"], $room_info["user_key"]));
			}
        }
        print '<a href="?action_exec=">Execute</a>';
	}

	function action_exec() {
        $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        print "<pre>";
        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            $meeting_db = new N2MY_DB($dsn,"meeting");
            $room_db = new N2MY_DB($dsn,"room");
            $where = "user_key = 0";
			$meeting_list = $meeting_db->getRowsAssoc($where, null, null, null, "user_key, meeting_key, room_key");
			foreach($meeting_list as $meeting) {
				$where = "room_key = '".addslashes($meeting["room_key"])."'";
				$room_info = $room_db->getRow($where);
				$where = "meeting_key = ".$meeting["meeting_key"];
				$data = array("user_key" => $room_info["user_key"]);
				$this->logger2->info(array($meeting["room_key"], $room_info["room_key"], $where, $data));
				print_r(array($meeting, $room_info["room_key"], $room_info["user_key"]));
				$meeting_db->update($data, $where);
			}
        }
	}
}

$main = new AppSetUserKey();
$main->execute();
?>