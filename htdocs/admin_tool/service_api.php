<?php
header("Content-type: text/html; charset=UTF-8;");
require_once("../../config/config.inc.php");
require_once("../../config/service_api_config.php");
require_once("dBug.php");
$sess_id = "N2MY_SESSION";


$host = "http://".$_SERVER["SERVER_NAME"];
$api = new API_Checker($host);
$api->_get_passage_time();

/**
 * アカウント管理関連
 */

print <<<EOM
<style type="text/css">
        body {
          font-family:Verdana, Arial, Helvetica, sans-serif; color:#000000; font-size:12px;
        }
</style>
EOM;
print "<h2>APIテストケース検証ツール</h2>";
print "<p>実行結果をクリックすることで、リクエスト・レスポンスの内容を参照できます</p>";

/*
$url = 'http://meeting-trunk.kiyomizu.mac/api/user/meetinglog/?action_get_detail=&N2MY_SESSION=4075e4fae24fb26c92757d70ace430a5&meeting_session_id=1d6fb55c61e77e5fe7c81dade9e35fab&output_type=php';
print $url;
print_r (unserialize(file_get_contents($url)));
die();
*/

print "<ol>";

/*******************************************
 * ユーザー用API
 *******************************************/

$api->add_log("*ユーザー用API\n\n");

/**************************
 * ユーザ認証関連
 **************************/

$api_path = "api/user/";
$api->add_log("**ユーザ認証関連 $api_path \n\n");

///*

    // ログイン
    $method_name  = 'action_login';
    $method_title = 'ログイン';
    $method_info  = 'ログイン処理を行いセッションキーを返す。以降のAPIは全てセッションキーを渡すことで実行可能となる。';
    $param = array(
        "id"  => $user_id,
        "pw"  => $user_pw,
        "lang" => "ja",
        "country" => "jp",
        "time_zone" => "9",
        "enc" => "",
        "output_type" => "php"
        );
    $param_property = array(
        "id"  => array(
            'name'        => 'ユーザーID',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ユーザーログイン用のID',
            ),
        "pw"  => array(
            'name'        => 'パスワード',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ユーザーログイン用のパスワード',
            ),
        );
    $_user_info = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    $session = $_user_info["data"]["session"];
    $user_info = $_user_info["data"]["user_info"];

    // ログインユーザーのパスワード変更
    $method_name  = 'action_change_password';
    $method_title = 'ログインユーザーのパスワード変更';
    $method_info  = 'ログインユーザー（メンバー）のパスワードの変更します。';
    $param = array(
        "passwd" => "pw999999",
        $sess_id => $session,
        );
    $param_property = array(
        "passwd"  => array(
            'name'        => 'パスワード',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '更新用の新ログインパスワード',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    // ログインユーザーのパスワード変更
    $method_name  = 'action_change_password';
    $method_title = 'ログインユーザーのパスワードを元に戻す';
    $method_info  = '';
    $param = array(
        "passwd" => $user_pw,
        $sess_id => $session,
        );
    $param_property = array(
        "passwd"  => array(
            'name'        => 'パスワード',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '更新用の新ログインパスワード',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    // ユーザー情報の更新
    $method_name  = 'action_update';
    $method_title = 'ユーザー情報の更新';
    $method_info  = 'ログインユーザーの情報の更新します。';
    $param = array(
        "passwd" => $member_pw,
        $sess_id => $session,
        );
    $param_property = array(
        "passwd"  => array(
            'name'        => 'パスワード',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '更新用の新ログインパスワード',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    // ログアウト
    $method_name  = 'action_logout';
    $method_title = 'ログアウト';
    $method_info  = 'セッションの破棄し、ログアウトします';
    $param = array(
        $sess_id => $session,
        );
    $param_property = array(
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    // ログイン
    $method_name  = 'action_login';
    $method_title = 'ログイン（MD5パスワード暗号化）';
    $method_info  = '';
    $param = array(
        "id"  => $user_id,
        "pw"  => md5($user_pw),
        "enc" => "md5",
        "output_type" => "php"
        );
    $param_property = array(
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    // ログイン
    $method_name  = 'action_login';
    $method_title = 'ログイン（SHA1パスワード暗号化）';
    $method_info  = '';
    $param = array(
        "id"  => $user_id,
        "pw"  => sha1($user_pw),
        "enc" => "sha1",
        "output_type" => "php"
        );
    $param_property = array(
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    // ログイン
    $method_name  = 'action_login';
    $method_title = 'メンバーログイン';
    $method_info  = '';
    $param = array(
        "id"  => $member_id,
        "pw"  => $member_pw,
        "output_type" => "php"
        );
    $param_property = array(
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    $session = $ret["data"]["session"];

    // ユーザー情報の更新
    $method_name  = 'action_update';
    $method_title = 'メンバー情報の更新';
    $method_info  = '';
    $param = array(
        "email" => $mail_from,
        "name" => "ててててすと".date("Y-m-d H:i:s"),
        "name_kana" => "テテテテスト".date("Y-m-d H:i:s"),
        "timezone" => $timezone,
        "lang" => "ja",
        "country" => "jp",
        $sess_id => $session,
        );
    $param_property = array(
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    // メンバー一覧取得
    $method_name  = 'action_get_member_list';
    $method_title = 'メンバー一覧取得';
    $method_info  = 'ログインユーザーに所属しているメンバー一覧を取得します。';
    $param = array(
        $sess_id => $session,
        );
    $param_property = array(
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $member_list = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    // 部屋一覧取得
    $method_name  = 'action_get_room_list';
    $method_title = '部屋一覧取得';
    $method_info  = 'ログインユーザが利用可能な部屋の一覧を取得します。';
    $param = array(
        $sess_id => $session,
        );
    $param_property = array(
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $room_list = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    list($room_no, $room_info) = each($room_list["data"]["room"]);
    $room_key = $room_info["room_info"]["room_key"];

    // 部屋詳細取得
    $method_name  = 'action_get_room_detail';
    $method_title = '部屋詳細取得';
    $method_info  = '部屋の詳細情報を取得します。';
    $param = array(
        "room_key" => $room_key,
        $sess_id => $session,
        );
    $param_property = array(
        "room_key"  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ログイン中のユーザーが所有する部屋のキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $room_detail = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    // 部屋状態取得（会議開催中のデータを取得）
    $method_name  = 'action_get_room_status';
    $method_title = '部屋状態取得';
    $method_info  = '部屋の利用状況（会議中、参加人数、参加者名など）を取得します。';
    $param = array(
        "room_key" => $room_key,
        $sess_id => $session,
        );
    $param_property = array(
        "room_key"  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ログイン中のユーザーが所有する部屋のキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
//*/

/**************************
 * 会議関連
 **************************/

$api_path = "api/user/meeting/";
$api->add_log("**会議関連 $api_path \n\n");

///*
    // 会議作成
    $method_name  = 'action_create';
    $method_title = '会議作成';
    $method_info  = '会議を作成します。';
    $param = array(
        "room_key" => $room_key,
        $sess_id => $session,
        );
    $param_property = array(
        "room_key"  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ログイン中のユーザーが所有する部屋のキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_start';
    $method_title = '会議開始';
    $method_info  = '指定した部屋の会議に参加します。会議キーの指定がない場合は、現在利用可能な会議に参加されます。';
    $param = array(
        "room_key" => $room_key,
        $sess_id => $session,
        );
    $param_property = array(
        "room_key"  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ログイン中のユーザーが所有する部屋のキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $meeting_status_url = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    print '<a target="_blank" href="'.$meeting_status_url["data"]["url"].'">会議開始</a><br />';
    $meeting_ticket = $meeting_status_url["data"]["meeting_ticket"];

    //
    $method_name  = 'action_knocker';
    $method_title = 'ノッカー';
    $method_info  = "会議中の参加者にメッセージを送信します。&br;（「会議開始」を押して、会議が始まった状態で「結果表示」を押してください。）";
    $param = array(
        "meeting_ticket" => $meeting_ticket,
        "message" => date("Y-m-d H:i:s"),
        $sess_id => $session,
        );
    $param_property = array(
        "meeting_ticket"  => array(
            'name'        => '会議チケット',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '対象の会議を特定するチケット',
            ),
        "message"  => array(
            'name'        => 'ノッカーメッセージ',
            'type'        => 'string(100)',
            'restriction' => '1～100文字以内',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ノッカーで会議室内に表示させる文字列',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_wb_upload';
    $method_title = '資料追加(資料)';
    $method_info  = '会議中のホワイトボードに資料を張り込みます&br;実際に検証する際は、<a href="document.html" target="_blank">こちら</a><br />';
    $param = array(
        "meeting_ticket" => $meeting_ticket,
        "file" => "@".N2MY_APP_DIR."htdocs/shared/images/vcube_logo.gif",
        $sess_id => $session,
        );
    $param_property = array(
        "meeting_ticket"  => array(
            'name'        => '会議チケット',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '対象の会議',
            ),
        "file"  => array(
            'name'        => '張込資料情報',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'Multipart/Form-data形式でポスト',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
/*
    //
    $method_name  = 'action_stop';
    $method_title = '会議終了（部屋を指定して終了）';
    $method_info  = '会議を終了します。';
    $param = array(
        "room_key" => $room_key,
        $sess_id => $session,
        );
    $param_property = array(
        "room_key"  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ログイン中のユーザーが所有する部屋のキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_start';
    $method_title = '会議開始';
    $method_info  = '';
    $param = array(
        "room_key" => $room_key,
        $sess_id => $session,
        );
    $param_property = array(
        "room_key"  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ログイン中のユーザーが所有する部屋のキー',
            ),
        "meeting_ticket"  => array(
            'name'        => '会議チケット',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '対象の会議',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $meeting_status_url = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    $meeting_ticket = $meeting_status_url["data"]["meeting_ticket"];

    //
    $method_name  = 'action_stop';
    $method_title = '会議終了（会議を指定して終了）';
    $method_info  = '';
    $param = array(
        "room_key" => $room_key,
        "meeting_ticket" => $meeting_ticket,
        $sess_id => $session,
        );
    $param_property = array(
        "room_key"  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '対象の会議が開催されている部屋のキー',
            ),
        "meeting_ticket"  => array(
            'name'        => '会議チケット',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '対象の会議',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
*/
//*/

/**************************
 * 予約関連
 **************************/

$api_path = "/api/user/reservation/";
$api->add_log("**予約関連 $api_path \n\n");

///*

    //
    $method_name  = 'action_get_list';
    $method_title = '予約一覧';
    $method_info  = '予約済みの一覧を取得します。';
    $param = array(
        "room_key"    => $room_key,
        "start_limit" => date("Y-m-d H:i:s"),
        "end_limit"   => date("Y-m-d H:i:s", time() + 3600 * 24 * 30),
        "limit"       => 20,
        "offset"      => 0,
        "sort_key"    => "reservation_starttime",
        "sort_type"   => "asc",
        $sess_id => $session,
        );
    $param_property = array(
        "room_key"  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '会議を予約する部屋のキー',
            ),
        "start_limit"  => array(
            'name'        => '開催時刻検索始点',
            'type'        => 'date',
            'restriction' => '日付形式（yyyy-mm-dd hh:mm:ss）',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '対象の会議',
            ),
        "end_limit"  => array(
            'name'        => '会議終了時刻',
            'type'        => 'date', // 型
            'restriction' => '日付形式（yyyy-mm-dd hh:mm:ss）',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '対象の会議',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $result = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    $reservation_list = $result["data"]["reservation_list"];
    $reservation_session = $reservation_list[0]["reservation_session"];

    //
    $method_name  = 'action_get_detail';
    $method_title = '予約内容詳細';
    $method_info  = '予約の詳細情報を取得します。';
    $param = array(
        "reservation_session" => $reservation_session,
        $sess_id => $session,
        );
    $param_property = array(
        "reservation_session"  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $reservation_info = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_add';
    $method_title = '予約追加';
    $method_info  = '予約を追加します。';
    $param = array(
        "room_key"     => $room_key,
        "name"         => date("Y-m-d H:i:s")."[新規]",
        "timezone"     => $timezone,
        "start"        => date("Y-m-d H:i:00", time() + 3600),
        "end"          => date("Y-m-d H:i:10", time() + 3600),
        "password"     => "password",
        "sender_name"  => $sender,
        "sender_email" => $mail_from,
        "info"         => "メール本文",
        $sess_id => $session,
        );
    $param_property = array(
        "room_key"  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議を予約する部屋のキー',
            ),
        "name"  => array(
            'name'        => '会議名',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "timezone" => array( // ！！！パラメータ名に揺らぎあり！！！
            'name'        => 'タイムゾーン',
            'type'        => 'int(64)', // 型
            'restriction' => '',
            'required'    => '',
            'default'     => '9',
            'addition'    => '-11 ～ 0 ～ 14',
            ),
        "start"  => array(
            'name'        => '会議開始時刻',
            'type'        => 'date', // 型
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '日付形式（yyyy-mm-dd hh:mm:ss）',
            ),
        "end"  => array(
            'name'        => '会議終了時刻',
            'type'        => 'date', // 型
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '日付形式（yyyy-mm-dd hh:mm:ss）',
            ),
        "password"  => array( // ！！！パラメータ名に揺らぎあり！！！
            'name'        => '会議パスワード',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "sender_name"  => array(
            'name'        => '送信者（会議主催者）名',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "sender_email"  => array(
            'name'        => '送信者（会議主催者）メールアドレス',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'メールアドレス形式',
            ),
        "info"  => array(
            'name'        => '招待メール本文',
            'type'        => 'string(32768)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $reservation_info = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    $reservation_session = $reservation_info["data"]["reservation_session"];

    //
    $method_name  = 'action_update';
    $method_title = '予約変更';
    $method_info  = '予約内容を変更します。';
    $param = array(
        "reservation_session" => $reservation_session,
        "room_key"     => $room_key,
        "name"         => date("Y-m-d H:i:s")."[更新]",
        "timezone"     => $timezone,
        "start"        => date("Y-m-d H:i:00", time() + 3600),
        "end"          => date("Y-m-d H:i:10", time() + 3600),
        "password"     => "password",
        "sender_name"  => $sender,
        "sender_email" => $mail_from,
        "info"         => "メール本文",
        $sess_id => $session,
        );
    $param_property = array(
        "reservation_session"  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            ),
        "room_key"  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議を予約する部屋のキー',
            ),
        "name"  => array(
            'name'        => '会議名',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "timezone" => array( // ！！！パラメータ名に揺らぎあり！！！
            'name'        => 'タイムゾーン',
            'type'        => 'int(64)',
            'restriction' => '',
            'required'    => '',
            'default'     => '9',
            'addition'    => '-11 ～ 0 ～ 14',
            ),
        "start"  => array(
            'name'        => '会議開始時刻',
            'type'        => 'date',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '日付形式（yyyy-mm-dd hh:mm:ss）',
            ),
        "end"  => array(
            'name'        => '会議終了時刻',
            'type'        => 'date',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '日付形式（yyyy-mm-dd hh:mm:ss）',
            ),
        "password"  => array( // ！！！パラメータ名に揺らぎあり！！！
            'name'        => '会議パスワード',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "sender_name"  => array(
            'name'        => '送信者（会議主催者）名',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "sender_email"  => array(
            'name'        => '送信者（会議主催者）メールアドレス',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'メールアドレス形式',
            ),
        "info"  => array(
            'name'        => '招待メール本文',
            'type'        => 'string(32768)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_add_invite';
    $method_title = '招待者追加（通常）';
    $method_info  = '予約した会議の招待メールを送信します。';
    $param = array(
        "reservation_session" => $reservation_session,
        "name"                => date("Y-m-d H:i:s")."[招待者新規]",
        "email"               => $mail_to,
        "timezone"            => $timezone,
        "lang"                => "ja",
        "type"                => "",
        $sess_id => $session,
        );
    $param_property = array(
        "reservation_session"  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            ),
        "name"  => array(
            'name'        => '招待者名',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "email"  => array(
            'name'        => '招待者メールアドレス',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'メールアドレス形式',
            ),
        "timezone" => array( // ！！！パラメータ名に揺らぎあり！！！
            'name'        => '招待者タイムゾーン',
            'type'        => 'string(16)',
            'restriction' => '',
            'required'    => '',
            'default'     => '9',
            'addition'    => '-11 ～ 0 ～ 14',
            ),
        "lang"  => array(
            'name'        => '招待者言語設定',
            'type'        => 'string(2)',
            'restriction' => '',
            'required'    => '',
            'default'     => 'ja',
            'addition'    => 'ja：日本語、en：英語',
            ),
        "type"  => array(
            'name'        => '招待者種別',
            'type'        => 'string(8)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'NULL : 通常、Audience : オーディエンス',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_add_invite';
    $method_title = '招待者追加1（通常）';
    $method_info  = '';
    $param = array(
        "reservation_session" => $reservation_session,
        "name"                => date("Y-m-d H:i:s")."[招待者新規2]",
        "email"               => $mail_to,
        "timezone"            => "-5",
        "lang"                => "en",
        "type"                => "audience",
        $sess_id => $session,
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info);

    //
    $method_name  = 'action_get_invite';
    $method_title = '招待者一覧2';
    $method_info  = '予約した会議に登録した招待者一覧を取得します。';
    $param = array(
        "reservation_session" => $reservation_session,
        $sess_id => $session,
        );
    $param_property = array(
        "reservation_session"  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $invite_list = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    list($invite_id, $invite_info) = each($invite_list["data"]["invite_list"]["guests"]);

    //
    $method_name  = 'action_delete_invite';
    $method_title = '招待者削除';
    $method_info  = '予約した会議の招待者を削除します。';
    $param = array(
        "reservation_session" => $reservation_session,
        "invite_id" => $invite_info["r_user_session"],
        "lang" => "ja",
        $sess_id => $session,
        );
    $param_property = array(
        "reservation_session"  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            ),
        "invite_id" => array(
            'name'        => '招待ID',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '',
            ),
        "lang"  => array(
            'name'        => '招待者言語設定',
            'type'        => 'string(2)',
            'restriction' => '',
            'required'    => '',
            'default'     => 'ja',
            'addition'    => 'ja：日本語、en：英語',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_get_invite';
    $method_title = '招待者一覧';
    $method_info  = '';
    $param = array(
        "reservation_session" => $reservation_session,
        $sess_id => $session,
        );
    $invite_list = $api->send($api_path, $method_name, $param, $method_title, $method_info);

    //
    $method_name  = 'action_add_document';
    $method_title = '資料追加(画像)';
    $method_info  = '予約した会議に事前資料アップロードを行います。';
    $param = array(
        "reservation_session" => $reservation_session,
        "file" => "@".N2MY_APP_DIR."htdocs/shared/images/vcube_logo.gif",
        "name" => "画像 - ".date("Y-m-d H:i:s"),
        $sess_id => $session,
        );
    $param_property = array(
        "reservation_session"  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            ),
        "file"  => array(
            'name'        => '張込資料情報',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'Multipart/Form-data形式でポスト',
            ),
        "name"  => array(
            'name'        => '張込資料名',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

//    $method_name  = 'action_add_document';
//    $method_title = '資料追加(ドキュメント)';
//    $method_info  = '';
//    $param = array(
//        "reservation_session" => $reservation_session,
//        "file" => "@".N2MY_APP_DIR."htdocs/ja/shared/PDF/Manual_Audience.pdf",
//        "name" => "資料 - ".date("Y-m-d H:i:s"),
//        $sess_id => $session,
//        );
//    $api->send($api_path, $method_name, $param, $method_title, $method_info);

    //
    $method_name  = 'action_get_document';
    $method_title = '資料一覧取得';
    $method_info  = '予約した会議の事前資料一覧を取得します。';
    $param = array(
        "reservation_session" => $reservation_session,
        $sess_id => $session,
        );
    $param_property = array(
        "reservation_session"  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $document_list = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    list($a, $document) = each($document_list["data"]["document_list"]);

    //
    $method_name  = 'action_update_document';
    $method_title = '資料名変更';
    $method_info  = '予約した会議の資料名を変更します。';
    $param = array(
        "document_id" => $document["document_id"],
        "name" => "名前変更 - ".date("Y-m-d"),
        $sess_id => $session,
        );
    $param_property = array(
        "document_id"  => array(
            'name'        => '資料ID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '事前アップロードした資料ID',
            ),
        "name"  => array(
            'name'        => '資料名',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_delete_document';
    $method_title = '資料削除';
    $method_info  = '予約した会議の資料を削除します。';
    $param = array(
        "document_id" => $document["document_id"],
        $sess_id => $session,
        );
    $param_property = array(
        "reservation_session"  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_get_document';
    $method_title = '資料一覧取得';
    $method_info  = '';
    $param = array(
        "reservation_session" => $reservation_session,
        $sess_id => $session,
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info);

    //
//    $method_name  = 'action_sort';
//    $method_title = '資料の表示順変更';
//    $method_info  = '予約した会議の資料表示順を変更します。';
//    $param = array(
//        $sess_id => $session,
//        );
//    $api->send($api_path, $method_name, $param, $method_title, $method_info);

    //
    $method_name  = 'action_delete';
    $method_title = '予約削除';
    $method_info  = '予約を取り消します。';
    $param = array(
        "reservation_session" => $reservation_session,
        $sess_id => $session,
        );
    $param_property = array(
        "reservation_session"  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

//*/

/**************************
 * 会議記録関連
 **************************/

$api_path = "api/user/meetinglog/";
$api->add_log("**会議記録関連 $api_path \n\n");

///*

    //
    $method_name  = 'action_get_list';
    $method_title = '会議記録一覧';
    $method_info  = '会議記録一覧を取得します。';
    $param = array(
        "room_key"  => $room_key,
        "meeting_name" => "",
        "user_name" => "",
        "start_limit" => "",
        "end_limit" => "",
        "offset" => "",
        "limit" => "",
        "sort_key" => "",
        "sort_type" => "",
        "output_type" => "",
        $sess_id    => $session,
        );
    $param_property = array(
        "room_key"  => array(
            'name'        => '部屋ID',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "meeting_name"  => array(
            'name'        => '会議名',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "user_name"  => array(
            'name'        => '参加者名',
            'type'        => '',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "start_limit"  => array(
            'name'        => '開始日時',
            'type'        => '',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "end_limit"  => array(
            'name'        => '終了日時',
            'type'        => '',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "offset"  => array(
            'name'        => '開始行',
            'type'        => '',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        "limit"  => array(
            'name'        => '取得件数',
            'type'        => 'int',
            'restriction' => '',
            'required'    => '',
            'default'     => '10',
            'addition'    => '',
            ),
        "sort_key"  => array(
            'name'        => 'ソート項目',
            'type'        => 'string',
            'restriction' => '',
            'required'    => '',
            'default'     => 'actual_start_datetime:実際の会議開始時間',
            'addition'    => '',
            ),
        "sort_type"  => array(
            'name'        => '昇順、降順',
            'type'        => '',
            'restriction' => '',
            'required'    => '',
            'default'     => 'asc:昇順',
            'addition'    => 'asc:昇順, desk:降順',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $meeting_logs = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    list($meeting_key, $meeting_log) = each($meeting_logs["data"]["meetinglog"]);
    $meeting_session_id = $meeting_log["meeting_session_id"];

    //
    $method_name  = 'action_get_detail';
    $method_title = '会議記録詳細';
    $method_info  = '会議記録の詳細情報を取得します。';
    $param = array(
        "meeting_session_id" => $meeting_session_id,
        $sess_id => $session,
        );
    $param_property = array(
        "meeting_session_id"  => array(
            'name'        => '会議セッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    /*
    $method_name  = 'action_play_meeting';
    $method_title = '映像再生';
    $method_info  = '映像を再生します。';
    $param = array(
        "meeting_session_id" => $meeting_session_id,
        $sess_id => $session,
        );
    $param_property = array(
        "meeting_session_id"  => array(
            'name'        => '会議セッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_play_whiteboard';
    $method_title = '議事録再生';
    $method_info  = '議事録を再生します。';
    $param = array(
        "meeting_session_id" => $meeting_session_id,
        $sess_id => $session,
        );
    $param_property = array(
        "meeting_session_id"  => array(
            'name'        => '会議セッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

//*/

/**************************
 * アドレス帳
 **************************/

$api_path = "api/user/addressbook/";
$api->add_log("**アドレス帳関連 $api_path \n\n");

///*

    //
    $method_name  = 'action_add';
    $method_title = 'アドレス帳追加';
    $method_info  = 'アドレス帳に追加します。';
    $param = array(
        "name"       => "name",
        "name_kana"  => "namae",
        "email"      => "sample@example.co.jp",
        "timezone"   => "9",
        "lang"       => "ja",
        $sess_id => $session,
        );
    $param_property = array(
        "name"  => array(
            'name'        => '会議セッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            ),
        "name_kana"  => array(
            'name'        => '会議セッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            ),
        "email"  => array(
            'name'        => '招待者メールアドレス',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'メールアドレス形式',
            ),
        "timezone" => array( // ！！！パラメータ名に揺らぎあり！！！
            'name'        => '招待者タイムゾーン',
            'type'        => 'string(16)',
            'restriction' => '',
            'required'    => '',
            'default'     => '9',
            'addition'    => '-11 ～ 0 ～ 14',
            ),
        "lang"  => array(
            'name'        => '招待者言語設定',
            'type'        => 'string(2)',
            'restriction' => '',
            'required'    => '',
            'default'     => 'ja',
            'addition'    => 'ja：日本語、en：英語',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_get_list';
    $method_title = 'アドレス帳一覧';
    $method_info  = 'アドレス帳一覧を取得します';
    $param = array(
        "sort_key"    => "name",
        "sort_type"   => "down",
        "page"        => 1,
        "limit"       => 20,
        "keyword"     => "",
        $sess_id      => $session,
        );
    $param_property = array(
        "page"  => array(
            'name'        => 'アドレス帳初期表示ページ',
            'type'        => 'int(10)',
            'restriction' => 'numeric',
            'required'    => '',
            'default'     => '1',
            'addition'    => '',
            ),
        "keyword"  => array(
            'name'        => '検索キーワード',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $address_list = $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);
    list($key, $address_info) = each($address_list["data"]["address_list"]);

    //
    $method_name  = 'action_update';
    $method_title = 'アドレス帳更新';
    $method_info  = 'アドレス帳の情報を更新します。';
    $param = array(
        "address_key"   => $address_info[0]["address_book_key"],
        "name"          => "name",
        "name_kana"     => "namae",
        "email"         => "sample@example.co.jp",
        "timezone"      => "9",
        "lang"          => "ja",
        $sess_id        => $session,
        );
    $param_property = array(
        "address_key"  => array(
            'name'        => 'アドレス帳キー',
            'type'        => 'int(20)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            ),
        "name"  => array(
            'name'        => '会議セッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            ),
        "name_kana"  => array(
            'name'        => '会議セッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            ),
        "email"  => array(
            'name'        => '招待者メールアドレス',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'メールアドレス形式',
            ),
        "timezone" => array( // ！！！パラメータ名に揺らぎあり！！！
            'name'        => '招待者タイムゾーン',
            'type'        => 'string(16)',
            'restriction' => '',
            'required'    => '',
            'default'     => '9',
            'addition'    => '-11 ～ 0 ～ 14',
            ),
        "lang"  => array(
            'name'        => '招待者言語設定',
            'type'        => 'string(2)',
            'restriction' => '',
            'required'    => '',
            'default'     => 'ja',
            'addition'    => 'ja：日本語、en：英語',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_delete';
    $method_title = 'アドレス帳削除';
    $method_info  = 'アドレス帳から削除します';
    $param = array(
        "address_key"   => $address_info["address_book_key"],
        $sess_id => $session,
        );
    $param_property = array(
        "address_key"  => array(
            'name'        => 'アドレス帳キー',
            'type'        => 'int(20)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
//    $method_name  = 'action_get_member';
//    $method_title = 'メンバー一覧';
//    $method_info  = '';
//    $param = array(
//        $sess_id => $session,
//        );
//    $api->send($api_path, $method_name, $param, $method_title, $method_info);


//*/

/**************************
 * ECOメーター
 **************************/

$api_path = "api/user/eco/";
$api->add_log("**エコメーター関連 $api_path \n\n");

///*

    //
    $method_name  = 'action_get_station_list';
    $method_title = '駅名検索';
    $method_info  = '駅名を検索します。';
    $param = array(
        "station" => "あ",
        $sess_id => $session,
        );
    $param_property = array(
        "station"  => array(
            'name'        => '参加者最寄り駅',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '駅名（広尾、中目黒、新大阪）',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

    //
    $method_name  = 'action_get_transit';
    $method_title = '経路情報';
    $method_info  = '指定した拠点（駅）の最短ルート情報を取得します。';
    $param = array(
        "station_list" => "中目黒,広尾,目黒",
        "end_place" => "箱根湯本",
        "sort" => "time",
        $sess_id => $session,
        );
    $param_property = array(
        "station_list"  => array(
            'name'        => '参加者最寄り駅',
            'type'        => 'string(65535)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '駅名（広尾、中目黒、新大阪）',
            ),
        "end_place"  => array(
            'name'        => '目的地',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '駅名（中目黒）',
            ),
        $sess_id  => array(
            'name'        => 'ミーティングセッションID',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'API認証用セッションID',
            ),
        );
    $api->send($api_path, $method_name, $param, $method_title, $method_info, $param_property);

//*/

/*******************************************
 * 管理者用API
 *******************************************/

$api->add_log("*管理者用API\n\n");

/*
    // メンバー追加
    $method_name  = 'action_add_member';
    $method_title = 'メンバー追加';
    $method_info  = '';
    $param = array(
        $sess_id => $session,
        "user_id"               => "test_user06",
        "member_id"             => "user06_member01",
        "member_password"       => "user06_member01",
        "member_name"           => "06メンバー01",
        "member_group"          => "",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // メンバー情報修正
    $method_name  = 'action_update_member';
    $method_title = 'メンバー情報修正';
    $method_info  = '';
    $param = array(
        $sess_id => $session,
        "member_id"       => "user06_member01",
        "member_password"       => "user06_member01",
        "member_name"           => "06メンバー01変更",
        "member_group"          => "",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // メンバーアカウント停止
    $method_name  = 'action_delete_member';
    $method_title = 'メンバーアカウント停止';
    $method_info  = '';
    $param = array(
        $sess_id => $session,
        "member_id"             => "test_member01",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // メンバーグループ追加
    $method_name  = 'action_add_member_group';
    $method_title = 'メンバーグループ追加';
    $method_info  = '';
    $param = array(
        $sess_id => $session,
        "user_id"       => "test_user06",
        "group_name"    => "グループ06テスト02",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // メンバーグループ名修正
    $method_name  = 'action_update_member_group';
    $method_title = 'メンバーグループ名修正';
    $method_info  = '';
    $param = array(
        "" => "",
        $sess_id => $session,
        "user_id"          => "test_user06",
        "group_name"    => "グループテスト改",
        "member_group_key" => $ret_gp["data"]["group"][0]["member_group_key"],
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // メンバーグループ取得
    $method_name  = 'action_get_member_group';
    $method_title = 'メンバーグループ取得';
    $method_info  = '';
    $param = array(
        $sess_id => $session,
        "user_id"       => "test_user06",
        );
    $ret_gp = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // メンバーグループ削除
    $method_name  = 'action_delete_member_group';
    $method_title = 'メンバーグループ削除';
    $method_info  = '';
    $param = array(
        $sess_id => $session,
        "user_id"          => "test_user02",
        "member_group_key" => $ret_gp["data"]["group"][0]["member_group_key"],
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // パスワード変更
    $method_name  = 'action_set_meetinglog_password';
    $method_title = 'パスワード変更';
    $param = array(
        $sess_id => $session,
        "room_key" => "vuser01",
        "meeting_session_id" => $ret_log["data"]["meeting"][0]["meeting_session_id"],
        "meetinglog_password" => "123456",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // パスワード削除
    $method_name  = 'action_unset_meetinglog_password';
    $method_title = 'パスワード削除';
    $param = array(
        $sess_id => $session,
        "room_key" => "vuser01",
        "meeting_session_id" => $ret_log["data"]["meeting"][0]["meeting_session_id"],
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // ログを保護
    $method_name  = 'action_set_meetinglog_protect';
    $method_title = 'ログを保護';
    $param = array(
        $sess_id => $session,
        "room_key" => "vuser01",
        "meeting_session_id" => $ret_log["data"]["meeting"][0]["meeting_session_id"],
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // ログの保護を解除
    $method_name  = 'action_unset_meetinglog_protect';
    $method_title = 'ログの保護を解除';
    $param = array(
        $sess_id => $session,
        "room_key" => "vuser01",
        "meeting_session_id" => $ret_log["data"]["meeting"][0]["meeting_session_id"],
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // 議事録（ログ自体）削除
    $method_name  = 'action_delete_meetinglog';
    $method_title = '議事録（ログ自体）削除';
    $param = array(
        $sess_id => $session,
        "room_key" => "vuser01",
        "meeting_session_id" => $ret_log["data"]["meeting"][0]["meeting_session_id"],
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // 議事録一覧
    $method_name  = 'action_get_meetinglog_list';
    $method_title = '議事録一覧';
    $param = array(
        $sess_id => $session,
        "room_key" => "vuser01",
        );
    $ret_log = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

*/

/*******************************************
 * サービス用API
 *******************************************/

$api->add_log("*アカウント管理用API\n\n");

/*

    // ユーザーアカウント追加
    $method_name  = 'action_add_user';
    $method_title = 'ユーザーアカウント追加';
    $param = array(
        $sess_id => $session,
        "user_id"             => "test_user06",
        "user_password"       => "test_user00",
        "user_admin_password" => "test_user00",
        "user_company_name"   => "テスト会社",
        "reseller_key"        => "1",
        "country_key"         => "1",
        "user_starttime"      => date("Y-m-d H:i:s"),
        "user_registtime"     => date("Y-m-d H:i:s"),
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // ユーザーアカウント情報変更
    $method_name  = 'action_update_user';
    $method_title = 'ユーザーアカウント情報変更';
    $param = array(
        $sess_id => $session,
        "user_id"             => "test_user06",
        "user_company_name"   => "会社名変更",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // ユーザーアカウント停止
    $method_name  = 'action_delete_user';
    $method_title = 'ユーザーアカウント停止';
    $param = array(
        $sess_id   => $session,
        "user_id"  => "test_user01",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // ユーザーログインパスワードのみ変更
    $method_name  = 'action_update_user_loginpassword';
    $method_title = 'ユーザーログインパスワードのみ変更';
    $param = array(
        $sess_id => $session,
        "user_id"                   => "test_user06",
        "user_password"             => "test_user06",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // ユーザーアドミンパスワードのみ変更
    $method_name  = 'action_update_user_adminpassword';
    $method_title = 'ユーザーアドミンパスワードのみ変更';
    $param = array(
        $sess_id => $session,
        "user_id"                   => "test_user06",
        "user_admin_password"       => "test_user06",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

    // ユーザー詳細
    $method_name  = 'action_get_user_detail';
    $method_title = 'ユーザー詳細';
    $param = array(
        $sess_id => $session,
        "user_id" => "test_user06",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";


    // 部屋名変更
    $method_name  = 'action_update_room';
    $method_title = '部屋名変更';
    $param = array(
        $sess_id => $session,
        "room_key"  => "vuser01",
        "room_name" => "部屋名変更2",
        );
    $ret = $api->send($api_path, $method_name, $param, $method_title, $method_info);
    print $api->_get_passage_time()."<br><br>";

//*/

$api->outputLog();

class API_Checker {

    var $url = "";
    var $log = "";
    var $method_list = "";

    function API_Checker($host) {
        $this->method_list = array();
        $this->base_url = $host;
    }

    function send($path, $method, $param, $title = "", $info = "", $param_proparty=array()) {

        // ミーティング共通パラメータ
        $param_proparty["lang"] = array(
            'name'        => '言語選択',
            'type'        => 'string(2)',
            'restriction' => '',
            'required'    => '',
            'default'     => 'ja',
            'addition'    => 'ja：日本語、en：英語',
            );
        $param_proparty["country"] = array(
            'name'        => '現在の所在地',
            'type'        => 'string(32)',
            'restriction' => '',
            'required'    => '',
            'default'     => 'jp',
            'addition'    => 'jp：日本、us：アメリカ（西）、us2：アメリカ（東）、cn：中国、sg：シンガポール、etc：その他、jp2：--',
            );
        $param_proparty["time_zone"] = array(
            'name'        => 'タイムゾーン',
            'type'        => 'string(16)',
            'restriction' => '',
            'required'    => '',
            'default'     => '9',
            'addition'    => '-11 ～ 0 ～ 14',
            );
        $param_proparty["enc"] = array(
            'name'        => 'API出力結果のエンコード',
            'type'        => 'string(64)', //･･･ここは適当
            'restriction' => '',
            'required'    => '',
            'default'     => 'null',
            'addition'    => 'null：エンコードなし, md5：MD5で暗号化, sha1：SHA1で暗号化',
            );
        $param_proparty["output_type"] = array(
            'name'        => 'API出力形式',
            'type'        => 'string(64)', //･･･ここは適当
            'restriction' => '',
            'required'    => '',
            'default'     => 'xml',
            'addition'    => 'xml：XML形式、json：JSON形式、php：PHPシリアライズ、yaml：YAML',
            );

        // 検索オプション共通パラメータ
        $param_proparty["limit"] = array(
            'name'        => '検索データ取得行数',
            'type'        => 'int(10)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'レコード取得制限件数',
            );
        $param_proparty["offset"] = array(
            'name'        => '検索データ取得開始行数',
            'type'        => 'int(10)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'レコード取得開始位置',
            );
        $param_proparty["sort_key"] = array(
            'name'        => '検索データソートキー',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'ソート対象の項目ID',
            );
        $param_proparty["sort_type"] = array(
            'name'        => '検索データソート順',
            'type'        => 'string(4)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'asc : 昇順、desc : 降順',
            );

        $param[$method] = "";
        $start = $this->_get_passage_time();
        $url = $this->base_url."/".$path;
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $param,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 20,
            );
        curl_setopt_array($ch, $option);
        $contents = $ret = curl_exec($ch);
        $time = $this->_get_passage_time() - $start;
        $ret = unserialize($contents);

        $query_str = "";
        $param["output_type"] = "xml";
        foreach ($param as $_key => $_value) {
            $post_data[$_key] .= $_value;
            $query_str .= "&" . $_key . "=" . urlencode($_value);
        }
        if ($query_str) {
            $query_str = "?" . substr($query_str, 1);
        }
        print "<li>".$title."<br/><font color=993322>".$method."</font>";
        print ' ( <a href="'.$url.$query_str.'" target="_blank">結果表示</a> )';
        if ($info) {
            print "<br/><font color='gray'>".$info."</font>";
        }
        if (!$ret) {
            print "<br /><font color='red'>実行エラー</font>";
        } elseif ($ret["status"] != "1") {
            print "<br /><font color='red'>パラメタエラー</font>";
        }

        $parse_url = parse_url($url);
        $parse_url["query"] = $post_data;

        require_once("lib/pear/XML/Serializer.php");
        $serializer = new XML_Serializer();
        $serializer->setOption('mode','simplexml');
        $serializer->setOption('encoding','UTF-8');
        $serializer->setOption('addDecl','ture');
        $serializer->setOption('rootName','result');
        $serializer->serialize($ret);
        $output = $serializer->getSerializedData();

        /**
         * pukiwiki用のコードをログにはく
         */
        $target = "z".substr(md5($path.$method), 0, 7);
        if (!in_array($target, $this->method_list)) {
            $this->method_list[] = $target;
            $this->log .= <<<EOM
*** $title  ( $method ) [#$target]
- 概要
 $info

- メソッド ( GET / POST )
 $method

- パラメタ
|引数|名称|型|制約|必須|デフォルト値|備考|例|h

EOM;
            foreach($post_data as $key => $val) {
                $_name = $param_proparty[$key]['name'];
                $_type = $param_proparty[$key]['type'];
                $_restriction = $param_proparty[$key]['restriction'];
                $_require = $param_proparty[$key]['required'];
                $_default = $param_proparty[$key]['default'];
                $_addition = $param_proparty[$key]['addition'];
                $this->log .= "|$key|$_name|$_type|$_restriction|$_require|$_default|$_addition|$val|"."\n";
            }
            $this->log .= "\n";
            $this->log .= "- サンプルレスポンス"."\n";
            $this->log .= " ".str_replace("\n", "\n ", $output)."\n";
            $this->log .= "\n";
            $this->log .= "- エラーコード表"."\n";
            $error_list = array("1" => "method not found");
            foreach($error_list as $key => $val) {
                $this->log .= ":$key|$val"."\n";
            }
            $this->log .= "----"."\n\n";
            $this->log .= "\n";
        }

        $result = array("URL" => $parse_url, "結果解析" => $ret, "処理時間" => $time. " / ". $this->_get_passage_time());
        if ($_REQUEST["detail"]) {
            new dBug(array("result" => $result));
        } else {
            new dBug(array("result" => $result), "", true);
        }
        return $ret;
    }

    function add_log($msg) {
        $this->log .= $msg;
    }

    /**
     *
     */
    function outputLog() {
        file_put_contents("../../logs/service_api.log", $this->log);
    }

    /**
     * 経過時間取得
     */
    function _get_passage_time($reset = false) {
        static $start_time;
        if (!isset($start_time) || $reset == true) {
            list($micro, $sec) = split(" ", microtime());
            $start_time = $micro + $sec - 0.00001;
        }
        list($micro, $sec) = split(" ", microtime());
        $passage_time = ($micro + $sec) - $start_time;
        return $passage_time;
    }
}
