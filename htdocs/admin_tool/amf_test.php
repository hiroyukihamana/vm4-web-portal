<?php
require_once("classes/AppFrame.class.php");
require_once("classes/amf/AMFTester.php");
require_once("lib/EZLib/EZCore/EZLogger2.class.php");
require_once("lib/EZLib/EZXML/EZXML.class.php");
require_once("dBug.php");

class AMF_Test extends AppFrame {

    var $amftest = null;
    var $participant_list = array();
    var $meeting_sequence_key = null;
    var $logger = "";
    var $dsn = "";
    var $server_dsn_key = "";
    var $room_info = "";
    var $options = "";

    function init() {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->server_dsn_key = N2MY_DEFAULT_DB;
    }

    function default_view() {
        $this->display("admin_tool/amf_test.t.html");
    }

    function action_start() {
        //データ取得
        $user_id = $this->request->get("user_id");
        $user_pw = $this->request->get("user_pw");
        // ログイン
        $param = array(
            'id'            => $user_id,
            'pw'            => $user_pw,
            'lang'          => 'ja',
            'country'       => 'jp',
            'timezone'      => '9',
            'output_type'   => 'php'
            );
        if (!$login_info = $this->sendApi('user/', 'action_login', $param)) {
            print 'Login error';
            exit();
        }
        $this->logger2->info($login_info);
        $token = $login_info['data']['session'];
        if (!$room_list = $this->sendApi('user/', 'action_get_room_list', array(), $token)) {
            print 'Room not exists';
            exit();
        }
        $room_info = $room_list['data']['rooms']['room'][0];
        $room_id = $room_info['room_info']['room_id'];
        $this->logger2->info($room_info);
        // 会議作成
        $param = array(
            'room_id'       => $room_id,
            'meeting_id'    => '',
            'type'          => 'normal',
            'name'          => 'test',
            'is_narrow'     => '9',
            'flash_version' => 'as3'
            );
        if (!$meeting_info = $this->sendApi('user/meeting/', 'action_start', $param, $token)) {
            print 'Failed create meeting';
            exit();
        }
        $this->logger2->info($meeting_info);
        $meeting_id = $meeting_info['data']['meeting_id'];
        $participant_key = $meeting_info['data']['participant_key'];
        $meeting_sequence_key = $meeting_info['data']['meeting_sequence_key'];
        // AMFテスト
        $this->amftest = new AMFTester();
        print "<ul>";

        if (!$ret = $this->amftest->calling("getMeetingKey",
            $meeting_id
            )) {
            print 'Get meeting_key error';
        }
        new dBug($ret, "");
        if ($ret['status'] == 0) {
            print 'Get meeting_key error';
        }
        $meeting_key = $ret['meeting_key'];
        // 一人目が参加
        print "<li>参加者の情報を更新</li>";
        $ret = $this->amftest->calling("updateParticipant",
            $this->server_dsn_key,
            $meeting_key,
            array(
                'action' => 'add',
                'user' => array(
                    'id'        => $participant_key,
                    'ip'        => "219.101.136.124",
                    'name'      => time(),
                    'port'      => 1935,
                    'protocol'  => 'rtmp',
                    'type'      => 'normal'
                    ),
                )
            );
        new dBug($ret, "");
        $ret = $this->amftest->calling("updateParticipant",
            $this->server_dsn_key,
            $meeting_key,
            array(
                'action' => 'add',
                'user' => array(
                    'id'        => 'kiyomizu1',
                    'ip'        => "219.101.136.124",
                    'name'      => time(),
                    'port'      => 1935,
                    'protocol'  => 'rtmp',
                    'type'      => 'h323'
                    ),
                )
            );
        new dBug($ret, "");
        $ret = $this->amftest->calling("updateParticipant",
            $this->server_dsn_key,
            $meeting_key,
            array(
                'action' => 'remove',
                'user' => array(
                    'id'        => $participant_key,
                    'ip'        => "219.101.136.124",
                    'name'      => time(),
                    'port'      => 1935,
                    'protocol'  => 'rtmp',
                    'type'      => 'normal'
                    ),
                )
            );
        new dBug($ret, "");
        $ret = $this->amftest->calling("updateParticipant",
            $this->server_dsn_key,
            $meeting_key,
            array(
                'action' => 'remove',
                'user' => array(
                    'id'        => 'kiyomizu1',
                    'ip'        => "219.101.136.124",
                    'name'      => time(),
                    'port'      => 1935,
                    'protocol'  => 'rtmp',
                    'type'      => 'h323'
                    ),
                )
            );
        new dBug($ret, "");
/*
        // 二人目が参加
        // 実際の会議開始日時を更新
        print "<li>実際の会議開始日時を更新</li>";
        $ret = $this->amftest->calling("startMeeting",
            $this->server_dsn_key,
            $meeting_key
            );
        new dBug($ret, "");

        // 会議の状態と残り時間を取得する
        print "<li>会議の状態と残り時間を取得する</li>";
        $ret = $this->amftest->calling("getMeetingStatus",
            $this->server_dsn_key,
            $meeting_key
            );
        new dBug($ret, "");

        print "<li>課金データ生成</li>";
        $ret = $this->amftest->calling("updateParticipantUptimeList",
            $this->server_dsn_key,
            $meeting_key,
            5,
            date('Y-m-d H:i:s')
            );
        new dBug($ret, "");
        // 参加者数が一致しなかった
        if (!$ret) {
*/
            // DBから無理やり取得して更新する
            $dsn = $this->get_dsn($this->server_dsn_key);
            require_once("classes/core/dbi/Participant.dbi.php");
            $objParticipant = new DBI_Participant($dsn);
            $participant_list[] = array(
                'id'        => 'kiyomizu3',
                'ip'        => '219.101.136.124',
                'name'      => 'kiyomizu hiroyuki',
                'port'      => '1935',
                'protocol'  => 'rtmp',
                'type'      => 'h323'
                );
            print "<li>人数が一致しないので再取得</li>";
            $ret = $this->amftest->calling("updateParticipantList",
                $this->server_dsn_key,
                $meeting_key,
                $participant_list
                );
            new dBug($ret, "");
/*
        }

        // 会議名変更
        print "<li>会議名変更</li>";
        $ret = $this->amftest->calling("alterMeetingName",
            $this->server_dsn_key,
            $meeting_key,
            "テスト会議"
            );
        new dBug($ret, "");


        // 会議の残り時間を取得する
        print "<li>会議の残り時間を取得する</li>";
        $ret = $this->amftest->calling("getRemainingTime",
            $this->server_dsn_key,
            $meeting_key
            );
        new dBug($ret, "");

        // 会議ログにパスワードを設定する
        print "<li>会議ログにパスワードを設定する</li>";
        $ret = $this->amftest->calling("setLogPassword",
            $this->server_dsn_key,
            $meeting_key,
            "",
            "password"
            );
        new dBug($ret, "");

        // パスワード認証に失敗させる
        print "<li>パスワード認証に失敗させる</li>";
        $ret = $this->amftest->calling("authMeetingLog",
            $this->server_dsn_key,
            $meeting_key,
            "password1"
            );
        new dBug($ret, "");

        // パスワードつきログの認証
        print "<li>パスワードつきログの認証</li>";
        $ret = $this->amftest->calling("authMeetingLog",
            $this->server_dsn_key,
            $meeting_key,
            "password"
            );
        new dBug($ret, "");

        // 会議ログのパスワードを解除する
        print "<li>会議ログのパスワードを解除する</li>";
        $ret = $this->amftest->calling("unsetLogPassword",
            $this->server_dsn_key,
            $meeting_key
            );
        new dBug($ret, "");

       // 会議にロックを設定する
        print "<li>会議にロックを設定する</li>";
        $ret = $this->amftest->calling("lockMeeting",
            $this->server_dsn_key,
            $meeting_key
            );
        new dBug($ret, "");

        // 会議のロックを解除する
        print "<li>会議のロックを解除する</li>";
        $ret = $this->amftest->calling("unlockMeeting",
            $this->server_dsn_key,
            $meeting_key
            );
        new dBug($ret, "");

        // 会議室ベースでの録画可能残量を取得する
        print "<li>会議室ベースでの録画可能残量を取得する</li>";
        $ret = $this->amftest->calling("getRemainingRecordableSize",
            $this->server_dsn_key,
            $meeting_key
            );
        new dBug($ret, "");

        // データセンター一覧取得
        print "<li>データセンター一覧取得</li>";
        $datacenters = $this->amftest->calling("getDataCenterList",
            $this->server_dsn_key);
        $i = array_rand($datacenters);
        $datacenter = $datacenters[$i];
        new dBug($datacenter);

        // 会議を終了してログを記録する
        print "<li>会議を終了してログを記録する</li>";
        $ret = $this->amftest->calling("stopMeeting",
            $this->server_dsn_key,
            $meeting_key,
            $meeting_sequence_key,
            1,
            1,
            15000);
        new dBug($ret, "");

exit();


/*

        // サーバ一覧取得
        print "<li>サーバ一覧取得</li>";
        $ret = $this->amftest->calling("getServerList", $this->account_dsn);
        new dBug($ret, "");

        // FMSの動作ステータス切り替え
        print "<li>FMSの動作ステータス切り替え</li>";
        $ret = $this->amftest->calling("setServerAvailability",
            1,
            false
            );
        new dBug($ret, "");

        print "<li>FMSの動作ステータス切り替え</li>";
        $ret = $this->amftest->calling("setServerAvailability2",
            "192.168.1.146",
            true
            );
        new dBug($ret, "");

        // サーバ一覧取得
        print "<li>サーバ一覧取得</li>";
        $ret = $this->amftest->calling("getServerList", null);
        new dBug($ret, "");

        // 会議記録ホワイトボード削除
        print "<li>会議記録ホワイトボード削除</li>";
        $ret = $this->amftest->calling("deleteRecordedMinutes",
            $this->server_dsn_key,
            $meeting_key,
            $this->meeting_sequence_key
            );
        new dBug($ret, "");

        // 会議記録ビデオ削除
        print "<li>会議記録ビデオ削除</li>";
        $ret = $this->amftest->calling("deleteRecordedVideo",
            $this->server_dsn_key,
            $meeting_key,
            $this->meeting_sequence_key
            );
        new dBug($ret, "");

        // エラーリポートをメールで送信する
        print "<li>エラーリポートをメールで送信する</li>";
        $ret = $this->amftest->calling("sendReport",
            "",
            "",
            "AMFチェックツールテスト".date("Y-m-d H:i:s"),
            "AMFのチェック。正常に完了しました。"
            );
        new dBug($ret, "");
        print "</ul>";
        return true;
*/
    }

    /**
     * 会議生成
     */
    function meeting_create($room_key, $start = null, $end = null) {
        require_once("classes/core/Core_Meeting.class.php");
        // サービスオプション情報
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->dsn);
        $_room_info = $obj_N2MY_Account->getRoomInfo($room_key);
        $this->room_info    = $_room_info["room_info"];
        $this->options      = $this->room_info["options"];
        // 会議情報取得
        require_once("classes/N2MY_Meeting.class.php");
        $obj_N2MY_Meeting = new N2MY_Meeting($this->dsn);
        $meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, null, null);
        $meeting_key = $meeting_info["meeting_key"];
        $room_name = "amf_checker";
        $meeting_name = date("YmdHis");
        $meeting_rec_size += $this->options["hdd_extention"] * 1024 * 1024 * 1024;  // 1GBtype/契約
        $size_recordable = $meeting_rec_size;
        $country_id = isset($user_option["country_id"]) ? $user_option["country_id"] : "jp";
        // 予約
        $starttime = ($start) ? $start : date("Y-m-d H:i:s");
        $endtime = ($end) ? $end : "";
        $param = array(
            "room_key"                  => $room_key,
            "user_key"                  => $this->room_info["user_key"],
            "layout_key"                => "1",
            "meeting_ticket"            => $meeting_key,
            "meeting_room_name"         => $this->room_info["room_name"],
            "meeting_country"           => $country_id,
            "meeting_name"              => $meeting_name,
            "meeting_tag"               => $room_key,
            "meeting_max_audience"      => $this->room_info["max_audience_seat"],
            "meeting_max_seat"          => $this->room_info["max_seat"],
            "meeting_size_recordable"   => $size_recordable,
            "meeting_size_uploadable"   => 0,
            "meeting_start_datetime"    => $starttime,
            "meeting_stop_datetime"     => $endtime,
            "ssl"                       => $this->options["meeting_ssl"],
            "hispec"                    => $this->options["high_quality"],
            "mobile"                    => $this->options["mobile_phone_number"],
            "h323"                      => $this->options["h323_number"],
            "sharing"                   => $this->options["desktop_share"],
            );
        $this->logger->info("param",__FILE__,__LINE__,$param);
        $obj_CORE_Meeting = new Core_Meeting( $this->dsn );
        //$core_session_key = $obj_CORE_Meeting->checkMeetingStatus( $param );
        $core_session_key = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $this->options);
        return array(
            "meeting_key"        =>  $meeting_key,
            "meeting_seesion_id" =>  $core_session_key,
            );
    }

    /**
     * 会議開始（参加者キー取得）
     */
    function meeting_start($meeting_seesion_id) {
        $this->logger->info("room_info",__FILE__,__LINE__,$this->room_info);
        require_once("classes/N2MY_Meeting.class.php");
        require_once("classes/core/Core_Meeting.class.php");
        $obj_N2MY_Meeting = new N2MY_Meeting($this->dsn);
        $obj_CORE_Meeting = new Core_Meeting( $this->dsn );
        $station = array("新宿", "渋谷", "品川", "新橋", "東京", "上野", "中目黒");
        $rand_keys = array_rand($station);
        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $this->room_info["room_key"]."@".$this->config->get("N2MY", "mail_wb_host") : "";
        $user_options = array(
//            "id"        => $this->session->get( "participant_key" ),
            "name"      => "test",
            "participant_email"         => "",
            "participant_station"       => $station[$rand_keys],
            "member_key"=> "",
            "narrow"    => 0,
            "lang"      => "ja",
            "skin_type" => "",
            "mode"        => "",
            "role"      => "default",
            "mail_upload_address"       => $mail_upload_address,
            );
        // 会議入室
        // 会議開始
        print "<li>会議参加</li>";
        $count_p = count($this->participant_list) + 1;
        if($count_p > 10){
            $type =  "audience";
        } else {
            $type = "normal";
        }
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($meeting_seesion_id, $type, $user_options);
        $this->logger->debug("meeting_detail",__FILE__,__LINE__,$meetingDetail);
        new dBug($meetingDetail);
        require_once('classes/core/dbi/MeetingSequence.dbi.php');
        $obj_MeetingSequence = new DBI_MeetingSequence( $this->dsn );
        $sequence_info    = $obj_MeetingSequence->getRow( sprintf( "meeting_key='%s'", $meetingDetail["meeting_key"] ), null, array( "meeting_sequence_key" => "desc"));
        $this->logger->debug("sequence_info",__FILE__,__LINE__,$sequence_info);
        $this->meeting_sequence_key = $sequence_info["meeting_sequence_key"];

        $this->participant_list[] = array(
            "id" => $meetingDetail["participant_key"],
            "ip" => $meetingDetail["participant_key"],
            "name" => "test_".count($this->participant_list),
            "type" => $type,
            );
        // 入退室時の参加者情報を一括で更新する
        print "<li>入退室時の参加者情報を一括で更新する</li>";
        $ret = $this->amftest->calling("updateParticipantList",
            $this->server_dsn_key,
            $meetingDetail["meeting_key"],
            $this->participant_list
            );
        new dBug(array($this->participant_list,$ret));
        $session_id = split("-", session_id());
        session_id($session_id[0]."-".count($this->participant_list));
        return $meetingDetail;
    }

    function getParticipantList() {
        $participant_list = array();
        foreach($this->participant_list as $key => $participant) {
            $participant_list[] = array(
                "participant_id" => $participant["id"],
                "type"           => $participant["type"],
            );
        }
        return $participant_list;
    }

    /**
     * 乱数ID発行
     */
    function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this->dec2any($c);
    }

    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=62, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr( '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }

    function sendApi($path, $method, $_param, $token = "") {
        $param[$method] = "";
        if ($token) {
            $param[N2MY_SESSION] = $token;
        }
        foreach($_param as $key => $val) {
            $param[$key] = $val;
        }
        $url = N2MY_LOCAL_URL."/api/v1/".$path;
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $param,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 20,
            );
        curl_setopt_array($ch, $option);
        $contents = curl_exec($ch);
        $ret = unserialize($contents);
        if (!$ret) {
            return false;
        } elseif ($ret["status"] != "1") {
            return false;
        } else {
            return $ret;
        }
    }

}

$main = new AMF_Test();
$main->execute();