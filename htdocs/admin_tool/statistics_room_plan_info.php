<?php

// 環境設定
require_once('classes/AppFrame.class.php');
// 部屋情報
require_once('classes/dbi/room_plan.dbi.php');

/* ***********************************************
 * 部屋の情報を請求管理DBへ反映するための処理
 *
 * ***********************************************/
class AppStatisticsRoomPlanInfo extends AppFrame {

    var $_dsn = null;
    var $account_dsn = null;

    function init()
    {
        // DBサーバ情報取得
        if (file_exists(sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ))) {
            $server_list = parse_ini_file( sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
        } else {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        }
        //$this->logger->info(__FUNCTION__.' # Array Of Dsn Lists! ', __FILE__, __LINE__, $this->_dsn);
    }


    /**
     * default view routine
     *
     */
    function default_view()
    {

        $this->logger->info(__FUNCTION__.' # GetMeetingRoomPlans Info Operation Start ... ', __FILE__, __LINE__);

        // 初期化
        $mtg_room_plans = array();

        // パラメタ取得
        $target_day = $this->request->get('target_day');
        //$target_day = date('Y-m-d', time() - (3600*24)) . ' 00:00:00'; // 常に前日

        // パラメータ検証 ( boolearn : true / false )
        $param_is_safe = $this->_check_param($target_day);

        // パラメータ判定
        if ($param_is_safe){
            // MTG利用統計用部屋プランデータ（差分データ）作成
            $this->mtg_room_plans = array();
            //foreach ($this->dsn as $server_key => $server_dsn) {
                $_tmp_data = null;
                $_tmp_data = $this->_get_room_plan_routine($this->_dsn, $target_day);
                if (is_array($_tmp_data) === true) {
                    //$this->logger->info(__FUNCTION__.' # RoomPlanInfo ... ', __FILE__, __LINE__, $_tmp_data);
                    //$this->logger->info(__FUNCTION__.' # RoomPlanInfo ... ', __FILE__, __LINE__, "### Merge Room Info!!");
                    $mtg_room_plans = array_merge($mtg_room_plans, $_tmp_data);
                    //$this->logger->info(__FUNCTION__.' # RoomPlanInfo ... ', __FILE__, __LINE__, $mtg_room_plans);
                }
            //}
        } else {
            $mtg_room_plans = array();
        }
        $this->logger->info(__FUNCTION__.' # RoomPlanInfo Count ... ', __FILE__, __LINE__, count($mtg_room_plans));
        //$this->logger->debug(__FUNCTION__.' # RoomPlanInfo ... ', __FILE__, __LINE__, $mtg_room_plans);

        // 戻り値編集
        //print_r($mtg_room_plans);
        print serialize($mtg_room_plans);

        $this->logger->info(__FUNCTION__.' # GetMeetingRoomPlans Info Operation End ... ', __FILE__, __LINE__);
    }


    /**
     * メイン取得
     *
     */
    private function _get_room_plan_routine($data_db_dsn, $target_day)
    {
        $room_plan_obj = new RoomPlanTable($data_db_dsn);
        $rows = null;
        $_rows = null;
        // -------------- //
        $where = ' `room_plan_registtime` LIKE \'' . addslashes($target_day) . '%\' ' .
                 ' OR `room_plan_updatetime` LIKE \'' . addslashes($target_day) .'%\' ';
        $coulumns = ' `room_plan_key`, `room_key`, `service_key`, `discount_rate`, ' .
                    ' `contract_month_number`, `room_plan_yearly`, `room_plan_yearly_starttime`, ' .
                    ' `room_plan_starttime`, `room_plan_endtime`, `room_plan_status`, ' .
                    ' `room_plan_registtime`, `room_plan_updatetime` ';
        // -------------- //
        $rows = $room_plan_obj->getRowsAssoc($where, null, null, null, $coulumns);
        foreach ($rows as $key => $val) {
            $_rows[] = $val;
        }
        return $_rows;
    }


    /**
     * パラメータ検証
     *
     */
    private function _check_param($_param = null)
    {
        $ret = false;
        //未設定チェック
        if (!$_param) {
            return $ret;
        }
        //文字数チェック
        if (strlen($_param) < 7) {
            return $ret;
        }
        //指定不可文字存在チェック
        if (preg_match("/[_%*?]/", $_param)) {
            return $ret;
        }
        //チェック OK!
        $ret = true;

        return $ret;

    }


}

$main = new AppStatisticsRoomPlanInfo();
$main->execute();
?>