<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker:
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:Hiroshi                                                      |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
*/

require_once('config/config.inc.php');
require_once("classes/dbi/faq_list.dbi.php");
require_once("classes/dbi/faq_data.dbi.php");
require_once("classes/AppFrame.class.php");

class AppFaq extends AppFrame {

    var $logger = null;
    var $page_contents = '';
    var $table;
    var $error;
    var $n;
    var $maintenance = 0; // 0 -> ニュースリリース　1-> メンテナンスリリース
    var $page_name = 'index.php';

    function init() {
        $this->logger =& EZLogger::getInstance();
        $this->_sess_page = md5(__FILE__);
        if ($_lang = $this->request->get("lang")) {
            $this->session->set('lang', $_lang, $this->_sess_page);
        }
        $lang = $this->session->get('lang', $this->_sess_page, $this->get_language());
        //$this->faqTable = new FaqTable($this->get_auth_dsn(), $lang);
        $this->faqList = new FaqListTable($this->get_auth_dsn(), $lang);
        $this->faqData = new FaqDataTable($this->get_auth_dsn());
    }

    function auth() {
        if ($_SESSION["admin_tool"]["auth"] == 1) {
            return;
        }
        $user_id = $_REQUEST["auth"]["user_id"];
        $user_pw = $_REQUEST["auth"]["user_pw"];
        if ($user_id && $user_pw) {
            $fp = fopen(".htpasswd", "r");
            if ($fp) {
                while(!feof($fp)){
                    $line = trim(fgets($fp, 4096));
                    list($id, $pass) = split(",", $line);
                    if($id == $user_id && $pass == sha1($user_pw)){
                        $_SESSION["admin_tool"]["auth"] = 1;
                        return;
                    }
                }
            }
        }
        print <<<EOM
<form action="" method="post">
<table>
    <tr>
        <td>Admin ID:</td>
        <td><input type="text" name="auth[user_id]" value="$user_id"/></td>
    </tr><tr>
        <td>Admin PW:</td>
        <td><input type="password" name="auth[user_pw]" value="" /></td>
    </tr><tr>
        <td></td>
        <td><input type="submit" value="OK" /></td>
    </tr>
</table>
</form>
EOM;
        exit;
    }

    /**
     * デフォルトページ
     */
    function default_view() {
        $this->action_top();
    }

    /**
     * 表示順を上げる
     */
    function action_up() {
        $ret = $this->faqList->sort($_REQUEST['key'], "up");
        $this->action_top();
    }

    /**
     * 表示順を下げる
     */
    function action_down() {
        $ret = $this->faqList->sort($_REQUEST['key'], "down");
        $this->action_top();
    }

    /**
     * 一覧表示
     */
    function action_top() {
        $faq_list = array();
        $obj_FaqTable = new FaqListTable( N2MY_MDB_DSN, $this->session->get('lang', $this->_sess_page) );
        $faq_list = array();
        $user_info = $this->session->get( "user_info" );
        $where = "lang = '".$this->session->get('lang', $this->_sess_page)."'";
        $faq_data = $this->faqData->getRowsAssoc($where, null, null, null, null, "faq_key");
        $this->logger2->info($this->faqData->_conn->last_query);
        if($rows = $obj_FaqTable->getAllList( $user_info["account_model"] ) ){
            foreach($rows as $faq){
                $q_and_key = array(
                    $faq['faq_key'],
                    $faq_data[$faq['faq_key']]['question'],
                    $faq_data[$faq['faq_key']]['answer'],
                    $faq['faq_status'],
                    $faq['faq_category'],
                    $faq['faq_subcategory'],
                    $faq['faq_order'],
                    $faq['faq_type'],
                    "original_question" => $faq['faq_question'],
                    "original_answer"   => $faq['faq_answer']
                    );
                if (!isset($faq_list[$faq['faq_category']][$faq['faq_subcategory']])) {
                    $faq_list[$faq['faq_category']][$faq['faq_subcategory']] = array();
                    array_push($faq_list[$faq['faq_category']][$faq['faq_subcategory']], $q_and_key);
                } else {
                    array_push($faq_list[$faq['faq_category']][$faq['faq_subcategory']], $q_and_key);
                }
            }
            $this->template->assign('service_general', isset($faq_list[1][1]) ? $faq_list[1][1] : "");
            $this->template->assign('service_meeting', isset($faq_list[1][2]) ? $faq_list[1][2] : "");
            $this->template->assign('service_seminar', isset($faq_list[1][2]) ? $faq_list[1][2] : "");
            $this->template->assign('price_general', isset($faq_list[2][1]) ? $faq_list[2][1] : "");
            $this->template->assign('price_meeting', isset($faq_list[2][2]) ? $faq_list[2][2] : "");
            $this->template->assign('price_seminar', isset($faq_list[2][3]) ? $faq_list[2][3] : "");
            $this->template->assign('env_general', isset($faq_list[3][1]) ? $faq_list[3][1] : "");
            $this->template->assign('env_meeting', isset($faq_list[3][2]) ? $faq_list[3][2] : "");
            $this->template->assign('env_seminar', isset($faq_list[3][3]) ? $faq_list[3][3] : "");
            $this->template->assign('trouble_general', isset($faq_list[4][1]) ? $faq_list[4][1] : "");
            $this->template->assign('trouble_meeting', isset($faq_list[4][2]) ? $faq_list[4][2] : "");
            $this->template->assign('trouble_seminar', isset($faq_list[4][3]) ? $faq_list[4][3] : "");
            $this->template->assign('mac_general', isset($faq_list[5][1]) ? $faq_list[5][1] : "");
            $this->template->assign('mac_meeting', isset($faq_list[5][2]) ? $faq_list[5][2] : "");
            $this->template->assign('mac_seminar', isset($faq_list[5][3]) ? $faq_list[5][3] : "");
            $this->display('admin_tool/faq.t.html');
        }
    }

    /**
     * 新規作成
     */
    function action_new() {
        if($faq_info = $this->session->get('faq_info')){
            foreach($faq_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin_tool/faq_form.t.html');
    }

    /**
     * 確認
     */
    function action_confirm(){
        $this->session->set('faq_info', $_POST);
        $faq_info = $this->session->get('faq_info');
        foreach($faq_info as $key => $value){
            $this->template->assign($key, $value);
        }
        $this->display('admin_tool/faq_confirm.t.html');
    }

    /**
     * 編集完了
     */
    function action_complete(){
        $faq_info = $this->session->get('faq_info');
        $faq_list = array(
            "category"      => $faq_info["cat_num"],
            "subcategory"   => $faq_info["sub_num"],
            "type"          => $faq_info["faq_type"],
            "question"      => $faq_info["original_q"],
            "answer"        => $faq_info["original_a"],
        );
        $faq_data = array(
            "question"          => $faq_info["q"],
            "answer"            => $faq_info["a"],
            "update_datetime"   => date("Y-m-d H:i:s")
        );
        $faq_key = $faq_info['key'];
        if ($faq_key) {
            $this->faqList->complete($faq_list, $faq_key);
        } else {
            $faq_key = $this->faqList->complete($faq_list);
        }

        // 言語別
        $where = "faq_key = ".$faq_key.
            " AND lang = '".$this->session->get('lang', $this->_sess_page)."'";
        if ($count = $this->faqData->numRows($where)) {
            $this->logger2->info(array($faq_data, $where));
            $this->faqData->update($faq_data, $where);
        } else {
            $faq_data["faq_key"]         = $faq_key;
            $faq_data["lang"]            = $this->session->get('lang', $this->_sess_page);
            $faq_data["create_datetime"] = date("Y-m-d H:i:s");
            $this->logger2->info(array($faq_data, $where));
            $this->faqData->add($faq_data);
        }
        $this->action_top();
        $this->session->remove('faq_info');
    }

    /**
     * 編集
     */
    function action_edit() {
        $where = "faq_key = ".$_REQUEST['key'];
        $info = $this->faqList->getRow($where);
        $where = "faq_key = ".$_REQUEST['key'].
            " AND lang = '".$this->session->get('lang', $this->_sess_page)."'";
        $faq = $this->faqData->getRow($where);
        $this->template->assign('cat_num', $info['faq_category']);
        $this->template->assign('sub_num', $info['faq_subcategory']);
        $this->template->assign('original_q', $info['faq_question']);
        $this->template->assign('original_a', $info['faq_answer']);
        $this->template->assign('q', $faq['question']);
        $this->template->assign('a', $faq['answer']);
        $this->template->assign('faq_type', $info['faq_type']);
        $this->template->assign('key', $info['faq_key']);
        $this->display('admin_tool/faq_form.t.html');
    }

    /**
     * 削除確認
     */
    function action_deleteconfirm() {
        $where = "news_key = ".$_REQUEST['id'];
        $ret = $this->table->select($where);
        $news_info = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $info = $this->n->newsEdit($news_info);
        $this->page_contents .= $this->n->showConfirm($info);
        $this->action_print();
    }

    /**
     * 削除
     */
    function action_delete(){
        $this->table->newsDelete($_REQUEST['id']);
        $this->action_top();
        $this->session->remove('news_info');
    }

    /**
     * ステータス変更
     */
    function action_status(){
        $_type = $this->request->get("type");
        $id = $this->request->get("key");
        switch ($_type) {
        case "activate" :
            $type = "1";
            break;
        case "deactivate" :
            $type = "2";
            break;
        case "delete" :
            $type = "0";
            break;
        }
        if ($type !== "") {
            $this->faqList->setStatus($type, $id);
        }
        $this->action_top();
    }

    /**
     *
     */
    function action_test() {
        $ret = $this->table->getReleaseList();
        $this->page_contents .= $this->n->makeTestPage($ret);
        $this->action_top();
    }

    function action_release() {
        $ret = $this->table->getReleaseList();
        $this->page_contents .= $this->n->makeRelease($ret);
        $this->action_top();
    }

    function action_back() {
       $this->page_contents .= $this->n->makeBack();
       $this->action_top();
    }

    function action_uploadpage() {
        $this->n->uploadPage();
    }

    function action_upload() {
        $this->n->upload($_FILES);
    }
}

$main =& new AppFaq();
$main->execute();
?>