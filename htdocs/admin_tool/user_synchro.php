<?php

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once('classes/mgm/dbi/user.dbi.php');

class AppMoveAccount extends AppFrame {

    var $_auth = null;
    var $_sess_page;
    var $account_dsn = null;

    function init()
    {
        $_COOKIE["lang"] = "ja";
        $this->_sess_page = md5(__FILE__);
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
    }

    /**
     *
     */    function auth()
    {

    }

    /**
     * デフォルトページ
     */
    function default_view($message = "")
    {
        $this->template->assign("auth_dsn", $this->account_dsn);
        $this->template->assign("message", $message);
        $this->display("admin_tool/user_synchro/index.t.html");
    }

    function action_user_synchro()
    {
        $dsn        = $this->request->get("dsn");
        $auth_dsn   = $this->request->get("auth_dsn");
        $server_key = $this->request->get("server_key");
        if (!$dsn || !$auth_dsn || !$server_key) {
            $message ="入力されていない項目があります。（ユーザー同期）";
            $this->default_view($message);
            exit;
        }

        //ユーザーIDを取得
        $user = new UserTable($dsn);
        $user_list = $user->getRowsAssoc(null,null,null,null,"user_id,user_registtime");
        if ( DB::isError( $user_list ) ) {
            $this->logger->error( $user_list->getUserInfo() );
            return $user_list;
        }
        $this->logger->trace("user_list",__FILE__,__LINE__,$user_list);

        $auth_user = new MgmUserTable($auth_dsn);
        $count = $auth_user->numRows();
        foreach ($user_list as $value) {
            $this->logger->trace("value",__FILE__,__LINE__,$value);
            $where = "user_id='".mysql_real_escape_string($value["user_id"])."'";
            $check = $auth_user->getRow($where);
            // 登録されていなかったら追加する
            if (!$check) {
                $value["server_key"] = $server_key;
                $user_add = $auth_user->add($value);
                if ( DB::isError( $user_add ) ) {
                    $this->logger->error( $user_add->getUserInfo() );
                    return $user_add;
                }
            } else {
                $this->logger->error("already exist",__FILE__,__LINE__,$value["user_id"]);
            }
        }
        $count_after = $auth_user->numRows();
        $add_count = $count_after - $count;
        if ($add_count < 0) {
            $add_count = 0;
        }
        $message = "ユーザーIDを".$add_count."件同期いたしました";
        $this->default_view($message);
    }

    function action_member_synchro()
    {
        $dsn        = $this->request->get("dsn");
        $auth_dsn   = $this->request->get("auth_dsn");
        $server_key = $this->request->get("server_key");
        if (!$dsn || !$auth_dsn || !$server_key) {
            $message ="入力されていない項目があります。（メンバー同期）";
            $this->default_view($message);
            exit;
        }

        //メンバーIDを取得
        $member = new MemberTable($dsn);
        $member_list = $member->getRowsAssoc(null,null,null,null,"member_id");
        if ( DB::isError( $member_list ) ) {
            $this->logger->error( $member_list->getUserInfo() );
            return $member_list;
        }
        $this->logger->trace("member_list",__FILE__,__LINE__,$member_list);

        $auth_user = new MgmUserTable($auth_dsn);
        $count = $auth_user->numRows();
        foreach ($member_list as $value) {
            $this->logger->trace("value",__FILE__,__LINE__,$value);
            $where = "user_id='".mysql_real_escape_string($value["member_id"])."'";
            $check = $auth_user->getRow($where);
            // 登録されていなかったら追加する
            if (!$check) {
                $data["user_id"]    = $value["member_id"];
                $data["server_key"] = $server_key;
                $user_add = $auth_user->add($data);
                if ( DB::isError( $user_add ) ) {
                    $this->logger->error( $user_add->getUserInfo() );
                    return $user_add;
                }
            } else {
                $this->logger->error("already exist",__FILE__,__LINE__,$value["member_id"]);
            }
        }
        $count_after = $auth_user->numRows();
        $add_count = $count_after - $count;
        if ($add_count < 0) {
            $add_count = 0;
        }
        $message = $add_count."件登録いたしました";
        $this->default_view($message);
    }
}
$main = new AppMoveAccount();
$main->execute();