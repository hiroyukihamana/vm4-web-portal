<?php
require_once("classes/AppFrame.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/member_room_relation.dbi.php");

class AppRoomRelation extends AppFrame {

    function init () {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->dsn = $this->get_dsn();
        $this->objUser = new UserTable($this->get_dsn());
        $this->objRoom = new RoomTable($this->get_dsn());
        $this->objMember = new MemberTable($this->get_dsn());
        $this->objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
    }

    function default_view() {

        print $this->_header();
print <<<EOM
<form action="/admin_tool/update/room_relation.php" name="search" method="post">
<input type="hidden" name="action_set_relation" value="">
<input type="submit" name="submit" value="検索">
</form>
EOM;
        print $this->_footer();
    }

    function action_set_relation() {
        $where = "room_key != ''";
        $member_room_list = $this->objMember->getRowsAssoc($where, array("member_key" => "ASC"), null, 0, "member_key,room_key");
        $add_cont = 0;
        $error_cont = 0;
        foreach ($member_room_list as $member) {
            $where = "member_key = ".$member["member_key"].
                     " AND room_key = '".$member["room_key"]."'";
            $count = $this->objRoomRelation->numRows($where);
            $data = array(
                    "member_key"      => $member["member_key"],
                    "room_key"        => $member["room_key"],
                    "create_datetime" => date("Y-m-d H:i:s")
                );
            if ($count == "0") {
                $ret = $this->objRoomRelation->add($data);
                if (DB::isError($ret)) {
                    $this->logger2->info($data, "set relation error");
                    $error_cont++;
                } else {
                    $add_cont++;
                }
            } else {
                $this->logger2->info($data, "already exist");
                $error_cont++;
            }

        }

        print $this->_header();

        print "Succeed Count：".$add_cont;
        print "<br />";
        print "Error Count：".$error_cont;

        print $this->_footer();
    }

function _header() {
        return <<<EOM
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Room Relation Key Add</title>
<style TYPE="text/css">
<!--
body {
text-align: center;
font-size: 12px;
font-family: "Arial, sans-serif, ＭＳ Ｐゴシック", Osaka, "ヒラギノ角ゴ Pro W3" ;
margin: 10px 0px 0px 0px;
}
.title {
font-size: 16px;
font-weight: bold;
margin: 20px auto 0px auto;
width: 760px
}
#content {
text-align: center;
}
#content table {
border-collapse: collapse;
border-spacing: 0;
empty-cells: show;
border: 1px solid #999999;
margin: 20px auto;
width: 760px
}
#content th {
border: 1px solid #999999;
padding: 4px 5px;
background-color: #EFEFEF;
text-align: left;
}
#content td {
border: 1px solid #999999;
padding: 4px 10px;
text-align: left;
}
.errorSection {
color: #FF0000;
}
-->
</style>
</head>
<body>
<div id="content">
<h1>Room Relation Key Add</h1>
EOM;
    }

    function _footer() {
       print "</body></html>";
    }

}
$main =& new AppRoomRelation();
$main->execute();