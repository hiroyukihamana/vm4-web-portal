<?php

// 環境設定
require_once ('classes/AppFrame.class.php');
// ユーザ情報
require_once ('classes/dbi/user.dbi.php');

class AppStatisticsUserInfo extends AppFrame {

	var $_dsn = null;
	var $account_dsn = null;
	var $user_obj = null;

	function init() {
		// DBサーバ情報取得
		if (file_exists(sprintf("%sconfig/slave_list.ini", N2MY_APP_DIR))) {
			$server_list = parse_ini_file(sprintf("%sconfig/slave_list.ini", N2MY_APP_DIR), true);
			$this -> _dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
			$this -> account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
		} else {
			$server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR), true);
			$this -> _dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
			$this -> account_dsn = $this -> config -> get("GLOBAL", "auth_dsn");
		}
		$this -> user_obj = new UserTable($this -> _dsn);

	}

	/**
	 * default view routine
	 *
	 */
	function default_view() {

		$this -> logger -> info(__FUNCTION__ . ' # GetMeetingUsers Info Operation Start ... ', __FILE__, __LINE__);

		// 初期化
		$mtg_users = array();

		// パラメタ取得
		$target_day = $this -> request -> get('target_day');
		if (!$target_day) {
			$target_day = date("Y-m-d", (time() - 3600 * 24));
		}
		$this -> logger2 -> info("target day is " . $target_day);
		//$target_day = date('Y-m-d', time() - (3600*24)) . ' 00:00:00'; // 常に前日

		// パラメータ検証 ( boolearn : true / false )
		$param_is_safe = $this -> _check_param($target_day);

		// パラメータ判定
		if ($param_is_safe) {
			// MTG利用統計用ユーザデータ（差分データ）作成
			$this -> mtg_users = array();
			$_tmp_data = null;
			$_tmp_data = $this -> _get_user_routine($target_day);
			if (is_array($_tmp_data) === true) {
				$mtg_users = array_merge($mtg_users, $_tmp_data);
			}
			//}
		} else {
			$mtg_users = array();
		}
		$this -> logger -> info(__FUNCTION__ . ' # UserInfo Count ... ', __FILE__, __LINE__, count($mtg_users));

		$log_folder = N2MY_DOCUMENT_ROOT . "admin_tool/statistics_data/user/";
		//TO CSV
		require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
		$csv = new EZCsv(true, 'SJIS', 'UTF-8');
		$csv_file = $log_folder . "user_" . date("Ymd", $this -> get_target_date($target_day)) . ".csv";
		$handle = fopen($csv_file, "w");
		$csv -> open($csv_file, "w");
		$header = $this -> get_csv_header();
		$csv -> setHeader($header);
		$csv -> write($header);
		if ($mtg_users) {
			foreach ($mtg_users as $mtg_user) {
				$csv -> write($mtg_user);
			}
		}
		$csv -> close();
		chmod($csv_file, 0777);
		fclose($handle);

		//Delete log a week ago
		$delete_log = $log_folder . "user_" . date("Ymd", ($this -> get_target_date($target_day) - 3600 * 24 * 7)) . ".csv";
		if (file_exists($delete_log)) {
			unlink($delete_log);
		}

		$this -> logger -> info(__FUNCTION__ . ' # GetMeetingUsers Info Operation End ... ', __FILE__, __LINE__);
	}

	/**
	 * メイン取得
	 *
	 */
	private function _get_user_routine($target_day) {
		$rows = array();
		$where = ' `user_registtime` LIKE \'' . addslashes($target_day) . '%\' ' . ' OR `user_updatetime` LIKE \'' . addslashes($target_day) . '%\' ' . ' OR `user_deletetime` LIKE \'' . addslashes($target_day) . '%\' ';
		$rs = $this -> user_obj -> getRowsAssoc($where);
		foreach ($rs as $key => $val) {
			$rows[] = $val;
		}
		return $rows;
	}

	/**
	 * パラメータ検証
	 *
	 */
	private function _check_param($_param = null) {
		$ret = false;
		//未設定チェック
		if (!$_param) {
			return $ret;
		}
		//文字数チェック
		if (strlen($_param) < 7) {
			return $ret;
		}
		//指定不可文字存在チェック
		if (preg_match("/[_%*?]/", $_param)) {
			return $ret;
		}
		//チェック OK!
		$ret = true;

		return $ret;

	}

	/**
	 * describe table in order to get table columns
	 */
	function get_csv_header() {
		$sql = "desc user";
		$rs = $this -> user_obj -> _conn -> query($sql);
		$rows = array();
		$row = array();
		if (DB::isError($rs)) {
			$this -> logger -> info(__FUNCTION__ . " # DBError", __FILE__, __LINE__, $sql);
			return false;
		} else {
			while ($row = $rs -> fetchRow(DB_FETCHMODE_ASSOC)) {
				$rows[$row['Field']] = $row['Field'];
			}
		}
		return $rows;
	}

	/**
	 * 【テーブル名】_【YYYYmmdd】.csv
	 */
	function get_target_date($date) {
		$year = substr($date, 0, 4);
		$month = substr($date, 5, 2);
		$day = substr($date, 8, 2);
		$timestamp = mktime(0, 0, 0, $month, $day, $year);
		return $timestamp;
	}

}

$main = new AppStatisticsUserInfo();
$main -> execute();
?>