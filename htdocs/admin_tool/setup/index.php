<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
class N2MYWebInstaller {

    var $base_dir = null;
    var $_is_windows_os = false;
    var $conn = null;

    function init() {
        $this->base_dir = realpath(dirname(__FILE__) . "/../../../");
        if (file_exists($this->base_dir."/config/config.inc.php")) {
            require_once($this->base_dir."/config/config.inc.php");
        }
        session_start();
        $htaccess = $this->base_dir . "/htdocs/.htaccess";
        if (!file_exists($htaccess)) {
            ini_set("session.save_path", $this->base_dir . "/tmp");
        }
        if (substr(PHP_OS, 0, 3) == 'WIN') {
            $this->_is_windows_os = true;
        } else {
            $this->_is_windows_os = false;
        }
        
    }

    function action_phpinfo() {
        phpinfo();
    }

    function action_create_htaccess() {
        // include_path
        $include_path = ".".
            PATH_SEPARATOR.$this->base_dir.
            PATH_SEPARATOR.$this->base_dir."/lib/pear";
        $backup = $_REQUEST["backup"];
        # 定数設定ファイル
        $setVal = array (
            "{INC_PATH}" => $include_path,
            "{N2MY_WEB_HOME}" => $this->base_dir
        );
        $original = $this->base_dir . '/.htaccess.tpl';
        $output_file = $this->base_dir . '/htdocs/.htaccess';
        $backup = $_REQUEST["backup"];
        if ($ret = $this->setConfig($original, $output_file, $setVal, $backup)) {
            $this->html_header($ret);
            return $this->default_view();
        }
        header("Location: index.php");
    }

    function action_create_inc_php() {
        $backup = $_REQUEST["backup"];
        foreach($_REQUEST["item"] as $key => $val){
            # 新設定ファイル
            $setVal["{".$key."}"] = $val;
        }
        $setVal["{N2MY_APP_DIR}"] = realpath(dirname(__FILE__)."/../../../");
        $ret = $this->setConfig($this->base_dir . '/config/config.inc.php.tpl', $this->base_dir . '/config/config.inc.php', $setVal, $backup);
        header("Location: index.php");
    }

    /**
     *
     */
    function action_create_config() {
        $type = $_POST["type"];
        switch ($type) {
            case "mcu":
                $output_file = $this->base_dir . '/config/mcu_config.ini';
                break;
            case "pgi":
                $output_file = $this->base_dir . '/config/pgi_config.ini';
                break;
            default:
                $output_file = $this->base_dir . '/config/config.ini';
                break;
        }
        $backup = $_REQUEST["backup"];
        // バックアップ
        if ((file_exists($output_file) == true) && ($backup)) {
            $backup_file = $output_file . "_" . date("YmdHis");
            copy($output_file, $backup_file);
            chmod($backup_file, 0777);
        }
        $ini = $_REQUEST["ini"];
        $original_ini = array();
        //
        if (file_exists($output_file)) {
            $original_ini = parse_ini_file($output_file, true);
        }
        require_once("lib/EZLib/EZUtil/EZArray.class.php");
        $objArray = new EZArray();
        $assoc_array = $objArray->mergeRecursiveReplace($original_ini, $ini);
        $content = '';
        $sections = '';
        foreach ($assoc_array as $key => $item) {
            if (is_array($item))
            {
                $sections .= "\n[{$key}]\n";
                foreach ($item as $key2 => $item2)
                {
                    if (is_numeric($item2) || is_bool($item2))
                        $sections .= "{$key2} = {$item2}\n";
                    else
                        $sections .= "{$key2} = \"{$item2}\"\n";
                }
            }
            else
            {
                if(is_numeric($item) || is_bool($item))
                    $content .= "{$key} = {$item}\n";
                else
                    $content .= "{$key} = \"{$item}\"\n";
            }
        }
        $content .= $sections;
        file_put_contents($output_file, $content);
        header("Location: index.php");
    }

    function action_create_db() {
        $dsn = $this->get_config("GLOBAL", "auth_dsn");
        $params = parse_url($dsn);
        if ($params["scheme"] == "mysql") {
            $host = $params["host"];
            if ($params["port"]) {
                $host .= ":".$params["port"];
            }
            $link = @mysql_connect($host, $params["user"], $params["pass"]);
            if (!$link) {
                $this->html_header('can not connect to mysql database.<br />' . mysql_error());
                return $this->default_view();
            } else {
                //
                $ret = mysql_query("create database ". substr($params["path"],1), $link);
                if (!$ret) {
                    $this->html_header('Mysql connected, but mysql user no authority to create a database.<br />' . mysql_error());
                    return $this->default_view();
                }
            }
        }
        header("Location: index.php");
    }

    function action_create_table() {
        $this->db_connect();
        $tables = $this->conn->getListOf("tables");
        if (count($tables) > 0) {
            $err = 'Tables are successfully created.';
            $this->html_header($err);
            return $this->default_view();
        }
        // web table
        $db_file = $this->base_dir . "/setup/account_db.sql";
        $create_db = file_get_contents($db_file);
        $querys = $this->parse_query($create_db);
        foreach ($querys as $key => $query) {
            $ret = $this->conn->query($query);
            if (DB :: isError($ret)) {
                die($query);
            }
        }
        // 完了
        $this->default_view();
        header("Location: index.php");
    }

    function get_config($group, $key, $default = "") {
        static $config;
        if (!isset ($config)) {
            $config = parse_ini_file($this->base_dir . '/config/config.ini', true);
        }
        if (isset ($config[$group][$key])) {
            return $config[$group][$key];
        } else {
            return $default;
        }
    }

    function default_view() {
        $server_name = $_SERVER["SERVER_NAME"];
        if (file_exists($this->base_dir."/config/config.inc.php")) {
            require_once($this->base_dir."/config/config.inc.php");
            # ベースURL
            $N2MY_BASE_URL = N2MY_BASE_URL;
            # サーバ内から実行する差異のURL
            $N2MY_LOCAL_URL = N2MY_LOCAL_URL;
            # PHPディレクトリ
            $N2MY_PHP_DIR = N2MY_PHP_DIR;
            # Rubyディレクトリ
            $N2MY_RUBY_DIR = N2MY_RUBY_DIR;
            # ImageMagickディレクトリ
            $N2MY_IMGMGC_DIR = htmlspecialchars(N2MY_IMGMGC_DIR);
            # デフォルトDB
            $N2MY_DEFAULT_DB = N2MY_DEFAULT_DB;
            # 可逆暗号化
            $N2MY_ENCRYPT_KEY = N2MY_ENCRYPT_KEY;
            # 可逆暗号化 IV(８文字)
            $N2MY_ENCRYPT_IV = N2MY_ENCRYPT_IV;
            # 可逆暗号化
            $VCUBE_ENCRYPT_KEY = VCUBE_ENCRYPT_KEY;
            # 可逆暗号化 IV(８文字)
            $VCUBE_ENCRYPT_IV = VCUBE_ENCRYPT_IV;
            #FMS SCP User
            $N2MY_SCP_USER = N2MY_SCP_USER;
            # メール送信元
            $N2MY_MAIL_FROM = N2MY_MAIL_FROM;
            # アカウント管理者問い合わせ先
            $N2MY_ACCOUNT_TO = N2MY_ACCOUNT_TO;
            # アカウント管理者問い合わせ元
            $N2MY_ACCOUNT_FROM = N2MY_ACCOUNT_FROM;
            # サービス管理者問い合わせ先
            $ADMIN_INQUIRY_TO = ADMIN_INQUIRY_TO;
            # サービス管理者問い合わせ元
            $ADMIN_INQUIRY_FROM = ADMIN_INQUIRY_FROM;
            # ユーザページお問い合わせ先
            $USER_ASK_TO = USER_ASK_TO;
            # ユーザページお問い合わせ元
            $USER_ASK_FROM = USER_ASK_FROM;
            # 予約時の送信元
            $RESERVATION_FROM = RESERVATION_FROM;
            #パスワードリセットお問い合わせ先
            $PW_RESET_TO = PW_RESET_TO;
            #パスワードリセットお問い合わせ元
            $PW_RESET_FROM = PW_RESET_FROM;
            # システムの警告メール送信元
            $N2MY_ALERT_TO = N2MY_ALERT_TO;
            # システムの警告メール送信先
            $N2MY_ALERT_FROM = N2MY_ALERT_FROM;
            # 会議記録移動エラーメール送信元
            $N2MY_LOG_ALERT_TO = N2MY_LOG_ALERT_TO;
            # 会議記録移動エラーメール送信先
            $N2MY_LOG_ALERT_FROM = N2MY_LOG_ALERT_FROM;
            # システムログがエラー以上の場合に警告メールを送信元
            $N2MY_ERROR_FROM = N2MY_ERROR_FROM;
            # システムログがエラー以上の場合に警告メールを送信先
            $N2MY_ERROR_TO = N2MY_ERROR_TO;

            # アプリケーション設定
            # ユーザー用メインメニュー会議室表示件数
            $MAINMENU_MAX_VIEW = MAINMENU_MAX_VIEW;
            # 会議予約一覧表示件数
            $RESERVATION_MAX_VIEW = RESERVATION_MAX_VIEW;
            # 録画容量(Mbyte)
            $RECORD_SIZE = RECORD_SIZE;
            $ECO_CLIENT_CO2 = ECO_CLIENT_CO2;
            # php.ini
            $SMTP            = ini_get("SMTP");
            $display_errors  = ini_get("display_errors");
            $log_errors      = ini_get("log_errors");
            $error_reporting = ini_get("error_reporting");
            # session
            $session_gc_maxlifetime = ini_get("session.gc_maxlifetime");
            # ロガーによるシステムエラーのメール送信
            $N2MY_ERROR_NOTIFICATION = N2MY_ERROR_NOTIFICATION;
        } else {
            # ベースURL
            $N2MY_BASE_URL = "http://".$_SERVER["SERVER_NAME"]."/";
            # サーバ内から実行する差異のURL
            $N2MY_LOCAL_URL = "http://".$_SERVER["SERVER_NAME"]."/";
            if ($this->_is_windows_os) {
                # PHPディレクトリ
                $N2MY_PHP_DIR = "C:/php/pear/";
                # Rubyディレクトリ
                $N2MY_RUBY_DIR = "C:/ruby/bin/";
                # ImageMagickディレクトリ
                $N2MY_IMGMGC_DIR = htmlspecialchars('C:/"Program Files"/ImageMagick-6.4.4-Q16/');
                # デフォルトDB
                $N2MY_DEFAULT_DB = "PJname_data";
                # 可逆暗号化
                $N2MY_ENCRYPT_KEY = "N2MY";
                # 可逆暗号化 IV(８文字)
                $VCUBE_ENCRYPT_IV = "N2MYN2MY";
                # 可逆暗号化
                $VCUBE_ENCRYPT_KEY = "N2MY";
                # 可逆暗号化 IV(８文字)
                $N2MY_ENCRYPT_IV = "N2MYN2MY";
                #FMS SCP User
                $N2MY_SCP_USER = "n2my";
                # 録画容量(Mbyte)
                $RECORD_SIZE = "50000";
                # session
                $session_gc_maxlifetime = "86400";
            } else {
                $N2MY_PHP_DIR = dirname(`which php`);
                $N2MY_RUBY_DIR = dirname(`which ruby`);
                $N2MY_IMGMGC_DIR = dirname(`which convert`);
                $N2MY_DEFAULT_DB = "";
                $N2MY_ENCRYPT_KEY = "";
                # 可逆暗号化 IV(８文字)
                $VCUBE_ENCRYPT_IV = "";
                # 可逆暗号化
                $VCUBE_ENCRYPT_KEY = "";
                # 可逆暗号化 IV(８文字)
                $N2MY_ENCRYPT_IV = "";
                #FMS SCP User
                $N2MY_SCP_USER = "";
                # 録画容量(Mbyte)
                $RECORD_SIZE = "500";
                # session
                $session_gc_maxlifetime = ini_get("session.gc_maxlifetime");
            }
            # メール送信元
            $N2MY_MAIL_FROM = "";
            # アカウント管理者問い合わせ先
            $N2MY_ACCOUNT_TO = "";
            # アカウント管理者問い合わせ元
            $N2MY_ACCOUNT_FROM = "";
            # サービス管理者問い合わせ先
            $ADMIN_INQUIRY_TO = "";
            # サービス管理者問い合わせ元
            $ADMIN_INQUIRY_FROM = "";
            # ユーザページお問い合わせ先
            $USER_ASK_TO = "";
            # ユーザページお問い合わせ元
            $USER_ASK_FROM = "";
            # 予約時の送信元
            $RESERVATION_FROM = "";
            #パスワードリセットお問い合わせ先
            $PW_RESET_TO = "";
            #パスワードリセットお問い合わせ元
            $PW_RESET_FROM = "";
            # システムの警告メール送信元
            $N2MY_ALERT_TO = "";
            # システムの警告メール送信先
            $N2MY_ALERT_FROM = "";
            # 会議記録移動エラーメール送信元
            $N2MY_LOG_ALERT_TO = "";
            # 会議記録移動エラーメール送信先
            $N2MY_LOG_ALERT_FROM = "";
            # システムログがエラー以上の場合に警告メールを送信元
            $N2MY_ERROR_FROM = "";
            # システムログがエラー以上の場合に警告メールを送信先
            $N2MY_ERROR_TO = "";
            # アプリケーション設定
            # ユーザー用メインメニュー会議室表示件数
            $MAINMENU_MAX_VIEW = "5";
            # 会議予約一覧表示件数
            $RESERVATION_MAX_VIEW = "5";
            # php.ini
            $SMTP            = ini_get("SMTP");
            $display_errors  = 0;
            $log_errors      = ini_get("log_errors");
            $error_reporting = ini_get("error_reporting");
            # ロガーによるシステムエラーのメール送信
            $N2MY_ERROR_NOTIFICATION = 0;
        }
        $this->html_header();
        print '<li><a href="?action_phpinfo" target="_blank">phpinfo</a>';
        print "<li>.htaccess file confirmation.<br />";
        print '<font color="green">...OK</font><br />';
        print "<br><br>";
        print "<li>Environment Configuration<br />";
        print<<<EOM
<script type="text/javascript">
<!--
  function setConfigSubmit(file_name, frm) {
    if (confirm("Are you sure to change the setting?")) {
      if (prompt("To avoid pressing send button by mistake, please input server host name.") != "$server_name") {
        alert("server host name is wrong!");
      } else {
        frm.submit();
      }
    }
  }
//-->
</script>
<fieldset><legend>config.inc.php</legend>
<form action="index.php" method="post">
<input type="hidden" name="action_create_inc_php" value="">
<input type="hidden" name="service_type" value="$this->service_type">
<input type="hidden" name="config_file" value="config/config.ini"/><fieldset><legend>External Command</legend><table><tr>
<td>BASE URL</td>
<td><input type="text" name="item[N2MY_BASE_URL]" value="$N2MY_BASE_URL" size="100"/></td></tr>
<tr>
<td>Server Inner Request URL</td>
<td><input type="text" name="item[N2MY_LOCAL_URL]" value="$N2MY_LOCAL_URL" size="100"/></td></tr>
<tr>
<td>PHP Path</td>
<td><input type="text" name="item[N2MY_PHP_DIR]" value="$N2MY_PHP_DIR" size="50"/></td></tr>
<tr>
<td>Ruby Path</td>
<td><input type="text" name="item[N2MY_RUBY_DIR]" value="$N2MY_RUBY_DIR" size="50"/></td></tr>
<tr>
<td>ImageMagick Path</td>
<td><input type="text" name="item[N2MY_IMGMGC_DIR]" value="$N2MY_IMGMGC_DIR" size="50"/></td></tr>
<tr>
<td>Default DB for Redundant</td>
<td><input type="text" name="item[N2MY_DEFAULT_DB]" value="$N2MY_DEFAULT_DB"/></td></tr>
<tr>
<td>Reversible Encryption KEY</td>
<td><input type="text" name="item[N2MY_ENCRYPT_KEY]" value="$N2MY_ENCRYPT_KEY"/></td></tr>
<tr>
<td>Reversible Encryption IV(8 characters)</td>
<td><input type="text" name="item[N2MY_ENCRYPT_IV]" value="$N2MY_ENCRYPT_IV"/></td></tr>
<tr>
<td>Reversible Encryption KEY(DB)</td>
<td><input type="text" name="item[VCUBE_ENCRYPT_KEY]" value="$VCUBE_ENCRYPT_KEY"/></td></tr>
<tr>
<td>Reversible Encryption IV(8 characters)(DB)</td>
<td><input type="text" name="item[VCUBE_ENCRYPT_IV]" value="$VCUBE_ENCRYPT_IV"/></td></tr>
<tr>
<td>FMS SCP User (Storage Whiteboard)</td>
<td><input type="text" name="item[N2MY_SCP_USER]" value="$N2MY_SCP_USER"/></td></tr>
</table>
</fieldset>
<fieldset>
<legend>Mail</legend><table>
<tr>
<td>General Service Mail Sender(excluded from below)</td>
<td><input type="text" name="item[N2MY_MAIL_FROM]" value="$N2MY_MAIL_FROM" size="70"/></td></tr>
<tr>
<td>Admin Account Inquiry Receiver</td>
<td><input type="text" name="item[N2MY_ACCOUNT_TO]" value="$N2MY_ACCOUNT_TO" size="70"/></td></tr>
</tr><tr>
<td>Admin Account Inquiry Sender</td>
<td><input type="text" name="item[N2MY_ACCOUNT_FROM]" value="$N2MY_ACCOUNT_FROM" size="70"/></td></tr>
</tr><tr>
<td>Admin Service Inquiry Receiver</td>
<td><input type="text" name="item[ADMIN_INQUIRY_TO]" value="$ADMIN_INQUIRY_TO" size="70"/></td></tr>
</tr><tr>
<td>Admin Service Inquiry Sender</td>
<td><input type="text" name="item[ADMIN_INQUIRY_FROM]" value="$ADMIN_INQUIRY_FROM" size="70"/></td></tr>
</tr><tr>
<td>User Page Inquiry Receiver</td>
<td><input type="text" name="item[USER_ASK_TO]" value="$USER_ASK_TO" size="70"/></td></tr>
</tr><tr>
<td>User Page Inquiry Sender</td>
<td><input type="text" name="item[USER_ASK_FROM]" value="$USER_ASK_FROM" size="70"/></td></tr>
</tr><tr>
<td>Reservation Mail Sender</td>
<td><input type="text" name="item[RESERVATION_FROM]" value="$RESERVATION_FROM" size="70"/></td></tr>
</tr><tr>
<td>Password Reset Inquiry Receiver</td>
<td><input type="text" name="item[PW_RESET_TO]" value="$PW_RESET_TO" size="70"/></td></tr>
</tr><tr>
<td>Password Reset Inquiry Sender</td>
<td><input type="text" name="item[PW_RESET_FROM]" value="$PW_RESET_FROM" size="70"/></td></tr>
</tr><tr>
<td>System Warming Mail Receiver</td>
<td><input type="text" name="item[N2MY_ALERT_TO]" value="$N2MY_ALERT_TO" size="70"/></td></tr>
</tr><tr>
<td>System Warming Mail Sender</td>
<td><input type="text" name="item[N2MY_ALERT_FROM]" value="$N2MY_ALERT_FROM" size="70"/></td></tr>
<tr>
<td>Meeting Record Transfer Error Mail Receiver</td>
<td><input type="text" name="item[N2MY_LOG_ALERT_TO]" value="$N2MY_LOG_ALERT_TO" size="70"/></td></tr>
<tr>
<td>Meeting Record Transfer Error Mail Sender</td>
<td><input type="text" name="item[N2MY_LOG_ALERT_FROM]" value="$N2MY_LOG_ALERT_FROM" size="70"/></td></tr>
<tr>
<td>Another System Error Mail Recevier</td>
<td><input type="text" name="item[N2MY_ERROR_FROM]" value="$N2MY_ERROR_FROM" size="70"/></td></tr>
<tr>
<td>Another System Error Mail Sender</td>
<td><input type="text" name="item[N2MY_ERROR_TO]" value="$N2MY_ERROR_TO" size="70"/></td></tr>
</table>
</fieldset>
<fieldset><legend>Mail</legend><table>
<tr>
<td>SMTP Server</td>
<td><input type="text" name="item[SMTP]" value="$SMTP"/></td></tr>
<tr>
<td>Room Count of Each Page in Mainpage</td>
<td><input type="text" name="item[MAINMENU_MAX_VIEW]" value="$MAINMENU_MAX_VIEW"/></td></tr>
<tr>
<td>Meeting Reservation Count of Each Page</td>
<td><input type="text" name="item[RESERVATION_MAX_VIEW]" value="$RESERVATION_MAX_VIEW"/></td></tr>
<tr>
<td>Video REC Storage(Mbyte)</td>
<td><input type="text" name="item[RECORD_SIZE]" value="$RECORD_SIZE"/></td></tr>
</table></fieldset><fieldset><legend>Errors</legend><table><tr>
<td><label for='display_errors'>Display Errors</td>
<td><input type="text" name="item[display_errors]" value="$display_errors"/></td></tr>
<tr>
<td><label for='log_errors'>Output Errors to Log File</td>
<td><input type="text" name="item[log_errors]" value="$log_errors"/></td></tr>
<tr>
<td><label for='error_reporting'>Error Log Level</td>
<td><input type="text" name="item[error_reporting]" value="$error_reporting"/></td></tr>
<tr>
<td>Session Validity Time(sec)</td>
<td><input type="text" name="item[session.gc_maxlifetime]" value="$session_gc_maxlifetime"/></td></tr>
<tr>
<td>Mail the Logger Errors?</td>
<td><input type="text" name="item[N2MY_ERROR_NOTIFICATION]" value="$N2MY_ERROR_NOTIFICATION"/></td></tr>
</table></fieldset>
<input type="checkbox" id="backup1" name="backup" value="1" />
<label for="backup1">Make a Backup</label>
    <input type="button" value="OK" onclick="setConfigSubmit('config.inc.php', this.form);" />
    </td>
    </tr>
    </table>
</form>
</fieldset>
EOM;


        if (file_exists($this->base_dir."/config/config.inc.php")) {
            print '<font color="green">...OK</font>';
        } else {
            print '<font color="red">...config.ini does not exist</font>';
        }
        print "<br><br>";
        // 設定ファイルの有無
        print "<li>Application Configuration<br />\n";
        print '<fieldset><legend>config.ini</legend><form action="index.php" method="post">';
        print '<input type="hidden" name="action_create_config" value="">';
        $this->create_form("step2");
        print<<<EOM
    <input type="checkbox" id="backup2" name="backup" value="1" />
    <label for="backup2">Make a Backup</label>
    <input type="button" value="OK" onclick="setConfigSubmit('config.ini', this.form);" />
    </td>
    </tr>
    </table>
</form>
</fieldset>
EOM;


        if (file_exists($this->base_dir."/config/config.ini")) {
            print '<font color="green">...OK</font>';
        } else {
            print '<font color="red">...config.ini does not exist</font>';
        }
        print "<br><br>";
        // 設定ファイルの有無
        print "<li>MCU Configuration<br />\n";
        print '<fieldset><legend>mcu_config.ini</legend><form action="index.php" method="post">';
        print '<input type="hidden" name="action_create_config" value="">';
        print '<input type="hidden" name="type" value="mcu">';
        $this->create_form("step3");
        print<<<EOM
    <input type="checkbox" id="backup2" name="backup" value="1" />
    <label for="backup2">Make a Backup</label>
    <input type="button" value="OK" onclick="setConfigSubmit('mcu_config.ini', this.form);" />
    </td>
    </tr>
    </table>
</form>
</fieldset>
EOM;
        if (file_exists($this->base_dir."/config/mcu_config.ini")) {
            print '<font color="green">...OK</font>';
        } else {
            print '<font color="red">...mcu_config.ini does not exist</font>';
        }
        print "<br><br>";
        if (!file_exists(N2MY_CONFIG_FILE)) {
            $this->html_footer();
        }
        print "</li>";
        // DB作成
        print "<li>generate account database<br />";
        $this->db_connect();
        print '<font color="green">...OK</font>';
        print "<li>generate account database tables<br />";
        $tables = $this->conn->getListOf("tables");
        if (count($tables) == 0) {
            print '<font color="red">Tables don\'t exist.<br />';
            print 'To generate new tables?</font><br />';
            print '<form action="index.php" method="post">' .
            '<input type="submit" name="action_create_table" value="YES" />' .
            '</form>';
            $this->html_footer();
        }
        print '<font color="green">...OK</font>';
        print '<li><a href="'.N2MY_BASE_URL.'admin_tool/">admin_tool page</a></li>';
        $this->html_footer();
    }

    function html_header($error = "") {
        static $is_head = null;
        if (isset ($is_head)) {
            return false;
        }
        header("Content-Type: text/html; charset=UTF-8");
        $fp = fopen($this->base_dir . "/version.dat", "r");
        $version = fread($fp, filesize($this->base_dir . "/version.dat"));
        fclose($fp);
        $server_name = $_SERVER["SERVER_NAME"];
        print<<<EOM
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>N2MY for Meeting Web Installer Ver $version [ $server_name ]</title>
<style type="text/css">
    body {
        font-family:Verdana, Arial, Helvetica, sans-serif; color:#000000; font-size:12px;
    }
</style>
</head>
<body>
<h2>N2MY for Meeting Web Installer Ver $version<br />
[ $server_name ]</h2>
<font color="red">$error</font>
<ol>
EOM;
        $is_head = true;
    }

    function create_form($step_id) {
        require_once("lib/EZLib/EZXML/EZXML.class.php");
        $contents = file_get_contents($this->base_dir."/setup/config.xml");
        $obj_XML = new EZXML();
        $list = $obj_XML->openXML($contents);
        $list["installer"]["files"];
        foreach($list["installer"]["files"] as $key => $_file) {
            foreach($_file["file"] as $file) {
                if ($file["_attr"]["id"] == $step_id) {
                    $output_file = $file["_attr"]["name"];
                    print $output_file;
                    print '<input type="hidden" name="config_file" value="'.$output_file.'"/>';
                    if (file_exists($this->base_dir."/".$output_file)) {
                        $parse_ini = parse_ini_file($this->base_dir."/".$output_file, true);
                    }
                    $sections = $file["section"];
                    foreach($sections as $_section) {
                        $section_id = $_section["_attr"]["id"];
                        $name       = $_section["_attr"]["name"];
                        $desc       = $_section["_attr"]["desc"];
                        $disable    = isset($_section["_attr"]["disable"]) ? $_section["_attr"]["disable"] : 0;
                        $params = $_section["param"];
                        if (!$disable) {
                            print "<fieldset>";
                            print '<legend>'.$name.'</legend>';
                            print "<table>";
                        }
                        foreach($params as $_param) {
                            $param_id      = $_param["_attr"]["id"];
                            if (isset($parse_ini[$section_id][$param_id])) {
                                $param_default = $parse_ini[$section_id][$param_id];
                            } else {
                                $param_default = $_param["_attr"]["default"];
                                $param_default = str_replace("{N2MY_APP_DIR}", N2MY_APP_DIR, $param_default);
                            }
                            $param_name    = $_param["_attr"]["name"];
                            $param_desc    = $_param["_attr"]["desc"];
                            $param_size    = isset($_param["_attr"]["size"]) ? $_param["_attr"]["size"] : 100;
                            $param_disable = isset($_param["_attr"]["disable"]) ? $_param["_attr"]["disable"] : 0;
                            if ($param_disable) {
                                print '<input type="hidden" name="ini['.$section_id.']['.$param_id.']" value="'.$param_default.'" size="'.$param_size.'"/>'."\n";
                            } else {
                                print "<tr>\n";
                                print "<td><label for='".$param_id."'>".$param_name."</label></td>\n";
                                print '<td>';
    //                            if ($param_desc) print '<font size="-1" color="blue">'.$param_desc.'</font><br />';
                                print '<input type="text" name="ini['.$section_id.']['.$param_id.']" value="'.$param_default.'" size="'.$param_size.'"/></td>';
                                print "</tr>\n";
                            }
                        }
                        if (!$disable) {
                            print "</table></fieldset>";
                        }
                    }
                }
            }
        }
    }

    function html_footer() {
        print "</ol></body></html>";
        exit;
    }

    /**
     *
     */
    function setConfig($original_file, $output_file, $setList = array (), $backup = true) {
        if (file_exists($original_file)) {
            if ((file_exists($output_file) == true) && ($backup)) {
                $backup_file = $output_file . "_" . date("YmdHis");
                copy($output_file, $backup_file);
                chmod($backup_file, 0777);
            }
            $fp = fopen($original_file, "r");
            $contents = fread($fp, filesize($original_file));
            fclose($fp);
            foreach ($setList as $search => $replace) {
                $contents = str_replace($search, $replace, $contents);
            }
            $fp = fopen($output_file, "w");
            if (!$fp) {
                return "No permission to write config files.";
            }
            if (!fwrite($fp, $contents)) {
                return "Permission Error";
            }
            fclose($fp);
            if (!chmod($output_file, 0777)) {
                return "Permission Error";
            }
            return "";
        } else {
            return "No original file.";
        }
    }

    function auth() {
        if ($_SESSION["admin_tool"]["auth"] == 1) {
            return true;
        }
        $user_id = $_REQUEST["auth"]["user_id"];
        $user_pw = $_REQUEST["auth"]["user_pw"];
        if ($user_id && $user_pw) {
            $fp = fopen("../.htpasswd", "r");
            if ($fp) {
                while (!feof($fp)) {
                    $line = trim(fgets($fp, 4096));
                    list ($id, $pass) = split(",", $line);
                    if ($id == $user_id && $pass == sha1($user_pw)) {
                        $_SESSION["admin_tool"]["auth"] = 1;
                        return true;
                    }
                }
            }
        }
        $this->html_header("Please login first.");
        print<<<EOM
<form action="index.php" method="post">
<table align="center">
    <tr>
      <td>Admin ID:</td>
      <td><input type="text" name="auth[user_id]" value="$user_id"/></td>
    </tr><tr>
      <td>Admin PW:</td>
      <td><input type="password" name="auth[user_pw]" value="" /></td>
    </tr>
    <tr>
      <td></td>
      <td><input type="submit" value="OK" /></td>
    </tr>
</table>
</form>
EOM;
        exit;
    }

    function execute() {
        if (method_exists($this, "init")) {
            $this->init();
        }
        $method_name = "default_view";
        if ($_REQUEST) {
            foreach ($_REQUEST as $key => $val) {
                if (is_string($key) && substr($key, 0, 7) == "action_") {
                    if (method_exists($this, $key)) {
                        $method_name = $key;
                        break;
                    }
                }
            }
        }
        if ($method_name == "action_create_htaccess") {
            $this->action_create_htaccess();
        }
        if (method_exists($this, "auth")) {
            $this->auth();
        }
        //
        if (!file_exists("../../.htaccess")) {
            die(file_get_contents("./index.t.html"));
        }
        $this->$method_name();
    }

    function parse_query($data) {
        // set default values
        $timeout_passed = FALSE;
        $error = FALSE;
        $read_multiply = 1;
        $finished = FALSE;
        $offset = 0;
        $max_sql_len = 0;
        $file_to_unlink = '';
        $sql_query = '';
        $sql_query_disabled = FALSE;
        $go_sql = FALSE;
        $executed_queries = 0;
        $run_query = TRUE;
        $charset_conversion = FALSE;
        $reset_charset = FALSE;
        $bookmark_created = FALSE;
        // init values
        $sql_delimiter = ';';
        $sql = '';
        $start_pos = 0;
        $i = 0;
        // Append new data to buffer
        $buffer = $data;
        // Current length of our buffer
        $len = strlen($buffer);
        // Grab some SQL queries out of it
        while ($i < $len) {
            $found_delimiter = false;
            // Find first interesting character, several strpos seem to be faster than simple loop in php:
            //while (($i < $len) && (strpos('\'";#-/', $buffer[$i]) === FALSE)) $i++;
            //if ($i == $len) break;
            $oi = $i;
            $p1 = strpos($buffer, '\'', $i);
            if ($p1 === FALSE) {
                $p1 = 2147483647;
            }
            $p2 = strpos($buffer, '"', $i);
            if ($p2 === FALSE) {
                $p2 = 2147483647;
            }
            $p3 = strpos($buffer, $sql_delimiter, $i);
            if ($p3 === FALSE) {
                $p3 = 2147483647;
            } else {
                $found_delimiter = true;
            }
            $p4 = strpos($buffer, '#', $i);
            if ($p4 === FALSE) {
                $p4 = 2147483647;
            }
            $p5 = strpos($buffer, '--', $i);
            if ($p5 === FALSE || $p5 >= ($len -2) || $buffer[$p5 +2] > ' ') {
                $p5 = 2147483647;
            }
            $p6 = strpos($buffer, '/*', $i);
            if ($p6 === FALSE) {
                $p6 = 2147483647;
            }
            $p7 = strpos($buffer, '`', $i);
            if ($p7 === FALSE) {
                $p7 = 2147483647;
            }
            $i = min($p1, $p2, $p3, $p4, $p5, $p6, $p7);
            unset ($p1, $p2, $p3, $p4, $p5, $p6, $p7);
            if ($i == 2147483647) {
                $i = $oi;
                if (!$finished) {
                    break;
                }
                // at the end there might be some whitespace...
                if (trim($buffer) == '') {
                    $buffer = '';
                    $len = 0;
                    break;
                }
                // We hit end of query, go there!
                $i = strlen($buffer) - 1;
            }

            // Grab current character
            $ch = $buffer[$i];

            // Quotes
            if (!(strpos('\'"`', $ch) === FALSE)) {
                $quote = $ch;
                $endq = FALSE;
                while (!$endq) {
                    // Find next quote
                    $pos = strpos($buffer, $quote, $i +1);
                    // No quote? Too short string
                    if ($pos === FALSE) {
                        // We hit end of string => unclosed quote, but we handle it as end of query
                        if ($finished) {
                            $endq = TRUE;
                            $i = $len -1;
                        }
                        break;
                    }
                    // Was not the quote escaped?
                    $j = $pos -1;
                    while ($buffer[$j] == '\\')
                        $j--;
                    // Even count means it was not escaped
                    $endq = (((($pos -1) - $j) % 2) == 0);
                    // Skip the string
                    $i = $pos;
                }
                if (!$endq) {
                    break;
                }
                $i++;
                // Aren't we at the end?
                if ($finished && $i == $len) {
                    $i--;
                } else {
                    continue;
                }
            }

            // Not enough data to decide
            if ((($i == ($len -1) && ($ch == '-' || $ch == '/')) || ($i == ($len -2) && (($ch == '-' && $buffer[$i +1] == '-') || ($ch == '/' && $buffer[$i +1] == '*')))) && !$finished) {
                break;
            }

            // Comments
            if ($ch == '#' || ($i < ($len -1) && $ch == '-' && $buffer[$i +1] == '-' && (($i < ($len -2) && $buffer[$i +2] <= ' ') || ($i == ($len -1) && $finished))) || ($i < ($len -1) && $ch == '/' && $buffer[$i +1] == '*')) {
                // Copy current string to SQL
                if ($start_pos != $i) {
                    $sql .= substr($buffer, $start_pos, $i - $start_pos);
                }
                // Skip the rest
                $j = $i;
                $i = strpos($buffer, $ch == '/' ? '*/' : "\n", $i);
                // didn't we hit end of string?
                if ($i === FALSE) {
                    if ($finished) {
                        $i = $len -1;
                    } else {
                        break;
                    }
                }
                // Skip *
                if ($ch == '/') {
                    // Check for MySQL conditional comments and include them as-is
                    if ($buffer[$j +2] == '!') {
                        $comment = substr($buffer, $j +3, $i - $j -3);
                        if (preg_match('/^[0-9]{5}/', $comment, $version)) {
                            if ($version[0] <= PMA_MYSQL_INT_VERSION) {
                                $sql .= substr($comment, 5);
                            }
                        } else {
                            $sql .= $comment;
                        }
                    }
                    $i++;
                }
                // Skip last char
                $i++;
                // Next query part will start here
                $start_pos = $i;
                // Aren't we at the end?
                if ($i == $len) {
                    $i--;
                } else {
                    continue;
                }
            }

            // End of SQL
            if ($found_delimiter || ($finished && ($i == $len -1))) {
                $tmp_sql = $sql;
                if ($start_pos < $len) {
                    $length_to_grab = $i - $start_pos;
                    if (!$found_delimiter) {
                        $length_to_grab++;
                    }
                    $tmp_sql .= substr($buffer, $start_pos, $length_to_grab);
                    unset ($length_to_grab);
                }
                // Do not try to execute empty SQL
                if (!preg_match('/^([\s]*;)*$/', trim($tmp_sql))) {
                    $sql = $tmp_sql;
                    $querys[] = $tmp_sql;
                    //                    $this->PMA_importRunQuery($sql, substr($buffer, 0, $i + strlen($sql_delimiter)));
                    $buffer = substr($buffer, $i +strlen($sql_delimiter));
                    // Reset parser:
                    $len = strlen($buffer);
                    $sql = '';
                    $i = 0;
                    $start_pos = 0;
                    // Any chance we will get a complete query?
                    //if ((strpos($buffer, ';') === FALSE) && !$finished) {
                    if ((strpos($buffer, $sql_delimiter) === FALSE) && !$finished) {
                        break;
                    }
                } else {
                    $i++;
                    $start_pos = $i;
                }
            }
        } // End of parser loop
        return $querys;
    }

    function db_connect() {
        require_once ('../../../lib/pear/DB.php');
        $dsn = $this->get_config("GLOBAL", "auth_dsn");
        $this->conn = DB :: connect($dsn);
        $params = parse_url($dsn);
        if (DB::isError($this->conn)) {
            print '<font color="red">Failed to connect database in Mysql server.<br />';
            print "Please check in Mysql, Or generate a new one.<br />";
            print '</font>';
            print '<table>';
            print "<tr><td>system</td><td>: ".$params["scheme"]."</td></tr>";
            print "<tr><td>host</td><td>: ".$params["host"]."</td></tr>";
            print "<tr><td>port</td><td>: ".$params["port"]."</td></tr>";
            print "<tr><td>user ID</td><td>: ".$params["user"]."</td></tr>";
            print "<tr><td>password</td><td>: ".$params["pass"]."</td></tr>";
            print "<tr><td>DB name</td><td>: ".substr($params["path"],1)."</td></tr>";
            print "<tr><td>query code</td><td>: ".$params["query"]."</td></tr>";
            print '</table>';
            print '<form action="index.php" method="post">' .
            '<input type="submit" name="action_create_db" value="generate new database" />' .
            '</form>';
            $this->html_footer();
        } else {
            // キャラセット指定
            $dsn_elem = parse_url($dsn);
            if (isset ($dsn_elem["query"])) {
                $query_opt = array ();
                parse_str($dsn_elem["query"], $query_opt);
                if (isset ($query_opt["charset"])) {
                    $this->conn->query("SET NAMES " . $query_opt["charset"]);
                }
            }
        }
    }
}

$main = new N2MYWebInstaller();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
