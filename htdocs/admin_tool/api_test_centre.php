<?php
header("Content-type: text/html; charset=UTF-8;");
require_once("config/config.inc.php");
require_once("config/service_api_config.php");
require_once("classes/N2MY_Api.class.php");
require_once("lib/pear/XML/Unserializer.php");
require_once("htdocs/admin_tool/dBug.php");
if (!$user_id || !$user_pw) {
    die("検証用のファイルがありません");
}

class AppApiDebug extends N2MY_Api
{
    function default_view() {

        // ユーザー
        global $user_id;
        global $user_pw;
        global $user_apw;
        global $mail_to;
        global $mail_from;
        global $sender;

        global $timezone;

        global $api_key;
        global $secret;

        $host = "http://".$_SERVER["SERVER_NAME"];
        $api_version = "v2";
        $api = new API_Checker($host, $api_version);
        $api->_get_passage_time();

        /**
         * アカウント管理関連
         */

        $debug_flg     = ($_REQUEST["debug"])     ? 'checked="checked" ' : "";
        $error_flg     = ($_REQUEST["error"])     ? 'checked="checked" ' : "";
        $reference_flg = ($_REQUEST["reference"]) ? 'checked="checked" ' : "";

        $meeting_flg     = ($_REQUEST["check"]["meeting"])      ? 'checked="checked" ' : "";
        $reservation_flg = ($_REQUEST["check"]["reservation"])  ? 'checked="checked" ' : "";
        $meetinglog_flg  = ($_REQUEST["check"]["meetinglog"])   ? 'checked="checked" ' : "";
        $addressbook_flg = ($_REQUEST["check"]["addressbook"])  ? 'checked="checked" ' : "";
        $eco_flg         = ($_REQUEST["check"]["eco"])          ? 'checked="checked" ' : "";
        $document_flg    = ($_REQUEST["check"]["document_flg"]) ? 'checked="checked" ' : "";

        $admin_meetinglog_flg = ($_REQUEST["check"]["admin_meetinglog_flg"]) ? 'checked="checked" ' : "";
        $admin_room_flg       = ($_REQUEST["check"]["admin_room_flg"]) ? 'checked="checked" ' : "";
        $admin_member_flg     = ($_REQUEST["check"]["admin_member_flg"]) ? 'checked="checked" ' : "";

        if ($admin_meetinglog_flg || $admin_room_flg || $admin_member_flg) {
            $admin_flg = 1;
        }


        $filename = basename(__FILE__);
        print <<<EOM
<style type="text/css">
        body {
          font-family:Verdana, Arial, Helvetica, sans-serif; color:#000000; font-size:12px;
        }
</style>
<body>
<h2>ターミナルAPI検証</h2>
<form action="$filename" method="post">
<input type="checkbox" name="debug" id="debug" value="1" $debug_flg /><label for="debug">デバッグ</label><br />
User ID: <input type="text" name="member_id" value="{$_REQUEST['member_id']}" /><br />
Password:<input type="text" name="member_pw" value="{$_REQUEST['member_pw']}" /><br />
Type:<input type="text" name="login_type" value="{$_REQUEST['login_type']}" /><br />
<input type="submit" name="action_execute" value="実行" />
</form>

EOM;

        if ($_REQUEST["action_execute"]) {
            $member_id = $_REQUEST['member_id'];
            $member_pw = $_REQUEST['member_pw'];
            print "<ol>";

            $api->add_log("*ユーザー用API\n\n");

            $api_path = "user/";
            $api->add_log("**ユーザ認証関連 $api_path \n\n");
            $api->add_group($api_path, "ユーザ認証関連", "ログインなどが行えます。");

            // ログイン
            $method_name  = 'action_login';
            $method_title = 'メンバーログイン';
            $method_info  = '';
            $param = array(
                "api_id"  => $api_key,
                "id"  => $member_id,
                "pw"   => hash('sha256', ($member_id . hash('sha256', $member_pw) . $secret)),
                "login_type" => $_REQUEST['login_type'],
                "output_type" => "php"
                );
            $ret = $api->send($api_path, $method_name, $param, null, $method_title, $method_info);
            $token = $ret["data"]["session"];
            $member_name = $ret["data"]["member_info"]['member_name'];
            $member_name_kana = $ret["data"]["member_info"]['member_name_kana'];

if (!$token) {
    die('</ol>ログインエラー');
}

            // 部屋一覧取得
            $method_name  = 'action_get_room_list';
            $method_title = '部屋一覧取得';
            $method_info  = 'ログインユーザが利用可能な部屋の一覧を取得します。';
            $param = array(
                );
            $room_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            list($room_no, $room_info) = each($room_list["data"]["rooms"]["room"]);
            $room_key = $room_info["room_info"][API_ROOM_ID];

            // 部屋状態取得（会議開催中のデータを取得）
            $method_name  = 'action_get_room_info';
            $method_title = '部屋状態取得';
            $method_info  = '部屋の利用状況（会議中、参加人数、参加者名など）を取得します。';
            $param = array(
                API_ROOM_ID => $room_key,
                );
            $room_status = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            $meeting_id = $room_status['data']['room_status']['meeting_id'];

            // ユーザー情報の更新
            $method_name  = 'action_update';
            $method_title = 'メンバー情報の更新';
            $method_info  = '';
            $param = array(
                "email"     => $mail_from,
                "name"      => $member_name,
                "name_kana" => $member_name_kana,
                "timezone"  => $timezone,
                "lang"      => "ja",
                "country"   => "jp",
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            // メンバー一覧取得
            $method_name  = 'action_get_member_list';
            $method_title = 'メンバー一覧取得';
            $method_info  = 'ログインユーザーに所属しているメンバー一覧を取得します。';
            $param = array(
                );
            $member_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            $member_id = $member_list['data']['members']['member'][0]['member_id'];

            //
            $method_name  = 'action_start';
            $method_title = '会議開始 (会議IDを指定して入室)';
            $method_info  = '指定した部屋の会議に参加するためのURLを発行します。' .
                    '会議キーの指定がない場合は、現在利用可能な会議に参加されます。注意：発酵時の会議が終了すると入室できません。';
            $param = array(
                API_ROOM_ID    => $room_key,
                API_MEETING_ID => $meeting_id,
                "type"        => "terminal",
                "name"        => "",
                "is_narrow"   => "",
                );
            $meeting_start = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            print '<a target="_blank" href="'.$meeting_start["data"]["url"].'">会議開始</a><br />';

            //
            $method_name  = 'action_knocker';
            $method_title = 'ノッカー';
            $method_info  = "会議中の参加者にメッセージを送信します。";
            $param = array(
                API_ROOM_ID => $room_key,
                "message" => date("Y-m-d H:i:s"),
                );
            $param_property = array(
                "message"  => array(
                    'name'        => 'ノッカーメッセージ',
                    'type'        => 'string(100)',
                    'restriction' => '1～100文字以内',
                    'required'    => '必須',
                    'default'     => '',
                    'addition'    => 'ノッカーで会議室内に表示させる文字列',
                    'example'     => 'メッセージ',
                    ),
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            //
            $method_name  = 'action_wb_upload';
            $method_title = '資料追加';
            $method_info  = '会議中のホワイトボードに資料を張り込みます。<br />';
            $param = array(
                API_ROOM_ID => $room_key,
                "file" => "@".N2MY_APP_DIR."htdocs/shared/images/ecowhats_bg.gif",
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            //
            $method_name  = 'action_stop';
            $method_title = '会議終了';
            $method_info  = '会議を終了します。';
                $param = array(
                    API_ROOM_ID => $room_key,
                    );
                $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

exit();
                $api_path = "user/reservation/";
            $api->add_log("**予約関連 $api_path \n\n");
            $api->add_group($api_path, "予約関連", "予約、招待、事前アップロードなど");

            $method_name  = 'action_add';
            $method_title = '予約追加(メンバーID指定)';
            $method_info  = '予約を追加します。';
            $param = array(
                API_ROOM_ID     => $room_key,
                "name"         => date("Y-m-d H:i:s")."[新規]",
                "timezone"     => $timezone,
                "start"        => date("Y-m-d H:i:00", time()),
                "end"          => date("Y-m-d H:i:00", time() + 60),
                "password"     => "",
                "send_mail"    => 0,
                "sender_name"  => $sender,
                "sender_email" => $mail_from,
                "info"         => "メール本文",
                "guests[0][name]"     => "kiyomizu",
                "guests[0][email]"    => "kiyomizu@vcube.co.jp",
                "guests[0][type]"     => "",
                "guests[0][timezone]" => "100",
                "guests[0][lang]"     => "ja",
                );
            $reservation_info = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            $reservation_session = $reservation_info["data"][API_RESERVATION_ID];

            //
            $method_name  = 'action_get_list';
            $method_title = '予約一覧';
            $method_info  = '予約済みの一覧を取得します。';
            $param = array(
                API_ROOM_ID   => $room_key,
                "start_limit" => strtotime('2009-01-01 00:00:00'), // date("Y-m-d H:i:s"),
                "end_limit"   => strtotime(date("Y-m-d H:i:s", time() + 3600 * 24 * 365)), // date("Y-m-d H:i:s", time() + 3600 * 24 * 30),
                "limit"       => 20,
                "offset"      => 0,
                "status"      => 'wait',
                "sort_key"    => "reservation_starttime",
                "sort_type"   => "asc",
                );
            $result = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            $reservation_list = $result["data"]["reservations"];
            $reservation_session = $reservation_list["reservation"][0][API_RESERVATION_ID];

            //
            $method_name  = 'action_get_detail';
            $method_title = '予約内容詳細';
            $method_info  = '予約の詳細情報を取得します。';
            $param = array(
                API_RESERVATION_ID => $reservation_session,
                );
            if ($reservation_list["reservation"][0]["reservation_pw"]) {
                $param["password"] = $user_apw;
            }
            $reservation_info = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            $method_name  = 'action_update';
            $method_title = '予約変更';
            $method_info  = '予約内容を変更します。';
            $param = array(
                API_RESERVATION_ID  => $reservation_session,
                "name"              => date("Y-m-d H:i:s")."[更新]",
                "timezone"          => $timezone,
                "start"             => date("Y-m-d H:i:00", time()),
                "end"               => date("Y-m-d H:i:00", time() + 60),
//                    "password"          => $reservaion_pw,
                "change_password"   => 1,
                "new_password"      => "",
                "send_mail"         => 0,
                "sender_name"       => $sender,
                "sender_email"      => $mail_from,
                "info"              => "メール本文",
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            //
            $method_name  = 'action_add_invite';
            $method_title = '招待者追加（通常）';
            $method_info  = '予約した会議の招待メールを送信します。';
            $param = array(
                API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                "name"                => date("Y-m-d H:i:s")."[招待者新規]",
                "email"               => $mail_to,
                "timezone"            => $timezone,
                "lang"                => "ja",
                "type"                => "",
                "send_mail"           => 0,
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            //
            $method_name  = 'action_add_invite';
            $method_title = '招待者追加（メンバー登録）';
            $method_info  = '予約した会議の招待メールを送信します。';
            $param = array(
                API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                "name"                => date("Y-m-d H:i:s")."[招待者新規]",
                "email"               => $mail_to,
                "timezone"            => $timezone,
                "lang"                => "ja",
                "type"                => "",
                "send_mail"           => 0,
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            $method_name  = 'action_get_invite';
            $method_title = '招待者一覧';
            $method_info  = '予約した会議に登録した招待者一覧を取得します。';
            $param = array(
                API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                );
            $invite_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            list($invite_id, $invite_info) = each($invite_list["data"]["guests"]["guest"]);

            //
            $method_name  = 'action_delete_invite';
            $method_title = '招待者削除';
            $method_info  = '予約した会議の招待者を削除します。';
            $param = array(
                API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                API_GUEST_ID => $invite_info[API_GUEST_ID],
                "lang" => "ja",
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            //
            $method_name  = 'action_add_document';
            $method_title = '資料追加';
            $method_info  = '予約した会議に事前資料アップロードを行います。';
            $param = array(
                API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                "file" => "@".N2MY_APP_DIR."htdocs/shared/images/ecowhats_bg.gif",
                "name" => "画像 - ".date("Y-m-d H:i:s"),
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            //
            $method_name  = 'action_get_document';
            $method_title = '資料一覧取得';
            $method_info  = '予約した会議の事前資料一覧を取得します。';
            $param = array(
                API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                );
            $document_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            list($a, $document) = each($document_list["data"]["documents"]["document"]);

            //
            $method_name  = 'action_update_document';
            $method_title = '資料名変更';
            $method_info  = '予約した会議の資料名を変更します。';
            $param = array(
                API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                "document_id" => $document["document_id"],
                "name" => "名前変更 - ".date("Y-m-d"),
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            //
            $method_name  = 'action_delete_document';
            $method_title = '資料削除';
            $method_info  = '予約した会議の資料を削除します。';
            $param = array(
                API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                "document_id" => $document["document_id"],
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            $method_name  = 'action_preview_document';
            $method_title = '資料の表示';
            $method_info  = '';
            $url = $host."/api/".$api_version."/".$api_path .
                    "?action_preview_document=" .
                    "&document_id=" .$document["document_id"].
                    "&page=1" .
                    "&".N2MY_SESSION."=".$token;
            print '<img src="'.$url.'" />';
            print $url;

            //
            $method_name  = 'action_delete';
            $method_title = '予約削除';
            $method_info  = '予約を取り消します。';
            $param = array(
                API_RESERVATION_ID => $reservation_session,
//                    "password"          => $new_reservaion_pw,
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            $api_path = "user/meetinglog/";
            $api->add_log("**会議記録関連 $api_path \n\n");
            $api->add_group($api_path, "会議記録関連", "会議記録関連");

            ///*
            $sv_start_time = strtotime('2009-01-01 00:00:00');
            $sv_end_time = strtotime(date("Y-m-d H:i:s", time() + 3600 * 24 * 30));
            //
            $method_name  = 'action_get_list';
            $method_title = '会議記録一覧';
            $method_info  = '会議記録一覧を取得します。';
            $param = array(
                API_ROOM_ID    => $room_key,
                "meeting_name" => "",
                "user_name"    => "",
                "start_limit"  => $sv_start_time, //"",
                "end_limit"    => $sv_end_time, //"",
                "page"         => 1,
                "limit"        => "5",
                "sort_key"     => "actual_start_datetime",
                "sort_type"    => "asc",
                );
            $meeting_logs = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            list($meeting_key, $meeting_log) = each($meeting_logs["data"]["meetinglogs"]["meetinglog"]);
            $meeting_session_id = $meeting_log[API_MEETING_ID];
            $meeting_sequence_id = $meeting_log['meeting_sequences']['meeting_sequence'][0]['meeting_sequence_key'];

            $method_name  = 'action_get_detail';
            $method_title = '会議記録詳細';
            $method_info  = '会議記録の詳細情報を取得します。';
            $param = array(
                API_MEETING_ID => $meeting_session_id,
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            //
            $method_name  = 'action_video_player';
            $method_title = '会議記録再生';
            $method_info  = '録画再生用のワンタイムURLを発行します。有効期限は5分間です。 ';
            $param = array(
                API_MEETING_ID  => $meeting_session_id,
                "sequence_id"   => $meeting_sequence_id,
                "lang"          => "ja",
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            $method_name  = 'action_minute_player';
            $method_title = '議事録再生';
            $method_info  = '議事録再生用のワンタイムURLを発行します。有効期限は5分間です。 ';
            $param = array(
                API_MEETING_ID  => $meeting_session_id,
                "sequence_id"   => $meeting_sequence_id,
                "lang"          => "ja",
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);

            $api_path = "user/addressbook/";
            $api->add_log("**アドレス帳関連 $api_path \n\n");
            $api->add_group($api_path, "アドレス帳関連", "アドレス帳の登録、更新、削除");

            //
            $method_name  = 'action_add';
            $method_title = 'アドレス帳追加';
            $method_info  = 'アドレス帳に追加します。';
            $param = array(
                "name"       => "name",
                "name_kana"  => "namae",
                "email"      => "sample@example.com",
                "timezone"   => "9",
                "lang"       => "ja",
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            //
            $method_name  = 'action_get_list';
            $method_title = 'アドレス帳一覧';
            $method_info  = 'アドレス帳一覧を取得します';
            $param = array(
                "sort_key"    => "name",
                "sort_type"   => "asc",
                "page"        => 1,
                "limit"       => 20,
                "keyword"     => "",
                );
            $address_list = $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            list($key, $address_info) = each($address_list["data"]["addresses"]["address"]);

            //
            $method_name  = 'action_update';
            $method_title = 'アドレス帳更新';
            $method_info  = 'アドレス帳の情報を更新します。';
            $param = array(
                API_ADDRESS_ID   => $address_info[API_ADDRESS_ID],
                "name"          => "name",
                "name_kana"     => "namae",
                "email"         => "sample@example.com",
                "timezone"      => "9",
                "lang"          => "ja",
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
            //
            $method_name  = 'action_delete';
            $method_title = 'アドレス帳削除';
            $method_info  = 'アドレス帳から削除します';
            $param = array(
                API_ADDRESS_ID   => $address_info[API_ADDRESS_ID],
                );
            $api->send($api_path, $method_name, $param, $token, $method_title, $method_info);
        }
    }
}

$main = new AppApiDebug();
$main->execute();

class API_Checker {

    var $version = "";
    var $url = "";
    var $log = "";
    var $method_list = "";
    var $xml = "";
    // ミーティング共通パラメータ
    var $param_proparty = array(
        API_ROOM_ID  => array(
            'name'        => '部屋キー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => 'ログイン中のユーザーが所有する部屋のキー',
            'example'     => 'api_test-1-6b4e',
            ),
        API_MEETING_ID  => array(
            'name'        => '会議キー',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '予約等、入室する会議が決まっている場合の会議キー',
            'example'     => '39e8c7366ca89150627993c09cad2430',
            ),
        API_RESERVATION_ID  => array(
            'name'        => '予約会議セッションキー',
            'type'        => 'string(64)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '予約した会議のセッションキー',
            'example'     => 'f327998e82dc9fa496889c79c80e5d1f',
            ),
        API_ADDRESS_ID  => array(
            'name'        => 'アドレス帳キー',
            'type'        => 'int(20)',
            'restriction' => '',
            'required'    => '必須',
            'default'     => '',
            'addition'    => '会議セッションキー',
            'example'     => '87',
            ),
        "lang" => array(
            'name'        => '言語選択',
            'type'        => 'string(2)',
            'restriction' => '',
            'required'    => '',
            'default'     => 'ja',
            'addition'    => 'ja: 日本語 / en: 英語',
            'example'     => 'ja',
            ),
        "country" => array(
            'name'        => '現在の所在地',
            'type'        => 'string(32)',
            'restriction' => '',
            'required'    => '',
            'default'     => 'jp',
            'addition'    => 'jp: 日本 / us: アメリカ（西） / us2: アメリカ（東） / cn: 中国 / sg: シンガポール / etc: その他 / jp2: --',
            'example'     => 'jp',
            ),
        "timezone" => array(
            'name'        => 'タイムゾーン',
            'type'        => 'string(16)',
            'restriction' => '',
            'required'    => '',
            'default'     => '9',
            'addition'    => '-11 ～ 14',
            'example'     => '9',
            ),
        "enc" => array(
            'name'        => 'API出力結果のエンコード',
            'type'        => 'string(64)', //･･･ここは適当
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'null: エンコードなし / md5: MD5で暗号化 / sha1: SHA1で暗号化',
            'example'     => '',
            ),
        "output_type" => array(
            'name'        => 'API出力形式',
            'type'        => 'string(64)', //･･･ここは適当
            'restriction' => '',
            'required'    => '',
            'default'     => 'xml',
            'addition'    => 'xml: XML形式 / json：JSON形式 / php: PHPシリアライズ / yaml: YAML',
            'example'     => 'xml',
            ),
        // 検索オプション共通パラメータ
        "limit" => array(
            'name'        => '検索データ取得行数',
            'type'        => 'int(10)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'レコード取得制限件数',
            'example'     => '20',
            ),
        "page" => array(
            'name'        => 'ページ番号',
            'type'        => 'int(10)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => '表示するページの表示番号',
            'example'     => '2',
            ),
        "sort_key" => array(
            'name'        => '検索データソートキー',
            'type'        => 'string(255)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'ソート対象の項目ID',
            'example'     => '',
            ),
        "sort_type" => array(
            'name'        => '検索データソート順',
            'type'        => 'string(4)',
            'restriction' => '',
            'required'    => '',
            'default'     => '',
            'addition'    => 'asc: 昇順 / desc: 降順',
            'example'     => 'asc',
            ),
        );
    function __construct($host, $version) {
        $this->version = $version;
        $this->sess_id = N2MY_SESSION;
        $this->param_proparty[N2MY_SESSION] = array(
                'name'        => 'ミーティングセッションID',
                'type'        => 'string(255)',
                'restriction' => '',
                'required'    => '必須',
                'default'     => '',
                'addition'    => 'API認証用セッションID',
                'example'     => 'l6teg8lv9rh35iemee2cjoq7b0',
            );
        $this->method_list = array();
        $this->base_url = $host;
    }

    function send($path, $method, $_param, $token = "", $title = "", $info = "", $param_proparty = array(), $override = false) {
        $param[$method] = "";
        if ($token) {
            $param[$this->sess_id] = $token;
        }
        foreach($_param as $key => $val) {
            $param[$key] = $val;
        }
        $start = $this->_get_passage_time();
        $url = $this->base_url."/api/".$this->version."/".$path;
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $param,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 20,
            );
        curl_setopt_array($ch, $option);
        $contents = $ret = curl_exec($ch);
        $time = $this->_get_passage_time() - $start;
        $ret = unserialize($contents);

        $query_str = "";
        $param["output_type"] = "xml";
        foreach ($param as $_key => $_value) {
            $post_data[$_key] .= $_value;
            $query_str .= "&" . $_key . "=" . urlencode($_value);
        }
        if ($query_str) {
            $query_str = "?" . substr($query_str, 1);
        }
        print "<li>".$title."<br/><font color=993322>".$method."</font>";
        print ' [ <a href="/api/reference/index.php?action_method=&path='."api/".$this->version."/".$path.'&method='.$method.'" target="_blank">リファレンス</a> ] ( <a href="'.$url.$query_str.'" target="_blank">結果表示</a> ) ';
        if ($info) {
            print "<br/><font color='gray'>".nl2br($info)."</font>";
        }
        if (!$ret) {
            print "<br /><font color='red'>実行エラー</font>";
        } elseif ($ret["status"] != "1") {
            print "<br /><font color='red'>パラメタエラー</font>";
        }

        $parse_url = parse_url($url);
        $parse_url["query"] = $post_data;

        require_once("lib/pear/XML/Serializer.php");
        $serializer = new XML_Serializer();
        $serializer->setOption('mode','simplexml');
        $serializer->setOption('encoding','UTF-8');
        $serializer->setOption('addDecl','ture');
        $serializer->setOption('rootName','result');
        $serializer->serialize($ret);
        $output = $serializer->getSerializedData();

        /**
         * pukiwiki用のコードをログにはく
         */
        $target = "z".substr(md5($path.$method), 0, 7);
        $input = array(
            "method"    => $method,
            "session"   => $token,
            "parameter" => $_param,
            );
        $result = array("Input" => $input, "Output" => $ret, "処理時間" => $time. " / ". $this->_get_passage_time());
        if ($_REQUEST["debug"] or !$ret["status"]) {
            new dBug(array("result" => $result));
        } else {
            new dBug(array("result" => $result), "", true);
        }
        return $ret;
    }

    function parse($data, $indent = 0) {
        static $result;
        if (!$indent) {
            $result = "";
        }
        $space = "                ".str_repeat(" ", $indent * 4);
        $sw = 0;
        foreach ($data as $key => $val) {
            if (is_array($val)) {
                if (!is_numeric($key)) {
                    $result .= $space.'<'.$key .' name="" info="" example="">'."\n";
                    $indent++;
                }
                if ($sw == 0) {
                    $this->parse($val, $indent);
                }
                if (!is_numeric($key)) {
                    $result .= $space.'</'.$key .'>'."\n";
                } else {
                    $sw = 1;
                }
            } else {
                if (!is_numeric($key)) {
                    $result .= $space.'<'.$key .' name="" info="" example="'.htmlspecialchars($val).'" />'."\n";
                }
            }
        }
        return $result;
    }

    function add_log($msg) {
        $this->log .= $msg;
    }

    function add_group($id, $name, $desc) {
        $this->group[$id]["name"] = $name;
        $this->group[$id]["desc"] = $desc;
    }

    /**
     *
     */
    function outputLog() {
        // wiki用
        // file_put_contents("../../logs/service_api.log", $this->log);
        // メソッド、入力、エラー情報
        @mkdir(N2MY_APP_DIR."/logs/api/".$this->version, 0777, true);
        $xml = '<api_list>'."\n";
        foreach ($this->xml as $key => $group) {
            $xml .= '    <group name="'.$this->group[$key]["name"].'" path="'.$key.'">' ."\n";
            $xml .= '    <desc><![CDATA['.$this->group[$key]["desc"].']]></desc>'."\n";
            foreach ($group as $methods) {
                foreach ($methods as $method) {
                    $xml .= $method;
                }
            }
            $xml .= '    </group>'."\n";
        }
        $xml .= '</api_list>';
        //new dBug($xml, "xml", true);
        file_put_contents(N2MY_APP_DIR."/logs/api/".$this->version."/api_info.xml", $xml);

        // レスポンス情報
        $xml = '<api_list>'."\n";
        foreach ($this->response_xml as $key => $group) {
            $xml .= '    <group name="'.$this->group[$key]["name"].'" path="'.$key.'">' ."\n";
            foreach ($group as $methods) {
                foreach ($methods as $method) {
                    $xml .= $method;
                }
            }
            $xml .= '    </group>'."\n";
        }
        $xml .= '</api_list>';
        //new dBug($xml, "xml", true);
        file_put_contents(N2MY_APP_DIR."/logs/api/".$this->version."/response_info.xml", $xml);
        print "APIの仕様書をログファイルに出力しました。";
    }

    /**
     * 経過時間取得
     */
    function _get_passage_time($reset = false) {
        static $start_time;
        if (!isset($start_time) || $reset == true) {
            list($micro, $sec) = split(" ", microtime());
            $start_time = $micro + $sec - 0.00001;
        }
        list($micro, $sec) = split(" ", microtime());
        $passage_time = ($micro + $sec) - $start_time;
        return $passage_time;
    }
}
