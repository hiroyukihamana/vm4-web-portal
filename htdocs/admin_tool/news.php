<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker:
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:Hiroshi                                                      |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
*/

require_once('config/config.inc.php');
require_once("dbi/news.dbi.php");
require_once("classes/AppFrame.class.php");

define('NEWS_IMAGE_WIDTH_MAX', 500);
define('NEWS_IMAGE_HIGHT_MAX', 500);

class AppNews extends AppFrame {

    var $table = null;
    var $error = null;
    var $maintenance = 0;
    var $page_name = 'news.php';
    var $page_title = 'News Release Admin';
    var $file_name = 'news_test.html';
    var $path_name = 'ja/news';
    var $is = 'news';
    var $_lang = 1;
    var $_htdocs_path ='../ja/shared/include/';
    var $_template_path ='../../../templates/ja/user/include/';

    function init () {
        $_COOKIE["lang"] = "ja";
        $this->_sess_page = md5(__FILE__);
        $_lang = $this->request->get("lang");
        if ($_lang) {
            $this->session->set('lang', $_lang, $this->_sess_page);
        }
        $lang = $this->session->get('lang', $this->_sess_page);
        $this->table = new NewsTable($this->get_auth_dsn(), $this->maintenance, $lang);
    }

    function auth()
    {
        if ($_SESSION["admin_tool"]["auth"] == 1) {
            return;
        }
        $user_id = $_REQUEST["auth"]["user_id"];
        $user_pw = $_REQUEST["auth"]["user_pw"];
        if ($user_id && $user_pw) {
            $fp = fopen(".htpasswd", "r");
            if ($fp) {
                while(!feof($fp)){
                    $line = trim(fgets($fp, 4096));
                    list($id, $pass) = split(",", $line);
                    if($id == $user_id && $pass == sha1($user_pw)){
                        $_SESSION["admin_tool"]["auth"] = 1;
                        return;
                    }
                }
            }
        }
        print <<<EOM
<form action="" method="post">
<table>
    <tr>
        <td>Admin ID:</td>
        <td><input type="text" name="auth[user_id]" value="$user_id"/></td>
    </tr><tr>
        <td>Admin PW:</td>
        <td><input type="password" name="auth[user_pw]" value="" /></td>
    </tr><tr>
        <td></td>
        <td><input type="submit" value="OK" /></td>
    </tr>
</table>
</form>
EOM;
        exit;
    }

    function default_view() {
        $this->action_top();
    }
    //ニュースの一覧、メニューの表示
    function action_top($message = '') {
        $this->_getList();
        $this->template->assign('folder_name', $this->path_name);
        $this->template->assign('message', $message);
        $this->template->assign('new_form', 0);
        $this->template->assign('page_name', $this->page_name);
        $this->display('admin_tool/news.t.html');
    }

    //新規ニュースのフォーム表示
    function action_new($message = "") {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $this->template->assign('message', $message);
        $news_info = $this->session->get('news_info');
        $this->template->assign('news_info', $news_info);
        $this->template->assign('new_form', 1);
        $this->template->assign('page_name', $this->page_name);
        $this->_getList();
        $this->display('admin_tool/news.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
    //ＤＢからリスト取得、表示生成
    function _getList(){
    }

    //確認ぺーじ
    function action_confirm($del = ''){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        if($del != 'del'){
            $this->session->set('news_info', $_POST);
        }

        $news_info = $this->session->get('news_info');

        $message = '';
        if(!$news_info['date']){
            $message .= NEWS_ERROR_DATE . "<br>";
        }
        if(!$news_info['title']){
            $message .= NEWS_ERROR_TITLE . "<br>";
        }
        if(!$news_info['contents']){
            $message .= NEWS_ERROR_CONTENTS . "<br>";
        }
        if($message){
            $this->action_new($message);
        }
        $this->template->assign('del', $del);
        $this->template->assign('news_info', $news_info);
        $this->template->assign('page_name', $this->page_name);
        $this->display('admin_tool/news_confirm.t.html');
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    //ＤＢにインサート
    function action_complete(){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $news_info = $this->session->get('news_info');
        $this->table->complete($news_info, $_REQUEST['id']);
        $this->action_top();
        $this->session->remove('news_info');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    //編集
    function action_edit() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $where = "news_key = ".$_REQUEST['id'];
        $ret = $this->table->select($where);
        $news = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $info['id'] = $news['news_key'];
        $info['date'] = $news['news_date'];
        $info['title'] = $news['news_title'];
        $info['contents'] = $news['news_contents'];

        $this->session->remove('news_info');
        $this->session->set('news_info', $info);

        $this->action_new();

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    //削除確認
    function action_deleteconfirm() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $where = "news_key = ".$_REQUEST['id'];
        $ret = $this->table->select($where);
        $news = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $info['id'] = $news['news_key'];
        $info['date'] = $news['news_date'];
        $info['title'] = $news['news_title'];
        $info['contents'] = $news['news_contents'];

        $this->session->remove('news_info');
        $this->session->set('news_info', $info);

        $this->action_confirm('del');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    //削除
    function action_delete(){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $this->table->newsDelete($_REQUEST['id']);
        $this->action_top();
        $this->session->remove('news_info');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
    //表示ステータスの変更
    function action_status(){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $this->table->changeStatus($_REQUEST['id'],$_REQUEST['type']);
        $this->action_top();

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
    //リリース
    function action_release() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $ret = $this->table->getReleaseList();
        $i = 0;
        $page_content = '';
        while($news = $ret->fetchRow(DB_FETCHMODE_ASSOC))
        {
            $news_date = ereg_replace('-', '.', $news['news_date']);
            $page_content .= <<<_EOT_
            <tr>
            <th scope="row">${news_date}</th>
            <td><a href="/{$this->path_name}/?action_desc&amp;id=${news['news_key']}">${news['news_title']}</a></td>
            </tr>
_EOT_;
            $title[$i] = $news['news_title'];
            $date[$i] = $news_date;
            $id[$i] = $news['news_key'];
            $i++;
        }
        //$page_content = mb_convert_encoding($page_content, "UTF-8", "EUC-JP");
        $filename = $this->_template_path.$this->is.'.html';
        $back_filename = $this->_template_path.'back_'.$this->is.'.html';

        $fp = fopen($filename, "r");
        $backup = fread($fp, filesize($filename));
        fclose($fp);

        $fp = fopen($back_filename, "w");
        $write = fputs($fp, $backup);
        fclose($fp);

        $fp = fopen($filename, "w");
        $write = fputs($fp, $page_content);
        fclose($fp);

        ////////////////////////////////////////
        //for top-page ssi include
        $page_content = '';
        for($i=0; $i< 5; $i++){
            $page_content .= <<<_EOT_
            <dt>${date[$i]}</dt>
            <dd><a href="/{$this->path_name}/?action_desc&amp;id={$id[$i]}">${title[$i]}</a></dd>
_EOT_;
            }
        //////////// ＰＨＰ側のテンプレート生成
        $filename = $this->_template_path.$this->is.'_index.t.html';
        $back_filename = $this->_template_path.$this->is.'_back.t.html';

        $fp = fopen($filename, "r");
        $backup = fread($fp, filesize($filename));
        fclose($fp);

        $fp = fopen($back_filename, "w");
        $write = fputs($fp, $backup);
        fclose($fp);

        $fp = fopen($filename, "w");
        $write = fputs($fp, $page_content);
        fclose($fp);
        /////////////HTML側のｈｔｍｌファイル生成
        $page_content = mb_convert_encoding($page_content, "UTF-8", "EUC-JP");

        $filename = $this->_htdocs_path.$this->is.'_index.html';
        $back_filename = $this->_htdocs_path.$this->is.'_back.html';

        $fp = fopen($filename, "r");
        $backup = fread($fp, filesize($filename));
        fclose($fp);

        $fp = fopen($back_filename, "w");
        $write = fputs($fp, $backup);
        fclose($fp);

        $fp = fopen($filename, "w");
        $write = fputs($fp, $page_content);
        fclose($fp);
        ///////////////////////////////////

        $this->action_top('<br><h3>'.NEWS_RELEASED.'</h3><br>');
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
    //テストページ生成
    function action_test() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $ret = $this->table->getReleaseList();
        $page_content .= '<html><head></head><body><table  border="0" cellspacing="0" cellpadding="0">';
          while($news = $ret->fetchRow(DB_FETCHMODE_ASSOC))
          {
            $title = $news['news_title'];
            $date = ereg_replace('-', '.', $news['news_date']);
            $contents = substr($news['news_contents'], 0, 50);
            $id = $news['news_key'];
            $page_content .= <<<_EOT_
                <tr>
                <td width="15" valign="top"><img src="/ja/images/ptr_01.gif" alt="" width="11" height="11" class="mg02"></td>
                <td width="60" valign="top" nowrap>
                <div class="date j3" style=" margin-right: 6px; "> ${date}</div>
                </td>
                <td valign="top" class="j3"><a href="/{$this->path_name}/?action_desc&amp;id={$id}">${title}</a></td>
                </tr>
_EOT_;
          }
        $page_content .= "</table><br></body></html>";
        $page_content = mb_convert_encoding($page_content, "UTF-8", "EUC-JP");
        $fp = fopen($this->file_name, "w");
        $write = fputs($fp, $page_content);
        fclose($fp);

        $this->action_top('<br><h3>'.NEWS_TEST_RELEASED.'</h><br>');
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    function action_back() {
       $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

       $filename = $this->_template_path.$this->is.'.html';
        $back_filename = $this->_template_path.'back_'.$this->is.'.html';

        $fp = fopen($back_filename, "r");
        $backup = fread($fp, filesize($back_filename));
        fclose($fp);

        $fp = fopen($filename, "w");
        $write = fputs($fp, $backup);
        fclose($fp);

        //PHP側のテンプレートのバックアップの復元
        $filename = $this->_template_path.$this->is.'_index.t.html';
        $back_filename = $this->_template_path.$this->is.'_back.t.html';

        $fp = fopen($back_filename, "r");
        $backup = fread($fp, filesize($back_filename));
        fclose($fp);

        $fp = fopen($filename, "w");
        $write = fputs($fp, $backup);
        fclose($fp);

        //HTML側のテンプレートの復元
        $filename = $this->_htdocs_path.$this->is.'_index.html';
        $back_filename = $this->_htdocs_path.$this->is.'_back.html';

        $fp = fopen($back_filename, "r");
        $backup = fread($fp, filesize($back_filename));
        fclose($fp);

        $fp = fopen($filename, "w");
        $write = fputs($fp, $backup);
        fclose($fp);

       $this->action_top('<br><h3>'.NEWS_BACK_RELEASED.'</h3><br>');
       $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    function action_uploadpage() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $this->template->assign('page_name', $this->page_name);
        $this->display('admin_tool/news_upload.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    function action_upload() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $userfile = $_FILES['userfile']['tmp_name'];
        $userfile_name = $_FILES['userfile']['name'];
        $userfile_size = $_FILES['userfile']['size'];
        $userfile_type = $_FILES['userfile']['type'];
        $userfile_error = $_FILES['userfile']['error'];
        // userfile_error was introduced at PHP 4.2.0
        // use this code with newer versions
        if ($userfile_error > 0) {
            $message .= 'Problem: ';
            switch ($userfile_error) {
                case 1 :
                    $message .= 'File exceeded upload_max_filesize';
                    break;
                case 2 :
                    $message .= 'File exceeded max_file_size';
                    break;
                case 3 :
                    $message .= 'File only partially uploaded';
                    break;
                case 4 :
                    $message .= 'No file uploaded';
                    break;
            }
        }
        // end of code for 4.2.0

        $ext = substr($userfile_name, -4);
        //$message .= $ext.'<br/>';
        // one more check: does the file have the right MIME type?

        if ($userfile_type != 'image/gif' && $userfile_type != 'image/pjpeg' && $userfile_type != 'image/jpeg' && $userfile_type != 'application/pdf') {
            $message .= 'NEWS_ERROR_FILE_TYPE';

        }

        list ($width, $height, $type, $attr) = getimagesize($userfile);

        if ($width > NEWS_IMAGE_WIDTH_MAX) {
            $message .= NEWS_ERROR_WIDTH.NEWS_IMAGE_WIDTH_MAX;

        }
        elseif ($height > NEWS_IMAGE_HIGHT_MAX) {
            $message .= NEWS_ERROR_HEIGHT.NEWS_IMAGE_HIGHT_MAX;
        }
        // ファイルのアップロード先
        $upfile = '../../'.$this->path_name.'/uploads/'.$userfile_name;
        // is_uploaded_file and move_uploaded_file added at version 4.0.3
        if (is_uploaded_file($userfile)) {
            if (!move_uploaded_file($userfile, $upfile)) {
                $message .= 'Problem: Could not move file to destination directory';

            }
        } else {
            $message .= 'Problem: Possible file upload attack. Filename: '.$userfile_name;

        }

      if($message == ""){
        if ($userfile_type == 'image/gif' || $userfile_type == 'image/pjpeg' || $userfile_type == 'image/jpeg') {
            $message .= '<img src="/'.$this->path_name.'/uploads/'.$userfile_name.'"><br />';
            $message .= '<pre>Image file：　&lt;img src="/'.$this->path_name.'/uploads/'.$userfile_name.'" /&gt;</pre><br>';
        } else {
            $message .= '<a href="/'.$this->path_name.'/uploads/'.$userfile_name.'">PDF</a><br>';
            $message .= '<pre class=c>PDF：　&lt;a href="/'.$this->path_name.'/uploads/'.$userfile_name.'"&gt;file title&lt;/a&gt;</pre><br>';
        }

            $message .= NEWS_UPLOAD;
      }
        $this->template->assign('message', $message);
        $this->display('admin_tool/news_upload.t.html');
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
}

$main =& new AppNews();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>