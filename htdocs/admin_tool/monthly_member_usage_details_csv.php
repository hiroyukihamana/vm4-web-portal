<?php

// 環境設定
require_once ('classes/AppFrame.class.php');
// ユーザ情報
require_once ('classes/dbi/member_usage_details.dbi.php');

class AppMonthlyMemberUsageDetailsCSV extends AppFrame {

	var $_dsn = null;
	var $account_dsn = null;
	var $member_usage_details_obj = null;

	function init() {
		// DBサーバ情報取得
		if (file_exists(sprintf("%sconfig/slave_list.ini", N2MY_APP_DIR))) {
			$server_list = parse_ini_file(sprintf("%sconfig/slave_list.ini", N2MY_APP_DIR), true);
			$this -> _dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
			$this -> account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
		} else {
			$server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR), true);
			$this -> _dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
			$this -> account_dsn = $this -> config -> get("GLOBAL", "auth_dsn");
		}
		$this -> member_usage_details_obj = new DBI_MemberUsageDetails($this -> _dsn);
	}

	function default_view() {
		$this -> logger -> info(__FUNCTION__ . ' # MonthlyMemberUsageDetails Info Operation Start ... ', __FILE__, __LINE__);
		// 初期化
		$data = array();
		// パラメタ取得
		$target_month = $this -> request -> get('target_month');
		if (!$target_month) {
			$target_month = date("Y-m");
		}
		$this -> logger2 -> info("target month is " . $target_month);
		//$target_month = date('Y-m', time()); // 常に当月（当月に前月利用分の作成を行うため）
		// パラメタ確認
		if ($target_month) {
			//-------------------------------------------------//
			//当月１日集計分から前月２日分集計結果までを取り込む
			//-------------------------------------------------//
			$_aryMonth = split('[/.-]', $target_month);
			//デリミタは、"/",".","-"のいずれか。
			$_year = $_aryMonth[0];
			$_month = $_aryMonth[1];
			// 処理年月（請求）チェック・取得
			if (!$_month) {
				$_year = date("Y", time());
				//デフォルト当月
				$_month = date("m", time());
				//デフォルト当月
			}
			//対象時期（終）
			$_end_date = $_year . "-" . $_month . "-02 00:00:00";
			$_end_datetime = mktime(0, 0, 0, $_month, 1, $_year);
			//対象時期（始）
			$_start_date = date("Y-m-02 00:00:00", strtotime("-1 month", $_end_datetime));
			//-------------------------------------------------//

			// MTG利用統計用ユーザデータ（差分データ）作成
			$this -> mtg_users = array();
			$_tmp_data = null;
			$_tmp_data = $this -> _get_data_routine($_start_date, $_end_date);
			if (is_array($_tmp_data) === true) {
				$data = array_merge($data, $_tmp_data);
			}
		} else {
			$data = array();
		}

		$log_folder = N2MY_DOCUMENT_ROOT . "admin_tool/statistics_data/member_usage_details/";
		//TO CSV
		require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
		$csv = new EZCsv(true, 'SJIS', 'UTF-8');
		$csv_file = $log_folder . "member_usage_details.csv";
		$handle = fopen($csv_file, "w");
		$csv -> open($csv_file, "w");
		$header = $this -> get_csv_header();
		$csv -> setHeader($header);
		$csv -> write($header);
		if ($data) {
			foreach ($data as $_data) {
				$csv -> write($_data);
			}
		}
		$csv -> close();
		chmod($csv_file, 0777);
		fclose($handle);

		$this -> logger -> info(__FUNCTION__ . ' # MonthlyMemberUsageDetails Info Operation End ... ', __FILE__, __LINE__);
	}

	/**
	 * メイン取得
	 *
	 */
	private function _get_data_routine($start_date, $end_date) {
		$rows = array();
		$where = ' `create_datetime` < \'' . addslashes($end_date) . '\' ' . ' AND `create_datetime` > \'' . addslashes($start_date) . '\' ';
		$this -> logger2 -> info($where);
		$rs = $this -> member_usage_details_obj -> getRowsAssoc($where);
		foreach ($rs as $key => $val) {
			$rows[] = $val;
		}
		return $rows;
	}

	/**
	 * describe table in order to get table columns
	 */
	function get_csv_header() {
		$sql = "desc member_usage_details";
		$rows = array();
		$row = array();
		$rs = $this -> member_usage_details_obj -> _conn -> query($sql);
		if (DB::isError($rs)) {
			$this -> logger -> info(__FUNCTION__ . " # DBError", __FILE__, __LINE__, $sql);
			return false;
		} else {
			while ($row = $rs -> fetchRow(DB_FETCHMODE_ASSOC)) {
				$rows[$row['Field']] = $row['Field'];
			}
		}
		return $rows;
	}

}

$main = new AppMonthlyMemberUsageDetailsCSV();
$main -> execute();
?>