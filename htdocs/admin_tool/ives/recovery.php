<?php

require_once ('classes/AppFrame.class.php');
require_once("classes/polycom/Polycom.class.php");


class Recovery extends AppFrame {
  function default_view() {
    print $this->_header();
print <<<EOM
<form action="/admin_tool/ives/recovery.php" name="submit" method="post">
<input type="hidden" name="action_recovery" value="">
<input type="submit" name="submit" value="Execute">
</form>
EOM;
        print $this->_footer();
  }

  function action_recovery() {
    try {
      $polycom = new PolycomClass($this->get_dsn());
      $polycom->recoveryDid();
    }
    catch (Exception $e) {
      $this->logger2->error("Recoverty: ".$e);
      print "Recovery Error";
      exit;
    }
    print "Finished";
    exit;
  }

  function _header() {
        return <<<EOM
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Teleconference Did Recovery</title>
<style TYPE="text/css">
<!--
body {
text-align: center;
font-size: 12px;
font-family: "Arial, sans-serif, ＭＳ Ｐゴシック", Osaka, "ヒラギノ角ゴ Pro W3" ;
margin: 10px 0px 0px 0px;
}
.title {
font-size: 16px;
font-weight: bold;
margin: 20px auto 0px auto;
width: 760px
}
#content {
text-align: center;
}
#content table {
border-collapse: collapse;
border-spacing: 0;
empty-cells: show;
border: 1px solid #999999;
margin: 20px auto;
width: 760px
}
#content th {
border: 1px solid #999999;
padding: 4px 5px;
background-color: #EFEFEF;
text-align: left;
}
#content td {
border: 1px solid #999999;
padding: 4px 10px;
text-align: left;
}
.errorSection {
color: #FF0000;
}
-->
</style>
</head>
<body>
<div id="content">
<h1>Teleconference Did Recovery</h1>
EOM;
  }

  function _footer() {
     print "</body></html>";
  }
}

$main       = new Recovery();
$main->execute();