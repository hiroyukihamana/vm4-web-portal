<?php
ini_set("soap.wsdl_cache_enabled", "0");
set_time_limit(650);
$type = $_REQUEST["type"];
$login_id = $_REQUEST["login_id"];
$profileId = $_REQUEST["profileId"];
$wsdl = "http://ws.visioassistance.net/v2/soapAccountManager.wsdl";
$sipClient = new SoapClient($wsdl);
if ($type == "create_sip_account") {
    if (!$login_id || !$profileId) {
        return false;
    }
    try {
        $subscriber = array();
        $subscriber["callerid"] = "vcube"; //vcube
        $subscriber["title"] = "vcube"; //vcube
        $subscriber["gender"] = "M";
        $subscriber["lastName"] = "vcube"; //vcube
        $subscriber["firstName"] = "vcube-trunk"; //vcube
        $subscriber["dateOfBirth"] = "1998-10-09"; //vcube
        $subscriber["fax"] = "0355019676"; //vcube
        $subscriber["homePhone"] = "0357683111"; //vcube
        $subscriber["mobile"] = "0357683111"; //vcube

        //-が利用できないので_に置き換え
        $subscriber["login"] = $login_id; //room_key
        $subscriber["mail"] = "vsupport@vcube.co.jp"; //vsupport?
        $subscriber["userPassword"] = create_id().rand(2,9);//ランダム
        $subscriber["address_1"] = "Nakameguro GT Tower 20F"; //vcube
        $subscriber["address_2"] = "2-1-1 Kamimeguro, Meguro-ku"; //vcube
        $subscriber["zipCode"] = "1530051"; //vcube
        $subscriber["city"] = "Tokyo";
        $subscriber["country"] = "Japan";
        $subscriber["preferedLanguage"] = "English";
        $subscriber["acceptMailHtml"] = false;
        $subscriber["acceptMailInformation"] = false;
        $subscriber["acceptProfilePublic"] = false;

        $account = array();
        $account["state"] = "Actif";
        $account["creationDate"] = date("Y-m-d");
        $account["activityEndDate"] = "0000-00-00";
        $account["inactivityEndDate"] = "0000-00-00";
        $account["paymentDate"] = "0000-00-00";

        $createSubscriber = array();
        $createSubscriber["subscriber"] = $subscriber;
        $createSubscriber["account"] = $account;
        //以下のデータは固定
        $createSubscriber["profileId"] = $profileId;
        $createSubscriber["roleId"] = 1;
        $createSubscriber["optionId"] = 127;
        $createSubscriber["customerId"] = 0;

        try {
            $result = $sipClient->createSubscriber($createSubscriber);
        } catch (SoapFault $fault) {
            throw $fault;
        }
        print $result->uid;
    } catch (SoapFault $e) {
        error_log(__FUNCTION__."#soap fault",__FILE__,__LINE__);
        print false;
    } catch (Exception $e) {
        error_log(__FUNCTION__."#exception",__FILE__,__LINE__);
        print false;
    }
} else if ($type == "get_sip_info") {
   $uid = $_REQUEST["uid"];
    if (!$uid) {
        return false;
    }
    try {
        $params = array("uid" => $uid);
        $result = $sipClient->getSipInformation($params);
        $accountInfo = array(
                "uid"	=> $result->uid,
                "name"	=> $result->name,
                "secret"=> $result->secret
            );
        print serialize($accountInfo);
    } catch (SoapFault $fault) {
        error_log(__FUNCTION__."#soap fault",__FILE__,__LINE__);
        print false;
    }
} else if ($type == "delete_sip_account") {
   $uid = $_REQUEST["uid"];
    if (!$uid) {
        print false;
    }
    try {
        $params = array("uid" => $uid);
        $result = $sipClient->deleteSubscriber($params);
    } catch (SoapFault $e) {
        error_log(__FUNCTION__."#soap fault",__FILE__,__LINE__);
        print false;
    }
    print true;
}

/**
 * 乱数ID発行
 */
function create_id() {
    list($a, $b) = explode(" ", microtime());
    $a = (int)($a * 100000);
    $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
    $c = $a.$b;
    // 36
    //print (base_convert($c, 10, 36));
    // 62
    return dec2any($c);
}

/**
 * 数値から62進数のID発行
 */
function dec2any( $num, $base=57, $index=false ) {
    if (! $base ) {
        $base = strlen( $index );
    } else if (! $index ) {
        $index = substr(
                '23456789ab' .
                'cdefghijkm' .
                'nopqrstuvw' .
                'xyzABCDEFG' .
                'HJKLMNPQRS' .
                'TUVWXYZ' ,0 ,$base );
    }
    $out = "";
    for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
        $a = floor( $num / pow( $base, $t ) );
        $out = $out . substr( $index, $a, 1 );
        $num = $num - ( $a * pow( $base, $t ) );
    }
    return $out;
}
