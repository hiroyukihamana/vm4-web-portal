<?php

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/dbi/meeting_extension_use_log.dbi.php");
require_once("classes/dbi/service.dbi.php");
require_once("classes/dbi/user_service_option.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once("lib/EZLib/EZUtil/EZCsv.class.php");

class AppMember extends AppFrame {

    var $_auth = null;
    var $_sess_page;
    var $account_dsn = null;

    function init() {
        $_COOKIE["lang"] = "en";
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
    }

    public function auth()
    {
    }

    public function default_view()
    {
        $this->action_top();
    }

    public function action_top2()
    {
        require_once( "classes/N2MY_IndividualAccount.class.php" );
        $accountClass = new N2MY_IndividualAccount( $this->get_dsn(), $this->account_dsn );
        $request = $this->request->getAll();

        $serviceStartYear = "2008";
        $thisYear = $request["year"] ? $request["year"] : date( "Y" );
        while( $serviceStartYear <= $thisYear ){
            $info["yearlist"][$serviceStartYear] = $serviceStartYear;
            $info["year"] = $serviceStartYear;
            $serviceStartYear += 1;
        }
        for( $i = 1; $i <= 12; $i++ ){
            $info["monthlist"][sprintf( "%02d", $i )] = sprintf( "%02d", $i );
        }
        $info["month"] = $request["month"] ? sprintf( "%02d", $request["month"] ) : date( "m" );
        $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        $info["userList"] = array();

        $header = $accountClass->getHeader();
        $csv = new EZCsv();
        $csv->setHeader( $header );
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="'.date("Ymd").'.csv"');
        $csv->write( $header );

        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            $objUser		= new UserTable( $dsn );
            $objMember		= new MemberTable( $dsn );
            $objMeeting		= new MeetingTable( $dsn );
            $objService		= new ServiceTable( $dsn );
            $objUserServiceOption		= new UserServiceOptionTable( $dsn );

            $objParticipant = new DBI_Participant( $dsn );
            $objMeetingdb	 = new N2MY_DB( $dsn );
            $objMGMdb		 = new N2MY_DB( $this->account_dsn );

            //メンバー課金ユーザー毎のユーザー情報（会社情報）と部屋情報、会議情報を取得
            $sql = "
SELECT
    u.user_id,
    u.user_key,
    u.max_rec_size,
    u.user_company_name,
    u.user_company_postnumber,
    u.user_company_address,
    u.user_company_phone,
    u.user_company_fax,
    u.user_starttime,
    u.user_registtime,
    u.user_updatetime,
    u.user_deletetime,
    u.service_key,
    u.user_plan_yearly,
    u.user_plan_yearly_starttime,
    u.user_plan_starttime,
    u.room_name,
    m.meeting_key,
    m.room_key,
    m.meeting_room_name,
    m.meeting_max_seat,
    m.meeting_size_used,
    m.meeting_log_owner,
    m.meeting_start_datetime,
    m.meeting_stop_datetime,
    m.meeting_extended_counter,
    m.has_recorded_minutes,
    m.has_recorded_video,
    m.is_active,
    m.is_reserved,
    m.create_datetime,
    m.update_datetime,
    m.use_flg,
    m.actual_start_datetime,
    m.actual_end_datetime,
    m.meeting_use_minute
FROM
(
    SELECT
        u.*, r.room_key, r.room_name
    FROM
    (
        SELECT
            u.*, up.service_key, up.user_plan_yearly, up.user_plan_yearly_starttime, up.user_plan_starttime
        FROM
            user_plan up
        LEFT JOIN
            user u
        ON
            up.user_key = u.user_key
        WHERE
            up.user_plan_status = 1 AND
            DATE_FORMAT( up.user_plan_starttime, '%Y-%m' ) <= '" . $info["year"]. "-" . $info["month"] . "'
    ) u
    LEFT JOIN
        room r
    ON
        u.user_key = r.user_key
    WHERE
        u.account_model = 'member' AND
        u.user_status=1 AND
        ( u.user_delete_status = 0 OR
        ( u.user_delete_status = 2 AND DATE_FORMAT( u.user_deletetime, '%Y-%m' ) >= '". $info["year"]. "-" . $info["month"] ."'"." ) )
) u
LEFT JOIN
    meeting m
ON
    u.room_key = m.room_key
WHERE
    m.use_flg=1 AND
    DATE_FORMAT( m.actual_end_datetime, '%Y-%m' ) = '". $info["year"]. "-" . $info["month"] ."' AND
    m.meeting_use_minute >= 1
ORDER BY
    u.user_key, m.meeting_key";

                    $rs_userList = $objMeetingdb->_conn->query( $sql );

                    /**
                     * ・meeting.serviceからserviceを取得
                     * ・mgmからplanを取得
                     * ・meeting.user_service_optionからoptionを取得
                     * ・meeting_keyからparticipantのリストをだしmemberとひもづける。
                     */
                    while( $ret_userInfo = $rs_userList->fetchRow( DB_FETCHMODE_ASSOC ) ){
                        $planSql = sprintf( "
SELECT
    service_key, service_name
FROM
    service
WHERE
    service_key='%s'", $ret_userInfo["service_key"] );
                        $rs_planInfo = $objMGMdb->_conn->query( $planSql );
                        $planInfo = $rs_planInfo->fetchRow( DB_FETCHMODE_ASSOC );
                        $ret_userInfo["service_name"] = $planInfo["service_name"];

                        //導入サポートの取得
                        $where = "user_key='" . $ret_userInfo["user_key"] . "' AND user_service_option_status=1 AND service_option_key = 14 AND DATE_FORMAT( user_service_option_starttime, '%Y-%m' ) = '" . $info["year"]. "-" . $info["month"] . "'";
                        $ret_userInfo["support"] = 	$objUserServiceOption->numRows( $where );

                        //シェアリング、容量追加オプションの取得
                        $optionList = 	$objUserServiceOption->getRowsAssoc( sprintf( "user_key='%s' AND user_service_option_status=1 AND service_option_key != 14", $ret_userInfo["user_key"] ) );
                        $desktop_share = 0;
                        $hdd_extension = 0;
                        for( $i = 0; $i < count( $optionList ); $i++ ){
                            switch( $optionList[$i]["service_option_key"] ){
                                case "3":
                                    $desktop_share = 1;
                                    break;

                                case "4":
                                    $hdd_extension += 1; //spellどうすっか
                                    break;
                            }
                        }
                        $ret_userInfo["desktop_share"] = $desktop_share;
                        $ret_userInfo["hdd_extension"] = $hdd_extension;

                        $sql = sprintf( "
SELECT
    p.participant_key,
    p.participant_name,
    p.participant_station,
    p.uptime_start,
    p.uptime_end,
    p.use_count,
    m.member_id,
    m.member_name,
    m.member_name_kana,
    m.room_key
FROM
    participant p
LEFT JOIN
    member m
ON
    p.member_key = m.member_key
WHERE
    meeting_key = '%d'
ORDER BY
    m.member_key
", $ret_userInfo["meeting_key"] );

                        $rs_memberList = $objMeetingdb->_conn->query( $sql );
                        $memberList = array();

                        while( $ret_memberInfo = $rs_memberList->fetchRow( DB_FETCHMODE_ASSOC ) ){
                                $ret_userInfo["participant_key"] = $ret_memberInfo["participant_key"];
                                $ret_userInfo["participant_name"] = $ret_memberInfo["participant_name"];
                                $ret_userInfo["participant_station"] = $ret_memberInfo["participant_station"];
                                $ret_userInfo["uptime_start"] = $ret_memberInfo["uptime_start"];
                                $ret_userInfo["uptime_end"] = $ret_memberInfo["uptime_end"];
                                $ret_userInfo["use_count"] = $ret_memberInfo["use_count"];
                                $ret_userInfo["member_id"] = $ret_memberInfo["member_id"];
                                $ret_userInfo["member_name"] = $ret_memberInfo["member_name"];
                                $ret_userInfo["member_name_kana"] = $ret_memberInfo["member_name_kana"];
                                $ret_userInfo["room_key"] = $ret_memberInfo["room_key"];

                        // CSV出力
                        $csv->write($ret_userInfo);
                        }
                    }
        }
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        exit;
    }

    public function action_show_detail()
    {
        $request = $this->request->getAll();
        //memberデータ
        $objMember = new MemberTable( $request["dsn"] );
        $objMeeting = new MeetingTable( $request["dsn"] );
        $objParticipant = new DBI_Participant( $request["dsn"] );
        $info["memberInfo"] = $objMember->getRow( sprintf( "member_id='%s'", mysql_real_escape_string($request["member_id"]) ) );

        $sql = "
SELECT
m.*, p.*
FROM
(
SELECT
    meeting_key, SUM( use_count ) as use_count
FROM
    participant
WHERE
    member_key= ". $info["memberInfo"]["member_key"] . "
GROUP BY
    meeting_key
) p
LEFT JOIN
    meeting m
ON
    p.meeting_key = m.meeting_key
WHERE
m.use_flg = 1 AND
m.is_active = 0 AND
DATE_FORMAT( m.actual_end_datetime, '%Y-%m' ) = '" . $request["year"]. "-" . $request["month"] ."'
";
            $ret = $objParticipant->_conn->query( $sql );
            if (DB::isError($ret)) {
                $this->logger2->error($ret->getUserInfo());
                exit;
            }

            while( $meetingInfo = $ret->fetchRow( DB_FETCHMODE_ASSOC ) ) {
                $where = sprintf( "meeting_key='%s'", $meetingInfo["meeting_key"] );
                $meetingInfo["participantList"] = $objParticipant->getRowsAssoc( $where, null, null, null, "DISTINCT( participant_name ) as participant_name" );
                $info["meetingList"][] = $meetingInfo;
                print_r($meetingInfo);echo"<br><br>";
            }
        $this->template->assign( "info", $info );
        $this->display( "admin_tool/member_detail2.t.html" );
    }

    public function action_top()
    {
        require_once( "classes/N2MY_IndividualAccount.class.php" );
        require_once( "classes/dbi/member_usage_details.dbi.php" );
        $serverini = "../../config/server_list.ini";
        $serverlist = parse_ini_file( $serverini, true );
        $dsn = $serverlist["SERVER_LIST"]["data1"];

        $accountClass = new N2MY_IndividualAccount( $dsn, $this->account_dsn );
        $objMemberUsageDetails = new DBI_MemberUsageDetails( $dsn );

        $request = $this->request->getAll();
        if( $request["year"] && $request["month"] ){
            $date["year"]	= $request["year"];
            $date["month"]	= $request["month"];
        } else {
            $lastdate = strtotime( "last Month" );
            $date["year"] = date( "Y", $lastdate );
            $date["month"] = date( "m", $lastdate );
        }
        $date["now"] = sprintf( "%04d-%02d", $date["year"], $date["month"] );

        $where = "date_format( create_datetime, '%Y-%m') = '" . $date["now"] . "'" ;
        $sort = array(
                    "user_key"=>'ASC',
                    "meeting_key"=>'ASC',
                    "participant_key"=>'ASC',
                    "member_key"=>'ASC',
                    );
        $list = $objMemberUsageDetails->getRowsAssoc( $where, $sort );

        $header = $accountClass->getHeader();
        $csv = new EZCsv();
        $csv->setHeader( $header );
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="'.date("Ymd").'.csv"');
        $csv->write( $header );
        for( $i = 0; $i < count( $list ); $i++ ){
            $data = array();
            foreach( $header as $key => $value ){
                $data[$key] = $list[$i][$key];
            }
            $csv->write( $data );
        }
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        exit;
    }
}


$main =& new AppMember();
$main->execute();
