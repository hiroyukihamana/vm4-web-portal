<?php
require_once("classes/AppFrame.class.php");
require_once("lib/EZLib/Transit.class.php");

class TransitTest extends AppFrame {

    var $obj_Transit = null;

    function init() {
        $uri = $this->config->get("N2MY", "transit_uri");
//        $transit_log = realpath(dirname(__FILE__)."/../../logs/")."/transit.log";
        $transit_log = "";
        $this->obj_Transit = Transit::factory($uri, $transit_log);
    }

    function auth() {

    }

    function action_add() {
        $list = $this->session->get("station_list");
        $station_list = $this->request->get("station_list");
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$station_list);
        if (is_array($station_list)) {
            foreach($station_list as $station) {
                $list[] = $station;
            }
        }
        $this->session->set("station_list", $list);
        header("Location: transit.php");
    }
/*
    function action_stress_test() {
        for($i = 0; $i < 500; $i++) {
            $data[] = "http://ecometer.vcube.net/expwww2/exp.cgi?val_from=%8B%FA%98H&val_to=%8B%F8%8A%D4&val_htmb=jcgi_details2&val_max_result=1&val_sorttype=2&val_co2mode=1";
        }
        $ret = $this->multiRequest($data);
        print "<pre>";
        print_r(array($data, $ret));
    }
*/
    function action_end_station() {
        $end_station = $this->request->get("station");
        $this->session->set("end_station", $end_station);
        header("Location: transit.php");
    }

    function action_clear_end_point() {
        $this->session->remove("end_station");
        header("Location: transit.php");
    }

    function action_clear() {
        $this->session->remove("station_list");
        header("Location: transit.php");
    }

    function action_delete() {
        $list = $this->session->get("station_list");
        $indexes = $this->request->get("index");
        foreach($indexes as $index) {
            unset($list[$index]);
        }
        $this->session->set("station_list", $list);
        header("Location: transit.php");
    }

    function action_transit() {
        set_time_limit(1000);
        $this->default_view();
        print "<hr>";
        $station_list = $this->session->get("station_list");
        $end_station = $this->session->get("end_station");
        // ２拠点以上
        if ((count($station_list) > 1) || ($station_list > 0 && $end_station)) {
            $sort = $this->request->get("sort");
            $short_route = $this->obj_Transit->getMultiStationRoute($station_list, $end_station, $sort, true);
            $shortest_route = $short_route["shortest_route"];
            print "<br>";
            print "<fieldset>";
            print '<legend>shortest route</legend>';
            print '<b><font color="blue">meeting station：'.$short_route["shortest_route"]["end"]."<br />";
            print 'execute count：'.$short_route["total"]["exec_count"]."times<br />";
            print 'execute time：'.$short_route["total"]["exec_time"]."seconds<br />";
            print '</font></b>';
            $this->view_routes($shortest_route);
            print "</fieldset>";
            print "<hr>";
            print "<fieldset>";
            print '<legend>route search result</legend>';
            foreach ($short_route["routes"] as $routes) {
                print "<h2>last station：".$routes["end"]."</h2>";
                $this->view_routes($routes);
            }
            print "</fieldset>";
            print '<textarea style="width:100%;" rows="20" readonly="readonly">';
            print_r($short_route);
            print "</textarea>";
        }
    }

    function action_route() {
        set_time_limit(1000);
        $this->default_view();
        print "<hr>";
        $from = $this->request->get("from");
        $to = $this->request->get("to");
        $option = array(
            "sort" => $this->request->get("sort"),
            "count" => $this->request->get("count", 5)
            );
        $ret = $this->obj_Transit->getStationRoute($from, $to, $option);
        print "<pre>";
        foreach($ret as $key => $routes) {
            $route = $routes["route"];
            print '<h2>alternate'.($key+1).'</h2>';
            print "<table border=1>\n";
            print "<colgroup>" .
                    "<col width=50 />" .
                    "<col width=50 />" .
                    "<col width=50 />" .
                    "<col width=50 />" .
                    "</colgroup>\n";
            print '<tr bgcolor="#ff9999">' .
                    "<th>distance(Km)</th>" .
                    "<th>time(min)</th>" .
                    "<th>cost(JPY)</th>" .
                    "<th>CO2(g)</th>" .
                    "<th>route</th>" .
                    "</tr>\n";
            print '<tr>' .
                    '<td>'.$routes["summary"]["move"]["move"].'</td>' .
                    '<td>'.$routes["summary"]["move"]["minute"].'</td>' .
                    '<td>'.$routes["summary"]["fare"]["fareunit"]["unit"].'</td>' .
                    '<td>'.$routes["summary"]["move"]["co2"].'</td>';
            print '<td>';
            foreach($route as $section) {
                print '<font color="#ff6666">▼'.$section["from"]["name"]." =&gt; ".$section["to"]["name"]."</font><br />";
                print 'distance:'.$section["move"]["move"].'km ';
                print 'time:'.$section["move"]["minute"].'min ';
                print 'cost:'.$section["fare"]["fareunit"]["unit"].'JPY ';
                print 'CO2:'.$section["move"]["co2"].'g<br />';
            }
            print '</td>';
            print "</tr>\n";
            print "</table>";
        }
    }

    /**
     *
     */
    function view_routes($routes) {
        print "<table border=1>\n";
        print "<colgroup>" .
                "<col width=200 />" .
                "<col width=50 />" .
                "<col width=50 />" .
                "<col width=50 />" .
                "<col width=50 />" .
                "</colgroup>\n";
        print '<tr bgcolor="#ff9999">' .
                "<th>departure station</th>" .
                "<th>distance(Km)</th>" .
                "<th>time(min)</th>" .
                "<th>cost(JPY)</th>" .
                "<th>CO2(g)</th>" .
                "<th>route</th>" .
                "</tr>\n";
        print '<tr bgcolor="#ffcccc">' .
                "<th>Total</th>" .
                "<th>".$routes["total"]["move"]."</th>" .
                "<th>".$routes["total"]["time"]."</th>" .
                "<th>".$routes["total"]["fare"]."</th>" .
                "<th>".$routes["total"]["co2"]."</th>" .
                "<th>&nbsp;</th>" .
                "</tr>\n";
        foreach($routes["start"] as $station => $route) {
            print '<tr>' .
                    '<td>'.$station.'</td>' .
                    '<td>'.$route["summary"]["move"]["move"].'</td>' .
                    '<td>'.$route["summary"]["move"]["minute"].'</td>' .
                    '<td>'.$route["summary"]["fare"]["fareunit"]["unit"].'</td>' .
                    '<td>'.$route["summary"]["move"]["co2"].'</td>';
            print '<td>';
            foreach($route["route"] as $section) {
                print '<font color="#ff6666">▼'.$section["from"]["name"]." =&gt; ".$section["to"]["name"]."</font><br />";
                print 'distance:'.$section["move"]["move"].'km ';
                print 'time:'.$section["move"]["minute"].'min ';
                print 'cost:'.$section["fare"]["fareunit"]["unit"].'JPY ';
                print 'CO2:'.$section["move"]["co2"].'g<br />';
            }
            print '</td>';
            print "</tr>\n";
        }
        print '<tr bgcolor="#ffcccc">' .
                "<th>Total</th>" .
                "<th>".$routes["total"]["move"]."</th>" .
                "<th>".$routes["total"]["time"]."</th>" .
                "<th>".$routes["total"]["fare"]."</th>" .
                "<th>".$routes["total"]["co2"]."</th>" .
                "<th>&nbsp;</th>" .
                "</tr>\n";
        print "</table>";
    }

    function default_view() {
print <<< DOC_END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<script src="../ja/shared/services/js/prototype.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
<script type="text/javascript">
<!--
    function check(frm, flg) {
        for(i = 0; i < frm.elements.length; i++) {
            if (frm.elements[i].name == "index[]") {
                if (flg) {
                    frm.elements[i].checked = true;
                } else {
                    frm.elements[i].checked = false;
                }
            }
        }
    }
//-->
</script>
</head>
<body>
DOC_END;
        $keyword = $this->request->get("station");
        $end_key = $this->request->get("end_station");

        $station_list = $this->session->get("station_list");
        $end_station = $this->session->get("end_station");

        print '<h1>search all route</h1>';
        print '<h3>search by station</h3>';
        print '<form method="post" action="transit.php">';
        print '<table><tr valign="top">';
        print '<td><input type="text" name="station" value="'.htmlspecialchars($keyword).'"/></td>';
        print '<td><input type="submit" value="search" /></td>';
        if ($keyword) {
            print '<td> =&gt; </td>';
            $ret = $this->obj_Transit->getStationList($keyword);
            print '<td><select size="5" name="station_list[]" multiple="multiple">';
            foreach($ret as $key => $station) {
                print '<option value="'.htmlspecialchars($key).'">'.htmlspecialchars($station).'</option>';
            }
            print '</select></td>';
            print '<td><input type="submit" name="action_add" value="add"/></td>';
        }
        print '</tr></table></form>';

        print '<h3>meeting spot</h3>';
        print '<form method="post" action="transit.php">';
        print '<table><tr valign="top">';
        print '<td><input type="text" name="end_station" value="'.htmlspecialchars($end_key).'"/></td>';
        print '<td><input type="submit" value="search" /></td>';
        if ($end_key) {
            print '<td> =&gt; </td>';
            $ret = $this->obj_Transit->getStationList($end_key);
            print '<td><select size="5" name="station">';
            foreach($ret as $key => $station) {
                print '<option value="'.htmlspecialchars($key).'">'.htmlspecialchars($station).'</option>';
            }
            print '</select></td>';
            print '<td><input type="submit" name="action_end_station" value="setting"/></td>';
        }
        print '</tr></table></form>';
        print '<font color="red">If leave meeting spot empty, it will automatically get the shortest route, and with 50(max) place.</font>';

        if ($station_list) {
            print "<hr><h3>route info</h3>";
            print '<form method="post" action="transit.php" name="frmList">';
            if ($end_station) {
                print "meeting station：".$end_station;
                print '<input type="submit" name="action_clear_end_point" value="クリア" /> ';
                print "<br />";
            }
            print count($station_list)." items<br />";
            print '<input type="button" value="select all" onclick="check(this.form,1);"/> ';
            print '<input type="button" value="deselect all" onclick="check(this.form,0);" /> ';
            print '<input type="submit" name="action_delete" value="delect by station" /> ';
            print '<input type="submit" name="action_transit" value="search route" /><br /><br />';
            $sort = $this->request->get("sort");
            $sort_list = array(
                "fare" => "cost",
                "time" => "time",
                "co2" => "CO2",
                );
            print 'sort：<select name="sort">';
            foreach ($sort_list as $key => $val) {
                if ($key == $sort) {
                    print '<option value="'.$key.'" selected="selected">'.$val.'</option>';
                } else {
                    print '<option value="'.$key.'">'.$val.'</option>';
                }
            }
            print '</select> ';
            $count = ($_REQUEST["count"]) ? $_REQUEST["count"] : 3;
print <<< DOC_END
<script type="text/javascript">
<!--
function action_route(from, to, sort) {
    var myCount = $("count").value;
    console.debug(myCount);
    location.href="transit.php?action_route=&from=" + from + "&to=" + to + "&sort=" + sort + "&count=" + myCount;
}
//-->
</script>
件数：<input type="text" id="count" value="$count" size="3"/>
DOC_END;
            print '<ul>';
            foreach($station_list as $key => $station) {
                print "<li>";
                print '<input type="checkbox" name="index[]" id="'.$key.'" value="'.$key.'" />';
                print '<label for="'.$key.'">'.$station.'</label>';
                if ($end_station) {
                    print ' ＞ '.$end_station."　route search";
                    foreach ($sort_list as $key => $val) {
                        print ' | <input type="button" onclick="action_route(\''.$station.'\', \''.$end_station.'\', \''.$key.'\');" value="'.$val.'" />';
                    }
                }
            }
            print "</ul>";
            print '<input type="button" value="select all" onclick="check(this.form,1);"/> ';
            print '<input type="button" value="deselect all" onclick="check(this.form,0);" /> ';
            print '<input type="submit" name="action_delete" value="delete by station" /> ';
            print '<input type="submit" name="action_transit" value="search route" /> ';
            //print '<input type="submit" name="action_stress_test" value="Load verification" /><br />';
            print "</form>";
        }
    }

    function multiRequest($data, $options = array()) {

      // array of curl handles
      $curly = array();
      // data to be returned
      $result = array();

      // multi handle
      $mh = curl_multi_init();

      // loop through $data and create curl handles
      // then add them to the multi-handle
      foreach ($data as $id => $d) {

        $curly[$id] = curl_init();

        $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
        curl_setopt($curly[$id], CURLOPT_URL,            $url);
        curl_setopt($curly[$id], CURLOPT_HEADER,         0);
        curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);

        // post?
        if (is_array($d)) {
          if (!empty($d['post'])) {
            curl_setopt($curly[$id], CURLOPT_POST,       1);
            curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
          }
        }

        // extra options?
        if (!empty($options)) {
          curl_setopt_array($curly[$id], $options);
        }
        curl_multi_add_handle($mh, $curly[$id]);
      }
      // execute the handles
      $running = null;
      do {
        curl_multi_exec($mh, $running);
      } while($running > 0);
      // get content and remove handles
      foreach($curly as $id => $c) {
        $result[$id] = curl_multi_getcontent($c);
        curl_multi_remove_handle($mh, $c);
      }
      // all done
      curl_multi_close($mh);
      return $result;
    }
}

$main = new TransitTest();
$main->execute();
?>