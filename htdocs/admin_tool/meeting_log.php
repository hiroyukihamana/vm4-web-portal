<?php

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("Auth.php");

class AppMeetingLog extends AppFrame {

    var $_auth = null;
    var $_sess_page;
    var $account_dsn = null;


    function init() {
        $_COOKIE["lang"]   = "ja";
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->_sess_page  = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->_name_space = md5(__FILE__);
    }

    function set_dsn($dsn) {
        $_SESSION[$this->_sess_page]["dsn"] = $dsn;
    }

    function get_dsn() {
        if (isset($_SESSION[$this->_sess_page]["dsn"])) {
            return $_SESSION[$this->_sess_page]["dsn"];
        }
    }

    function action_s3up(){
        //echo ("aaaa");
        $this->set_submit_key($this->_name_space);
        $table = "user";
        $template = "admin_tool/meeting_log/index.t.html";
        $this->_display($template);
    }

    function action_s3up_complete(){
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_s3up();
            exit;
        }
        // リクエスト取得取得
        $meeting_key = $this->request->get("meeting_key");
        $meeting_sequence_key = $this->request->get("meeting_sequence_key");
        $fms_url = $this->request->get("fms_url");

        // ミーティングデータ取得
        $meeting_db = new N2MY_DB($this->get_dsn(), "meeting");
        $meeting_info = $meeting_db->getRow( sprintf( "meeting_key ='%s' ", $meeting_key ) );
        $message = "";
        if($meeting_info == null){
            $message = "ミーティングデータがありません。";
        }

        // ミーティングシーケンスデータ取得
        $meeting_sequence_db = new N2MY_DB($this->get_dsn(), "meeting_sequence");
        $meeting_sequence_info = $meeting_sequence_db->getRow( sprintf( "meeting_key = '%s' AND meeting_sequence_key ='%s' ",$meeting_key, $meeting_sequence_key ) );
        if($meeting_sequence_info == null){
            $message = "ミーティングデータがありません。";
        }


        // FMSサーバデータ取得
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        $obj_FmsService = new DBI_FmsServer(N2MY_MDB_DSN);
        $where = sprintf("server_address = '%s' " , $fms_url);
        $fms_server_info = $obj_FmsService->getRow($where);
        if($fms_server_info == null){
            $message = "FMSサーバデータがありません。".$message;
        }

//        var_dump($fms_server_info);

        if(!$message){
            // FMSサーバのAPIにアクセス
            $fms_app_name = $this->config->get("CORE", "app_name");
            $url = "http://".$fms_url.":".$fms_server_info["server_port"]."/fms_api.php" .
                    "?app_name=" .urlencode($fms_app_name).
                    "&method=upload".
                    "&meeting_sequence_key=".$meeting_sequence_key.
                    "&meeting_id=".$meeting_info["meeting_session_id"].
                    "&dir=".$meeting_info["fms_path"];
            $this->logger2->debug($url);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
            curl_setopt($ch, CURLOPT_TIMEOUT, 500);
            $result = curl_exec($ch);
            curl_close($ch);
            if($result == 1){
                $message = "会議記録移動に成功しました。";
            }else {
                if($result & intval( "00100", 2 )){
                    $message = $message."<br>・録画データの移動に失敗しました。";
                }
                if($result & intval( "10000", 2 )){
                    $message = $message."<br>・議事録データの移動に失敗しました。";
                }
            }

           // echo ($result);
        }

        $this->template->assign("message",$message);
        return $this->action_s3up();
    }

    function action_s3up_check(){
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_s3up();
            exit;
        }
        // リクエスト取得取得
        $meeting_key = $this->request->get("meeting_key");
        $meeting_sequence_key = $this->request->get("meeting_sequence_key");
        $fms_url = $this->request->get("fms_url");
        // ミーティングデータ取得
        $meeting_db = new N2MY_DB($this->get_dsn(), "meeting");
        $meeting_info = $meeting_db->getRow( sprintf( "meeting_key ='%s' ", $meeting_key ) );
        $message = "";
        if($meeting_info == null){
            $message = "ミーティングデータがありません。";
        }

        // ミーティングシーケンスデータ取得
        $meeting_sequence_db = new N2MY_DB($this->get_dsn(), "meeting_sequence");
        $meeting_sequence_info = $meeting_sequence_db->getRow( sprintf( "meeting_key = '%s' AND meeting_sequence_key ='%s' ",$meeting_key, $meeting_sequence_key ) );
        if($meeting_sequence_info == null){
            $message = "ミーティングデータがありません。";
        }

        // FMSサーバデータ取得
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        $obj_FmsService = new DBI_FmsServer(N2MY_MDB_DSN);
        $where = sprintf("server_address = '%s' " , $fms_url);
        $fms_server_info = $obj_FmsService->getRow($where);
        if($fms_server_info == null){
            $message = "FMSサーバデータがありません。".$message;
        }

        if(!$message){
            // FMSサーバのAPIにアクセス
            $fms_app_name = $this->config->get("CORE", "app_name");
            $url = "http://".$fms_url.":".$fms_server_info["server_port"]."/fms_api.php" .
                    "?app_name=" .urlencode($fms_app_name).
                    "&method=check".
                    "&meeting_sequence_key=".$meeting_sequence_key.
                    "&meeting_id=".$meeting_info["meeting_session_id"].
                    "&dir=".$meeting_info["fms_path"];
            $this->logger2->debug($url);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
            curl_setopt($ch, CURLOPT_TIMEOUT, 500);
            $result = curl_exec($ch);
            curl_close($ch);
            $message = "・録画データ";
            // ビット演算なので注意
            if($result & intval( "0000010", 2 )){
                $message =  $message."<br>元データディレクトリ：○";
            }else{
                $message =  $message."<br>元データディレクトリ：×";
            }
            if($result & intval( "0000100", 2 )){
                $message =  $message."<br>録画tarファイル：○";
            }else{
                $message =  $message."<br>tarファイル：×";
            }
            if($result & intval( "0001000", 2 )){
                $message =  $message."<br>S3サーバにアップ済み：○";
            }else{
                $message =  $message."<br>S3サーバにアップ済み：×";
            }
            $message = $message."<br>・議事録データ";
            if($result & intval( "0010000", 2 )){
                $message =  $message."<br>元データディレクトリ：○";
            }else{
                $message =  $message."<br>元データディレクトリ：×";
            }
            if($result & intval( "0100000", 2 )){
                $message =  $message."<br>tarファイル：○";
            }else{
                $message =  $message."<br>tarファイル：×";
            }
            if($result & intval( "1000000", 2 )){
                $message =  $message."<br>S3サーバにアップ済み：○";
            }else{
                $message =  $message."<br>S3サーバにアップ済み：×";
            }
            //echo ($result);
        }

        $this->template->assign("message",$message);
        $this->template->assign("meeting_key",$meeting_key);
        $this->template->assign("meeting_sequence_key",$meeting_sequence_key);
        $this->template->assign("fms_url",$fms_url);
        return $this->action_s3up();

    }

    /**
     * デフォルトページ
     */
    function default_view(){
        return $this->action_s3up();
    }

    function _display ($template) {
        $staff_auth = $_SESSION["_authsession"]["data"]["authority"];
        $this->template->assign("authority", $staff_auth);
        $this->display($template);
    }

}

$main = new AppMeetingLog();
$main->execute();