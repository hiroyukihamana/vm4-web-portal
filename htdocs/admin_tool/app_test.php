<?php
require_once("classes/N2MY_Api.class.php");
require_once("classes/dbi/user.dbi.php");

class Test1 extends N2MY_Api
{
    var $user = null;

    function init() {
        $dsn = $this->config->get("GLOBAL", "dsn");
        $this->user = new UserTable($dsn);
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$dsn);
    }

    function auth() {
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__);
    }

    function default_view() {
        $user_list = $this->user->getRowsAssoc(null, null, 10, 0);
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$user_list);
    }

    function action_test() {
        $user_list = $this->user->getRowsAssoc(null, null, 10, 0);
        var_dump($user_list);
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__);
    }
}

$main = new Test1();
$main->execute();