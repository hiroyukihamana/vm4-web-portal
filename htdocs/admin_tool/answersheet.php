<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Questionnaire.class.php");
require_once("classes/mgm/dbi/user_agent.dbi.php");
require_once("classes/dbi/answersheet.dbi.php");
require_once("classes/dbi/answer.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/answer_branch.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once("classes/core/dbi/Meeting.dbi.php");

class Answersheet extends AppFrame {
    var $_dsn = null;
    var $account_dsn = null;

    function init() {
        // DBサーバ情報取得
        if (file_exists(sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ))) {
            $server_list = parse_ini_file( sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
        } else {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        }
    }

    function default_view($message = "") {
        $date = date("Y-m-d H:i:s");
        print $this->_header();
print <<<EOM
<script type="text/javascript" src="/shared/js/lib/jquery.js"></script>
<script type="text/javascript" src="/shared/js/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="/shared/js/calendar/lang/calendar-en_US.js"></script>
<script type="text/javascript" src="/shared/js/calendar/lang/calendar-ja_JP.js"></script>
<script type="text/javascript" src="/shared/js/calendar/calendar-setup_stripped.js"></script>
<script type="text/javascript" src="/shared/js/lib/jquery.corner.js" charset="UTF-8"></script>
<link rel="stylesheet" type="text/css" media="all" href="/shared/js/calendar/skins/tiger/theme.css" title="Aqua" />
<script type="text/javascript">
<!--

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

function ourDateStatusFunc(date, y, m, d) {
    var today = new Date();
    if (date.getTime() < (today.getTime()) - 365 * DAY) {
        return true;
    } else if (date.getTime() > (today.getTime()) + 365 * DAY) {
        return true;
    } else {
        return false;
    }
}

jQuery(document).ready(function(){
    // start date
    var calParams = {
        inputField     :    "start_cal",
        ifFormat       :    "%Y/%m/%d",
        showsTime      :    false,
        button         :    "start_btn",
        singleClick    :    true,
        step           :    1,
        dateStatusFunc :    ourDateStatusFunc
        };
    Calendar.setup(calParams);
    calParams["button"] = "start_cal";
    Calendar.setup(calParams);
    // end date
    var endParam = {
        inputField     :    "end_cal",
        ifFormat       :    "%Y/%m/%d",
        showsTime      :    false,
        button         :    "end_btn",
        singleClick    :    true,
        step           :    1,
        dateStatusFunc :    ourDateStatusFunc
        };
    Calendar.setup(endParam);
    endParam["button"] = "end_cal";
    Calendar.setup(endParam);
});
//-->
</script>
<form action="/admin_tool/answersheet.php" name="search" method="post" id="LogFormSearch">
<input type="hidden" name="action_answer_log_csv" value="">
<span id="startCalId"></span>
<input type="hidden" name="page" value='1' />
<input type="hidden" name="_action" value='' />
開始日<input class="input-date" type="text" name="start" id="start_cal" value="" autocomplete="off" readonly="readonly" />
<input type="button" class="bt-cal" id="start_btn" value="カレンダー"/> ～
終了日<input class="input-date" type="text" name="end" id="end_cal" value="" autocomplete="off" readonly="readonly" />
<input type="button" class="bt-cal" id="end_btn" value="カレンダー"/>

<br /><br />

<input type="submit" value="ダウンロード" />
</form>
{$message}
EOM;
        print $this->_footer();
    }

    function action_answer_log_csv() {
        $objUser = new N2MY_DB($this->dsn, "user");
        $start_date = $this->request->get("start");
        $end_date = $this->request->get("end");
        if (!$start_date) {
            $this->default_view("開始日付を選択してください");
            exit;
        }
        if (!$end_date) {
           $end_date = date("Y-m-d");
        }

        //入室者取得
        $obj_UserAgent = new MgmUserAgentTable($this->account_dsn);
        $where_agent = "create_datetime >= '".$start_date." 00:00:00'".
                 " AND create_datetime <= '".$end_date." 23:59:59'";
        $user_count = $obj_UserAgent->numRows($where_agent);
        if (DB::isError($user_count)) {
            $this->logger2->error($user_count->getUserInfo());
            return false;
        }

        //アンケート回答者数
        $obj_Answersheet = new AnswersheetTable($this->_dsn);
        $where = "created_datetime >= '".$start_date." 00:00:00'".
                 " AND created_datetime <= '".$end_date." 23:59:59'";
        $answer_count = $obj_Answersheet->numRows($where);
        if (DB::isError($answer_count)) {
            $this->logger2->error($answer_count->getUserInfo());
            return false;
        }

        //問題取得
        $obj_Questionnaire = new N2MY_Questionnaire($this->get_dsn());
        $question_list = $obj_Questionnaire->getQuestionnaire("meeting_end", $this->request->get("lang","ja"));

        //問1回答数
        $this->logger->debug("question",__FILE__,__LINE__,$question_list);
        $answers = array();
        $obj_Answer = new AnswerTable($this->_dsn);
        $obj_User = new UserTable($this->_dsn);
        $obj_AnswerBranch = new AnswerBranchTable($this->_dsn);
        $obj_Participant = new DBI_Participant($this->_dsn);
        $obj_Meeting = new DBI_Meeting($this->_dsn);
        foreach ($question_list as $_key => $question) {
            $answers[$_key]["text"] = $question["text"];
            $where_answer = "created_datetime >= '".$start_date." 00:00:00'".
                            " AND created_datetime <= '".$end_date." 23:59:59'".
                            " AND question_id = ".$question["question_id"].
                            " AND comment != ''";
            $comment_list = $obj_Answer->getRowsAssoc($where_answer, null, null, 0, "answersheet_id, comment, created_datetime");
            if (!empty($comment_list)) {
                foreach ($comment_list as $comment) {
                    $participant_data = array();
                    $where = "answersheet_id = ".addslashes($comment["answersheet_id"]);
                    $participant_key = $obj_Answersheet->getRow($where, "participant_key");
            $this->logger->debug("comment",__FILE__,__LINE__,array($comment,$participant_key));
                    if ($participant_key) {
                        $participant_data = array();
                        $where = "participant_key = ".addslashes($participant_key["participant_key"]);
                        //$participant_data = $obj_UserAgent->getRow($where);
                        $participant = $obj_Participant->getRow($where, "meeting_key, participant_name,uptime_start,uptime_end");
                        $where = "meeting_key = ".addslashes($participant["meeting_key"]);
                        $meeting = $obj_Meeting->getRow($where, "user_key,room_key,meeting_name");
                        $sql = "SELECT user.user_id,room.room_name FROM user" .
                                " LEFT JOIN room" .
                                " ON user.user_key = room.user_key".
                                " WHERE user.user_key = ".addslashes($meeting["user_key"]).
                                " AND room.room_key = '".addslashes($meeting["room_key"])."'";
                        $user = $obj_User->_conn->getRow($sql);
                        if (DB::isError($user)) {
                            print $count->getUserInfo();
                        }
$this->logger2->debug($user);
                        $participant_data["participant_name"] = $participant["participant_name"];
                        $participant_data["created_datetime"] = $comment["created_datetime"];
                        $participant_data["uptime_start"] = $participant["uptime_start"];
                        $participant_data["uptime_end"] = $participant["uptime_end"];
                        $participant_data["meeting_name"] = $meeting["meeting_name"];
                        $participant_data["room_name"] = $user[1];
                        $participant_data["user_id"] = $user[0];
                        $participant_data["comment"] = $comment["comment"];

                    }
                    $participant_list[] = $participant_data;
                }

                $answers[$_key]["comment"] = $participant_list;
            }

            foreach ($question["branch"] as $__key =>  $branch) {
                $answers[$_key]["branch"][$__key]["text"] = $branch["text"];
                $where_branch = "created_datetime >= '".$start_date." 00:00:00'".
                                " AND created_datetime <= '".$end_date." 23:59:59'".
                                " AND question_id = ".$question["question_id"].
                                " AND question_branch_id   = ".$branch["question_branch_id"];
                $branch_count = $obj_AnswerBranch->numRows($where_branch);
                if (DB::isError($branch_count)) {
                    $this->logger2->error($branch_count->getUserInfo());
                    return false;
                }
                $answers[$_key]["branch"][$__key]["count"] = $branch_count;
            }
        }

        $this->logger->debug("answers",__FILE__,__LINE__,$answers);
        ob_start();
        $this->template->assign('user_count',$user_count);
        $this->template->assign('answer_count',$answer_count);
        $this->template->assign('start_date',$start_date);
        $this->template->assign('end_date',$end_date);
        $this->template->assign('answer_list',$answers);
        $this->template->display('common/admin_tool/answersheet/answersheet.t.csv');
        $contents = ob_get_contents();
        if ($this->_lang == "ja_JP") {
            $contents = mb_convert_encoding($contents, 'SJIS', 'UTF-8');
        }
        ob_clean();
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="answersheet.t.csv"');
        print $contents;
    }

function _header() {
        return <<<EOM
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>アンケート集計</title>
<style TYPE="text/css">
<!--
body {
text-align: center;
font-size: 12px;
font-family: "Arial, sans-serif, ＭＳ Ｐゴシック", Osaka, "ヒラギノ角ゴ Pro W3" ;
margin: 10px 0px 0px 0px;
}
#content {
text-align: center;
}
#content table {
border-collapse: collapse;
border-spacing: 0;
empty-cells: show;
border: 1px solid #999999;
margin: 20px auto;
width: 760px
}
#content th {
border: 1px solid #999999;
padding: 4px 5px;
background-color: #EFEFEF;
text-align: left;
}

.errorSection {
color: #FF0000;
}
-->
</style>
</head>
<body>
<div id="content">
<h1>アンケート集計</h1>
EOM;
    }

    function _footer() {
       print "</body></html>";
    }

}
$main = new Answersheet();
$main->execute();
