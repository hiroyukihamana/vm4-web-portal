<?php
require_once("classes/AppFrame.class.php");
require_once("classes/dbi/appli_file.dbi.php");

class AppAppliManager extends AppFrame {
	
	var $obj_appliFile = null;

    function init () {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->dsn = $this->get_dsn();
        $this->obj_appliFile = new AppliFileTable($this->account_dsn);
        $this->_name_space = md5(__FILE__);
    }

    function default_view($err_message = null) {
    	$_tmpFile = array();
    	$staging_installer = array();
    	$staging_binary = array();
    	$released_installer = array();
    	$released_binary = array();
    	
    	$where_staging = "is_staging = 1 AND is_deleted = 0";
    	$sort_staging = array("create_datetime" => "asc");
    	$_tmpFile = $this->obj_appliFile->getRowsAssoc($where_staging, $sort_staging);
    	foreach ($_tmpFile as $file) {
    		if($file["file_type"] == "installer") {
    			$staging_installer = $file;
    		} else if($file["file_type"] == "binary") {
    			$staging_binary = $file;
    		}
    	}
    	
    	$where_released = "is_released = 1 AND is_deleted = 0";
    	$sort_released = array("update_datetime" => "asc");
    	$_tmpFile = $this->obj_appliFile->getRowsAssoc($where_released, $sort_released);
    	foreach ($_tmpFile as $file) {
    		if($file["file_type"] == "installer") {
    			$released_installer = $file;
    		} else if($file["file_type"] == "binary") {
    			$released_binary = $file;
    		}
    	}
    	$this->logger2->info($staging_installer);
    	$this->template->assign("staging_installer", $staging_installer);
    	$this->template->assign("staging_binary", $staging_binary);
    	$this->template->assign("released_installer", $released_installer);
    	$this->template->assign("released_binary", $released_binary);
    	$this->template->assign("err_message", $err_message);
    	$this->display("admin_tool/appli_manager/index.t.html");
    	
    	return ;
    }
    
    function action_upload_file() {
    	$request = $this->request->getAll();
    	
    	$type		= $request["type"];
    	$version	= $request["version"];
    	$file		= $_FILES["upload_file"];
    	$this->logger2->info($file);
    	
    	if(!$version) {
    		$err_message = "please input the version of this file";
    		$this->default_view($err_message);
    		return false;
    	}
    	if(!$file["error"]) {
    		//upload file
    		$file_path = N2MY_APP_DIR.APPLICATION_FILES_DIR."staging/".$type."/".$version."/";
    		$file_name = constant(strtoupper($type)."_FILE_NAME");
    	    if(!is_dir($file_path)) {
                mkdir($file_path, 0777);
            }
            if(file_exists($file_path.$file_name)) {
            	unlink($file_path.$file_name);
            }
            move_uploaded_file($file["tmp_name"],$file_path.$file_name);
            //$this->logger2->info($file_path.$file_name);
            //$this->logger2->info($file["tmp_name"]);
    		
    		//refresh staging file
    		$where_StagingFile = sprintf("file_type='%s' AND is_staging=1",$type);
    		$delete_StagingFile_data = array("is_staging" => 0);
    		$res = $this->obj_appliFile->update($delete_StagingFile_data, $where_StagingFile);
    		if (DB::isError($res)) {
    			$this->logger2->error( $delete_StagingFile_data, "delete old staging file failed");
    		}
    		//todo: delete the old file?
    		 
    		$new_stagingFile_data = array("file_type"	=> $type,
    							 "file_name"	=> $type == "installer"?INSTALLER_FILE_NAME:BINARY_FILE_NAME,
    							 "file_size"	=> $file[size],
    							 "version"		=> $version,
    							 "hash"			=> hash_file('md5', $file_path.$file_name),
    							 "is_staging"	=> 1,);
    		$res = $this->obj_appliFile->add($new_stagingFile_data);
    		if (DB::isError($res)) {
    			$this->logger2->error( $delete_StagingFile_data, "add staging file failed");
    		}
    		$this->default_view();
    		return true;
    	} else {
    		$err_message = "please select a right file.";
    		$this->default_view($err_message);
    		return false;
    	}
    	
    }
    
    function action_set_release() {
    	$appli_file_key = $this->request->get("file_key");
    	$where_newReleasedFile = sprintf("appli_file_key='%s' AND is_staging=1",$appli_file_key);
    	$appli_file_info = $this->obj_appliFile->getRow($where_newReleasedFile);
    	
    	if(!$appli_file_info["is_released"]) {
    		$where_oldReleasedFile = sprintf("file_type='%s' AND is_released=1",$appli_file_info["file_type"]);
    		$data_deleteReleasedFile = array("is_released" => 0,
    				"is_deleted"  => 1,);
    		$res = $this->obj_appliFile->update($data_deleteReleasedFile, $where_oldReleasedFile);
    		if (DB::isError($res)) {
    			$this->logger2->error( $data_deleteReleasedFile, "delete old released file failed");
    		}
    		$file_name = constant(strtoupper($appli_file_info["file_type"])."_FILE_NAME");
    		$staging_file = N2MY_APP_DIR.APPLICATION_FILES_DIR."staging/".$appli_file_info["file_type"]."/".$appli_file_info["version"]."/".$file_name;
    		$released_file_path = N2MY_APP_DIR.APPLICATION_FILES_DIR."released/".$appli_file_info["file_type"]."/".$appli_file_info["version"]."/";
    		if(!is_dir($released_file_path)) {
    			mkdir($released_file_path, 0777);
    		}
    		$this->logger2->info($staging_file);
    		$this->logger2->info($released_file_path.$file_name);
    		copy($staging_file, $released_file_path.$file_name);
    		
     		$data_newReleasedFile = array(
    				"is_released"	=> 1,);
    		$res = $this->obj_appliFile->update($data_newReleasedFile, $where_newReleasedFile);
    		if (DB::isError($res)) {
    			$this->logger2->error( $data_newReleasedFile, "add new released file failed");
    		}
    	}
    	
    	$this->default_view();
    }
    
}
$main =& new AppAppliManager();
$main->execute();