<?php

// 環境設定
require_once ('classes/AppFrame.class.php');
// 部屋情報
require_once ('classes/dbi/room_plan.dbi.php');

/* ***********************************************
 * 部屋の情報を請求管理DBへ反映するための処理
 *
 * ***********************************************/
class AppStatisticsRoomPlanInfo extends AppFrame {

	var $_dsn = null;
	var $account_dsn = null;
	var $room_plan_obj = null;

	function init() {
		// DBサーバ情報取得
		if (file_exists(sprintf("%sconfig/slave_list.ini", N2MY_APP_DIR))) {
			$server_list = parse_ini_file(sprintf("%sconfig/slave_list.ini", N2MY_APP_DIR), true);
			$this -> _dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
			$this -> account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
		} else {
			$server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR), true);
			$this -> _dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
			$this -> account_dsn = $this -> config -> get("GLOBAL", "auth_dsn");
		}
		$this -> room_plan_obj = new RoomPlanTable($this -> _dsn);

	}

	function default_view() {

		$this -> logger -> info(__FUNCTION__ . ' # GetMeetingRoomPlans Info Operation Start ... ', __FILE__, __LINE__);

		// 初期化
		$mtg_room_plans = array();

		// パラメタ取得
		$target_day = $this -> request -> get('target_day');
		if (!$target_day) {
			$target_day = date("Y-m-d", (time() - 3600 * 24));
		}
		$this -> logger2 -> info("target day is " . $target_day);
		//$target_day = date('Y-m-d', time() - (3600*24)) . ' 00:00:00'; // 常に前日

		// パラメータ検証 ( boolearn : true / false )
		$param_is_safe = $this -> _check_param($target_day);

		// パラメータ判定
		if ($param_is_safe) {
			// MTG利用統計用部屋プランデータ（差分データ）作成
			$this -> mtg_room_plans = array();
			$_tmp_data = null;
			$_tmp_data = $this -> _get_room_plan_routine($target_day);
			if (is_array($_tmp_data) === true) {
				$mtg_room_plans = array_merge($mtg_room_plans, $_tmp_data);
			}
		} else {
			$mtg_room_plans = array();
		}
		$this -> logger -> info(__FUNCTION__ . ' # RoomPlanInfo Count ... ', __FILE__, __LINE__, count($mtg_room_plans));

		$log_folder = N2MY_DOCUMENT_ROOT . "admin_tool/statistics_data/room_plan/";
		//TO CSV
		require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
		$csv = new EZCsv(true, 'SJIS', 'UTF-8');
		$csv_file = $log_folder . "room_plan_" . date("Ymd", $this -> get_target_date($target_day)) . ".csv";
		$handle = fopen($csv_file, "w");
		$csv -> open($csv_file, "w");
		$header = $this -> get_csv_header();
		$csv -> setHeader($header);
		$csv -> write($header);
		if ($mtg_room_plans) {
			foreach ($mtg_room_plans as $mtg_room_plan) {
				$csv -> write($mtg_room_plan);
			}
		}
		$csv -> close();
		chmod($csv_file, 0777);
		fclose($handle);

		//Delete log a week ago
		$delete_log = $log_folder . "room_plan_" . date("Ymd", ($this -> get_target_date($target_day) - 3600 * 24 * 7)) . ".csv";
		if (file_exists($delete_log)) {
			unlink($delete_log);
		}

		$this -> logger -> info(__FUNCTION__ . ' # GetMeetingRoomPlans Info Operation End ... ', __FILE__, __LINE__);
	}

	/**
	 * メイン取得
	 *
	 */
	private function _get_room_plan_routine($target_day) {
		$rows = array();
		$where = ' `room_plan_registtime` LIKE \'' . addslashes($target_day) . '%\' ' . ' OR `room_plan_updatetime` LIKE \'' . addslashes($target_day) . '%\' ';
		$rs = $this -> room_plan_obj -> getRowsAssoc($where);
		foreach ($rs as $key => $val) {
			$rows[] = $val;
		}
		return $rows;
	}

	/**
	 * パラメータ検証
	 *
	 */
	private function _check_param($_param = null) {
		$ret = false;
		//未設定チェック
		if (!$_param) {
			return $ret;
		}
		//文字数チェック
		if (strlen($_param) < 7) {
			return $ret;
		}
		//指定不可文字存在チェック
		if (preg_match("/[_%*?]/", $_param)) {
			return $ret;
		}
		//チェック OK!
		$ret = true;

		return $ret;
	}

	/**
	 * describe table in order to get table columns
	 */
	function get_csv_header() {
		$sql = "desc room_plan";
		$rs = $this -> room_plan_obj -> _conn -> query($sql);
		$rows = array();
		$row = array();
		if (DB::isError($rs)) {
			$this -> logger -> info(__FUNCTION__ . " # DBError", __FILE__, __LINE__, $sql);
			return false;
		} else {
			while ($row = $rs -> fetchRow(DB_FETCHMODE_ASSOC)) {
				$rows[$row['Field']] = $row['Field'];
			}
		}
		return $rows;
	}

	/**
	 * 【テーブル名】_【YYYYmmdd】.csv
	 */
	function get_target_date($date) {
		$year = substr($date, 0, 4);
		$month = substr($date, 5, 2);
		$day = substr($date, 8, 2);
		$timestamp = mktime(0, 0, 0, $month, $day, $year);
		return $timestamp;
	}

}

$main = new AppStatisticsRoomPlanInfo();
$main -> execute();
?>