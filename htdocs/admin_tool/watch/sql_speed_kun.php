<?php
    session_start();
    function auth()
    {
        if ($_SESSION["admin_tool"]["auth"] == 1) {
            return true;
        }
        $user_id = $_REQUEST["auth"]["user_id"];
        $user_pw = $_REQUEST["auth"]["user_pw"];
        if ($user_id && $user_pw) {
            $fp = fopen("../.htpasswd", "r");
            if ($fp) {
                while(!feof($fp)){
                    $line = trim(fgets($fp, 4096));
                    list($id, $pass) = split(",", $line);
                    if($id == $user_id && $pass == sha1($user_pw)){
                        $_SESSION["admin_tool"]["auth"] = 1;
                        return true;
                    }
                }
            }
        }
        print <<<EOM
<form action="" method="post">
<table>
    <tr>
        <td>Admin ID:</td>
        <td><input type="text" name="auth[user_id]" value="$user_id"/></td>
    </tr><tr>
        <td>Admin PW:</td>
        <td><input type="password" name="auth[user_pw]" value="" /></td>
    </tr><tr>
        <td></td>
        <td><input type="submit" value="OK" /></td>
    </tr>
</table>
</form>
EOM;
        exit;
    }

auth();

require_once("DB.php");
set_time_limit(0);
// DSN
$dsn = "mysql://n2my:n2myforsi@localhost/v4trunk_web";
$sql = $_REQUEST["sql"];
$query_checker = new QueryPerformanceChecker($dsn);
$query_checker->form();
if ($_REQUEST["mode"] == "performance") {
    $querys = $query_checker->import_query($sql);
    foreach($querys as $key => $val) {
        $query_checker->performance($val);
    }
}

class QueryPerformanceChecker {

    var $db = null;

    function QueryPerformanceChecker($dsn)
    {
        // WEB
        $this->db = DB::connect($dsn);
        if (DB::isError($this->db)) {
            die($this->db->getUserInfo());
        }
        $this->db->setFetchMode(DB_FETCHMODE_ASSOC);
        $this->db->query("SET NAMES utf8");
    }

    function form() {
$head = <<<EOM
table.searchList {
background-color:#FFFFFF;
border-collapse:collapse;
border-left:1px solid #663300;
border-spacing:0pt;
border-top:1px solid #663300;
empty-cells:show;
}
.searchList th {
background-color:Wheat;
background-position:left top;
border-bottom:1px solid #663300;
border-right:1px solid #663300;
color:#330000;
padding:0.3em 1em;
text-align:center;
white-space:nowrap;
}
.searchList th.action {
background-color:Wheat;
}
.searchList td {
border-bottom:1px solid #663300;
border-right:1px solid #663300;
padding:0.3em 1em;
white-space:nowrap;
}
EOM;
        $num = isset($_REQUEST["num"]) ? $_REQUEST["num"] : "1000";
        print "<html><head><style TYPE='text/css'>";
        print $head;
        print "</style></head><body>\r\n";
        print "<form method='post'>\r\n";
        print 'repeat:<input type="text" name="num" value="'.$num.'">';
        print "<textarea name='sql' cols='150' rows='5'>".$_REQUEST["sql"]."</textarea>";
        print "<input type='submit' name='mode' value='performance'>\r\n";
        print "</form>\r\n";
    }

    function performance($sql) {
        $this->_get_passage_time(true);
        print "<table class='searchList'>";
        print '<tr><td colspan="2">'.$sql.'</td></tr>';
        print "<tr><th>COUNT</th><th>TIME</th></tr>";
        $count = $_REQUEST["num"];
        for ($i = 0; $i < $count; $i++) {
            $ret = $this->db->query($sql);
            if (DB::isError($ret)) {
                die("Error!!");
            }
            if (($i % 100) == 0) {
                $aaa = $this->_get_passage_time();
                print "<tr><td>".$i."</td><td>".$aaa."</td></tr>";
            }
        }
        $aaa = $this->_get_passage_time();
        print "<tr><th>$count</th><th>".$aaa."</th></tr>";
        print "<tr><th>Average</th><th>".($aaa / $count)."</th></tr>";
        print "</table>";
    }

    /**
     * 経過時間取得
     */
    function _get_passage_time($reset = false) {
        static $start_time;
        if (!isset($start_time) || $reset == true) {
            list($micro, $sec) = split(" ", microtime());
            $start_time = $micro + $sec - 0.00001;
        }
        list($micro, $sec) = split(" ", microtime());
        $passage_time = ($micro + $sec) - $start_time;
        return $passage_time;
    }

    function import_query($data)
    {
        // set default values
        $timeout_passed = FALSE;
        $error = FALSE;
        $read_multiply = 1;
        $finished = FALSE;
        $offset = 0;
        $max_sql_len = 0;
        $file_to_unlink = '';
        $sql_query = '';
        $sql_query_disabled = FALSE;
        $go_sql = FALSE;
        $executed_queries = 0;
        $run_query = TRUE;
        $charset_conversion = FALSE;
        $reset_charset = FALSE;
        $bookmark_created = FALSE;
        // init values
        $sql_delimiter = ';';
        $sql = '';
        $start_pos = 0;
        $i = 0;
        // Append new data to buffer
        $buffer = $data;
        // Current length of our buffer
        $len = strlen($buffer);
        // Grab some SQL queries out of it
        while ($i < $len) {
            $found_delimiter = false;
            // Find first interesting character, several strpos seem to be faster than simple loop in php:
            //while (($i < $len) && (strpos('\'";#-/', $buffer[$i]) === FALSE)) $i++;
            //if ($i == $len) break;
            $oi = $i;
            $p1 = strpos($buffer, '\'', $i);
            if ($p1 === FALSE) {
                $p1 = 2147483647;
            }
            $p2 = strpos($buffer, '"', $i);
            if ($p2 === FALSE) {
                $p2 = 2147483647;
            }
            $p3 = strpos($buffer, $sql_delimiter, $i);
            if ($p3 === FALSE) {
                $p3 = 2147483647;
            } else {
                $found_delimiter = true;
            }
            $p4 = strpos($buffer, '#', $i);
            if ($p4 === FALSE) {
                $p4 = 2147483647;
            }
            $p5 = strpos($buffer, '--', $i);
            if ($p5 === FALSE || $p5 >= ($len - 2) || $buffer[$p5 + 2] > ' ') {
                $p5 = 2147483647;
            }
            $p6 = strpos($buffer, '/*', $i);
            if ($p6 === FALSE) {
                $p6 = 2147483647;
            }
            $p7 = strpos($buffer, '`', $i);
            if ($p7 === FALSE) {
                $p7 = 2147483647;
            }
            $i = min ($p1, $p2, $p3, $p4, $p5, $p6, $p7);
            unset($p1, $p2, $p3, $p4, $p5, $p6, $p7);
            if ($i == 2147483647) {
                $i = $oi;
                if (!$finished) {
                    break;
                }
                // at the end there might be some whitespace...
                if (trim($buffer) == '') {
                    $buffer = '';
                    $len = 0;
                    break;
                }
                // We hit end of query, go there!
                $i = strlen($buffer) - 1;
            }

            // Grab current character
            $ch = $buffer[$i];

            // Quotes
            if (!(strpos('\'"`', $ch) === FALSE)) {
                $quote = $ch;
                $endq = FALSE;
                while (!$endq) {
                    // Find next quote
                    $pos = strpos($buffer, $quote, $i + 1);
                    // No quote? Too short string
                    if ($pos === FALSE) {
                        // We hit end of string => unclosed quote, but we handle it as end of query
                        if ($finished) {
                            $endq = TRUE;
                            $i = $len - 1;
                        }
                        break;
                    }
                    // Was not the quote escaped?
                    $j = $pos - 1;
                    while ($buffer[$j] == '\\') $j--;
                    // Even count means it was not escaped
                    $endq = (((($pos - 1) - $j) % 2) == 0);
                    // Skip the string
                    $i = $pos;
                }
                if (!$endq) {
                    break;
                }
                $i++;
                // Aren't we at the end?
                if ($finished && $i == $len) {
                    $i--;
                } else {
                    continue;
                }
            }

            // Not enough data to decide
            if ((($i == ($len - 1) && ($ch == '-' || $ch == '/'))
                || ($i == ($len - 2) && (($ch == '-' && $buffer[$i + 1] == '-') || ($ch == '/' && $buffer[$i + 1] == '*')))
                ) && !$finished) {
                break;
            }

            // Comments
            if ($ch == '#'
                    || ($i < ($len - 1) && $ch == '-' && $buffer[$i + 1] == '-' && (($i < ($len - 2) && $buffer[$i + 2] <= ' ') || ($i == ($len - 1) && $finished)))
                    || ($i < ($len - 1) && $ch == '/' && $buffer[$i + 1] == '*')
                    ) {
                // Copy current string to SQL
                if ($start_pos != $i) {
                    $sql .= substr($buffer, $start_pos, $i - $start_pos);
                }
                // Skip the rest
                $j = $i;
                $i = strpos($buffer, $ch == '/' ? '*/' : "\n", $i);
                // didn't we hit end of string?
                if ($i === FALSE) {
                    if ($finished) {
                        $i = $len - 1;
                    } else {
                        break;
                    }
                }
                // Skip *
                if ($ch == '/') {
                    // Check for MySQL conditional comments and include them as-is
                    if ($buffer[$j + 2] == '!') {
                        $comment = substr($buffer, $j + 3, $i - $j - 3);
                        if (preg_match('/^[0-9]{5}/', $comment, $version)) {
                            if ($version[0] <= PMA_MYSQL_INT_VERSION) {
                                $sql .= substr($comment, 5);
                            }
                        } else {
                            $sql .= $comment;
                        }
                    }
                    $i++;
                }
                // Skip last char
                $i++;
                // Next query part will start here
                $start_pos = $i;
                // Aren't we at the end?
                if ($i == $len) {
                    $i--;
                } else {
                    continue;
                }
            }

            // End of SQL
            if ($found_delimiter || ($finished && ($i == $len - 1))) {
                $tmp_sql = $sql;
                if ($start_pos < $len) {
                    $length_to_grab = $i - $start_pos;
                    if (!$found_delimiter) {
                        $length_to_grab++;
                    }
                    $tmp_sql .= substr($buffer, $start_pos, $length_to_grab);
                    unset($length_to_grab);
                }
                // Do not try to execute empty SQL
                if (!preg_match('/^([\s]*;)*$/', trim($tmp_sql))) {
                    $sql = $tmp_sql;
                    //$this->PMA_importRunQuery($sql, substr($buffer, 0, $i + strlen($sql_delimiter)));
                    $querys[] = $sql;
                    $buffer = substr($buffer, $i + strlen($sql_delimiter));
                    // Reset parser:
                    $len = strlen($buffer);
                    $sql = '';
                    $i = 0;
                    $start_pos = 0;
                    // Any chance we will get a complete query?
                    //if ((strpos($buffer, ';') === FALSE) && !$finished) {
                    if ((strpos($buffer, $sql_delimiter) === FALSE) && !$finished) {
                        break;
                    }
                } else {
                    $i++;
                    $start_pos = $i;
                }
            }
        }
        return $querys;
    }

    function PMA_importRunQuery($sql = '', $full = '') {
        print $sql."<br>";
    }
}

?>
