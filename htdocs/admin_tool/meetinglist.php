<?php

require_once("classes/AppFrame.class.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/video_conference.dbi.php");
require_once("Auth.php");
require_once("Pager.php");
require_once("config/config.inc.php");


class MeetingList extends AppFrame {

	var $_auth = null;
	var $_sess_page;
	var $dsn = null;
	var $account_dsn = null;
	var $session = null;
	
	function init() {
		$_COOKIE["lang"]   = "en";
		$this->session   = EZSession::getInstance();
		$this->session->set("lang","en");
		$this->_sess_page  = "_adminauthsession";
		$this->dsn = $_SESSION[$this->_sess_page]["dsn"];
		$this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
		//$this->_name_space = md5(__FILE__);
		
		$this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
		
	}
	
	function auth() {
		$auth_detail = "*";
		$this->_auth = new Auth("DB", array(
				"dsn" => $this->account_dsn,
				"table" => "staff",
				"cryptType" => "sha1",
				"db_fields" => $auth_detail,
				"usernamecol" => "login_id",
				"passwordcol" => "login_password"
		));
		if (!$this->_auth->getAuth()) {
			$this->_auth->start();
			if ($_SESSION["_authsession"]["data"]["status"] == "0") {
				$this->_auth->logout();
				$this->request->set("password", "");
				$this->_auth->start();
			}
			if ($this->_auth->checkAuth()) {
				//操作ログ登録
				$operation_data = array (
						"staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
						"action_name"        => "login",
						"table_name"         => "staff",
						"keyword"            => $_SESSION["_authsession"]["username"],
						"info"               => "login",
						"operation_datetime" => date("Y-m-d H:i:s"),
				);
				$this->add_operation_log($operation_data);
			} else {
				exit();
			}
		}
	}
	
	function default_view() {
		//reset
		$this->logger2->debug($_SESSION[$this->_sess_page]["meeting_list"],"set");
		unset($_SESSION[$this->_sess_page]["meeting_list"]);
		$this->action_meeting_list();
	}
	
	function action_meeting_list() {
		//get account type
		$action_name = __FUNCTION__;
		//get request/search filters
		$request = $this->request->getAll();
		$this->logger->debug("request",__FILE__,__LINE__,$request);
		$sort_key = $this->request->get("sort_key");
		$sort_type = $this->request->get("sort_type");
		if (!$sort_type) {
			$sort_type = "desc";
		}
		$page = $this->request->get("page");
		$page_cnt = $this->request->get("page_cnt");
		$reset = $this->request->get("reset");
		//set form_info
		//
		if ($reset == 1) {
			unset($_SESSION[$this->_sess_page]["meeting_list"]["request"]);
		}
		if(!isset($_SESSION[$this->_sess_page]["meeting_list"]) || $reset == 1) {
			$_SESSION[$this->_sess_page]["meeting_list"]['sort_key']  = "meeting_key";
			$_SESSION[$this->_sess_page]["meeting_list"]['sort_type'] = "desc";
			$_SESSION[$this->_sess_page]["meeting_list"]['page'] = 1;
			$_SESSION[$this->_sess_page]["meeting_list"]['page_cnt'] = 20;
		}  else {
			// ページ
			if ($page) {
				$_SESSION[$this->_sess_page]["meeting_list"]['page'] = $page;
			}
			// ソート
            if ($sort_key) {
            	if($_SESSION[$this->_sess_page]["meeting_list"]['sort_key'] != $sort_key || $_SESSION[$this->_sess_page]["meeting_list"]['sort_type'] != $sort_type)
            		$_SESSION[$this->_sess_page]["meeting_list"]['page'] = 1;
                $_SESSION[$this->_sess_page]["meeting_list"]['sort_key']  = $sort_key;
                $_SESSION[$this->_sess_page]["meeting_list"]['sort_type'] = $sort_type;
            }
            // ページ
            if ($page_cnt) {
                $_SESSION[$this->_sess_page]["meeting_list"]['page_cnt'] = $page_cnt;
            }
            $request = $this->request->get("form");
            if ($request) {
                $_SESSION[$this->_sess_page]["meeting_list"]['page'] = 1;
                $_SESSION[$this->_sess_page]["meeting_list"]["request"] = $request;
            }
        }
        $form_data = $_SESSION[$this->_sess_page]["meeting_list"];
        $this->template->assign("form_data",$form_data);
        if (isset($form_data["request"]))
        	$this->template->assign("request",$form_data["request"]);
        //user id => user key
		if($form_data["request"]["user_id"] && trim($form_data["request"]["user_id"]) != '') {
			$user_id = $form_data["request"]["user_id"];
			$user_db = new N2MY_DB($this->dsn, "user");
			$user_key = $user_db->getOne("user_id = '" . mysql_real_escape_string($user_id) . "'", "user_key");
			if($user_key == "")
				$user_key = -1;
			$this->logger->debug("user key",__FILE__,__LINE__,$user_key);
			unset($form_data["request"]["user_id"]);
			$form_data["request"]["user_key"] = $user_key;
		}
		$this->logger->debug("form data",__FILE__,__LINE__,$form_data);
		if (isset($form_data["request"])) {
			$form_filter = $form_data["request"];
		}
        //set table
		//get fms db
		$fms_db = new N2MY_DB($this->account_dsn, "fms_server");
		$_server_info = $fms_db->getRowsAssoc("", null ,null, null, null, null);
		foreach($_server_info as $_key => $val) {
			$server_info[$val["server_key"]] = $val;
		}
		$fms_app_name = $this->config->get("CORE", "app_name", "Vrms");
		$fms = array(
				"server_info" => $server_info,
				"app_name" => $fms_app_name,
		);
		$this->template->assign("fms",$fms);
		
		$table = "meeting";
		$type = "data";
		$this->logger->debug("dsn",__FILE__,__LINE__,$this->dsn);
		$data_db = new N2MY_DB($this->dsn, $table);
		$columns = $data_db->getTableInfo($type);
		$columns = $this->_get_relation_data($this->dsn, $columns);
		//columns customize
		foreach($columns as $key => $setting)
		{
			if($key == "meeting_key" || $key == "room_key" || $key == "server_key" || $key == "meeting_start_datetime") {
				$columns[$key]["listform_status"] = 1;
			} else {
				$columns[$key]["listform_status"] = 0;
			}
				
		}
		$sequence_key = array( "id" => "meeting_sequence_key",
							   "label" => "sequence key",
							   "desc" => null,
							   "type" => "number",
							   "sub_type" => null,
							   "mode" => "int",
						   	   "size" => 10,
							   "default" => null,
							   "item" => array("search" => null),
							   "searchform_status" => 0,
							   "listform_status" => 1,
							   "sort" => 1
							  );
		$columns = array_slice($columns, 0, 2, true) + array("meeting_sequence_key" => $sequence_key) + array_slice($columns, 2, count($columns)-2, true);
		
		$page    = $form_data['page'];
		$limit   = $form_data['page_cnt'];
		$offset  = ($limit * ($page - 1));
		$where = $data_db->getWhere($columns, $form_filter);
		//$this->logger->debug("where",__FILE__,__LINE__,$where);
		
		//db join rows
		$column = "meeting.meeting_key AS meeting_key, meeting_sequence_key, ms.server_key AS server_key, ms.start_datetime AS meeting_start_datetime, room_key, meeting_session_id, fms_path, temporary_did, is_active";
		$relate = "meeting.meeting_key = ms.meeting_key";
		$where = str_replace("meeting_start_datetime","start_datetime",$where);
		$rows = $data_db->innerJoin("meeting_sequence ms",$relate ,$where, array($form_data['sort_key'] => $form_data['sort_type']),$limit,$offset,$column);
		
		//get conference id
		$video_conference_db = new N2MY_DB($this->dsn, "video_conference");
		foreach($rows as $key => $data)
		{
			if($data["temporary_did"]) {
				$rows[$key]["conference_id"] = $video_conference_db->getOne("meeting_key ='".$data["meeting_key"]."'", "conference_id");
			} else
				$rows[$key]["conference_id"] = "";
		}	
		//$this->logger->debug("rows",__FILE__,__LINE__,$rows);
		$total_count = $data_db->numRowsInnerJoin("meeting_sequence ms",$relate ,$where);
		//set view
		$pager_info = $this->setPager($limit, $page, $total_count);
		$this->template->assign("page", array(
				"pager" => $pager_info,
				"action" => $action_name,
				"after_action" => $this->request->get("after_action"))
		);
		//$this->logger->debug("pager_info",__FILE__,__LINE__,$pager_info);
		
		$this->template->assign("columns",$columns);
		$this->template->assign("rows",$rows);
		$this->template->assign("type",$type);
		$this->template->assign("table",$table);
		$template = "admin_tool/meetinglist.t.html";
		$this->_display($template);
	}
	
	function _get_relation_data($dsn, $columns) {
		foreach ($columns as $key => $value) {
			if (isset($value["item"]["relation"])) {
				$relation = $value["item"]["relation"];
				if ($relation["db_type"] == "account") {
					$relation_db = new N2MY_DB($this->account_dsn, $relation["table"]);
				} else {
					$relation_db = new N2MY_DB($dsn, $relation["table"]);
				}
				if ($relation["status_name"]) {
					$where = $relation["status_name"]."='".$relation["status"]."'";
					$relation_data = $relation_db->getRowsAssoc($where);
				} else {
					$relation_data = $relation_db->getRowsAssoc();
				}
				if ($relation["default_key"] || $relation["default_value"]) {
					$columns[$key]["item"]["relation_data"][$relation["default_key"]] = $relation["default_value"];
				}
				foreach ($relation_data as $_data) {
					$columns[$key]["item"]["relation_data"][$_data[$relation["key"]]] = $_data[$relation["value"]];
				}
			}
		}
		return $columns;
	}
	
	function _display ($template) {
        $staff_auth = $_SESSION["_authsession"]["data"]["authority"];
        $this->template->assign("authority", $staff_auth);
        $this->display($template);
	}
}

$main = new MeetingList();
$main->execute();

?>