<?php

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");

class AppRecordDownload extends AppFrame {

    var $server_list = null;
    var $downloadOptions= array();

    function init() {
        $_COOKIE["lang"] = "en";
        //複数のサーバーがある
        $this->server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);

    }
    /**
    *   登録検証失敗するとき、エラーcallback
    */
    public function error($error_msg)
    {
        if ($error_msg == "Not Authed") {
            session_destroy();
            header("Location: /admin_tool/index.php");
            return;
        }
    }
    public function auth()
    {
        if ($_SESSION["_authsession"]["data"]["status"] == 0 || $_SESSION["_authsession"]["data"]["authority"] != 2) {
            return "Not Authed";
        }
    }
    /**
    *   会議情報検索
    */
    public function action_search()
    {
        if ($downloadInfo = $this->getDownloadInfo()) {
            if ($FmsServerInfo = $this->getFmsServerAddressById($downloadInfo['server_key'])) {
                //fmsサーバーaddressとserver_key同欄表示
                 $downloadInfo['server_key'] .= "($FmsServerInfo[address])";
                $this->render_search($downloadInfo);
            }else{
                $this->action_error(' FMS_SERVER NOT FOUND ERROR ');
            }
        }
    }
    /**
    *   会議記録ダウンロード
    *   TODO ダーバーのPHP今5.2.5 , 5.3＋場合に、action_downloadとaction_searchはlambda expressionでリファクタリングする
    */
    public function action_download()
    {
        if ($downloadInfo = $this->getDownloadInfo()) {
            if ($FmsServerInfo = $this->getFmsServerAddressById($downloadInfo['server_key'])) {
                $srcLocation = "http://$FmsServerInfo[address]:$FmsServerInfo[port]/fms_download.php?fms_path=$downloadInfo[fms_path]&meeting_session=$downloadInfo[meeting_session_id]&app_name=$downloadInfo[app_name]&download_option=$downloadInfo[download_option]";
                $data = file_get_contents($srcLocation);
                if ($data == "File Not Found") { //ファイルない
                    $this->action_error("File Not Found");
                }else{ //ファイル転送
                    set_time_limit(0);
                    ini_set('memory_limit', '256M');
                    Header ( "Content-type: application/octet-stream" );  
                    Header ( "Accept-Ranges: bytes" );  
                    Header ( "Accept-Length: " . strlen( $data) );  
                    Header ( "Content-Disposition: attachment; filename= record.zip" );
                    echo $data; 
                }
            }else{
                $this->action_error(' FMS_SERVER NOT FOUND ERROR ');
            }
        }
    }
    public function getAppName()
    {
        $config_file = N2MY_APP_DIR."config/config.ini";
        $config = parse_ini_file($config_file, true);
        $app_name = $config['CORE']['app_name'];
        return $app_name;
    }
    /**
    *   会議記録ダウンロード関数を取る
    *   fms_path meeting_session_id app_name download_option 
    */
    public function getDownloadInfo()
    {
        if ($form = $this->assignFormValue()) {
            $downloadInfo = $this->getMeetingInfoByMeetingAndUserID($form['meeting_key'],$form['user_id'],true);
            if ($downloadInfo) {
                $downloadInfo['download_option'] = $form['download_option'];
                $downloadInfo['app_name'] = $this->getAppName();;
                return $downloadInfo;
            }else{
                $this->action_error(' RECORD NOT FOUND ERROR ');
            }   
        }
    }
    /**
    *   requestから、入力情報検証して処理する
    *   
    */
    public function assignFormValue()
    {
        $form = $this->request->get("form");
        $form['meeting_key'] = trim($form['meeting_key']);
        $form['user_id'] = trim($form['user_id']);
        $this->template->assign( "form_data",$form);
        $errMsg = $this->varifyFormInput($form);
        if ($errMsg) {
            $this->action_error(" $errMsg ");
            return;
        }
        return $form;
    }
    /**
    *   サーバーIDでサーバーアクセスすることに関する情報取る
    *   
    */
    public function getFmsServerAddressById($fms_server_id)
    {
        require_once('classes/mgm/dbi/FmsServer.dbi.php');
        $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
        $fmsAddressRecord = $obj_FmsServer->getServerRecordByServerKey($fms_server_id);
        $serverAddress['address'] = $fmsAddressRecord['server_address'];
        $serverAddress['port'] = $fmsAddressRecord['server_port'];
        return $serverAddress;
    }
    /**
    *   ミーティングIDとユーザーIDでダウンロードすることに関する情報取る
    *   $meeting_key 
    *   $user_id
    *   $needAllInfo=false trueの場合に、meetingテーブル全部列取ります(search)。falseの場合にダウンロードするため必要な列取ります(download)。
    */
    public function getMeetingInfoByMeetingAndUserID($meeting_key,$user_id,$needAllInfo=false)
    {
        foreach( $this->server_list["SERVER_LIST"] as $key => $dsn ){
            $user_key = $this->getUserKeyByUserIDOnServer($user_id, $dsn);
            if (!$user_key) {
                continue;
            }
            $meeting_table = new MeetingTable($dsn);
            $meetingInfo = $meeting_table->getDownloadInfoByMeetingKeyAndUserKey($meeting_key,$user_key,$needAllInfo);
            if ($meetingInfo['fms_path'] && $meetingInfo['meeting_session_id'] && $meetingInfo['server_key']) {
                $meetingInfo['dsn'] =  $dsn;
                return $meetingInfo;
            }
        }
    }
    /**
    *   ユーザーIDからユーザーKEYにコンバート
    *   
    */
    public function getUserKeyByUserIDOnServer($UserID, $dsn)
    {
        $user_table = new UserTable($dsn);
        $excapedUserId = mysql_real_escape_string($UserID);
        $where = "user_id= '$excapedUserId'";
        $searchResult = $user_table->getRow($where,"user_key as userKey ",array() ,1);
        return $searchResult['userKey'];
    }
    /**
    *   入力検証
    *   $form ページから入力内容
    */
    function varifyFormInput($form)
    {
        
        $download_option=$form['download_option'];
        if (!is_numeric( $form['meeting_key'] ) ){
            return "MEETING_KEY SHOULD BE NUMBER.";
        }
        if (!is_numeric($download_option) || $download_option>2 || $download_option<0) {
            return "UNKNOW OPTIONS.";
        }
        return null;
    }
    public function default_view()
    {
        $this->action_top();
    }

    public function action_top()
    {
        if (count($this->downloadOptions) == 0) {
            $this->downloadOptions['download_options'] = array('All','Video','Whiteboard');
            $this->downloadOptions['default_download_option_select'] = 0;
        }
        $this->template->assign( "title", 'Meeting Record Download' );
        $this->template->assign( "download_options", $this->downloadOptions['download_options'] );
        $this->template->assign( "default_download_option_select", $this->downloadOptions['default_download_option_select'] );
        $this->display( "admin_tool/meeting_record_download.t.html" );
    }
    /**
    *  エラー表示
    *   $msg 表示したいエラーッセージ
    */
    public function action_error($msg)
    {
        $info['error'] = 1;
        $info['error_msg'] = " $msg ";
        $this->template->assign( "info", $info );
        $this->action_top();
    }
    /**
    *  検索したミーティング情報を表示
    *   $meetingInfo 表示したいミーティング情報
    */
    public function render_search($meetingInfo)
    {
        $data_db = new N2MY_DB($meetingInfo['dsn'], "meeting");
        $columns = $data_db->getTableInfo('data');
        $this->template->assign( "columns", $columns );
        $this->template->assign( "row", $meetingInfo );
        $this->template->assign( "searchResult", 1 );
        if ($meetingInfo['has_recorded_video'] == 0 ||  //don't have flv
            $meetingInfo['has_recorded_minutes'] == 0 //don't have fso
            ) {
            $this->downloadOptions['download_options'] = $meetingInfo['has_recorded_video']?array(1=>'Video'):array(2=>'Whiteboard');
            $this->downloadOptions['default_download_option_select'] = $meetingInfo['has_recorded_video']?1:2;
            if ($meetingInfo['has_recorded_video'] == $meetingInfo['has_recorded_minutes']) {
                $this->downloadOptions['download_options'] = array('No record');
            }
        }
        $this->action_top();
    }
}


$main =& new AppRecordDownload();
$main->execute();