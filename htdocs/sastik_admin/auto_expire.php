<?php
require_once("classes/AppFrame.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/mgm/dbi/user.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("lib/EZLib/EZUtil/EZCsv.class.php");

class AppUserExpire extends AppFrame {

    var $_sess_page;
    var $account_dsn = null;
    var $objUser = null;

    /**
     * 初期化
     */
    function init() {
        $_COOKIE["lang"] = "ja";
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->_sess_page = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->_name_space = md5(__FILE__);
        if ($this->get_dsn()) {
            $this->objUser = new UserTable($this->get_dsn());
        }
    }

    function get_dsn() {
        if (isset($_SESSION[$this->_sess_page]["dsn"])) {
            return $_SESSION[$this->_sess_page]["dsn"];
        }
    }

    function action_download() {
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="user_expire_template.csv"');
        print file_get_contents(N2MY_APP_DIR."templates/common/sastik_admin/user/expire_sample.csv");
    }

    /**
     * ユーザー一括予約停止確認
     */
    public function action_import_confirm() {
        $err_msg = array();
        // ファイルアップロード
        if ($_FILES["import_file"]['error']) {
            $err_msg[] = "ファイルが選択されていません。CSVファイルをアップロードしてください";
        } else {
            $request["upload_file"] = $_FILES["import_file"];
            $data = pathinfo($request["upload_file"]["name"]);
            if ($data['extension'] != "csv") {
                $err_msg[] = "ファイルがCSVではありません。CSVファイルをアップロードしてください";
            } else {
                // パース
                $filename = $this->get_work_dir()."/".tmpfile();
                move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
                $request["upload_file"]["tmp_name"] = $filename;
                $this->session->set('user_info', $request);
                // メッセージ
                $msg_format = $this->get_message("error");
                $msg = $this->get_message("ERROR");
                $csv = new EZCsv(true, "UTF-8", "UTF-8");
                $csv->open($filename, "r");
                $rows = array();
                while($row = $csv->getNext(true)) {
                    $rows[] = $row;
                }
                $csv->close();
                if (count($rows) == 0) {
                    $err_msg[] = 'データがありません';
                }
            }
        }

        $err_flg = 0;

        foreach($rows as $row) {
            // エラーチェック
            if (!$this->user_check($row)) {
                $err_flg = 1;
            }
        }
        if ($err_flg == 1) {
            $err_msg[] = 'CSVファイルの情報にエラーがあります。入力した内容を見直して下さい';
        }
        if ($err_msg) {
            unlink($filename);
        } else {
            file_put_contents($filename, serialize($rows));
        }
        $this->template->assign('form_err_msg', $err_msg);
        $this->template->assign("err_flg", $err_flg);
        $this->template->assign("rows", $rows);
        $this->display('sastik_admin/user/expire_list.t.html');
    }

    /**
     * ユーザー一括削除確認
     */
    function action_import() {
        // アップロード済みのファイルを読み込み
        $user_info = $this->session->get('user_info');
        $contents = file_get_contents($user_info["upload_file"]["tmp_name"]);
        // 件数確認
        $rows = unserialize($contents);
        if (count($rows) == 0) {
            $err_msg[] = 'データがありません';
        } else {
            // データが正常か確認
            foreach ($rows as $key => $row) {
                if (!$this->user_check($row)) {
                    $err_msg[] = 'データが正しく有りません。もう一度確認して下さい。';
                    break;
                } else {
                    $rows[$key] = $row;
                }
            }
        }
        // エラー
        if ($err_msg) {
            print_r($err_msg);
        } else {
            $result = array(
                "ok" => 0,
                "ng" => 0,
                );
            $ok_user_list = array();
            $ng_user_list = array();
            foreach($rows as $row) {
                // 予約停止日登録
                $ret = $this->user_reservation_expire($row);
                if (DB::isError($ret)) {
                    $this->logger2->error($ret->getUserInfo());
                    $result["ng"]++;
                    $ng_user_list[] = $row;
                } else {
                    $this->logger->info(__FUNCTION__."#user reservation expire successful!",__FILE__,__LINE__,$row);
                    $result["ok"]++;
                    $ok_user_list[] = $row;
                }
            }
        }
        // 結果表示
        $this->template->assign('ok_user_list', $ok_user_list);
        $this->template->assign('ng_user_list', $ng_user_list);
        $this->template->assign("result", $result);
        $this->display('sastik_admin/user/expire_done_list.t.html');
    }

    /**
     * ユーザー予約停止登録
     */
    function user_reservation_expire($user_info) {

        $expiredatetime = $user_info["user_expiredatetime"];
        $today = date("Y/m/d H:i:s");
        if (strtotime($expiredatetime) < strtotime($today)) {
             $this->user_reservation_expire('CSVファイルの停止予定日は、'.$today.'以降の日にちを設定する必要があります');
             exit;
        }
        $data["user_expiredatetime"] = $expiredatetime;
        $data["user_updatetime"] = date("Y-m-d H:i:s");
        $data["user_delete_status"] = 1;
        $user_id = $user_info["user_id"];
        $where = "user_id = '".addslashes($user_id)."'";

        $data_db = new N2MY_DB($this->get_dsn(), "user");
        $result = $data_db->update($data, $where);

        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "user",
            "keyword"            => "user_id",
            "info"               => $user_id,
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);

        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
            return false;
        }

        $this->logger->info(__FUNCTION__."#user reservation expire successful!",__FILE__,__LINE__,$data);
        return true;
    }

    /**
     * 入力確認
     * @param mixed $data 確認データ
     */
    function user_check($data) {
        $expiredatetime = $data["user_expiredatetime"];
        $this->check_obj = new EZValidator($data);

        // 日付の範囲チェック
        if (strtotime($expiredatetime) < strtotime(date("Y/m/d H:i:s"))) {
            $this->check_obj->set_error("user_expiredatetime", "expiredatetime_range_over");
        }
        //member, user, 認証DBのuser のテーブルをチェック
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        // ユーザー存在チェック
        if ($obj_MgmUserTable->numRows( sprintf("user_id='%s'", stripslashes($data["user_id"]) ) )  ==  0 ){
            $this->check_obj->set_error("user_id", "user_notfound");
        }
        if (EZValidator::isError($this->check_obj)) {
            return false;
        }
        return true;
    }

    public function default_view() {
         $this->action_top();
    }

    public function action_top() {
        $this->display( "sastik_admin/user/expire_list.t.html" );
    }
}

$main =& new AppUserExpire();
$main->execute();