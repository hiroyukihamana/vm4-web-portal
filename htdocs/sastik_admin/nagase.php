<?php
require_once("classes/AppFrame.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/mgm/dbi/user.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/member_group.dbi.php");
require_once("lib/EZLib/EZUtil/EZCsv.class.php");

class AppMember extends AppFrame {

    var $_auth = null;
    var $_sess_page;
    var $account_dsn = null;
    var $objUser = null;
    var $objMember = null;
    var $server_key = null;

    /**
     * 初期化
     */
    function init() {
        $_COOKIE["lang"] = "ja";
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->_sess_page = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->_name_space = md5(__FILE__);
        if ($this->get_dsn()) {
            $this->objUser = new UserTable($this->get_dsn());
            $this->objMember = new MemberTable($this->get_dsn());
            $this->objMemberGroup = new MemberGroupTable($this->get_dsn());
        }
    }

    /**
     * DSN取得
     */
    function get_dsn() {
        static $dsn;
        if ($dsn) {
            return $dsn;
        } else {
            if ($id = $this->request->get("user_id")) {
                $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
                if (!$user_info = $obj_MGMClass->getUserInfoById( $id ) ){
                    die("ユーザーIDが間違っています");
                    return false;
                }
                if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
                    die("サーバー情報が有りません");
                    return false;
                }
            }
        }
        $this->logger2->info($server_info);
        $dsn = $server_info["dsn"];
        return $dsn;
    }

    function action_download() {
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="member_template.csv"');
        print file_get_contents(N2MY_APP_DIR."templates/common/sastik_admin/member/sample.csv");
    }

    /**
     * メンバー一括登録確認
     */
    public function action_import_confirm() {
        //
        $user_id = $this->request->get("user_id");
        $strict = $this->request->get("strict", 0);
        $err_msg = array();
        if (!trim($user_id)) {
            $err_msg[] = "ユーザーIDが入力されていません";
        } else {
            $where = "user_id = '".addslashes($user_id)."'";
            $user_info = $this->objUser->getRow($where);
            if (!$user_info) {
                $err_msg[] = "ユーザーIDが正しくありません";
            }
        }
        // ファイルアップロード
        if ($_FILES["import_file"]['error']) {
            $err_msg[] = "CSVファイルをアップロードしてください";
        } else {
            $request["upload_file"] = $_FILES["import_file"];
            $data = pathinfo($request["upload_file"]["name"]);
            if ($data['extension'] != "csv") {
                $err_msg[] = "CSVファイルをアップロードしてください";
            } else {
                // パース
                $filename = $this->get_work_dir()."/".tmpfile();
                move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
                $request["upload_file"]["tmp_name"] = $filename;
                $this->session->set('member_info', $request);
                // メッセージ
                $msg_format = $this->get_message("error");
                $msg = $this->get_message("ERROR");
                $csv = new EZCsv(true, "UTF-8", "UTF-8");
                $csv->open($filename, "r");
                $rows = array();
                while($row = $csv->getNext(true)) {
                    $row["user_key"] = $user_info["user_key"];
                    $group_info = $this->getGroup($user_info["user_key"], $row["member_group_name"]);
                    $row["member_group"] = isset($group_info["member_group_key"]) ? $group_info["member_group_key"] : "";
                    $row["member_status"] = 0;
                    $rows[] = $row;
                }
                $csv->close();
                if (count($rows) == 0) {
                    $err_msg[] = 'データがありません';
                } else {
                    if($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") {
                        // 現在の登録件数
                        $where = 'user_key = '.$user_info["user_key"].
                            ' AND member_status = 0';
                        $now_count = $this->objMember->numRows($where);
                        if ($user_info["max_member_count"] <= $now_count + count($rows)) {
                            $err_msg[] = MEMBER_ERROR_OVER_COUNT;
                        }
                    }
                }
            }
        }
        $err_flg = 0;
        // エラーチェック
        foreach($rows as $row) {
            if (!$this->member_check($row, $strict)) {
                $err_fields = $this->_error_obj->error_fields();
                $error = array();
                foreach ($err_fields as $key) {
                    $type = $this->_error_obj->get_error_type($key);
                    $_msg = sprintf($msg[$type], $this->_error_obj->get_error_rule($key, $type));
                    $error[$key] = $_msg;
                    $err_flg = 1;
                }
                $row["error"] = $error;
            }
        }
        if ($err_flg == 1) {
            $err_msg[] = 'CSVファイルの情報にエラーがあります';
        }
        if ($err_msg) {
            unlink($filename);
        } else {
            file_put_contents($filename, serialize($rows));
        }
        $this->template->assign('user_info', $user_info);
        $this->template->assign('form_err_msg', $err_msg);
        $this->template->assign("err_flg", $err_flg);
        $this->template->assign("rows", $rows);
        $this->display('sastik_admin/member/add_list.t.html');
    }

    /**
     * メンバー一括登録確認
     */
    function action_import() {
        // ユーザー情報
        $user_id = $this->request->get("user_id");
        $strict  = $this->request->get("strict");
        $err_msg = array();
        if (!trim($user_id)) {
            $err_msg[] = "ユーザーIDが入力されていません";
        }
        $where = "user_id = '".addslashes($user_id)."'";
        $user_info = $this->objUser->getRow($where);
        if (!$user_info) {
            $err_msg[] = "ユーザーIDが正しくありません";
        }
        // アップロード済みのファイルを読み込み
        $member_info = $this->session->get('member_info');
        $contents = file_get_contents($member_info["upload_file"]["tmp_name"]);
        // 件数確認
        $rows = unserialize($contents);
        if (count($rows) == 0) {
            $err_msg[] = 'データがありません';
        } else {
            // メンバー、センター課金時は上限件数を確認
            if ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") {
                // 現在の登録件数
                $where = 'user_key = '.$user_info["user_key"].
                    ' AND member_status = 0';
                $now_count = $this->objMember->numRows($where);
                if ($user_info["max_member_count"] <= $now_count + count($rows)) {
                    $err_msg[] = MEMBER_ERROR_OVER_COUNT;
                }
            }
            // データが正常か確認
            foreach ($rows as $key => $row) {
                $row["user_key"] = $user_info["user_key"];
                $group_info = $this->getGroup($user_info["user_key"], $row["member_group_name"], $this->request->get('auto_group'));
                unset($row["member_group_name"]);
                $row["member_group"] = isset($group_info["member_group_key"]) ? $group_info["member_group_key"] : "";
                if (!$row["member_pass"] && $this->request->get('auto_pw')) {
                    $row["member_pass"] = $this->create_pw();
                }
                $row["member_status"] = 0;
                // メンバータイプ
                if ($user_info["account_model"] == "member" && $row["member_type"] ) {
                    $row["member_type"] = '';
                }
                if (!$this->member_check($row, $strict)) {
                    $err_msg[] = 'データが正しく有りません。もう一度確認して下さい。';
                    break;
                } else {
                    $rows[$key] = $row;
                }
            }
        }
        // エラー
        if ($err_msg) {
            print_r($err_msg);
        } else {
            // サーバーKEY取得
            $obj_MgmUserTable = new MgmUserTable( N2MY_MDB_DSN );
            $where = "user_id = '".addslashes($user_id)."'";
            $this->server_key = $obj_MgmUserTable->getOne($where, "server_key");
            $result = array(
                "ok" => 0,
                "ng" => 0,
                );
            $ok_member_list = array();
            $ng_member_list = array();
            foreach($rows as $row) {
                // メンバー登録
                require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
                $row["member_pass"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $row["member_pass"]);
                $member_room_keys = explode(",", $row["room_key"]);
                $row["room_key"] = $member_room_keys[0];
                $row = array_merge(array_diff($row, array("")));
                $ret = $this->member_add($row);
                if (DB::isError($ret)) {
                    $this->logger2->error($ret->getUserInfo());
                    $result["ng"]++;
                    $ng_member_list[] = $row;
                } else {
                    //表示用に復号化
                    $row["member_pass"] =  EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $row["member_pass"]);
                    $this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$row);
                    $result["ok"]++;
                    $ok_member_list[] = $row;
                    if ($row["member_type"] == "terminal") {
                        //部屋のリレーション追加
                        $add_member_info = $this->objMember->getRow("member_id = '".addslashes($row['member_id'])."'");
                        if ($member_room_keys && $add_member_info) {
                            $mrRation_db = new N2MY_DB($this->get_dsn(), "member_room_relation");
                            foreach ($member_room_keys as $room_key) {
                                $mrr = $mrRation_db->getRow(sprintf("member_key = '%s' AND room_key = '%s'", $add_member_info["member_key"], $room_key));
                                if(!$mrr){
                                    $data = array("member_key" => $add_member_info["member_key"],
                                                  "room_key"   => $room_key,
                                                  "create_datetime" => date("Y-m-d H:i:s"));
                                    $this->logger2->info($data);
                                    $mrRation_db->add($data);
                                    if (DB::isError($ret)) {
                                        $this->logger2->info($res->getMessage());
                                        $this->logger2->error(__FILE__.__LINE__.":".__FUNCTION__."#DB ERROR!");
                                        throw new Exception("database error");
                                    }
                                }
                            }
                        }
                    }
                    // メンバー、センター課金時は上限件数を確認
                    if ($user_info['account_model'] == "member" || ($user_info['account_model'] == "centre" && $row["member_type"] == "centre")
                       || $user_info['account_model'] == "free") {
                        // 部屋追加
                        require_once( "classes/dbi/room.dbi.php" );
                        require_once( "classes/dbi/user_plan.dbi.php" );
                        require_once( "classes/dbi/room_plan.dbi.php" );
                        $objUserPlan = new UserPlanTable( $this->get_dsn() );
                        $objRoomPlanTable = new RoomPlanTable( $this->get_dsn() );
                        if ($now_plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1")) {
                            $service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
                            $service_info = $service_db->getRow("service_key = '".addslashes($now_plan_info['service_key'])."' AND service_status = 1");
                            $this->logger2->info(array($now_plan_info, $service_info));
                        }
                        $where = sprintf( "user_key=%d", $user_info["user_key"] );
                        $obj_Room = new RoomTable( $this->get_dsn() );
                        $count = $obj_Room->numRows($where);
                        $room_key = $user_info["user_id"]."-".($count + 1)."-".substr( md5( uniqid( time(), true ) ), 0, 4 );
                        $room_data = array (
                            "room_key"              => $room_key,
                            "max_seat"              => ($service_info["max_seat"]) ? $service_info["max_seat"] : 10,
                            "max_audience_seat"     => ($service_info["max_audience_seat"]) ? $service_info["max_audience_seat"] : 0,
                            "max_whiteboard_seat"   => ($service_info["max_whiteboard_seat"]) ? $service_info["max_whiteboard_seat"] : 0,
                            "max_room_bandwidth"    => ($service_info["max_room_bandwidth"]) ? $service_info["max_room_bandwidth"] : 2048,
                            "max_user_bandwidth"    => ($service_info["max_user_bandwidth"]) ? $service_info["max_user_bandwidth"] : 256,
                            "min_user_bandwidth"    => ($service_info["min_user_bandwidth"]) ? $service_info["min_user_bandwidth"] : 30,
                            "meeting_limit_time"    => ($service_info["meeting_limit_time"]) ? $service_info["meeting_limit_time"] : 0,
                            "default_camera_size"   => ($service_info["default_camera_size"]) ? $service_info["default_camera_size"] : "",
                            "user_key"              => $user_info["user_key"],
                            "room_name"             => $row['member_name'],
                            "room_status"           => "1",
                            "room_registtime"       => date("Y-m-d H:i:s"),
                            );
                        $this->logger2->info($room_data);
                        if ($service_info["service_name"] == "Paperless_Free") {
                            $room_data["max_seat"] = 0;
                            $room_data["whiteboard_page"] = "10";
                            $room_data["whiteboard_size"] = "2";
                            $whiteboard_filetype = array(
                                "document" => array("ppt","pdf"),
                                "image"    => array("jpg"),
                            );
                            $room_data["whiteboard_filetype"] =serialize($whiteboard_filetype);
                        }
                        if ($service_info["service_name"] == "VCUBE_Doctor_Standard") {
                            $room_data["invited_limit_time"] = $service_info["invited_limit_time"] ? $service_info["invited_limit_time"] : 30;
                        }
                        $add_room = $obj_Room->add($room_data);
                        if (DB::isError($add_room)) {
                            return $this->logger2->error( $add_room->getUserInfo());
                        }
                        $ret = $this->objMember->update(
                            array("room_key" => $room_key),
                            "member_id = '".addslashes($row['member_id'])."'");
                        if (DB::isError($ret)) {
                            return $this->logger2->error( $add_room->getUserInfo());
                        }
                        //部屋のリレーション追加
                        $this->logger2->info($room_key);
                        if ($room_key && ($user_info["account_model"] == "member" || $row["member_type"] == "centre" || $row["member_type"] == "free")) {
                            $add_member_info = $this->objMember->getRow("member_id = '".addslashes($row['member_id'])."'");
                            $mrRation_db = new N2MY_DB($this->get_dsn(), "member_room_relation");
                            $mrr = $mrRation_db->getRow(sprintf("member_key = '%s' AND room_key = '%s'", $add_member_info["member_key"], $room_key));
                            if(!$mrr){
                              $relationData = array();
                              $relationData["member_key"]      = $add_member_info["member_key"];
                              $relationData["room_key"]        = $room_key;
                              $relationData["create_datetime"] = date("Y-m-d H:i:s");
                              $res = $mrRation_db->add($relationData);
                              if (PEAR::isError($res)) {
                                  $this->logger2->info($res->getMessage());
                                  $this->logger2->error(__FILE__.__LINE__.":".__FUNCTION__."#DB ERROR!");
                                  throw new Exception("database error");
                              }
                            }
                        }
                        //relation tableへ追加
                        require_once( "classes/mgm/MGM_Auth.class.php" );
                        $obj_MGMAuthClass = new MGM_AuthClass( $this->account_dsn );
                        $obj_MGMAuthClass->addRelationData( $user_info["user_id"], $room_key, "mfp" );
                        // プラン設定
                        $planList = $objUserPlan->getRowsAssoc(sprintf("user_key='%s'", $user_info["user_key"]));
                        for( $i = 0; $i < count( $planList ); $i++ ){
                            $data = array(
                                "room_key"                  => $room_key,
                                "service_key"               => $planList[$i]["service_key"],
                                "discount_rate"             => $planList[$i]["discount_rate"],
                                "contract_month_number"     => $planList[$i]["contract_month_number"],
                                "room_plan_yearly"          => $planList[$i]["user_plan_yearly"],
                                "room_plan_yearly_starttime"=> $planList[$i]["user_plan_yearly_starttime"],
                                "room_plan_starttime"       => $planList[$i]["user_plan_starttime"],
                                "room_plan_endtime"         => $planList[$i]["user_plan_endtime"],
                                "room_plan_status"          => $planList[$i]["user_plan_status"],
                                "room_plan_registtime"      => $planList[$i]["user_plan_registtime"],
                                "room_plan_updatetime"      => $planList[$i]["user_plan_updatetime"]
                             );
                            $addPlan = $objRoomPlanTable->add( $data );
                            if (DB::isError( $addPlan ) ) {
                                $this->logger2->info( $addPlan->getUserInfo() );
                            }
                        }
                        // オプション追加
                        require_once("classes/dbi/ordered_service_option.dbi.php");
                        $ordered_service_option = new OrderedServiceOptionTable( $this->get_dsn() );
                        $this->objMeetingDB = new N2MY_DB( $this->get_dsn(), "user_service_option" );
                        $condition = sprintf( "user_key='%s' AND service_option_key !=14", $user_info["user_key"] );
                        $optionList = $this->objMeetingDB->getRowsAssoc( $condition );
                        for ( $i = 0; $i < count( $optionList ); $i++ ) {
                            $data = array(
                                "room_key"                          => $room_key,
                                "user_service_option_key"           => $optionList[$i]["user_service_option_key"],
                                "service_option_key"                => $optionList[$i]["service_option_key"],
                                "ordered_service_option_status"     => $optionList[$i]["user_service_option_status"],
                                "ordered_service_option_starttime"  => $optionList[$i]["user_service_option_starttime"],
                                "ordered_service_option_registtime" => $optionList[$i]["user_service_option_registtime"],
                                "ordered_service_option_deletetime" => $optionList[$i]["user_service_option_deletetime"]
                            );
                            $ordered_service_option->add( $data );
                        }
                    }
                    //vcubeidに登録
                    $wsdl = $this->config->get('VCUBEID','wsdl');
                    if ($wsdl) {
                    }
                }
            }
        }
        // 結果表示
        $this->template->assign('ok_member_list', $ok_member_list);
        $this->template->assign('ng_member_list', $ng_member_list);
        $this->template->assign("result", $result);
        $this->display('sastik_admin/member/done_list.t.html');
    }

    /**
     * メンバー追加
     */
    function member_add($member_info) {
        //認証サーバーへ追加
        $obj_MgmUserTable = new MgmUserTable( N2MY_MDB_DSN );
        $auth_data = array(
            "user_id"     => $member_info['member_id'],
            "server_key"  => $this->server_key
            );
        $result = $obj_MgmUserTable->add( $auth_data );
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
            return false;
        } else {
            $ret = $this->objMember->add($member_info);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                return false;
            } else {
                $this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$auth_data);
                return true;
            }
        }
    }

    function getGroup($user_key, $group_name, $auto_add = false) {
        static $member_group_list = null;
        if (!isset($member_group_list)) {
            $where = "user_key = ".$user_key;
            $member_group_list = $this->objMemberGroup->getRowsAssoc($where, null, null, null, null, "member_group_name");
            $this->logger2->info($member_group_list);
        }
        if (isset($member_group_list[$group_name])) {
            return $member_group_list[$group_name];
        } else {
            // 無ければ自動追加
            if ($auto_add && trim($group_name) != "") {
                $data = array(
                    'user_key'                => $user_key,
                    'member_group_name'       => trim($group_name),
                );
                $key = $this->objMemberGroup->add($data);
                if (DB::isError($key)) {
                    $this->logger2->warn($key->getUserInfo());
                } else {
                    $data['member_group_key'] = $key;
                    $member_group_list[$group_name] = $data;
                    return $key;
                }
            }
        }
        return false;
    }

    /**
     * 入力確認
     * @param mixed $data 確認データ
     * @param string $mode チェックモード(追加, 更新)
     */
    function member_check($data, $stricit) {
        //member, user, 認証DBのuser のテーブルをチェック
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        // 重複
        if ($obj_MgmUserTable->numRows( sprintf("user_id='%s'", mysql_real_escape_string($data["member_id"]) ) )  > 0 ){
            $duplication = true;
        }
        if ($this->request->get("auto_pw")) {
            if ($data["member_pass"] == "") {
                $data["member_pass"] = 'abcde01234';
            }
        }
        // 厳密な入力チェック
        if (!$stricit) {
            $this->objMember->rules = array(
                "member_id" => array(
                    "minlen" => 3,
                    "maxlen" => 200,
                    "required" => true,
                    ),
                "member_pass" => array(
                    "required" => true,
                    ),
                "member_email" => array(
//                    "required" => true,
                    "email" => true,
                    ),
                "member_status" => array(
                    "required" => true,
                    ),
                "member_type" => array(
                    "allow" => array("centre", "terminal", "free")
                ),
            );
            $this->_error_obj = $this->objMember->check($data, $this->objMember->rules);
        } else {
            $this->_error_obj = $this->objMember->default_check($data);
        }
        if ($duplication) {
            $this->_error_obj->set_error("member_id", "duplicate");
        }
        if (!$this->request->get("auto_group")) {
            if ($data["member_group_name"] != "" && $data["member_group"] == "") {
                $this->_error_obj->set_error("member_group", "not_equal");
            }
        }
        if ($data["member_type"] == "terminal") {
            // Terminalの際は部屋キー必須
            $where = "user_id = '".addslashes($this->request->get("user_id"))."'";
            $user_info = $this->objUser->getRow($where);
            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"] );
            if (!$data['room_key'] && $data["member_type"] == "terminal") {
                $this->_error_obj->set_error("room_key", "roomkey_deny");
            } else {
                $member_room_keys = explode(",", $data['room_key']);
                foreach ($member_room_keys as $member_room_key) {
                    if (!array_key_exists($member_room_key, $rooms)) {
                         $this->logger2->info($member_room_key);
                         $this->_error_obj->set_error("room_key", "roomkey_deny");
                    }
                }
            }
        }
        if (EZValidator::isError($this->_error_obj)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * XML形式でエクスポート
     */
    public function action_export() {
        // ユーザー情報
        $user_id = $this->request->get("user_id");
        if (!trim($user_id)) {
            die("ユーザーIDが入力されていません");
        }
        $strict  = $this->request->get("strict");
        $where = "user_id = '".addslashes($user_id)."'";
        $user_info = $this->objUser->getRow($where);
        if (!$user_info) {
            die("ユーザーIDが正しくありません");
        }
        $where = "user_id = '".addslashes($user_id)."'";
        $user_info = $this->objUser->getRow($where);
        // ヘッダ
        header('Content-Type: text/xml;');
        header('Content-Disposition: attachment; filename="member_'.date("Ym").'.xml"');
        print '<?xml version="1.0" encoding="UTF-8"?>'."\n";
        print '<userList name="status" value="1">'."\n";
        $user_key = $user_info["user_key"];
        // グループ一覧
        $where = "user_key = ".$user_key;
        $sort = array("member_group_name" => "asc");
        $group_list = $this->objMemberGroup->getRowsAssoc($where, $sort, null, null, "member_group_key,member_group_name","member_group_key");
        // ユーザー一覧
        $where = "user_key = ".$user_key.
            " AND member_status >= 0";
        $sort = array(
            "member_group" => "asc",
            "member_name" => "asc",
        );
        $_list = $this->objMember->getRowsAssoc($where, $sort);
        foreach($_list as $_key => $row) {
            $group_id = $row["member_group"];
            if ($group_list[$group_id]) {
                $group_list[$group_id]["id"] = $group_id;
                $group_list[$group_id]["name"] = $group_list[$group_id]["member_group_name"];
                $group_list[$group_id]["members"][] = $row;
            } else {
                $group_list["undefined"]["id"] = $group_id;
                $group_list["undefined"]["name"] = $group_list[$group_id]["member_group_name"];
                $group_list["undefined"]["members"][] = $row;
            }
        }
        $this->logger2->info($group_list);
        // グループ
        foreach ($group_list as $group) {
            print '<group type="String" name="'.$group["name"].'" value="'.$group["id"].'" role="group">'."\n";
            $member_list = $group["members"];
            // メンバー
            foreach ($member_list as $member) {
                print ' <userInfo type="String" name="'.$member["member_name"].'" value="'.$member["member_key"].'" role="user" status="0"/>'."\n";
            }
            print '</group>'."\n";
        }
        print '</userList>'."\n";
    }

    public function default_view() {
         $this->action_top();
    }

    /**
     * 乱数PW発行
     */
    function create_pw() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        $c .= rand(2,9);
        return $this->dec2any($c);
    }

    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=57, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr(
                    '23456789ab' .
                    'cdefghijkm' .
                    'nopqrstuvw' .
                    'xyzABCDEFG' .
                    'HJKLMNPQRS' .
                    'TUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }

    public function action_top() {
        $this->display( "sastik_admin/member/add_list.t.html" );
    }
}


$main =& new AppMember();
$main->execute();