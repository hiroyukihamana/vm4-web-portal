<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("Auth.php");
require_once("Pager.php");
require_once("config/config.inc.php");
require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/N2MY_IvesClient.class.php");

class AppAdminTool extends AppFrame {

    var $_auth = null;
    var $_sess_page;
    var $account_dsn = null;

    function init() {
        $_COOKIE["lang"]   = "ja";
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->_sess_page  = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->_name_space = md5(__FILE__);
    }

    function set_dsn($dsn) {
        $_SESSION[$this->_sess_page]["dsn"] = $dsn;
    }

    function get_dsn() {
        if (isset($_SESSION[$this->_sess_page]["dsn"])) {
            return $_SESSION[$this->_sess_page]["dsn"];
        }
    }

    /**
     *
     */
    function auth() {
        $auth_detail = "*";
        $this->_auth = new Auth("DB", array(
            "dsn" => $this->account_dsn,
            "table" => "staff",
            "cryptType" => "sha1",
            "db_fields" => $auth_detail,
            "usernamecol" => "login_id",
            "passwordcol" => "login_password"
            ));
        if (!$this->_auth->getAuth()) {
            $this->_auth->start();
            if ($_SESSION["_authsession"]["data"]["status"] == "0") {
                $this->_auth->logout();
                $this->request->set("password", "");
                $this->_auth->start();
            }
            if ($this->_auth->checkAuth()) {
                //操作ログ登録
                $operation_data = array (
                    "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                    "action_name"        => "login",
                    "table_name"         => "staff",
                    "keyword"            => $_SESSION["_authsession"]["username"],
                    "info"               => "login",
                    "operation_datetime" => date("Y-m-d H:i:s"),
                );
                $this->add_operation_log($operation_data);
            } else {
                exit();
            }
        }
    }

    /**
     * 一覧取得
     */
    function action_list() {
        $this->logger->debug("request",__FILE__,__LINE__,$_REQUEST);
        $reset = $this->request->get("reset");
        if ($reset == 1) {
            $this->session->remove("temp", "action_list");
            $this->session->remove("user_data");
            $this->session->remove("form_data");
            $this->session->remove("before_data");
        }
        $regist = $this->request->get("regist");
        if ($regist) {
            $this->session->set("regist", $regist);
        }
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $this->logger->trace("request",__FILE__,__LINE__,$request);
        $this->session->remove("where_data");
        $required = $this->session->get("required");
        $message = $this->session->get("message");
        $this->logger->trace("required",__FILE__,__LINE__,$required);
        if ($required || $message) {
            $this->template->assign("message",$message);
            $this->template->assign("required",$required);
        }
        $type = $this->request->get("type");
        if (!$type) {
            $type = $this->session->get("type");
        }
        if ($type == "account") {
            $dsn = $this->account_dsn;
        } else {
            $dsn = $this->get_dsn();
        }
        $table = $this->request->get("table");
        if (!$table) {
            $table = $this->session->get("table");
        } else {
            $this->session->remove("check");
        }
        $this->session->set("type", $type);
        $this->session->set("table", $table);
        $check = $this->request->get("check");
        if (!$check) {
            $check = $this->session->get("check");
        } else {
            $this->session->set("check", $check);
        }
        $this->logger->trace("check_list",__FILE__,__LINE__,$check);
        $sort_key = $this->request->get("sort_key");
        $sort_type = $this->request->get("sort_type");
        if (!$sort_type) {
            $sort_type = "asc";
        }
        //会議一覧の時はfmsサーバー取得（FMSのログを参照するため）
        if ($table == "meeting") {
            $fms_db = new N2MY_DB($this->account_dsn, "fms_server");
            $_server_info = $fms_db->getRowsAssoc("", null ,null, null, null, null);
            $this->logger->trace("fms_server",__FILE__,__LINE__,$_server_info);
            foreach($_server_info as $_key => $val) {
                $server_info[$val["server_key"]] = $val;
            }
            $fms_app_name = $this->config->get("CORE", "app_name", "Vrms");
            $fms = array(
                "server_info" => $server_info,
                "app_name" => $fms_app_name,
                );
            $this->template->assign("fms",$fms);
        }
        $data_db = new N2MY_DB($dsn, $table);
        $this->logger->trace("dsn",__FILE__,__LINE__,$dsn);
        $action_name = __FUNCTION__;
        $columns = $data_db->getTableInfo($type);
        $columns = $this->_get_relation_data($dsn, $columns);
        $this->logger->trace("columns",__FILE__,__LINE__,$columns);
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $sort_key, $sort_type);
        $this->logger->debug("form_data",__FILE__,__LINE__,$form_data);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
            $this->template->assign("request",$request);
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where = $data_db->getWhere($columns, $request);
        $this->logger->debug("where",__FILE__,__LINE__,$where);
        // CSVダウンロード
        if (isset($_REQUEST["csv"])) {
            //操作ログ登録
            $operation_data = array (
                "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => $table,
                "keyword"            => "csv_download",
                "info"               => serialize($form_data),
                "operation_datetime" => date("Y-m-d H:i:s"),
            );
            $this->add_operation_log($operation_data);
            require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            $csv = new EZCsv(true, "UTF-8", "UTF-8");
            $dir = $this->get_work_dir();
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");
            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="'.date("Ym").'.csv"');
            // ヘッダ２
            foreach($columns as $key => $column) {
                $header[$key] = $column["label"];
            }
            $csv->setHeader($header);
            $csv->write($header);

            // データ取得
            $rs = $data_db->select($where, array($form_data['sort_key'] => $form_data['sort_type']));
            while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                $csv->write($row);
            }

//            $rows = $data_db->getRowsAssoc($where, array($form_data['sort_key'] => $form_data['sort_type']));
//            foreach($rows as $row) {
//                $csv->write($row);
//            }
            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            while($str = fread($fp, 4096)) {
                print $str;
            }
            @fclose($fp);
//            $contents = fread($fp, filesize($tmpfile));
//            print $contents;
            $this->logger2->info(memory_get_peak_usage());
            unlink($tmpfile);
            return true;
        } else {
        // データ取得
        $rows = $data_db->getRowsAssoc($where, array($form_data['sort_key'] => $form_data['sort_type']), $limit, $offset);
        }
        $total_count = $data_db->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name,
            "after_action" => $this->request->get("after_action"))
            );
        $add_form = $this->session->get("temp", $action_name);
        $this->template->assign("add_form", array(
            "data" => $add_form,
            "ok_action" => $action_name,
            "ng_action" => $action_name
          ));
        $this->template->assign("columns",$columns);
        $this->template->assign("rows",$rows);
        $this->template->assign("type",$type);
        $this->template->assign("table",$table);
        $this->session->remove("message");
        if ($type == "account") {
            $template = "sastik_admin/account/".$table."/index.t.html";
        } else {
            $template = "sastik_admin/".$table."/index.t.html";
        }
        $this->_display($template);
    }

    /**
     * 優先DC・除外DC画面
     */
    function action_datacenter_list() {
        $this->logger->debug("request",__FILE__,__LINE__,$_REQUEST);
        if ($this->request->get("reset") == 1) {
            $this->session->remove("temp", "action_list");
            $this->session->remove("user_data");
            $this->session->remove("form_data");
            $this->session->remove("before_data");
        }
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $this->session->set("request", $request);

        $this->logger->trace("request",__FILE__,__LINE__,$request);
        $dsn = $this->get_dsn();
        $table = $this->request->get("table");
        if (!$table) {
            $table = $this->session->get("table");
        }
        $this->session->set("table", $table);

        require_once("classes/dbi/". $table .".dbi.php");
        if ($table == "datacenter_priority") {
            $datacenter_db = new DatacenterPriorityTable($dsn);
        } else {
            $datacenter_db = new DatacenterIgnoreTable($dsn);
        }
        $action_name = __FUNCTION__;
        $columns = $datacenter_db->getTableInfo("data");
        $columns = $this->_get_relation_data($dsn, $columns);
        $this->logger->trace("columns",__FILE__,__LINE__,$columns);
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $this->request->get("sort_key"), "asc");
        $this->logger->debug("form_data",__FILE__,__LINE__,$form_data);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
            $this->template->assign("request",$request);
            $this->session->set("request", $request);
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where   = $datacenter_db->getWhere($columns, $request);
        $this->logger->debug("where",__FILE__,__LINE__,$where);

        // データ取得
        $rows = $datacenter_db->getRowsAssoc($where, array($form_data['sort_key'] => $form_data['sort_type']), $limit, $offset);
        $total_count = $datacenter_db->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager"        => $pager_info,
            "action"       => $action_name,
            "after_action" => $this->request->get("after_action"))
            );
        $user_id  = "";
        if ($request["user_key"]) {
            $user_key = $request["user_key"];
            $user_db  = new N2MY_DB($dsn, "user");
            $where    = "user_key = '".addslashes($user_key)."'";
            $this->logger->trace("where",__FILE__,__LINE__,$where);
            $user_rows = $user_db->getRowsAssoc($where);
            foreach ($user_rows as $user_row) {
                foreach ($user_row as $key => $colum) {
                    if ($key == "user_id") {
                        $user_id = $colum;
                        break;
                    }
                }
            }
        }
        $this->session->set("user_data", array("user_id"=> $user_id, "user_key" => $request["user_key"]));
        $this->session->set("form_data", $form_data);
        $this->template->assign("columns",$columns);
        $this->template->assign("rows",$rows);
        $this->template->assign("table",$table);
        $this->template->assign("user_id",$user_id);
        $this->session->remove("message");
        $template = "sastik_admin/".$table."/index.t.html";

        $this->_display($template);
    }

    /**
     * 優先DC設定画面
     */
    function action_datacenter_priority_sort($priority_data = "", $modify_user_key, $modify_add_row) {
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $this->logger->trace("request",__FILE__,__LINE__,$request);

        $required = $this->session->get("required");
        if ($required) {
            $message = $this->session->get("message");
            $this->session->remove("required");
            $this->session->remove("message");
            $this->template->assign("message",$message);
            $this->template->assign("required",$required);
        }
        $dsn = $this->get_dsn();

        require_once("classes/dbi/datacenter_priority.dbi.php");
        $datacenter_priority_db = new DatacenterPriorityTable($dsn);
        $columns = $datacenter_priority_db->getTableInfo("data");
        $columns = $this->_get_relation_data($dsn, $columns);
        $this->logger->trace("columns",__FILE__,__LINE__,$columns);
        // 検索条件設定・取得
        $where = $datacenter_priority_db->getWhere($columns, $request);
        $this->logger->debug("where",__FILE__,__LINE__,$where);

        // データ取得
        $sort_key  = "sort";
        $sort_type = "asc";
        $rows      = $datacenter_priority_db->getRowsAssoc($where, array($sort_key => $sort_type));
        foreach ($columns as $key => $column) {
            if ($column["item"]["relation_data"]) {
                $datacenterkeys = $column["item"]["relation_data"];
                $this->template->assign("datacenter_key_columns", $datacenterkeys);
            }
            if ($key == "country") {
                $this->template->assign("country_columns",$column["item"]["select"]);
            }
        }
        $user_key = $request["user_key"];
        $where    = "user_key = '".addslashes($user_key)."'";
        $this->logger->trace("where",__FILE__,__LINE__,$where);

        if (!empty($priority_data)) {
            $dc_priority_rows = $priority_data;
            $this->template->assign("user_key",$modify_user_key);
            $this->template->assign("add_row",$modify_add_row);
        } else {
            $this->template->assign("user_key",$user_key);
            $dc_priority_rows = $datacenter_priority_db->getRowsAssoc($where, array($sort_key => $sort_type));
            $this->session->set("before_data", $dc_priority_rows);
        }
        foreach ($dc_priority_rows as $key => $value) {
            $sort[] =  $value["sort"];
        }
        $this->template->assign("add_sort",max($sort) + 1);
        $this->template->assign("dc_priority_rows",$dc_priority_rows);
        $this->template->assign("columns",$columns);
        $this->template->assign("rows",$rows);
        $template = "sastik_admin/datacenter_priority/add_datacenter.t.html";

        $this->_display($template);
    }

     /**
     * DB作成
     */
    function action_db_create() {
        // DB作成
        $account_db = new N2MY_DB($this->account_dsn, "db_server");
        $server_key = $this->request->get("key");
        $this->logger2->info($server_key);
        $where = sprintf("server_key = %d", $server_key);
        $server_info = $account_db->getRow($where);
        $dsn_param = parse_url($server_info["dsn"]);
        $this->logger->info("dsn",__FILE__,__LINE__,$dsn_param);
        $db_name = substr($dsn_param["path"], 1);
        $this->logger->info("db_name",__FILE__,__LINE__,$db_name);
        $sql = "create database ".$db_name;
        $mysql_link = mysql_connect($dsn_param["host"].":".$dsn_param["port"], $dsn_param["user"], $dsn_param["pass"]);
        $result = mysql_query($sql);
        if (!$mysql_link || !$result) {
            $this->logger->error("connect error",__FILE__,__LINE__,mysql_error());
        } else {
            mysql_close($mysql_link);
            // データ生成
            $data_db = new N2MY_DB($server_info["dsn"]);
            $tables = $data_db->_conn->getListOf("tables");
            if (count($tables) == 0) {
                $db_file = N2MY_APP_DIR . "setup/meeting.sql";
                $create_db = file_get_contents($db_file);
                $querys = $data_db->parse_query($create_db);
                foreach ($querys as $key => $query) {
                    $ret = $data_db->_conn->query($query);
                    if (DB :: isError($ret)) {
                        $this->logger2->info($ret->getUserInfo());
                        die($query);
                    }
                }
            } else {
                print "すでに生成済み";
            }
            $account_db = new N2MY_DB($this->account_dsn, "db_server");
            $rows = $account_db->getRowsAssoc("server_status = 1");
            $str = '[SERVER_LIST]'."\n";
            foreach($rows as $row) {
                $str .= $row["host_name"]. ' = "'.$row["dsn"].'"'."\n";
            }
            $this->logger->info("server_list",__FILE__,__LINE__,$str);
            file_put_contents(N2MY_APP_DIR."config/server_list.ini", $str);
        }
        return $this->action_list();
    }

     /**
     * DB登録
     */
    function action_add() {
        $after_action = $this->request->get("after_action");
        $error_action = $this->request->get("error_action");
        if ($after_action) {
            $error_action = $error_action."&after_action=".$after_action;
        }
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            header("Location: index.php?".$error_action);
            exit;
        }
        $this->session->remove("message");
        $this->session->remove("required");
            $type = $this->session->get("type");
        if ($type == "account") {
            $dsn = $this->account_dsn;
        } else {
            $dsn = $this->get_dsn();
        }
        $table = $this->session->get("table");
        $this->logger->trace("add_data",__FILE__,__LINE__,$dsn."#".$type);
        $data_db = new N2MY_DB($dsn, $table);
        $temp_name = $this->request->get("temp");
        $form_data = $this->request->get("add_form");
        $regist = $this->session->get("regist");
        if ($regist) {
            $form_data[$regist] = date("Y-m-d H:i:s");
        }
        $this->session->set("temp", $form_data, $temp_name);
        $columns = $data_db->getTableInfo($type);
        $rules = $this->getRule($columns);
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        if ("user" == $table) {
            $form_data["user_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $this->create_id().rand(2,9));
            $form_data["user_admin_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $this->create_id().rand(2,9));
        } elseif ("api_auth" == $table) {
            $form_data['api_key'] = hash('sha256',uniqid());
            $form_data['secret'] = hash('sha256',uniqid());
            $form_data['create_datetime'] = date("Y-m-d H:i:s");
            $form_data['update_datetime'] = date("Y-m-d H:i:s");
        }
        if ($table == "notification") {
            $form_data["status"] = 0;
        }
        $err_obj = $data_db->check($form_data, $rules);
        if ($table == "notification") {
            $now_datetime = date("Y-m-d 23:59:59");
            $start_datetime = strtotime($form_data["start_datetime"]);
            $form_data["start_datetime"] = date("Y-m-d H:i:s",$start_datetime);
            if ($now_datetime > $form_data["start_datetime"]) {
                $message["start_datetime"] = "指定された開始日時が不正です。";
                $err_obj->set_error("start_datetime", "invalid");
            }
            $end_datetime = strtotime($form_data["end_datetime"]);
            $form_data["end_datetime"] = date("Y-m-d H:i:s",$end_datetime);
            if (($now_datetime > $form_data["end_datetime"]) || ($form_data["start_datetime"] > $form_data["end_datetime"])){
                $this->logger2->info(array($now_datetime,$form_data["start_datetime"],$form_data["end_datetime"]));
                $message["end_datetime"] = "指定された終了日時が不正です。";
                $err_obj->set_error("end_datetime", "invalid");
            }
        }
        $this->logger->trace("rules",__FILE__,__LINE__,$err_obj);
        $check = $this->session->get("check");
        $this->logger2->trace($_SESSION);
        $count = 0;
        $required = array();
        if (EZValidator::isError($err_obj)) {
            $this->logger2->trace($err_obj);
            $err_fields = $err_obj->error_fields();
            $this->logger->error("error",__FILE__,__LINE__,$err_fields);
            foreach ($err_fields as $value) {
                $type = $err_obj->get_error_type($value);
                if ($type == "required") {
                    $required[$value] = "1";
                }
            }
            if ($required || $message) {
                $message["required"] = "入力されていません。";
                $this->session->set("message",$message);
                $this->session->set("required",$required);
            }
            header("Location: index.php?".$error_action."#fragment2");
            exit;
        } else {
            $user_db = new N2MY_DB($this->account_dsn, "user");
            if ($check) {
                $where = $check."='".htmlspecialchars($form_data[$check])."'";
                $this->logger->trace("where",__FILE__,__LINE__,$where);
                $count = $data_db->numRows($where);
                //userの場合はアカウントテーブルもチェック
                if ($table == "user") {
                    $where = "user_id='".mysql_real_escape_string($form_data["user_id"])."'";
                    $count = $user_db->numRows($where);
                }
                if (is_numeric($count) && $count > 0) {
                    $message["check"][$check] = "入力された".$check."はすでに登録されています。";
                    $required["check"][$check] = "1";
                    $this->session->set("message",$message);
                    $this->session->set("required",$required);
                    header("Location: index.php?".$error_action."#fragment2");
                    exit;
                }
            }
            if($type == "account" && $table == "sastik_account_translation") {
            	//決め打ち
            	$check_items = array("account_host_key", "account_id");
            	$check = $check_items[0];
                $where = "account_host_key = '" . addslashes($form_data["account_host_key"]) . "'";
                $where .= " AND account_id = '" . addslashes($form_data["account_id"]) . "'";
                
                // 定義されたラベル名に変換
                $_check_arr = array();
                foreach($check_items as $ci) {
                    $_check_arr[] = $columns[$ci]["label"];
                }
                $check_label = '(' . implode(',', $_check_arr) . ')';
                
                $count = $data_db->numRows($where);
                if (is_numeric($count) && $count > 0) {
                    $message["check"][$check] = "入力された".$check_label."はすでに登録されています。";
                    $required["check"][$check] = "1";
                    $this->session->set("message",$message);
                    $this->session->set("required",$required);
                    header("Location: index.php?".$error_action."#fragment2");
                    exit;
                }
            }
            if ($table == "staff") {
                $form_data["login_password"] = sha1($form_data["login_password"]);
            }
            foreach ($form_data as $_key => $data) {
                if (!$data && $data != 0) {
                    $form_data[$_key] = $columns[$_key]["default"];
                    $this->logger->trace("not_addform_data",__FILE__,__LINE__,$columns[$_key]);
                }
            }
            if ($table == "user") {
                $message = $this->_user_validator($form_data, "add");
                if ($message) {
                    $required["user"] = "1";
                    $this->session->set("message",$message);
                    $this->session->set("required",$required);
                    header("Location: index.php?".$error_action."#fragment2");
                    exit;
                }
                if ($form_data["user_starttime"] == "0000-00-00 00:00:00") {
                    $form_data["user_starttime"] = date("Y-m-d H:i:s");
                }
                $form_data["user_registtime"] = date("Y-m-d H:i:s");
                //$form_data["meeting_version"] = $this->_getMeetingVersion();
                $agency = $form_data["agency"];
                unset($form_data["agency"]);
                $data_db->add($form_data);
                $user_data = array(
                    "user_id" => $form_data["user_id"],
                    "server_key" => $this->session->get("now_server_key"),
                    "user_id" => $form_data["user_id"],
                    "user_registtime" => date("Y-m-d H:i:s"),
                    );
                $user_db->add($user_data);
                $user_info = $data_db->getRow("user_id = '".mysql_real_escape_string($form_data["user_id"])."'");
                if (( "member" == $form_data["account_model"] ) || ("centre" == $form_data["account_model"])) {
                    $after_action = "action_individual";
                }
                $after_action = $after_action."&user_key=".$user_info["user_key"];
                $this->logger->info("user_key",__FILE__,__LINE__,$user_info);
                $where_agency = "user_id = '".htmlspecialchars($form_data["user_id"])."'";

                $agency_rel_db = new N2MY_DB($dsn, "agency_relation_user");
                if ($agency) {
                    $data = array("user_key" => $user_info["user_key"],
                                  "agency_id" => $agency);
                    $ret = $agency_rel_db->add($data);
                    if (DB::isError($ret)) {
                        $this->logger2->error($ret->getUserInfo());
                        return false;
                    }
                }
            } elseif($table == "member") {
                $agency = $form_data["agency"];
                unset($form_data["agency"]);
                $data_db->add($form_data);
                $member_info = $data_db->getRow("member_id = '".mysql_real_escape_string($form_data["member_id"])."'");
                $agency_rel_db = new N2MY_DB($dsn, "agency_relation_member");
                if ($agency) {
                    $data = array("member_key" => $member_info["member_key"],
                                  "agency_id" => $agency);
                    $ret = $agency_rel_db->add($data);
                    if (DB::isError($ret)) {
                        $this->logger2->error($ret->getUserInfo());
                        return false;
                    }
                }
            } elseif($table == "sastik_account_translation" || $table == "sastik_account_host") {
                $form_data["create_datetime"] = date("Y-m-d H:i:s");
                unset($form_data["user_registtime"]);
                $data_db->add($form_data);
            } else {
                $data_db->add($form_data);
            }
            $this->logger->trace("form_data",__FILE__,__LINE__,$form_data);
            //操作ログ登録
            $operation_data = array (
                "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => $table,
                "keyword"            => "add data",
                "info"               => serialize($form_data),
                "operation_datetime" => date("Y-m-d H:i:s"),
            );
            $this->add_operation_log($operation_data);

            $this->session->remove("temp", $temp_name);
            $this->session->remove("valid_required");
            $this->session->remove("check");
            $this->session->remove("regist");
            $data = array();
            if ($table == "user") {
                $data["user_company"] = $form_data["user_company"];
                $data["user_password"] = $form_data["user_password"];
                $data["user_admin_password"] = $form_data["user_admin_password"];
                $this->action_adduser_done($data);
            } else if ($table == "member") {
                $data["member_name"] = $form_data["member_name"];
                $data["member_pass"] = $form_data["member_pass"];
                $this->action_addmember_done($data);
            } else {
                header("Location: index.php?".$after_action);
            }

        }
    }

    function action_adduser_done($data) {
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $user_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $data["user_password"]);
        $user_admin_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $data["user_admin_password"]);

        $this->template->assign("user_company", $data["user_company"]);
        $this->template->assign("user_password", $user_password);
        $this->template->assign("user_admin_password", $user_admin_password);
        $template = "sastik_admin/user/add_user_done.t.html";
        $this->_display($template);
    }

     /**
     * 部屋追加画面
     */
    function action_add_room($message = "") {
        $this->set_submit_key($this->_name_space);
        $user_key = $this->request->get("user_key");
        if (!$user_key) {
            $user_key = $this->session->get("user_key");
        }
        if ($message) {
            $this->template->assign("message",$message);
            $user_key = $this->session->get("user_key");
        }
        $this->session->set("user_key",$user_key);
        $sort_key = $this->request->get("sort_key");
        if (!$sort_key) {
            $sort_key = "room_key";
        }
        $sort_type = $this->request->get("sort_type");
        if(!$sort_type) {
            $sort_type = "asc";
        }

        $data_db = new N2MY_DB($this->get_dsn(), "room");
        $action_name = __FUNCTION__;
        $columns = $data_db->getTableInfo("data");
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $sort_key, $sort_type);
        $this->logger->trace("form_data",__FILE__,__LINE__,$form_data);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where = "user_key='".$user_key."'";
        $this->logger->trace("columns",__FILE__,__LINE__,$columns);
        // データ取得
        $rows = $data_db->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $total_count = $data_db->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name)
            );
        $add_form = $this->session->get("temp", $action_name);
        $this->template->assign("add_form", array(
            "data" => $add_form,
            "ok_action" => $action_name,
            "ng_action" => $action_name
          ));
        $this->template->assign("columns",$columns);
        $this->logger->trace("data",__FILE__,__LINE__,$rows);
        $this->template->assign("rows",$rows);
        $this->session->remove("message");

        $plan_db = new N2MY_DB($this->account_dsn, "service");
        $where = "service_status='1'";
        $service_list = $plan_db->getRowsAssoc($where);
        $this->logger->debug("service_list",__FILE__,__LINE__,$service_list);
        $this->template->assign("user_key",$user_key);
        $this->template->assign("service_list",$service_list);
        $template = "sastik_admin/user/add_room.t.html";
        $this->_display($template);
    }

     /**
     * 部屋追加
     */
    function action_add_room_complete() {
        require_once "classes/dbi/room.dbi.php";
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_add_room();
            exit;
        }
        $form_data = $this->request->get("add_form");
        if (!$form_data["room_name"]) {
            $message .= "部屋名を入力してください。<br />";
        }
        if ( !$form_data["discount_rate"] ) {
            $form_data["discount_rate"] = 0;
        } else if (!is_numeric($form_data["discount_rate"])) {
            $message .= "割引率は数値で入力してください。<br />";
        }
        if ($form_data["room_plan_yearly"]) {
            if (!$form_data["room_plan_endtime"]) {
                $form_data["room_plan_endtime"] = "0000-00-00 00:00:00";
            }
            if (!$form_data["contract_month_number"]) {
                $message .= "契約期間（月数）を選択してください。<br />";
            }
        } else {
            $form_data["room_plan_endtime"] = "0000-00-00 00:00:00";
            $form_data["contract_month_number"] = "0";
        }
        if ( $message ) {
            $this->action_add_room($message);
            exit;
        }
        if (!$form_data["room_starttime"]) {
            $form_data["room_starttime"] = date("Y-m-d 00:00:00");
        }
        $user_key = $this->request->get("user_key");
        $where = "user_key='".addslashes($user_key)."'";
        $user_db = new N2MY_DB($this->get_dsn(), "user");
        $user_data = $user_db->getRow($where);
        $room_db = new RoomTable($this->get_dsn());
        $count = $room_db->numRows($where);
        $room_key = $user_data["user_id"]."-".($count + 1)."-".substr(md5(uniqid(time(), true)), 0,4);
        $room_sort = $count + 1;
        //部屋登録
        $room_data = array (
            "room_key"            => $room_key,
            "room_name"           => $form_data["room_name"],
            "user_key"            => $user_key,
            "room_sort"           => $room_sort,
            "room_status"         => "1",
            "use_teleconf"        => "1",
            "use_pgi_dialin"      => "1",
            "use_pgi_dialin_free" => "1",
            "use_pgi_dialout"     => "1",
            "room_registtime"     => date("Y-m-d H:i:s"),
            );
        $add_room = $room_db->add($room_data);
        if (DB::isError($add_room)) {
            $this->logger2->info( $add_room->getUserInfo());
        }
        $acc_user_db = new N2MY_DB($this->account_dsn, "user");
        $where = "user_id='".addslashes($user_data["user_id"])."'";
        $acc_user_data = $acc_user_db->getRow($where);
        $relation_db = new N2MY_DB($this->account_dsn, "relation");
        $relation_data = array(
            "relation_key" => $room_key,
            "relation_type" => "mfp",
            "user_key" => $acc_user_data["user_id"],
            "status" => "1",
            "create_datetime" => date("Y-m-d H:i:s"),
            );
        $relation_db->add($relation_data);
        $this->action_room_plan_add($room_key, $form_data);

        //メンバーが登録されているか確認
        $member_db = new N2MY_DB($this->get_dsn(), "member");
        $member_ids = $member_db->getRowsAssoc("user_key='".$user_key."'", null, null, null, "member_key, member_id");
        $this->logger2->info($member_ids);
        $wsdl = $this->config->get('VCUBEID','wsdl');
        if ($member_ids && $wsdl) {
        }

        //操作ログ登録
        $operation_data = array (
                "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => "room",
                "keyword"            => $room_key,
                "info"               => serialize($room_data),
                "operation_datetime" => date("Y-m-d H:i:s"),
            );
            $this->add_operation_log($operation_data);
        header("Location: index.php?action_add_room");
    }

     /**
     * 部屋詳細表示
     */
    function action_room_detail($message = "", $option_message = "", $option_expire_date_time = "") {
        $this->set_submit_key($this->_name_space);
        $room_key         = $this->request->get("room_key");
        $this->session->set("table", "room");
        $sort_key_plan    = $this->request->get("sort_key_plan");
        $sort_type_plan   = $this->request->get("sort_type_plan");
        $sort_key_option  = $this->request->get("sort_key_option");
        $sort_type_option = $this->request->get("sort_type_option");

        $sort_key_plan    = isset($sort_key_plan)    ? $sort_key_plan    : "room_plan_key";
        $sort_type_plan   = isset($sort_type_plan)   ? $sort_type_plan   : "desc";
        $sort_key_option  = isset($sort_key_option)  ? $sort_key_option  : "ordered_service_option_key";
        $sort_type_option = isset($sort_type_option) ? $sort_type_option : "desc";

        //部屋情報取得
        $room_db        = new N2MY_DB($this->get_dsn(), "room");
        $where          = "room_key='".$room_key."'";
        $room_detail    = $room_db->getRow($where);
        $room_columns   = $room_db->getTableInfo("data");
        $page["action"] = __FUNCTION__;

        $this->template->assign("room_columns",$room_columns);
        $this->template->assign("room_detail",$room_detail);

        //ユーザー情報取得（アカウントモデル取得の為）
        $user_db   = new N2MY_DB($this->get_dsn(), "user");
        $user_info = $user_db->getRow("user_key='".$room_detail["user_key"]."'");

        $this->template->assign("account_model",$user_info["account_model"]);

        $this->logger->debug("user_info",__FILE__,__LINE__,$user_info);

        //部屋プラン取得
        $service_db      = new N2MY_DB($this->account_dsn, "service");
        $service         = $service_db->getRowsAssoc("service_status = '1'");
        $service_columns = $service_db->getTableInfo("data");

        $this->template->assign(array("service_columns" => $service_columns,
                                      "service"         => $service));

        $room_plan_db     = new N2MY_DB($this->get_dsn(), "room_plan");
        $where            = "room_key='".$room_key."'";
        $room_plan_detail = $room_plan_db->getRowsAssoc($where, array($sort_key_plan => $sort_type_plan));
        $this->logger->trace("plan",__FILE__,__LINE__,$room_plan_detail);
        $room_plan_columns = $room_plan_db->getTableInfo("data");
        $room_plan_columns = $this->_get_relation_data($this->account_dsn, $room_plan_columns);
        $this->logger->trace("new_columuns",__FILE__,__LINE__,$room_plan_columns);

        $this->template->assign(array("room_plan_columns" => $room_plan_columns,
                                      "room_plan_detail"  => $room_plan_detail));
        //現在有効なプランを取得
        $where_now    = "room_key='".$room_key."'".
                        " AND room_plan_status = 1";
        $now_plan_key = $room_plan_db->getRow($where_now);
        $this->logger->debug("now_plan",__FILE__,__LINE__,$now_plan_key);

        //現在有効なプランが無い場合は開始待ちのプランを取得
        if (!$now_plan_key) {
            $where_now = "room_key='".$room_key."'".
                         " AND room_plan_status = 2";
            $now_plan_key = $room_plan_db->getRow($where_now);
        }

        if ($now_plan_key["service_key"]) {
            $where_service_key = "service_key = ".$now_plan_key["service_key"];
            $now_plan          = $service_db->getRow($where_service_key);
            $this->template->assign(array("now_plan"     => $now_plan["service_name"],
                                          "now_plan_key" => $now_plan["service_key"]));
        }

        // オプション情報取得
        $service_option_db = new N2MY_DB($this->account_dsn, "service_option");
        $service_option    = $service_option_db->getRowsAssoc();

        $option_db         = new N2MY_DB($this->get_dsn(), "ordered_service_option");
        $option_columns    = $this->_get_relation_data($this->account_dsn, $option_db->getTableInfo("data"));
        $option_data       = $option_db->getRowsAssoc($where, array($sort_key_option => $sort_type_option));

        $this->logger->debug("option_data",__FILE__,__LINE__,$option_data);

        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
           require_once("classes/dbi/pgi_setting.dbi.php");
           require_once('classes/pgi/PGiSystem.class.php');
           require_once("classes/pgi/PGiRate.class.php");
           $pgi_setting_table = new PGiSettingTable($this->get_dsn());
           $pgi_settings      = $pgi_setting_table->findByRoomKey($room_key);
           $this->logger2->info($pgi_settings);
           $pgi_systems       = PGiSystem::findConfigAll();
           $pgi_rate_config   = PGiRate::findConfigAll();
           $pgi_gm_rate_config   = PGiRate::findGmConfigAll();
           $default_system_key= $this->config->get("PGI", 'system_key');
           if (0 < count($pgi_settings)) {
                $pgi_setting_keys = array();
                foreach ($pgi_settings as $setting) {
                    $pgi_setting_keys[] = $setting['pgi_setting_key'];
                }
               $pgi_rates = $this->getPGiRateBySettingKey($pgi_setting_keys);
           }
        }
        foreach ($option_data as $data) {
            if ($data["ordered_service_option_status"] == 0) {
                continue;
            }
            foreach ($option_columns as $key => $colum) {
                if (isset($data[$key])) {
                    $option_flg = true;
                }
            }
        }
        if (empty($option_expire_date_time)) {
            $option_expire_date_time = date("Y/m/d 23:59:59");
        }
        $this->template->assign(array("room_key"           => $room_key,
                                      "now_ym"             => date('Y-m').'-01',
                                      "page"               => $page,
                                      "service_option"     => $service_option,
                                      "option_columns"     => $option_columns,
                                      "message"            => $message,
                                      "option_message"     => $option_message,
                                      "option_data"        => $option_data,
                                      "option_flg"         => $option_flg,
                                      "pgi_systems"        => @$pgi_systems,  // if enabled pgi
                                      "json_pgi_systems"   => @json_encode($pgi_systems),  // if enabled pgi
                                      "pgi_settings"       => @$pgi_settings, // if enabled pgi
                                      "pgi_rate_config"    => @$pgi_rate_config,
                                      "pgi_gm_rate_config" => @$pgi_gm_rate_config,
                                      "pgi_rates"          => @$pgi_rates,
                                      "default_system_key" => @$default_system_key,
                                      "option_date_default"=> $option_expire_date_time,
                                  ));
        $this->_display("sastik_admin/room/detail.t.html");
    }

    function action_delete_pgi_setting()
    {
        $pgi_setting_key = $this->request->get("pgi_setting_key");
        $room_key        = $this->request->get("room_key");

        if (!$pgi_setting_key || !$room_key) {
            $this->exitWithError('room_key or pgi_setting_key not found');
        }

        require_once("classes/dbi/pgi_setting.dbi.php");
        $pgi_setting_table = new PGiSettingTable($this->get_dsn());
        $pgi_setting       = $pgi_setting_table->findByKey($pgi_setting_key);
        if (!$pgi_setting) {
            $this->exitWithError('pgi_setting not found');
        }

        // 当月の設定は消せない
//        if ($pgi_setting['startdate'] == date('Y-m').'-01') {
//            $this->exitWithError( "disable delete setting pgi_setting:$pgi_setting_key");
//        }
        if ($pgi_setting['room_key'] != $room_key) {
            $this->exitWithError( 'room_key not match pgi_setting[room_key]');
        }

        $data  = array('is_deleted' => 1);
        $where = sprintf("room_key='%s' AND pgi_setting_key='%s'", $room_key, $pgi_setting_key);
        $pgi_setting_table->update($data, $where);
        if ($pgi_setting['room_key'] != $room_key) {
            $this->exitWithError('room_key not match pgi_setting[room_key]');
        }

        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "pgi_setting",
            "keyword"            => $room_key,
            "info"               => serialize($_REQUEST),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);

        $this->action_room_detail($message = "");
    }
     /**
     * メンバー追加画面
     */
    function action_add_member($message = "") {
        $this->set_submit_key($this->_name_space);
        $user_key = $this->request->get("user_key");
        if (!$user_key) {
            $user_key = $this->session->get("user_key");
        }
        if ($message) {
            $this->template->assign("add_member_form", $this->session->get("add_member_form"));
            $this->template->assign("message",$message);
            $user_key = $this->session->get("user_key");
        }
        $this->session->set("user_key",$user_key);
        $sort_key = $this->request->get("sort_key");
        if (!$sort_key) {
            $sort_key = "member_key";
        }
        $sort_type = $this->request->get("sort_type");
        if(!$sort_type) {
            $sort_type = "asc";
        }
        //ユーザー情報
        $objUser = new N2MY_DB( $this->get_dsn(), "user" );
        $user_info = $objUser->getRow("user_key='".$user_key."'");

        //部屋情報取得
        $room_db = new N2MY_DB($this->get_dsn(), "room");
        $room_list = $room_db->getRowsAssoc("user_key='".$user_key."'", null, null, null, "room_key, room_name");

        //グループ取得
        $group_db = new N2MY_DB($this->get_dsn(), "member_group");
        $group_list = $group_db->getRowsAssoc( sprintf( "user_key='%s'", $user_key ) );

        //代理店取得
        $agency_db = new N2MY_DB($this->account_dsn, "agency");
        $agency_list = $agency_db->getRowsAssoc( sprintf( "is_deleted='%s'", "0" ) );

        //タイムゾーン
        $timezoneList = $this->get_timezone_list();

        $data_db = new N2MY_DB($this->get_dsn(), "member");
        $action_name = __FUNCTION__;
        $columns = $data_db->getTableInfo("data");
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $sort_key, $sort_type);
        $this->logger->trace("form_data",__FILE__,__LINE__,$form_data);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where = "user_key='".$user_key."'";
        $this->logger->trace("columns",__FILE__,__LINE__,$columns);
        // データ取得
        $rows = $data_db->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $total_count = $data_db->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name)
            );
        $add_form = $this->session->get("temp", $action_name);
        $this->template->assign("add_form", array(
            "data" => $add_form,
            "ok_action" => $action_name,
            "ng_action" => $action_name
          ));
        $this->template->assign("columns",$columns);
        $this->logger->trace("data",__FILE__,__LINE__,$rows);
        $this->template->assign("rows",$rows);
        $this->session->remove("message");

        $plan_db = new N2MY_DB($this->account_dsn, "service");
        $where = "service_status='1'";
        $service_list = $plan_db->getRowsAssoc($where);
        $this->logger->debug("service_list",__FILE__,__LINE__,$service_list);
        $this->template->assign("user_key",$user_key);
        $this->template->assign("user_info",$user_info);
        $this->template->assign("room_list",$room_list);
        $this->template->assign("agency_list",$agency_list);
        $template = "sastik_admin/user/add_member.t.html";
        $this->_display($template);
    }

     /**
     * メンバー追加
     */
    function action_add_member_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_add_member();
            exit;
        }
        $form_data = $this->request->get("add_form");
        $this->session->set("add_member_form", $form_data);
        //ユーザー情報
        $user_db = new N2MY_DB( $this->get_dsn(), "user" );
        $user_info = $user_db->getRow("user_key='".$form_data["user_key"]."'");

        $member_db = new N2MY_DB($this->get_dsn(), "member");
        $member_list = $member_db->getRowsAssoc( sprintf( "user_key ='%s' and member_status > -1", $form_data['user_key'] ) );

        $message = null;
        if(!$form_data['member_name']){
            $message .= '<li>'.MEMBER_ERROR_NAME . '</li>';
        } elseif (mb_strlen($form_data['member_name']) > 30) {
             $message .= '<li>'.MEMBER_ERROR_NAME_LENGTH . '</li>';
        }
        if (mb_strlen($form_data['member_name_kana']) > 30) {
             $message .= '<li>'.MEMBER_ERROR_NAME_KANA_LENGTH . '</li>';
        }
        $this->logger2->info($form_data['member_id']);
        if (!preg_match('/^[[:alnum:]@+._-]{3,255}$/', $form_data['member_id'])) {
            $this->logger2->info($form_data['member_id']);
            $message .= '<li>'.USER_ERROR_ID_LENGTH . '</li>';
        } elseif($this->_isExistMember($form_data['member_id'])){
            $message .= '<li>'.MEMBER_ERROR_ID_EXIST . '</li>';
        }
        // メンバー課金の場合のみ必須
        if($user_info["account_model"] == "member" && !$form_data['member_email']){
            $message .= '<li>'.MEMBER_ERROR_EMAIL . '</li>';
        }
        if ($form_data['member_email'] != "") {
            if(!EZValidator::valid_email($form_data['member_email'])){
                $message .= '<li>'.MEMBER_ERROR_EMAIL_INVALID . '</li>';
            }
        }
        if (!$form_data['member_pass'] && !$form_data['member_pass_check']) {
            $message .= '<li>'.MEMBER_ERROR_PASS . '</li>';
        //ユーザーパスワードをチェック
        } elseif (!preg_match('/^[!-\[\]-~@._=`]{8,128}$/', $form_data['member_pass'])) {
            $message .= '<li>'.USER_ERROR_PASS_INVALID_01. '</li>';
        } elseif (!preg_match('/[[:alpha:]]+/',$form_data['member_pass']) || preg_match('/^[[:alpha:]]+$/',$form_data['member_pass'])) {
            $message .= '<li>'.USER_ERROR_PASS_INVALID_02. '</li>';
        }
        if( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") &&
            $user_info["max_member_count"] <= count( $member_list ) ) {
                $message .= '<li>'.MEMBER_ERROR_OVER_COUNT . '</li>';
        }
        // Terminalの際は部屋キー必須
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"] );
        if (!$form_data['member_room_keys'] && $form_data["member_type"] == "terminal") {
            $message .= '<li>'.MEMBER_ERROR_ROOM_DENY . '</li>';
        }
        if ($form_data['member_room_keys']) {
            foreach ($form_data['member_room_keys'] as $member_room_key) {
                if (($form_data['member_type'] == "terminal") &&
                    !array_key_exists($member_room_key, $rooms)) {
                        $message .= '<li>'.MEMBER_ERROR_ROOM_DENY . '</li>';
                }
            }
        }
        // タイプチェック
        if ($form_data["member_type"] && $user_info["account_model"] == "member") {
            $message .= '<li>タイプが正しく有りません。</li>';
        }

        if( $message ){
            $this->action_add_member($message);
            exit;
        }
        if (!$form_data["room_starttime"]) {
            $form_data["room_starttime"] = date("Y-m-d 00:00:00");
        }
        $room_db = new N2MY_DB($this->get_dsn(), "room");

        //部屋情報登録
        if( $user_info["account_model"] == "member" || ($user_info["account_model"] == "centre" && $form_data["member_type"] == "centre") ){
            require_once( "classes/dbi/user_plan.dbi.php" );
            require_once( "classes/dbi/room_plan.dbi.php" );
            $objUserPlan = new UserPlanTable( $this->get_dsn() );
            $objRoomPlanTable = new RoomPlanTable( $this->get_dsn() );
            if ($now_plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1")) {
                $service_db = new N2MY_DB( $this->account_dsn, "service" );
                $service_info = $service_db->getRow("service_key = '".addslashes($now_plan_info['service_key'])."' AND service_status = 1");
                $this->logger2->info(array($now_plan_info, $service_info));
            }
            $where = sprintf( "user_key=%d", $user_info["user_key"] );
            $count = $room_db->numRows($where);
            $room_key = $user_info["user_id"]."-".($count + 1)."-".substr( md5( uniqid( time(), true ) ), 0, 4 );
            $room_sort = $count + 1;
            //部屋登録
            $room_data = array (
                "room_key"              => $room_key,
                "max_seat"              => ($service_info["max_seat"]) ? $service_info["max_seat"] : 10,
                "max_audience_seat"     => ($service_info["max_audience_seat"]) ? $service_info["max_audience_seat"] : 0,
                "max_whiteboard_seat"   => ($service_info["max_whiteboard_seat"]) ? $service_info["max_whiteboard_seat"] : 0,
                "user_key"              => $user_info["user_key"],
                "room_name"             => $form_data['member_name'],
                "room_sort"             => $room_sort,
                "room_status"           => "1",
                "room_registtime"       => date("Y-m-d H:i:s"),
                );
            if ($service_info["service_name"] == "VCUBE_Doctor_Standard") {
                $room_data["invited_limit_time"] = $service_info["invited_limit_time"] ? $service_info["invited_limit_time"] : 30;
            }
            $this->logger2->info($room_data);
            $add_room = $room_db->add($room_data);
            if (DB::isError($add_room)) {
                $this->logger2->info( $add_room->getUserInfo());
            }
            $relation_db = new N2MY_DB($this->account_dsn, "relation");
            $relation_data = array(
                "relation_key" => $room_key,
                "relation_type" => "mfp",
                "user_key" => $user_info["user_id"],
                "status" => "1",
                "create_datetime" => date("Y-m-d H:i:s"),
                );
            $relation_db->add($relation_data);
            /* userのプランからroom_planへコピー*/
            $planList = $objUserPlan->getRowsAssoc(sprintf("user_key='%s'", $user_info["user_key"]));
            for( $i = 0; $i < count( $planList ); $i++ ){
                $data = array(
                            "room_key"=>$room_key,
                            "service_key"=>$planList[$i]["service_key"],
                            "discount_rate"=>$planList[$i]["discount_rate"],
                            "contract_month_number"=>$planList[$i]["contract_month_number"],
                            "room_plan_yearly"=>$planList[$i]["user_plan_yearly"],
                            "room_plan_yearly_starttime"=>$planList[$i]["user_plan_yearly_starttime"],
                            "room_plan_starttime"=>$planList[$i]["user_plan_starttime"],
                            "room_plan_endtime"=>$planList[$i]["user_plan_endtime"],
                            "room_plan_status"=>$planList[$i]["user_plan_status"],
                            "room_plan_registtime"=>$planList[$i]["user_plan_registtime"],
                            "room_plan_updatetime"=>$planList[$i]["user_plan_updatetime"]
                             );
                $addPlan = $objRoomPlanTable->add( $data );
                if (DB::isError( $addPlan ) ) {
                    $this->logger2->info( $addPlan->getUserInfo() );
                }
            }

            // userのプランオプション(user_service_option)からmemberの部屋のオプションを追加する(ordered_service_option)
            require_once("classes/dbi/ordered_service_option.dbi.php");
            $ordered_service_option = new OrderedServiceOptionTable( $this->get_dsn() );

            $user_service_option_db = new N2MY_DB( $this->get_dsn(), "user_service_option" );
//            $condition = sprintf( "user_key='%s' AND user_service_option_starttime<='%s' AND user_service_option_status=1", $user_info["user_key"], date( "Y-m-d" ) );
            $condition = sprintf( "user_key='%s' AND service_option_key !=14", $user_info["user_key"] );
            $optionList = $user_service_option_db->getRowsAssoc( $condition );

            for( $i = 0; $i < count( $optionList ); $i++ ){
                $data = array(
                    "room_key" => $room_key,
                    "user_service_option_key" => $optionList[$i]["user_service_option_key"],
                    "service_option_key" => $optionList[$i]["service_option_key"],
                    "ordered_service_option_status" => $optionList[$i]["user_service_option_status"],
                    "ordered_service_option_starttime" => $optionList[$i]["user_service_option_starttime"],
                    "ordered_service_option_registtime" => $optionList[$i]["user_service_option_registtime"],
                    "ordered_service_option_deletetime" => $optionList[$i]["user_service_option_deletetime"]
                );
                $ordered_service_option->add( $data );
            }
        } else {
            $room_key = $form_data['member_room_keys'][0];
        }

        //member追加
        $user_key = $user_info['user_key'];
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $temp = array(
            "user_key"         => $user_info['user_key'],
            "member_id"        => $form_data['member_id'],
            "member_pass"      => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $form_data['member_pass']),
            "member_email"     => $form_data['member_email'],
            "member_name"      => $form_data['member_name'],
            "member_name_kana" => $form_data['member_name_kana'],
            "member_status"    => (($form_data['member_status_key']) ? $form_data['member_status_key'] : 0),
            "member_type"      => (($form_data['member_type']) ? $form_data['member_type'] : ""),
            "member_group"     => (($form_data['member_group_key']) ? $form_data['member_group_key'] : 0),
            "room_key"         => $room_key,
            "timezone"         => $form_data['timezone'],
            "lang"             => $form_data['lang'],
            "create_datetime"  => date("Y-m-d H:i:s"),
            "update_datetime"  => date("Y-m-d H:i:s"),
        );
        $ret = $member_db->add( $temp );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$temp);
            $wsdl = $this->config->get('VCUBEID','wsdl');
            if ($wsdl) {
            }
        }
        $member_info = $member_db->getRow("member_id = '".addslashes($form_data['member_id'])."'");
        $this->logger2->debug($form_data);
        //部屋のリレーション追加
        $mrRation_db = new N2MY_DB($this->get_dsn(), "member_room_relation");
        if ($room_key && ($user_info["account_model"] == "member" || $form_data["member_type"] == "centre" || $form_data["member_type"] == "free")) {
            $mrr = $mrRation_db->getRow(sprintf("member_key = '%s' AND room_key = '%s'", $member_info["member_key"], $room_key));
            if(!$mrr){
              $relationData = array();
              $relationData["member_key"]      = $member_info["member_key"];
              $relationData["room_key"]        = $room_key;
              $relationData["create_datetime"] = date("Y-m-d H:i:s");
              $res = $mrRation_db->add($relationData);
              if (PEAR::isError($res)) {
                  $this->logger2->info($res->getMessage());
                  $this->logger2->error(__FILE__.__LINE__.":".__FUNCTION__."#DB ERROR!");
                  throw new Exception("database error");
              }

            }
        } else if ($form_data["member_type"] == "terminal" && $form_data['member_room_keys']) {
            foreach ($form_data['member_room_keys'] as $room_key) {
                $data = array("member_key" => $member_info["member_key"],
                              "room_key"   => $room_key,
                              "create_datetime" => date("Y-m-d H:i:s"));
                $this->logger2->debug($data);
                $mrRation_db->add($data);
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                }
            }
        }

        $agency = $form_data["agency"];
        $agency_rel_db = new N2MY_DB($this->get_dsn(), "agency_relation_member");
        if ($agency) {
            $data = array("member_key" => $member_info["member_key"],
                          "agency_id" => $agency);
            $ret = $agency_rel_db->add($data);
            if (DB::isError($ret)) {
                $this->logger2->error($ret->getUserInfo());
                return false;
            }
        }

        //認証サーバーへ追加
        $obj_MgmUserTable = new N2MY_DB( $this->account_dsn, "user" );
        $auth_data = array(
            "user_id"     => $form_data['member_id'],
            "server_key"    => $this->session->get("now_server_key")
            );
        $result = $obj_MgmUserTable->add( $auth_data );
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$auth_data);
        }

        $this->logger->info(__FUNCTION__."#member_add",__FILE__,__LINE__,$temp);
        $this->session->remove('add_member_info');
        //操作ログ登録
        $operation_data = array (
                "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => "member",
                "keyword"            => $form_data["member_id"],
                "info"               => serialize($form_data),
                "operation_datetime" => date("Y-m-d H:i:s"),
            );
            $this->add_operation_log($operation_data);
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $member_pass = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_info["member_pass"]);
        $this->template->assign("member_name", $member_info["member_name"]);
        $this->template->assign("member_pass", $member_pass);
        $template = "sastik_admin/member/add_member_done.t.html";
        $this->_display($template);
    }

    /**
     * id からそのユーザーが存在するかチェック
     */
    private function _isExistMember($id){
        //member, user, 認証DBのuser のテーブルをチェック
        $user_db = new N2MY_DB($this->get_dsn(), "user");
        $member_db = new N2MY_DB($this->get_dsn(), "member");
        $id = mysql_real_escape_string($id);
        if ( $user_db->numRows( sprintf("user_id='%s'", $id ) ) > 0 ||
             $member_db->numRows( sprintf("member_id='%s'", $id ) )  > 0 ||
             $user_db->numRows( sprintf("user_id='%s'", $id ) )  > 0 ){
            return true;
        } else {
            return false;
        }
    }

   /**
    * 会議詳細
    */
    function action_meeting_detail() {
        $meeting_key = $this->request->get("meeting_key");
        if ($meeting_key) {
            // サーバ一覧
            $server = new N2MY_DB($this->account_dsn, "fms_server");
            $_server_info = $server->getRowsAssoc("", null ,null, null, null, null);
            foreach($_server_info as $_key => $val) {
                $server_info[$val["server_key"]] = $val;
            }
            $fms_app_name = $this->config->get("CORE", "app_name", "Vrms");
            $form = array(
                "action" => "action_meeting"
                );
            $fms = array(
                "server_info" => $server_info,
                "app_name" => $fms_app_name,
                );
            // 会議詳細
            $meeting_obj = new N2MY_DB($this->get_dsn(), "meeting");
            $meeting_columns = $meeting_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $meeting_detail = $meeting_obj->getRow($where);
            $this->logger->info("meeting_detail",__FILE__,__LINE__,$meeting_detail);
            // オプション情報
            $options_db = new N2MY_DB($this->account_dsn, "options");
            $option_list = $options_db->getRowsAssoc("", null, null, 0, "*", "option_key");
            // 会議オプション
            $meeting_option_obj = new N2MY_DB($this->get_dsn(), "meeting_options");
            $meeting_option_columns = $meeting_option_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $meeting_option_list = $meeting_option_obj->getRowsAssoc($where);
            foreach($meeting_option_list as $key => $row) {
                $row["option_name"] = $option_list[$row["option_key"]]["option_name"];
                $meeting_option_list[$key] = $row;
            }
            // データセンタ切り替え一覧
            $server_change_obj = new N2MY_DB($this->get_dsn(), "meeting_sequence");
            $server_change_columns = $server_change_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $server_change_list = $server_change_obj->getRowsAssoc($where);
            // ミーティング利用時間チェック
            $meeting_uptime_obj = new N2MY_DB($this->get_dsn(), "meeting_uptime");
            $meeting_uptime_columns = $meeting_uptime_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $meeting_uptime_list = $meeting_uptime_obj->getRowsAssoc($where);
            // 予約有り
            if ($meeting_detail["is_reserved"] == 1) {
                // 予約詳細
                require_once("classes/dbi/reservation.dbi.php");
                $reservation_obj = new ReservationTable($this->get_dsn());
                $reservation_columns = $reservation_obj->getTableInfo("data");
                $where = "meeting_key = '".addslashes($meeting_detail["meeting_ticket"])."'";
                $reservation_detail = $reservation_obj->getRow($where);
                $this->template->assign("reservation_detail", $reservation_detail);
                $this->template->assign("reservation_columns", $reservation_columns);
                // 招待者一覧
                require_once("classes/dbi/reservation_user.dbi.php");
                $invite_obj = new ReservationUserTable($this->get_dsn());
                $invite_columns = $invite_obj->getTableInfo("data");
                $where = "reservation_key = ".$reservation_detail["reservation_key"];
                $invite_list = $invite_obj->getRowsAssoc($where);
                $this->template->assign("invite_list", $invite_list);
                $this->template->assign("invite_columns", $invite_columns);
            }
            // 参加者一覧
            $participant_obj = new N2MY_DB($this->get_dsn(), "participant");
            $participant_columns = $participant_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $participant_list = $participant_obj->getRowsAssoc($where);
            // 各種利用時間
            $use_time_obj = new N2MY_DB($this->get_dsn(), "meeting_use_log");
            $query = "SELECT type, count(DISTINCT create_datetime) as use_time_count FROM".
                " meeting_use_log".
                " WHERE meeting_key = '".$meeting_key."'" .
                " GROUP BY type";
            $rs = $use_time_obj->_conn->query($query);
            if (DB::isError($rs)) {
                $this->logger->error("db error",__FILE__,__LINE__,$rs->getUserInfo());
            }
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                $this->logger->info("row",__FILE__,__LINE__,$row);
                $use_time[$row["type"]] = $row["use_time_count"];
            }
            $this->logger->info("use_time",__FILE__,__LINE__,$use_time);
            // 資料一覧
            $document_obj = new N2MY_DB($this->get_dsn(), "document");
            $document_columns = $document_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $document_list = $document_obj->getRowsAssoc($where);
            $this->template->assign("fms", $fms);
            $this->template->assign("meeting_detail", $meeting_detail);
            $this->template->assign("meeting_columns", $meeting_columns);
            $this->template->assign("option_list", $option_list);
            $this->template->assign("meeting_option_list", $meeting_option_list);
            $this->template->assign("meeting_option_columns", $meeting_option_columns);
            $this->template->assign("server_change_list", $server_change_list);
            $this->template->assign("server_change_columns", $server_change_columns);
            $this->template->assign("meeting_uptime_list", $meeting_uptime_list);
            $this->template->assign("meeting_uptime_columns", $meeting_uptime_columns);
            $this->template->assign("participant_list", $participant_list);
            $this->template->assign("participant_columns", $participant_columns);
            $this->template->assign("use_time", $use_time);
            $this->template->assign("document_columns", $document_columns);
            $this->template->assign("document_list", $document_list);
        }
        $this->_display("sastik_admin/meeting/detail.t.html");
    }

     /**
     * ルームプラン登録（プランに合わせて人数、オプションも登録）
     */
    function action_room_plan_add($room_key, $data) {
        $service_key = $data["service_key"];
        $start_time = $data["room_starttime"];
        $end_time = $data["room_plan_endtime"];
        $discount_rate = $data["discount_rate"];
        $contract_month_number = $data["contract_month_number"];
        $today = date("Y-m-d 23:59:59");
        if (strtotime($start_time) <= strtotime($today)) {
            //プラン登録
            $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
            $room_plan = array (
                "room_key" => $room_key,
                "service_key" => $service_key,
                "room_plan_status" => "1",
                "room_plan_starttime" => $start_time,
                "room_plan_endtime" => $end_time,
                "discount_rate" => $discount_rate,
                "contract_month_number" => $contract_month_number,
                "room_plan_registtime" => date("Y-m-d H:i:s"),
                );
            $add_plan = $room_plan_db->add($room_plan);
            if (DB::isError($add_plan)) {
                $this->logger2->info( $add_plan->getUserInfo());
            }
            $service_db = new N2MY_DB($this->account_dsn, "service");
            $service_info = $service_db->getRow("service_key = ".$service_key);
            $this->logger2->info($service_key);

            //シート登録
            $room_db = new N2MY_DB($this->get_dsn(), "room");
            //オーディエンスオプション数で人数変更
            require_once("classes/dbi/ordered_service_option.dbi.php");
            $ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
            if ($service_info["max_audience_seat"] == "0") {
                $where = "room_key = '".addslashes($room_key)."'".
                         " AND service_option_key = 7".
                         " AND ordered_service_option_status = 1";
                $audience_count = $ordered_service_option->numRows($where);
                $this->logger->trace("audience",__FILE__,__LINE__,$audience_count);
                if ($audience_count > 0 ) {
                    $max_audience = $audience_count."0";
                } else {
                    $max_audience = $service_info["max_audience_seat"];
                }
            } else {
                $max_audience = $service_info["max_audience_seat"];
            }
            $max_whiteboard = $service_info["max_whiteboard_seat"];
            $where = "room_key = '".addslashes($room_key)."'";
            $room_seat = array (
                "max_seat" => $service_info["max_seat"],
                "max_audience_seat" => $max_audience,
                "max_whiteboard_seat" => $max_whiteboard,
                "meeting_limit_time" => $service_info["meeting_limit_time"],
                "default_camera_size" => $service_info["default_camera_size"],
                "hd_flg"              => $service_info["hd_flg"],
                "room_updatetime" => date("Y-m-d H:i:s"),
                );
            if ($service_info["max_room_bandwidth"] != 0) {
                $room_seat["max_room_bandwidth"] = $service_info["max_room_bandwidth"];
            }
            if ($service_info["max_user_bandwidth"] != 0) {
                $room_seat["max_user_bandwidth"] = $service_info["max_user_bandwidth"];
            }
            if ($service_info["min_user_bandwidth"] != 0) {
                $room_seat["min_user_bandwidth"] = $service_info["min_user_bandwidth"];
            }
            if ($service_info["service_name"] == "Paperless_Free" || $service_info["service_name"] == "Meeting_Free" || $service_info["service_name"] == "PGi_Paperless_Free") {
                $room_seat["whiteboard_page"] = "10";
                $room_seat["whiteboard_size"] = "2";
                $whiteboard_filetype = array(
                    "document" => array("ppt","pdf"),
                    "image"    => array("jpg"),
                );
                $room_seat["whiteboard_filetype"] =serialize($whiteboard_filetype);
            } else {
                $room_seat["whiteboard_page"] = $service_info["whiteboard_page"] ? $service_info["whiteboard_page"] : 100;
                $room_seat["whiteboard_size"] = $service_info["whiteboard_size"] ? $service_info["whiteboard_size"] : 20;
                $room_seat["cabinet_size"] = $service_info["cabinet_size"] ? $service_info["cabinet_size"] : 20;
                if ($service_info["document_filetype"]) {
                    $whiteboard_filetype["document"] = explode(",", $service_info["document_filetype"]);
                }
                if ($service_info["image_filetype"]) {
                    $whiteboard_filetype["image"] = explode(",", $service_info["image_filetype"]);
                }
                if ($service_info["cabinet_filetype"]) {
                    $cabinet_filetype = explode(",", $service_info["cabinet_filetype"]);
                }
                $room_seat["cabinet_filetype"] =serialize($cabinet_filetype);
            }
            if ($service_info["service_name"] == "VCUBE_Doctor_Standard") {
                $room_seat["invited_limit_time"] = $service_info["invited_limit_time"] ? $service_info["invited_limit_time"] : 30;
            }
            $add_seat = $room_db->update($room_seat, $where);
            //プランのオプション情報取得
            $add_options = array();
            if ($service_info["meeting_ssl"] == 1) {
                $add_options[] = "2";
            }
            if ($service_info["desktop_share"] == 1) {
                $add_options[] = "3";
            }
            if ($service_info["high_quality"] == 1) {
                $add_options[] = "5";
            }
            if ($service_info["mobile_phone"] > 0) {
                $add_options[] = "6";
            }
            if ($service_info["h323_client"] > 0) {
                $add_options[] = "8";
            }
            //hdd_extentionは数値分登録
            if ($service_info["hdd_extention"] > 0) {
                $add_options[] = "4";
            }
            // ホワイトボードプラン
            if ($service_info["whiteboard"] == 1) {
                $add_options[] = "16";
            }
            // マルチカメラ
            if ($service_info["multicamera"] == 1) {
                $add_options[] = "18";
            }
            // 電話連携
            if ($service_info["telephone"] == 1) {
                $add_options[] = "19";
            }
            // 録画GW
            if ($service_info["record_gw"] == 1) {
                $add_options[] = "20";
            }
            // スマートフォン
            if ($service_info["smartphone"] == 1) {
                $add_options[] = "21";
            }
            // 資料共有映像再生許可
            if ($service_info["whiteboard_video"] == 1) {
                $add_options[] = "22";
            }
            // PGi連携
            if ($service_info["teleconference"] == 1) {
                $add_options[] = "23";
            }
            // 最低帯域アップ
            if ($service_info["video_conference"] == 1) {
                $add_options[] = "24";
            }
            // 最低帯域アップ
            if ($service_info["minimumBandwidth80"] == 1) {
                $add_options[] = "25";
            }
            // 最低帯域アップ
            if ($service_info["h264"] == 1) {
                $add_options[] = "26";
            }
            //オプション登録
            if ($add_options) {
                foreach ($add_options as $value) {
                    $where = "room_key = '".addslashes($room_key)."'".
                             " AND service_option_key = ".$value.
                             " AND ordered_service_option_status = 1";
                    $count = $ordered_service_option->numRows($where);
                    $this->logger2->debug($count);
                    if ($count == 0 || "4" == $value || "6" == $value || "8" == $value) {
                        if ("4" == $value) {
                            $this->logger2->info($value);
                            for ($num = 1; $num <= $service_info["hdd_extention"]; $num++ ) {
                                $this->action_room_option_add($room_key, $value, null);
                            }
                        } else if ("6" == $value) {
                            $this->logger2->info($value);
                            for ($num = 1; $num <= $service_info["mobile_phone"]; $num++ ) {
                                $this->action_room_option_add($room_key, $value, null);
                            }
                        } else if ("8" == $value) {
                            $this->logger2->info($value);
                            for ($num = 1; $num <= $service_info["h323_client"]; $num++ ) {
                                $this->action_room_option_add($room_key, $value, null);
                            }
                        } else {
                            $this->action_room_option_add($room_key, $value, null);
                        }
                    }
                }
                if (DB::isError($add_seat)) {
                    $this->logger2->info( $add_seat->getUserInfo());
                }
            }
            //プレミアムプランへの変更時はオーディエンスオプションを停止
            if ($service_info["max_audience"] != 0) {
                $where = "room_key = '".addslashes($room_key)."'".
                             " AND service_option_key = 7".
                             " AND ordered_service_option_status = 1";
                $data = array (
                    "ordered_service_option_status" => "0"
                );
                $audience_option = $ordered_service_option->update($data, $where);
            }
            //自動停止日の設定があったら、expire_deteを更新する
            if ($service_info["service_expire_day"] > 0) {
                $where = "room_key = '".addslashes($room_key)."'";
                $room_info = $room_db->getRow($where, "user_key");
                $this->logger2->info(array($service_info,$room_info));
                if ($room_info["user_key"]) {
                    $where = "user_key = ".addslashes($room_info["user_key"]);
                    $expire_date = strtotime($start_time);
                    $expire_date = date( "Y-m-d 00:00:00" , strtotime( "+".$service_info["service_expire_day"]." day" , $expire_date) );
                    $data = array("user_expiredatetime" => $expire_date);
                    $objUser = new N2MY_DB($this->get_dsn(), "user");
                    $updateUser = $objUser->update($data, $where);
                    if (DB::isError($updateUser)) {
                        $this->logger2->info( $updateUser->getUserInfo());
                    }
                }
            }
        } else {
            //プラン登録
            $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
                $room_plan = array (
                "room_key" => $room_key,
                "service_key" => $service_key,
                "room_plan_status" => "2",
                "room_plan_starttime" => $start_time,
                "room_plan_endtime" => $end_time,
                "discount_rate" => $discount_rate,
                "contract_month_number" => $contract_month_number,
                "room_plan_registtime" => date("Y-m-d H:i:s"),
            );
            $add_plan = $room_plan_db->add($room_plan);
            if (DB::isError($add_plan)) {
                $this->logger2->info( $add_plan->getUserInfo());
            }
        }
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "room_plan",
            "keyword"            => $room_key,
            "info"               => serialize($room_plan),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return true;
    }

     /**
     * ルームプラン登録（プランに合わせて人数、オプションも登録）
     */
    function action_user_option_add($user_key = null) {
        if (!$user_key) {
            $user_key = $this->request->get('user_key');
        }
        if (!$user_key) {
            $this->logger2->warn('required user_key');
            return false;
        }
        //プラン登録
        $user_plan_db = new N2MY_DB($this->get_dsn(), "user_plan");
        $user_plan_info = $user_plan_db->getRow("user_key = '".$user_key."' AND user_plan_status = 1");
        if (DB::isError($user_plan_info)) {
            $this->logger2->info( $user_plan_info->getUserInfo());
            return false;
        }
        $service_db = new N2MY_DB($this->account_dsn, "service");
        $service_info = $service_db->getRow("service_key = ".$user_plan_info['service_key']);
        //プランのオプション情報取得
        $add_options = array();
        if ($service_info["meeting_ssl"] == 1) {
            $add_options[] = "2";
        }
        if ($service_info["desktop_share"] == 1) {
            $add_options[] = "3";
        }
        if ($service_info["high_quality"] == 1) {
            $add_options[] = "5";
        }
        if ($service_info["mobile_phone"] > 0) {
            $add_options[] = "6";
        }
        if ($service_info["h323_client"] > 0) {
            $add_options[] = "8";
        }
        //hdd_extentionは数値分登録
        if ($service_info["hdd_extention"] > 0) {
            $add_options[] = "4";
        }
        // ホワイトボードプラン
        if ($service_info["whiteboard"] == 1) {
            $add_options[] = "16";
        }
        // マルチカメラ
        if ($service_info["multicamera"] == 1) {
            $add_options[] = "18";
        }
        // 電話連携
        if ($service_info["telephone"] == 1) {
            $add_options[] = "19";
        }
        // 録画GW
        if ($service_info["record_gw"] == 1) {
            $add_options[] = "20";
        }
        // スマートフォン
        if ($service_info["smartphone"] == 1) {
            $add_options[] = "21";
        }
        // 資料共有映像再生許可
        if ($service_info["whiteboard_video"] == 1) {
            $add_options[] = "22";
        }
        // PGi連携
        if ($service_info["teleconference"] == 1) {
            $add_options[] = "23";
        }
        // 最低帯域アップ
        if ($service_info["video_conference"] == 1) {
            $add_options[] = "24";
        }
        // 最低帯域アップ
        if ($service_info["minimumBandwidth80"] == 1) {
            $add_options[] = "25";
        }
        // 最低帯域アップ
        if ($service_info["h264"] == 1) {
            $add_options[] = "26";
        }
        // 登録されているオプションを全て破棄
        require_once( "classes/dbi/user_service_option.dbi.php" );
        $objUserServiceOption = new UserServiceOptionTable( $this->get_dsn() );
        $option_reset = array(
            'user_service_option_status' => 0,
            'user_service_option_deletetime' => date('Y-m-d H:i:s')
            );
        $where = "user_key = '".addslashes($user_key)."'" .
            " AND user_service_option_status = 1";
        $this->logger2->info(array($option_reset, $where, $add_options));
        $objUserServiceOption->update($option_reset, $where);
        // メンバーのオプションも破棄
        $objMember = new N2MY_DB($this->get_dsn(), "member");
        $objOrderedServiceOption = new N2MY_DB( $this->get_dsn(), "ordered_service_option" );
        $where = sprintf("user_key=%s", $user_key);
        $memberList = $objMember->getRowsAssoc($where);
        // メンバー一覧
        foreach($memberList as $member) {
            $data = array(
                'ordered_service_option_status' => 0,
                'ordered_service_option_deletetime' => date('Y-m-d H:i:s')
            );
            $where = "room_key = '".addslashes($member['room_key'])."'" .
                " AND ordered_service_option_status = '1'";
            $objOrderedServiceOption->update($data, $where);
        }
        // オプション情報を更新
        foreach($add_options as $val) {
            $data = array(
                "user_key"                      => $user_key,
                "service_option_key"            => $val,
                "user_service_option_starttime" => $user_plan_info["user_plan_starttime"]
            );
            // ユーザプランを追加し、部屋にも設定する。
            $this->_action_user_serviceoption_add($data);
        }
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "user_plan",
            "keyword"            => $add_options,
            "info"               => serialize(array($user_key, $add_options)),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return true;
    }

     /**
     * ルームプラン変更
     */
    function action_room_plan_change() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_room_detail();
            exit;
        }
        $room_key = $this->request->get("room_key");
        if ($form_data = $this->request->get("change_form")) {
            $change_time = $form_data["room_starttime"];
            $service_key = $form_data["service_key"];
        } else {
            $service_key = $this->request->get("service_key");
            $change_time = $this->request->get("plan_start_time");
        }
        if ( !$form_data["discount_rate"] ) {
            $form_data["discount_rate"] = 0;
        } else if (!is_numeric($form_data["discount_rate"])) {
            $message .= "割引率は数値で入力してください。<br />";
        }
        if ($form_data["room_plan_yearly"]) {
            if (!$form_data["room_plan_endtime"]) {
                $form_data["room_plan_endtime"] = "0000-00-00 00:00:00";
            }
            if (!$form_data["contract_month_number"]) {
                $message .= "契約期間（月数）を選択してください。<br />";
            }
        } else {
            $form_data["room_plan_endtime"] = "0000-00-00 00:00:00";
            $form_data["contract_month_number"] = "0";
        }
        if ( $message ) {
            $this->action_room_detail($message);
            exit;
        }
        $this->logger->trace("plan_start",__FILE__,__LINE__,$change_time);
        if (!$change_time || $change_time == "undefined") {
            $change_time = date("Y-m-d 00:00:00");
            $form_data["room_starttime"] = date("Y-m-d 00:00:00");
        }
        $this->logger->debug("form_data",__FILE__,__LINE__,$form_data);

        //現在のプランを停止
        if (strtotime($change_time) <= strtotime(date("Y-m-d 23:59:59"))) {
            $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
            $where = "room_key='".$room_key."'".
                     " AND room_plan_status = 1";
            //現在のプランを取得
            $now_plan = $room_plan_db->getRow($where);

            $data = array(
                "room_plan_status" => 0,
                "room_plan_updatetime" => date("Y-m-d H:i:s"),
            );
            $room_plan_db->update($data, $where);
            //サービスダウングレードの際はオプションも削除
            $this->logger->debug("now_plan",__FILE__,__LINE__,$now_plan["service_key"]);
            if ($now_plan["service_key"] == "44") {
                $this->action_room_option_delete($room_key, "3", null);
                $this->action_room_option_delete($room_key, "5", null);
                $this->action_room_option_delete($room_key, "6", null);
            } else if ($service_key == "41" || $service_key == "42") {
                $this->action_room_option_delete($room_key, "3", null);
                $this->action_room_option_delete($room_key, "6", null);
            } else if ($service_key < "73") {
                $this->action_room_option_delete($room_key, "26", null);
            }
        }
        //新しいプランを登録
        $this->action_room_plan_add($room_key, $form_data);
        $this->request->set("room_key", $room_key);
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "room_plan",
            "keyword"            => $room_key,
            "info"               => serialize($service_key),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return $this->action_room_detail();
    }

    function action_pgi_setting() {

        require_once("classes/dbi/pgi_setting.dbi.php");
        require_once("classes/dbi/pgi_rate.dbi.php");
        $pgiSettingTable = new PGiSettingTable($this->get_dsn());
        $pgiRateTable    = new PGiRateTable($this->get_dsn());

        $room_key = $this->request->get("room_key");
        if (!$this->check_submit_key($this->_name_space) || !$room_key) {
            return $this->action_room_detail();
        }

        if (!$form_data = $this->request->get("add_form")) {
            return $this->action_room_detail();
        }

        try {
            $pgiSettings = $pgiSettingTable->findByRoomKey($room_key);
        } catch (Exception $e) {
            die('error : '.__FILE__.__LINE__);
        }

        $validated = $this->validatePGiSetting($form_data, $room_key);
        if ($validated['message']) {
            $this->action_room_detail($validated['message']);
            exit;
        }

        $validated['room_key'] = $room_key;

        $this->request->set("room_key", $room_key);


        $pgi_setting_data = array('room_key'   => $room_key,
                                  'startdate'  => $validated['data']['startdate'],
                                  'system_key' => $validated['data']['system_key'],
                                  'company_id' => $validated['data']['company_id'],);
        try {
            $pgi_setting_key = $pgiSettingTable->add($pgi_setting_data);
        } catch (Exception $e) {
            $this->exitWithError($e->getMessage());
        }

        require_once('classes/pgi/PGiSystem.class.php');
        $pgi_system = PGiSystem::findBySystemKey($pgi_setting_data['system_key']);

        if ($pgi_system->rates) { // 設定にデフォルト料金が登録されているsystemはpgi_rateに料金を保存
            require_once("classes/pgi/PGiRate.class.php");
            if ($pgi_setting_data["system_key"] == "VCUBE GM PRODUCTION") {
                $rate_config = PGiRate::findGmConfigAll();
            } else {
                $rate_config = PGiRate::findConfigAll();
            }
            foreach ($rate_config as $pgi_rate) {
                $pgi_rate_data = array('pgi_setting_key' => $pgi_setting_key,
                                       'rate_type'       => (string)$pgi_rate->type,
                                       'rate'            => $validated['data'][(string)$pgi_rate->type]);
                try {
                    $pgiRateTable->add($pgi_rate_data);
                } catch (Exception $e) {
                    $this->exitWithError($e->getMessage());
                }
            }
        }
        //操作ログ登録
        $operation_data = array ("staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                                 "action_name"        => __FUNCTION__,
                                 "table_name"         => "pgi_setting, pgi_rate",
                                 "keyword"            => $room_key,
                                 "info"               => serialize($pgi_setting_data['system_key']),
                                 "operation_datetime" => date("Y-m-d H:i:s"),
        );

        $this->add_operation_log($operation_data);
        return $this->action_room_detail();
    }

    /*
     * オプション登録、停止
     */
    function action_room_option() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_room_detail();
            exit;
        }
        $room_key    = $this->request->get("room_key");
        $action_type = $this->request->get("action_type");
        if ($form_data = $this->request->get("add_form")) {
            $option_start_time          = $form_data["option_start_time"];
        } else if ($form_data = $this->request->get("expire_form")) {
            $option_expire_date_time    = $form_data["option_expire_date_time"];
        }
        if (isset($form_data)) {
            if ($action_type == "expire") {
                $ordered_service_option_key = $form_data["ordered_service_option_key"];
            } else {
                $service_option_key         = $form_data["service_option_key"];
            }
        } else {
            $option_start_time          = $this->request->get("option_start_time");
            $service_option_key         = $this->request->get("service_option_key");
            $ordered_service_option_key = $this->request->get("ordered_service_option_key");
        }

        $option_db      = new N2MY_DB($this->account_dsn, "service_option");
        $service_option = $option_db->getRowsAssoc();
        $option_db      = new N2MY_DB($this->get_dsn(), "ordered_service_option");
        $columns        = $option_db->getTableInfo("data");
        if ($service_option_key && ($action_type == "add")) {
            if ($service_option_key == 24) {// ivesconference
                $this->action_create_conference($room_key);
            }
            $this->action_room_option_add($room_key, $service_option_key, $option_start_time);
        } else if ($ordered_service_option_key && ($action_type == "expire")) {
            $this->action_room_option_expire($room_key, $ordered_service_option_key, $option_expire_date_time);
        } else if ($service_option_key && ($action_type == "delete")) {
            $this->action_room_option_delete($room_key, $service_option_key, $ordered_service_option_key);
        }

        return $this->action_room_detail();
    }

    //オプション登録
    function action_room_option_add($room_key, $service_option_key, $option_start_time) {
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
        $this->logger->trace("start_time",__FILE__,__LINE__,$option_start_time);
        if (!$option_start_time) {
            $option_start_time = date("Y-m-d H:i:s");
        }

        if ($service_option_key == 23) {// teleconference
            require_once("classes/dbi/pgi_setting.dbi.php");
            $pgi_setting_table = new PGiSettingTable($this->get_dsn());

            $month_first_day = substr($option_start_time ,0 ,7);
            $pgi_setting = $pgi_setting_table->findEnablleAtYM($room_key, $month_first_day);
            if (count($pgi_setting) <= 0) {
                 $this->action_room_detail('', 'teleconferenceのオプションを付与するには、'.$month_first_day.'以降のPGi設定を作成する必要があります');
                 exit;
            }
        }

        $today = date("Y-m-d 23:59:59");
        if (strtotime($option_start_time) >= strtotime($today))  {
            $status = "2";
        } else {
            $status = "1";
        }
        $data = array(
            "room_key" => $room_key,
            "service_option_key" => $service_option_key,
            "ordered_service_option_status" => $status,
            "ordered_service_option_starttime" => $option_start_time,
            "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
            "ordered_service_option_deletetime" => "0000-00-00 00:00:00",
            );
        //オーディエンスオプションの際は部屋の人数変更
        if ($service_option_key == 7) {
            $where = "room_key = '".addslashes($room_key)."'".
                     " AND room_plan_status = 1";
            $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
            $room_plan = $room_plan_db->getRow($where);
            if ($room_plan["service_key"] == "45" || $room_plan["service_key"] == "46" || $room_plan["service_key"] == "47") {
                $this->logger2->error("not add audience");
            } else if (!$room_plan) {
                //プランが無かった場合は予約があるか確認
                $where = "room_key = '".addslashes($room_key)."'".
                     " AND room_plan_status = 2";
                $room_plan_reserve = $room_plan_db->getRow($where);
                if ($room_plan_reserve) {
                    $ordered_service_option->add($data);
                }
            } else {
                $ordered_service_option->add($data);
                $where_coount = "room_key = '".addslashes($room_key)."'".
                     " AND service_option_key = ".addslashes($service_option_key).
                     " AND ordered_service_option_status = 1";
                $audience_count = $ordered_service_option->numRows($where_coount);
                if ($audience_count > 0) {
                    $seat_data["max_seat"] = "9";
                    $audience_seat = $audience_count."0";
                    $this->logger2->info($audience_seat);
                } else {
                    $seat_data["max_seat"] = "10";
                    $audience_seat = "0";
                }
                $seat_data["max_audience_seat"] = $audience_seat;
                $where_room = "room_key = '".addslashes($room_key)."'";
                $room_db = new N2MY_DB($this->get_dsn(), "room");
                $room_data = $room_db->update($seat_data, $where_room);
            }
        } else {
            $ordered_service_option->add($data);
            if ($service_option_key == 23) {// teleconference
                $room_db       = new N2MY_DB($this->get_dsn(), "room");
                $teleconf_data = array('use_teleconf'=>1, 'use_pgi_dialin'=>1, 'use_pgi_dialin_free'=>1, 'use_pgi_dialout'=>1);
                $room_db->update($teleconf_data , "room_key = '".addslashes($room_key)."'");
            }
        }
        // スマートフォン連携時に電話連携がなければ自動でセットする
        if ($service_option_key == 21) {
            $where = "room_key = '".addslashes($room_key)."'".
                 " AND service_option_key = 19".
                 " AND ordered_service_option_status = 1";
            $count = $ordered_service_option->numRows($where);
            if ($count == 0) {
                $data2 = $data;
                $data2["service_option_key"] = 19;
                $ordered_service_option->add($data2);
            }
        }
        //テレビ会議連携の場合は人数を9に変更
        if ($service_option_key == 24) {
            $seat_data = array("max_seat" => 9);
            $where_room = "room_key = '".addslashes($room_key)."'";
            $room_db = new N2MY_DB($this->get_dsn(), "room");
            $room_data = $room_db->update($seat_data, $where_room);
        }
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "ordered_service_option",
            "keyword"            => $room_key,
            "info"               => serialize($data),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return true;
    }

     /**
     * オプション停止
     */
    function action_room_option_delete($room_key, $service_option_key, $ordered_service_option_key) {
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
        if (!$room_key) {
            $room_key = $this->request->get("room_key");
        }
        if (!$service_option_key) {
            $service_option_key = $this->request->get("service_option_key");
        }
        $where = "room_key = '".addslashes($room_key)."'".
                 " AND service_option_key = ".addslashes($service_option_key);
        if ($ordered_service_option_key) {
            $where .= " AND ordered_service_option_key = ".addslashes($ordered_service_option_key);
        }
        $this->logger->trace("where",__FILE__,__LINE__,$where);
        $data = array(
            "ordered_service_option_status" => 0,
            "ordered_service_option_deletetime" => date("Y-m-d H:i:s"),
        );
        $ordered_service_option->update($data, $where);

        // ivesconference
        if ($service_option_key == 24) {
            $where = "room_key = '".addslashes($room_key)."'".
                " AND ordered_service_option_status = 1".
                " AND service_option_key = ".addslashes($service_option_key);
            $iveOptions = $ordered_service_option->getRowsAssoc($where);
            // オプション数が０になったらカンファレンス削除
            if (count($iveOptions) <= 0) {
              $this->action_remove_conference($room_key);
              $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
              $where = "room_key='".$room_key."'".
                       " AND room_plan_status = 1";
              //現在のプランを取得
              $now_plan = $room_plan_db->getRow($where);
              $objService = new N2MY_DB( $this->account_dsn, "service" );
              $where = sprintf( "service_key=%s", $now_plan["service_key"] );
              $now_plan_info = $objService->getRow( $where );
              $max_seat = $now_plan_info["max_seat"] ? $now_plan_info["max_seat"] : 10;
              $this->logger2->info($now_plan_info);
              $room_db = new N2MY_DB($this->get_dsn(), "room");
              $where_room = "room_key = '".addslashes($room_key)."'";
              $seat_data = array("max_seat" => $max_seat);
              $room_data = $room_db->update($seat_data, $where_room);
            }
        }

        //オーディエンスオプションの際は部屋の人数変更
        if ($service_option_key == 7) {
            $where = "room_key = '".addslashes($room_key)."'".
                     " AND room_plan_status = 1";
            $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
            $room_plan = $room_plan_db->getRow($where);
            $where_coount = "room_key = '".addslashes($room_key)."'".
                     " AND service_option_key = ".addslashes($service_option_key).
                     " AND ordered_service_option_status = 1";
            $audience_count = $ordered_service_option->numRows($where_coount);
            if ($audience_count > 0) {
                $audience_seat = $audience_count."0";
                $this->logger2->info($audience_seat);
            }
            $room_db = new N2MY_DB($this->get_dsn(), "room");
            $where_room = "room_key = '".addslashes($room_key)."'";
            $room_data = $room_db->getRow($where_room);
            if ($room_plan["service_key"] != "45" && $room_plan["service_key"] != "46" && $room_plan["service_key"] != "47") {
                if ($audience_count > 0) {
                    $seat_data["max_seat"] = "9";
                    $seat_data["max_audience_seat"] = $audience_count."0";
                    $room_data = $room_db->update($seat_data, $where_room);
                } else {
                    $seat_data["max_seat"] = "10";
                    $seat_data["max_audience_seat"] = "0";
                    $room_data = $room_db->update($seat_data, $where_room);
                }
            } else if ($room_plan["service_key"] == "45") {
                $seat_data["max_seat"] = "9";
                $seat_data["max_audience_seat"] = "11";
                $room_data = $room_db->update($seat_data, $where_room);
            } else if ($room_plan["service_key"] == "46") {
                $seat_data["max_seat"] = "9";
                $seat_data["max_audience_seat"] = "21";
                $room_data = $room_db->update($seat_data, $where_room);
            } else if ($room_plan["service_key"] == "47") {
                $seat_data["max_seat"] = "9";
                $seat_data["max_audience_seat"] = "31";
                $room_data = $room_db->update($seat_data, $where_room);
            }
        }

        //資料共有オプションの場合は、メンバーがいたらVCUBE ID側のservice情報を停止する
        $wsdl = $this->config->get('VCUBEID','wsdl');
        if ($service_option_key == 16 && $wsdl) {
        }
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "ordered_service_option",
            "keyword"            => $room_key,
            "info"               => $service_option_key,
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return true;
    }

    /*
     * 登録済みのオプションに終了日を設定
     */
    public function action_room_option_expire($room_key, $ordered_service_option_key, $option_expire_date_time) {
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
        $this->logger->trace("expire_date_time",__FILE__,__LINE__,$option_expire_date_time);
        $today = date("Y/m/d H:i:s");
        if (strtotime($option_expire_date_time) < strtotime($today)) {
             $this->action_room_detail('', '停止予定日は、'.$today.'以降の日にちを設定する必要があります', $option_expire_date_time);
             exit;
        }
        $where = "room_key = '".addslashes($room_key)."'".
                 " AND ordered_service_option_key = ".addslashes($ordered_service_option_key);
        $this->logger->trace("where",__FILE__,__LINE__,$where);

        $data = array(
            "ordered_service_option_expiredatetime" => $option_expire_date_time,
        );
        $ordered_service_option->update($data, $where);
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "ordered_service_option",
            "keyword"            => $room_key,
            "info"               => $ordered_service_option_key,
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return true;
    }

    /*
     * メンバー課金を利用しているユーザーの
     * プラン追加
     */
    public function action_userplan_add( $message="" )
    {
        $this->set_submit_key($this->_name_space);
        $user_key = $this->request->get("user_key");
        if (!$user_key) {
            $user_key = $this->session->get("user_key");
        }
        if ($message) {
            $this->template->assign("message",$message);
            $user_key = $this->session->get("user_key");
        }
        $this->session->set("user_key",$user_key);

        $sort_key = $this->request->get("sort_key");
        if (!$sort_key) {
            $sort_key = "user_plan_key";
        }
        $sort_type = $this->request->get("sort_type");
        if(!$sort_type) {
            $sort_type = "asc";
        }

        $data_db = new N2MY_DB( $this->get_dsn(), "user_plan" );
        $action_name = __FUNCTION__;
        $columns = $data_db->getTableInfo("data");
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $sort_key, $sort_type);
        $this->logger->trace("form_data",__FILE__,__LINE__,$form_data);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where = "user_key='".$user_key."'";
        $this->logger->trace("columns",__FILE__,__LINE__,$columns);

        // データ取得
        $rows = $data_db->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $total_count = $data_db->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name)
            );
        $add_form = $this->session->get("temp", $action_name);
        $this->template->assign("add_form", array(
            "data" => $add_form,
            "ok_action" => $action_name,
            "ng_action" => $action_name
          ));
        $this->template->assign("columns",$columns);
        $this->logger->trace("data",__FILE__,__LINE__,$rows);
        $this->template->assign("rows",$rows);
        $this->session->remove("message");

        $plan_db = new N2MY_DB($this->account_dsn, "service");
        $where = "service_status='1'";
        $service_list = $plan_db->getRowsAssoc($where);
        $this->logger->info("service_list",__FILE__,__LINE__,$service_list);
        $this->template->assign("user_key",$user_key);
        $this->template->assign("service_list",$service_list);
        $template = "sastik_admin/user/add_plan.t.html";
        $this->_display($template);
    }

    /*
     * メンバー課金を利用しているユーザーの
     * プラン追加
     */
    public function action_add_userplan_complete( $request = null )
    {
        $data_db = new N2MY_DB( $this->get_dsn(), "user_plan" );
        $formData = $request ? $request : $this->request->get( "add_form" );
        $formData["user_plan_registtime"] = date( "Y-m-d H:i:s" );
        $formData["user_plan_status"] = ( strtotime( $formData["user_plan_starttime"] ) <= strtotime( date( "Y-m-d 23:59:59" ) ) ) ? 1 : 2 ;
        $formData["user_plan_yearly_starttime"] = $formData["user_plan_yearly"] ? $formData["user_plan_starttime"] : "";
        $result = $data_db->add( $formData );
        if( DB :: isError( $result ) ){
            $this->logger2->error( $result );
        }

        //memberList
        require_once( "classes/dbi/member.dbi.php" );
        require_once( "classes/dbi/room_plan.dbi.php" );
        $objMember = new MemberTable( $this->get_dsn() );
        $where = sprintf( "user_key=%s", $formData["user_key"] );
        $memberList = $objMember->getRowsAssoc( $where );

        /** メンバーが存在する場合roomplanへ追加**/
        $objRoomPlan = new RoomPlanTable( $this->get_dsn() );
        for( $i = 0; $i < count( $memberList ); $i++ ){
            $roomPlanData = array(
                                "room_key"                  => $memberList[$i]["room_key"],
                                "service_key"               => $formData["service_key"],
                                "room_plan_yearly"          => $formData["user_plan_yearly"],
                                "room_plan_yearly_starttime"=> $formData["user_plan_yearly_starttime"],
                                "room_plan_starttime"       => $formData["user_plan_starttime"],
                                "room_plan_status"          => $formData["user_plan_status"],
                                "room_plan_registtime"      => $formData["user_plan_registtime"] );
            $objRoomPlan->add( $roomPlanData );
        }
        header( "Location: index.php?action_individual" );
    }

    /**
     * メンバー課金ユーザーのサービスオプション
     */
    public function action_individual($message = "")
    {
        $this->set_submit_key($this->_name_space);
        $user_key = $this->request->get("user_key");
        $user_key = ( ! $user_key ) ? $this->session->get( "user_key" ) : $user_key;

        $objUser = new N2MY_DB( $this->get_dsn(), "user" );
        $user_info = $objUser->getRow("user_key='".$user_key."'");
        $this->template->assign( "user_key", $user_key );
        $this->template->assign( "user_info", $user_info );
        $this->session->set( "user_key", $user_key );

        if ($centre_plan = $this->config->get("CENTRE", "plan_list")) {
            $service_db = new N2MY_DB( $this->account_dsn, "service" );
            $service_option_list = $service_db->getRowsAssoc("service_key IN (".addslashes($centre_plan).") AND service_status = '1'");
            $this->template->assign("centre_plan_list", $service_option_list );
        }
//        $this->session->set("table", "user_serviceplan");
        $sort_key_plan = $this->request->get("sort_key_plan");
        $sort_type_plan = $this->request->get("sort_type_plan");

        $sort_key_plan = isset($sort_key_plan) ? $sort_key_plan : "user_plan_key";
        $sort_type_plan = isset($sort_type_plan) ? $sort_type_plan : "desc";
        //プラン情報取得
        $objUserPlan = new N2MY_DB( $this->get_dsn(), "user_plan" );
        $where = "user_key='".$user_key."'";
        $plan_list = $objUserPlan->getRowsAssoc( $where, array($sort_key_plan => $sort_type_plan) );
        $this->logger->debug("plan_list",__FILE__,__LINE__, $plan_list );
        $this->template->assign( "user_plan_list", $plan_list );

        $user_plan_columns = $objUserPlan->getTableInfo( "data" );
        $this->logger->trace("new_columuns",__FILE__,__LINE__,$user_plan_columns);
        $this->template->assign("user_plan_columns",$user_plan_columns);
        //現在有効なプランを取得
        $where_now = "user_key='".$user_key."'".
                 " AND user_plan_status = 1";
        $now_plan_info = $objUserPlan->getRow( $where_now );
        $this->logger->info("now_plan",__FILE__,__LINE__,$now_plan_info );
        //現在有効なプランが無い場合は開始待ちのプランを取得
        if (!$now_plan_info) {
            $where_now = "user_key='".$user_key."'".
                 " AND user_plan_status = 2";
            $now_plan_info = $objUserPlan->getRow( $where_now );
        }

        /* 登録されているプランがあればサービス名を取得*/
        if( $now_plan_info ){
            $objService = new N2MY_DB( $this->account_dsn, "service" );
            $where = sprintf( "service_key=%s", $now_plan_info["service_key"] );
            $now_plan_info = $objService->getRow( $where );
        }
        $this->template->assign( "now_plan_info", $now_plan_info );

        //オプション情報取得
        $sort_key_option = $this->request->get("sort_key_option");
        $sort_type_option = $this->request->get("sort_type_option");
        $sort_key_option = isset($sort_key_option) ? $sort_key_option : "user_service_option_key";
        $sort_type_option = isset($sort_type_option) ? $sort_type_option : "desc";

        $service_option_db = new N2MY_DB( $this->account_dsn, "service_option" );
        $service_option = $service_option_db->getRowsAssoc();

        $objUserServiceOption = new N2MY_DB($this->get_dsn(), "user_service_option");
        $option_columns = $objUserServiceOption->getTableInfo("data");
        $option_columns = $this->_get_relation_data( $this->account_dsn, $option_columns);
        $where = sprintf( "user_key=%s AND service_option_key != 14", $user_key );
        $option_data = $objUserServiceOption->getRowsAssoc( $where, array($sort_key_option => $sort_type_option));
        $where = sprintf( "user_key=%s AND service_option_key = 14", $user_key );
        $supoprt_data = $objUserServiceOption->numRows( $where );
        $this->logger->debug("option_data",__FILE__,__LINE__,$option_data);
        $this->template->assign("page", $page);
        $this->template->assign("support_num",$supoprt_data);
        $this->template->assign("service_option",$service_option);
        $this->template->assign("message",$message);
        $this->template->assign("option_columns",$option_columns);
        $this->template->assign("option_data",$option_data);

        $template = "sastik_admin/individual/detail.t.html";
        $this->_display( $template );
    }

     /**
     * ユーザープラン変更
     */
    public function action_user_plan_change()
    {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_individual();
            exit;
        }
        $request = $this->request->getAll();

        if ( ! $request["change_form"]["user_plan_starttime"] ) {
            $request["change_form"]["user_plan_starttime"] = date("Y-m-d 00:00:00");
        }
        if (!$request["change_form"]["discount_rate"]) {
            $request["change_form"]["discount_rate"] = 0;
        } else if (!is_numeric($request["change_form"]["discount_rate"])) {
            $message .= "割引率は数値で入力してください。<br />";
        }
        if ($request["change_form"]["user_plan_yearly"]) {
            if (!$request["room_plan_endtime"]) {
                $form_data["room_plan_endtime"] = "0000-00-00 00:00:00";
            }
            if (!$request["change_form"]["contract_month_number"]) {
                $message .= "契約期間（月数）を選択してください。<br />";
            }
        } else {
            $request["change_form"]["user_plan_endtime"] = "0000-00-00 00:00:00";
            $request["change_form"]["contract_month_number"] = "0";
        }
        if ( $message ) {
            $this->action_individual($message);
            exit;
        }
        /** 現在のアクティブなプランを取得 */
        require_once( "classes/dbi/user_plan.dbi.php" );
        $objUserPlan = new UserPlanTable( $this->get_dsn(), "user_plan" );
        $where = sprintf( "user_key='%s' AND user_plan_status=1", $request["change_form"]["user_key"] );
        $nowPlanInfo = $objUserPlan->getRow( $where );

        /** アクティブなプランがない場合、最新のプランを取得 **/
        if( ! $nowPlanInfo ){
            $where = sprintf( "user_key='%s'", $request["change_form"]["user_key"] );
            $sort = array( "user_plan_registtime" => "DESC" );
            $nowPlanInfo = $objUserPlan->getRow( $where, null, $sort );
        }
        /* 一つも登録がなかった場合新規登録 */
//        if( ! $nowPlanInfo ){
//            $this->action_add_userplan_complete( $request["change_form"] );
//            exit;
//        }
        $now = date( "Y-m-d H:i:s");

        //memberList
        $objMember = new N2MY_DB( $this->get_dsn(), "member" );
        $objOrderedServiceOption = new N2MY_DB( $this->get_dsn(), "ordered_service_option" );
        $where = sprintf( "user_key=%s", $request["change_form"]["user_key"] );
        $memberList = $objMember->getRowsAssoc( $where );

        // 現在のプランを停止
        if (strtotime( $request["change_form"]["user_plan_starttime"]) <= strtotime(date("Y-m-d 23:59:59" ))) {
            /* 現在アクティブなプランが存在する場合は無効にする */
            if( 1 == $nowPlanInfo["user_plan_status"] ){
                $userPlanData = array(
                    "user_plan_status"        => 0,
                    "user_plan_updatetime"    => $now
                    );
                $where = sprintf( "user_plan_key='%s'", $nowPlanInfo["user_plan_key"] );
                $objUserPlan->update( $userPlanData, $where );
            }
            // 新規プランのデータ作成
            $userPlanData = array(
                "user_key"              => $request["change_form"]["user_key"],
                "service_key"           => $request["change_form"]["service_key"],
                "user_plan_starttime"   => $request["change_form"]["user_plan_starttime"],
                "user_plan_endtime"     => ( $request["change_form"]["user_plan_endtime"] ) ? $request["change_form"]["user_plan_endtime"] : "0000-00-00",
                "discount_rate"         => $request["change_form"]["discount_rate"],
                "contract_month_number" => $request["change_form"]["contract_month_number"],
                "user_plan_registtime"  => $now,
                "user_plan_status"      => 1
                );
            $newUserPlanKey = $objUserPlan->add( $userPlanData );
            //room_plan( member ) へも適用
            $objRoomPlan = new N2MY_DB( $this->get_dsn(), "room_plan" );
            for( $i = 0; $i < count( $memberList ); $i++ ){
                // 現在のプラン
                $where = sprintf( "room_key='%s' AND room_plan_status=1", $memberList[$i]["room_key"] );
                $roomPlanInfo = $objRoomPlan->getRow( $where );
                if( ! $roomPlanInfo ){
                    $where = sprintf( "room_key='%s'", $memberList[$i]["room_key"] );
                    $sort = array( "room_plan_registtime" => "DESC" );
                    $roomPlanInfo = $objRoomPlan->getRow( $where, null, $sort );
                }
                /* 一つもルームプランが追加されていなかった場合は新規ユーザープランからコピー */
                if( ! $roomPlanInfo ){
                    $roomPlanInfo = array(
                        "room_plan_yearly"              => $userPlanData["user_plan_yearly"],
                        "room_plan_yearly_starttime"    => $userPlanData["room_plan_yearly_starttime"]
                    );
                }
                /* 現在有効なルームプランが存在する場合は無効にする */
                if( 1 == $roomPlanInfo["room_plan_status"] ){
                    $roomPlanData = array(
                        "room_plan_status"      => 0,
                        "room_plan_updatetime"  => $now
                        );
                    $where = sprintf( "room_plan_key=%s", $roomPlanInfo["room_plan_key"] );
                    $objRoomPlan->update( $roomPlanData, $where );
                }
                $newRoomPlanInfo = array(
                    "room_key"                  => $memberList[$i]["room_key"],
                    "service_key"               => $request["change_form"]["service_key"],
                    "room_plan_starttime"       => $request["change_form"]["user_plan_starttime"],
                    "room_plan_endtime"         => ( $request["change_form"]["user_plan_endtime"] ) ? $request["change_form"]["user_plan_endtime"] : "0000-00-00",
                    "discount_rate"             => $request["change_form"]["discount_rate"],
                    "contract_month_number"     => $request["change_form"]["contract_month_number"],
                    "room_plan_status"          => 1,
                    "room_plan_registtime"      => date( "Y-m-d H:i:s" )
                    );
                $objRoomPlan->add( $newRoomPlanInfo );
                //部屋の情報更新
                $room_db = new N2MY_DB($this->get_dsn(), "room");
                $service_db      = new N2MY_DB($this->account_dsn, "service");
                $where_service_key = "service_key = ".$request["change_form"]["service_key"];
                $new_plan_info          = $service_db->getRow($where_service_key);
                $room_data = array (
                    "max_seat"              => ($new_plan_info["max_seat"]) ? $new_plan_info["max_seat"] : 10,
                    "max_audience_seat"     => ($new_plan_info["max_audience_seat"]) ? $new_plan_info["max_audience_seat"] : 0,
                    "max_whiteboard_seat"   => ($new_plan_info["max_whiteboard_seat"]) ? $new_plan_info["max_whiteboard_seat"] : 0,
                );
                if ($memberList[$i]["room_key"]) {
                    $where_room = "room_key = '".$memberList[$i]["room_key"]."'";
                   $room_data = $room_db->update($room_data, $where_room);
                }
            }
            // オプションも更新
            $this->action_user_option_add($request["change_form"]["user_key"]);
        //プランの予約
        } else {
            $userPlanData = array(
                "user_key"              => $request["change_form"]["user_key"],
                "service_key"           => $request["change_form"]["service_key"],
                "user_plan_starttime"   => $request["change_form"]["user_plan_starttime"],
                "user_plan_endtime"     => ($request["change_form"]["user_plan_endtime"] ) ? $request["change_form"]["user_plan_endtime"] : "0000-00-00",
                "discount_rate"         => $request["change_form"]["discount_rate"],
                "contract_month_number" => $request["change_form"]["contract_month_number"],
                "user_plan_registtime"  => $now,
                "user_plan_status"      => 2
            );
            // 新しいプランを登録
            $objUserPlan->add( $userPlanData );
            //room_plan( member ) へも適用
            $objRoomPlan = new N2MY_DB( $this->get_dsn(), "room_plan" );
            for( $i = 0; $i < count( $memberList ); $i++ ){
                // 現在のプラン
                $where = sprintf( "room_key='%s' AND room_plan_status=1", $memberList[$i]["room_key"] );
                $roomPlanInfo = $objRoomPlan->getRow( $where );
                $roomPlanData = array(
                    "room_key"              => $memberList[$i]["room_key"],
                    "service_key"           => $request["change_form"]["service_key"],
                    "room_plan_starttime"   => $request["change_form"]["user_plan_starttime"],
                    "room_plan_endtime"     => ( $request["change_form"]["user_plan_endtime"] ) ? $request["change_form"]["user_plan_endtime"] : "0000-00-00",
                    "discount_rate"         => $request["change_form"]["discount_rate"],
                    "contract_month_number" => $request["change_form"]["contract_month_number"],
                    "room_plan_status"      => 2,
                    "room_plan_registtime"  => $now
                    );
                $objRoomPlan->add( $roomPlanData );
            }
        }

        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "room_plan",
            "keyword"            => $request["change_form"]["service_key"],
            "info"               => serialize( $request ),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        header( "Location: ?action_individual" );
        exit;
    }

    public function action_user_support_add()
    {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_individual();
            exit;
        }
        require_once( "classes/dbi/user_service_option.dbi.php" );
        $request = $this->request->getAll();
        $objUserServiceOption = new UserServiceOptionTable( $this->get_dsn() );
        $data = array(
                    "user_key"                        => $request["add_form"]["user_key"],
                    "service_option_key"            => 14,
                    "user_service_option_status"    => 1,
                    "user_service_option_starttime"    => date( "Y-m-d" ),
                    "user_service_option_registtime"=> date( "Y-m-d H:i:s" )
                    );
        for( $i = 0; $i < $request["add_form"]["support_num"]; $i++ ){
            $objUserServiceOption->add( $data );
        }
        $this->add_operation_log( $data );
        header( "Location: ?action_individual" );
        exit;
    }

    //オプション登録(member課金)
    public function action_user_serviceoption_add()
    {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_individual();
            exit;
        }
        $add_form = $this->request->get( "add_form" );
        // オプション追加＆メンバーのオプション追加
        $this->session->set( "user_key", $add_form["user_key"] );
        $this->_action_user_serviceoption_add($add_form);
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "user_service_option",
            "keyword"            => $add_form["user_key"],
            "info"               => serialize($add_form),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        header( "Location: ?action_individual" );
        exit;
    }

    public function _action_user_serviceoption_add($add_form) {
        $user_key = $add_form["user_key"];
        require_once( "classes/dbi/user_service_option.dbi.php" );
        $objUserServiceOption = new UserServiceOptionTable( $this->get_dsn() );
        if ( ! $add_form["user_service_option_starttime"] ) {
            $add_form["user_service_option_starttime"] = date("Y-m-d");
        }
        // sharing optionは一つだけ
        if( 3 == $add_form["service_option_key"] ){
            $where = sprintf( "user_key=%s AND user_service_option_status=1", $user_key );
            $optionList = $objUserServiceOption->getRowsAssoc( $where );
            for( $i = 0; $i < count( $optionList ); $i++ ){
                if( 3 == $optionList[$i]["service_option_key"] ){
                    return false;
                }
            }
        }
        $today = date("Y-m-d 23:59:59");
        $add_form["user_service_option_status"] = ( strtotime( $add_form["user_service_option_starttime"] ) >= strtotime( $today ) ) ? 2 : 1;
        $add_form["user_service_option_registtime"] = date("Y-m-d H:i:s");
        $result = $objUserServiceOption->add( $add_form );
        // 属しているメンバーのオプションにも追加
        $objMember = new N2MY_DB($this->get_dsn(), "member");
        $objOrderedServiceOption = new N2MY_DB( $this->get_dsn(), "ordered_service_option" );
        $where = sprintf("user_key=%s", $user_key);
        $memberList = $objMember->getRowsAssoc($where);
        $data = array(
            "user_service_option_key"           => $result,
            "service_option_key"                => $add_form["service_option_key"],
            "ordered_service_option_status"     => $add_form["user_service_option_status"],
            "ordered_service_option_registtime" => $add_form["user_service_option_registtime"],
            "ordered_service_option_starttime"  => $add_form["user_service_option_starttime"]
            );
        for($i = 0; $i < count( $memberList ); $i++){
            $data["room_key"] = $memberList[$i]["room_key"];
            $this->logger2->info($data["room_key"]);
            $result = $objOrderedServiceOption->add( $data );
        }
    }

    //オプション削除(member課金)
    function action_user_serviceoption_delete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_individual();
            exit;
        }
        require_once("classes/dbi/user_service_option.dbi.php");
        $objUserServiceOption = new UserServiceOptionTable($this->get_dsn());
        $request = $this->request->getAll();
        $where = sprintf( "user_key=%s AND user_service_option_key=%s", $request["user_key"], $request["user_service_option_key"] );
        $data = array(
            "user_service_option_status" => 0,
            "user_service_option_deletetime" => date("Y-m-d H:i:s"),
        );
        $objUserServiceOption->update( $data, $where );

        /** 属しているメンバーのオプションにも削除**/
        $objMember = new N2MY_DB( $this->get_dsn(), "member" );
        $objOrderedServiceOption = new N2MY_DB( $this->get_dsn(), "ordered_service_option" );
        $where = sprintf( "user_key=%s", $request["user_key"] );
        $memberList = $objMember->getRowsAssoc( $where );
        for( $i = 0; $i < count( $memberList ); $i++ ){
            $where = sprintf( "room_key='%s' AND user_service_option_key='%s'", $memberList[$i]["room_key"], $request["user_service_option_key"] );
            $data = array(
                        "ordered_service_option_status"        => 0,
                        "ordered_service_option_deletetime"    => date("Y-m-d H:i:s")
                        );
            $result = $objOrderedServiceOption->update( $data, $where );
            if( DB::isError( $result ) ) {
                $this->logger2->error( $result->getUserInfo() );
            }
        }

        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "user_service_option",
            "keyword"            => $request["user_key"],
            "info"               => serialize( $data ),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        header( "Location: ?action_individual" );
        exit;
    }

    public function action_intra_fms($message=null)
    {
        $this->set_submit_key($this->_name_space);
        $user_key = $this->request->get("user_key");
        if (!$user_key) {
            $user_key = $this->session->get("user_key");
        }
        if ($message) {
            $this->template->assign("message",$message);
            $user_key = $this->session->get("user_key");
        }
        $this->session->set("user_key",$user_key);
        $this->template->assign("user_key",$user_key);
        $sort_key = $this->request->get("sort_key");
        if (!$sort_key) {
            $sort_key = "fms_key";
        }
        $sort_type = $this->request->get("sort_type");
        if(!$sort_type) {
            $sort_type = "asc";
        }

        $data_db = new N2MY_DB($this->get_dsn(), "user_fms_server");
        $action_name = __FUNCTION__;
        $columns = $data_db->getTableInfo("data");
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $sort_key, $sort_type);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where = "user_key='".$user_key."'";
        // データ取得
        $rows = $data_db->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $total_count = $data_db->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name)
            );
        $add_form = $this->session->get("temp", $action_name);
        $this->template->assign("add_form", array(
            "data" => $add_form,
            "ok_action" => $action_name,
            "ng_action" => $action_name
          ));
        $this->template->assign("rows",$rows);
        $this->template->assign("columns",$columns);
        $template = "sastik_admin/user/intra_fms.t.html";
        $this->_display($template);
    }

    public function action_add_intra_fms()
    {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_intra_fms();
            exit;
        }
        $form_data = $this->request->get("add_form");
        if (!$form_data["server_address"]) {
            $message .= "server_addressを入力してください。<br />";
        }
        if ( !$form_data["server_port"] ) {
            $message .= "server_portを入力してください。<br />";
        }
        if ( $message ) {
            $this->action_intra_fms($message);
            exit;
        }
        $user_key = $this->request->get("user_key");
        $user_fms_db = new N2MY_DB($this->get_dsn(), "user_fms_server");
        //登録
        $intra_fms_data = array (
            "user_key" => $user_key,
            "server_address" => $form_data["server_address"],
            "server_port"  => $form_data["server_port"],
            "is_available" => "1",
            "registtime" => date("Y-m-d H:i:s"),
            );
        $add_fms = $user_fms_db->add($intra_fms_data);
        if (DB::isError($add_fms)) {
            $this->logger2->info($add_fms->getUserInfo());
        }
        //操作ログ登録
        $operation_data = array (
                "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => "user_fms_server",
                "keyword"            => $add_fms,
                "info"               => serialize($add_fms),
                "operation_datetime" => date("Y-m-d H:i:s"),
            );
            $this->add_operation_log($operation_data);
        header("Location: index.php?action_intra_fms");
    }

    /**
     * 修正画面表示
     */
    function action_modify() {
        $required = $this->session->get("required");
        if ($required) {
            $message = $this->session->get("message");
            $this->template->assign("message",$message);
            $this->template->assign("required",$required);
        }

        $where_data["type"] = $this->request->get("type");
        $where_data["table"] = $this->request->get("table");
        $where_data["uptime"] = $this->request->get("uptime");

        if ($where_data["type"] == "account") {
            $dsn = $this->account_dsn;
        } else {
            $dsn = $this->get_dsn();
        }

        $data_db = new N2MY_DB($dsn, $where_data["table"]);
        $action_name = __FUNCTION__;
        $columns = $data_db->getTableInfo($where_data["type"]);
        $columns = $this->_get_relation_data($dsn, $columns);

        //すでに登録されていないかチェック
        $where_data["key"] = $this->request->get("key");
        $where_data["value"] = $this->request->get("value");
        if (!$where_data["key"] || !$where_data["value"]) {
            $where_data = $this->session->get("where_data");
            $modify_form = $this->session->get("temp", $action_name);
        } else {
            $this->session->set("where_data", $where_data);
            $where = $where_data["key"]."='".$where_data["value"]."'";
            $modify_form = $data_db->getRow($where);
            if ($where_data["table"] == "staff") {
                $modify_form["login_password"] = "";
            }
            require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
            if ($where_data["table"] == "user") {
                $modify_form["user_password"] = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $modify_form["user_password"]);
                $modify_form["user_admin_password"] = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $modify_form["user_admin_password"]);
                $where_agency = "user_id = ".addslashes($modify_form["user_key"]);
                $agency_rel_db = new N2MY_DB($dsn, "agency_relation_user");
                $agency_data = $agency_rel_db->getRow($where);
                $modify_form["agency"] = $agency_data["agency_id"];
            }
            if ($where_data["table"] == "member") {
                $modify_form["member_pass"] = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $modify_form["member_pass"]);
            }
        }
        $after_action = $this->request->get("after_action");
        if (!$after_action) {
            $after_action = "action_list";
        }
        $uptime = $this->request->get("uptime");
        $this->session->set("uptime", $uptime);
        $this->template->assign("action",$action_name);
        $this->template->assign("after_action",$after_action);
        $this->template->assign("columns",$columns);
        $this->template->assign("modify_form",$modify_form);
        $this->template->assign("where_data",$where_data);
        $this->session->remove("message");
        $template = "sastik_admin/include/modify.t.html";
        $this->_display($template);
    }

     /**
     * 修正内容を登録
     */
    function action_modify_complete() {
        $this->session->remove("message");
        $this->session->remove("required");

        if (!$type = $this->request->get("type")) {
            $type = $this->session->get("type");
        }
        if (!$table = $this->request->get("table")) {
            $table = $this->session->get("table");
        }
        if ($type == "account") {
            $dsn = $this->account_dsn;
        } else {
            $dsn = $this->get_dsn();
        }
        $this->logger->trace("modify_form",__FILE__,__LINE__,$dsn."#".$type);
        $data_db = new N2MY_DB($dsn, $table);
        $after_action = $this->request->get("after_action");
        $temp_name = $this->request->get("temp");
        $form_data = $this->request->get("modify_form");
        $uptime = $this->session->get("uptime");
        if ($uptime) {
            $form_data[$uptime] = date("Y-m-d H:i:s");
        }
        $this->session->set("temp", $form_data, $temp_name);
        $columns = $data_db->getTableInfo($type);
        $rules = $this->getRule($columns);
        $err_obj = $data_db->check($form_data, $rules);
        $error_action = $this->request->get("error_action");
        if (EZValidator::isError($err_obj)) {
            $this->logger2->info($err_obj);
            $err_fields = $err_obj->error_fields();
            $message["required"] = "入力されていません";
            $this->logger->info("error",__FILE__,__LINE__,$err_fields);
            $required = array();
            foreach ($err_fields as $value) {
                $required[$value] = "1";
            }
            $this->logger->debug("message",__FILE__,__LINE__,$message);
            $this->logger->info("required",__FILE__,__LINE__,$required);
            if ($required) {
                $this->session->set("message",$message);
                $this->session->set("required",$required);
            }
            $this->logger->debug("error_action",__FILE__,__LINE__,$error_action);
            header("Location: index.php?".$error_action);
        } else {
            $where_data["key"] = $this->request->get("key");
            $where_data["value"] = $this->request->get("value");
            if (!$where_data["key"] || !$where_data["value"]) {
                $where_data = $this->session->get("where_data");
            } else {
                $where = $where_data["key"]."='".$where_data["value"]."'";
            }
            if ($table == "staff" && $form_data["login_password"]) {
                $form_data["login_password"] = sha1($form_data["login_password"]);
            } else if ($table == "staff" && !$form_data["login_password"]) {
                $staff_db = new N2MY_DB($this->account_dsn, "staff");
                $staff_data = $staff_db->getRow($where);
                $form_data["login_password"] = $staff_data["login_password"];
            }
            require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
            if ($table == "user") {
                $where_agency = "user_id = '".htmlspecialchars($form_data["user_id"])."'";
                $user_db = new N2MY_DB($dsn, "user");
                $user_data = $user_db->getRow($where, "user_key");

                $where_agency = "user_id = ".addslashes($user_data["user_key"]);
                $agency_rel_db = new N2MY_DB($dsn, "agency_relation_user");
                $agency_data = $agency_rel_db->getRow($where);
                if ($form_data["agency"]) {
                    if ($agency_data["agency_id"] != $form_data["agency"]) {
                        $where_remove = "user_key = ".addslashes($user_data["user_key"]);
                        $ret = $agency_rel_db->remove($where);
                        if (DB::isError($ret)) {
                            $this->logger2->error($ret->getUserInfo());
                            return false;
                        } else {
                            $data = array("user_key" => $user_data["user_key"],
                                          "agency_id" => $form_data["agency"]);
                            $ret = $agency_rel_db->add($data, $where);
                            if (DB::isError($ret)) {
                                $this->logger2->error($ret->getUserInfo());
                                return false;
                            }
                        }
                    }  else {
                        $data = array("user_key" => $user_data["user_key"],
                                      "agency_id" => $form_data["agency"]);
                        $ret = $agency_rel_db->update($data, $where);
                        if (DB::isError($ret)) {
                            $this->logger2->error($ret->getUserInfo());
                            return false;
                        }
                    }
                } else if (!$form_data["agency"] && $agency_data) {
                    $where_remove = "user_key = ".addslashes($user_data["user_key"]);
                    $ret = $agency_rel_db->remove($where);
                    if (DB::isError($ret)) {
                        $this->logger2->error($ret->getUserInfo());
                        return false;
                    }
                }
                unset($form_data["agency"]);
                $form_data["user_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $form_data["user_password"]);
                $form_data["user_admin_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $form_data["user_admin_password"]);
            }
            if ($table == "member") {
                //認証サーバーのアップデート
                $member_db = new N2MY_DB($dsn, "member");
                //現状のデータを取得
                $now_member_info = $member_db->getRow($where);
                $this->logger2->info($now_member_info,"now_member_info");
                require_once ('classes/mgm/dbi/user.dbi.php');
                $obj_MgmUserTable = new MgmUserTable( $this->account_dsn );
                $auth_data = array(
                    "user_id"     => $form_data['member_id']
                    );
                $this->logger2->info($auth_data);
                $result = $obj_MgmUserTable->update( $auth_data, sprintf( "user_id='%s'", mysql_real_escape_string($now_member_info['member_id']) ) );
                $form_data["member_pass"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $form_data["member_pass"]);
            }
            if($type == "account") {
                if(preg_match('/^sastik_account_/', $table)) {
	                //決め打ち
	                if($table == "sastik_account_host") {
                        $check_items = array("user_id", "account_host");
                        $check_key = "account_host_key";
                        $check = $check_items[0];
                	}
                    elseif($table == "sastik_account_translation") {
                        $check_items = array("account_host_key", "account_id");
                        $check_key = "account_translation_key";
                        $check = $check_items[0];
                    }
                    $where_cond = array();
                    foreach($check_items as $ci) {
                        $where_cond[] = $ci . " = '" . addslashes($form_data[$ci]) . "'";
                    }
                    if($check_key !== $where_data["key"]) {
                        $this->logger2->error("'" . $where_data["key"] . "' has been passed but we expect '" . $check_key . "'.");
                        return false;
                    }
                    
                    $where_cond[] = $where_data["key"] . " != '" . addslashes($where_data["value"]) . "'";
                    $where_cond = implode(" AND ", $where_cond);
                    
                    $count = $data_db->numRows($where_cond);
                    if (is_numeric($count) && $count > 0) {
                        $this->logger->debug("error_action",__FILE__,__LINE__,$error_action);
                        header("Location: index.php?".$error_action);
                        return false;
                    }
                }
            }
            $data_db->update($form_data, $where);
            //操作ログ登録
            $operation_data = array (
                "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => "$table",
                "keyword"            => "",
                "info"               => serialize($form_data),
                "operation_datetime" => date("Y-m-d H:i:s"),
            );
            $this->add_operation_log($operation_data);
            $this->session->remove("temp", $temp_name);
            $this->session->remove("where_data");
            $this->session->remove("valid_required");
            $this->session->remove("uptime");
            header("Location: index.php?".$after_action);
        }
    }

    function action_staff_modify() {
        $this->request->set("type", "account");
        $this->request->set("table", "staff");
        $this->request->set("key", "staff_key");
        $this->request->set("value", $_SESSION[_authsession]["data"]["staff_key"]);
        $this->request->set("uptime", "update_datetime");
        $this->request->set("after_action", "action_staff_modify");
        $this->request->set("error_action", "action_modify");
        $this->template->assign("hide_back",1);
        $hide_columns["authority"] = "1";
        $this->template->assign("hide_columns", $hide_columns);
        $this->action_modify();
    }

     /**
     * データセンター優先順位更新
     */
    function action_modify_datacenter_priority() {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $this->session->remove("message");
        $dsn       = $this->get_dsn();
        $form_data = $this->request->getAll();
        $this->logger2->trace($_SESSION);
        $message   = null;
        $required  = null;
        $user_key  = $form_data["user_key"];
        $error     = false;
        $table     = "datacenter_priority";

        // 列情報取得
        $form_add_row            = ($form_data["form_add_row"]) ? $form_data["form_add_row"] : null;
        $add_row                 = ($form_data["add_row"]) ? $form_data["add_row"] : array();
        $user_key                = ($form_data["user_key"]) ? $form_data["user_key"] : array();
        $sort                    = ($form_data["sort"]) ? $form_data["sort"] : array();
        $datacenter_key          = ($form_data["datacenter_key"])     ? $form_data["datacenter_key"]     : array();
        $country                 = ($form_data["country"])   ? $form_data["country"]   : array();
        $datacenter_priority_key = ($form_data["datacenter_priority_key"]) ? $form_data["datacenter_priority_key"]     : array();
        $create_datetime         = ($form_data["create_datetime"])  ? $form_data["create_datetime"]  : array();
        $priority_data           = array();

        if (!empty($form_data)) {
            foreach($form_data["sort"] as $key => $columns) {
                if ($add_row[$key]) {
                    $add_datacenter_key = $datacenter_key[$key];

                    require_once("classes/dbi/datacenter_ignore.dbi.php");
                    $datacenter_ignore_db = new DatacenterIgnoreTable($dsn);
                    $where = "user_key = '".addslashes($user_key)."'".
                             " AND datacenter_key = '".addslashes($add_datacenter_key)."'";
                    $this->logger->trace("where",__FILE__,__LINE__,$where);
                    $total_count = $datacenter_ignore_db->numRows($where);
                    if (0 < $total_count) {
                        $message["required"] = "除外DCにデータセンターが設定されています。";
                        $required["datacenter_key"] = "1";
                    }
                }
                $priority_data[$key]["add_row"] = $add_row[$key];
                $priority_data[$key]["sort"] = $sort[$key];
                $priority_data[$key]["datacenter_key"] = $datacenter_key[$key];
                $priority_data[$key]["country"] = $country[$key];
                $priority_data[$key]["datacenter_priority_key"] = $datacenter_priority_key[$key];
                $priority_data[$key]["create_datetime"] = $create_datetime[$key];
            }
        }
        $before_data = $this->session->get("before_data");
        foreach ($before_data as $columns) {
            $exist_key = false;
            foreach ($columns as $column_key => $column_value) {
                if ($column_key == "datacenter_priority_key") {
                    $before_datacenter_priority_key[] = $column_key;
                    if (empty($priority_data)) {
                        $delete_priority_key[] = $column_value;
                        break;
                    }
                    foreach ($priority_data as $key => $value) {
                        if ($priority_data[$key]["datacenter_priority_key"] == $column_value) {
                            $exist_key = true;
                        }
                    }
                    if (!$exist_key) {
                        $delete_priority_key[] = $column_value;
                        break;
                    }
                }
            }
        }
        if ($form_add_row) {
            $datacenter_key = $form_data["datacenter_key"];
            $datacenter_db  = new N2MY_DB($this->account_dsn, "datacenter");
            foreach ($datacenter_key as $key => $column) {
                if ((string)$key != "add") {
                    $before_add_datacenter_key[$column] = $column;
                }
            }
            if (!empty($before_add_datacenter_key)) {
                $where = " datacenter_key NOT IN (". implode(",",$before_add_datacenter_key) .")";
                if (!$datacenter_db->numRows($where)) {
                    $message["required"] = "全てのデータセンターが設定されています。";
                }
            }
            if (empty($message)) {
                foreach ($datacenter_key as $key => $column) {
                    if ($column == $add_datacenter_key &
                        !$priority_data[$key]["add_row"]) {
                        $message["required"] = "既にデータセンターが設定されています。";
                        break;
                    }
                }
            }
        }
        if (!empty($message)) {
            $required["datacenter_key"] = "1";
            $this->session->set("message",$message);
            $this->session->set("required",$required);

            $this->template->assign(array("user_key" => $user_key,
                                          "datacenter_key"  => $form_data["datacenter_key"],
                                          "country"  => $form_data["country"],
                                          "fragment1" => true));
            $this->action_datacenter_priority_sort($priority_data, $user_key, $form_add_row);
            exit;
        } else {
            require_once("classes/dbi/datacenter_priority.dbi.php");
            $datacenter_priority_db = new DatacenterPriorityTable($dsn);
            foreach ($delete_priority_key as $key => $value) {
                $where = "datacenter_priority_key = '".addslashes($value). "'";
                $datacenter_priority_db->remove($where);
                //操作ログ登録
                $operation_data = array (
                    "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                    "action_name"        => __FUNCTION__,
                    "table_name"         => $table,
                    "keyword"            => "delete data",
                    "info"               => serialize($form_data),
                    "operation_datetime" => date("Y-m-d H:i:s"),
                );
                $this->add_operation_log($operation_data);
            }
            $sort_index = 1;
            foreach ($priority_data as $data_key => $columns) {
                foreach ($columns as $column_key => $column) {
                    if ($column_key == "sort") {
                        $priority_data[$data_key]["sort"] = (string)$sort_index;
                        $sort_index++;
                        break;
                    }
                }
            }
            foreach ($priority_data as $key => $columns) {
                if ( $priority_data[$key]["add_row"]) {
                    $add_data = array(
                        "user_key" => $user_key,
                        "country"  => $priority_data[$key]["country"],
                        "datacenter_key" => $priority_data[$key]["datacenter_key"],
                        "sort" => $priority_data[$key]["sort"],
                        "create_datetime" => date("Y-m-d H:i:s"),
                    );
                    $datacenter_priority_db->add($add_data);
                    $keyword = "add data";
                    $this->logger->trace("add_data",__FILE__,__LINE__,$add_data);
                } else {
                    $modify_data = array(
                        "user_key"       => $user_key,
                        "country"        => $priority_data[$key]["country"],
                        "datacenter_key" => $priority_data[$key]["datacenter_key"],
                        "sort"           => $priority_data[$key]["sort"],
                    );
                    $where   = "datacenter_priority_key = '".addslashes($priority_data[$key]["datacenter_priority_key"])."'";
                    $datacenter_priority_db->update($modify_data, $where);
                    $keyword = "update data";
                    $this->logger->trace("modify_data",__FILE__,__LINE__,$modify_data);
                }
                //操作ログ登録
                $operation_data = array (
                    "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                    "action_name"        => __FUNCTION__,
                    "table_name"         => $table,
                    "keyword"            => $keyword,
                    "info"               => serialize($form_data),
                    "operation_datetime" => date("Y-m-d H:i:s"),
                );
                $this->add_operation_log($operation_data);
            }
            $this->session->remove("before_data");

            $this->action_datacenter_priority_sort();
        }
    }

     /**
     * 停止
     */
    function action_delete() {
        $type = $this->session->get("type");
        if ($type == "account") {
            $dsn = $this->account_dsn;
        } else {
            $dsn = $this->get_dsn();
        }
        $table = $this->session->get("table");
        $column_key   = $this->request->get("key");
        $column_value = $this->request->get("value");
        $status_key   = $this->request->get("status_key");
        $data_db = new N2MY_DB($dsn, $table);
        $columns = $data_db->getTableInfo($type);
        foreach($columns as $colum_key => $colum){
            $is_update = strpos($colum_key, "update");
            $is_time   = strpos($colum_key, "time");
            if (($is_update !== false) && ($is_time !== false)) {
                $updatetime = $colum_key;
            };
        };
        $where = $column_key." = '".addslashes($column_value)."'";
        $data[$status_key] = 0;
        if ($updatetime) {
            $data[$updatetime] == date("Y-m-d H:i:s");
        }
        $data_db->update($data, $where);

        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "$table",
            "keyword"            => "",
            "info"               => serialize($where),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return $this->action_list();
    }

     /**
     * DBサーバーを停止
     */
    function action_delete_db_server() {
        $server_key = $this->request->get("key");
        $account_db = new N2MY_DB($this->account_dsn, "db_server");
        $columns = $account_db->getTableInfo("account");
        $where = "server_key = ".addslashes($server_key);
        $data = array(
            "server_status" => 0,
            "server_updatetime" => date("Y-m-d H:i:s"),
        );
        $account_db->update($data, $where);
        //server_listを更新
        $rows = $account_db->getRowsAssoc("server_status = 1");
        $str = '[SERVER_LIST]'."\n";
        foreach($rows as $row) {
            $str .= $row["host_name"]. ' = "'.$row["dsn"].'"'."\n";
        }
        $this->logger->info("server_list",__FILE__,__LINE__,$str);
        file_put_contents(N2MY_APP_DIR."config/server_list.ini", $str);
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "db_server",
            "keyword"            => "server_key",
            "info"               => $server_key,
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return $this->action_list();
    }

     /**
     * ユーザーを停止
     */
    function action_delete_user() {
        $user_key = $this->request->get("value");
        $data_db = new N2MY_DB($this->get_dsn(), "user");
        $where = "user_key = ".addslashes($user_key);
        $data = array(
            "user_delete_status" => 2,
            "user_deletetime" => date("Y-m-d H:i:s"),
        );
        $data_db->update($data, $where);
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "user",
            "keyword"            => "user_key",
            "info"               => $user_key,
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return $this->action_list();
    }

    /*
     * 部屋の停止
     */
    function action_delete_room () {
        //部屋のステータス変更
        $room_key = $this->request->get("room_key");
        $room_db = new N2MY_DB($this->get_dsn(), "room");
        $where = "room_key = '".addslashes($room_key)."'";
        $room_data = array (
            "room_status" => 0,
            "room_deletetime" => date("Y-m-d H:i:s"),
        );
        $room_db->update($room_data, $where);
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "user",
            "keyword"            => $room_key,
            "info"               => serialize($room_data),
            "operation_datetime" => date("Y-m-d H:i:s")
        );
        $this->add_operation_log($operation_data);

        //プランステータス変更
        $plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
        $plan_data = array (
            "room_plan_status" => 0,
            "room_plan_updatetime" => date("Y-m-d H:i:s"),
        );
        $plan_db->update($plan_data, $where);

        //アカウントDB リレーションテーブルステータス変更
        $relation_db = new N2MY_DB($this->account_dsn, "relation");
        $where = "relation_key = '".addslashes($room_key)."'";
        $relation_data = array (
            "status" => 0,
        );
        $relation_db->update($relation_data, $where);

        //VCUBE IDのserviceリレーション
        $wsdl = $this->config->get('VCUBEID','wsdl');
        if ($wsdl) {
        }

        return $this->action_list();
    }

    /*
     * 部屋の停止・停止予約設定
     */
    function action_room_expire ($message = "") {
        $this->template->assign("room_key", $this->request->get("room_key"));
        $room_expire_date_time = $this->request->get("room_expire_date_time");
        if (strtotime(date("0000-00-00 00:00:00")) < strtotime($room_expire_date_time)) {
             $this->template->assign("option_date_default", date("Y/m/d H:i:s", strtotime($room_expire_date_time)));
        } else {
             if ($room_expire_date_time = "0000-00-00 00:00:00") {
                 $room_expire_date_time = date("Y/m/d 23:59:59");
             }
             $this->template->assign("option_date_default", $room_expire_date_time);
        }
        $this->template->assign("message", $message);
        $this->display("sastik_admin/room/expire.t.html");
    }

    /*
     * 部屋の停止予約設定
     */
    function action_reservation_room_expire () {
        $room_key = $this->request->get("room_key");
        $room_expire_date_time = $this->request->get("room_expire_date_time");
        $this->logger->trace("expire_date_time",__FILE__,__LINE__,$room_expire_date_time);
        $today = date("Y/m/d H:i:s");
        if (strtotime($room_expire_date_time) < strtotime($today)) {
             $this->action_room_expire('停止予定日は、'.$today.'以降の日にちを設定する必要があります');
             exit;
        }
        $room_db = new N2MY_DB($this->get_dsn(), "room");
        $where = "room_key = '".addslashes($room_key)."'";
        $room_data = array (
            "room_expiredatetime" => $room_expire_date_time,
        );
        $room_db->update($room_data, $where);
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "user",
            "keyword"            => $room_key,
            "info"               => serialize($room_data),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return $this->action_list();
    }


     /**
     * 優先・除外DC停止
     */
    function action_delete_datacenter() {
        $after_action = $this->request->get("after_action");
        $error_action = $this->request->get("error_action");
        if ($after_action) {
            $error_action = $error_action."&after_action=".$after_action;
        }
        $type         = $this->session->get("type");
        $dsn          = $this->get_dsn();
        $table        = $this->session->get("table");
        $column_key   = $this->request->get("key");
        $column_value = $this->request->get("value");
        $table = $this->session->get("table");
        if (!$table) {
            $table = $this->request->get("table");
        }
        require_once("classes/dbi/". $table .".dbi.php");
        if ($table == "datacenter_priority") {
            $datacenter_db = new DatacenterPriorityTable($dsn);
        } else {
            $datacenter_db = new DatacenterIgnoreTable($dsn);
        }
        $where = $column_key." = '".addslashes($column_value)."'";
        $datacenter_db->remove($where);
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "$table",
            "keyword"            => $column_key,
            "info"               => $column_key,
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);

        $this->action_datacenter_list();
    }

    /*
     * 操作ログ登録
     */
    function add_operation_log($operation_data) {
        $this->logger->debug("operation_log",__FILE__,__LINE__,$operation_data);
        $operation_db = new N2MY_DB($this->account_dsn, "operation_log");
        $operation_data["create_datetime"] = date("Y-m-d H:i:s");
        $add_data = $operation_db->add($operation_data);
        if (DB::isError($add_data)) {
            $this->logger2->error($add_data->getUserInfo());
            return false;
        }
        return true;
    }

     /**
     * 会議強制終了
     */
    function action_meeting_stop() {
        $room_key = $this->request->get("room_key");
        $meeting_ticket = $this->request->get("meeting_ticket");
        require_once ( "classes/core/Core_Meeting.class.php" );
        $obj_CoreMeeting = new Core_Meeting( $this->get_dsn() );
        $meeting_db = new N2MY_DB($this->get_dsn(), "meeting");
        $where = "meeting_ticket = '".addslashes($meeting_ticket)."'";
        $meeting_info = $meeting_db->getRow($where);
        $this->logger2->info($meeting_info);
        // 予約があれば予約も終了
        if ($meeting_info["is_reserved"]) {
            require_once ("classes/dbi/reservation.dbi.php");
            $reservation_obj = new ReservationTable($this->get_dsn());
            $reservation_info = $reservation_obj->getRow("meeting_key = '".addslashes($meeting_ticket)."'");
            $reservation_obj->cancel($reservation_info["reservation_session"]);
        }
        $obj_CoreMeeting->stopMeeting($meeting_info["meeting_key"]);
        // 操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "user",
            "keyword"            => $room_key,
            "info"               => $meeting_ticket,
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        return $this->action_list();
    }

    /**
     * DB状況
     */
    function action_db_status() {
        require_once("classes/dbi/user.dbi.php");
        $user = new UserTable($this->get_dsn());
        $db_status_rs = $user->_conn->query("SHOW GLOBAL STATUS");
        while ($db_status_row = $db_status_rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $val_name = $db_status_row["Variable_name"];
            $section = substr($val_name, 0, strpos($val_name, "_"));
            $name = substr($val_name, (strpos($val_name, "_") + 1));
            $rows[$section][$name] = $db_status_row["Value"];
        }
        $this->template->assign("rows", $rows);
        $this->_display("sastik_admin/db/index.t.html");
    }

    /*
     * HDD容量取得
     */
    function action_user_hdd() {
        require_once("classes/dbi/user.dbi.php");
        // 部屋一覧
        $user_obj = new UserTable($this->get_dsn());
        $option_db = new N2MY_DB($this->get_dsn(), "ordered_service_option");
        $sql = "SELECT user.user_id" .
                ", user_company_name" .
                ", room.room_key" .
                " FROM user, room" .
                " WHERE user.user_key = room.user_key" .
                " GROUP BY room.room_key" .
                " ORDER BY user_id, room_key";
        $rs = $user_obj->_conn->query($sql);
        while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $user_id = $row["user_id"];
            $room_key = $row["room_key"];
            $room_data[$user_id][$room_key] = $row;
            $where = "room_key = '".addslashes($room_key)."'".
                    "AND service_option_key = '3'".
                    "AND ordered_service_option_status = '1'";
            $total_count = $option_db->numRows($where);
            $this->logger2->trace($total_count);
            $hdd_ext[$room_key] = $total_count;

        }
        // 容量取得
        $meeting_db = new N2MY_DB($this->get_dsn(), "meeting");
        $sql = "SELECT room_key, sum(meeting_size_used) as hdd_use_size" .
                " FROM `meeting`" .
                " GROUP BY room_key";
        $rs = $meeting_db->_conn->query($sql);
        while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $room_key = $row["room_key"];
            $meeting_hdd[$room_key] = $row["hdd_use_size"];
        }
        foreach($room_data as $user_id => $user) {
            foreach($user as $room_key => $room) {
                $max_size = (1024 * 1024 * 500) + (1024 * 1024 * 1024 * (($hdd_ext[$room_key]) ? $hdd_ext[$room_key] : 0));
                $uze_size = isset($meeting_hdd[$room_key]) ? $meeting_hdd[$room_key] : "0";
                $room_data[$user_id][$room_key]["max_size"] = number_format($max_size);
                $room_data[$user_id][$room_key]["uze_size"] = number_format($uze_size);
                $room_data[$user_id][$room_key]["use_percent"] = ($uze_size / $max_size * 100);
            }
        }
        $this->template->assign("room_data", $room_data);
        $this->_display("sastik_admin/total/use_hdd_size.t.html");
    }

    /*
     * 利用統計（ユーザー毎）
     */
    function action_user_total () {
        //アカウント管理のユーザーから来た場合はserverをセッションに登録
        $server_key = $this->request->get("server_key");
        if ($server_key) {
            require_once("classes/mgm/MGM_Auth.class.php");
            $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
            $server_info = $obj_MGMClass->getServerInfo($server_key);
        }
        if (isset($server_info["dsn"])) {
            $this->set_dsn($server_info["dsn"]);
            $this->session->set("now_server_key", $server_key);
        }

        $user_id = $this->request->get("user_id");
        $start = $this->request->get("start", date("Y-m-01 00:00:00"));
        $end = $this->request->get("end", date("Y-m-t 23:59:59"));
        $user_obj = new N2MY_DB($this->get_dsn(), "user");
        $where = "user_id ='".addslashes($user_id)."'";
        $user_data = $user_obj->getRow($where);

        if (isset($user_id) && isset($user_data)) {
            //部屋の情報を取得
            $room_obj = new N2MY_DB($this->get_dsn(), "room");
            $where = "user_key ='".addslashes($user_data["user_key"])."'";
            $rooms = $room_obj->getRowsAssoc($where, null, null, null, "room_key, room_name");
            $use_data = array();
            $total["use_time"] = "";
            $total["count"] = "";
            foreach ($rooms as $room) {
                $this->logger->debug("room_key",__FILE__,__LINE__,$room["room_key"]);
                //部屋毎の会議データを取得
                $meeting_obj = new N2MY_DB($this->get_dsn(), "meeting");
                $where = "room_key ='".$room["room_key"]."'".
                         " AND create_datetime >= '".addslashes($start)."'".
                         " AND create_datetime <= '".addslashes($end)."'".
                         " AND use_flg = '1'";
                $meeting_data = $meeting_obj->getRowsAssoc($where);
                $use_data[$room["room_key"]]["room_name"] = $room["room_name"];
                $use_data[$room["room_key"]]["use_time"] = "0";
                foreach ($meeting_data as $data) {
                    $use_data[$room["room_key"]]["use_time"] += $data["meeting_use_minute"];
                }
                $use_data[$room["room_key"]]["use_count"] = $meeting_obj->numRows($where);
                //全部屋のトータル
                $total["use_time"] += $use_data[$room["room_key"]]["use_time"];
                $total["count"] += $use_data[$room["room_key"]]["use_count"];
            }
            $this->template->assign("use_data", $use_data);
            $this->template->assign("total", $total);
        }
        $this->template->assign("user_id", $user_id);
        $this->template->assign("start", $start);
        $this->template->assign("end", $end);
        $this->_display("sastik_admin/total/room_log.t.html");
    }

    function _total_form() {
        // 年
        for($year = 2006; $year <= date("Y"); $year++ ) {
            $year_list[] = $year;
        }
        // 月
        for($month = 1; $month <= 12; $month++ ) {
            $month_list[] = $month;
        }
        $this->template->assign("year_list", $year_list);
        $this->template->assign("month_list", $month_list);
    }

    public function action_total_user() {
        require("classes/dbi/user.dbi.php");
        $objUser = new UserTable($this->get_dsn());
        $columns = $objUser->getTableInfo("data");
        // いらない検索フォーム取り除く
        unset($columns["user_key"]);
        unset($columns["country_key"]);
        unset($columns["user_password"]);
        unset($columns["user_admin_password"]);
        unset($columns["user_company_address"]);
        unset($columns["user_company_address"]);
        unset($columns["user_company_phone"]);
        unset($columns["user_company_fax"]);
        unset($columns["user_staff_department"]);
        unset($columns["user_staff_firstname"]);
        unset($columns["user_staff_lastname"]);
        unset($columns["max_member_count"]);
        unset($columns["staff_key"]);
        unset($columns["user_staff_email"]);
        unset($columns["user_company_postnumber"]);
        unset($columns["user_registtime"]);
        unset($columns["user_updatetime"]);
        unset($columns["addition"]);
        unset($columns["max_rec_size"]);
//        unset($columns["user_starttime"]);
//        unset($columns["user_deletetime"]);

        $this->template->assign("columns", $columns);
        // リセット
        $reset = $this->request->get("reset");
        if ($reset == 1) {
            $this->session->remove("temp", "action_total_user");
        }
        $request = $this->request->getAll();
        $action_name = __FUNCTION__;
        $form_data = $this->set_form($action_name, "user_key", "asc");
        // 月別の表示
        $yy = ($form_data["request"]["yy"]) ? $form_data["request"]["yy"] : date("Y");
        $mm = ($form_data["request"]["mm"]) ? $form_data["request"]["mm"] : date("m");
        $now_date = mktime(0,0,0,$mm,1,$yy);
        $prev_date = mktime(0,0,0,$mm - 1,1,$yy);
        $next_date = mktime(0,0,0,$mm + 1,1,$yy);
        $this->template->assign("now_date", $now_date);
        $this->template->assign("prev_date", $prev_date);
        $this->template->assign("next_date", $next_date);
        $this->logger2->info($form_data);
        // 値を設定
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
            $this->template->assign("request",$request);
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        // 検索条件設定・取得
        $where = $objUser->getWhere($columns, $request);
        $sql = "select user.*" .
            ", SUM(meeting.meeting_use_minute) AS meeting_use_minute" .
            " FROM user" .
            " LEFT JOIN meeting ON user.user_key = meeting.user_key" .
            " AND actual_start_datetime like '".date("Y-m",$now_date)."%'";
        if ($where) {
            $sql .= " WHERE ".$where;
        }
        $sql .= " GROUP BY meeting.user_key";
        if (strtolower($form_data['sort_type']) == "desc") {
            $sql .= " ORDER BY ".$form_data['sort_key']." DESC";
        } else {
            $sql .= " ORDER BY ".$form_data['sort_key']." ASC";
        }
        // CSVダウンロード
        if ($_REQUEST["csv"]) {
            $ret = $objUser->_conn->query($sql);
        } else {
            $ret = $objUser->_conn->limitQuery($sql, $offset, $limit);
        }
        $rows = array();
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
        } else {
            while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
                $rows[] = $row;
            }
        }
        if ($_REQUEST["csv"]) {
            require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            $csv = new EZCsv(true, "UTF-8", "UTF-8");
            $dir = $this->get_work_dir();
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");
            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="'.date("Ym").'.csv"');
            // ヘッダ２
            $header = array(
                "user_id" => "ユーザーID",
                "user_company_name" => "ユーザー名",
                "user_starttime" => "利用開始日",
                "user_deletetime" => "利用停止日",
                "user_status" => "利用状態",
                "delete_status" => "削除フラグ",
                "account_model" => "課金モデル",
                "invoice_flg" => "請求対象",
                "meeting_use_minute" => "利用時間(分)",
                );
            $csv->setHeader($header);
            $csv->write($header);
            foreach($rows as $row) {
                $csv->write($row);
            }
            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            $contents = fread($fp, filesize($tmpfile));
            fclose($fp);
            print $contents;
            unlink($tmpfile);
            return true;
        }
        // ページャー
        $total_count = $objUser->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name,
            "after_action" => $this->request->get("after_action"))
            );
        $this->template->assign("rows", $rows);
        $this->_display("sastik_admin/total/top.t.html");
    }

    /**
     * ユーザー別の利用時間集計
     */
    public function action_total_user2() {
        static $user_columns;
        $objMeetingDateLog = new N2MY_DB( N2MY_MDB_DSN, "meeting_date_log");
        $server_list = parse_ini_file( N2MY_APP_DIR."config/server_list.ini", true);
        require("classes/dbi/user.dbi.php");
        $users = array();
        foreach($server_list["SERVER_LIST"] as $key => $dsn) {
            $objUser = new UserTable( $dsn , "user");
            if (!$user_columns) {
                $user_columns = $objUser->getTableInfo("data");
            }
            $rows = $objUser->getRowsAssoc();
            foreach ($rows as $user_info) {
                $users[$user_info["user_id"]] = $user_info;
            }
        }
        $sql = "SELECT user_id" .
            ",SUM(use_time) AS use_time" .
            ",SUM(use_time_mobile_normal) AS use_time_mobile_normal" .
            ",SUM(use_time_mobile_special) AS use_time_mobile_special" .
            ",SUM(use_time_hispec) AS use_time_hispec" .
            ",SUM(use_time_audience) AS use_time_audience" .
            ",SUM(use_time_h323) AS use_time_h323" .
            ",SUM(use_time_h323ins) AS use_time_h323ins" .
            /*
            ",SUM(account_time_mobile_normal) AS account_time_mobile_normal" .
            ",SUM(account_time_mobile_special) AS account_time_mobile_special" .
            ",SUM(account_time_hispec) AS account_time_hispec" .
            ",SUM(account_time_audience) AS account_time_audience" .
            ",SUM(account_time_h323) AS account_time_h323" .
            ",SUM(account_time_h323ins) AS account_time_h323ins" .
            */
            ",SUM(meeting_cnt) AS meeting_cnt" .
            ",SUM(participant_cnt) AS participant_cnt" .
            " FROM meeting_date_log";
        $request = $this->request->getAll();
        if (isset($request["year"])) {
            $request["year"] = date("Y");
        }
        if (isset($request["month"])) {
            $request["month"] = date("m");
        }
        $cond = $request["cond"];
        $cond["log_date"] = $request["year"]."-".$request["month"];
        $columns = $objMeetingDateLog->getTableInfo("account");
        $columns["log_date"]["item"]["search"] = "like";
        $where = $objMeetingDateLog->getWhere($columns, $cond);
        if ($where) {
            $sql .= " WHERE ".$where;
        }
        $sql = $sql." GROUP BY user_id";
        $rs = $objMeetingDateLog->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
        }
        $user_logs = array();
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $row["user_info"] = $users[$row["user_id"]];
            $user_logs[] = $row;
        }
        $this->logger2->info(array($user_logs, $sql));
        unset($columns["log_no"]);
        unset($columns["log_date"]);
        unset($columns["staff_id"]);
        unset($columns["room_key"]);
        unset($columns["account_time_mobile_normal"]);
        unset($columns["account_time_mobile_special"]);
        unset($columns["account_time_hispec"]);
        unset($columns["account_time_audience"]);
        unset($columns["account_time_h323"]);
        unset($columns["account_time_h323ins"]);
        unset($columns["create_datetime"]);
        unset($columns["update_datetime"]);
        // 年
        for($year = 2006; $year <= date("Y"); $year++ ) {
            $year_list[] = $year;
        }
        // 月
        for($month = 1; $month <= 12; $month++ ) {
            $month_list[] = $month;
        }
        $this->template->assign("year_list", $year_list);
        $this->template->assign("month_list", $month_list);

        $this->template->assign("request", $request);
        $this->template->assign("user_columns", $user_columns);
        $this->template->assign("columns", $columns);
        $this->template->assign("user_logs", $user_logs);
        $this->_display("sastik_admin/total/top.t.html");
    }

    public function action_shift()
    {
        $this->_display("sastik_admin/shift.t.html");
    }

    public function action_shift2()
    {
        $request = $this->request->getAll();

        $this->session->set( "dbList", $request );
        $this->_display("sastik_admin/shift2.t.html");
    }

    function getRule($columns) {
        $rules = array();
        foreach($columns as $colum => $colum_info) {
            $this->logger->trace("not_null",__FILE__,__LINE__,$colum_info);
            if (($colum_info["flags"]["not_null"] == "1") && ($colum_info["flags"]["auto_increment"] == "")) {
                $rules[$colum]["required"] = true;
            }
        }
        $this->logger2->trace($rules);
        return $rules;
    }

    /**
     * デフォルトページ
     */
    function default_view(){
        return $this->render_main();
    }

//===================================================================
// メニュー
//===================================================================
    /**
     * メインページ（フレームでメニューとコンテンツを分ける）
     */
    function action_main() {
        return $this->render_main();
    }

    /**
     * メニュー（権限によって表示切り替えなどを行う。但し、権限はチェックは表示側でも行う）
     */
    function action_menu() {
        return $this->render_menu();
    }

    /**
     * トップページ（統計情報やイベント管理など、ログイン直後の画面）
     */
    function action_top() {
        $staff_auth = $_SESSION["_authsession"]["data"]["authority"];
        if ($staff_auth == 3) {
            return $this->action_total_user();
        }
        $this->session->remove("sort_key");
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );

        $this->request->set("table", $this->request->get("table"));
        $this->request->set("type", $this->request->get("type"));
        $this->request->set("check", $this->request->get("check"));
        $form[$this->request->get("key")] = $this->request->get("value");
        $this->request->set("form", $form);
        $this->request->set("sort", $this->request->get("key"));
        $this->request->set("reset", $this->request->get("reset"));
        return $this->action_list();
    }

    /**
     * ログアウト
     */
    function action_logout() {
        $url = $this->get_redirect_url("sastik_admin/index.php");
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => "logout",
            "table_name"         => "staff",
            "keyword"            => $_SESSION["_authsession"]["username"],
            "info"               => "logout",
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
        $this->_auth->logout();
        unset($_SESSION[$this->_sess_page]);
        header("Location: ".$url);
    }

    /**
     * Flash Version統計用（一時的なバージョンです）
     */
    function action_flash_version() {
        $objFlash = new N2MY_DB($this->get_dsn(), "flash_version");
        $columns = $objFlash->getTableInfo("data");
        $action_name = __FUNCTION__;
        $form_data = $this->set_form($action_name, "user_key", "asc");
        $this->logger2->info($form_data);
        $request = $form_data["request"];
        // 月別の表示
        $yy = ($request["yy"]) ? $request["yy"] : date("Y");
        $mm = ($request["mm"]) ? $request["mm"] : date("m");
        $now_date = mktime(0,0,0,$mm,1,$yy);
        $prev_date = mktime(0,0,0,$mm - 1,1,$yy);
        $next_date = mktime(0,0,0,$mm + 1,1,$yy);
        $this->template->assign("now_date", $now_date);
        $this->template->assign("prev_date", $prev_date);
        $this->template->assign("next_date", $next_date);
        $where = "";
        if ($request["user_id"]) {
            $where .= " AND user.user_id like '%".addslashes($request["user_id"])."%'";
        }
        if ($request["user_status"]) {
            $where .= " AND user.user_status = '".addslashes($request["user_status"])."'";
        }
        if ($request["flash_version"]) {
            foreach($request["flash_version"] as $version) {
                $_cond[] = "flash_version like '".$version."%'";
            }
            $where .= " AND (".join($_cond, " OR ").")";
        }
        //
        if ($request["create_datetime"]["min"]) {
            $where = " AND flash_version.create_datetime >= '".addslashes($request["create_datetime"]["min"])."'";
        }
        if ($request["create_datetime"]["max"]) {
            $where = " AND flash_version.create_datetime <= '".addslashes($request["create_datetime"]["max"])."'";
        }
        $this->logger2->info($where);
        // 検索条件設定・取得
        $sql = "SELECT user.user_id" .
                ", user.user_company_name" .
                ", flash_version" .
                ", count(*) as count" .
                " FROM user, flash_version" .
            " WHERE user.user_id = flash_version.user_id".
            $where.
            " GROUP BY user.user_id, flash_version";
        $this->logger2->info($sql);
        $rs = $objFlash->_conn->query($sql);
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        $this->logger2->info($rows);
        require("classes/dbi/user.dbi.php");
        $objUser = new UserTable($this->get_dsn());
        $user_columns = $objUser->getTableInfo("data");
        // 月
        for($version = 5; $version <= 10; $version++ ) {
            $version_list[$version] = $version;
        }
        $this->template->assign("page", array(
            "action" => $action_name)
            );
        $this->template->assign("version_list", $version_list);
        $this->template->assign("user_columns",$user_columns);
        $this->template->assign("rows",$rows);
        $this->display("sastik_admin/total/flash_version.t.html");
    }

//===================================================================
// 表示
//===================================================================
    function render_main() {
        // セッションにdsnを登録
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $server_info = "";
        $server_key = $this->request->get("server_key");
        $account_db = new N2MY_DB($this->account_dsn, "db_server");
        $db_server_list = $account_db->getRowsAssoc("server_status = 1");
        $this->template->assign("db_server_list", $db_server_list);
        if ($server_key) {
            $server_info = $obj_MGMClass->getServerInfo($server_key);
        } else {
            $server_key = $db_server_list[0]["server_key"];
            $server_info = $obj_MGMClass->getServerInfo($server_key);
        }
        $this->session->set("now_server_key", $server_key);
        if (isset($server_info["dsn"])) {
            $this->set_dsn($server_info["dsn"]);
        }
        $this->logger->trace("dsn",__FILE__,__LINE__,$this->get_dsn());
        $this->display("sastik_admin/index.t.html");
    }

    function render_menu() {
        $server_key = $this->session->get("now_server_key");
        $this->logger->debug("server_key",__FILE__,__LINE__,$server_key);
        $account_db = new N2MY_DB($this->account_dsn, "db_server");
        $db_server_list = $account_db->getRowsAssoc("server_status = 1");
        $this->logger2->debug($db_server_list);
        $this->logger->trace("dsn",__FILE__,__LINE__,$this->get_dsn());
        $this->template->assign("select_server_key", $server_key);
        $this->template->assign("db_server_list", $db_server_list);
        $this->_display("sastik_admin/menu.t.html");
    }

    /**
     * フォームの内容を再現
     */
    function set_form($form_name, $default_sort_key, $default_sort_type) {
        //
        // デフォルトのソート順指定
        $sort_key = $this->request->get("sort_key");
        $sort_type = $this->request->get("sort_type");
        $page = $this->request->get("page");
        $page_cnt = $this->request->get("page_cnt");
        $reset = $this->request->get("reset");
        // デフォルト
        if (!isset($_SESSION[$this->_sess_page][$form_name]) || $reset == 1 || $reset == 2) {
            $_SESSION[$this->_sess_page][$form_name]['sort_key']  = $default_sort_key;
            $_SESSION[$this->_sess_page][$form_name]['sort_type'] = $default_sort_type;
            $_SESSION[$this->_sess_page][$form_name]['page'] = 1;
            $_SESSION[$this->_sess_page][$form_name]['page_cnt'] = 20;
        }
        if ($reset == 1) {
        $this->logger->debug("session",__FILE__,__LINE__,$_SESSION);
            unset($_SESSION[$this->_sess_page][$form_name]["request"]);
        } else {
            // ソート
            if ($sort_key) {
                $_SESSION[$this->_sess_page][$form_name]['sort_key']  = $sort_key;
                $_SESSION[$this->_sess_page][$form_name]['sort_type'] = $sort_type;
            }
            // ページ
            if ($page) {
                $_SESSION[$this->_sess_page][$form_name]['page'] = $page;
            }
            // ページ
            if ($page_cnt) {
                $_SESSION[$this->_sess_page][$form_name]['page_cnt'] = $page_cnt;
            }
            $request = $this->request->get("form");
            if ($request) {
                $_SESSION[$this->_sess_page][$form_name]['page'] = 1;
                $_SESSION[$this->_sess_page][$form_name]["request"] = $request;
            }
        }
        return $_SESSION[$this->_sess_page][$form_name];
    }

    /**
     * クエリーを自動で作成
     */
    function _get_query($columns, $request, $and_or = "AND") {
        $where = "";
        $and_or = " ".$and_or." ";
        $condition = array();
        foreach($columns as $key => $item){
            $fields .= ",".$key;
            switch ($item["type"]) {
            // 数値
            case "integer":
                switch($item["search"]) {
                // 同一
                case "equal":
                    if ($request[$key]) {
                        $condition[] = $key." = ".addslashes($request[$key]);
                    }
                    break;
                // 範囲指定
                case "range":
                    if ($request[$key]["min"]) {
                        $condition[] = $key." >= ".addslashes($request[$key]["min"]);
                    }
                    if ($request[$key]["max"]) {
                        $condition[] = $key." <= ".addslashes($request[$key]["max"]);
                    }
                    break;
                }
                break;
            // 文字列
            case "string":
                if ($request[$key] !== "") {
                    switch($item["search"]) {
                    // 同一
                    case "equal":
                        $condition[] = $key." = '".addslashes($request[$key])."'";
                        break;
                    case "prefix":
                        $condition[] = $key." like '".addslashes($request[$key])."%'";
                        break;
                    case "suffix":
                        $condition[] = $key." like '%".addslashes($request[$key])."'";
                        break;
                    case "like":
                        $condition[] = $key." like '%".addslashes($request[$key])."%'";
                        break;
                    }
                }
                break;
            case "datetime":
                if ($request[$key]["min"]) {
                    $condition[] = $key." >= '".$request[$key]["min"]."'";
                }
                if ($request[$key]["max"]) {
                    $condition[] = $key." <= '".$request[$key]["max"]."'";
                }
                break;
            }
            if ($condition) {
                $where = implode($and_or, $condition);
            }
        }
        $fields = substr($fields, 1);
        $ret = array(
            "fields" => $fields,
            "where" => $where
        );
        return $ret;
    }

    function _get_relation_data($dsn, $columns) {
        foreach ($columns as $key => $value) {
            if (isset($value["item"]["relation"])) {
                $relation = $value["item"]["relation"];
                if ($relation["db_type"] == "account") {
                    $relation_db = new N2MY_DB($this->account_dsn, $relation["table"]);
                } else {
                    $relation_db = new N2MY_DB($dsn, $relation["table"]);
                }
                if ($relation["status_name"]) {
                    $where = $relation["status_name"]."='".$relation["status"]."'";
                    $relation_data = $relation_db->getRowsAssoc($where);
                } else {
                    $relation_data = $relation_db->getRowsAssoc();
                }
                if ($relation["default_key"] || $relation["default_value"]) {
                    $columns[$key]["item"]["relation_data"][$relation["default_key"]] = $relation["default_value"];
                }
                foreach ($relation_data as $_data) {
                    $columns[$key]["item"]["relation_data"][$_data[$relation["key"]]] = $_data[$relation["value"]];
                }
            }
        }
        return $columns;
    }

    function _user_validator($form_data, $type) {
        //ユーザーID
        if ($type == "add") {
            if (!preg_match('/^[[:alnum:]._-]{3,30}$/', $form_data['user_id'])) {
                $message["user_id"] .= USER_ERROR_ID_LENGTH;
            }
        }
        //ユーザーパスワードをチェック
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $form_data['user_password'])) {
            $message["user_password"] .= USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$form_data['user_password']) || preg_match('/^[[:alpha:]]+$/',$form_data['user_password'])) {
            $message["user_password"] .= USER_ERROR_PASS_INVALID_02;
        }
        //管理者パスワード
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $form_data['user_admin_password'])) {
            $message["user_admin_password"] .= USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$form_data['user_admin_password']) || preg_match('/^[[:alpha:]]+$/',$form_data['user_admin_password'])) {
            $message["user_admin_password"] .= USER_ERROR_PASS_INVALID_02;
        }
        return $message;
    }

    function action_user_agent_summary() {
        require_once 'classes/mgm/dbi/user_agent.dbi.php';
        $objUserAgent = new MgmUserAgentTable(N2MY_MDB_DSN);
        $columns = $objUserAgent->getTableInfo();
        $condition = array();
        if ($start = $this->request->get("start")) {
            $condition[] = "create_datetime > '".$start."'";
        }
        if ($end = $this->request->get("end")) {
            $condition[] = "create_datetime < '".$end."'";
        }
        $where = join(" AND ", $condition);
        $as2_summary = array();
        $as3_summary = array();
        if ($where) {
            $where = " WHERE ".$where;
            $query = "SELECT date_format(create_datetime, '%Y-%m') as date" .
                    ", user_id" .
                    ", appli_version" .
                    ", COUNT(user_id) as cnt" .
                    " FROM user_agent".
                    $where.
                    " AND appli_version = 'as2'".
                    " GROUP BY date_format(create_datetime, '%Y-%m'), appli_version" .
                    " ORDER BY appli_version, date, user_id";
            $rs = $objUserAgent->_conn->query($query);
            if (!DB::isError($rs)) {
                while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $as2_summary[] = $row;
                }
            }
            $query = "SELECT date_format(create_datetime, '%Y-%m') as date" .
                    ", user_id" .
                    ", appli_version" .
                    ", COUNT(user_id) as cnt" .
                    " FROM user_agent".
                    $where.
                    " AND appli_version = 'as3'".
                    " GROUP BY date_format(create_datetime, '%Y-%m'), appli_version" .
                    " ORDER BY appli_version, date, user_id";
            $rs = $objUserAgent->_conn->query($query);
            if (!DB::isError($rs)) {
                while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $as3_summary[] = $row;
                }
            }
        }
        $this->template->assign("as2_summary", $as2_summary);
        $this->template->assign("as3_summary", $as3_summary);
        $this->display("sastik_admin/account/user_agent/index.t.html");
    }

    /**
     * 乱数ID発行
     */
    function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this->dec2any($c);
    }

    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=57, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr(
                    '23456789ab' .
                    'cdefghijkm' .
                    'nopqrstuvw' .
                    'xyzABCDEFG' .
                    'HJKLMNPQRS' .
                    'TUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }

    function _display ($template) {
        $staff_auth = $_SESSION["_authsession"]["data"]["authority"];
        $this->template->assign("authority", $staff_auth);
        $this->display($template);
    }

    function action_add_room_list_form($message = "") {
        if ($message) {
            $this->template->assign("message",$message);
            $user_key = $this->session->get("user_key");
        }
        $this->set_submit_key($this->_name_space);
        $user_key = $this->request->get("user_key");
        $plan_db = new N2MY_DB($this->account_dsn, "service");
        $where = "service_status='1'";
        $service_list = $plan_db->getRowsAssoc($where);
        $this->logger->debug("service_list",__FILE__,__LINE__,$service_list);
        $this->template->assign("user_key",$user_key);
        $this->template->assign("service_list",$service_list);
        $template = "sastik_admin/user/add_room_list.t.html";
        $this->_display($template);
    }

    function action_add_room_list_complete() {
        require_once "classes/dbi/room.dbi.php";
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_add_room_list_form();
            exit;
        }
        $form_data = $this->request->get("add_form");
        $user_id = $form_data["user_id"];
        $where = "user_id='".addslashes($user_id)."'";
        $user_db = new N2MY_DB($this->get_dsn(), "user");
        $acc_user_db = new N2MY_DB($this->account_dsn, "user");
        $relation_db = new N2MY_DB($this->account_dsn, "relation");
        $user_data = $user_db->getRow($where);
        if (DB::isError($user_data)) {
            $message .= "ユーザーIDが正しく有りません<br />";
        } elseif (!$user_data) {
            $message .= "ユーザーIDが正しく有りません<br />";
        }
        $room_db = new RoomTable($this->get_dsn());
        if (!$form_data["room_name"]) {
            $message .= "部屋名を入力してください。<br />";
        } else if (mb_strlen($form_data['room_name']) > 20) {
            $message .= "部屋名は20文字以内で入力してください。<br />";
        }
        if ( !$form_data["discount_rate"] ) {
            $form_data["discount_rate"] = 0;
        } else if (!is_numeric($form_data["discount_rate"])) {
            $message .= "割引率は数値で入力してください。<br />";
        }
        if ($form_data["room_plan_yearly"]) {
            if (!$form_data["room_plan_endtime"]) {
                $form_data["room_plan_endtime"] = "0000-00-00 00:00:00";
            }
            if (!$form_data["contract_month_number"]) {
                $message .= "契約期間（月数）を選択してください。<br />";
            }
        } else {
            $form_data["room_plan_endtime"] = "0000-00-00 00:00:00";
            $form_data["contract_month_number"] = "0";
        }
        $room_count = $this->request->get("count");
        if (!is_numeric($room_count)) {
            $message .= "件数は数値で入力してください。<br />";
        }
        if ( $message ) {
            $this->action_add_room_list_form($message);
            exit;
        }
        if (!$form_data["room_starttime"]) {
            $form_data["room_starttime"] = date("Y-m-d 00:00:00");
        }
        for ($i = 0; $i < $room_count; $i++) {
            $where = "user_key='".addslashes($user_data["user_key"])."'";
            $count = $room_db->numRows($where);
            $room_key = $user_data["user_id"]."-".($count + 1)."-".substr(md5(uniqid(time(), true)), 0,4);
            $room_sort = $count + 1;
            //部屋登録
            $room_data = array (
                "room_key" => $room_key,
                "room_name" => $form_data["room_name"].($count + 1),
                "user_key"  => $user_data["user_key"],
                "room_sort"             => $room_sort,
                "room_status" => "1",
                "room_registtime" => date("Y-m-d H:i:s"),
                );
            $add_room = $room_db->add($room_data);
            if (DB::isError($add_room)) {
                $this->logger2->info( $add_room->getUserInfo());
            }
            $where = "user_id='".addslashes($user_data["user_id"])."'";
            $acc_user_data = $acc_user_db->getRow($where);
            $relation_data = array(
                "relation_key" => $room_key,
                "relation_type" => "mfp",
                "user_key" => $acc_user_data["user_id"],
                "status" => "1",
                "create_datetime" => date("Y-m-d H:i:s"),
                );
            $relation_db->add($relation_data);
            $this->action_room_plan_add($room_key, $form_data);
            //操作ログ登録
            $operation_data = array (
                    "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                    "action_name"        => __FUNCTION__,
                    "table_name"         => "room",
                    "keyword"            => $room_key,
                    "info"               => serialize($room_data),
                    "operation_datetime" => date("Y-m-d H:i:s"),
                );
            $this->add_operation_log($operation_data);
        }
        header("Location: index.php?action_add_room");
    }

    function action_agency_list() {
        $agency_id = $this->request->get("agency_id");
        $table = $this->request->get("table");
        $this->logger->info($table);
        if ($agency_id) {
            $this->session->set("agency_id",$agency_id);
        } else {
            $agency_id = $this->session->get("table");;
        }
        if ($table) {
            $this->session->set("table",$table);
        } else {
            $table = $this->session->get("table");;
        }
        $sort_type = $this->request->get("sort_type");
        $action_name = __FUNCTION__;
        // 検索条件設定・取得
        $sort_type = $sort_type ? $sort_type : "desc";
        $form_data = $this->set_form($action_name, "user_key", $sort_type);
        $page    = $form_data['page'] ? $form_data['page']: "1";
        $limit   = $form_data['page_cnt'] ? $form_data['page_cnt'] : "20";
        $offset  = ($limit * ($page - 1));
        if ($table == "user") {
            $agencyRelationObj = new N2MY_DB($this->get_dsn(), "agency_relation_user");
            $sql = "select agency_relation_user.user_key, user.user_id, user.user_company_name, agency_relation_user.user_key" .
                " FROM agency_relation_user" .
                " LEFT JOIN user ON agency_relation_user.user_key = user.user_key" .
                " WHERE agency_relation_user.agency_id = ".addslashes($agency_id);
            $sql .= " GROUP BY agency_relation_user.user_key ".$sort_type;
        } elseif ($table == "member") {
            $agencyRelationObj = new N2MY_DB($this->get_dsn(), "agency_relation_member");
            $sql = "select agency_relation_member.member_key, member.member_id, member.member_name, agency_relation_member.member_key" .
                " FROM agency_relation_member" .
                " LEFT JOIN member ON agency_relation_member.member_key = member.member_key" .
                " WHERE agency_relation_member.agency_id = ".addslashes($agency_id);
            $sql .= " GROUP BY agency_relation_member.member_key ".$sort_type;
        }
        $total_count = $agencyRelationObj->numRows("agency_id = ".mysql_real_escape_string($agency_id));
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => "action_agency_list",
            "after_action" => $this->request->get("after_action"))
            );
        $ret = $agencyRelationObj->_conn->limitQuery($sql, $offset, $limit);
        $rows = array();
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
        } else {
            while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
                $rows[] = $row;
            }
        }
        $this->template->assign("rows", $rows);
        if ($table == "user") {
            $this->display("sastik_admin/account/agency/user_list.t.html");
        } elseif ($table == "member") {
            $this->display("sastik_admin/account/agency/member_list.t.html");
        }
    }

    function action_table_info_builder() {
        $table = $this->request->get("table");
        $type  = $this->request->get("type");
        if (!$table || !$type) {
            print 'error';
        } else {
            if ($type == "account") {
                $dsn = $this->account_dsn;
            } else {
                $type = 'data';
                $dsn = $this->get_dsn();
            }
            $table = new N2MY_DB($dsn, $table);
            if (DB::isError($table)) {
                print $table->getUserInfo();
            } else {
                if (!$xml = $table->tableInfoBuilder($type)) {
                    print 'build error';
                } else {
                    print $xml;
                }
            }
        }
    }

    private function exitWithError($msg)
    {
        $b = debug_backtrace();

        $from = sprintf("called %s+%s : %s",$b[0]['file'], $b[0]['line'], $b[1]['function']);
        $this->logger->warn($msg.$from);

        exit($msg.$from);
    }
    //{{{validatePGiSetting
    private function validatePGiSetting($form, $room_key)
    {
        $res = array('data'    => $form,
                     'message' => '');

        if (!$form["pgi_start_date"]) {
            $res['message'] .= "開始日を入力してください。<br />";
        } else {
            list($y, $m) = explode('/',$form["pgi_start_date"]);
            if (!checkdate($m, 1, $y)) {
                $res['message'] .= "開始日が不正です。<br />";
            } else {
                $startdate = str_replace('/','-',$form["pgi_start_date"])."-01";
                // 過去分の設定はできません
                if ($startdate < date('Y-m').'-01') {
                    $res['message'].= "過去の設定はできません<br />";
                }
            }
        }
        $res['data']['startdate']  = $startdate;

        require_once('classes/pgi/PGiSystem.class.php');
        $pgiSystems    = PGiSystem::findConfigAll();
        $pgiSystemKeys = array();
        $selectedSystem         = null;
        foreach ($pgiSystems as $system) {
            if (@$form["system_key"] == (string)$system->key) {
                $selectedSystem = $system;
            }
        }
        if (!$form["system_key"]) {
            $res['message'] .= "system_keyを入力してください。<br />";
        } elseif (!$selectedSystem) {
            $res['message'] .= "system_keyが不正です<br />";
        }

        if ($form["company_id_type"] == 'default') {
            $form["company_id"]        = $selectedSystem->defaultCompanyID;
            $res['data']['company_id'] = $selectedSystem->defaultCompanyID;
        }

        if (!$form["company_id"]) {
            $res['message'] .= "company_idを入力してください。<br />";
        } else {
            if ($selectedSystem && $selectedSystem->rates) {
                require_once("classes/pgi/PGiRate.class.php");
                // validate rate's values
                if ($form["system_key"] == "VCUBE GM PRODUCTION") {
                    $rate_config = PGiRate::findGmConfigAll();
                } else {
                    $rate_config = PGiRate::findConfigAll();
                }

                foreach ($rate_config as $rate){
                    $col  = (string)$rate->type;
                    $name = (string)$rate->description;
                    if (!$form[$col]) {
                        $res['message'] .= $name."を入力してください。<br />";
                    } elseif (is_int($form[$col])) {
                        $res['message'] .= $name."は数字で入力してください。<br />";
                    } elseif ($form[$col] <= 0) {
                        $res['message'] .= $name."は0以上で入力してください。<br />";
                    }
                }
            }
        }

        require_once("classes/dbi/pgi_setting.dbi.php");
        $pgi_setting_table = new PGiSettingTable($this->get_dsn());
        try {
            $pgi_settings  = $pgi_setting_table->findByRoomKey($room_key);
        } catch(Exception $e) {
            die('error'.$e->getMessage());
        }
        if (0 == count($pgi_settings)) {
            return $res;
        }

        foreach ($pgi_settings as $setting) {
            if ($startdate == $setting['startdate']) {
                $res['message'] .= $startdate."～の設定は登録済みです(会議作成時にも自動生成されます）<br />";
                return $res;
            }
        }

        return $res;
    }
    function action_create_member_id() {
        $prefix = "VCH000";
        $member_list = array();
        for ($i = 1; $i <= 1000; $i++) {
            $member_list[] = array("id" => $prefix.sprintf("%04d",$i), "pass" => $this->create_id().rand(2,9));
        }
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            $csv = new EZCsv(true, "UTF-8", "UTF-8");
            $dir = $this->get_work_dir();
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");
            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="'.date("Ym").'.csv"');
            // ヘッダ２
            $header["id"] = "ID";
            $header["pass"] = "Password";
            $csv->setHeader($header);
            $csv->write($header);

           foreach($member_list as $row) {
                $csv->write($row);
            }
            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            while($str = fread($fp, 4096)) {
                print $str;
            }
            @fclose($fp);
            $this->logger2->info(memory_get_peak_usage());
            unlink($tmpfile);
            return true;

    }
    private function getPGiRateBySettingKey($pgi_setting_keys)
    {
        require_once "classes/dbi/pgi_rate.dbi.php";
        $pgi_rate_table = new PGiRateTable($this->get_dsn());
        $pgi_rates = $pgi_rate_table->findByPGiSettingKeys($pgi_setting_keys);
        if (count($pgi_rates) <= 0) {
            return array();
        }
        $res = array();
        foreach ($pgi_rates as $rate) {
            $res[$rate['pgi_setting_key']][$rate['rate_type']] = $rate['rate'];
        }
        return $res;
    }

// START

    //}}}
      //{{{action_create_conferen ce
    private function action_create_conference($roomKey)
    {
        try {
            $dsn = $this->get_dsn();
            $ivesSettingTable = new IvesSettingTable($dsn);

            $ives = $ivesSettingTable->findLatestByRoomKey($roomKey);

            if (PEAR::isError($ives)) {
                throw new Exception("select ives_setting room_key failed PEAR said : ".$roomKey);
            }

            require_once("classes/polycom/Polycom.class.php");
            require_once("classes/dbi/video_conference.dbi.php");
            $polycom	= new PolycomClass($this->get_dsn());
            $room_db	= new RoomTable($this->get_dsn());

            $where   	= "room_key = '".addslashes($roomKey)."'";
            $room		= $room_db->getRow($where);

//            $ives = $res[0];
            // 新規追加
            if (!$ives || $ives["is_deleted"]) {
                //SIPアカウント作成
                $ignore_sip_flg = $this->config->get("IVES", "ignore_create_sip_account");
                if (!$ignore_sip_flg) {
                    for( $count = 0; $count < 3; $count++ ) {
                        $sip_uid = $this->action_create_sip_account($room, $count);
                        if ($sip_uid) {
                            $this->logger2->debug($sip_uid);
                            $sip_info = $this->action_get_sip_info($sip_uid);
                            if ($sip_info) {
                                break;
                            }
                        }
                    }
                    if (!$sip_uid || !$sip_info) {
                        $this->logger->warn(__FUNCTION__."#sip fault",__FILE__,__LINE__,array($sip_uid, $sip_info));
                        $this->action_room_detail('', 'SIP アカウント の作成に失敗しました');
                        exit;
                    }
                    $this->logger2->info(array($sip_info->name, $sip_info->secret, $sip_info));
                    $sip_info = array(
                        "uid" => $sip_info->uid,
                        "name" => $sip_info->name,
                        "secret" => $sip_info->secret,
                    );
                } else {
                    $sip_info = array(
                        "uid" => rand(10000, 99999),
                        "name" => rand(1000000, 9999999),
                        "secret" => $this->create_id().rand(2,9),
                    );
                }

                $did = $polycom->createDid();
                $data = array(
                    'room_key'           => $roomKey,
                    'user_key'           => $room["user_key"],
                    'ives_did'           => $did,
                    'sip_uid'            => $sip_info["uid"],
                    'client_id'          => $sip_info["name"],
                    'client_pw'          => $sip_info["secret"],
                    'startdate'          => date("Y-m-d H:i:s"),
                    'registtime'         => date("Y-m-d H:i:s")
                );
                $this->logger2->info($data);
                $res = $ivesSettingTable->add($data);
                $this->creteVideoOption($room, $did);
            }
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
            $this->action_room_detail('', 'MCU Conference の作成に失敗しました');
            exit;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            $this->action_room_detail('', 'MCU Conference の作成に失敗しました');
            exit;
        }
    }

    private function creteVideoOption($room, $did) {
        require_once("classes/polycom/Polycom.class.php");
        require_once("classes/dbi/video_conference.dbi.php");
        $polycom	= new PolycomClass($this->get_dsn());
        $ivesClient = new N2MY_IvesClient($this->get_dsn());
        $video		= new VideoConferenceTable($this->get_dsn());
        $where		= "room_key = '".addslashes($room["room_key"])."'".
          " AND is_active = 1";
        $videoInfo	= $video->getRow($where);
        // remove conference
        if ($videoInfo) {
      $conf = $ivesClient->callGetConference($videoInfo["conference_id"]);
      if ($conf->return) {
        $polycom->removeConference($videoInfo["conference_id"]);
      }
        }
        else {
      $conference = $polycom->checkExistDidInConferences($did);
      if ($conference) {
        $polycom->removeConference($conference->id);
      }
        }
        // create conference
        $confId = $polycom->createConference($did);
        //
        $video->removeAllByRoomKey($room["room_key"]);
    $data = array(
            "user_key"		=> $room["user_key"],
            "room_key"		=> $room["room_key"],
      "conference_id"	=> $confId,
      "did"			=> $did,
      "is_active"		=> 1,
      "createtime"	=> date("Y-m-d H:i:s"));
    $video->add($data);
    }

    //}}}
      //{{{action_remove_conferen ce
    private function action_remove_conference($roomKey)
    {
        try {
            require_once("classes/polycom/Polycom.class.php");
            require_once("classes/dbi/video_conference.dbi.php");
            require_once("classes/polycom/Polycom.class.php");
            $dsn = $this->get_dsn();
            $ivesClient			= new N2MY_IvesClient($dsn);
            $ivesSettingTable	= new IvesSettingTable($dsn);
            $video				= new VideoConferenceTable($dsn);
            $polycom			= new PolycomClass($dsn);

            $res = $ivesSettingTable->findByRoomKey($roomKey);
            if (PEAR::isError($res) || empty($res[0]["ives_did"])) {
                throw new Exception("select ives_setting room_key failed PEAR said : ".$roomKey);
            }

            $ivesSettingList = $res[0];
            $did          = $ivesSettingList["ives_did"];
            $confId       = $ivesSettingList["ives_conference_id"];
            $where = "room_key = '".$roomKey."'";
            $data  = array(
                "is_deleted" => 1
            );
            $res = $ivesSettingTable->update($data, $where);

            if (PEAR::isError($res)) {
                throw new Exception("ives setting update room failed : ".$roomKey);
            }

          // remove
            $where = "room_key = '".$roomKey."'".
                  " AND did = '".$did."'".
                  " AND is_active = 1";
            $videoInfo = $video->getRow($where);

          // remove conference
          if ($videoInfo) {
        $conf = $ivesClient->callGetConference($videoInfo["conference_id"]);
        if ($conf->return) {
          $polycom->removeConference($videoInfo["conference_id"]);
        }
          }
          else {
        $conference = $polycom->checkExistDidInConferences($did);
        if ($conference) {
          $polycom->removeConference($conference->id);
        }
          }

            // remove from video table.
          $video->removeAllByRoomKey($roomKey);

          //sip account 削除
          $this->action_delete_sip_account($ivesSettingList["sip_uid"]);

        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
            $this->action_room_detail('', 'MCU Conference の削除に失敗しました');
            exit;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            $this->action_room_detail('', 'MCU Conference の削除に失敗しました');
            exit;
        }
    }
    //}}}

    //{{{findMeeting
    private function findMeeting($meetingKey)
    {
        $this->logger2->info("-------findMeeting--------" . $meetingKey);

        $objMeeting = new DBI_Meeting($this->get_dsn());
        $where      = "meeting_ticket='".mysql_real_escape_string($meetingKey)."'";

        return $objMeeting->getRow($where);
    }

    //}}}
    //{{{ updateSetting
    private function updateSetting($ivesSettingKey, $data)
    {
        $setting  = new IvesSettingTable($this->get_dsn());
        $where = sprintf("ives_setting_key = '%s' and client_id = ''",
                         mysql_real_escape_string($ivesSettingKey));

        $res = $setting->update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("update room failed PEAR said : ".$res->getUserInfo());
        }
    }
    //}}}

    function action_create_sip_account($room_info, $count = 0) {
        try {
            $dsn = $this->get_dsn();

            require_once("classes/N2MY_IvesSipClient.class.php");
            $ivesClient = new N2MY_IvesSipClient($dsn);

            if ($count > 0) {
                $room_key = $room_info["room_key"]."_".$count;
            } else {
                $room_key = $room_info["room_key"];
            }
            $login_id = str_replace("-", "_", $room_key);

            $subscriber = array();
            $subscriber["callerid"] = "vcube"; //vcube
            $subscriber["title"] = "vcube"; //vcube
            $subscriber["gender"] = "M";
            $subscriber["lastName"] = "vcube"; //vcube
            $subscriber["firstName"] = "vcube"; //vcube
            $subscriber["dateOfBirth"] = "1998-10-09"; //vcube
            $subscriber["fax"] = "0355019676"; //vcube
            $subscriber["homePhone"] = "0357683111"; //vcube
            $subscriber["mobile"] = "0357683111"; //vcube

            //-が利用できないので_に置き換え
            $subscriber["login"] = $login_id; //room_key
            $subscriber["mail"] = "vsupport@vcube.co.jp"; //vsupport?
            $subscriber["userPassword"] = $this->create_id().rand(2,9);//ランダム
            $subscriber["address_1"] = "Nakameguro GT Tower 20F"; //vcube
            $subscriber["address_2"] = "2-1-1 Kamimeguro, Meguro-ku"; //vcube
            $subscriber["zipCode"] = "1530051"; //vcube
            $subscriber["city"] = "Tokyo";
            $subscriber["country"] = "Japan";
            $subscriber["preferedLanguage"] = "English";
            $subscriber["acceptMailHtml"] = false;
            $subscriber["acceptMailInformation"] = false;
            $subscriber["acceptProfilePublic"] = false;

            $account = array();
            $account["state"] = "Actif";
            $account["creationDate"] = date("Y-m-d");
            $account["activityEndDate"] = "0000-00-00";
            $account["inactivityEndDate"] = "0000-00-00";
            $account["paymentDate"] = "0000-00-00";

            $createSubscriber = array();
            $createSubscriber["subscriber"] = $subscriber;
            $createSubscriber["account"] = $account;
            //以下のデータは固定
            $createSubscriber["profileId"] = 281;
            $createSubscriber["roleId"] = 1;
            $createSubscriber["optionId"] = 127;
            $createSubscriber["customerId"] = 0;

            $result = $ivesClient->createSubscriber($createSubscriber);
            $this->logger2->info(array($result, $createSubscriber));
            return $result->uid;
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
            return false;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            return false;
        }
    }

    function action_get_sip_info($uid) {
        $dsn = $this->get_dsn();
        $this->logger2->info($uid);
        require_once("classes/N2MY_IvesSipClient.class.php");
        $ivesSipClient = new N2MY_IvesSipClient($dsn);
        try {
            $result = $ivesSipClient->getSipInformation($uid);
            $this->logger2->debug($result);
            return $result;
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__,$result);
            return false;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__,$result);
            return false;
        }
    }

    function action_delete_sip_account($uid) {
        $dsn = $this->get_dsn();
        require_once("classes/N2MY_IvesSipClient.class.php");
        $ivesSipClient = new N2MY_IvesSipClient($dsn);
        try {
            $result = $ivesSipClient->deleteSubscriber($uid);
            return true;
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__,array($uid,$result));
            return false;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__,array($uid,$result));
            return false;
        }
    }

    function action_add_vcubeid_service($user_info, $member_ids, $room_key = null) {
    }

    function action_remove_vcubeid_service($room_key) {
    }

    function action_service_login() {
        $id = $this->request->get("id");
        $type = $this->request->get("type");
        if (!$id || !$type) {
           print "parameter error!";
           exit;
        }
        switch ($type) {
        case "user":
            require_once("classes/dbi/user.dbi.php");
            $obj_User = new UserTable($this->get_dsn());
            $where = "user_id = '".mysql_real_escape_string($id)."'";
            $user_info = $obj_User->getRow($where, "user_id,user_password,user_admin_password");
            if ($user_info) {
                require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
                $user_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_password"]);
                $location = N2MY_BASE_URL."../services/login/index.php?action_showlogin&user_id=".$user_info["user_id"]."&user_password=".$user_password."&country=jp&lang=ja&time_zone=9";
                header("Location: ".$location);
            } else {
                print "user_id error";
            }
        case "member":
            require_once("classes/dbi/member.dbi.php");
            $obj_Member = new MemberTable($this->get_dsn());
            $where = "member_id = '".mysql_real_escape_string($id)."'";
            $member_info = $obj_Member->getRow($where, "member_id,member_pass");
            if ($member_info) {
                require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
                $member_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_info["member_pass"]);
                $location = N2MY_BASE_URL."../services/login/index.php?action_showlogin&user_id=".$member_info["member_id"]."&user_password=".$member_password."&country=jp&lang=ja&time_zone=9";
                header("Location: ".$location);
            } else {
                print "member_id error";
            }
            break;
        }
        exit;
    }

    function _getMeetingVersion() {
        static $version;
        if ($version) {
            return $version;
        }
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }


}

$main = new AppAdminTool();
$main->execute();
