<?php
require_once 'classes/AppFrame.class.php';
class AppRtmpProtocol extends AppFrame {
    function init() {
    }

    function default_view() {
        print <<<EOM
<html>
<body>
<form id="roomEditForm" method="post">
group_id : <input type="text" name="group_id" value="">
<input type="submit" name="action_confirm" value="確認">
</form>
</body>
</html>
EOM;
    }

    function action_confirm() {
        print <<<EOM
<html>
<body>
<head>
<script type="text/javascript" src="/shared/js/lib/prototype.js"></script>
<script type="text/javascript">
function moveRight(id) {
    var denybox = document.getElementById(id + '_deny');
    var allowbox = document.getElementById(id + '_allow');
    var option_list = allowbox.getElementsByTagName('option');
    for (var i = option_list.length; i > 0; i--) {
    if (option_list[i-1].selected) {
    denybox.insertBefore(allowbox.removeChild(option_list[i-1]), null);
    }
    }
}

function moveLeft(id) {
    var denybox = document.getElementById(id + '_deny');
    var allowbox = document.getElementById(id + '_allow');
    var option_list = denybox.getElementsByTagName('option');
    for (var i = option_list.length; i > 0; i--) {
    if (option_list[i-1].selected) {
    allowbox.insertBefore(denybox.removeChild(option_list[i-1]), null);
    }
    }
}

function moveUpElement(id) {
    var selectbox = document.getElementById(id + '_allow');
    var option_list = selectbox.getElementsByTagName('option');
    for (var i = 0; i < option_list.length; i++) {
    if (option_list[i].selected) {
    if (i > 0 && !option_list[i-1].selected) {
    selectbox.insertBefore(selectbox.removeChild(option_list[i]), option_list[i-1]);
    selectbox.focus();
    }
    }
    }
}

function moveDownElement(id) {
    var selectbox = document.getElementById(id + '_allow');
    var option_list = selectbox.getElementsByTagName('option');
    for (var i = option_list.length-1; i >= 0; i--) {
    if (option_list[i].selected) {
    if (i < option_list.length-1 && !option_list[i+1].selected) {
    selectbox.insertBefore(selectbox.removeChild(option_list[i+1]), option_list[i]);
    selectbox.focus();
    }
    }
    }
}

function save() {
    if (confirm("RTMPの設定を変更します。よろしいですか？")) {
        var allowbox = document.getElementById('protocol_allow');
        if (allowbox) {
            var option_list = allowbox.getElementsByTagName('option');
            var rtmp_protocol = Array();
            if (option_list.length == 0) {
                alert("プロトコルを選択してください");
                return false;
            }
            for (var i = 0; i < option_list.length; i++) {
                rtmp_protocol.push(option_list[i].value);
            }
            $("rtmp_protocol").value = rtmp_protocol.join(",");
        }
        $("_action").name = "action_update";
        $("roomEditForm").submit();
    }
}
</script>
</head>
<body>
EOM;
        $group_id       = $this->request->get('group_id');
        $rtmp_protocol  = $this->request->get('rtmp_protocol');
        if (!$group_id) {
            print 'グループID指定なし';
            return $this->default_view();
        } else {
            require_once 'classes/dbi/user_group.dbi.php';
            require_once 'classes/dbi/user.dbi.php';
            require_once 'classes/dbi/room.dbi.php';
            $objUserGroup   = new UserGroupTable($this->get_dsn());
            $objUser        = new UserTable($this->get_dsn());
            $objRoom        = new RoomTable($this->get_dsn());
            if (!$group_info = $objUserGroup->getRow("group_id = '".addslashes($group_id)."'")) {
                print 'グループID指定なし';
                return $this->default_view();
            } else {
                print '<b>グループID</b> : '.htmlspecialchars($group_id).'<br />';
                $where = "user_group = '".addslashes($group_id)."'".
                    " AND user_status = 1" .
                    " AND user.user_delete_status != 2";
                $user_list = $objUser->getRowsAssoc($where, null, null, null, 'user_key, user_company_name');
                print '<table border="1">';
                print '<tr>';
                print '<th>room_key</th>';
                print '<th>room_name</th>';
                print '<th>rtmp_protocol</th>';
                print '</tr>';
                foreach($user_list as $user_info) {
                    print '<tr>';
                    $room_list = $objRoom->getRowsAssoc("user_key = ".$user_info['user_key'], null, null, null, 'room_key, room_name, rtmp_protocol');
                    foreach($room_list as $room_info) {
                        print '<td>'.$room_info["room_key"].'</td>';
                        print '<td>'.$room_info["room_name"].'</td>';
                        print '<td>'.$room_info["rtmp_protocol"].'</td>';
                    }
                    print '</tr>';
                }
                print '</table>';
                print '<form action="'.$_SERVER["SCRIPT_NAME"].'" id="roomEditForm" method="post">';
                print '<input type="hidden" name="_action" id="_action" />';
                print '<input type="hidden" name="group_id" value="'.htmlspecialchars($group_id).'" />';
                print <<<EOM
<table border="0" class="port-sec">
<tr valign="top">
<td>
<p>有効</p>
    <div>
    <select id="protocol_allow" multiple="multiple" size="10">
    <option value="rtmp:1935">rtmp:1935</option>
    <option value="rtmp:80">rtmp:80</option>
    <option value="rtmp:8080">rtmp:8080</option>
    <option value="rtmps:443">rtmps:443</option>
    <option value="rtmpt:80">rtmpt:80</option>
    <option value="rtmpt:8080">rtmpt:8080</option>
    <option value="rtmpe:1935">rtmpe:1935</option>
    <option value="rtmpe:80">rtmpe:80</option>
    <option value="rtmpe:8080">rtmpe:8080</option>
    <option value="rtmpte:80">rtmpte:80</option>
    <option value="rtmpte:8080">rtmpte:8080</option>
    </select>
    <br />
    <input type="hidden" id="rtmp_protocol" name="rtmp_protocol" value="">
    <span>
        <a href='javascript:;void(0);' onclick='moveUpElement("protocol")' class="bt-up">↑</a>
        <a href='javascript:;void(0);' onclick='moveDownElement("protocol")' class="bt-down">↓</a>
    </span>
    </div>
</td>
<td valign="middle">
    <a href='javascript:;void(0);' onclick='moveRight("protocol");'><span><span>→</span></span></a>
    <a href='javascript:;void(0);' onclick='moveLeft("protocol");'><span><span>←</span></span></a>
</td>
<td>
    <p>無効</p>
    <div>
    <select id="protocol_deny" multiple="multiple" size="10">
    </select>
    </div>
</td>

</tr>
</table>
<p class="error"></p>
<input type="button" value="更新" onclick="save();" />
</form>
EOM;
            }
        }
    }

    function action_update() {
        $group_id       = $this->request->get('group_id');
        $rtmp_protocol  = $this->request->get('rtmp_protocol');
        require_once 'classes/dbi/user_group.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        require_once 'classes/dbi/room.dbi.php';
        $objUserGroup   = new UserGroupTable($this->get_dsn());
        $objUser        = new UserTable($this->get_dsn());
        $objRoom        = new RoomTable($this->get_dsn());
        if ($group_info = $objUserGroup->getRow("group_id = '".addslashes($group_id)."'")) {
            $where = "user_group = '".addslashes($group_id)."'".
                " AND user_status = 1" .
                " AND user.user_delete_status != 2";
            $user_list = $objUser->getRowsAssoc($where, null, null, null, 'user_key, user_company_name');
            print '<pre>';
            print_r($user_list);
            foreach($user_list as $user_info) {
                $room_list = $objRoom->getRowsAssoc("user_key = ".$user_info['user_key'], null, null, null, 'room_key');
                if ($room_list) {
                    $data = array(
                        'rtmp_protocol'     => $rtmp_protocol,
                        'room_updatetime'   => date('Y-m-d H:i:s')
                    );
                    $objRoom->update($data, "user_key = ".$user_info['user_key']);
                }
            }
        }
        $this->action_confirm();
    }
}
$main = new AppRtmpProtocol();
$main->execute();
?>