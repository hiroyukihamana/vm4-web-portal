<?php
require_once 'classes/AppFrame.class.php';
require_once 'lib/EZLib/EZUtil/EZCsv.class.php';
require_once 'classes/dbi/user_group.dbi.php';
require_once 'classes/dbi/user.dbi.php';
require_once 'classes/dbi/room.dbi.php';
require_once 'classes/dbi/meeting.dbi.php';
require_once 'classes/core/dbi/Participant.dbi.php';
require_once 'htdocs/sastik_admin/dBug.php';

class AppGroupAccount extends AppFrame {

    function init() {
    }

    function auth() {
    }

    function default_view() {
        // DB
print <<<EOM
<a href="?action_detail=&debug=1">■</a> <a href="?action_detail">詳細</a><br />
<a href="?action_summary=&debug=1">■</a> <a href="?action_summary">集計</a><br />
<a href="?action_billing=&debug=1">■</a> <a href="?action_billing">請求</a><br />
EOM;
    }

    /**
     * 指定した月の詳細を取得する
     * @todo ユーザー毎の出力
     * @todo 月別の指定
     * @todo CSV出力
     */
    function action_detail() {
        $group_id = $this->request->get('group_id');
        $yy = $this->request->get('yy');
        $mm = $this->request->get('mm');
        // DB
        $server_list_ini = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        $db_list = $server_list_ini['SERVER_LIST'];
        $rows = array();
        foreach ($db_list as $db_key => $dsn) {
            // グループユーザー
            $objUserGroup   = new UserGroupTable($dsn);
            $objUser        = new UserTable($dsn);
            $objRoom        = new RoomTable($dsn);
            $objMeeting     = new MeetingTable($dsn);
            $objParticipant = new DBI_Participant($dsn);

            $where = "status = '1'" .
                " AND group_id = '".addslashes($group_id)."'";
            $group_list = $objUserGroup->getRowsAssoc($where);
            foreach($group_list as $group) {
                // print_r($group);
                $sql = "SELECT user.user_id" .
                    ", room.room_name" .
                    ", meeting.meeting_name" .
                    ", participant.participant_key" .
                    ", participant.participant_name" .
                    ", participant.uptime_start" .
                    ", participant.uptime_end" .
                    " FROM user" .
                    ", room" .
                    ", meeting" .
                    ", participant" .
                    " WHERE user.user_status = 1" .
                    " AND user.user_delete_status != 2" .
                    " AND user.user_group = '".addslashes($group['group_id'])."'".
                    " AND user.user_key = room.user_key".
                    " AND meeting.room_key = room.room_key" .
                    " AND meeting.meeting_key = participant.meeting_key" .
                    " AND meeting.meeting_key != 281708" .
                    " AND participant.is_active = '0'".
                    " AND participant.uptime_end LIKE '".$yy.'-'.$mm."%'" .
                    " ORDER BY user_id" .
                    ", room.room_name" .
                    ", meeting.meeting_name" .
                    ", participant.participant_key" .
                    ", participant.participant_name" .
                    ", participant.uptime_start" .
                    ", participant.uptime_end";
                $meeting_rs = $objMeeting->_conn->query($sql);
                if (DB::isError($meeting_rs)) {
                    die($meeting_rs->getUserInfo());
                }
                // ミーティング
                while ($meeting = $meeting_rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $time = strtotime($meeting['uptime_end']) - strtotime($meeting['uptime_start']);
                    $rows[] = array(
                        "user_id"           => $meeting['user_id'],
                        "room_name"         => $meeting['room_name'],
                        "meeting_name"      => $meeting['meeting_name'],
                        "participant_name"  => $meeting['participant_name'],
                        "meeting_in"        => $meeting['uptime_start'],
                        "meeting_out"       => $meeting['uptime_end'],
                        "use_time"          => floor($time / 3600).gmdate(':i:s', $time), //gmdate('H:i:s', strtotime($meeting['uptime_end']) - strtotime($meeting['uptime_start'])),
                        "use_second"        => $time,
                        );
                }
            }
        }
        // ヘッダ２
        $header = array(
            "user_id"           => "ユーザーID",
            "room_name"         => "会議室名",
            "meeting_name"      => "会議名",
            "participant_name"  => "会議入室名",
            "meeting_in"        => "入室時刻",
            "meeting_out"       => "退出時刻",
            "use_time"          => "利用時間",
            );
        if ($this->request->get('debug')) {
            $header['use_second'] = '利用秒数';
            $this->debug_print($header, $rows);
        } else {
            require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            $csv = new EZCsv(true, "SJIS", "UTF-8");
            $dir = $this->get_work_dir();
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");
            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="detail_'.$group_id.$yy.$mm.'.csv"');
            $csv->setHeader($header);
            $csv->write($header);
            foreach($rows as $row) {
                $csv->write($row);
            }
            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            $contents = fread($fp, filesize($tmpfile));
            fclose($fp);
            print $contents;
            unlink($tmpfile);
            return true;
        }
    }

    /**
     * 月単位の集計を取得する
     */
    function action_summary() {
        $group_id = $this->request->get('group_id');
        $yy = $this->request->get('yy');
        $mm = $this->request->get('mm');
        // DB
        $server_list_ini = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        $db_list = $server_list_ini['SERVER_LIST'];

        $rows = array();
        foreach ($db_list as $db_key => $dsn) {
            // グループユーザー
            $objUserGroup   = new UserGroupTable($dsn);
            $objUser        = new UserTable($dsn);
            $objRoom        = new RoomTable($dsn);
            $objMeeting     = new MeetingTable($dsn);
            $objParticipant = new DBI_Participant($dsn);
            $where = "status = '1'" .
                " AND group_id = '".addslashes($group_id)."'";
            $group_list = $objUserGroup->getRowsAssoc($where);
            foreach($group_list as $group) {
                // 集計用の配列を作成
                $sql = "SELECT user.user_id" .
                    ", room.room_key" .
                    ", room.room_name" .
                    " FROM user" .
                    ", room" .
                    " WHERE user.user_key = room.user_key" .
                    " AND user.user_status = 1" .
                    " AND user.user_delete_status != 2" .
                    " AND user.user_group = '".addslashes($group['group_id'])."'" .
                    " AND room.room_status = 1" .
                    " ORDER BY user.user_id" .
                    ", room.room_key" .
                    ", room.room_name";
                $room_rs = $objUser->_conn->query($sql);
                while ($room = $room_rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $data["user_id"]           = $room['user_id'];
                    $data["room_name"]         = $room['room_name'];
                    for($i = 0; $i < 12; $i++) {
                        $current_time = mktime(0,0,0,$mm + $i, 1, $yy);
                        $data[date('Ym', $current_time)] = 0;
                    }
                    $rows[$room['room_key']] = $data;
                }
                // 集計行生成
                $rows['__summary']['user_id']           = '合計';
                $rows['__summary']['room_name']         = '';
                for($i = 0; $i < 12; $i++) {
                    $current_time = mktime(0,0,0,$mm + $i, 1, $yy);
                    $rows['__summary'][date('Ym', $current_time)] = 0;
                }
                // 値のあった箇所だけ埋める
                $sql = "SELECT meeting.room_key" .
                    ", DATE_FORMAT(participant.uptime_end, '%Y%m') AS ym" .
                    ", SUM((UNIX_TIMESTAMP(participant.uptime_end) - UNIX_TIMESTAMP(participant.uptime_start))) as time".
                    " FROM user" .
                    ", room" .
                    ", meeting" .
                    ", participant" .
                    " WHERE user.user_status = 1" .
                    " AND user.user_delete_status != 2" .
                    " AND user.user_group = '".addslashes($group['group_id'])."'".
                    " AND user.user_key = room.user_key".
                    " AND room.room_key = meeting.room_key" .
                    " AND meeting.meeting_key = participant.meeting_key" .
                    " AND meeting.meeting_key != 281708" .
                    " AND participant.uptime_end > '".$yy.'-'.$mm."-01 00:00:00'".
                    " GROUP BY meeting.room_key, DATE_FORMAT(participant.uptime_end, '%Y%m')";
                $meeting_rs = $objMeeting->_conn->query($sql);
                if (DB::isError($meeting_rs)) {
                    die($meeting_rs->getUserInfo());
                }
                // ミーティング
                while ($meeting = $meeting_rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $room_key = $meeting['room_key'];
                    $ym = $meeting['ym'];
                    $rows[$room_key][$ym] = floor($meeting['time'] / 3600).gmdate(':i:s', $meeting['time']); // gmdate('H:i:s', $meeting['time']);
//                    $rows[$room_key][$ym] = $meeting['time'];
                    $rows['__summary'][$ym] += $meeting['time'];
                }
            }
        }
        // 合計の時間を整形
        for($i = 0; $i < 12; $i++) {
            $current_time = mktime(0,0,0,$mm + $i, 1, $yy);
            $ym = date('Ym', $current_time);
            if ($rows['__summary'][$ym]) {
                $rows['__summary'][$ym] = floor($rows['__summary'][$ym] / 3600).gmdate(':i:s', $rows['__summary'][$ym]); //gmdate('H:i:s', $rows['__summary'][$ym]);
            }
        }

        // ヘッダ２
        $header["user_id"]      = "ユーザーID";
        $header["room_name"]    = "会議室名";
        for($i = 0; $i < 12; $i++) {
            $current_time = mktime(0,0,0,$mm + $i, 1, $yy);
            $header[date('Ym', $current_time)]    = date('Y年m月', $current_time);
        }
        if ($this->request->get('debug')) {
            $this->debug_print($header, $rows);
        } else {
            require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            $csv = new EZCsv(true, "SJIS", "UTF-8");
            $dir = $this->get_work_dir();
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");
            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="summary_'.$group_id.$yy.$mm.'.csv"');
            $csv->setHeader($header);
            $csv->write($header);
            foreach($rows as $row) {
                $csv->write($row);
            }
            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            $contents = fread($fp, filesize($tmpfile));
            fclose($fp);
            print $contents;
            unlink($tmpfile);
            return true;
        }
    }

    /**
     * 請求書発行
     */
    function action_billing() {
        $group_id = $this->request->get('group_id');
        $yy = $this->request->get('yy');
        $mm = $this->request->get('mm');
        // DB
        $server_list_ini = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        $db_list = $server_list_ini['SERVER_LIST'];
        $start_y = 2010;
        $start_m = 8;
        $rows = array();
        foreach ($db_list as $db_key => $dsn) {
            // グループユーザー
            $objUserGroup   = new UserGroupTable($dsn);
            $objUser        = new UserTable($dsn);
            $objRoom        = new RoomTable($dsn);
            $objMeeting     = new MeetingTable($dsn);
            $objParticipant = new DBI_Participant($dsn);
            $where = "status = '1'" .
                " AND group_id = '".addslashes($group_id)."'";
            $group_list = $objUserGroup->getRowsAssoc($where);
            foreach($group_list as $group) {
                // 初期化
                $limit_user = 0;
                $running_time = 0;
                $where = "user_status = 1" .
                    " AND user.user_delete_status != 2" .
                    " AND user_group = '".addslashes($group['group_id'])."'";
                $total_user = $objUser->numRows($where);
                // ユーザごとの利用状況
                $sql = "SELECT meeting.user_key" .
                    ", SUM((UNIX_TIMESTAMP(participant.uptime_end) - UNIX_TIMESTAMP(participant.uptime_start))) as time".
                    " FROM user" .
                    ", meeting" .
                    ", participant" .
                    " WHERE user.user_status = 1" .
                    " AND user.user_delete_status != 2" .
                    " AND user.user_group = '".addslashes($group['group_id'])."'".
                    " AND user.user_key = meeting.user_key".
                    " AND meeting.meeting_key = participant.meeting_key" .
                    " AND meeting.meeting_key != 281708" .
                    " AND participant.uptime_end LIKE '".$yy.'-'.$mm."%'".
                    " GROUP BY meeting.user_key";
                $meeting_rs = $objMeeting->_conn->query($sql);
                if (DB::isError($meeting_rs)) {
                    die($meeting_rs->getUserInfo());
                }
                // ミーティング
                while ($meeting = $meeting_rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $minute = floor($meeting['time'] / 60);
                    if ($minute > $group['free_time']) {
                        $fee = (($minute - $group['free_time']) * $group['running_fee']) + $group['basic_fee'];
                        if ($fee > $group['limit_fee']) {
                            $limit_user++;
                        } else {
                            $running_time += ($minute - $group['free_time']);
                        }
                    }
                }
                $basic_fee = $total_user * $group['basic_fee'];
                $limit_fee = ($group['limit_fee'] - $group['basic_fee']) * $limit_user;
                $running_fee = $running_time * $group['running_fee'];
                print <<<EOM
<pre>
[ {$group['group_name']} ]
基本料金: $basic_fee 円 ( $total_user アカウント x {$group['basic_fee']} 円 )
従量課金分: $running_fee 円 ( $running_time 分 x {$group['running_fee']} 円 )
上限額: $limit_fee 円 ( $limit_user アカウント x ( {$group['limit_fee']} - {$group['basic_fee']} ) 円 )
<hr />
EOM;
            }
        }
    }

    function debug_print($header, $rows) {
//        print '<table border=1>';
//        print '<tr>';
//        foreach ($header as $key => $val) {
//            print '<th>'.htmlspecialchars($val).'</th>';
//        }
//        print '</tr>';
//        foreach ($rows as $row) {
//            print '<tr>';
//            foreach ($header as $key => $val) {
//                print '<td>'.htmlspecialchars($row[$key]).'</td>';
//            }
//            print '</tr>';
//        }
//        print '</table>';
        $this->template->assign('headers', $header);
        $this->template->assign('rows', $rows);
        $this->display('sastik_admin/user_group/detail.t.html');
    }

}

$main = new AppGroupAccount();
$main->execute();