<?php

require_once ('classes/AppFrame.class.php');
require_once ('lib/sastik/common.php');

class SastikAccountTranslationQueryForm extends AppFrame {
	public function default_view() {
		$template_path = 'sastik_admin/query.t.html';
		$this->display($template_path);
	}
	
	public function action_query() {
		$dsn = $this->get_auth_dsn();

		$requests = $this->request->getAll();		
		$this->logger2->debug($requests);
		
		if(empty($requests['user_id']) || empty($requests['account_id'])) {
			$member_id = false;
		}
		else {
			$member_id = generateMemberID($requests['user_id'], $requests['account_id']);
		}

		echo json_encode(array(
			'member_id' => htmlspecialchars($member_id === false ? '' : $member_id)
		));
	}
}

$main = new SastikAccountTranslationQueryForm();
$main->execute();

?>

