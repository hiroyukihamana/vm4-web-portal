document.getElementsByTagNames = function(nodes) {
 var elems = new Array;
 for(var i=0; i<nodes.length; i++) {
 var tag = nodes[i];
 elems = elems.concat( $A(document.getElementsByTagName(tag)) );
 }
 return elems;
}

document.isIE = Boolean(window.attachEvent);
document.isIE6 = Boolean(document.isIE && navigator.appVersion.match(/MSIE 6/));
document.isIE7 = Boolean(document.isIE && navigator.appVersion.match(/MSIE 7/));
document.isSafari = Boolean(navigator.appVersion.indexOf('KHTML') > -1);
