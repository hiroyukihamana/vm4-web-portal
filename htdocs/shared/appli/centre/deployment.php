<?php print '<?xml version="1.0" encoding="UTF-8" ?>'."\n";
    if ($_REQUEST["url"]) {
        $url .= $_REQUEST["url"];
    } else {
        $url = ($_SERVER["HTTPS"]) ? 'https://' : 'http://';
        $url .= $_SERVER["HTTP_HOST"];
    }
    $version_ini = parse_ini_file('setup/version.ini', true);
    $version = $version_ini["APPLI"]['centre'];
    if (preg_match('/^vTerminal/', $_SERVER['HTTP_USER_AGENT']) == true) {
        $appli = 'vTerminal';
    } else {
        $appli = 'vCentre';
    }
?>
<deploy>
    <version><?php print $version; ?></version>
    <location><?php print $url;?>/shared/appli/centre/</location>
    <initialEntryPoint>
       <commandLine file="Centre.exe" parameters="/initialize" />
    </initialEntryPoint>
    <userAgent><?php print $appli;?>/1.0 (ja_JP)</userAgent>

    <assembly name="Centre.exe">
        <version>1.4.0.0</version>
        <hash method="sha1">a79f8512d43e4693bca360d8618410bc350d94bf</hash>
        <size>741848</size>
    </assembly>
    <assembly name="CentreUpdateManager.exe">
        <version>1.4.0.0</version>
        <hash method="sha1">4d278301509ad1637cabb81d25b4753cfe949073</hash>
        <size>198616</size>
    </assembly>
    <assembly name="vrms_rs2.ocx">
        <version>2.1.0.6</version>
        <hash method="sha1">77c1bc088b21f72181b9a67bfa412750d16acf36</hash>
        <size>636176</size>
    </assembly>
    <assembly name="vrmsaecc.ocx">
        <version>0.7.3.1</version>
        <hash method="sha1">fb0ccb10f0ffe6445d04ea4311eadbae341fe09f</hash>
        <size>1378200</size>
    </assembly>
    <assembly name="Sharing3/Sharing3.exe">
        <version>1.0.2.0</version>
        <hash method="sha1">d0e7188c6e28ccd2a99734a16a92f6e4cc1951ee</hash>
        <size>1156440</size>
    </assembly>
    <assembly name="Sharing3/vshooks.dll">
        <hash method="sha1">869289af554d57e38632df1fabac4b53b7d8fa8a</hash>
        <size>54616</size>
    </assembly>

    <file name="Sharing3/languages.xml">
        <hash method="sha1">d97d13ef2c4c4fd4e2d919e6333c694d39ca29fb</hash>
        <size>826</size>
    </file>

    <file name="Centre.swf"/>
    <file name="config.xml"/>
    <file name="language.xml"/>
    <file name="country.xml"/>
    <file name="location.xml"/>
    <file name="phone_type.xml"/>
    <file name="region.xml"/>
    <file name="assets/en_US/skin.swf"/>
    <file name="assets/zh_CN/skin.swf"/>
    <file name="assets/ja_JP/skin.swf"/>

</deploy>
