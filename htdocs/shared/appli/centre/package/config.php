<?php
require_once 'classes/AppFrame.class.php';

class Centre_config extends AppFrame {

    function default_view() {
        header('Content-Type: text/xml; charset=UTF-8');
        if ($_REQUEST["url"]) {
            $url .= $_REQUEST["url"];
        } else {
            $url = ($_SERVER["HTTPS"]) ? 'https://' : 'http://';
            $url .= $_SERVER["HTTP_HOST"];
        }
        $config = parse_ini_file('config/config.ini', TRUE);
        // 言語の未設定時対応
        require_once 'lib/EZLib/EZUtil/EZLanguage.class.php';
        $lang_list = $this->get_language_list();
        $lang_keys = array_keys($lang_list);
        $lang_key = EZLanguage::getLangCd($lang_keys[0]);
        // 地域の未設定時対応
        $country_list = $this->get_country_list();
        $country_keys = array_keys($country_list);
        $country_key = $country_keys[0];
        // 時差の未設定時対応
        $timezone_key = $config["GLOBAL"]["time_zone"];
        // 直接指定されていればそちらを優先
        $lang     = isset($config['CENTRE']['default_lang'])        ? $config['CENTRE']['default_lang'] : $lang_key;
        $locale   = isset($config['CENTRE']['default_locale'])      ? $config['CENTRE']['default_locale'] : $country_key;
        $timezone = isset($config['CENTRE']['default_timezone'])    ? $config['CENTRE']['default_timezone'] : $timezone_key;
        $version_ini = parse_ini_file('setup/version.ini', true);
        if (preg_match('/^vTerminal/', $_SERVER['HTTP_USER_AGENT']) == true) {
            $appli = 'terminal';
        } else {
            $appli = 'centre';
        }
        $version = $version_ini["APPLI"]['centre'];
        print <<<EOM
<?xml version="1.0" encoding="utf-8"?>
<CONFIG>
    <Version>{$version}</Version>
    <!-- centre / terminal -->
    <Type>{$appli}</Type>
    <APIBaseUrl>{$url}</APIBaseUrl>
    <!-- ja_JP/zh_CN/en_US -->
    <DefaultLanguage>{$lang}</DefaultLanguage>
    <!-- -12 -11 -10 -9 -8 -7 -6 -5 -4.5 -4 -3.5 -3 -2 -1 0 1 2 3 4 5 5.5 6 7 8 9 9.5 10 10.5 11 12 13 14 -->
    <DefaultTimezone>{$timezone}</DefaultTimezone>
    <!-- jp us us2 cn sg etc jp2 -->
    <DefaultLocation>{$locale}</DefaultLocation>
    <!-- windows air -->
    <Platform>windows</Platform>
</CONFIG>
EOM;
    }
}

$main = new Centre_config();
$main->execute();
