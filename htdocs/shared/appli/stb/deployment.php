<?php print '<?xml version="1.0" encoding="UTF-8" ?>'."\n";
    if ($_REQUEST["url"]) {
        $url .= $_REQUEST["url"];
    } else {
        $url = ($_SERVER["HTTPS"]) ? 'https://' : 'http://';
        $url .= $_SERVER["HTTP_HOST"];
    }
    $version_ini = parse_ini_file('setup/version.ini', true);
    $version = $version_ini["APPLI"]['stb'];
    if (preg_match('/^vSTB/', $_SERVER['HTTP_USER_AGENT']) == true) {
        $appli = 'vSTB';
    }
?>
<deploy>
    <version><?php print $version; ?></version>
    <location><?php print $url;?>/shared/appli/centre/</location>
    <initialEntryPoint>
       <commandLine file="Centre.exe" parameters="/initialize" />
    </initialEntryPoint>
    <userAgent><?php print $appli;?>/1.0 (ja_JP)</userAgent>

    <assembly name="Centre.exe">
        <version>1.1.0.0</version>
        <hash method="sha1">074e3abbd78e779eb93ba0d14877a91a91780642</hash>
        <size>735832</size>
    </assembly>
    <assembly name="CentreUpdateManager.exe">
        <version>1.1.0.0</version>
        <hash method="sha1">8294be90f097643a025481ab8a8a92302bb0f4cf</hash>
        <size>199768</size>
    </assembly>
    <assembly name="vrms_rs2.ocx">
        <version>2.1.0.6</version>
        <hash method="sha1">77c1bc088b21f72181b9a67bfa412750d16acf36</hash>
        <size>636176</size>
    </assembly>
    <assembly name="vrmsaecc.ocx">
        <version>0.7.3.1</version>
        <hash method="sha1">fb0ccb10f0ffe6445d04ea4311eadbae341fe09f</hash>
        <size>1378200</size>
    </assembly>
    <assembly name="Sharing3/Sharing3.exe">
        <version>1.0.2.0</version>
        <hash method="sha1">d0e7188c6e28ccd2a99734a16a92f6e4cc1951ee</hash>
        <size>1156440</size>
    </assembly>
    <assembly name="Sharing3/vshooks.dll">
        <hash method="sha1">869289af554d57e38632df1fabac4b53b7d8fa8a</hash>
        <size>54616</size>
    </assembly>

    <file name="Sharing3/languages.xml">
        <hash method="sha1">d97d13ef2c4c4fd4e2d919e6333c694d39ca29fb</hash>
        <size>826</size>
    </file>

    <file name="Centre.swf"/>
    <file name="config.xml"/>
    <file name="language.xml"/>
    <file name="assets/en_US/skin.swf"/>
    <file name="assets/zh_CN/skin.swf"/>
    <file name="assets/ja_JP/skin.swf"/>

</deploy>
