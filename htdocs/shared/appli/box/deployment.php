<?php
require_once("classes/AppFrame.class.php");
require_once("classes/dbi/appli_file.dbi.php");

class AppFileDeploy extends AppFrame {
	
	var $obj_appliFile = null;

    function init () {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        //$this->dsn = $this->get_dsn();
        $this->obj_appliFile = new AppliFileTable($this->account_dsn);
        $this->_name_space = md5(__FILE__);
    }

    function default_view() {
    	$file_status = $this->request->get("type")?$this->request->get("type"):"released";
    	$user_agent  = $_SERVER['HTTP_USER_AGENT']; 
    	
    	$installer_fileInfo = null;
    	$binary_fileInfo = null;
    	
    	if(preg_match('/windows|win32/i', $user_agent)) {
    		$os_platform = "WIN";
    	} else if(preg_match('/macintosh|mac os x/i', $user_agent)) {
    		$os_platform = "MAC";
    	}
    	if($os_platform == "MAC") {
    		//todo:set static data
    		$deploy_xml = $this->template->fetch(N2MY_APP_DIR."templates/common/appli/box/deployment_static.t.xml");
    	} else {
    		if($file_status == "staging") {
    			$where_staging = "is_staging = 1 AND is_deleted = 0";
    			$sort_staging = array("create_datetime" => "asc");
    			$_tmpFile = $this->obj_appliFile->getRowsAssoc($where_staging, $sort_staging);
    			foreach ($_tmpFile as $file) {
    				if($file["file_type"] == "installer") {
    					$installer_fileInfo = $file;
    				} else if($file["file_type"] == "binary") {
    					$binary_fileInfo = $file;
    				}
    			}
    		} else if($file_status == "released") {
    			$where_released = "is_released = 1 AND is_deleted = 0";
    			$sort_released = array("update_datetime" => "asc");
    			$_tmpFile = $this->obj_appliFile->getRowsAssoc($where_released, $sort_released);
    			foreach ($_tmpFile as $file) {
    				if($file["file_type"] == "installer") {
    					$installer_fileInfo = $file;
    				} else if($file["file_type"] == "binary") {
    					$binary_fileInfo = $file;
    				}
    			}
    		}
    		//$this->logger2->info($installer_fileInfo);
    		//$this->logger2->info($binary_fileInfo);
    		$this->template->assign("file_location", N2MY_BASE_URL.APPLICATION_FILES_URL.$file_status."/");
    		$this->template->assign("updater", $installer_fileInfo);
    		$this->template->assign("application", $binary_fileInfo);
    		$deploy_xml = $this->template->fetch(N2MY_APP_DIR."templates/common/appli/box/deployment.t.xml");
    	}
    	
    	print $deploy_xml;
    	return ;
    }
    
}
$main =& new AppFileDeploy();
$main->execute();