<?php
require_once('../../../../config/config.inc.php');
header("Content-Type: text/xml; charset=UTF-8;");
print '<?xml version="1.0" encoding="UTF-8"?>'."\n";
print '<result>'."\n";
print '<minimum_version>1.0.1.36</minimum_version>'."\n";
print '<checker_url>'.N2MY_BASE_URL.'services/tools/checker/</checker_url>'."\n";
print '<meeting_api_url>'.N2MY_BASE_URL.'api/v1/</meeting_api_url>'."\n";
print '<requirements_url>http://www.nice2meet.us/ja/requirements/meeting.html</requirements_url>'."\n";
print '</result>';
?>