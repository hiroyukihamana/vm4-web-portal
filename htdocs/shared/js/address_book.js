function confirmDelete() {
    return (confirm(_(3294, '削除してよろしいですか？')));
}

function all_select(type) {
    var id = new Array();
    var id_index = 0;
    for(i = 0; i < document.list_form.length; i++) {
        if (document.list_form[i].type == "checkbox") {
            document.list_form[i].checked = true;
            id[id_index++] = document.list_form[i].value;
        }
    }
    ajax_select_all(id, type, 1);
}

function all_clear(type) {
    var id = new Array();
    var id_index = 0;
    for(i = 0; i < document.list_form.length; i++) {
        if (document.list_form[i].type == "checkbox") {
            document.list_form[i].checked = false;
            id[id_index++] = document.list_form[i].value;
        }
    }
    ajax_select_all(id, type, 0);
}

function form_clear() {
    for(i = 0; i < document.search_form.length; i++) {
        if (document.search_form[i].type == "checkbox") {
            document.search_form[i].checked = false;
        } else if (document.search_form[i].type == "text") {
            document.search_form[i].value = "";
        } else if (document.search_form[i].type == "select-one") {
            document.search_form[i].selectedIndex = 0;
        }
    }
}

function edit(key) {
    location.href = "?action_form&key="+key;
}

function del(key) {
    if (confirm(_(3295, "削除します。よろしいですか？"))) {
        location.href = "?action_delete&key[]="+key;
    }
}

function select_delete(frm) {
    if (confirm(_(3296, "選択したアドレスを全て削除します。よろしいですか？"))) {
        frm._action.name = "action_delete";
        frm.submit();
    }
}

function add_invite(mode) {
    var mySelectedAddress = Array();
    var mtAddressFrom = document.list_form;
    for (i = 0; i < mtAddressFrom.elements.length; i++) {
        if (mtAddressFrom.elements[i].name == 'key[]') {
            if (mtAddressFrom.elements[i].checked == true) {
                mySelectedAddress.push(mtAddressFrom.elements[i].value);
            }
            if(mySessionAddressBookList && mode != "sales") {
                for(j = 0; j < mySessionAddressBookList.length; j++) {
                    if(mySessionAddressBookList[j].key == mtAddressFrom.elements[i].value){
                        mySessionAddressBookList.splice(j,1);
                    }
                }
            }
        }
    }
    var myType = "";
    if ($("addType")) {
        myType = $F("addType");
    }
    for(i = 0; i < myAddressBookList.length; i++) {
        if (mySelectedAddress.indexOf(myAddressBookList[i].key) != -1) {
            if(mode == "sales") {
                if (opener.window.add_row_sales(
                myAddressBookList[i].name,
                myAddressBookList[i].email,
                myAddressBookList[i].timezone,
                myAddressBookList[i].lang,
                1) == false) {
                    break;
                }
            } else {
                if (opener.window.add_row(
                myAddressBookList[i].name,
                myAddressBookList[i].email,
                myAddressBookList[i].member_key,
                myType,
                myAddressBookList[i].timezone,
                myAddressBookList[i].lang,
                1) == false) {
                    break;
                }
            }
        }
    }
    if(mode != "sales") {
        if(mySessionAddressBookList) {
            for(i = 0; i < mySessionAddressBookList.length; i++) {
                if (opener.window.add_row(
                mySessionAddressBookList[i].name,
                mySessionAddressBookList[i].email,
                mySessionAddressBookList[i].member_key,
                myType,
                mySessionAddressBookList[i].timezone,
                mySessionAddressBookList[i].lang,
                1) == false) {
                    break;
                }
            }
        }
    }
    window.close();
}

function save() {
    // client
    if (!check()) {
        alert(_(3297, "入力エラー"));
        return false;
    }
    // server
    var url = '/services/address/index.php';
    var pars = jQuery("#input_form").serialize();
    jQuery.ajax({
            type: 'POST',
            url: url,
            data: pars,
            success: resultAction
        });
}

function resultAction(request) {
    jQuery(".err_msg").html("");
    json = eval("(" + request + ")");
    if (json["status"]) {
        location.href="?action_address_list";
    } else {
        var errors = json["data"]["errors"];
        for(i = 0; i<errors.length; i++) {
            jQuery('#err_' + errors[i]["field"]).html(errors[i]["err_msg"]);
        }
        alert(_(3297, "入力エラー"));
    }
}

function check() {
    return true;
}

function ajax_send_check(check,aid,type){
    j$.ajax({
        type: "POST",
        url:"/services/address/",
        data:{action_check_address:"", id:aid, check_status:check, type:type},
        success:function(result){
        }
    });
}

function ajax_select_all(id, type, select_flag){
    j$.ajax({
        type: "POST",
        url:"/services/address/",
        data:{
            action_select_all:"", 
            id : id, 
            type : type,
            select_flag : select_flag},
        success:function(result){
        }
    });
}