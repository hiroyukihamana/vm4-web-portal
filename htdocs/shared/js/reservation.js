jQuery.noConflict();
var j$ = jQuery;


var myTableHandler = (function () {
    return function (e) {
        var t = e.target || e.srcElement;
        var c = e.currentTarget || this;
        var r;
        var tb;

        if ('INPUT' == t.tagName && 'button' == t.type) {
            if (t.name == 'del') {
                r = t.parentNode.parentNode;
                c.deleteRow(r.rowIndex);
            } else if (t.name == 'invite_del') {
                r = t.parentNode.parentNode;
                c.deleteRow(r.rowIndex);
            } else if (t.name == 'up') {
                r = t.parentNode.parentNode;
                tb = r.parentNode;
                if (r.rowIndex > 1) {
                    tb.insertBefore(tb.rows[r.rowIndex - 1], tb.rows[r.rowIndex - 2]);
                }
            } else if (t.name == 'down') {
                r = t.parentNode.parentNode;
                tb = r.parentNode;
                if (r.rowIndex < tb.rows.length) {
                    tb.insertBefore(tb.rows[r.rowIndex], tb.rows[r.rowIndex - 1]);
                }
            }
        }
    };
})();

function inviteCheck(myInvite) {
    if (!myInvite["name"]) {
        $("name").focus();
        return false;
    }
    if (!myInvite["email"]) {
        $("email").focus();
        return false;
    } else {
        var myEmail = $F("email");
        if (!validEmail(myEmail)) {
            alert(gettext(3268, "招待する相手のメールアドレスが不正です。"));
            $("email").focus();
            return false;
        }
    }
    return true;
}

function keyEnter(event) {
    if (event.keyCode == 13) {
        add_row_form();
        Event.stop(event);
        return;
    }
}

function add_row_form() {
    if (!$("guest_on")) {
        return false;
    }
    var myInvite = new Array();
    myInvite["name"]     = $F("name");
    myInvite["email"]    = $F("email");
    myInvite["member_key"]    = $F("member_key");
    if ($('type')) {
        myInvite["type"] = $F("type");
    } else {
        myInvite["type"] = "";
    }
    myInvite["timezone"] = $F("timezone");
    myInvite["lang"]     = $F("lang");
    add_row(myInvite["name"], myInvite["member_key"], myInvite["email"], myInvite["type"], myInvite["timezone"], myInvite["lang"]);
    $("name").value = "";
    $("email").value = "";
}


function add_storage_row(id, name) {

    var doc_row = "<tr>";
    doc_row += "<td>"+name+"</td>";
    doc_row += "<td><input type='hidden' name='storage_file_id[]' value='"+id+"'>";
    doc_row += "<input type='text' name='storage_file_name[]' class='inputDoc' value=\""+name+"\"></td>";
    doc_row += "<td class='td-edit'>" + _(3287, '<input type="button" name="del" value="" class="del_btn" />') +"</td>";
    doc_row += "</tr>";
    j$("#storageDocTableBody").append(doc_row);
}

function add_row(name, email, member_key, type, timezone, lang, flg) {
    var table = $('table1');
    var myMax = 50;
    if (table.rows.length >= myMax) {
        alert(gettext(3269, "最大%s人まで招待可能です", myMax));
        return false;
    }
    if (flg) {
        form1 = $("reservationForm");
        var myNames = document.getElementsByName("guests[name][]");
        var myEmails = document.getElementsByName("guests[email][]");
        var myMemberKeys = document.getElementsByName("guests[member_key][]");
        var myTypes = document.getElementsByName("guests[type][]");
        var myLangs = document.getElementsByName("guests[lang][]");
        var myTimezones = document.getElementsByName("guests[timezone][]");
        var getstsLength = 0;
        for(i = 0; i < myEmails.length; i++) {
            if ((myEmails[i].value == "") && (myNames[i].value == "")) {
                myNames[i].value = name;
                myEmails[i].value = email;
                myMemberKeys[i].value = member_key;
                if (type) myTypes[i].value = type;
                if (timezone) myTimezones[i].value = timezone;
                if (lang) myLangs[i].value = lang;
                return true;
            }
        }
    }
    var myItem, myOption, myLabel, myElemId;
    new_row = table.insertRow(table.rows.length);
    var i = 0;
    if (!$("add_invite")) {
        new_row.insertCell(i).innerHTML = '<input type="hidden" name="guests[r_user_session][]"><input type="hidden" name="guests[member_key][]" value="'+ member_key +'">'+
                                          '<input type="text" class="input-name disabled" name="guests[name][]" value="'+ name +'" readonly="readonly">';
    } else {
        new_row.insertCell(i).innerHTML = '<input type="hidden" name="guests[r_user_session][]"><input type="hidden" name="guests[member_key][]" value="'+ member_key +'">'+
                                          '<input type="text" class="input-name" name="guests[name][]" value="'+ name +'">';
    }
    i++;
    if (!$("add_invite")) {
        new_row.insertCell(i).innerHTML = '<input type="text" class="input-mail disabled" name="guests[email][]" value="'+email+'" readonly="readonly">';
    } else {
        new_row.insertCell(i).innerHTML = '<input type="text" class="input-mail" name="guests[email][]" value="'+email+'">';
    }
    i++;
    myItem = "";
    myElemId = "lang";
    for(j = 0; j < $(myElemId).options.length; j++) {
        var myOption = $(myElemId).options[j];
        myItem = myItem + '<option value="' + myOption.value + '"';
        if (myOption.value == lang) {
            myItem = myItem + ' selected="selected"';
        }
        myItem = myItem + ">" + myOption.text + '</option>';
    }
    new_row.insertCell(i).innerHTML = '<select name="guests['+myElemId+'][]">' + myItem + '</select>';
    i++;
    myItem = "";
    myElemId = "timezone";
    for(j = 0; j < $(myElemId).options.length; j++) {
        var myOption = $(myElemId).options[j];
        myItem = myItem + '<option value="' + myOption.value + '"';
        if (myOption.value == 100) {
            myLabel = _(3271,'予約者と同じ');
        } else {
            if (myOption.value >= 0) {
                myLabel = 'GMT +' + myOption.value;
            } else {
                myLabel = 'GMT ' + myOption.value;
            }
        }
        if (myOption.value == timezone) {
            myItem = myItem + ' selected="selected"';
        }
        myItem = myItem + ">" + myLabel + '</option>';
    }
    new_row.insertCell(i).innerHTML = '<select name="guests['+myElemId+'][]">' + myItem + '</select>';
    i++;
    if ($('type')) {
        myItem = "";
        myElemId = "type";
        for(j = 0; j < $(myElemId).options.length; j++) {
            var myOption = $(myElemId).options[j];
            myItem = myItem + '<option value="' + myOption.value + '"';
            if (myOption.value == type) {
                myItem = myItem + ' selected="selected"';
            }
            myItem = myItem + ">" + myOption.text + '</option>';
        }
        new_row.insertCell(i).innerHTML = '<select name="guests['+myElemId+'][]">' + myItem + '</select>';
        i++;
    }
    new_row.insertCell(i).innerHTML = _(3272,'<input type="button" name="invite_del" value="削除" class="bt70-x" />');
}

function add_row_sales(name, email, timezone, lang) {
    j$("input[name='guest[name]']").val(name);
    j$("input[name='guest[email]']").val(email);
    j$("select[name='guest[lang]']").val(lang);
    j$("select[name='guest[timezone]']").val(timezone);
}

function findText(elemID, value) {
    var myElem = $(elemID);
    for(i = 0; i < myElem.length; i++) {
        if(myElem.options[i].value == value) {
            return myElem.options[i].text;
        }
    }
}

function list_view() {
    document.mailForm._action.name = "action_top";
    document.mailForm.submit();
}

function roomChange() {
    document.mailForm._action.name = "action_change_room";
    document.mailForm.submit();
}

function form_confirm(action) {
    if (!reservation_form_check(action)) {
        return false;
    }
    document.mailForm._action.name = "action_"+action;
    if ($("guest_on")) {
        $("name").disabled = false;
        $("email").disabled = false;
    }
    var mySort = $$(".document_sort");
    if (0 < mySort.length) {
      for (i = 0; i < mySort.length; i++) {
        mySort[i].value = i + 1;
      }
    }
    document.mailForm.submit();
}

function reservation_form_check(action) {
    if ($("reservationName")) {
        if (!$F("reservationName")) {
            alert(_(3273, "会議名が入力されていません"));
            $("reservationName").focus();
            return false;
        }
    }
    if ($("pass_on")) {
        if ($F("pass_on") && $F("pass_change")) {
            if (!$F("reservationPw")) {
                alert(_(3274, "会議のパスワードが入力されていません。"));
                $("reservationPw").focus();
                return false;
            }
            if (!$F("reservationPwConfirm")) {
                alert(_(9064, "確認用パスワードが入力されていません。"));
                $("reservationPwConfirm").focus();
                return false;
            }
            if (!$F("reservationPw") != !$F("reservationPwConfirm")) {
                alert(_(3275, "確認用パスワードが一致しません。"));
                $("reservationPw").focus();
                return false;
            }
        }
    }
    if ($("guest_on")) {
        if ($F("guest_on")) {
            if(j$("#sales-staff-table").length){
                //セルースモード
                var guestName = j$("input[name='guest[name]']").val();
                var guestEmail = j$("input[name='guest[email]']").val();
                var staffName = j$("input[name='staff[name]']").val();
                var staffEmail = j$("input[name='staff[email]']").val();
                if(guestName == "") {
                    alert(_(3278, "招待する相手の名前が入力されていません。"));
                    j$("input[name='guest[name]']").focus();
                    return false;
                } else if(guestEmail == "") {
                    alert(_(3279, "招待する相手のメールアドレスが入力されていません。"));
                    j$("input[name='guest[email]']").focus();
                    return false;
                } else if(!validEmail(guestEmail)) {
                    alert(_(3277, "招待する相手のメールアドレスが不正です。"));
                    j$("input[name='guest[email]']").focus();
                    return false;
                } else if(staffName == "") {
                    alert(_(9456, "スタッフの名前が入力されていません。"));
                    j$("input[name='staff[name]']").focus();
                    return false;
                } else if(staffEmail == "") {
                    alert(_(9457, "スタッフのメールアドレスが入力されていません。"));
                    j$("input[name='staff[email]']").focus();
                    return false;
                } else if(!validEmail(staffEmail)) {
                    alert(_(9458, "スタッフのメールアドレスが不正です。"));
                    j$("input[name='staff[email]']").focus();
                    return false;
                }

                // MTGVFOUR-827 セールスモード（監視者）
                if ($F("manager_on")) {
                    var managerName = j$("input[name='manager[name]']").val();
                    var managerEmail = j$("input[name='manager[email]']").val();
                    if(managerName == "") {
                        alert(_(9462, "監視者の名前が入力されていません。"));
                        j$("input[name='manager[name]']").focus();
                        return false;
                    } else if(managerEmail == "") {
                        alert(_(9463, "監視者のメールアドレスが入力されていません。"));
                            j$("input[name='manager[email]']").focus();
                                  return false;
                    } else if(!validEmail(managerEmail)) {
                        alert(_(9464, "監視者のメールアドレスが不正です。"));
                        j$("input[name='manager[email]']").focus();
                        return false;
                    }
                }

            } else {
                form1 = $("reservationForm");
                if($F("authority_on") == 1){
                    if (!$F("authority_name")) {
                        alert(_(9459, "議長の名前が入力されていません。"));
                        $("authority_name").focus();
                        return false;
                    }
                    if (!$F("authority_mail")) {
                        alert(_(9460, "議長のアドレスが入力されていません。"));
                        $("authority_mail").focus();
                        return false;
                    } else {
                        email = $F("authority_mail");
                        if (!validEmail(email)) {
                            alert(_(9461, "議長のメールアドレスが不正です。"));
                            $("authority_mail").focus();
                            return false;
                        }
                    }
                }
                var myNames = document.getElementsByName("guests[name][]");
                var myEmails = document.getElementsByName("guests[email][]");
                var getstsLength = 0;
                for(i = 0; i < myNames.length; i++) {
                    if(myNames[i].value != "") {
                        if(myEmails[i].value == "") {
                            alert(_(3279, "招待する相手のメールアドレスが入力されていません。"));
                            myEmails[i].focus();
                            return false;
                        } else if(!validEmail(myEmails[i].value)) {
                            alert(_(3277, "招待する相手のメールアドレスが不正です。"));
                            myEmails[i].focus();
                            return false;
                        } else {
                            getstsLength++;
                        }

                    }else {
                        if(myEmails[i].value != ""){
                            alert(_(3278, "招待する相手の名前が入力されていません。"));
                            myNames[i].focus();
                            return false;
                        }
                    }
                }
            }
            if (!$("mail_send_modify") && getstsLength == 0) {
                alert(_(3278, "招待する相手の名前が入力されていません。"));
                myNames[0].focus();
                return false;
            }
            if (!$F("sender")) {
                alert(_(3280, "差出人（あなたのお名前）が入力されていません。"));
                $("sender").focus();
                return false;
            }
            if (!$F("senderMail")) {
                alert(_(3281, "送信元（あなたのメールアドレス）が入力されていません。"));
                $("senderMail").focus();
                return false;
            } else {
                email = $F("senderMail");
                if (!validEmail(email)) {
                    alert(_(3282, "送信元（あなたのメールアドレス）が不正です。"));
                    $("senderMail").focus();
                    return false;
                }
            }
            if($F("organizer_on") == 1){
              if (!$F("organizer_name")) {
                  alert(_(8593, "主催者の名前が入力されていません。"));
                  $("organizer_name").focus();
                  return false;
              }
              if (!$F("organizer_mail")) {
                  alert(_(8594, "主催者のアドレスが入力されていません。"));
                  $("organizer_mail").focus();
                  return false;
              } else {
                  email = $F("organizer_mail");
                  if (!validEmail(email)) {
                      alert(_(8595, "主催者のメールアドレスが不正です。"));
                      $("organizer_mail").focus();
                      return false;
                  }
              }
            }

        }
    }


    var myDocuments = $$(".inputDoc");
    var myMax = 20;
    var table = $('storageDocTableBody');
    if(table){
      if(myMax < table.rows.length){
        alert(_(8515, "ストレージから登録できるファイル数は%s個までです", myMax));
          return false;
      }
    }
    if (0 < myDocuments.length) {
      if (action == "modify_confirm") {
          for (var i = 0; i < myDocuments.length; i++) {
              if (myDocuments[i].value == "" && myDocuments[i].name != "filename[]") {
                alert(_(8514, "ファイル名を入力してください"));
                return false;
              }
          }
      }

        for (var i = 0; i < myDocuments.length; i++) {
            if (myDocuments[i].value.match(/[\\\/=*?\"|:{}<>]+/g)) {
                alert(_(8488, "ファイル名に(　\\ \/ : * ? \" < > |　)は使えません。"));
                return false;
            }
        }

    }

    return true;
}

function add_document(e) {
    if ($('docTableBody')) {
        var table = $('docTableBody');
        var myMax = 20;
        var myDocumentItem;
        if (e) {
            if (table.rows.length >= myMax) {
                alert(_(3284, "パソコンから登録できるファイルは%s個までです", myMax));
                return false;
            }
            new_row = table.insertRow(table.rows.length);
            //new_row.insertCell(0).innerHTML = '<input type="button" name="up" value="↑" class="bt-up" /><input type="button" name="down" value="↓" class="bt-down" />';
            if ($('document_vector')) {
                myDocumentItem = '<select id="format" name="format[]"><option value="bitmap">'+_(6657, '通常')+'</option><option value="vector">'+_(6658,'高画質')+'</option></select>';
            } else {
                myDocumentItem = '<input type="hidden" name="format[]" class="format" />';
            }
            new_row.insertCell(0).innerHTML = '<input type="file" name="document[]" size="25" class="input-file" />' + myDocumentItem;
            new_row.insertCell(1).innerHTML = '<input type="hidden" name="sort[]" class="document_sort" /><input type="text" name="filename[]" size="10" class="inputDoc" />';
            new_row.insertCell(2).innerHTML = _(3287, '<input type="button" name="del" value="削除" class="bt70-x" />');
        }
    }
}

function schedule_list(frm) {
    if ($("start_cal")) {
        var myStartDate = new Date($F('start_cal')+' '+$F("startHour")+':'+$F("startMinute")+':00');
        var myEndDate = new Date($F('end_cal')+' '+$F("endHour")+':'+$F("endMinute")+':00');
        var myDays = Math.floor((myEndDate - myStartDate) / (24*60*60*1000)) + 1;
        var myMonth = myStartDate.getMonth() + 1;
        params = "action_schedule_list="
            +"&reservation_place=" + $F( "timeDifference" )
            +"&room_key=" + $F("roomKey")
            +"&s_year="  + myStartDate.getFullYear()
            +"&s_month=" + myMonth
            +"&s_day="   + myStartDate.getDate()
            +"&days=" + myDays
            +"&cashbuster=" + new Date().getTime();
        var httpObj = new Ajax.Updater("timeTable", "/services/api.php",
                    {
                    method: 'get',
                    parameters: params
                }
        );
    }
}

    function documentStatus() {
        var httpObj = new Ajax.Request('/services/reservation/index.php',
            {
                method: "get",
                parameters: "action_wb_status=?"
                    +"&mode=" + reservation_mode + "&cashbuster=" + new Date().getTime(),
                onSuccess: function(myResponse) {
                    var myResponseText = myResponse.responseText;
                    parseDocument(myResponseText);
                }
            }
        );
    }

    function parseDocument(json_data) {
        var meetingData = eval("(" + json_data + ")");
        var myWaitCnt = 0;
        for(var i = 0; i < meetingData.length; i++) {
            viewDocument(meetingData[i]);
            if (meetingData[i].status == 0 || meetingData[i].status == 1) {
                myWaitCnt++;
            }
        }
        if (myWaitCnt > 0) {
            setTimeout("documentStatus()", 10000);
        }
    }

    function viewDocument(myDocument) {
        document_id = myDocument.document_id;
        var myDocumentHTML = "";
        var myStatus = "";
        switch (myDocument.status) {
        case "-1":
            myStatus = _(3288, '<span class="error">変換エラー</span>');
            break;
        case "0":
            myStatus = _(3289, '<span class="uploading">変換待ち</span>');
            break;
        case "1":
            myStatus = _(3290, '<span class="pending">変換中</span>');
            break;
        case "2":
            myStatus = _(3291, '<span class="done">変換完了</span>');
            break;
        case "3":
            myStatus = _(3292, '<span class="done">削除済み</span>');
            break;
        default:
            myStatus = _(3293, '<span class="error">変換エラー</span>');
            break;
        }
        $(document_id).innerHTML = myStatus;
    }


function openTimeMap( id ) {
    var dw = window.open( "/service/reservation/?mode=gmt&no=" + id, "TimeMapWindow", "width=459, height=412, resizable=no, location=no, menubar=no, scrollbars=no, status=no, toolbar=no" );
    dw.moveTo( (screen.availWidth-459) / 2, (screen.availHeight-412)/2 );
}

function setTimeZone( id, zone ) {
    var time = GmtTable.getTime( zone );
    for ( var i = 0; i < document.mailForm[ id ].length; i++ ) {
        if ( document.mailForm[ id ].options[ i ].value == time.toString() ) {
            document.mailForm[ id ].selectedIndex = i;
            break;
        }
    }
}

function displayUserList() {
    el = $('invitationSection');
    if (el) {
        if (on) {
            el.style.visibility = 'visible';
            el.style.display    = 'block';
        }
        else {
            el.style.visibility = 'hidden';
            el.style.display    = 'none';
        }
    }
}

function displayPwForm() {
    if ($("pass_on")) {
        if ($F("pass_on") == 1) {
            //j$("#pwForm").slideDown("fast");
            j$("#pwForm").show();
        }
        else {
//            j$("#pwForm").slideUp("fast");
            j$("#pwForm").hide();
        }
    }
}

function displayGuestSec() {
    if ($("guest_on")) {
        if ($F("guest_on")) {
//            j$("#guest-col").slideDown("fast");
          j$("#mail-resend-sec").show();
            j$("#guest-col").show();
            j$("#reminder-sec").show();
            j$("#mail-type-sec").show();
        }
        else {
//            j$("#guest-col").slideUp("fast");
          j$("#mail-resend-sec").hide();
            j$("#guest-col").hide();
            j$("#reminder-sec").hide();
            j$("#mail-type-sec").hide();
        }
    }
}

function displayUploadSec() {
    if ($("upload_on")) {
        if ($F("upload_on")) {
//            j$("#upload-sec").slideDown("fast");
            j$("#upload-sec").show();
        }
        else {
//            j$("#upload-sec").slideUp("fast");
            j$("#upload-sec").hide();
        }
    }
}
//20130311 山口追加
function displayMailSendSec() {
    if ($("mail_send_on")) {
        if ($F("mail_send_on")) {
            j$("#mail_send-cont").show();
            j$('input[name="mail_send_type"][value="2"]').attr("checked","checked");
        }
        else {
            j$("#mail_send-cont").hide();
            j$('input[name="mail_send_type"][value="0"]').attr("checked","checked");
        }
    }
}
function displayTeleconfSec() {
    if ($("teleconf_flg")) {
        if ($F("teleconf_flg")) {
            j$("#teleconf-sec").show();
        }
        else {
            j$("#teleconf-sec").hide();
        }
    }
}

//function displayClipSec() {
//    if ($("clip_on")) {
//        if ($F("clip_on")) {
//            j$("#clip-sec").show();
//        }
//        else {
//            j$("#clip-sec").hide();
//        }
//    }
//}

function displayLimitedFunctionSec() {
    if ($("limited_function_on")) {
        if ($F("limited_function_on")) {
//            j$("#limited_function-sec").slideDown("fast");
            j$("#limited_function-sec").show();
        }
        else {
//            j$("#limited_function-sec").slideUp("fast");
            j$("#limited_function-sec").hide();
        }
    }
}
function displayTcEtcNoteInput() {
   var selectedTc = $$('input[name="info[tc_type]"]:checked');

   if (0 == selectedTc.length) {
        return false;
   }

   if (selectedTc[0].value == 'etc') {
        $('voice_conference_info').show();
   } else {
        $('voice_conference_info').hide();
   }

   if (selectedTc[0].value == 'pgi') {
        $('pgi_info').show();
   } else {
        $('pgi_info').hide();
   }
}

function openSubWindow(url, w, width, height)
{
    p = "width="+width+",height="+height+",directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no";
    window.open(url, w, p);
}

function changeCal(prefix) {
    var myFrm = document.mailForm;
    var myEndTime = new Date($F("endYear"),
        $F("endMonth") - 1,
        $F("endDay"),
        $F("endHour"),
        $F("endMinute"),
        0);
    var myStartTime = new Date($F("startYear"),
        $F("startMonth") - 1,
        $F("startDay"),
        $F("startHour"),
        $F("startMinute"),
        0);
    if (myStartTime.getTime() <= myEndTime.getTime()) {
        return true;
    }
    var mySetDate = new Date();
    if (prefix == "start") {
        mySetDate.setTime(myStartTime.getTime() + (3600*1000));
        var myHour = myFrm.endHour;
        var myMin = myFrm.endMinute;
    } else {
        mySetDate.setTime(myEndTime.getTime() - (3600*1000));
        var myHour = myFrm.startHour;
        var myMin = myFrm.startMinute;
    }
    var mm = mySetDate.getMonth() + 1;
    if ( mm < 10) mm = "" + "0" + mm;
    var dd = mySetDate.getDate();
    if ( dd < 10 ) dd = "" + "0" + dd;
    var myStrDate = mySetDate.getFullYear() + "/" + mm + "/" + dd;
    if (prefix == "start") {
        endCalObj.setFormValue(myStrDate);
    } else {
        startCalObj.setFormValue(myStrDate);
    }
    for (i = 0; i < myHour.length; i++) {
        if (myHour.options[i].value == mySetDate.getHours()) {
            myHour.options[i].selected = true;
        }
    }
    for (i = 0; i < myMin.length; i++) {
        if (myMin.options[i].value == mySetDate.getMinutes()) {
            myMin.selectedIndex = i;
        }
    }
}

function address_book() {
    var room_key = $F("roomKey");
    myWindow = window.open("../address/?reset=1&room_key="+room_key, "address_book", 'width=900, height=650, menubar=no, toolbar=no, scrollbars=yes');
    myWindow.moveTo((screen.availWidth-900)/2, (screen.availHeight-650)/2);
    myWindow.focus();
}
/*
function add_address_book() {
  var room_key = $F("roomKey");
    var guestNames = document.getElementsByName("guests[name][]");
    var guestEmails = document.getElementsByName("guests[email][]");
    var guestLangs = document.getElementsByName("guests[lang][]");
    var guestTimezones = document.getElementsByName("guests[timezone][]");
    var frm = document.createElement("form");
    frm.action = "../address/?action_add_address=&room_key="+room_key;
    frm.method = "post";
    frm.target = "action_add_address";
    var count = guestNames.length;
    for(i = 0; i < count; i++){
        var inputNames = document.createElement("input");
        inputNames.type = "hidden";
        inputNames.name = "guests[name][]";
        inputNames.value = guestNames[i].value;
        frm.appendChild(inputNames);
        var inputEmails = document.createElement("input");
        inputEmails.type = "hidden";
        inputEmails.name = "guests[email][]";
        inputEmails.value = guestEmails[i].value;
        frm.appendChild(inputEmails);
        var inputLang = document.createElement("input");
        inputLang.type = "hidden";
        inputLang.name = "guests[lang][]";
        inputLang.value = guestLangs[i].value;
        frm.appendChild(inputLang);
        var inputTimezone = document.createElement("input");
        inputTimezone.type = "hidden";
        inputTimezone.name = "guests[timezone][]";
        inputTimezone.value = guestTimezones[i].value;
        frm.appendChild(inputTimezone);
    }
    myWindow = window.open("about:blank", frm.target, 'width=800, height=650, menubar=no, toolbar=no, scrollbars=yes');
    myWindow.moveTo((screen.availWidth-800)/2, (screen.availHeight-650)/2);
    document.body.appendChild(frm);
    frm.submit();
    myWindow.focus();
    document.body.removeChild(frm);
}
*/
function shared_file_list() {
    var room_key = $F("roomKey");
    myWindow = window.open("../storage/?action_select_file", "shared_file_list", 'width=800, height=650, menubar=no, toolbar=no, scrollbars=yes');
    myWindow.moveTo((screen.availWidth-800)/2, (screen.availHeight-650)/2);
    myWindow.focus();
}

function timezone() {
    tWindow = window.open("../reservation/?action_timezone", "timezone", 'width=480, height=620, menubar=no, toolbar=no, scrollbars=yes');
    tWindow.moveTo((screen.availWidth-480)/2, (screen.availHeight-620)/2);
    tWindow.focus();
}
function openPGiDescription() {
    tWindow = window.open("../reservation/?action_pgi_description", "pgi_desc", 'width=480, height=620, menubar=no, toolbar=no, scrollbars=yes');
    tWindow.moveTo((screen.availWidth-480)/2, (screen.availHeight-620)/2);
    tWindow.focus();
}

function setTimezone(){
    var count = time_zone_list.length;
    var num = -1;
    var country = j$("#country").val().toLowerCase();
    var county_obj = new RegExp(country);
    var time_count = 0;
    var match_zone = new Array();
    j$("select[name='info[reservation_place]'] > option").remove();
    if(country == '' || country == ' '){
        j$("#timeDifference").html(timezome_dsp);
    }else{
        count = time_zone_list.length;
        for(var i = 0; i < count; i++){
            time_count = time_zone_list[i].length;
            for(var z = 0; z < time_count; z++){
                if (time_zone_list[i][z].toLowerCase().match(county_obj) ) {
                     match_zone.push(time_zone_list[i][0]);
                     break;
                 }
            }
            if(j$.inArray(country, time_zone_list[i]) != -1){
                num = i;
                //break;
            }
        }
        var match_zone_count = match_zone.length;
        for(i = 0; i < match_zone_count; i++){
            j$("select[name='info[reservation_place]']").append(j$('<option>').html(timezome_tmp[match_zone[i]]).val(match_zone[i]));
        }
    }
}

function togglePasswordIuputfield() {
    var change_flg = j$('input[name="change_flg"]:checked').val();
    if (change_flg == 1) {
        j$('div.passwordSection').css('display', 'block');
    } else if (change_flg == 2) {
        j$('div.passwordSection').css('display', 'none');
    } else {

    }
}

function displayOrgnaizerForm() {
    if ($("organizer_on").checked) {
        j$("#orgnaizer_form").show();
    }
    else {
        j$("#orgnaizer_form").hide();
    }
}

function displayAuthorityForm() {
    if ($("authority_on").checked) {
        j$("#authority_form").show();
    }
    else {
        j$("#authority_form").hide();
    }
}

function displayManagerForm() {
    if ($("manager_on").checked) {
        j$("#manager_form").show();
    }
    else {
        j$("#manager_form").hide();
    }
}

function salesRecFlg(){
  var val = j$('input:radio[name="info[is_rec_flg]"]:checked').val();
    if(val == undefined|| j$('input[name="info[is_auto_rec_flg]"]') == undefined){
       return;
    }
  if(val == 1){
    j$('input[name="info[is_auto_rec_flg]"]').attr("disabled", false);
  }else{
    j$('input[name="info[is_auto_rec_flg]"]').attr("disabled", 'disabled');
    j$('input[name="info[is_auto_rec_flg]"]').val(['0']);
  }
}

/**
 * 予約開始日時から予約終了日時までの空きポート数を返す
 * @param reservation_session 予約変更時のみ必要、編集対象のセッションID
 */

function view_restPort(room_key,reservation_session,port){
    j$("#view_restPortButton").attr("disabled", "true");
    j$("#view_restPortButton").val( _(9396, '更新中'));
    j$("#rest_port").html( _(9397, '計算処理中・・・'));
    var start_time, end_time;
    if( j$('#start_cal') ) {
    	start_time = parseInt(new Date(j$('#start_cal').val()+' '+j$("#startHour").val()+':'+j$("#startMinute").val()+':00').getTime()/1000);
    	end_time = parseInt(new Date(j$('#end_cal').val()+' '+j$("#endHour").val()+':'+j$("#endMinute").val()+':00').getTime()/1000);
    }
    // 予約可能ポート表示
    j$.ajax({
    	type:'GET',
        url:'/services/reservation/index.php?action_get_port&cb=' + new Date().getTime(),
        dataType : 'json',
    	data : {
        	room_key:room_key,
            reservation_session:reservation_session,
            start_time:start_time,
            end_time:end_time,
            time_diff:j$('#timeDifference').val()},
        success : function(rest_port) {
        	if(!rest_port || rest_port < 0) rest_port = 0;
        	var offset = j$('#select_max_port option:selected').val();
        	if(isFinite(port)){
        		offset  = Number(port);
        	}
            j$('#select_max_port').empty();
            for(var i = 2; i <= rest_port; i++) j$('#select_max_port').append(j$('<option>').val(i).text(i));
            if(!offset || offset<2 || Number(rest_port) < offset) offset = rest_port;
            j$('#select_max_port option:[value='+offset+']').attr('selected', true);
            j$("#rest_port").html( _(9394, '　あと<span class="attention"> %s </span>人予約ができます。',rest_port));
            j$("#view_restPortButton").val( _(9395, '空き状況更新'));
            j$("#view_restPortButton").attr("disabled", "");
        }
    });
}

//clear_timezone 20130412山口追加
j$(document).ready( function() {
        j$("#clear_timezone").click(function(){
            j$("#country").val("");
            j$("#timeDifference").html(timezome_dsp);
        });
        togglePasswordIuputfield();
});
j$(document).ready( function() {
   var dColor = '#999999';    //ヒント（初期値）の文字色
   var fColor = '#333333';    //通常入力時の文字色
    j$('#country').css('color',dColor);
  j$('#country').focus(function(){
    if(j$(this).val() == this.defaultValue || j$(this).val() == ""){
      j$(this).val('');
      j$(this).css('color', fColor);
    }
  })
  //フォーカスが外れたときの処理
  .blur(function(){
     if(j$(this).val() == this.defaultValue || j$(this).val() == ''){
       j$(this).val(this.defaultValue);
       j$(this).css('color',dColor);
    };
   });
  //
  salesRecFlg();
});

j$('input[name="change_flg"]').live("change", function(){
    togglePasswordIuputfield();
});

j$('input[name="info[is_rec_flg]"]').live("change", function(){
  salesRecFlg();
});

var post_flg = 0;

j$("div.document-list").find("input").live("click",function(){
    if(post_flg){
        return;
    }
    j$current = j$(this);
    var move = j$(this).attr("name");
    var maxMove = j$("div.document-list").length;
    var current_sort = parseInt(j$(this).closest("div.document-list").attr("sort"));
    var document_id = j$(this).closest("div.document-list").children("p").children("span.uploadDone").attr("id");
    var move_sort = null;
    if(move == "up") {
        move_sort = current_sort - 1;
    }else if(move == "down") {
        move_sort = current_sort + 1;
    } else {
        move_sort = 0;
    }

    if(move_sort > 0 && move_sort <= maxMove) {
        post_flg = 1;
        j$current = j$(this);
        j$.ajax({
            url: "/services/reservation/?action_change_document_sort",
            type: "post",
            data: { id: document_id, move: move},
            success: function(data){
                if(data == "1") {
                    var cur_sort_content = j$current.closest("div.document-list").html();
                    var mov_sort_content = j$("div.document-list[sort='"+move_sort+"']").html();
                    j$current.closest("div.document-list").html(mov_sort_content);
                    j$("div.document-list[sort='"+move_sort+"']").html(cur_sort_content);
                    post_flg = 0;
                }
            },
            error:function(){
                alert("failure");
                post_flg = 0;
            }
        });
    }
});
