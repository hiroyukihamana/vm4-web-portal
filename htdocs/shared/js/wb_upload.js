var myTableHandler = (function () {
    return function (e) {
        var t = e.target || e.srcElement;
        var c = e.currentTarget || this;
        var r;
        var tb;

        if ('INPUT' == t.tagName && 'button' == t.type) {
            if (t.name == 'del') {
/*
                if (!confirm("削除します。よろしいですか？")) {
                    return false;
                }
*/
                r = t.parentNode.parentNode;
                c.deleteRow(r.rowIndex);
            } else if (t.name == 'invite_del') {
                r = t.parentNode.parentNode;
                c.deleteRow(r.rowIndex);
            } else if (t.name == 'up') {
                r = t.parentNode.parentNode;
                tb = r.parentNode;
                if (r.rowIndex > 1) {
                    tb.insertBefore(tb.rows[r.rowIndex - 1], tb.rows[r.rowIndex - 2]);
                }
            } else if (t.name == 'down') {
                r = t.parentNode.parentNode;
                tb = r.parentNode;
                if (r.rowIndex < tb.rows.length) {
                    tb.insertBefore(tb.rows[r.rowIndex], tb.rows[r.rowIndex - 1]);
                }
            }
        }
    };
})();

function shared_file_list() {
    var room_key = $F("room_key");
    myWindow = window.open("../storage/?action_select_file", "shared_file_list", 'width=800, height=650, menubar=no, toolbar=no, scrollbars=yes');
    myWindow.moveTo((screen.availWidth-800)/2, (screen.availHeight-650)/2);
    myWindow.focus();
}

var count = 0;

function add_storage_row(id, name, owner_mid) {
	var addMax = 10;
    var doc_row = '<tr><input type="hidden" name="sort[]" class="document_sort" />';
    doc_row += '<td><input type="button" name="up" value="↑" class="bt-up" /><input type="button" name="down" value="↓" class="bt-down" /></td>';
    doc_row += "<td>"+name+"</td>";
    doc_row += "<td><input type='hidden' name='storage_file_id[]' value='"+id+"'>";
    doc_row += '<input type="hidden" name="sort['+count+']" class="document_sort" />';
    doc_row += "<input type='text' name='storage_file_name[]' class='input-name' value=\""+name+"\"></td>";
    doc_row += '<td class="td-edit"><input type="button" name="del" value="' + _(8430, '取消') + '" class="bt70-x" />' +"</td>";
    doc_row += "<td></td>";
    doc_row += "</tr>";
    j$("#storageDocTableBody").append(doc_row);
    count++;
}

function keyEnter(event) {
    if (event.keyCode == 13) {
        add_row_form();
        Event.stop(event);
        return;
    }
}

function list_view() {
    document.mailForm._action.name = "action_top";
    document.mailForm.submit();
}

function form_confirm(action) {
	var myDocuments = $$(".input-name");
	if (0 < myDocuments.length) {
	    for (var i = 0; i < myDocuments.length; i++) {
	        if (myDocuments[i].value == "") {
	          alert(_(8514, "ファイル名を入力してください"));
	          return false;
	        }
	    }
        for (var i = 0; i < myDocuments.length; i++) {
            if (myDocuments[i].value.match(/[\\\/=*?\"|:{}<>]+/g)) {
                alert(_(8488, "ファイル名に(　\\ \/ : * ? \" < > |　)は使えません。"));
                return false;
            }
        }
	}
	if(window.confirm(_(8431, "変更しますか？"))){
		document.mailForm._action.name = "action_"+action;
    	var mySort = $$(".document_sort");
    	for(i = 0; i < mySort.length; i++) {
        	mySort[i].value = i + 1;
    	}
    	document.mailForm.submit();
	}
}

function delete_clip_confirm(clip_key){
	if(window.confirm(_(7425, "削除しますか？"))){
		obj = document.createElement('input');
		obj.name = "delete_clip";
		obj.value = clip_key;
		obj.type = "hidden";
		document.mailForm.appendChild(obj);
		document.mailForm._action.name = "action_delete_clip";
        document.mailForm.submit();
	}
}

function delete_confirm(document_id) {
	if(window.confirm(_(7425, "削除しますか？"))){
		obj = document.createElement('input');
		obj.name = "delete_document";
		obj.value = document_id;
		obj.type = "hidden";
		document.mailForm.appendChild(obj);
		document.mailForm._action.name = "action_delete_document";
        document.mailForm.submit();
	}
}

function add_document(e) {
    if ($('docTableBodyAdd')) {
        var table = $('docTableBody');
        var table_add = $('docTableBodyAdd');
        if(!table) {
            var now_row = table_add.rows.length;
        }else{
            var now_row = table.rows.length + table_add.rows.length;
        }
        var myMax = 150;
        var addMax = 10;
        if (e) {
            if (now_row >= myMax) {
                alert("最大" + myMax + "ファイルまで登録可能です");
                return false;
            }
            if (table_add.rows.length >= addMax) {
                alert("一度に" + addMax + "ファイルまで送信可能です");
                return false;
            }
            //new_row = table.insertRow(table.rows.length);
            new_row = table_add.insertRow(table_add.rows.length);
            var cell01 = new_row.insertCell(0);
            cell01.setAttribute("class","td2");
            cell01.innerHTML = '<input type="button" name="up" value="↑" class="bt-up" /><input type="button" name="down" value="↓" class="bt-down" />';
            new_row.insertCell(1).innerHTML = '<input type="file" name="document[]" size="35" class="inputFile" /><select name="format[]" id="format" class="quality"><option value="bitmap">通常</option><option value="vector">高画質</option></select>';
            new_row.insertCell(2).innerHTML = '<input type="hidden" name="sort[]" class="document_sort" /><input type="text" name="filename[]" size="10" class="inputText" />';
            new_row.insertCell(3).innerHTML = '<input type="button" name="del" value="取消" class="bt70-x" />';

        }
    }
}


    function documentStatus() {
        var httpObj = new Ajax.Request('/services/room/wb_upload.php',
            {
                method: "get",
                parameters: "action_wb_status="
                    +"&room_key=" + $("room_key").value
                    +"&cashebuster=" + new Date().getTime(),
                onSuccess: function(myResponse) {
                    var myResponseText = myResponse.responseText;
                    parseDocument(myResponseText);
                }
            }
        );
    }

    function parseDocument(json_data) {
        var meetingData = eval("(" + json_data + ")");
        var myWaitCnt = 0;
        for(var i = 0; i < meetingData.length; i++) {
            viewDocument(meetingData[i]);
            if (meetingData[i].status == 0 || meetingData[i].status == 1) {
                myWaitCnt++;
            }
        }
        if (myWaitCnt > 0) {
            setTimeout("documentStatus()", 3000);
        }
    }

    function viewDocument(myDocument) {
        document_id = myDocument.document_id;
        var myDocumentHTML = "";
        var myStatus = "";
        switch (myDocument.status) {
        case "-1":
            myStatus = '<span class="error">変換エラー</span>';
            break;
        case "0":
            myStatus = '<span class="uploading">変換待ち</span>';
            break;
        case "1":
            myStatus = '<span class="pending">変換中</span>';
            break;
        case "2":
            myStatus = '<span class="done">変換完了</span>';
            break;
        case "3":
            myStatus = '<span class="done">削除済み</span>';
            break;
        default:
            myStatus = '<span class="error">変換エラー</span>';
            break;
        }
        $(document_id).innerHTML = myStatus;
    }


