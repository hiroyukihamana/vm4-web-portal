var dom = new Object();
	dom.event = new Object();
	
dom.event.addEventListener = function(elm, type, func, useCapture){
	if(!elm){ return false; }
	if(!useCapture){
		useCapture = false;
	}
	if(elm.addEventListener){
		elm.addEventListener(type, func, false);
	} else if(elm.attachEvent){
		elm.attachEvent('on'+type, func);
	} else {
		return false;
	}
	return true;
}
	
dom.event.removeEventListener = function(elm, type, func, useCapture){
	if(!elm){ return false; }
	if(!useCapture){
		useCapture = false;
	}
	if(elm.removeEventListner) {
		elm.removeEventListener(type, func, false);
	} else if(elm.detachEvent) {
		elmdetachEvent('on'+type, func);
	} else {
		return false;
	}
	return true;
}

dom.event.target = function(evt){
	if(evt && evt.target){
		if(evt.target.nodeType == 3){
			return evt.target.parentNode;
		} else {
			return evt.target;
		}
	} else if(window.event && window.event.srcElement){
		return window.event.srcElement;
	} else {
		return null;
	}
}