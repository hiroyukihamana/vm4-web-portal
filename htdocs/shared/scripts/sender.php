<?php
require_once("classes/AppFrame.class.php");
class AppSharing2 extends AppFrame {

    function init() {
    }

    function default_view() {
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/core/dbi/SharingServer.dbi.php';
        $sharing = new DBI_SharingServer(N2MY_MDB_DSN);
        $meeting = new MeetingTable($this->get_dsn());
        $meeting_key = $this->session->get("meeting_key");
        $where = "meeting_key = '".addslashes($meeting_key)."'";
        $sharing_server_key = $meeting->getOne($where, "sharing_server_key");
        if (DB::isError($sharing_server_key)) {
            $this->logger2->error($sharing_server_key->getUserInfo());
        }
        $where = "server_key = '".addslashes($sharing_server_key)."'";
        $sharing_server = $sharing->getOne($where, "server_address");
        if (DB::isError($sharing_server)) {
            $this->logger2->error($sharing_server->getUserInfo());
        } else {
            $this->logger2->info($sharing_server);
        }
        $frame["lang"] = $this->_lang;
        $this->template->assign("__frame",$frame);
        $this->template->assign("sharing_server", $sharing_server);
        $template_path = "common/sharing/scripts/sender.t.js";
        $output = $this->fetch($template_path);
        header("Content-Type: application/x-javascript; charset=utf-8");
        print $output;
    }
}
$main = new AppSharing2();
$main->execute();
?>