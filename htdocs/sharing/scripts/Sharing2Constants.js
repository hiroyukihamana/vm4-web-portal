//
// "N2MY Sharing2" Constants & Functions
//

function _Constants() {

    // アプリケーションモード
    //////////////////////////////////////
    this.APP_MODE_SENDER				= 0;
    this.APP_MODE_VIWER					= 1;
    this.APP_MODE_CONTROLLER			= 2;

    // PNGの圧縮率
    //////////////////////////////////////
    this.COMP_LV_HIGHEST				= 9;	// "High CPU & Low Band" Usage
    this.COMP_LV_HIGH					= 7;
    this.COMP_LV_NORMAL					= 5;
    this.COMP_LV_LOW					= 3;
    this.COMP_LV_LOWEST					= 1;	// "Low CPU & High Band" Usage

    // 送信方式
    //////////////////////////////////////
    this.SEND_MODE_ALWAYS				= 0;	// 常に全てを送信
    this.SEND_MODE_CHANGED				= 1;	// 変化があった場合に、全てを送信
    this.SEND_MODE_CLIPPED_CHANGED		= 2;	// 変化があった場合に、変化領域の矩形を送信

    // マウスカーソル
    //////////////////////////////////////
    this.WITH_OUT_CURSOR				= 0;
    this.WITH_CURSOR					= 1;

    // レイヤードウィンドウの扱い（バルーンなど）
    //////////////////////////////////////
    this.WITH_OUT_LAYERED_WINDOW		= 0;
    this.WITH_LAYERED_WINDOW			= 1;

    // スクロールモード
    //////////////////////////////////////
    this.SCROLL_MODE_LOCKED				= 0;
    this.SCROLL_MODE_SCROLLABLE			= 1;

    // キャプチャ可能な最大サイズ
    //////////////////////////////////////
    this.MAX_WIDTH						= 2048;
    this.MAX_HEIGHT						= 1536;

    // キャプチャ可能な最小サイズ
    //////////////////////////////////////
    this.MIN_WIDTH						= 640;
    this.MIN_HEIGHT						= 480;

    // リモート操作
    //////////////////////////////////////
    this.CONTROL_OFF					= 0;
    this.CONTROL_ON						= 1;

    // 表示モード
    //////////////////////////////////////
    this.VIEW_MODE_NORMAL				= 0;
    this.VIEW_MODE_STRETCH				= 1;

    // アラート
    //////////////////////////////////////
    this.ALERT_TITLE					= _(3846, "Screen Sharing");
    this.ALERT_REQUEST					= _(3847, "\nhas requested to\nshare control of your desktop.\nDo you want to allow this?");
    this.ALERT_ANIMATION_TIME			= 200;	// ms

    // 停止状態
    //////////////////////////////////////
    this.TERMINATED_REASONS = new Array();
    this.TERMINATED_REASONS[ 1 ]		= _(3848, "Sharing has been stopped");
    this.TERMINATED_REASONS[ 2000 ]		= _(3849, "Unknown error has occurred");
    this.TERMINATED_REASONS[ 2001 ]		= _(3850, "Disconnected");
    this.TERMINATED_REASONS[ 2002 ]		= _(3851, "Sharing has not started");
    this.TERMINATED_REASONS[ 2003 ]		= _(3852, "Version do not match");
    this.TERMINATED_REASONS[ 4000 ]		= _(3853, "Unknown network error has occurred");
    this.TERMINATED_REASONS[ 4001 ]		= _(3854, "Already connected");
    this.TERMINATED_REASONS[ 4002 ]		= _(3855, "Connection failed");
    this.TERMINATED_REASONS[ 4003 ]		= _(3856, "No response from server");
    this.TERMINATED_REASONS[ 4004 ]		= _(3857, "Request has been canceled");
    this.TERMINATED_REASONS[ 4005 ]		= _(3858, "Received incorrect data");
    this.TERMINATED_REASONS[ 9000 ]		= _(3859, "Unknown error");
    this.TERMINATED_REASONS[ 9001 ]		= _(3860, "Unknown error");
    this.TERMINATED_REASONS[ 9002 ]		= _(3861, "Insufficient system resource (GDI)");
    this.TERMINATED_REASONS[ 9003 ]		= _(3862, "Insufficient system resource");
    this.TERMINATED_REASONS[ 9004 ]		= _(3863, "Insufficient memory");
    this.TERMINATED_REASONS[ 9005 ]		= _(3864, "Insufficient memory");
    this.TERMINATED_REASONS[ 9006 ]		= _(3865, "Insufficient memory");
}

_Constants.prototype.getIntervalTimeByFPS = function( fps ) { return Math.floor( 1000 / fps ); }
_Constants.prototype.getTerminatedReason = function( reasonNo ) { return this.TERMINATED_REASONS[ reasonNo ] ? this.TERMINATED_REASONS[ reasonNo ] : null; }

var Constants = new _Constants();
