
if ( reason == 0 || reason == 1 || reason == 2001 ) {
	if ( eventListener ) {
		eventListener.sharingEndHandler();
	}
}

else {
	
	alert(
		Constants.getTerminatedReason( reason ) + "\n" +
		"Code : " + reason
	);
}

location.href = "/ja/emp.html";
