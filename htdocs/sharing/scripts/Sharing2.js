
var CaptureFrameCaption = _(6802, "この枠の内側が共有されます");
var QuitConfirmText = _(6803, "デスクトップ共有を終了しますか？");
var QuitTitleText = _(6804, "デスクトップ共有の終了");

var Sharing2ObjectClass = function() {};
Sharing2ObjectClass.prototype = {
	"create": function() {
		var sharing2Environment = new Sharing2EnvironmentClass();
		if (sharing2Environment.isWin())
			return new Sharing2Win();
		else if (sharing2Environment.isMac())
			return new Sharing2Mac();
	}
};

var Sharing2Win = function(){}
Sharing2Win.prototype = {
	
	"setSharing2SenderObject": function(onloadHandlerName) {
		document.write( '<object' );
		document.write( '	classid="clsid:0DFD9332-83D2-41cd-8403-3AA1DE66A2E9"' );
		document.write( '	codebase="/sharing/win32/vrms_rs2.ocx#version=2,1,0,6" id="N2MYRS2" width="1" height="1"' );
		document.write( '	onload="loadCheck()"' );
		document.write( '>' );
		document.write( '<param name="Mode" value="0" />' );
		document.write( '<param name="Language" value="0" />' );
		document.write( '<param name="CaptureFrameCaption" value="' + CaptureFrameCaption + '" />' );
		document.write( '<param name="QuitConfirmText" value="' + QuitConfirmText + '" />' );
		document.write( '<param name="QuitTitleText" value="' + QuitTitleText + '" />' );
		document.write( '</object>' );
	},
	
	"setSharing2ViewerObject": function() {
		document.write( '<object' );
		document.write( '	classid="clsid:0DFD9332-83D2-41cd-8403-3AA1DE66A2E9"' );
		document.write( '	codebase="/sharing/win32/vrms_rs2.ocx#version=2,1,0,6" id="N2MYRS2" width="800" height="600"' );
		document.write( '	onload="loadCheck()"' );
		document.write( '>' );
		document.write( '<param name="Mode" value="2" />' );
		document.write( '<param name="Language" value="0" />' );
		document.write( '<param name="CaptureFrameCaption" value="' + CaptureFrameCaption + '" />' );
		document.write( '<param name="QuitConfirmText" value="' + QuitConfirmText + '" />' );
		document.write( '<param name="QuitTitleText" value="' + QuitTitleText + '" />' );
		document.write( '</object>' );
	},

	"getSharing2Object": function() {
		return document.getElementById( "N2MYRS2" );
	}
};

var Sharing2Mac = function(){}
Sharing2Mac.prototype = {
	"sharingObject": null,
	"setSharing2SenderObject": function() {
		document.write('<embed name="viewport" ');
			document.write('type="application/x-vrms_rs2-viewport" ');
//			document.write('width="300" height="100" ');
			document.write('CaptureCycle="100" ');
//			document.write('Server="' + "192.168.1.52" + '" ');
//			document.write('Port="' + "2192" + '" ');
//			document.write('SharingID="' + shairingId + '" ');
			document.write('CaptureMode="1" ');
			document.write('CaptureFrameCaption="' + CaptureFrameCaption + '" ');
			document.write('QuitConfirmText="' + QuitConfirmText + '" ');
			document.write('QuitTitleText="' + QuitTitleText + '" ');
			
		document.write('/>');
		//this.sharingObject = document.embeds["viewport"];
		this.setSharing2Object(document.embeds["viewport"]);
	},
	
	"setSharing2ViewerObject": function(shairingId) {
		document.write('<embed name="controller" ');
			document.write('type="application/x-vrms_rs2-viewer" ');
			document.write('CaptureCycle="100" ');
//			document.write('SharingID="' + shairingId + '" ');
			document.write('Control="1" ');
			document.write('CaptureFrameCaption="' + CaptureFrameCaption + '" ');
			document.write('QuitConfirmText="' + QuitConfirmText + '" ');
			document.write('QuitTitleText="' + QuitTitleText + '" ');
		document.write('/>');
		
		//this.sharingObject = document.embeds["controller"];
		this.setSharing2Object(document.embeds["controller"]);
	},
	
	"setSharing2Object": function(value) {
		this.sharingObject = value;
	},
	
	"getSharing2Object": function() {
		return this.sharingObject;
	},
	
	"senderTerminated": function(reason) {
		if ( reason == 0 || reason == 1 || reason == 2001 ) {
//			if ( eventListener ) {
//				eventListener.sharingEndHandler();
//			}
			window.parent.sharing2_endSharing_from_ocx();
		}
		else {
			alert(
				Constants.getTerminatedReason( reason ) + "\n" +
				"Code : " + reason
			);
		}
		//location.href = "/ja/emp.html";
	},
	
	"viewerTerminated": function (reason) {
		if ( reason == 0 || reason == 1 || reason == 2001 ) {
			parent.onSharingEnd();
			//eventListener.onSharingEnd();
		}
		else if ( reason == 2002 ) {
			parent.onBeforeSharingStart();
			setTimeout( "rConnectSuccess()", 2000 );
		}
		else {
			alert(
				Constants.getTerminatedReason( reason ) + "\n" +
				"Code : " + reason
			);
			parent.onSharingError();
			//eventListener.onSharingError();
		}
	},
	
	// viewer
	"sizeChanged": function(w, h) {
		var sharingObject = this.getSharing2Object();
		this.sharingObject.width = w;
		this.sharingObject.height = h;

		parent.onSizeChange(w, h);
//		if ( eventListener )
//			eventListener.onSizeChange( newWidth, newHeight );
	},
	
	//
	"alertSelected": function(result) {
		controlRequestAnswer( result == 0 ? 1 : 0 );
	}
};

//var sharing2 = new Sharing2ObjectClass().create();