var Sharing2EnvironmentClass = function() {}
Sharing2EnvironmentClass.prototype = {
	"create": function() {
		if (this.isMac())
			return new MacSharing2EnvironmentClass();
		else if (this.isWin())
			return new WindowsSharing2EnvironmentClass();
		else
			return null;
	},

	"isMac": function() {return (navigator.userAgent.indexOf("Mac") != -1);},
	"isWin": function() {return (navigator.userAgent.indexOf("Win") != -1);}
}

var MacSharing2EnvironmentClass = function() {}
MacSharing2EnvironmentClass.prototype = {
	"LOWEST_OS_VERSION": [{"major": 10, "minor": 5}],
	"ALLOW_BROWSE": {"name": "SAFARI", "version": ""},
	"checkOsVersion": function() {
		//var version = "10_5";
		//version.match(/([0-9]+)_([0-9]+)/);
		navigator.userAgent.match(/([0-9]+)_([0-9]+)/);
		for (var i = 0; i < this.LOWEST_OS_VERSION.length; i++) {
			if (RegExp.$1 > this.LOWEST_OS_VERSION[i].major ||
			   (RegExp.$1 == this.LOWEST_OS_VERSION[i].major && 
				RegExp.$2 >= this.LOWEST_OS_VERSION[i].minor)) return true;
		}
		return false;
	},
	
	"checkBrowse": function() {
	/*
		for (var i = 0; i < this.ALLOW_BROWSE_LIST.length; i++) {
			if (this.checkBrowseType(this.ALLOW_BROWSE_LIST[i].name) &&
			   (!this.ALLOW_BROWSE_LIST[i].version || this.checkBrowseVersion())) return true;
		}
		*/
		return (this.checkBrowseType(this.ALLOW_BROWSE.name) &&
			   (!this.ALLOW_BROWSE.version || this.checkBrowseVersion()))? true: false;
	},
	
	"checkBrowseType": function(name) {
		return (navigator.userAgent.toUpperCase().indexOf(name) >= 0 && navigator.userAgent.toUpperCase().indexOf("CHROME") == -1);
	},
	
	"checkBrowseVersion": function() {
		return true;
	},
	
	"checkPlugin": function() {
		return (this.checkSafariViewerPlugin() && this.checkSafariViewportPlugin());
	},
	
	// private
	"checkSafariViewerPlugin": function() {
		for(var i = 0; i < navigator.mimeTypes.length; i++) {
			if(navigator.mimeTypes[i].type == "application/x-vrms_rs2-viewport"){
				return true;
			}
		}
		return false;
	},

	// private
	"checkSafariViewportPlugin": function() {
		for(var i = 0; i < navigator.mimeTypes.length; i++) {
			if(navigator.mimeTypes[i].type == "application/x-vrms_rs2-viewport"){
				return true;
			}
		}
		return false;
	}
}


var WindowsSharing2EnvironmentClass = function() {}
WindowsSharing2EnvironmentClass.prototype = {
	"ALLOW_BROWSE_LIST": [{"name": "WINDOWS", "version": ""}],
	"checkOsVersion": function() {return true;},
	
	// 特に指定がないのでOsのみ判別
	"checkBrowse": function() {return /*@cc_on!@*/false;},
	
	"checkPlugin": function() {
		try {
			new ActiveXObject("N2MYRS2.n2myrs2Ctrl.1");
			return true;
		} 
		catch(e) {
			return false;
		}
	}
}
//var gSharing2Environment = new Sharing2EnvironmentClass();
