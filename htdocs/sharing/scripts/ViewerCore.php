<?php
require_once 'classes/AppFrame.class.php';

class AppSharing2 extends AppFrame {

    function init() {
    }

    function default_view() {
        $this->display('sharing/viewer_core.t.html');
    }

}

$main = new AppSharing2();
$main->execute();