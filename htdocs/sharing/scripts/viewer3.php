<?php
require_once 'classes/AppFrame.class.php';

class AppSharing3 extends AppFrame {

    function init() {
    }

    function default_view() {
        $request = $this->request->getAll();
        $this->logger2->info($request);
        if (!$request || !$request["publish_id"]) {
            return false;
        }
        $this->template->assign("flash_vars",$request);
        $this->display('sharing/sharing3_viewer.t.html');
    }

}

$main = new AppSharing3();
$main->execute();