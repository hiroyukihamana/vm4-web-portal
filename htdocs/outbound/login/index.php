<?php
require_once("classes/AppFrame.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/member.dbi.php");

class AppOutboundLogin extends AppFrame {

    var $_user_key = null;
    var $_offset = null;
    var $_name_space = null;
    var $customerMessageTable = null;

    function init()
    {
        $this->_name_space = md5(__FILE__);
    }
    
    function default_view()
    {
        $this->action_show_top();
    }
    
    function action_show_top($message = "")
    {
        if ($message) {
            $this->template->assign('message', $message);
        }
        $this->display( 'outbound/login.t.html' );
    }
    
    function action_number_login()
    {
        //パスワード入力ハンドル
        $message = null;
        $number_id = $this->request->get("number_id");
        if(!$number_id || !is_numeric($number_id)){
            $message = OUTBOUND_ERROR_PASS_NUM;
        } elseif(!$member_info = $this->_isExist($number_id)){
            $message = OUTBOUND_ERROR_SALES_EXIST;
        }
        //エラーがなければマイページへ
        if($message){
            $this->action_show_top($message);
            exit;
        }else{
            $url = $this->get_redirect_url("outbound/".$member_info["outbound_id"]);
            header("Location: ".$url);
        }
    }

    function _isExist($number_id)
    {
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );

        if ( ! $server_info = $obj_MGMClass->getRelationDsn( $number_id, "outbound_number" ) ){
            return false;
        }
        $obj_Member = new MemberTable( $this->get_dsn() );
        $where = "outbound_number_id = '".addslashes($number_id)."'".
                 " AND member_status = 0".
                 " AND use_sales = 1";
        $member_info = $obj_Member->getRow($where);
        $this->logger->info("member_info",__FILE__,__LINE__,$member_info);

        if($member_info){
            return $member_info;
        }else{
            return false;
        }
    }
    
    function action_chenge_loginlang()
    {
        setcookie("lang", $this->request->get("lang"),  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        $this->session->set("lang", $this->request->get("lang"));
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$this->request->get("lang"));

        $redirect_url = $this->get_redirect_url("outbound/login/index.php");
        header("Location: ".$redirect_url);
    }

    private function render_valid()
    {
        $this->display( 'outbound/error.t.html' );
    }
}

$main = new AppOutboundLogin();
$main->execute();

?>
