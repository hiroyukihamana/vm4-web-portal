<?php
require_once ("classes/dbi/room.dbi.php");
require_once ("classes/dbi/user.dbi.php");
require_once ("classes/AppFrame.class.php");
require_once ("classes/mgm/MGM_Auth.class.php");

class AppTopMenu extends AppFrame {

    var $obj_room = null;
    var $obj_user = null;
    var $col_cnt = 2;
    var $lang = "ja";
    var $country = "auto";
    var $_name_space = null;

    function init() {
        $this->obj_room = new RoomTable($this->get_dsn());
        $this->obj_user = new UserTable($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function default_view() {
        $this->render_top();
    }

    function render_top() {
        $cnt = 0;
        $line = 0;
        $user_xml = simplexml_load_file(N2MY_APP_DIR . 'config/custom/ichishin/user.xml');
        $user_id = (string)$user_xml->user_id;
        if ($user_id) {
            $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
            if (!$user_info = $obj_MGMClass->getUserInfoById( $user_id ) ){
                die("ユーザーIDが間違っています");
                return false;
            }
            if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
                die("サーバー情報が有りません");
                return false;
            }
        }
        $user_info = $this->obj_user->getRow('user_id = "' . mysql_real_escape_string($user_id) . '"');
        $this->session->set("user_info", $user_info);
        $use_rooms = $this->get_sales_room_list($user_info['user_key']);
        foreach ($use_rooms as $row) {
            $addition = unserialize($row["addition"]);
            $rooms[$line][] = array(
                "room_key"   => $row["room_key"], 
                "room_image" => $row["room_image"], 
                "room_name"  => $row["room_name"], 
                "title"      => $addition["title"], 
                "comment"    => $row["comment"], 
                "sort"       => $row["sort"], 
            );
            $room_keys[] = $row["room_key"];
            $cnt++;
            if (($cnt % $this->col_cnt) == 0) {
                $line++;
            }
        }
        /*
        $obj_relation = new N2MY_DB($this->get_auth_dsn(), 'relation');
        $relation_info = $obj_relation->getRow('status = 1 AND user_key = "' . mysql_real_escape_string((string)$user_xml->user_id) . '"');
        if (DB::isError($relation_info)) {
            $this->logger->error("inbound relation doesn't exist");
            return false;
        }
        $obj_MGMClass = new MGM_AuthClass($this->get_auth_dsn());
        if (!$server_info = $obj_MGMClass->getRelationDsn($relation_info['relation_key'], $relation_info['relation_type'])) {
            return false;
        }
        */
        $this->session->set("server_info", $server_info);
        $this->session->set("service_mode", "sales");
        $this->session->set("lang", $this->lang);
        $this->set_submit_key($this->_name_space);
        $room_keys_string = join($room_keys, ',');
        $this->template->assign('user_id', $user_info['user_key']);
        $this->template->assign('meeting_version', $user_info['meeting_version']);
        $this->template->assign('server', $this->config->get("N2MY", "fms_server"));
        $this->template->assign("rooms", $rooms);
        $template_file = N2MY_APP_DIR . "templates/custom/ichishin/common/u/ichishin/index.t.html";
        $this->template->display($template_file);
    }

    /**
     * 会議開始
     */
    function action_meeting_start() {
        $narrow = $this->request->get("is_narrow");
        $fl_ver = $this->request->get('fl_ver');
        $room_key = $this->request->get("room_key");
        $room_info = $this->obj_room->get_detail($room_key);
        $meeting_key = $this->request->get("meeting_key");
        $addition = unserialize($room_info["addition"]);
        $login_type = "customer";
        $this->session->set("login_type", $login_type);
        $user_info = $this->session->get('user_info');
        require_once ("classes/N2MY_Meeting.class.php");
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // 最後に実行されたMeetingKeyを取得
        $where = "room_key = '" . mysql_real_escape_string($room_key) . "'";
        $room_info = $this->obj_room->getRow($where);
        $last_meeting_key = $room_info["meeting_key"];
        $this->logger->debug(__FUNCTION__ . "last_meeting_key", __FILE__, __LINE__, $last_meeting_key);
        // ステータス確認
        $rooms[$room_key] = $last_meeting_key;
        $last_meeting_status = $this->api_room_status($rooms);
        if ($last_meeting_status == "1") {
            // 会議情報取得
            if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_key)) {
                $message = array(
                    "title"          => $this->get_message("MEETING_START", "using_title"), 
                    "text"           => $this->get_message("MEETING_START", "using_text"), 
                    "back_url"       => "javascript:window.close();", 
                    "back_url_label" => $this->get_message("MEETING_START", "using_back_url_label"),
                );
                $this->logger2->info($message);
            }
        }
        $meeting_key = $meeting_info["meeting_key"];
        // オプション指定
        $options = array(
            "meeting_name" => $meeting_info["meeting_name"],
            "user_key"     => $user_info["user_key"],
            "start_time"   => $meeting_info["start_time"],
            "end_time"     => $meeting_info["end_time"],
            "country_id"   => $this->country,
            "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);

        //名前取得
        $name = urldecode($this->request->get("name"));
        if ($name == "undefined") {
            $name = "";
        }

        //インバウンド情報
        require_once ("classes/dbi/inbound.dbi.php");
        $obj_Inbound = new InboundTable($this->get_dsn());
        $where_inbound = "user_key = " . $room_info["user_key"];
        $inbound_data = $obj_Inbound->getRow($where_inbound);
        // モバイル判別
        if (strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod") || strpos($_SERVER['HTTP_USER_AGENT'],"iPad")) {
            $base_url = parse_url(N2MY_BASE_URL);
            $referer = N2MY_BASE_URL."u/ichishin";
            $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://info?inbound_id=".$inbound_data['inbound_id']."&entrypoint=".$base_url["host"]."&callbackurl=".urlencode($referer);
            $this->logger2->info($url);
            header("Location: ".$url);
            exit;
        }

        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key . "@" . $this->config->get("N2MY", "mail_wb_host") : "";
        $user_options = array(
            "user_key"  => $user_info["user_key"],
            "name"      => $name,
            "participant_email"         => "",
            "participant_station"       => "",
            "participant_country"       => $this->country,
            "member_key"=> "",
            "narrow"    => $this->request->get("is_narrow"),
            "lang"      => $this->lang,
            "skin_type" => $this->request->get("skin_type", ""),
            "mode"      => "",
            "role"      => $login_type,
            "mail_upload_address"       => $mail_upload_address,
            "account_model"       => $user_info["account_model"],
            );

        // 会議入室
        $type = "customer";
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);
        if (!$meetingDetail) {
            return $this->render_valid();

        }

        //ルームーキーからオプションの契約状況を取得
        // require_once ("classes/N2MY_Account.class.php");
        // $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        // $room_option = $obj_N2MY_Account->getRoomOptionList($room_key);
        // if ($room_option["scanner_twain"]) {
        // $this->session->set("scanner_twain_flg", "1");
        // } else {
        // $this->session->set("scanner_twain_flg", "0");
        // }

        $meeting_addition = unserialize($meetingDetail["addition"]);
        $display_size = $this->request->get("display_size") ? $this->request->get("display_size") : $meeting_addition["display_size"];

        $this->session->set("room_key", $room_key);
        $this->session->set("type", $meetingDetail["participant_type_name"]);
        $this->session->set("meeting_type", $type);
        $this->session->set("meeting_key", $meetingDetail["meeting_key"]);
        $this->session->set("participant_key", $meetingDetail["participant_key"]);
        $this->session->set("participant_name", $name);
        $this->session->set("mail_upload_address", $mail_upload_address);
        $this->session->set("reload_type", 'normal');
        $this->session->set("fl_ver", $fl_ver);
        $this->session->set("display_size", $display_size);
        $this->session->set("meeting_version", $user_info["meeting_version"]);
        $this->session->set("user_agent_ticket", uniqid());
        if (!$room_info["room_password"]) {
            $redirect_url = N2MY_BASE_URL . "u/ichishin/?action_meeting_display";
            header("Location:" . $redirect_url);
        } else {
            $params = array("action_room_auth_login" => "", "room_key" => $room_key, "ns" => $this->_name_space);
            $url = $this->get_redirect_url("u/ichishin", $params);
            $this->template->assign('lang', $this->get_language());
            $this->logger->info(__FUNCTION__ . "#end", __FILE__, __LINE__, $url);
            header("Location: " . $url);
            $redirect_url = N2MY_BASE_URL . "u/ichishin/index.php?action_meeting_display";
            $this->session->set("redirect_url", $redirect_url, $this->_name_space);
            $this->template->assign('ns', $this->_name_space);
            $this->template->assign('room_key', $room_key);
            $this->display('user/room/login.t.html');
        }
    }

    function action_meeting_display() {
        $user_info = $this->session->get('user_info');
        $meeting_key = $this->session->get('meeting_key');
        require_once ("classes/dbi/meeting.dbi.php");
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $where = "meeting_key = '" . mysql_real_escape_string($meeting_key) . "'" . " AND user_key = '" . $user_info["user_key"] . "'" . " AND is_active = 1" . " AND is_deleted = 0";
        $meeting_info = $obj_Meetng->getRow($where);
        $this->template->assign('meeting_info', $meeting_info);
        $room_info = $this->obj_room->getRow("room_key = '" . mysql_real_escape_string($meeting_info['room_key']) . "'");
        $this->template->assign("room_info", $room_info);
        $this->template->assign('skinName', "Skins");
        $this->template->assign('charset', "UTF-8");
        $this->template->assign('locale', $this->get_language());
        $this->template->assign('meeting_version', $user_info['meeting_version']);

        $certificate = substr(md5(uniqid(rand(), true)), 1, 16);
        $this->template->assign('certificateId', $certificate);
        $this->template->assign('apiUrl', '/services/api.php?action_issue_certificate');
        $this->template->assign('user_agent_ticket', $this->session->get("user_agent_ticket"));

        // V-Cube Meetingのバージョンが違う場合、キャッシュしたデータを使わせない。
        $this->template->assign('cachebuster', $this->get_meeting_version());
        $this->template->assign('meeting_type', htmlspecialchars('customer', ENT_QUOTES));
        $this->template->assign('participant_name', $this->session->get("participant_name"));
        $this->template->assign('display_size', htmlspecialchars($this->session->get("display_size"), ENT_QUOTES));
        $this->display(sprintf('core/%s/meeting_as3.t.html', "meeting"));
    }

    function action_room_auth_login($message = "") {
        $this->logger->trace(__FUNCTION__ . "#start", __FILE__, __LINE__);
        $this->template->assign('message', $message);
        $this->template->assign('ns', $this->request->get("ns"));
        $room_key = $this->request->get("room_key");
        $this->template->assign('room_key', $room_key);
        $template_file = N2MY_APP_DIR . "templates/custom/ichishin/common/u/ichishin/auth.t.html";
        $this->template->display($template_file);
    }

    /**
     * 各部屋の状態を取得
     */
    function action_inbound_status() {
        $this->logger->trace(__FUNCTION__ . "#start", __FILE__, __LINE__);
        $user_xml = simplexml_load_file(N2MY_APP_DIR . 'config/custom/ichishin/user.xml');
        $user_info = $this->obj_user->getRow("user_id = '" . mysql_real_escape_string((string)$user_xml->user_id) . "'");
        require_once ("classes/core/Core_Meeting.class.php");
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $use_rooms = $this->get_sales_room_list($user_info['user_key']);
        foreach ($use_rooms as $key => $val) {
            $query[$val["room_key"]] = $val["meeting_key"];
        }
        $room_list = $obj_CoreMeeting->getMeetingStatus($query);
        $this->logger->trace("room_list", __FILE__, __LINE__, $use_rooms);
        $list = array();
        if ($room_list) {
            foreach ($room_list as $key => $room) {
                $participants = array();
                $room["staff"] = 0;
                $room["customer"] = 0;
                foreach ($room["participants"] as $participant) {
                    switch ($participant["participant_type_name"]) {
                        case "staff" :
                            $room["staff"]++;
                            if (1 == $participant["is_away"]) {
                                $room["is_away"] = 1;
                            }
                            break;
                        case "customer" :
                            $room["customer"]++;
                            break;
                    }
                }
                //情報量を減らすため、participantsはいれない
                $room["participants"] = "";
                //入室者がnormalが一人でis_awayだったらstatusを0にする。
                if (1 == $room["staff"] && 0 == $room["customer"] && 1 == $room["is_away"]) {
                    $room["status"] = "0";
                }
                $room['fl_ver'] = 'as3';
                $room['display_size'] = '16to9';
                $list[] = $room;
            }
            // エンティティ処理
            array_walk_recursive($list, create_function('&$val, $key', '$val = htmlspecialchars($val);'));
        } else {
            $list = "";
        }
        $this->logger->debug(__FUNCTION__ . "#url", __FILE__, __LINE__, array($list, $this->_get_passage_time()));
        header("Content-Type: text/javascript; charset=UTF-8");
        $this->nocache_header();
        print json_encode($list);
        $this->logger->trace(__FUNCTION__ . "#end", __FILE__, __LINE__, $room_list);
    }

    function get_sales_room_list($user_key) {
        require_once ("classes/dbi/room.dbi.php");
        $room_table = new RoomTable($this->get_dsn());
        $where = "user_key = " . $user_key . " AND use_sales_option = 1 AND room_status = 1";
        $rooms = $room_table->getRowsAssoc($where, array("room_key" => "asc"));
        if (DB::isError($rooms)) {
            $this->logger->error("room doesn't exist");
            return false;
        }
        return $rooms;
    }

    /**
     * 会議室の状態を取得
     *
     * @return boolean true:会議中、false:空室
     */
    function api_room_status($rooms) {
        require_once ("classes/core/Core_Meeting.class.php");
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $room_list = $obj_CoreMeeting->getMeetingStatus($rooms);
        $status = $room_list[0]["status"];
        $this->logger2->debug($status);
        return $status;
    }

    private function get_meeting_version() {
        $version_file = N2MY_APP_DIR . "version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }

    function nocache_header() {
        // 日付が過去
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        // 常に修正されている
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        // HTTP/1.1
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        // HTTP/1.0
        header("Pragma: no-cache");
    }

}

$main = new AppTopMenu();
$main->execute();
