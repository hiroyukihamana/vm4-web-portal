<?php
require_once("classes/AppFrame.class.php");
class FmsUpdater extends AppFrame {

    function default_view() {
        $time = 0;
        print floor($time / 3600).gmdate(':i:s', $time);
//        header('Location: /services/not_found.php');
    }

    function action_result() {
        require_once("lib/EZLib/EZMail/EZSmtp.class.php");
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        //
        $error_cd  = $this->request->get('error_cd');
        $error_msg = $this->request->get('error_msg');
        $user_id   = $this->request->get('user_id');
        $server    = $this->request->get('server');

        $body = <<<EOM
< アップデート環境 >
ユーザーID     : $user_id
FMSサーバー    : $server
アプリケーション : $app_name
------------------------
< 結果 >
エラーコード    : $error_cd
エラーメッセージ : $error_msg
EOM;
        $lang = EZLanguage::getLangCd("ja");
        $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
        $obj_SendMail->setFrom(N2MY_MAIL_FROM);
        $obj_SendMail->setReturnPath('kiyomizu@vcube.co.jp');
        $obj_SendMail->setTo('kiyomizu@vcube.co.jp');
        $obj_SendMail->setSubject('FMS Auto Update');
        $obj_SendMail->setBody($body);
        // メール送信
        $result = $obj_SendMail->send();
        $this->logger->info(__FUNCTION__."#result",__FILE__,__LINE__,$result);
        return $result;
    }
}
$main = new FmsUpdater();
$main->execute();
?>