<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("Auth.php");
require_once("Pager.php");
require_once("config/config.inc.php");
require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/N2MY_IvesClient.class.php");
require_once("classes/mgm/dbi/mcu_server.dbi.php");
require_once("classes/mcu/config/McuConfigProxy.php");

class AppViewerTool extends AppFrame {

    var $_auth = null;
    var $_sess_page;
    var $account_dsn = null;
    var $session = null;

    function init() {
        $_COOKIE["lang"]   = "en";
        $this->session   = EZSession::getInstance();
        $this->session->set("lang","en");
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->_sess_page  = "_adminauthsession";
        $this->_name_space = md5(__FILE__);
        $this->configProxy  = new McuConfigProxy();
        define("WSDL_DOMAIN", "mcu02.nice2meet.us:8080");
        // DBサーバ情報取得
        if (file_exists(sprintf("%sconfig/slave_list.ini", N2MY_APP_DIR))) {
                $server_list = parse_ini_file(sprintf("%sconfig/slave_list.ini", N2MY_APP_DIR), true);
                $this -> _dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
                $this -> account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
        } else {
                $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR), true);
                $this -> _dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
                $this -> account_dsn = $this -> config -> get("GLOBAL", "auth_dsn");
        }
    }

    function set_dsn($dsn) {
        $_SESSION[$this->_sess_page]["dsn"] = $this->_dsn;
    }

    function get_dsn() {
        return $this->_dsn;
    }

    /**
     *
     */
    function auth() {
        $auth_detail = "*";
        $this->_auth = new Auth("DB", array(
            "dsn" => $this->account_dsn,
            "table" => "staff",
            "cryptType" => "sha1",
            "db_fields" => $auth_detail,
            "usernamecol" => "login_id",
            "passwordcol" => "login_password"
            ));
        if (!$this->_auth->getAuth()) {
            $this->_auth->start();
            if ($_SESSION["_authsession"]["data"]["status"] == "0") {
                $this->_auth->logout();
                $this->request->set("password", "");
                $this->_auth->start();
            }
            if (!$this->_auth->checkAuth()) {
                exit();
            }
        }
    }

    /**
     * 一覧取得
     */
    function action_list() {
        $this->logger->debug("request",__FILE__,__LINE__,$_REQUEST);
        $reset = $this->request->get("reset");
        if ($reset == 1) {
            $this->session->remove("temp", "action_list");
            $this->session->remove("user_data");
            $this->session->remove("form_data");
            $this->session->remove("before_data");
        }
        $regist = $this->request->get("regist");
        if ($regist) {
            $this->session->set("regist", $regist);
        }
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $this->logger->trace("request",__FILE__,__LINE__,$request);
        $this->session->remove("where_data");
        $required = $this->session->get("required");
        $message = $this->session->get("message");
        $this->logger->trace("required",__FILE__,__LINE__,$required);
        if ($required || $message) {
            $this->template->assign("message",$message);
            $this->template->assign("required",$required);
        }
        $type = $this->request->get("type");
        if (!$type) {
            $type = $this->session->get("type");
        }
        if ($type == "account") {
            $dsn = $this->account_dsn;
        } else {
            $dsn = $this->get_dsn();
        }
        $table = $this->request->get("table");
        if (!$table) {
            $table = $this->session->get("table");
        } else {
            $this->session->remove("check");
        }
        $this->session->set("type", $type);
        $this->session->set("table", $table);
        $check = $this->request->get("check");
        if (!$check) {
            $check = $this->session->get("check");
        } else {
            $this->session->set("check", $check);
        }
        $this->logger->trace("check_list",__FILE__,__LINE__,$check);
        $sort_key = $this->request->get("sort_key");
        $sort_type = $this->request->get("sort_type");
        if (!$sort_type) {
            $sort_type = "asc";
        }
        //会議一覧の時はfmsサーバー取得（FMSのログを参照するため）
        if ($table == "meeting") {
            $fms_db = new N2MY_DB($this->account_dsn, "fms_server");
            $_server_info = $fms_db->getRowsAssoc("", null ,null, null, null, null);
            $this->logger->trace("fms_server",__FILE__,__LINE__,$_server_info);
            foreach($_server_info as $_key => $val) {
                $server_info[$val["server_key"]] = $val;
            }
            $fms_app_name = $this->config->get("CORE", "app_name", "Vrms");
            $fms = array(
                "server_info" => $server_info,
                "app_name" => $fms_app_name,
                );
            $this->template->assign("fms",$fms);
        }
        $data_db = new N2MY_DB($dsn, $table);
        $this->logger->trace("dsn",__FILE__,__LINE__,$dsn);
        $action_name = __FUNCTION__;
        $columns = $data_db->getTableInfo($type);
        $columns = $this->_get_relation_data($dsn, $columns);
        $this->logger->trace("columns",__FILE__,__LINE__,$columns);
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $sort_key, $sort_type);
        $this->logger->debug("form_data",__FILE__,__LINE__,$form_data);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
            $this->template->assign("request",$request);
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where = $data_db->getWhere($columns, $request);
        $this->logger->debug("where",__FILE__,__LINE__,$where);
        // CSVダウンロード
        if (isset($_REQUEST["csv"])) {
            require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            $csv = new EZCsv(true, "UTF-8", "UTF-8");
            $dir = $this->get_work_dir();
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");
            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="'.date("Ym").'.csv"');
            // ヘッダ２
            foreach($columns as $key => $column) {
                $header[$key] = $column["label"];
            }
            $csv->setHeader($header);
            $csv->write($header);

            // データ取得
            $rs = $data_db->select($where, array($form_data['sort_key'] => $form_data['sort_type']));
            while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                $csv->write($row);
            }

//            $rows = $data_db->getRowsAssoc($where, array($form_data['sort_key'] => $form_data['sort_type']));
//            foreach($rows as $row) {
//                $csv->write($row);
//            }
            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            while($str = fread($fp, 4096)) {
                print $str;
            }
            @fclose($fp);
//            $contents = fread($fp, filesize($tmpfile));
//            print $contents;
            $this->logger2->info(memory_get_peak_usage());
            unlink($tmpfile);
            return true;
        } else {
            // データ取得
            $rows = $data_db->getRowsAssoc($where, array($form_data['sort_key'] => $form_data['sort_type']), $limit, $offset);
        }
        $total_count = $data_db->numRows($where);
        if ($table == "member" && $rows) {
            $memberRoomRelation = new N2MY_DB($this->get_dsn(), "member_room_relation");
            foreach ($rows as $member) {
                $where = "member_key = ".$member["member_key"];
                $room_relation_list = $memberRoomRelation->getRowsAssoc($where);
                $room_keys = array();
                if ($room_relation_list) {
                    foreach ($room_relation_list as $room) {
                        $room_keys[] = $room["room_key"];
                    }
                    $this->logger2->debug($room_keys);
                    $member["room_key"] = implode(",", $room_keys);
                }
                $members[] = $member;
            }
            $rows = $members;
        }
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name,
            "after_action" => $this->request->get("after_action"))
            );
        $add_form = $this->session->get("temp", $action_name);
        $this->template->assign("add_form", array(
            "data" => $add_form,
            "ok_action" => $action_name,
            "ng_action" => $action_name
          ));
        $this->template->assign("columns",$columns);
        $this->template->assign("rows",$rows);
        $this->template->assign("type",$type);
        $this->template->assign("table",$table);
        $this->session->remove("message");
        if ($type == "account") {
            $template = "viewer_tool/account/".$table."/index.t.html";
        } else {
            $template = "viewer_tool/".$table."/index.t.html";
        }
        $this->_display($template);
    }

    /**
     * 優先DC・除外DC画面
     */
    function action_datacenter_list() {
        $this->logger->debug("request",__FILE__,__LINE__,$_REQUEST);
        if ($this->request->get("reset") == 1) {
            $this->session->remove("temp", "action_list");
            $this->session->remove("user_data");
            $this->session->remove("form_data");
            $this->session->remove("before_data");
        }
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $this->session->set("request", $request);

        $this->logger->trace("request",__FILE__,__LINE__,$request);
        $dsn = $this->get_dsn();
        $table = $this->request->get("table");
        if (!$table) {
            $table = $this->session->get("table");
        }
        $this->session->set("table", $table);

        require_once("classes/dbi/". $table .".dbi.php");
        if ($table == "datacenter_priority") {
            $datacenter_db = new DatacenterPriorityTable($dsn);
        } else {
            $datacenter_db = new DatacenterIgnoreTable($dsn);
        }
        $action_name = __FUNCTION__;
        $columns = $datacenter_db->getTableInfo("data");
        $columns = $this->_get_relation_data($dsn, $columns);
        $this->logger->trace("columns",__FILE__,__LINE__,$columns);
        // 検索条件設定・取得
        $form_data = $this->set_form($action_name, $this->request->get("sort_key"), "asc");
        $this->logger->debug("form_data",__FILE__,__LINE__,$form_data);
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
            $this->template->assign("request",$request);
            $this->session->set("request", $request);
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        $where   = $datacenter_db->getWhere($columns, $request);
        $this->logger->debug("where",__FILE__,__LINE__,$where);

        // データ取得
        $rows = $datacenter_db->getRowsAssoc($where, array($form_data['sort_key'] => $form_data['sort_type']), $limit, $offset);
        $total_count = $datacenter_db->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager"        => $pager_info,
            "action"       => $action_name,
            "after_action" => $this->request->get("after_action"))
            );
        $user_id  = "";
        if ($request["user_key"]) {
            $user_key = $request["user_key"];
            $user_db  = new N2MY_DB($dsn, "user");
            $where    = "user_key = '".addslashes($user_key)."'";
            $this->logger->trace("where",__FILE__,__LINE__,$where);
            $user_rows = $user_db->getRowsAssoc($where);
            foreach ($user_rows as $user_row) {
                foreach ($user_row as $key => $colum) {
                    if ($key == "user_id") {
                        $user_id = $colum;
                        break;
                    }
                }
            }
        }
        $this->session->set("user_data", array("user_id"=> $user_id, "user_key" => $request["user_key"]));
        $this->session->set("form_data", $form_data);
        $this->template->assign("columns",$columns);
        $this->template->assign("rows",$rows);
        $this->template->assign("table",$table);
        $this->template->assign("user_id",$user_id);
        $this->session->remove("message");
        $template = "viewer_tool/".$table."/index.t.html";

        $this->_display($template);
    }

     /**
     * 部屋詳細表示
     */
    function action_room_detail($message = "", $option_message = "", $option_expire_date_time = "", $ives_setting_message = "") {
        $this->set_submit_key($this->_name_space);
        $room_key         = $this->request->get("room_key");
        $this->session->set("table", "room");
        $sort_key_plan    = $this->request->get("sort_key_plan");
        $sort_type_plan   = $this->request->get("sort_type_plan");
        $sort_key_option  = $this->request->get("sort_key_option");
        $sort_type_option = $this->request->get("sort_type_option");
        $sort_key_ives_option  = $this->request->get("sort_key_ives_option");
        $sort_type_ives_option = $this->request->get("sort_type_ives_option");

        $sort_key_plan         = isset($sort_key_plan)    ? $sort_key_plan    : "room_plan_key";
        $sort_type_plan        = isset($sort_type_plan)   ? $sort_type_plan   : "desc";
        $sort_key_option       = isset($sort_key_option)  ? $sort_key_option  : "ordered_service_option_key";
        $sort_type_option      = isset($sort_type_option) ? $sort_type_option : "desc";
        $sort_key_ives_option  = isset($sort_key_ives_option)  ? $sort_key_ives_option  : "ives_setting_key";
        $sort_type_ives_option = isset($sort_type_ives_option) ? $sort_type_ives_option : "desc";

        //部屋情報取得
        $room_db        = new N2MY_DB($this->get_dsn(), "room");
        $where          = "room_key='".$room_key."'";
        $room_detail    = $room_db->getRow($where);
        $room_columns   = $room_db->getTableInfo("data");
        $page["action"] = __FUNCTION__;

        $this->template->assign("room_columns",$room_columns);
        $this->template->assign("room_detail",$room_detail);

        //ユーザー情報取得（アカウントモデル取得の為）
        $user_db   = new N2MY_DB($this->get_dsn(), "user");
        $user_info = $user_db->getRow("user_key='".$room_detail["user_key"]."'");

        $this->template->assign("account_model",$user_info["account_model"]);

        $this->logger->debug("user_info",__FILE__,__LINE__,$user_info);

        //部屋プラン取得
        $service_db      = new N2MY_DB($this->account_dsn, "service");
        $service         = $service_db->getRowsAssoc("service_status = '1'");
        $service_columns = $service_db->getTableInfo("data");

        $this->template->assign(array("service_columns" => $service_columns,
                                      "service"         => $service));

        $room_plan_db     = new N2MY_DB($this->get_dsn(), "room_plan");
        $where            = "room_key='".$room_key."'";
        $room_plan_detail = $room_plan_db->getRowsAssoc($where, array($sort_key_plan => $sort_type_plan));
        $this->logger->trace("plan",__FILE__,__LINE__,$room_plan_detail);
        $room_plan_columns = $room_plan_db->getTableInfo("data");
        $room_plan_columns = $this->_get_relation_data($this->account_dsn, $room_plan_columns);
        $this->logger->trace("new_columuns",__FILE__,__LINE__,$room_plan_columns);

        $this->template->assign(array("room_plan_columns" => $room_plan_columns,
                                      "room_plan_detail"  => $room_plan_detail));
        //現在有効なプランを取得
        $where_now    = "room_key='".$room_key."'".
                        " AND room_plan_status = 1";
        $now_plan_key = $room_plan_db->getRow($where_now);
        $this->logger->debug("now_plan",__FILE__,__LINE__,$now_plan_key);

        //現在有効なプランが無い場合は開始待ちのプランを取得
        if (!$now_plan_key) {
            $where_now = "room_key='".$room_key."'".
                         " AND room_plan_status = 2";
            $now_plan_key = $room_plan_db->getRow($where_now);
        }

        if ($now_plan_key["service_key"]) {
            $where_service_key = "service_key = ".$now_plan_key["service_key"];
            $now_plan          = $service_db->getRow($where_service_key);
            $this->template->assign(array("now_plan"     => $now_plan["service_name"],
                                          "now_plan_key" => $now_plan["service_key"]));
        }

        // オプション情報取得
        $service_option_db = new N2MY_DB($this->account_dsn, "service_option");
        $service_option    = $service_option_db->getRowsAssoc();

        $option_db         = new N2MY_DB($this->get_dsn(), "ordered_service_option");
        $option_columns    = $this->_get_relation_data($this->account_dsn, $option_db->getTableInfo("data"));
        $option_data       = $option_db->getRowsAssoc($where, array($sort_key_option => $sort_type_option));

        $this->logger->debug("option_data",__FILE__,__LINE__,$option_data);

        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
           require_once("classes/dbi/pgi_setting.dbi.php");
           require_once('classes/pgi/PGiSystem.class.php');
           require_once("classes/pgi/PGiRate.class.php");
           $pgi_setting_table = new PGiSettingTable($this->get_dsn());
           $pgi_settings      = $pgi_setting_table->findByRoomKey($room_key);
           $pgi_systems       = PGiSystem::findConfigAll();
           $pgi_rate_config   = PGiRate::findConfigAll();
           $pgi_gm_rate_config   = PGiRate::findGmConfigAll();
           $default_system_key= $this->config->get("PGI", 'system_key');
           if (0 < count($pgi_settings)) {
                $pgi_setting_keys = array();
                foreach ($pgi_settings as $setting) {
                    $pgi_setting_keys[] = $setting['pgi_setting_key'];
                }
               $pgi_rates = $this->getPGiRateBySettingKey($pgi_setting_keys);
           }
        }
        foreach ($option_data as $data) {
            if ($data["ordered_service_option_status"] == 0) {
                continue;
            }
            foreach ($option_columns as $key => $colum) {
                if (isset($data[$key])) {
                    $option_flg = true;
                }
            }
        }
        if (empty($option_expire_date_time)) {
            $option_expire_date_time = date("Y/m/d 23:59:59");
        }

        $mcu_server_db           = new N2MY_DB($this->get_auth_dsn(), "mcu_server");
        $mcu_servers             = $mcu_server_db->getRowsAssoc("is_available = 1", array('server_key' => 'asc'), null, 0, "server_key,server_address");

        $media_mixer_db           = new N2MY_DB($this->get_auth_dsn(), "media_mixer");
        $media_mixers             = $media_mixer_db->getRowsAssoc("is_available = 1", array('media_mixer_key' => 'asc'), null, 0, "media_mixer_key,media_ip");

        $ives_setting_db         = new N2MY_DB($this->get_dsn(), "ives_setting");
        $ives_setting_columns    = $this->_get_relation_data($this->account_dsn, $ives_setting_db->getTableInfo("data"));
        $ives_setting_data       = $ives_setting_db->getRowsAssoc($where, array($sort_key_ives_option => $sort_type_ives_option));
        $active_ives_setting     = $ives_setting_db->getRow($where . " AND is_deleted = 0");
        if(DB::isError($active_ives_setting) || !is_array($active_ives_setting)) {
            $active_ives_setting = array();
            $ives_setting_flg = false;
        }
        else {
            $ives_setting_flg = true;
        }
        $this->logger->debug("option_data",__FILE__,__LINE__,$option_data);
        $this->template->assign(array("room_key"                  => $room_key,
                                      "now_ym"                    => date('Y-m').'-01',
                                      "page"                      => $page,
                                      "service_option"            => $service_option,
                                      "option_columns"            => $option_columns,
                                      "message"                   => $message,
                                      "option_message"            => $option_message,
                                      "option_data"               => $option_data,
                                      "option_flg"                => $option_flg,
                                      "pgi_systems"               => @$pgi_systems,  // if enabled pgi
                                      "json_pgi_systems"          => @json_encode($pgi_systems),  // if enabled pgi
                                      "pgi_settings"              => @$pgi_settings, // if enabled pgi
                                      "pgi_rate_config"           => @$pgi_rate_config,
                                      "pgi_gm_rate_config"        => @$pgi_gm_rate_config,
                                      "pgi_rates"                 => @$pgi_rates,
                                      "default_system_key"        => @$default_system_key,
                                      "option_date_default"       => $option_expire_date_time,
                                      "mcu_servers"               => $mcu_servers,
                                      "media_mixers"              => $media_mixers,
                                      "ives_setting_columns"      => $ives_setting_columns,
                                      "ives_setting_data"         => $ives_setting_data,
                                      "active_ives_setting"       => $active_ives_setting,
                                      "ives_setting_message"      => $ives_setting_message,
                                      "ives_setting_flg"          => $ives_setting_flg,
                                      ));
        $this->_display("viewer_tool/room/detail.t.html");
    }

    /**
     * id からそのユーザーが存在するかチェック
     */
    private function _isExistMember($id){
        //member, user, 認証DBのuser のテーブルをチェック
        $user_db = new N2MY_DB($this->get_dsn(), "user");
        $member_db = new N2MY_DB($this->get_dsn(), "member");
        $id = mysql_real_escape_string($id);
        if ( $user_db->numRows( sprintf("user_id='%s'", $id ) ) > 0 ||
             $member_db->numRows( sprintf("member_id='%s'", $id ) )  > 0 ||
             $user_db->numRows( sprintf("user_id='%s'", $id ) )  > 0 ){
            return true;
        } else {
            return false;
        }
    }

   /**
    * 会議詳細
    */
    function action_meeting_detail() {
        $meeting_key = $this->request->get("meeting_key");
        if ($meeting_key) {
            // サーバ一覧
            $server = new N2MY_DB($this->account_dsn, "fms_server");
            $_server_info = $server->getRowsAssoc("", null ,null, null, null, null);
            foreach($_server_info as $_key => $val) {
                $server_info[$val["server_key"]] = $val;
            }
            $fms_app_name = $this->config->get("CORE", "app_name", "Vrms");
            $form = array(
                "action" => "action_meeting"
                );
            $fms = array(
                "server_info" => $server_info,
                "app_name" => $fms_app_name,
                );
            // 会議詳細
            $meeting_obj = new N2MY_DB($this->get_dsn(), "meeting");
            $meeting_columns = $meeting_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $meeting_detail = $meeting_obj->getRow($where);
            $this->logger->debug("meeting_detail",__FILE__,__LINE__,$meeting_detail);
            // オプション情報
            $options_db = new N2MY_DB($this->account_dsn, "options");
            $option_list = $options_db->getRowsAssoc("", null, null, 0, "*", "option_key");
            // 会議オプション
            $meeting_option_obj = new N2MY_DB($this->get_dsn(), "meeting_options");
            $meeting_option_columns = $meeting_option_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $meeting_option_list = $meeting_option_obj->getRowsAssoc($where);
            foreach($meeting_option_list as $key => $row) {
                $row["option_name"] = $option_list[$row["option_key"]]["option_name"];
                $meeting_option_list[$key] = $row;
            }
            // データセンタ切り替え一覧
            $server_change_obj = new N2MY_DB($this->get_dsn(), "meeting_sequence");
            $server_change_columns = $server_change_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $server_change_list = $server_change_obj->getRowsAssoc($where);
            // ミーティング利用時間チェック
            $meeting_uptime_obj = new N2MY_DB($this->get_dsn(), "meeting_uptime");
            $meeting_uptime_columns = $meeting_uptime_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $meeting_uptime_list = $meeting_uptime_obj->getRowsAssoc($where);
            // 予約有り
            if ($meeting_detail["is_reserved"] == 1) {
                // 予約詳細
                require_once("classes/dbi/reservation.dbi.php");
                $reservation_obj = new ReservationTable($this->get_dsn());
                $reservation_columns = $reservation_obj->getTableInfo("data");
                $where = "meeting_key = '".addslashes($meeting_detail["meeting_ticket"])."'";
                $reservation_detail = $reservation_obj->getRow($where);
                $this->template->assign("reservation_detail", $reservation_detail);
                $this->template->assign("reservation_columns", $reservation_columns);
                // 招待者一覧
                require_once("classes/dbi/reservation_user.dbi.php");
                $invite_obj = new ReservationUserTable($this->get_dsn());
                $invite_columns = $invite_obj->getTableInfo("data");
                $where = "reservation_key = ".$reservation_detail["reservation_key"];
                $invite_list = $invite_obj->getRowsAssoc($where);
                $this->template->assign("invite_list", $invite_list);
                $this->template->assign("invite_columns", $invite_columns);
            }
            // 参加者一覧
            $participant_obj = new N2MY_DB($this->get_dsn(), "participant");
            $participant_columns = $participant_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $participant_list = $participant_obj->getRowsAssoc($where);
            // 各種利用時間
            $use_time_obj = new N2MY_DB($this->get_dsn(), "meeting_use_log");
            $query = "SELECT type, count(DISTINCT create_datetime) as use_time_count FROM".
                " meeting_use_log".
                " WHERE meeting_key = '".$meeting_key."'" .
                " GROUP BY type";
            $rs = $use_time_obj->_conn->query($query);
            if (DB::isError($rs)) {
                $this->logger->error("db error",__FILE__,__LINE__,$rs->getUserInfo());
            }
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                $this->logger->info("row",__FILE__,__LINE__,$row);
                $use_time[$row["type"]] = $row["use_time_count"];
            }
            $this->logger->info("use_time",__FILE__,__LINE__,$use_time);
            // 資料一覧
            $document_obj = new N2MY_DB($this->get_dsn(), "document");
            $document_columns = $document_obj->getTableInfo("data");
            $where = "meeting_key = ".$meeting_key;
            $document_list = $document_obj->getRowsAssoc($where);
            $this->template->assign("fms", $fms);
            $this->template->assign("meeting_detail", $meeting_detail);
            $this->template->assign("meeting_columns", $meeting_columns);
            $this->template->assign("option_list", $option_list);
            $this->template->assign("meeting_option_list", $meeting_option_list);
            $this->template->assign("meeting_option_columns", $meeting_option_columns);
            $this->template->assign("server_change_list", $server_change_list);
            $this->template->assign("server_change_columns", $server_change_columns);
            $this->template->assign("meeting_uptime_list", $meeting_uptime_list);
            $this->template->assign("meeting_uptime_columns", $meeting_uptime_columns);
            $this->template->assign("participant_list", $participant_list);
            $this->template->assign("participant_columns", $participant_columns);
            $this->template->assign("use_time", $use_time);
            $this->template->assign("document_columns", $document_columns);
            $this->template->assign("document_list", $document_list);
        }
        $this->_display("viewer_tool/meeting/detail.t.html");
    }

    private function _getDefaultPGI(){
        $pgi_form["pgi_start_date"] =date("Y/m");
        $pgi_form["system_key"] = "VCUBE HYBRID PRODUCTION";
        $pgi_form["company_id_type"] = "default";
        $pgi_form["company_id"] = "282363";
        $pgi_form["dialin_local"] ="";
        $pgi_form["dialin_free"] ="";
        $pgi_form["dialin_mobile"] ="";
        $pgi_form["dialin_area1"] ="";
        $pgi_form["dialin_area2"] ="";
        $pgi_form["dialin_area3"] ="";
        $pgi_form["dialin_area4"] ="";
        $pgi_form["dialin_area5"] ="";
        $pgi_form["dialout"] ="";
        $pgi_form["dialout_mobile"]="";
        $pgi_form["gm_dialin_local_tokyo"] = "15";
        $pgi_form["gm_dialin_local_other"] = "15";
        $pgi_form["gm_dialin_local_area1"] = "15";
        $pgi_form["gm_dialin_local_area2"] = "20";
        $pgi_form["gm_dialin_local_area3"] = "25";
        $pgi_form["gm_dialin_local_area4"] = "30";
        $pgi_form["gm_dialin_navidial"] = "25";
        $pgi_form["gm_dialin_free"] = "25";
        $pgi_form["gm_dialin_free_mobile"] = "40";
        $pgi_form["gm_dialin_free_area1"] = "20";
        $pgi_form["gm_dialin_free_area2"] = "20";
        $pgi_form["gm_dialin_free_area3"] = "30";
        $pgi_form["gm_dialin_free_area4"] = "60";
        $pgi_form["gm_dialout"] = "20";
        $pgi_form["gm_dialout_mobile"] = "50";
        $pgi_form["gm_dialout_area1"] = "20";
        $pgi_form["gm_dialout_area2"] = "25";
        $pgi_form["gm_dialout_area3"] = "40";
        $pgi_form["gm_dialout_area4"] = "70";
        $pgi_form["gm_dialout_area5"] = "1000";

        return $pgi_form;
    }

    /**
     * メンバー課金ユーザーのサービスオプション
     */
    public function action_individual($message = "")
    {
        $this->set_submit_key($this->_name_space);
        $user_key = $this->request->get("user_key");
        $user_key = ( ! $user_key ) ? $this->session->get( "user_key" ) : $user_key;

        $objUser = new N2MY_DB( $this->get_dsn(), "user" );
        $user_info = $objUser->getRow("user_key='".$user_key."'");
        $this->template->assign( "user_key", $user_key );
        $this->template->assign( "user_info", $user_info );
        $this->session->set( "user_key", $user_key );

        if ($centre_plan = $this->config->get("CENTRE", "plan_list")) {
            $service_db = new N2MY_DB( $this->account_dsn, "service" );
            $service_option_list = $service_db->getRowsAssoc("service_key IN (".addslashes($centre_plan).") AND service_status = '1'");
            $this->template->assign("centre_plan_list", $service_option_list );
        }
        if($this->request->get("model") == "sales") {
          $service_db = new N2MY_DB( $this->account_dsn, "service" );
          $service_option_list = $service_db->getRowsAssoc("use_sales_plan = '1' AND service_status = '1'");
          $this->template->assign("sales_plan_list", $service_option_list );
        }

        if($this->request->get("model") == "roomsetting") {
            $service_db = new N2MY_DB( $this->account_dsn, "service" );
            $service_option_list = $service_db->getRowsAssoc("use_one_plan = '1' AND service_status = '1'");
            $this->template->assign("roomsetting_plan_list", $service_option_list );
        }

        // ユーザープラン取得
        $service_db = new N2MY_DB( $this->account_dsn, "service" );
        $member_service_list = $service_db->getRowsAssoc("use_member_plan = '1' AND service_status = '1'");
        $this->template->assign("member_service_list", $member_service_list );


//        $this->session->set("table", "user_serviceplan");
        $sort_key_plan = $this->request->get("sort_key_plan");
        $sort_type_plan = $this->request->get("sort_type_plan");

        $sort_key_plan = isset($sort_key_plan) ? $sort_key_plan : "user_plan_key";
        $sort_type_plan = isset($sort_type_plan) ? $sort_type_plan : "desc";
        //プラン情報取得
        $objUserPlan = new N2MY_DB( $this->get_dsn(), "user_plan" );
        $where = "user_key='".$user_key."'";
        $plan_list = $objUserPlan->getRowsAssoc( $where, array($sort_key_plan => $sort_type_plan) );
        $this->logger->debug("plan_list",__FILE__,__LINE__, $plan_list );
        $this->template->assign( "user_plan_list", $plan_list );

        $user_plan_columns = $objUserPlan->getTableInfo( "data" );
        $this->logger->trace("new_columuns",__FILE__,__LINE__,$user_plan_columns);
        $this->template->assign("user_plan_columns",$user_plan_columns);
        //現在有効なプランを取得
        $where_now = "user_key='".$user_key."'".
                 " AND user_plan_status = 1";
        if($this->request->get("model") == "sales") {
          $where_now .= " AND sales_plan_flg = '1'";
        }
        $now_plan_info = $objUserPlan->getRow( $where_now );
        //現在有効なプランが無い場合は開始待ちのプランを取得
        if (!$now_plan_info) {
            $where_now = "user_key='".$user_key."'".
                 " AND user_plan_status = 2";
            $now_plan_info = $objUserPlan->getRow( $where_now );
        }

        /* 登録されているプランがあればサービス名を取得*/
        if( $now_plan_info ){
            $objService = new N2MY_DB( $this->account_dsn, "service" );
            $where = sprintf( "service_key=%s", $now_plan_info["service_key"] );
            $now_plan_info = $objService->getRow( $where );
        }
        $this->template->assign( "now_plan_info", $now_plan_info );

        //オプション情報取得
        $sort_key_option = $this->request->get("sort_key_option");
        $sort_type_option = $this->request->get("sort_type_option");
        $sort_key_option = isset($sort_key_option) ? $sort_key_option : "user_service_option_key";
        $sort_type_option = isset($sort_type_option) ? $sort_type_option : "desc";

        $service_option_db = new N2MY_DB( $this->account_dsn, "service_option" );
        if($this->request->get("model") == "option") {
          $service_option = $service_option_db->getRowsAssoc("option_status = 1 AND use_user_option = 1");
        }else {
          $service_option = $service_option_db->getRowsAssoc("option_status = 1");
        }

        $objUserServiceOption = new N2MY_DB($this->get_dsn(), "user_service_option");
        $option_columns = $objUserServiceOption->getTableInfo("data");
        $option_columns = $this->_get_relation_data( $this->account_dsn, $option_columns);
        $where = sprintf( "user_key=%s AND service_option_key != 14", $user_key );
        $option_data = $objUserServiceOption->getRowsAssoc( $where, array($sort_key_option => $sort_type_option));
        $where = sprintf( "user_key=%s AND service_option_key = 14", $user_key );
        $supoprt_data = $objUserServiceOption->numRows( $where );
        $this->logger->debug("option_data",__FILE__,__LINE__,$option_data);
        $this->template->assign("page", $page);
        $this->template->assign("support_num",$supoprt_data);
        $this->template->assign("service_option",$service_option);
        $this->template->assign("message",$message);
        $this->template->assign("model",$this->request->get("model"));
        $this->template->assign("option_columns",$option_columns);
        $this->template->assign("option_data",$option_data);
        $template = "viewer_tool/individual/detail.t.html";
        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            require_once("classes/dbi/pgi_setting.dbi.php");
            require_once('classes/pgi/PGiSystem.class.php');
            require_once("classes/pgi/PGiRate.class.php");
            $pgi_setting_table = new N2MY_DB($this->get_dsn(), "user_pgi_setting");
            $pgi_settings      = $pgi_setting_table->getRowsAssoc("is_deleted = 0 AND user_key = " . $user_key);
            $pgi_systems       = PGiSystem::findConfigAll();
            $pgi_rate_config   = PGiRate::findConfigAll();
            $pgi_gm_rate_config   = PGiRate::findGmConfigAll();
            $default_system_key= $this->config->get("PGI", 'system_key');
            if (0 < count($pgi_settings)) {
                $pgi_setting_keys = array();
                foreach ($pgi_settings as $setting) {
                    $pgi_setting_keys[] = $setting['user_pgi_setting_key'];
                }
                $pgi_rates = $this->getUserPGiRateBySettingKey($pgi_setting_keys);
            }

            $this->template->assign(array(
                    "pgi_systems"               => @$pgi_systems,  // if enabled pgi
                    "json_pgi_systems"          => @json_encode($pgi_systems),  // if enabled pgi
                    "pgi_settings"              => @$pgi_settings, // if enabled pgi
                    "pgi_rate_config"           => @$pgi_rate_config,
                    "pgi_gm_rate_config"        => @$pgi_gm_rate_config,
                    "pgi_rates"                 => @$pgi_rates,
                    "default_system_key"        => @$default_system_key,
            ));
        }
        $this->_display( $template );
    }

	/**
	 * One プランセティング
	 */
	 public function action_one_plan_setting($message=""){
        $this->set_submit_key($this->_name_space);
        $user_key = $this->request->get("user_key");
        $user_key = !$user_key ? $this->session->get("user_key") : $user_key;
        $objUser = new N2MY_DB($this->get_dsn(), "user");
        $user_info = $objUser->getRow("user_key = " . $user_key);
        $this->template->assign("user_key", $user_key);
        $this->session->set("user_key", $user_key);
    	$service_db = new N2MY_DB($this->account_dsn, "service");
    	$service_list = $service_db->getRowsAssoc("use_one_plan = '1' AND service_status = '1'");
    	$this->template->assign("service_list", $service_list);
        $sort_key_plan  = $this->request->get("sort_key_plan");
        $sort_type_plan = $this->request->get("sort_type_plan");
        $sort_key_plan = isset($sort_key_plan) ? $sort_key_plan : "user_plan_key";
        $sort_type_plan = isset($sort_type_plan) ? $sort_type_plan : "desc";
        //プラン情報取得
        $objUserPlan = new N2MY_DB($this->get_dsn(), "user_plan");
        $where = "user_key = " . $user_key;
        $plan_list = $objUserPlan->getRowsAssoc($where, array($sort_key_plan => $sort_type_plan));
        $this->template->assign("user_plan_list", $plan_list);
        $user_plan_columns = $objUserPlan->getTableInfo("data");
        $this->template->assign("user_plan_columns", $user_plan_columns);
        //現在有効なプランを取得
        $where_now = "user_plan_status = 1 AND user_key =" . $user_key;
        $now_plan_info = $objUserPlan->getRow($where_now);
        //現在有効なプランが無い場合は開始待ちのプランを取得
        if (!$now_plan_info) {
            $where_now = "user_plan_status = 2 AND user_key=" . $user_key;
            $now_plan_info = $objUserPlan->getRow($where_now);
        }
        /* 登録されているプランがあればサービス名を取得*/
        if($now_plan_info){
            $where = "service_key= " . $now_plan_info["service_key"];
            $now_plan_info = $service_db->getRow($where);
        }
        $this->template->assign("now_plan_info", $now_plan_info);

        //オプション情報取得
        $sort_key_option = $this->request->get("sort_key_option");
        $sort_type_option = $this->request->get("sort_type_option");
        $sort_key_option = isset($sort_key_option) ? $sort_key_option : "user_service_option_key";
        $sort_type_option = isset($sort_type_option) ? $sort_type_option : "desc";
        $service_option_db = new N2MY_DB($this->account_dsn, "service_option");
        $service_option = $service_option_db->getRowsAssoc("option_status = 1");
        $objUserServiceOption = new N2MY_DB($this->get_dsn(), "user_service_option");
        $option_columns = $objUserServiceOption->getTableInfo("data");
        $option_columns = $this->_get_relation_data( $this->account_dsn, $option_columns);
		$where = "service_option_key != 14 AND user_key = " . $user_key;
        $option_data = $objUserServiceOption->getRowsAssoc($where, array($sort_key_option => $sort_type_option));
        $this->template->assign("page", $page);
        $this->template->assign("service_option",$service_option);
        $this->template->assign("message",$message);
        $this->template->assign("option_columns",$option_columns);
        $this->template->assign("option_data",$option_data);
        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            require_once("classes/dbi/pgi_setting.dbi.php");
            require_once('classes/pgi/PGiSystem.class.php');
            require_once("classes/pgi/PGiRate.class.php");
            $pgi_setting_table = new N2MY_DB($this->get_dsn(), "user_pgi_setting");
            $pgi_settings      = $pgi_setting_table->getRowsAssoc("is_deleted = 0 AND user_key = " . $user_key);
            $pgi_systems       = PGiSystem::findConfigAll();
            $pgi_rate_config   = PGiRate::findConfigAll();
            $pgi_gm_rate_config   = PGiRate::findGmConfigAll();
            $default_system_key= $this->config->get("PGI", 'system_key');
            if (0 < count($pgi_settings)) {
                $pgi_setting_keys = array();
                foreach ($pgi_settings as $setting) {
                    $pgi_setting_keys[] = $setting['user_pgi_setting_key'];
                }
                $pgi_rates = $this->getUserPGiRateBySettingKey($pgi_setting_keys);
            }
            $this->template->assign(array(
                    "pgi_systems"               => @$pgi_systems,  // if enabled pgi
                    "json_pgi_systems"          => @json_encode($pgi_systems),  // if enabled pgi
                    "pgi_settings"              => @$pgi_settings, // if enabled pgi
                    "pgi_rate_config"           => @$pgi_rate_config,
                    "pgi_gm_rate_config"        => @$pgi_gm_rate_config,
                    "pgi_rates"                 => @$pgi_rates,
                    "default_system_key"        => @$default_system_key,
            ));
        }
        $template = "viewer_tool/one_plan/detail.t.html";
        $this->_display( $template );
	 }

    /**
     * DB状況
     */
    function action_db_status() {
        require_once("classes/dbi/user.dbi.php");
        $user = new UserTable($this->get_dsn());
        $db_status_rs = $user->_conn->query("SHOW GLOBAL STATUS");
        while ($db_status_row = $db_status_rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $val_name = $db_status_row["Variable_name"];
            $section = substr($val_name, 0, strpos($val_name, "_"));
            $name = substr($val_name, (strpos($val_name, "_") + 1));
            $rows[$section][$name] = $db_status_row["Value"];
        }
        $this->template->assign("rows", $rows);
        $this->_display("viewer_tool/db/index.t.html");
    }

    /*
     * HDD容量取得
     */
    function action_user_hdd() {
        require_once("classes/dbi/user.dbi.php");
        // 部屋一覧
        $user_obj = new UserTable($this->get_dsn());
        $option_db = new N2MY_DB($this->get_dsn(), "ordered_service_option");
        $sql = "SELECT user.user_id" .
                ", user_company_name" .
                ", room.room_key" .
                " FROM user, room" .
                " WHERE user.user_key = room.user_key" .
                " GROUP BY room.room_key" .
                " ORDER BY user_id, room_key";
        $rs = $user_obj->_conn->query($sql);
        while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $user_id = $row["user_id"];
            $room_key = $row["room_key"];
            $room_data[$user_id][$room_key] = $row;
            $where = "room_key = '".addslashes($room_key)."'".
                    "AND service_option_key = '3'".
                    "AND ordered_service_option_status = '1'";
            $total_count = $option_db->numRows($where);
            $this->logger2->trace($total_count);
            $hdd_ext[$room_key] = $total_count;

        }
        // 容量取得
        $meeting_db = new N2MY_DB($this->get_dsn(), "meeting");
        $sql = "SELECT room_key, sum(meeting_size_used) as hdd_use_size" .
                " FROM `meeting`" .
                " GROUP BY room_key";
        $rs = $meeting_db->_conn->query($sql);
        while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $room_key = $row["room_key"];
            $meeting_hdd[$room_key] = $row["hdd_use_size"];
        }
        foreach($room_data as $user_id => $user) {
            foreach($user as $room_key => $room) {
                $max_size = (1024 * 1024 * 500) + (1024 * 1024 * 1024 * (($hdd_ext[$room_key]) ? $hdd_ext[$room_key] : 0));
                $uze_size = isset($meeting_hdd[$room_key]) ? $meeting_hdd[$room_key] : "0";
                $room_data[$user_id][$room_key]["max_size"] = number_format($max_size);
                $room_data[$user_id][$room_key]["uze_size"] = number_format($uze_size);
                $room_data[$user_id][$room_key]["use_percent"] = ($uze_size / $max_size * 100);
            }
        }
        $this->template->assign("room_data", $room_data);
        $this->_display("viewer_tool/total/use_hdd_size.t.html");
    }

    /*
     * 利用統計（ユーザー毎）
     */
    function action_user_total () {
        //アカウント管理のユーザーから来た場合はserverをセッションに登録
        $server_key = $this->request->get("server_key");
        if ($server_key) {
            require_once("classes/mgm/MGM_Auth.class.php");
            $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
            $server_info = $obj_MGMClass->getServerInfo($server_key);
        }
        if (isset($server_info["dsn"])) {
            $this->set_dsn($server_info["dsn"]);
            $this->session->set("now_server_key", $server_key);
        }

        $user_id = $this->request->get("user_id");
        $start = $this->request->get("start", date("Y-m-01 00:00:00"));
        $end = $this->request->get("end", date("Y-m-t 23:59:59"));
        $user_obj = new N2MY_DB($this->get_dsn(), "user");
        $where = "user_id ='".addslashes($user_id)."'";
        $user_data = $user_obj->getRow($where);

        if (isset($user_id) && isset($user_data)) {
            //部屋の情報を取得
            $room_obj = new N2MY_DB($this->get_dsn(), "room");
            $where = "user_key ='".addslashes($user_data["user_key"])."'";
            $rooms = $room_obj->getRowsAssoc($where, null, null, null, "room_key, room_name");
            $use_data = array();
            $total["use_time"] = "";
            $total["count"] = "";
            foreach ($rooms as $room) {
                $this->logger->debug("room_key",__FILE__,__LINE__,$room["room_key"]);
                //部屋毎の会議データを取得
                $meeting_obj = new N2MY_DB($this->get_dsn(), "meeting");
                $where = "room_key ='".mysql_real_escape_string($room["room_key"])."'".
                         " AND create_datetime >= '".addslashes($start)."'".
                         " AND create_datetime <= '".addslashes($end)."'".
                         " AND use_flg = '1'";
                $meeting_data = $meeting_obj->getRowsAssoc($where);
                $use_data[$room["room_key"]]["room_name"] = $room["room_name"];
                $use_data[$room["room_key"]]["use_time"] = "0";
                foreach ($meeting_data as $data) {
                    $use_data[$room["room_key"]]["use_time"] += $data["meeting_use_minute"];
                }
                $use_data[$room["room_key"]]["use_count"] = $meeting_obj->numRows($where);
                //全部屋のトータル
                $total["use_time"] += $use_data[$room["room_key"]]["use_time"];
                $total["count"] += $use_data[$room["room_key"]]["use_count"];
            }
            $this->template->assign("use_data", $use_data);
            $this->template->assign("total", $total);
        }
        $this->template->assign("user_id", $user_id);
        $this->template->assign("start", $start);
        $this->template->assign("end", $end);
        $this->_display("viewer_tool/total/room_log.t.html");
    }

    function _total_form() {
        // 年
        for($year = 2006; $year <= date("Y"); $year++ ) {
            $year_list[] = $year;
        }
        // 月
        for($month = 1; $month <= 12; $month++ ) {
            $month_list[] = $month;
        }
        $this->template->assign("year_list", $year_list);
        $this->template->assign("month_list", $month_list);
    }

    public function action_total_user() {
        require("classes/dbi/user.dbi.php");
        $objUser = new UserTable($this->get_dsn());
        $columns = $objUser->getTableInfo("data");
        // いらない検索フォーム取り除く
        unset($columns["user_key"]);
        unset($columns["country_key"]);
        unset($columns["user_password"]);
        unset($columns["user_admin_password"]);
        unset($columns["user_company_address"]);
        unset($columns["user_company_address"]);
        unset($columns["user_company_phone"]);
        unset($columns["user_company_fax"]);
        unset($columns["user_staff_department"]);
        unset($columns["user_staff_firstname"]);
        unset($columns["user_staff_lastname"]);
        unset($columns["max_member_count"]);
        unset($columns["staff_key"]);
        unset($columns["user_staff_email"]);
        unset($columns["user_company_postnumber"]);
        unset($columns["user_registtime"]);
        unset($columns["user_updatetime"]);
        unset($columns["addition"]);
        unset($columns["max_rec_size"]);
//        unset($columns["user_starttime"]);
//        unset($columns["user_deletetime"]);

        $this->template->assign("columns", $columns);
        // リセット
        $reset = $this->request->get("reset");
        if ($reset == 1) {
            $this->session->remove("temp", "action_total_user");
        }
        $request = $this->request->getAll();
        $action_name = __FUNCTION__;
        $form_data = $this->set_form($action_name, "user_key", "asc");
        // 月別の表示
        $yy = ($form_data["request"]["yy"]) ? $form_data["request"]["yy"] : date("Y");
        $mm = ($form_data["request"]["mm"]) ? $form_data["request"]["mm"] : date("m");
        $now_date = mktime(0,0,0,$mm,1,$yy);
        $prev_date = mktime(0,0,0,$mm - 1,1,$yy);
        $next_date = mktime(0,0,0,$mm + 1,1,$yy);
        $this->template->assign("now_date", $now_date);
        $this->template->assign("prev_date", $prev_date);
        $this->template->assign("next_date", $next_date);
        $this->logger2->info($form_data);
        // 値を設定
        if (isset($form_data["request"])) {
            $request = $form_data["request"];
            $this->template->assign("request",$request);
        }
        $page    = $form_data['page'];
        $limit   = $form_data['page_cnt'];
        $offset  = ($limit * ($page - 1));
        // 検索条件設定・取得
        $where = $objUser->getWhere($columns, $request);
        $sql = "select user.*" .
            ", SUM(meeting.meeting_use_minute) AS meeting_use_minute" .
            " FROM user" .
            " LEFT JOIN meeting ON user.user_key = meeting.user_key" .
            " AND actual_start_datetime like '".date("Y-m",$now_date)."%'";
        if ($where) {
            $sql .= " WHERE ".$where;
        }
        $sql .= " GROUP BY meeting.user_key";
        if (strtolower($form_data['sort_type']) == "desc") {
            $sql .= " ORDER BY ".$form_data['sort_key']." DESC";
        } else {
            $sql .= " ORDER BY ".$form_data['sort_key']." ASC";
        }
        // CSVダウンロード
        if ($_REQUEST["csv"]) {
            $ret = $objUser->_conn->query($sql);
        } else {
            $ret = $objUser->_conn->limitQuery($sql, $offset, $limit);
        }
        $rows = array();
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
        } else {
            while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
                $rows[] = $row;
            }
        }
        if ($_REQUEST["csv"]) {
            require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            $csv = new EZCsv(true, "UTF-8", "UTF-8");
            $dir = $this->get_work_dir();
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");
            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="'.date("Ym").'.csv"');
            // ヘッダ２
            $header = array(
                "user_id" => "User ID",
                "user_company_name" => "User Name",
                "user_starttime" => "Start Time",
                "user_deletetime" => "Stop Time",
                "user_status" => "Status",
                "delete_status" => "Delete Flag",
                "account_model" => "Pay Mode",
                "invoice_flg" => "Invocie Flag",
                "meeting_use_minute" => "Meeting Time(min)",
                );
            $csv->setHeader($header);
            $csv->write($header);
            foreach($rows as $row) {
                $csv->write($row);
            }
            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            $contents = fread($fp, filesize($tmpfile));
            fclose($fp);
            print $contents;
            unlink($tmpfile);
            return true;
        }
        // ページャー
        $total_count = $objUser->numRows($where);
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => $action_name,
            "after_action" => $this->request->get("after_action"))
            );
        $this->template->assign("rows", $rows);
        $this->_display("viewer_tool/total/top.t.html");
    }

    /**
     * ユーザー別の利用時間集計
     */
    public function action_total_user2() {
        static $user_columns;
        $objMeetingDateLog = new N2MY_DB( N2MY_MDB_DSN, "meeting_date_log");
        $server_list = parse_ini_file( N2MY_APP_DIR."config/server_list.ini", true);
        require("classes/dbi/user.dbi.php");
        $users = array();
        foreach($server_list["SERVER_LIST"] as $key => $dsn) {
            $objUser = new UserTable( $dsn , "user");
            if (!$user_columns) {
                $user_columns = $objUser->getTableInfo("data");
            }
            $rows = $objUser->getRowsAssoc();
            foreach ($rows as $user_info) {
                $users[$user_info["user_id"]] = $user_info;
            }
        }
        $sql = "SELECT user_id" .
            ",SUM(use_time) AS use_time" .
            ",SUM(use_time_mobile_normal) AS use_time_mobile_normal" .
            ",SUM(use_time_mobile_special) AS use_time_mobile_special" .
            ",SUM(use_time_hispec) AS use_time_hispec" .
            ",SUM(use_time_audience) AS use_time_audience" .
            ",SUM(use_time_h323) AS use_time_h323" .
            ",SUM(use_time_h323ins) AS use_time_h323ins" .
            /*
            ",SUM(account_time_mobile_normal) AS account_time_mobile_normal" .
            ",SUM(account_time_mobile_special) AS account_time_mobile_special" .
            ",SUM(account_time_hispec) AS account_time_hispec" .
            ",SUM(account_time_audience) AS account_time_audience" .
            ",SUM(account_time_h323) AS account_time_h323" .
            ",SUM(account_time_h323ins) AS account_time_h323ins" .
            */
            ",SUM(meeting_cnt) AS meeting_cnt" .
            ",SUM(participant_cnt) AS participant_cnt" .
            " FROM meeting_date_log";
        $request = $this->request->getAll();
        if (isset($request["year"])) {
            $request["year"] = date("Y");
        }
        if (isset($request["month"])) {
            $request["month"] = date("m");
        }
        $cond = $request["cond"];
        $cond["log_date"] = $request["year"]."-".$request["month"];
        $columns = $objMeetingDateLog->getTableInfo("account");
        $columns["log_date"]["item"]["search"] = "like";
        $where = $objMeetingDateLog->getWhere($columns, $cond);
        if ($where) {
            $sql .= " WHERE ".$where;
        }
        $sql = $sql." GROUP BY user_id";
        $rs = $objMeetingDateLog->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
        }
        $user_logs = array();
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $row["user_info"] = $users[$row["user_id"]];
            $user_logs[] = $row;
        }
        $this->logger2->info(array($user_logs, $sql));
        unset($columns["log_no"]);
        unset($columns["log_date"]);
        unset($columns["staff_id"]);
        unset($columns["room_key"]);
        unset($columns["account_time_mobile_normal"]);
        unset($columns["account_time_mobile_special"]);
        unset($columns["account_time_hispec"]);
        unset($columns["account_time_audience"]);
        unset($columns["account_time_h323"]);
        unset($columns["account_time_h323ins"]);
        unset($columns["create_datetime"]);
        unset($columns["update_datetime"]);
        // 年
        for($year = 2006; $year <= date("Y"); $year++ ) {
            $year_list[] = $year;
        }
        // 月
        for($month = 1; $month <= 12; $month++ ) {
            $month_list[] = $month;
        }
        $this->template->assign("year_list", $year_list);
        $this->template->assign("month_list", $month_list);

        $this->template->assign("request", $request);
        $this->template->assign("user_columns", $user_columns);
        $this->template->assign("columns", $columns);
        $this->template->assign("user_logs", $user_logs);
        $this->_display("viewer_tool/total/top.t.html");
    }

    public function action_shift()
    {
        $this->_display("viewer_tool/shift.t.html");
    }

    public function action_shift2()
    {
        $request = $this->request->getAll();

        $this->session->set( "dbList", $request );
        $this->_display("viewer_tool/shift2.t.html");
    }

    function getRule($columns) {
        $rules = array();
        foreach($columns as $colum => $colum_info) {
            $this->logger->trace("not_null",__FILE__,__LINE__,$colum_info);
            if (($colum_info["flags"]["not_null"] == "1") && ($colum_info["flags"]["auto_increment"] == "")) {
                $rules[$colum]["required"] = true;
            }
        }
        $this->logger2->trace($rules);
        return $rules;
    }

    /**
     * デフォルトページ
     */
    function default_view(){
        return $this->render_main();
    }

//===================================================================
// メニュー
//===================================================================
    /**
     * メインページ（フレームでメニューとコンテンツを分ける）
     */
    function action_main() {
        return $this->render_main();
    }

    /**
     * メニュー（権限によって表示切り替えなどを行う。但し、権限はチェックは表示側でも行う）
     */
    function action_menu() {
        return $this->render_menu();
    }

    /**
     * トップページ（統計情報やイベント管理など、ログイン直後の画面）
     */
    function action_top() {
        $staff_auth = $_SESSION["_authsession"]["data"]["authority"];
        if ($staff_auth == 3) {
            return $this->action_total_user();
        }
        $this->session->remove("sort_key");
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );

        $this->request->set("table", $this->request->get("table"));
        $this->request->set("type", $this->request->get("type"));
        $this->request->set("check", $this->request->get("check"));
        $form[$this->request->get("key")] = $this->request->get("value");
        $this->request->set("form", $form);
        $this->request->set("sort", $this->request->get("key"));
        $this->request->set("reset", $this->request->get("reset"));
        return $this->action_list();
    }

    /**
     * ログアウト
     */
    function action_logout() {
        $url = $this->get_redirect_url("viewer_tool/index.php");
        $this->_auth->logout();
        unset($_SESSION[$this->_sess_page]);
        header("Location: ".$url);
    }

    /**
     * Flash Version統計用（一時的なバージョンです）
     */
    function action_flash_version() {
        $objFlash = new N2MY_DB($this->get_dsn(), "flash_version");
        $columns = $objFlash->getTableInfo("data");
        $action_name = __FUNCTION__;
        $form_data = $this->set_form($action_name, "user_key", "asc");
        $this->logger2->info($form_data);
        $request = $form_data["request"];
        // 月別の表示
        $yy = ($request["yy"]) ? $request["yy"] : date("Y");
        $mm = ($request["mm"]) ? $request["mm"] : date("m");
        $now_date = mktime(0,0,0,$mm,1,$yy);
        $prev_date = mktime(0,0,0,$mm - 1,1,$yy);
        $next_date = mktime(0,0,0,$mm + 1,1,$yy);
        $this->template->assign("now_date", $now_date);
        $this->template->assign("prev_date", $prev_date);
        $this->template->assign("next_date", $next_date);
        $where = "";
        if ($request["user_id"]) {
            $where .= " AND user.user_id like '%".addslashes($request["user_id"])."%'";
        }
        if ($request["user_status"]) {
            $where .= " AND user.user_status = '".addslashes($request["user_status"])."'";
        }
        if ($request["flash_version"]) {
            foreach($request["flash_version"] as $version) {
                $_cond[] = "flash_version like '".$version."%'";
            }
            $where .= " AND (".join($_cond, " OR ").")";
        }
        //
        if ($request["create_datetime"]["min"]) {
            $where = " AND flash_version.create_datetime >= '".addslashes($request["create_datetime"]["min"])."'";
        }
        if ($request["create_datetime"]["max"]) {
            $where = " AND flash_version.create_datetime <= '".addslashes($request["create_datetime"]["max"])."'";
        }
        $this->logger2->info($where);
        // 検索条件設定・取得
        $sql = "SELECT user.user_id" .
                ", user.user_company_name" .
                ", flash_version" .
                ", count(*) as count" .
                " FROM user, flash_version" .
            " WHERE user.user_id = flash_version.user_id".
            $where.
            " GROUP BY user.user_id, flash_version";
        $this->logger2->info($sql);
        $rs = $objFlash->_conn->query($sql);
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        $this->logger2->info($rows);
        require("classes/dbi/user.dbi.php");
        $objUser = new UserTable($this->get_dsn());
        $user_columns = $objUser->getTableInfo("data");
        // 月
        for($version = 5; $version <= 10; $version++ ) {
            $version_list[$version] = $version;
        }
        $this->template->assign("page", array(
            "action" => $action_name)
            );
        $this->template->assign("version_list", $version_list);
        $this->template->assign("user_columns",$user_columns);
        $this->template->assign("rows",$rows);
        $this->display("viewer_tool/total/flash_version.t.html");
    }

//===================================================================
// 表示
//===================================================================
    function render_main() {
        // セッションにdsnを登録
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $server_info = "";
        $server_key = $this->request->get("server_key");
        $account_db = new N2MY_DB($this->account_dsn, "db_server");
        $db_server_list = $account_db->getRowsAssoc("server_status = 1");
        $this->template->assign("db_server_list", $db_server_list);
        if ($server_key) {
            $server_info = $obj_MGMClass->getServerInfo($server_key);
        } else {
            $server_key = $db_server_list[0]["server_key"];
            $server_info = $obj_MGMClass->getServerInfo($server_key);
        }
        $this->session->set("now_server_key", $server_key);
        if (isset($server_info["dsn"])) {
            $this->set_dsn($server_info["dsn"]);
        }
        $this->logger->trace("dsn",__FILE__,__LINE__,$this->get_dsn());
        $this->display("viewer_tool/index.t.html");
    }

    function render_menu() {
        $server_key = $this->session->get("now_server_key");
        $this->logger->debug("server_key",__FILE__,__LINE__,$server_key);
        $account_db = new N2MY_DB($this->account_dsn, "db_server");
        $db_server_list = $account_db->getRowsAssoc("server_status = 1");
        $this->logger2->debug($db_server_list);
        $this->logger->trace("dsn",__FILE__,__LINE__,$this->get_dsn());
        $this->template->assign("select_server_key", $server_key);
        $this->template->assign("db_server_list", $db_server_list);
        $this->_display("viewer_tool/menu.t.html");
    }

    /**
     * フォームの内容を再現
     */
    function set_form($form_name, $default_sort_key, $default_sort_type) {
        //
        // デフォルトのソート順指定
        $sort_key = $this->request->get("sort_key");
        $sort_type = $this->request->get("sort_type");
        $page = $this->request->get("page");
        $page_cnt = $this->request->get("page_cnt");
        $reset = $this->request->get("reset");
        // デフォルト
        if (!isset($_SESSION[$this->_sess_page][$form_name]) || $reset == 1 || $reset == 2) {
            $_SESSION[$this->_sess_page][$form_name]['sort_key']  = $default_sort_key;
            $_SESSION[$this->_sess_page][$form_name]['sort_type'] = $default_sort_type;
            $_SESSION[$this->_sess_page][$form_name]['page'] = 1;
            $_SESSION[$this->_sess_page][$form_name]['page_cnt'] = 20;
        }
        if ($reset == 1) {
        $this->logger->debug("session",__FILE__,__LINE__,$_SESSION);
            unset($_SESSION[$this->_sess_page][$form_name]["request"]);
        } else {
            // ソート
            if ($sort_key) {
                $_SESSION[$this->_sess_page][$form_name]['sort_key']  = $sort_key;
                $_SESSION[$this->_sess_page][$form_name]['sort_type'] = $sort_type;
            }
            // ページ
            if ($page) {
                $_SESSION[$this->_sess_page][$form_name]['page'] = $page;
            }
            // ページ
            if ($page_cnt) {
                $_SESSION[$this->_sess_page][$form_name]['page_cnt'] = $page_cnt;
            }
            $request = $this->request->get("form");
            if ($request) {
                $_SESSION[$this->_sess_page][$form_name]['page'] = 1;
                $_SESSION[$this->_sess_page][$form_name]["request"] = $request;
            }
        }
        return $_SESSION[$this->_sess_page][$form_name];
    }

    /**
     * クエリーを自動で作成
     */
    function _get_query($columns, $request, $and_or = "AND") {
        $where = "";
        $and_or = " ".$and_or." ";
        $condition = array();
        foreach($columns as $key => $item){
            $fields .= ",".$key;
            switch ($item["type"]) {
            // 数値
            case "integer":
                switch($item["search"]) {
                // 同一
                case "equal":
                    if ($request[$key]) {
                        $condition[] = $key." = ".addslashes($request[$key]);
                    }
                    break;
                // 範囲指定
                case "range":
                    if ($request[$key]["min"]) {
                        $condition[] = $key." >= ".addslashes($request[$key]["min"]);
                    }
                    if ($request[$key]["max"]) {
                        $condition[] = $key." <= ".addslashes($request[$key]["max"]);
                    }
                    break;
                }
                break;
            // 文字列
            case "string":
                if ($request[$key] !== "") {
                    switch($item["search"]) {
                    // 同一
                    case "equal":
                        $condition[] = $key." = '".addslashes($request[$key])."'";
                        break;
                    case "prefix":
                        $condition[] = $key." like '".addslashes($request[$key])."%'";
                        break;
                    case "suffix":
                        $condition[] = $key." like '%".addslashes($request[$key])."'";
                        break;
                    case "like":
                        $condition[] = $key." like '%".addslashes($request[$key])."%'";
                        break;
                    }
                }
                break;
            case "datetime":
                if ($request[$key]["min"]) {
                    $condition[] = $key." >= '".$request[$key]["min"]."'";
                }
                if ($request[$key]["max"]) {
                    $condition[] = $key." <= '".$request[$key]["max"]."'";
                }
                break;
            }
            if ($condition) {
                $where = implode($and_or, $condition);
            }
        }
        $fields = substr($fields, 1);
        $ret = array(
            "fields" => $fields,
            "where" => $where
        );
        return $ret;
    }

    function _get_relation_data($dsn, $columns) {
        foreach ($columns as $key => $value) {
            if (isset($value["item"]["relation"])) {
                $relation = $value["item"]["relation"];
                if ($relation["db_type"] == "account") {
                    $relation_db = new N2MY_DB($this->account_dsn, $relation["table"]);
                } else {
                    $relation_db = new N2MY_DB($dsn, $relation["table"]);
                }
                if ($relation["status_name"]) {
                    $where = $relation["status_name"]."='".$relation["status"]."'";
                    $relation_data = $relation_db->getRowsAssoc($where);
                } else {
                    $relation_data = $relation_db->getRowsAssoc();
                }
                if ($relation["default_key"] || $relation["default_value"]) {
                    $columns[$key]["item"]["relation_data"][$relation["default_key"]] = $relation["default_value"];
                }
                foreach ($relation_data as $_data) {
                    $columns[$key]["item"]["relation_data"][$_data[$relation["key"]]] = $_data[$relation["value"]];
                }
            }
        }
        return $columns;
    }

    function _user_validator($form_data, $type) {
        //ユーザーID
        if ($type == "add") {
            if (!preg_match('/^[[:alnum:]._-]{3,30}$/', $form_data['user_id'])) {
                $message["user_id"] .= USER_ERROR_ID_LENGTH;
            }
        }
        //ユーザーパスワードをチェック
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $form_data['user_password'])) {
            $message["user_password"] .= USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$form_data['user_password']) || preg_match('/^[[:alpha:]]+$/',$form_data['user_password'])) {
            $message["user_password"] .= USER_ERROR_PASS_INVALID_02;
        }
        //管理者パスワード
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $form_data['user_admin_password'])) {
            $message["user_admin_password"] .= USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$form_data['user_admin_password']) || preg_match('/^[[:alpha:]]+$/',$form_data['user_admin_password'])) {
            $message["user_admin_password"] .= USER_ERROR_PASS_INVALID_02;
        }
        return $message;
    }

    function action_user_agent_summary() {
        require_once 'classes/mgm/dbi/user_agent.dbi.php';
        $objUserAgent = new MgmUserAgentTable(N2MY_MDB_DSN);
        $columns = $objUserAgent->getTableInfo();
        $condition = array();
        if ($start = $this->request->get("start")) {
            $condition[] = "create_datetime > '".$start."'";
        }
        if ($end = $this->request->get("end")) {
            $condition[] = "create_datetime < '".$end."'";
        }
        $where = join(" AND ", $condition);
        $as2_summary = array();
        $as3_summary = array();
        if ($where) {
            $where = " WHERE ".$where;
            $query = "SELECT date_format(create_datetime, '%Y-%m') as date" .
                    ", user_id" .
                    ", appli_version" .
                    ", COUNT(user_id) as cnt" .
                    " FROM user_agent".
                    $where.
                    " AND appli_version = 'as2'".
                    " GROUP BY date_format(create_datetime, '%Y-%m'), appli_version" .
                    " ORDER BY appli_version, date, user_id";
            $rs = $objUserAgent->_conn->query($query);
            if (!DB::isError($rs)) {
                while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $as2_summary[] = $row;
                }
            }
            $query = "SELECT date_format(create_datetime, '%Y-%m') as date" .
                    ", user_id" .
                    ", appli_version" .
                    ", COUNT(user_id) as cnt" .
                    " FROM user_agent".
                    $where.
                    " AND appli_version = 'as3'".
                    " GROUP BY date_format(create_datetime, '%Y-%m'), appli_version" .
                    " ORDER BY appli_version, date, user_id";
            $rs = $objUserAgent->_conn->query($query);
            if (!DB::isError($rs)) {
                while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $as3_summary[] = $row;
                }
            }
        }
        $this->template->assign("as2_summary", $as2_summary);
        $this->template->assign("as3_summary", $as3_summary);
        $this->display("viewer_tool/account/user_agent/index.t.html");
    }

    /**
     * 乱数ID発行
     */
    function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this->dec2any($c);
    }

    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=57, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr(
                    '23456789ab' .
                    'cdefghijkm' .
                    'nopqrstuvw' .
                    'xyzABCDEFG' .
                    'HJKLMNPQRS' .
                    'TUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }

    function _display ($template) {
        $staff_auth = $_SESSION["_authsession"]["data"]["authority"];
        $this->template->assign("authority", $staff_auth);
        $this->display($template);
    }

    function action_agency_list() {
        $agency_id = $this->request->get("agency_id");
        $table = $this->request->get("table");
        $this->logger->info($table);
        if ($agency_id) {
            $this->session->set("agency_id",$agency_id);
        } else {
            $agency_id = $this->session->get("table");;
        }
        if ($table) {
            $this->session->set("table",$table);
        } else {
            $table = $this->session->get("table");;
        }
        $sort_type = $this->request->get("sort_type");
        $action_name = __FUNCTION__;
        // 検索条件設定・取得
        $sort_type = $sort_type ? $sort_type : "desc";
        $form_data = $this->set_form($action_name, "user_key", $sort_type);
        $page    = $form_data['page'] ? $form_data['page']: "1";
        $limit   = $form_data['page_cnt'] ? $form_data['page_cnt'] : "20";
        $offset  = ($limit * ($page - 1));
        if ($table == "user") {
            $agencyRelationObj = new N2MY_DB($this->get_dsn(), "agency_relation_user");
            $sql = "select agency_relation_user.user_key, user.user_id, user.user_company_name, agency_relation_user.user_key" .
                " FROM agency_relation_user" .
                " LEFT JOIN user ON agency_relation_user.user_key = user.user_key" .
                " WHERE agency_relation_user.agency_id = ".addslashes($agency_id);
            $sql .= " GROUP BY agency_relation_user.user_key ".$sort_type;
        } elseif ($table == "member") {
            $agencyRelationObj = new N2MY_DB($this->get_dsn(), "agency_relation_member");
            $sql = "select agency_relation_member.member_key, member.member_id, member.member_name, agency_relation_member.member_key" .
                " FROM agency_relation_member" .
                " LEFT JOIN member ON agency_relation_member.member_key = member.member_key" .
                " WHERE agency_relation_member.agency_id = ".addslashes($agency_id);
            $sql .= " GROUP BY agency_relation_member.member_key ".$sort_type;
        }
        $total_count = $agencyRelationObj->numRows("agency_id = ".mysql_real_escape_string($agency_id));
        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager" => $pager_info,
            "action" => "action_agency_list",
            "after_action" => $this->request->get("after_action"))
            );
        $ret = $agencyRelationObj->_conn->limitQuery($sql, $offset, $limit);
        $rows = array();
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
        } else {
            while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
                $rows[] = $row;
            }
        }
        $this->template->assign("rows", $rows);
        if ($table == "user") {
            $this->display("viewer_tool/account/agency/user_list.t.html");
        } elseif ($table == "member") {
            $this->display("viewer_tool/account/agency/member_list.t.html");
        }
    }

    function action_table_info_builder() {
        $table = $this->request->get("table");
        $type  = $this->request->get("type");
        if (!$table || !$type) {
            print 'error';
        } else {
            if ($type == "account") {
                $dsn = $this->account_dsn;
            } else {
                $type = 'data';
                $dsn = $this->get_dsn();
            }
            $table = new N2MY_DB($dsn, $table);
            if (DB::isError($table)) {
                print $table->getUserInfo();
            } else {
                if (!$xml = $table->tableInfoBuilder($type)) {
                    print 'build error';
                } else {
                    print $xml;
                }
            }
        }
    }

    private function exitWithError($msg)
    {
        $b = debug_backtrace();

        $from = sprintf("called %s+%s : %s",$b[0]['file'], $b[0]['line'], $b[1]['function']);
        $this->logger->warn($msg.$from);

        exit($msg.$from);
    }
    //{{{validatePGiSetting
    private function validatePGiSetting($form, $room_key , $user_key = null )
    {
        $res = array('data'    => $form,
                     'message' => '');

        if (!$form["pgi_start_date"]) {
            $res['message'] .= "Please input the start time.<br />";
        } else {
            list($y, $m) = explode('/',$form["pgi_start_date"]);
            if (!checkdate($m, 1, $y)) {
                $res['message'] .= "Start Time is not correct.<br />";
            } else {
                $startdate = str_replace('/','-',$form["pgi_start_date"])."-01";
                // 過去分の設定はできません
                if ($startdate < date('Y-m').'-01') {
                    $res['message'].= "Can not be a past time.<br />";
                }
            }
        }
        $res['data']['startdate']  = $startdate;

        require_once('classes/pgi/PGiSystem.class.php');
        $pgiSystems    = PGiSystem::findConfigAll();
        $pgiSystemKeys = array();
        $selectedSystem         = null;
        foreach ($pgiSystems as $system) {
            if (@$form["system_key"] == (string)$system->key) {
                $selectedSystem = $system;
            }
        }
        if (!$form["system_key"]) {
            $res['message'] .= "Please input the system_key.<br />";
        } elseif (!$selectedSystem) {
            $res['message'] .= "The system_key is not correct.<br />";
        }

        if ($form["company_id_type"] == 'default') {
            $form["company_id"]        = $selectedSystem->defaultCompanyID;
            $res['data']['company_id'] = $selectedSystem->defaultCompanyID;
        }

        if (!$form["company_id"]) {
            $res['message'] .= "Please input the company_id.<br />";
        } else {
            if ($selectedSystem && $selectedSystem->rates) {
                require_once("classes/pgi/PGiRate.class.php");
                // validate rate's values
                $rate_config = PGiRate::findGmConfigAll();

                foreach ($rate_config as $rate){
                    $col  = (string)$rate->type;
                    $name = (string)$rate->description;
                    if (!$form[$col]) {
                        $res['message'] .= "Please input the ".$name.".<br />";
                    } elseif (is_int($form[$col])) {
                        $res['message'] .= $name." should be a number.<br />";
                    } elseif ($form[$col] <= 0) {
                        $res['message'] .= $name."should be larger than 0.<br />";
                    }
                }
            }
        }

        require_once("classes/dbi/pgi_setting.dbi.php");
        try {
            if(!$user_key){
                $pgi_setting_table = new PGiSettingTable($this->get_dsn());
                $pgi_settings  = $pgi_setting_table->findByRoomKey($room_key);
            }
            else{
                $pgi_setting_table = new N2MY_DB($this->get_dsn(), "user_pgi_setting");
                $pgi_settings      = $pgi_setting_table->getRowsAssoc("is_deleted = 0 AND user_key = " . $user_key);
            }
        } catch(Exception $e) {
            die('error'.$e->getMessage());
        }
        if (0 == count($pgi_settings)) {
            return $res;
        }

        foreach ($pgi_settings as $setting) {
            if ($startdate == $setting['startdate']) {
                $res['message'] .= $startdate."have already been set.(auto generate when meeting start）<br />";
                return $res;
            }
        }

        return $res;
    }
    function action_create_member_id() {
        $prefix = "VCH000";
        $member_list = array();
        for ($i = 1; $i <= 1000; $i++) {
            $member_list[] = array("id" => $prefix.sprintf("%04d",$i), "pass" => $this->create_id().rand(2,9));
        }
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            $csv = new EZCsv(true, "UTF-8", "UTF-8");
            $dir = $this->get_work_dir();
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");
            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="'.date("Ym").'.csv"');
            // ヘッダ２
            $header["id"] = "ID";
            $header["pass"] = "Password";
            $csv->setHeader($header);
            $csv->write($header);

           foreach($member_list as $row) {
                $csv->write($row);
            }
            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            while($str = fread($fp, 4096)) {
                print $str;
            }
            @fclose($fp);
            $this->logger2->info(memory_get_peak_usage());
            unlink($tmpfile);
            return true;

    }
    private function getPGiRateBySettingKey($pgi_setting_keys)
    {
        require_once "classes/dbi/pgi_rate.dbi.php";
        $pgi_rate_table = new PGiRateTable($this->get_dsn());
        $pgi_rates = $pgi_rate_table->findByPGiSettingKeys($pgi_setting_keys);
        if (count($pgi_rates) <= 0) {
            return array();
        }
        $res = array();
        foreach ($pgi_rates as $rate) {
            $res[$rate['pgi_setting_key']][$rate['rate_type']] = $rate['rate'];
        }
        return $res;
    }

    private function getUserPGiRateBySettingKey($pgi_setting_keys)
    {
        require_once "classes/dbi/user_pgi_rate.dbi.php";
        $pgi_rate_table = new UserPGiRateTable($this->get_dsn());
        $pgi_rates = $pgi_rate_table->findByPGiSettingKeys($pgi_setting_keys);
        if (count($pgi_rates) <= 0) {
            return array();
        }
        $res = array();
        foreach ($pgi_rates as $rate) {
            $res[$rate['pgi_setting_key']][$rate['rate_type']] = $rate['rate'];
        }
        return $res;
    }

// START

    //}}}

    //{{{findMeeting
    private function findMeeting($meetingKey)
    {
        $this->logger2->info("-------findMeeting--------" . $meetingKey);

        $objMeeting = new DBI_Meeting($this->get_dsn());
        $where      = "meeting_ticket='".mysql_real_escape_string($meetingKey)."'";

        return $objMeeting->getRow($where);
    }

    //}}}
    function action_get_sip_info($uid) {
        try {
            $dsn = $this->get_dsn();
            $sipTransferUrl = $this->configProxy->get("sipTransferUrl");
            if (!$sipTransferUrl) {
                $sipTransferUrl = N2MY_BASE_URL."/viewer_tool/ives/sip_account.php";
            }
            $formData = array(
                "type"      => "get_sip_info",
                "uid"  => $uid,
            );
            $option = array(
                CURLOPT_URL             => $sipTransferUrl,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $formData,
                CURLOPT_RETURNTRANSFER  => 1,
                CURLOPT_CONNECTTIMEOUT  => 650,
                CURLOPT_TIMEOUT         => 650
            );
            $ch = curl_init();
            curl_setopt_array($ch, $option);
            $result = curl_exec($ch);
            curl_close($ch);
            $this->logger2->info($result);
            return unserialize($result);
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
            return false;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            return false;
        }
    }

    function action_service_login() {
        $id = $this->request->get("id");
        $type = $this->request->get("type");
        if (!$id || !$type) {
           print "parameter error!";
           exit;
        }
        switch ($type) {
        case "user":
            require_once("classes/dbi/user.dbi.php");
            $obj_User = new UserTable($this->get_dsn());
            $where = "user_id = '".mysql_real_escape_string($id)."'";
            $user_info = $obj_User->getRow($where, "user_id,user_password,user_admin_password");
            if ($user_info) {
                require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
                $user_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_password"]);
                $location = N2MY_BASE_URL."../services/login/index.php?action_showlogin&user_id=".$user_info["user_id"]."&user_password=".$user_password."&country=jp&lang=ja&time_zone=9";
                header("Location: ".$location);
            } else {
                print "user_id error";
            }
        case "member":
            require_once("classes/dbi/member.dbi.php");
            $obj_Member = new MemberTable($this->get_dsn());
            $where = "member_id = '".mysql_real_escape_string($id)."'";
            $member_info = $obj_Member->getRow($where, "member_id,member_pass");
            if ($member_info) {
                require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
                $member_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_info["member_pass"]);
                $location = N2MY_BASE_URL."../services/login/index.php?action_showlogin&user_id=".urlencode($member_info["member_id"])."&user_password=".$member_password."&country=jp&lang=ja&time_zone=9";
                header("Location: ".$location);
            } else {
                print "member_id error";
            }
            break;
        }
        exit;
    }

    function _getMeetingVersion() {
        static $version;
        if ($version) {
            return $version;
        }
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }

    // ビデオ会議詳細
    public function action_video_conference_detail() {
        require_once('classes/dbi/video_conference.dbi.php');
        require_once('classes/dbi/video_conference_participant.dbi.php');
        require_once('classes/core/dbi/Participant.dbi.php');
        $vc_table = new VideoConferenceTable($this->get_dsn());
        $vc_participant_table = new VideoConferenceParticipantTable($this->get_dsn());
        $participant_table = new DBI_Participant($this->get_dsn());

        $vc_primary_key = 'video_conference_key';
        $vc_participant_primary_key = 'video_participant_key';
        $participant_ref_key = 'participant_key';

        // パラメータ取得
        $vc_key = $this->request->get($vc_primary_key, '-1');
        $sort_order = $this->request->get('sort_type', 'asc');
        $sort_key = $this->request->get('sort_key', $vc_participant_primary_key);
        // カラム名取得
        $vc_columns = $vc_table->getTableInfo('data');
        $vc_participant_columns = $vc_participant_table->getTableInfo('data');
        // video conference情報取得
        $vc_rows = $vc_table->getRowsAssoc("$vc_primary_key=" . addslashes($vc_key), array("$vc_primary_key" => "desc"), 1);
        // ビデオ会議参加者の情報取得
        $vc_participant_rows = $vc_participant_table->getRowsAssoc("$vc_primary_key=" . addslashes($vc_key), array(addslashes($sort_key) => addslashes($sort_order)));

        $page = array(
            "action"    => __FUNCTION__,
            "sort_key"  => $sort_key,
            "sort_type" => $sort_order,
        );

        // 表示
        $this->template->assign("content", $str);
        $this->template->assign("page", $page);
        $this->template->assign("table", $vc_table->table);
        $this->template->assign("vc_columns", $vc_columns);
        $this->template->assign("vc_primary_key", $vc_primary_key);
        $this->template->assign("vc_row", count($vc_rows) ? $vc_rows[0] : NULL);
        $this->template->assign("vc_participant_columns", $vc_participant_columns);
        $this->template->assign("vc_participant_rows", $vc_participant_rows);

        $template = "viewer_tool/video_conference/detail.t.html";
        $this->_display($template);
    }
}

$main = new AppViewerTool();
$main->execute();
