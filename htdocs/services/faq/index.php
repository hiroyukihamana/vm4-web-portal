<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:Hirohsi                                                      |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//

require_once("classes/AppFrame.class.php");
//require_once('classes/authorization.class.php');
require_once("classes/N2MY_Account.class.php");
require_once("classes/dbi/faq_list.dbi.php");
require_once('config/config.inc.php');

class AppFaq extends AppFrame {

    var $logger = null;
    var $_lang_num = 0;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->user_auth = "invite";
        $this->checkAuth();
    }

    function action_answer(){
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__);
        $obj_FaqTable = new FaqListTable( $this->get_auth_dsn(), $this->get_language() );
        $cat_num = $_REQUEST['cat'];

        $faq_list = array();

        $user_info = $this->session->get( "user_info" );
        if( $ret = $obj_FaqTable->getList( $user_info["account_model"] ) ){
/*
            while($faq = $ret->fetchRow(DB_FETCHMODE_ASSOC)){
                $faq_category = $faq['faq_category'];
                $faq_subcategory = $faq['faq_subcategory'];
                $q_and_a = array($faq['question'],$faq['answer'],$faq['faq_key']);
                if(!isset($faq_list[$faq_category][$faq_subcategory])){
                    $faq_list[$faq_category][$faq_subcategory] = array();
                    array_push($faq_list[$faq_category][$faq_subcategory], $q_and_a);
                }else{
                    array_push($faq_list[$faq_category][$faq_subcategory], $q_and_a);
                }
            }
*/

            // faqデータをcsvから所得
            require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            // questionをを取得
            $csv_question = new EZCsv(true, "UTF-8", "UTF-8", "\t", true);
            $lang = $this->get_language();
            $file =  N2MY_APP_DIR."config/lang/".$lang."/faq_question.csv";
            $csv_question->open($file, "r");
            // answerを取得
            $csv_ans = new EZCsv(true, "UTF-8", "UTF-8", "\t", true);
            $file =  N2MY_APP_DIR."config/lang/".$lang."/faq_answer.csv";
            $csv_ans->open($file, "r");

            // answerを配列にする
            $ans_list = array();
            while($faq_ans = $csv_ans->getNext(true)){
                $ans_list[$faq_ans['faq_key']] = $faq_ans;
            }
            // faqリスト作成
            while($faq = $csv_question->getNext(true) ){
                $faq_category = $faq['faq_category'];
                $faq_subcategory = $faq['faq_subcategory'];
                $q_and_a = array($faq['faq_question'],$ans_list[$faq['faq_key']]['faq_answer'],$faq['faq_key']);
                if(!isset($faq_list[$faq_category][$faq_subcategory])){
                    $faq_list[$faq_category][$faq_subcategory] = array();
                    array_push($faq_list[$faq_category][$faq_subcategory], $q_and_a);
                }else{
                    array_push($faq_list[$faq_category][$faq_subcategory], $q_and_a);
                }
            }


            $this->template->assign('cat_num', $cat_num);
            $this->template->assign('general', isset($faq_list[$cat_num][1]) ? $faq_list[$cat_num][1] : array());
            $this->template->assign('meeting', isset($faq_list[$cat_num][2]) ? $faq_list[$cat_num][2] : array());
            $this->template->assign('seminar', isset($faq_list[$cat_num][3]) ? $faq_list[$cat_num][3] : array());

            $this->display('user/faq/answer.t.html');
        }
        else{
            header("Location: index.html");
            exit();
        }

        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);

    }
    function default_view() {
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__);
        $obj_FaqTable = new FaqListTable( $this->get_auth_dsn(), $this->get_language() );
        $faq_list = array();

        $user_info = $this->session->get( "user_info" );
        if($ret = $obj_FaqTable->getList( $user_info["account_model"] ) ){
/*
            while($faq = $ret->fetchRow(DB_FETCHMODE_ASSOC)){
                $faq_category = $faq['faq_category'];
                $faq_subcategory = $faq['faq_subcategory'];
                if (trim($faq['question'])) {
                    $q_and_key = array($faq['question'],$faq['faq_key'],$faq['faq_category']);
                    if(!isset($faq_list[$faq_category][$faq_subcategory])){
                        $faq_list[$faq_category][$faq_subcategory] = array();
                        array_push($faq_list[$faq_category][$faq_subcategory], $q_and_key);
                    } else {
                        array_push($faq_list[$faq_category][$faq_subcategory], $q_and_key);
                    }
                }
            }
*/
            // faqデータをcsvから所得
            require_once("lib/EZLib/EZUtil/EZCsv.class.php");
            $csv = new EZCsv(true, "UTF-8", "UTF-8", "\t", true);
            $lang = $this->get_language();
            $file =  N2MY_APP_DIR."config/lang/".$lang."/faq_question.csv";
            $csv->open($file, "r");
            $header = $csv->getHeader();
            while($faq = $csv->getNext(true)){
                $faq_category = $faq['faq_category'];
                $faq_subcategory = $faq['faq_subcategory'];
                if (trim($faq['faq_question'])) {
                    $q_and_key = array($faq['faq_question'],$faq['faq_key'],$faq['faq_category']);
                        if(!isset($faq_list[$faq_category][$faq_subcategory])){
                        $faq_list[$faq_category][$faq_subcategory] = array();
                        array_push($faq_list[$faq_category][$faq_subcategory], $q_and_key);
                    } else {
                        array_push($faq_list[$faq_category][$faq_subcategory], $q_and_key);
                    }
                }
            }

            $this->template->assign('service_general',  isset($faq_list[1][1]) ? $faq_list[1][1] : array());
            $this->template->assign('service_meeting',  isset($faq_list[1][2]) ? $faq_list[1][2] : array());
            $this->template->assign('service_seminar',  isset($faq_list[1][3]) ? $faq_list[1][3] : array());
            $this->template->assign('price_general',    isset($faq_list[2][1]) ? $faq_list[2][1] : array());
            $this->template->assign('price_meeting',    isset($faq_list[2][2]) ? $faq_list[2][2] : array());
            $this->template->assign('price_seminar',    isset($faq_list[2][3]) ? $faq_list[2][3] : array());
            $this->template->assign('env_general',      isset($faq_list[3][1]) ? $faq_list[3][1] : array());
            $this->template->assign('env_meeting',      isset($faq_list[3][2]) ? $faq_list[3][2] : array());
            $this->template->assign('env_seminar',      isset($faq_list[3][3]) ? $faq_list[3][3] : array());
            $this->template->assign('trouble_general',  isset($faq_list[4][1]) ? $faq_list[4][1] : array());
            $this->template->assign('trouble_meeting',  isset($faq_list[4][2]) ? $faq_list[4][2] : array());
            $this->template->assign('trouble_seminar',  isset($faq_list[4][3]) ? $faq_list[4][3] : array());
            $this->template->assign('mac_general',      isset($faq_list[5][1]) ? $faq_list[5][1] : array());
            $this->template->assign('mac_meeting',      isset($faq_list[5][2]) ? $faq_list[5][2] : array());
            $this->template->assign('mac_seminar',      isset($faq_list[5][3]) ? $faq_list[5][3] : array());

            $this->display('user/faq/index.t.html');
        }
        else{
            header("Location: index.html");
            exit();
        }


        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);
    }
}

$main =& new AppFaq();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
