<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once ('classes/AppFrame.class.php');

class AppLogout extends AppFrame {

    function default_view() {
        setcookie(session_name(), '', time() - 1800, '/');
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        if ($user_info || $member_info) {
            $this->logger2->debug(array ($user_info, $member_info));
            // 操作ログ
            $this->add_operation_log("logout");
        } else {
            $this->logger2->debug(array ("session error", $user_info, $member_info));
        }
        // ログアウト画面表示
        $this->display('user/yoyaku/yoyaku_logout.t.html');
        $this->session->clear();
    }
}

$main = new AppLogout();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
