<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/AppFrame.class.php");
require_once("classes/core/dbi/MeetingSequence.dbi.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/N2MY_Reservation.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/reservation.dbi.php");
require_once("classes/vcubeid/vcubeIdCore.class.php");

class AppInviteMenu extends AppFrame
{
    private $obj_Auth = null;
    var $obj_vcubeId = null;
    var $serverName = null;
    var $consumerKey = null;
    var $wsseId = null;
    var $wssePw = null;
    var $domain = null;
    var $isSSL = null;
    var $isSha1 = null;
    function init() {
        $this->_name_space = md5(__FILE__);
        $serverName = $_SERVER["SERVER_NAME"];
        if ($serverName == $this->config->get('VCUBE_ONE','meeting_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
        } else if($serverName == $this->config->get('VCUBE_ONE','document_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_document_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_doc_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_doc_wssePw');
        } else if($serverName == $this->config->get('VCUBE_ONE','sales_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_sales_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_sls_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_sls_wssePw');
        }else{
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
        }
        $this->domain = $this->config->get('VCUBE_ONE','vcubeid_domain');
        $this->isSSL = $this->config->get('VCUBE_ONE','vcube_isSSL') ? $this->config->get('VCUBE_ONE','isSSL') : false;
        $this->isSha1 = $this->config->get('VCUBE_ONE','vcube_isSha1') ? $this->config->get('VCUBE_ONE','isSha1') : false;
        $this->obj_vcubeId = new vcubeIdCore($this->domain, $this->wsseId, $this->wssePw, $this->isSSL, $this->isSha1);
    }

    function action_invite() {
        $this->logger->debug(__FUNCTION__."#start", __FILE__, __LINE__);
        if( 1 == $this->request->get( "action_invite" ) ){
            $this->session->removeAll();
            $r_user_session = $this->request->get( "r_user_session" );
            $this->session->set("r_user_session", $r_user_session);
        }
        $this->logger->debug(__FUNCTION__."#end", __FILE__, __LINE__);
        return $this->action_showTop();
    }

    function action_guest() {
        $this->session->removeAll();
        if ($reservation_id = $this->request->get( "reservation_id" )) {
            require_once( "lib/EZLib/EZUtil/EZEncrypt.class.php");
            $reservation_id = str_replace("-", "/", $reservation_id);
            $reservation_id = str_replace(' ', '+', $reservation_id);
            $reservation_id = str_replace('_', '+', $reservation_id);
            $reservation_id = EZEncrypt::decrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $reservation_id);
            $this->session->set("reservation_id", $reservation_id);
        }
        return $this->render_guest();
    }

    function action_invitedGuest() {
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $submit_key = md5(microtime());
        $knocker_build = md5("knockerbuild_submit_key");
        $this->session->set("knockerbuild_submit_key",$submit_key,$knocker_build);
        $this->template->assign('action', 'action_invitedGuest');
        $request = $this->request->getAll();
        $this->logger->debug(__FUNCTION__."#start", __FILE__, __LINE__, $request);
        //リダイレクト判定のため現在URLを取得
        $current_url = $_SERVER["REQUEST_URI"];
        if ($this->request->get("pin_login") || 1 == $this->request->get("action_invitedGuest")) {
            $this->session->removeAll();
            $meeting_session_id = $this->request->get("meeting_session_id");
            // タイムゾーンのセキュリティ強化
            $time_zone     = $this->request->get("time_zone", $this->request->getCookie("time_zone") ? $this->request->getCookie("time_zone") : "");
            $timezone_list = $this->get_timezone_list();
            if ($time_zone && !array_key_exists($time_zone, $timezone_list)) {
                $timezone_keys = array_keys($timezone_list);
                $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
            }
            $this->session->set("time_zone", $time_zone);
            setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $lang  = $this->request->get("lang", $this->request->getCookie("lang") ? $this->request->getCookie("lang") : "");
            $this->session->set('lang', $lang);
            setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            if($this->request->get('pin_login')){
                $this->session->set('pin_login', 1);
            }
            if (!$server_info = $obj_MGMClass->getRelationDsn($meeting_session_id, "meeting")) {
                $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $meeting_session_id);
                $message["body"]  = $this->get_message("INVITATION", "finish_error_body");
                $message["title"] = $this->get_message("INVITATION", "finish_error_body");
                $this->template->assign('message', $message);
                return $this->display('common.t.html');
            }
            $meeting_invitation_id = $request["meeting_invitation_id"];
            $this->session->set("server_info", $server_info);
            $dsn = $server_info["dsn"];
            $this->session->set("invitedGuest", 1);
            // 言語はブラウザの設定を使用
            $this->session->set("meeting_invitation_id", $meeting_invitation_id);
            $this->session->set("meeting_session_id", $meeting_session_id);
        } else {
            $server_info           = $this->session->get("server_info");
            $dsn                   = $server_info["dsn"];
            $meeting_invitation_id = $this->session->get("meeting_invitation_id");
            $meeting_session_id    = $this->session->get("meeting_session_id");
        }
        $objMeeting = new MeetingTable($dsn);
        $meetingInfo = $objMeeting->getRow(sprintf("meeting_session_id='%s'", $meeting_session_id));
        require_once "classes/dbi/meeting_invitation.dbi.php";
        $objMeetingInvitationTable = new MeetingInvitationTable($dsn);
        $invitationInfo = $objMeetingInvitationTable->getRow(
                                                            sprintf("meeting_session_id='%s' AND status = 1", $meeting_session_id),
                                                            // sprintf("user_session_id='%s' AND status = 1", $meeting_invitation_id),
                                                            "*",
                                                            array("invitation_key"=>"desc"));
        $type = "";
        if ($meeting_invitation_id == $invitationInfo["user_session_id"]) {
            $type = "user";
        } elseif ($meeting_invitation_id == $invitationInfo["authority_session_id"]) {
        	$type = "user";
            $this->session->set("authority_flg", 1);
        } elseif ($meeting_invitation_id == $invitationInfo["audience_session_id"]) {
            $type = "audience";
        } elseif($this->session->get('pin_login')){
            $type = "user";
        }
        $this->session->set("contact_name", $invitationInfo["contact_name"]);
        //予約会議だった場合の処理
        if ($meetingInfo["is_reserved"] && $invitationInfo["meeting_ticket"]) {
            $reservation = new ReservationTable($this->get_dsn());
            $reservation_info = $reservation->getRow(sprintf("meeting_key='%s'", $invitationInfo["meeting_ticket"]));

            $login_start = strtotime($reservation_info['reservation_starttime']);
            $login_end = strtotime($reservation_info['reservation_endtime']);
            $obj_Participant = new DBI_Participant($this->get_dsn());
            // 電話を含めない人数を取得
            $pcount  = $obj_Participant->getCount( $meetingInfo['meeting_key'] );
            $now_time = time();
            // 開催期間中、又は参加者が０人以上
            if (1 == $reservation_info["reservation_status"] &&
                $login_start < $now_time && ($now_time < $login_end || $pcount > 0)) {
            } else {
                // 現地時間
                $time_zone = $reservation_info["reservation_place"];
                $obj_N2MY_Reservation = new N2MY_Reservation($this->get_dsn());
                $reservation_info['reservation_starttime'] = $obj_N2MY_Reservation->convTimeZone($reservation_info['reservation_starttime'], $time_zone);
                $reservation_info['reservation_endtime'] = $obj_N2MY_Reservation->convTimeZone($reservation_info['reservation_endtime'], $time_zone);
                $local_datetime = EZDate::getLocateTime(time(), $time_zone, N2MY_SERVER_TIMEZONE);
                $this->template->assign("now_date", $local_datetime);
                if($local_datetime < $reservation_info['reservation_starttime']){
                    $this->template->assign("message", $this->get_message("DEFINE", "RESERVATION_ERROR_NOTSTARTED"));
                }else{
                    $this->template->assign("message", $this->get_message("DEFINE", "RESERVATION_ERROR_FINISHED"));
                }
                $this->template->assign("start", $reservation_info["reservation_starttime"]);
                $this->template->assign("end", $reservation_info["reservation_endtime"]);
                $this->template->assign("reservation_place", $time_zone);
                $this->template->assign("reservation_name", $reservation_info["reservation_name"]);
                $this->template->assign("organizer_name", $reservation_info["organizer_name"]);
                return $this->display('user/reservation/error.t.html');
            }
        }
        $obj_N2MY_Account = new N2MY_Account($dsn);
        if (false === $obj_N2MY_Account->checkRemoteAddress($meetingInfo["user_key"], $_SERVER["REMOTE_ADDR"])){
            $this->session->removeAll();
            return $this->render_valid();
        }
        // 部屋情報取得
        $room_info = $obj_N2MY_Account->getRoomInfo($meetingInfo["room_key"]);
        if ($room_info["room_info"]["use_sales_option"]) {
            $type = "customer";
            $customer_flg = 1;
            $this->session->set("customer_flg", 1);
            $this->template->assign('customer_flg', $customer_flg);
            $this->session->set("service_mode","sales");
        }
        if (($meetingInfo["is_active"] && !$meetingInfo["is_deleted"]) && $type) {
            $this->logger->trace(__FUNCTION__."#meeting_info", __FILE__, __LINE__, $room_info);
            $this->session->set("room_info", array($meetingInfo["room_key"] => $room_info));
            $room_info["room_info"]["meeting_key"] = $meetingInfo["meeting_ticket"];
            $room_info["room_info"]["participant_num"] = 0;
            $room_info["room_info"]["audience_num"] = 0;
            $room_info["room_info"]["whiteboard_num"] = 0;
            if($room_info["options"]["desktop_share"]) {
                $isContractShare = 1;
            }
            $this->session->set('guest', 1);    // ゲスト
            $this->session->set('login', 1);    // ログイン
            // ユーザー情報
            $objUser = new UserTable($dsn);
            $user_where = " user_key = '".addslashes($meetingInfo["user_key"])."'" .
                    " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                    " AND user_delete_status < 2";
            $user_info = $objUser->getRow($user_where);
            $this->session->set('user_info', $user_info);

            // 所在地・タイムゾーン
            $_country_key = $this->solveLocaleInfo();
            $this->session->set("login_type", 'invitedGuest');
            /* ブラウザ以外のデバイス */
            // 資料共有ユーザー
            if (strpos($_SERVER['HTTP_USER_AGENT'], "iPad;")) {
                // モバイルではオーディエンスモードでの入室を禁止する
                if( $type == "audience"){
                    $message = array(
                            "title" => $this->get_message("MEETING_START", "mobile_audience_title"),
                            "text" => $this->get_message("MEETING_START", "mobile_audience_text"),
                            "back_url" => "javascript:window.close();",
                            "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                    );
                    $this->template->assign("message", $message);
                    $this->display('user/message.t.html');
                    exit;
                } elseif ($this->session->get("authority_flg")) {
                    $message = array(
                        "title" => $this->get_message("MEETING_START", "mobile_authority_title"),
                        "text" => $this->get_message("MEETING_START", "mobile_authority_text"),
                        "back_url" => "javascript:window.close();",
                        "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                    );
                    $this->template->assign("message", $message);
                    $this->display('user/message.t.html');
                    exit;
                }
                $this->session->set('invited_meeting_audience', ($type == "audience") ? 1 : 0);
                $this->session->set('invited_meeting_ticket', $meetingInfo['meeting_ticket']);
                $this->logger2->info($meetingInfo['meeting_session_id']);
                if ($room_info["room_info"]["use_sales_option"]){
                    $name = $invitationInfo["contact_name"] ? $invitationInfo["contact_name"] : "";
                    $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&name=".$name."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                } else {
                    $url    = "vcube-meeting-docshare://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                }
                $this->logger2->info($url);
                header("Location: ".$url);
                exit();
            }
            // モバイル判別
            if (strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod")) {
                // モバイルではオーディエンスモードでの入室を禁止する
                if( $type == "audience"){
                    $message = array(
                            "title" => $this->get_message("MEETING_START", "mobile_audience_title"),
                            "text" => $this->get_message("MEETING_START", "mobile_audience_text"),
                            "back_url" => "javascript:window.close();",
                            "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                    );
                    $this->template->assign("message", $message);
                    $this->display('user/message.t.html');
                    exit;
                } elseif ($this->session->get("authority_flg")) {
                    $message = array(
                        "title" => $this->get_message("MEETING_START", "mobile_authority_title"),
                        "text" => $this->get_message("MEETING_START", "mobile_authority_text"),
                        "back_url" => "javascript:window.close();",
                        "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                    );
                    $this->template->assign("message", $message);
                    $this->display('user/message.t.html');
                    exit;
                }
                $this->session->set('invited_meeting_ticket', $meetingInfo['meeting_ticket']);
                if ($room_info["room_info"]["use_sales_option"]){
                    $name = $invitationInfo["contact_name"] ? $invitationInfo["contact_name"] : "";
                    $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&name=".$name."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                } else {
                    $url    = $this->config->get("N2MY","mobile_protcol", 'vcube-meeting')."://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                }
                $this->logger2->info($url);
                header("Location: ".$url);
                exit();
            }
            $room_info["room_info"]["audience"] = ($type == "audience") ? 1 : 0;
            if ($meetingInfo["intra_fms"]) {
                require_once("classes/dbi/user_fms_server.dbi.php");
                $obj_UserFmsServer = new UserFmsServerTable($this->get_dsn());
                $serverInfo = $obj_UserFmsServer->getRow(sprintf("user_key=%d", $meetingInfo["user_key"]));
                $server = $serverInfo["server_address"];
            } else {
                $server = $this->config->get("N2MY","fms_server");
            }
            //eco
            require_once( "classes/N2MY_Eco.class.php" );
            $thisMonth = date( "Y-m" );
            $lastYear = date( "Y-m", strtotime( "last year +1 month" ) );
            $objEco = new N2MY_Eco( $this->get_dsn(), N2MY_MDB_DSN );
            $ecoInfo = $objEco->getYearlyLog( $user_info["user_id"], $thisMonth, $lastYear );
            $ecoInfo["thisMonth"] = $thisMonth;
            $ecoInfo["lastYear"] = $lastYear;
            $this->template->assign( 'ecoInfo', $ecoInfo );

            $lang = $this->get_language();
            $this->_set_template_dir($lang);
            $vcube_login = $this->session->get("vcubeid_login");
            $member_login = $this->session->get("member_login");
            if ($user_info["account_model"] == "free" && $user_info["user_status"] == "3" && !$vcube_login) {
                 $assign_info = array(
                        "room_info" => $room_info,
                        "c" => $_country_key,
                        "sess_id" => "",
                        "isContractShare" => $isContractShare,
                        "room_str" => $meetingInfo["room_key"],
                        "action" => __FUNCTION__,
                        "server" => $server,
                        "user_clip" => $user_info['use_clip_share'],
                 );
                 $this->session->set("assign_info", $assign_info);
                 $this->action_vcubeid_login();
            } else if ($user_info["account_model"] == "member" && $user_info["user_status"] == "1" && !$member_login  && !$user_info["external_user_invitation_flg"]){
                // メンバー課金のログイン処理
                  $assign_info = array(
                        "room_info" => $room_info,
                        "c" => $_country_key,
                        "sess_id" => "",
                        "isContractShare" => $isContractShare,
                        "room_str" => $reservation_info["room_key"],
                        "action" => __FUNCTION__,
                        "server" => $server,
                        "user_clip" => $user_info['use_clip_share'],
                 );
                 $this->session->set("assign_info", $assign_info);
                 $this->action_memberCharges_login();
            }  else {
                $this->template->assign( 'action', __FUNCTION__ );
                if ($room_info["room_info"]["use_sales_option"]) {
                    $displaySize = "16to9";
                } else {
                    $displaySize = $_COOKIE["displaySize"];
                }
                $this->template->assign( 'displaySize', $_COOKIE["displaySize"] );
                $this->template->assign('room', $room_info);
                $this->template->assign('server', $server);
                $this->template->assign('c', $_country_key);
                $this->template->assign( 'displaySize', $_COOKIE["displaySize"] );
                $this->template->assign('customer_flg', $customer_flg);
                // 部屋一覧
                $this->template->assign('isContractShare', $isContractShare);
                //
                $this->template->assign('room_str', $meetingInfo["room_key"]);
                $this->template->assign("hash", md5($_SERVER["HTTP_HOST"]."/".$reservation_info["room_key"]));
                $this->template->assign('server', $this->config->get("N2MY","fms_server"));
                $this->template->assign('serverDsnKey', $this->get_dsn_key() );
                $this->template->assign('use_clip_share', $user_info['use_clip_share']);
                $this->template->assign("knockerbuild_submit_key",$submit_key);

                if($user_info["use_port_plan"] == "1" && $user_info["entry_mode"] == "1"){
                    if($meetingInfo["is_one_time_meeting"]){
                        if($meetingInfo["is_reserved"] && $invitationInfo["meeting_ticket"]){
                            if($pcount >=  $reservation_info["max_port"]){
                                $message["body"] = $this->get_message("MEETING_START", "max_participant_error_body");
                                $this->template->assign('message', $message);
                                return $this->display('common.t.html');
                            }else{
                                // URL チェック
                                if( strpos($current_url,"invite.php") === false ) {
                                    $url = N2MY_BASE_URL."services/invite.php";
                                    header("Location: ".$url);
                                    exit();
                                } else {
                                    return $this->display("user/invite.t.html");
                                }
                            }
                        }else{
                            $message["body"] = $this->get_message("MEETING_START", "max_portplan_error");
                            $this->template->assign('message', $message);
                            return $this->display('common.t.html');
                        }
                    }else{
                        if($meetingInfo["is_reserved"] && $invitationInfo["meeting_ticket"]){
                            if($pcount >=  $reservation_info["max_port"]){
                                $message["body"] = $this->get_message("MEETING_START", "max_participant_error_body");
                                $this->template->assign('message', $message);
                                return $this->display('common.t.html');
                            }else{
                                // URL チェック
                                if( strpos($current_url,"invite.php") === false ) {
                                    $url = N2MY_BASE_URL."services/invite.php";
                                    header("Location: ".$url);
                                    exit();
                                } else {
                                    return $this->display("user/invite.t.html");
                                }
                            }
                        }else{
                            $message["body"] = $this->get_message("MEETING_START", "max_portplan_error");
                            $this->template->assign('message', $message);
                            return $this->display('common.t.html');
                        }
                    }
                }else{
                    // URL チェック
                    if( strpos($current_url,"invite.php") === false ) {
                        $url = N2MY_BASE_URL."services/invite.php";
                        header("Location: ".$url);
                        exit();
                    } else {
                        return $this->display("user/invite.t.html");
                    }
                }
            }
        } else {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $meetingInfo);
            $message["title"] = $this->get_message("INVITATION", "finish_error_body");
            $message["body"] = $this->get_message("INVITATION", "finish_error_body");
            $this->template->assign('message', $message);
            return $this->display('common.t.html');
        }
    }

    public function action_showTop()
    {
        if ($this->request->get("lang")) {
            $_COOKIE["lang"] = $this->request->get("lang");
            $this->session->set("lang", $_COOKIE["lang"]);
        }
        $lang = $this->get_language();
        $this->_set_template_dir($lang);

        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $r_user_session = $this->session->get("r_user_session");

        $obj_N2MY_Reservation = new N2MY_Reservation( $this->get_dsn() );
        $reservation_info = $obj_N2MY_Reservation->checkReservationInfo( $r_user_session );
        if ($reservation_info["r_user_place"] && $reservation_info["r_user_place"] == "100") {
            $this->session->set("time_zone", $reservation_info["reservation_place"]);
        } else {
            $this->session->set("time_zone", $reservation_info["r_user_place"]);
        }

        $objMeeting = new MeetingTable($this->get_dsn());
        $meetingInfo = $objMeeting->getRow(sprintf("meeting_ticket='%s'", $reservation_info["meeting_key"]));
        // ユーザー情報
        $objUser = new UserTable($this->get_dsn());
        $user_where = " user_key = '".addslashes($meetingInfo["user_key"])."'" .
                " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                " AND user_delete_status < 2";
        $user_info = $objUser->getRow($user_where);
        if (!$user_info) {
            $message = array(
                "title" => $this->get_message("MEETING_START", "cancel_title"),
                "text" => $this->get_message("MEETING_START", "cancel_text"),
                "back_url" => "javascript:window.close();",
                "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                );
            $this->template->assign("message", $message);
            return $this->display('user/message.t.html');
        }
        $this->session->set('user_info', $user_info);

        if ($meetingInfo["intra_fms"]) {
            $this->checkFms($meetingInfo["user_key"]);
        } else {
            if (!$server_info = $obj_MGMClass->getRelationDsn($r_user_session, "invite")){
                $message .= "<li>".$this->get_message("DEFINE", "LOGIN_ERROR_INVALIDUSER") . "</li>";
                return $this->display('user/reservation/error.t.html');
            } else {
                $this->session->set("server_info", $server_info );
                $this->render_invite();
            }
        }
    }

    function action_vcubeid_login ($message = "") {
        $this->template->assign("message", $message);
        return $this->display('user/reservation/vcubeid_login.t.html');
    }

    function action_render_pin_login ($message = ""){
        $this->template->assign('message', $message);
        $time_zone = $this->request->get("time_zone", $this->request->getCookie("time_zone") ? $this->request->getCookie("time_zone") : "");
        $lang = $this->request->get("lang", $this->request->getCookie("lang") ? $this->request->getCookie("lang") : "");
        $country = $this->request->get("country", $this->request->getCookie("country"));
        $remote_addr = $_SERVER["REMOTE_ADDR"];
        $selected_country_key = $this->request->getCookie("selected_country_key");
        $cookie_remote_addr = $this->request->getCookie("remote_addr");
        if ($cookie_remote_addr != $remote_addr) {
            $selected_country_key = "auto";
        }
        $this->template->assign("country", $country);
        $this->template->assign("selected_country_key", $selected_country_key);
        $this->template->assign('lang', $lang);
        $this->template->assign('time_zone',$time_zone);
        $template = 'user/login/pin_login.t.html';
        return $this->display($template);
    }

    /**
     * PCでも暗証番号を入力して会議に入室する
     */
    function action_check_pin_login(){
        if (!defined("N2MY_SERVER_TIMEZONE")) {
            date_default_timezone_set($this->config->get('GLOBAL', 'time_zone_name', date_default_timezone_get()));
            $_tz = (int)(substr( date( 'O' ), 0, 3));
            // サーバータイムゾーン設定
            define("N2MY_SERVER_TIMEZONE", $_tz);
        }
        $kind   = $this->request->get('kind');
        $pin_cd = $this->request->get('pin_cd');
        $lang = $this->request->get('lang');
        $time_zone = $this->request->get('time_zone');
        $_country_key = $this->request->get('country');
        if(!$pin_cd || !is_numeric($pin_cd)){
            $message .= "<li>".PIN_LOGIN_ERROR_INVALID_PIN_CORD . "</li>";
            if($kind == "mobile"){
                return $this->action_render_pin_login_mobile($message);
            }else{
                return $this->action_render_pin_login($message);
            }
        }
        $obj_MGMClass = new MGM_AuthClass($this->get_auth_dsn());
        $obj_Server   = new N2MY_DB($this->get_auth_dsn(), 'db_server');
        $serverInfo = $obj_Server->getRow('server_status = 1');
        // 会議情報
        $objMeeting = new MeetingTable($serverInfo["dsn"]);
        $where = "pin_cd = '" . mysql_real_escape_string($pin_cd) . "'";
        $meetingInfo = $objMeeting->getRow($where);
        if (!$meetingInfo) {
            $message .= "<li>".PIN_LOGIN_ERROR_INVALID_PIN_CORD . "</li>";
            if($kind == "mobile"){
                return $this->action_render_pin_login_mobile($message);
            }else{
                return $this->action_render_pin_login($message);
            }
        }
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
        }
        $language_list = $this->get_language_list();
        if (!array_key_exists($lang, $language_list)) {
            $lang = 'en';
        }
        // 招待情報 会議延長対応
        if ($meetingInfo["is_reserved"]){
            $objReservation = new ReservationTable($this->get_dsn());
            $reservationInfo = $objReservation->getRow(sprintf("meeting_key='%s'", mysql_real_escape_string($meetingInfo["meeting_ticket"])));
            $login_start = strtotime($reservationInfo['reservation_starttime']);
            $login_end = strtotime($reservationInfo['reservation_endtime']);
            $obj_Participant = new DBI_Participant($this->get_dsn());
            $pcount = $obj_Participant->numRows("meeting_key = ".$meetingInfo['meeting_key']." AND is_active = 1");
            $now_time = time();
            // 開催期間中、又は参加者が０人以上
            if (1 == $reservationInfo["reservation_status"] &&
                    $login_start < $now_time && ($now_time < $login_end || $pcount > 0)) {
            }else{
                $this->template->assign("message", $this->get_message("DEFINE", "RESERVATION_ERROR_NOTSTARTED"));
                $local_datetime = EZDate::getLocateTime(time(), $time_zone, N2MY_SERVER_TIMEZONE);
                $this->template->assign("now_date", $local_datetime);
                $obj_N2MY_Reservation = new N2MY_Reservation($this->get_dsn());
                $reservationInfo['reservation_starttime'] = $obj_N2MY_Reservation->convTimeZone($reservationInfo['reservation_starttime'], $time_zone);
                $reservationInfo['reservation_endtime'] = $obj_N2MY_Reservation->convTimeZone($reservationInfo['reservation_endtime'], $time_zone);
                $this->template->assign("start", $reservationInfo["reservation_starttime"]);
                $this->template->assign("end", $reservationInfo["reservation_endtime"]);
                $this->template->assign("reservation_place", $time_zone);
                $this->template->assign("reservation_name", $reservationInfo["reservation_name"]);
                $this->template->assign("organizer_name", $reservationInfo["organizer_name"]);
                if($kind == "mobile"){
                    return $this->display('user/reservation/error_mobile.t.html');
                }else{
                    return $this->display('user/reservation/error.t.html');
                }
            }
        }
        $result_country_key = $this->_get_country_key($_country_key);
        if (!$result_country_key) {
            $country_list = $this->get_country_list();
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    $this->session->set('country_key', $country_row["country_key"]);
                    $result_country_key = $_country_key;
                }
            }
        }else {
            $this->session->set('country_key', $result_country_key);
        }
        $this->session->set('selected_country_key', $_country_key);
        $this->session->set("lang", $lang);
        $this->session->set('time_zone', $time_zone);
        $limit = time() + 365 * 24 * 3600;
        setcookie("lang", $lang, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("country", $result_country_key, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("selected_country_key", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("time_zone", $time_zone, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("remote_addr", $_SERVER["REMOTE_ADDR"], N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        header("Location: invite.php?action_invitedGuest&pin_login=1&meeting_session_id=" . $meetingInfo['meeting_session_id']);
    }

    function action_set_preference() {
        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;
        // 言語コード
        $request = $this->request->getAll();
        if ($request['lang']) {
            setcookie("lang", $request['lang'],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $this->session->set("lang", $request['lang']);
        }
        if ($request['time_zone']) {
            setcookie("time_zone", $request['time_zone'],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $this->session->set("time_zone", $request['time_zone']);
        }
        if($request['is_mobile']){
            header("Location: invite.php?action_render_pin_login_mobile");
        }else{
            header("Location: invite.php?action_render_pin_login");
        }
    }
    /**
     * メンバー課金 招待ログイン画面
     * @return boolean
     */
    function action_memberCharges_login($message = ""){
      $this->template->assign("message", $message);
      return $this->display('user/reservation/member_charges_login.t.html');
    }

    /**
     * メンバー課金 会議招待でのログイン処理
     * @return boolean
     */
    function action_check_memberCharges_login(){
      // ログイン処理用クラス読み込み
      require_once ('classes/N2MY_Auth.class.php');
        require_once ('classes/mgm/MGM_Auth.class.php');
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $obj_Auth = new N2MY_Auth( $this->get_dsn() );
        // 入力データ取得
        $member_id      = $this->request->get("user_id");
        $member_pw      = $this->request->get("user_password");
        // ユーザー情報取得
        $user_info = $this->session->get('user_info');

        // ログインチェック
        $login_info = $obj_Auth->checkMember($member_id, $member_pw,$user_info['user_key'], null, null, $user_info['external_member_invitation_flg']);
        if ( !$login_info ) {
          $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
          $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
          $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
          return $this->action_memberCharges_login($message);
        }

        //メンバー情報書き換え 差出人→招待者
        $this->session->set('member_info', $login_info);
        // ログインフラグ
        $this->session->set("member_login", 1);

        $assign_info = $this->session->get("assign_info");
        $this->logger2->debug($assign_info);
        $this->template->assign( 'action', $assign_info["action"] );
        $this->template->assign( 'displaySize', $_COOKIE["displaySize"] );
        $this->template->assign('room', $assign_info["room_info"]);
        $this->template->assign('server', $assign_info["server"] ? $assign_info["server"] : $this->config->get("N2MY","fms_server"));
        $this->template->assign('c', $assign_info["c"]);
        $this->template->assign( 'displaySize', $_COOKIE["displaySize"] );
        $this->template->assign('sess_id' , $this->session->get("r_user_session"));
        // 部屋一覧
        $this->template->assign('isContractShare', $assign_info["isContractShare"]);
        //
        $this->template->assign('room_str', $assign_info["room_str"]);
        $this->template->assign("hash", md5($_SERVER["HTTP_HOST"]."/".$assign_info["room_str"]));
        $this->template->assign('serverDsnKey', $this->get_dsn_key() );
        $this->template->assign('use_clip_share', $assign_info['use_clip_share']);
        $url = N2MY_BASE_URL."services/invite.php";
        header("Location: ".$url);

    }
    /**
     * セールス 招待ログイン画面
     * 4.10.3.0 MTGVFOUR-827 【S&S】Web_マネージャー機能の追加対応
     */
    function action_sales_login($message = ""){
        $this->template->assign("message", $message);
        return $this->display('user/reservation/sales_login.t.html');
    }
    /**
     * セールス 招待ログイン画面
     * 4.10.3.0 MTGVFOUR-827 【S&S】Web_マネージャー機能の追加対応
     */
    function action_check_sales_login(){
        // ログイン処理用クラス読み込み
        require_once ('classes/N2MY_Auth.class.php');
        $obj_Auth = new N2MY_Auth( $this->get_dsn());
        $is_admin = null;
        // 入力データ取得
        $user_id      = $this->request->get("user_id");
        $user_pw      = $this->request->get("user_password");
        $lang         = $this->request->get('lang');
        $time_zone    = $this->request->get('time_zone');
        $_country_key = $this->request->get('country');
        $r_user_key   = $this->session->get("r_user_key");
        //ログインチェック
        $login_info   = $obj_Auth -> check($user_id, $user_pw);
        if(!$login_info["member_info"]){
            require_once ("classes/dbi/member.dbi.php");
            $obj_member  = new MemberTable($this->get_dsn());
            $login_chek = $obj_member->getRow("vcube_one_member_id = '" . mysql_real_escape_string($user_id) . "'");
            if(!$login_chek){
                $this->logger2->info($login_chek,"VCUBEメンバーID認証エラー");
                return $this -> sales_error_message();
            }
            $res = $this -> validateVcubeId($user_id, $user_pw);
            if(!$res){
                $this->logger2->info($res,"VID認証エラー");
                return $this -> sales_error_message();
            }
            
            if(count($res['contracts']) > 1){
                foreach($res["contracts"] as $key => $date){
                    $list = $obj_member->getRow("member_id = '".$date["member_id"]."'");
                    if($date["role"] == 10 || $list["use_ss_watcher"] == 1){
                        if($r_user_key == $list["user_key"]){
                            $login_info["member_info"] = $list;
                            $is_admin = true;
                            break;
                        }
                    }    
                }
            }else{
                $list = $obj_member->getRow("member_id = '".$res["contracts"][0]["member_id"]."'");
                $login_info["member_info"] = $list;
                if($res["contracts"][0]["role"]==10)$is_admin = true;
            }
            if($r_user_key != $login_info["member_info"]["user_key"]){
                $this->logger2->info(array($login_info,$r_user_key),"user_keyaアンマッチ");
                return $this -> sales_error_message();
            }
        }else if($r_user_key != $login_info["user_info"]["user_key"]){
            $this->logger2->info(array($login_info,$r_user_key),"user_keyaアンマッチ");
            return $this -> sales_error_message();
        }        
        //マネージャ以外はエラー
        if($login_info["member_info"]["use_ss_watcher"] || $is_admin){
            $this->session->set("is_manager" , 1);
            $this->session->set("sales_login", 1);
        }else{
            $this->logger2->info(array($login_info,$is_admin),"Not Manager");
            return $this -> sales_error_message();
        }
        $this->session->set('member_info', $login_info["member_info"]);
        $assign_info = $this->session->get("assign_info");
        $this->template->assign('action'         , $assign_info["action"] );
        $this->template->assign('displaySize'    , $_COOKIE["displaySize"] );
        $this->template->assign('room'           , $assign_info["room_info"]);
        $this->template->assign('server'         , $assign_info["server"] ? $assign_info["server"] : $this->config->get("N2MY","fms_server"));
        $this->template->assign('c'              , $assign_info["c"]);
        $this->template->assign('displaySize'    , $_COOKIE["displaySize"] );
        $this->template->assign('sess_id'        , $this->session->get("r_user_session"));
        $this->template->assign('isContractShare', $assign_info["isContractShare"]);
        $this->template->assign('room_str'       , $assign_info["room_str"]);
        $this->template->assign("hash"           , md5($_SERVER["HTTP_HOST"]."/".$assign_info["room_str"]));
        $this->template->assign('serverDsnKey'   , $this->get_dsn_key() );
        $this->template->assign('use_clip_share' , $assign_info['use_clip_share']);
        $url = N2MY_BASE_URL."services/invite.php";
        //所在地&言語&タイムゾーン設定
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
        }
        $language_list = $this->get_language_list();
        if (!array_key_exists($lang, $language_list)) {
            $lang = 'en';
        }
        $result_country_key = $this->_get_country_key($_country_key);
        if (!$result_country_key) {
            $country_list = $this->get_country_list();
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    $this->session->set('country_key', $country_row["country_key"]);
                    $result_country_key = $_country_key;
                }
            }
        }else {
            $this->session->set('country_key', $result_country_key);
        }
        $this->session->set('selected_country_key', $_country_key);
        $this->session->set("lang", $lang);
        $this->session->set('time_zone', $time_zone);
        $this->session->remove('r_user_key');
        setcookie("lang", $lang, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("country", $result_country_key, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("selected_country_key", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("time_zone", $time_zone, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("remote_addr", $_SERVER["REMOTE_ADDR"], N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        header("Location: ".$url);

    }

    /**
     * セールス 招待ログインエラー出力用
     * 4.10.3.0 MTGVFOUR-827 【S&S】Web_マネージャー機能の追加対応
     */
    function sales_error_message(){
        $message .= "<li>".VCUBEID_LOGIN_ERROR_INVALIDUSER . "</li>";
        $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
        $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
        return $this->action_sales_login($message);
    }

    /**
     * セールス 招待ログイン用
     * 4.10.3.0 MTGVFOUR-827 【S&S】Web_マネージャー機能の追加対応
     */
    function getToken($id , $pw){
        $vcubeid = $this->obj_vcubeId;
        $token = $vcubeid->getToken($this->consumerKey, $id, sha1($pw));
        return $token;
    }

    /**
     * セールス 招待ログイン用
     * 4.10.3.0 MTGVFOUR-827 【S&S】Web_マネージャー機能の追加対応
     */    
    function getMemberId($vIdAuthToken , $vcubeId){
        $vcubeid = $this -> obj_vcubeId;
        // ステータス確認
        $response_id = $vcubeid -> ssologin($this -> consumerKey, $vIdAuthToken, $vcubeId);
        $contracts = $response_id['contracts']['contract'];
        if(!array_key_exists(0, $contracts)){
            $tmp = $contracts;
            unset($contracts);
            $contracts[] = $tmp;
        }
        $_contracts = array();
        foreach ($contracts as $contract){
            $xml = (array)simplexml_load_string($contract['settintXml']);
            $xml_role = (array)$xml['role'];
            $_contracts[] = array(
                    'user_id'      => $contract['meeting']['userId'],
                    'member_id'    => $contract['meeting']['memberId'],
                    'company_name' => $contract['name'],
                    'role'         => $xml_role['permission'],
                    'is_portal_header' => ( $contract['one']['header'] === "true" ),
                    'contract_id' => $contract['id'],
            );
        }
        return array(
                'result'    => $response_id['result'],
                'contracts' => $_contracts,
        );
    }

    /**
     * V-cube ID認証
     * セールス 招待ログイン用
     * 4.10.3.0 MTGVFOUR-827 【S&S】Web_マネージャー機能の追加対応
     */
    function validateVcubeId($user_id, $user_pw){
        if(!$user_id || !$user_pw){
            return false;
        }
        $token = $this -> getToken($user_id, $user_pw);
        if(!$token['result']){
            return false;
        }
        $res = $this -> getMemberId($token["vIdAuthToken"] , $user_id);
        if(!$res['result']){
            return false;
        }
        $res['vIdAuthToken'] = $token['vIdAuthToken'];
        return $res;
    }

    function action_check_login () {
        require_once ('classes/N2MY_Auth.class.php');
        require_once ('classes/mgm/MGM_Auth.class.php');

        //リダイレクト判定のため現在URLを取得
        $current_url = $_SERVER["REQUEST_URI"];
        $user_id      = $this->request->get("user_id");
        $user_pw      = $this->request->get("user_password");
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $obj_Auth = new N2MY_Auth( $this->get_dsn() );
        if ($login_info = $obj_Auth->check($user_id, $user_pw)) {
            if ($login_info["member_info"]["member_type"] != 'free') {
                $message .= "<li>".VCUBEID_LOGIN_ERROR_INVALIDUSER . "</li>";
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
                return $this->action_vcubeid_login($message);
            }
            $submit_key = md5(microtime());
            $knocker_build = md5("knockerbuild_submit_key");
            $this->session->set("knockerbuild_submit_key",$submit_key,$knocker_build);
            $this->session->set("vcubeid_login", 1);
            //メンバー情報書き換え 差出人→招待者
            $this->session->set('member_info', $login_info["member_info"]);
            $assign_info = $this->session->get("assign_info");
            $this->logger2->info($assign_info);
            $this->template->assign( 'action', $assign_info["action"] );
            $this->template->assign( 'displaySize', $_COOKIE["displaySize"] );
            $this->template->assign('room', $assign_info["room_info"]);
            $this->template->assign('server', $assign_info["server"] ? $assign_info["server"] : $this->config->get("N2MY","fms_server"));
            $this->template->assign('c', $assign_info["c"]);
            $this->template->assign( 'displaySize', $_COOKIE["displaySize"] );
            // 部屋一覧
            $this->template->assign('isContractShare', $assign_info["isContractShare"]);
            //
            $this->template->assign('room_str', $assign_info["room_str"]);
            $this->template->assign("hash", md5($_SERVER["HTTP_HOST"]."/".$assign_info["room_str"]));
            $this->template->assign('serverDsnKey', $this->get_dsn_key() );
            $this->template->assign('use_clip_share', $assign_info['use_clip_share']);
            $this->template->assign("knockerbuild_submit_key",$submit_key);
            
            // URL チェック
            if( strpos($current_url,"invite.php") === false ) {
                $url = N2MY_BASE_URL."services/invite.php";
                header("Location: ".$url);
                exit();
            } else {
                return $this->display("user/invite.t.html");
            }
        } else {
            $message .= "<li>".VCUBEID_LOGIN_ERROR_INVALIDUSER . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
            return $this->action_vcubeid_login($message);
        }
    }

    /**
     * 会議開始画面を表示
     */
    function action_start_view() {
        $this->render_start_view();
    }

    function default_view() {

        if ($this->session->get("r_user_session")) {
            return $this->render_invite();
        } else if(1 == $this->session->get("invitedGuest")) {
            return $this->action_invitedGuest();
        } else {
            return $this->render_guest();
        }
    }
/*
    function action_change_lang() {
        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;
        // 言語コード
        $lang = $this->request->get("lang");
        $sess = $this->request->get("sessid");
        $c = $this->request->get("c");
        if ($lang) {
            setcookie("lang", $lang, $limit_time, N2MY_COOKIE_PATH);
            $this->session->set("lang", $lang);
        }

        $url    = sprintf("%sr/%s&c=%s", $this->get_redirect_url(), $sess, $c);
        header("Location: ".$url);
        exit();
    }
*/
    /**
     * 招待ユーザのメインメニューを表示
     */
    function render_guest() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        //サーバー取得
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $reservation_id = $this->session->get("reservation_id");
        //リダイレクト判定のため現在URLを取得
        $current_url = $_SERVER["REQUEST_URI"];
        if ( ! $server_info = $obj_MGMClass->getRelationDsn( $reservation_id, "reservation" ) ){
            $message .= "<li>".$this->get_message("DEFINE", "LOGIN_ERROR_INVALIDUSER") . "</li>";
            return $this->display('user/reservation/error.t.html');
        }
        $this->session->set("server_info", $server_info );
        $reservation = new ReservationTable( $this->get_dsn() );
        $lang = $this->get_language();
        $this->_set_template_dir($lang);
        $where = "reservation_session = '".addslashes($reservation_id)."'";
        if ( !$reservation_info = $reservation->getRow( $where )) {
            return $this->display('user/reservation/error.t.html');
        // 予約が削除されたか招待者から除外された
        } elseif ($reservation_info["reservation_status"] == "0") {
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            if (false === $obj_N2MY_Account->checkRemoteAddress($reservation_info["user_key"], $_SERVER["REMOTE_ADDR"])){
                $this->session->removeAll();
                return $this->render_valid();
            }
            $message = array(
                "title" => $this->get_message("MEETING_START", "cancel_title"),
                "text" => $this->get_message("MEETING_START", "cancel_text"),
                "back_url" => "javascript:window.close();",
                "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                );
            $this->template->assign("message", $message);
            return $this->display('user/message.t.html');
        } else {
            $time_zone = $reservation_info["reservation_place"];
            $login_start = strtotime($reservation_info['reservation_starttime']);
            $login_end = strtotime($reservation_info['reservation_endtime']);
            $now_time = time();
            // 現地時間
            $reservation_info['reservation_starttime'] = EZDate::getLocateTime($reservation_info['reservation_starttime'], $time_zone, N2MY_SERVER_TIMEZONE);
            $reservation_info['reservation_endtime'] = EZDate::getLocateTime($reservation_info['reservation_endtime'], $time_zone, N2MY_SERVER_TIMEZONE);

            $objMeeting = new MeetingTable($this->get_dsn());
            $meetingInfo = $objMeeting->getRow(sprintf("meeting_ticket='%s'", $reservation_info["meeting_key"]));
            $obj_Participant = new DBI_Participant($this->get_dsn());
            $pcount = $obj_Participant->numRows("meeting_key = ".$meetingInfo['meeting_key']." AND is_active = 1");
            // 開催日時の確認
            //開催前
            if ( $login_start > $now_time ) {
                $this->template->assign("message", $this->get_message("DEFINE", "RESERVATION_ERROR_NOTSTARTED"));
                $local_datetime = EZDate::getLocateTime(time(), $time_zone, N2MY_SERVER_TIMEZONE);
                $this->template->assign("now_date", $local_datetime);
                $this->template->assign("start", $reservation_info["reservation_starttime"]);
                $this->template->assign("end", $reservation_info["reservation_endtime"]);
                $this->template->assign("reservation_place", $time_zone);
                $this->template->assign("reservation_name", $reservation_info["reservation_name"]);
                $this->template->assign("organizer_name", $reservation_info["organizer_name"]);
                return $this->display('user/reservation/error.t.html');
            // 開催期間中、又は参加者が０人以上
            } elseif ($login_start < $now_time && ($now_time < $login_end || $pcount > 0)) {
                $submit_key = md5(microtime());
                $knocker_build = md5("knockerbuild_submit_key");
                $this->session->set("knockerbuild_submit_key",$submit_key,$knocker_build);
                /* セッション情報取得 */
                $meetingInfo = $objMeeting->getRow(sprintf("meeting_ticket='%s'", $reservation_info["meeting_key"]));
                $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
                $room_info = $obj_N2MY_Account->getRoomInfo( $reservation_info["room_key"] );
                // ユーザー情報
                $objUser = new UserTable($this->get_dsn());
                $user_where = " user_key = '".addslashes($meetingInfo["user_key"])."'" .
                    " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                    " AND user_delete_status < 2";
                $user_info = $objUser->getRow($user_where);
                $this->session->set('user_info', $user_info);
                $this->session->set("room_info", array($reservation_info["room_key"] => $room_info));

                /* template上で見当たらないので削除中*/
                //$room_info["isReserve"] = $room->isReservationRoomStatus( $r[$i]['room_key'] );
                $room_info["room_info"]["meeting_key"] = $reservation_info["meeting_key"];
                $room_info["room_info"]["now_reserve_start"] = $reservation_info["reservation_starttime"];
                $room_info["room_info"]["now_reserve_end"] = $reservation_info["reservation_endtime"];
                $room_info["room_info"]["reservation_place"] = $time_zone;
                $room_info["room_info"]["participant_num"] = 0;
                $room_info["room_info"]["audience_num"] = 0;
                $room_info["room_info"]["whiteboard_num"] = 0;
                if($room_info["options"]["desktop_share"]) {
                    $isContractShare = 1;
                }
                // ゲスト
                $this->session->set('guest', 1);
                // ログイン
                $this->session->set('login', 1);

                $_country_key = $this->solveLocaleInfo();

                $this->session->set("login_type", 'invite');
                $this->logger->trace(__FUNCTION__."#meeting_info", __FILE__, __LINE__, $room_info);
                /* ブラウザ以外のデバイス */
                // 資料共有ユーザー
                if (strpos($_SERVER['HTTP_USER_AGENT'], "iPad;")) {
                    $this->session->set('invited_meeting_audience', 0);
                    $this->session->set('invited_meeting_ticket', $meetingInfo['meeting_ticket']);
                    if ($room_info["room_info"]["use_sales_option"]){
                        $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                    } else {
                        $url    = "vcube-meeting-docshare://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                    }
                     $this->logger2->info($url);
                    header("Location: ".$url);
                    exit();
                }
                //モバイル判別
                if (strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod")) {
                    $this->session->set('invited_meeting_ticket', $meetingInfo['meeting_ticket']);
                    if ($room_info["room_info"]["use_sales_option"]){
                        $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                    } else {
                        $url    = $this->config->get("N2MY", "mobile_protcol", 'vcube-meeting')."://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                    }
                    $this->logger2->info($url);
                    header("Location: ".$url);
                    exit();
                }
                $this->template->assign('room', $room_info);
                $this->template->assign('server', $this->config->get("N2MY","fms_server"));
                $this->template->assign('c', $_country_key);
                $this->template->assign( 'displaySize', $_COOKIE["displaySize"] );
                // 部屋一覧
                $this->template->assign('isContractShare', $isContractShare);
                //
                $this->template->assign('room_str', $reservation_info["room_key"]);
                $this->template->assign("hash", md5($_SERVER["HTTP_HOST"]."/".$reservation_info["room_key"]));
                $this->template->assign('server', $this->config->get("N2MY","fms_server"));
                $this->template->assign('serverDsnKey', $this->get_dsn_key() );
                $this->template->assign("knockerbuild_submit_key",$submit_key);
                // URL チェック
                if( strpos($current_url,"invite.php") === false ) {
                    $url = N2MY_BASE_URL."services/invite.php";
                    header("Location: ".$url);
                    exit();
                } else {
                    return $this->display("user/invite.t.html");
                }
            //会議終了
            } else {
                $this->template->assign("message", $this->get_message("DEFINE", "RESERVATION_ERROR_FINISHED"));
                $local_datetime = EZDate::getLocateTime(time(), $time_zone, N2MY_SERVER_TIMEZONE);
                $this->template->assign("now_date", $local_datetime);
                $this->template->assign("start", $reservation_info["reservation_starttime"]);
                $this->template->assign("end", $reservation_info["reservation_endtime"]);
                $this->template->assign("reservation_place", $time_zone);
                $this->template->assign("reservation_name", $reservation_info["reservation_name"]);
                $this->template->assign("organizer_name", $reservation_info["organizer_name"]);
                return $this->display('user/reservation/error.t.html');
            }
        }
        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);
    }

    /**
     * 招待ユーザのメインメニューを表示
     */
    function render_invite() {

        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        //サーバー取得
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $r_user_session = $this->session->get("r_user_session");
        //リダイレクト判定のため現在URLを取得
        $current_url = $_SERVER["REQUEST_URI"];
        if ( ! $server_info = $obj_MGMClass->getRelationDsn( $r_user_session, "invite" ) ){
            $message .= "<li>".$this->get_message("DEFINE", "LOGIN_ERROR_INVALIDUSER") . "</li>";
            return $this->display('user/reservation/error.t.html');
        }
        $submit_key = md5(microtime());
        $knocker_build = md5("knockerbuild_submit_key");
        $this->session->set("knockerbuild_submit_key",$submit_key,$knocker_build);
        $this->session->set("server_info", $server_info );
        if ($this->request->get("lang")) {
             $_COOKIE["lang"] = $this->request->get("lang");
            $this->session->set("lang", $_COOKIE["lang"]);
        }
        $obj_N2MY_Reservation = new N2MY_Reservation( $this->get_dsn() );
        $lang = $this->get_language();
        $this->_set_template_dir($lang);
        // ユーザごとに割り振られたキーから、予約IDを取得
        // sessionの確認
        if ( !$reservation_info = $obj_N2MY_Reservation->checkReservationInfo( $r_user_session )) {
            return $this->display('user/reservation/error.t.html');
        // 予約が削除されたか招待者から除外された
        } elseif ($reservation_info["r_user_status"] == "0" || $reservation_info["reservation_status"] == "0") {
            $message = array(
                "title" => $this->get_message("MEETING_START", "cancel_title"),
                "text" => $this->get_message("MEETING_START", "cancel_text"),
                "back_url" => "javascript:window.close();",
                "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                );
            $this->template->assign("message", $message);
            return $this->display('user/message.t.html');
        } else {
          //セルース対応
          $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
          $room_info = $obj_N2MY_Account->getRoomInfo($reservation_info["room_key"]);
          if ($room_info["room_info"]["use_sales_option"]) {
            $this->session->set("r_user_key",$reservation_info["user_key"]);
            $this->session->set("service_mode","sales");
            if($reservation_info["r_member_key"]) {
              $type = "staff";
                    $objMember = new MemberTable($this->get_dsn());
                    $member_info = $objMember->getDetail($reservation_info["r_member_key"]);
                    $this->session->set("member_info",$member_info);
                    $limit_time = time() + 365 * 24 * 3600;
                    setcookie("personal_email_secure", $member_info["member_email"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
                    setcookie("personal_name", $member_info["member_name"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            } else {
              $type = "customer";
              $customer_flg = 1;
              $this->session->set("customer_flg", 1);
              $this->template->assign('customer_flg', $customer_flg);
            }
          }

          //議長対応
          if($reservation_info["r_user_authority"]) {
              $this->session->set("authority_flg", 1);
         	  //$this->template->assign('authority_flg', 1);
          }

            //メンバー課金
            if( $reservation_info["r_member_key"] && !$this->session->get("member_login") ){
                require_once ('classes/N2MY_Auth.class.php');
                $obj_Auth = new N2MY_Auth( $this->get_dsn() );
                $login_info = $obj_Auth->getMemberInfo( $reservation_info["r_member_key"] );
                if( $login_info["user_info"]["account_model"] == "member" || $login_info["user_info"]["account_model"] == "centre" ){
                    $this->session->set('user_info', $login_info["user_info"]);
                    $this->session->set('member_info', $login_info["member_info"]);
                    //
                    if( 1 == $this->request->get( "action_invite" ) ){
                        $limit_time = time() + 365 * 24 * 3600;
                        setcookie("personal_email_secure", $_COOKIE["personal_email"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
                        setcookie("personal_name", $login_info["member_info"]["member_name"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
                    }
                }
            }
            if ($reservation_info["r_user_place"] == 100) {
                $time_zone = $reservation_info["reservation_place"];
            } else {
                $time_zone = $reservation_info["r_user_place"];
            }
            $login_start = strtotime($reservation_info['reservation_starttime']);
            $login_end = strtotime($reservation_info['reservation_endtime']);
            $now_time = time();

            // 現地時間
            $reservation_info['reservation_starttime'] = $obj_N2MY_Reservation->convTimeZone($reservation_info['reservation_starttime'], $time_zone);
            $reservation_info['reservation_endtime'] = $obj_N2MY_Reservation->convTimeZone($reservation_info['reservation_endtime'], $time_zone);

            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            /**
             * 接続可能なipか確認
             * とりあえずはオプション扱いではなく
             * 自分のkeyとひもづくiplistがあれば契約しているとみなし
             * 接続元のipと照らし合わせる
             */
            if (false === $obj_N2MY_Account->checkRemoteAddress($reservation_info["user_key"], $_SERVER["REMOTE_ADDR"])){
                $this->session->removeAll();
                return $this->render_valid();
            }
            $objMeeting = new MeetingTable($this->get_dsn());
            $meetingInfo = $objMeeting->getRow(sprintf("meeting_ticket='%s'", $reservation_info["meeting_key"]));
            $obj_Participant = new DBI_Participant($this->get_dsn());
            $pcount = $obj_Participant->numRows("meeting_key = ".$meetingInfo['meeting_key']." AND is_active = 1");
            $user_info = $this->session->get('user_info');
            $this->template->assign("use_clip_share" , $user_info['use_clip_share']);
            // 開催日時の確認
            //開催前
            if ( $login_start > $now_time ) {
                $this->template->assign("message", $this->get_message("DEFINE", "RESERVATION_ERROR_NOTSTARTED"));
                $local_datetime = EZDate::getLocateTime(time(), $time_zone, N2MY_SERVER_TIMEZONE);
                $this->template->assign("now_date", $local_datetime);
                $this->template->assign("start", $reservation_info["reservation_starttime"]);
                $this->template->assign("end", $reservation_info["reservation_endtime"]);
                $this->template->assign("reservation_place", $time_zone);
                $this->template->assign("reservation_name", $reservation_info["reservation_name"]);
                $this->template->assign("organizer_name", $reservation_info["organizer_name"]);
                return $this->display('user/reservation/error.t.html');
            // 開催期間中、又は参加者が０人以上
            } elseif ($login_start < $now_time && ($now_time < $login_end || $pcount > 0)) {
                /* セッション情報を取得 */
                $room_info = $obj_N2MY_Account->getRoomInfo( $reservation_info["room_key"] );
                if($room_info["room_info"]["meeting_key"] !=  $reservation_info["meeting_key"]) {
                  //現在進行中の会議をチェック
                  $objMeeting = new MeetingTable($this->get_dsn());
                  $where = "meeting_ticket ='".$room_info["room_info"]["meeting_key"]."'";
                  $meeting_key = $objMeeting->getRowsAssoc($where, null, null, null, "meeting_key");
                  $obj_Participant = new DBI_Participant($this->get_dsn());
                  $meeting_pCount = $obj_Participant->numRows("meeting_key = '".$meeting_key[0]["meeting_key"]."' AND is_active = 1");
                  $this->logger->debug(__FUNCTION__."#start", __FILE__, __LINE__,$meeting_pCount);
                  //前の会議を延長の場合
                  if($meeting_pCount > 0)
                  {
                    $this->template->assign("message", $this->get_message("DEFINE", "RESERVATION_ERROR_WAIT"));
                    $local_datetime = EZDate::getLocateTime(time(), $time_zone, N2MY_SERVER_TIMEZONE);
                    $this->template->assign("now_date", $local_datetime);
                    $this->template->assign("start", $reservation_info["reservation_starttime"]);
                    $this->template->assign("end", $reservation_info["reservation_endtime"]);
                    $this->template->assign("reservation_place", $time_zone);
                    $this->template->assign("reservation_name", $reservation_info["reservation_name"]);
                    $this->template->assign("organizer_name", $reservation_info["organizer_name"]);
                    return $this->display('user/reservation/error.t.html');
                  }
                }
                $this->session->set("room_info", array($reservation_info["room_key"] => $room_info));

                /* template上で見当たらないので削除中*/
                //$room_info["isReserve"] = $room->isReservationRoomStatus( $r[$i]['room_key'] );
                $room_info["room_info"]["meeting_key"] = $reservation_info["meeting_key"];
                $room_info["room_info"]["audience"] = $reservation_info["r_user_audience"];
                $room_info["room_info"]["now_reserve_start"] = $reservation_info["reservation_starttime"];
                $room_info["room_info"]["now_reserve_end"] = $reservation_info["reservation_endtime"];
                $room_info["room_info"]["reservation_place"] = $time_zone;
                $room_info["room_info"]["participant_num"] = 0;
                $room_info["room_info"]["audience_num"] = 0;
                $room_info["room_info"]["whiteboard_num"] = 0;
                if($room_info["options"]["desktop_share"]) {
                    $isContractShare = 1;
                }
                $this->session->set('guest', 1);                    // ゲスト
                $this->session->set('login', 1);                    // ログイン

                //所在地・タイムゾーン
				$_country_key = $this->solveLocaleInfo();

                $this->session->set("login_type", 'invite');
                $this->session->set('invite', true);
                $this->logger->trace(__FUNCTION__."#meeting_info", __FILE__, __LINE__, $room_info);
                /* ブラウザ以外のデバイス */
                // iPad
                if (strpos($_SERVER['HTTP_USER_AGENT'], "iPad;")) {
                    // モバイルではオーディエンスモードでの入室を禁止する
                    if( $room_info["room_info"]["audience"] == 1){
                        $message = array(
                                "title" => $this->get_message("MEETING_START", "mobile_audience_title"),
                                "text" => $this->get_message("MEETING_START", "mobile_audience_text"),
                                "back_url" => "javascript:window.close();",
                                "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                        );
                        $this->template->assign("message", $message);
                        $this->display('user/message.t.html');
                        exit;
                    } elseif ($this->session->get("authority_flg")) {
	                	$message = array(
	                			"title" => $this->get_message("MEETING_START", "mobile_authority_title"),
	                			"text" => $this->get_message("MEETING_START", "mobile_authority_text"),
	                			"back_url" => "javascript:window.close();",
	                			"back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
	                	);
	                	$this->template->assign("message", $message);
	                	$this->display('user/message.t.html');
	                	exit;
	                }
                    $this->session->set('invited_meeting_audience', $reservation_info["r_user_audience"]);
                    $this->session->set('invited_meeting_ticket', $meetingInfo['meeting_ticket']);
                    if ($room_info["room_info"]["use_sales_option"]){
                        $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                    } else {
                        $url    = "vcube-meeting-docshare://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                    }
                    $this->logger2->info($url);
                    header("Location: ".$url);
                    exit();
                }
                //モバイル判別
                if (strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod")) {
                    // モバイルではオーディエンスモードでの入室を禁止する
                    if( $room_info["room_info"]["audience"] == 1){
                        $message = array(
                                "title" => $this->get_message("MEETING_START", "mobile_audience_title"),
                                "text" => $this->get_message("MEETING_START", "mobile_audience_text"),
                                "back_url" => "javascript:window.close();",
                                "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                        );
                        $this->template->assign("message", $message);
                        $this->display('user/message.t.html');
                        exit;
                    } elseif ($this->session->get("authority_flg")) {
	                	$message = array(
	                			"title" => $this->get_message("MEETING_START", "mobile_authority_title"),
	                			"text" => $this->get_message("MEETING_START", "mobile_authority_text"),
	                			"back_url" => "javascript:window.close();",
	                			"back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
	                	);
	                	$this->template->assign("message", $message);
	                	$this->display('user/message.t.html');
	                	exit;
	                }
                    $this->session->set('invited_meeting_ticket', $meetingInfo['meeting_ticket']);
                    if ($room_info["room_info"]["use_sales_option"]){
                        $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                    } else {
                        $url    = $this->config->get("N2MY", "mobile_protcol", 'vcube-meeting')."://join?session=".session_id()."&room_id=".$meetingInfo["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meetingInfo['pin_cd'];
                    }
                    $this->logger2->info($url);
                    header("Location: ".$url);
                    exit();
                }

                //eco
                require_once( "classes/N2MY_Eco.class.php" );
                $thisMonth = date( "Y-m" );
                $lastYear = date( "Y-m", strtotime( "last year +1 month" ) );
                $objEco = new N2MY_Eco( $this->get_dsn(), N2MY_MDB_DSN );
                $ecoInfo = $objEco->getYearlyLog( $user_info["user_id"], $thisMonth, $lastYear );
                $ecoInfo["thisMonth"] = $thisMonth;
                $ecoInfo["lastYear"] = $lastYear;
                $this->template->assign( 'ecoInfo', $ecoInfo );

                //セールスログイン処理 [4.10.3.0 MTGVFOUR-827 【S&S】Web_マネージャー機能の追加対応]
                require_once("classes/N2MY_DBI.class.php");
                $r_db     = new N2MY_DB($this->get_dsn(),"reservation_user");
                $where   = sprintf("r_user_session = '%s'",$r_user_session);
                $r_data = $r_db->getRow($where,"r_manager_flg");
                $manager_flg = $r_data["r_manager_flg"];
                
                $vcube_login  = $this->session->get("vcubeid_login");
                $member_login = $this->session->get("member_login");
                $sales_login  = $this->session->get("sales_login");
                if ($user_info["account_model"] == "free" && $user_info["user_status"] == "3" && !$vcube_login) {
                     $assign_info = array(
                            "room_info" => $room_info,
                            "c" => $_country_key,
                            "sess_id" => $r_user_session,
                            "isContractShare" => $isContractShare,
                            "room_str" => $reservation_info["room_key"],
                     );
                     $this->session->set("assign_info", $assign_info);
                     $this->action_vcubeid_login();
                } else if ($user_info["account_model"] == "member" && $user_info["user_status"] == "1" && !$member_login && !$user_info["external_user_invitation_flg"]){
                // メンバー課金のログイン処理
                  $assign_info = array(
                            "room_info" => $room_info,
                            "c" => $_country_key,
                            "sess_id" => $r_user_session,
                            "isContractShare" => $isContractShare,
                            "room_str" => $reservation_info["room_key"],
                     );
                     $this->session->set("assign_info", $assign_info);
                     $this->action_memberCharges_login();
                } else if($room_info["room_info"]["use_sales_option"] && !$sales_login && $manager_flg) {
                //セールスログイン処理 [4.10.3.0 MTGVFOUR-827 【S&S】Web_マネージャー機能の追加対応]
                    $assign_info = array(
                            "room_info" => $room_info,
                            "c" => $_country_key,
                            "sess_id" => $r_user_session,
                            "isContractShare" => $isContractShare,
                            "room_str" => $reservation_info["room_key"],
                    );
                    $this->session->set("assign_info", $assign_info);
                    //所在地&言語&タイムゾーン設定
                    $time_zone = $this->request->get("time_zone", $this->request->getCookie("time_zone") ? $this->request->getCookie("time_zone") : "");
                    $timezone_list = $this->get_timezone_list();
                    if (!array_key_exists($time_zone, $timezone_list)) {
                        $timezone_keys = array_keys($timezone_list);
                        $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
                    }
                    $lang = $this->request->get("lang", $this->request->getCookie("lang") ? $this->request->getCookie("lang") : "");
                    $country = $this->request->get("country", $this->request->getCookie("country"));
                    $selected_country_key = $this->request->getCookie("selected_country_key");
                    $cookie_remote_addr = $this->request->getCookie("remote_addr");
                    $remote_addr = $_SERVER["REMOTE_ADDR"];
                    if($cookie_remote_addr != $remote_addr)$selected_country_key = "auto";
                    $this->template->assign('message', $message);
                    $this->template->assign('lang', $lang);
                    $this->template->assign("country", $country);
                    $this->template->assign('time_zone',$time_zone);
                    $this->template->assign("selected_country_key", $selected_country_key);
                    $this->action_sales_login();
                } else {
                    $this->template->assign('is_manager', $this->session->get("is_manager"));
                    $this->template->assign('displaySize', $_COOKIE["displaySize"] );
                    $this->template->assign('room', $room_info);
                    $this->template->assign('server', $this->config->get("N2MY","fms_server"));
                    $this->template->assign('c', $_country_key);
                    $this->template->assign('sess_id', $r_user_session);
                    $this->template->assign('displaySize', $_COOKIE["displaySize"] );
                    // 部屋一覧
                    $this->template->assign('isContractShare', $isContractShare);
                    //
                    $this->template->assign('room_str', $reservation_info["room_key"]);
                    $this->template->assign("hash", md5($_SERVER["HTTP_HOST"]."/".$reservation_info["room_key"]));
                    $this->template->assign('server', $this->config->get("N2MY","fms_server"));
                    $this->template->assign('serverDsnKey', $this->get_dsn_key() );
                    $this->template->assign("knockerbuild_submit_key",$submit_key);
                    // URL チェック
                    if( strpos($current_url,"invite.php") === false ) {
                        $url = N2MY_BASE_URL."services/invite.php";
                        header("Location: ".$url);
                        exit();
                    } else {
                        return $this->display("user/invite.t.html");
                    }
                }

            //会議終了
            } else {
                $this->template->assign("message", $this->get_message("DEFINE", "RESERVATION_ERROR_FINISHED"));
                $local_datetime = EZDate::getLocateTime(time(), $time_zone, N2MY_SERVER_TIMEZONE);
                $this->template->assign("now_date", $local_datetime);
                $this->template->assign("start", $reservation_info["reservation_starttime"]);
                $this->template->assign("end", $reservation_info["reservation_endtime"]);
                $this->template->assign("reservation_place", $time_zone);
                $this->template->assign("reservation_name", $reservation_info["reservation_name"]);
                $this->template->assign("organizer_name", $reservation_info["organizer_name"]);
                return $this->display('user/reservation/error.t.html');
            }
        }
        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);
    }

    /**
     * 会議開始画面を表示
     */
    function render_start_view() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->display('user/start_invite.t.html');
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * set Cookie  = country_key, selected_country_key, time_zone
     * set Session = country_key, selected_country_key
     * @return country_key
     */
	private function solveLocaleInfo(){
		// 所在地（すでにCookieにはいっている場合はCookieを優先）
		$_country_key = $_COOKIE ['country'] ? $_COOKIE ['country'] : 'auto';
		// if key=auto then use maxmind
		$result_country_key = parent::_get_country_key( $_country_key );
		$selected_country_key = $_COOKIE ['selected_country_key'] ? $_COOKIE ['selected_country_key'] : $_country_key;
		if (! $result_country_key) {
			// 地域コードの不正な指定を防ぐ
			$country_list = parent::get_country_list();
			if (! array_key_exists( $_country_key, $country_list )) {
				$country_keys = array_keys( $country_list );
				$_country_key = $country_keys [0];
			}
			foreach ( $country_list as $country_key => $country_row ) {
				if ($_country_key == $country_key) {
					$result_country_key = $country_row ['country_key'];
					break;
				}
			}
		}
		$this->session->set( 'selected_country_key', $selected_country_key );
		$this->session->set( 'country_key', $result_country_key );
		setcookie( 'country', $result_country_key, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH, N2MY_COOKIE_DOMAIN, N2MY_COOKIE_SECURE, N2MY_COOKIE_HTTPONRY );
		setcookie( 'selected_country_key', $selected_country_key, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH, N2MY_COOKIE_DOMAIN, N2MY_COOKIE_SECURE, N2MY_COOKIE_HTTPONRY );

		// タイムゾーン
		$time_zone = $this->request->getCookie( 'time_zone', $this->config->get( 'GLOBAL', 'time_zone', 9 ) );
		setcookie( 'time_zone', $time_zone, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH, N2MY_COOKIE_DOMAIN, N2MY_COOKIE_SECURE, N2MY_COOKIE_HTTPONRY );

		return $_country_key;
	}

    private function checkFms($userKey)
    {
        require_once("classes/dbi/user_fms_server.dbi.php");
        $objFmsServer = new UserFmsServerTable($this->get_dsn());
        //複数契約は後ほど
        $serverInfo = $objFmsServer->getRow(sprintf("user_key='%s'", $userKey));

        $this->template->assign("serverInfo", $serverInfo);
        $url = N2MY_BASE_URL."services/invite.php";
        $this->template->assign("url", $url);
        $this->template->assign("actionName", "action_distinction");
        $this->display("user/login/check_fms.t.html");
    }

    public function render_valid() {
        $message["title"] = $this->get_message("WHITELIST", "title");
        $message["body"] = $this->get_message("WHITELIST", "body");
        $this->template->assign('message', $message);
        $this->display('common.t.html');
    }

    /**
     * intraFMS契約ユーザーのクライアント、intraFMS間のネットワークチェック結果
     */
    public function action_distinction()
    {
        if ("true" == $this->request->get("result")) {
            return $this->render_invite();
        } else {
//            $this->template->assign("connectionFail", "1");

            $message["title"] = $this->get_message("INTRAFMS", "title");
            $message["body"] = $this->get_message("INTRAFMS", "body");
            $this->template->assign('message', $message);
            $this->display('common.t.html');
            exit;
            return $this->display('user/reservation/error.t.html');
        }
    }

    // ダミーの内容を渡しGATEは正常とするための画面
    function action_error_url() {
            $message["body"] = $this->get_message("MEETING_START", "max_portplan_error");
        $this->template->assign('message', $message);
        return $this->display('common.t.html');
    }
    /**
     * モバイルの暗証番号ログイン用画面表示
     * webからアクセスする場合エラーページを出力
     */
    function action_render_pin_login_mobile ($message = ""){
        if(strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod") || strpos($_SERVER['HTTP_USER_AGENT'],"iPad")){
            $time_zone = $this->request->get("time_zone", $this->request->getCookie("time_zone") ? $this->request->getCookie("time_zone") : "");
            $timezone_list = $this->get_timezone_list();
            if (!array_key_exists($time_zone, $timezone_list)) {
                $timezone_keys = array_keys($timezone_list);
                $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
            }
            $lang = $this->request->get("lang", $this->request->getCookie("lang") ? $this->request->getCookie("lang") : "");
            $country = $this->request->get("country", $this->request->getCookie("country"));
            $selected_country_key = $this->request->getCookie("selected_country_key");
            $cookie_remote_addr = $this->request->getCookie("remote_addr");
            $remote_addr = $_SERVER["REMOTE_ADDR"];
            if($cookie_remote_addr != $remote_addr)$selected_country_key = "auto";
            $this->template->assign('message', $message);
            $this->template->assign('lang', $lang);
            $this->template->assign("country", $country);
            $this->template->assign('time_zone',$time_zone);
            $this->template->assign("selected_country_key", $selected_country_key);
            $template = 'user/login/pin_login_mobile.t.html';
        }else{
            $message["title"] = $this->get_message("DEFINE", "PIN_LOGIN_ERROR_MOBILE_PAGE_TITLE");
            $message["body"] = $this->get_message("DEFINE", "PIN_LOGIN_ERROR_MOBILE_PAGE_BODY");
            $this->template->assign('message', $message);
            $template = 'user/login/error.t.html';
        }
        return $this->display($template);
    }

}

$main =& new AppInviteMenu();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
