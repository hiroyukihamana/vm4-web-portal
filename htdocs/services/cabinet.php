<?php

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/N2MY_FileUpload.class.php");
require_once("classes/dbi/file_cabinet.dbi.php");
require_once("classes/dbi/meeting.dbi.php");

class AppCabinet extends AppFrame
{
    var $logger = null;

    function init()
    {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->objFileUpload = new N2MY_FileUpload();
        $this->objFileCabinet = new FileCabinetTable($this->get_dsn());
        $this->objMeeting = new MeetingTable($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth()
    {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    public function default_view()
    {
        $userInfo = $this->session->get("user_info");
        $request = $this->request->getAll();
        $cabinetInfo = $this->objFileCabinet->getRow(sprintf("cabinet_id='%s'", $request["cabinet_id"]));
        $meetingInfo = $this->objMeeting->getRow(sprintf("meeting_key='%s'", $cabinetInfo["meeting_key"]), null, null, 1, 0, "user_key");
        $this->logger2->info(array($meetingInfo, $userInfo, $cabinetInfo));
        $file_name = $cabinetInfo['file_name'];
        /**
         * 管理者のみ追加
         */
        if ($meetingInfo["user_key"] == $userInfo["user_key"] &&
            1 == $cabinetInfo["status"]) {
            $fullpath = $this->objFileUpload->getFullPath( $cabinetInfo["file_cabinet_path"] );
            $this->objFileUpload->checkFile( $fullpath, $cabinetInfo["tmp_name"] );
            $targetFile = sprintf( "%s%s", $fullpath, $cabinetInfo["tmp_name"] );
            $content_length = filesize( $targetFile );
            $useragent = getenv("HTTP_USER_AGENT");

            $cabinetInfo["file_name"] = str_replace(" ","+",$cabinetInfo["file_name"]);

            // ie
            if( ereg( "MSIE", $useragent ) ) {
                $fileName = mb_convert_encoding( $cabinetInfo["file_name"] , "SJIS-win" );
                Header("Content-Disposition: attachment; filename=" . $fileName );
                Header('Pragma: private');
                Header('Cache-Control: private');
                Header("Content-Type: application/octet-stream-dummy");
            } else {
                if ( preg_match( "/safari/i", $useragent ) && !preg_match("/chrome/i",$useragent)) {
                    //Safariの場合は全角文字が全て化けるので、何かしら固定のファイル名にして回避
                    $cabinetInfo["file_name"] = "";
                }
                Header("Content-Disposition: attachment; filename=" . $cabinetInfo["file_name"] );
                Header("Content-Type: application/octet-stream; name=" . $cabinetInfo["file_name"] );
            }
            Header("Content-Length: ". $content_length );
            readfile($targetFile);
            // 操作ログ
            $this->add_operation_log('meetinglog_cabinet_download', array(
                'room_key'      => $meetingInfo['room_key'],
                'meeting_name'  => $meetingInfo['meeting_name'],
                'file_name'     => $file_name,
                ));
            exit;
        } else
            $this->logger2->warn(array($meetingInfo, $userInfo, $cabinetInfo));
            $this->render_fault();
    }

    private function checkFile( $path, $name )
    {
        if( ! $this->objFileUpload->checkFile( $path, $name ) ) { $this->render_fault(); }
    }

    private function render_fault()
    {
        header("Location: /services/not_found.php");
        /*
        $this->session->clear();
        $this->template->assign('message', UNLAWFUL_ACCESS);
        $this->display('user/session_error.t.html');
        exit();
        */
    }
}

$main = new AppCabinet();
$main->execute();
