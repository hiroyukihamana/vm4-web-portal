<?php


require_once('classes/AppFrame.class.php');
require_once ('classes/dbi/pw_reset.dbi.php');

class AppPwResetBatch extends AppFrame
{

    function init()
    {
      $this->pw_reset    = new PwResetTable($this->get_dsn());
    }

    function default_view()
    {
        $this->action_pw_data_delete;
    }

    /**
     * 登録から24時間経過したのは削除します！
     */
    function action_pw_data_delete() {
        $sql = "DELETE FROM pw_reset WHERE create_datetime < (UTC_TIMESTAMP() - INTERVAL 24 HOUR)";
        $rs = $this->pw_reset->_conn->query($sql);
        if (DB::isError($rs)) {
            print $rs->getUserInfo();
            return false;
        }
        new dBug($rs->result);
    }
}

$main = new AppBatch();
$main->execute();

?>
