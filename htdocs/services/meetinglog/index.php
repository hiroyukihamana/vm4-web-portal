<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once('config/config.inc.php');
require_once('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");
require_once("classes/N2MY_MeetingLog.class.php");
require_once("classes/core/Core_Meeting.class.php");
require_once("classes/core/dbi/MeetingSequence.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once("classes/dbi/file_cabinet.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/dbi/ordered_service_option.dbi.php");
require_once('classes/dbi/user_service_option.dbi.php');
require_once('classes/dbi/room.dbi.php');
require_once("classes/dbi/reservation.dbi.php");
require_once 'lib/EZLib/EZUtil/EZMath.class.php';
require_once('lib/pear/Pager.php');
require_once ("classes/N2MY_Storage.class.php");
class AppMeetingLog extends AppFrame {

    /**
     * ログクラス
     * @access public
     */
    var $logger = null;
    var $meetingLog = null;

    function init() {
        // 予約専用ページらかログインした場合は予約作成ページに飛ばす
        if($this->session->get('login_type') == 'reservation'){
            $url = $this->get_redirect_url("services/reservation/?action_create=&new=1");
            header("Location: ".$url);
        }
        $this->obj_N2MY_Account     = new N2MY_Account($this->get_dsn());
        $this->obj_CoreMeeting      = new Core_Meeting($this->get_dsn());
        $this->obj_Meeting          = new MeetingTable($this->get_dsn());
        $this->objMeetingSequence   = new DBI_MeetingSequence($this->get_dsn());
        $this->obj_MeetingLog       = new N2MY_MeetingLog($this->get_dsn());
        $this->objParticipant       = new DBI_Participant($this->get_dsn());
        $this->objFileCabinet       = new FileCabinetTable($this->get_dsn());
        $this->_name_space          = md5(__FILE__);
        $this->obj_Reservation      = new ReservationTable($this->get_dsn());
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $user_info   = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
       (($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") && $member_info)?
            $this->action_show_membertop():
            $this->action_show_top();
    }

    //自分が開催したもの及び参加した会議を表示
    private function action_show_membertop()
    {
        if (1 == $this->request->get("reset")) {
            // 検索クエリーをクリア
            $this->session->remove("condition", $this->_name_space);
        } else if ($this->request->get("action_show_membertop") != 1) {
            // 検索指定
            $search                 = $this->request->get("search");
            $search["page_cnt"]     = $this->request->get("page_cnt");
            $search["page"]         = $this->request->get("page", 1);
            $search["sort_key"]     = $this->request->get("sort_key")  ? htmlspecialchars($this->request->get("sort_key"),ENT_QUOTES)  : "actual_start_datetime";
            $search["sort_type"]    = $this->request->get("sort_type") ? htmlspecialchars($this->request->get("sort_type"),ENT_QUOTES) : "desc";
            $this->session->set("condition", $search, $this->_name_space);
        }
		$this->display_memberlist();
    }

	function display_memberlist(){
        $member_info     = $this->session->get("member_info");
        $user_info       = $this->session->get("user_info");
        $search = $this->session->get("condition", $this->_name_space);
        $param = array(
            "room_key"              => $member_info["room_key"],
            "user_key"              => $user_info["user_key"],
            "title"                 => $search["title"],
            "participant"           => $search["participant"],
            "meeting_size_used"     => $search["size"],
            "start_datetime"        => ($search["start"]) ? EZDate::getLocateTime($search["start"]." 00:00:00", N2MY_SERVER_TIMEZONE, N2MY_USER_TIMEZONE, "Y-m-d H:i:s") : "",
            "end_datetime"          => ($search["end"])   ? EZDate::getLocateTime($search["end"]." 23:59:59", N2MY_SERVER_TIMEZONE, N2MY_USER_TIMEZONE, "Y-m-d H:i:s") : "",
            "has_recorded_video"    => $search["has_recorded_video"],
            "has_recorded_minutes"  => $search["has_recorded_minutes"],
            "is_locked"             => $search["is_locked"],
        );
        //ページ情報取得
        $meeting_info = $this->obj_CoreMeeting->getParticipatedInfo($member_info["member_key"] , $user_info["user_key"], $param);
        //部屋情報取得
        $obj_room = new N2MY_DB($this->get_dsn(), 'room');
		$where = "room_status = 1 AND user_key = " . $user_info['user_key'];
		$_room_list = $obj_room->getRowsAssoc($where);
		foreach ($_room_list as $_room) {
			$room_list[$_room['room_key']]['room_info'] = $_room;
		}
		if($room_list){
			$this->template->assign('room_list', $room_list);
		}
        //pager
        $page_cnt     = $search["page_cnt"];
        $current_page = $search["page"];
        // ページ情報取得
        if (!$page_cnt) {
            $page_cnt = 10;
        }
        $current_page = ($current_page > 1) ? $current_page :1;
        $pager        = $this->setPager($page_cnt, $current_page, $meeting_info["count"]);
        $this->template->assign('pager', $pager);
        // 会議参加者一覧を取得
        if ($pager["start"] > 0) {
            $offset = $pager["start"] - 1;
        } else {
            $offset = 0;
        }
        if (!$search["sort_key"]){
            $search["sort_key"]  = "meeting_key";
        }
        if (!$search["sort_type"]){
            $search["sort_type"] = "desc";
        }
		$param["member_key"] = $member_info["member_key"];
        $param["limit"]      = $page_cnt;
        $param["offset"]     = $offset;
        $param["sort_key"]   = $search["sort_key"];
        $param["sort_type"]  = $search["sort_type"];
        $list = $this->obj_CoreMeeting->getParticipatedMeetingList($param);
        $log_list = array();
        foreach ($list as $key => $val){
            $val["meeting_use_time"]      = $val["meeting_use_minute"];
            $val["meeting_size_used"]     = EZMath::number_format($val["meeting_size_used"], 1024);
            $val["actual_start_datetime"] = EZDate::getLocateTime($val["actual_start_datetime"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE);
            $log_list[] = $val;
        }
        $objMeeting  = new MeetingTable($this->get_dsn());
        $serverInfo   = $this->session->get("server_info");
        $obj_Storage  = new N2MY_Storage($this->get_dsn(), $serverInfo["host_name"]);
        $logSize      = $obj_Storage->get_total_size($user_info["user_key"]);
        //$logSize    = $objMeeting->getOne(sprintf("user_key='%s'", $user_info["user_key"]), "sum(meeting_size_used)");
        $objUserServiceOption = new UserServiceOptionTable($this->get_dsn());
        $where = "user_key = '".addslashes($user_info["user_key"])."'".
            " AND service_option_key = 4" .
            " AND user_service_option_status = 1";
        $count        = $objUserServiceOption->numRows($where);
        //$max_rec_size = ($count * 500);
        $max_rec_size = $user_info["max_storage_size"];
        $this->template->assign('search', $search);
        $this->template->assign('max_rec_size'   , $max_rec_size);
        $this->template->assign("total_log_size" , round($logSize / (1024 * 1024)));
        $this->template->assign('log_list'       , $log_list);
        $this->display('user/meetinglog/index.t.html');

	}

    /**
     * 議事録検索
     */
    function action_show_top(){
        if (1 == $this->request->get("reset")) {
            // 検索クエリーをクリア
            $this->session->remove("condition", $this->_name_space);
        } else if ($this->request->get("action_show_top") != 1) {
            // 検索指定
            $search                 = $this->request->get("search");
            $search["page_cnt"]     = $this->request->get("page_cnt");
            $search["page"]         = $this->request->get("page", 1);
            $search["sort_key"]     = $this->request->get("sort_key")  ? htmlspecialchars($this->request->get("sort_key"),ENT_QUOTES)  : "actual_start_datetime";
            $search["sort_type"]    = $this->request->get("sort_type") ? htmlspecialchars($this->request->get("sort_type"),ENT_QUOTES) : "desc";
            $room_key = $this->get_room_key($room_key);
            $search["room_key"]    = $room_key;
            $this->session->set("condition", $search, $this->_name_space);
        }
        $this->display_list();
    }

    public function action_sort_list() {
        $search = $this->session->get("condition", $this->_name_space);
        $search["page"]         = 1;
        $search["sort_key"]     = $this->request->get("sort_key")  ? htmlspecialchars($this->request->get("sort_key"),ENT_QUOTES)   : "actual_start_datetime";
        $search["sort_type"]    = $this->request->get("sort_type") ? htmlspecialchars($this->request->get("sort_type"),ENT_QUOTES)  : "desc";
        $sort_key_rules = array("actual_start_datetime","meeting_name","meeting_name","meeting_use_minute","meeting_size_used");
        if (!EZValidator::valid_allow($search["sort_key"], $sort_key_rules)) {
            $search["sort_key"] = "actual_start_datetime";
        }
        $sort_type_rules = array("asc","desc");
        if (!EZValidator::valid_allow($search["sort_type"], $sort_type_rules)) {
            $search["sort_type"] = "desc";
        }
        $this->session->set("condition", $search, $this->_name_space);
		$user_info = $this->session->get("user_info");
		if($user_info['account_model'] == 'member' || $user_info['account_model'] == 'centre'){
			$this->display_memberlist();
		}else{
	        $this->display_list();
		}
    }

    public function display_list() {
        $search = $this->session->get("condition", $this->_name_space);
		$user_info = $this->session->get('user_info');
        $member_info = $this->session->get('member_info');
        if (!$user_info['is_add_room'] && !$user_info['is_one_time_meeting'] && !$search["room_key"]) {
            $room_list = $this->session->get("room_info");
            if (count($room_list) == 1) {
                $room_info          = array_shift($room_list);
                $search["room_key"] = $room_info["room_info"]["room_key"];
            }
        }
        $page_cnt     = $search["page_cnt"];
        $current_page = $search["page"];
        $user_info    = $this->session->get('user_info');
        if($this->check_one_account() && $this->session->get("service_mode") == "meeting"){
            $room_list = $this->get_ONE_whitelist_room_list($user_info["user_key"], $member_info["member_key"], true);
        }else{
            $room_list = $this->session->get("room_info");
        }
        if($this->session->get("service_mode") != "meeting" && ($member_info["use_ss_watcher"] || $this->session->get('role') == '10')){
            if($this->obj_N2MY_Account->getSalesRoomCntCheck( $user_info["user_key"] )){
                // 分割表示の際、Room情報に不備があるため紐付いている部屋を再設定
                $room_list = $this->obj_N2MY_Account->getOwnRoom( $member_info["member_key"], $user_info["user_key"], $this->session->get("service_mode") );  
                $this->session->set( "room_info", $room_list );
            }

            foreach($room_list as $key => $info){
                if($info["room_info"]["outbound_id"] == $member_info["outbound_id"]){
                    $search["room_key"] = $info["room_info"]["room_key"];
                    $room[$key] = $info;
                }
                $room_list = $room;
            }
            $room_count = count($room_list);
        }
        require_once ("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable( $this->get_dsn() );
        $where = "user_key = ".mysql_real_escape_string($user_info["user_key"]);
        $rooms = $obj_Room->getRowsAssoc($where, null, null, null, "room_key, meeting_key");
        foreach( $rooms as $key => $val ){
            $query[$val["room_key"]] = $val["meeting_key"];
        }
        $this->obj_CoreMeeting->checkReservationMeetingStatus($query);
        // 会議の検索ヒット件数を取得
        $param = array(
            "user_key"              => $user_info["user_key"],
            "room_key"              => $search["room_key"],
            "title"                 => $search["title"],
            "delete"                => 0,
            "participant"           => $search["participant"],
            "meeting_size_used"     => $search["size"],
            "start_datetime"        => ($search["start"]) ? EZDate::getLocateTime($search["start"]." 00:00:00", N2MY_SERVER_TIMEZONE, N2MY_USER_TIMEZONE, "Y-m-d H:i:s") : "",
            "end_datetime"          => ($search["end"])   ? EZDate::getLocateTime($search["end"]." 23:59:59", N2MY_SERVER_TIMEZONE, N2MY_USER_TIMEZONE, "Y-m-d H:i:s") : "",
            "has_recorded_video"    => $search["has_recorded_video"],
            "has_recorded_minutes"  => $search["has_recorded_minutes"],
            "is_locked"             => $search["is_locked"],
            "is_reserved"           => $search["is_reserved"],
            );
        $this->logger2->debug($param);
        $template_file = "user/meetinglog/index.t.html";
        $serverInfo   = $this->session->get("server_info");
        $obj_Storage  = new N2MY_Storage($this->get_dsn(), $serverInfo["host_name"]);
        $used_storage_size = $obj_Storage->get_total_size($user_info["user_key"]);
        $this->template->assign("total_log_size" , round($used_storage_size / (1024 * 1024)));
        $max_rec_size = $user_info["max_storage_size"];
        $this->template->assign('max_rec_size', $max_rec_size);
        if($this->check_one_account() && $this->session->get("service_mode") == "meeting"){
            if(!$room_list){
                $this->template->assign("total_use_time" , 0);
                $this->template->assign('page_cnt'       , $page_cnt);
                $this->template->assign("log_size_unit"  , "MB");
                $this->template->assign('room_list'      , null);
                $this->template->assign('log_list'       , null);
                $pager = $this->setPager($page_cnt, $current_page, 0);
                $this->template->assign("pager", $pager);
                $this->display($template_file);
                return;
            }
            $param["room_key"] = array_keys($room_list);
        }
        $summary         = $this->obj_CoreMeeting->getSummary($param);
        $allnum          = $summary["meeting_count"];
        $total_log_size  = $summary["total_size_used"];
        // ページ情報取得
        if (!$page_cnt) $page_cnt = 10;
        $pager = $this->setPager($page_cnt, $current_page, $allnum);
        $offset = ($pager["start"] > 0) ? $pager["start"] - 1 : 0;
        if (!$search["sort_key"])  $search["sort_key"]  = "actual_start_datetime";
        if (!$search["sort_type"]) $search["sort_type"] = "desc";
        $current_page = ($current_page > 1) ? $current_page :1;
        $param["limit"]     = $page_cnt;
        $param["offset"]    = $offset;
        $param["sort_key"]  = $search["sort_key"];
        $param["sort_type"] = $search["sort_type"];
        $list = $this->obj_CoreMeeting->getMeetingInfoList($param);
        // データ整形
        $log_list = array();
        $meeting_keys = array();
        foreach ($list as $key => $val){
            $val["meeting_size_used"]     = EZMath::number_format($val["meeting_size_used"], 1024);
            $val["actual_start_datetime"] = EZDate::getLocateTime($val["actual_start_datetime"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE);
            require_once("classes/dbi/fms_watch_status.dbi.php");
            $objFmsWatch  = new FmsWatchTable($this->get_dsn());
            $where        = sprintf("meeting_key=%d", $val["meeting_key"]);
            $fmsWatchInfo = $objFmsWatch->getRow($where);
            $val["fms_watch_status"] = $fmsWatchInfo["status"];
            $val["options"] = $room_list[$val["room_key"]]["options"];
            $log_list[] = $val;
            $meeting_keys[] = $val['meeting_key'];
        }

        if ($meeting_keys) {
            try {
                require_once('classes/dbi/meeting_clip.dbi.php');
                $meeting_clip_table  = new MeetingClipTable($this->get_dsn());
                $count_meeting_clips = $meeting_clip_table->countByMeetingKeys($meeting_keys);
            } catch (Exception $e) {
                $this->logger2->error(__FILE__.':'.__LINE__.' => '.$e->getMessage()); // fix me error template
                die(__LINE__);
            }
        }
        // 部屋の容量
        /*if ($search["room_key"]) {
            if ($user_info["account_model"] == 'member'){
                $max_rec_size = MEMBER_RECORD_SIZE + ($room_list[$search["room_key"]]["options"]["hdd_extention"] * 500);
            } else {
                $max_rec_size = RECORD_SIZE + ($room_list[$search["room_key"]]["options"]["hdd_extention"] * 1024);
            }
        }*/
        $this->template->assign("total_use_time" , $summary["total_use_time"]);
        $this->template->assign('page_cnt'       , $page_cnt);
        $this->template->assign('pager'          , $pager);
        $this->template->assign("log_size_unit"  , "MB");
        $this->template->assign('room_list'      , $room_list);
        $this->template->assign('log_list'       , $log_list);
        $this->template->assign('search'         , $search);
        $this->template->assign('count_meeting_clips' , $count_meeting_clips);
        $this->template->assign('room_count' , $room_count);
        $this->display($template_file);
    }

    /**
     * パスワードを入力
     */
    function action_auth($message = null) {
        $room_key = $this->request->get("room_key");
        $meeting_id = $this->request->get("meeting_key");
        $page_cnt = $this->request->get("page_cnt");
        $pager = $this->request->get("pager");
        if ($message){
            $this->template->assign('message', $message);
        }
        $this->template->assign('room_key' , $room_key);
        $this->template->assign('meeting_session_id' , $meeting_id);
        $this->template->assign('page_cnt' , $page_cnt);
        $this->template->assign('pager' , $pager);
        $this->display('user/meetinglog/auth.t.html');
    }

    function action_pw_check() {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__,$this->request->getAll());
        $room_key = $this->request->get("room_key");
        $meeting_session_id = $this->request->get("meeting_key");
        $meetinglog_pw = $this->request->get("meetinglog_pw");
        //
        $check_flg = $this->obj_MeetingLog->checkPassword($room_key, $meeting_session_id, $meetinglog_pw);
        if ($check_flg) {
            $where = "room_key = '".mysql_real_escape_string( $room_key )."'" .
                " AND meeting_session_id = '".mysql_real_escape_string( $meeting_session_id )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
            $meeting_data = $this->obj_Meeting->getOne($where, "meeting_key");
            $this->request->set("meeting_key",$meeting_data);
            $this->request->set("page_cnt",$this->request->get("page_cnt"));
            $this->request->set("pager",$this->request->get("pager"));
            $this->action_showDetail();
            exit;
        } else {
            $err_msg = RESERVATION_ERROR_NOTMUCTH_AUTH_PW;
        }
        return $this->action_auth($err_msg);
    }

    public function action_showDetail()
    {
        $userInfo = $this->session->get("user_info");
        $request     = $this->request->getAll();
        $where       = sprintf("meeting_key='%s' AND user_key='%s'", $request["meeting_key"], $userInfo["user_key"]);
        $meetingInfo = $this->obj_Meeting->getRow($where);
        // 不正アクセス対策
        if (! $meetingInfo) {
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$meetingInfo);
            return $this->default_view();
        }
        $meetingInfo["meeting_size_used"] = EZMath::number_format($meetingInfo["meeting_size_used"], 1024);
        $meetingInfo["participantList"]   = $this->objParticipant->getList($meetingInfo ["meeting_key"], false);
    foreach ($meetingInfo["participantList"] as $participant) {
          $participation_time = strtotime($participant["uptime_end"]) - strtotime($participant["uptime_start"]);
          if($participant["participant_name"] && $participation_time > 0 ) {
              if ($participant["participant_type_name"] != "audience"
                && $participant["participant_type_name"] != "multicamera_audience"
                && $participant["participant_type_name"] != "ss_watcher"
                && $participant["participant_type_name"] != "whiteboard_audience"
                && $participant["participant_type_name"] != "invisible_wb_audience" ) {
                      $participants_list[] = $participant;
              } else if ($participant["participant_type_name"] == "audience"
                      || $participant["participant_type_name"] == "multicamera_audience"
                      || $participant["participant_type_name"] == "whiteboard_audience"
                      || $participant["participant_type_name"] == "invisible_wb_audience" ) {
                  $audiences[] = $participant;
              }
          }
        }
        $meetingInfo["participants"] = $participants_list;
        $meetingInfo["audiences"]    = $audiences;
        if($meetingInfo["has_shared_memo"] == 1){
            require_once("classes/dbi/shared_file.dbi.php");
            $objSharedFile = new sharedFileTable($this->get_dsn());
            $shared_where = sprintf("status = 1 AND meeting_key='%s'", $request["meeting_key"]);
            $sharedFileList = $objSharedFile->getRowsAssoc($shared_where);
            $this->template->assign("sharedFileList"     , $sharedFileList);
        }
        $where = sprintf("meeting_key='%s'", $request["meeting_key"]);
        $sequeceList = $this->objMeetingSequence->getRowsAssoc($where, array("meeting_sequence_key"=>"asc"));
        $list["count"] = 0;
        for ($i = 0; $i < count($sequeceList); $i++) {
            if ($sequeceList[$i]["has_recorded_video"] || $sequeceList[$i]["record_status"] == "complete") {
                $data = sprintf("%s,log_video,%s,%s", $request["meeting_key"], $this->session->get("lang"), $sequeceList[$i]["meeting_sequence_key"]);
                $minute_data = sprintf("%s,log_minute,%s,%s", $meetingInfo["meeting_key"], $this->session->get("lang"), $sequeceList[$i]["meeting_sequence_key"]);
                if (file_exists(N2MY_MOVIES_DIR.$sequeceList[$i]["record_filepath"])) {
                    $record_filesize = EZMath::number_format(filesize(N2MY_MOVIES_DIR.$sequeceList[$i]["record_filepath"]), 1024);
                } else {
                    $record_filesize = 0;
                }
                $videoData = array(
                                "key"                   => $sequeceList[$i]["meeting_sequence_key"],
                                "has_recorded_video"    => $sequeceList[$i]["has_recorded_video"],
                                "record_id"             => $sequeceList[$i]["record_id"],
                                "record_endtime"        => $sequeceList[$i]["record_endtime"] ? EZDate::getLocateTime( $sequeceList[$i]["record_endtime"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE) : "",
                                "record_second"         => $sequeceList[$i]["record_second"],
                                "record_status"         => $sequeceList[$i]["record_status"],
                                "record_result"         => $sequeceList[$i]["record_result"],
                                "record_filepath"       => $sequeceList[$i]["record_filepath"],
                                "record_file_extension" => end(explode('.', $sequeceList[$i]["record_filepath"])),
                                "record_filesize"       => $record_filesize,
                                );
                $list["video"][] = $videoData;
                $list["count"]++;
                if ($sequeceList[$i]["has_recorded_video"]) {
                    $list["video_flg"] = 1;
                }
            }
            if ($sequeceList[$i]["has_recorded_minutes"]) $list["minute"][] = $sequeceList[$i]["meeting_sequence_key"];
            if ($sequeceList[$i]["has_shared_memo"]){
                $list["memo"][] = $sequeceList[$i]["meeting_sequence_key"];
            }
        }
        $meetingInfo["sequeceList"]           = $list;
        $meetingInfo["actual_start_datetime"] = EZDate::getLocateTime($meetingInfo["actual_start_datetime"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE);

        //cabinet
        $where = sprintf("meeting_key='%s' AND status = 1", $request["meeting_key"]);
        $meetingInfo["cabinetList"] = $this->objFileCabinet->getRowsAssoc($where, array("cabinet_id"=>"asc"));

        $user_info = $this->session->get('user_info');
        require_once('classes/N2MY_Clip.class.php');
        $n2my_clip = new N2MY_Clip($this->get_dsn());
        try {
            $clips = $n2my_clip->findIncDeletedByUsersMeetingKey($meetingInfo['meeting_key'], $user_info['user_key']);
        } catch (Exception $e) {
            $this->logger2->error(__FILE__.':'.__LINE__.' => '.$e->getMessage()); // fix me error template
            die(__LINE__);
        }
        // 主催者取得
        $meetingInfo["organizer"] = $this->obj_Reservation->getOne("meeting_key = '" .$meetingInfo["meeting_ticket"]."'", "organizer_name");
        $this->template->assign("clips" , $clips);
        $room_info = $this->get_room_info($meetingInfo["room_key"]);
        $page = $request["page"];
        $page_cnt = $request["page_cnt"];
        // XSS 対策
        if (!$page_cnt || !is_int((int)$page_cnt) || $page_cnt < 1) {
            $page_cnt = 10;
        }
        if (!$page || !is_int((int)$page) || $page < 1) {
            $page = 1;
        }
		$has_password = 0;
		if($meetingInfo['meeting_log_password'] && strlen($meetingInfo['meeting_log_password']) != 0){
			$has_password = 1;
		}
        //部屋情報取得
        $member_info = $this->session->get("member_info");
        if(($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") && $member_info){
	        $obj_room = new N2MY_DB($this->get_dsn(), 'room');
			$where = "room_status = 1 AND user_key = " . $user_info['user_key'];
			$_room_list = $obj_room->getRowsAssoc($where);
			foreach ($_room_list as $_room) {
				$room_list[$_room['room_key']]['room_info'] = $_room;
			}
        }else{
        	$room_list = $this->session->get("room_info");
        }
		$this->template->assign('room_list', $room_list);
		$this->template->assign('has_password', $has_password);
        $this->template->assign("room_info"     , $room_info);
        $this->template->assign("info"     , $meetingInfo);
        $this->template->assign("page"     , $page);
        $this->template->assign("page_cnt" , $page_cnt);
        $this->set_submit_key();
        $this->display('user/meetinglog/detail.t.html');
    }

    /**
     * 共有メモデータDL
     */
    function action_get_memo(){
        require_once("classes/dbi/shared_file.dbi.php");
        $objSharedFile = new sharedFileTable($this->get_dsn());
        $shared_where = sprintf("status = 1 AND shared_file_key='%s'", $this->request->get("shared_file_key"));
        $sharedFile = $objSharedFile->getRow($shared_where);

        $where       = sprintf("meeting_key='%s'", $this->request->get("meeting_key"));
        $meetingInfo = $this->obj_Meeting->getRow($where);
        $user_info = $this->session->get('user_info');
        // エラー処理
        if(!$meetingInfo || $user_info["user_key"] != $meetingInfo["user_key"] || !$sharedFile){
            $this->logger2->warn("meeting_key:".$this->request->get("meeting_key")." NOT MEETING DATA");
            header("Location: index.php");
            return ;
        }
        if(!is_file($sharedFile["file_path"])){
            $this->logger2->warn("shared_file_key:".$this->request->get("shared_file_key")." NOT FOUND FILE");
            header("Location: index.php");
            return ;
        }
        // セッションのタイムゾーンを元に時間を形成
        $time_zone = $this->session->get("time_zone");
        if(!$time_zone){
            $time_zone = 9;
        }
        $time = (EZDate::getLocateTime($sharedFile["create_datetime"], $time_zone, N2MY_SERVER_TIMEZONE));

        $file_name = sprintf( "%s%s%s", "vcmemo_", date("YmdHi" , $time), ".txt" );
        // ファイルダウンロード
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=" . $file_name);
        // 対象ファイルを出力する。
        readfile($sharedFile["file_path"]);
    }

    function action_create_movie() {
        if ($this->check_submit_key()) {
            $this->_create_movie();
        }
        return $this->action_showDetail();
    }

    private function _create_movie() {
        $meeting_session_id   = $this->request->get("meeting_session_id");
        $meeting_sequence_key = $this->request->get("meeting_sequence_key");
        $_delete_meeting_data = $this->request->get("delete_meeting_data");
        $user_info            = $this->session->get("user_info");
        $room_key             = $this->get_room_key();
        // 会議情報
        $where = "meeting_session_id = '".$meeting_session_id."'" .
            " AND room_key = '".addslashes($room_key)."'" .
            " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where, "meeting_key, meeting_name, user_key, room_key, meeting_log_password");
        $this->logger2->info($meeting_info);
        if (!$meeting_info) {
            $this->logger2->warn($where);
            return false;
        }
        //パスワードチェック
        if ( $meeting_info['meeting_log_password'] && strlen( $meeting_info['meeting_log_password']) != 0 ) {
            return false;
        }
        // シーケンス情報
        $where = "meeting_key = '".$meeting_info["meeting_key"]."'" .
            " AND (record_status = '' OR record_status IS NULL OR record_status = 'error')".
            " AND meeting_sequence_key = ".$meeting_sequence_key.
            " AND has_recorded_video = 1";
        $meeting_sequence_info = $this->objMeetingSequence->getRow($where);
        $this->logger2->info($meeting_sequence_info);
        if (!$meeting_sequence_info) {
            $this->logger2->warn($where);
            return false;
        }
        // 容量取得
        $obj_OrderedServiceOption = new OrderedServiceOptionTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'".
            " AND service_option_key = 4" .
            " AND ordered_service_option_status = 1";
        $count = $obj_OrderedServiceOption->numRows($where);
        /*if ($user_info["account_model"] == 'member'){
            $max_rec_size = ($count * 500 * 1024 * 1024);
        } else {
            $max_rec_size = ($count * 1024 * 1024 * 1024) + (RECORD_SIZE * 1024 * 1024);
        }*/
        $max_rec_size = $user_info["max_storage_size"]  * 1024 * 1024;
        // 会議の検索ヒット件数を取得
        require_once("classes/N2MY_Storage.class.php");
        $obj_Storage = new N2MY_Storage($this->get_dsn());
        $total_used_size = $obj_Storage->get_total_size($user_info["user_key"]);
        if ($max_rec_size < $total_used_size) {
            // シーケンス情報変更
            $meeting_sequence_data = array(
                "record_status"     => "error",
                "record_result"     => "E1000",
                "record_end_proc"   => "",
                "record_filepath"   => "",
                );
            $this->logger2->warn(array("max_rec_size" => $max_rec_size, "total_used_size" => $total_used_size), "部屋の容量が足りない");
            $where = "meeting_sequence_key = ".$meeting_sequence_key;
            $result = $this->objMeetingSequence->update($meeting_sequence_data, $where);
            return false;
        }
        // メンテナンス時間を取得
        $fms_server_key = $meeting_sequence_info["server_key"];
        require_once 'classes/mgm/dbi/FmsServer.dbi.php';
        $objFmsServer = new DBI_FmsServer(N2MY_MDB_DSN);
        $where = "server_key = '".$fms_server_key."'";
        $fms_server_info = $objFmsServer->getRow($where);
        if ($fms_server_info["maintenance_time"]) {
            $disable_starttime = $fms_server_info["maintenance_time"];
            $disable_duration = 60 * 10;
        } else {
            $disable_starttime = "";
            $disable_duration = "";
        }
        $data = $meeting_info["meeting_key"].
            ",log_video" .
            "," .$this->_lang.
            ",".$meeting_sequence_info["meeting_sequence_key"];
        $room_info = $this->get_room_info($room_key);
        if ("wmv" == $room_info["room_info"]["rec_gw_convert_type"]) {
            $output_type = "0";
        } else {
            $output_type = "2";
        }
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $data);
        $encrypted_data = str_replace("/", "-", $encrypted_data);
        $encrypted_data = str_replace("+", "_", $encrypted_data);
        $ondemand_url = N2MY_LOCAL_URL."rec_gw/".$encrypted_data;
        $callback_url = N2MY_LOCAL_URL."services/api.php?action_record_callback";
        $url = $this->config->get("RECORD_GW", "url")."api/receive.php";
        $user_id = $user_info["user_id"];
        $path = $user_id."/".$meeting_info["room_key"]."/";
        $file = $meeting_session_id."_".$meeting_sequence_key.".".$room_info["room_info"]["rec_gw_convert_type"];
        // umaskを設定し元に戻す
        $old = umask(0);
        @mkdir(N2MY_MOVIES_DIR.$path, 0777, true);
        umask($old);
        $ch = curl_init();
        $post_data = array(
            "key"               => $meeting_session_id."_".$meeting_sequence_info["meeting_sequence_key"],
            "play_url"          => $ondemand_url,
            "output_type"       => $output_type,       // movファイル
            "duration"          => $meeting_sequence_info["record_second"],
            "callback_url"      => $callback_url,
            "output_fqdn"       => $_SERVER["SERVER_NAME"],
            "scp_user"          => $this->config->get("RECORD_GW", "user"),
            "output_file"       => N2MY_MOVIES_DIR.$path.$file,
            "disable_starttime" => $disable_starttime,
            "disable_duration"  => $disable_duration,
            );
        $option = array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 1,
            CURLOPT_TIMEOUT        => 10,
            CURLOPT_POST           => 10,
            CURLOPT_POSTFIELDS     => $post_data,
            );
        $this->logger2->info($option);
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        $xml = new EZXML();
        $data = $xml->openXML($ret);
        if (!$data) {
            $this->logger2->info($ret, "XML DATA parse error!!");
            return false;
        }
        if ($data["response"]["status"][0]["_data"]) {
            $where                  = "meeting_sequence_key = ".$meeting_sequence_key;
            $record_id              = $data["response"]["queue_key"][0]["_data"];
            $record_endtime         = $data["response"]["record_end"][0]["_data"];
            $record_end_proc        = ($_delete_meeting_data[$meeting_sequence_key] == "1") ? "delete" : "";
            $meeting_sequence_data  = array("record_id"       => $record_id,
                                            "record_status"   => "convert",
                                            "record_endtime"  => $record_endtime,
                                            "record_end_proc" => $record_end_proc,
                                            "record_filepath" => $path.$file,);
            $this->logger2->info(array($meeting_sequence_data, $where));
            $result = $this->objMeetingSequence->update($meeting_sequence_data, $where);
            $this->add_operation_log('meetinglog_create_movie', array(
                'room_key'      => $meeting_info['room_key'],
                'meeting_name'  => $meeting_info['meeting_name']
                ));
            if (DB::isError($result)) {
                $this->logger2->error($result->getUserInfo());
                return false;
            }
            require_once 'classes/mgm/MGM_Auth.class.php';
            $objMgmRelationTable = new MGM_AuthClass(N2MY_MDB_DSN);
            $this->logger2->info(array($user_id, $record_id, "record_gw"));
            if (!$result = $objMgmRelationTable->addRelationData($user_id, $record_id, "record_gw")) {
                return false;
            } else {
                $this->logger2->info(array($option, $meeting_sequence_data));
                file_get_contents($this->config->get("RECORD_GW", "url")."api/dispach.php");
                return true;
            }
        } else {
            $err_cd = $data["response"]["error"][0]["code"][0]["_data"];
            $err_msg = $data["response"]["error"][0]["message"][0]["_data"];
            // シーケンス情報変更
            $meeting_sequence_data = array(
                "record_status"   => "error",
                "record_result"   => $err_cd,
                "record_end_proc" => "",
                "record_filepath" => "",
                );
            $this->logger2->warn(array($err_cd, $err_msg));
            $where  = "meeting_sequence_key = ".$meeting_sequence_key;
            $result = $this->objMeetingSequence->update($meeting_sequence_data, $where);
            if (DB::isError($result)) {
                $this->logger2->error($result->getUserInfo());
                return false;
            }
            return false;
        }
    }

    /**
     * キャンセル処理
     */
    function action_cancel_movie() {
        if ($this->check_submit_key()) {
            $this->_cancel_movie();
        }
        return $this->action_showDetail();
    }

    function _cancel_movie() {
        $meeting_session_id   = $this->request->get("meeting_session_id");
        $meeting_sequence_key = $this->request->get("meeting_sequence_key");
        // 会議情報
        $where = "meeting_session_id = '".$meeting_session_id."'" .
            " AND room_key = '".addslashes($this->get_room_key())."'" .
            " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where, "meeting_key, user_key, room_key");
        $this->logger2->info($meeting_info);
        if (!$meeting_info) {
            $this->logger2->warn($where);
            return false;
        }
        // シーケンス情報
        $where = "meeting_key = '".$meeting_info["meeting_key"]."'" .
            " AND meeting_sequence_key = ".$meeting_sequence_key.
            " AND record_status = 'convert'".
            " AND has_recorded_video = 1";
        $meeting_sequence_info = $this->objMeetingSequence->getRow($where);
        $this->logger2->info($meeting_sequence_info);
        if (!$meeting_sequence_info) {
            $this->logger2->warn($where);
            return false;
        }
        // キャンセル処理
        file_get_contents($this->config->get("RECORD_GW", "url")."api/cancel.php?queue_key=".$meeting_sequence_info["record_id"]);
        $where = "meeting_sequence_key = ".$meeting_sequence_key;
        $meeting_sequence_data = array(
            "record_id"       => "",
            "record_endtime"  => "",
            "record_status"   => "",
            "record_result"   => "",
            "record_end_proc" => "",
            "record_filepath" => "",
            );
        $this->logger2->info(array($meeting_sequence_data, $where));
        $result = $this->objMeetingSequence->update($meeting_sequence_data, $where);
        if (DB::isError($result)) {
            $this->logger2->error($result->getUserInfo());
            return false;
        }
        // ワンタイムURL破棄
        require_once 'classes/mgm/MGM_Auth.class.php';
        $objMgmRelationTable = new MGM_AuthClass(N2MY_MDB_DSN);
        $objMgmRelationTable->deleteRelationData($meeting_sequence_info["record_id"], "record_gw");
        return true;
    }

    /**
     * 削除
     */
    function action_delete_movie() {
        if ($this->check_submit_key()) {
            $this->_delete_movie();
        }
        return $this->action_showDetail();
    }

    function _delete_movie() {
        $meeting_session_id   = $this->request->get("meeting_session_id");
        $meeting_sequence_key = $this->request->get("meeting_sequence_key");
        // 会議情報
        $where = "meeting_session_id = '".$meeting_session_id."'" .
            " AND room_key = '".addslashes($this->get_room_key())."'" .
            " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where, "meeting_key, user_key, room_key");
        $this->logger2->info($meeting_info);
        if (!$meeting_info) {
            $this->logger2->warn($where);
            return false;
        }
        // シーケンス情報
        $where = "meeting_key = '".$meeting_info["meeting_key"]."'" .
            " AND meeting_sequence_key = ".$meeting_sequence_key.
            " AND record_status = 'complete'";
        $meeting_sequence_info = $this->objMeetingSequence->getRow($where);
        $this->logger2->info($meeting_sequence_info);
        if (!$meeting_sequence_info) {
            $this->logger2->warn($where);
            return false;
        }
        $where = "meeting_sequence_key = ".$meeting_sequence_key;
        $meeting_sequence_data = array(
            "record_id"         => "",
            "record_endtime"    => "",
            "record_status"     => "",
            "record_result"     => "",
            "record_end_proc"   => "",
            "record_filepath"   => "",
            );
        $this->logger2->info(array($meeting_sequence_data, $where));
        $result = $this->objMeetingSequence->update($meeting_sequence_data, $where);
        if (DB::isError($result)) {
            $this->logger2->error($result->getUserInfo());
            return false;
        }
        $this->logger2->info(N2MY_MOVIES_DIR.$meeting_sequence_info["record_filepath"], "Delete movie file");
        unlink(N2MY_MOVIES_DIR.$meeting_sequence_info["record_filepath"]);
        $meetingLog = new N2MY_MeetingLog($this->get_dsn());
        $meetingLog->getMeetingSize($meeting_session_id);
        return true;
    }

    /**
     * 変換されたファイルをダウンロード
     */
    function action_download_movie() {
        $meeting_session_id    = $this->request->get("meeting_session_id");
        $meeting_sequence_key  = $this->request->get("meeting_sequence_key");
        $room_key              = $this->request->get("room_key");
        $sequence_no           = $this->request->get("no");
        $useragent = getenv("HTTP_USER_AGENT");
        // 会議情報
        $where = "meeting_session_id = '".$meeting_session_id."'" .
            " AND room_key = '".addslashes($this->get_room_key())."'" .
            " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where, "meeting_key, meeting_name, user_key, room_key");
        $this->logger2->info($meeting_info);
        if (!$meeting_info) {
            $this->logger2->warn($where);
            $this->display("user/404_error.t.html");
            return false;
        }
        // シーケンス情報
        $where = "meeting_key = '".$meeting_info["meeting_key"]."'" .
            " AND meeting_sequence_key = ".$meeting_sequence_key.
            " AND record_status = 'complete'";
        $meeting_sequence_info = $this->objMeetingSequence->getRow($where);
        $this->logger2->info($meeting_sequence_info);
        if (!$meeting_sequence_info) {
            $this->logger2->warn($where);
            $this->display("user/404_error.t.html");
            return false;
        }
        $file = N2MY_MOVIES_DIR.$meeting_sequence_info["record_filepath"];
        if (!file_exists($file)) {
            $this->logger2->warn($file, "File not exists");
            $this->display("user/404_error.t.html");
            return false;
        }
        $extension = end(explode('.', $meeting_sequence_info["record_filepath"]));
        if ($meeting_info["meeting_name"]) {
            $file_name = $meeting_info["meeting_name"]."_".$sequence_no.".".$extension;
        } else {
            $file_name = "no_title_".$sequence_no.".".$extension;
        }
        if ($this -> is_browser_IE($useragent)) {
            $file_name = mb_convert_encoding( $file_name , "SJIS-win" );
            Header('Pragma: private');
            Header('Cache-Control: private');
/*
        } elseif (ereg( "Safari", $useragent)) {
            //Safariの場合は全角文字が全て化けるので、何かしら固定のファイル名にして回避
            $file_name = "";
*/
        }
        $this->logger2->info($meeting_sequence_info);
        header("Content-Disposition: attachment; filename=" . $file_name );
        header("Content-Type: application/octet-stream; name=" . $file_name );
        header("Content-Length: " . filesize($file) );
        ob_end_flush();
        $fp = fopen($file, "r");
        while($str = fread($fp, 4096)) {
            print $str;
        }
        fclose($fp);
    }

    function action_download_vote_csv() {
    	require_once("classes/dbi/quick_vote_log.dbi.php");
    	$obj_quickVoteLog = new QuickVoteLogTable($this->get_dsn($server_dsn_key));
    	$where = "meeting_key = ".$this->request->get("meeting_key");
    	//get actual_start_time
    	$actual_start_datetime = $this->obj_Meeting->getOne($where, "actual_start_datetime");
    	$results = $obj_quickVoteLog->getRowsAssoc($where);
    	$csv_data = array();
    	foreach($results as $result) {
    		$csv_data[$result["number"]][$result["result"]]++;
    	}

    	// CSV出力
    	require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
    	$output_encoding = $this->get_output_encoding();
    	$csv = new EZCsv(true,$output_encoding,"UTF-8");
    	$dir = $this->get_work_dir();
    	$tmpfile = tempnam($dir, "csv_");
    	$csv->open($tmpfile, "w");

    	header('Content-Type: application/octet-stream;');
    	header('Content-Disposition: attachment; filename="vote_result_'.$actual_start_datetime.'.csv"');

    	// ヘッダ
    	$header = array(
    		"num"   => "num",
    		"1"     => "1",
    		"2"     => "2",
    		"3"     => "3",
    		"4"     => "4",
    		"5"     => "5",
    		"Yes"   => "Yes",
    		"No"    => "No",
    	);
    	$csv->setHeader($header);
    	$csv->write($header);

    	foreach($csv_data as $num => $result){
    		$line["num"] = $num;
    		$line["1"]   = $result["1"]?$result["1"]:"0";
    		$line["2"]   = $result["2"]?$result["2"]:"0";
    		$line["3"]   = $result["3"]?$result["3"]:"0";
    		$line["4"]   = $result["4"]?$result["4"]:"0";
    		$line["5"]   = $result["5"]?$result["5"]:"0";
    		$line["Yes"] = $result["Yes"]?$result["Yes"]:"0";
    		$line["No"]  = $result["No"]?$result["No"]:"0";
    		$this->logger2->info($line);
    		$csv->write($line);
    	}
    	// CSV出力
    	$csv->close();
    	$fp = fopen($tmpfile, "r");
    	$contents = fread($fp, filesize($tmpfile));
    	fclose($fp);

    	print $contents;
    	unlink($tmpfile);
    }

    private function isRecorded($meeting_session_id) {
        $obj_Meeting           = new DBI_Meeting( $this->get_dsn() );
        $obj_MeetingSequence   = new DBI_MeetingSequence( $this->get_dsn() );
        $where                 = "meeting_session_id = '".addslashes($meeting_session_id)."'";
        $meeting_key           = $obj_Meeting->getOne($where, "meeting_key");
        $where                 = "meeting_key = ".$meeting_key;
        $meeting_sequence_list = $obj_MeetingSequence->getRowsAssoc($where);
        $this->logger2->info($meeting_sequence_list);
        foreach ($meeting_sequence_list as $meeting_sequence_info) {
            if ($meeting_sequence_info["record_status"] == "convert") {
                return true;
            }
        }
        return false;
    }

	/**
	 * GET USER_AGENT INFO
	 *
	 * eg:
	 * IE 11: Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv 11.0) like Gecko
	 * IE 10: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)
	 */
	private function is_browser_IE($user_agent){
		//IE 11
		$ie_11_reg = '/Trident.*rv[ :]*11\./';
		if(preg_match($ie_11_reg, $user_agent)){
			return true;
		}
		//IE 5-10
		$ie_other_version_reg = '/msie/i';
	    if(preg_match($ie_other_version_reg, $user_agent)){
			return true;
		}
		return false;
	}
}

$main = new AppMeetingLog();
$main->execute();
