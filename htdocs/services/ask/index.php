<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");
require_once('config/config.inc.php');

class AppAsk extends AppFrame
{

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->user_auth = "invite";
        $this->checkAuth();
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->action_ask();
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * フォームの表示
     */
    function action_ask($message="") {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        // エラー表示
        if($message){
            $this->template->assign('message', $message);
        }

        // 入力データ再表示
        if($ask_info = $this->session->get('user_ask_info')){
            foreach($ask_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }

        // 入力画面表示
        $this->display('user/ask/index.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
    /**
     * 確認処理
     */
    function action_ask_confirm(){
        $this->set_submit_key($this->_name_space);
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $request = $this->request->getAll();
        $this->session->set('user_ask_info', $request);

        $message = "";
        if(!$request['ask_contents']){
            $message .= "<li>".USER_ASK_ERROR_CONTENTS ."</li>";
        }
        if(!$request['ask_name']){
            $message .= "<li>".USER_ASK_ERROR_NAME ."</li>";
        }
        if(!$request['ask_email']){
            $message .= "<li>".USER_ASK_ERROR_EMAIL ."</li>";
        }elseif( ! EZValidator::valid_email( $request['ask_email'] ) ){
            $message .= "<li>".USER_ASK_ERROR_INVALIDEMAIL ."</li>";
        }
        // 入力チェック
        if($message != ""){
            $this->action_ask($message);
        }else{
            // 入力データ表示
            foreach($request as $key => $value){
                $this->template->assign($key, $value);
            }
            // 確認画面表示
            $this->display('user/ask/confirm.t.html');
        }

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
    /**
     * 完了処理
     */
    function action_ask_complete(){
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_ask();
        }
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        if($ask_info = $this->session->get('user_ask_info')){
            foreach($ask_info as $key => $value){
                $this->template->assign($key, $value);
            }
            require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
            $user_info = $this->session->get('user_info');
            $this->template->assign('user_id', $user_info['user_id']);
            $this->template->assign('n2my_address', $_SERVER['HTTP_HOST']);
            //v-cube
            $mail = new EZSmtp(null, "", "UTF-8");
            $mail->setTo(USER_ASK_TO);
            $mail->setFrom(USER_ASK_FROM);
            $mail->setReturnPath(NOREPLY_ADDRESS);
            $service_mode = $this -> session -> get('service_mode');
            if($service_mode == 'sales'){
                $mail->setSubject(USER_ASK_SUBJECT_SALES);
            }else{
                $mail->setSubject(USER_ASK_SUBJECT);
            }
            $login_id = $user_info['user_id'];
            $member_info = $this->session->get('member_info');
            if($member_info){
                $login_id = $member_info['member_id'];
            }
            $this->template->assign('login_id', $login_id);
            $body = $this->fetch("0");
            $mail->setBody($body);
            $mail->send();
            // user
            $mail = new EZSmtp(null, $this->_lang, "UTF-8");
            $frame["lang"] = $this->_lang;
            $this->template->assign("__frame",$frame);
            $body = $this->fetch("1");
            $mail->setSubject(ASK_SUBJECT_FOR_USER);
            $mail->setTo($ask_info['ask_email']);
            $mail->setFrom(USER_ASK_FROM);
            $mail->setReturnPath(NOREPLY_ADDRESS);
            $mail->setBody($body);
            $mail->send();
            $this->session->remove('user_ask_info');
        }
        // 完了画面表示
        $this->display('user/ask/complete.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
        override
        $flag = 1, userに送信
        $flag = 0, adminに送信
    **
    */
    function fetch($flag) {
        if($flag){
            $custom  = $this->config->get('SMARTY_DIR','custom');
            $lang_cd = $this->_lang;
            require_once ("lib/EZLib/EZHTML/EZTemplate.class.php");
            $template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
            if($this->session->get('service_mode') == "sales"){
                $template_file = "custom/sales/common/multi_lang/ask/ask_user_vcube.t.txt";
            }else{
                $template_file = "common/mail_template/meeting/ask/ask_user_vcube.t.txt";
            }
            if($custom){
                $custom_template_file = "custom/" . $custom . "/common/multi_lang/ask/ask_user_vcube.t.txt";
                if (file_exists($template->template_dir.$custom_template_file)){
                    $template_file = $custom_template_file;
                }else{
                    $custom_template_file = "custom/" . $custom. "/" . $lang_cd."/mail/ask/ask_user_vcube.t.txt";
                    if (file_exists($template->template_dir.$custom_template_file)) {
                        $template_file = $custom_template_file;
                    }
                }
            }
        }else{
            $template_file = "common/mail/user_ask.m.txt";
        }
        return parent::fetch($template_file);
    }
}

$main =& new AppAsk();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
