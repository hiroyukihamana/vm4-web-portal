<?php
require_once("config/config.inc.php");
require_once("classes/AppFrame.class.php");

class AppFileDownload extends AppFrame
{
    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
    }

    function default_view() {
        require_once("classes/N2MY_FileUpload.class.php");
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/dbi/file_cabinet.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");

        $objFileUpload  = new N2MY_FileUpload();
        $objMeeting     = new MeetingTable($this->get_dsn());
        $objFileCabinet = new FileCabinetTable($this->get_dsn());
        $objParticipant = new DBI_Participant($this->get_dsn());

        $meeting_session_id = $this->request->get('meeting_key');
        $cabinet_id         = $this->request->get('cabinet_id');
        $participant_id     = session_id();
        // ファイル出力
        $meeting_info   = $objMeeting->getRow("meeting_session_id='".addslashes($meeting_session_id)."'");
        $dir            = $objFileUpload->getDir( $meeting_info['meeting_key'] );
        $destDir        = $objFileUpload->getFullPath( $dir, $cabinet_id );
        if (file_exists($destDir)) {
            header("Content-Type: application/octet-stream");
            header("Content-Length: ".filesize($destDir));
            readfile($destDir);
            // 操作ログ
            $participant_name = $objParticipant->getOne("meeting_key = '".$meeting_info['meeting_key']."' AND participant_session_id = '".$participant_id."'", 'participant_name');
            $file_name        = $objFileCabinet->getOne("meeting_key = '".$meeting_info['meeting_key']."' AND tmp_name = '".$cabinet_id."'", 'file_name');
            $this->add_operation_log('cabinet_download', array(
                'room_key'          => $meeting_info['room_key'],
                'meeting_name'      => $meeting_info['meeting_name'],
                'particpant_name'   => $participant_name,
                'file_name'         => $file_name,
                ));
        }
    }
}
$main = new AppFileDownload();
$main->execute();