<?php

require_once("config/config.inc.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_FileUpload.class.php");
require_once("lib/EZLib/EZUtil/EZFile.class.php");

class AppCheckFile extends AppFrame
{
    var $logger = null;

    function init()
    {
    }

    public function default_view()
    {
        $request = $_REQUEST;
        $this->logger2->debug( $request );
        // 会議キーに変換
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass(N2MY_MDB_DSN);
        $meeting_key = $obj_MGMClass->getMeetingID($request["meeting_key"]);

        $objDocument = new N2MY_FileUpload();
        $dir = $objDocument->getDir( $meeting_key );
        $destDir = $objDocument->getFullPath( $dir );
        print $objDocument->checkFile( $destDir, $request["name"] ) ? 1 : 0;
    }
}

$main = new AppCheckFile();
$main->execute();
