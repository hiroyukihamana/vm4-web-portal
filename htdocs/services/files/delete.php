<?php
require_once("config/config.inc.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_FileUpload.class.php");
require_once("lib/EZLib/EZUtil/EZFile.class.php");

class AppFileDelete extends AppFrame
{
    var $logger = null;
    var $meetingLog = null;

    function init()
    {
        $this->objFileUpload = new N2MY_FileUpload();
    }

    public function default_view()
    {
        $this->logger2->debug($_REQUEST);
        $request = $_REQUEST;
        // 会議キーに変換
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass(N2MY_MDB_DSN);
        $meeting_key = $obj_MGMClass->getMeetingID($request["meeting_key"]);

        $dir = $this->objFileUpload->getDir( $meeting_key );
        $destDir = $this->objFileUpload->getFullPath( $dir );

        if( false !== $destDir && $request["name"] ){
            $destFile = sprintf( "%s%s", $destDir, $request["name"] );
            $this->checkFile($destDir, $request["name"]);
            $this->objFileUpload->deleteDir( $destFile );
            require_once( "classes/dbi/file_cabinet.dbi.php" );
            $serverList = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true );
            list( $key, $dsn ) = each( $serverList["SERVER_LIST"] );
            $objFileCabinet = new FileCabinetTable( $dsn );
            $data = array(
                "status"        => 0,
                "updatetime"    => date( "Y-m-d H:i:s" )
                );
            $where = sprintf(
                "meeting_key='%s' AND tmp_name='%s' AND status = 1",
                addslashes($meeting_key),
                addslashes($request["name"])
                );
            $objFileCabinet->update( $data, $where );
            print 1;
        } else {
            $this->render_400();
        }
    }

    private function checkFile($destDir, $name)
    {
        if (! $this->objFileUpload->checkFile($destDir, $name)) $this->render_400();
    }

    private function render_400()
    {
        $this->logger2->info($_REQUEST);
        print 0;
        exit;
    }
}

$main = new AppFileDelete();
$main->execute();
