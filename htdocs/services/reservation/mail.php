<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once ("classes/AppFrame.class.php");
require_once ("classes/N2MY_Reservation.class.php");
require_once ("classes/mgm/MGM_Auth.class.php");
require_once ('lib/EZLib/EZUtil/EZDate.class.php');
require_once ("lib/EZLib/EZUtil/EZString.class.php");
require_once ("classes/dbi/reservation_user.dbi.php");

class AppReservationMail extends AppFrame {

    var $_name_space = null;
    var $_reservation = null;
    var $_ssl_mode = null;
    var $_reservation_obj = null;
    private $pgi_wait_sec_max   = 20;
    private $pgi_wait_sec_sleep = 2;
    private $n2myResevation     = null;

    /**
     * 初期処理
     */
    function init() {
        $this->_name_space = md5(__FILE__);
        $this->n2myResevation = new N2MY_Reservation($this->get_dsn());
    }

    /**
     * 認証画面
     */
    function action_send_mail()
    {

        $request_mail_info = $this->request->get('mail_info');
        if (!$request_mail_info) {
            $this->logger->info('mail_info is empty');
            exit;
        }

        $mail_info = unserialize($request_mail_info);
        $this->logger->debug('mail_info : '.print_r($mail_info,true));
        if (!$mail_info) {
            $this->logger->info('mail_info unserialize failed');
            exit;
        }

        $this->logger->info('action_send_mail end');

        if (!isset($mail_info['reservation_key']) || !isset($mail_info['type'])
         || !isset($mail_info['info'])) {
            $this->logger->info('parameter required '.print_r($mail_info, true));
            exit;
        }
        require_once ("classes/dbi/ordered_service_option.dbi.php");
        $service_option  = new OrderedServiceOptionTable($this->get_dsn());
        $enable_teleconf = $service_option->enableOnRoom(23, $mail_info["info"]["room_key"]); // 23 = teleconf
        if (!$this->config->get('IGNORE_MENU', 'teleconference') && $enable_teleconf) {
            $meeting = $this->findMeetingForPGi($mail_info['reservation_key']);
        } else {
            $meeting = null;
        }
        //$this->logger2->info($mail_info['guest']);
        // send mail to participants
        if (0 < count($mail_info['guest'])) {
            foreach ($mail_info['guest'] as $type => $guests) {
                foreach ($guests as $guest) {
                    if($guest){
                        if($guest["r_organizer_flg"]){
                            $mail_info['info']["organizer"] = $guest;
                        }
                        $this->n2myResevation->sendMailToParticipant($guest, $mail_info['info'], $type, $mail_info["account_model"]);
                    }
                }
            }
        }
        // send mail to owner
        if ($mail_info['owner']['is_send']) {
            $add_list = array();
            if (@$mail_info['guest']['create']) {
                $add_list = array_merge($add_list, $mail_info['guest']['create']);
            }
            if (@$mail_info['guest']['modify']) {
                $add_list = array_merge($add_list, $mail_info['guest']['modify']);

            }
            $mail_info['info']['reservation_key'] = $mail_info['reservation_key'];
            $this->n2myResevation->sendMailToOwner($mail_info['info'], $mail_info['type'],
                                                   $mail_info['owner']['lang'], $add_list, $meeting);
        }
        exit;
    }
    //{{{ findMeetingForPGi
    private function findMeetingForPGi($reservation_key)
    {
        $reserve_table = new ReservationTable($this->get_dsn());
        $where         = "reservation_key='".mysql_real_escape_string($reservation_key)."'";

        $reservation   = $reserve_table->getRow($where);
        if (!$reservation) {
            $this->logger->error('reservation not found');
            return false;
        }

        $meeting_dbi = new DBI_Meeting($this->get_dsn());
        $where       = "meeting_ticket='".mysql_real_escape_string($reservation['meeting_key'])."'";

        $wait_sec = 0;
        while ($wait_sec <= $this->pgi_wait_sec_max) {
            $meeting  = $meeting_dbi->getRow($where);
            if (!$meeting) {
                $this->logger->error('meeting not found meeting_ticket:'.$meeting_key);
                return false;
            }
            if ($meeting['pgi_conference_id']) {
                $this->logger->info('pgi found  wait_sec:'.
                                    $wait_sec." pgi_conference_id : ".$meeting['pgi_conference_id']);
                break;
            }
            sleep($this->pgi_wait_sec_sleep);
            $wait_sec += $this->pgi_wait_sec_sleep;
        }

        return $meeting;

    }
    //}}}
}

$main = new AppReservationMail();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
