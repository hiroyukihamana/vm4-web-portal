<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once('classes/AppFrame.class.php');
require_once ('classes/vcubeid/vcubeIdCore.class.php');

class AppAMF extends AppFrame {
/**
 * V-cube ID認証用
 **/
    private $obj_Auth = null;
    var $obj_vcubeId = null;
    var $serverName = null;
    var $consumerKey = null;
    var $wsseId = null;
    var $wssePw = null;
    var $domain = null;
    var $isSSL = null;
    var $isSha1 = null;
    function init() {
        $serverName = $_SERVER["SERVER_NAME"];
        if ($serverName == $this->config->get('VCUBE_ONE','meeting_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
        } else if($serverName == $this->config->get('VCUBE_ONE','document_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_document_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_doc_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_doc_wssePw');
        } else if($serverName == $this->config->get('VCUBE_ONE','sales_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_sales_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_sls_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_sls_wssePw');
        }else{
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
        }
        $this->domain = $this->config->get('VCUBE_ONE','vcubeid_domain');
        $this->isSSL = $this->config->get('VCUBE_ONE','vcube_isSSL') ? $this->config->get('VCUBE_ONE','isSSL') : false;
        $this->isSha1 = $this->config->get('VCUBE_ONE','vcube_isSha1') ? $this->config->get('VCUBE_ONE','isSha1') : false;
        $this->obj_vcubeId = new vcubeIdCore($this->domain, $this->wsseId, $this->wssePw, $this->isSSL, $this->isSha1);
    }
    function getToken($id , $pw){
        $vcubeid = $this->obj_vcubeId;
        $token = $vcubeid->getToken($this->consumerKey, $id, $pw);
        return $token;
    }
    function getMemberId($vIdAuthToken , $vcubeId){
        $vcubeid = $this -> obj_vcubeId;
        // ステータス確認
        $response_id = $vcubeid -> ssologin($this -> consumerKey, $vIdAuthToken, $vcubeId);
        $contracts = $response_id['contracts']['contract'];
        if(!array_key_exists(0, $contracts)){
            $tmp = $contracts;
            unset($contracts);
            $contracts[] = $tmp;
        }
        $_contracts = array();
        foreach ($contracts as $contract){
            $xml = (array)simplexml_load_string($contract['settintXml']);
            $xml_role = (array)$xml['role'];
            $_contracts[] = array(
                    'user_id'      => $contract['meeting']['userId'],
                    'member_id'    => $contract['meeting']['memberId'],
                    'company_name' => $contract['name'],
                    'role'         => $xml_role['permission'],
                    'is_portal_header' => ( $contract['one']['header'] === "true" ),
                    'contract_id' => $contract['id'],
            );
        }
        return array(
                'result'    => $response_id['result'],
                'contracts' => $_contracts,
        );
    }
    function validateVcubeId($user_id, $user_pw){
        if(!$user_id || !$user_pw){
            return false;
        }
        $token = $this -> getToken($user_id, $user_pw);
        if(!$token['result']){
            return false;
        }
        $res = $this -> getMemberId($token["vIdAuthToken"] , $user_id);
        if(!$res['result']){
            return false;
        }
        $res['vIdAuthToken'] = $token['vIdAuthToken'];
        return $res;
    }
/**
 * V-cube ID認証用
 **/

    function default_view() {
        return $this->_formatXml("101", "Method not found");
    }

    /**
     * 録画可能か
     */
    public function action_get_recordable_size() {
        $server_dsn_key = $this->request->get("db_host");
        $meeting_session_id = $this->request->get("meeting_key");
        $meeting_key = $this->getMeetingKey($meeting_session_id);
        $this->logger2->debug($meeting_key);
        if ( !$server_dsn_key || !$meeting_key) {
                $this->logger2->warn($this->request->getAll(), "Invalid parameter");
            return $this->_formatXml("102", "Invalid parameter");
        } else {
            require_once("classes/core/dbi/Meeting.dbi.php");
            require_once("classes/mgm/dbi/FmsServer.dbi.php");
            require_once("classes/dbi/user.dbi.php");
            require_once("classes/dbi/room.dbi.php");
            $dsn = $this->get_dsn_value($server_dsn_key);
            $obj_Meeting = new DBI_Meeting( $dsn );
            $obj_Room    = new RoomTable( $dsn );
            $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", $meeting_key ) );
            $where = "room_key = '".addslashes($meeting_info["room_key"])."'";
            $room_info = $obj_Room->getRow($where);
            $this->logger2->debug($room_info);
            if( !$room_info["user_key"] ){
                $this->logger2->warn($this->request->getAll(), "Meeting not found");
                return $this->_formatXml("103", "Object not found");
            } else {
                $obj_User = new UserTable( $dsn );
                $user_info = $obj_User->getRow( sprintf( "user_key='%s' AND user_delete_status != 2", $room_info["user_key"] ) );
            }
            if (!$user_info) {
                $this->logger2->warn($this->request->getAll(), "User not found");
                return $this->_formatXml("103", "Object not found");
            }
            if ($meeting_info) {
                require_once("classes/N2MY_Storage.class.php");
                $obj_Storage = new N2MY_Storage($dsn);
                $total_used_size = $obj_Storage->get_total_size($user_info["user_key"]);
                $remaining_size = ($meeting_info["meeting_size_recordable"] - $total_used_size);
                $this->logger2->info(array("meeting_key" => $meeting_info["meeting_key"], "total_used_size" => $total_used_size,"remaining_size" => $remaining_size));
            }
            return $this->_formatXml(0, "", array("value" => $remaining_size));
        }
    }

    /**
     * ストレージサイズに空きがあるかの確認
     */
    public function action_getStorageRemainSize(){
        require_once("classes/core/dbi/Participant.dbi.php");
        require_once("classes/core/dbi/Meeting.dbi.php");
        // リクエスト取得
        $server_dsn_key = $this->request->get("server_dsn_key");
        $dsn = $this->get_dsn($server_dsn_key);
        $participant_id = $this->request->get('participant_id');
        $member_flag = $this->request->get('member_flag');
        // 参加者情報取得
        $obj_Participant    = new DBI_Participant($dsn);
        $participant_info = $obj_Participant->getRow(sprintf( "participant_key='%s'", mysql_real_escape_string($participant_id )));
        $storage_login_id = $participant_info["storage_login_id"];

        $member_key = 0;
        $user_key = 0;
        require_once("classes/dbi/user.dbi.php");
        $obj_user = new UserTable($dsn);
        // メンバー、ユーザー情報取得
        if($member_flag == 1){
            require_once("classes/dbi/member.dbi.php");
            $obj_member = new MemberTable($dsn);
            $login_info = $obj_member->getRow(sprintf( "member_id='%s' AND member_status = 0", mysql_real_escape_string($storage_login_id )));
            $member_key = $login_info["member_key"];
            $user_key = $login_info["user_key"];
            $user_info = $obj_user->getRow( "user_key=" . $user_key);
        }else{
            $user_info = $obj_user->getRow(sprintf( "user_id='%s'", mysql_real_escape_string($storage_login_id )));
            $user_key = $user_info["user_key"];
        }
        if (!$user_info) {
            $this->logger2->warn($this->request->getAll(), "User not found");
            return $this->_formatXml("103", "Object not found");
        }
        require_once("classes/N2MY_Storage.class.php");
        $obj_Storage = new N2MY_Storage($dsn);
        $total_used_size = $obj_Storage->get_total_size($user_info["user_key"]);
        $max_storage_size =  $user_info["max_storage_size"] * 1024 * 1024;
        $remain_size = $max_storage_size - $total_used_size;
        return $this->_formatXml(0, "", array("value" => $remain_size));

    }
    /*
     * ファイルダウンロード用
     */
    public function action_echoBack(){
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/N2MY_Sharing.class.php");
        require_once("classes/dbi/shared_file.dbi.php");
        // リクエスト取得
        $server_dsn_key = $this->request->get("server_dsn_key");
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_Meeting = new DBI_Meeting( $dsn );
        $objSharedFile = new SharedFileTable($dsn);
        $meeting_session_id = $this->request->get("meeting_key");
        $meeting_key = $this->getMeetingKey($meeting_session_id);
        $memoData = $_REQUEST["memoData"];
        // bese64で送られてくるはず
        $binary_data = $_REQUEST["data"];
        $content_type = $_REQUEST["contentType"];

        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", mysql_real_escape_string($meeting_key )));
        if(!$meeting_info){
            $this->logger2->warn(__FUNCTION__,__FILE__,__LINE__,array($server_dsn_key, $meeting_key, $mailAddress, $locale));
            $result = (string) 0;
        }

        if (!$server_dsn_key || !$meeting_key) {
            $this->logger2->warn(__FUNCTION__,__FILE__,__LINE__,array($server_dsn_key, $meeting_key, $mailAddress, $locale));
            $result = (string) 0;
        } else {
            if($binary_data){
                $result = base64_decode($binary_data);
            }else{
                // 改行コードを\r\nに統一
                $memoData = str_replace("\r\n","\n",$memoData);
                $memoData = str_replace("\r","\n",$memoData);
                $memoData = str_replace("\n","\r\n",$memoData);
                // メモファイルを返す
                $result = $memoData;
            }
        }
        // ヘッダー書き換え
        if($content_type){
            header('Content-type: ' . $content_type);
        }
        echo($result);
        return ;
    }

    /*
     * 共有メモファイルをダウンロードする TODO 使わなくなるかも
     */
    public function action_dlSharedMemo(){
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/N2MY_Sharing.class.php");
        require_once("classes/dbi/shared_file.dbi.php");
        $server_dsn_key = $this->request->get("server_dsn_key");
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_Meeting = new DBI_Meeting( $dsn );
        $objSharedFile = new SharedFileTable($dsn);
        $meeting_key = $this->request->get("meeting_key");
        $shared_file_key =  $this->request->get("shared_file_key");
//        require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
//        $file_name = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $this->request->get("file"));

        // ファイル情報取得
        $shared_file_info = $objSharedFile->getRow(sprintf("shared_file_key = '%s'", mysql_real_escape_string($shared_file_key )));
        $file_name = $shared_file_info["file_name"];
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", mysql_real_escape_string($meeting_key )));
        $this->logger2->info("1");
        // ミーティングデータ確認
        if(!$meeting_info){
            $this->logger2->warn(__FUNCTION__,__FILE__,__LINE__,array($server_dsn_key, $meeting_key));
            return "error!! no meeting";
        }
        $this->logger2->info("2");
        $objSharing     = new N2MY_Sharing($dsn);
        $dir = $objSharing->getDir($meeting_key) . "tmp/";
        $full_path = $objSharing->getFullPath($dir,$file_name);
        if(!is_file($full_path)){
           $this->logger2->warn(__FUNCTION__,__FILE__,__LINE__,array($server_dsn_key, $meeting_key));
            return "error!! no File";
        }
        $this->logger2->info("3");
        // ファイルダウンロード
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=" . $file_name);
        // 対象ファイルを出力する。
        readfile($full_path);
        //echo $dir;
        //echo $file_name;
        //echo $this->request->get("file");

    }

   /*
    * ストレージ一覧取得
    */
   public function action_getStorageList(){
       require_once("classes/core/dbi/Participant.dbi.php");
       require_once("classes/core/dbi/Meeting.dbi.php");
       // リクエスト取得
       $server_dsn_key = $this->request->get("server_dsn_key");
       $dsn = $this->get_dsn($server_dsn_key);
       $participant_id = $this->request->get('participant_id');
       $member_flag = $this->request->get('member_flag');
       $search_type = $this->request->get('search_type');
       $search_key = $this->request->get('search_key');
       $sort_key = $this->request->get('sort_key');
       $sort_type = $this->request->get('sort_type');

       // 参加者情報取得
       $obj_Participant    = new DBI_Participant($dsn);
       $participant_info = $obj_Participant->getRow(sprintf( "participant_key='%s'", mysql_real_escape_string($participant_id )));
       $storage_login_id = $participant_info["storage_login_id"];

       $member_key = 0;
       $user_key = 0;
       // メンバー、ユーザー情報取得
       if($member_flag == 1){
           require_once("classes/dbi/member.dbi.php");
           $obj_member = new MemberTable($dsn);
           $login_info = $obj_member->getRow(sprintf( "member_id='%s' AND member_status = 0", mysql_real_escape_string($storage_login_id )));
           $member_key = $login_info["member_key"];
           $user_key = $login_info["user_key"];
       }else{
           require_once("classes/dbi/user.dbi.php");
           $obj_user = new UserTable($dsn);
           $login_info = $obj_user->getRow(sprintf( "user_id='%s'", mysql_real_escape_string($storage_login_id )));
           $user_key = $login_info["user_key"];
       }

       // 検索条件
       $search_type = $this->request->get('search_type');
       $search_key = $this->request->get('search_key');
       $sort_key = $this->request->get('sort_key');
       if($sort_key == null){
           $sort_key = "update_datetime";
       }
       $sort_type = $this->request->get('sort_type');
       if($sort_type == null){
           $sort_type = "desc";
       }


       require_once ("classes/core/dbi/Storage.dbi.php");
       require_once ("classes/dbi/storage_favorite.dbi.php");
       require_once ("classes/N2MY_Storage.class.php");
       $obj_Storage  = new N2MY_Storage($dsn, $server_dsn_key);
       $obj_StorageFavorite    = new StorageFavoriteTable($dsn);
       if($member_flag == 0 || $login_info["use_shared_storage"] == 0){
           $where = "category != 'personal_white' AND category != 'video' AND status = 2 AND storage_file.user_key=" . $user_key. " AND storage_file.member_key = " . $member_key;
       }else{
           $where = "category != 'personal_white' AND category != 'video' AND status = 2 AND storage_file.user_key=" . $user_key. " AND (storage_file.member_key = " . $member_key. " OR storage_file.member_key = 0)";
       }

        if($search_type == "nav"){
            switch($search_key){
                case "all":
                    break;
                case "favorite":
                    $favorite = $obj_Storage->_getWhereFavorite($member_key);
                    break;
                case "document":
                    $where = $where . " AND category='document'";
                    break;
                case "image":
                    $where = $where . " AND category='image'";
                    break;
                case "shared_folder":
                    $where = $where . " AND storage_file.member_key = '0'";
                    break;
                case "private_folder":
                    $where = $where . " AND storage_file.member_key =" . $member_key;
                    break;
            }
        }else if ($search_type == "keyword"){
            $searchKeywords = str_replace("　", " ", $search_key);
            // スペース区切りの検索対応
            $searchKeywords_list = split(" " , $searchKeywords);
            foreach($searchKeywords_list as $keyword){
                $where = $where . " AND user_file_name LIKE '%".mysql_real_escape_string($keyword)."%'";
            }
        }

        $sort = array($sort_key => $sort_type);

        $list = $obj_Storage->getList($where, $sort, 1000, 0, "storage_file.*",$favorite);
        $list_count = count($list);
        $where = " user_key = " . $user_key . " AND member_key = " . $member_key;
        $favoreite_list = array();
        $favoreite_data = $obj_StorageFavorite->getRowsAssoc($where);
        // お気に入りリスト作成
        foreach($favoreite_data as $favoreite){
            $favoreite_list[] = $favoreite["storage_file_key"];
        }
        for($i = 0; $i < $list_count; $i++){
            // お気に入り確認
            if(in_array($list[$i]["storage_file_key"] , $favoreite_list)){
                $list[$i]["favorite"] = 1;
            }else{
                $list[$i]["favorite"] = 0;
            }
            // プレビュー用のpath作成
            if ($list[$i]["format"] == "vector" && $list[$i]["category"] == "document") {
                $ex = ".swf";
            }else{
                $ex = ".jpg";
            }
            for($z = 0; $z < $list[$i]["document_index"]; $z++){
                // プレビュー用のpath作成
                $return["fileName"] = sprintf( "%s_%d%s", $list[$i]["document_id"], ($z+1),$ex );
                $targetImage = sprintf( "%s%s%s/%s", N2MY_BASE_URL."docs/", $list[$i]["file_path"], $list[$i]["document_id"], $return["fileName"] );
                $list[$i]["preview"][]=$targetImage;
            }


        }
        return $this->_formatXml(0, "", array("sort_type" =>  $sort_type,"value" => $list));
    }

    /**
     * ストレージログイン
     */
    public function action_storageLogin(){
        // リクエスト取得
        $server_dsn_key = $this->request->get("server_dsn_key");
        $dsn = $this->get_dsn($server_dsn_key);
        $login_id = $this->request->get("login_id");
        $password = $this->request->get("password");
        $participant_id = $this->request->get('participant_id');
        require_once ('classes/N2MY_Auth.class.php');
        require_once ('classes/mgm/MGM_Auth.class.php');
        $counter = 0;
        $mbuf = "";
        $upd_login_id = "";
        $is_member = 0;

        // meeting
        $obj_Auth = new N2MY_Auth($dsn);
        $login_info = $obj_Auth->check($login_id, $password ,"sha1");
        if($login_info){
            if($login_info['user_info']['max_storage_size'] > 0){
                if($login_info['member_info']){
                    $is_member = 1;
                    $upd_login_id = $login_info['member_info']['member_id'];
                } else {
                    $upd_login_id = $login_id;
                }
                $counter++;
                $mbuf[$counter-1] = array("is_member"    => $is_member,
                                          "is_vid"       => 0,
                                          "user_id"      => $login_info['user_info']['user_id'],
                                          "member_id"    => $upd_login_id,
                                          "company_name" => $login_info['user_info']['user_company_name']);
            }
        }
        require_once ('classes/dbi/member.dbi.php');
        require_once ('classes/dbi/user.dbi.php');
        $obj_Member = new MemberTable($dsn);
        $objUser    = new UserTable($dsn);
        // VID(member)
        $mmb_info = $obj_Member->getRow("vcube_one_member_id = '" . mysql_real_escape_string($login_id) . "'");
        if($mmb_info){
            // VID(VcubeId)
            $res = $this -> validateVcubeId($login_id, $password);
            if($res){
                if(count($res['contracts']) > 1){
                    foreach($res['contracts'] as $key => $resp){
                        $user_info = $objUser->getRow("user_id = '" . mysql_real_escape_string($resp['user_id']) . "'");
                        if($user_info['max_storage_size'] > 0){
                            $counter++;
                            $mbuf[$counter-1] = array("is_member"    => 1,
                                                      "is_vid"       => 1,
                                                      "user_id"      => $resp['user_id'],
                                                      "member_id"    => $resp['member_id'],
                                                      "company_name" => $resp['company_name']);
                        }
                    }
                } else {
                    $member_id  = $res['contracts'][0]["member_id"];
                    $login_info = $obj_Auth -> checkVcubeId($member_id);
                    if($login_info['user_info']['max_storage_size'] > 0){
                        $counter++;
                        $mbuf[$counter-1] = array("is_member"    => 1,
                                                  "is_vid"       => 1,
                                                  "user_id"      => $login_info['user_info']['user_id'],
                                                  "member_id"    => $member_id,
                                                  "company_name" => $login_info['user_info']['company_name']);
                        $is_member    = 1;
                        $upd_login_id = $member_id;
                    }
                }
            }
        }
        switch($counter) {
        case 0:
            return $this->_formatXml(200, "LOGIN FAILED");
            break;
        case 1:
            //$participant_id書き換え
            $this->action_storageParticipantUpdate($dsn,$participant_id,$upd_login_id);
            // リターン(success member_flag)
            return $this->_formatXml(0, "", array("member_flag" => $is_member));
            break;
        default:
            // モバイルの場合は複数VID許容しない
            $useragent = getenv("HTTP_USER_AGENT");
            if (strpos($useragent, 'iOS') || strpos($useragent, 'Android')) {
                return $this->_formatXml(200, "LOGIN FAILED");
            }
            // session set
            $list = serialize($mbuf);
            $this->session->set("login_id"       , $login_id );
            $this->session->set("counter"        , $counter );
            $this->session->set("mbuf"           , $list );
            // リターン(select user value)
            return $this->_formatXml(0, "", array("status" => "select user", "value" => $mbuf));
            break;
        }
    }
    public function action_storageUserSelect(){
        // リクエスト取得
        $server_dsn_key = $this->request->get("server_dsn_key");
        $user_id        = $this->request->get("user_id");
        $participant_id = $this->request->get("participant_id");
        $dsn            = $this->get_dsn($server_dsn_key);
        // session get
        $login_id = $this->session->get("login_id" );
        $counter  = $this->session->get("counter");
        $mbuf     = $this->session->get("mbuf");
        $list     = unserialize($mbuf);
        for( $idx=0; $idx<$counter;$idx++)
        {
            if( strcmp($user_id , $list[$idx]['user_id'] ) == 0 )
            {
                $is_member    = $list[$idx]['is_member'];
                $is_vid       = $list[$idx]['is_vid'];
                $user_id      = $list[$idx]['user_id'];
                $member_id    = $list[$idx]['member_id'];
                if( $is_vid == 0) { //meeting
                    if( $is_member == 1) {
                        $upd_login_id = $member_id;
                    } else {
                        $upd_login_id = $login_id;
                    }
                } else {            //vid
                    $upd_login_id = $member_id;
                }
                //$participant_id書き換え
                $this->action_storageParticipantUpdate($dsn,$participant_id,$upd_login_id);
                // リターン
                return $this->_formatXml(0, "", array("member_flag" => $is_member));
            }
        }
        return $this->_formatXml(200, "LOGIN FAILED");
    }
    function action_storageParticipantUpdate($dsn,$participant_id,$login_id){
        //$participant_id書き換え
        require_once("classes/core/dbi/Participant.dbi.php");
        // 参加者情報取得
        $obj_Participant    = new DBI_Participant($dsn);
        $data = array(
                "storage_login_id" => $login_id
                );
        $where = "participant_key = " . $participant_id;
        $obj_Participant->update($data, $where);
    }
    /*
     * 個人ストレージ
     */
    public function action_PersonalWBLogin(){
    	// リクエスト取得
    	$server_dsn_key = $this->request->get("server_dsn_key");
    	$dsn = $this->get_dsn($server_dsn_key);
    	$login_id = $this->request->get("login_id");
    	$password = $this->request->get("password");
    	$participant_id = $this->request->get('participant_id');
    	require_once ('classes/N2MY_Auth.class.php');
    	require_once ('classes/mgm/MGM_Auth.class.php');
    	require_once("classes/core/dbi/Participant.dbi.php");
    	// 参加者情報取得
    	$obj_Participant    = new DBI_Participant($dsn);
    	//get user_info
    	$where = "participant_key = " . $participant_id;
    	$user_key = $obj_Participant->getOne($where, "user_key");
    	require_once("classes/dbi/user.dbi.php");
    	//get member_list
    	require_once ('classes/dbi/member.dbi.php');
    	$obj_Member = new MemberTable($dsn);
    	$where =  sprintf( "user_key='%s' and member_id = '%s' and member_status ='0'", $user_key,addslashes($login_id));
    	$is_user_member = $obj_Member->numRows($where);
    	if(!$is_user_member) {
    		return $this->_formatXml(201, "DIFFERENT USER ID");
    	}
    	$obj_Auth = new N2MY_Auth($dsn);
    	$login_info = $obj_Auth->check($login_id, $password ,"sha1");
    	if(!$login_info){
    		return $this->_formatXml(200, "LOGIN FAILED");
    	}
    	if($login_info["user_info"]["max_storage_size"] <= 0){
    		return $this->_formatXml(201, "NO STORAGE USER");
    	}
    	//$participant_id書き換え

    	$data = array(
    			"personal_wb_login_id" => $login_id
    	);
    	$where = "participant_key = " . $participant_id;
    	$obj_Participant->update($data, $where);
    	// リターン
    	return $this->_formatXml(0, "", array("status" => "success"));
    }

    /**
     * プレビュー
     */
    function action_storage_preview(){
        // リクエスト取得
        $server_dsn_key = $this->request->get("server_dsn_key");
        $dsn = $this->get_dsn($server_dsn_key);
        $storage_file_key = $this->request->get("storage_file_key");
        $meeting_key = $this->request->get("meeting_key");
        $index = $this->request->get("index");
    }

    /*
     *  資料貼り付け
     */
    function action_storageUpload(){
        require_once ("classes/core/dbi/Storage.dbi.php");
        require_once ("classes/dbi/storage_favorite.dbi.php");
        require_once ("classes/N2MY_Storage.class.php");
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        require_once("classes/dbi/user.dbi.php");
        // リクエスト取得
        $server_dsn_key = $this->request->get("server_dsn_key");
        $dsn = $this->get_dsn($server_dsn_key);
        $storage_file_key = $this->request->get("storage_file_key");
        $participant_id = $this->request->get('participant_id');
        $meeting_session_id = $this->request->get("meeting_key");
        $meeting_key = $this->getMeetingKey($meeting_session_id);

        $obj_Storage  = new N2MY_Storage($dsn, $server_dsn_key);
        $obj_Meeting = new DBI_Meeting( $dsn );
        $obj_User = new UserTable($dsn);
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", mysql_real_escape_string($meeting_key )));
        // ミーティングデータ確認
        if(!$meeting_info){
            return $this->_formatXml(0, "", array("success" =>  0));
        }

        $user_id = $obj_User->getOne("user_key = ".$meeting_info["user_key"], "user_id");
        if($obj_Storage->meetingCopyFile($storage_file_key , $meeting_info , $participant_id, $user_id)){
            return $this->_formatXml(0, "", array("success" =>  1));
        }else{
            return $this->_formatXml(0, "", array("success" =>  0));
        }

    }

    /**
     * 共有メモをメールで送信
     */
    public function action_sendSharedMemoMail(){

        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/N2MY_Sharing.class.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        // リクエスト取得
        $server_dsn_key = $this->request->get("server_dsn_key");
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_Meeting = new DBI_Meeting( $dsn );

        $meeting_session_id = $this->request->get("meeting_key");
        $meeting_key = $this->getMeetingKey($meeting_session_id);
        $mailAddress = $this->request->get("mailAddress");
        $memoData = $_REQUEST["memoData"];
        $locale = $this->request->get("locale");
        $participant_id = $this->request->get('participant_id');
        $obj_Participant    = new DBI_Participant($dsn);

        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", mysql_real_escape_string($meeting_key )));
        // ミーティングデータ確認
        if(!$meeting_info){
            return $this->_formatXml("103", "Object not found");
        }

        $participant_info = $obj_Participant->getRow(sprintf( "participant_key='%s'", mysql_real_escape_string($participant_id )));

        $session_id = $participant_info["participant_session_id"];
       // 値確認
        if (!$server_dsn_key || !$meeting_key || !$mailAddress) {
            $this->logger2->error(__FUNCTION__,__FILE__,__LINE__,array($server_dsn_key, $meeting_key, $mailAddress, $locale));
            $result = (string) 0;
        } else {
            switch($locale) {
                case "zh_CN":
                    $lang = "zh";
                    break;
                case "en_US":
                    $lang = "en";
                    break;
                case "zh_TW":
                    $lang = "zh-tw";
                    break;
                case "fr_FR":
                    $lang = "fr";
                    break;
                case "in_ID":
                    $lang = "in";
                    break;
                case "th_TH":
                    $lang = "th";
                    break;
                case "ko_KR":
                    $lang = "ko";
                    break;
                case "ja_JP":
                default:
                    $lang = "ja";
            }
            $objSharing     = new N2MY_Sharing($dsn);

            // セッションの開始
            session_id($session_id);
            // セッションのタイムゾーンを元に時間を形成
            $time_zone = $this->session->get("time_zone");
            if(!$time_zone){
                $time_zone = 9;
            }
            $time = (EZDate::getLocateTime((date("U")), $time_zone, N2MY_SERVER_TIMEZONE));

            $file_name = sprintf( "%s%s%s", "vcmemo_", date("YmdHi" , $time), ".txt" );
            // メモファイル作成
            $file = $objSharing->makeMemoDate($meeting_key, $memoData, $file_name, ".txt", true);
            if(!$file){
                return $this->_formatXml("103", "Object not found");
            }
            // 添付メール送信
            $result = $objSharing->sendMemoData($meeting_key , $mailAddress, $file["full_path"], $file["file_name"] , $lang , $time_zone);
            // メールを送信したらメモファイルを削除
            unlink($file["full_path"]);
        }
        return $this->_formatXml(0, "", array("value" => $result));

    }

    public function action_sendInvitationMail()
    {
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/dbi/reservation.dbi.php");

        $server_dsn_key = $this->request->get("server_dsn_key");
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_Meeting = new DBI_Meeting( $dsn );
        $request = $this->request->getAll();
        $meeting_session_id = $this->request->get("meeting_key");
        $meeting_key = $this->getMeetingKey($meeting_session_id);
//        $meeting_sequence_key = $this->request->get("meeting_sequence_key");
        $mailAddress = $this->request->get("mailAddress");
        $locale = $this->request->get("locale");
        $userType = $this->request->get("userType");

        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", mysql_real_escape_string($meeting_key )), "meeting_ticket, is_reserved" );
        if ($meeting_info["is_reserved"] == "1") {
             $obj_Reservation    = new ReservationTable( $dsn );
             $where = " meeting_key = '".mysql_real_escape_string($meeting_info["meeting_ticket"])."'".
             " AND reservation_status = 1";
             $reservation_info = $obj_Reservation->getRow($where);
             if( $reservation_info["reservation_pw_type"] == 2 || $reservation_info["reservation_pw"] == "")
             {
                $reservationPassword  = "";
             } else {
                $reservationPassword = $reservation_info["reservation_pw"];
             }
        } else {
             $reservationPassword  = "";
        }

        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$request);
        if (!$server_dsn_key ||
            !$meeting_key ||
//            !$meeting_sequence_key ||
            !$mailAddress) {
            $this->logger2->error(__FUNCTION__,__FILE__,__LINE__,array($server_dsn_key, $meeting_key, $mailAddress, $locale));
            $result = (string) 0;
        } else {
            switch($locale) {
                case "zh_CN":
                    $lang = "zh";
                    break;
                case "en_US":
                    $lang = "en";
                    break;
                case "zh_TW":
                    $lang = "zh-tw";
                    break;
                case "fr_FR":
                    $lang = "fr";
                    break;
                case "in_ID":
                    $lang = "in";
                    break;
                case "th_TH":
                    $lang = "th";
                    break;
                case "ko_KR":
                	$lang = "ko";
                	break;
                case "ja_JP":
                default:
                    $lang = "ja";
            }
            require_once ("classes/N2MY_Invitation.class.php");
            $objInvidation = new N2MY_Invitation($dsn);
            $result = $objInvidation->sendInvitationMail($meeting_key, $mailAddress, $lang, $userType, $reservationPassword);
        }

        return $this->_formatXml(0, "", array("value" => $result));
    }

    /**
     * ファイルを受け取りhtdocs/files/tmpに一時保存しておく
     * @param file
     * @param auth_key
     * @param core_session_key
     *
     */
    public function action_tmpFileSave(){

        // auth_key確認
        $auth_key = $this->request->get("auth_key");
        // auth_key 確認
        if($auth_key != N2MY_FLASH_API_AUTH_KEY){
            $this->logger2->warn("auth_key error");
            return $this->_formatXml("102", "Invalid parameter");
        }

        // 会議キー確認
        require_once("classes/core/dbi/Meeting.dbi.php");
        $dsn = $this->get_dsn();
        $obj_Meeting = new DBI_Meeting( $dsn );

        $meeting_session_id = $this->request->get("core_session_key");
        $where = "meeting_session_id = '" .$meeting_session_id."'";

        $meeting_info = $obj_Meeting->getRow($where);
        $this->logger2->info('core_session_key:'.$meeting_session_id);
        if(!$meeting_info){
            $this->logger2->warn("core_session_key error");
            return $this->_formatXml("102", "Invalid parameter");
        }

        // ファイル情報取得
        $file = $_FILES;
        $date = date('Ymd');

        if($file["file"]["error"]){
            $this->logger2->warn("not file");
            return $this->_formatXml("102", "Invalid parameter");
        }

        //ディレクトリ作成 実際に保存される絶対パス
        $temp_dir = N2MY_TMP_FILE_DIR.$date;
        if(!is_dir($temp_dir)){
            mkdir( $temp_dir , 0777,true);
            chmod( $temp_dir , 0777);
        }

        // ファイル情報を保存
        require_once("classes/dbi/tmp_file.dbi.php");
        $obj_tmp_file = new TmpFiletTable( $dsn );

        $fileinfo = pathinfo($file["file"]["name"]);
        // ランダム文字列ファイル名作成
        $tmp_file_id =  $this->_makeRandStr(40);

        // id重複確認
        while(1){
            $where = "status = 1 AND tmp_file_id = '".$tmp_file_id."'" ;
            $tmp_file_info = $obj_tmp_file->getRow($where);
            if($tmp_file_info){
                $tmp_file_id =  $this->_makeRandStr(40);
            }
            else{
                break;
            }
        }

        if($fileinfo["extension"]){
            $extension = $fileinfo["extension"];
        }else{
            $extension = "pdf";
        }

        $tmp_file_name = $tmp_file_id .".". $extension;

        // DB登録用のパス作成
        $path = substr($temp_dir , strlen(N2MY_APP_DIR))."/";

        $tmp_file_data = array(
                "tmp_file_id" => $tmp_file_id,
                "path"=> $path,
                "file_name" => $tmp_file_name,
                "status" => 1,
                "dl_flag" =>0
                );
        $this->logger2->info($file["file"]);
        $this->logger2->info($tmp_file_data);


        if($file["file"]){
            // 実ファイル保存
            if (!move_uploaded_file($file["file"]["tmp_name"] ,  $temp_dir .'/'.$tmp_file_name) ){
                $this->logger2->warn("file move error");
                return $this->_formatXml("102", "Invalid parameter");
            }
        }else{
            // バイナリデータで来た場合
            $binary_data = $_REQUEST["file"];
            if(!$binary_data){
                $this->logger2->warn("binary_data error");
                return $this->_formatXml("102", "Invalid parameter");
            }
            $max_tmp_file_size = $this->config->get("N2MY", "max_tmp_file_size") ? $this->config->get("N2MY", "max_tmp_file_size") : 50000000;
            if($max_tmp_file_size < strlen($binary_data) ){
                $this->logger2->warn("binary_data size over error");
                return $this->_formatXml("102", "Invalid parameter");
            }
            $file_data = base64_decode($binary_data);
            $fp = fopen($temp_dir .'/'.$tmp_file_name, "w" );
            fwrite($fp, $file_data);
            fclose($fp);
        }

        //DBにデータ追加
        $obj_tmp_file->add($tmp_file_data);
        chmod($temp_dir .'/'.$tmp_file_name, 0777);
        $data = array(
                "tmp_file_id" => $tmp_file_id,
                );

        return $this->_formatXml(0, "", array($data));
    }

    /**
     * ファイルDLを行う
     * @param tmp_file_id
     * @param auth_key
     *
     */
    public function action_tmpFileDL(){
        // auth_key確認
        $auth_key = $this->request->get("auth_key");
        // auth_key 確認
        if($auth_key != N2MY_FLASH_API_AUTH_KEY){
            $this->logger2->warn("auth_key error");
            return $this->_formatXml("102", "Invalid parameter");
        }

        $tmp_file_id =  $this->request->get("tmp_file_id");
        $this->logger2->info($tmp_file_id);
        $file_path = $this->_getFilePath($tmp_file_id);

        if(!$file_path){
            return $this->_formatXml("102", "Invalid parameter");
        }

        // オープンできるか確認
        if (!($fp = fopen($file_path, "r"))) {
            $this->logger2->warn("open error:" . $file_path);
            return $this->_formatXml("102", "Invalid parameter");
        }
        fclose($fp);

        // ファイルサイズの確認
        if (($content_length = filesize($file_path)) == 0) {
            $this->logger2->warn("file size 0:" . $file_path);
            return $this->_formatXml("102", "Invalid parameter");
        }

        // ダウンロード用のHTTPヘッダ送信
        header("Content-Disposition: inline; filename=\"".basename($file_path)."\"");
        header("Content-Length: ".$content_length);
        header("Content-Type: application/octet-stream");

        // ファイルを読んで出力
        if (!readfile($file_path)) {
            $this->logger2->warn("read file error:". $file_path);
            return $this->_formatXml("102", "Invalid parameter");
        }
        $dsn = $this->get_dsn();
        require_once("classes/dbi/tmp_file.dbi.php");
        $obj_tmp_file = new TmpFiletTable( $dsn );
        $tmp_file_data = array(
                "dl_flag" =>1
                );
        $where = "status = 1 AND tmp_file_id = '".addslashes($tmp_file_id)."'";
        $obj_tmp_file->update($tmp_file_data, $where);


        //return $this->_formatXml(0, "", array("result" => $file_path));


    }

    /**
     * ファイル削除を行う
     * @param tmp_file_id
     * @param auth_key
     * @param memo(not Required)
     *
     */
    public function action_tmpFileDelete(){
        $auth_key = $this->request->get("auth_key");
        if($auth_key != N2MY_FLASH_API_AUTH_KEY){
            return $this->_formatXml("102", "Invalid parameter");
        }

        $memo = $this->request->get("memo");
        $tmp_file_id =  $this->request->get("tmp_file_id");
        $this->logger2->info($tmp_file_id);
        $file_path = $this->_getFilePath($tmp_file_id);

        if(!$file_path){
            return $this->_formatXml("102", "Invalid parameter");
        }

        // ファイル削除
        unlink($file_path);

        // DB更新
        $dsn = $this->get_dsn();
        require_once("classes/dbi/tmp_file.dbi.php");
        $obj_tmp_file = new TmpFiletTable( $dsn );
        $tmp_file_data = array(
                "status" => 0,
                "memo"   => $memo
        );
        $this->logger2->info("FileDelete memo:".$memo);
        $where = "tmp_file_id = '".addslashes($tmp_file_id)."'";
        $obj_tmp_file->update($tmp_file_data, $where);

        return $this->_formatXml(0, "", array("value" => 1));
    }

    /*
     * tmp_file_idからpathを作成
     */
    private function _getFilePath($tmp_file_id){
        $dsn = $this->get_dsn();
        require_once("classes/dbi/tmp_file.dbi.php");
        $obj_tmp_file = new TmpFiletTable( $dsn );

        //$tmp_file_id =  $this->request->get("tmp_file_id");

        $where = "status = 1 AND tmp_file_id = '".addslashes ($tmp_file_id)."'";
        $tmp_file_info = $obj_tmp_file->getRow($where);
        if(!$tmp_file_info){
            $this->logger2->warn("no data");
            return false;
        }

        $file_path = N2MY_APP_DIR.$tmp_file_info["path"].$tmp_file_info["file_name"];
        // ファイルの存在確認
        if (!file_exists($file_path)) {
            $this->logger2->warn("no file:" . $file_path);
            return false;
        }

        $this->logger2->info($tmp_file_info);
        return $file_path;
    }

    /**
     * ランダム文字列生成 (英数字)
     * $length: 生成する文字数
     */
    private function _makeRandStr($length) {
        $str = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z'));
        $r_str = null;
        for ($i = 0; $i < $length; $i++) {
            $r_str .= $str[rand(0, count($str))];
        }
        return $r_str;
    }

    /**
     * XMLのフォーマット
     */
    private function _formatXml($error_cd = 0, $error_msg = "", $result = array()) {
        $error = $error_cd ? 1 : 0;
        $data = array(
            "status" => array("error" => $error, "reason" => $error_cd, "error_msg" => $error_msg),
            "result" => $result,
        );
        $this->_outputXML($data);
    }

    /**
     * XMLをシリアライズして出力
     */
    private function _outputXML($data) {
        require_once("lib/pear/XML/Serializer.php");
        $serializer = new XML_Serializer();
        $serializer->setOption('mode','simplexml');
        $serializer->setOption('encoding','UTF-8');
        $serializer->setOption('addDecl','ture');
        $serializer->setOption('rootName','flashAPI');
        $serializer->serialize($data);
        $output = $serializer->getSerializedData();
        header("Content-Type: text/xml; charset=UTF-8");
        print $output;
    }

    function getMeetingKey($meeting_id) {
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass(N2MY_MDB_DSN);
        return $obj_MGMClass->getMeetingID($meeting_id);
    }

    function action_log_file_download() {
        require_once("classes/N2MY_FileUpload.class.php");
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/dbi/file_cabinet.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        $objFileUpload  = new N2MY_FileUpload();
        $objMeeting     = new MeetingTable($this->get_dsn());
        $objFileCabinet = new FileCabinetTable($this->get_dsn());
        $objParticipant = new DBI_Participant($this->get_dsn());
        $meeting_session_id = $this->request->get('meeting_key');
        $cabinet_id         = $this->request->get('cabinet_id');
        $participant_id     = $this->request->get('participant_id');
        // ファイル出力
        $meeting_info   = $objMeeting->getRow("meeting_session_id='".addslashes($meeting_session_id)."'");
        $dir            = $objFileUpload->getDir( $meeting_info['meeting_key'] );
        $destDir        = $objFileUpload->getFullPath( $dir, $cabinet_id );
        if (file_exists($destDir)) {
            // 操作ログ
            $participant_name = $objParticipant->getOne("meeting_key = '".$meeting_info['meeting_key']."' AND participant_key = '".$participant_id."'", 'participant_name');
            $file_name        = $objFileCabinet->getOne("meeting_key = '".$meeting_info['meeting_key']."' AND tmp_name = '".$cabinet_id."'", 'file_name');
            $this->add_operation_log('cabinet_download', array(
                'room_key'          => $meeting_info['room_key'],
                'meeting_name'      => $meeting_info['meeting_name'],
                'participant_name'  => $participant_name,
                'file_name'         => $file_name,
                ));
            print 1;
        } else {
            print 0;
        }
    }

    function action_change_datacenter() {
        $server_dsn_key     = $this->request->get('dsn_key');
        $meeting_key        = $this->request->get('meeting_key');
        $datacenter_key     = $this->request->get('datacenter_key');
        $participant_key    = $this->request->get('participant_key');
        if (!$server_dsn_key || !$meeting_key || !$datacenter_key) {
            $this->logger2->info(array($server_dsn_key, $meeting_key, $datacenter_key));
            return $this->_formatXml(102, "Invalid parameter");
        }
        require_once("classes/core/Core_Meeting.class.php");
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/core/dbi/MeetingOptions.dbi.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        require_once("classes/core/dbi/DataCenter.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        require_once("classes/mgm/MGM_Auth.class.php");
//        $obj_MeetingSequence = new DBI_MeetingSequence( $dsn );
        $obj_Datacenter     = new DBI_DataCenter( N2MY_MDB_DSN );
        $obj_FmsServer      = new DBI_FmsServer( N2MY_MDB_DSN );
        $obj_MGMClass       = new MGM_AuthClass(N2MY_MDB_DSN);

        // ハッシュした会議キーから連番のキーを参照
        $dsn = $this->get_dsn($server_dsn_key);
        $obj_CoreMeeting    = new Core_Meeting($dsn, N2MY_MDB_DSN);
        $obj_Meeting        = new DBI_Meeting($dsn);
        $obj_MeetingOptions = new DBI_MeetingOptions($dsn);
        $obj_Participant    = new DBI_Participant($dsn);
        $meeting_key = $obj_MGMClass->getMeetingID($meeting_key);
        // 会議情報取得
        if (!$now_meeting_info = $obj_Meeting->getRow("meeting_key = '".addslashes($meeting_key)."' AND is_deleted = 0")) {
            return $this->_formatXml("103", "Object not found", array("meeting_key" => $meeting_key));
        }
        // データセンタ情報取得
        if (!$datacenter_info = $obj_Datacenter->getRow("datacenter_key='".addslashes($datacenter_key)."'")) {
            return $this->_formatXml("103", "Object not found", array("datacenter_key" => $datacenter_key));
        }
        // 名前取得
        $where = "meeting_key = '".$now_meeting_info['meeting_key']."' AND participant_key = ".$participant_key;
        $participant_info = $obj_Participant->getRow($where);
        if (!$participant_info) {
            return $this->_formatXml("103", "Object not found", array("participant_key" => $participant_key));
        }
        $participant_name = escapeshellarg($participant_info['participant_name']);
        // SSLオプション取得
        $sslOption = $obj_MeetingOptions->getRow("meeting_key='".addslashes($meeting_key)."' AND option_key=1");
        $is_ssl = $sslOption ? 1 : 0;
        // 現在のFMSアドレス
        $before_server_info = $obj_FmsServer->getRow("server_key='".addslashes($now_meeting_info['server_key'])."'");
        require_once("classes/dbi/fms_deny.dbi.php");
        try {
            $objFmsDeny = new FmsDenyTable( $dsn );
            $_fms_deny_keys = $objFmsDeny->findByUserKey($now_meeting_info["user_key"], null, null, 0, "fms_server_key");
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            return $this->_formatXml("103", "Object not found");
        }
        $fms_deny_keys = array();
        $deny_fms = $obj_FmsServer->getRowsAssoc("server_address = '".$before_server_info["server_address"]."' AND is_available = 1");
        if ($deny_fms) {
            foreach ($deny_fms as $fms_server) {
                $fms_deny_keys[] = $fms_server["server_key"];
            }
        }
        if ($_fms_deny_keys) {
            foreach ($_fms_deny_keys as $fms_key) {
                $fms_deny_keys[] = $fms_key["fms_server_key"];
            }
        }
        // FMSサーバ切り替え
        if ($datacenter_key == "2001") {
            $fms_country_priority = $this->config->get('FMS_COUNTRY_PRIORITY', $now_meeting_info["meeting_country"]);
            if (!$fms_country_priority) {
                $fms_country_priority =$this->config->get('FMS_COUNTRY_PRIORITY', "etc");
            }
            $datacenter_country_list = split(',', $fms_country_priority);
            $server_key = $obj_FmsServer->getGlobalLinkFmsList($is_ssl, $fms_deny_keys, $datacenter_country_list ,$now_meeting_info["need_fms_version"]);
        } else {
            $server_key = $obj_FmsServer->getKeyByDatacenterKey($datacenter_info["datacenter_key"], $datacenter_info["datacenter_country"], $is_ssl, false, $fms_deny_keys, $now_meeting_info["need_fms_version"], null);
        }
        if ($server_key) {
            if ($server_key > 1001 || $server_key == 2001) {
                $fms_address = $before_server_info["server_address"];
            } else {
                $fms_address = $before_server_info["local_address"] ? $before_server_info["local_address"] : $before_server_info["server_address"];
            }

            // データセンタ切り替え
            $obj_CoreMeeting->changeFMS($meeting_key, $server_key);
            // FMSに切り替えを通知
            $rtmp = new EZRtmp();
            $uri = 'rtmp://'.$fms_address."/".$this->config->get('CORE', 'app_name').'/'.$now_meeting_info['fms_path'].$now_meeting_info['meeting_session_id'];
            $cmd .= "\"h = RTMPHack.new; " .
                    "h.connection_uri = '" . $uri . "';" .
                    "h.connection_args = [0x900];" .
                    "h.method_name = 'doChangeDataCenter';" .
                    "h.method_args = [$datacenter_key, $participant_key, $participant_name];" .
                    "h.execute\"";
            $this->logger2->info($cmd);
            $result = $rtmp->run($cmd);
            $after_server_info = $obj_FmsServer->getRow("server_key='".addslashes($server_key)."'");
            return $this->_formatXml(0, "", array(
                'before'    => $before_server_info["server_address"],
                'after'     => $after_server_info["server_address"],
                ));
        } else {
            // 失敗
            return $this->_formatXml("103", "切り替え可能なFMSが存在しない");
        }
    }

    public function action_get_password() {
        $server_dsn_key     = $this->request->get('dsn_key');
        $meeting_session_id = $this->request->get("meeting_key");
        if (!$server_dsn_key || !$meeting_session_id) {
            $this->logger2->info(array($server_dsn_key, $meeting_session_id));
            return $this->_formatXml(102, "Invalid parameter");
        }
        $meeting_key = $this->getMeetingKey($meeting_session_id);
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/dbi/reservation.dbi.php");
        $dsn = $this->get_dsn_value($server_dsn_key);

        $obj_Meeting = new DBI_Meeting( $dsn );
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", mysql_real_escape_string($meeting_key )), "meeting_ticket, is_reserved" );
        if( DB::isError($meeting_info) || $meeting_info === false ){
            $this->logger2->error("db error:meeting_key=" . $meeting_key );
            return $this->_formatXml("103", "Object not found", array("meeting_key" => $meeting_key));
        }
        if ($meeting_info["is_reserved"] == "1") {
             $obj_Reservation    = new ReservationTable( $dsn );
             $where = " meeting_key = '".mysql_real_escape_string($meeting_info["meeting_ticket"])."'".
             " AND reservation_status = 1";
             $reservation_info = $obj_Reservation->getRow($where, "reservation_pw");
             if( DB::isError($reservation_info) || !$reservation_info["reservation_pw"] ){
                 $this->logger2->error("passwd error:meeting_key=" . $meeting_key );
                 return $this->_formatXml("103", "Object not found", array("meeting_key" => $meeting_key));
             }
        } else {
            return $this->_formatXml("103", "Object not found", array("meeting_key" => $meeting_key));
        }

        return $this->_formatXml(0, "", array(
                'passwd'    => $reservation_info["reservation_pw"]
                ));
//    	return array("status" => true, "passwd" => "1111");
    }
}

$main =& new AppAMF();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */