<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once('lib/amf/app/Gateway.php');
require_once('classes/AppFrame.class.php');

class AppFlashGateway extends AppFrame {
    function default_view() {
        $action = $this->action_default();
        if (PEAR::isError($action)) {
            return $action;
        }
    }

    function action_default() {
        $gateway =& new Gateway();
        //set class_path
        $path = '../../../classes/amf/';
        $gateway->setBaseClassPath($path);
        $gateway->service();
    }
}

$main =& new AppFlashGateway();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */