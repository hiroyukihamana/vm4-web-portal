<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("lib/EZLib/EZCore/EZFrameApi.class.php");
require_once ("classes/AppFrame.class.php");
require_once ("classes/N2MY_Storage.class.php");
require_once ("classes/mgm/MGM_Auth.class.php");
require_once ("lib/EZLib/EZUtil/EZDate.class.php");
require_once ("lib/EZLib/EZUtil/EZMath.class.php");
require_once ("lib/EZLib/EZUtil/EZString.class.php");
require_once ("classes/dbi/storage_favorite.dbi.php");
require_once ("config/config.inc.php");
require_once ("classes/N2MY_Clip.class.php");
require_once ("classes/ValidateClip.class.php");
require_once ("classes/dbi/clip.dbi.php");
require_once ("classes/dbi/meeting_clip.dbi.php");
require_once ("classes/dbi/meeting.dbi.php");
require_once ("classes/ClipFormat.class.php");
require_once ("classes/core/dbi/Storage.dbi.php");

class AppStorage extends AppFrame {

    private $_name_space                 = null;
    private $_name_space_toppage;
    private $_userKey                    = null;
    private $_userId                     = null;
    private $_memberKey                  = null;
    private $_obj_Storage      = null;
    private $_obj_StorageFavorite        = null;
    private $_obj_StorageTable        = null;
    private $_obj_N2MY_Clip        = null;

    /**
     * 初期処理
     */
    function init() {
        $this->_name_space = md5(__FILE__);
        $this->_name_space_toppage = md5( __FILE__ . 'toppage' );
        $this->template->assign('is_not_display_portal_header', true);
    }
    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        // 認証後のみ
        $userInfo                      = $this->session->get("user_info");
        if($userInfo["max_storage_size"] == 0){
            header("Location: ".'/');
        }
        $userKey                       = $userInfo["user_key"];
        $serverInfo                    = $this->session->get("server_info");
        $memberInfo                    = $this->session->get("member_info");
        $this->_userKey                = $userKey;
        if($memberInfo["member_key"]){
            $this->_memberKey              = $memberInfo["member_key"];
        }else{
            $this->_memberKey              = 0;
        }
        $dsn                           = $this->get_dsn();
        $this->_obj_Storage  = new N2MY_Storage($dsn, $serverInfo["host_name"]);
        $this->_userId                 = $this->_obj_Storage->getUserId("", $userKey);
        $this->_obj_StorageFavorite    = new StorageFavoriteTable($dsn);
        $this->_obj_StorageTable        = new DBI_StorageFile($dsn);
        $this->_obj_N2MY_Clip = new N2MY_Clip( $this->get_dsn() );
    }

    /**
     * TODO 小森修正中
     */
    function action_searchList($request = null) {
        $return_flag = true;
        if(!$request){
            $request = $this->request->getAll();
            $return_flag = false;
        }
        if ($request["searchType"]) {
            $search_type = $request["searchType"];
            $this->session->set("search_type" , $search_type);

        }
        if ($request["searchKeyword"]) {
            $searchKeywords = $request["searchKeyword"];
        }
        $sort = array();

        if ($request["sort_key"]) {
            $sort_key = $request["sort_key"];
            $sort_type = $request["sort_type"];
            $sort = array($sort_key => $sort_type);
            $this->session->set("sort_key" , $sort_key);
            $this->session->set("sort_type" , $sort_type);
        }
        $member_info = $this->session->get("member_info");
        $user_info = $this->session->get("user_info");

//        $this->logger2->info();
        if($this->_memberKey == 0 || $member_info["use_shared_storage"] == 0){
            $where = "status !=3 AND storage_file.user_key=" . $this->_userKey. " AND storage_file.member_key = " . $this->_memberKey;
        }else{
            $where = "status !=3 AND storage_file.user_key=" . $this->_userKey. " AND (storage_file.member_key = " . $this->_memberKey. " OR storage_file.member_key = 0)";
        }

        switch($search_type){
            case "all":
                break;
            case "favorite":
                $favorite = $this->_obj_Storage->_getWhereFavorite($this->_memberKey);
                break;
            case "owner":
                $where = $where . " AND owner=".$this->_memberKey;
                break;
            case "document":
                $where = $where . " AND category='document'";
                break;
            case "image":
                $where = $where . " AND category='image'";
                break;
            case "video":
                $where = $where . " AND category='video'";
                break;
        }
        if($searchKeywords){
            $searchKeywords = str_replace("　", " ", $searchKeywords);
            // スペース区切りの検索対応
            $searchKeywords_list = split(" " , $searchKeywords);
            foreach($searchKeywords_list as $keyword){
                $where = $where . " AND user_file_name LIKE '%".mysql_real_escape_string($keyword)."%'";
            }

        }

        //$this->logger2->info($where);

        $list = $this->action_list($this->_obj_Storage->getList($where, $sort, 1000, 0, "storage_file.*",$favorite));


        if($return_flag){
            return $list;
        }else{
            $size = array(
                "use_size" => $this->number_format(($this->_obj_Storage->get_total_size($this->_userKey)),1024),
                "max_size" => $user_info["max_storage_size"],
            );
            echo json_encode(array("list" => $list , "size" => $size));
        }

    }

    function action_addListRow($addRow = false, $listOffset = null) {
        $rowset     = $this->session->get("rowset");
        $searchType = $rowset["search_type"] ? $rowset["search_type"] : "";
        $request = $this->request->getAll();
        if (empty($listOffset)) {
            $listOffset = $request["offset"] ? $request["offset"] : 0;
        }
        $upRow = $rowset["up_row"] ? $rowset["up_row"] : 0;

        if ($addRow) {
            $limit     = $listOffset;
            $offset    = 0;
        } else {
            $limit     = 10;
            $offset    = $listOffset;
        }

        $userKey   = $this->_userKey;
        $memberKey = $this->_memberKey;
        $options = array(
            "user_key"       => $userKey,
            "member_key"     => $memberKey,
            "limit"          => $limit,
            "offset"         => $offset,
            "sort_key"       => $rowset["sort_key"]  ? $rowset["sort_key"]  : "update_datetime",
            "search_type"    => $rowset["search_type"],
            "search_keyword" => $rowset["search_keyword"]
        );
        if ($addRow) {
            $upRow = 0;
        }
        $this->logger->info($options);
        $this->session->set("condition", $options, $this->_name_space);

        $ret = $this->action_list($options, true);
        $this->logger->info($ret);

        if ($addRow) {
            $limit = 10;
            $ret   = array_reverse($ret);
        }

        $options["offset"] = $offset + $limit;
        $nextRow           = true;
        $objWhiteboardStorage = $this->_obj_Storage;
        if (!$objWhiteboardStorage->getStorageList($options)) {
            $nextRow = false;
        }

        $rowset["offset"] = $offset;
        $this->session->set("rowset", array(
            "offset"         => $offset,
            "limit"          => $limit,
            "sort_key"       => $rowset["sort_key"],
            "up_row"         => $upRow,
            "search_type"    => $rowset["search_type"],
            "search_keyword" => $rowset["search_keyword"]
        ));

       $charges = $objWhiteboardStorage->getCharges($userKey, $memberKey);

       $retArray = array("rowset" => $rowset, "list" => $ret, "next_row" => $nextRow, "up_row" => $upRow, "charges" => $charges);
       if ($addRow) {
           return $retArray;
       }
       echo json_encode($retArray);
    }

    function action_adminFolder() {
        $userKey            = $this->_userKey;
        $memberKey          = $this->_memberKey;
        $request            = $this->request->getAll();
        $offset             = $request["offset"];
        $this->logger->info($request);

        $options            = array();
        $options["user_key"] = $userKey;

        if (empty($memberKey)) {
            $options["member_key"] = 0;
        } else {
            $options["member_key"] = $memberKey;
        }
        $ret = $this->_obj_Storage->getFolderFirstNode($options);
        if (!empty($ret["error"])) {
            return $ret;
        }
        if (!empty($ret)) {
            $this->template->assign("folder", $ret);
        }

        $storageIds = array();
        foreach ($request as $key => $value) {
            if ("action_adminFolder" != $key && "offset" != $key) {
                $storageIds[] = $value;
            }
        }
        $this->template->assign('storage_ids', implode(", ", $storageIds));
        $this->template->assign('offset'     , $offset);
        $this->template->assign('user_key'   , $userKey);
        $this->template->assign('member_key' , $memberKey);
        $this->display('user/whiteboardStorage/box.t.html');
    }

    function action_getAdminFolder() {
        $userKey     = $this->_userKey;
        $memberKey   = $this->_memberKey;
        $options     = array("user_key" => $userKey);
        if (empty($memberKey)) {
            $options["member_key"]  = "0";
        } else {
            $options["member_key"]  = $memberKey;
        }
        $this->logger->info($options);

        $objWhiteboardStorage = $this->_obj_Storage;
        $resultNode       = array_filter($objWhiteboardStorage->getParentOfChildrenNode($options));
        $this->logger->info($resultNode);

        if (!empty($resultNode["error"])) {
            $this->logger2->warn("error parent of children node");
            echo json_encode($resultNode);
            return;
        }
        $storageIds    = $this->request->get("storage_ids");
        $folderIds = array();

        foreach ($storageIds as $key => $storageId) {
            $resultSelectFolder = $objWhiteboardStorage->getFolderId($storageId);
            foreach ($resultSelectFolder as $columns) {
                if (!empty($columns["folder_id"])) {
                    $folderIds[] = $columns["folder_id"];
                }
            }
        }
        $returnNode       = array();
        $nodeLineList     = array();
        $docStatus        = false;
        $nextFolderId = "";

        $options = array(
            "user_key"   => $userKey,
            "member_key" => $memberKey
        );

        for ($i = 0; $i < count($resultNode); $i++) {
            $rowColumns           = $resultNode[$i];
            $folderId             = $rowColumns["children_folder_id"];
            $options["folder_id"] = $folderId;
            $searchResult  = array_search($folderId, $folderIds);
            if (!$docStatus) {
                if (is_int($searchResult)) {
                    $docStatus = true;
                } else {
                    $docStatus = $this->nodeSearch($options, $folderIds);
                }
            }
            $nodeLineList = array_merge($nodeLineList, array($rowColumns));

            if (empty($rowColumns["parent_folder_id"])) {
                $returnNode = array_merge($returnNode, $nodeLineList);
                $nodeLineList = array();
                continue;
            }

            $nextFolderId = $resultNode[$i + 1]["parent_folder_id"];
            if (empty($nextFolderId) ||
                $nextFolderId != $rowColumns["parent_folder_id"]) {
                    if ($docStatus) {
                        foreach ($nodeLineList as $columns) {
                            $returnNode[] = $columns;
                        }
                        $docStatus = false;
                    }
                $nodeLineList = array();
            }
        }
        $retArray = array(
            "lists"      => $returnNode,
            "folder_ids" => implode(", ", $folderIds));

        $this->logger2->debug($retArray);
        $this->logger->info($retArray);
        echo json_encode($retArray);
    }

    function action_getFolder($rowStatus = false, $folderId = null) {
        if (empty($folderId)) {
            $folderId = $this->request->get("node");
        }

        $options = array(
            "user_key"   => $this->_userKey,
            "member_key" => $this->_memberKey,
        );

        $objWhiteboardStorage     = $this->_obj_Storage;
        $options["folder_id"] = $folderId;
        $this->logger->info($options);

        $ret = $objWhiteboardStorage->getFolderNode($options);
        $this->logger->info($ret);

        if ($rowStatus || !empty($ret["error"])) {
            return $ret;
        }
        echo json_encode($ret);
    }

    function action_modifyFolderName() {
    	if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn('error submit key');
    		echo json_encode(array("error" => "error"));
    		return;
    	}
    	
        $folderId   = $this->request->get('node');
        $folderName = $this->request->get('folder_name');

        $ret = $this->_obj_Storage->updateFolderName(
            $folderId, $folderName, $this->_userKey);

        echo json_encode($ret);
    }

    function action_top() {
        $this->session->remove('whiteboard_storage_auth', $this->_name_space);
        $this->session->remove('rowset', $this->_name_space);
        $this->session->set("rowset",
            array("offset" => 0,
                  "limit"  => 10,
                  "up_row" => 0));
        $options = array();
        $options["user_key"]   = $this->_userKey;
        $objStorage      = $this->_obj_Storage;


        $options["member_key"] = $this->_memberKey;
        //$this->logger->info($options);

        $this->session->set("condition", $options, $this->_name_space);
        $this->template->assign('is_not_display_portal_header', false);
        parent::set_submit_key($this->_name_space_toppage);
        
        $this->render_top();
    }

    function action_select_file(){
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        $this->template->assign("user_key"    , $this->_userKey);
        $this->template->assign("member_key"  , $this->_memberKey);
        // サイズ計算
        $size = $this->number_format(($this->_obj_Storage->get_total_size($this->_userKey)),1024);
        $this->template->assign("max_storage_size" , $user_info["max_storage_size"]);
        $this->template->assign("size" , $size);
        $this->template->assign("use_shared_storage" , $member_info["use_shared_storage"]);
        
        $this->display('user/storage/select_file.t.html');
    }

/*
    private function action_list($options = null, $addrowStatus = false) {
        $objWhiteboardStorage = $this->_obj_Storage;
        if (0 == $objWhiteboardStorage->getCount($options)) {
            return false;
        }
        $lists = $objWhiteboardStorage->getStorageList($options, $addrowStatus);

        $userKey              = $this->_userKey;
        $memberKey            = $options["member_key"];
        foreach ($lists as $key => $value) {
            $where         = "storage_id      = '".addslashes($value["storage_id"])."'";
            $where        .= " AND user_key   = '".addslashes($userKey)."'";
            if (empty($memberKey)){
                $where    .= " AND member_key = '".addslashes(0)."'";
            } else {
                $where    .= " AND member_key = '".addslashes($memberKey)."'";
            }
            if ($this->_obj_StorageFavorite->getStorageKey($where)) {
                $storageFavorite = true;
            } else {
                $storageFavorite = false;
            }
            if (!empty($value["update_datetime"])) {
                $datetime = $value["update_datetime"];
            } else {
                $datetime = $value["create_datetime"];
            }

            $storageFileSize = $this->number_format($value["storage_file_size"], 1024);
            $datetime        = $objWhiteboardStorage->formatStorageDatetime($datetime);

            if ($value["member_key"]) {
                $member_id = $objWhiteboardStorage->getMemberId($memberKey);
            }
            $rtnArray[] = array_merge($value, array(
                "storage_file_size" => $storageFileSize,
                "storage_favorite"  => $storageFavorite,
                "storage_datetime"  => $datetime)
            );
        }
        return $rtnArray;
    }
*/
    /**
     * 共有資料一覧表示 TODO 小森修正中
     */
    private function render_top() {
        $options = $this->session->get("condition", $this->_name_space);
        $this->session->remove("condition");
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        $this->template->assign("user_key"    , $options["user_key"]);
        $this->template->assign("member_key"  , $options["member_key"]);
        $this->template->assign("common_box"  , $options["common_box"]);
        $this->template->assign("private_box" , $options["private_box"]);
        $this->template->assign("max_storage_size" , $user_info["max_storage_size"]);
        $this->template->assign("use_shared_storage" , $member_info["use_shared_storage"]);
        $default_folder = 0;
        if($options["member_key"] == 0){
            $default_folder = "00";
        }
        $this->template->assign("default_folder" , $default_folder);
        // サイズ計算
        $size = $this->number_format(($this->_obj_Storage->get_total_size($options["user_key"])),1024);
        $this->template->assign("size" , $size);
				
        $this->display('user/storage/list.t.html');
    }

    function getMessage($lang, $group, $id) {
        static $message;
        if (!$message[$lang]) {
            $config_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
            $message[$lang] = parse_ini_file($config_file, true);
        }
        return $message[$lang][$group][$id];
    }

    /**
     * ファイル削除 TODO
     */
    function action_delete() {
    	$__submit_key = $this->request->get('__submit_key');
		if ( ! $__submit_key || $__submit_key !== $this->session->get('__SUBMIT_KEY', $this->_name_space_toppage) ) {
    		$this->logger2->warn('error submit key');
			echo json_encode(array("error" => "error"));
    		return;
		}
    	
        $userKey            = $this->_userKey;
        $memberKey          = $this->_memberKey;
        $listOffset         = $this->request->get("offset");
        $storageIds         = $this->request->get('storage_ids');
        $terroitorys        = $this->request->get('territorys');
        $owners             = $this->request->get('owners');
        $memberKey          = $this->_memberKey;

        if(empty($storageIds)){
            $this->logger->warn("empty storage_ids");
            echo json_encode(array("error" => "error"));
            return;
        }

        // 正常なファイルかチェック
        foreach($storageIds as $key => $value) {
            $storage_info = $this->_obj_Storage->getStorageInfo($value);
            $this->logger2->debug($storage_info, '_debug storage info');
            if($userKey != $storage_info["user_key"] || $member_key != 0 && $member_key != $this->_memberKey){
                echo json_encode(array("error" => "DELETE ERROE CODE:1"));
                return;
            }
            $storage_infos[] = $storage_info;
        }
        // 削除
        foreach($storageIds as $key => $value) {
            $result = $this->_obj_Storage->delete_file($value,$userKey, $memberKey);
            if(!$result){
                echo json_encode(array("error" => "DELETE ERROE CODE:2"));
                return;
            }
        }

        // 動画データの処理
        foreach($storage_infos as $info){
            if($info["category"] == "video"){
                $this->action_clip_delete_done($info["clip_key"] , $info["storage_file_key"]);
            }

        }

        echo json_encode("success");
    }

     /*
      * TODO 4970 変換キャンセル処理(削除も行われる)
      */
    function action_cancel(){
    	$__submit_key = $this->request->get('__submit_key');
		if ( ! $__submit_key || $__submit_key !== $this->session->get('__SUBMIT_KEY', $this->_name_space_toppage) ) {
    		$this->logger2->warn('error submit key');
    		echo json_encode(array("error" => "error"));
    		return;
    	}
    	
        $userKey            = $this->_userKey;
        $storage_id         = $this->request->get('storage_id');
        $memberKey          = $this->_memberKey;
        if(empty($storage_id)){
            $this->logger->warn("empty storage_ids");
            echo json_encode(array("error" => "error"));
            return;
        }

        $storage_info = $this->_obj_Storage->getStorageInfo($storage_id);
        if($userKey != $storage_info["user_key"]){
            echo json_encode(array("error" => "CANCEL ERROE CODE:1"));
            return;
        }

        // doc、imageのキャンセル
        if($storage_info["category"] == "document" || $storage_info["category"] == "image" ){
            $result = $this->_obj_Storage->cancel_file($storage_info , $userKey , $memberKey);
            if(!$result){
                echo json_encode(array("error" => "CANCEL ERROE CODE:2"));
                return;
            }
        }
        // 動画のキャンセル
        if($storage_info["category"] == "video"){
            // ここではステータスを削除に変えるだけにする
            $result = $this->_obj_Storage->setStatus($storage_id , STORAGE_STATUS_DELETE , $memberKey);
            if(!$result){
                echo json_encode(array("error" => "CANCEL ERROE CODE:3"));
                return;
            }
        }

        echo json_encode($result);
    }



    function action_modifyFolderDoc() {
    	if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn('error submit key');
    		echo json_encode(array("error" => "error"));
    		return;
    	}
    	
        $options             = array();
        $userKey             = $this->_userKey;
        $memberKey           = $this->_memberKey;
        $options["user_id"]  = $this->_userId;
        $options["user_key"] = $userKey;
        $objWhiteboardStorage    = $this->_obj_Storage;

        if (empty($memberKey)) {
            $options["member_key"] = "0";
        } else {
            $options["member_key"] = $memberKey;
        }
        $options["storage_ids"]    = $this->request->get('storage_ids');
        $addFolderIds = array_flip($this->request->get('add_folder_ids'));
        unset($addFolderIds[""]);
        $options["add_folder_ids"] = array_flip($addFolderIds);

        $deleteFolderIds = array_flip($this->request->get('del_folder_ids'));
        unset($deleteFolderIds[""]);
        $options["del_folder_ids"] = array_flip($deleteFolderIds);

        $this->logger2->debug($options);

        $this->logger2->debug($options["storage_ids"]);

        $ret = $objWhiteboardStorage->modifyFolderDoc($options);
        $this->logger2->info($ret);

        echo json_encode($ret);
    }

    /**
     * デフォルトページ
     */
    function default_view()
    {
        $this->action_top();
    }

    /**
     * 入力フォーマットを整形 TODO 動画用の処理も追加する
     * 小森修正中
     */
    private function formatStorageData($format, $userKey, $memberKey) {
        $this->logger2->info($_FILES);
        // ファイル一覧
        if(empty($_FILES)) {
            return;
        }
//        $this->logger2->info($_FILES);

         // 対応フォーマット情報取得
        $lists = $this->_obj_Storage->getSupportFormat();
        $file = $_FILES["file"];
        $fileinfo = pathinfo($file["name"]);
        $string = new EZString();
        $storageKey = $string->create_id();
        $this->logger2->debug($file);
        $error = $file["error"];
        if ($error != 0) {
            $this->logger->error("error:" . $error);
            return;
        }

        if (0 == $file["size"]) {
            $error = 4;
        // 容量制限
        } else {
            if (in_array(strtolower($fileinfo["extension"]), $lists['image_ext_list'])) {
                $maxsize = $this->config->get("STORAGE", "max_image_size");
                $format = "bitmap";
                $category = "image";
            // 資料
            } else if (in_array(strtolower($fileinfo["extension"]), $lists['document_ext_list'])){
                $maxsize = $this->config->get("STORAGE", "max_file_size");
                $category = "document";
            }
            else if (in_array(strtolower($fileinfo["extension"]), $lists['video_ext_list'])) {
                $maxsize = $this->config->get("STORAGE", "max_video_size");
                $category = "video";
            }
            else{
                return;
            }
            if ($maxsize * 1024 * 1024 < $file["size"] ) {
                $error = 4;
            }
        }

        if (0 != $error) {
            $this->logger->error("error size over");
            return 4;
        }
        $dir = $this->get_work_dir();
//        $this->logger->info($dir);

        if($category != "video"){
            $tmpName = $dir."/".$storageKey.".".$fileinfo["extension"];
            move_uploaded_file($file["tmp_name"], $tmpName);
        }else{
            $tmpName = $file["tmp_name"];
        }
        $name = $file["name"];
        $convetFormat = $format ? $format : "bitmap";
        $retArray = array(
            "name"        => $name,
            "type"        => $file["type"],
            "tmp_name"    => $tmpName,
            "error"       => $error,
            "size"        => $file["size"],
            "format"      => $convetFormat,
            "extension"   => $fileinfo["extension"],
            "category"    => $category,
        );
        $this->logger2->debug($retArray);

        return $retArray;
    }
    /**
     * 入力チェック
     */
    function check($lists, $method = "") {
        if ($method != "delete") {
            $message = "";
        }
        $objWhiteboardStorage = $this->_obj_Storage;

        // 事前ファイルアップロード
        if(isset($_FILES["whiteboard_storage"])) {
            // 対応フォーマット取得
            $supportDocumentList = $objWhiteboardStorage->getSupportFormat();
            foreach($_FILES["whiteboard_storage"]["name"] as $key => $filename) {
                if ($filename) {
                    $fileParam = pathinfo($filename);
                    $fileExtension = strtolower($fileParam["extension"]);
                    // 画像
                    if (in_array($fileExtension, $supportDocumentList['image_ext_list'])) {
                        $maxsize = $this->config->get("STORAGE", "max_image_size");
                    // 資料
                    } else {
                        $maxsize = $this->config->get("STORAGE", "max_file_size");
                    }
                    if ($_FILES["whiteboard_storage"]["size"][$key] > $maxsize * 1024 * 1024) {
                        $message .= '<li>'.$this->get_message("ERROR", "maxfilesize"). '</li>';
                    }
                }
            }
        }
        // 入力エラー
        if ($message) {
            $this->logger->info(array($lists, $message));
        }
        return $message;
    }

    /**
     * プレビュー取得 TODO
     */
    public function action_show_preview()
    {
        $request = $this->request->getAll();
        //$this->logger->info($request);
        $objStorage = $this->_obj_Storage;
        $storageKey = $request["storage_file_key"];
        $member_key = $request["member_key"];

        if ($member_key != 0 && $member_key != $this->_memberKey) {
        	$storage_info = null;
        }else{
        	$storage_info = $objStorage->getStorageDetail($storageKey, $member_key, $this->_userKey);
        }
        
        if( DB::isError($storage_info) || empty($storage_info) ){
        	$this->alert($storage_info);
        }
        
        if ($storage_info["format"] == "vector" && $storage_info["category"] == "document") {
            $return["fileName"] = sprintf( "%s_%d.swf", $storage_info["document_id"], $request["index"] );
            $path = N2MY_DOC_DIR.$storage_info["file_path"].$storage_info["document_id"]."/".$return["fileName"];
            //$this->logger2->info($path);

            if (file_exists($path)){
                header('Content-type: application/x-shockwave-flash');
                header('Content-Length: '.filesize($path));
                $fp = fopen($path, "r");
                if ($fp) {
                    while (!feof($fp)) {
                        $buffer = fgets($fp, 4096);
                        echo $buffer;
                    }
                    fclose($fp);
                }
            }
            return;
        }

        $fileInfo = $objStorage->getPreviewFileInfo( $storage_info, $request["index"] );
        $this->logger->info($fileInfo);

        //resize
        $max_width  = 480;
        $max_height = 320;
        $percent    = 0.5;

        list($width, $height) = getimagesize($fileInfo["targetImage"]);
        $x_ratio = $max_width / $width;
        $y_ratio = $max_height / $height;

        if( ($width <= $max_width) && ($height <= $max_height) ){
            $tn_width  = $width;
            $tn_height = $height;
        } elseif (($x_ratio * $height) < $max_height) {
            $tn_height = ceil($x_ratio * $height);
            $tn_width  = $max_width;
        } else {
            $tn_width  = ceil($y_ratio * $width);
            $tn_height = $max_height;
        }

        header('Content-type: image/jpeg');

        // 再サンプル
        $image_p = imagecreatetruecolor($tn_width, $tn_height);
        $image = imagecreatefromjpeg($fileInfo["targetImage"]);

        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
        imagejpeg($image_p, null, 100);
    }

    function action_upload() {
        $offset = $this->request->get("offset");
        
        parent::set_submit_key($this->_name_space);
        $this->template->assign('sort_key'  , $this->request->get('sort_key'));
        $this->template->assign('user_key'   , $this->_userKey);
        $this->template->assign('member_key', $this->_memberKey);
        $this->template->assign('offset'    , $offset);
        
        $this->display('user/storage/upload.t.html');
    }
   /**
    * ファイルアップ完了
    * TODO 小森修正中
    */
    function action_uploadComplete() {
		if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn('error submit key');
			echo json_encode(array("error" => "error"));
			return;
		}
        $userKey          = $this->_userKey;
        $memberKey        = $this->_memberKey;
        $format           = $this->request->get('format');
        $user_info = $this->session->get("user_info");
        $objStorage = $this->_obj_Storage;
        $total_size = $this->_obj_Storage->get_total_size($userKey);
        if($total_size > $user_info["max_storage_size"] * 1024 * 1024){
            $this->logger->error("size over");
            echo json_encode(array("error" => "size over"));
            return;
        }
        if(!($createDocument = $this->formatStorageData($format, $userKey, $memberKey))) {
            $this->logger->error("format error");
            echo json_encode(array("error" => "format error"));
            return;
        }

        if($createDocument == 4){
            $this->logger->error("FILE size over");
            echo json_encode(array("error" => "size error"));
            return;
        }
//      $this->logger2->info($createDocument);
        if($createDocument["category"] == 'video'){
            $n2my_clip  = new N2MY_Clip($this->get_dsn());
            $params = array(
                    'title'       => $createDocument['name'],
                    'user_key'    => $userKey,
                    );
            try {
                $clip_id = $n2my_clip->add($params, $createDocument["tmp_name"]);
                $add_res = $objStorage->add($userKey, $memberKey, $createDocument["tmp_name"], $createDocument["name"], $createDocument["size"], $format, $createDocument["category"], $createDocument["extension"], "as2",null,$clip_id);
                if($add_res === false){
                    echo json_encode(array("error" => "DB error"));
                    return;
                }
            } catch (Exception $e) {
                    $this->logger2->error($e->getMessage());
                    echo json_encode(array("error" => "DB error"));
                    return;
            }
        }else{
            $document_id = $objStorage->addDocument($createDocument["tmp_name"], $createDocument["name"], $createDocument["format"], $userKey, $memberKey);
            // DBへ登録
            $add_res = $objStorage->add($userKey, $memberKey, $createDocument["tmp_name"], $createDocument["name"], $createDocument["size"], $format, $createDocument["category"], $createDocument["extension"], "as2",$document_id);
            if($add_res === false){
                echo json_encode(array("error" => "DB error"));
                return;
            }
        }
        // tmpファイルを一応削除処理 (容量圧迫の原因になっていた)
        if(is_file($createDocument["tmp_name"])){
            unlink($createDocument["tmp_name"]);
        }

        $request = array(
            "searchType" => "all",
            "sort_key"   => $this->session->get("sort_key"),
            "sort_type"  => $this->session->get("sort_type"),
        );

        $list = $this->action_searchList($request);
        $size = array(
            "use_size" => $this->number_format(($this->_obj_Storage->get_total_size($userKey)),1024),
            "max_size" => $user_info["max_storage_size"],
        );

        $account = array(
                "user_key" => $userKey,
                "member_key" => $memberKey,
                );


/*
        $objStorage->modifyCharges($userKey, $memberKey, $createDocument["size"], true);
*/
        //追加行を取得
        echo json_encode(array("list" => $list , "size" => $size , "account" => $account));
      //  echo json_encode($this->action_addListRow(true));
    }

    /**
     * TODO ファイルを移動する
     */
    function action_move() {
    	if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn('error submit key');
    		echo json_encode(array("error" => "error"));
    		return;
    	}
    	
        $userKey     = $this->_userKey;
        $memberKey   = $this->_memberKey;
        $storageIds = $this->request->get('storage_ids');
        // 移動先
        $toMemberkey = $this->request->get('toMemberKey');
        $toFolder    = $this->request->get('toFolder');

        if(empty($storageIds)){
            $this->logger->warn("empty storage_ids");
            echo json_encode(array("error" => "error"));
            return;
        }

        $objStorage = $this->_obj_Storage;
        // 正常なファイルかチェック
        foreach($storageIds as $key => $value) {
            $storage_info = $objStorage->getStorageInfo($value);
            if($userKey != $storage_info["user_key"]){
                echo json_encode(array("error" => "MOVE ERROE CODE:1"));
                return;
            }
            $storage_infos[] = $storage_info;
        }

        $folder_info = $objStorage->getFolderInfo($toFolder);
        if($toFolder != 0 && ($folder_info["status"] != 1 || $folder_info["user_key"] != $userKey)){
            echo json_encode(array("error" => "MOVE ERROE CODE:3"));
            return;
        }

        foreach($storageIds as $key => $value) {
            $result = $objStorage->moveFile($value , $toMemberkey , $toFolder , $memberKey, $userKey);
            if(!$result){
                echo json_encode(array("error" => "MOVE ERROE CODE:2"));
                return;
            }
            //$this->logger2->info($result);
        }

        echo json_encode("success");


    }

    /**
     * TODO ファイルをコピーする
     */
    function action_copy() {
    	if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn('error submit key');
    		echo json_encode(array("error" => "error"));
    		return;
    	}
    	
        $userKey    = $this->_userKey;
        $memberKey  = $this->_memberKey;
        $listOffset = $this->request->get("offset");
        $territorys = $this->request->get('territorys');
        $storageIds = $this->request->get('storage_ids');
         // 移動先
        $toMemberkey = $this->request->get('toMemberKey');
        $toFolder    = $this->request->get('toFolder');

        $user_info = $this->session->get("user_info");

        $storageId = array();
        if(empty($storageIds)){
            $this->logger->warn("empty storage_ids");
            echo json_encode("error");
            return;
        }
        $objStorage = $this->_obj_Storage;
        $total_size = $objStorage->get_total_size($userKey);

        // 正常なファイルかチェック
        foreach($storageIds as $key => $value) {
            $storage_info = $objStorage->getStorageInfo($value);
            if($userKey != $storage_info["user_key"]){
                echo json_encode(array("error" => "COPY ERROE CODE:1"));
                return;
            }
            $storage_infos[] = $storage_info;
            $total_size = $total_size + $storage_info["file_size"];
        }

        $folder_info = $objStorage->getFolderInfo($toFolder);
        if($toFolder != 0 && ($folder_info["status"] != 1 || $folder_info["user_key"] != $userKey)){
            echo json_encode(array("error" => "COPY ERROE CODE:2"));
            return;
        }

        if($total_size > $user_info["max_storage_size"] * 1024 * 1024){
            echo json_encode(array("error" => "size over"));
            return;
        }

        foreach($storageIds as $key => $value) {
            $result = $objStorage->copyFile($value , $toMemberkey , $toFolder , $memberKey,$userKey);
            if($result === false){
                echo json_encode(array("error" => "COPY ERROE CODE:3"));
                return;
            }
        }


        echo json_encode("success");

/*
        $error = array();
        foreach($storageIds as $key => $value) {
            $storageKey  = $objWhiteboardStorage->getStorageKey($value);
            $storageName = $objWhiteboardStorage->getStorageName($value);
            $territory   = $territorys[$key];

            if (!$objWhiteboardStorage->isSelectedFile($storageKey, $userKey, $memberKey, $territory)) {
                $this->logger->error("empty selected file");
                $error[] = array("name" => $storageName, "message" => "duplicate");
                continue;
            }
            $ret = $objWhiteboardStorage->fileCopy($storageKey, $userKey, $memberKey, $territory);
            if (!empty($ret)) {
                $error[] = array("name" => $storageName, "message" => $ret["error"]);
                continue;
            }
            $size = $objWhiteboardStorage->getStorageFileSize($value);

            $objWhiteboardStorage->modifyCharges($userKey, $memberKey, $size, true);
        }
        //追加行を取得
        echo json_encode(array_merge($this->action_addListRow(true, $listOffset), array("copy_error" => $error)));
*/
    }

    function action_download() {
        $get = $this->request->getAll();
        $storageIds = array();
        foreach ($get as $key => $value) {
            if ("action_download" != $key) {
                $storageIds[$key] = $value;
            }
        }
        $this->logger->info($storageIds);

        $ret = $this->_obj_Storage->downloadFile($storageIds, $this->_userKey, $this->_memberKey);
        if (isset($ret)) {
            echo json_encode($ret);
        }
    }

    /**
     * お気に入りセット TODO 小森修正中
     */
    function action_favorite() {
    	$__submit_key = $this->request->get('__submit_key');
		if ( ! $__submit_key || $__submit_key !== $this->session->get('__SUBMIT_KEY', $this->_name_space_toppage) ) {
    		$this->logger2->warn('error submit key');
    		echo json_encode(array("error" => "error"));
    		return;
    	}
    	
        $userKey   = $this->_userKey;
        $memberKey = $this->_memberKey;
        $storageId = $this->request->get('storage_id');
        $checked   = $this->request->get('checked');
        if(empty($storageId)){
            $this->logger->error("empty storage_id");
            echo json_encode("error");
            return;
        }
        if ($checked) {
            $ret = $this->_obj_Storage->deleteFavorite($storageId, $userKey, $memberKey);
        } else {
            $ret = $this->_obj_Storage->addFavorite($storageId, $userKey, $memberKey);
        }
        if (isset($ret)) {
            echo json_encode($ret);
        }
    }

    function action_some_favorite() {
    	$__submit_key = $this->request->get('__submit_key');
		if ( ! $__submit_key || $__submit_key !== $this->session->get('__SUBMIT_KEY', $this->_name_space_toppage) ) {
    		$this->logger2->warn('error submit key');
    		echo json_encode(array("error" => "error"));
    		return;
    	}
    	
        $userKey    = $this->_userKey;
        $storageIds = $this->request->get('storage_ids');

        $this->logger2->info($storageIds);
        $checked = $this->request->get('checked');
        if(empty($storageIds)){
            echo json_encode("error");
            return;
        }
        foreach ($storageIds as $storageId) {
            if (is_bool($checked)) {
                $ret = $this->_obj_Storage->deleteFavorite($storageId, $userKey, $this->_memberKey);
            } else {
                $ret = $this->_obj_Storage->addFavorite($storageId, $userKey, $this->_memberKey);
            }

            if (isset($ret)) {
                echo json_encode($ret);
            }
        }

    }

    /**
     * TODO
     */
    function action_modifyName() {
    	parent::set_submit_key( $this->_name_space );
    	
        $offset             = $this->request->get("offset");
        $storage_key        = $this->request->get('storage_key');
//        $this->logger2->info($storageId);
        $objStorage   = $this->_obj_Storage;
//        $storageKey         = $objStorage->getStorageKey($storageId);
        $ret = $objStorage->getStorageInfo($storage_key);

        $this->template->assign('sort_key', $this->request->get('sort_key'));
        $this->template->assign('storage_key', $storage_key);
        $this->template->assign('storage_name', $ret['user_file_name']);
        $this->template->assign('user_key', $this->_userKey);
        $this->template->assign('member_key', $this->_memberKey);
        $this->display('user/storage/update_storage_name.t.html');
    }

    /**
     * TODO
     */
    function action_modifyNameComplete() {
    	if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn('error submit key');
    		echo json_encode(array("error" => "error"));
    		return;
    	}
    	
        $listOffset       = $this->request->get("offset");
        $storageName      = $this->request->get('storage_name');
        $storageKey       = $this->request->get('storage_key');
        $territory        = $this->request->get('territory');
        $objStorage = $this->_obj_Storage;

        $res = $objStorage->updateFileName($storageKey, $storageName, $this->_userKey);
        if(!$res){
            echo json_encode(array("error" => "error"));
            return;
        }
        $update_datetime = $objStorage->getStorageDatetime($storageKey);

        echo json_encode(array("user_file_name" => $storageName, "update_datetime" => $update_datetime));
    }

    private function number_format($number, $digit = 1000, $unit = array('Byte', 'KB', 'MB', 'GB', 'TB', 'PB'), $decimals = 2) {
        if ($number == 0) {
            $e = 0;
            $number_view = "0 ".$unit[0];
        } else {
            if ($number > 0) {
                $e = floor(log($number)/log($digit));
                // メガ(MB)以上になったら単位はメガ(MB)でそろえる
                if($e >= 2){
                    $e = 2;
                }
                if ($number >= 1000) {
                    $number_view = sprintf('%.'.$decimals.'f '.$unit[$e], ($number/pow($digit, floor($e))));
                } else {
                    $number_view = sprintf('%d '.$unit[$e], ($number/pow($digit, floor($e))));
                }
            }
        }
        return $number_view;
    }

    function nodeSearch($options, $folderIds) {
        $result = $this->_obj_Storage->getPartNode($options);
        $this->logger2->debug($result);
        $this->logger->info($result);
        foreach ($result as $columns) {
            $folderId = $columns["children_folder_id"];
            $searchResult = array_search($folderId, $folderIds);
            if (is_int($searchResult)) {
                $this->logger->info("equals sub folder_id");
                return true;
            }
        }
        $this->logger2->debug($searchResult);
        return false;
    }

    /**
     * TODO
     */
    function action_get_file_in_folder($request = null){
        $return_flag = true;
        if(!$request){
            $request = $this->request->getAll();
            $return_flag = false;
        }
        $storage_folder_key = $request['folder_key'];
        $user_key = $request['user_key'];
        $member_key = $request['member_key'];
        if ($request["sort_key"]) {
            $sort_key = $request["sort_key"];
            $sort_type = $request["sort_type"];
            $sort = array($sort_key => $sort_type);
            $this->session->set("sort_key" , $sort_key);
            $this->session->set("sort_type" , $sort_type);
        }
        if($user_key != $this->_userKey){
            echo json_encode(array("error" => "user_key error"));
            return;
        }

        $folders = $this->_obj_Storage->getInFolder($storage_folder_key, $member_key, $user_key);
//$this->logger2->info($folders);

        $where = "status !=3 AND storage_folder_key = " . $storage_folder_key . " AND storage_file.user_key=" . $user_key. " AND storage_file.member_key = " . $member_key;
        
		$list = $this->action_list($this->_obj_Storage->getList($where, $sort, 1000, 0, "*"));

        $user_info = $this->session->get("user_info");
        $size = array(
            "use_size" => $this->number_format(($this->_obj_Storage->get_total_size($this->_userKey)),1024),
            "max_size" => $user_info["max_storage_size"],
        );
        echo json_encode(array("list" => $list , "size" => $size , "folders" => $folders));
    }


    /**
     * ファイルリストを取得したらお気に入り確認やサイズの成形を行うため必ず通す TODO
     */
    function action_list($list){
        $list_count = count($list);
        $where = " user_key = " . $this->_userKey . " AND member_key = " . $this->_memberKey;
        // お気に入り取得
        $favoreite_list = array();
        $favoreite_data = $this->_obj_StorageFavorite->getRowsAssoc($where);
        // お気に入りリスト作成
        foreach($favoreite_data as $favoreite){
            $favoreite_list[] = $favoreite["storage_file_key"];
        }
        //$this->logger2->info($favoreite_list);
        for($i = 0; $i < $list_count; $i++){
            // サイズ成形
            $list[$i]["file_size"] = $this->number_format($list[$i]["file_size"], 1024);
            // お気に入り確認
            if(in_array($list[$i]["storage_file_key"] , $favoreite_list)){
                $list[$i]["favorite"] = 1;
            }
        }
        return $list;
    }

    function action_format(){
        $this->display('user/storage/format.t.html');
    }

    /** TODO
     * 動画詳細表示
     */
    public function action_showDetail()
    {
    	parent::set_submit_key($this->_name_space);
    	
        $request = $this->request->getAll();
        $clip_info             = $this->_getClipInfo();
        $clip_info["duration"] = gmdate('H:i:s', $clip_info["duration"]);

        $storage_file_info = $this->_obj_Storage->getStorageInfo($request["storage_file_key"] );
        if( $storage_file_info["clip_key"] != $request["clip_key"] || $storage_file_info["user_key"] != $this->_userKey
        	|| $storage_file_info['member_key'] != 0 && $this->_memberKey != $storage_file_info['member_key']
        ){
            $this->alert($storage_file_info);
            $this->errorToExit(CLIP_ERROR_NOT_FOUND);
        }
        $meeting_list          = $this->_getMeetingClipList( $clip_info["clip_key"] , $request["storage_file_key"]);
        $vido_config           = $this->config->getAll( "VIDEO" );
        
        $this->template->assign("skin",        $vido_config["base_skin"]);
        $this->template->assign("skin_width",  $vido_config["base_skin_width"]);
        $this->template->assign("skin_height", $vido_config["base_skin_height"]);
        $this->template->assign("client_id",   $vido_config["client_id"]);
        $this->template->assign("clip_key",    $clip_info["clip_key"]);
        $this->template->assign( "clip_info",     $clip_info );
        $this->template->assign( "meeting_list",  $meeting_list );
        $storage_file_info["file_size"] = $this->number_format($storage_file_info["file_size"] , 1024);
        $this->template->assign( "storage_file_info",  $storage_file_info );
        $this->display( "user/storage/clip_detail.t.html" );
    }

    /** TODO
     * 動画詳細情報取得
     */
    private function _getClipInfo($clip_key = null)
    {

        if($clip_key == null){
            $request = $this->request->getAll();
        }else{
            $request["clip_key"] = $clip_key;
        }
        if( empty( $request["clip_key"] ) ){
            $this->logger2->error("clip key lost");
            $this->errorToExit(CLIP_ERROR_KEY_LOST);
        }

        $clip_obj = new ClipTable( $this->get_dsn() );

        $wheres[] = sprintf( "clip_key   = '%s'", mysql_real_escape_string( $request["clip_key"] ) );
        $wheres[] = sprintf( "user_key   = '%s'", mysql_real_escape_string( $this->_userKey ) );
        $wheres[] = sprintf( "is_deleted = '%s'", 0 );

        $rs = $clip_obj->getRow( implode( " AND ", $wheres ), "*", array(), 1 );
        if (DB::isError($rs) || empty($rs) ) {
            $this->alert($rs);
            $this->errorToExit(CLIP_ERROR_NOT_FOUND);
            exit;
        }

        return $rs;

    }

    /** TODO
     * 動画利用中ミーティング取得
     */
    private function _getMeetingClipList( $clip_key , $storage_file_key )
    {

        //ミーティングクリップからミーティング記録を取得
        $meeting_clip_obj = new MeetingClipTable( $this->get_dsn() );
        $wheres[] = sprintf( "clip_key   = '%s'", mysql_real_escape_string( $clip_key ) );
        $wheres[] = sprintf( "is_deleted = '%s'", 0 );
        $wheres[] = sprintf( "storage_file_key = '%s'", $storage_file_key );

        $rs = $meeting_clip_obj->select( implode( " AND ", $wheres ) );
        if( DB::isError($rs) || $rs === false ){
            $this->logger2->error("db error:clip_key=".$clip_key);
            return;
        }
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $meeting_keys[] = $row["meeting_key"];
        }

        if( empty( $meeting_keys ) )  return;

        //ミーティング情報を取得
        $user_info = $this->session->get( "user_info" );
        $meeting_obj = new MeetingTable( $this->get_dsn() );

        $wheres = array();
        $wheres[] = sprintf( "user_key = '%s'", $user_info["user_key"] );
        $wheres[] = sprintf( "meeting_key in ('%s')", implode( "','", $meeting_keys ) );

        $rs = $meeting_obj->select( implode( " AND ", $wheres ) );
        if( DB::isError($rs) || $rs === false ){
            $this->logger2->error("db error:meeting_key=" . implode( " AND ", $wheres ) );
            return;
        }

        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $ret[] = $row;
        }

        return $ret;

    }

    /** TODO
     * 動画編集実行
     */
    public function action_edit_done()
    {
    	if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn('error submit key');
    		$this->action_showDetail();
    		return;
    	}

        $clip_info = $this->_getClipInfo();
        $user_info = $this->session->get( "user_info" );

        $request = $this->request->getAll();

        $data["user_file_name"]       = $request["user_file_name"];
        $data["description"] = $request["description"];
        $data["last_update_member_key"] = $this->_memberKey;

        $where = sprintf( "clip_key = '%s' AND user_key = '%s' AND storage_file_key = '%s'", $request["clip_key"] ,  $this->_userKey ,  $request["storage_file_key"]  );

        $rs = $this->_obj_StorageTable->update( $data, $where );
        if( DB::isError($rs) ) {
            //$this->logger2->error("clip edit error");
            $this->errorToExit(CLIP_ERROR_EDIT);
        }
        $this->template->assign( "message", CLIP_INFO_EDIT );


        $this->action_showDetail();
    }

    /** TODO
     * 動画削除実行
     */
    private function action_clip_delete_done($clip_key ,$storage_file_key )
    {
        $clip_info             = $this->_getClipInfo($clip_key);
        $clip_info["duration"] = gmdate('H:i:s', $clip_info["duration"]);

        $user_info = $this->session->get( "user_info" );
        $meeting_clip_obj = new MeetingClipTable( $this->get_dsn() );
        $delete_data = array(
                "is_deleted" => 1
        );
        // ミーティングクリップも削除
        $meeting_clip_obj->update($delete_data, "storage_file_key = " . $storage_file_key);
        // 同クリップキーがないかチェック
        $where = sprintf("clip_key = '%s' AND status = %s" , $clip_key , 2);
        $res = $this->_obj_StorageTable->numRows($where);
        // 動画削除処理
        if($res == 0){
            try {
                $this->_obj_N2MY_Clip->delete( $clip_key, $user_info['user_key']);
            } catch (Exception $e) {
                $this->logger2->error( $e->getMessage() );
            }
        }
    }

    // 詳細画面からの削除 TODO
    function action_clip_delete(){
    	if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn('error submit key');
    		$this->action_showDetail();
    		return;
    	}
        $clip_info             = $this->_getClipInfo();
        $clip_info["duration"] = gmdate('H:i:s', $clip_info["duration"]);

        $request = $this->request->getAll();
        $storage_file_info     =$this->_obj_Storage->getStorageInfo($request["storage_file_key"] );

        $result = $this->_obj_Storage->delete_file($request["storage_file_key"],$this->_userKey, $this->_memberKey);
        $this->action_clip_delete_done($request["clip_key"], $request["storage_file_key"]);

        $user_info = $this->session->get( "user_info" );

        $this->template->assign( "clip_info", $clip_info );
        $this->template->assign( "storage_file_info", $storage_file_info );
        $this->display( "user/storage/clip_delete_done.t.html" );
    }

    // -------4930

    function action_getFolderTree($user_key = null , $member_key = null){
        $objWhiteboardStorage    = $this->_obj_Storage;
        if($user_key == null){
            $user_key = $this->_userKey;
        }
        if($member_key == null){
            $member_key = $this->_memberKey;
        }

        echo json_encode($this->getFolderTreeList($user_key,$member_key));

    }

    // フォルダをツリー構造で取得する
    private function getFolderTreeList($user_key = null , $member_key = null){
        $objWhiteboardStorage    = $this->_obj_Storage;
        if($user_key == null){
            $user_key = $this->_userKey;
        }
        if($member_key == null){
            $member_key = $this->_memberKey;
        }
        // 1の場合使えない
        $not_use = 1;
        if($member_key != null){
            $private_folder = $objWhiteboardStorage->getPrivateFolder($user_key,$member_key);
            $private_folder_tree =  $objWhiteboardStorage->setFolderTree($private_folder);
        }else{
            // 個人ストレージが使えな
            $private_folder_tree = $not_use;
        }

        $member_info  = $this->session->get("member_info");
        if($member_key == null || $member_info["use_shared_storage"] == 1){
            $shared_folder = $objWhiteboardStorage->getSharedFolder($user_key,$member_key);
            $shared_folder_tree =  $objWhiteboardStorage->setFolderTree($shared_folder);
        }else{
            // 共有ストレージが使えない
            $shared_folder_tree = $not_use;
        }
        return (array("private_folder_tree" => $private_folder_tree , "shared_folder_tree" => $shared_folder_tree));
    }

    // TODO フォルダ削除
    function action_deleteFolder() {
    	$__submit_key = $this->request->get('__submit_key');
		if ( ! $__submit_key || $__submit_key !== $this->session->get('__SUBMIT_KEY', $this->_name_space_toppage) ) {
			$this->logger2->warn( 'error submit key' );
			echo json_encode( array ( "error" => "error" ) );
			return;
		}
		
        $objWhiteboardStorage    = $this->_obj_Storage;
        $folder_key =  $this->request->get("folder_key");
        $folder_info = $objWhiteboardStorage->getFolderInfo($folder_key);
        $user_key = $this->_userKey;
        $member_key = $this->_memberKey;
        if($folder_info["user_key"] != $user_key || !$folder_info || $folder_key == 0){
            echo json_encode(array("error" => 1));
            return;
        }
        // 指定フォルダの削除
        $res = $objWhiteboardStorage->deleteFolder($folder_key , $folder_info["member_key"] , $folder_info["user_key"]);
        if($res === false){
            echo json_encode(array("error" => 1));
            return;
        }

        // 配下フォルダ削除
        $children_folder_info = $objWhiteboardStorage->getChildrenFolder($folder_key,$folder_info["member_key"]);
        $this->logger2->info($children_folder_info);
        foreach($children_folder_info as $folder){
            $objWhiteboardStorage->deleteFolder($folder["storage_folder_key"] , $member_key , $user_key);
        }

        if($folder_info["parent_storage_folder_key"] == 0){
            $parent_folder_info = array(
                    "storage_folder_key" => 0,
                    "user_key"           => $user_key,
                    "member_key"         => $folder_info["member_key"]
                    );
        }else{
            $parent_folder_info = $objWhiteboardStorage->getFolderInfo($folder_info["parent_storage_folder_key"]);
        }
        echo json_encode(array("parent_storage_folder" => $parent_folder_info));
    }

    // TODO 4930 フォルダ作成画面
    function action_addFolder() {
    	parent::set_submit_key($this->_name_space);
    	
        if($this->request->get("is_private")){
            $member_key = $this->_memberKey;
        }else{
            $member_key = 0;
        }

        $folder_key =  $this->request->get("folder_key");
        if($folder_key){
            $folder_info = $this->_obj_Storage->getFolderInfo($folder_key);
            $this->template->assign('folder_name', $folder_info["folder_name"]);
        }

        $this->template->assign('user_key'   , $this->_userKey);
        $this->template->assign('member_key', $member_key);
        $this->template->assign('parent_folder', $this->request->get("parent_folder"));
        $this->template->assign('folder_key', $folder_key);
        $this->display('user/storage/add_folder.t.html');
    }


    // TODO 4930 フォルダ作成
    function action_addCompleteFolder(){
    	$this->logger2->info($this->request->get('__submit_key'));
    	if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn($this->request->get('__submit_key'), 'error submit key');
    		echo json_encode(array("error" => "error"));
    		return;
    	}
    	
        $objWhiteboardStorage    = $this->_obj_Storage;
        $user_key = $this->_userKey;
        // member_keyがあれば個人ストレージに登録する
        if($this->request->get("member_key")){
            // 念のためセッション情報を使う
            $member_key = $this->_memberKey;
        }else{
            $member_key = 0;
        }
        $parent_folder = $this->request->get("parent_folder");
        $folder_name = $this->request->get("folder_name");

        $result = $objWhiteboardStorage->addFolder($parent_folder , $user_key , $member_key , $folder_name);
        if($parent_folder == 0){
            $parent_folder_info = array(
                    "storage_folder_key" => 0,
                    "user_key"           => $user_key,
                    "member_key"         => $member_key
            );
        }else{
            $parent_folder_info = $objWhiteboardStorage->getFolderInfo($parent_folder);
        }
        if($result != 1){
            echo json_encode(array("error" => $result));
        }else{
            echo json_encode(array("success" => $result , "parent_storage_folder" => $parent_folder_info));
        }
    }

    // TODO 4930 フォルダ名前変更
    function action_updataNameCompleteFolder(){
    	if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn('error submit key');
    		echo json_encode(array("error" => "error"));
    		return;
    	}
    	
        $objWhiteboardStorage    = $this->_obj_Storage;
        $folder_key = $this->request->get("folder_key");
        $folder_name = $this->request->get("folder_name");
        $user_key    = $this->_userKey;
        if($folder_key == 0){
            echo json_encode(array("error" => 1));
            return;
        }

        $folder_info = $objWhiteboardStorage->getFolderInfo($folder_key);
        if(!$folder_info || $folder_info["user_key"] != $user_key){
            echo json_encode(array("error" => 1));
            return ;
        }
        $result = $objWhiteboardStorage->updataNameFolder($folder_key , $folder_name);
        if($result != 1){
            echo json_encode(array("error" => $result));
        }else{
            echo json_encode(array("success" => $result , "folder_info" => $folder_info));
        }
    }

    // TODO 4930 フォルダ移動
    function action_moveFolder(){
    	if ( ! $this->request->get('__submit_key') || ! parent::check_submit_key( $this->_name_space ) ) {
    		$this->logger2->warn('error submit key');
    		echo json_encode(array("error" => -1));
    		return;
    	}
    	
        $folder_key  = $this->request->get("folder_key");
        $to_member_key = $this->request->get("toMemberKey");
        $to_folder_key    = $this->request->get("toFolder");
        $user_key    = $this->_userKey;
        $my_member_key = $this->_memberKey;

        if($folder_key == 0){
            echo json_encode(array("error" => -1));
            return;
        }
        $objWhiteboardStorage    = $this->_obj_Storage;
        $folder_info = $objWhiteboardStorage->getFolderInfo($folder_key);
        if(!$folder_info || $folder_info["user_key"] != $user_key){
            echo json_encode(array("error" => 1));
            return ;
        }

        if($to_folder_key != 0){
            $to_folder_info = $objWhiteboardStorage->getFolderInfo($to_folder_key);
            if(!$to_folder_info || $to_folder_info["user_key"] != $user_key){
                echo json_encode(array("error" => 1));
                return ;
            }
        }else{
            $key = 0;
            if($to_member_key == 0){
                $key = "00";
            }
            $to_folder_info = array(
                    "storage_folder_key" => $key,
                    "user_key"           => $user_key,
                    "member_key"         => $to_member_key
            );
        }

        $result = $objWhiteboardStorage->moveFolder($folder_key, $to_member_key, $to_folder_key , $my_member_key , $user_key);
        if($result != 1){
            echo json_encode(array("error" => $result));
            return;
        }
        echo json_encode(array("success" => $result, "folder_info" => $folder_info, "to_folder_info" => $to_folder_info));
    }

    // TODO 4930 フォルダコピー
    function action_copyFolder(){
    	parent::set_submit_key($this->_name_space);
    	
        $folder_key  = $this->request->get("folder_key");
        $toMemberKey = $this->request->get("toMemberKey");
        $toFolder    = $this->request->get("toFolder");
        $user_key    = $this->_userKey;
        echo json_encode("success");
    }

    // TODO 4930 フォルダ移動画面
    function action_moveFolderForm(){
    	parent::set_submit_key($this->_name_space);
    	
        $folder_key = $this->request->get("folder_key");
        $user_key = $this->_userKey;
        $member_key = $this->_memberKey;
        $this->template->assign('user_key'   , $user_key);
        $this->template->assign('member_key', $member_key);
        $this->template->assign("folder_key",$folder_key);
        $this->template->assign("folder_tree",$this->getFolderTreeList());
        $this->display('user/storage/move_folder_form.t.html');
    }

    // TODO 4930 フォルダコピー画面
    function action_copyFolderForm(){
    	parent::set_submit_key($this->_name_space);
    	
        $folder_key = $this->request->get("folder_key");
        $user_key = $this->_userKey;
        $member_key = $this->_memberKey;
        $this->template->assign('user_key'   , $user_key);
        $this->template->assign('member_key', $member_key);
        $this->template->assign("folder_key",$folder_key);
        $this->template->assign("folder_tree",$this->getFolderTreeList());
        $this->display('user/storage/copy_folder_form.t.html');
    }

    // TODO 4930 ファイル移動画面
    function action_moveFileForm(){
    	parent::set_submit_key($this->_name_space);
        $files = $this->request->get("files");
        $user_key = $this->_userKey;
        $member_key = $this->_memberKey;
        $this->template->assign('user_key'   , $user_key);
        $this->template->assign('member_key', $member_key);
        $this->template->assign("folder_tree",$this->getFolderTreeList());
        $this->display('user/storage/move_file_form.t.html');
    }

    // TODO 4930 ファイルコピー画面
    function action_copyFileForm(){
    	parent::set_submit_key($this->_name_space);
    	
        $files = $this->request->get("files");
        $user_key = $this->_userKey;
        $member_key = $this->_memberKey;
        $this->template->assign('user_key'   , $user_key);
        $this->template->assign('member_key', $member_key);
        $this->template->assign("folder_tree",$this->getFolderTreeList());
        $this->display('user/storage/copy_file_form.t.html');
    }

    /**
     * 不正アクセス検知
     * DB::isError の場合は不具合の可能性有り
     */
    private function alert($ret) {
    	$this->logger2->error($ret, 'storage Unauthorized access');
    }
    
    private function errorToExit($message){
    	$this->template->assign("message", $message);
    	$this->display("user/clip/error.t.html");
    	exit;
    }
}

$main = new AppStorage();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
