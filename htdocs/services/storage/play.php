<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Account.class.php");
require_once('classes/core/dbi/Meeting.dbi.php');
require_once("classes/N2MY_Storage.class.php");
require_once("classes/dbi/meeting.dbi.php");

class Service_LogPlayer extends AppFrame
{

    private $_obj_Storage      = null;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth() {
        $this->checkAuth();
        $serverInfo                    = $this->session->get("server_info");
        $this->_obj_Storage  = new N2MY_Storage($this->get_dsn(), $serverInfo["host_name"]);
        $this->obj_Meeting          = new MeetingTable($this->get_dsn());
    }

    function default_view()
    {
        $storage_key = $this->request->get('storage_key');
        $user_info = $this->session->get("user_info");
        $file_info = $this->_obj_Storage->getStorageInfo($storage_key);
        if($user_info["user_key"] != $file_info["user_key"]){
            return;
        }

        $personal_white_id = explode( "," , $file_info["personal_white_id"]);
        $meeting_key = $personal_white_id[0];
        $where       = sprintf("meeting_key='%s' AND user_key='%s'", $meeting_key, $user_info["user_key"]);
        $meeting_info = $this->obj_Meeting->getRow($where);

        $width  = 640;
        $height = 480;

        $file_url =  sprintf( "%s%s%s", N2MY_BASE_URL."docs/", $file_info["file_path"], $file_info["file_name"] );
        //assign values
        $this->template->assign('charset', $this->config->get( "GLOBAL", "html_output_char" ) );
        $this->template->assign('locale' , $this->get_language() );
        $this->template->assign('width' , $width );
        $this->template->assign('height' , $height );
        $this->template->assign('file_url' , $file_url );
        //server host key
        $this->template->assign("server_dsn_key", $this->get_dsn_key() );
        $this->template->assign("meeting_key", $meeting_info["meeting_session_id"] );
        $this->template->assign("core_session_key", $meeting_info["meeting_session_id"] );
        $this->template->assign("auth_key", N2MY_FLASH_API_AUTH_KEY );
        $max_tmp_file_size = $this->config->get("N2MY", "max_tmp_file_size") ? $this->config->get("N2MY", "max_tmp_file_size") : 50000000;
        $this->template->assign("maxTmpFileSize", $max_tmp_file_size );
        $this->template->assign("meeting_version", $user_info["meeting_version"] );
        //$this->logger2->info($file_url);
        // service info
        $frame["service_info"] = $this->get_message("SERVICE_INFO");
        $this->template->assign("__frame", $frame);
        $this->template->assign("api_url", N2MY_BASE_URL);
        $content_delivery_base_url = $this->get_content_delivery_base_url();
        $this->template->assign("content_delivery_base_url", $content_delivery_base_url);
        $this->display('user/storage/personal_whiteboard_as3.t.html');
    }

    private function get_content_delivery_base_url() {
        $protocol = '';
        $content_delivery_base_url = '';
        $country_key = $this->session->get("country_key");

        if($this->config->get("CONTENT_DELIVERY", "is_enabled", false)) {
            $country_key = $this->session->get("country_key");
            // 中国からの入室の場合、データの参照元をCDNからではなくBravからに設定する。
            $content_delivery_host = $this->config->get("CONTENT_DELIVERY_HOST", $country_key, "");
            if($content_delivery_host) {
                if(strlen($content_delivery_host)) {
                    $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
                }
                $content_delivery_base_url = $protocol . $content_delivery_host;
                $meeting_file_dir = $this->config->get("CONTENT_DELIVERY", "meeting_file_dir", "");
                if(strlen($meeting_file_dir)) {
                    $content_delivery_base_url .= '/' . $meeting_file_dir;
                }
            }
        }
        return $content_delivery_base_url;
    }
}

$main =& new Service_LogPlayer();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
