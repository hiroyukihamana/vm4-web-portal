<?php
require_once('classes/AppFrame.class.php');
require_once('classes/dbi/address_book.dbi.php');
require_once('classes/dbi/member.dbi.php');
require_once("lib/pear/Pager.php");
require_once ('classes/dbi/address_book_group.dbi.php');
require_once ('classes/dbi/member_group.dbi.php');

class AppAddress extends AppFrame
{
    var $logger = null;
    var $_name_space = null;
    var $_form = array();
    var $objAddressBook = null;
    var $objAddressBookGroup = null;
    var $objMember = null;
    var $objMemberGroup = null;
    var $_check = null;
    /**
     * 一ページの表示件数
     */
    var $_limit = 10;

    /**
     * 初期処理
     */
    function init()
    {
        $this->logger =& EZLogger2::getInstance();
        $this->_name_space = md5(__FILE__);
        $this->objAddressBook = new AddressBookTable($this->get_dsn());
        $this->objAddressBook->max_address_cnt = $this->config->get("N2MY", "address_book_count", 500);
        $this->objMember = new MemberTable($this->get_dsn());
        $this->objAddressBookGroup = new AddressBookGroupTable($this->get_dsn());
        $this->objMemberGroup = new MemberGroupTable($this->get_dsn());
        
        $this->template->assign('is_not_display_portal_header', true);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        if ( "free" == $user_info["account_model"] || "free" == $member_info["member_type"] ) {
            $this->display("user/404_error.t.html");
            exit();
        }
    }

    /**
     * 追加／更新
     *
     * @todo 入力チェック
     * @todo 追加／更新の処理は分けた方がいい
     */
    function action_edit()
    {
        $submit_key = $this->session->get('__SUBMIT_KEY', $this->_name_space);
        $in = $this->request->get("__submit_key");
        if ($in !== $submit_key) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_member_list();
        }
        $request = $this->request->getAll();;
        $this->logger2->info($request);
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
    $address_book_group_name = $request["address_book_group_name"];
    $where = "group_name = '".$address_book_group_name."' AND user_key = ".$user_info["user_key"]." AND status!=0";
    $address_book_group  = $this->objAddressBookGroup->getRow($where);
    $data = array(
            "name"      => $request["name"],
            "name_kana" => $request["name_kana"],
            "email"     => $request["email"],
            "timezone"  => $request["timezone"],
            "lang"      => $request["lang"],
            "user_key"  => $user_info["user_key"],
            "is_deleted" => 0,
          "address_book_group_key" => empty($address_book_group["address_book_group_key"])?0:$address_book_group["address_book_group_key"]
        );
        // メンバー
        if ($member_info) {
            $data["member_key"] = $member_info["member_key"];
        }
        // 入力チェック
        $objCheck =& $this->objAddressBook->check($data);
        // 登録件数チェック
        $err_msg = array();

        if (EZValidator::isError($objCheck)) {

            $fields = $objCheck->error_fields();
            foreach($fields as $field) {
                $type = $objCheck->get_error_type($field);
                $err_msg["errors"][] = array(
                    "field"   => $field,
                    "err_cd"  => $type,
                    "err_msg" => sprintf($this->get_message("ERROR", $type), $objCheck->get_error_rule($field, $type)),
                    );
            }
        }
        //言語リストのバリデーション
        $language_list = $this->get_language_list();
        if(! EZValidator::valid_language( $data['lang'],$language_list ))
    {
      $lang_err = array (
          "field"   => "lang",
          "err_cd"  => "list",
          "err_msg" => MEMBER_ERROR_LANGUAGE,
          );
      if(empty($err_msg))
        $err_msg["errors"][0] = $lang_err;
      else array_push($err_msg["errors"],$lang_err);
        }
        //$this->logger->info($err_msg);

        if(!empty($err_msg))
        {
      $data = array(
      "status" => false,
      "data"   => $err_msg,
      );
        }else {
            $where = " user_key = ".addslashes($user_info["user_key"]).
                     " AND email = '".addslashes($request["email"])."'";
            if ($member_info["member_key"]) {
                $where .= " AND member_key = ".addslashes($member_info["member_key"]);
            } else {
                $where .= " AND member_key IS NULL";
            }
            //有効データ参照
            $where_m = $where." AND is_deleted = 0";
            if ($request["id"]) {
                $where_m = $where_m." AND address_book_key != ".addslashes($request["id"]);
            }

            $_m_list = $this->objAddressBook->numRows($where_m);
            //すでに削除されてるデータ数
            $where_d = $where." AND is_deleted = 1";
            $_d_list = $this->objAddressBook->numRows($where_d);
            // 追加
            $error = null;
            if ($request["id"] == "" && $_m_list == 0 && $_d_list == 0) {
                $insert_id = $this->objAddressBook->add($data);
                $status = true;
            // すでに有効データある場合はエラー
            } else if ($_m_list > 0) {
                $status = false;
                $field = "email";
                $type  = "duplicate";
                $error["errors"][] = array(
                            "field"   => 'email',
                            "err_cd"  => $type,
                            "err_msg" => sprintf($this->get_message("ERROR", $type), $objCheck->get_error_rule($field, $type)),
                        );
            //削除済みのデータがあった場合は情報更新
            } else if ($_m_list == 0 && $_d_list > 0) {
                $delete_data = $this->objAddressBook->getRow($where);
                $where = "address_book_key = ".addslashes($delete_data["address_book_key"]).
                    " AND user_key = ".addslashes($user_info["user_key"]);

                if ($member_info["member_key"]) {
                    $where .= " AND member_key = ".addslashes($member_info["member_key"]);
                } else {
                    $where .= " AND member_key IS NULL";
                }
                $insert_id = $this->objAddressBook->update($data, $where);
                $status = true;
            } else {
                $where = "address_book_key = ".addslashes($request["id"]).
                    " AND user_key = ".addslashes($user_info["user_key"]);

                if ($member_info["member_key"]) {
                    $where .= " AND member_key = ".addslashes($member_info["member_key"]);
                } else {
                    $where .= " AND member_key IS NULL";
                }
                $insert_id = $this->objAddressBook->update($data, $where);
                $status = true;
            }
            $data = array(
                "status" => $status,
                "data"   => $error,
                );
        }
        //チェックに問題がなければ Submit_Key削除
        if($status == true ) {
            $this->session->remove('__SUBMIT_KEY', $this->_name_space);
        }
        print json_encode($data);
    }

    /**
     * ユーザの削除を行う
     *
     * @todo 未実装
     * @todo 入力チェック
     * @todo 二重処理対応
     */
    function action_delete() {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_address_list();
        }
        $user_info = $this->session->get("user_info");
        $keys = $this->request->get("key", array());
        $address_book_keys = array();
        if (is_array($keys)) {
            foreach ($keys as $key) {
                if (is_numeric($key)) {
                    $address_book_keys[] = $key;
                }
            }
        }
        if ($address_book_keys) {
            $delete_list = join($address_book_keys, ",");
            $where = "is_deleted = 0".
                " AND address_book_key in (".addslashes($delete_list).")" .
                " AND user_key = ".$user_info["user_key"];
            if ($member_info = $this->session->get("member_info")) {
                $where .= " AND member_key = ".$member_info["member_key"];
            }
            $data = array(
                "is_deleted" => 1,
                "update_datetime" => date("Y-m-d H:i:s"),
                );
            $this->objAddressBook->update($data, $where);
        }
        $this->render_address_list();
    }

    /**
     * メンバー一覧
     */
    function action_member_list() {
        $room_key = $this->request->get("room_key");
        if ($room_key) {
            $this->session->set("room_key", $room_key, $this->_name_space);
        }
        $clear = $this->request->get("reset", null);
        if ($clear) {
          $this->session->remove("session_address_list");
        }
        $this->render_member_list();
    }

    /**
     * アドレス帳トップ画面表示
     */
    function action_address_list() {
        $clear = $this->request->get("reset", null);
        if ($clear) {
          $this->session->remove("session_address_list");
        }
        $this->render_address_list();
    }

    function action_check_address() {
      $check_status = $this->request->get("check_status");
      $address_id = $this->request->get("id");
      $type = $this->request->get("type");
      if($type == "addressbook") {
        if($check_status == "true") {
          //get address
          $where = "address_book_key ='$address_id'";
          $row = $this->objAddressBook->getRow($where);
          $data["address_book_key"] = $row["address_book_key"];
          $data["member_key"] = "";
          $data["name"] = $row["name"];
          $data["email"] = $row["email"];
          $data["lang"] = $row["lang"];
          $data["timezone"] = $row["timezone"];
          //save in session
          $session_address_list = $this->session->get("session_address_list");
          $session_address_list[$address_id] = $data;
          $this->logger2->info($session_address_list);
          $this->session->set("session_address_list", $session_address_list);
        } else {
          //delete in session
          $session_address_list = $this->session->get("session_address_list");
          unset($session_address_list[$address_id]);
          $this->session->set("session_address_list", $session_address_list);
        }
      } elseif($type == "member") {
      if($check_status == "true") {
          //get address
          $where = "member_key ='$address_id'";
          $row = $this->objMember->getRow($where);
          $data["address_book_key"] = $row["member_key"];
          $data["member_key"] = $row["member_key"];
          $data["name"] = $row["member_name"];
          $data["email"] = $row["member_email"];
          $data["lang"] = $row["lang"];
          $data["timezone"] = $row["timezone"];
          //save in session
          $session_address_list = $this->session->get("session_address_list");
          $session_address_list[$address_id] = $data;
          $this->logger2->info($session_address_list);
          $this->session->set("session_address_list", $session_address_list);
        } else {
          //delete in session
          $session_address_list = $this->session->get("session_address_list");
          unset($session_address_list[$address_id]);
          $this->session->set("session_address_list", $session_address_list);
        }
      }
    }

    /**
	 * 全て選択と全て選択解除 
	 */
    function action_select_all(){
    	$ids                  = $this -> request -> get('id');
    	$type                 = $this -> request -> get('type');
		$select_flag          = $this -> request -> get('select_flag');
		$session_address_list = $this -> session -> get('session_address_list');
		if(!$select_flag){
			foreach ($ids as $id) {
				if(array_key_exists($id, $session_address_list)){
					unset($session_address_list[$id]);
				}
			}
		}else{
			if($type == 'address'){
				foreach ($ids as $id) {
					if(array_key_exists($id, $session_address_list)){
						continue;
					}
					$where = 'address_book_key = ' . $id;
					$row   = $this -> objAddressBook -> getRow($where);
	    			$data['address_book_key'] = $row['address_book_key'];
	    			$data['name']             = $row['name'];
	    			$data['email']            = $row['email'];
	    			$data['lang']             = $row['lang'];
	    			$data['timezone']         = $row['timezone'];
	    			$session_address_list[$id] = $data;
				}
			}elseif($type == 'member'){
				foreach ($ids as $id) {
					if(array_key_exists($ids, $session_address_list)){
						continue;
					}
					$where = 'member_key = ' . $id;
	    			$row   = $this -> objMember -> getRow($where);
	    			$data['member_key']       = $row['member_key'];
	    			$data['address_book_key'] = $row['member_key'];
	    			$data['name']             = $row['member_name'];
	    			$data['email']            = $row['member_email'];
	    			$data['lang']             = $row['lang'];
	    			$data['timezone']         = $row['timezone'];
	    			$session_address_list[$id] = $data;
				}
			}
		}
		$this->session->set("session_address_list", $session_address_list);
    }
    
    public function action_invitationList()
    {
        $this->session->set("invitation", 1, $this->_name_space);
        $this->session->set("meeting_key", $this->request->get("meeting_key"), $this->_name_space);
        $this->default_view();
    }

    /**
     * ユーザ追加画面
     */
    function action_form() {
        $this->render_form();
    }

    function default_view() {
      $clear = $this->request->get("reset", null);
      if ($clear) {
        $this->session->remove("session_address_list");
      }
        // アドレス帳を表示
        /* 会議内よりアドレス帳を表示 */
        if ($this->request->get("invitation")) {
            $this->session->set("invitation", 1, $this->_name_space);
            if (!$room_key = $this->request->get("room_key")) {
                $this->logger2->error($room_key);
                $room_info = null;
            } else {
                $this->session->set("room_key", $room_key, $this->_name_space);
            }
            $this->session->set("room_info", $room_info, $this->_name_space);
            $this->session->set("meeting_key", $this->request->get("meeting_key"), $this->_name_space);
        }
        else {
            if ($room_key = $this->request->get("room_key")) {
                $this->session->set("room_key", $room_key, $this->_name_space);
            }
            $this->session->remove("invitation", $this->_name_space);
            $this->session->set("meeting_key", $this->_name_space);
        }
        if ($this->config->get("IGNORE_MENU", "display_address") == 0 && $this->session->get("service_mode") == "sales") {
            return $this->render_address_list();
        } elseif ($this->config->get("IGNORE_MENU", "admin_member") == 0) {
            return $this->render_member_list();
        } elseif ($this->config->get("IGNORE_MENU", "display_address") == 0) {
            return $this->render_address_list();
        }
        return $this->render_address_list();
    }

    /**
     * トップ画面表示
     */
    function render_address_list() {
        $a_list = $this->session->get("condition", $this->_name_space);
        $as_key  = $a_list["sort"]["key"];
        $as_type = $a_list["sort"]["type"];

        if($as_type == "down" || $as_type == NULL || 1 == $this->request->get("reset")) {
          $form_data = $this->set_form("name", "down");
        } else {
          $form_data = $this->set_form($as_key, $as_type);
        }
        $sort_key  = $form_data["sort"]["key"];
        $sort_type = $form_data["sort"]["type"];
        $page      = $form_data["sort"]["page"];
        $limit     = $form_data["sort"]["page_cnt"];
        $user_info = $this->session->get("user_info");

        $this->logger->debug($user_info);
        $offset    = ($limit * ($page - 1));
        $columns = $this->objAddressBook->getTableInfo("data");
        $where = " where address_book.is_deleted = 0"." AND address_book.user_key = ".$user_info["user_key"];
        if ($member_info = $this->session->get("member_info")) {
            if (!$form_data["request"]["type"]) {
                $where .= " AND address_book.member_key = ".$member_info["member_key"];
            }else{
                $where .= " AND (address_book.member_key = ".$member_info["member_key"] . " OR address_book.member_key IS NULL )";
            }
        } else {
            $where .= " AND address_book.member_key IS NULL";
        }
        if ($form_data["request"]["name"]) {
          $where .= " AND (address_book.name like '%".addslashes($form_data["request"]["name"])."%'" .
              " OR address_book.name_kana like '%".addslashes($form_data["request"]["name"])."%'".
              " OR address_book.email like '%".addslashes($form_data["request"]["name"])."%'"    .
              " OR address_book_group.group_name like '%".addslashes($form_data["request"]["name"])."%')";
        }

        $sql = "SELECT count(address_book_key) FROM address_book" .
            " LEFT JOIN address_book_group" .
            " ON (address_book.user_key = address_book_group.user_key".
            " AND address_book.address_book_group_key = address_book_group.address_book_group_key)";
        $sql .= $where;
        $count = $this->objAddressBook->_conn->getOne($sql);
        if (DB::isError($count)) {
          print $count->getUserInfo();
        }
        // データ取得
        $sql = "SELECT address_book.*, address_book_group.group_name FROM address_book".
            " LEFT JOIN address_book_group" .
            " ON (address_book.user_key = address_book_group.user_key".
            " AND address_book.address_book_group_key = address_book_group.address_book_group_key)";
        $sql .= $where;
        $sql .= " ORDER BY ";
        $sort_columns = array("name" , "name_kana" , "email" ,"group_name", "lang", "timezone");
        if(!in_array($sort_key, $sort_columns)){
          $sort_key = "name";
        }
        if (strtolower($sort_type) == "desc") {
          $sql .= $sort_key." DESC";
        } else {
          $sql .= $sort_key." ASC";
        }
        $rs = $this->objAddressBook->_conn->limitQuery($sql, $offset, $limit);
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
          $rows[] = $row;
        }
        $pager = $this->setPager($limit, $page, $count, $sort_key, $sort_type);
        $pager["action"] = "action_address_list=";
        $this->session->set("condition", $form_data, $this->_name_space);
        // テンプレート
        $session_address_list = $this->session_address_list_process($this->session->get("session_address_list"));
        $this->template->assign("session_address_list", $session_address_list);
        $this->template->assign("form_data", $form_data);
        $this->template->assign("pager", $pager);
        $this->template->assign("rows", $rows);
        $room_key = $this->session->get("room_key", $this->_name_space);
        $this->template->assign("room_key", $room_key);
        $this->template->assign("meeting_key", $this->session->get("meeting_key", $this->_name_space));
        $this->template->assign("invitation", $this->session->get("invitation", $this->_name_space));
        $this->display('user/address/index.t.html');
    }

  /**
     * セッションの中のアドレス帳情報取得
     */
    function session_address_list_process($orginal_list){
        if($orginal_list){
            $processed_list = array();
            $processed_address = null;
            foreach ($orginal_list as $orginal_address){
                $processed_address = $this->objAddressBook->getRow(sprintf("address_book_key = %s", $orginal_address['address_book_key']));
                if($processed_address){
                    $processed_list[] = $processed_address;
                }
            }
            return $processed_list;
        }
    }
    
    /**
     * メンバー一覧管理
     */
    function render_member_list() {
        $m_list = $this->session->get("condition", $this->_name_space);
        $s_key  = $m_list["sort"]["key"];
        $s_type = $m_list["sort"]["type"];

        if($s_type == "down" || $s_type == NULL || 1 == $this->request->get("reset")) {
          $form_data = $this->set_form("member_name", "down");
        } else {
          $form_data = $this->set_form($s_key, $s_type);
        }

        $sort_key  = $form_data["sort"]["key"];
        $sort_type = $form_data["sort"]["type"];
        $page      = $form_data["sort"]["page"];
        $limit     = $form_data["sort"]["page_cnt"];
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );

        $offset    = ($limit * ($page - 1));
        // 条件
        $where = " WHERE member_status = 0 AND outbound_id IS NULL" .
                " AND member.user_key = ".$user_info["user_key"];
        if ($form_data["request"]["name"]) {
            $where .= " AND (member.member_name like '%".addslashes($form_data["request"]["name"])."%'" .
                    " OR member.member_name_kana like '%".addslashes($form_data["request"]["name"])."%'".
                    " OR member.member_email like '%".addslashes($form_data["request"]["name"])."%'"    .
                    " OR member_group.member_group_name like '%".addslashes($form_data["request"]["name"])."%')";
        }
        //member課金
        if ( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") && $member_info ) {
            $where = sprintf( "%s AND member.member_key != '%s'", $where, $member_info["member_key"] );
        }
        // 件数
        $sql = "SELECT count(member_key) FROM member" .
                " LEFT JOIN member_group" .
                " ON (member.user_key = member_group.user_key".
                " AND member.member_group = member_group.member_group_key)";
        $sql .= $where;
        $count = $this->objMember->_conn->getOne($sql);
        if (DB::isError($count)) {
            print $count->getUserInfo();
        }
        // データ取得
        $sql = "SELECT member.*, member_group_name FROM member".
                " LEFT JOIN member_group" .
                " ON (member.user_key = member_group.user_key".
                " AND member.member_group = member_group.member_group_key)";
        $sql .= $where;
        $sql .= " ORDER BY ";
        $sort_columns = array("member_name" , "member_name_kana" , "member_group_name" , "member_email" , "lang", "timezone");
        if(!in_array($sort_key, $sort_columns)){
            $sort_key = "member_name";
        }
        if (strtolower($sort_type) == "desc") {
            $sql .= $sort_key." DESC";
        } else {
            $sql .= $sort_key." ASC";
        }
        $rs = $this->objMember->_conn->limitQuery($sql, $offset, $limit);
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        $pager = $this->setPager($limit, $page, $count, $sort_key, $sort_type);
        $pager["action"] = "action_member_list=";

        $this->session->set("condition", $form_data, $this->_name_space);

        // テンプレート
        $session_address_list = $this->session->get("session_address_list");
        $this->template->assign("session_address_list", $session_address_list);
        $this->template->assign("form_data", $form_data);
        $this->template->assign("pager", $pager);
        $this->template->assign("rows", $rows);
        $room_key = $this->session->get("room_key", $this->_name_space);
        $this->template->assign("room_key", $room_key);
        $this->template->assign("meeting_key", $this->session->get("meeting_key", $this->_name_space));
        $this->template->assign("invitation", $this->session->get("invitation", $this->_name_space));
        $this->display('user/address/member.t.html');
    }

    function set_form($default_sort_key = null, $default_sort_type = null) {
        //
        // デフォルトのソート順指定
          $sort_key  = $this->request->get("sort_key", $default_sort_key);
          $sort_type = $this->request->get("sort_type", $default_sort_type);
          $page      = $this->request->get("page");
          $page_cnt  = $this->request->get("page_cnt");
          $request   = $this->request->get("form");
          $reset     = $this->request->get("reset");

        // デフォルト
        if ($reset == 1) {
            $this->session->remove('request', $this->_name_space);
            $this->session->remove("condition", $this->_name_space);
            $sort = array(
                'key'  => $default_sort_key,
                'type' => $default_sort_type,
                'page'      => 1,
                'page_cnt'  => $this->_limit,
            );
        } else {
            // ソート
            if ($sort_key) {
                $sort['key']  = $sort_key;
                $sort['type'] = $sort_type;
            }
            $sort['page_cnt'] = $page_cnt ? $page_cnt : $this->_limit;
            if ($request) {
                $sort['page'] = 1;
                $this->session->set('request', $request, $this->_name_space);
            } else {
                $sort['page'] = $page ? $page : 1;
                $request = $this->session->get('request', $this->_name_space);
            }
        }
        return array(
            "sort" => $sort,
            "request" => $request
            );
    }

    /**
     * アドレス帳選択
     */
    function render_form() {
        $this->set_submit_key($this->_name_space);
        $user_info = $this->session->get("user_info");
        if ($key = $this->request->get("key")) {
            $where .= "is_deleted = 0" .
                " AND user_key = ".$user_info["user_key"].
                " AND address_book_key = ".$key;
            if ($member_info = $this->session->get("member_info")) {
                $where .= " AND member_key = ".$member_info["member_key"];
            }
            $request = $this->objAddressBook->getRow($where);
        }
        $address_book_group_list_info = $this->objAddressBookGroup->getRowsAssoc("status = 1 AND user_key =".$user_info["user_key"]);
        if (!empty($address_book_group_list_info)){
          $address_book_group_list = array();
          foreach ($address_book_group_list_info as $address_book_group){
        $address_book_group_list[$address_book_group['address_book_group_key']] = $address_book_group["group_name"];
          }
          $request["address_book_group_list"] = $address_book_group_list;
        }

        $this->template->assign("form_data", $request);
        $this->display('user/address/input.t.html');
    }

    /**
     * 会議内よりアドレス帳を呼び出し
     * 選択したユーザーに対してメールを送信する
     */
    public function action_sendMailToInviteGuest()
    {
        require_once("classes/N2MY_invitation.class.php");

        $objInvidation = new N2MY_Invitation($this->get_dsn());
        $keyList = $this->request->get("key");
        $meeting_key = $this->session->get("meeting_key", $this->_name_space);
        $inviteGuestList = $this->request->get("data");

        if ($keyList = $this->request->get("key")) {
            for ($i = 0; $i < count($inviteGuestList); $i++) {
                if (in_array($inviteGuestList["key"][$i], $keyList)) {
                    $result = $objInvidation->sendInvitationMail(
                                            $meeting_key,
                                            $inviteGuestList["mailAddress"][$i],
                                            $inviteGuestList["locale"][$i]
                                            );
                }
            }
        }
    }
}

$main = new AppAddress();
$main->execute();
