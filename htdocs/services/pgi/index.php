<?php
require_once('classes/AppFrame.class.php');
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/pgi/PGiPhoneNumber.class.php");

class AppPGi extends AppFrame {

    public function default_view()
    {
        $this->action_phone_numbers();
    }
    public function action_phone_numbers($message = "")
    {
        $meeting_session_id   = $this->request->get("meeting_session_id");
        if (!$meeting_session_id) {
            die('$meeting_session_id required');
        }

        $meeting_table     = new MeetingTable( $this->get_dsn() );
        $meeting           = $meeting_table->findByMeetingSessionId($meeting_session_id);
        $use_dialin        = $meeting['use_pgi_dialin'] || $meeting['use_pgi_dialin_free'] || $meeting['use_pgi_dialin_lo_call'];
        $is_display_number = $use_dialin && ($meeting['pgi_api_status'] == 'complete');

        if ($is_display_number) {
            $pgi_phone_numbers = unserialize($meeting['pgi_phone_numbers']);
            $phone_types       = array();
            if ($meeting['use_pgi_dialin_free']) {
                $phone_types[] = 'Free';
            }
            if ($meeting['use_pgi_dialin']) {
                $phone_types[] = 'Local';
                $phone_types[] = 'Toll';
            }
            if ($meeting['use_pgi_dialin_lo_call']) {
                $phone_types[] = 'Lo-call';
            }
            $extract_conditions = array('Types'    => $phone_types);
            $pgi_phone_numbers = PGiPhoneNumber::extracts($pgi_phone_numbers, $extract_conditions);
            $pgi_phone_number_list = array();
            foreach ($pgi_phone_numbers as $k=>$v) {
                $l_config       = PGiPhoneNumber::findLocationConfigByID($v['LocationID']);
                $country = (string)$l_config->countryId ? (string)$l_config->countryId : "OTHER";
                $region = (string)$l_config->regionId ? (string)$l_config->regionId : "0";
                if (!$pgi_phone_number_list[$region]) {
                    $r_config       = PGiPhoneNumber::findRegionConfigByRegion($region);
                    $r_name = $r_config->name;
                    $default_r_name = (string)$r_name->en_US;
                    $pgi_phone_number_list[$region] =  array(
                            "RegionId" => (string)$r_config->id,
                            "RegionName" => (string)$r_name->{$this->_lang},
                    );
                }
                if (!$pgi_phone_number_list[$region]["CountryList"][$country]) {
                    $c_config       = PGiPhoneNumber::findCountryConfigById($country);
                    $c_name = $c_config->name;
                    $default_c_name = (string)$c_name->en_US;
                    $pgi_phone_number_list[$region]["CountryList"][$country] = array (
                                "Id"            => $country,
                                "Code"            => (string)$c_config->code,
                                "CountryName"=> (string)$c_name->{$this->_lang},
                    );
                }
                $l_name         = $l_config->name;
                $default_l_name = (string)$l_name->en_US;
                $v['LocationName'] = (string)$l_name->{$this->_lang};
                if (!$v['LocationName']){
                    $v['LocationName'] = $v['Location'];
                }
                $t_config = PGiPhoneNumber::findTypeConfigByKey($v['Type']);
                $t_name   = $t_config->name;
                $default_t_name = (string)$t_name->en_US;
                $v['TypeName'] = (string)$t_name->{$this->_lang};
                if ($v['LocationName']) {
                    $pgi_phone_number_list[$region]["CountryList"][$country]["DialIn"][] = $v;
                }

            }
            $this->logger2->debug($pgi_phone_number_list);

        } else {
            $pgi_phone_numbers = null;
        }
        $this->template->assign(array("meeting"            => $meeting,
                                      "pgi_phone_numbers"  => $pgi_phone_number_list));
        $this->display('pgi/tel_no.t.html');
    }
}

$main = new AppPGi();
$main->execute();
?>
