<?php

require_once('classes/AppFrame.class.php');
require_once('classes/dbi/user.dbi.php');
require_once('classes/dbi/member.dbi.php');
require_once('classes/dbi/ordered_service_option.dbi.php');
require_once("classes/dbi/inbound.dbi.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/N2MY_Account.class.php");

class AppInbound extends AppFrame {


    var $_name_space = null;
    /**
     * 一ページの表示件数
     */
    var $_limit = 10;
    /**
     * 初期処理
     */
    function init() {
        $this->_name_space = md5(__FILE__);
        $user_info = $this->session->get("user_info");
        $this->obj_User         = new UserTable($this->get_dsn());
        $this->obj_Member       = new MemberTable($this->get_dsn());
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->obj_Inbound      = new InboundTable ($this->get_dsn());
    }

    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        if($this->session->get("service_mode") != "sales") {
            //$message = NO_INBOUND_OPTION;
            $this->template->assign("message", $message);
            $this->display("user/404_error.t.html");
        }
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->action_show_top();
    }

    function action_show_top () {
        $this->set_submit_key($this->_name_space);
        $user_info = $this->session->get("user_info");
        // オプション契約がある場合はリストを表示
        if ($user_info["use_sales"]) {
            $this->session->set("inbound_option", "1");
            $where = "user_key = ".$user_info["user_key"];
            $inbound_list = $this->obj_Inbound->getRowsAssoc($where);
            $this->logger->info(__FILE__,__FUNCTION__,__LINE__,$inbound_list);
            if($inbound_list == null) {
                $data = null;
                $message = "NO INBOUND MADOGUCHI";
                $this->template->assign("message", $message);
            }
            else {
                $data = array();
                foreach ($inbound_list as $inbound) {
                    $iframe_size = $this->obj_Inbound->get_iframe_size($inbound["design_key"]);
                    $this->logger2->info($iframe_size);
                    $inbound["design_info"]["width"] = $iframe_size["width"];
                    $inbound["design_info"]["height"] = $iframe_size["height"];
                    $inbound["url"] = $user_info["login_url"].$inbound["url"];
                    $addition = unserialize($inbound["addition"]);
                    if ("original" == $inbound["design_type"]) {
                        $inbound["design_info"]["width"] = $addition["iframe_width"];
                        $inbound["design_info"]["height"] = $addition["iframe_height"];
                    }
                    $data[] = $inbound;
                }
            }
            $this->template->assign("data", $data);
            $this->display('admin/inbound/index.t.html');
        } else {
            //$message = NO_INBOUND_OPTION;
            $this->template->assign("message", $message);
            $this->display("user/404_error.t.html");
          //$inbound_data = $this->obj_Inbound->getRow($where);
            //$this->request->set("inbound_id", $inbound_data["inbound_id"]);
            //$this->action_show_detail();
        }
    }

    function action_show_detail($message = ""){
        $this->set_submit_key($this->_name_space);
        $user_info = $this->session->get("user_info");
        if (!$inbound_id = $this->request->get("inbound_id")) {
            $inbound_id = $this->session->get("inbound_id");
        } else {
            $this->session->set("inbound_id", $inbound_id);
        }
        $this->logger->debug("inbound_id",__FILE__,__LINE__,$inbound_id);
        $url = $user_info["login_url"];
        $where = "user_key = ".$user_info["user_key"].
                 " AND inbound_id = '".$inbound_id."'";
        $inbound_info = $this->obj_Inbound->getRow($where);

        $iframe_size = $this->obj_Inbound->get_iframe_size($inbound_info["design_key"]);
        $design_info["width"] = $iframe_size["width"];
        $design_info["height"] = $iframe_size["height"];
        $addition = unserialize($inbound_info["addition"]);
        if ("original" == $inbound_info["design_type"]) {
            $design_info["width"] = $addition["iframe_width"];
            $design_info["height"] = $addition["iframe_height"];
        }

        $obj_Room = new RoomTable($this->get_dsn());
        $form_data = $this->set_form("member_name", "asc");
        $sort_key  = $form_data["sort"]["key"];
        $sort_type = $form_data["sort"]["type"];
        $page      = $form_data["sort"]["page"];
        $limit     = $form_data["sort"]["page_cnt"];
        $offset    = ($limit * ($page - 1));
        $sort_key_rules = array("member_name","member_id","member_group");
        if (!EZValidator::valid_allow($sort_key, $sort_key_rules)) {
            $sort_key = "member_name";
        }
        $sort_type_rules = array("asc","desc");
        if (!EZValidator::valid_allow($sort_type, $sort_type_rules)) {
            $sort_type = "desc";
        }

        require_once ("classes/N2MY_Inbound.class.php");
        $obj_n2myInbound = new N2MY_Inbound($this->get_dsn());
        $members = $obj_n2myInbound->inboundRoomFlg($user_info["user_key"], $inbound_id, "1", $sort_key, $sort_type, $limit, $offset);
        $members_count = $obj_n2myInbound->inboundRoomFlgCount($user_info["user_key"], $inbound_id, "1");
        if ($members) {
          require_once ('classes/dbi/member_group.dbi.php');
          $obj_Group = new MemberGroupTable ($this->get_dsn());
          foreach($members as $value) {
            $value["group_name"] = $obj_Group->getOne( sprintf( "member_group_key='%s' AND user_key != 0", $value["member_group"] ), "member_group_name" );
            $inbound_members[] = $value;
          }
        }
        $pager = $this->setPager($limit, $page, $members_count, $sort_key, $sort_type);
        $pager["action"] = "action_show_detail";
        
        $this->logger2->info($inbound_info,"inbound_info");

        $this->template->assign("inbound_info", $inbound_info);
        $this->template->assign("design_info", $design_info);
        $this->template->assign("url", $url);
        $this->template->assign("user_info", $user_info);
        $this->template->assign("pager", $pager);
        $this->template->assign("inbound_members", $inbound_members);
        $this->template->assign("inbound_option", $this->session->get("inbound_option"));
        $this->template->assign("message", $message);
        $this->display('admin/inbound/detail.t.html');
    }

    /**
     * インバウンドの名前変更
     */
    function action_edit_name(){
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_show_top();
        }
        $request = $this->request->getAll();
        $where = "inbound_id = '".$request["inbound_id"]."'";
        $data = array(
                "inbound_name" => $request["inbound_name"]
        );
        $inbound_list = $this->obj_Inbound->update($data, $where);
        if ($request["after_action"] == "detail") {
            $this->action_show_detail();
        } else {
            $this->action_show_top();
        }
    }

    function action_change_customerinfo_flg() {
        $user_info = $this->session->get("user_info");
        $customer_info_flg = $this->request->get("customer_info_flg");
    }

    function action_inbound_setting($message = ""){
        $inbound_id = $this->session->get("inbound_id");
        $this->set_submit_key($this->_name_space);
        $user_info = $this->session->get("user_info");
        $where = "user_key = ".$user_info["user_key"].
                 " AND inbound_id = '".$inbound_id."'";
        $inbound_info = $this->obj_Inbound->getRow($where);
        $this->logger->trace("inbound_info",__FILE__,__LINE__,$inbound_info);
        $this->template->assign("user_info", $user_info);
        $this->template->assign("inbound_info", $inbound_info);
        $this->template->assign("message", $message);
        $this->template->assign("inbound_option", $this->session->get("inbound_option"));
        $this->display('admin/inbound/setting.t.html');
    }

    function action_inbound_setting_modify(){
        $inbound_id = $this->session->get("inbound_id");
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_inbound_setting();
        }
        $request = $this->request->getAll();
        $this->logger->debug("request",__FILE__,__LINE__,$request);
        $user_info = $this->session->get("user_info");
        $where = "user_key = ".$user_info["user_key"].
                 " AND inbound_id = '".$inbound_id."'";
        if (999 < $request["reenter_time"] || !is_numeric($request["reenter_time"])) {
             $this->action_inbound_setting( INBOUND_SETTING_ERROR );
             exit;
        }
        $data = array(
            "reenter_time" => $request["reenter_time"],
            "customer_info_flg" => $request["customer_info_flg"],
            "customer_name" => $request["customer_name"],
        );
        $this->obj_Inbound->update($data, $where);
        $this->request->set("inbound_id",$request["inbound_id"]);
        $this->action_show_detail( INBOUND_SETTING_COMPLETE );
    }

    function action_design_setting($message = ""){
        $this->set_submit_key($this->_name_space);
        $inbound_id = $this->session->get("inbound_id");
        $user_info = $this->session->get("user_info");
        $where = "user_key = ".$user_info["user_key"].
                 " AND inbound_id = '".$inbound_id."'";
        $inbound_info = $this->obj_Inbound->getRow($where);
        $addition = unserialize($inbound_info["addition"]);
        $addition["absence_image"]["dir"] .= "?".strtotime($inbound_info["update_datetime"]);
        $addition["enter_image"]["dir"] .= "?".strtotime($inbound_info["update_datetime"]);
        $this->logger2->debug($addition);
        $this->template->assign("user_info", $user_info);
        $this->template->assign("inbound_info", $inbound_info);
        $this->template->assign("addition", $addition);
        $this->template->assign("message", $message);
        $this->template->assign("inbound_option", $this->session->get("inbound_option"));
        $this->display('admin/inbound/design.t.html');
    }

    function action_design_change(){
        $inbound_id = $this->session->get("inbound_id");
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            $this->request->set("inbound_id",$inbound_id);
            return $this->action_show_detail();
        }
        $design_key = $this->request->get("design_type");
        if ("original" == $design_key) {
            $design_key = $this->request->get("original_layout");
            $design_type = "original";
        } else {
            $design_type = "normal";
        }
        $user_info = $this->session->get("user_info");
        $where = "user_key = ".$user_info["user_key"].
                 " AND inbound_id = '".$inbound_id."'";
        $data = array(
            "design_key" => $design_key,
            "design_type" => $design_type,
        );
        $this->obj_Inbound->update($data, $where);
        $this->request->set("inbound_id",$inbound_id);
        $this->action_show_detail( INBOUND_DESIGN_COMPLETE );
    }

    function action_design_original($message = ""){
        $inbound_id = $this->session->get("inbound_id");
        $user_info = $this->session->get("user_info");
        $where = "user_key = ".$user_info["user_key"].
                 " AND inbound_id = '".$inbound_id."'";
        $inbound_info = $this->obj_Inbound->getRow($where);
        $addition = unserialize($inbound_info["addition"]);
        $request = $this->session->get("design_original");
        if ($request) {
            $addition["original_layout"] = $request["layout"];
        }
        $this->template->assign("user_info", $user_info);
        $this->template->assign("inbound_info", $inbound_info);
        $this->template->assign("addition", $addition);
        $this->template->assign("message", $message);
        $this->template->assign("inbound_option", $this->session->get("inbound_option"));
        $this->display('admin/inbound/design_original.t.html');
    }

    //オリジナル画像登録確認ページ
    function action_original_design_confirm()
    {
        $this->set_submit_key($this->_name_space);
        $inbound_id = $this->session->get("inbound_id");
        $request = $this->request->getAll();
        $this->logger2->info($request);
        if ("3" == $request["layout"]) {
            $request["size"] = $request["size2"];
        }
        $this->session->set("design_original", $request);
        $user_info = $this->session->get("user_info");
        if (!$request["layout"]) {
            $message = INBOUND_DESIGN_ORIGINAL_LAYOUT;
        }
        $where = "user_key = ".$user_info["user_key"].
                 " AND inbound_id = '".$inbound_id."'";
        $inbound_info = $this->obj_Inbound->getRow($where);
        $addition = unserialize($inbound_info["addition"]);
        foreach ($_FILES as $_key => $image) {
            $up_image = "";
            if ($image["name"]) {
                $up_image = $this->_image_upload($_key);
            } else {
                $this->logger->debug("addition",__FILE__,__LINE__,$addition[$_key]);
                if (!$addition[$_key]) {
                    $message[] = INBOUND_DESIGN_ORIGINAL_IMAGE;
                } else {
                    //現在アップされている画像サイズ確認
                    $max_width = "240";
                    $max_height = "70";
                    if ("enter_image" == $_key) {
                        if ($max_width < $addition[$_key]["width"] || $max_height < $addition[$_key]["height"]){
                            $this->logger->error("error",__FILE__,__LINE__,$_key." size error");
                            $up_image["error"] = $_key;
                        }
                    } else if ("3" != $request["layout"]) {
                        if ($max_width < $addition[$_key]["width"] || $max_height < $addition[$_key]["height"]){
                            $this->logger->error("error",__FILE__,__LINE__,$_key." size error");
                            $up_image["error"] = $_key;
                        }
                    }
                }
            }
            if ($up_image["error"]) {
                if ($up_image["error"] == "online_image") {
                    $message[] = INBOUND_DESIGN_ORIGINAL_IMAGESIZE_1;
                } else if ($up_image["error"] == "busy_image") {
                    $message[] = INBOUND_DESIGN_ORIGINAL_IMAGESIZE_2;
                } else if ($up_image["error"] == "absence_image") {
                    $message[] = INBOUND_DESIGN_ORIGINAL_IMAGESIZE_3;
                } else if ($up_image["error"] == "enter_image") {
                    $message[] = INBOUND_DESIGN_ORIGINAL_IMAGESIZE_4;
                }
            } else if ($image["name"]) {
                $button_info["image"][$_key] = '/photo/'.$user_info["user_id"].'/inbound/'.$inbound_info["inbound_id"].'/tmp/'.$up_image["name"];
            }
        }
        $button_info["layout"] = $request["layout"];
        if ($message) {
            $this->action_design_original($message);
            exit;
        }

        $this->logger->debug("image",__FILE__,__LINE__,$button_info);
        $this->session->set("design_original", $button_info);
        $this->template->assign("images", $button_info);
        $this->template->assign("inbound_id", $request["inbound_id"]);
        $this->template->assign("inbound_option", $this->session->get("inbound_option"));
        $this->display('admin/inbound/design_original_confirm.t.html');
    }

    //オリジナル画像登録
    function action_original_design_complete()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_design_original();
        }
        $request = $this->session->get("design_original");
        $inbound_id = $this->session->get("inbound_id");
        $user_info = $this->session->get("user_info");
        //現在の情報を取得
        $where = "user_key = ".$user_info["user_key"].
                 " AND inbound_id = '".$inbound_id."'";
        $inbound_info = $this->obj_Inbound->getRow($where);
        $this->logger2->info($request);
        $addition = unserialize($inbound_info["addition"]);
        if ($request["image"]) {
            foreach ($request["image"] as $_key => $image) {
                $default_dir = realpath(dirname(__FILE__).'/../../..');
                $tmp_dir = $default_dir.$image;
                $file_explode = explode("/", $image);
                $file_name = $file_explode[6];
                $this->logger->info("file",__FILE__,__LINE__,array($file_explode,$file_name));
                $image_dir = '/photo/'.$user_info["user_id"].'/inbound/'.$inbound_info["inbound_id"].'/'.$file_name;
                $up_dir = $default_dir.$image_dir;
                if (file_exists($up_dir)) {
                    unlink($up_dir);
                }
                $this->logger2->info(array($tmp_dir, $up_dir));
                rename($tmp_dir, $up_dir);
                list($width, $height, $type, $attr) = getimagesize($up_dir);
                $addition[$_key] = array(
                            "dir"    => $image_dir,
                            "width"  => $width,
                            "height" => $height,
                            );
            }
        }
        $addition["original_layout"] = $request["layout"];
        if ("1" == $request["layout"]) {
            $addition["iframe_width"] = $addition["online_image"]["width"];
            $addition["iframe_height"] = $addition["online_image"]["height"] + $addition["enter_image"]["height"];
        } else if("2" == $request["layout"]) {
            $addition["iframe_width"] = $addition["online_image"]["width"] + $addition["enter_image"]["width"];
            $addition["iframe_height"] = $addition["enter_image"]["height"];
        } else if ("3" == $request["layout"]) {
            $addition["iframe_width"] = $addition["enter_image"]["width"];
            $addition["iframe_height"] = $addition["enter_image"]["height"];
        }
        $data = array(
                "addition"    => serialize($addition),
        		"update_datetime" => date("Y-m-d H:i:s"),
        );
        $this->logger2->debug($data);
        $this->obj_Inbound->update($data, $where);

        $this->session->remove("design_original");
        $this->action_design_setting("design_done");
    }

    //画像をテンポラリディレクトリにアップデート
    function _image_upload($image_name)
    {
        $inbound_id = $this->session->get("inbound_id");
        if($_FILES[$image_name]["size"] > 1000000){
            $message = INBOUND_DESIGN_ORIGINAL_FILESIZE;
            $this->logger->error("error",__FILE__,__LINE__,$message);
            $this->action_design_original($message);
            exit;
        } else if($_FILES[$image_name]["type"] != "image/jpeg" &&
           $_FILES[$image_name]["type"] != "image/jpg" &&
           $_FILES[$image_name]["type"] != "image/pjpeg" &&
           $_FILES[$image_name]["type"] != "image/gif" &&
           $_FILES[$image_name]["type"] != "image/png" &&
           $_FILES[$image_name]["type"] != "image/x-png" &&
           $_FILES[$image_name]["type"] != ""){
            $error = NEWS_ERROR_FILE_TYPE;
            $this->logger->error("error",__FILE__,__LINE__,$error);
            $this->template->assign("error", $error);
        } else if($_FILES[$image_name]["error"] == 0){
            $this->logger->debug("FILES",__FILE__,__LINE__,$_FILES["file"]);
            $file_tmp  = $_FILES[$image_name]["tmp_name"];
            $user_info = $this->session->get('user_info');
            $up_dir = N2MY_DOCUMENT_ROOT.'/photo/'.$user_info["user_id"].'/inbound/'.$inbound_id.'/tmp/';
            if( !is_dir( $up_dir ) ){
                mkdir( $up_dir , 0777, true);
                chmod( $up_dir ,0777 );
            }
            // 最大の大きさ
            $max_width = "240";
            $max_height = "70";

            // コンテントタイプ
            header('Content-type: image/jpeg');
            // 新規サイズを取得します
            list($width_original, $height_original, $type, $attr) = getimagesize($file_tmp);
            $this->logger->debug("image_size",__FILE__,__LINE__,array($width_original, $height_original, $max_width, $max_height));
            //設定サイズより大きかったらエラー
            if ($max_width < $width_original || $max_height < $height_original){
                $this->logger->error("error",__FILE__,__LINE__,$image_name." size error");
                $value["error"] = $image_name;
                return $value;
                exit;
            }

            if($_FILES[$image_name]["type"] == "image/jpeg" ||
                $_FILES[$image_name]["type"] == "image/jpg" ||
                $_FILES[$image_name]["type"] == "image/pjpeg"){
                $up_image_name = $image_name.".jpg";
                $up_file = $up_dir."/".$up_image_name;
                move_uploaded_file($file_tmp, $up_file);
                $file_type = "jpg";
            //GIF出力
            } else if($_FILES[$image_name]["type"] == "image/gif"){
                $up_image_name = $image_name.".gif";
                $up_file = $up_dir."/".$up_image_name;
                move_uploaded_file($file_tmp, $up_file);
                $file_type = "gif";
            //PNG出力
            } else if($_FILES[$image_name]["type"] == "image/png" ||
                      $_FILES[$image_name]["type"] == "image/x-png"){
                $up_image_name = $image_name.".png";
                $up_file = $up_dir."/".$up_image_name;
                move_uploaded_file($file_tmp, $up_file);
                $file_type = "png";
            }
            if (file_exists($up_file)) {
                switch ($_FILES[$image_name]["type"]) {
                    case "jpg":
                        if (file_exists($up_dir."/".$image_name.".png")) {
                            unlink($up_dir."/".$image_name.".png");
                        }
                        if (file_exists($up_dir."/".$image_name.".gif")) {
                            unlink($up_dir."/".$image_name.".gif");
                        }
                        break;
                    case "gif":
                        if (file_exists($up_dir."/".$image_name.".png")) {
                            unlink($up_dir."/".$image_name.".png");
                        }
                        if (file_exists($up_dir."/".$image_name.".jpg")) {
                            unlink($up_dir."/".$image_name.".jpg");
                        }
                        break;
                    case "png":
                        if (file_exists($up_dir."/".$image_name.".jpg")) {
                            unlink($up_dir."/".$image_name.".jpg");
                        }
                        if (file_exists($up_dir."/".$image_name.".gif")) {
                            unlink($up_dir."/".$image_name.".gif");
                        }
                        break;
                }
            } else {
                $this->logger->error(__FUNCTION__,__FILE__,__LINE__, "file up error");
            }
        }
        $value["name"] = $up_image_name;
        return $value;
    }

    function action_member_setting($form_status = ""){
        $this->set_submit_key($this->_name_space);
        $search = $this->request->get("search_member");
        $inbound_id = $this->session->get("inbound_id");
        $this->logger->debug("inbound_id",__FILE__,__LINE__,$inbound_id);
        $user_info = $this->session->get("user_info");
        $form_data = $this->set_form("member_name", "asc");
        $sort_key  = $form_data["sort"]["key"];
        $sort_type = $form_data["sort"]["type"];
        $sort_key_rules = array("member_name","member_id","member_group");
        if (!EZValidator::valid_allow($sort_key, $sort_key_rules)) {
            $sort_key = "member_name";
        }
        $sort_type_rules = array("asc","desc");
        if (!EZValidator::valid_allow($sort_type, $sort_type_rules)) {
            $sort_type = "desc";
        }
        $page      = $form_data["sort"]["page"];
        $limit     = $form_data["sort"]["page_cnt"];
        $offset    = ($limit * ($page - 1));
        require_once ("classes/N2MY_Inbound.class.php");
        $obj_n2myInbound = new N2MY_Inbound($this->get_dsn());
        $members = $obj_n2myInbound->inboundRoomFlg($user_info["user_key"], null, "0", $sort_key, $sort_type, $limit, $offset, $search);
        if ( !$members ) {
        	// 不正page指定対応
        	$offset = 0;
        	$page = 1;
        	$members = $obj_n2myInbound->inboundRoomFlg($user_info["user_key"], null, "0", $sort_key, $sort_type, $limit, $offset, $search);
        }
        require_once ('classes/dbi/member_group.dbi.php');
        $obj_Group = new MemberGroupTable ($this->get_dsn());
        if ($members) {
            foreach($members as $value) {
                if ($value["member_group"] != "0"){
                    $value["group_name"] = $obj_Group->getOne( sprintf( "member_group_key='%s' AND user_key != 0", $value["member_group"] ), "member_group_name" );
                    $other_members[] = $value;
                } else {
                    $other_members[] = $value;
                    $value["group_name"] = "";
                }
            }
        }
        require_once ("classes/N2MY_Inbound.class.php");
        $obj_n2myInbound = new N2MY_Inbound($this->get_dsn());
        $count = $obj_n2myInbound->inboundRoomFlgCount($user_info["user_key"], null, "0", $search);
        $pager = $this->setPager($limit, $page, $count, $sort_key, $sort_type);
        $this->template->assign("search", $search);
        $this->template->assign("count", $count);
        $this->template->assign("pager", $pager);
        $this->template->assign("user_info", $user_info);
        $this->template->assign("inbound_id", $inbound_id);
        $this->template->assign("form_status", $form_status);
        $this->template->assign("other_members", $other_members);
        $this->template->assign("inbound_option", $this->session->get("inbound_option"));
        $this->template->assign('is_not_display_portal_header', true);
        
        $this->display('admin/inbound/member.t.html');
    }

    function action_add_inbound_members(){
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_member_setting();
        }
        $room_keys = $this->request->get("key");
        $inbound_id = $this->session->get("inbound_id");
        $obj_Room = new RoomTable($this->get_dsn());
        foreach ($room_keys as $room_key) {
            $where = "room_key = '".addslashes($room_key)."'";
            $data = array(
                "inbound_flg" => "1",
                "inbound_id"  => $inbound_id,
            );
            $obj_Room->update($data, $where);
        }
        $this->logger->debug("room_keys",__FILE__,__LINE__,$room_keys);
        $this->action_member_setting("add_complete");
    }

    function action_add_member_complete() {
        $this->action_show_detail(INBOUND_MEMBER_ADD_COMPLETE);
    }

    function action_delete_inbound_member(){
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_show_detail();
        }
        $room_key = $this->request->get("room_key");
        $inbound_id = $this->request->get("inbound_id");
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'";
        $data = array(
            "inbound_flg" => "0",
            "inbound_id"  => null,
        );
        $obj_Room->update($data, $where);
        $this->action_show_detail(INBOUND_MEMBER_DELETE_COMPLETE);
    }

    function set_form($default_sort_key = null, $default_sort_type = null) {
        //
        // デフォルトのソート順指定
        $sort_key  = $this->request->get("sort_key", $default_sort_key);
        $sort_type = $this->request->get("sort_type", $default_sort_type);
        $page      = $this->request->get("page");
        $page_cnt  = $this->request->get("page_cnt");
        $request   = $this->request->get("form");
        $reset     = $this->request->get("reset");
        $this->logger2->debug(array($sort_key,$sort_type));
        // デフォルト
        if ($reset == 1) {
            $this->session->remove('request', $this->_name_space);
            $sort = array(
                'key'  => $default_sort_key,
                'type' => $default_sort_type,
                'page'      => 1,
                'page_cnt'  => $this->_limit,
            );
        } else {
            // ソート
            if ($sort_key) {
                $sort['key']  = $sort_key;
                $sort['type'] = $sort_type;
            }
            $sort['page_cnt'] = $page_cnt ? $page_cnt : $this->_limit;
            if ($request) {
                $sort['page'] = 1;
                $this->session->set('request', $request, $this->_name_space);
            } else {
                $sort['page'] = $page ? $page : 1;
                $request = $this->session->get('request', $this->_name_space);
            }
        }
        return array(
            "sort" => $sort,
            "request" => $request
            );
    }
}

$main = new AppInbound();
$main->execute();