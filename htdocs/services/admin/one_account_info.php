<?php

require_once('classes/AppFrame.class.php');

class AppAdminOneAccountInfo extends AppFrame
{

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    /**
     * V-CUBE One アカウントのご案内表示
     * one_master_id_password を複合化する
     */
    function action_admin_one_account_info(){
        $admin_info = $this->session->get('user_info');
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $one_master_id_password_decrypt = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $admin_info["one_master_id_password"]);

        $this->template->assign("one_master_id_password", $one_master_id_password_decrypt);

        $this->display('admin/account/one_account_info.t.html');

    }
}

$main =& new AppAdminOneAccountInfo();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
