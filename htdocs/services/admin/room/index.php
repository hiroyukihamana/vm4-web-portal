<?php
require_once('classes/AppFrame.class.php');
require_once('classes/dbi/mail_doc_conv.dbi.php');
require_once('classes/dbi/user.dbi.php');

class AppMember extends AppFrame
{

    var $_name_space = null;
    var $mailDocConvObj = null;
    var $err_obj = null;

    function init() {
        $this->_name_space = md5(__FILE__);
        $user_info = $this->session->get("user_info");
        $this->mailDocConvObj = new MailDocConvet($this->get_dsn(), $user_info["user_key"]);
        $this->userObj        = new UserTable($this->get_dsn());
    }

    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace("test", __FILE__, __LINE__, __FUNCTION__);
        $this->render_list();
    }

    function action_show_top() {
        $this->render_list();
    }

    function render_list() {
        // 設定の有効・無効
        $user_info = $this->session->get("user_info");
        $where = "user_key = ".$user_info["user_key"];
        $user_info = $this->userObj->getRow($where);
        if ($user_info["addition"]) {
            $addition = unserialize($user_info["addition"]);
            $mail_doc_conv = isset($addition["mail_doc_conv"]) ? $addition["mail_doc_conv"] : 2;
        } else {
            $mail_doc_conv = 2;
        }
        $this->template->assign("mail_doc_conv", $mail_doc_conv);
        // 登録一覧
        $where = "is_active = 1";
        $list = $this->mailDocConvObj->getList($where);
        $this->template->assign("mail_list", $list);
        $this->display('admin/room/mail_document/list.t.html');
    }

    function action_add_form() {
        if ($this->request->get("new") == 1) {
            $this->clear_session();
        }
        $this->render_add_form();
    }

    function render_add_form() {
        $form_data = $this->get_session();
        if (EZValidator::isError($this->err_obj)) {
            $msg_format = $this->get_message("error");
            $msg = $this->get_message("ERROR");
            $err_fields = $this->err_obj->error_fields();
            foreach ($err_fields as $key) {
                $type = $this->err_obj->get_error_type($key);
                $_msg = sprintf($msg[$type], $this->err_obj->get_error_rule($key, $type));
                $error[$key] = sprintf($msg_format, $_msg);
            }
            $this->logger->info(__FUNCTION__."#error", __FILE__, __LINE__, $error);
            $this->template->assign("err", $error);
        }
        $this->template->assign("form_data", $form_data);
        $this->display('admin/room/mail_document/add_form.t.html');
    }

    function action_add_confirm() {
        $form_data = $this->request->getAll();
        $this->set_session($form_data);
        $this->logger->error($form_data);
        $this->err_obj = $this->check($form_data);
        if (EZValidator::isError($this->err_obj)) {
            return $this->render_add_form();
        }
        $this->render_add_confirm();
    }

    function render_add_confirm() {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $form_data = $this->get_session();
        $this->template->assign("form_data", $form_data);
        $this->display('admin/room/mail_document/add_confirm.t.html');
    }

    function action_add_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_list();
        }
        $form_data = $this->get_session();
        $data = array(
            "name"             => $form_data["name"],
            "mail_address"     => $form_data["mail_address"]
            );
        $this->mailDocConvObj->add($data);
        // 操作ログ
        $this->add_operation_log('mail_wb_upload_address_add', array('mail' => $form_data["mail_address"]));
        $this->render_add_complete();
    }

    function render_add_complete() {
        $this->display('admin/room/mail_document/add_complete.t.html');
    }

    function action_edit_form() {
        if ($uniq_key = $this->request->get("uniq_key")) {
            $where = "uniq_key = ". $uniq_key;
            $form_data = $this->mailDocConvObj->getRow($where);
            $this->set_session($form_data);
        }
        $this->render_edit_form();
    }

    function render_edit_form() {
        $form_data = $this->get_session();
        $this->template->assign("form_data", $form_data);
        $this->display('admin/room/mail_document/edit_form.t.html');
    }

    function action_edit_confirm() {
        $form_data = $this->request->getAll();
        $this->set_session($form_data);
        $this->err_obj = $this->check($form_data);
        if (EZValidator::isError($this->err_obj)) {
            return $this->render_add_form();
        }
        $this->set_session($this->request->getAll());
        $this->render_edit_confirm();
    }

    function render_edit_confirm() {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $form_data = $this->get_session();
        $this->template->assign("form_data", $form_data);
        $this->display('admin/room/mail_document/edit_confirm.t.html');
    }

    function action_edit_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_list();
        }
        $form_data = $this->get_session();
        $data = array(
            "name"             => $form_data["name"],
            "mail_address"     => $form_data["mail_address"]
            );
        $where = "uniq_key = ".$form_data["uniq_key"];
        $this->mailDocConvObj->update($data, $where);
        // 操作ログ
        $this->add_operation_log('mail_wb_upload_address_update', array('mail' => $form_data["mail_address"]));
        $this->render_edit_complete();
    }

    function render_edit_complete() {
        $this->display('admin/room/mail_document/edit_complete.t.html');
    }

    function action_delete() {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_list();
        }
        $where = "uniq_key = ".$this->request->get("uniq_key");
        $this->mailDocConvObj->delete($where);
        // 操作ログ
        $mail = $this->mailDocConvObj->getOne($where, "mail_address");
        $this->add_operation_log('mail_wb_upload_address_delete', array('mail' => $mail));
        $this->render_list();
    }

    function action_set_active() {
        $is_active = $this->request->get("is_active");
        $user_info = $this->session->get("user_info");
        $where = "user_key = ".$user_info["user_key"];
        $user_info = $this->userObj->getRow($where);
        // 既に項目がある場合は上書き
        if ($user_info["addtion"]) {
            $addition = unserialize($user_info["addtion"]);
        }
        $addition["mail_doc_conv"] = $is_active;
        $data["addition"] = serialize($addition);
        $ret = $this->userObj->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        }
        $this->render_list();
    }

    function action_set_extend_bandwith(){
    	$user_info = $this->session->get('user_info');
    	$data = array();
    	$room_key = mysql_real_escape_string( $this->request->get( 'room_key' ) );
    	if ( ! array_key_exists( $room_key, $this->session->get( 'room_info' ) ) ) {
    		$this->logger2->debug($_REQUEST, $room_key);
    		echo json_encode(false);
    		return "";
    	}

    	$where = sprintf( 'room_key = "%s" AND user_key=%d', $room_key , $user_info['user_key'] );
        $data["use_extend_bandwidth"] = $this->request->get( "use_extend_bandwidth" ) ? 1 : 0;
        //max_user_bandwidth?
        if($data["use_extend_bandwidth"]) {
            $data["max_user_bandwidth"] = "384";
        } else {
        	$data["max_user_bandwidth"] = "256";
        }
        $data["room_updatetime"] = date("Y-m-d H:i:s");
        require_once 'classes/dbi/room.dbi.php';
        $obj_Room = new RoomTable($this->get_dsn());
        $this->logger2->info(array($room_key, $where, $data), 'update extend_bandwith');
        $ret = $obj_Room->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            echo json_encode(false);
            return "";
        }
        echo json_encode(true);
        return "";
    }

    function action_add_room() {
    	$user_info = $this->session->get("user_info");
    	require_once("classes/dbi/user_room_setting.dbi.php");
    	$obj_UserRoomSetting = new UserRoomSettingTable($this->get_dsn());
    	$where = sprintf( "status = 1 AND user_key=%s", $user_info["user_key"] );
    	$now_UserRoomSetting = $obj_UserRoomSetting->getRow($where);
    	if(!$now_UserRoomSetting) {
    		$this->logger2->warn(array("message" => "user room setting haven't been setup.","user_info" => $user_info));
    		//error exit
    		$message = array(
    				"title" => ADD_ROOM_TITLE,
    				"text" => ADD_ROOM_ERROR_CONTENT,
    				"back_url" => "/services/admin/?action_room_plan",
    				"back_url_label" => BACK_BUTTON);
    		$this->template->assign("message", $message);
    		$this->display('admin/message.t.html');
    		return false;
    	}
    	$room_info = $now_UserRoomSetting;

    	$this->session->set("room_max_seat", $room_info["max_seat"], $this->_name_space);
    	$this->session->set("room_max_whiteboard_seat", $room_info["max_whiteboard_seat"], $this->_name_space);
    	$this->session->set("room_info", $room_info, $this->_name_space);
    	$this->session->set("room_type", $this->request->get("room_type"), $this->_name_space);
    	$this->render_add_room();
    }

    function render_add_room() {
    	$this->set_submit_key($this->_name_space);
    	if (EZValidator::isError($this->err_obj)) {
    		$msg_format = $this->get_message("error");
    		$msg        = $this->get_message("ERROR");
    		$err_fields = $this->err_obj->error_fields();
    		foreach ($err_fields as $key) {
    			$type = $this->err_obj->get_error_type($key);
    			$_msg = sprintf($msg[$type], $this->err_obj->get_error_rule($key, $type));
    			$error[$key] = sprintf($msg_format, $_msg);
    		}
    		$this->logger->info(__FUNCTION__."#error", __FILE__, __LINE__, $error);
    		$this->template->assign("error_info", $error);
    	}
    	$user_info = $this->session->get("user_info");
    	$room_info = $this->session->get("room_info", $this->_name_space);
    	if ($room_info["cabinet_filetype"]) {
    		$room_info["cabinet_filetype"] = unserialize($room_info["cabinet_filetype"]);
    	}
    	require_once( "classes/dbi/user_service_option.dbi.php" );
    	$objUserServiceOption = new UserServiceOptionTable( $this->get_dsn() );
    	$where = sprintf( "user_key=%s AND user_service_option_status=1", $user_info["user_key"] );
    	$optionList = $objUserServiceOption->getRowsAssoc( $where );
    	//$this->logger2->info($optionList);
    	if($optionList) {
    		foreach($optionList as $option) {
    			$row[] = $option["service_option_key"];
    			if($option["service_option_key"] == "23" && !$this->config->get("IGNORE_MENU", "teleconference")) {
    				$enable_teleconf = 1;
    				$this->template->assign(array("enable_teleconf" => $enable_teleconf,
    						"is_trial"        => $user_info['user_status'] == 2,
    						"service_name"    => $user_info['service_name']));
    			}
    		}
    		require_once("classes/N2MY_Account.class.php");
    		$obj_Account = new N2MY_Account($this->get_dsn());
    		$room_info["options"] = $obj_Account->getOptionList($row);
    		if($this->session->get("room_type", $this->_name_space) == "document")
    			$room_info["options"]["whiteboard"] = 1;
    	}
    	$this->logger2->info($room_info);

    	if (!$room_info["whiteboard_filetype"]) {
    		$document_ext = $this->config->getAll('DOCUMENT_FILES');
    		$image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
    		$room_info["whiteboard_filetype"] = serialize(array(
    				"document" => array_keys($document_ext),
    				"image"    => array_keys($image_ext)));
    	}
    	if (!$room_info["transceiver_number"]) {
    		$room_info["transceiver_number"] = 11;
    	}

    	if ($room_info_detail["options"]["meeting_ssl"]) {
    		$protocol_list = array(
    				"rtmpe:1935"  => "rtmpe:1935",
    				"rtmpe:80"    => "rtmpe:80",
    				"rtmpe:8080"  => "rtmpe:8080",
    				"rtmps-tls:443"   => "rtmps-tls:443",
    				"rtmps:443"   => "rtmps:443",
    				"rtmpte:80"   => "rtmpte:80",
    				"rtmpte:8080" => "rtmpte:8080",
    		);
    	} else {
    		$protocol_list = array(
    				"rtmp:1935"   => "rtmp:1935",
    				"rtmp:80"     => "rtmp:80",
    				"rtmp:8080"   => "rtmp:8080",
    				"rtmps-tls:443"   => "rtmps-tls:443",
    				"rtmps:443"   => "rtmps:443",
    				"rtmpt:80"    => "rtmpt:80",
    				"rtmpt:8080"  => "rtmpt:8080",
    		);
    	}
    	$protocol_deny_list  = array();
    	$protocol_allow_list = array();
    	$list                = array();

    	if (!$room_info["rtmp_protocol"]) {
    		$protocol_allow_list = $protocol_list;
    	} else {
    		$list = split(",", $room_info["rtmp_protocol"]);
    		foreach($list as $protocol) {
    			if (array_key_exists($protocol, $protocol_list)) {
    				$protocol_allow_list[$protocol] = $protocol_list[$protocol];
    			}
    		}
    	}
    	foreach($protocol_list as $key => $value) {
    		if (!array_key_exists($key, $protocol_allow_list)) {
    			$protocol_deny_list[$key] = $value;
    		}
    	}
    	$document_ext = $this->config->getAll('DOCUMENT_FILES');
    	$image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
    	if ($room_info["whiteboard_filetype"]) {
    		$filetype = unserialize($room_info["whiteboard_filetype"]);
    		$this->logger2->debug($filetype);
    		$room_info["whiteboard_filetype"] = array();
    		if ($filetype["document"]) {
    			foreach($filetype["document"] as $val) {
    				foreach($document_ext as $type => $extensions) {
    					$_extensions = split(",", $extensions);
    					// 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
    					if (in_array($val, $_extensions) || $val == $type) {
    						$room_info["whiteboard_filetype"][$type] = 1;
    					}
    				}
    			}
    		}
    		if ($filetype["image"]) {
    			foreach($filetype["image"] as $val) {
    				foreach($image_ext as $type => $extensions) {
    					$_extensions = split(",", $extensions);
    					// 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
    					if (in_array($val, $_extensions) || $val == $type) {
    						$room_info["whiteboard_filetype"][$type] = 1;
    					}
    				}
    			}
    		}
    	}

    	$net_speed_list = $this->get_message("NET_SPEED_LIST");
    	foreach ($net_speed_list as $key => $val) {
    		$net_speed_list[$key] = substr($val, strpos($val, ":") + 1);
    	}
        //V-CUBE One対応：会議室とメンバーの分類機能
        if($this->check_one_account() && ($this->session->get("service_mode") == "meeting")){
            require_once("classes/dbi/member.dbi.php");
            $obj_member = new MemberTable($this->get_dsn());
            $where = "member_status = 0 AND user_key = " . $user_info["user_key"];
            $member_list = $obj_member->getRowsAssoc($where);
            if($member_list){
                $this->template->assign("member_whitelist_deny", $member_list);
            }
        }

    	$this->template->assign(array(
    			"room_type"           => $this->session->get("room_type", $this->_name_space),
    			"room_max_seat"       => $this->session->get("room_max_seat", $this->_name_space),
    			"room_max_whiteboard_seat" => $this->session->get("room_max_whiteboard_seat", $this->_name_space),
    			"image_files"         => $image_ext,
    			"document_files"      => $document_ext,
    			"protocol_list"       => $protocol_list,
    			"net_speed_list"      => $net_speed_list,
    			"protocol_deny_list"  => $protocol_deny_list,
    			"protocol_allow_list" => $protocol_allow_list,
    			"room_info"           => $room_info));

    	$rec_disable_flag = 0;
    	if($room_info["options"]["whiteboard"]){
    		$rec_disable_flag = 1;
    	}
    	$this->template->assign("rec_disable_flag",$rec_disable_flag);
    	$this->display('admin/room/add.t.html');
    }

    function action_add_room_complete() {
    	// 二重処理チェック
    	if (!$this->check_submit_key($this->_name_space)) {
    		$this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
    		return $this->action_add_room();
    	}
    	//get user plan or options]
    	$request = $this->request->getAll();
    	$this->logger2->info($request);
    	if ($request["cabinet_filetype"]) {
    		foreach ($request["cabinet_filetype"] as $extension) {
    			$extensions[] = strtolower($extension);
    		}
    		$request["cabinet_filetype"] = $extensions;
    	}

    	$user_info = $this->session->get("user_info");
    	require_once("classes/dbi/user_room_setting.dbi.php");
    	$obj_UserRoomSetting = new UserRoomSettingTable($this->get_dsn());
    	$where = sprintf( "status = 1 AND user_key=%s", $user_info["user_key"] );
    	$now_UserRoomSetting = $obj_UserRoomSetting->getRow($where);

    	if($request["is_default_add"]) {
    		$add_room_data = $now_UserRoomSetting;
    		if($this->session->get("room_type", $this->_name_space) == "document")
    			$default_room_name = "DOC";
    		else
    			$default_room_name = "ROOM";
    		$add_room_data["room_name"] = $add_room_data["room_name"]?$add_room_data["room_name"]:$default_room_name;

    		unset($add_room_data["user_room_setting_key"]);
    		unset($add_room_data["create_datetime"]);
    		unset($add_room_data["update_datetime"]);
    		unset($add_room_data["delete_datetime"]);
    		unset($add_room_data["expire_datetime"]);
    		unset($add_room_data["status"]);

    		unset($add_room_data["pgi_client_id"]);
    		unset($add_room_data["pgi_client_pw"]);
    		unset($add_room_data["is_device_check"]);
    	} else {
    		$add_room_data = array(
    				"user_key"					  => $user_info["user_key"],
    				"max_seat"					  => $request["max_seat"]?$request["max_seat"]:$now_UserRoomSetting["max_seat"],
    				"max_audience_seat"			  => $now_UserRoomSetting["max_audience_seat"],
    				"max_whiteboard_seat"		  => $request["max_whiteboard_seat"],
    				"max_guest_seat"			  => $now_UserRoomSetting["max_guest_seat"],
    				"max_guest_seat_flg"		  => $now_UserRoomSetting["max_guest_seat_flg"],
    				"extend_max_seat"			  => $now_UserRoomSetting["extend_max_seat"],
    				"extend_max_audience_seat"	  => $now_UserRoomSetting["extend_max_audience_seat"],
    				"extend_seat_flg"			  => $now_UserRoomSetting["extend_seat_flg"],
    				"max_room_bandwidth"		  => $now_UserRoomSetting["max_room_bandwidth"],
    				"max_user_bandwidth"		  => $now_UserRoomSetting["max_user_bandwidth"],
    				"min_user_bandwidth"		  => $now_UserRoomSetting["min_user_bandwidth"],
    				"max_shared_memo_size"		  => $now_UserRoomSetting["max_shared_memo_size"],
    				"default_camera_size"         => $now_UserRoomSetting["default_camera_size"],
    				"default_auto_rec_use_flg"	  => $now_UserRoomSetting["default_auto_rec_use_flg"],
    				"invited_limit_time"		  => $now_UserRoomSetting["invited_limit_time"],
    				"meeting_limit_time"		  => $now_UserRoomSetting["meeting_limit_time"],
    				"mobile_mix"				  => $now_UserRoomSetting["mobile_mix"],
    				"use_sound_codec"			  => $now_UserRoomSetting["use_sound_codec"],
    				"hd_flg"					  => $now_UserRoomSetting["hd_flg"],
    				"active_speaker_mode_only_flg"=> $now_UserRoomSetting["active_speaker_mode_only_flg"],
    				"room_name"                   => $request["room_name"],
    				"cabinet"                     => $request["cabinet"],
    				"cabinet_size"                => $request["cabinet_size"],
    				"cabinet_filetype"            => serialize($request["cabinet_filetype"]),
    				"mfp"                         => $request["mfp"],
    				"default_layout_4to3"         => $request["default_layout_4to3"],
    				"default_layout_16to9"        => $request["default_layout_16to9"],
    				"rtmp_protocol"               => $request["rtmp_protocol"],
    				"whiteboard_page"             => $request["whiteboard_page"],
    				"whiteboard_size"             => $request["whiteboard_size"],
    				"whiteboard_filetype"         => serialize($request["whiteboard_filetype"]),
    				"default_microphone_mute"     => $request["default_microphone_mute"],
    				"default_camera_mute"         => $request["default_camera_mute"],
    				"is_auto_transceiver"         => $request["is_auto_transceiver"],
    				"default_h264_use_flg"        => $request["default_h264_use_flg"],
    				"default_agc_use_flg"         => $request["default_agc_use_flg"],
    				"transceiver_number"          => $request["transceiver_number"]?$request["transceiver_number"]:0,
    				"is_auto_voice_priority"      => $request["is_auto_voice_priority"],
    				"is_netspeed_check"           => $request["is_netspeed_check"],
    				"is_wb_no"                    => $request["is_wb_no"],
    				"is_device_skip"              => $request["is_device_skip"],
    				"is_participant_notifier"     => $request["is_participant_notifier"],
    				"use_teleconf"                => $request["use_teleconf"]?1:0,
    				"use_pgi_dialin"              => $request["use_pgi_dialin"]?1:0,
    				"use_pgi_dialin_free"         => $request["use_pgi_dialin_free"]?1:0,
    				"use_pgi_dialin_lo_call"      => $request["use_pgi_dialin_lo_call"]?1:0,
    				"use_pgi_dialout"             => $request["use_pgi_dialout"]?1:0,
    				"audience_chat_flg"           => $request["audience_chat_flg"],
    				"disable_rec_flg"             => $request["disable_rec_flg"],
    				"is_personal_wb"              => $request["is_personal_wb"],
    				"is_convert_wb_to_pdf"        => $request["is_convert_wb_to_pdf"],
    				"mobile_4x4_layout"           => $request["mobile_4x4_layout"]?1:0,
    				"rec_gw_convert_type"         => $request["rec_gw_convert_type"] ? $request["rec_gw_convert_type"] : "mov",
    				"has_member_whitelist"        => $request["has_member_whitelist"] ? $request["has_member_whitelist"] : "0",
    				"rec_gw_userpage_setting_flg" => isset($request["rec_gw_userpage_setting_flg"]) ? $request["rec_gw_userpage_setting_flg"] : "1",
    				"customer_camera_use_flg"     => isset($request["customer_camera_use_flg"]) ? $request["customer_camera_use_flg"] : "1",
    				"customer_camera_status"      => isset($request["customer_camera_status"]) ? $request["customer_camera_status"] : "1",
    				"customer_sharing_button_hide_status" => isset($request["customer_sharing_button_hide_status"]) ? $request["customer_sharing_button_hide_status"] : "1",
    				"wb_allow_flg"                => isset($request["wb_allow_flg"]) ? $request["wb_allow_flg"] : "0",
    				"chat_allow_flg"              => isset($request["chat_allow_flg"]) ? $request["chat_allow_flg"] : "0",
    				"print_allow_flg"             => isset($request["print_allow_flg"]) ? $request["print_allow_flg"] : "0",
    				"active_speaker_mode_use_flg"     => isset($request["active_speaker_mode_use_flg"]) ? $request["active_speaker_mode_use_flg"] : "0",
    				"active_speaker_mode_speaker_count"  => $request["active_speaker_mode_speaker_count"],
    				"active_speaker_mode_user_count"  => $request["active_speaker_mode_user_count"],
    				"active_speaker_default_layout_4to3" => $request["active_speaker_mode_4to3"],
    				"active_speaker_default_layout_16to9"=> $request["active_speaker_mode_16to9"],
    				"prohibit_extend_meeting_flg" => isset($request["prohibit_extend_meeting_flg"]) ? $request["prohibit_extend_meeting_flg"] : "0",
    		);
    		if($add_room_data["active_speaker_mode_only_flg"] == 1) {
    			$add_room_data["active_speaker_mode_use_flg"] = 1;
    		}
    	}
    	if($user_info["account_plan"] == "one_id_host"){
    	    $add_room_data["max_seat"] = $now_UserRoomSetting["max_seat"];
    	    $add_room_data["max_whiteboard_seat"] = $now_UserRoomSetting["max_whiteboard_seat"];;
    	}
    	require_once("classes/dbi/room.dbi.php");
    	$obj_Room = new RoomTable($this->get_dsn());

    	$where_room = sprintf( "user_key=%s", $user_info["user_key"] );
    	$_count = $obj_Room->numRows($where_room);
    	$room_key = $user_info["user_id"]."-".($_count + 1)."-".substr(md5(uniqid(time(), true)), 0,4);
    	$add_room_data["room_key"] = $room_key;
    	$add_room_data["room_status"] = 1;

    	$add_room_data["room_sort"] = $_count+1;
    	$add_room_data["room_registtime"] = date("Y-m-d H:i:s");

    	//add room
    	$this->logger2->info($add_room_data);


    	$rules = array(
    			"room_name"     => array("required" => true,
    					"maxlen"   => 20),
    			"cabinet"       => array("required" => true,
    					"allow"    => array("0", "1")),
    			"mfp"           => array("required" => true,
    					"allow"    => array("all", "select", "deny")),);
    	if(!$request["is_default_add"]) {
    		$rules["rtmp_protocol"] = array("required" => true,);
    	}
    	$this->err_obj = $obj_Room->check($add_room_data, $rules);

    	if ($request["cabinet_filetype"]) {
    		foreach ($request["cabinet_filetype"] as $ext) {
    			if (ereg("[^0-9a-z]", $ext)) {
    				$this->err_obj->set_error("cabinet_filetype", "alnum", $ext);
    				break;
    			}
    		}
    	}
    	if (!$this->config->get("IGNORE_MENU", "teleconference")) {
    		if ($add_room_data['use_teleconf'] && !$add_room_data['use_pgi_dialin']
    				&& !$add_room_data['use_pgi_dialin_free'] && !$add_room_data['use_pgi_dialin_lo_call'] && !$add_room_data['use_pgi_dialout']) {
    			$this->err_obj->set_error("teleconf", "minitem", 1);
    		}
    	}
//     	if (!$this->config->get("IGNORE_MENU", "video_conference")) {
//     		require_once("classes/mcu/resolve/Resolver.php");
//     		if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($room_key))) {
//     			require_once("classes/dbi/ives_setting.dbi.php");
//     			$obj_ivesSetting = new IvesSettingTable($this->get_dsn());
//     			$ives_data = array("mcu_hd_flg" => $request["mcu_hd_flg"] ? 1: 0);
//     			$where_ives = "room_key ='".addslashes($request["room_key"])."'".
//     					" AND is_deleted = 0";
//     			$obj_ivesSetting->update($ives_data, $where_ives);
//     		}
//     	}
    	// エラー
    	if (EZValidator::isError($this->err_obj)) {
    		$this->session->set("room_info", $add_room_data, $this->_name_space);
    		return $this->render_add_room();
    	}
    	if($add_room_data["max_seat"] <= 12) {
    		$add_room_data["active_speaker_mode_only_flg"] = 0;
    	}
    	if($this->session->get("room_type", $this->_name_space) == "document") {
    		$add_room_data["active_speaker_mode_only_flg"] = 0;
    		$add_room_data["active_speaker_mode_use_flg"] = 0;
    		$add_room_data["disable_rec_flg"] = 1;
    	}

    	$ret = $obj_Room->add($add_room_data);
    	if (DB::isError($ret)) {
    		$this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
    		return false;
    	}

    	$relation_db = new N2MY_DB($this->get_auth_dsn(), "relation");
    	$relation_data = array(
    			"relation_key" => $room_key,
    			"relation_type" => "mfp",
    			"user_key" => $user_info["user_id"],
    			"status" => "1",
    			"create_datetime" => date("Y-m-d H:i:s"),
    	);
    	$relation_db->add($relation_data);

    	//add room option
    	require_once( "classes/dbi/user_service_option.dbi.php" );
        $objUserServiceOption = new UserServiceOptionTable( $this->get_dsn() );
        $where = sprintf( "user_key=%s AND user_service_option_status=1", $user_info["user_key"] );
        $optionList = $objUserServiceOption->getRowsAssoc( $where );
    	$this->logger2->info($optionList);
    	$objOrderedServiceOption = new N2MY_DB( $this->get_dsn(), "ordered_service_option" );
    	if($optionList) {
    		foreach ($optionList as $option) {
    			$data = array(
    					"room_key"						    => $room_key,
    					"user_service_option_key"           => $option["user_service_option_key"],
    					"service_option_key"                => $option["service_option_key"],
    					"ordered_service_option_status"     => 1,
    					"ordered_service_option_registtime" => date("Y-m-d H:i:s"),
    					"ordered_service_option_starttime"  => date("Y-m-d H:i:s"),
    			);
    			$result = $objOrderedServiceOption->add( $data );
    			if ($option["service_option_key"] == "23") {
    				require_once("classes/dbi/pgi_setting.dbi.php");
    				$pgi_setting_table = new PGiSettingTable($this->get_dsn());
    				$pgi_settings  = $pgi_setting_table->findByRoomKey($room_key);
    				if (!$pgi_settings) {
    					require_once( "classes/ONE_Account.class.php" );
    					$obj_ONEAccount = new ONE_Account($this->get_dsn());
    					//$pgi_form = $obj_ONEAccount->get_user_pgi_setting();
    					//pgi setting
    					$obj_ONEAccount->set_default_room_pgi_setting($user_info["user_key"],$room_key);
    				}
//     				$room_data = array("use_teleconf" => "1",
//     						"use_pgi_dialin"      => "1",
//     						"use_pgi_dialin_free" => "1",
//     						"use_pgi_dialin_lo_call" => "1",
//     						"use_pgi_dialout"     => "1");
//     				$where_room = "room_key = '".addslashes($room_key)."'";
//     				$room_data = $obj_Room->update($room_data, $where_room);
    			}
    		}
    	}

    	if($this->session->get("room_type", $this->_name_space) == "document") {
    		$data = array(
    				"room_key"						    => $room_key,
    				"user_service_option_key"           => $option["user_service_option_key"],
    				"service_option_key"                => 16,
    				"ordered_service_option_status"     => 1,
    				"ordered_service_option_registtime" => date("Y-m-d H:i:s"),
    				"ordered_service_option_starttime"  => date("Y-m-d H:i:s"),
    		);
    		$result = $objOrderedServiceOption->add( $data );
    	} else {
    	    require_once( "classes/dbi/user_plan.dbi.php" );
    		$obj_UserPlan = new UserPlanTable($this->get_dsn());
    		$where = sprintf( "user_key='%s' AND user_plan_status=1", $user_info["user_key"] );
    		$nowPlanInfo = $obj_UserPlan->getRow( $where );
    		require_once("classes/dbi/service.dbi.php");
    		$obj_Service = new ServiceTable($this->get_auth_dsn());
    		$where_user_service = sprintf( "service_key='%s'", $nowPlanInfo["service_key"] );
    		$now_plan_info = $obj_Service->getRow($where_user_service);

    		require_once( "classes/ONE_Account.class.php" );
    		$obj_ONEAccount = new ONE_Account($this->get_dsn());
    		//MCU対応
    		$mcu_data = array(
    				"ives_mcu_server_key" =>  ONE_DEFAULT_MCU_SERVER,
    				"ives_profile_id" => $now_plan_info["use_trial_plan"]?321:323,
    				"ives_num_profiles" => $add_room_data["max_seat"]>20?20:$add_room_data["max_seat"],
    				"ives_use_active_speaker" => 1,
    				"ives_use_hd_profile" => 0,
    		);

    		if($obj_ONEAccount->set_room_mcu_server($room_key, $mcu_data)) {
    			$data = array(
    					"room_key"						    => $room_key,
    					"user_service_option_key"           => $option["user_service_option_key"],
    					"service_option_key"                => 24,
    					"ordered_service_option_status"     => 1,
    					"ordered_service_option_registtime" => date("Y-m-d H:i:s"),
    					"ordered_service_option_starttime"  => date("Y-m-d H:i:s"),
    			);
    			$result = $objOrderedServiceOption->add( $data );
    		} else {
    			$this->logger2->warn("部屋のMCUを紐付きが失敗しました。"," add room for admin page");
    		}
    	}
        // V-CUBE One対応：会議室とメンバー紐付け機能追加（管理者＞会議室設定）
        if($this->check_one_account() && ($this->session->get("service_mode") == "meeting")){
            $obj_member_room_whitelist = new N2MY_DB($this->get_dsn(), "member_room_whitelist");
            if($request["has_member_whitelist"] == 1 && $request["whitelist_members"]){
                $whitelist_member_keys = explode(",", $request["whitelist_members"]);
                foreach ($whitelist_member_keys as $key) {
                    $whitelist_data = array(
                        "member_key"       => $key,
                        "room_key"         => $room_key,
                        "whitelist_status" => 1,
                        "create_datetime"  => date("Y-m-d H:i:s")
                    );
                    $obj_member_room_whitelist->add($whitelist_data);
                }
            }
            $this->logger2->info("-------Process Member Whitelist End------");
        }
    	$message = array(
    				"title" => ADD_ROOM_TITLE,
    				"text" => ADD_ROOM_SUCCESS_CONTENT,
    				"back_url" => "/services/admin/?action_room_plan",
    				"back_url_label" => BACK_BUTTON);
    	$this->template->assign("message", $message);
    	$this->display('admin/message.t.html');
    	return true;
    }

    function set_session($data) {
        $this->session->set("form_data", $data, $this->_name_space);
    }

    function get_session() {
        return $this->session->get("form_data", $this->_name_space);
    }

    function clear_session() {
        $this->session->remove("form_data", $this->_name_space);
    }

    function check($data) {
        return $this->mailDocConvObj->check($data);
    }

    function action_set_all($err = null) {
        $document_ext = $this->config->getAll('DOCUMENT_FILES');
        $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
        $room_info["whiteboard_filetype"] = serialize(array(
            "document"  => array_keys($document_ext),
            "image"     => array_keys($image_ext)
            ));
        // 初期化
        if (!$room_info["cabinet"])                     $room_info["cabinet"] = 1;
        if (!$room_info["cabinet_size"])                $room_info["cabinet_size"] = 20;
        if (!$room_info["mfp"])                         $room_info["mfp"] = "all";
        if (!$room_info["default_layout_4to3"])         $room_info["default_layout_4to3"] = 'normal';
        if (!$room_info["default_layout_16to9"])        $room_info["default_layout_16to9"] = 'small';
        if (!$room_info["whiteboard_page"])             $room_info["whiteboard_page"] = 100;
        if (!$room_info["whiteboard_size"])             $room_info["whiteboard_size"] = 20;
        if (!$room_info["default_microphone_mute"])     $room_info["default_microphone_mute"] = "off";
        if (!$room_info["default_camera_mute"])         $room_info["default_camera_mute"] = "off";
        if (!$room_info["default_h264_use_flg"])        $room_info["default_h264_use_flg"] = 1;
        if (!$room_info["default_agc_use_flg"])         $room_info["default_agc_use_flg"] = 1;
        if (!$room_info["is_auto_transceiver"])         $room_info["is_auto_transceiver"] = 0;
        if (!$room_info["transceiver_number"])          $room_info["transceiver_number"] = 11;
        if (!$room_info["is_auto_voice_priority"])      $room_info["is_auto_voice_priority"] = 0;
        if (!$room_info["is_netspeed_check"])           $room_info["is_netspeed_check"] = "";
        if (!$room_info["is_wb_no"])                    $room_info["is_wb_no"] = "1";
        if (!$room_info["is_device_skip"])              $room_info["is_device_skip"] = "0";
        if (!$room_info["is_participant_notifier"])     $room_info["is_participant_notifier"] = "1";
        if (!$room_info["audience_chat_flg"])           $room_info["audience_chat_flg"] = "0";
        if (!$room_info["disable_rec_flg"])             $room_info["disable_rec_flg"] = "0";
        if (!$room_info["is_personal_wb"])              $room_info["is_personal_wb"] = "1";
        if (!$room_info["is_convert_wb_to_pdf"])        $room_info["is_convert_wb_to_pdf"] = "1";
        if (!$room_info["is_invite_button"])            $room_info["is_invite_button"] = "1";
        if (!$room_info["rec_gw_convert_type"])         $room_info["rec_gw_convert_type"] = "mov";
        if (!$room_info["rec_gw_userpage_setting_flg"]) $room_info["rec_gw_userpage_setting_flg"] = "0";
		if (!$room_info["mobile_4x4_layout"])           $room_info["mobile_4x4_layout"]  = "0";
		if (!$room_info["hd_flg"])                      $room_info["hd_flg"]             = "1";
        if (!$room_info["use_teleconf"])            	$room_info["use_teleconf"]       = "1";
        if (!$room_info["use_pgi_dialin"])          	$room_info["use_pgi_dialin"]     = "1";
        if (!$room_info["use_pgi_dialin_free"])     	$room_info["use_pgi_dialin_free"]= "1";
        if (!$room_info["use_pgi_dialout"])         	$room_info["use_pgi_dialout"]    = "1";
    	if (!$room_info["use_pgi_dialin_lo_call"])      $room_info["use_pgi_dialin_lo_call"]    = "1";
        // if (!$room_info["has_member_whitelist"])        $room_info["has_member_whitelist"]    = "0";
    	if (!$room_info["is_convert_personal_wb_to_pdf"])      $room_info["is_convert_personal_wb_to_pdf"]    = "1";

        //sales
        if (!$room_info["customer_camera_use_flg"])             $room_info["customer_camera_use_flg"]    = "1";
        if (!$room_info["customer_camera_status"])              $room_info["customer_camera_status"]    = "1";
        if (!$room_info["customer_sharing_button_hide_status"]) $room_info["customer_sharing_button_hide_status"]    = "1";
        if (!$room_info["wb_allow_flg"])                        $room_info["wb_allow_flg"]    = "0";
        if (!$room_info["chat_allow_flg"])                      $room_info["chat_allow_flg"]    = "0";
        if (!$room_info["print_allow_flg"])                     $room_info["print_allow_flg"]    = "0";
        if (!$room_info["prohibit_extend_meeting_flg"])         $room_info["prohibit_extend_meeting_flg"]    = "0";

        $user_info = $this->session->get("user_info");
        if($this->session->get("service_mode") == "sales" || $user_info["meeting_version"] == null && $user_info['account_model'] !== 'centre') {
          $room_info["default_layout_16to9"] = "normal";
        }
        $this->session->set("room_info", $room_info, $this->_name_space);
        return $this->render_set_form($err);
    }

    function render_set_form($err = null) {
        if (EZValidator::isError($err)) {
            $msg_format = $this->get_message("error");
            $msg        = $this->get_message("ERROR");
            $err_fields = $this->err_obj->error_fields();
            foreach ($err_fields as $key) {
                $type = $this->err_obj->get_error_type($key);
                $_msg = sprintf($msg[$type], $this->err_obj->get_error_rule($key, $type));
                $error[$key] = sprintf($msg_format, $_msg);
            }
            $this->logger->info(__FUNCTION__."#error", __FILE__, __LINE__, $error);
            $this->template->assign("error_info", $error);
        }
        $this->set_submit_key();
        $room_info = $this->session->get("room_info", $this->_name_space);
        $room_list = $this->get_room_info();
        $enable_teleconf = false;
        foreach ($room_list as $room_key => $room_detail) {
            if (@$room_detail["options"]["teleconference"]) {
                $enable_teleconf = true;
            }
            // 暗号化、TLSオプション無し
            if ($room_detail["options"]["meeting_ssl"] == 0) {
                $protocol_list = array(
                    "rtmp:1935"   => "rtmp:1935",
                    "rtmp:80"     => "rtmp:80",
                    "rtmp:8080"   => "rtmp:8080",
                    "rtmps-tls:443"   => "rtmps-tls:443",
                    "rtmps:443"   => "rtmps:443",
                    "rtmpt:80"    => "rtmpt:80",
                    "rtmpt:8080"  => "rtmpt:8080",
                );
            // 暗号化オプションあり
            } else {
                $protocol_list2 = array(
                    "rtmpe:1935"  => "rtmpe:1935",
                    "rtmpe:80"    => "rtmpe:80",
                    "rtmpe:8080"  => "rtmpe:8080",
                    "rtmps-tls:443"   => "rtmps-tls:443",
                    "rtmps:443"   => "rtmps:443",
                    "rtmpte:80"   => "rtmpte:80",
                    "rtmpte:8080" => "rtmpte:8080",
                );
            }
        }
        // 通常プロトコル
        $protocol_deny_list = array();
        $protocol_allow_list = array();
        $list = array();
        if ($protocol_list) {
            if (!$room_info["rtmp_protocol"]) {
                $protocol_allow_list = $protocol_list;
            } else {
                $list = split(",", $room_info["rtmp_protocol"]);
                foreach($list as $protocol) {
                    if (array_key_exists($protocol, $protocol_list)) {
                        $protocol_allow_list[$protocol] = $protocol_list[$protocol];
                    }
                }
            }
            foreach($protocol_list as $key => $value) {
                if (!array_key_exists($key, $protocol_allow_list)) {
                    $protocol_deny_list[$key] = $value;
                }
            }
        }
        // 暗号化プロトコル
        $protocol_deny_list2 = array();
        $protocol_allow_list2 = array();
        if ($protocol_list2) {
            $list = array();
            if (!$room_info["rtmp_protocol"]) {
                $protocol_allow_list2 = $protocol_list2;
            } else {
                $list = split(",", $room_info["rtmp_protocol"]);
                foreach($list as $protocol) {
                    if (array_key_exists($protocol, $protocol_list2)) {
                        $protocol_allow_list2[$protocol] = $protocol_list2[$protocol];
                    }
                }
            }
            foreach($protocol_list2 as $key => $value) {
                if (!array_key_exists($key, $protocol_allow_list2)) {
                    $protocol_deny_list2[$key] = $value;
                }
            }
        }
        $document_ext = $this->config->getAll('DOCUMENT_FILES');
        $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
        if ($room_info["whiteboard_filetype"]) {
            $filetype = unserialize($room_info["whiteboard_filetype"]);
            $room_info["whiteboard_filetype"] = array();
            if ($filetype["document"]) {
                foreach($filetype["document"] as $val) {
                    foreach($document_ext as $type => $extensions) {
                        $_extensions = split(",", $extensions);
                        // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                        if (in_array($val, $_extensions) || $val == $type) {
                            $room_info["whiteboard_filetype"][$type] = 1;
                        }
                    }
                }
            }
            if ($filetype["image"]) {
                foreach($filetype["image"] as $val) {
                    foreach($image_ext as $type => $extensions) {
                        $_extensions = split(",", $extensions);
                        // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                        if (in_array($val, $_extensions) || $val == $type) {
                            $room_info["whiteboard_filetype"][$type] = 1;
                        }
                    }
                }
            }
        }
        if ($room_info["cabinet_filetype"]) {
            $room_info["cabinet_filetype"] = unserialize($room_info["cabinet_filetype"]);
        }
        $net_speed_list = $this->get_message("NET_SPEED_LIST");
        foreach($net_speed_list as $key => $val) {
            $net_speed_list[$key] = substr($val, strpos($val, ":") + 1);
        }
        $user_info = $this->session->get("user_info");
        $this->template->assign(array("image_files"           => $image_ext,
                                      "document_files"        => $document_ext,
                                      "net_speed_list"        => $net_speed_list,
                                      "room_info"             => $room_info,
                                      "is_trial"              => $user_info['user_status'] == 2,
                                      "protocol_list"         => $protocol_list,
                                      "protocol_deny_list"    => $protocol_deny_list,
                                      "protocol_allow_list"   => $protocol_allow_list,
                                      "protocol_list2"        => $protocol_list2,
                                      "protocol_deny_list2"   => $protocol_deny_list2,
                                      "protocol_allow_list2"  => $protocol_allow_list2,
                                      "is_set_all"            => true,
                                      "enable_teleconf"       => $enable_teleconf, ));
        $this->template->assign("all_edit" , 1);

        //一括設定では46系ターミナルにおいて、３拠点レイアウトは選択不可
        $this->template->assign("terminal_count", 0);

        $this->display('admin/room/all.t.html');
    }


    function action_edit_all() {
        // 二重処理チェック
        if (!$this->check_submit_key()) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_set_form();
        }
        $user_info = $this->session->get( "user_info" );
        $request = $this->request->getAll();
        require_once 'classes/dbi/room.dbi.php';
        $obj_Room = new RoomTable($this->get_dsn());
        if ($request["cabinet_filetype"]) {
            foreach ($request["cabinet_filetype"] as $extension) {
                $extensions[] = strtolower($extension);
            }
            $request["cabinet_filetype"] = $extensions;
        }
        $data = array(
            "cabinet"                     => $request["cabinet"],
            "cabinet_size"                => $request["cabinet_size"],
            "cabinet_filetype"            => serialize($request["cabinet_filetype"]),
            "mfp"                         => $request["mfp"],
            "default_layout_4to3"         => $request["default_layout_4to3"],
            "default_layout_16to9"        => $request["default_layout_16to9"],
            "whiteboard_page"             => $request["whiteboard_page"],
            "whiteboard_size"             => $request["whiteboard_size"],
            "whiteboard_filetype"         => serialize($request["whiteboard_filetype"]),
            "default_microphone_mute"     => $request["default_microphone_mute"],
            "default_camera_mute"         => $request["default_camera_mute"],
            "default_agc_use_flg"         => $request["default_agc_use_flg"],
            "is_auto_transceiver"         => $request["is_auto_transceiver"],
            "transceiver_number"          => $request["transceiver_number"],
            "is_auto_voice_priority"      => $request["is_auto_voice_priority"],
            "is_wb_no"                    => $request["is_wb_no"],
            "is_device_skip"              => $request["is_device_skip"],
            "is_participant_notifier"     => $request["is_participant_notifier"],
        	"audience_chat_flg"           => $request["audience_chat_flg"],
            "disable_rec_flg"             => $request["disable_rec_flg"],
            "is_personal_wb"              => $request["is_personal_wb"],
            "is_convert_wb_to_pdf"        => $request["is_convert_wb_to_pdf"],
        	"is_convert_personal_wb_to_pdf" => $request["is_convert_personal_wb_to_pdf"],
        	'is_invite_button'            => $request['is_invite_button'],
            "hd_flg"                      => $request["hd_flg"],
            "mobile_4x4_layout"           => $request["mobile_4x4_layout"],
            "rec_gw_userpage_setting_flg" => $request["rec_gw_userpage_setting_flg"],
            "customer_camera_use_flg"     => $request["customer_camera_use_flg"],
            "customer_camera_status"      => $request["customer_camera_status"],
            "customer_sharing_button_hide_status" => $request["customer_sharing_button_hide_status"],
            "wb_allow_flg"                => $request["wb_allow_flg"],
            "chat_allow_flg"              => $request["chat_allow_flg"],
            "print_allow_flg"             => $request["print_allow_flg"],
            "prohibit_extend_meeting_flg" => $request["prohibit_extend_meeting_flg"],
            );

        $enable_teleconf = $this->_has_teleconferenc_room();

        if ($user_info["meeting_version"] != 0 && !$this->config->get("IGNORE_MENU", "teleconference") && ($enable_teleconf || $user_info["promotion_flg"]) ) {
            if ($request['use_teleconf']) {
                $data['use_teleconf']        = 1;
                $data['use_pgi_dialin']      = $request['use_pgi_dialin']      ? 1: 0;
                $data['use_pgi_dialin_free'] = $request['use_pgi_dialin_free'] ? 1: 0;
                $data['use_pgi_dialout']     = $request['use_pgi_dialout']     ? 1: 0;
                $data['use_pgi_dialin_lo_call']     = $request['use_pgi_dialin_lo_call']     ? 1: 0;
            } else {
                $data['use_teleconf']           = 0;
                $data['use_pgi_dialin']         = 0;
                $data['use_pgi_dialin_free']    = 0;
                $data['use_pgi_dialout']        = 0;
                $data['use_pgi_dialin_lo_call'] = 0;
            }
        }
        $rules = array("cabinet" => array("required" => true,
                                          "allow"    => array("0", "1")),
                       "mfp"     => array("required" => true,
                                          "allow"    => array("all", "select", "deny")),);
        $this->err_obj = $obj_Room->check($data, $rules);
        if ($request["cabinet_filetype"]) {
            foreach($request["cabinet_filetype"] as $ext) {
                if (ereg("[^0-9a-z]", $ext)) {
                    $this->err_obj->set_error("cabinet_filetype", "alnum", $ext);
                    break;
                }
            }
        }
        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            if ($data['use_teleconf'] && !$data['use_pgi_dialin']
             && !$data['use_pgi_dialin_free'] && !$data['use_pgi_dialin_lo_call'] && !$data['use_pgi_dialout']) {
                $this->err_obj->set_error("teleconf", "minitem", 1);
                foreach (array('use_teleconf', 'use_pgi_dialin', 'use_pgi_dialin_free', 'use_pgi_dialin_lo_call','use_pgi_dialout') as $col) {
                    $teleconf_data[$col] = $data[$col];
                    unset($data[$col]);
                }
            }

        }
        // エラー
        if (EZValidator::isError($this->err_obj)) {
            $room_info = $this->session->get("room_info", $this->_name_space);
            $this->session->set("room_info", $data, $this->_name_space);
            return $this->action_set_all($this->err_obj);
        }
        //-------------------------------
        //V4MTGVFOUR-2232 【One】管理者IDでS&Sにログインした際にタイムアウトが発生する
        //Room情報全取得ではなく、RoomKey取得だけで問題なさそうだが検証ができていないので、
        //冗長化ではあるがis_room_splitチェックを行い影響を少なくして対応とする。
        //-------------------------------
        if($this->session->get("is_room_split") && $this->session->get("service_mode") == "sales"){
            require_once ("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account( $this->get_dsn() );
            $room_keys = $obj_N2MY_Account->getSalesRoomKey( $user_info["user_key"] );
            foreach ($room_keys as $room_key) {
                $where = sprintf( "room_key='%s' AND user_key='%s'", $room_key, $user_info["user_key"] );
                $obj_Room->update($data, $where);
            }
        }else{
            $room_list = $this->get_room_info();
            foreach ($room_list as $room_key => $room_info) {
                if ($room_info["options"]["meeting_ssl"]) {
                    $data["rtmp_protocol"] = $request["rtmp_protocol2"];
                } else {
                    $data["rtmp_protocol"] = $request["rtmp_protocol"];
                }
                $where = sprintf( "room_key='%s' AND user_key='%s'", $room_key, $user_info["user_key"] );
                if ($room_info["options"]["teleconference"] && !$this->config->get("IGNORE_MENU", "teleconference")) {
                    $obj_Room->update($data, $where);
                } else {
                    $obj_Room->update($data, $where);
                }
            }
        }

        // 操作ログ
        $this->add_operation_log('setup_room');
        header("Location: /services/admin/?action_room_plan&setting=1");
    }

    function action_video_conference_setting($room_key = "", $message = "") {
        $user_info = $this->session->get("user_info");
        $room_key = $room_key ? $room_key : $this->request->get("room_key");

        require_once("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable($this->get_dsn());
        $room_info = $obj_Room->getRow(sprintf("user_key = %s AND room_key='%s'", $user_info["user_key"], $room_key), "room_key, room_name");

        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($room_key))) {
            $ives_setting = $videoConferenceClass->getIvesInfoByRoomKey($room_key);
            $ives_setting["video_conference_address"] = $videoConferenceClass->getRegularAddress($ives_setting["ives_did"]);
        }
        $this->logger2->info($ives_setting);

        $this->template->assign("room_info", $room_info);
        $this->template->assign("ives_setting", $ives_setting);
        $this->template->assign("message", $message);
        // 登録一覧
        $this->display('admin/room/video_conference/index.t.html');
    }

    function action_edit_video_conference_setting() {
        require_once("classes/mgm/dbi/mcu_server.dbi.php");
        require_once("classes/mcu/config/McuConfigProxy.php");

        $user_info = $this->session->get("user_info");
        $room_key = $this->request->get("room_key");

        if (!$room_key) {
            $this->action_video_conference_setting();
            exit;
        }
        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($room_key))) {
            $ives_setting = $videoConferenceClass->getIvesInfoByRoomKey($room_key);
            $ives_setting["video_conference_address"] = $videoConferenceClass->getRegularAddress($ives_setting["ives_did"]);
        }
        require_once("classes/dbi/ives_setting.dbi.php");
        $obj_ivesSetting = new IvesSettingTable($this->get_dsn());
        $update_where = "ives_setting_key = " . $ives_setting["ives_setting_key"] . " AND is_deleted = 0";
        $data = array();
        //会議中、予約があった場合は変更できないようにする
        require_once 'classes/dbi/room.dbi.php';
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'";
        $room_info = $obj_Room->getRow($where, "room_key, meeting_key");

        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $where = "meeting_ticket = '".$room_info["meeting_key"]."'".
            " AND is_active = 1";
        $rowNowMeeting = $obj_Meeting->getRow($where);

        require_once ("classes/dbi/reservation.dbi.php");
        $obj_Reservation = new ReservationTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'".
                 " AND reservation_status = 1".
                 " AND (reservation_starttime > '".date("Y-m-d H:i:s")."' OR reservation_endtime > '".date("Y-m-d H:i:s")."')";
        $reservation_count = $obj_Reservation->numRows($where);
        if ($reservation_count > 0 || $rowNowMeeting) {
            $this->action_video_conference_setting($room_key, "reservation_error");
            exit;
        }
        if ($ives_setting["ignore_video_conference_address"] == 1) {
            $data = array("ignore_video_conference_address" => 0);
        } else {
            //Did再生成して非表示状態にする
            $mcu_db = new McuServerTable($this->get_auth_dsn());
            $mcu_host = $mcu_db->getActiveServerAddress($ives_setting["mcu_server_key"]);
            if (!$mcu_host) {
                $this->logger2->error("get mcu server host error");
                $this->action_video_conference_setting($room_key, "error");
                exit;
            }
            $vad = ($ives_setting["use_active_speaker"] == 1) ? true : false;
            require_once("classes/polycom/Polycom.class.php");
            require_once("classes/dbi/operation_log.dbi.php");
            $polycom = new PolycomClass($this->get_dsn());
            $polycom->setWsdlDomain($mcu_host);
            $polycom->removeAdhocConferenceTemplate($ives_setting["ives_did"]);
            $polycom->setWsdlDomain($mcu_host);
            $did = $polycom->createDid();
            $polycom->createAdhocConferenceTemplate($did, $vad);
            $this->logger2->info(array("room_key" => $room_key, "old_did" => $ives_setting["ives_did"], "new_did" => $did));
            $objOperationLog    = new OperationLogTable($this->get_dsn());
            $opration_log = array(
                "user_key"              => $user_info["user_key"],
                "remote_addr"           => $_SERVER["REMOTE_ADDR"],
                "action_name"           => 'change_ives_did',
                "operation_datetime"    => date("Y-m-d H:i:s"),
                "info"                  => serialize(array(
                    "room_key"          => $room_key,
                    "old_ives_did"      => $ives_setting["ives_did"],
                    "new_ives_did"      => $did,
                    ))
                );
            $objOperationLog->add($opration_log);
            $data = array("ives_did" => $did,
                          "ignore_video_conference_address" => 1);
        }
        $ives_setting = $obj_ivesSetting->update($data, $update_where);
        $this->action_video_conference_setting($room_key, "complete");
    }
}

$main = new AppMember();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
