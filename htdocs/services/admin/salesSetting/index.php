<?php
require_once('classes/AppFrame.class.php');
require_once('classes/N2MY_DBI.class.php');
require_once('classes/dbi/user.dbi.php');
require_once('classes/dbi/sales_setting_image.dbi.php');
require_once('classes/dbi/browser_title.dbi.php');

class AppAdminSalesSetting extends AppFrame
{

    var $_name_space = null;
    var $salesSettingTable = null;
    var $browserTitleTable = null;

    /**
     * 初期処理
     */
    function init()
    {
        $this->_name_space = md5(__FILE__);
        $this->salesSettingTable = new SalesSettingImageTable($this->get_dsn());
        $this->browserTitleTable = new BrowserTitleTable($this->get_dsn());
    }

    /**
     * ログイン認証
     */
    function auth()
    {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    /**
     * デフォルト処理
     */
    function default_view()
    {
        $this->action_sales_setting();
    }

    /**
     * S&S 入室時のロゴを差し替えられるようにする
     */
    function action_sales_setting($message = '')
    {
        $this->set_submit_key($this->_name_space);
        $userInfo = $this->session->get('user_info');
        $browserTitle = $this->browserTitleTable->getRow('status = 1 AND user_key = ' . $userInfo['user_key']);
        $this->template->assign('browser_title', $browserTitle['title']);
        $this->template->assign('logo_image', $this->salesSettingTable->getImageInfo($userInfo['user_key'], 'logo'));
        $this->template->assign('header_image', $this->salesSettingTable->getImageInfo($userInfo['user_key'], 'header'));
        $this->template->assign('seatLogo_image', $this->salesSettingTable->getImageInfo($userInfo['user_key'], 'seatLogo'));
        $this->template->assign('message', $message);
        $this->template->assign('user_info', $userInfo);
        $this->template->assign('header_size', array(
            'width' => $this->config->get('SALES_CUSTOM_IMAGE', 'header_width_recommend'),
            'height' => $this->config->get('SALES_CUSTOM_IMAGE', 'header_height_recommend')));
        $this->template->assign('logo_size', array(
            'width' => $this->config->get('SALES_CUSTOM_IMAGE', 'logo_width_recommend'),
            'height' => $this->config->get('SALES_CUSTOM_IMAGE', 'logo_height_recommend')));
        $this->template->assign('seatLogo_size', array(
            'width' => $this->config->get('SALES_CUSTOM_IMAGE', 'seatLogo_width_recommend'),
            'height' => $this->config->get('SALES_CUSTOM_IMAGE', 'seatLogo_height_recommend')));
        $this->display('admin/salesSetting/index.t.html');
    }

    /**
     * ブラウザタイトル
     */
    function action_set_title()
    {
        $message = '';
        $request = $this->request->getAll();
        $userInfo = $this->session->get('user_info');
        if ($this->check_submit_key($this->_name_space)) {
            if (mb_strlen($request['browser_title']) > SALES_SETTING_BROWSER_TITLE_LENGTH) {
                $message = '<li>' . BROWSER_TITLE_LENGTH_ERROR . '</li>';
                return $this->action_sales_setting($message);
            }
            if($this->browserTitleTable->setTitle($userInfo['user_key'], $request['browser_title'])){
                $message = '<li>' . BROWSER_TITLE_CHANGE_SUCCESS . '</li>';
            }
        }
        return $this->action_sales_setting($message);
    }

    /**
     * お知らせバナー
     */
    function action_set_announcement()
    {
        $message = '';
        $request = $this->request->getAll();
        if ($this->check_submit_key($this->_name_space)) {
            $userInfo = $this->session->get('user_info');
            $objUser = new UserTable($this->get_dsn());
            $data = array(
                'use_announcement' => $request['announcement'],
                'user_updatetime' => date('Y-m-d H:i:s', time()),
            );
            $where = 'user_key = ' . $userInfo['user_key'];
            $ret = $objUser->update($data, $where);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__ . "#DB ERROR!", __FILE__, __LINE__, $ret->getUserInfo());
            } else {
                $userInfo = $objUser->getRow($where);
                $this->session->set('user_info', $userInfo);
                $message = '<li>' . ANNOUNCEMENT_CHANGE_SUCCESS . '</li>';
            }
        }
        return $this->action_sales_setting($message);
    }

    /**
     * メインページ・ヘッダー
     * 入室前設定画面
     * カスタマー映像枠
     */
    function action_upload_image()
    {
        $message = '';
        $request = $this->request->getAll();
        if ($this->check_submit_key($this->_name_space)) {
            //バナーアップロード
            if ($_FILES) {
                $move_path = $this->get_work_dir() . "/salesSetting.jpg";
                $result = $this->validate_image($request['target'], $move_path);
                if ($result["message"]) {
                    $message = $result["message"];
                } else {
                    $upload_ret = $this->upload_image($result, $move_path);
                }
            }
        }
        if ($upload_ret) {
            switch ($upload_ret['target']) {
                case 'logo':
                    $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_LOGO_SUCCESS . '</li>';
                    break;
                case 'header':
                    $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_HEADER_SUCCESS . '</li>';
                    break;
                case 'seatLogo':
                    $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_SEATLOGO_SUCCESS . '</li>';
                    break;
            }
        }
        return $this->action_sales_setting($message);
    }

    function action_delete_image()
    {
        $request = $this->request->getAll();
        if ($this->check_submit_key($this->_name_space)) {
            $userInfo = $this->session->get('user_info');
            $condition = sprintf('status = 1 AND image_id = "%s"', mysql_real_escape_string($request['image_id']));
            $image = $this->salesSettingTable->getRow($condition);
            if($image){
                $this->salesSettingTable->update(array(
                    'status' => 0,
                    'update_datetime' => date("Y-m-d H:i:s")
                ), $condition);
                $imagePath = N2MY_APP_DIR . 'salesSetting/' . $userInfo['user_id'] . '/' . $image['type'] . '/' . $image['image_id'] . '.' . $image['extension'];
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                    switch ($image['type']) {
                        case 'logo':
                            $message .= '<li>' . ADMIN_SALESSETTING_DELETE_LOGO_SUCCESS . '</li>';
                            break;
                        case 'header':
                            $message .= '<li>' . ADMIN_SALESSETTING_DELETE_HEADER_SUCCESS . '</li>';
                            break;
                        case 'seatLogo':
                            $message .= '<li>' . ADMIN_SALESSETTING_DELETE_SEATLOGO_SUCCESS . '</li>';
                            break;
                    }
                }
            }
        }
        return $this->action_sales_setting($message);
    }

    function validate_image($target, $move_path)
    {
        $message = $path = $type = null;
        if (!$_FILES[$target]['tmp_name']) {
            $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_ERROR . '</li>';
        } else {
            $userfile = $_FILES[$target]['tmp_name'];
            $userfile_name = $_FILES[$target]['name'];
            $userfile_size = $_FILES[$target]['size'];
            $userfile_type = $_FILES[$target]['type'];
            $userfile_error = $_FILES[$target]['error'];
            if ($userfile_error > 0) {
                switch ($userfile_error) {
                    case 1 :
                        $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_SIZE_ERROR . '</li>';
                        break;
                    case 2 :
                        $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_SIZE_ERROR . '</li>';
                        break;
                    case 3 :
                        $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_SIZE_ERROR . '</li>';
                        break;
                    case 4 :
                        $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_SIZE_ERROR . '</li>';
                        break;
                }
            }
            if ($userfile_type != 'image/gif' && $userfile_type != 'image/pjpeg' && $userfile_type != 'image/jpeg' && $userfile_type != 'image/jpg' && $userfile_type != 'image/png' && $userfile_type != 'image/x-png') {
                $message .= '<li>' . ADMIN_SALESSETTING_UPOAD_FILE_TYPE_ERROR . '</li>';
            } else {
                list($width, $height, $type, $attr) = getimagesize($userfile);
                if ($userfile_size > 2000000) {
                    $message .= '<li>' . PROFILE_ERROR_FILE_SIZE . '</li>';
                }
                switch ($target) {
                    case 'header' :
                        if ($width > $this->config->get('SALES_CUSTOM_IMAGE', 'header_width_max')) {
                            $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_WIDTH . '</li>';
                        } 
                        if ($height > $this->config->get('SALES_CUSTOM_IMAGE', 'header_height_max')) {
                            $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_HEIGHT . '</li>';
                        }
                        break;
                    case 'logo' :
                        if ($width > $this->config->get('SALES_CUSTOM_IMAGE', 'logo_width_max')) {
                            $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_WIDTH . '</li>';
                        } 
                        if ($height > $this->config->get('SALES_CUSTOM_IMAGE', 'logo_height_max')) {
                            $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_HEIGHT . '</li>';
                        }
                        break;
                    case 'seatLogo' :
                        if ($width > $this->config->get('SALES_CUSTOM_IMAGE', 'seatLogo_width_max')) {
                            $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_WIDTH . '</li>';
                        } 
                        if ($height > $this->config->get('SALES_CUSTOM_IMAGE', 'seatLogo_height_max')) {
                            $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_HEIGHT . '</li>';
                        }
                        break;
                }
                if ($userfile_type == 'image/gif') {
                    $img_in = imagecreatefromgif($userfile);
                } else if ($userfile_type == 'image/png' || $userfile_type == 'image/x-png') {
                    $img_in = imagecreatefrompng($userfile);
                } else {
                    $img_in = imagecreatefromjpeg($userfile);
                }
                ImageJPEG($img_in, $move_path);
                // ファイルのアップロード先
                if (is_uploaded_file($userfile)) {
                    if (!move_uploaded_file($userfile, $move_path)) {
                        $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_MOVE_ERROR . '</li>';
                    }
                } else {
                    $message .= '<li>' . ADMIN_SALESSETTING_UPLOAD_FILE_MOVE_ERROR . '</li>';
                }
            }
        }
        return array(
            "message" => $message,
            "path" => $move_path,
            "type" => $type,
            "width" => $width,
            "height" => $height,
            "target" => $target
        );
    }

    function upload_image($image_info, $move_path)
    {
        $userInfo = $this->session->get("user_info");
        $move_folder = N2MY_APP_DIR . 'salesSetting/' . $userInfo['user_id'] . '/' . $image_info['target'];
        if (!is_dir($move_folder)) {
            mkdir($move_folder, 0777, true);
        }
        $up_dir = $move_folder . "/";
        if (!is_dir($up_dir)) {
            if (!mkdir($up_dir)) {
                $this->logger->error(__FUNCTION__ . "#error_mkdir", __FILE__, __LINE__, $up_dir);
            } else {
                chmod($up_dir, 0777);
            }
        }
        $images = glob($up_dir . '*', GLOB_MARK);
        if ($images) {
            foreach ($images as $image) {
                unlink($image);
            }
            $this->salesSettingTable->update(
                array(
                    'status' => 0,
                    'update_datetime' => date("Y-m-d H:i:s"),
                ),
                'status = 1 AND user_key = ' . $userInfo['user_key'] . ' AND type = "' . $image_info['target'] . '"'
            );
        }
        $imageID = $this->create_id();
        $imageExtension = '';
        $up_file = $up_dir . $imageID;
        switch ($image_info['type']) {
            case '1' :
                $imageExtension = 'gif';
                break;
            case '2' :
                $imageExtension = 'jpg';
                break;
            case '3' :
                $imageExtension = 'png';
                break;
        }
        $up_file = $up_file . '.' . $imageExtension;
        if (rename($move_path, $up_file)) {
            $imageData = array(
                'image_id' => $imageID,
                'user_key' => $userInfo['user_key'],
                'type' => $image_info['target'],
                'extension' => $imageExtension,
                'height' => $image_info['height'],
                'width' => $image_info['width'],
                'status' => 1,
                'create_datetime' => date('Y-m-d H:i:s')
            );
            if ($image_info['target'] == 'header') {
                $resizeImage = $this->resizeImage($move_path, $up_file, $image_info['target']);
                if ($resizeImage) {
                    $imageData['width'] = $resizeImage['width'];
                    $imageData['height'] = $resizeImage['height'];
                }
            }
            $this->salesSettingTable->add($imageData);
            $image_info['path'] = $up_file;
            return $image_info;
        }
        return false;
    }

    function get_image_info($target)
    {
        $userInfo = $this->session->get('user_info');
        $image = $this->salesSettingTable->getRow(
            sprintf("user_key = %s AND type = '%s' AND status = 1", $userInfo['user_key'], $target)
        );
        if (!$image) {
            return false;
        }
        $imageBasePath = 'salesSetting/' . $userInfo['user_id'] . '/' . $target . '/' . $image['image_id'] . '.' . $image['extension'];
        $imagePath = N2MY_APP_DIR . $imageBasePath;
        list($width, $height, $type) = getimagesize($imagePath);
        $imageInfo = array(
            'width' => $width,
            'height' => $height,
            'type' => $type,
            'image_id' => $image['image_id']
        );
        return $imageInfo;
    }

    function resizeImage($imageFrom, $imageTo, $type)
    {
        list($width, $height) = getimagesize($imageTo);
        $recommendingHeight = $this->config->get("SALES_CUSTOM_IMAGE", $type . '_height_recommend');
        $recommendingWidth = $this->config->get("SALES_CUSTOM_IMAGE", $type . '_width_recommend');
        if (!$width || !$height || !$recommendingWidth || !$recommendingHeight || (($width <= $recommendingWidth) && ($height <= $recommendingHeight))) {
            return false;
        }
        if (rename($imageTo, $imageFrom)) {
            $heightRatio = $recommendingHeight / $height;
            $widthRatio = $recommendingWidth / $width;
            if ($heightRatio <= $widthRatio) {
                $newHeight = $height;
                $newWidth = ceil($heightRatio * $width);
            } else {
                $newWidth = $width;
                $newHeight = ceil($widthRatio * $height);
            }
            require_once('lib/EZLib/EZUtil/EZImage.class.php');
            $EZImage = new EZImage(N2MY_IMGMGC_DIR);
            $image = $EZImage->convert($imageFrom, $imageTo, $newWidth, $newHeight);
            if (!$image) {
                return false;
            }
            return array(
                "width" => $image[0]['x'],
                "height" => $image[0]['y'],
                "type" => $image[0]['type'],
            );
        }
        return false;
    }
}

$main = new AppAdminSalesSetting();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
