<?php
require_once('classes/AppFrame.class.php');

require_once('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");

class AppAdminAsk extends AppFrame
{
    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    function default_view(){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $this->action_ask();

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }


    function action_ask($message = ""){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        // エラー表示
        if($message){
            $this->template->assign('message', $message);
        }
        // 入力データ再表示
        if($ask_info = $this->session->get('admin_ask_info')){
            foreach($ask_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        // 入力画面表示
        $this->display('admin/ask/index.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    function action_ask_confirm(){
        $this->set_submit_key($this->_name_space);
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $request = $this->request->getAll();
        $this->session->set('admin_ask_info', $request);

        // 入力チェック
        $message = "";
        if(!$request['subject']){
            $message .= "<li>".INQUIRY_ERROR_SUBJECT . "</li>";
        }
        if(!$request['body']){
            $message .= "<li>".INQUIRY_ERROR_BODY . "</li>";
        }

        if($message != ""){
            //　入力ミス
            $this->action_ask($message);
        }else{
            // 入力データ表示
            foreach($request as $key => $value){
                $this->template->assign($key, $value);
            }
            // 確認画面表示
            $this->display('admin/ask/confirm.t.html');
        }

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    function action_ask_complete(){
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_ask();
        }
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        if($ask_info = $this->session->get('admin_ask_info')){

            $user_info = $this->session->get('user_info');
            $login_id = $user_info['user_id'];
            $member_info = $this->session->get('member_info');
            if($member_info){
                $login_id = $member_info['member_id'];
            }
            
            $this->template->assign('login_id', $login_id);
            foreach($user_info as $key => $value){
                $this->template->assign($key, $value);
            }
            foreach($ask_info as $key => $value){
                $this->template->assign($key, $value);
            }
            require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
            $this->template->assign('n2my_address', $_SERVER['HTTP_HOST']);
            // vcube
            $mail = new EZSmtp(null, "", "UTF-8");
            $body = $this->fetch("0");
            $mail->setTo(ADMIN_INQUIRY_TO);
            $mail->setFrom(ADMIN_INQUIRY_FROM);
            $mail->setReturnPath(NOREPLY_ADDRESS);
            $mail->setSubject(INQUIRY_SUBJECT);
            if($this->session->get('service_mode') == "sales"){
                $mail->setSubject(INQUIRY_SUBJECT_SALES);
            }
            $mail->setBody($body);
            $mail->send();

            // user
            if ($user_info["user_staff_email"]) {
                $mail = new EZSmtp(null, $this->_lang, "UTF-8");
                $frame["lang"] = $this->_lang;
                $this->template->assign("__frame",$frame);
                $body = $this->fetch("1");
                $mail->setSubject(ASK_SUBJECT_FOR_USER);
                $mail->setTo($user_info["user_staff_email"]);
                $mail->setFrom(USER_ASK_FROM);
                $mail->setReturnPath(NOREPLY_ADDRESS);
                $mail->setBody($body);
                $mail->send();
            }
            $this->session->remove('admin_ask_info');
        }
        // 完了画面表示
        $this->display('admin/ask/done.t.html');
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
        override
        $flag = 1, userに送信
        $flag = 0, adminに送信
    **
    */
    function fetch($flag) {
        if($flag){
            $custom  = $this->config->get('SMARTY_DIR','custom');
            $lang_cd = $this->_lang;
            require_once ("lib/EZLib/EZHTML/EZTemplate.class.php");
            $template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
            if($this->session->get('service_mode') == "sales"){
                $template_file = "custom/sales/common/multi_lang/ask/ask_admin_vcube.t.txt";
            }else{
                $template_file = "common/mail_template/meeting/ask/ask_admin_vcube.t.txt";
            }
            if($custom){
                $custom_template_file = "custom/" . $custom . "/common/multi_lang/ask/ask_admin_vcube.t.txt";
                if (file_exists($template->template_dir.$custom_template_file)){
                    $template_file = $custom_template_file;
                }else{
                    $custom_template_file = "custom/" . $custom. "/" . $lang_cd."/mail/ask/ask_admin_vcube.t.txt";
                    if (file_exists($template->template_dir.$custom_template_file)) {
                        $template_file = $custom_template_file;
                    }         
                }
            }
        }else{
            $template_file = "common/mail/inquiry.t.txt";
        }
        return parent::fetch($template_file);
    }
}

$main =& new AppAdminAsk();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
