<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once ('classes/AppFrame.class.php');
require_once ('classes/mgm/dbi/user.dbi.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/member.dbi.php');
require_once ('classes/dbi/member_group.dbi.php');
require_once ('classes/dbi/member_status.dbi.php');
require_once ('classes/dbi/user.dbi.php');
require_once ('config/config.inc.php');
require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');

class AppMember extends AppFrame
{
    var $logger = null;
    var $_name_space = null;
    var $memberTable = null;
    var $groupTable = null;
    var $statusTable = null;
    var $account_dsn = null;
    var $max_address_cnt = null;

    function init() {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->memberTable = new MemberTable($this->get_dsn());
        $this->groupTable = new MemberGroupTable($this->get_dsn());
        $this->_name_space = md5(__FILE__);
        $this->max_address_cnt  = 500;
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }
    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace("test", __FILE__, __LINE__, __FUNCTION__);
        $this->action_show_top();
    }

    function action_show_top()
    {
        $this->display('admin/member/index.t.html');
    }

    function action_admin_member($message = ""){
        $this->session->remove("conditions");
        $this->render_admin_member();
    }

    function action_search_member() {
        $this->setCond();
        $this->render_admin_member();
    }

    function action_pager() {
        $this->render_admin_member();
    }

    function render_admin_member($message = ""){

        if($message){
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        // グループ一覧
        $where = "user_key = ".addslashes($user_key);
        $group_list = $this->groupTable->getRowsAssoc($where);
        $this->template->assign('group_list', $group_list);
        //user_keyからメンバーを取得
        $conditions = $this->getCond();
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$conditions);
        $sort_key  = ($conditions["sort_key"]) ? $conditions["sort_key"] : "member_group";
        $sort_type = ($conditions["sort_type"]) ? $conditions["sort_type"] : "asc";
        $sort_key_rules = array("member_name","member_id","member_group");
        if (!EZValidator::valid_allow($sort_key, $sort_key_rules)) {
            $sort_key = "member_name";
        }
        $sort_type_rules = array("asc","desc");
        if (!EZValidator::valid_allow($sort_type, $sort_type_rules)) {
            $sort_type = "desc";
        }
        $limit = $this->request->get("page_cnt");
        if (!$limit) {
            $limit = 10;
        }
        $page      = $this->request->get("page", 1);
        $offset    = ($page - 1) * $limit;
        // 条件
        $where = "user_key = ".addslashes($user_key).
            " AND member_status > -1";
        if ($conditions["member_group"]) {
            $where .= " AND member_group = ".addslashes($conditions["member_group"]);
        }
        if ($conditions["member_name"]) {
            $where .= " AND (member_name like '%".addslashes($conditions["member_name"])."%'";
            if($this->session->get("role") == "10"){
                // Oneアカウント時、vcube_one_member_idを検索対象とする
                $where .= " OR vcube_one_member_id like '%".addslashes($conditions["member_name"])."%')";
            }else{
                $where .= " OR member_id like '%".addslashes($conditions["member_name"])."%')";
            }
        }
        if($this->session->get("service_mode") == "sales") {
        	$where .= " AND outbound_id IS NOT NULL";
            if($this->session->get("role") == "10") {
                // S&S管理者の場合、自分自身を非表示とする
                $member_info = $this->session->get("member_info");
                $where .= " AND vcube_one_member_id <> '" . $member_info["vcube_one_member_id"] . "'";
            }
        } else {
        	$where .= " AND outbound_id IS NULL";
        }
        $member_list = $this->memberTable->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $count = $this->memberTable->numRows($where);
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        $member_info = array();
        if ($user_info['account_model'] == 'member') {
            if ($conditions['month'] == 'prev') {
                // 前月
                $condition = array(
                    'actual_start_datetime' => array(
                        'min' => date('Y-m-01 00:00:00', strtotime("-1 Month")),
                        'max' => date('Y-m-01 00:00:00'),
                    )
                );
            } else {
                // 当月
                $condition = array(
                    'actual_start_datetime' => array(
                        'min' => date('Y-m-01 00:00:00'),
                        'max' => date('Y-m-01 00:00:00', strtotime("+1 Month")),
                    )
                );
            }
            require_once("classes/dbi/member_usage_details.dbi.php");
            $objMemberUseLog = new DBI_MemberUsageDetails($this->get_dsn());
            $use_count = $objMemberUseLog->getSummaryUseCount($user_key, $condition);
            $this->template->assign('total_use_count', $use_count);
        }
        foreach( $member_list as $member ){
            if ($user_info['account_model'] == 'member') {
                $condition['member_key'] = $member['member_key'];
                $use_count = $objMemberUseLog->getSummaryUseCount($user_key, $condition);
            }
            $member_info[] = array(
                            "auth_user_key" => $obj_MgmUserTable->getOne( sprintf( "user_id='%s'", mysql_real_escape_string($member["member_id"])), "user_key" ),
                            "group"         => $this->_getMemberGroupName( $member['member_group'] ),
                            "status"        => $this->_getMemberStatusName( $member['member_status'] ),
                            "key"           => $member['member_key'],
                            "id"            => $member['member_id'],
                            "name"          => $member['member_name'],
                            "type"          => $member['member_type'],
                            "one_member_id" => $member['vcube_one_member_id'],
                            "use_count"     => $use_count
                            );
        }
        if($user_info["account_model"] == "member" || $user_info["account_model"] == "centre" || ($this->session->get("service_mode")=="sales" && !$this->check_one_account()))
        	$add_flg = $user_info["max_member_count"] <= $this->_getMemberCount($user_info['user_key']) ? false : true ;
        else
        	$add_flg = true;
        $this->template->assign("add_flg", $add_flg );
        $this->template->assign('info', $member_info);
        $this->template->assign('vcubeid_add_use_flg', $user_info["vcubeid_add_use_flg"]);
        if($this->session->get("service_mode") == "sales") {
            require_once( "classes/dbi/user_plan.dbi.php" );
            $objUserPlan = new UserPlanTable( $this->get_dsn() );
            $sales_plan = $objUserPlan->getOne("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1 AND sales_plan_flg = 1", "service_key");
            $this->template->assign("has_sales_plan", $sales_plan?$sales_plan:0);
        }
        // ページング
        $this->template->assign('conditions', $conditions);
        $pager = $this->setPager($limit, $page, $count);
        $this->template->assign('pager', $pager);
        $this->display('admin/member/members/index.t.html');
    }

    function setCond() {
        $conditions  = $this->request->get("conditions");
        if ($conditions) {
            $this->session->set("conditions", $conditions);
        }
    }

    function getCond() {
        $condition = $this->session->get("conditions");
        return $condition;
    }

    function action_change_vcubeid_flg() {
        $user_info = $this->session->get("user_info");
        $vcubeid_use_flg = $this->request->get("vcubeid_add_use_flg");
        $user_info["vcubeid_add_use_flg"] = $vcubeid_use_flg;
        $objUser = new UserTable($this->get_dsn());
        $where = "user_key = ". $user_info["user_key"];
        $data = array("vcubeid_add_use_flg" => $vcubeid_use_flg );
        $objUser->update($data, $where);
        $this->session->set("user_info", $user_info);
        $this->action_admin_member();
    }

    function action_add_member($message = "")
    {
        $member_info = ( 1 == $this->request->get( "action_add_member") ) ? "" : $this->session->get('new_member_info');
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        $obj_MemberStatus = new MemberStatusTable( $this->get_auth_dsn() );
        $group_list = $this->groupTable->getRowsAssoc( sprintf( "user_key='%s'", $user_key ) );
        foreach( $group_list as $group_info ){
            $member_group[] = array(
                                "key" => $group_info['member_group_key'],
                                "name" => $group_info['member_group_name']
                                );
        }

        $member_status_list = $obj_MemberStatus->getRowsAssoc();
        foreach( $member_status_list as $status_info) {
                $member_status[] = array(
                    "key"     => $status_info['member_status_key'],
                    "name"     => $status_info['member_status_name']
                );
        }

        //エラー時にユーザインプットの再表示
        if( $member_info ){
            foreach($member_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        if($message){
            $this->template->assign('message', $message);
        }

        $this->template->assign('user_info', $user_info);
        $this->template->assign('member_group', $member_group);
        $this->template->assign('member_status', $member_status);
        $this->display('admin/member/members/add/index.t.html');
    }

    function action_add_member_confirm()
    {
        $this->set_submit_key($this->_name_space);
        // 入力値を保持
        $request = $this->request->getAll();
        $this->session->set('new_member_info', $request);
        $user_info = $this->session->get( "user_info");

        $member_list = $this->memberTable->getRowsAssoc( sprintf( "user_key ='%s' and member_status > -1", $user_info['user_key'] ) );

        $message = null;
        $message = $this->check_member_info($request);

        if( $message ){
            $this->action_add_member($message);
        }elseif ($user_info["max_member_count"] != 0 && $this->_getMemberCount($user_info["user_key"]) >= $user_info["max_member_count"]) {
            //契約上限値越えた
            $message .= '<li>'.MEMBER_ERROR_OVER_COUNT . '</li>';
            $this->action_add_member($message);
        } else {
            foreach($request as $key => $value){
                if($key == "member_group_key"){
                    $this->template->assign('member_group_name', $this->_getMemberGroupName($value));
                }elseif($key == "member_status_key"){
                    $this->template->assign('member_status_name', $this->_getMemberStatusName($value));
                }elseif($key == "member_pass"){
                    $length = mb_strlen($value);
                    $value = "";
                    for($i=0;$i<$length;$i++){
                        $value .= "*";
                    }
                }else if($key == "timezone"){
                    if( 100 == $value ){
                        $timezone = MEMBER_UNDEFINED_TIMEZONE;
                    } else {
                        $timezoneList = $this->get_timezone_list();
                        foreach( $timezoneList as $k => $zone ){
                            if( $zone["key"] == $value ){
                                $timezone = $zone["value"];
                                break 1;
                            }
                        }
                    }
                    $this->template->assign( 'timezoneInfo', $timezone );
                }
                $this->template->assign($key, $value);
            }
            $this->display('admin/member/members/add/confirm.t.html');
        }
    }
    public function action_add_member_csv() {
        $this->template->assign('max_address_cnt',  $this->max_address_cnt);
        $this->display('admin/member/members/csv_index.t.html');
    }

    function action_format_download() {
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");

        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="sample_member.csv"');

        // ヘッダ
        $header = array("member_id", "member_pass", "name", "name_kana", "email", "group_name", "timezone", "lang(ja.en.zh-cn.zh-tw.fr.th.in.ko)", "storage_flg","flag","error");
        $sample = array("vcube@example.jp","vcube201307","ＳＡＭＰＬＥ","sample","vcube@example.jp","SPGroup","9","en","Y","A","This is a sample data. Please delete.");
        $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->setHeader($header);
        $csv->write($header);
        $csv->write($sample);
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
    }

    public function action_import_member_csv() {
        $request["upload_file"] = $_FILES["import_file"];

        $data = pathinfo($request["upload_file"]["name"]);
        //CSVファイルチェック
        if (empty($data['filename'])) {
            $message .= CSV_NO_FILE_ERROR;
            return $this->action_import_complete($message);
        } elseif($data['extension'] != "csv") {
            $message .= CSV_FILE_FORMAT_ERROR ;
            return $this->action_import_complete($message);
        }
        $filename = $this->get_work_dir()."/".tmpfile();
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open($filename, "r");
        //取り込んだファイルのヘッダチェック
        $head_ex = "member_id,member_pass,name,name_kana,email,group_name,timezone,lang(ja.en.zh-cn.zh-tw.fr.th.in.ko),storage_flg,flag,error";
        $h_ar = implode(",", $csv->getHeader());
        if($head_ex !== $h_ar) {
            $message .= CSV_FILE_FORMAT_ERROR;
            return $this->action_import_complete($message);
        }
        $rows = array();
        $count = 0;
        while($row = $csv->getNext(true)) {
            $row["lang"] = $row["lang(ja.en.zh-cn.zh-tw.fr.th.in.ko)"];
            unset($row["lang(ja.en.zh-cn.zh-tw.fr.th.in.ko)"]);
            if($row["timezone"] == "")
            	$row["timezone"] = "100";
            $rows[] = $row;
            $count++;
        }
        $csv->close();
        unlink($filename);
        //記録メンバー数チェック
        if(empty($rows)) {
            $message .= CSV_FILE_FORMAT_ERROR;
            return $this->action_import_complete($message);
        } elseif($count > $this->max_address_cnt) {
            $message .= CSV_LIMIT_ERROR;
            return $this->action_import_complete($message);
        }

        //追加・編集・削除処理
        $user_info = $this->session->get( "user_info" );
        $result_array = array();
        $error_flg = 0;

        foreach($rows as $member_row) {

            switch($member_row["flag"]) {
                case "X":
                    break;
                case "A":
                    if(!$this->_isExist($member_row["member_id"])){
                        //追加こちから
                        if ($error_message = $this->_isMemberInfo($member_row)){
                            //入力情報をバリデーションエラー
                            $error_msg = "";
                            foreach($error_message as $msg) {
                                $error_msg .= $msg." ";
                            }
                            $member_row[error] = $error_msg;
                            $error_flg = 1;
                        } elseif ($user_info["max_member_count"] != 0 && $this->_getMemberCount($user_info["user_key"]) >= $user_info["max_member_count"]) {
                            //契約上限値越えた
                            $member_row[error] = MEMBER_ERROR_OVER_COUNT;
                            $error_flg = 1;
                        } else {
                            //追加処理
                            if($member_row["group_name"]) {
                                $group_key = $this->_isGroupExist($member_row["group_name"],$user_info["user_key"],true);
                                $member_row["member_group"] = $group_key;
                            }
                            $add_data = array(
                                "user_key"           => $user_info["user_key"],
                                "member_id"          => $member_row['member_id'],
                                "member_pass"        => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_row['member_pass']),
                                "member_email"       => $member_row['email'],
                                "member_name"        => $member_row['name'],
                                "member_name_kana"   => $member_row['name_kana'],
                                "member_status"      => "0",
                                "member_type"        => (($member_row['member_type']) ? $member_row['member_type'] : ""),
                                "member_group"       => $member_row['member_group']?$member_row['member_group']:0,
                                "timezone"           => $member_row['timezone'],
                                "lang"               => $member_row['lang'],
                                "use_shared_storage" => $member_row['storage_flg'] == "Y"? 1:0,
                                "use_ss_watcher"     => $member_row['use_ss_watcher']
                            );
                            
                            $this->logger2->info($add_data);
                            $ret = $this->memberTable->add( $add_data );
                            if (DB::isError($ret)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                            } else {
                                $this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$temp);
                            }
                            //認証サーバーへ追加
                            $obj_MgmUserTable = new MgmUserTable( N2MY_MDB_DSN );
                            $session = $this->session->get( "server_info" );
                            $auth_data = array(
                                "user_id"     => $member_row['member_id'],
                                "server_key"    => $session["server_key"]
                            );
                            $result = $obj_MgmUserTable->add( $auth_data );
                            if (DB::isError($result)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
                            } else {
                                $this->logger->info(__FUNCTION__."#add user_id successful!",__FILE__,__LINE__,$auth_data);
                            }
                            //メンバー課金対応
                            //部屋追加
                            if( $user_info["account_model"] == "member" || $user_info["account_model"] == "centre" ){
                            	require_once( "classes/dbi/room.dbi.php" );
                            	require_once( "classes/dbi/user_plan.dbi.php" );
                            	require_once( "classes/dbi/room_plan.dbi.php" );
                            	$obj_Room = new RoomTable( $this->get_dsn() );
                            	$objUserPlan = new UserPlanTable( $this->get_dsn() );
                            	$objRoomPlanTable = new RoomPlanTable( $this->get_dsn() );
                            	if ($now_plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1")) {
                            		$service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
                            		$service_info = $service_db->getRow("service_key = '".addslashes($now_plan_info['service_key'])."' AND service_status = 1");
                            		$this->logger2->debug(array($now_plan_info, $service_info));
                            	}
                            	$where = sprintf( "user_key=%d", $user_info["user_key"] );
                            	$count = $obj_Room->numRows($where);
                            	$room_key = $user_info["user_id"]."-".($count + 1)."-".substr( md5( uniqid( time(), true ) ), 0, 4 );
                            	$room_sort = $count + 1;
                            	//部屋登録
                            	$room_data = array (
                            			"room_key"              => $room_key,
                            			"max_seat"              => ($service_info["max_seat"]) ? $service_info["max_seat"] : 10,
                            			"max_audience_seat"     => ($service_info["max_audience_seat"]) ? $service_info["max_audience_seat"] : 0,
                            			"max_whiteboard_seat"   => ($service_info["max_whiteboard_seat"]) ? $service_info["max_whiteboard_seat"] : 0,
                            			"max_room_bandwidth"    => ($service_info["max_room_bandwidth"]) ? $service_info["max_room_bandwidth"] : "2048",
                            			"max_user_bandwidth"    => ($service_info["max_user_bandwidth"]) ? $service_info["max_user_bandwidth"] : "256",
                            			"min_user_bandwidth"    => ($service_info["min_user_bandwidth"]) ? $service_info["min_user_bandwidth"] : "30",
                            			"user_key"              => $user_info["user_key"],
                            			"room_name"             => $member_row['name'],
                            			"room_sort"             => $room_sort,
                            			"room_status"           => "1",
                            			"room_registtime"       => date("Y-m-d H:i:s"),
                            	);
                            	$this->logger2->info($room_data);
                            	$add_room = $obj_Room->add($room_data);
                            	if (DB::isError($add_room)) {
                            		$this->logger2->info( $add_room->getUserInfo());
                            	}
                            	//ユーザープランからroom_planへコピー
                            	$planList = $objUserPlan->getRowsAssoc(sprintf("user_key='%s'", $user_info["user_key"]));
                            	for( $i = 0; $i < count( $planList ); $i++ ){
                            		$data = array(
                            				"room_key"=>$room_key,
                            				"service_key"=>$planList[$i]["service_key"],
                            				"discount_rate"=>$planList[$i]["discount_rate"],
                            				"contract_month_number"=>$planList[$i]["contract_month_number"],
                            				"room_plan_yearly"=>$planList[$i]["user_plan_yearly"],
                            				"room_plan_yearly_starttime"=>$planList[$i]["user_plan_yearly_starttime"],
                            				"room_plan_starttime"=>$planList[$i]["user_plan_starttime"],
                            				"room_plan_endtime"=>$planList[$i]["user_plan_endtime"],
                            				"room_plan_status"=>$planList[$i]["user_plan_status"],
                            				"room_plan_registtime"=>$planList[$i]["user_plan_registtime"],
                            				"room_plan_updatetime"=>$planList[$i]["user_plan_updatetime"]
                            		);
                            		$addPlan = $objRoomPlanTable->add( $data );
                            		if (DB::isError( $addPlan ) ) {
                            			$this->logger2->info( $addPlan->getUserInfo() );
                            		}
                            	}
                            	// userのプランオプション(user_service_option)からmemberの部屋のオプションを追加する(ordered_service_option)
                            	$this->objMeetingDB = new N2MY_DB( $this->get_dsn(), "user_service_option" );
                            	// $condition = sprintf( "user_key='%s' AND user_service_option_starttime<='%s' AND user_service_option_status=1", $user_info["user_key"], date( "Y-m-d" ) );
                            	$condition = sprintf( "user_key='%s' AND service_option_key !=14", $user_info["user_key"] );
                            	$optionList = $this->objMeetingDB->getRowsAssoc( $condition );
                            	for( $i = 0; $i < count( $optionList ); $i++ ){
                            		$data = array(
                            				"room_key" => $room_key,
                            				"user_service_option_key" => $optionList[$i]["user_service_option_key"],
                            				"service_option_key" => $optionList[$i]["service_option_key"],
                            				"ordered_service_option_status" => $optionList[$i]["user_service_option_status"],
                            				"ordered_service_option_starttime" => $optionList[$i]["user_service_option_starttime"],
                            				"ordered_service_option_registtime" => $optionList[$i]["user_service_option_registtime"],
                            				"ordered_service_option_deletetime" => $optionList[$i]["user_service_option_deletetime"]
                           			);
                            		require_once("classes/dbi/ordered_service_option.dbi.php");
                            		$ordered_service_option = new OrderedServiceOptionTable( $this->get_dsn() );
                           			$ordered_service_option->add( $data );
                            	}
                            	//部屋リレーション追加
                            	$where = "user_key = ".$user_info["user_key"].
                            	" AND member_id='".mysql_real_escape_string($member_row['member_id'])."'";
                            	$info = $this->memberTable->getRow( $where, "member_key" );
                            	require_once("classes/dbi/member_room_relation.dbi.php");
                            	$objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
                            	$data = array("member_key" => $info["member_key"],
                            			"room_key"   => $room_key,
                            			"create_datetime" => date("Y-m-d H:i:s"));
                            	$this->logger2->debug($data);
                            	$objRoomRelation->add($data);
                            	if (DB::isError($ret)) {
                            		$this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                            	}
                            	$this->logger2->debug("容量追加");
                            	//部屋keyメンバーrowに入れる
                            	$update_data = array(
                            			"room_key" => $room_key,
                            	);
                            	$ret = $this->memberTable->update( $update_data, sprintf("member_key='%s'", $info["member_key"]));
                            	if (DB::isError($ret)) {
                            		$this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                            	} else {
                            		$this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$temp);
                            	}
                            	//容量追加
                            	$obj_UserTable = new UserTable( $this->get_dsn() );
                           		if ($now_plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1")) {
                           			$service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
                           			$service_info = $service_db->getRow("service_key = '".addslashes($now_plan_info['service_key'])."' AND service_status = 1");
                           			$this->logger2->info(array($now_plan_info, $service_info));
                           			$update_data = array(
                            		    "max_storage_size" => intval($user_info["max_storage_size"]) + intval($service_info["member_storage_size"])
                            		);
                            		$update_where = "user_key='".addslashes($user_info["user_key"])."'";
                            	    //容量更新あるよって、user_infoも最新データに変更する
                            	    $obj_UserTable->update($update_data, $update_where);
                            	}
                            	$user_info = $obj_UserTable->getRow("user_key='".addslashes($user_info["user_key"])."'");
                            	$this->session->set("user_info", $user_info);
                            }
                        }
                    } else {
                        //編集こちから
                        $member_key = $this->_isMemberExist($member_row["member_id"],$user_info["user_key"]);
                        if(!$member_key) {
                            //他ユーザーのメンバーがこのメンバーIDを使っています
                            $member_row[error] = MEMBER_ERROR_ID_EXIST;
                            $error_flg = 1;
                        } elseif ($error_message = $this->_isMemberInfo($member_row)) {
                            //入力情報をバリデーションエラー
                            $error_msg = "";
                            foreach($error_message as $msg) {
                                $error_msg .= $msg." ";
                            }
                            $member_row[error] = $error_msg;
                            $error_flg = 1;
                        } else {
                            //更新処理
                            if($member_row["group_name"]) {
                                $group_key = $this->_isGroupExist($member_row["group_name"],$user_info["user_key"],true);
                                $member_row["member_group"] = $group_key;
                            }
                            $edit_data = array(
                                "member_pass"        => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_row['member_pass']),
                                "member_email"       => $member_row['email'],
                                "member_name"        => $member_row['name'],
                                "member_name_kana"   => $member_row['name_kana'],
                                "member_status"      => "0",
                                "member_type"        => (($member_row['member_type']) ? $member_row['member_type'] : ""),
                                "member_group"       => $member_row['member_group'],
                                "timezone"           => $member_row['timezone'],
                                "lang"               => $member_row['lang'],
                                "use_shared_storage" => $member_row['storage_flg'] == "Y"? 1:0,
                                "use_ss_watcher"     => $member_row['use_ss_watcher']
                            );
                            $where =  sprintf( "member_key='%s'", $member_key );
                            $this->logger2->info($edit_data);
                            $result = $this->memberTable->update($edit_data, $where);
                            if (DB::isError($result)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
                            } else {
                                $this->logger->info(__FUNCTION__."#edit member successful!",__FILE__,__LINE__,$auth_data);
                            }
                        }
                    }
                    break;
                case "D":
                    //削除こちから
                    $member_key = $this->_isMemberExist($member_row["member_id"],$user_info["user_key"]);
                    if(!$member_key) {
                        //メンバーIDが使っていません
                        $member_row[error] = MEMBER_ERROR_DELETE;
                        $error_flg = 1;
                    } else {
                        //削除処理
                    	require_once 'classes/N2MY_Account.class.php';
                    	$obj_n2my_account = new N2MY_Account( parent::get_dsn() );
                    	$obj_n2my_account->deleteMember( $member_key );
                    	$obj_user = new UserTable( parent::get_dsn() );
                    	$user_info = $obj_user->getRow( sprintf( 'user_key=%d', $user_info['user_key'] ) );
                    	$this->session->set('user_info', $user_info);
                    }
                    break;
                default:
                    $member_row[error] = MEMBER_ERROR_CSV_FLAG;
                    $error_row[] = $member_row;
                    $error_flg = 1;
                    break;
            }
            $result_array[] = $member_row;
        }
        $this->logger2->info($result_array);
        if($error_flg) {
            $message = "一部のデータの登録ができませんでした。";
        }
        return $this->action_import_complete($message, $result_array,$error_flg);
    }

    public function action_import_complete($message="",$result_array = "",$error_flg = 0) {
        if ($message) {
            $this->template->assign('message', $message);
        }
        if ($error_flg) {
            $this->session->set("result_array",$result_array);
            $this->template->assign('is_error', true);
        }
        $this->display('admin/member/members/csv_done.t.html');
    }

    public function action_export_member_csv() {
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];

        // CSV出力
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="member_list_'.date("Ymd").'.csv"');
        $header = array(
            "member_id"   => "member_id",
            "member_pass" => "member_pass",
            "name"        => "name",
            "name_kana"   => "name_kana",
            "email"       => "email",
            "group_name"  => "group_name",
            "timezone"    => "timezone",
            "lang"        => "lang(ja.en.zh-cn.zh-tw.fr.th.in.ko)",
            "storage_flg" => "storage_flg",
            "flag"        => "flag",
        	"error"       => "error",
        );
        $csv->setHeader($header);
        $csv->write($header);
        $where =  sprintf( "user_key='%s' and member_status ='0' AND outbound_id IS NULL", $user_key );
        $_list = $this->memberTable->getRowsAssoc($where);
        foreach($_list as $_key => $row) {
            $line = array();
            $line["member_id"]      = $row["member_id"];
            $line["member_pass"]    = "";
            $line["name"]           = $row["member_name"];
            $line["name_kana"]      = $row["member_name_kana"];
            $line["email"]          = $row["member_email"];
            $line["group_name"]     = $this->_getMemberGroupName($row["member_group"]);
            $line["timezone"]       = $row["timezone"];
            $line["lang"]           = $row["lang"];
            $line["storage_flg"]    = $row["use_shared_storage"]?"Y":"N";
            $line["use_ss_watcher"] = $row["use_ss_watcher"];
            $line["flag"]           = "X";
            $line["error"]           = "";

            $this->logger2->info($line);
            $csv->write($line);
        }

        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);

        print $contents;
        unlink($tmpfile);
    }

    public function action_err_csv_download() {
        $rows = $this->session->get("result_array");
        if(!empty($rows)) {
            // エラーの出力
            $output_encoding = $this->get_output_encoding();
            $csv = new EZCsv(true,$output_encoding,"UTF-8");
            $dirName = "/member";
            $dir = $this->get_work_dir().$dirName;
            if( !is_dir( $dir ) ){
                mkdir( $dir );
                chmod( $dir ,0777 );
            }
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");

            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="member_err.csv"');

            $header = array(
                "member_id"   => "member_id",
                "member_pass" => "member_pass",
                "name"        => "name",
                "name_kana"   => "name_kana",
                "email"       => "email",
                "group_name"  => "group_name",
                "timezone"    => "timezone",
                "lang"        => "lang(ja.en.zh-cn.zh-tw.fr.th.in.ko)",
                "storage_flg" => "storage_flg",
                "flag"        => "flag",
                "error"       => "error"
            );
            $csv->setHeader($header);
            $csv->write($header);

            foreach($rows as $a_key => $row) {
                $err_list["member_id"]      = $row["member_id"];
                $err_list["member_pass"]    = $row["member_pass"];
                $err_list["name"]           = $row["name"];
                $err_list["name_kana"]      = $row["name_kana"];
                $err_list["email"]          = $row["email"];
                $err_list["group_name"]     = $row["group_name"];
                $err_list["timezone"]       = $row["timezone"];
                $err_list["lang"]           = $row["lang"];
                $err_list["storage_flg"]    = $row["storage_flg"];
                $err_list["flag"]           = $row["flag"];
                $err_list["error"]          = $row["error"];

                $this->logger2->info($err_list);
                $csv->write($err_list);
            }
            $csv->close();
            $fp = fopen($tmpfile, "r");
            $contents = fread($fp, filesize($tmpfile));
            fclose($fp);
            chmod($tmpfile, 0777);
            print $contents;

            unlink($tmpfile);
        }
        unset($_SESSION['tmp_file']);
    }

    private function _isMemberExist($member_id,$user_key) {
        $where = sprintf( "user_key = '%s' AND member_id='%s'", $user_key, mysql_real_escape_string($member_id));
        $member_key = $this->memberTable->getOne($where, "member_key");
        return $member_key;
    }

    private function _getMemberCount($user_key) {
        $where = sprintf( "user_key = '%s' AND member_status >='0'", $user_key );
        $member_count = $this->memberTable->numRows($where);
        return $member_count;
    }

    private function _isMemberInfo($data) {
        $message = array();
        if(!$data['name']){
            $message[] = MEMBER_ERROR_NAME;
        } elseif (mb_strlen($data['name']) > 50) {
            $message[] = MEMBER_ERROR_NAME_LENGTH;
        }
        if (mb_strlen($data['name_kana']) > 50) {
            $message[] = MEMBER_ERROR_NAME_KANA_LENGTH;
        }

        $user_info = $this->session->get( "user_info");

        if($user_info['member_id_format_flg'] == 0){
            if (!EZValidator::valid_vcube_id($data['member_id'])) {
                $message[] = MEMBER_ERROR_ID_EMAIL;
            }elseif(mb_strlen($data['member_id']) > 255){
                $message[] = MEMBER_ERROR_ID_LENGTH;
            }
        }else{
            $regex_rule = '/^[[:alnum:]!#$%&\'*+\-\/=?^_`{|}~.@]{3,255}$/';
            $valid_result = EZValidator::valid_regex($data['member_id'], $regex_rule);
            if ($valid_result == false){
                $this->logger2->info($data['member_id']);
                $message[] = MEMBER_ERROR_ID_LENGTH;
            }
        }

//         if($this->_isExist($data['member_id'])){
//         	$message[] = MEMBER_ERROR_ID_EXIST;
//         }

        if (!$data['member_pass']) {
            $message[] = MEMBER_ERROR_PASS;
        } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $data['member_pass'])) {
            $message[] = USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$data['member_pass']) || preg_match('/^[[:alpha:]]+$/',$data['member_pass'])) {
            $message[] = USER_ERROR_PASS_INVALID_02;
        }
        if ($data['email']) {
            if (!EZValidator::valid_email($data['email'])){
                $message[] = MEMBER_ERROR_EMAIL_INVALID;
            } elseif (mb_strlen($data['email']) > 255){
                $message[] = MEMBER_ERROR_EMAIL_LENGTH;
            }
        }
        if ($data['group_name']) {
            if (mb_strlen($data['group_name']) > 50) {
                $message .= "<li>".MEMBER_ERROR_GROUP_NAME_LENGTH . "</li>";
            }
        }
        $language_list = $this->get_language_list();
        if(! EZValidator::valid_language( $data['lang'],$language_list ))
            $message[] = MEMBER_ERROR_LANGUAGE;
        //timezone validation
        $timezone_list = $this->get_timezone_list();
        if(!EZValidator::valid_language( $data['timezone'],$timezone_list ) && $data['timezone'] != "100")
            $message[] = MEMBER_ERROR_TIMEZONE;
        if($data["storage_flg"] !== "Y" && $data["storage_flg"] !== "N")
            $message[] = MEMBER_ERROR_STORAGE;

        return $message;
    }

    private function _isGroupExist($group_name,$user_key, $isCreate = false) {
        $group_key = $this->groupTable->getOne( sprintf( "member_group_name='%s' AND user_key = '%s'", $group_name, $user_key),"member_group_key");
        if($group_key){
            return $group_key;
        } elseif ($isCreate) {
            //add group
            $data = array();
            $data["member_group_name"] = trim($group_name);
            $data["user_key"] = $user_key;
            $key = $this->groupTable->add($data);
            return $key;
        } else {
            return 0;
        }
    }
    //member_group_key からそのmember_group_nameの取得
    private function _getMemberGroupName($key)
    {
        if ($key == 0) {
            return false;
        } else {
            $group = $this->groupTable->getRow( sprintf( "member_group_key='%s' AND user_key != 0", $key ) );
            return $group['member_group_name'];
        }
    }

    //member_status_key からそのmember_status_nameの取得
    private function _getMemberStatusName($key){
        if ($key == 0) {
            return false;
        } else {
            $obj_MemberStatus = new MemberStatusTable( $this->get_auth_dsn() );
            $ret = $obj_MemberStatus->select('member_status_key = '.$key);
            $status = $ret->fetchRow(DB_FETCHMODE_ASSOC);
            return $status['member_status_name'];
        }
    }

    /**
     * id からそのユーザーが存在するかチェック
     */
    private function _isExist($id){
        //member, user, 認証DBのuser のテーブルをチェック
        $obj_UserTable = new UserTable( $this->get_dsn() );
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        $id = mysql_real_escape_string($id);
        if ( $obj_UserTable->numRows( sprintf("user_id='%s'", $id ) ) > 0 ||
             $this->memberTable->numRows( sprintf("member_id='%s'", $id ) )  > 0 ||
             $obj_MgmUserTable->numRows( sprintf("user_id='%s'", $id ) )  > 0 ){
            return true;
        } else {
            return false;
        }
    }

    private function check_member_info( $data ){
        $message = null;
        if(!$data['member_name']){
            $message .= '<li>'.MEMBER_ERROR_NAME . '</li>';
        } elseif (mb_strlen($data['member_name']) > 50) {
            $message .= '<li>'.MEMBER_ERROR_NAME_LENGTH . '</li>';
        }
        if (mb_strlen($data['member_name_kana']) > 50) {
            $message .= '<li>'.MEMBER_ERROR_NAME_KANA_LENGTH . '</li>';
        }

        $user_info = $this->session->get("user_info");

        if($user_info['member_id_format_flg'] == 0){
            if (!EZValidator::valid_vcube_id($data['member_id'])) {
                $message .= '<li>'.MEMBER_ERROR_ID_EMAIL . '</li>';
            }elseif(mb_strlen($data['member_id']) > 255){
                $message .= '<li>'.MEMBER_ERROR_ID_LENGTH . '</li>';
            }
        }else{
            $regex_rule = '/^[[:alnum:]!#$%&\'*+\-\/=?^_`{|}~.@]{3,255}$/';
            $valid_result = EZValidator::valid_regex($data['member_id'], $regex_rule);
            if ($valid_result == false){
                $message .= '<li>'.MEMBER_ERROR_ID_LENGTH.'</li>';
            }
        }
        if($this->_isExist($data['member_id'])){
            $message .= '<li>'.MEMBER_ERROR_ID_EXIST . '</li>';
        }
        //言語をバリデーション
        $language_list = $this->get_language_list();
        if(! EZValidator::valid_language( $data['lang'],$language_list ))
            $message .= '<li>'.MEMBER_ERROR_LANGUAGE . '</li>';
        // メンバー課金の場合のみ必須
        if($user_info["account_model"] == "member" && !$data['member_email']){
            $message .= '<li>'.MEMBER_ERROR_EMAIL . '</li>';
        }
        if ($data['member_email'] != "") {
            if(!EZValidator::valid_email($data['member_email'])){
                $message .= '<li>'.MEMBER_ERROR_EMAIL_INVALID . '</li>';
            } elseif($user_info["account_model"] == "member" &&
            ! $this->duplicateEmail( $user_info["user_key"], $data['member_email'] ) ){
                $message .= '<li>'.MEMBER_ERROR_EMAIL_DUPLICATE . '</li>';
            }
        }
        if (!$data['member_pass'] && !$data['member_pass_check']) {
            $message .= '<li>'.MEMBER_ERROR_PASS . '</li>';
        }elseif ($data['member_pass'] != $data['member_pass_check']) {
            $message .= '<li>'.MEMBER_ERROR_PASS_DIFF . '</li>';
        //ユーザーパスワードをチェック
        } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $data['member_pass'])) {
            $message .= '<li>'.USER_ERROR_PASS_INVALID_01. '</li>';
        } elseif (!preg_match('/[[:alpha:]]+/',$data['member_pass']) || preg_match('/^[[:alpha:]]+$/',$data['member_pass'])) {
            $message .= '<li>'.USER_ERROR_PASS_INVALID_02. '</li>';
        }
        $where = "member_status != -1 AND user_key = " . $user_info["user_key"];
        $member_list = $this->memberTable->getRowsAssoc($where);
        if( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre" || $this->session->get("service_mode") == "sales") &&
            $user_info["max_member_count"] <= count( $member_list ) ) {
            $message .= '<li>'.MEMBER_ERROR_OVER_COUNT . '</li>';
        }
        // メンバー、センター以外
        if ($data['member_room_keys']) {
            foreach ($data['member_room_keys'] as $member_room_key) {
                if (($data['member_type'] == "terminal") &&
                        !array_key_exists($member_room_key, $this->get_room_info())) {
                    $message .= '<li>'.MEMBER_ERROR_ROOM_DENY . '</li>';
                }
            }
        }
        return $message;
    }

    /**
     * member_id と member_keyからそのIDがそのメンバーのものかチェック
     */
    private function _isExist_edit( $id, $key, $auth_user_key ){
        //member  と　user のテーブルをチェック
        $obj_UserTable = new UserTable( $this->get_dsn() );
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        $auth_info = $obj_MgmUserTable->numRows( sprintf( "user_key != '%s' AND user_id='%s'", $auth_user_key, mysql_real_escape_string($id) ) );
        if( $auth_info > 0 ){
            $this->logger2->info(array( $id, $key, $auth_user_key ));
            return false;
        }
        return true;
        /*
        $info = $this->memberTable->getRow( sprintf( "member_key='%s'", $key ) );
        $this->logger2->info($info);
        if ( $obj_UserTable->numRows( sprintf( "user_id='%s'", $id  ) ) > 0 ||
             $this->memberTable->numRows( sprintf( "member_id='%s'", $id ) ) > 1) {
            return true;
        } elseif ( $this->memberTable->numRows( sprintf( "member_id='%s'", $id ) ) == 1 &&
                   $id != $info['member_id']) {
            return true;
        } else {
            return false;
        }
        */
    }

    //同一ユーザーキーをもつメンバー同士でのメールアドレス重複チェック
    /*
    private function duplicateEmail( $user_key, $email )
    {
        require_once( "classes/dbi/address_book.dbi.php" );
        $obj_AddressBook = new AddressBookTable( $this->get_dsn() );
        $addressInfo = $obj_AddressBook->numRows( sprintf( "user_key = '%s' AND email = '%s'", $user_key, $email ) );
        return $addressInfo > 0 ? false : true;
    }
    */
    private function duplicateEmail( $user_key, $email, $member_key=null )
    {
        require_once( "classes/dbi/member.dbi.php" );
        $obj_Member = new MemberTable( $this->get_dsn() );
        $where = $member_key ?
                    sprintf( "user_key = '%s' AND member_key != '%s' AND member_email = '%s' AND member_status=0", $user_key, $member_key, $email ):
                    sprintf( "user_key = '%s' AND member_email = '%s' AND member_status=0", $user_key, $email );
        $addressInfo = $obj_Member->numRows( $where );
        return $addressInfo > 0 ? false : true;
    }

    /**
     * ＤＢにメンバー情報の登録
     */
    function action_add_member_complete()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            header( "Location: ?action_add_member=1" );
            exit;
        }
        //部屋追加
        $user_info = $this->session->get( "user_info" );
        $request = $this->session->get('new_member_info');

        $obj_Room = new RoomTable( $this->get_dsn() );
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable( $this->get_dsn() );
        //メンバー課金
        if( $user_info["account_model"] == "member" || $user_info["account_model"] == "centre" ){
            require_once( "classes/dbi/room.dbi.php" );
            require_once( "classes/dbi/user_plan.dbi.php" );
            require_once( "classes/dbi/room_plan.dbi.php" );
            $objUserPlan = new UserPlanTable( $this->get_dsn() );
            $objRoomPlanTable = new RoomPlanTable( $this->get_dsn() );
            if ($now_plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1")) {
                $service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
                $service_info = $service_db->getRow("service_key = '".addslashes($now_plan_info['service_key'])."' AND service_status = 1");
                $this->logger2->info(array($now_plan_info, $service_info));
            }
            $where = sprintf( "user_key=%d", $user_info["user_key"] );
            $count = $obj_Room->numRows($where);
            $room_key = $user_info["user_id"]."-".($count + 1)."-".substr( md5( uniqid( time(), true ) ), 0, 4 );
            $room_sort = $count + 1;
            //部屋登録
            $room_data = array (
                "room_key"              => $room_key,
                "max_seat"              => ($service_info["max_seat"]) ? $service_info["max_seat"] : 10,
                "max_audience_seat"     => ($service_info["max_audience_seat"]) ? $service_info["max_audience_seat"] : 0,
                "max_whiteboard_seat"   => ($service_info["max_whiteboard_seat"]) ? $service_info["max_whiteboard_seat"] : 0,
                "max_room_bandwidth"    => ($service_info["max_room_bandwidth"]) ? $service_info["max_room_bandwidth"] : "2048",
                "max_user_bandwidth"    => ($service_info["max_user_bandwidth"]) ? $service_info["max_user_bandwidth"] : "256",
                "min_user_bandwidth"    => ($service_info["min_user_bandwidth"]) ? $service_info["min_user_bandwidth"] : "30",
                "user_key"              => $user_info["user_key"],
                "room_name"             => $request['member_name'],
                "room_sort"             => $room_sort,
                "room_status"           => "1",
                "room_registtime"       => date("Y-m-d H:i:s"),
                );
            $this->logger2->info($room_data);
            $add_room = $obj_Room->add($room_data);
            if (DB::isError($add_room)) {
                $this->logger2->info( $add_room->getUserInfo());
            }
            /* userのプランからroom_planへコピー*/
            $planList = $objUserPlan->getRowsAssoc(sprintf("user_key='%s'", $user_info["user_key"]));
            for( $i = 0; $i < count( $planList ); $i++ ){
                $data = array(
                            "room_key"=>$room_key,
                            "service_key"=>$planList[$i]["service_key"],
                            "discount_rate"=>$planList[$i]["discount_rate"],
                            "contract_month_number"=>$planList[$i]["contract_month_number"],
                            "room_plan_yearly"=>$planList[$i]["user_plan_yearly"],
                            "room_plan_yearly_starttime"=>$planList[$i]["user_plan_yearly_starttime"],
                            "room_plan_starttime"=>$planList[$i]["user_plan_starttime"],
                            "room_plan_endtime"=>$planList[$i]["user_plan_endtime"],
                            "room_plan_status"=>$planList[$i]["user_plan_status"],
                            "room_plan_registtime"=>$planList[$i]["user_plan_registtime"],
                            "room_plan_updatetime"=>$planList[$i]["user_plan_updatetime"]
                             );
                $addPlan = $objRoomPlanTable->add( $data );
                if (DB::isError( $addPlan ) ) {
                    $this->logger2->info( $addPlan->getUserInfo() );
                }
            }

            // userのプランオプション(user_service_option)からmemberの部屋のオプションを追加する(ordered_service_option)
            $this->objMeetingDB = new N2MY_DB( $this->get_dsn(), "user_service_option" );
//            $condition = sprintf( "user_key='%s' AND user_service_option_starttime<='%s' AND user_service_option_status=1", $user_info["user_key"], date( "Y-m-d" ) );
            $condition = sprintf( "user_key='%s' AND service_option_key !=14", $user_info["user_key"] );
            $optionList = $this->objMeetingDB->getRowsAssoc( $condition );

            for( $i = 0; $i < count( $optionList ); $i++ ){
                $data = array(
                    "room_key" => $room_key,
                    "user_service_option_key" => $optionList[$i]["user_service_option_key"],
                    "service_option_key" => $optionList[$i]["service_option_key"],
                    "ordered_service_option_status" => $optionList[$i]["user_service_option_status"],
                    "ordered_service_option_starttime" => $optionList[$i]["user_service_option_starttime"],
                    "ordered_service_option_registtime" => $optionList[$i]["user_service_option_registtime"],
                    "ordered_service_option_deletetime" => $optionList[$i]["user_service_option_deletetime"]
                );
                $ordered_service_option->add( $data );
            }
        } else {
            $room_key = $request['member_room_key'];
        }

        //member追加
        $obj_UserTable = new UserTable( $this->get_dsn() );

        $user_key = $user_info['user_key'];
        $temp = array(
            "user_key"           => $user_key,
            "member_id"          => $request['member_id'],
            "member_pass"        => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $request['member_pass']),
            "member_email"       => $request['member_email'],
            "member_name"        => $request['member_name'],
            "member_name_kana"   => $request['member_name_kana'],
            "member_status"      => (($request['member_status_key']) ? $request['member_status_key'] : 0),
            "member_type"        => (($request['member_type']) ? $request['member_type'] : ""),
            "member_group"       => $request['member_group_key'],
            "room_key"           => $room_key,
            "timezone"           => $request['timezone'],
            "lang"               => $request['lang'],
            "use_shared_storage" => $request['use_shared_storage'],
            "use_ss_watcher"      => $request['use_ss_watcher']
        );
        $ret = $this->memberTable->add( $temp );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$temp);
        }

        if ($this->session->get("service_mode") == "sales") {
            require_once( "classes/dbi/room.dbi.php" );
            $obj_Room = new RoomTable( $this->get_dsn() );
            $where = sprintf( "user_key=%d", $user_info["user_key"] );
            $obj_Room = new RoomTable( $this->get_dsn() );
            $count = $obj_Room->numRows($where);
            $room_key = $user_info["user_id"]."-".($count + 1)."-".substr( md5( uniqid( time(), true ) ), 0, 4 );
            $room_sort = $count + 1;
            //部屋登録
            $room_data = array (
                "room_key"            => $room_key,
                "room_name"           => $request['member_name'],
                "user_key"            => $user_info["user_key"],
                "room_sort"           => $room_sort,
                "room_status"         => "1",
                "use_teleconf"        => "1",
                "use_pgi_dialin"      => "1",
                "use_pgi_dialin_free" => "1",
                "use_pgi_dialout"     => "1",
                "use_sales_option"    => "1",
                "max_ss_watcher_seat" => "1",
                "room_registtime"     => date("Y-m-d H:i:s"),
            );
            $this->logger2->info($room_data);
            $add_room = $obj_Room->add($room_data);
            if (DB::isError($add_room)) {
                return $this->logger2->error( $add_room->getUserInfo());
            } else {
                //add service plan
                require_once( "classes/dbi/room_plan.dbi.php" );
                $service_db = new N2MY_DB( $this->account_dsn, "service" );
                //セールスプランを選択また検討中、今MOCKします。
                //$sales_plan　= "89";
                require_once( "classes/dbi/user_plan.dbi.php" );
                $objUserPlan = new UserPlanTable( $this->get_dsn() );
                $sales_plan = $objUserPlan->getOne("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1 AND sales_plan_flg = 1", "service_key");
                $service_info = $service_db->getRow("service_key = '".$sales_plan."' AND service_status = 1");
                if($service_info) {
                    $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
                    $room_plan = array (
                        "room_key" => $room_key,
                        "service_key" => $service_info["service_key"],
                        "room_plan_status" => "1",
                        "room_plan_starttime" => date("Y-m-d 00:00:00"),
                        "room_plan_endtime" => "0000-00-00 00:00:00",
                        "discount_rate" => "0",
                        "contract_month_number" => "0",
                        "room_plan_registtime" => date("Y-m-d H:i:s"),
                    );
                    $add_plan = $room_plan_db->add($room_plan);
                    $where = "room_key = '".addslashes($room_key)."'";
                    $room_seat = array (
                        "max_seat" => $service_info["max_seat"],
                        "max_audience_seat" => $service_info["max_audience_seat"],
                        "max_whiteboard_seat" => $service_info["max_whiteboard_seat"],
                        "meeting_limit_time" => $service_info["meeting_limit_time"],
                        "default_camera_size" => $service_info["default_camera_size"],
                        "hd_flg"              => $service_info["hd_flg"],
                        "room_updatetime" => date("Y-m-d H:i:s"),
                    );
                    if ($service_info["max_room_bandwidth"] != 0) {
                        $room_seat["max_room_bandwidth"] = $service_info["max_room_bandwidth"];
                    }
                    if ($service_info["max_user_bandwidth"] != 0) {
                    $room_seat["max_user_bandwidth"] = $service_info["max_user_bandwidth"];
                    }
                    if ($service_info["min_user_bandwidth"] != 0) {
                        $room_seat["min_user_bandwidth"] = $service_info["min_user_bandwidth"];
                    }
                    $room_seat["whiteboard_page"] = $service_info["whiteboard_page"] ? $service_info["whiteboard_page"] : 100;
                    $room_seat["whiteboard_size"] = $service_info["whiteboard_size"] ? $service_info["whiteboard_size"] : 20;
                    $room_seat["cabinet_size"] = $service_info["cabinet_size"] ? $service_info["cabinet_size"] : 20;
                    $add_seat = $obj_Room->update($room_seat, $where);
                } else {
                    $this->logger2->error("セールスサービスプランがないので、スタッフ追加できないはずです！");
                }
                $relation_db = new N2MY_DB($this->account_dsn, "relation");
                $where = "user_key = ".$user_key." AND member_id='". mysql_real_escape_string($request['member_id'])."'";
                $info = $this->memberTable->getRow( $where );
                //memberのoutbound_idを確認
                $outbound_id = $info["outboud_id"] ? $info["outboud_id"] : "";
                if (!$outbound_id) {
                   $outbound_db = new N2MY_DB($this->get_dsn(), "outbound");
                    // outboundID設定がない場合は作成
                    $outbound_where = "user_key = ".$user_key;
                    $outbound_count = $outbound_db->numRows($outbound_where);
                    if ($outbound_count == 0 || !$outbound_count) {
                        // 登録
                        $outbound_data = array(
                                "user_key"   => $user_key,
                                "status"     => 1,
                                "create_datetime" => date("Y-m-d H:i:s"),
                                "update_datetime" => date("Y-m-d H:i:s"),
                        );
                        $outbound_db->add($outbound_data);
                    }
                    $count = 1;
                    while($count > 0) {
                        $outbound_id = $this->create_id();
                        $where = "outbound_id = '".$outbound_id."'";
                        $count = $this->memberTable->numRows($where);
                        $this->logger->info("outbound",__FILE__,__LINE__,array($outbound_id, $count));
                    }
                    //スタッフナンバー発行
                    $outbound_where = "user_key = ".$user_key;
                    $outbound_info = $outbound_db->getRow($outbound_where);
                    $outbound_key = sprintf("%03d",$outbound_info["outbound_key"]);
                    $outbound_where = "user_key = ".$user_key.
                    " AND outbound_id != ''";
                    $sales_member_count = $this->memberTable->numRows($outbound_where);
                    $outbound_sort = $sales_member_count + 1;
                    $outbound_sort = sprintf("%03d",$outbound_sort);
                    $outbound_number_id = $outbound_key.$outbound_sort;
                    $member_update_data = array(
                            "outbound_id" => $outbound_id,
                            "outbound_number_id" => $outbound_number_id,
                            "use_sales" => 1);
                    $where_member = "member_key = ".$info["member_key"];
                    $this->memberTable->update($member_update_data, $where_member);
                    $where_room = "room_key = '".$room_key."'";
                    $room_update_data = array("outbound_id" => $outbound_id);
                    $add_room = $obj_Room->update($room_update_data, $where_room);
                    $relation_data = array(
                        "relation_key" => $outbound_id,
                        "relation_type" => "outbound",
                        "user_key" => $request["member_id"],
                        "status" => "1",
                        "create_datetime" => date("Y-m-d H:i:s"),
                    );
                    $relation_db->add($relation_data);
                    $relation_data_number = array(
                            "relation_key" => $outbound_number_id,
                            "relation_type" => "outbound_number",
                            "user_key" => $request["member_id"],
                            "status" => "1",
                            "create_datetime" => date("Y-m-d H:i:s"),
                    );
                    $relation_db->add($relation_data_number);
                }
                //add inbound&relation if needed.
                $inbound_db = new N2MY_DB($this->get_dsn(), "inbound");
                // inbound設定がない場合は作成
                $inbound_where = "user_key = ".$user_info["user_key"];
                $inbound_count = $inbound_db->numRows($inbound_where);
                if ($inbound_count == 0 || !$inbound_count) {
                    $count = 1;
                    while($count > 0) {
                        // 念のため8ケタにそろえる
                        $inbound_id = substr($this->create_id(), 0 ,8);
                        $where = "inbound_id = '".$inbound_id."'";
                        $count = $inbound_db->numRows($where);
                    }
                    // 登録
                    $inbound_data = array(
                        "inbound_id" => $inbound_id,
                        "user_key"   => $user_info["user_key"],
                        "status"     => 1,
                        "create_datetime" => date("Y-m-d H:i:s"),
                        "update_datetime" => date("Y-m-d H:i:s"),
                    );
                    $this->logger2->info($inbound_data);
                    $inbound_db->add($inbound_data);
                    //relation追加
                    $relation_data = array(
                        "relation_key" => $inbound_id,
                        "relation_type" => "inbound_id",
                        "user_key" => $user_info["user_id"],
                        "status" => 1,
                        "create_datetime" => date("Y-m-d H:i:s"),
                    );
                    $relation_db->add($relation_data);
                }
                //roomオプション付与
                $obj_user_service_option    = new N2MY_DB($this->get_dsn(), "user_service_option");
                $obj_ordered_service_option = new N2MY_DB($this->get_dsn(), "ordered_service_option");
                $user_service_options = $obj_user_service_option->getRowsAssoc("user_service_option_status = 1 AND user_key = " . $user_info["user_key"]);
                if($user_service_options){
                    foreach ($user_service_options as $user_service_option) {
                        $ordered_service_option_data = array(
                            "room_key"                           => $room_key,
                            "user_service_option_key"            => $user_service_option["user_service_option_key"],
                            "service_option_key"                 => $user_service_option["service_option_key"],
                            "ordered_service_option_status"      => 1,
                            "ordered_service_option_starttime"   => $user_service_option["user_service_option_starttime"],
                            "ordered_service_option_registtime"  => date("Y-m-d H:i:s")
                        );
                        $obj_ordered_service_option->add($ordered_service_option_data);
                    }
                }
            }
        }
        //認証サーバーへ追加
        $obj_MgmUserTable = new MgmUserTable( N2MY_MDB_DSN );
        $session = $this->session->get( "server_info" );
        $auth_data = array(
            "user_id"       => $request['member_id'],
            "server_key"    => $session["server_key"]
            );
        $result = $obj_MgmUserTable->add( $auth_data );
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$auth_data);
        }
        //relation tableへ追加
        require_once( "classes/mgm/MGM_Auth.class.php" );
        $obj_MGMAuthClass = new MGM_AuthClass( N2MY_MDB_DSN );
        $obj_MGMAuthClass->addRelationData( $user_info["user_id"], $room_key, "mfp" );
        //部屋のリレーション追加
        if ($room_key) {
            $where = "user_key = ".$user_key.
                     " AND member_id='".mysql_real_escape_string($request['member_id'])."'";
            $info = $this->memberTable->getRow( $where, "member_key" );
            require_once("classes/dbi/member_room_relation.dbi.php");
            $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
            $data = array("member_key" => $info["member_key"],
                          "room_key"   => $room_key,
                          "create_datetime" => date("Y-m-d H:i:s"));
            $this->logger2->debug($data);
            $objRoomRelation->add($data);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            }
            $this->logger2->info("容量追加");
            //容量追加
            if($user_info["account_model"] == ""){
                $update_data = array(
                    "max_storage_size" => $user_info["max_storage_size"] + 1000.
                );
                $update_where = "user_key='".addslashes($user_info["user_key"])."'";
                $obj_UserTable->update($update_data, $update_where);
            } else if($user_info["account_model"] == "member" || $user_info["account_model"] == "centre" ) {
                require_once( "classes/dbi/user_plan.dbi.php" );
                $objUserPlan = new UserPlanTable( $this->get_dsn() );
                if ($now_plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1")) {
                    $service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
                    $service_info = $service_db->getRow("service_key = '".addslashes($now_plan_info['service_key'])."' AND service_status = 1");
                    $this->logger2->info(array($now_plan_info, $service_info));
                    $update_data = array(
                            "max_storage_size" => intval($user_info["max_storage_size"]) + intval($service_info["member_storage_size"])
                    );
                    $update_where = "user_key='".addslashes($user_info["user_key"])."'";
                    //容量更新あるよって、user_infoも最新データに変更する
                    $obj_UserTable->update($update_data, $update_where);
                }
            }
            $user_info = $obj_UserTable->getRow("user_key='".addslashes($user_info["user_key"])."'");
            $this->session->set("user_info", $user_info);
        }
        //V-CUBE IDへ登録
        if($user_info['member_id_format_flg'] == '0'){
            /*
            $wsdl = $this->config->get('VCUBEID','wsdl');
            $meetingConsumerKey = $this->config->get('VCUBEID','meeting_consumer_key');
            $paperkessConsumerKey = $this->config->get('VCUBEID','paperless_consumer_key');
            require_once("classes/dbi/ordered_service_option.dbi.php");
            $where_room = "user_key ='".addslashes($user_info["user_key"])."'";
            $rooms = $obj_Room->getRowsAssoc($where_room, null, null, null, "room_key");
            foreach($rooms as $room) {
                $roomkeys[] = $room["room_key"];
            }
            if ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") {
                $where = "room_key = '".addslashes($room_key)."'";
            } else {
                $this->logger2->debug(array($rooms, $roomkeys));
                $where = "room_key in ('".join("','", $roomkeys)."')";
            }
            $where .= " AND service_option_key = 16".
                    " AND ordered_service_option_status = 1";
            $paperless_count = $ordered_service_option->numRows($where);
            $consumerKeys = array();
            if (count($roomkeys) > $paperless_count) {
                $consumerKeys[] = $meetingConsumerKey;
            }
            if($paperless_count > 0){
                $consumerKeys[] = $paperkessConsumerKey;
            }
            if ($wsdl) {
                try {
                    //VCUBE ID側にアカウント登録
                    $soap = new SoapClient($wsdl,array('trace' => 1));
                    $member_response = $soap->addVcubeId(1, $request['member_id'], sha1($request['member_pass']),
                    $request['member_name'], $request['member_name_kana'], $request['member_email']);
                    if ($member_response["result"] || $member_response["code"] == 1002) {
                        foreach ($consumerKeys as $consumerKey) {
                            $soap = new SoapClient($wsdl,array('trace' => 1));
                            $response = $soap->addService($request['member_id'], $consumerKey);
                            if ($response["code"] == true) {
                                $this->logger2->info("add service complete".$request['member_id'].":".$consumerKey);
                            } else if ($response["code"] == "1002") {
                                $this->logger2->info("already added service".$request['member_id'].":".$consumerKey);
                            } else {
                                $this->logger2->error("add service error".$request['member_id'].":".$consumerKey);
                            }
                        }
                    }
                } catch (Exception $e) {
                    $this->logger2->warn($e->getMessage());
                }
            }
            */
        }
        $this->logger->info(__FUNCTION__."#member_add",__FILE__,__LINE__,$temp);
        $this->session->remove('new_member_info');
        // 操作ログ
        $this->add_operation_log('member_add', array('member_id' => $request['member_id']));
        $this->display('admin/member/members/add/done.t.html');
    }

    /**
     * 乱数ID発行
     */
    function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this->dec2any($c);
    }
    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=57, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr(
                '23456789ab' .
                'cdefghijkm' .
                'nopqrstuvw' .
                'xyzABCDEFG' .
                'HJKLMNPQRS' .
                'TUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }

    function action_edit_member($message = ""){
        
        if($message){
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        $obj_MemberStatus = new MemberStatusTable( N2MY_MDB_DSN );

        $group_list = $this->groupTable->getRowsAssoc( sprintf( "user_key='%s'", $user_key ) );
        foreach( $group_list as $group_info ){
            $member_group[] = array(
                                "key" => $group_info['member_group_key'],
                                "name" => $group_info['member_group_name']
                                );
        }
        $member_status_list = $obj_MemberStatus->getRowsAssoc();
        foreach( $member_status_list as $status_info) {
                $member_status[] = array(
                    "key"     => $status_info['member_status_key'],
                    "name"     => $status_info['member_status_name']
                );
        }

        $this->template->assign('member_group', $member_group);
        $this->template->assign('member_status', $member_status);

        if($this->request->get('mode') == "edit_user"){
            //member_keyからメンバーを取得
            $member_key = $this->request->get('member_key');
            $info = $this->memberTable->getRow( sprintf( "user_key='%s' AND member_key='%s'", $user_key , $member_key ) );
            // 不正アクセス対策
            if(!$info){
                $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
                return $this->action_admin_member();
            }
            $member_info['auth_user_key']      = $this->request->get( "auth_user_key" );
            $member_info['member_key']         = $member_key;
            $member_info['member_id']          = $info['member_id'];
            $member_info['member_email']       = $info['member_email'];
            $member_info['member_pass']        = $info['member_pass'];
            $member_info['member_pass_check']  = $info['member_pass'];
            $member_info['member_name']        = $info['member_name'];
            $member_info['member_name_kana']   = $info['member_name_kana'];
            $member_info['member_status_key']  = $info['member_status'];
            $member_info['member_type']        = $info['member_type'];
            $member_info['member_group_key']   = $info['member_group'];
            $member_info['timezone']           = $info['timezone'];
            $member_info['lang']               = $info['lang'];
            $member_info['use_shared_storage'] = $info['use_shared_storage'];
            $member_info['use_ss_watcher']     = $info['use_ss_watcher'];
            $member_info['vcube_one_member_id']     = $info['vcube_one_member_id'];
            require_once("classes/dbi/member_room_relation.dbi.php");
            $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
            $where = "member_key = '".$member_key."'";
            $relation_info = $objRoomRelation->getRowsAssoc($where, array("create_datetime" => "desc"));
            
            if ($relation_info) {
                foreach ($relation_info as $relation) {
                    $member_room_keys[$relation["room_key"]] = $relation["room_key"];
                }
            }
            $member_info['member_room_keys']  = $member_room_keys;
            $this->logger2->debug($member_info['member_room_keys']);
        } else {
            $member_info = $this->session->get('m_user_edit_info');
        }
        foreach($member_info as $key => $value){
            $this->template->assign($key, $value);
        }
        $this->display('admin/member/members/edit/index.t.html');
    }

    function action_edit_one_member_confirm(){
        // oneアカウントのメンバー変更処理
        $request = $this->request->getAll();
        $this->session->set('m_user_edit_info', $request);
        $user_info = $this->session->get( "user_info");
        
        foreach($request as $key => $value){
            $this->template->assign($key, $value);            
            if($key == "member_key"){
                $memberInfo = $this->memberTable->getRow( sprintf( "user_key='%s' AND member_key='%s'",$user_info["user_key"] , $value ));
            }
        }
        
        // one アカウントのため、vcube_one_member_idカラムを参照
        $this->template->assign("member_name", $memberInfo["member_name"]);
        $this->template->assign("member_id", $memberInfo["vcube_one_member_id"]);
        
        $this->display('admin/member/members/edit/confirm.t.html');
        
    }

    function action_edit_member_confirm(){
        // 入力値を保持
        $request = $this->request->getAll();
        $this->session->set('m_user_edit_info', $request);
        $user_info = $this->session->get( "user_info");
    
        $message = null;
    
        if (!$request['member_name']) {
            $message .= '<li>'.MEMBER_ERROR_NAME . '</li>';
        }
        if ($request['member_type'] == 'terminal') {
            if (!preg_match('/^[[:alnum:]!#$%&\'*+\-\/=?^_`{|}~.@]{3,255}$/', $request['member_id']))
                $message .= '<li>'.MEMBER_ERROR_ID_LENGTH . '</li>';
        } else {
            if($user_info['member_id_format_flg'] == 0){
                if (!EZValidator::valid_vcube_id($request['member_id'])) {
                    $message .= '<li>'.MEMBER_ERROR_ID_EMAIL . '</li>';
                }elseif(mb_strlen($request['member_id']) > 255){
                    $message .= '<li>'.MEMBER_ERROR_ID_LENGTH . '</li>';
                }
            }else{
                $regex_rule = '/^[[:alnum:]!#$%&\'*+\-\/=?^_`{|}~.@]{3,255}$/';
                $valid_result = EZValidator::valid_regex($request['member_id'], $regex_rule);
                if ($valid_result == false){
                    $message .= '<li>'.MEMBER_ERROR_ID_LENGTH.'</li>';
                }
            }
            if (!$this->_isExist_edit($request['member_id'], $request['member_key'], $request["auth_user_key"] )){
                $message .= '<li>'.MEMBER_ERROR_ID_EXIST . '</li>';
            }
        }
        $language_list = $this->get_language_list();
        if(! EZValidator::valid_language( $request['lang'],$language_list ))
            $message .= '<li>'.MEMBER_ERROR_LANGUAGE . '</li>';
    
        // メンバー課金のみ必須
        if ($user_info["account_model"] == "member" && !$request['member_email']) {
            $message .= '<li>'.MEMBER_ERROR_EMAIL . '</li>';
        }
        if ($request['member_email'] != "") {
            if( ! EZValidator::valid_email( $request['member_email'] ) ){
                $message .= '<li>'.MEMBER_ERROR_EMAIL_INVALID . '</li>';
            } elseif ($user_info["account_model"] == "member" &&
                            ! $this->duplicateEmail( $user_info["user_key"], $request['member_email'], $request["member_key"] ) ){
                $message .= '<li>'.MEMBER_ERROR_EMAIL_DUPLICATE . '</li>';
            }
        }
        // password変更
        if ($request["chgPasswd"]) {
            if (!$request['member_pass'] && !$request['member_pass_check']) {
                $message .= '<li>'.MEMBER_ERROR_PASS . '</li>';
            }elseif ($request['member_pass'] != $request['member_pass_check']) {
                $message .= '<li>'.MEMBER_ERROR_PASS_DIFF . '</li>';
                //ユーザーパスワードをチェック
            } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $request['member_pass'])) {
                $message .= '<li>'.USER_ERROR_PASS_INVALID_01. '</li>';
            } elseif (!preg_match('/[[:alpha:]]+/',$request['member_pass']) || preg_match('/^[[:alpha:]]+$/',$request['member_pass'])) {
                $message .= '<li>'.USER_ERROR_PASS_INVALID_02. '</li>';
            }
        }
        // メンバー、センター以外
        if ($request['member_room_keys']) {
            foreach ($request['member_room_keys'] as $member_room_key) {
                if (($request['member_type'] == "terminal") &&
                !array_key_exists($member_room_key, $this->get_room_info())) {
                    $message .= '<li>'.MEMBER_ERROR_ROOM_DENY . '</li>';
                }
            }
        }
        if ($message) {
            $this->action_edit_member($message);
        } else {
    
            foreach($request as $key => $value){
                if($key == "member_group_key"){
                    $this->template->assign('member_group_name', $this->_getMemberGroupName( $value ));
                }elseif($key == "member_status_key"){
                    $this->template->assign('member_status_name', $this->_getMemberStatusName($value));
                }elseif($key == "member_pass"){
                    $length = mb_strlen($value);
                    $value = "";
                    for($i=0;$i<$length;$i++){
                        $value .= "*";
                    }
                }else if($key == "timezone"){
                    if( 100 == $value ){
                        $timezone = MEMBER_UNDEFINED_TIMEZONE;
                    } else {
                        $timezoneList = $this->get_timezone_list();
                        foreach( $timezoneList as $k => $zone ){
                            if( $zone["key"] == $value ){
                                $timezone = $zone["value"];
                                break 1;
                            }
                        }
                    }
                    $this->template->assign( 'timezoneInfo', $timezone );
                }
                $this->template->assign($key, $value);
            }
            $this->display('admin/member/members/edit/confirm.t.html');
        }
    }
    
    function action_edit_one_member_complete()
    {
        $request = $this->session->get('m_user_edit_info');
        $user_info = $this->session->get('user_info');
        $this->memberTable->ss_watcher_update($request['use_ss_watcher'],$user_info["user_key"],$request['member_key']);

        // 操作ログ
        $this->add_operation_log('member_update', array('member_id' => $request['member_id']));
        $this->display('admin/member/members/edit/done.t.html');
        
    }
    
    
    function action_edit_member_complete()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_admin_member();
        }
        $request = $this->session->get('m_user_edit_info');
        $temp = array(
            "member_id"          => $request['member_id'],
//            "member_pass"      => $request['member_pass'],
            "member_email"       => $request['member_email'],
            "member_name"        => $request['member_name'],
            "member_name_kana"   => $request['member_name_kana'],
            "member_status"      => (($request['member_status_key']) ? $request['member_status_key'] : 0),
            "member_type"        => $request['member_type'],
            "member_group"       => $request['member_group_key'],
            "timezone"           => $request['timezone'],
            "lang"               => $request['lang'],
            "use_shared_storage" => $request['use_shared_storage'],
            "use_ss_watcher"     => $request['use_ss_watcher']
        );
        
        //
        $user_info = $this->session->get('user_info');
        if ($request['member_room_keys'][0]) {
            $temp["room_key"] = $request['member_room_keys'][0];
        }
        if ($request["chgPasswd"]) {
            $temp["member_pass"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $request['member_pass']);
        }
        // 削除処理
        $ret = $this->memberTable->update($temp, sprintf( "member_key='%s'", $request['member_key'] ) );
        // エラー処理
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#edit member successful!",__FILE__,__LINE__,$temp);
        }
        if ($request['member_type'] == "terminal" && $request['member_room_keys']) {
            //一度登録されているKeyは削除して、再度登録
            require_once("classes/dbi/member_room_relation.dbi.php");
            $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
            $where_remove = "member_key = ".addslashes($request['member_key']);
            $ret = $objRoomRelation->remove($where_remove);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            }
            foreach ($request['member_room_keys'] as $room_key) {
                $data = array("member_key" => $request['member_key'],
                              "room_key"   => $room_key,
                              "create_datetime" => date("Y-m-d H:i:s"));
                $this->logger2->debug($data);
                $objRoomRelation->add($data);
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                }
            }
        }
        //認証サーバーのアップデート
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        $session = $this->session->get( "server_info" );
        $auth_data = array(
            "user_id"     => $request['member_id']
            );
        $result = $obj_MgmUserTable->update( $auth_data, sprintf( "user_key='%s'", $request['auth_user_key'] ) );
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#update auth_user successful!",__FILE__,__LINE__,$auth_data);
        }
        $memberInfo = $this->memberTable->getRow( sprintf( "user_key='%s' AND member_key='%s'",$user_info["user_key"] , $request['member_key'] ) );
        if ($memberInfo["use_sales"]) {
            // リレーションテーブルの更新
            require_once ("classes/mgm/dbi/relation.dbi.php");
            $obj_MgmRelationTable = new MgmRelationTable($this->get_auth_dsn());
            $where_outbound = "relation_key = '".$memberInfo["outbound_id"]."'".
                              " AND relation_type = 'outbound'".
                              " AND status = 1";
            $relation_data = array(
                "user_key" => $request['member_id'],
                );
            $obj_MgmRelationTable->update($relation_data, $where_outbound);

            $where_outbound_id = "relation_key = '".$memberInfo["outbound_number_id"]."'".
                              " AND relation_type = 'outbound_number'".
                              " AND status = 1";
            $obj_MgmRelationTable->update($relation_data, $where_outbound_id);

        }

        $this->logger->info(__FUNCTION__."#member_edit",__FILE__,__LINE__,$temp);
        $this->session->remove('m_user_edit_info');
        // 操作ログ
        $this->add_operation_log('member_update', array('member_id' => $request['member_id']));
        $this->display('admin/member/members/edit/done.t.html');
    }

    function action_delete_member(){
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_admin_member();
        }
        $user_info = $this->session->get( "user_info");
        $member_key = $this->request->get('member_key');
        $memberInfo = $this->memberTable->getRow( sprintf( "user_key='%s' AND member_key='%s'",$user_info["user_key"] , $member_key ) );
        // 不正アクセス対策
        if(!$memberInfo){
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
            return $this->action_admin_member();
        }

        require_once 'classes/N2MY_Account.class.php';
        $obj_n2my_account = new N2MY_Account( parent::get_dsn() );
        $obj_n2my_account->deleteMember( $memberInfo['member_key'] );

        require_once 'classes/dbi/user.dbi.php';
        $obj_user = new UserTable( parent::get_dsn() );
        $user_info = $obj_user->getRow(sprintf('user_key=%d', $memberInfo['user_key']));
        $this->session->set('user_info', $user_info);

        // 操作ログ
        $this->add_operation_log('member_delete', array('member_id' => $memberInfo['member_id']));
        $message .= "<li>".MEMBER_DELETE_USER . "</li>";
        $this->render_admin_member($message);
    }

    function action_admin_group($message = ""){

        if ($message) {
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        $group_list = $this->groupTable->getRowsAssoc( sprintf( "user_key='%s'", $user_key ) );
        foreach( $group_list as $group_info ){
            $member_group[] = array(
                                "key" => $group_info['member_group_key'],
                                "name" => $group_info['member_group_name']
                                );
        }

        $this->template->assign('info', $member_group);
        $this->display('admin/member/group/index.t.html');
    }

    function action_add_group(){
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_admin_group();
        }
        $message = "";
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        $group_name =$this->request->get('group_name');
        //user_keyからグループを取得
        if (!$this->request->get('group_name')) {
            $message .= "<li>".MEMBER_ERROR_GROUP_NAME . "</li>";
        } elseif (mb_strlen($group_name) > 50) {
            $message .= "<li>".MEMBER_ERROR_GROUP_NAME_LENGTH . "</li>";
        } elseif ( $this->groupTable->numRows( sprintf( "user_key='%s' AND member_group_name='%s'", $user_key, addslashes($group_name) ) ) > 0) {
            $message .= "<li>".MEMBER_ERROR_GROUP_NAME_EXIST . "</li>";
        } else {
            $temp = array(
                "user_key" => $user_key,
                "member_group_name" => $this->request->get('group_name')
            );
            $this->groupTable->add($temp);
            $message .= "<li>".MEMBER_NEW_GROUP_NAME . "</li>";
        }
        // 操作ログ
        $this->add_operation_log('member_group_add', array('group_name' => $group_name));
        $this->action_admin_group($message);
    }

    public function action_edit_group()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_admin_group();
        }
        $message = "";
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        $group_key = $this->request->get('group_key');
        $group_name = $this->request->get('group_name');

        $group_info = $this->groupTable->getRow(sprintf( "user_key='%s' AND member_group_key='%s'",$user_info["user_key"] , $group_key ));
        // 不正アクセス対策
        if(!$group_info){
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
            return $this->action_admin_group();
        }

        // 未入力
        if (!$group_name) {
            $message .= "<li>".MEMBER_ERROR_GROUP_NAME . "</li>";
        // グループ名変更無しで、
        } elseif ( $this->groupTable->numRows( sprintf( "user_key='%s' AND member_group_name='%s'", $user_key, addslashes($group_name))) > 0 ) {
            $message .= "<li>".MEMBER_ERROR_GROUP_NAME_EXIST . "</li>";
        } else {
            $temp = array(
                "member_group_name" => $group_name
            );
            $this->groupTable->update($temp, sprintf( "member_group_key='%s'", $group_key ) );
            $message .= "<li>".MEMBER_EDIT_GROUP_NAME . "</li>";
        }
        // 操作ログ
        $this->add_operation_log('member_group_update', array('group_name' => $group_name));
        $this->action_admin_group($message);
    }

    function action_delete_group()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_admin_group();
        }
        $message = "";
        $group_key = $this->request->get('group_key');
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        $group_info = $this->groupTable->getRow(sprintf( "user_key='%s' AND member_group_key='%s'",$user_info["user_key"] , $group_key ));
        // 不正アクセス対策
        if(!$group_info){
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
            return $this->action_admin_group();
        }
        $temp['user_key'] = 0;
        $this->groupTable->update( $temp, sprintf( "member_group_key='%s'", $group_key) );
        unset($temp);
        $temp['member_group'] = 0;
        $this->memberTable->update($temp, sprintf("user_key = '%s' AND member_group = '%s'", $user_key, $group_key));
        $message .= "<li>".MEMBER_DELETE_GROUP_NAME . "</li>";
        // 操作ログ
        $group_name = $this->groupTable->getOne(sprintf( "member_group_key='%s'", $group_key), 'member_group_name');
        $this->add_operation_log('member_group_delete', array('group_name' => $group_name));
        $this->action_admin_group($message);
    }

    function showAdminLogin($message = ""){

        if ($message) {
            $this->template->assign('message', $message);
        }
        $this->display('user/admin_login.tmpl');
    }

    function showAdminTop(){
        $main = new Main();
        if ($message = $main->checkAdminLoginInfo()) {
            showAdminLogin($message);
            exit;
        }
        header("Location: /service/admin.php");
    }
}

$main =& new AppMember();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>

