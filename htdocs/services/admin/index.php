<?php

require_once('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");

/*新型roomクラスに移行中*/
require_once('classes/N2MY_DBI.class.php');
require_once('classes/dbi/room.dbi.php');
require_once('classes/dbi/user.dbi.php');
require_once("classes/dbi/ordered_service_option.dbi.php");

class AppAdmin extends AppFrame
{
    var $err_obj = null;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    function action_env() {
        $this->set_submit_key($this->_name_space);
        $lang       = $this->request->get("lang", $_COOKIE["lang"]);
        $country    = $this->request->get("country", $_COOKIE["country"]);
        $time_zone  = $this->request->get("time_zone", $_COOKIE["time_zone"]);
        $this->template->assign("lang", $lang);
        $this->template->assign("country", $country);
        $this->template->assign("time_zone", $time_zone);
        $this->display('admin/personal.t.html');
    }

    function action_set_env() {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_showTop();
        }
        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;
        // 地域コード
        $_country_key = $this->request->get("country_key");
        if ($_country_key !== "") {
            $country_list = $this->get_country_list();
            // 改ざんされた場合の対応
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
                $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    setcookie("country", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
                    $this->session->set("country_key", $country_row["country_key"]);
                }
            }
        }
        // 言語コード
        $lang = $this->request->get("lang");
        if ($lang !== "") {
            setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $this->session->set("lang", $lang);
        }
        // タイムゾーン
        $time_zone = $this->request->get("time_zone");
        if (is_numeric($time_zone)) {
            setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $this->session->set("time_zone", $time_zone);
        }
        header("Location: index.php");
    }

    function action_change_user_mode() {
      $request = $this->request->getAll();
      $user_info = $this->session->get("user_info");
        $this->logger2->info($request["service_mode"]);
      if($user_info["use_sales"] == 1) {
        if ($request["service_mode"] == "meeting") { $this->session->set("service_mode","sales"); }
        elseif($request["service_mode"] == "sales") { $this->session->set("service_mode","meeting"); }
      } else {
        //avoid url send request
        $this->session->set("service_mode",null);
      }
      header("Location: index.php");
    }

	function action_showtop() {
		$flag = ( $this->obj_N2MY_Account->checkChangeVersion( $this->session->get( 'user_info' ) ) );
		$this->logger->trace( __FUNCTION__ . "#start", __FILE__, __LINE__ );
		$this->template->assign( "flag", $flag );

		// V5宣伝バナー描画判定
		if ( $this->config->get( 'N2MY', 'display_v5_info', false ) ) {
			$user_info = $this->session->get('user_info');
			$ignore_v5_info_user_ids = explode( ',', $this->config->get( 'N2MY', 'ignore_v5_info_user_ids' ) );
			$is_display_v5_info = !in_array( $user_info['user_id'], $ignore_v5_info_user_ids );
			$this->template->assign( 'display_v5_info', $is_display_v5_info );
		}

		$this->display( 'admin/mainmenu.t.html' );
		$this->logger->trace( __FUNCTION__ . "#end", __FILE__, __LINE__ );
	}

    function action_version_change(){
      $this->display('admin/versionup.t.html');
    }

    function action_version_change_complete(){

      // バージョンアップ
      if($this->obj_N2MY_Account->changeVersion($this->session->get('user_info'))){
        // セッションのバージョン情報も書き換え
        $user_info = $this->session->get('user_info');
        $user_info["meeting_version"] = "4.7.0.0";
        $this->session->set('user_info',$user_info);
        $this->display('admin/versionup_complete.t.html');
      }else{
         header("Location: index.php");
      }
    }

    //　「利用料金の照会」
    function action_charge(){
        $this->display('admin/charge/index.t.html');
    }

    /**
     * 設定　→　会議室設定　から　「戻る」ボタンの遷移先
     */
    function action_admin_security() {
        $this->display('admin/security/index.t.html');
    }

    # 現在の契約状況を表示
    function action_room_plan($message = ""){
    	$user_info   = $this->session->get('user_info');
        $service_mode = $this->session->get( 'service_mode' );

        // 会議室数が多いとレンダリング時にブラウザ負荷が大きくなるのでページングさせる
        $room_count = $this->obj_N2MY_Account->getRoomCount( $user_info['user_key'], $service_mode );
        $is_display_room_sort = true;
        if( $room_count <= MAX_ADD_ROOM ){
        	$rooms = parent::get_room_list(true, true);
        }else{
        	require_once 'classes/dbi/room.dbi.php';
        	$obj_room = new RoomTable( parent::get_dsn() );
        	$where = sprintf('user_key = %d AND is_one_time_meeting = 0 AND room_status = 1 AND use_sales_option = %d', $user_info['user_key'], $service_mode == 'sales');
        	$sort = array( 'room_sort' => 'ASC', 'room_key' => 'ASC' );

        	// 会議室名での検索
        	// mb_trim
        	$room_name = preg_replace( '/(^\s+)|(\s+$)/us', '', $this->request->get( 'room_name' ) );
        	$this->template->assign( 'query_room_name', $room_name );
        	if ( $room_name !== '' ) {
        		$is_display_room_sort = false;
        		$where .= sprintf(' AND room_name LIKE "%%%s%%"', mysql_real_escape_string($room_name));
        		$room_count = $obj_room->numRows($where);
        	}
        	// １ページ当たりの上限描画部屋数
        	$limit = $this->request->get( 'page_cnt', 10 );
        	$page = $this->request->get( 'page', 1 );
        	if ( $room_count < $limit * ($page - 1) ) $page = 1;
        	$pager = parent::setPager( $limit, $page, $room_count );
        	$this->logger2->debug($pager);
        	$this->template->assign( 'pager', $pager );

        	// 会議室情報の取得
        	$rooms = array();
        	$room_keys = $obj_room->getCol( $where, 'room_key', $sort, $limit, $limit * ($page - 1) );
        	foreach ( $room_keys as $room_key ) {
        		$rooms[$room_key] = $this->obj_N2MY_Account->getRoomInfo( $room_key );
        		// オプション情報の展開
        		$rooms[$room_key]['options'] = $this->obj_N2MY_Account->getRoomOptionList( $room_key );
        	}
        }
        $this->template->assign('is_display_room_sort', $is_display_room_sort);

        $document_ext = $this->config->getAll('DOCUMENT_FILES');
        $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');

        //4.9.9.8 暫定対応 VMTG-3480 管理者ページ＞会議室設定トップに暫定アラート表示
        $is_alert_use_active_speaker = false;
        $active_speaker_banner_show_flag = false;
        foreach ($rooms as $key => $room) {
        	if(!$room['option']['whiteboard']){
        	    $active_speaker_banner_show_flag = true;
        	}
        	if( ! $room['options']['whiteboard'] || $room['room_info']['max_audience_seat'] != 0){
        		$is_alert_use_active_speaker = true;
        	}

            $filetype = array();
            // ホワイトボードのファイルタイプ
            if ($room["room_info"]["whiteboard_filetype"]) {
                $whiteboard_filetype = unserialize($room["room_info"]["whiteboard_filetype"]);
                if ($whiteboard_filetype["document"]) {
                    foreach($whiteboard_filetype["document"] as $val) {
                        foreach($document_ext as $type => $extensions) {
                            $_extensions = split(",", $extensions);
                            // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                            if (in_array($val, $_extensions) || $val == $type) {
                                $filetype[] = $type;
                            }
                        }
                    }
                }
                if ($whiteboard_filetype["image"]) {
                    foreach($whiteboard_filetype["image"] as $val) {
                        foreach($image_ext as $type => $extensions) {
                            $_extensions = split(",", $extensions);
                            // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                            if (in_array($val, $_extensions) || $val == $type) {
                                $filetype[] = $type;
                            }
                        }
                    }
                }
            // 初期化
            } else {
                $filetype = array_keys($document_ext + $image_ext);
            }
            // 資料のタイプ
            if ($rooms[$key]["room_info"]["cabinet_filetype"]) {
                $cabinet_filetype = @unserialize($rooms[$key]["room_info"]["cabinet_filetype"]);
                if (is_array($cabinet_filetype)) {
                    $rooms[$key]["room_info"]["cabinet_filetype"] = join(", ", $cabinet_filetype);
                } else {
                    $rooms[$key]["room_info"]["cabinet_filetype"] = "";;
                }
            }
            $rooms[$key]["room_info"]["whiteboard_filetype"] = join(", ", $filetype);
            // プロトコルのデフォルト
            if (!$rooms[$key]["room_info"]["rtmp_protocol"]) {
                if ($rooms[$key]["options"]["meeting_ssl"]) {
                    $protocol_list = array(
                        "rtmpe:1935",
                        "rtmpe:80",
                        "rtmpe:8080",
                        "rtmps-tls:443",
                        "rtmps:443",
                        "rtmpte:80",
                        "rtmpte:8080",
                        );
                } else {
                    $protocol_list = array(
                        "rtmp:1935",
                        "rtmp:80",
                        "rtmp:8080",
                        "rtmps-tls:443",
                        "rtmps:443",
                        "rtmpt:80",
                        "rtmpt:8080",
                        );
                }
                $rooms[$key]["room_info"]["rtmp_protocol"] = join(", ", $protocol_list);
            } else {
                $rooms[$key]["room_info"]["rtmp_protocol"] = join(", ", split(",", $rooms[$key]["room_info"]["rtmp_protocol"]));
            }
            if (!$rooms[$key]["room_info"]["transceiver_number"]) $rooms[$key]["room_info"]["transceiver_number"] = 11;
            //利用帯域追加
            if($room["room_info"]["use_extend_bandwidth"] === null) {
              require_once("classes/dbi/room_plan.dbi.php");
              $obj_RoomPlan = new RoomPlanTable($this->get_dsn());
              $room_plan_key = $obj_RoomPlan->getOne("room_plan_status=1 and room_key='".$room["room_info"]["room_key"]."'", "service_key");
              $this->logger2->debug($room_plan_key);
              if($room_plan_key) {
                require_once("classes/dbi/service.dbi.php");
                $obj_Service = new ServiceTable($this->get_auth_dsn());
                $room_plan = $obj_Service->getRow("service_key=".$room_plan_key);
                if($room_plan["bandwidth_up_flg"]) {
                  if($room_plan["max_user_bandwidth"] == $room["room_info"]["max_user_bandwidth"])
                    $rooms[$key]["room_info"]["use_extend_bandwidth"] = 1;
                  else
                    $rooms[$key]["room_info"]["use_extend_bandwidth"] = 0;
                } else {
                  //非表示部屋
                  $rooms[$key]["room_info"]["use_extend_bandwidth"] = -1;
                }
              } else {
                //プランがない部屋
                $rooms[$key]["room_info"]["use_extend_bandwidth"] = -1;
              }
            }
            if ($rooms[$key]["options"]["video_conference_number"] > 0) {
                require_once("classes/dbi/ives_setting.dbi.php");
                require_once("classes/mgm/dbi/mcu_server.dbi.php");
                $obj_ivesSetting = new IvesSettingTable($this->get_dsn());
                $obj_mcuServer = new McuServerTable(N2MY_MDB_DSN);
                $where = "room_key = '" . addslashes($rooms[$key]["room_info"]["room_key"]) . "' AND is_deleted = 0";
                $ives_setting = $obj_ivesSetting->getRow($where);
                $mcuServer = $obj_mcuServer->getRow(sprintf("server_key = '%s'", $ives_setting["mcu_server_key"]));
                $address = sprintf("%s@%s", $ives_setting["ives_did"], $mcuServer["server_address"]);
                $ives_setting["video_conference_address"] = $address;
                $rooms[$key]["ives_setting"] = $ives_setting;
            }
        }
        // 回線速度一覧
        $net_speed_list = $this->get_message("NET_SPEED_LIST");
        foreach($net_speed_list as $key => $val) {
            $net_speed_list[$key] = substr($val, strpos($val, ":") + 1);
        }
        $has_bandwidth_extend = 0;
        foreach ($rooms as $room) {
          if($room["room_info"]["use_extend_bandwidth"] >= 0)
            $has_bandwidth_extend = 1;
        }

        //部屋追加対応
        if($user_info["account_plan"] == "one_id_host"){
            $this->template->assign("add_room_enable", count($rooms) < $user_info["max_room_count"] ? 1 : 0);
        }else{
            $this->template->assign("add_room_enable", count($rooms) < MAX_ADD_ROOM ? 1 : 0);
        }
        $this->template->assign(array("net_speed_list" => $net_speed_list,
                                      'data'           => $rooms,
                                      'message'        => $message,
                                      'is_trial'       => $user_info['user_status'] == 2,
                                      'active_speaker_banner_show_flag' => $active_speaker_banner_show_flag,
                                      'has_bandwidth_extend' => $has_bandwidth_extend));

        $this->template->assign("is_alert_use_active_speaker", $is_alert_use_active_speaker);
        $this->display('admin/room/index.t.html');
    }

    //部屋のソートを変更
    public function action_room_up(){
        $obj_Room = new RoomTable($this->get_dsn());
        $user_info = $this->session->get('user_info');

        $obj_Room->up_sort($user_info['user_key'], $_REQUEST['room_key']);
        $this->action_room_plan();
    }

    //部屋のソートを変更
    public function action_room_down(){
        $obj_Room = new RoomTable($this->get_dsn());
        $user_info = $this->session->get('user_info');

        $obj_Room->down_sort($user_info['user_key'],$_REQUEST['room_key']);
        $this->action_room_plan();
    }


    function action_edit_room() {
        $obj_Room  = new RoomTable($this->get_dsn());
        $user_info = $this->session->get('user_info');
        $room_info = $obj_Room->get_detail($this->request->get("room_key"));
        if($room_info["user_key"] != $user_info['user_key']){
            $this->display("error.t.html");
            exit;
        }
        if (!$room_info["whiteboard_filetype"]) {
            $document_ext = $this->config->getAll('DOCUMENT_FILES');
            $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
            $room_info["whiteboard_filetype"] = serialize(array(
                "document" => array_keys($document_ext),
                "image"    => array_keys($image_ext)));
        }
        if (!$room_info["transceiver_number"]) {
            $room_info["transceiver_number"] = 11;
        }
        $this->session->set("room_info", $room_info, $this->_name_space);
        $this->render_edit_room();
    }

    function render_edit_room() {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $room_info = $this->session->get("room_info", $this->_name_space);
        if (EZValidator::isError($this->err_obj)) {
            $msg_format = $this->get_message("error");
            $msg        = $this->get_message("ERROR");
            $err_fields = $this->err_obj->error_fields();
            foreach ($err_fields as $key) {
                $type = $this->err_obj->get_error_type($key);
                $_msg = sprintf($msg[$type], $this->err_obj->get_error_rule($key, $type));
                $error[$key] = sprintf($msg_format, $_msg);
            }
            $this->logger->info(__FUNCTION__."#error", __FILE__, __LINE__, $error);
            $this->template->assign("error_info", $error);
        }

        if($this->get_room_info($room_info["room_key"])){
            $room_info_detail = $this->get_room_info($room_info["room_key"]);
        }else{
            $room_info_detail = $this->obj_N2MY_Account->getOwnRoomByRoomKey( $room_info["room_key"], $user_info["user_key"]);
        }

        if ($room_info_detail["options"]["meeting_ssl"]) {
            $protocol_list = array(
                "rtmpe:1935"  => "rtmpe:1935",
                "rtmpe:80"    => "rtmpe:80",
                "rtmpe:8080"  => "rtmpe:8080",
                "rtmps-tls:443"   => "rtmps-tls:443",
                "rtmps:443"   => "rtmps:443",
                "rtmpte:80"   => "rtmpte:80",
                "rtmpte:8080" => "rtmpte:8080",
                );
        } else {
            $protocol_list = array(
                "rtmp:1935"   => "rtmp:1935",
                "rtmp:80"     => "rtmp:80",
                "rtmp:8080"   => "rtmp:8080",
                "rtmps-tls:443"   => "rtmps-tls:443",
                "rtmps:443"   => "rtmps:443",
                "rtmpt:80"    => "rtmpt:80",
                "rtmpt:8080"  => "rtmpt:8080",
                );
        }
        $protocol_deny_list  = array();
        $protocol_allow_list = array();
        $list                = array();

        if (!$room_info["rtmp_protocol"]) {
            $protocol_allow_list = $protocol_list;
        } else {
            $list = split(",", $room_info["rtmp_protocol"]);
            foreach($list as $protocol) {
                if (array_key_exists($protocol, $protocol_list)) {
                    $protocol_allow_list[$protocol] = $protocol_list[$protocol];
                }
            }
        }
        foreach($protocol_list as $key => $value) {
            if (!array_key_exists($key, $protocol_allow_list)) {
                $protocol_deny_list[$key] = $value;
            }
        }
        if ($room_info["cabinet_filetype"]) {
            $room_info["cabinet_filetype"] = unserialize($room_info["cabinet_filetype"]);
        }
        $document_ext = $this->config->getAll('DOCUMENT_FILES');
        $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
        if ($room_info["whiteboard_filetype"]) {
            $filetype = unserialize($room_info["whiteboard_filetype"]);
            $this->logger2->debug($filetype);
            $room_info["whiteboard_filetype"] = array();
            if ($filetype["document"]) {
                foreach($filetype["document"] as $val) {
                    foreach($document_ext as $type => $extensions) {
                        $_extensions = split(",", $extensions);
                        // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                        if (in_array($val, $_extensions) || $val == $type) {
                            $room_info["whiteboard_filetype"][$type] = 1;
                        }
                    }
                }
            }
            if ($filetype["image"]) {
                foreach($filetype["image"] as $val) {
                    foreach($image_ext as $type => $extensions) {
                        $_extensions = split(",", $extensions);
                        // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                        if (in_array($val, $_extensions) || $val == $type) {
                            $room_info["whiteboard_filetype"][$type] = 1;
                        }
                    }
                }
            }
        }
        $net_speed_list = $this->get_message("NET_SPEED_LIST");
        foreach ($net_speed_list as $key => $val) {
            $net_speed_list[$key] = substr($val, strpos($val, ":") + 1);
        }

        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            try {
                $service_option  = new OrderedServiceOptionTable($this->get_dsn());
                $enable_teleconf = $service_option->enableOnRoom(23, $room_info["room_key"]); // 23 = teleconf
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                exit;
            }
            $user_info = $this->session->get("user_info");
            $this->template->assign(array("enable_teleconf" => $enable_teleconf,
                                          "is_trial"        => $user_info['user_status'] == 2,
                                          "service_name"    => $user_info['service_name']));
        }

		 if (!$this->config->get("IGNORE_MENU", "video_conference")) {
	     	require_once("classes/dbi/ives_setting.dbi.php");
            $obj_ivesSetting = new IvesSettingTable($this->get_dsn());
            $where_ives = sprintf('room_key = "%s" AND is_deleted = 0', $room_info['room_key']);
            $ives_setting_info = $obj_ivesSetting->getRowsAssoc($where_ives);
			$this->template->assign('ives_setting', $ives_setting_info[0]);
        }

        if($room_info_detail["options"]){
            $room_info["options"] = $room_info_detail["options"];
        }else{
            $room_info["options"] = $room_info_detail[$room_info["room_key"]]["options"];
        }
        // 初期値設定
        if($room_info["active_speaker_mode_user_count"] == 0){
            $room_info["active_speaker_mode_user_count"] = 5;
        }
        // if($room_info["active_speaker_mode_speaker_count"] == 0){
            // $room_info["active_speaker_mode_speaker_count"] = 2;
        // }
        $this->template->assign(array("image_files"         => $image_ext,
                                      "document_files"      => $document_ext,
                                      "protocol_list"       => $protocol_list,
                                      "net_speed_list"      => $net_speed_list,
                                      "protocol_deny_list"  => $protocol_deny_list,
                                      "protocol_allow_list" => $protocol_allow_list,
                                      "room_info"           => $room_info));
        //V-CUBE One対応：会議室とメンバーの分類機能
        $user_info = $this->session->get("user_info");
        $terminal_count = 0;
        if($this->check_one_account() && ($this->session->get("service_mode") == "meeting")){
            require_once("classes/dbi/member.dbi.php");
            $member_list_allow = array();
            $member_list_deny  = array();
            $obj_member = new MemberTable($this->get_dsn());
            if($room_info["has_member_whitelist"] == 0){
                $where = "member_status = 0 AND user_key = " . $user_info["user_key"];
                $member_list_deny = $obj_member->getRowsAssoc($where);
            }elseif($room_info["has_member_whitelist"] == 1){
                $obj_member_room_whitelist = new N2MY_DB($this->get_dsn(), "member_room_whitelist");
                $sort_rule = array(
                    "member_key" => "ASC"
                );
                $where = "whitelist_status = 1 AND room_key = '" . mysql_real_escape_string($room_info["room_key"]) . "'";
                $room_whitelist = $obj_member_room_whitelist->getRowsAssoc($where, $sort_rule);
                if($room_whitelist){
                    foreach ($room_whitelist as $whitelist) {
                        $where = sprintf("member_status = 0 AND member_key = %s AND user_key = %s", $whitelist["member_key"], $user_info["user_key"]);
                        $member = $obj_member->getRow($where);
                        if($member){
                            $member_list_allow[$member["member_key"]] = $member;
                        }
                    }
                }
                $where = "member_status = 0 AND user_key = " . $user_info["user_key"];
                if($member_list_allow){
                    $where .= sprintf(" AND member_key NOT IN (%s)", implode(",", array_keys($member_list_allow)));
                }
                $member_list_deny = $obj_member->getRowsAssoc($where);
            }
            $this->template->assign("member_whitelist_deny", $member_list_deny);
            $this->template->assign("member_whitelist_allow", $member_list_allow);
        } else if ($user_info["meeting_version"] == "" && $user_info["account_model"] != "member") {
            //terminalでの利用部屋かチェック
            require_once("classes/dbi/member_room_relation.dbi.php");
            $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
            $where = "room_key = '".mysql_real_escape_string($room_info["room_key"])."'";
            $terminal_count = $objRoomRelation->numRows($where);
        }
        $this->template->assign("terminal_count", $terminal_count);
        // 部屋のプラン確認
        require_once('classes/dbi/room_plan.dbi.php');
        // 資料共有プランだったら録画機能欄を表示させない(今後増えるかも)
        $rec_disable_flag = 0;
        if($room_info["options"]["whiteboard"]){
          $rec_disable_flag = 1;
        }
        $this->template->assign("rec_disable_flag",$rec_disable_flag);
        $this->display('admin/room/edit.t.html');
    }

    function action_delete_room() {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_list();
        }
    	$room_key = $this->request->get("room_key");
    	$user_info = $this->session->get("user_info");
    	$room_count = $this->obj_N2MY_Account->getRoomCount($user_info["user_key"], $this->session->get("service_mode"));
    	if($room_count < 2) {
    		$message = DELETE_ROOM_ERROR_MIN;
    		$this->action_room_plan($message);
    		return false;
    	}
    	require_once('classes/N2MY_Reservation.class.php');
    	$obj_Reservation = new N2MY_Reservation($this->get_dsn());
    	//現在進行形の予約会議・未来の予約が存在しないかを検索
    	$reservation_list = $obj_Reservation->getList($room_key, array("status" => "valid"));
    	if(count($reservation_list) > 0) {
//     		$message = array(
//     				"title" => "会議室削除",
//     				"text" => "この会議室に会議予約有りますので、削除できない。",
//     				"back_url" => "/services/admin/?action_room_plan",
//     				"back_url_label" => "戻す");
//     	    $this->template->assign("message", $message);
//     	    $this->display('admin/message.t.html');
//
    		$message = DELETE_ROOM_ERROR_CONTENT;
    		$this->action_room_plan($message);
    		return false;
    	}
    	//開催中会議を紐付く部屋を削除できない
    	require_once("classes/N2MY_Meeting.class.php");
    	require_once("classes/dbi/meeting.dbi.php");
    	require_once("classes/core/dbi/Participant.dbi.php");
    	$obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
    	$obj_Meeting = new MeetingTable($this->get_dsn());
    	$obj_Participant = new DBI_Participant($this->get_dsn());
    	$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_key);
    	$last_meeting_key = $obj_Meeting->findMeetingKeyByMeetingTicket($meeting_info["meeting_key"]);
    	$participant_count = count($obj_Participant->getList($last_meeting_key,true));
    	$this->logger2->info($meeting_info);
    	if($meeting_info["last_meeting_status"] != 0 && $participant_count > 0) {
    		$message = DELETE_ROOM_ERROR_CONTENT;
    		$this->action_room_plan($message);
    		return false;
    	}
    	require_once("classes/dbi/room.dbi.php");
    	$obj_Room = new RoomTable($this->get_dsn());
    	$where = "room_key = '".addslashes($room_key)."'";
    	$room_data = array (
    			"room_status" => 0,
    			"room_deletetime" => date("Y-m-d H:i:s"),
    	);
    	$obj_Room->update($room_data, $where);
    	$plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
    	$plan_where = "room_key='".$room_key."'".
    			" AND room_plan_status = 1";
    	$plan_data = array (
    			"room_plan_status" => 0,
    			"room_plan_updatetime" => date("Y-m-d H:i:s"),
    	);
    	$plan_db->update($plan_data, $plan_where);
    	$relation_db = new N2MY_DB($this->get_auth_dsn(), "relation");
    	$relation_where = "relation_key = '".addslashes($room_key)."'";
    	$relation_data = array(
    			"status" => "0",
    	);
    	$relation_db->update($relation_data, $relation_where);
    	$objOrderedServiceOption = new N2MY_DB( $this->get_dsn(), "ordered_service_option" );
    	$where_telconf =  "room_key='".$room_key."'".
    			" AND ordered_service_option_status = 1 AND service_option_key = 23";
    	if($objOrderedServiceOption->numRows($where_telconf)) {
    		require_once("classes/dbi/pgi_setting.dbi.php");
    		require_once("classes/dbi/pgi_rate.dbi.php");
    		$pgiSettingTable = new PGiSettingTable($this->get_dsn());
    		$pgiRateTable    = new PGiRateTable($this->get_dsn());
    		$where_pgiSettingKey = "room_key='".$room_key."' AND is_deleted=0";
    		$pgiSettingKey = $pgiSettingTable->getOne($where_pgiSettingKey, "pgi_setting_key");
    		$where_pgiSetting = "room_key='".$room_key."'";
    		$data = array(
    				"is_deleted" => 1,
    				"updatetime" => date("Y-m-d H:i:s"),
    		);
    		$pgiSettingTable->update($data, $where_pgiSetting);
    		$where_pgiRates = "pgi_setting_key='".$pgiSettingKey."'";
    		$pgiRateTable->update($data, $where_pgiRates);
    	}
    	$option_where = "room_key='".$room_key."'".
    			" AND ordered_service_option_status = 1";
    	$option_data = array (
    			"ordered_service_option_status" => 0,
    			"ordered_service_option_deletetime" => date("Y-m-d H:i:s"),
    	);
    	$result = $objOrderedServiceOption->update( $option_data, $option_where );
    	//delete mcu setting
    	$ivesSettingTable = new IvesSettingTable($this->get_dsn());
    	$where_ivesSetting = "is_deleted = 0 AND room_key='".$room_key."'";
    	$delete_ives_setting_data = array(
    			"is_deleted" => 1,
    			"updatetime" => date("Y-m-d H:i:s"),
    	);
    	$ivesSettingTable->update($delete_ives_setting_data, $where_ivesSetting);
        // V-CUBE One: delete all whitelist relation
        // if(($user_info["account_plan"] == "one") && ($this->session->get("service_mode") == "meeting")){
            // $obj_member_room_whitelist = new N2MY_DB($this->get_dsn(), "member_room_whitelist");
            // $whitelist_where = "whitelist_status = 1 AND room_key = '" . mysql_real_escape_string($room_key) ."'";
            // if($obj_member_room_whitelist->numRows($whitelist_where) >= 1){
                // $whitelist_data_disable = array(
                    // "whitelist_status" => 0,
                    // "update_datetime"  => date("Y-m-d H:i:s")
                // );
                // $obj_member_room_whitelist->update($whitelist_data_disable, $whitelist_where);
            // }
        // }
    	$this->action_room_plan();
    	return true;
    }

    function action_edit(){
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_room_plan();
        }
        $user_info = $this->session->get( "user_info" );
        $request = $this->request->getAll();
        $obj_Room = new RoomTable( $this->get_dsn() );
        $room_info = $obj_Room->get_detail($this->request->get("room_key"));
        if($room_info["user_key"] == $user_info['user_key']){
            $room_key = $room_info["room_key"];
        }else{
            $this->display("error.t.html");
            exit;
        }

        if ($request["cabinet_filetype"]) {
            foreach ($request["cabinet_filetype"] as $extension) {
                $extensions[] = strtolower($extension);
            }
            $request["cabinet_filetype"] = $extensions;
        }
        $data = array(
            "room_key"                    => $request["room_key"],
            "room_name"                   => $request["room_name"],
            "cabinet"                     => $request["cabinet"],
            "cabinet_size"                => $request["cabinet_size"],
            "cabinet_filetype"            => serialize($request["cabinet_filetype"]),
            "mfp"                         => $request["mfp"],
            "default_layout_4to3"         => $request["default_layout_4to3"],
            "default_layout_16to9"        => $request["default_layout_16to9"],
            "rtmp_protocol"               => $request["rtmp_protocol"],
            "whiteboard_page"             => $request["whiteboard_page"],
            "whiteboard_size"             => $request["whiteboard_size"],
            "whiteboard_filetype"         => serialize($request["whiteboard_filetype"]),
            "default_microphone_mute"     => $request["default_microphone_mute"],
            "default_camera_mute"         => $request["default_camera_mute"],
            "is_auto_transceiver"         => $request["is_auto_transceiver"],
            "default_h264_use_flg"        => $request["default_h264_use_flg"],
            "default_agc_use_flg"         => $request["default_agc_use_flg"],
            "transceiver_number"          => $request["transceiver_number"],
            "is_auto_voice_priority"      => $request["is_auto_voice_priority"],
            "is_netspeed_check"           => $request["is_netspeed_check"],
            "is_wb_no"                    => $request["is_wb_no"],
            "is_device_skip"              => $request["is_device_skip"],
            "is_participant_notifier"     => $request["is_participant_notifier"],
            "audience_chat_flg"           => $request["audience_chat_flg"],
            "disable_rec_flg"             => $request["disable_rec_flg"],
            "is_personal_wb"              => $request["is_personal_wb"],
            "is_convert_wb_to_pdf"        => $request["is_convert_wb_to_pdf"],
            "is_convert_personal_wb_to_pdf"=>$request["is_convert_personal_wb_to_pdf"],
        	'is_invite_button'            => $request['is_invite_button'],
            "mobile_4x4_layout"           => $request["mobile_4x4_layout"],
            "rec_gw_convert_type"         => $request["rec_gw_convert_type"] ? $request["rec_gw_convert_type"] : "mov",
            "rec_gw_userpage_setting_flg" => isset($request["rec_gw_userpage_setting_flg"]) ? $request["rec_gw_userpage_setting_flg"] : "1",
            "customer_camera_use_flg"     => isset($request["customer_camera_use_flg"]) ? $request["customer_camera_use_flg"] : "1",
            "customer_camera_status"      => isset($request["customer_camera_status"]) ? $request["customer_camera_status"] : "1",
            "customer_sharing_button_hide_status" => isset($request["customer_sharing_button_hide_status"]) ? $request["customer_sharing_button_hide_status"] : "1",
            "wb_allow_flg"                => isset($request["wb_allow_flg"]) ? $request["wb_allow_flg"] : "0",
            "chat_allow_flg"              => isset($request["chat_allow_flg"]) ? $request["chat_allow_flg"] : "0",
            "print_allow_flg"             => isset($request["print_allow_flg"]) ? $request["print_allow_flg"] : "0",
            "active_speaker_mode_use_flg"     => isset($request["active_speaker_mode_use_flg"]) ? $request["active_speaker_mode_use_flg"] : "0",
            // "active_speaker_mode_speaker_count"  => $request["active_speaker_mode_speaker_count"],
            "active_speaker_mode_user_count"  => $request["active_speaker_mode_user_count"],
            // "active_speaker_default_layout_4to3" => $request["active_speaker_mode_4to3"],
            "active_speaker_default_layout_16to9"=> $request["active_speaker_mode_16to9"],
            "prohibit_extend_meeting_flg" => isset($request["prohibit_extend_meeting_flg"]) ? $request["prohibit_extend_meeting_flg"] : "0",
            );
        $service_option  = new OrderedServiceOptionTable($this->get_dsn());
        $enable_teleconf = $service_option->enableOnRoom(23, $room_key); // 23 = teleconf

        if ($user_info["meeting_version"] != 0 && !$this->config->get("IGNORE_MENU", "teleconference") && ($enable_teleconf || $user_info["promotion_flg"]) ) {
            if ($request['use_teleconf']) {
                $data['use_teleconf']        = 1;
                $data['use_pgi_dialin']      = $request['use_pgi_dialin']      ? 1: 0;
                $data['use_pgi_dialin_free'] = $request['use_pgi_dialin_free'] ? 1: 0;
                $data['use_pgi_dialout']     = $request['use_pgi_dialout']     ? 1: 0;
                $data['use_pgi_dialin_lo_call']     = $request['use_pgi_dialin_lo_call']     ? 1: 0;
            } else {
                $data['use_teleconf']        = 0;
                $data['use_pgi_dialin']      = 0;
                $data['use_pgi_dialin_free'] = 0;
                $data['use_pgi_dialout']     = 0;
                $data['use_pgi_dialin_lo_call']     = 0;
            }
        }
        $rules = array("rtmp_protocol" => array("required" => true,),
                       "room_name"     => array("required" => true,
                                                "maxlen"   => 20),
                       "cabinet"       => array("required" => true,
                                                "allow"    => array("0", "1")),
                       "mfp"           => array("required" => true,
                                                "allow"    => array("all", "select", "deny")),);
        $this->err_obj = $obj_Room->check($data, $rules);
        if ($request["cabinet_filetype"]) {
            foreach ($request["cabinet_filetype"] as $ext) {
                if (ereg("[^0-9a-z]", $ext)) {
                    $this->err_obj->set_error("cabinet_filetype", "alnum", $ext);
                    break;
                }
            }
        }
        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            if ($data['use_teleconf'] && !$data['use_pgi_dialin']
             && !$data['use_pgi_dialin_free'] && !$data['use_pgi_dialin_lo_call'] && !$data['use_pgi_dialout']) {
                $this->err_obj->set_error("teleconf", "minitem", 1);
            }
        }
        if (!$this->config->get("IGNORE_MENU", "video_conference")) {
            require_once("classes/mcu/resolve/Resolver.php");
            if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($room_key))) {
               require_once("classes/dbi/ives_setting.dbi.php");
                $obj_ivesSetting = new IvesSettingTable($this->get_dsn());
                $ives_data = array("mcu_hd_flg" => $request["mcu_hd_flg"] ? 1: 0);
                $where_ives = "room_key ='".addslashes($request["room_key"])."'".
                              " AND is_deleted = 0";
                $obj_ivesSetting->update($ives_data, $where_ives);
            }
        }
        // エラー
        if (EZValidator::isError($this->err_obj)) {
            $this->session->set("room_info", $data, $this->_name_space);
            return $this->render_edit_room();
        }
        // V-CUBE One対応：会議室とメンバー紐付け機能追加（管理者＞会議室設定）
        if($this->check_one_account() && ($this->session->get("service_mode") == "meeting")){
            $data["has_member_whitelist"] = $request["has_member_whitelist"];
            $obj_member_room_whitelist = new N2MY_DB($this->get_dsn(), "member_room_whitelist");
            $where = "whitelist_status = 1 AND room_key = '" . mysql_real_escape_string($room_key) ."'";
            if($obj_member_room_whitelist->numRows($where) >= 1){
                $whitelist_data  = array(
                    "whitelist_status" => 0,
                    "update_datetime"  => date("Y-m-d H:i:s")
                );
                $obj_member_room_whitelist->update($whitelist_data, $where);
            }
            if($request["has_member_whitelist"] == 1 && $request["whitelist_members"]){
                $whitelist_member_keys = explode(",", $request["whitelist_members"]);
                foreach ($whitelist_member_keys as $key) {
                    $whitelist_data = array(
                        "member_key"       => $key,
                        "room_key"         => $room_key,
                        "whitelist_status" => 1,
                        "create_datetime"  => date("Y-m-d H:i:s")
                    );
                    $obj_member_room_whitelist->add($whitelist_data);
                }
            }
            $this->logger2->info("-------Process Member Whitelist End------");
        }
        // 部屋情報更新
        if ($room_key && $user_info["user_key"]) {
            $this->logger2->debug($data);
            $obj_Room->update($data, sprintf("room_key='%s' AND user_key='%s'", $request["room_key"], $user_info["user_key"]));
        }
        $this->add_operation_log('setup_room', array( 'room_key' => $request["room_key"]));
        $this->logger2->info($data);
        return $this->action_room_plan();
    }

    function action_reset() {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_showTop();
        }
        $user_info = $this->session->get( "user_info" );
        $request   = $this->request->getAll();
        $obj_Room  = new RoomTable( $this->get_dsn() );
        $data = array(
            "cabinet"                     => "1",
            "cabinet_size"                => 20,
            "cabinet_filetype"            => "",
            "mfp"                         => "all",
            "default_layout_4to3"         => "normal",
            "default_layout_16to9"        => "small",
            "rtmp_protocol"               => "",
            "whiteboard_page"             => 100,
            "whiteboard_size"             => 20,
            "whiteboard_filetype"         => "",
            "default_microphone_mute"     => "off",
            "default_camera_mute"         => "off",
            "default_h264_use_flg"        => 0,
            "default_agc_use_flg"         => 1,
            "is_auto_transceiver"         => 0,
            "transceiver_number"          => 11,
            "is_auto_voice_priority"      => 0,
            "is_netspeed_check"           => "",
            "is_wb_no"                    => 1,
            "is_device_skip"              => 0,
            "is_participant_notifier"     => 1,
            "audience_chat_flg"           => 0,
            "disable_rec_flg"             => 0,
            "is_personal_wb"              => 1,
            "is_convert_wb_to_pdf"        => 1,
            "is_convert_personal_wb_to_pdf" => 1,
        	'is_invite_button'            => 1,
            "mobile_4x4_layout"           => 0,
            "rec_gw_convert_type"         => "mov",
            "has_member_whitelist"        => 0,
            "customer_camera_use_flg"     => 1,
            "customer_camera_status"      => 1,
            "customer_sharing_button_hide_status" => 1,
            "wb_allow_flg"                => 0,
            "chat_allow_flg"              => 0,
            "print_allow_flg"             => 0,
            "active_speaker_mode_use_flg"     => 0,
            "active_speaker_mode_user_count"  => 2,
            // "active_speaker_default_layout_4to3"  => "normal",
            "active_speaker_default_layout_16to9" => "small",
            "prohibit_extend_meeting_flg" => 0,
            );

		if($this->session->get("service_mode") == "sales" || $user_info["meeting_version"] == null) {
			if ($user_info["account_model"] != "member") {
				//terminalでの利用部屋かチェック
				require_once("classes/dbi/member_room_relation.dbi.php");
				$objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
				$where = "room_key = '".mysql_real_escape_string($room_info["room_key"])."'";
				$terminal_count = $objRoomRelation->numRows($where);
				if($terminal_count == 0){
					$data["default_layout_16to9"] = "normal";
				}
			}else{
        		$data["default_layout_16to9"] = "normal";
			}
        }


        $service_option  = new OrderedServiceOptionTable($this->get_dsn());
        $enable_teleconf = $service_option->enableOnRoom(23, $request["room_key"]); // 23 = teleconf

        if ($enable_teleconf || $user_info["promotion_flg"]) {
            $data += array(
                "use_teleconf"              => 1,
                "use_pgi_dialin"            => 1,
                "use_pgi_dialin_free"       => 1,
                "use_pgi_dialout"           => 1,
                "use_pgi_dialin_lo_call"    => 1
             );
        }

        $obj_Room->update( $data, sprintf( "room_key='%s' AND user_key='%s'", $request["room_key"], $user_info["user_key"] ) );
        if (!$this->config->get("IGNORE_MENU", "video_conference")) {
            require_once("classes/mcu/resolve/Resolver.php");
            if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($request["room_key"]))) {
               require_once("classes/dbi/ives_setting.dbi.php");
                $obj_ivesSetting = new IvesSettingTable($this->get_dsn());
                $ives_data = array("mcu_hd_flg" => $request["mcu_hd_flg"] ? 1: 0);
                $where_ives = "room_key ='".addslashes()."'".
                              " AND is_deleted = 1";
                $obj_ivesSetting->update($ives_data, $where_ives);
            }
         }
        if($this->check_one_account() && $this->session->get("service_mode") == "meeting"){
            $obj_member_room_whitelist = new N2MY_DB($this->get_dsn(), "member_room_whitelist");
            $where = "whitelist_status = 1 AND room_key = '" . mysql_real_escape_string($request["room_key"]) ."'";
            if($obj_member_room_whitelist->numRows($where) >= 1){
                $whitelist_data  = array(
                    "whitelist_status" => 0,
                    "update_datetime"  => date("Y-m-d H:i:s")
                );
                $obj_member_room_whitelist->update($whitelist_data, $where);
            }
            // $obj_member = new N2MY_DB($this->get_dsn(), "member");
            // $where = "member_status = 0 AND use_sales = 0 AND user_key = " . $user_info["user_key"];
            // $member_list = $obj_member->getCol($where, "member_key");
            // if($member_list){
                // foreach ($member_list as $member_key) {
                    // $whitelist_data = array(
                        // "member_key"       => $member_key,
                        // "room_key"         => $request["room_key"],
                        // "whitelist_status" => 1,
                        // "create_datetime"  => date("Y-m-d H:i:s")
                    // );
                    // $obj_member_room_whitelist->add($whitelist_data);
                // }
            // }
            // $this->logger2->info("-------Process Member Whitelist End------");
        }
        return $this->action_room_plan();
    }

    function action_contract(){
        $admin_info = $this->session->get('user_info');
        foreach ($admin_info as $key => $value) {
            $this->template->assign($key, $value);
        }
        if ($admin_edit_info = $this->session->get('admin_edit_info')) {
            foreach($admin_edit_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin/account/index.t.html');
    }

    function action_option_list(){
        $admin_info = $this->session->get('user_info');
        foreach ($admin_info as $key => $value) {
            $this->template->assign($key, $value);
        }
        if ($admin_edit_info = $this->session->get('admin_edit_info')) {
            foreach($admin_edit_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin/account/option.t.html');
    }

    function action_admin_info(){
        $admin_info = $this->session->get('user_info');
        foreach ($admin_info as $key => $value) {
            $this->template->assign($key, $value);
        }
        if ($admin_edit_info = $this->session->get('admin_edit_info')) {
            foreach($admin_edit_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin/account/account.t.html');
    }

    /**
     * パスワード変更画面
     */
    function action_login_password_form()
    {
        $this->render_login_password_form();
    }

    /**
     * パスワード変更画面表示
     */
    function render_login_password_form($msg = null)
    {
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $this->template->assign("message", $msg);
        $this->template->assign("request", $request);
        $this->display("admin/account/login_password.t.html");
    }

    /**
     * パスワード設定完了画面
     */
    function action_login_password_complete()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_showTop();
        }
        $old_password     = $this->request->get("old_password");
        $new_password     = $this->request->get("new_password");
        $confirm_password = $this->request->get("confirm_password");
        $user_info        = $this->session->get("user_info");
        $user_key         = $user_info["user_key"];
        $msg              = "";

        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $db_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_password"]);
        if ($old_password != $db_password) {
            $msg = ADMIN_PASSWORD_ERROR1;
        } elseif (($new_password != $confirm_password) || (trim($new_password) == "")) {
            $msg .= ADMIN_PASSWORD_ERROR2;
        //ユーザーパスワードをチェック
        } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $new_password)) {
            $msg .= USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$new_password) || preg_match('/^[[:alpha:]]+$/',$new_password)) {
            $msg .= USER_ERROR_PASS_INVALID_02;
        }
        if ($msg) {
            return $this->render_login_password_form($msg);
        }
        $user = new UserTable($this->get_dsn());
        $data = array(
            "user_password" => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $new_password),
            "user_updatetime" => date("Y-m-d H:i:s")
            );
        $where = "user_key = ".$user_key;
        $ret = $user->update($data, $where);
        if (DB::isError($ret)) {
            return $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        }
        $users = $user->getRowsAssoc($where);
        $new_user_info = $users[0];
        $this->session->set("user_info", $new_user_info);
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,array("old" => $user_info, "new" => $new_user_info));
        // 操作ログ
        $this->add_operation_log('change_login_password');
        return $this->render_login_password_complete();
    }

    /**
     * パスワード設定完了画面表示
     */
    function render_login_password_complete()
    {
        $this->display("admin/account/login_password_complete.t.html");
    }

    /**
     * 管理者パスワード変更画面
     */
    function action_admin_password_form()
    {
        $this->render_admin_password_form();
    }

    /**
     * 管理者パスワード変更画面表示
     */
    function render_admin_password_form($msg = null)
    {
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $this->template->assign("message", $msg);
        $this->template->assign("request", $request);
        $this->display("admin/account/admin_password.t.html");
    }

    /**
     * 管理者パスワード設定完了画面
     */
    function action_admin_password_complete()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_showTop();
        }
        $old_password = $this->request->get("old_password");
        $new_password = $this->request->get("new_password");
        $confirm_password = $this->request->get("confirm_password");
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];
        $msg = "";
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $db_admin_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_admin_password"]);
        if ($old_password != $db_admin_password) {
            $msg = ADMIN_PASSWORD_ERROR1;
        } elseif (($new_password != $confirm_password) || (trim($new_password) == "")) {
            $msg .= ADMIN_PASSWORD_ERROR2;
        //ユーザーパスワードをチェック
        } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $new_password)) {
            $msg .= USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$new_password) || preg_match('/^[[:alpha:]]+$/',$new_password)) {
            $msg .= USER_ERROR_PASS_INVALID_02;
        }
        if ($msg) {
            return $this->render_admin_password_form($msg);
        }
        $user = new UserTable($this->get_dsn());
        $data = array(
            "user_admin_password" => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $new_password),
            "user_updatetime" => date("Y-m-d H:i:s")
            );
        $where = "user_key = ".$user_key;
        $ret = $user->update($data, $where);
        if (DB::isError($ret)) {
            return $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        }
        $users = $user->getRowsAssoc($where);
        $new_user_info = $users[0];
        $this->session->set("user_info", $new_user_info);
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,array("old" => $user_info, "new" => $new_user_info));
        // 操作ログ
        $this->add_operation_log('change_admin_password');
        $this->render_admin_password_complete();
    }

    /**
     * 管理者パスワード設定完了画面表示
     */
    function render_admin_password_complete()
    {
        $this->display("admin/account/admin_password_complete.t.html");
    }

    /**
     * 管理者パスワード入力チェック
     */
    function _admin_password_check()
    {

    }

    function action_admin_info_confirm(){

        $admin = new Admin();
        if($message = $admin->checkAdminInfo()){
            $this->action_admin_info($message);
            exit;
        }

        $admin_edit_info = $this->session->get('admin_edit_info');
        $len = strlen($admin_edit_info["user_password"]);
        $this->template->assign("user_password", str_pad("",$len, "*"));
        $len = strlen($admin_edit_info["user_admin_password"]);
        $this->template->assign("user_admin_password", str_pad("",$len, "*"));
        $this->display('admin/account/confirm.t.html');
    }

    function action_admin_info_complete(){

        $admin = new Admin();
        $admin->updateAdminInfo();

        $this->display('admin/account/done.t.html');
    }


    function action_inquiry($message = ""){

        if($message){
            $this->template->assign('message', $message);
        }
        if($inquiry_info = $this->session->get('inquiry_info')){
            foreach($inquiry_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin/ask/index.t.html');
    }

    function action_inquiry_confirm(){

        $admin = new Admin();
        if($message = $admin->checkInquiryInfo()){

            $this->action_inquiry($message);
            exit;
        }

        $inquiry_info = $this->session->get('inquiry_info');
        foreach($inquiry_info as $key => $value){
            $value = htmlspecialchars($value);
            //$value = nl2br($value);
            $this->template->assign($key, $value);
        }

        $this->display('admin/ask/confirm.t.html');
    }

    function action_inquiry_complete(){

        $admin = new Admin();
        $admin->sendInquiry();

        $this->display('admin/ask/done.t.html');
    }

    function action_logout(){
        //アドミンログイン情報を消去
        $this->session->remove('admin_login');
        header("Location: ".'/services/');
    }

    function showMeetingLogContorol(){}

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace("test", __FILE__, __LINE__, __FUNCTION__);
        $this->action_showtop();
    }

    function action_questionnairelog_player(){

        if($meeting_key = $_REQUEST['meeting_key']){

            $this->session->set('log_meeting_key', $meeting_key);

            $lang = $this->session->get('lang');
            if($lang == "en"){
                $lang = 2;
            }else{
                $lang = 1;
            }

            $accessLog = new AccessLog();
            $accessLog->access('questionnaireLogplayer');

            $this->template->assign('lang', $lang);
            $this->display('user/questionnaire_log_player.t.html');
        }
    }

    function action_log_player(){

        if($meeting_key = $_REQUEST['meeting_key']){

            $this->session->set('log_meeting_key', $meeting_key);

            $lang = $this->session->get('lang');
            if($lang == "en"){
                $lang = 2;
            }else{
                $lang = 1;
            }

            $accessLog = new AccessLog();
            $accessLog->access('logplayer');

            $this->template->assign('lang', $lang);
            $this->template->assign('isPresen',$_REQUEST['isPresen']);
            $this->display('admin/meetinglog/player.t.html');
        }
    }

    function action_lastlog_player(){
        if($meeting_key = $_REQUEST['meeting_key']){

            $this->session->set('log_meeting_key', $meeting_key);

            $lang = $this->session->get('lang');
            if($lang == "en"){
                $lang = 2;
            }else{
                $lang = 1;
            }

            $accessLog = new AccessLog();
            $accessLog->access('lastlogplayer');

            $this->template->assign('lang', $lang);
            $this->template->assign('isPresen',$_REQUEST['isPresen']);
            $this->display('admin/meetinglog/log_player.t.html');
        }
    }



    /**
     * 会議記録パスワード確認フォーム表示（レコード削除時）
     */
    function showPwdDialog(){

        $this->template->assign('log_key', $this->log_key);
        $this->display('admin/admin_meetinglog_delete_pwd.t.html');
        exit;
    }

    function action_admin_log(){
        $admin_info = $this->session->get('user_info');
        foreach ($admin_info as $key => $value) {
            $this->template->assign($key, $value);
        }
        if ($admin_edit_info = $this->session->get('admin_edit_info')) {
            foreach($admin_edit_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin/log.t.html');
    }

    function action_pgi_info(){
        $this->template->assign("no_banner", 1);
        $this->display('admin/pgi_info.t.html');
    }
}

$main =& new AppAdmin();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
