<?php
require_once('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");

class AppSecurity extends AppFrame
{

    var $_max_comment_size = 25;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    function default_view() {
        return $this->action_top();
    }

    function action_top() {
        $this->display('admin/security/index.t.html');
    }

    function action_ip_list() {
        return $this->display_ip_list();
    }

    function display_ip_list($message = null) {
        require_once("classes/mgm/dbi/ip_whitelist.dbi.php");
        $objIpList = new IpWhiteListTable(N2MY_MDB_DSN);
        $user_info = $this->session->get("user_info");
        $where = "user_key = '".addslashes($user_info["user_key"])."'";
        $ip_list = $objIpList->getRowsAssoc($where, null, null, null);
        $this->template->assign("ip_list", $ip_list);
        if ($message) {
            $this->template->assign("message", $message);
        }
        $this->set_submit_key();
        $this->display('admin/security/ip_list.t.html');
    }

    function action_edit_comment(){
        if (!$this->check_submit_key()) {
            return $this->display_ip_list();
        }
        // コメント編集
        $comment = $this->request->get("comment");
        if(mb_strlen($comment) > $this->_max_comment_size){
            $message = $this->get_message("ERROR", "maxlen");
            return $this->display_ip_list(sprintf($message , $this->_max_comment_size));
        }
        require_once("classes/mgm/dbi/ip_whitelist.dbi.php");
        $objIpList = new IpWhiteListTable(N2MY_MDB_DSN);
        $user_info = $this->session->get("user_info");
        $where = "ip_whitelist_key = '".addslashes($this->request->get("key"))."'" .
                " AND user_key = '".addslashes($user_info["user_key"])."'";
        $data = array(
                "comment"            => $comment,
                "update_datetime"   => date("Y-m-d H:i:s"),
        );
        $objIpList->update($data, $where);
        // 操作ログ
        $ip = $objIpList->getOne($where, 'ip');
        $this->add_operation_log('ip_comment_edit', array('ip' => $ip));
        $this->display_ip_list();
    }

    function action_add_ip() {
        // 二重処理
        if (!$this->check_submit_key()) {
            return $this->display_ip_list();
        }
        $ip = $this->request->get("ip");
        if (!$ip = $this->isIPFormat($ip)) {
            $message = $this->get_message("ERROR", "regex");
            return $this->display_ip_list($message);
        }
        require_once("classes/mgm/dbi/ip_whitelist.dbi.php");
        $objIpList = new IpWhiteListTable(N2MY_MDB_DSN);
        $user_info = $this->session->get("user_info");
        $data = array(
            "user_key"          => $user_info["user_key"],
            "ip"                => $ip,
            "status"            => 1,
            "create_datetime"   => date("Y-m-d H:i:s"),
            "update_datetime"   => date("Y-m-d H:i:s"),
            );
        $objIpList->add($data);
        // 操作ログ
        $this->add_operation_log('ip_add', array('ip' => $ip));
        return $this->display_ip_list();
    }

    function action_set_enabled() {
        // 二重処理
        if (!$this->check_submit_key()) {
            return $this->display_ip_list();
        }
        require_once("classes/mgm/dbi/ip_whitelist.dbi.php");
        $objIpList = new IpWhiteListTable(N2MY_MDB_DSN);
        $user_info = $this->session->get("user_info");
        $where = "ip_whitelist_key = '".$this->request->get("key")."'" .
            " AND user_key = '".addslashes($user_info["user_key"])."'";
        $data = array(
            "status"            => 1,
            "update_datetime"   => date("Y-m-d H:i:s"),
            );
        $objIpList->update($data, $where);
        // 操作ログ
        $ip = $objIpList->getOne($where, 'ip');
        $this->add_operation_log('ip_enable', array('ip' => $ip));
        return $this->display_ip_list();
    }

    function action_set_disabled() {
        // 二重処理
        if (!$this->check_submit_key()) {
            return $this->display_ip_list();
        }
        require_once("classes/mgm/dbi/ip_whitelist.dbi.php");
        $objIpList = new IpWhiteListTable(N2MY_MDB_DSN);
        $user_info = $this->session->get("user_info");
        $where = "ip_whitelist_key = '".$this->request->get("key")."'" .
            " AND user_key = '".addslashes($user_info["user_key"])."'";
        $data = array(
            "status"            => 0,
            "update_datetime"   => date("Y-m-d H:i:s"),
            );
        $objIpList->update($data, $where);
        // 操作ログ
        $ip = $objIpList->getOne($where, 'ip');
        $this->add_operation_log('ip_disable', array('ip' => $ip));
        return $this->display_ip_list();
    }

    function action_delete_ip() {
        // 二重処理
        if (!$this->check_submit_key()) {
            return $this->display_ip_list();
        }
        require_once("classes/mgm/dbi/ip_whitelist.dbi.php");
        $objIpList = new IpWhiteListTable(N2MY_MDB_DSN);
        $user_info = $this->session->get("user_info");
        $where = "ip_whitelist_key = '".$this->request->get("key")."'" .
            " AND status = 0".
            " AND user_key = '".addslashes($user_info["user_key"])."'";
        $ip = $objIpList->getOne($where, 'ip');
        $objIpList->remove($where);
        // 操作ログ
        $this->add_operation_log('ip_delete', array('ip' => $ip));
        return $this->display_ip_list();
    }

    function isIPFormat($ip) {
        require_once "lib/pear/Net/IPv4.php";
        $ip_address = sprintf("%s.%s.%s.%s", $ip[0],$ip[1],$ip[2],$ip[3]);
        if ($ip["bit"]) {
            $ip_address .= "/".$ip["bit"];
        }
        $ipv4 = new Net_IPv4();
        // IPチェック
        if ($ipv4->validateIP($ip_address)) {
            if ($ipv4->validateNetmask($ip_address)) {
                return false;
            } else {
                return $ip_address;
            }
        } else {
            $net = $ipv4->parseAddress($ip_address);
            if ($net->netmask) {
                return $ip_address;
            }
        }
        return false;
    }

    function action_meeting_setting() {
        require_once 'classes/dbi/user.dbi.php';
        $user_info = $this->session->get("user_info");
        $objUser = new UserTable($this->get_dsn());
        $where = "user_key = ". $user_info["user_key"];
        $security_info = $objUser->getRow($where, "is_compulsion_pw,compulsion_pw,is_minutes_delete,force_stop_meeting_flg");
        if ($security_info["is_compulsion_pw"] == 1) {
            $security_info["is_compulsion_pw"] = 2;
        }
        $this->session->set('security_info', $security_info, $this->_name_space);
        $this->session->set('is_empty_compulsion_pw', empty($security_info['compulsion_pw']), $this->_name_space);
        $this->display_meeting_setting();
    }

    function display_meeting_setting($err = null) {
        $this->set_submit_key();
        $security_info = $this->session->get("security_info", $this->_name_space);
        if (EZValidator::isError($err)) {
            $error_info = $this->get_error_info($err);
            $this->template->assign('error_info', $error_info);
        }
        $this->template->assign('security_info', $security_info);
        $this->display('admin/security/meeting.t.html');
    }

    function action_setting() {

        if (!$this->check_submit_key()) {
            return $this->display_meeting_setting();
        }
        $request = $this->request->getAll();
        $security_info = $request["security_info"];
        $this->session->set("security_info", $security_info, $this->_name_space);
        $err = $this->_check($request);
        if (EZValidator::isError($err)) {
            $is_empty_compulsion_pw = $this->session->get('is_empty_compulsion_pw', $this->_name_space);
            $this->template->assign("is_show_no_change_radio", !$is_empty_compulsion_pw);
            return $this->display_meeting_setting($err);
        }
        require_once 'classes/dbi/user.dbi.php';
        $user_info = $this->session->get("user_info");
        $objUser = new UserTable($this->get_dsn());
        $where = "user_key = ". $user_info["user_key"];
        
        $data["is_minutes_delete"] = $security_info["is_minutes_delete"];
        $user_info["is_minutes_delete"] = $security_info["is_minutes_delete"];
        if ($security_info["is_compulsion_pw"] == 0) {
            $this->session->set('is_empty_compulsion_pw', true, $this->_name_space);
            $data["is_compulsion_pw"] = 0;
            $data["compulsion_pw"] = "";
            $user_info["is_compulsion_pw"]  = 0;
            $user_info["compulsion_pw"]     = "";
        } elseif ($security_info["is_compulsion_pw"] == 1) {
            $this->session->set('is_empty_compulsion_pw', false, $this->_name_space);
            $data["is_compulsion_pw"] = 1;
            $data["compulsion_pw"] = $security_info["compulsion_pw"];
            $user_info["is_compulsion_pw"]  = $data["is_compulsion_pw"];
            $user_info["compulsion_pw"]     = $data["compulsion_pw"];
        } elseif ($security_info["is_compulsion_pw"] == 2) {
            //$data["is_compulsion_pw"] = 1;
        }
        $data["force_stop_meeting_flg"] = $security_info["force_stop_meeting_flg"];
        $user_info["force_stop_meeting_flg"] = $security_info["force_stop_meeting_flg"];

        $port_check = $request["port_check"];
        $this->port_plan_setting_modify($port_check);

        //セッションの情報を更新
        $entry_mode = $objUser -> getOne('user_key = ' . $user_info["user_key"] , "entry_mode");
        $user_info["entry_mode"] = $entry_mode;

        $this->session->set("user_info", $user_info);
        if($security_info['is_compulsion_pw'] == 1){
            $security_info['is_compulsion_pw'] = 2;
        }
        $this->session->set("security_info", $security_info, $this->_name_space);
        
        //$this->logger2->info($user_info);
        $objUser->update($data, $where);
        $this->template->assign("message", 1);
        $this->add_operation_log('security_setup', array(
            'is_minutes_delete' => $security_info["is_minutes_delete"],
            'is_compulsion_pw'  => $data["is_compulsion_pw"],
            'force_stop_meeting_flg' => $security_info["force_stop_meeting_flg"],
            'port_check_flg' => $user_info["entry_mode"],
            ));

        return $this->display_meeting_setting();
    }

    function action_mail_setting() {
        require_once 'classes/dbi/user.dbi.php';
        $user_info = $this->session->get("user_info");
        $objUser = new UserTable($this->get_dsn());
        $where = "user_key = ". $user_info["user_key"];
        $mail_setting_info = $objUser->getRow($where, "guest_url_format,guest_mail_type");
        $this->session->set('mail_setting_info', $mail_setting_info, $this->_name_space);
        $this->display_mail_setting();
    }

    function display_mail_setting() {
        $this->set_submit_key();
        $mail_setting_info = $this->session->get("mail_setting_info", $this->_name_space);
        if (EZValidator::isError($err)) {
            $error_info = $this->get_error_info($err);
            $this->template->assign('error_info', $error_info);
        }
        $this->logger2->info($mail_setting_info,"user_info");
        $this->template->assign('mail_setting_info', $mail_setting_info);
        $this->display('admin/security/mail_setting.t.html');
    }

    function action_mail_setting_modify() {
        if (!$this->check_submit_key()) {
            return $this->display_mail_setting();
        }
        $request = $this->request->getAll();
        $mail_setting_info = $request["mail_setting_info"];
        $this->logger2->info($mail_setting_info,"mail_setting_info");
        $this->session->set("mail_setting_info", $mail_setting_info, $this->_name_space);
        require_once 'classes/dbi/user.dbi.php';
        $user_info = $this->session->get("user_info");
        $objUser = new UserTable($this->get_dsn());
        $where = "user_key = ". $user_info["user_key"];
        $objUser->update($mail_setting_info, $where);
        $this->template->assign("message", 1);
        $user_info["guest_url_format"] = $mail_setting_info["guest_url_format"];
        $user_info["guest_mail_type"] = $mail_setting_info["guest_mail_type"];
        $this->session->set("user_info",$user_info);
        //LOG
        $this->add_operation_log("mail_setting",$mail_setting_info);
        return $this->display_mail_setting();
    }

    function _check($request) {
        $security_info = $request["security_info"];
        $rules = array();
        if ($security_info['is_compulsion_pw'] == 1) {
            $rules = array(
                "protocol" => array(
                    "allow" => array("443", "rtmp"),
                ),
                "compulsion_pw" => array(
                    "required" => true,
                    "minlen" => 6,
                    "maxlen" => 16,
                    "alnum" => true,
                    "equal" => $request["confirm_compulsion_pw"],
                    )
                );
        }
        $check_obj = new EZValidator($security_info);
        foreach($rules as $field => $rules) {
            $check_obj->check($field, $rules);
        }
        return $check_obj;
    }

    function port_plan_setting_modify($port_check) {
        $user_info = $this->session->get("user_info");
        require_once 'classes/dbi/user.dbi.php';
        $objUser = new UserTable($this->get_dsn());

        if($port_check == "0"){
            // entry_mode （0：通常運用）切り替え
            if($user_info["entry_mode"] !== "0"){
                $where = "user_key = ". $user_info["user_key"];
                $data  = array(
                        "entry_mode"        => "0",
                        "user_updatetime"   => date("Y-m-d H:i:s"),
                         );
                $objUser->update($data, $where);
            } 
        }elseif($port_check == "1"){
            if($user_info["entry_mode"] == "0"){ 
                require_once 'classes/dbi/reservation.dbi.php';
                require_once 'classes/dbi/room.dbi.php';
                $objReservation = new ReservationTable($this->get_dsn());
                $objRoom = new RoomTable($this->get_dsn());

                $max_connect_participant = $objUser -> getOne('user_key = ' . $user_info["user_key"] , "max_connect_participant");

                // entry_mode （1：ポート制御）切り替え
                $where = "user_key = ". $user_info["user_key"];
                $data  = array(
                        "entry_mode"        => "1",
                        "user_updatetime"   => date("Y-m-d H:i:s"),
                        );
                $objUser->update($data, $where);

                // 予約会議の参加者人数変更
                $where = "user_key = '". $user_info["user_key"] . "' AND reservation_endtime > '". date("Y-m-d H:i:s") . "' AND reservation_status = '1'";
                $reservation_info = $objReservation->getList($where);

                foreach($reservation_info as $field => $list) {
                    $where = "room_key = '". $list["room_key"] . "'";
                    $room_info = $objRoom->getRow($where);
                    $room_sum  = $room_info["max_seat"] + $room_info["max_whiteboard_seat"];

                    // 通常とWBの合計が、契約ポート数を超える場合は、契約ポート数を参加可能人数に登録
                    if($max_connect_participant < $room_sum){
                        $room_sum = $max_connect_participant;
                    }
                    $where = "reservation_key = '".addslashes( $list["reservation_key"])."'";
                    $data  = array(
                            "max_port"                 => $room_sum,
                            "reservation_updatetime"   => date("Y-m-d H:i:s"),
                            );
                    $objReservation->update($data, $where);
                }

                // 人数を超過している時間帯がある場合、警告を表示する
                require_once("classes/N2MY_Reservation.class.php");
                $this->obj_N2MY_Reservation = new N2MY_Reservation($this->get_dsn());
                $ganttChartsArray = $this->obj_N2MY_Reservation->ganttCharts(time(),null,$user_info["user_key"]);
                $checkPortOverArray = $this->obj_N2MY_Reservation->checkPortOver($max_connect_participant,$ganttChartsArray);

                if($checkPortOverArray){
                    $this->template->assign("message_port_alert", 1);
                }
            }
        }
    }
}
$main = new AppSecurity();
$main->execute();
