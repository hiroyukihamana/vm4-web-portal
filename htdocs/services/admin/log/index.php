<?php
require_once("classes/core/Core_Meeting.class.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/N2MY_Eco.class.php");
require_once("classes/N2MY_MeetingLog.class.php");
require_once("classes/AppFrame.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/room_plan.dbi.php");
require_once("classes/dbi/service.dbi.php");
require_once("lib/EZLib/EZUtil/EZCsv.class.php");

require_once('classes/dbi/member.dbi.php');
require_once('classes/dbi/inbound.dbi.php');
require_once('classes/core/dbi/Meeting.dbi.php');
require_once('classes/core/dbi/Participant.dbi.php');

class AppAdminLog extends AppFrame
{
    var $room_plan;
    var $service;
    private $obj_Room;
    private $obj_RoomPlan;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->objN2MYEco = new N2MY_Eco($this->get_dsn(), N2MY_MDB_DSN);
        $this->obj_N2MY_MeetingLog = new N2MY_MeetingLog($this->get_dsn());
        $this->objCoreMeeting = new Core_Meeting($this->get_dsn());
        $this->_name_space = md5(__FILE__);
        $this->obj_Room = new RoomTable( $this->get_dsn() );
        $this->obj_RoomPlan = new RoomPlanTable( $this->get_dsn() );
        $this->obj_Service = new ServiceTable( $this->get_dsn() );

        $this->obj_Member      = new MemberTable($this->get_dsn());
        $this->obj_Participant = new DBI_Participant($this->get_dsn());
        $this->obj_Meeting     = new DBI_Meeting($this->get_dsn());
        $this->obj_Inbound     = new InboundTable($this->get_dsn());
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    /**
     * アクセスログ出力
     */
    function action_operation_log_csv()
    {
        $user_info = $this->session->get('user_info');
        $search_form = $this->request->get('search');
        $where = "user_key = ".$user_info['user_key'];
        if ($search_form['member_id']) {
            $where .= " AND member_id = '".addslashes($search_form['member_id'])."'";
        }
        if (!$search_form['start']) { $search_form['start'] = date('Y-m-d');
        }
        if ($search_form['start']) {
            $where .= " AND operation_datetime >= '".addslashes($search_form['start'])."'";
        }
        if ($search_form['end']) {
            $where .= " AND operation_datetime <= '".addslashes($search_form['end'])." 23:59:59'";
        }
        if($search_form['start']){
            $timeDiff = $this->getTimeDiff($search_form['start'], $now = date('Y/m/d'));
            if($timeDiff > 365){
                $this->logger2->warn(__FUNCTION__."#ADMIN ACCESS LOG OVER 1 YEAR AGO CAN'T BE ACCESSED",__FILE__,__LINE__);
                header("Location: /services/admin/log/?action_operation_log");
            }
        }
        if ($search_form['action']) {
            $where .= " AND action_name = '".addslashes($search_form['action'])."'";
        }
        if ($ipList = $this->getIgroreIpAddr()) {
            foreach ($ipList as $ip) {
                $where .= " AND remote_addr != '".$ip."'";
            }
        }
        require_once ("classes/dbi/operation_log.dbi.php");
        $objOperationLog = new OperationLogTable($this->get_dsn());
        $operation_list = $objOperationLog->getRowsAssoc($where, array("operation_datetime" => "asc"));
        $room_list = $this->obj_Room->getRowsAssoc("user_key = ".$user_info['user_key'], null, null, null, "room_key, room_name, room_status", "room_key");
        foreach($operation_list as $key => $log) {
              $operation_list[$key]['info']         = unserialize($log["info"]);
        }
        ob_start();
        $frame["lang"] = $this->_lang;
        $this->logger->info("lang",__FILE__,__LINE__,$this->_lang);
        $this->template->assign('__frame',$frame);
        $this->template->assign('room_list',$room_list);
        $this->template->assign('opration_logs',$operation_list);
        $this->template->display('common/admin/log/opration.t.csv');
        $contents = ob_get_contents();
        $output_encoding = $this->get_output_csv_encoding();
        if ($output_encoding != 'UTF-8') {
            $contents = mb_convert_encoding($contents, $output_encoding, 'UTF-8');
        }
        ob_clean();
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="operation.csv"');
        print $contents;
    }

    function action_operation_log() {
        $user_info = $this->session->get('user_info');
        $search_form = $this->request->get('search');
        // ページャ
        $limit  = $this->request->get("page_cnt", 20);
        $page   = $this->request->get("page", 1);
        $offset = ($page - 1) * 20;

        $where = "user_key = ".$user_info['user_key'];

        $room_list = $this->obj_Room->getRowsAssoc($where, null, null, null, "room_key, room_name, room_status", "room_key");
        require_once ("classes/dbi/operation_log.dbi.php");
        $objOperationLog = new OperationLogTable($this->get_dsn());

        $where = "user_key = ".$user_info['user_key'];
        if ($search_form['member_id']) {
            $where .= " AND member_id = '".addslashes($search_form['member_id'])."'";
        }
        if (!$search_form['start']) $search_form['start'] = date('Y/m/01');
        if (!$search_form['end'])   $search_form['end'] = date('Y/m/d', gmmktime(0, 0, 0, date('m') +1, 1, date('Y')));
        if ($search_form['start']) {
            $where .= " AND operation_datetime >= '".addslashes($search_form['start'])."'";
        }
        if ($search_form['end']) {
            $where .= " AND operation_datetime <= '".addslashes($search_form['end'])." 23:59:59'";
        }
        if ($search_form['action']) {
            $where .= " AND action_name = '".addslashes($search_form['action'])."'";
        }
        if($search_form['start']){
            $timeDiff = $this->getTimeDiff($search_form['start'], date('Y/m/d'));
            if($timeDiff > 365){
                $this->logger2->warn(__FUNCTION__."#ADMIN ACCESS LOG OVER 1 YEAR AGO CAN'T BE ACCESSED",__FILE__,__LINE__);
                header("Location: /services/admin/log/?action_operation_log");
            }
        }
        if ($ipList = $this->getIgroreIpAddr()) {
            foreach ($ipList as $ip) {
                $where .= " AND remote_addr != '".$ip."'";
            }
        }
        //$where = "user_key = ".$user_info['user_key'];
        $opration_log_list = $objOperationLog->getRowsAssoc($where, array('operation_datetime' => 'desc'), $limit, $offset);
        foreach( $opration_log_list as $key => $val ){
            $opration_log_list[$key]["operation_datetime"]  = EZDate::getLocateTime( $val["operation_datetime"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE);
            $opration_log_list[$key]["info"]                = $info = unserialize($val["info"]);
        }
        $count  = $objOperationLog->numRows($where);
        $pager = $this->setPager($limit, $page, $count);

        $this->template->assign('search', $search_form);
        $this->template->assign('pager', $pager);
        $this->template->assign('room_list', $room_list);
        $this->template->assign('opration_log_list', $opration_log_list);
        $this->display('admin/log/opration.t.html');
    }

    /**
     * 部屋のログ出力
     */
    function action_month_log()
    {
        $room_key = $this->get_room_key();
        if ($room_key == "") {
            $user_info = $this->session->get('user_info');
            $room_list = parent::get_room_list(false, true);
//            $room_list = $this->session->get("room_info");
            foreach($room_list as $_key => $room) {
                $rooms[] = $room["room_info"]["room_key"];
            }
            $room_key = implode(",",$rooms);
        }
        $month = $this->request->get("month");
        $y = substr($month,0,4);
        $m = substr($month,4,2);
        $end_time = mktime(0,0,0,($m + 1), 1, $y) - 1;
        $start_time = mktime(0,0,0,($m), 1, $y);
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array(
            $room_key,
            date("Y-m-d H:i:s", $start_time),
            date("Y-m-d H:i:s", $end_time)));
        $this->output_csv($room_key, $start_time, $end_time);
        exit();
    }

	/**
	 * V-Cube ONE 専用
	 */
	function action_month_log_for_ONE(){
        $month = $this->request->get("month");
        $y = substr($month,0,4);
        $m = substr($month,4,2);
	    $end_time = mktime(0,0,0,($m + 1), 1, $y) - 1;
        $start_time = mktime(0,0,0,($m), 1, $y);
        $output_encoding = $this->get_output_csv_encoding();
        $csv = new EZCsv(false,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="'.date("Ym",$end_time).'.csv"');
        // ヘッダ２
        $head_room          = $this->get_message("CSV_LOG","room","Room Title");
        $head_title         = $this->get_message("CSV_LOG","title","Meeting Title");
        $head_date          = $this->get_message("CSV_LOG","date","Date");
        $head_participant   = $this->get_message("CSV_LOG","participants","Participants");
        $head_size          = $this->get_message("CSV_LOG","size","Size(KByte)");
        $head_use_time      = $this->get_message("CSV_LOG","use_time","Use Time(Minute)");
        $header = array($head_room, $head_title, $head_date, $head_participant, $head_size, $head_use_time);
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->write($header);
		$user_info = $this->session->get('user_info');
		$member_info = $this->session->get('member_info');
        $query = array(
            "user_key"          => $user_info['user_key'],
            "member_key"        => empty($member_info) ? '' : $member_info['member_key'],
            "room_key"          => $this->get_room_key(),
            "limit"             => null,
            "offset"            => null,
        	"sort_key"          => "actual_start_datetime",
        	"sort_type"         => "asc",
            "start_datetime"    => date("Y-m-d H:i:s", $start_time),
            "end_datetime"      => date("Y-m-d H:i:s", $end_time),
    	);
        $return = $this->obj_N2MY_MeetingLog->getList_for_ONE($query);
        if (isset( $return ) ){
            foreach( $return as $_key1 => $meeting) {
            	if($meeting['is_one_time_meeting']){
            		$line["room_name"]  = "-";
            	}else{
	                $line["room_name"]  = $meeting["meeting_room_name"];
            	}
                $line["meeting_name"]   = $meeting["meeting_name"];
                $line["date"]           = substr($meeting["actual_start_datetime"], 0, 10);
                // 参加者一覧
                $users = array();
                if( $meeting["participants"] ){
                    foreach( $meeting["participants"] as $_key2 => $user) {
                        if ($user["participant_type_name"] != "audience" && $user["participant_name"] != "") {
                            $users[] = $user["participant_name"];
                        }
                    }
                }
                $line["users"]       = join($users, ",");
                $line["size"]        = number_format( ( $meeting["meeting_size_used"] / 1024), 2 );
                $line["use_time"]    = $meeting["meeting_use_minute"];
                $csv->write($line);
            }
        }
        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
	}

    public function action_eco_monthlylog()
    {
        $month = $this->request->get("month");
        $y = substr($month,0,4);
        $m = substr($month,4,2);
        $targetDate = mktime(0,0,0,($m), 1, $y);
        $userInfo = $this->session->get("user_info");
        $info = $this->objN2MYEco->getMonthlyLogList($userInfo["user_key"], date("Y-m", $targetDate));

        // CSV出力
        $csv = new EZCsv();
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="'.date("Ym",$targetDate).'.csv"');

        //集計期間
        $csv->write(array($this->get_message("ECO_MONTYLY_LOG","date"), date("Y/m", $targetDate)));
        //植林効果
        $csv->write(array(sprintf($this->get_message("ECO_MONTYLY_LOG","tree"), date("Y", $targetDate), date("m", $targetDate)), round($info["co2Total"] / 38)));
        //合計往復交通費
        $csv->write(array(sprintf($this->get_message("ECO_MONTYLY_LOG","fareTotal"), date("Y", $targetDate), date("m", $targetDate)), $info["fareTotal"]));
        //往復時間
        $csv->write(array(sprintf($this->get_message("ECO_MONTYLY_LOG","minuteTotal"), date("Y", $targetDate), date("m", $targetDate)), $info["minuteTotal"]));
        //往復co2
        $csv->write(array(sprintf($this->get_message("ECO_MONTYLY_LOG","co2Total"), date("Y", $targetDate), date("m", $targetDate)), sprintf('%.1f ', ($info["co2Total"] / 1000))));
        //会議数
        $csv->write(array(sprintf($this->get_message("ECO_MONTYLY_LOG","numMeeting"), date("Y", $targetDate), date("m", $targetDate)), $info["numMeeting"]));
        //拠点入力ユーザー
        $csv->write(array(sprintf($this->get_message("ECO_MONTYLY_LOG","numUser"), date("Y", $targetDate), date("m", $targetDate)), $info["numStationName"]));
        //未入力ユーザー
        $csv->write(array(sprintf($this->get_message("ECO_MONTYLY_LOG","numInvalidUser"), date("Y", $targetDate), date("m", $targetDate)), $info["numNoStationName"]));
        $csv->write();
        //header
        $csv->write(array(
                        $this->get_message("ECO_MONTYLY_LOG","startdate"),
                        $this->get_message("ECO_MONTYLY_LOG","meetingTitle"),
                        $this->get_message("ECO_MONTYLY_LOG","participants"),
                        $this->get_message("ECO_MONTYLY_LOG","stationName"),
                        $this->get_message("ECO_MONTYLY_LOG","venue"),
                        $this->get_message("ECO_MONTYLY_LOG","fare"),
                        $this->get_message("ECO_MONTYLY_LOG","minute"),
                        $this->get_message("ECO_MONTYLY_LOG","co2")
                        ));
        //会議log出力
        for ($i = 0; $i < count($info["meetingList"]); $i++) {
            $meeting = $info["meetingList"][$i];
            $csv->write(array(
                        $meeting["actual_start_datetime"],
                        $meeting["meeting_name"]
                        ));
            for ($j = 0; $j < count($meeting["participantList"]); $j++) {
                $participant = $meeting["participantList"][$j];
                $csv->write(array(
                        "",
                        "",
                        $participant["participant_name"],
                        $participant["participant_station"],
                        $participant["venue"] ? "○" : "",
                        $participant["fare"],
                        $participant["minute"],
                        $participant["co2"]
                        ));
            }
        }

        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
    }

    public function action_eco_yearlylog()
    {
        $year = $this->request->get("year");
        $month = ($year == date("Y")) ? date("m") : 12;
        $userInfo = $this->session->get("user_info");
        $info = $this->objN2MYEco->getYearlyLogList($userInfo["user_key"], $year, $month);

        // CSV出力
        $csv = new EZCsv();
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="'.$year.'.csv"');

        //集計期間
        $csv->write(array($this->get_message("ECO_YEARLY_LOG","date"), sprintf("%s/1〜%s", $year, $year, $month)));
        //植林効果
        $csv->write(array(sprintf($this->get_message("ECO_YEARLY_LOG","tree"), $year, $year, $month), round($info["co2Total"] / 38)));
        //合計往復交通費
        $csv->write(array(sprintf($this->get_message("ECO_YEARLY_LOG","fareTotal"), $year, $year, $month), $info["fareTotal"]));
        //往復時間
        $csv->write(array(sprintf($this->get_message("ECO_YEARLY_LOG","minuteTotal"), $year, $year, $month), $info["minuteTotal"]));
        //往復co2
        $csv->write(array(sprintf($this->get_message("ECO_YEARLY_LOG","co2Total"), $year, $year, $month), sprintf('%.1f ', ($info["co2Total"] / 1000))));
        //会議数
        $csv->write(array(sprintf($this->get_message("ECO_YEARLY_LOG","numMeeting"), $year, $year, $month), $info["numMeeting"]));
        //拠点入力ユーザー
        $csv->write(array(sprintf($this->get_message("ECO_YEARLY_LOG","numUserTotal"), $year, $year, $month), $info["numStationName"]));
        //未入力ユーザー
        $csv->write(array(sprintf($this->get_message("ECO_YEARLY_LOG","numInvalidUserTotal"), $year, $year, $month), $info["numNoStationName"]));
        $csv->write();

        //header
        $csv->write(array(
                        $this->get_message("ECO_YEARLY_LOG","month"),
                        $this->get_message("ECO_YEARLY_LOG","fare"),
                        $this->get_message("ECO_YEARLY_LOG","minute"),
                        $this->get_message("ECO_YEARLY_LOG","co2"),
                        $this->get_message("ECO_YEARLY_LOG","tree"),
                        $this->get_message("ECO_YEARLY_LOG","numUser"),
                        $this->get_message("ECO_YEARLY_LOG","numInvalidUser")
                        ));

        for ($i = 0; $i < count($info["list"]); $i++) {
            $monthlyInfo = $info["list"][$i];
            $csv->write(array(
                        $monthlyInfo["date"],
                        $monthlyInfo["fareTotal"],
                        $monthlyInfo["minuteTotal"],
                        sprintf('%.1f ', ($monthlyInfo["co2Total"] / 1000)),
                        round($monthlyInfo["co2Total"] / 38),
                        $monthlyInfo["numStationName"],
                        $monthlyInfo["numNoStationName"],
                        ));
        }
        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
    }

    function action_log() {
        $room_key = $this->get_room_key();
        $start = $this->request->get("start", "0000-00-00 00:00:00");
        $start_time = strtotime($start);
        $end = $this->request->get("end", date("Y-m-d H:i:s"));
        $end_time = strtotime($end);
        $this->output_csv($room_key, $start_time, $end_time);
        exit();
    }

    function output_csv($room_key = "", $start_time, $end_time) {
        // CSV出力
        $output_encoding = $this->get_output_csv_encoding();
        $csv = new EZCsv(false,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="'.date("Ym",$end_time).'.csv"');
        // ヘッダ２
        $head_room          = $this->get_message("CSV_LOG","room","Room Title");
        $head_title         = $this->get_message("CSV_LOG","title","Meeting Title");
        $head_date          = $this->get_message("CSV_LOG","date","Date");
        $head_participant   = $this->get_message("CSV_LOG","participants","Participants");
        $head_size          = $this->get_message("CSV_LOG","size","Size(KByte)");
        $head_use_time      = $this->get_message("CSV_LOG","use_time","Use Time(Minute)");
        $header = array($head_room, $head_title, $head_date, $head_participant, $head_size, $head_use_time);
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->write($header);
        $query = array(
            "room_key"          => $room_key,
            "limit"             => null,
            "offset"            => null,
            "sort_key"          => "actual_start_datetime",
            "sort_type"         => "asc",
            "start_datetime"    => date("Y-m-d H:i:s", $start_time),
            "end_datetime"      => date("Y-m-d H:i:s", $end_time),
        );
        $return = $this->obj_N2MY_MeetingLog->getList($query, true);
        $log_list = array();
        if (isset( $return ) ){
            foreach( $return as $_key1 => $meeting) {
                $line["room_name"]      = $meeting["meeting_room_name"];
                $line["meeting_name"]   = $meeting["meeting_name"];
                $line["date"]           = substr($meeting["actual_start_datetime"], 0, 10);
                // 参加者一覧
                $users = array();
                if( $meeting["participants"] ){
                    foreach( $meeting["participants"] as $_key2 => $user) {
                        if ($user["participant_type_name"] != "audience" && $user["participant_name"] != "") {
                            $users[] = $user["participant_name"];
                        }
                    }
                }
                $line["users"]       = join($users, ",");
                $line["size"]        = number_format( ( $meeting["meeting_size_used"] / 1024), 2 );
                $line["use_time"]    = $meeting["meeting_use_minute"];
                $this->logger2->debug($line);
                $csv->write($line);
            }
        }
        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
    }

    /**
     * 部屋名を取得
     */
    function _get_room_name($room_key) {
        static $rooms;
        if (!isset($rooms)) {
            $user_info = $this->session->get('user_info');
            $room_table = new RoomTable($this->get_dsn());
            $_rooms = $room_table->use_room($user_info['user_key']);
            foreach($_rooms as $_key => $val) {
                $rooms[$val["room_key"]] = $val["room_name"];
            }
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__, $rooms);
        }
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__, $rooms[$room_key]);
        return $rooms[$room_key];
    }

    function default_view()
    {
        $this->render_contract();
    }

    /**
     * 契約内容表示
     */
    function render_contract() {

        //予約会議終了チェック
        $user_info    = $this->session->get('user_info');
        require_once ("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable( $this->get_dsn() );
        $where = "user_key = ".mysql_real_escape_string($user_info["user_key"]);
        $rooms = $obj_Room->getRowsAssoc($where, null, null, null, "room_key, meeting_key");
        foreach( $rooms as $key => $val ){
            $query[$val["room_key"]] = $val["meeting_key"];
        }
        require_once("classes/core/Core_Meeting.class.php");
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $obj_CoreMeeting->checkReservationMeetingStatus($query);

        //ユーザーキーからオーディエンスオプションの契約状況を取得
        require_once("classes/dbi/meeting_use_log.dbi.php");
        $meeting_use_log = new MeetingUseLogTable( $this->get_dsn() );
        // 月
        $month = $this->request->get("month");
        $month_count = $this->config->get("ADMIN_LOG", "month_count", 12);
        for ($i = 0; $i < $month_count; $i++) {
            $month_list[] = mktime(0,0,0,(date("m") - $i),1,date("Y"));
        }
        $this->template->assign("select_month",$month);
        $this->template->assign("month_list",$month_list);
        // 年
        $startYear = 2008;
        $endYear = date("Y");
        $year_list = array();
        for ($year = $endYear; $year >= $startYear; $year--) {
            $year_list[] = $year;
        }
        $this->template->assign("select_year",$endYear);
        $this->template->assign("year_list",$year_list);
        // 部屋情報
//        $room_list = $this->session->get("room_info");
//        $user_info = $this->session->get('user_info');
		$service_mode = $this->session->get("service_mode");
		if( !$this->check_one_account() || $service_mode != 'meeting'){
            if($this->obj_N2MY_Account->getSalesRoomCntCheck( $user_info["user_key"] )){
                $this->template->assign( "is_room_split", $this->obj_N2MY_Account->getSalesRoomCntCheck( $user_info["user_key"] ) );
                $where = "user_key =" .addslashes($user_info['user_key']).
                " AND is_one_time_meeting = 0 AND room_status = 1";
                if ( $service_mode == "sales" ) {
                    $where .= " AND use_sales_option = 1";
                } else {
                    $where .= " AND use_sales_option != 1";
                }
                $sort = array("room_sort" => "ASC", "room_key" => "ASC");
                $room_list = $this->obj_Room->getRowsAssoc($where, $sort, null, null, "room_key,room_name","room_key");
            } else {
                $room_list = $this->obj_N2MY_Account->getRoomList($user_info['user_key'], $service_mode);
            }
            
            foreach($room_list as $key => $room_info) {
	            // 会議の検索ヒット件数を取得
	            $where = "room_key = '".$key."'".
	                " AND create_datetime >= '".date("Y-m-01 00:00:00")."'".
	                " AND create_datetime < '".date("Y-m-01 00:00:00", strtotime("+1 month"))."'".
	                " AND type IS NULL".
	                " AND participant_id IS NULL";
	            $cnt = $meeting_use_log->numRows($where);
	            $room_list[$key]["use_time"] = $cnt;
	            // オプション情報の拡張
	            $room_list[$key]["options"] = $this->obj_N2MY_Account->getRoomOptionList($key);
	        }
	        $this->template->assign( "room_list", $room_list );
    	}

        $where = "user_key = '".$user_info["user_key"]."'"." AND status = 1";
        $inbound_list = $this->obj_Inbound->getRowsAssoc($where, null, null, null, "inbound_id, inbound_name, inbound_sort");

        $this->template->assign( "inbound_list", $inbound_list );

        // 表示
        $this->display("admin/log/index.t.html");
    }

    /**
     * 出力するCSVファイルのエンコーディング形式
     */
    function get_output_csv_encoding() {
        // 現在の言語設定で判断。
        $lang = $this->get_language();

        switch($lang) {
            case 'ja_JP':
                return 'SJIS';
            case 'en_US':
            case 'zh_CN':
            default:
                return 'UTF-8';
        }
    }

    //tashita
    //利用一覧ログを取得
    function action_get_log(){
        //タイプ取得
        $type = $this->request->get("type");
        //利用状況を取得
        //-------------------------------
        //V4MTGVFOUR-2232 【One】管理者IDでS&Sにログインした際にタイムアウトが発生する
        //Room情報全取得ではなく、RoomKey取得だけで問題なさそうだが検証ができていないので、
        //冗長化ではあるがis_room_splitチェックを行い影響を少なくして対応とする。
        //-------------------------------
        if($this->session->get("is_room_split")){
            $user_info = $this->session->get("user_info");
            $rooms = $this->obj_N2MY_Account->getSalesRoomKey( $user_info["user_key"] );
        }else{
            $room_info = parent::get_room_list(false, true);            
        }
        //ログ取得開始日を取得
        $startDate = $this->request->get($type."StartDate");
        //ログ取得終了日を取得
        $endDate = $this->request->get($type."EndDate");
        $timeDiff = $this->getTimeDiff($startDate, $now = date('Y/m/d'));
        if($timeDiff > 365){
            $this->logger2->warn(__FUNCTION__."#SALES LOG OVER 1 YEAR AGO CAN'T BE ACCESSED",__FILE__,__LINE__);
            header("Location: /services/admin/log/");
        }
        $start_date = date('Y-m-d H:i:s', mktime(0, 0, 0, date("n", strtotime($startDate)), date("j", strtotime($startDate)), date("Y", strtotime($startDate))));
        $end_date = date('Y-m-d H:i:s', mktime(23, 59, 59, date("n", strtotime($endDate)), date("j", strtotime($endDate)), date("Y", strtotime($endDate))));

        //XMLファイルからデータを取得
        $eventlog_xml = array();
        //V4MTGVFOUR-2232 【One】管理者IDでS&Sにログインした際にタイムアウトが発生する
        if(!$this->session->get("is_room_split")){
            foreach($room_info as $_key => $room) {
                $rooms[] = $room["room_info"]["room_key"];
            }
        }
        $event_logs = $this->getSalesLog($rooms, $start_date, $end_date);
        $this->logger->info("log_data",__FILE__,__LINE__,$event_logs);
        if ("list" == $type) {
            $this->output_list_csv($event_logs, $start_date, $end_date);
        } else if ("summary" == $type) {
            $this->output_summary_csv($event_logs, $start_date, $end_date);
        } else if ("time" == $type) {
            $this->output_time_csv($event_logs, $start_date, $end_date);
        }
    }

    //利用一覧ログcsvファイルを作成
    function output_list_csv($event_logs, $start_date, $end_date) {
        //日付のみに変更
        $start_date = date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime($end_date));

        //配列を作成
        $list_data = array();
        //room_keyごとのデータを取得
        $former_room_key = null;
        $member_data = null;
        foreach($event_logs as $_key => $list_meeting) {
            //room_keyごとのデータを取得
            if($list_meeting){
                //menber_idを取得
                $room_key = $list_meeting["room_key"];
                if($room_key != $former_room_key) {
	                require_once('classes/dbi/member_room_relation.dbi.php');
	                $obj_memberRoom = new MemberRoomRelationTable( $this->get_dsn() );
	                $where_member = "room_key = '".addslashes($room_key)."'";
	                $member_key = $obj_memberRoom->getOne($where_member, "member_key");
	                $where_member = "member_key = '".$member_key."'";
	                $member_data = $this->obj_Member->getRow($where_member);
	                $former_room_key = $room_key;
                }
                //予約会議の場合はCallTimeはreservedにする
                //$is_reserved = $this->obj_Meeting->getIsReserved($list_meeting["meeting_key"]);
                //$calltime =($is_reserved == "1") ? "reserved" : $list_meeting["time"]["calltime"];
                //配列にデータを入れる
                $list_data[] = array(
                    "member_id" => $member_data["member_id"],
                    "callin_time" => date('Y-m-d H:i:s', EZDate::getLocateTime( $list_meeting["start_time"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE)),
                    "customer_name" => $list_meeting["customer_name"],
                    "conecttime" => $list_meeting["connect_time"],
                );
            }
        }
        //$this->logger2->info($list_data);
        //タイムゾーン取得
        $time_zone = $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE);
        $this->template->assign("time_zone",$time_zone);

        // CSV出力
        header('Pragma: Public');
        header('Cache-Control: public');
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="list_'.date("Ymd").'.csv"');
        $this->template->assign("list_data",$list_data);
        $this->template->assign("start_date",$start_date);
        $this->template->assign("end_date",$end_date);
	    $frame["lang"] = $this->get_language();
        $this->template->assign('__frame',$frame);
        $output = mb_convert_encoding($this->template->fetch('common/admin/log/list.t.csv'), $this->get_output_csv_encoding());
        print $output;

    }

    //利用一覧ログcsvファイルを作成
    function output_summary_csv($event_logs, $start_date, $end_date) {

        //日付のみに変更
        $start_date = date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime($end_date));
        //配列を作成
        $list_data = array();
        //日数を取得
        $startDay = strtotime($start_date);
        $endDay = strtotime($end_date);
        $usetime_term = ($endDay - $startDay)/ 86400 +1;

        //room_keyごとのデータを取得
        $this->logger->trace("eventlog",__FILE__,__LINE__,$usetime_term);
        //meeting_keyごとのデータを取得
        $all_conecttime = "";
        $all_calltime = "";
        $starttime_data = array();
        if($event_logs){
            foreach($event_logs as $row)	{
                $room_key = $row["room_key"];
                $this->logger->trace("room_data",__LINE__,__FILE__,$room_key);
                // 部屋ごとの接続時間
                $conecttime = $row["connect_time"];
                $list_data[$room_key]["all_conecttime"] += $conecttime;
                // 部屋ごとの呼出時間
                //$calltime = $row["time"]["calltime"];
                //$list_data[$room_key]["callTime"] += $calltime;
                // 回数
                $list_data[$room_key]["usetime"]++;
            }
        }
        foreach($list_data as $room_key => $data){
            $list_data[$room_key]["usetime_average"] = round($data["usetime"] / $usetime_term, 3);
            //$list_data[$room_key]["call_average"] = $data["callTime"] / $data["usetime"];
            //menber_idを取得
            require_once('classes/dbi/member_room_relation.dbi.php');
            $obj_memberRoom = new MemberRoomRelationTable( $this->get_dsn() );
            $where_member = "room_key = '".addslashes($room_key)."'";
            $member_key = $obj_memberRoom->getOne($where_member, "member_key");
            $where_member = "member_key = '".$member_key."'";
            $member_data = $this->obj_Member->getRow($where_member);
            $former_room_key = $room_key;
            $this->logger2->info($member_data);
            $list_data[$room_key]["member_id"] = $member_data["member_id"];
        }
        $this->logger->trace("list_data",__FILE__,__LINE__,$list_data);
        //タイムゾーン取得
        $time_zone = $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE);
        $this->template->assign("time_zone",$time_zone);

        // CSV出力
        header('Pragma: Public');
        header('Cache-Control: public');
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="summary_'.date("Ymd").'.csv"');
        $this->template->assign("list_data",$list_data);
        $this->template->assign("start_date",$start_date);
        $this->template->assign("end_date",$end_date);
	    $frame["lang"] = $this->get_language();
        $this->template->assign('__frame',$frame);
        $output = mb_convert_encoding($this->fetch('common/admin/log/summary.t.csv'), $this->get_output_csv_encoding());
        print $output;

    }

    //利用時間一覧ログcsvファイルを作成
    function output_time_csv($event_logs, $start_date, $end_date) {

        $user_info = $this->session->get("user_info");

        //日付のみに変更
        $start_date = date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime($end_date));

        //配列を作成
        $list_data = array();
        //room_keyごとのデータを取得
        $former_room_key = null;
        $member_data = null;
        foreach($event_logs as $_key => $time_meeting) {
            //room_keyごとのデータを取得
            if($time_meeting){
                //menber_idを取得
                $room_key = $time_meeting["room_key"];
                if($room_key != $former_room_key) {
	                require_once('classes/dbi/member_room_relation.dbi.php');
	                $obj_memberRoom = new MemberRoomRelationTable( $this->get_dsn() );
	                $where_member = "room_key = '".addslashes($room_key)."'";
	                $member_key = $obj_memberRoom->getOne($where_member, "member_key");
	                $where_member = "member_key = '".$member_key."'";
	                $member_data = $this->obj_Member->getRow($where_member);
	                $former_room_key = $room_key;
                }

                $where_meeting = "meeting_key = '".$time_meeting["meeting_key"]."'";
                $meeting_data = $this->obj_Meeting->getRow($where_meeting);

                // ONEアカウントの場合、マスターIDが出力されるので memberTableのvcube_one_member_idを表示
                if($this->check_one_account()){
                    $list_data[] = array(
                        "start_time" => $time_meeting["start_time"],
                        "connect_time" => $time_meeting["connect_time"],
                        "end_time" => $time_meeting["end_time"],
                        "member_id" => $member_data["vcube_one_member_id"],
                        "meeting_room_name" => $meeting_data["meeting_room_name"],
                        "participant_names" => '"'.str_replace(array("\r\n","\n","\r"), "," , $meeting_data["participant_names"]).'"',
                    );
                }else{
                    $list_data[] = array(
                        "start_time" => $time_meeting["start_time"],
                        "connect_time" => $time_meeting["connect_time"],
                        "end_time" => $time_meeting["end_time"],
                        "member_id" => $member_data["member_id"],
                        "meeting_room_name" => $meeting_data["meeting_room_name"],
                        "participant_names" => '"'.str_replace(array("\r\n","\n","\r"), "," , $meeting_data["participant_names"]).'"',
                    );
                }
            }
        }
        //$this->logger2->info($list_data);
        //タイムゾーン取得
        $time_zone = $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE);
        $this->template->assign("time_zone",$time_zone);

        // CSV出力
        header('Pragma: Public');
        header('Cache-Control: public');
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="time_'.date("Ymd").'.csv"');
        $this->template->assign("list_data",$list_data);
        $this->template->assign("start_date",$start_date);
        $this->template->assign("end_date",$end_date);
	    $frame["lang"] = $this->get_language();
        $this->template->assign('__frame',$frame);
        $output = mb_convert_encoding($this->template->fetch('common/admin/log/time.t.csv'), $this->get_output_csv_encoding());
        print $output;

    }

    function action_get_inbound_customer_log () {
        $user_info = $this->session->get("user_info");
        $inbound_id = $this->request->get("inbound_id");
        $where = "inbound_id = '".$inbound_id."'";
        $inbound_info = $this->obj_Inbound->getRow($where, "inbound_id, inbound_name, inbound_sort");


        ////ログ取得開始日を取得
        //$startYear = $this->request->get("inboundStartYear");
        //$startMonth = $this->request->get("inboundStartMonth");
        //$startDay = $this->request->get("inboundStartDay");
        //
        ////ログ取得終了日を取得
        //$endYear = $this->request->get("inboundEndYear");
        //$endMonth = $this->request->get("inboundEndMonth");
        //$endDay = $this->request->get("inboundEndDay");
        //
        //$start_date = date('Y-m-d H:i:s', mktime(0, 0, 0, $startMonth, $startDay, $startYear));
        //$end_date = date('Y-m-d H:i:s', mktime(23, 59, 59, $endMonth, $endDay, $endYear));

        //ログ取得開始日を取得
        $startDate = $this->request->get("inboundStartDate");

        //ログ取得終了日を取得
        $endDate = $this->request->get("inboundEndDate");


        $start_date = date('Y-m-d H:i:s', mktime(0, 0, 0, date("n", strtotime($startDate)), date("j", strtotime($startDate)), date("Y", strtotime($startDate))));
        $end_date = date('Y-m-d H:i:s', mktime(23, 59, 59, date("n", strtotime($endDate)), date("j", strtotime($endDate)), date("Y", strtotime($endDate))));

        //指定期間のインバウンドログ取得
        require_once("classes/dbi/inbound_log.dbi.php");
        $obj_InboundLog = new InboundLogTable($this->get_dsn());
        $where_inbound = "user_key = ".$user_info["user_key"].
                         " AND inbound_id= '".$inbound_id."'".
                         " AND event = 'onClick'";
        if($start_date != ""){
            $where_inbound .= " AND create_datetime >= '".addslashes($start_date). "'";
        }
        if($end_date != ""){
            $where_inbound .= " AND create_datetime <= '".addslashes($end_date). "'";
        }
        $inbound_click_log = $obj_InboundLog->getRowsAssoc($where_inbound, array("create_datetime" => "asc"), null , null, "meeting_key, participant_key, remote_addr, create_datetime");

        //会議キーがあった場合は情報取得
        if ($inbound_click_log) {
            require_once ("classes/core/dbi/Participant.dbi.php");
            $obj_Participant = new DBI_Participant ($this->get_dsn());
            require_once ("classes/dbi/meeting.dbi.php");
            $obj_Meeting = new MeetingTable ($this->get_dsn());
            foreach ($inbound_click_log as $log) {
                $value = "";
                $value["remote_addr"] = $log["remote_addr"];
                $value["click_time"] = date('Y-m-d H:i:s', EZDate::getLocateTime( $log["create_datetime"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE));
                if ($log["meeting_key"]) {
                    $where_customer = "participant_key = ".$log["participant_key"];
                    $customer_info = $obj_Participant->getRow($where_customer, "participant_name, uptime_start, use_count");
                    $value["participant_name"] = $customer_info["participant_name"];
                    if ($customer_info["uptime_start"]) {
                        $value["meeting_start_time"] = date('Y-m-d H:i:s', EZDate::getLocateTime( $customer_info["uptime_start"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE));
                        $value["meeting_use_minute"] = $customer_info["use_count"];
                    }
                }
                $inbound_log[] = $value;
                $this->logger2->trace($inbound_log);
            }
        }
        //タイムゾーン取得
        $time_zone = $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE);
        $this->template->assign("time_zone",$time_zone);

        // CSV出力
        header('Pragma: Public');
        header('Cache-Control: public');
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="customer_'.date("Ymd").'.csv"');
        $this->template->assign("inbound_info", $inbound_info);
        $this->template->assign("list_data",$inbound_log);
        $this->template->assign("start_date",$start_date);
        $this->template->assign("end_date",$end_date);
	    $frame["lang"] = $this->get_language();
        $this->template->assign('__frame',$frame);
        $output = mb_convert_encoding($this->fetch('common/admin/log/customer.t.csv'), $this->get_output_csv_encoding());
        print $output;
    }

    function action_get_staff_log () {
        //利用状況を取得
        $room_key = $this->request->get("room_key");

        ////ログ取得開始日を取得
        //$startYear = $this->request->get("staffStartYear");
        //$startMonth = $this->request->get("staffStartMonth");
        //$startDay = $this->request->get("staffStartDay");
        //
        ////ログ取得終了日を取得
        //$endYear = $this->request->get("staffEndYear");
        //$endMonth = $this->request->get("staffEndMonth");
        //$endDay = $this->request->get("staffEndDay");
        //
        //$start_date = date('Y-m-d H:i:s', mktime(0, 0, 0, $startMonth, $startDay, $startYear));
        //$end_date = date('Y-m-d H:i:s', mktime(23, 59, 59, $endMonth, $endDay, $endYear));


        //ログ取得開始日を取得
        $startDate = $this->request->get("staffStartDate");

        //ログ取得終了日を取得
        $endDate = $this->request->get("staffEndDate");


        $start_date = date('Y-m-d H:i:s', mktime(0, 0, 0, date("n", strtotime($startDate)), date("j", strtotime($startDate)), date("Y", strtotime($startDate))));
        $end_date = date('Y-m-d H:i:s', mktime(23, 59, 59, date("n", strtotime($endDate)), date("j", strtotime($endDate)), date("Y", strtotime($endDate))));

        //部屋名取得
        require_once ("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'";
        $room_name = $obj_Room->getOne($where, "room_name");
        //ログ取得
        $log_list = $this->getSalesLog($room_key,$start_date,$end_date);


        $time_zone = $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE);
        $this->template->assign("time_zone",$time_zone);

        // CSV出力
        header('Pragma: Public');
        header('Cache-Control: public');
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="staff_'.date("Ymd").'.csv"');

        $this->template->assign("list_data",$log_list);
        $this->template->assign("room_name", $room_name);
        $this->template->assign("start_date",$start_date);
        $this->template->assign("end_date",$end_date);
	    $frame["lang"] = $this->get_language();
        $this->template->assign('__frame',$frame);
        $output = mb_convert_encoding($this->fetch('common/admin/log/staff.t.csv'), $this->get_output_csv_encoding());
        print $output;
    }

    function getSalesLog($room_list,$start_date,$end_date){
    	$eventlog = array();
    	if ($room_list) {
    		$room_keys = "";
    		if(!is_array($room_list)) {
    			$room_keys = "'".addslashes($room_list)."'";
    		} else {
    			foreach($room_list as $room){
    				if ($room) {
    					if($room_keys != ""){
    						$room_keys .= ",";
    					}
    					$room_keys .= "'".addslashes($room)."'";
    				}
    			}
    		}
    		$where = "actual_start_datetime > 0 AND uptime_start > 0";
    		$where .= " AND room_key in (".$room_keys.")";
    		// start_dateがあったら以降取得
    		if($start_date != ""){
    			$where .= " AND meeting.create_datetime >= '".addslashes($start_date). "'";
    		}
    		// end_dateがあったら以前を取得
    		if($end_date != ""){
    			$where .= " AND meeting.create_datetime <= '".addslashes($end_date). "'";
    		}
    		$table = "participant";
    		$relate = "meeting.meeting_key = participant.meeting_key";
    		$column = "meeting.meeting_key, room_key, actual_start_datetime, actual_end_datetime, uptime_start, uptime_end, participant_name, participant_type_key";
    		$meetinglog_list = $this->obj_Meeting->innerJoin($table, $relate, $where, array("meeting.create_datetime" => "asc"),null,0, $column);

    		foreach($meetinglog_list as $meetinglog) {
    			$mk = $meetinglog["meeting_key"];
    			$eventlog[$mk]["meeting_key"] = $mk;
    			$eventlog[$mk]["room_key"] = $meetinglog["room_key"];
    			if($meetinglog["participant_type_key"] == "16"){
    				$eventlog[$mk]["staff_name"] = $meetinglog["participant_name"];
    			}elseif($meetinglog["participant_type_key"] != "23"){
        				$eventlog[$mk]["customer_name"] = $meetinglog["participant_name"];
        				$eventlog[$mk]["start_time"] = $meetinglog["uptime_start"];
        				$eventlog[$mk]["end_time"] = $meetinglog["uptime_end"];
        				$eventlog[$mk]["connect_time"] = strtotime($meetinglog["uptime_end"]) - strtotime($meetinglog["uptime_start"]);
    			}
    		}
    	}
    	return $eventlog;
    }

    public function getTimeDiff($start, $end){
        return ( strtotime($end) - strtotime($start) ) / 60 / 60 / 24;
    }

}
$main =& new AppAdminLog();
$main->execute();
?>