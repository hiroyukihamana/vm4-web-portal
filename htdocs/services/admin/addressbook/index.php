<?php

require_once ('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/address_book.dbi.php');
require_once ('classes/dbi/user.dbi.php');
require_once ('config/config.inc.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
require_once ('classes/dbi/address_book_group.dbi.php');

class AppAdminAddressBook extends AppFrame
{
    var $logger = null;
    var $_name_space = null;
    var $objAddressBook = null;
    var $groupTable = null;

    function init() {
 		$this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->objUser          = new UserTable($this->get_dsn());
        $this->objAddressBook   = new AddressBookTable($this->get_dsn());
        $this->groupTable       = new AddressBookGroupTable($this->get_dsn());
        //$this->max_address_cnt  = $this->config->get("N2MY", "address_book_count");
        $this->max_address_cnt  = 2000;
        $this->max_address_ex   = 500;
        $this->_name_space      = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->action_show_top();
    }

    /**
     * top画面
     */
    function action_show_top() {
        
       $this->display('admin/member/index.t.html');
    }

    function action_admin_address_book(){
        $this->session->remove("conditions");
        $this->render_admin_address_book();
    }
    
    /**
     * アドレス帳のメインページ
     */
    function render_admin_address_book($message=""){
        $this->set_submit_key($this->_name_space);
        if($message){
            $this->template->assign('message', $message);
        }
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        // address_book_group get
        $address_book_group_list = $this->groupTable->getRowsAssoc(sprintf("user_key = '%s' AND status = '1'", $user_key));
        $this->template->assign('address_book_group_list', $address_book_group_list);
        $conditions = $this->getCond();
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$conditions);
        $sort_key  = ($conditions["sort_key"]) ? $conditions["sort_key"] : "address_book_group_key";
        $sort_type = ($conditions["sort_type"]) ? $conditions["sort_type"] : "desc";
        $sort_key_rules = array("address_book_group_key","name","email");
        if (!EZValidator::valid_allow($sort_key, $sort_key_rules)) {
            $sort_key = "address_book_group_key";
        }
        $sort_type_rules = array("asc","desc");
        if (!EZValidator::valid_allow($sort_type, $sort_type_rules)) {
            $sort_type = "desc";
        }
        $limit = $this->request->get("page_cnt");
        if (!$limit) {
            $limit = 10;
        }
        $page      = $this->request->get("page", 1);
        $offset    = ($page - 1) * $limit;
        // address_book_info get
        $where = "is_deleted = '0' AND member_key is NULL AND user_key = '" .addslashes($user_key)."'";
        if($conditions['address_book_group_key']){
            $where .= " AND address_book_group_key='".addslashes($conditions['address_book_group_key'])."'";
        }
        if($conditions['name']){
            $where .= " AND (name like '%".addslashes($conditions["name"])."%'";
            $where .= " OR email like '%".addslashes($conditions["name"])."%')";
            
        }
        $address_book_list = $this->objAddressBook->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $count = $this->objAddressBook->numRows($where);
        $address_book_info = array();
        foreach ($address_book_list as $address_book_sample){
            $address_book_group_key = $address_book_sample['address_book_group_key'];
            $address_book_info[] = array(
                'name'  => $address_book_sample['name'],
                'email' => $address_book_sample['email'],
                'key'   => $address_book_sample['address_book_key'],
                'group_key'  => $address_book_sample['address_book_group_key'],
                'group_name' => $address_book_group_key == '0' ? '' : $this->groupTable->getOne(sprintf("address_book_group_key = '%s'", $address_book_sample['address_book_group_key']), 'group_name')
            );
        }
        $this->template->assign('address_book_info', $address_book_info);
        $this->template->assign('conditions', $conditions);
        $pager = $this->setPager($limit, $page, $count);
        $this->template->assign('pager', $pager);
        $this->display('admin/addressbook/index.t.html');
        
    }
    
    /**
     * アドレス帳の検索
     */
    function action_search(){
        $this->setCond();
        $this->render_admin_address_book();
    }
    
    /**
     * アドレス帳の追加
     */
    function action_add($message = ''){
        $this->set_submit_key($this->_name_space);
        if($message){
            $this->template->assign('message', $message);
        }
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        $group_list = $this->groupTable->getRowsAssoc( sprintf( "user_key='%s' AND status='1'", $user_key ) );
        $address_book_group_list = array();
        foreach( $group_list as $group_info ){
            $address_book_group_list[] = array(
                    "key" => $group_info['address_book_group_key'],
                    "name" => $group_info['group_name']
            );
        }
        $this->template->assign('address_book_group_list', $address_book_group_list);

        $session_data = $this->session->get('add_addressbook_info');
        $address_book_info = array();
        $address_book_info['group_name']               = $session_data['group_name'];
        $address_book_info['address_book_group_key']   = $session_data['address_book_group_key'];
        $address_book_info['name']                     = $session_data['name'];
        $address_book_info['name_kana']                = $session_data['name_kana'];
        $address_book_info['email']                    = $session_data['email'];
        $address_book_info['lang']                     = $session_data['lang'];
        $address_book_info['timezone']                 = $session_data['timezone'];
        $this->template->assign('address_book_info', $address_book_info);
        $this->session->remove('add_addressbook_info');
        $this->display('admin/addressbook/add/index.t.html');
    }
    
    function action_add_confirm(){
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_admin_address_book();
        }
        $request = $this->request->getAll();
        $user_info = $this->session->get('user_info');
        $address_book_group_key = $request['group_name']=='0'? 0 : $this->groupTable->getOne(sprintf("user_key = '%s' AND group_name='%s' AND status='1'", $user_info['user_key'], addslashes($request['address_book_group'])), 'address_book_group_key') ;
        $address_book_info = array();
        $address_book_info['address_book_key']         = $request['address_book_key'];
        $address_book_info['address_book_group_key']   = $address_book_group_key;
        $address_book_info['name']                     = $request['name'];
        $address_book_info['name_kana']                = $request['name_kana'];
        $address_book_info['email']                    = $request['email'];
        $address_book_info['group_name']               = $request['address_book_group'];
        $address_book_info['lang']                     = $request['lang'];
        $address_book_info['timezone']                 = $request['timezone'];
        $this->session->set('add_addressbook_info', $address_book_info);
        if( 100 == $address_book_info['timezone'] ){
            $timezone_info = MEMBER_UNDEFINED_TIMEZONE;
        } else {
            $timezoneList = $this->get_timezone_list();
            foreach( $timezoneList as $k => $zone ){
                if( $zone["key"] == $address_book_info['timezone'] ){
                    $timezone_info = $zone["value"];
                    break 1;
                }
            }
        }
        $this->template->assign( 'timezoneInfo', $timezone_info );
        
        $message = null;
        if (!$request['name']) {
            $message .= '<li>'.MEMBER_ERROR_NAME . '</li>';
        }
        if ($request['name'] && mb_strlen($request['name']) > 50){
            $message .= '<li>'.ADDRESS_NAME_LENGTH .'</li>';
        }
        if (!$request['email']) {
            $message .= '<li>'.MEMBER_ERROR_EMAIL . '</li>';
        }
        if ($request['email'] != "") {
            if( ! EZValidator::valid_email( $request['email'] ) ){
                $message .= '<li>'.MEMBER_ERROR_EMAIL_INVALID . '</li>';
            } elseif ( $this->duplicateEmail( $user_info["user_key"], $request['email']) ){
                $message .= '<li>'.MEMBER_ERROR_EMAIL_DUPLICATE . '</li>';
            }
        }
        if($message){
            $this->action_add($message);
        }else{
            $this->template->assign('address_book_info', $address_book_info);
            $this->display('admin/addressbook/add/confirm.t.html');
        }
    }
    
    function action_add_complete(){
        $request = $this->session->get('add_addressbook_info');
        $user_info = $this->session->get('user_info');
        $data = array(
        	'name'                   => $request['name'],
            'name_kana'              => $request['name_kana'],
            'email'                  => $request['email'],
            'timezone'               => $request['timezone'],
            'lang'                   => $request['lang'],
            'is_deleted'             => 0,
            'address_book_group_key' => $request['address_book_group_key'] ? $request['address_book_group_key'] : 0,
        );
        if($info = $this->existingEmail($user_info['user_key'], $request['email'])){
            $ret = $this->objAddressBook->update($data, sprintf( "address_book_key='%s'", addslashes($info['address_book_key']) ) );
        }else{
            $data['user_key'] = $user_info['user_key'];
            $ret = $this->objAddressBook->add($data);
        }
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        }
        $this->session->remove('add_addressbook_info');
        $this->display('admin/addressbook/add/done.t.html');
        
    } 
    
    /**
     * アドレス帳の編集
     */
    function action_edit($message = ''){
        $this->set_submit_key($this->_name_space);
        if($message){
            $this->template->assign('message', $message);
        }
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        $group_list = $this->groupTable->getRowsAssoc( sprintf( "user_key='%s' AND status='1'", $user_key ) );
        $address_book_group_list = array();
        foreach( $group_list as $group_info ){
            $address_book_group_list[] = array(
                    "key" => $group_info['address_book_group_key'],
                    "name" => $group_info['group_name']
            );
        }
        $this->template->assign('address_book_group_list', $address_book_group_list);
        $address_book_info = array();
        $mode = $this->request->get('mode');
        if($mode == "edit"){
            $address_book_key = $this->request->get('address_book_key');
            $info = $this->objAddressBook->getRow( sprintf( "user_key='%s' AND address_book_key='%s'", $user_key , $address_book_key ) );
            // 不正アクセス対策
            if(!$info){
                $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
                return $this->action_admin_address_book();
            }
            $address_book_info['address_book_key']         = $address_book_key;
            $address_book_info['group_name']               = $info['address_book_group_key'] == '0' ? '' : $this->groupTable->getOne(sprintf("address_book_group_key = '%s'", $info['address_book_group_key']), 'group_name');
            $address_book_info['address_book_group_key']   = $info['address_book_group_key'];
        }else{
            $info = $this->session->get('edit_addressbook_info');
            $address_book_info['address_book_key']         = $info['address_book_key'];
            $address_book_info['group_name']               = $info['address_book_group'];
            $address_book_info['address_book_group_key']   = $this->groupTable->getOne(sprintf("user_key = '%s' AND group_name='%s' AND status='1'", $user_info['user_key'], addslashes($info['address_book_group'])), 'address_book_group_key');
        }
        $address_book_info['name']                     = $info['name'];
        $address_book_info['name_kana']                = $info['name_kana'];
        $address_book_info['email']                    = $info['email'];
        $address_book_info['lang']                     = $info['lang'];
        $address_book_info['timezone']                 = $info['timezone'];
        $this->template->assign('address_book_info', $address_book_info);
        $this->display('admin/addressbook/edit/index.t.html');
    }
    
    /**
     * アドレス帳の検索確認ページ
     */
    function action_edit_confirm(){
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_admin_address_book();
        }
        $request = $this->request->getAll();
        $this->session->set('edit_addressbook_info', $request);
        $user_info = $this->session->get( "user_info");
        $message = null;
        if (!$request['name']) {
            $message .= '<li>'.MEMBER_ERROR_NAME . '</li>';
        }
        if ($request['name'] && mb_strlen($request['name']) > 50){
            $message .= '<li>'.ADDRESS_NAME_LENGTH .'</li>';
        }
        if (!$request['email']) {
            $message .= '<li>'.MEMBER_ERROR_EMAIL . '</li>';
        }
        if ($request['email'] != "") {
            if( ! EZValidator::valid_email( $request['email'] ) ){
                $message .= '<li>'.MEMBER_ERROR_EMAIL_INVALID . '</li>';
            } else{
                $where = sprintf( "user_key = '%s' AND email = '%s' AND is_deleted = 0 AND member_key is NULL", $user_info['user_key'], $request['email']);
                $infos = $this->objAddressBook->getRowsAssoc($where);
                if($infos){
                    foreach ($infos as $info){
                        if($info['address_book_key'] != $request['address_book_key']){
                            $message .= '<li>'.MEMBER_ERROR_EMAIL_DUPLICATE . '</li>';
                            break;
                        }
                    }
                }
            }
        }
        if($message){
            $this->action_edit($message);
        }else{
            $user_info = $this->session->get('user_info');
            $address_book_group_key = $this->groupTable->getOne(sprintf("user_key = '%s' AND group_name='%s' AND status='1'", $user_info['user_key'], addslashes($request['address_book_group'])), 'address_book_group_key');
            $address_book_info = array();
            $address_book_info['address_book_key']         = $request['address_book_key'];
            $address_book_info['name']                     = $request['name'];
            $address_book_info['email']                    = $request['email'];
            $address_book_info['address_book_group_key']   = $address_book_group_key;
            $address_book_info['group_name']               = $request['address_book_group'];
            $address_book_info['name_kana']                = $request['name_kana'];
            $address_book_info['lang']                     = $request['lang'];
            $address_book_info['timezone']                 = $request['timezone'];
            $this->template->assign('address_book_info', $address_book_info);
            if( 100 == $address_book_info['timezone'] ){
                $timezone_info = MEMBER_UNDEFINED_TIMEZONE;
            } else {
                $timezoneList = $this->get_timezone_list();
                foreach( $timezoneList as $k => $zone ){
                    if( $zone["key"] == $address_book_info['timezone'] ){
                        $timezone_info = $zone["value"];
                        break 1;
                    }
                }
            }
            $this->template->assign( 'timezoneInfo', $timezone_info );
            $this->display('admin/addressbook/edit/confirm.t.html');
        }
    }
    
    function action_edit_complete(){
        $request = $this->session->get('edit_addressbook_info');
        $user_info = $this->session->get('user_info');
        $address_book_group_key = $this->groupTable->getOne(sprintf("user_key = '%s' AND group_name='%s' AND status='1'", $user_info['user_key'], addslashes($request['address_book_group'])), 'address_book_group_key');
        $data = array(
                "name"                     => $request['name'],
                "name_kana"                => $request['name_kana'],
                "email"                    => $request['email'],
                "timezone"                 => $request['timezone'],
                "lang"                     => $request['lang'],
                "address_book_group_key"   => $address_book_group_key
        );
        $ret = $this->objAddressBook->update($data, sprintf( "address_book_key='%s' ", addslashes($request['address_book_key']) ) );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } 
        $this->session->remove('edit_addressbook_info');
        $this->display('admin/addressbook/edit/done.t.html');
    }
    
    /**
     * アドレス帳の削除
     */
    function action_delete(){
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_admin_address_book();
        }
        $user_info = $this->session->get( "user_info");
        $address_book_key = $this->request->get('address_book_key');
        $address_book_info = $this->objAddressBook->getRow( sprintf( "user_key='%s' AND address_book_key='%s'",$user_info["user_key"] , $address_book_key ) );
        // 不正アクセス対策
        if(!$address_book_info){
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
            return $this->action_admin_address_book();
        }
        $data['is_deleted'] = 1;
        $ret = $this->objAddressBook->update($data, sprintf( "address_book_key = '%s'", $address_book_key ) );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        } 
        $message .= "<li>".ADDRESS_DELETE_ADDRESS . "</li>";
        $this->render_admin_address_book($message);
        
    }
    
    /**
     * 検索とソート条件取得
     */
    function getCond() {
        $condition = $this->session->get("conditions");
        return $condition;
    }
    
    /**
     * 検索とソート条件設定
     */
    function setCond() {
        $conditions  = $this->request->get("conditions");
        if ($conditions) {
            $this->session->set("conditions", $conditions);
        }
    }
    
    function action_pager() {
        $this->render_admin_address_book();
    }
    
    /**
     * 既存の削除したメール検索
     * yes: アドレス帳情報
     * no : false
     */
    function existingEmail( $user_key, $email, $member_key=null ){
        $where = $member_key ?
        sprintf( "user_key = '%s' AND email = '%s' AND member_key  = '%s'", $user_key, $email, $member_key ):
        sprintf( "user_key = '%s' AND email = '%s' AND member_key is NULL", $user_key, $email );
        $addressInfo = $this->objAddressBook->getRow( $where );
        return $addressInfo ? $addressInfo : false;
    }
    
    /**
     * メール重複性検査
     * yes: true
     * no : false
     */
    function duplicateEmail( $user_key, $email, $member_key=null )
    {
        $where = $member_key ?
        sprintf( "user_key = '%s' AND email = '%s' AND is_deleted = 0 AND member_key  = '%s' ", $user_key, $email, $member_key):
        sprintf( "user_key = '%s' AND email = '%s' AND is_deleted = 0 AND member_key is NULL", $user_key, $email);
        $addressInfo = $this->objAddressBook->numRows( $where );
        return $addressInfo > 0 ? true : false;
    }
    
    /**
     * テンプレートダウンロード
     */
    function action_format_download() {
    	$output_encoding = $this->get_output_encoding();
    	$csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");

        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="sample_addressbook.csv"');

        // ヘッダ
        $header = array("name", "name_kana", "email", "lang(ja.en.zh-cn.zh-tw.fr.th.in.ko)","group_name", "timezone", "flag","error");
        // サンプル行
        $sample = array("sample","sample","vcube@example.jp","en","","","A","This is a sample data. Please delete.");
        $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->setHeader($header);
        $csv->write($header);
        $csv->write($sample);
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
    }

    function action_add_csv(){
    	$this->template->assign('max_address_cnt',  $this->max_address_ex);
        $this->display('admin/addressbook/csv_index.t.html');
    }
    /**
     * インポート
     */
    function action_import() {
        $message = "";
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];

        //現段階でアドレス帳に登録されている件数
        $sql = "SELECT count(*) FROM address_book";
        $where = " where user_key =".$user_key.
                 " AND member_key IS NULL".
                 " AND is_deleted = 0";
        $sql .= $where;
        $count = $this->objAddressBook->_conn->getOne($sql);
        if (DB::isError($count)) {
            print $count->getUserInfo();
        }

        // ファイルアップロード
        $request["upload_file"] = $_FILES["import_file"];
        $data = pathinfo($request["upload_file"]["name"]);

        //CSVファイルチェック
        if (empty($data['filename'])) {
            $message .= CSV_NO_FILE_ERROR;
            return $this->action_import_complete($message);
        } elseif($data['extension'] != "csv") {
            $message .= CSV_FILE_FORMAT_ERROR ;
            return $this->action_import_complete($message);
        }

        //ローカル保管
        $filename = $this->get_work_dir()."/".tmpfile();
        $this->logger2->debug($filename);
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $this->session->set('address_book_info', $request);
        // メッセージ
        $msg_format = $this->get_message("error");
        $msg = $this->get_message("ERROR");
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open($filename, "r");
        $rows = array();
        $err_flg = 0;
        $i = 0;

        //正しいheader
        $head_ex = "name,name_kana,email,lang(ja.en.zh-cn.zh-tw.fr.th.in.ko),group_name,timezone,flag,error";
        //取り込んだファイルのheder
        $h_ar = implode(",", $csv->getHeader());
        if($head_ex !== $h_ar) {
            $message .= CSV_FILE_FORMAT_ERROR;
            return $this->action_import_complete($message);
        }
        //現在登録されているデータ取得
        $where = "user_key = " . $user_key.
        " AND member_key IS NULL";
        $_list_ad = $this->objAddressBook->getRowsAssoc($where);
        while($row = $csv->getNext(true)) {

            $row["user_key"] = $user_key;
            $row["lang"]     = $row["lang(ja.en.zh-cn.zh-tw.fr.th.in.ko)"];
            $row["memo"]     = NULL;
            if(empty($row["timezone"])) {
                $row["timezone"] = 100;
            }
            //名前変わるので消す。
            unset($row["error"]);
            unset($row["lang(ja.en.zh-cn.zh-tw.fr.th.in.ko)"]);

            //削除flagの場合、is_deletedに1。
            if($row["flag"] == "D") {
                $row["is_deleted"] = 1;
            } elseif($row["flag"] == "A"){
                $i++;
                $row["is_deleted"] = 0;
            }
            
            //削除と更新しないフラグの時はcheckしないよ☆
                if($row["flag"] != "X") {
                    $error = array();
                    if(!$this->address_check($row)) {
                        $err_fields = $this->_error_obj->error_fields();
                        foreach ($err_fields as $key) {
                            $type = $this->_error_obj->get_error_type($key);
                            $err_rule = $this->_error_obj->get_error_rule($key, $type);
                            if (($type == "allow") || $type == "deny") {
                                $err_rule = join("，", $err_rule);
                            }
                            if($key == "timezone"){
                                $err_rule = "-12～14、100";
                            }
                            $_msg = sprintf($msg[$type], $err_rule);
                            $error[$key] = $_msg;
                            $err_flg = 1;
                        }
                    }

                    if($row["flag"] == "D" && $row["is_deleted"] == 1) {
                        foreach($_list_ad as $_Lad) {
                            if($row["email"] == $_Lad["email"] && $_Lad["is_deleted"] == 1) {
                                $key = "delete";
                                $_msg = CSV_RECORD_ERROR;
                                $error[$key] = $_msg;
                                $err_flg = 1;
                                break;
                            }
                        }
                    }
                    $row["error"] = $error;
                }
            $rows[] = $row;
        }
        
        //空のファイルだったら。
        if(empty($rows)) {
            $message .= CSV_FILE_FORMAT_ERROR;
            return $this->action_import_complete($message);
        }

        //インポートファイル削除
        unlink($filename);

        //上限件数チェック
        if( $i > $this->max_address_ex) {
            $message .= CSV_LIMIT_ERROR;
            return $this->action_import_complete($message);
            break;
        }
        $address_count = $i + $count;
        if( $address_count > $this->max_address_cnt) {
            $message .= CSV_LIMIT_UPLOAD_ERROR;
            return $this->action_import_complete($message);
            break;
        }

        //エラー数チェック。
        $a_add = array();
        $a_err = array();
        foreach($rows as $ad_val) {
            if($ad_val["error"]) {
                $a_err[] = $ad_val;
            } else {
                $a_add[] = $ad_val;
            }
        }

        //処理
        foreach($a_add as $ad_val) {
            // 既にメールアドレスが登録されていたら編集フラグにします。
        	$array = array();
        	foreach ($ad_val as $key => $value){
        		if($key != "group_name"){
        			$array[$key] = $value;
        		}else{
        			$isCreate = ($ad_val["flag"] == "A");
        			$array["address_book_group_key"] = ($this->_isGroupExist($value, $isCreate))?($this->_isGroupExist($value, $isCreate)):0;
        		}
        	}
        	$ad_val = $array;
            $where = sprintf("user_key = %s AND email = '%s' AND member_key IS NULL",$user_key , addslashes($ad_val["email"]));
            if ($ad_val["flag"] == "A" && $this->objAddressBook->numRows($where) > 0){
                $ad_val["flag"] = "M";
            }
            if($ad_val["flag"] == "A") { //登録
                unset($ad_val["flag"]);
                unset($ad_val["error"]);
                $ret = $this->address_book_add($ad_val);
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                } else {
                    $this->logger->debug(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,array($rows["user_key"], $rows["email"]));
                }
            } elseif($ad_val["flag"] == "M") { //編集
                //編集
                unset($ad_val["flag"]);
                unset($ad_val["error"]);
                $where = sprintf("user_key = %s AND email = '%s' AND member_key IS NULL",$user_key , addslashes($ad_val["email"]));
                $ret = $this->address_book_update($ad_val,$where);
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                } else {
                    $this->logger->debug(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,array($rows["user_key"], $rows["email"]));
                }
            }  elseif($ad_val["flag"] == "D") {//削除
                 //削除
                 unset($ad_val["flag"]);
                 unset($ad_val["error"]);
                 $where = sprintf("user_key = %s AND email = '%s'  AND member_key IS NULL",$user_key , addslashes($ad_val["email"]));
                 $ret = $this->address_book_update($ad_val,$where);
                 if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                 } else {
                    $this->logger->debug(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,array($rows["user_key"], $rows["email"]));
                 }
            } elseif($ad_val["flag"] == "X") {//変更しない {
                $this->logger->debug(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,array($rows["user_key"], $rows["email"]));
            }
        }

        //エラー画面もしくは完了画面
        if(!empty($a_err)) {
            return $this->action_err_complete($rows);
        } else {
            return $this->action_import_complete();
        }

     }


    /**
     * エラー完了画面
     */
    function action_err_complete($rows="") {
        if(!empty($rows)) {
            $_SESSION['rows'] = $rows;
            $this->display('admin/addressbook/err_complete.t.html');
        } else {
            $this->display('admin/addressbook/index.t.html');
        }
    }


    /**
     * エラー内容ダウンロード
     */
    function action_err_csv_download() {
        $rows = $_SESSION['rows'];
        if(!empty($rows)) {
            // エラーの出力
            $output_encoding = $this->get_output_encoding();
            $csv = new EZCsv(true,$output_encoding,"UTF-8");
            $dirName = "/address";
            $dir = $this->get_work_dir().$dirName;
            if( !is_dir( $dir ) ){
                mkdir( $dir );
                chmod( $dir ,0777 );
            }
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");

            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="address_book_err.csv"');

            // ヘッダ
            $header = array(
                "name"       => "name",
                "name_kana"  => "name_kana",
                "email"      => "email",
                "lang"       => "lang(ja.en.zh-cn.zh-tw.fr.th.in.ko)",
            	"group_name" => "group_name",
                "timezone"   => "timezone",
                "flag"       => "flag",
                "memo"       => "error"
            );

            $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $header);

            $csv->setHeader($header);
            $csv->write($header);

            foreach($rows as $a_key => $add_row) {

                $err_list["name"]           = $add_row["name"];
                $err_list["name_kana"]      = $add_row["name_kana"];
                $err_list["email"]          = $add_row["email"];
                $err_list["lang"]           = $add_row["lang"];
                $err_list["group_name"]     = $add_row["group_name"];
                $err_list["timezone"]       = $add_row["timezone"];
                $err_list["flag"]           = $add_row["flag"];

                if($add_row["error"]) {
                    $err = array();
                    foreach($add_row["error"] as $e_key => $err_mg) {
                        $err[] = "[".$e_key."]".$err_mg;
                        $err_list["memo"] = implode("，", $err);
                    }
                } else {
                    $err_list["memo"]         = "";
                }
                $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $err_list);
                $csv->write($err_list);

            }

            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            $contents = fread($fp, filesize($tmpfile));
            fclose($fp);
            chmod($tmpfile, 0777);
            print $contents;

            unlink($tmpfile);

        }

        unset($_SESSION['tmp_file']);

    }


    /**
     * add
     */
    function address_book_add($address_book_info) {
        $ret = $this->objAddressBook->add($address_book_info);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        } else {
            $this->logger->debug(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,$address_book_info);
                return true;
            }
    }

    /**
     * edit
     */
    function address_book_update($address_book_info,$where) {
        $ret = $this->objAddressBook->update($address_book_info,$where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        } else {
            $this->logger->debug(__FUNCTION__."#update address_book successful!",__FILE__,__LINE__,$address_book_info);
                return true;
            }
    }

    /**
     * delete
     */
    function address_book_delete($address_book_info,$where) {
        $ret = $this->objAddressBook->update($address_book_info["is_deleted"],$where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        } else {
            $this->logger->debug(__FUNCTION__."#delete address_book successful!",__FILE__,__LINE__,$address_book_info);
                return true;
            }
    }

    /**
     * 完了画面
     */
    function action_import_complete($message="") {
        // エラーを表示用
        if ($message) {
            $this->template->assign('message', $message);
        }
        $this->display('admin/addressbook/complete.t.html');
    }

    /**
     * エクスポート
     */
     function action_export() {
        //ユーザKey取得
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];

        // CSV出力
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");

        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="address_book'.date("Ymd").'.csv"');

        // ヘッダ
        $header = array(
            "name"       => "name",
            "name_kana"  => "name_kana",
            "email"      => "email",
            "lang"       => "lang(ja.en.zh-cn.zh-tw.fr.th.in.ko)",
        	"group_name" => "group_name",	
            "timezone"   => "timezone",
            "flag"       => "flag",
            "memo"       => "error"
        );
        $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->setHeader($header);
        $csv->write($header);

        //一覧
        $where = "user_key = " . $user_key . " AND member_key IS NULL AND is_deleted = 0";
        $sort = array (
          "name" => "asc",
          "email" => "asc"
        );

        $_list = $this->objAddressBook->getRowsAssoc($where, $sort);
        foreach($_list as $_key => $row) {
              $line = array();
              $group_info = $this->groupTable->getRow(sprintf("address_book_group_key='%s'", $row["address_book_group_key"]));
              $line["name"]               = $row["name"];
              $line["name_kana"]          = $row["name_kana"];
              $line["email"]              = $row["email"];
              $line["lang"]               = $row["lang"];
              $line["group_name"]         = $group_info["group_name"];
              $line["timezone"]           = $row["timezone"];
              $line["flag"]               = "X";
              $line["memo"]               = "";

              $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $line);
              $csv->write($line);
        }

        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);

        print $contents;
        unlink($tmpfile);
    }

    /**
     * 厳密な入力チェック
     */
    function address_check($data) {
        $rules = $this->objAddressBook->rules;
        $rules["name"]["regex"]      = "/^[^\\r\\n]*$/D";
        $rules["name_kana"]["regex"] = "/^[^\\r\\n]*$/D";
        $rules["email"]["regex"]     = "/^[^\\r\\n]*$/D";
        $rules["lang"]["allow"]      = array_keys($this->get_language_list());
        $rules["country"]["allow"]   = array_keys($this->get_country_list());
        $rules["timezone"]["allow"]  = $this->get_timezone_list();
        foreach($rules["timezone"]["allow"] as $key => $val){
            $time[] = $val["key"];
            $rules["timezone"]["allow"] = $time;
        }
        array_push($rules["timezone"]["allow"], 100);

        $rules["flag"] = array("required" => true, "allow" => array("A", "D", "X"));

        $this->_error_obj = $this->objAddressBook->check($data, $rules);
        if (EZValidator::isError($this->_error_obj)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 出力するCSVファイルのエンコーディング形式
     */
    function get_output_csv_encoding() {
        // 現在の言語設定で判断。
        $lang = $this->get_language();

        switch($lang) {
            case 'ja_JP':
                return 'SJIS';
            case 'en_US':
            case 'zh_CN':
                return 'UTF-8';
            default:
                return 'SJIS';
        }
    }
    
    function _isGroupExist($group_name, $isCreate = false) {
    	$user_info = $this->session->get('user_info');
    	$group_name = trim($group_name);
    	$where = "group_name = '".$group_name." ' AND status != 0 AND user_key=".$user_info["user_key"];
    	$group_info = $this->groupTable->getRow($where);
    	if($group_info["group_name"] == $group_name){
    		return $group_info["address_book_group_key"];
    	} elseif ($isCreate) {
    		//add group
    		$data = array();
    		$data["group_name"] = $group_name;
    		$data["user_key"] = $user_info["user_key"];
    		$data["status"]   = 1;
    		$key = $this->groupTable->add($data);
    		return $key;
    	} else {
    		return 0;
    	}
    }
    
    function action_address_group($message = null){
    	if ($message) {
    		$this->template->assign('message', $message);
    	}
    	//user_key取得
    	$user_info = $this->session->get('user_info');
    	$user_key = $user_info['user_key'];
    	$where = "user_key =".$user_key." AND status =1";
    	$group_list = $this->groupTable->getRowsAssoc($where);
    	$this->template->assign('info', $group_list);
    
    	$this->display('admin/addressbook/address_group.t.html');
    }
    
    function action_add_group(){
    	$message = "";
    	//user_key取得
    	$user_info = $this->session->get('user_info');
    	$user_key = $user_info['user_key'];
    	$group_name =$this->request->get('group_name');
    	//user_keyからグループを取得
    	if (!$this->request->get('group_name')) {
    		$message .= "<li>".MEMBER_ERROR_GROUP_NAME . "</li>";
    	} elseif (mb_strlen($group_name) > 50) {
    		$message .= "<li>".MEMBER_ERROR_GROUP_NAME_LENGTH . "</li>";
    	} else{
    		$where = "user_key =".$user_key." AND group_name ='".addslashes($group_name)."'";
    		$group_info = $this->groupTable->getRow($where);
    		if($group_info["status"]){
    			$message .= "<li>".MEMBER_ERROR_GROUP_NAME_EXIST . "</li>";
    		}else{
    			$data = array(
    					"user_key" => $user_key,
    					"group_name" => $group_name,
    					"status" =>1
    			);
    			$this->groupTable->add($data);
    			$message .= "<li>".MEMBER_NEW_GROUP_NAME . "</li>";
    		}
    	}
    	// 操作ログ
    	$this->add_operation_log('member_group_add', array('group_name' => $group_name));
    	$this->action_address_group($message);
    }
    
    
    function action_edit_group()
    {
    	$message = "";
    	$user_info = $this->session->get('user_info');
    	$user_key = $user_info['user_key'];
    	$group_name =$this->request->get('group_name');
    	$where = "address_book_group_key = ".$this->request->get('group_key'); 
    	$group_info = $this->groupTable->getRow($where);
    	if(!$group_info){
    		$this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
    		return $this->action_address_group();
    	}
    	//user_keyからグループを取得
    	if (!$this->request->get('group_name')) {
    		$message .= "<li>".MEMBER_ERROR_GROUP_NAME . "</li>";
    	} elseif (mb_strlen($group_name) > 50) {
    		$message .= "<li>".MEMBER_ERROR_GROUP_NAME_LENGTH . "</li>";
    	} elseif ( $this->groupTable->numRows( sprintf( "status = 1 AND user_key='%s' AND group_name='%s'", $user_key, addslashes($group_name) ) ) > 0) {
    		$message .= "<li>".MEMBER_ERROR_GROUP_NAME_EXIST . "</li>";
    	} else {
    		$data = array(
    				"user_key" => $user_key,
    				"group_name" => $group_name
    		);
    		$where = "address_book_group_key = ".$group_info['address_book_group_key']." AND user_key=".$user_key;
    		$this->logger2->info($where);
    		$this->groupTable->update($data, $where);
    		$message .= "<li>".MEMBER_EDIT_GROUP_NAME . "</li>";
    	}
    	// 操作ログ
    	$this->add_operation_log('member_group_edit', array('group_name' => $group_name));
    	$this->action_address_group($message);
    }
    
    function action_delete_group()
    {
    	$message = "";
    	$group_key = $this->request->get('group_key');
    	$group_name =$this->request->get('group_name');
    	$user_info = $this->session->get('user_info');
    	$where = "address_book_group_key = ".$group_key;
    	$group_info = $this->groupTable->getRow($where);
    	// 不正アクセス対策
    	if(!$group_info){
    		$this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
    		return $this->action_address_group();
    	}
    	
    	$where = "address_book_group_key = ".$group_info['address_book_group_key']." AND user_key=".$user_info["user_key"];
    	$data  = array(
    			"status" => 0
    	);
    	$this->groupTable->update($data, $where);
    	
    	$address_book_data = array(
    		"address_book_group_key " => 0
    	);
    	$address_book_where = " address_book_group_key = ".$group_key." AND user_key=".$user_info["user_key"];
    	$this->objAddressBook->update($address_book_data, $address_book_where);
    	
    	
    	$message .= "<li>".MEMBER_DELETE_GROUP_NAME . "</li>";
    	// 操作ログ
    	$this->add_operation_log('member_group_delete', array('group_name' => $group_name));
    	$this->action_address_group($message);
    }
    
}

$main = new AppAdminAddressBook();
$main->execute();
