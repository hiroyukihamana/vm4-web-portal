<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:kiyomizu                                                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once('classes/AppFrame.class.php');
//require_once('classes/authorization.class.php');
require_once("classes/N2MY_Account.class.php");
require_once('config/config.inc.php');

class AppDownloadPage extends AppFrame
{
    /**
     * ログクラス
     * @access public
     */
    var $logger = null;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->user_auth = "invite";
        $this->checkAuth();
    }

    function action_get_flash() {
        $this->render_get_flash();
    }

    function render_get_flash() {
        $this->display('user/download/flash.t.html');
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->render_download();
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
    /**
     * フォームの表示
     */
    function render_download() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $user_info = $this->session->get("user_info");
        $this->logger2->info($user_info);
        if ($user_info['account_model'] == "member") {
            require_once( "classes/dbi/user_plan.dbi.php" );
            $objUserPlan = new UserPlanTable( $this->get_dsn() );
            $plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1");
            $this->logger2->info($plan_info);
            $this->template->assign('plan_info', $plan_info);
        }
        $version_ini = parse_ini_file('setup/version.ini', true);
        $this->template->assign('version', $version_ini);
        if ($this->request->get("action_show_manual")) {
	        $this->display('user/download/manual.t.html');
        } else {
	        $this->display('user/download/index.t.html');
        }
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }
}

$main =& new AppDownloadPage();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
