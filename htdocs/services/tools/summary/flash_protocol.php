<?php
/*
 * Created on 2007/10/29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_DBI.class.php");
require_once("classes/AppFrame.class.php");

class N2MY_SUM_Flash extends AppFrame
{

    function init() {
    	if ($dsn_key = $this->request->get("serverDsnKey")) {
    		$this->dsn = $this->get_dsn_value($dsn_key);
    	} else {
	        $this->dsn = $this->get_dsn();
    	}
    }

    function default_view() {
    	$objFlash = new N2MY_DB($this->get_dsn(), "fms_protocol");
    	$request = $this->request->getAll();
    	$remote_addr = $_SERVER["REMOTE_ADDR"];
    	$data = array(
			"meeting_key"   => $request["meeting_key"],
			"remote_addr"   => $remote_addr,
			"fms_host"      => $request["fms_host"],
			"fms_protocol"  => $request["fms_protocol"],
			"fms_port"      => $request["fms_port"],
			"connect_time"  => $request["connect_time"],
			"create_datetime" => date("Y-m-d H:i:s"),
			);
		$this->logger2->info($data);
    	$ret = $objFlash->add($data);
    	if (DB::isError($ret)) {
    		print "0";
    	} else {
    		print "1";
    	}
    }
}

$main = new N2MY_SUM_Flash();
$main->execute();
?>
