<?php
/*
 * Created on 2007/10/29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/AppFrame.class.php");

class ToolsChecker extends AppFrame
{
    /**
     * シンプルチェッカーの表示
     */
    function default_view() {
        $this->display("user/tools/checker/index.t.html");
    }

    /**
     * 接続先一覧取得
     */
    function action_get_server() {
        require_once "classes/core/dbi/DataCenter.dbi.php";
        require_once "classes/mgm/dbi/FmsServer.dbi.php";
        $objDataCenter = new DBI_DataCenter(N2MY_MDB_DSN);
        $objFmsServer = new DBI_FmsServer(N2MY_MDB_DSN);
        // データセンタ一覧
        $where = "status = 1";
        $data_center_list = $objDataCenter->getRowsAssoc($where);
        $fms_server_list = array();
        foreach($data_center_list as $key => $data_center) {
            // FMSサーバ取得
            $where = "datacenter_key = ".$data_center["datacenter_key"].
                " AND is_available = 1"." AND use_global_link = 0";
            $sort = array(
                "is_ssl" => "asc",
                "server_priority" => "desc",
            );
            $fms_server = $objFmsServer->getRow($where, "server_key, server_address", $sort);
            if ($fms_server) {
                $data_center['fms_host'] = $fms_server["server_address"];
                $fms_server_list[] = $data_center;
            }
        }
        foreach($data_center_list as $key => $data_center) {
            $sv_country = "";
            $data_center_name_ja = $data_center['datacenter_name_ja'] ; 
            $data_center_name_cn = $data_center['datacenter_name_cn'] ; 
            $data_center_name_tw = $data_center['datacenter_name_tw'] ; 
            $data_center_name_fr = $data_center['datacenter_name_fr'] ; 
            $data_center_name_en = $data_center['datacenter_name_en'] ; 
            // FMSサーバ取得
            $where = "datacenter_key = ".$data_center["datacenter_key"].
                " AND is_ssl = 0"." AND server_priority > 0"." AND is_available = 1"." AND use_global_link = 1";
            $sort = array(
                "server_country" => "asc",
                "server_priority" => "desc",
            );
            $fms_server_lst = $objFmsServer->getRowsAssoc($where,$sort);
            foreach($fms_server_lst as $key => $fms_server_row) {
                if ($fms_server_row) {
                    if ($sv_country == $fms_server_row['server_country']) {
                        continue;
                    } 
                    $sv_country = $fms_server_row['server_country'];
                    $data_center_name = "";
                    $data_center_name = $data_center_name_ja . "("  . $fms_server_row['server_country'] . ")"; 
                    $data_center['datacenter_name_ja'] = $data_center_name; 
                    $data_center_name = "";
                    $data_center_name = $data_center_name_cn . "("  . $fms_server_row['server_country'] . ")"; 
                    $data_center['datacenter_name_cn'] = $data_center_name; 
                    $data_center_name = "";
                    $data_center_name = $data_center_name_tw . "("  . $fms_server_row['server_country'] . ")"; 
                    $data_center['datacenter_name_tw'] = $data_center_name; 
                    $data_center_name = "";
                    $data_center_name = $data_center_name_fr . "("  . $fms_server_row['server_country'] . ")"; 
                    $data_center['datacenter_name_fr'] = $data_center_name; 
                    $data_center_name = "";
                    $data_center_name = $data_center_name_en . "("  . $fms_server_row['server_country'] . ")"; 
                    $data_center['datacenter_name_en'] = $data_center_name; 

                    $data_center['fms_host'] = $fms_server_row["server_address"];
                    $fms_server_list[] = $data_center;
                }
            }
        }
        $this->template->assign("fms_server_list", $fms_server_list);
        $lang = $this->request->get("lang") ? $this->request->get("lang") : $this->_lang;
        $this->template->assign("lang", $lang);
        $this->display("user/tools/checker/server.t.xml");
    }

    /**
     * 言語情報取得
     */
    function action_get_language() {
        $this->display("user/tools/checker/language.t.xml");
    }

    /**
     * 設定情報
     */
    function action_get_define() {
        $this->display("user/tools/checker/define.t.xml");
    }
}

$main = new ToolsChecker();
$main->execute();
