<?php
require_once( "classes/AppFrame.class.php" );

class AppEcoSimulator extends AppFrame
{
    function init()
    {
    }

    public function default_view()
    {
        $this->display("eco/eco.simulation.t.html");
    }

    public function action_get_stationlist()
    {
        require_once(realpath(dirname(__FILE__)."/../../../").'/lib/EZLib/Transit.class.php');
        //$transit_log = realpath(dirname(__FILE__)."/../../../var/")."/transit.log";
        $uri = $this->config->get("N2MY", "transit_uri");

        $logger2 = EZLogger2::getInstance($this->config);
        $logger2->open();
        $stationName = urldecode($this->request->get("station"));

        $Transit = Transit::factory($uri);
        $ret1 = $Transit->getStationList($stationName, false);
        $return = array();
        foreach( $ret1 as $key => $value ){
            $return[] = array( "key" => $key, "name" => $value );
        }
        header("Content-type: application/x-javascript; charset=utf-8");
        if( $return ){
            print(json_encode($return));
        }else{
            print( "" );
        }
        exit;
    }

    public function action_get_multistation_route()
    {
        require_once(realpath(dirname(__FILE__)."/../../../").'/lib/EZLib/Transit.class.php');
        $logger2 = EZLogger2::getInstance($this->config);
        $logger2->open();

        $logger2->debug($this->request->getAll());
        $stationList = array();
        $data = array();
        for ($i = 1; $i <= 5; $i++) {
            if ("undefined" != $this->request->get("name".$i)){
                $stationInfo["name"] = urldecode($this->request->get("name".$i));
                $stationInfo["num"] = "undefined" != $this->request->get("num".$i) ? $this->request->get("num".$i) : 1;
                $stationList[] = $stationInfo;
            }
        }
        $endStation = urldecode($this->request->get("dest"));
        $uri = $this->config->get("N2MY", "transit_uri");
        //$transit_log = realpath(dirname(__FILE__)."/../../../var/")."/transit.log";
        $Transit = Transit::factory($uri);
        $ret1 = $Transit->getMultiStationRouteWithNum($stationList, $endStation, false);

        $return = array();
        foreach( $ret1 as $key => $value ){
            $return[] = array( "key" => $key, "name" => $value );
        }
        $data = array(
                    "endpoint"=>$endStation,
                    "startpoint"=>serialize($stationList),
                    "move"=>$return[0]["name"]["total"]["move"] * 2,
                    "time"=>$return[0]["name"]["total"]["time"] * 2,
                    "fare"=>$return[0]["name"]["total"]["fare"] * 2,
                    "co2"=>$return[0]["name"]["total"]["co2"] * 2,
                    "num_trips"=>$this->request->get("numTrips"),
                    "num_expances"=>$this->request->get("numExpances") ? $this->request->get("numExpances") : 0,
                    "entrytime"=>date("Y-m-d H:i:s")
                    );
        require_once(realpath(dirname(__FILE__)."/../../../").'/classes/mgm/dbi/eco_simulator_log.dbi.php');
        $objEco = new EcoSimulatorLogTable($this->get_auth_dsn());
        $objEco->add($data);

        header("Content-type: application/x-javascript; charset=utf-8");
        if( $return[0]["name"]["total"] ){
            print(json_encode($return[0]["name"]["total"]));
        }else{
            print( "" );
        }
        exit;
    }
}

/*
$action = $_REQUEST["action"];
if (function_exists($action)) {
    echo $action($_REQUEST["station"]);
    exit;
}
*/
$main =& new AppEcoSimulator();
$main->execute();
