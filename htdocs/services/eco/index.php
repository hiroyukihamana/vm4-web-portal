<?php
require_once( "classes/AppFrame.class.php" );
require_once( "classes/N2MY_Eco.class.php" );
require_once( "lib/EZLib/EZUtil/EZMath.class.php" );

class AppEco extends AppFrame
{
    var $obj_Eco;

    //杉の木が一日に吸収する量->３０日に変更
//    var $cedar = 1140;    //38 * 30
    var $cedar = 38;    //38 * 30
    var $releaseday = "2008-07-13";

    function init()
    {
        if ($dsn_key = $this->request->get("serverDsnKey", N2MY_DEFAULT_DB)) {
            $this->_dsn = $this->get_dsn_value($dsn_key);
        } else {
            $this->_dsn = $this->get_dsn();
        }
        $this->obj_Eco = new N2MY_Eco( $this->get_dsn(), N2MY_MDB_DSN );
        $this->_name_space = md5(__FILE__);
    }

    public function action_monthly_log()
    {
        $request = $this->request->getAll();
        $validator = new EZValidator($request);
        $rules = array(
            "room_key" => array(
                "allow" => array_keys($this->session->get("room_info")),
                ),
        );
        foreach($rules as $field => $rule) {
            $validator->check($field, $rule);
        }
        // とりあえず警告
        if (EZValidator::isError($validator)) {
            $this->logger2->warn($this->get_error_info($validator));
            return $this->display('error.t.html');
            exit;
        }
        if( 1 == $request["action_monthly_log" ] ){
            $targetdate = date( "Y-m-1" );
        } else {
            $targetdate = $request["date"] ? $request["date"] : date( "Y-m-1" );
        }
        if($request["room_key"]) {
        	$log_info = $this->obj_Eco->getAllEcoLog( $request["room_key"], $targetdate );
        } else {
        	$user_info = $this->session->get("user_info");
        	$log_info = $this->obj_Eco->getAllUserEcoLog( $user_info["user_id"], $targetdate );
        }
        
        $dateArray = date_parse( $targetdate );
        $log_info["room_key"] = $request["room_key"];
        $log_info["thisdate"] = $targetdate;
        $log_info["nowdate"] = date( "Y-m" );
        $log_info["lastdate"] = date( "Y-m-1", mktime( 0, 0, 0, $dateArray["month"] - 1, $dateArray["day"], $dateArray["year"] ) );
        $log_info["nextdate"] = strtotime ( $targetdate ) < strtotime ( date( "Y-m-1" ) ) ? date( "Y-m-d", mktime( 0, 0, 0, $dateArray["month"] + 1, $dateArray["day"], $dateArray["year"] ) ) : "";
        $thistime = date( "n" ) + date( "Y" ) * 12;
        $firsttime = substr( $log_info["all"]["min"], 5, 2 ) + substr( $log_info["all"]["min"], 0, 4 ) * 12;
        $period = ( $thistime - $firsttime ) + 1;

        //average
        /*
        $co2_ave_info = $this->get_co2_info( $log_info["all"]["eco_co2"] / $period );
        $log_info["all"]["eco_ave_co2"] = $co2_ave_info["value"];
        $log_info["all"]["eco_ave_co2_unit"] = $co2_ave_info["unit"];
        $log_info["all"]["eco_ave_tree"] = round( $log_info["all"]["eco_co2"] / $this->cedar, 2 );
        */

        //total
        $co2_all_info = $this->get_co2_info($log_info["all"]["eco_co2"]);
        $log_info["all"]["eco_tree"] = round($log_info["all"]["eco_co2"] / $this->cedar / $log_info["all"]["period"], 2);
        $log_info["all"]["eco_co2"] = $co2_all_info["value"];
        $log_info["all"]["eco_co2_unit"] = $co2_all_info["unit"];
        $eco_all_time = $this->makeTime( $log_info["all"]["eco_time"] );
        $log_info["all"]["eco_hour"] = $eco_all_time["hour"];
        $log_info["all"]["eco_minute"] = $eco_all_time["minute"];

        //今月
//        $log_info["monthly"]["eco_tree"] = round( $log_info["monthly"]["eco_co2"] / $this->cedar, 2 );
//echo$targetdate; echo"<br>";echo date("Y-m-1");

        $thisdate = date_parse($targetdate);
        if ($thisdate["year"].$thisdate["month"] == date("Yn")){
//        if ($targetdate == date("Y-m-1")){
            $period = date("d");
            $log_info["thismonth"] = 1;
        } else {
            $log_info["thismonth"] = 0;
            $period = date("d", mktime(0, 0, 0, $thisdate['month'] + 1, 0, $thisdate['year']));
        }

        $log_info["monthly"]["eco_tree"] = round( $log_info["monthly"]["eco_co2"] / $this->cedar / $period, 2 );
        $co2_info = $this->get_co2_info( $log_info["monthly"]["eco_co2"] );
        $log_info["monthly"]["eco_co2"] = $co2_info["value"];
        $log_info["monthly"]["eco_co2_unit"] = $co2_info["unit"];
        $eco_monthly_time = $this->makeTime( $log_info["monthly"]["eco_time"] );
        $log_info["monthly"]["eco_hour"] = $eco_monthly_time["hour"];
        $log_info["monthly"]["eco_minute"] = $eco_monthly_time["minute"];

        //先月比
//        $log_info["lastMonth"]["eco_tree"] = round($log_info["lastMonth"]["eco_co2"] / $this->cedar, 2);

        //co2 unit
        $max = $this->getMaxData( $log_info["imagedata"]["eco_co2"] );
        $info = $this->get_co2_info( $max );
        $log_info["yearly"]["eco_co2_unit"] = $info["unit"];
        $this->session->set( "log_info", $log_info, $this->_name_space );
        $this->render_monthly_log();
    }

    public function action_meeting_log()
    {
        $request = $this->request->getAll();
        $log_info = $this->obj_Eco->getMeetingLog( $request["id"] );


        $co2 = ($log_info["meeting"]["eco_co2"] < 0) ? 0 : $log_info["meeting"]["eco_co2"];
        $log_info["meeting"]["eco_tree"] = round( $co2 * 52 / 14000, 2 );

        $co2_info = $this->get_co2_info($co2);
        $log_info["meeting"]["eco_co2"] = $co2_info["value"];
        $log_info["meeting"]["eco_co2_unit"] = $co2_info["unit"];
        $log_info["meeting"]["eco_fare"] = number_format( $log_info["meeting"]["eco_fare"] );

        $eco_time = $this->makeTime( $log_info["meeting"]["eco_time"] );
        $log_info["meeting"]["eco_hour"] = $eco_time["hour"];
        $log_info["meeting"]["eco_minute"] = $eco_time["minute"];

        $this->session->set( "log_info", $log_info, $this->_name_space );
        $this->render_meeting_log();
    }

    private function render_monthly_log()
    {
        $log_info = $this->session->get( "log_info", $this->_name_space );
        $this->template->assign( "log_info", $log_info );
        $this->display( "eco/eco.monthly.t.html" );
    }

    private function render_meeting_log()
    {
        $log_info = $this->session->get( "log_info", $this->_name_space );
        $request = $this->request->getAll();
        // ECOメーター情報
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        $obj_Meeting = new MeetingTable( $this->get_dsn() );
        $objParticipant = new DBI_Participant( $this->get_dsn() );
        $where = "meeting_ticket = '".addslashes($request["id"])."'";
        $meeting_info = $obj_Meeting->getRow($where);
        if ($meeting_info) {
            $eco_info = unserialize($meeting_info["eco_info"]);
            // 参加者一覧取得
            $where = "meeting_key = ".addslashes($meeting_info ["meeting_key"]).
                " AND participant_name IS NOT NULL";
    //            " AND use_count > 0";
            $sort = array( "update_datetime"=>"asc");
            $participant_list = $objParticipant->getRowsAssoc($where, $sort);
            // 参加者の最後に入室したステータスだけ取得
            foreach ($participant_list as $row) {
            	    $tmp_count = $participants[$row["participant_session_id"]]["use_count"]?$participants[$row["participant_session_id"]]["use_count"]:0;
                    $participants[$row["participant_session_id"]] = $row;
                    $participants[$row["participant_session_id"]]["use_count"] = $tmp_count + $row["use_count"];
            }
            $this->logger2->info($participants);
            $summary_info["time"] = 0;
            $summary_info["fare"] = 0;
            $summary_info["eco"]  = 0;
            foreach($participants as $key => $participant) {
                if ($participant["participant_station"] != $eco_info["end"]) {
                    $start_station = $participant["participant_station"];
                    $transit_info = $eco_info["start"][$start_station]["summary"];
                    // 往復で計算する
                    $participants[$key]["eco_time"] = $transit_info["move"]["minute"] * 2;
                    $participants[$key]["eco_fare"] = $transit_info["fare"]["fareunit"]["unit"] * 2;
                    $participants[$key]["eco_co2"]  = ($transit_info["move"]["co2"] * 2);
    //                $participants[$key]["eco_co2"]  = ($transit_info["move"]["co2"] * 2);
                    $participants[$key]["end_point"] = false;
                    $summary_info["time"] += $participants[$key]["eco_time"];
                    $summary_info["fare"] += $participants[$key]["eco_fare"];
                    $summary_info["co2"]  += $participants[$key]["eco_co2"];

                    $participants[$key]["eco_time"] = $this->makeTime($participants[$key]["eco_time"]);
                    $participants[$key]["eco_co2"]  = $this->get_co2_info($participants[$key]["eco_co2"]);
                // 集合拠点は無視
                } else {
                    $participants[$key]["end_point"] = true;
                    $participants[$key]["eco_time"] = "-";
                    $participants[$key]["eco_fare"] = "-";
                    $participants[$key]["eco_co2"] = "-";
                }
                $participants[$key]["pc_co2"]  = EZMath::number_format((ECO_CLIENT_CO2 * $participant["use_count"]), 1000, array('g', 'Kg', 't', 'Kt'), 2);
                $summary_info["pc_co2"]  += $participant["use_count"];
            }
            $summary_info["pc_co2"]  = EZMath::number_format((ECO_CLIENT_CO2 * $summary_info["pc_co2"]), 1000, array('g', 'Kg', 't', 'Kt'), 2);
            $summary_info["time"] = $this->makeTime($summary_info["time"]);
            $summary_info["co2"]  = EZMath::number_format($summary_info["co2"], 1000, array('g', 'Kg', 't', 'Kt'), 2);
        }

        $this->template->assign("log_info", $log_info );
        $this->template->assign("meeting_info", $meeting_info);
        $this->template->assign("participants", $participants);
        $this->template->assign("eco_info", $eco_info);
        $this->template->assign("summary_info", $summary_info);
        $this->display( "eco/eco.meeting.t.html" );
    }

    /**
     * ECOメーター拠点編集機能画面
     */
    public function action_eco_edit_form() {
        // 拠点と駅名データを取得
        $request = $this->request->getAll();
        // ECOメーター情報
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        $obj_Meeting = new MeetingTable( $this->get_dsn() );
        $objParticipant = new DBI_Participant( $this->get_dsn() );
        $where = "meeting_ticket = '".mysql_real_escape_string($request["id"])."'";
        $meeting_info = $obj_Meeting->getRow($where);
        if ($meeting_info) {
            $eco_info = unserialize($meeting_info["eco_info"]);

            $where = "meeting_key = ".addslashes($meeting_info ["meeting_key"]).
                " AND participant_name IS NOT NULL";
    //            " AND use_count > 0";
            $sort = array( "update_datetime"=>"asc");
            $participant_list = $objParticipant->getRowsAssoc($where, $sort);
            // 参加者の最後に入室したステータスだけ取得
            foreach ($participant_list as $row) {
                $participants[$row["participant_session_id"]] = $row;
                $participants[$row["participant_session_id"]]["use_count"] = $participants[$row["participant_session_id"]]["use_count"] + $row["use_count"];
            }
            $summary_info["time"] = 0;
            $summary_info["fare"] = 0;
            $summary_info["eco"]  = 0;
            foreach($participants as $key => $participant) {
                if ($participant["participant_station"] == $eco_info["end"]) {
                    $end_point = $participant["participant_key"];
                }
            }
        }
        $this->session->set("meeting_info", $meeting_info);
        $this->session->set("participants", $participants);
        $this->session->set("end_point", $end_point);
        return $this->render_eco_edit_form();
    }

    /**
     * ECOメーター拠点編集機能画面出力
     */
    public function render_eco_edit_form($err = null) {
        $meeting_info = $this->session->get("meeting_info");
        $participants = $this->session->get("participants");
        $end_point    = $this->session->get("end_point");

        $this->template->assign("meeting_info", $meeting_info);
        $this->template->assign("participants", $participants);
        $this->template->assign("end_point", $end_point);
        if ($err) {
            $this->template->assign("err", $err);
        }
        $this->set_submit_key($this->_name_space);
        // フォームに反映
        $this->display( "eco/edit.t.html" );
    }

    /**
     * ECOメーター拠点編集
     */
    public function action_eco_edit() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_eco_edit_form();
        }
        require_once('lib/EZLib/Transit.class.php');
        $uri = $this->config->get("N2MY", "transit_uri");
        $transit_log = realpath(dirname(__FILE__)."/../../../var/")."/transit.log";
        $Transit = Transit::factory( $uri, $transit_log );
        $meeting_ticket = $this->request->get("id");
        $station_list   = $this->request->get("station");
        $endpoint       = $this->request->get("endpoint");
        $participants = $this->session->get("participants");
        if (!$meeting_ticket || !$station_list) {
            return $this->render_eco_edit_form();
        }
        foreach($participants as $key => $participant) {
            $participants[$key] = $participant;
            $participants[$key]["participant_station"] = $station_list[$participant["participant_key"]];
        }
        $this->session->set("participants", $participants);
        $this->session->set("end_point", $endpoint);
        // 入力チェック
        $err = array();
        foreach($station_list as $key => $station) {
            if (!$station) {
                if ($endpoint == $key) {
                    $err[$key] = true;
                }
            } else {
                $ret = $Transit->getStationList($station, false, 50);
                if (!in_array($station, $ret)) {
                    $err[$key] = true;
                }
            }
        }
        if ($err) {
            return $this->render_eco_edit_form($err);
        }
        // 参加者の拠点情報を更新
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $objParticipant = new DBI_Participant($this->get_dsn());
        $where = "meeting_ticket = '".addslashes($meeting_ticket)."'";
        $meeting_info = $obj_Meeting->getRow($where);
        $this->logger2->info($station_list);
        if (!DB::isError($meeting_info)) {
            foreach($station_list as $participant_key => $station) {
                $where = "meeting_key = ".$meeting_info["meeting_key"].
                    " AND participant_key = '".addslashes($participant_key)."'";
                $data = array(
                    "participant_station" => $station,
                    "update_datetime"     => date("Y-m-d H:i:s"),
                );
                $objParticipant->update($data, $where);
            }
            // 経路検索結果取得
            $this->obj_Eco->createReport($meeting_info["meeting_key"], $station_list[$endpoint], false, false);
        } else {
            return $this->render_eco_edit_form();
        }
        // 結果画面表示
        $this->action_meeting_log();
    }

    /**
     * Ajaxで一覧取得
     */
    public function action_station_list() {
        require_once('lib/EZLib/Transit.class.php');
        $request = $this->request->getAll();
        $uri = $this->config->get("N2MY", "transit_uri");
        $transit_log = realpath(dirname(__FILE__)."/../../../var/")."/transit.log";
        $Transit = Transit::factory( $uri, $transit_log );
        $ret1 = $Transit->getStationList($request["station"], false, 50);
        $return["results"] = array();
        foreach( $ret1 as $key => $value ){
            $return["results"][] = array( "id" => $key, "value" => $value );
        }
        header("Content-type: application/x-javascript; charset=utf-8");
        print( json_encode( $return ) );
    }


    public function action_get_stationlist() {
        require_once('lib/EZLib/Transit.class.php');
        $request = $this->request->getAll();
        $uri = $this->config->get("N2MY", "transit_uri");
        $transit_log = realpath(dirname(__FILE__)."/../../../var/")."/transit.log";
        $Transit = Transit::factory( $uri, $transit_log );
        $ret1 = $Transit->getStationList($request["station_name"], false);
        $return = array();
        foreach( $ret1 as $key => $value ){
            $return[] = array( "key" => $key, "value" => $value );
        }
        header("Content-type: application/x-javascript; charset=utf-8");
        if( $return ){
            print( json_encode( $return ) );
        }else{
            print( "" );
        }
    }

    public function action_image_co2()
    {
        $log_info = $this->session->get( "log_info", $this->_name_space );
        $max = $this->getMaxData( $log_info["imagedata"]["eco_co2"] );
        $info = $this->get_co2_info( $max );
        for( $i = 0; $i < count( $log_info["imagedata"]["eco_co2"] ); $i++ ){
            $data[] = $log_info["imagedata"]["eco_co2"][$i] / $info["denominator"];
        }
        $verticaldata = $this->getVerticalData( $data );

        $this->obj_Eco->setData( $log_info["imagedata"]["horizontal"], $data );
        $this->obj_Eco->setYValue( $verticaldata["max"], $verticaldata["division"] );
        $this->obj_Eco->setLineColor( $r = 188, $g = 244, $b = 46 );
        //$this->obj_Eco->setUnit( sprintf( "%s/月", $info["unit"] ) );
        $this->obj_Eco->flushImage();
    }

    public function action_image_trans()
    {
        $log_info = $this->session->get( "log_info", $this->_name_space );
        foreach( $log_info["imagedata"]["eco_fare"] as $key => $value ){
            $data[] = $value / 10000;
        }
        $verticaldata = $this->getVerticalData( $data );

        $this->obj_Eco->setData( $log_info["imagedata"]["horizontal"], $data );
        $this->obj_Eco->setYValue( $verticaldata["max"], $verticaldata["division"] );
        $this->obj_Eco->setLineColor( $r = 0, $g = 156, $b = 0 );
        //$this->obj_Eco->setUnit( "万円/月" );
        $this->obj_Eco->flushImage();
    }

    public function action_image_time()
    {
        $log_info = $this->session->get( "log_info", $this->_name_space );
        for( $i = 0; $i < count( $log_info["imagedata"]["eco_time"] ); $i++ ){
            $data[] = round( $log_info["imagedata"]["eco_time"][$i] / 60 );
        }
        $verticaldata = $this->getVerticalData( $data );

        $this->obj_Eco->setData( $log_info["imagedata"]["horizontal"], $data );
        $this->obj_Eco->setYValue( $verticaldata["max"], $verticaldata["division"] );
        $this->obj_Eco->setLineColor( $r = 255, $g = 165, $b = 0 );
        //$this->obj_Eco->setUnit( "時間/月" );
        $this->obj_Eco->flushImage();
    }

    private function get_co2_info( $co2 )
    {
        $s = array('g', 'Kg', 't', 'kt', 'Mt', 'Gt');
        if ($co2 == 0) {
            $e = 0;
            $result["value"] = "0";
            $result["unit"] = $s[0];
        } else {
            if ($co2 > 0) {
                $e = floor(log($co2)/log(1000));
                $result["value"] = sprintf('%.2f ', ($co2/pow(1000, floor($e))));
                $result["unit"] = $s[$e];
            } else {
                // マイナス
                $co2 = $co2 * -1;
                $e = floor(log($co2)/log(1000));
                $result["value"] = sprintf('-%.2f ', ($co2/pow(1000, floor($e))));
                $result["unit"] = $s[$e];
            }
        }
        if( $co2 >= 1000000 ){
            $result["denominator"] = 1000000;
        } else {
            $result["denominator"] = 1000;
        }
        return $result;
    }

    private function getMaxData( $data )
    {
        $max = 0;
        foreach( $data as $key => $value ){
            $max = max( $max, $value );
        }
        return $max;
    }

    private function getVerticalData( $data )
    {
        $max = $this->getMaxData( $data );
        if( $max == 0 ){
            $return["division"] = 10;
            $return["max"] = 100;
        } else if( $max < 1 ){
            $return["division"] = 10;
            $return["max"] = 1;
        } else {
            $max /= 0.9;
            $unit = "";
            for( $i = 0; $i < strlen( floor ( $max ) ) - 1; $i++ ){
                $unit .= 0;
            }
            $return["division"] = substr( $max, 0, 1 ) + 1;
            $return["max"] = $return["division"] . $unit;
        }
        return $return;
    }

    private function makeTime( $time )
    {
        $return = array();
        $return["hour"] = floor( $time / 60 );
        $return["minute"] = fmod( $time, 60 );
        return $return;
    }
    /**
     * デフォルトページ
     */
    function default_view()
    {
        return $this->display('error.t.html');
    }
}

$main =& new AppEco();
$main->execute();
