<?php

require_once ('classes/AppFrame.class.php');
require_once ('classes/N2MY_Auth.class.php');
require_once ('classes/mgm/MGM_Auth.class.php');
require_once ('classes/dbi/member.dbi.php');
require_once ('classes/dbi/user.dbi.php');
require_once ('classes/dbi/pw_reset.dbi.php');
require_once ('config/config.inc.php');
require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
require_once ('lib/EZLib/EZCore/EZLogger2.class.php');

class AppPwReset extends AppFrame
{
    function init() {
      $this->pw_reset    = new PwResetTable($this->get_dsn());
      $this->member      = new MemberTable($this->get_dsn());
      $this->user        = new UserTable($this->get_dsn());
      $this->_name_space = md5(__FILE__);
      $dir               = dirname(__FILE__);
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->action_form();
    }

    /**
     * フォームの表示
     */
    function action_form($message="", $user_id="", $name="", $email="", $phone="") {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->set_submit_key($this->_name_space);
        // エラーを表示。
        if ($message) {
            $this->template->assign('user_id', $user_id);
            $this->template->assign('name', $name);
            $this->template->assign('message', $message);
            $this->template->assign('email', $email);
            $this->template->assign('phone', $phone);
        }
        // 入力画面表示
        $datetime = time();
        $this->template->assign('datetime', $datetime);
        $this->display('user/pwReset/index.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
    * 送信＆完了処理
    **/
    function action_complete() {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_form();
        }
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        $request      = $this->request->getAll();
        $user_id      = $this->request->get('user_id');
        $name         = $this->request->get('name');
        $email        = $this->request->get('email');
        $phone        = $this->request->get('phone');
        $captcha_code = $this->request->get('captcha_code');

        // 入力チェック
        if(!$user_id){
            $message .= "<li>".PW_RESET_NULL_USER_ID . "</li>";
        }else{
            $obj_MGMClass = new MGM_AuthClass($this->get_auth_dsn());
            $user_data    = $obj_MGMClass->getUserInfoById($user_id);
            $this->logger2->info($user_data);
            $message = "";
            if (!$user_data['user_id']) {
                $message .= "<li>".PW_RESET_ERROR_USER_ID . "</li>";
                $user_id = "";
            } else {
                // メンバー情報取得。
                $where = " member_id = '".mysql_real_escape_string($user_id) ."'".
                         " AND member_status = 0";
                $member_info = $this->member->getRow($where);
                if (empty($member_info)) {
                    $where = " user_id = '".mysql_real_escape_string($user_id)."'".
                             " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                             " AND user_delete_status < 2";
                    $user_info = $this->user->getRow($where);
                } else {
                    $where = " user_key = '".mysql_real_escape_string($member_info['user_key'])."'".
                             " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                             " AND user_delete_status < 2";
                    $user_info = $this->user->getRow($where);
                }
                if (!$user_info['user_key']) {
                    $message .= "<li>".PW_RESET_ERROR_USER_ID . "</li>";
                    $user_id = "";
                }
            }
        }

        if (!$name) {
            $message .= "<li>".PW_RESET_NULL_NAME ."</li>";
        }elseif(mb_strlen($name) > 50){
            $message .= "<li>".PW_RESET_ERROR_NAME ."</li>";
        }

        if (!$email) {
            $message .= "<li>".PW_RESET_NULL_EMAIL ."</li>";
        }else{
            require_once ('lib/EZLib/EZHTML/EZValidator.class.php');
            if (!EZValidator::valid_email($email)) {
                $message .= '<li>' . PW_RESET_INVALID_EMAIL . '</li>';
            }
        }

        if (!$phone) {
            $message .= "<li>".PW_RESET_NULL_PHONE ."</li>";
        }elseif(mb_strlen($phone) > 50){
            $message .= "<li>".PW_RESET_ERROR_PHONE ."</li>";
        }

        if ($captcha_code !== $_SESSION['random_code']) {
            $message .= "<li>".PW_RESET_ERROR_CAPTCHA_CODE ."</li>";
        }

        if($message != ""){
            return $this->action_form($message, $user_id, $name, $email, $phone);
        }


        // 許可Key
        $authorize_key =  sha1(uniqid(rand(), true));
        // 拒否Key
        $ignore_key    = sha1(uniqid(rand(), true));

        if (empty($member_info)){
            // 申請者がuser_id
            $temp = array(
                    "authorize_key" => $authorize_key,
                    "ignore_key"    => $ignore_key,
                    "user_key"      => $user_info['user_key'],
                    "name"          => $name,
                    "email"         => $email,
                    "phone"         => $phone
            );

        } else {
            // 申請者がmember_id
            $temp = array(
                    "authorize_key" => $authorize_key,
                    "ignore_key"    => $ignore_key,
                    "user_key"      => $member_info['user_key'],
                    "member_key"    => $member_info['member_key'],
                    "name"          => $name,
                    "email"         => $email,
                    "phone"         => $phone
            );
        }

        //DBに追加
        $ret = $this->pw_reset->add($temp);
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return false;
        } else {
            $this->logger->info(__FUNCTION__."#add member successful!",__FILE__,__LINE__,$temp);
        }

        //メール送信
        require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
        //assign
        $this->template->assign('authorize_key', $authorize_key);
        $this->template->assign('ignore_key', $ignore_key);
        $this->template->assign('name', $name);
        $this->template->assign('email', $email);
        $this->template->assign('phone', $phone);
        $this->template->assign('company_name', $user_info['user_company_name']);
        $this->template->assign('user_company_phone', $user_info['user_company_phone']);
        $this->template->assign('user_staff_email', $user_info['user_staff_email']);
        $this->template->assign('member_email', $member_info['member_email']);
        $frame["lang"] = $this->_lang;
        $this->template->assign("__frame",$frame);
        if (empty($member_info['member_id'])) {
            $this->template->assign('user_id', $user_info['user_id']);
        } else {
            $this->template->assign('member_id', $member_info['member_id']);
        }

        if(!$member_info){
            //V-CUBE宛
            $mail = new EZSmtp(null, "", "UTF-8");
            $body = $this->fetch('', '0');
            $mail->setTo(PW_RESET_TO);
            $mail->setFrom(PW_RESET_FROM);
            $mail->setReturnPath(NOREPLY_ADDRESS);
            $mail->setSubject(PW_RESET_SUBJECT_VCUBE);
            $mail->setBody($body);
            $mail->send();
        }else{
            $mail = new EZSmtp(null, $this->_lang, "UTF-8");
            $mode = "pw_reset_admin";
            $body = $this->fetch($mode, "1");
            $mail->setSubject(PW_RESET_SUBJECT);
            $mail->setFrom(PW_RESET_FROM);
            $mail->setReturnPath(NOREPLY_ADDRESS);
            $mail->setBody($body);
            if($user_info['user_staff_email']){
                $mail->setTo($user_info['user_staff_email']);
            }else{
                $mail->setTo(PW_RESET_TO);
            }
            $mail->send();
        }
        unset($_SESSION['random_code']);
        // 画面表示
        $this->display('user/pwReset/complete.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }


    /**
     * 許可確認画面
     */
    function action_authorize_confirm($message="", $authorize_key="") {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->set_submit_key($this->_name_space);

        //24時間経過していたら削除しておく。
        $sql = "DELETE FROM pw_reset WHERE create_datetime < (UTC_TIMESTAMP() - INTERVAL 24 HOUR)";
        $rs = $this->pw_reset->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger2->error($rs);
            return false;
        }

        //情報取得
        if (empty($authorize_key)) {
            $authorize_key = $_GET['id'];
        }
        $authorize_info = $this->pw_reset->getAuthorizeInfo( $authorize_key );
        $this->template->assign('authorize_key', $authorize_key);

        //削除されたKEYの場合、無効画面
        if (empty($authorize_info)) {
            return $this->display('user/pwReset/error.t.html');
        }

        //エラーを表示。
        if ($message) {
            $this->template->assign('message', $message);
            $this->display('user/pwReset/authorize_confirm.t.html');
        }

        // 画面表示
        $this->display('user/pwReset/authorize_confirm.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * 許可（パスワードリセット）
     */
    function action_authorize_complete() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        //パラメタ
        $request = $this->request->getAll();
        $user_id        = $this->request->get('user_id');
        $authorize_key  = $this->request->get('authorize_key');
        $obj_MGMClass   = new MGM_AuthClass($this->get_auth_dsn());
        $user_data      = $obj_MGMClass->getUserInfoById($user_id);
        $authorize_info = $this->pw_reset->getAuthorizeInfo($authorize_key);
        $authorize_data      = $this->member->getDetail($authorize_info['member_key']);
        $m_user_data         = $this->user->find($authorize_data['user_key']);

        $message = "";
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_authorize_confirm($message, $authorize_key);
        }
        if ($m_user_data['user_id'] !== $user_data['user_id']) {
            $message .= "<li>".PW_RESET_ERROR_USER_ID . "</li>";
            return $this->action_authorize_confirm($message, $authorize_key);
        }

        //memberPW初期化
        $authorize_data['member_pass'] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $this->create_id().rand(2,9));
        $ret = $this->member->update($authorize_data, sprintf( "member_key='%s'", $authorize_data['member_key'] ) );
        // エラー処理
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getMemberInfo());
            return false;
        } else {
            $this->logger->info(__FUNCTION__."#edit member successful!",__FILE__,__LINE__,$authorize_data);
        }

        //メール送信
        require_once ("lib/EZLib/EZMail/EZSmtp.class.php");

        //assign
        $member_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, str_replace(" ","+",$authorize_data['member_pass']));
        $this->template->assign('member_id', $authorize_data['member_id']);
        $this->template->assign('member_pass', $member_password);

        //管理者のメールアドレス
        $where = "user_key = '".$authorize_data["user_key"]."'";
        $user_mail = $this->user->getOne($where, "user_staff_email");

        //メンバー用
        $mail = new EZSmtp(null, $this->_lang, "UTF-8");
        $frame["lang"] = $this->_lang;
        $this->template->assign("__frame",$frame);
        $mode = "pw_reset_authorize";
        $body = $this->fetch($mode, "1");
        $mail->setSubject(PW_RESET_AUTHORIZE_SUBJECT);
        $mail->setTo($authorize_data['member_email']);
        $mail->setFrom(PW_RESET_FROM);
        $mail->setReturnPath(PW_RESET_FROM);
        $mail->setBody($body);
        $mail->send();

        //管理者用
        $mail = new EZSmtp(null, $this->_lang, "UTF-8");
        $mode = "pw_reset_authorize_admin";
        $body = $this->fetch($mode, "1");
        $mail->setSubject(PW_RESET_AUTHORIZE_ADMIN_SUBJECT);
        $mail->setTo($user_mail);
        $mail->setFrom(PW_RESET_FROM);
        $mail->setReturnPath(PW_RESET_FROM);
        $mail->setBody($body);
        $mail->send();

        //DBから削除。
        $where = "pw_reset_id = ".$authorize_info['pw_reset_id'];
        $this->pw_reset->remove($where);

        // 画面表示
        $this->display('user/pwReset/authorize_complete.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * 拒否確認画面
     */
    function action_ignore_confirm($message="",$ignore_key=""){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->set_submit_key($this->_name_space);

        //24時間経過していたら削除しておく。
        $sql = "DELETE FROM pw_reset WHERE create_datetime < (UTC_TIMESTAMP() - INTERVAL 24 HOUR)";
        $rs = $this->pw_reset->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger2->error($rs);
            return false;
        }

        //情報取得
        if (empty($ignore_key)) {
            $ignore_key = $_GET['id'];
        }
        $ignore_info = $this->pw_reset->getIgnoreInfo($ignore_key);
        $this->template->assign('ignore_key', $ignore_key);


        //確認。
        if( empty($ignore_info) ){
            return $this->display('user/pwReset/error.t.html');
        }

        // エラー表示。
        if( $message ){
            $this->template->assign('message', $message);
        }

        // 画面表示
        $this->display('user/pwReset/ignore_confirm.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * 拒否
     */
    function action_ignore_complete() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

        //パラメタ
        $request = $this->request->getAll();
        $user_id      = $this->request->get('user_id');
        $ignore_key   = $this->request->get('ignore_key');

        //入力チェック
        $obj_MGMClass = new MGM_AuthClass($this->get_auth_dsn());
        $user_data = $obj_MGMClass->getUserInfoById($user_id);
        $ignore_info = $this->pw_reset->getIgnoreInfo($ignore_key);
        $ignore_data      = $this->member->getDetail($ignore_info['member_key']);
        $m_user_data      = $this->user->find($ignore_data['user_key']);

        $message = "";
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_ignore_confirm($message, $ignore_key);
        }
        if ($m_user_data['user_id'] !== $user_data['user_id']) {
            $message .= "<li>".PW_RESET_ERROR_USER_ID . "</li>";
            return $this->action_ignore_confirm($message, $ignore_key);
        }

        //メール送信
        require_once ('lib/EZLib/EZMail/EZSmtp.class.php');
        //メンバー用
        $mail = new EZSmtp(null, $this->_lang, "UTF-8");
        $frame["lang"] = $this->_lang;
        $this->template->assign("__frame",$frame);
        $mode = "pw_reset_ignore";
        $body = $this->fetch($mode, "1");
        $mail->setSubject(PW_RESET_IGNORE_SUBJECT);
        $mail->setTo($ignore_data['member_email']);
        $mail->setFrom(PW_RESET_FROM);
        $mail->setReturnPath(PW_RESET_FROM);
        $mail->setBody($body);
        $mail->send();

        //管理者用
        $mail = new EZSmtp(null, $this->_lang, "UTF-8");
        $mode = "pw_reset_ignore_admin";
        $body = $this->fetch($mode, "1");
        $mail->setSubject(PW_RESET_IGNORE_ADMIN_SUBJECT);
        $mail->setTo( $m_user_data['user_staff_email']);
        $mail->setFrom(PW_RESET_FROM);
        $mail->setReturnPath(PW_RESET_FROM);
        $mail->setBody($body);
        $mail->send();

        //DBから物理削除。
        $where = "pw_reset_id = ".$ignore_info['pw_reset_id'];
        $this->pw_reset->remove($where);

        // 画面表示
        $this->display('user/pwReset/ignore_complete.t.html');

        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * 認証コードの発行
     */
    function action_captcha() {
        //文字作成
        $string = '';
        for ($i = 0; $i < 5; $i++) {
            $string .= chr(rand(97, 122));
        }
        $_SESSION['random_code'] = $string;

        //画像サイズと色
        $image = imagecreatetruecolor(90, 30);
        $black = imagecolorallocate($image, 0, 0, 0);
        $white = imagecolorallocate($image, 255, 255, 255);
        $color = imagecolorallocate($image, 255, 100, 100);
        //
        imagefilledrectangle($image,0,0,399,29,$white);
        imagettftext ($image, 15, 0, 10, 20, $color,'./AHGBold.ttf', $_SESSION['random_code']);

        $im = imagerotate($image, rand(-10,10), $white);

        header("Content-type: image/png");
        imagepng($im);
    }

    /**
     override
     $flag = 1, userに送信
     $flag = 0, adminに送信
     **
     */
    function fetch($mode, $flag) {
        if($flag){
            $custom  = $this->config->get('SMARTY_DIR','custom');
            $lang_cd = $this->_lang;
            require_once ("lib/EZLib/EZHTML/EZTemplate.class.php");
            $template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
            $template_file = "common/mail_template/meeting/pw_reset/" . $mode . ".t.txt";
            if($custom){
                $custom_template_file = "custom/" . $custom . "/common/multi_lang/pw_reset/" . $mode . ".t.txt";
                if (file_exists($template->template_dir.$custom_template_file)){
                    $template_file = $custom_template_file;
                }else{
                    $custom_template_file = "custom/" . $custom. "/" . $lang_cd."/mail/pw_reset/" . $mode . ".t.txt";
                    if (file_exists($template->template_dir.$custom_template_file)) {
                        $template_file = $custom_template_file;
                    }
                }
            }
        }else{
            $template_file = "common/mail/pw_reset_vcube.t.txt";
        }
        $this->logger->info("path is ". $template_file);
        return parent::fetch($template_file);
    }    
    
     /**
     * 乱数ID発行
     */
    function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this->dec2any($c);
    }


    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=57, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr(
                    '23456789ab' .
                    'cdefghijkm' .
                    'nopqrstuvw' .
                    'xyzABCDEFG' .
                    'HJKLMNPQRS' .
                    'TUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }

}

$main =& new AppPwReset();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
