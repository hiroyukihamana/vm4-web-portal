<?php
/*
 * Created on 2007/03/05
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once ('classes/AppFrame.class.php');
require_once ('classes/N2MY_Auth.class.php');
require_once ('classes/mgm/MGM_Auth.class.php');

//require_once("classes/dbi/user.dbi.php");
//require_once("classes/dbi/member.dbi.php");
//require_once("classes/dbi/room.dbi.php");
//require_once("classes/dbi/reservation.dbi.php");
//require_once("Meeting2.class.php");
//require_once("Login2.class.php");

//require_once("EZXML/EZXML.class.php");

class N2MY_Meeting_API extends AppFrame
{
    var $session_id = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
    }

    /**
     * ログイン処理
     *
     * @param string id ユーザーID
     * @param string pw ユーザーパスワード
     * @return string $output テンプレートを表示
     */
    function action_login()
    {
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        $id = $this->request->get("id");
        $pw = $this->request->get("pw");
        $pw_encode = $this->request->get("enc", 1);
        $_country_key = $this->request->get("country", "auto");
        $user_type = "";
        // ユーザ確認

        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if ( ! $user_info = $obj_MGMClass->getUserInfoById( $id ) ){
            return false;
//            $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
//            return $this->action_showTop($message);
        }
        if ( ! $server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] ) ){
            return false;
//            return $this->action_server_down();
        } else {
            $this->session->set( "server_info", $server_info );
        }

        // ログインチェック
        $obj_Auth = new N2MY_Auth( $this->get_dsn() );
        if ($login_info = $obj_Auth->check( $id, $pw, "md5" ) ) {
            $status = 1;
            $this->session->set('login', '1');
            $this->session->set('user_info', $login_info["user_info"]);
            if( $login_info["member_info"] )
                $this->session->set('member_info', $login_info["member_info"] );

            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $rooms = $obj_N2MY_Account->getRoomList($login_info["user_info"]["user_key"]);
            $this->session->set('room_info', $rooms);
            
            $result_country_key = $this->_get_country_key($_country_key);
            if (!$result_country_key) {
                // 地域コードの不正な指定を防ぐ
                $country_list = $this->get_country_list();
                if (!array_key_exists($_country_key, $country_list)) {
                    $country_keys = array_keys($country_list);
                    $_country_key = $country_keys[0];
                    $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
                }
                foreach ($country_list as $country_key => $country_row) {
                    if ($_country_key == $country_key) {
                        $this->session->set('country_key', $country_row["country_key"]);
                        $result_country_key = $_country_key;
                    }
                }
                $this->session->set('country_key', $result_country_key);
            }else {
                $this->session->set('country_key', $result_country_key);
            }
        $this->session->set('selected_country_key', $_country_key);
        } else {
            $status = 0;
            session_regenerate_id(true);
        }
        $this->logger->info(__FUNCTION__."#login user!", __FILE__, __LINE__, $login_info);
        $result = array(
            "status" => $status,
            "session_id" => session_id()
            );
        $this->template->assign("result", $result);
        $output = $this->fetch("common/user/api/meeting/login.t.xml");
        header("Content-Type: text/xml; charset=UTF-8");
        print $output;
    }


    /**
     * ログアウト処理
     *
     * @param
     * @return
     */
    function action_logout()
    {
    }

    /**
     * 部屋一覧取得
     *
     * @param
     * @return string $output 部屋の一覧をxmlで出力
     */
    function action_get_room_list()
    {
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        $room_list = $this->get_room_info();
        $_SESSION[_EZSESSION_NAMESPACE]["room_info"] = $room_list;
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$room_list);
        $status = 0;
        if ($room_list) {
            $status = 1;
        }
        $this->template->assign("status", $status);
        $this->template->assign("rooms", $room_list);
        $output = $this->fetch('common/user/api/meeting/room_list.t.xml');
        header("Content-Type: text/xml; charset=UTF-8");
        print $output;
    }

    /**
     * 会議キー取得
     *
     * @param boolean $header_flg ヘッダを出力
     * @return string $output  会議キー、会議名をxmlで出力
     */
    function action_create_meeting($header_flg = true)
    {
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        $room_key = $this->get_room_key();
        $provider_id = $this->config->get("CORE","provider_id");
        $provider_pw = $this->config->get("CORE","provider_pw");
        $core_url = $this->config->get("CORE","base_url");
        $meeting = new Meeting($this->get_dsn(), $provider_id, $provider_pw, $core_url);
        // 現在利用可能な会議情報取得
        $now_meeting = $meeting->getNowMeeting($room_key);
        $meeting_key = $now_meeting["meeting_key"];
        $meeting_name = $now_meeting["meeting_name"];
        // センタサーバ指定
        $country_id = "jp";
        if (isset($extension["locale"])) {
            switch ($extension["locale"]) {
            case "jp" :
            case "us" :
            case "cn" :
                $country_id = $extension["locale"];
                break;
            }
        }
        // オプション指定
        $option = array(
            "meeting_name" => $meeting_name,
            "start_time" => $now_meeting["start_time"],
            "end_time" => $now_meeting["end_time"],
            "country_id" => $country_id,
        );
        $core_meeting_key = $meeting->createMeeting($room_key, $meeting_key, $option);
        $now_meeting["meeting_key"] = $core_meeting_key;
        if (!$meeting_key) {
            $this->_result["status"] = 0;
        }
        // セッションに情報を保持
        $now_meeting["meeting_password"] = ($now_meeting["meeting_password"]) ? md5($now_meeting["meeting_password"]) : "";
        $_SESSION["meeting_info"][$core_meeting_key] = $now_meeting;
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__,$now_meeting);
        $this->template->assign("meeting", $now_meeting);
        $output = $this->fetch("user/api/meeting/create_meeting.t.xml");
        if ($header_flg) {
            header("Content-Type: text/xml; charset=UTF-8");
        }
        print $output;
    }

    /**
     * 予約一覧（検索）
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_limit 検索開始日 (デフォルト null)
     * @param datetime end_limit 検索終了日 (デフォルト null)
     * @param string sort_key ソート基準 (デフォルト 予約会議開始時間)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @param int limit 表示件数 (デフォルト null)
     * @param int offset 開始件数 (デフォルト null)
     * @return string $output 会議キー、予約キー、会議名、開始時間、終了時間をxmlで出力
     */
    function action_get_reservation_list()
    {
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        $room_key = $this->request->get("room_key");
        $meeting_name = $this->request->get("meeting_name", "");
        $start_limit = $this->request->get("start_limit", "");
        $end_limit = $this->request->get("end_limit", "");
        $sort_key = $this->request->get("sort_key", "reservation_starttime");
        $sort_type = $this->request->get("sort_type", "asc");
        $limit = $this->request->get("limit", null);
        $offset = $this->request->get("offset", null);
        // 部屋情報取得
        $room_table = new RoomTable($this->get_dsn());
        $room["room_key"] = $room_key;
        $this->template->assign("room", $room);
        // 検索
        $this->_reservation_obj = new ReservationTable($this->get_dsn());
        $where = "room_key LIKE '$room_key'" .
                " AND reservation_status = 1";
        if ($start_limit && $end_limit) {
            $where .= " AND (" .
                    "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                    " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                    " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                    " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
        }
        //全体の件数取得
        $count = $this->_reservation_obj->numRows($where);
        $this->template->assign("count", $count);

        if ($meeting_name) {
            $where .= " AND meeting_name = '$meeting_name'";
        }
        $reservations = $this->_reservation_obj->getList($where, "reservation_starttime", $sort_type, $limit, $offset);
        // 全体の件数
        // 一覧表示

        $this->template->assign("reservations", $reservations);
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__,$reservations);
        $output = $this->fetch("user/api/meeting/reservation_list.t.xml");
        header("Content-Type: text/xml; charset=UTF-8");
        print $output;
    }

    /**
     * 予約内容詳細（予約、招待者、事前資料）
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return string $output 予約キー、会議室名、会議名、開始時間、終了時間、パスワード、招待者、資料名をxmlで出力
     */
    function action_get_reservation_detail()
    {
        // 予約情報
        // 招待者
        // 資料一覧
    }

    /**
     * 予約追加
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_time 開始日時
     * @param datetime end_time 終了日時
     * @param string r_user_name 参加者名 (デフォルト null)
     * @param string r_user_email 参加者メールアドレス (デフォルト null)
     * @param string r_user_lang 参加者言語 (デフォルト null)
     * @param string r_user_timezone 参加者タイムゾーン (デフォルト null)
     * @return boolean
     */
    function action_create_reservation()
    {
        // 予約一覧表示
        // 予約追加
    }

    /**
     * 予約変更
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string meeting_name 会議名
     * @param datetime start_time 変更後開始時間
     * @param datetime end_time　変更後終了時間
     * @return boolean
     */
    function action_update_reservation()
    {
        // 予約情報表示
        // 予約変更
    }

    /**
     * 予約削除
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return boolean
     */
    function action_delete_reservation()
    {
        // 予約一覧表示
        // 予約削除
    }

    /**
     * 招待者一覧
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return string $output 名前、メールアドレス、言語、タイムゾーンをxmlで出力
     */
    function action_get_invite()
    {
        // 予約一覧表示
        // 招待者一覧取得
    }

    /**
     * 招待者追加
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string r_user_name 参加者名
     * @param string r_user_email 参加者メールアドレス
     * @param string r_user_lang 参加者言語
     * @param string r_user_timezone 参加者タイムゾーン
     * @return boolean
     */
    function action_add_invite()
    {
        // 予約一覧表示
        // 招待者一覧取得
    }

    /**
     * 招待者削除
     *
     * @param string room_key ルームキー
     * @param string reservation_session
     * @param int r_user_key 参加者キー
     * @return boolean
     */
    function action_delete_invite()
    {
        // 予約一覧表示
        // 招待者一覧取得
    }

    /**
     * 資料一覧
     *
     * @param string $room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param int offset 開始件数 (デフォルト null)
     * @param int limit 表示件数 (デフォルト null)
     * @param string sort_key ソート基準 (デフォルト ソート番号順)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @return string $output 資料キー、資料名、資料タイプ、資料サイズ、ソート番号をxmlで出力
     */
    function action_get_document($reservation_session)
    {
        // 予約情報取得
        // 資料一覧取得
    }

    /**
     * 資料追加
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string file ファイル情報
     * @param string file_name ファイル名
     * @param string file_type ファイルの種類
     * @return boolean
     */
    function action_create_document()
    {
        // 資料一覧取得

    }

    /**
     * 資料変更
     *
     * @param string room_key ルームキー
     * @param string document_id ドキュメントID
     * @param string document_name ドキュメント名
     * @return boolean
     */
    function action_update_document()
    {
        // 資料一覧取得
    }

    /**
     * 資料削除
     *
     * @param string room_key ルームキー
     * @param string document_id ドキュメントID
     * @return boolean
     */
    function action_delete_document()
    {
        // 資料一覧取得
    }

    /**
     * 資料ソート順を上げる
     *
     * @param string room_key ルームキー
     * @param int meeting_key 会議キー
     * @param string document_id ドキュメントID
     * @return
     */
    function action_up_document()
    {
        // 予約情報取得
        // 資料一覧取得
    }

    /**
     * 資料ソート順を下げる
     *
     * @param string room_key ルームキー
     * @param string meeting_key 会議キー
     * @param int document_id ドキュメントID
     * @return
     */
    function action_down_document()
    {
        // 予約情報取得
        // 資料一覧取得
    }

    /**
     * 議事録一覧（検索）
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param string user_name 参加者名 (デフォルト null)
     * @param datetime start_limit 検索開始日 (デフォルト null)
     * @param datetime end_limit 検索終了日 (デフォルト null)
     * @param int offset 開始件数 (デフォルト null)
     * @param int limit 表示件数 (デフォルト null)
     * @param string sort_key ソート基準 (デフォルト 会議開始時間)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @return $string $output 会議キー、会議名、日時、参加者、サイズ、映像有無、議事録有無
     */
    function action_get_meetinglog_list()
    {
        // 会議情報取得
    }

    /**
     * 議事録（ログ自体）削除
     *
     * @param string room_key ルームキー
     * @param string meeting_key 会議キー
     * @return boolean
     */
    function action_delete_meetinglog()
    {
        // 議事録情報取得
    }

    /**
     * 議事録（映像のみ）削除
     *
     * @param string room_key ルームキー
     * @param string meeting_key 会議キー
     * @return boolean
     */
    function action_delete_logvideo()
    {
        // 議事録情報取得
    }

    /**
     * 議事録（議事録のみ）削除
     *
     * @param string room_key ルームキー
     * @param string meeting_key 会議キー
     * @return boolean
     */
    function action_delete_logminutes()
    {
        // 議事録情報取得
    }

    /**
     * 議事録パスワード変更
     *
     * @param string room_key ルームキー
     * @param string meeting_key 会議キー
     * @param string meetinglog_password パスワード
     * @return boolean
     */
    function action_modify_meetinglog_password()
    {
        // 議事録情報取得
    }

    /**
     * 会議開始（会議キーを指定して開始）
     *
     * @param boolean $header_flg ヘッダを出力
     * @param string type 参加タイプ (デフォルト normal)
     * @param int meeting_key 会議キー
     * @param string name ユーザー名
     * @param string narrow narrow (デフォルト 0)
     * @param string lang 言語 (デフォルト ja)
     * @param string  skin_type レイアウトタイプ (デフォルト null)
     * @param string role 通常、招待者
     * @return string $output urlをxmlで出力
     */
     /*
    function action_start_meeting_with_key($header_flg = true)
    {
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        $meeting_key = $this->request->get("meeting_key");
        $type = $this->request->get("type", "normal");
        // ユーザ情報を整形
        $user_info["id"] = $_SESSION["user_info"]["user_id"];
        // リクエストパラメタで上書き可能
        $user_info["name"]         = $this->request->get("name", $_SESSION["user_info"]["user_name"]);
        $user_info["narrow"]     = $this->request->get("narrow", 0);
        $user_info["lang"]         = $this->request->get("lang", "ja");
        $user_info["skin_type"] = $this->request->get("skin_type", "");
        $user_info["role"]         = $this->request->get("role");
        // 会議開始URL取得
        $this->session->set("lang", $user_info["lang"]);
        $provider_id = $this->config->get("CORE","provider_id");
        $provider_pw = $this->config->get("CORE","provider_pw");
        $core_url = $this->config->get("CORE","base_url");
        $meeting = new Meeting($this->get_dsn(), $provider_id, $provider_pw, $core_url);
        $url = $meeting->startMeetingUrl($meeting_key, $type, $user_info);
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__,array(
            $meeting_key,
            $type,
            $user_info,
            "url" => $url));
        $_SESSION["meeting_info"][$meeting_key]["start_url"] = $url;
        $redirect_url = "?action_auth_start&meeting_key=".$meeting_key."&n2my_session=".session_id();
        $this->template->assign("url", $redirect_url);
        $output = $this->fetch("user/api/meeting/start_meeting.t.xml");
        if ($header_flg) {
            header("Content-Type: text/xml; charset=UTF-8");
        }
        print $output;
    }
    */

    /**
     * 会議開始（会議を作成して、そのまま開始）
     *
     * @param
     * @return 会議データを作成し、リダイレクトで会議室に入室
     */
    function action_start_meeting()
    {
        // 会議作成
        /*
        ob_start();
        $this->action_create_meeting();
        $content = ob_get_contents();
        ob_end_clean();
        $xml = new EZXML();
        $data = $xml->openXML($content);
        $meeting_key = $data["result"]["createMeeting"][0]["meetingKey"][0]["_attr"]["value"];
        $this->logger->info(__FUNCTION__."#meeting_key", __FILE__, __LINE__, $meeting_key);
        // 会議開始URL生成
        $this->request->set("meeting_key", $meeting_key); // 会議キーを指定
        ob_start();
        $this->action_start_meeting_with_key();
        $content = ob_get_contents();
        ob_end_clean();
        $xml = new EZXML();
        $data = $xml->openXML($content);
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, $data);
        $redirect_url = $data["result"]["data"][0]["url"][0]["_attr"]["value"];
        // そのままリダイレクト
        $hostname = $_SERVER["SERVER_NAME"];
        $port = "";
        // SSL対応
        if (isset ($_SERVER["HTTPS"])) {
            $schema = "https";
            // デフォルトポートであれば不要
            if ($_SERVER["SERVER_PORT"] != "443") {
                $port = ":" . $_SERVER["SERVER_PORT"];
            }
        } else {
            $schema = "http";
            // デフォルトポートであれば不要
            if ($_SERVER["SERVER_PORT"] != "80") {
                $port = ":" . $_SERVER["SERVER_PORT"];
            }
        }
        $url = $schema . "://" . $hostname . $port . $_SERVER["SCRIPT_NAME"].$redirect_url;
        $this->logger->info(__FUNCTION__."#redirect_dir", __FILE__, __LINE__, $url);
        header("Location: ".$url);
        */
        //services/index.php
        require_once("classes/N2MY_Meeting.class.php");
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // 現在利用可能な会議情報取得
        $room_key = $this->get_room_key();
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");
        // ユーザ情報
        $user_info = $this->session->get("user_info");
        // 会議キー
        $meeting_key = $this->request->get("meeting_key");
        // 会議情報取得
        if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_key)) {
            die("error!");
        }
        $meeting_key = $meeting_info["meeting_key"];
        $country_key = $this->session->get("country_key");
        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();
        if (!array_key_exists($country_key, $country_list)) {
            $_country_data = array_shift($country_list);
            $country_key = $_country_data["country_key"];
        }
        // オプション指定
        $options = array(
            "user_key"     => $user_info["user_key"],
            "meeting_name" => $meeting_info["meeting_name"],
            "start_time"   => $meeting_info["start_time"],
            "end_time"     => $meeting_info["end_time"],
            "country_id"   => $country_key,
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);
        // ユーザごとのオプションを取得
        $type        = $this->request->get("type");
        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";
        $lang = $this->request->get("lang");
        $user_options = array(
            "name"      => $this->request->get("name", $user_info["user_info"]["user_name"]),
            "narrow"    => $this->request->get("is_narrow"),
            "lang"      => $lang,
            "skin_type" => $this->request->get("skin_type", ""),
            "role"      => $login_type,
            "mail_upload_address"       => $mail_upload_address,
            "account_model"       => $user_info["user_info"]["account_model"],
            );
        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);
        if ( !$meetingDetail )
            return $this->render_valid();
        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set( "lang", $lang );
        $this->session->set( "type", $meetingDetail["participant_type_name"] );
        $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set( "participant_key", $meetingDetail["participant_key"] );
        $this->session->set( "mail_upload_address", $mail_upload_address );
        header( "Location: ./?action_meeting_display");
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,array($room_key, $meeting_info));
    }


    function action_meeting_display()
    {
        $type = $this->session->get( "type" );
        $this->template->assign('charset', "UTF-8" );
        $this->template->assign('locale' ,  $this->get_language());
        $this->display( sprintf( 'core/%s/meeting_base.t.html', $type ) );
    }

    /**
     * 認証会議への入室
     *
     * @param string meeting_key 会議キー
     * @param string n2my_session n2myのセッションID
     * @param string reservation_pw 予約パスワード
     * @return パスワードがあっていれば入室、間違っていれば再度認証画面に遷移
     */
    function action_auth_start()
    {
        $meeting_key = $this->request->get("meeting_key");
        $session_id = $this->request->get("n2my_session");
        if ($meeting_key) {
            $meeting_info = $_SESSION["meeting_info"][$meeting_key];
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__, $meeting_info);
            // パスワードあり
            if ($meeting_info["meeting_password"]) {
                $password = $this->request->get("reservation_pw");
                $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array($password, md5($password)));
                // 入力パスワード同じならOK
                if (md5($password) == $meeting_info["meeting_password"]) {
                    $url = $meeting_info["start_url"];
                    $this->logger->info(__FUNCTION__."#予約パスワード付き", __FILE__, __LINE__, $url);
                    header("Location: ".$url);
                    exit();
                } else {
                    $this->template->assign("session_id", $session_id);
                    $this->template->assign("meeting_key", $meeting_key);
                    return $this->display("user/api/meeting/auth.t.html");
                }
            } else {
                // パスワード無し／予約なし
                $url = $meeting_info["start_url"];
                $this->logger->info(__FUNCTION__."#予約無し", __FILE__, __LINE__, $url);
                header("Location: ".$url);
                exit();
            }
        }
    }

    /**
     * ユーザーログインパスワードのみ変更
     *
     * @param string user_id  ユーザーID
     * @param string user_passwood ユーザーログインパスワード
     * @return boolean
     */
    function action_modify_user_loginpassword()
    {
        // ユーザーログインパスワード変更
    }

    /**
     * ユーザーアドミンパスワードのみ変更
     *
     * @param string user_id  ユーザーID
     * @param string user_admin_passwood ユーザーアドミンパスワード
     * @return boolean
     */
    function action_modify_user_adminpassword()
    {
        // ユーザー追加
    }

    /**
     * ユーザーアカウント追加
     *
     * @param int reseller_key リセラーキー
     * @param string country_key 国キー (デフォルト 1（日本）)
     * @param string user_id  ユーザーID
     * @param string user_passwood ユーザーパスワード
     * @param string user_admin_password ユーザーアドミンパスワード
     * @param string user_company_name 会社名 (デフォルト null)
     * @param string user_company_postnumber 会社郵便番号 (デフォルト null)
     * @param string user_company_address 会社住所 (デフォルト null)
     * @param string user_company_phone 会社電話番号 (デフォルト null)
     * @param string user_company_fax 会社FAX番号 (デフォルト null)
     * @param string user_staff_firstname 担当者姓 (デフォルト null)
     * @param string user_staff_lastname 担当者名 (デフォルト null)
     * @param string user_staff_email 担当者メールアドレス (デフォルト null)
     * @param datetime user_starttime 利用開始日 (デフォルト null)
     * @param datetime user_registtime 請求開始日 (デフォルト null)
     * @return boolean
     */
    function action_add_user()
    {
        // ユーザー追加
    }

    /**
     * ユーザーアカウント情報変更
     *
     * @param int reseller_key リセラーキー (デフォルト null)
     * @param string country_key 国キー (デフォルト 1（日本）)
     * @param string user_id  ユーザーID
     * @param string user_passwood ユーザーパスワード (デフォルト null)
     * @param string user_admin_password ユーザーアドミンパスワード (デフォルト null)
     * @param string user_company_name 会社名 (デフォルト null)
     * @param string user_company_postnumber 会社郵便番号 (デフォルト null)
     * @param string user_company_address 会社住所 (デフォルト null)
     * @param string user_company_phone 会社電話番号 (デフォルト null)
     * @param string user_company_fax 会社FAX番号 (デフォルト null)
     * @param string user_staff_firstname 担当者姓 (デフォルト null)
     * @param string user_staff_lastname 担当者名 (デフォルト null)
     * @param string user_staff_email 担当者メールアドレス (デフォルト null)
     * @param datetime user_starttime 利用開始日 (デフォルト null)
     * @param datetime_registtime 請求開始日 (デフォルト null)
     * @return boolean
     */
    function action_modify_user()
    {
        // ユーザー情報取得
        // ユーザー情報変更
    }

    /**
     * ユーザーアカウント削除
     *
     * @param string user_id  ユーザーID
     * @return boolean
     */
    function action_delete_user()
    {
        // ユーザーアカウント削除
    }

    /**
     * メンバーアカウント追加
     *
     * @param string user_id  ユーザーID
     * @param string member_id メンバーID
     * @param string member_password メンバーパスワード
     * @param string member_name メンバー名
     * @param int membaer_group メンバーグループ番号
     * @return boolean
     */
    function action_add_member()
    {
        // ユーザーアカウント追加
    }

    /**
     * メンバーアカウント修正
     *
     * @param string user_id  ユーザーID
     * @param string member_id メンバーID
     * @param string member_password メンバーパスワード
     * @param string member_name メンバー名
     * @param int membaer_group メンバーグループ番号
     * @return boolean
     */
    function action_modify_member()
    {
        // メンバーアカウント修正
    }

    /**
     * メンバーアカウント削除
     *
     * @param string user_id  ユーザーID
     * @param string member_id  メンバーID
     * @return boolean
     */
    function action_delete_member()
    {
        // メンバーアカウント削除
    }

    /**
     * グループ追加
     *
     * @param string user_id  ユーザーID
     * @param string group_name グループ名
     * @return boolean
     */
    function action_add_member_group()
    {
        // メンバーグループ追加
    }

    /**
     * グループ名修正
     *
     * @param string user_id  ユーザーID
     * @param string group_name グループ名
     * @return boolean
     */
    function action_modify_member_group()
    {
        // メンバーグループ名修正
    }

    /**
     * グループ削除
     *
     * @param string user_id  ユーザーID
     * @param string group_id グループID
     * @return boolean
     */
    function action_delete_member_group()
    {
        // メンバーグループ削除
    }

    /**
     * 部屋名変更
     *
     * @param string room_key 部屋キー
     * @return boolean
     */
    function action_modify_room()
    {
        // 部屋名変更
    }

    /**
     * 部屋追加
     *
     * @param string user_id  ユーザーID
     * @return boolean
     */
    function action_add_room()
    {
        // 部屋追加
    }

    /**
     * 部屋削除
     *
     * @param string user_id  ユーザーID
     * @param string room_key  ルームキー
     * @return boolean
     */
    function action_delete_room()
    {
        // 部屋削除
    }

    /**
     * オプション追加
     *
     * @param string room_key 部屋キー
     * @param string service_option_key  追加するオプションキー
     * @return boolean
     */
    function action_add_option()
    {
        // オプション追加
    }

    /**
     * オプション削除
     *
     * @param string room_key 部屋キー
     * @param string service_option_key  削除するオプションキー
     * @return boolean
     */
    function action_delete_option()
    {
        // オプション追加
    }

    /**
     * default_view
     *
     * @param string pw パスワード
     * @return
     */
    function default_view()
    {
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__);
        //$this->display("user/api/meeting/debug.t.html");
        $pw = $this->request->get("pw");
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__,md5($pw));
        return $this->logger->info(__FUNCTION__, __FILE__, __LINE__);
    }

    /**
     * デバック用
     *
     * @param
     * @return
     */
    function action_debug()
    {
        $this->action_login();
        $this->action_get_room_list();
        $this->action_create_meeting();
        $this->action_logout();
    }
}
$session_id = isset($_REQUEST["n2my_session"]) ? $_REQUEST["n2my_session"] : null;
if ($session_id) {
    session_id($session_id);
}
$main = new N2MY_Meeting_API();
$main->execute();
?>
