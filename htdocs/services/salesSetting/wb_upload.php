<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once ("classes/AppFrame.class.php");
require_once ("classes/N2MY_Document.class.php");
//require_once ("classes/N2MY_Wbupload.class.php");
require_once ("classes/mgm/MGM_Auth.class.php");
require_once ('lib/EZLib/EZUtil/EZDate.class.php');
require_once ("lib/EZLib/EZUtil/EZString.class.php");
require_once ("classes/dbi/reservation_user.dbi.php");
require_once ("classes/N2MY_Storage.class.php");
require_once ("classes/core/dbi/Storage.dbi.php");
require_once ("classes/N2MY_Clip.class.php");

class AppWhiteBoardUpload extends AppFrame {

    var $_name_space = null;
    var $_ssl_mode = null;
    var $obj_N2MYDocument = null;
    var $obj_N2MYClip     = null;
    /**
     * 初期処理
     */
    function init() {
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        // 認証後のみ
        $server_info = $this->session->get("server_info");
        $this->obj_N2MYDocument    = new N2MY_Document($this->get_dsn());
        $this->meetingClipTable = new MeetingClipTable($this->get_dsn());
        $this->obj_N2MYClip = new N2MY_Clip($this->get_dsn());
        //$this->obj_N2MYWbupload    = new N2MY_Wbupload($this->get_dsn(), $server_info["host_name"]);
    }

    /**
     * 事前アップロード一覧
     */
    function action_top($room_key = "", $message="") {
        $user_info = $this->session->get( "user_info" );
        if (!$room_key) {
            $room_key = $this->request->get("room_key");
        }

        $room_info = $this->get_room_info($room_key);
        // room_key チェック
        if($room_info["room_info"]["user_key"] != $user_info["user_key"] || !$room_info["room_info"]["use_sales_option"]){
            header("Location: /services/salesSetting");
            return;
        }

        if ($message) {
            $this->session->set("message", $message);
        }
        // 部屋毎の事前アップロードファイル取得
        $where = "document_path = '".$user_info["user_id"]."/".$room_key."/sales/'".
                 " AND document_mode = 'sales_pre'";
        $_documents = $this->obj_N2MYDocument->getList($where);
        $documents = array();
        foreach($_documents as $document) {
            $document_id = $document["document_id"];
            $documents[$document_id] = array(
                "document_id"     => $document_id,
                "name"            => $document["document_name"],
                "type"            => $document["document_extension"],
                "status"          => $document["document_status"],
                "category"        => $document["document_category"],
                "index"           => $document["document_index"],
                "sort"            => $document["document_sort"],
                "format"          => $document["document_format"],
                "version"         => $document["document_version"],
                "is_storage"      => $document["is_storage"],
                "create_datetime" => $document["create_datetime"],
                "update_datetime" => $document["update_datetime"],
            );
        }
//var_dump($documents);
        $this->setDocumentData($documents);
        $this->render_top();
    }

    /**
     * 事前アップロード一覧表示
     */
    function render_top($room_key = "", $message="") {

        $this->set_submit_key($this->_name_space);
        $room_key = $this->request->get("room_key");
        $room_info = $this->get_room_info($room_key);
        $documents = $this->getDocumentData();
        $message = $this->session->get("message");
        if ($message) {
            $this->logger2->info($message);
            $this->template->assign("message", $message);
            $this->session->remove("message");
        }
        $objDocument = new N2MY_Document($this->get_dsn());
        $support_document_list = $objDocument->getSupportFormat($room_key);
        foreach($support_document_list["document_list"] as $key => $value){
            $document_list[] = $key;
        }
        foreach($support_document_list["image_list"] as $key => $value){
            $image_list[] = $key;
        }

        $image_maxsize = ($room_info["room_info"]["whiteboard_size"] <= 5) ? $room_info["room_info"]["whiteboard_size"] : 5;
        $document_maxsize = ($room_info["room_info"]["whiteboard_size"] <= 20) ? $room_info["room_info"]["whiteboard_size"] : 20;
        $user_info = $this->session->get( "user_info" );
        $clips = $this->obj_N2MYClip->getClipListByRoomKey($room_key);
        $this->logger2->trace($documents_user);
        $this->template->assign("image_maxsize", $image_maxsize);
        $this->template->assign("document_maxsize", $document_maxsize);
        $this->template->assign("document_list", $document_list);
        $this->template->assign("image_list", $image_list);
        $this->template->assign("documents", $documents);
        $this->template->assign("room_key", $room_key);
        $this->template->assign("clips", $clips);
        $this->display('user/salesSetting/upload.t.html');
    }

    /**
     * 資料追加
     */
    function action_edit_document() {

        $request = $this->request->getAll();

        $room_key = $request["room_key"];
        $document_data = $this->formatDocumentData($request);
//$this->logger2->info($document_data);
        // 入力チェック
        if ($message = $this->check($document_data)) {
            return $this->action_top($room_key, $message);
        } else {
            if (!$this->check_submit_key($this->_name_space)) {
                $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
                return $this->render_top($room_key);
            }
            $serverInfo = $this->session->get("server_info");
            // 更新処理
            $obj_storage = new N2MY_Storage($this->get_dsn(), $serverInfo["host_name"]);
            $obj_storage->salesRoomDocUpdate($document_data, $room_key);
        }

        // 登録完了（一覧表示）
        header("Location: /services/salesSetting/wb_upload.php?room_key=".$room_key);
    }

    function action_delete_document() {
        $request = $this->request->getAll();
        $delete_document["document_id"] = $request["delete_document"];
        $delete_document["is_storage"] = 1;
        $this->obj_N2MYDocument->delete($delete_document);
        $this->action_top($request["room_key"]);
    }

    function action_delete_clip() {
        $request = $this->request->getAll();
        $delete_clip = $request["delete_clip"];
        $room_key = $request["room_key"];
        $this->obj_N2MYClip->salesDeleteRelations($room_key , $delete_clip);
        $this->action_top($request["room_key"]);
    }

    /**
     * 入力内容を維持
     */
    private function setDocumentData($document_data) {
        $this->session->set("document_data", $document_data);
    }

    /**
     * 設定されている値を取得
     */
    private function getDocumentData() {
        return $this->session->get("document_data");
    }

    /**
     * 入力内容をクリア
     */
    private function clearDocumentData() {
        return $this->session->remove("document_data");
    }

    /**
     * デフォルトページ
     */
    function default_view()
    {
        $this->action_top();
    }

    /**
     * 入力フォーマットを整形
     */
    private function formatDocumentData($document_data) {
        // ファイル一覧

        // ストレージ
        require_once ("classes/N2MY_Storage.class.php");
        $objStorage = new N2MY_Storage($this->get_dsn(), $this->session->get("server_info"));
        $storage_count = count($document_data["storage_file_id"]);
        for($i = 0; $i < $storage_count; $i++){
            $storage_info = $objStorage->getStorageInfo($document_data["storage_file_id"][$i]);
            $storage_info["name"] = $document_data["storage_file_name"][$i]?$document_data["storage_file_name"][$i]:$storage_info["user_file_name"];
            $storage_info["is_storage"] = 1;
            $storage_info["document_sort"] = $document_data["sort"][$i];
            if($storage_info["category"] == "document" || $storage_info["category"] == "image"){
                $document_data["storage_documents"][] = $storage_info;
                $document_data["documents_dsp"][] = $storage_info;
            }else if ($storage_info["category"] == "video"){
                $document_data['clips'][] = $storage_info;
            }
            else{

            }
        }

        $documents = array();

        // すでにアップ済みのドキュメント
        foreach($document_data["id"] as $id){
            $document["id"] = $id;
            $document["document_name"] = $document_data["filename"][$id];
            $document["document_sort"] = $document_data["sort"][$id];
            $documents[] = $document;
        }

        // 資料をソート
        $sort_arr = array();
        foreach($documents AS $uniqid => $row){
            foreach($row AS $key=>$value){
                $sort_arr[$key][$uniqid] = $value;
            }
        }
        if ($documents) {
            array_multisort($sort_arr["sort"], SORT_ASC, $documents);
        }

        // アップ済み動画
        foreach($document_data["clip_key"] as $clip_key){
            // 削除する動画はリストから外す
            if(!in_array($clip_key , $document_data[delete_clip])){
                $clip["meeting_clip_name"] = $document_data["meeting_clip_name"][$clip_key];
                $clip["clip_key"] = $clip_key;
                $clip["storage_file_key"] = $document_data["storage_file_key"][$clip_key];
                $document_data['clips'][] = $clip;
            }
        }

        $document_data["documents"] = $documents;
        $deletes = array();
        foreach($document_data["delete_documents"] as $delete_document){
            $delete["document_id"] = $delete_document;
            $delete["is_storage"] = $document_data["is_storage"][$delete_document];
            $deletes[] = $delete;
        }
        $document_data["delete_documents"] = $deletes;
        $this->logger2->debug($document_data);
        return $document_data;
    }

    /**
     * 入力チェック
     */
    function check($document_data, $method = "") {
        if($document_data["info"]){
            $document_data = $document_data["info"];
        }
        $room_key = $document_data["room_key"];
        $room_info = $this->get_room_info($room_key);
            // ストレージファイルのチェック
        if($document_data["storage_documents"]){
            // 対応フォーマット取得
            require_once ("classes/N2MY_Document.class.php");
            $objDocument = new N2MY_Document($this->get_dsn());
            $support_document_list = $objDocument->getSupportFormat($room_key);
            foreach($document_data["storage_documents"] as $storage){
                $extension = strtolower($storage["extension"]);
                if (!in_array($extension, $support_document_list['support_ext_list'])) {
                    $message .= '<li>'.$this->get_message("ERROR", "filetype"). '</li>';
                }else if($room_info["room_info"]["whiteboard_page"] < $storage["document_index"]){
                    $message .= '<li>'.$this->get_message("ERROR", "maxindex"). '</li>';
                }else{
                    if (in_array($storage["extension"], $support_document_list['image_ext_list'])) {
                        $maxsize = ($room_info["room_info"]["whiteboard_size"] <= 5) ? $room_info["room_info"]["whiteboard_size"] : 5;
                        // 資料
                    }else {
                        $maxsize = ($room_info["room_info"]["whiteboard_size"] <= 20) ? $room_info["room_info"]["whiteboard_size"] : 20;
                    }
                    if ($storage["file_size"] > $maxsize * 1024 * 1024) {
                        $message .= '<li>'.$this->get_message("ERROR", "maxfilesize"). '</li>';
                    }
                }

            }
        }
        // 動画の重複チェック
        if($document_data["clips"]){
            foreach($document_data["clips"] as $clips){
                $clip_keys[] = $clips["clip_key"];
            }
            // クリックキーで重複チェックする
            $clip_count = count($clip_keys);
            $count_unique = count(array_unique($clip_keys));
            if($clip_count != $count_unique){
                $message .= '<li>'.$this->get_message("ERROR", "uniquevideo"). '</li>';
            }

        }
        // 入力エラー
        if ($message) {
            $this->logger2->info(array($document_data, $message));
        }
        return $message;
    }

    function action_wb_status() {
        $user_info = $this->session->get( "user_info" );
        $room_key = $this->request->get("room_key");
        $where = "document_path = '".$user_info["user_id"]."/".$room_key."/'".
                 " AND document_mode = 'pre'";
        $documents = $this->obj_N2MYDocument->getList($where);
        if (DB::isError($documents)) {
            $this->logger2->error($documents->getUserInfo());
            return false;
        }
        $document_list = array();
        foreach($documents as $document) {
            $document_id = $document["document_id"];
            $document_list[] = array(
                "document_id"     => $document["document_id"],
                "name"            => $document["document_name"],
                "size"            => $document["document_size"],
                "category"        => $document["document_category"],
                "extension"       => $document["document_extension"],
                "status"          => $document["document_status"],
                "index"           => $document["document_index"],
                "sort"            => $document["document_sort"],
                "format"          => $document["document_format"],
                "version"         => $document["document_version"],
                "create_datetime" => $document["create_datetime"],
                "update_datetime" => $document["update_datetime"],
            );
        }
        print json_encode($document_list);
    }

    function action_wb_view() {
        $document_id = $this->request->get("document_id");
        $page = $this->request->get("page", 1);
        $reservation_data = $this->getReservationData();
        $this->logger2->info($reservation_data);
        $meeting_key = $reservation_data["info"]["meeting_key"];
        if ($document_data = $this->obj_N2MY_Reservation->getDocumentData($meeting_key, $document_id)) {
            $this->logger2->info($document_data);
            $file = N2MY_DOC_DIR.$document_data["document_path"].$document_id."/".$document_id."_".$page.".jpg";
            if (file_exists($file)) {
                header("Content-Type: image/jpg;");
                header("Cache-Control: public, must-revalidate");
                print file_get_contents($file);
                exit();
            }
        }
        print "error!";
    }

    /**
     * プレビュー取得
     */
    public function action_show_preview()
    {
        $request = $this->request->getAll();
        require_once ("classes/N2MY_Document.class.php");
        $objDocument = new N2MY_Document( $this->get_dsn() );
        $documentInfo = $objDocument->getDetail( $request["document_id"] );
        if ($documentInfo["document_format"] == "vector") {
            $return["fileName"] = sprintf( "%s_%d.swf", $documentInfo["document_id"], $request["index"] );
            $path = N2MY_DOC_DIR.$documentInfo["document_path"].$documentInfo["document_id"]."/".$return["fileName"];
            if (file_exists($path)){
                header('Content-type: application/x-shockwave-flash');
                header('Content-Length: '.filesize($path));
                $fp = fopen($path, "r");
                if ($fp) {
                    while (!feof($fp)) {
                        $buffer = fgets($fp, 4096);
                        echo $buffer;
                    }
                    fclose($fp);
                }
            }
        } else {
            $fileInfo = $objDocument->getPreviewFileInfo( $documentInfo, $request["index"] );
            //resize
            $max_width = 480;
            $max_height = 320;
            $percent = 0.5;

            list($width, $height) = getimagesize($fileInfo["targetImage"]);
            $x_ratio = $max_width / $width;
            $y_ratio = $max_height / $height;

            if( ($width <= $max_width) && ($height <= $max_height) ){
                $tn_width = $width;
                $tn_height = $height;
            } elseif (($x_ratio * $height) < $max_height) {
                $tn_height = ceil($x_ratio * $height);
                $tn_width = $max_width;
            } else {
                $tn_width = ceil($y_ratio * $width);
                $tn_height = $max_height;
            }

    //        $new_width = $width * $percent;
    //        $new_height = $height * $percent;
            header('Content-type: image/jpeg');
            // 再サンプル
            $image_p = imagecreatetruecolor($tn_width, $tn_height);
            $image = imagecreatefromjpeg($fileInfo["targetImage"]);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
            imagejpeg($image_p, null, 100);

    //        mb_http_output("pass");
    //        header( sprintf( "Content-type: %s", $fileInfo["mime"] ) );
    //        header( sprintf( "Content-Disposition: inline; filename=%s", $fileInfo["fileName"] ) );
    //        readfile( $fileInfo["targetImage"] );
        }
    }
}

$main = new AppWhiteBoardUpload();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
