<?php

require_once("classes/AppFrame.class.php");
require_once("classes/core/dbi/Meeting.dbi.php");
require_once( "lib/EZLib/EZUtil/EZEncrypt.class.php");

class AppOndemand extends AppFrame
{
    var $logger = null;

    function init()
    {
    }

    public function default_view()
    {
        $request = $this->request->getAll();
//        print $_SERVER["QUERY_STRING"];
        $params = split('&', $_SERVER["QUERY_STRING"]);
        foreach($params as $param) {
            if (substr($param, 0, strpos($param, '=')) == 'id') {
                $id = substr($param, (strpos($param, '=') + 1));
            }
        }
        $key = N2MY_ENCRYPT_KEY;
        $iv = N2MY_ENCRYPT_IV;
        $id = str_replace("-", "/", $id);
        $id = str_replace(' ', '+', $id);
        $id = str_replace('_', '+', $id);
        $encryptdata = EZEncrypt::decrypt($key, $iv, $id);

        $urlInfo = split(",", $encryptdata);
        $this->logger2->info($urlInfo);
        $mode = $urlInfo[1];
        $lang = $urlInfo[2];
        $meetingKey = $urlInfo[0];
        $meetingSequenceKey = $urlInfo[3];
        $this->session->set("login", 1);
        $this->session->set("lang", $lang);
        $serverList = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true );
        list( $key, $dsn ) = each( $serverList["SERVER_LIST"] );

        $obj_Meeting  = new DBI_Meeting($dsn);

        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        $objMeetingSequence = new DBI_MeetingSequence($dsn);
        $where = "meeting_key = '" .$meetingKey."'".
            " AND meeting_sequence_key = '".$meetingSequenceKey."'";
        $meeting_sequence_info = $objMeetingSequence->getRow($where);

        if ($rec_gw = $this->request->get("rec_gw")) {
            $this->session->set("ondemand", 0);
            $this->session->set("record_gw", 1);
            $this->template->assign("api_url", N2MY_LOCAL_URL);
            $where = sprintf( "meeting_key='%s' AND is_deleted=0", $meetingKey );
            $meeting_info = $obj_Meeting->getRow( $where );
            if (!$meeting_sequence_info) {
                $this->logger2->warn($urlInfo, "録画GW用のオンデマンド再生無効");
                $meeting_info = "";
            } else {
                $record_id = $meeting_sequence_info["record_id"];
                require_once 'classes/mgm/MGM_Auth.class.php';
                $objMgmRelationTable = new MGM_AuthClass(N2MY_MDB_DSN);
                if (!$server_info = $objMgmRelationTable->getRelationDsn($record_id, "record_gw")) {
                    $this->logger2->warn($urlInfo, "録画GW用のオンデマンド再生無効");
                    $meeting_info = "";
                }
            }
        } else {
            $this->session->set("ondemand", 1);
            $this->session->set("record_gw", 0);
            $this->template->assign("api_url", N2MY_BASE_URL);
            $where = sprintf( "meeting_key='%s' AND is_deleted=0 AND is_publish=1", $meetingKey );
            $meeting_info = $obj_Meeting->getRow( $where );
        }

        //$this->session->set( "display_deleteButton", false );
        //set parameters into session
        if ( ! $meeting_info || !$meeting_sequence_info ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$meeting_info);
            $message["pageTitle"] = $this->get_message("ONDEMAND", "pagetitle");
            $message["title"] = $this->get_message("ONDEMAND", "title");
            $message["body"] = $this->get_message("ONDEMAND", "body");
            $this->template->assign('message', $message);
            $this->display( "common.t.html" );
            exit;
        }
        $this->session->set( "ondemand_log_meeting_key", $meetingKey );
        $this->session->set( "ondemand_log_sequence_key", $meetingSequenceKey );
        $this->session->set( "ondemand_log_type", $mode );

        //assign values
        $this->template->assign('charset', $this->config->get( "GLOBAL", "html_output_char" ) );
        $this->template->assign('locale' , $this->get_language() );
        $this->template->assign('meeting_type' , "ondemand");

        // service info
        $frame["service_info"] = $this->get_message("SERVICE_INFO");
        $this->template->assign("__frame", $frame);
        $log_fl_ver = $this->request->get("log_fl_ver");
        $this->session->set("log_fl_ver", $log_fl_ver);
        //display template
        switch ( $mode ) {
            case 'log_video':
                if ($log_fl_ver == "as2") {
                $this->logger2->info($log_fl_ver);
                    $this->display( "core/meeting_log/video_base.t.html" );
                } else {
                    $this->display( "core/meeting_log/video_base_as3.t.html" );
                }
                break;

            default:
                $this->display( "core/meeting_log/minutes_base.t.html" );
                break;
        }
    }
}

$main = new AppOndemand();
$main->execute();
