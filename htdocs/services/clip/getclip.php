<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("config/config.inc.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Clip.class.php");
require_once("classes/dbi/clip.dbi.php");

class AppGetclip extends AppFrame {

    /**
     * ログクラス
     * @access public
     */
    var $logger     = null;
    var $meetingLog = null;

    function init() {
        $this->obj_N2MY_Clip = new N2MY_Clip( $this->get_dsn() );
        $this->clip_obj      = new ClipTable( $this->get_dsn() );
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->action_showDetail();
    }

    /**
     * プレイヤーレンダリング処理
     */
    public function action_showDetail()
    {

        $request = $this->request->getAll();
        if( empty($request["clip_key"]) ){
            $this->logger2->error($request, "clip_key lost");
            exit;
        }

        $user_info   = $this->session->get( "user_info" );

        $wheres[] = sprintf( "clip_key = '%s'", mysql_real_escape_string( $request["clip_key"] ) );
        $wheres[] = sprintf( "user_key = '%s'", mysql_real_escape_string( $user_info["user_key"] ) );
        $wheres[] = sprintf( "clip_status = '%s'", 2 );
        $wheres[] = sprintf( "is_deleted = '%s'", 0 );

        $cnt = $this->clip_obj->numRows( implode( " AND ", $wheres ) );
        if( $cnt != 1 ){
            $this->logger2->error( "clip not found" );
            exit;
        }

        $vido_config = $this->config->getAll( "VIDEO" );
        $this->template->assign("skin",        $vido_config["base_skin"]);
        $this->template->assign("skin_width",  $vido_config["base_skin_width"]);
        $this->template->assign("skin_height", $vido_config["base_skin_height"]);
        $this->template->assign("client_id",   $vido_config["client_id"]);
        $this->template->assign("clip_key",    $request["clip_key"]);
        $this->display('user/clip/getclip.t.html');

    }
}

$main = new AppGetclip();
$main->execute();
