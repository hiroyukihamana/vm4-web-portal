<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once('lib/amf/app/Gateway.php');
$path = '../../../webapp/classes/';
$gateway = new Gateway();
$gateway->setBaseClassPath($path);
$gateway->service();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
