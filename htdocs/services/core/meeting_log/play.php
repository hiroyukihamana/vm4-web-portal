<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Account.class.php");
require_once('classes/core/dbi/Meeting.dbi.php');

class Service_LogPlayer extends AppFrame
{
    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth() {
        $this->checkAuth();
    }

    function default_view()
    {
        $request = $this->request->getAll();
        $member_info = $this->session->get( "member_info" );
        $user_info = $this->session->get( "user_info");
        $obj_Meeting  = new DBI_Meeting( $this->get_dsn() );
        $string_positiont = strrpos($request["id"], "_");
        $check_room_key = str_split($request["id"], $string_positiont);
        $request["room_key"] = $check_room_key[0];
/*
        if($user_info["is_add_room"]) {
            require_once("classes/dbi/room.dbi.php");
            $objRoom = new RoomTable($this->get_dsn());
            $where = "user_key='".$user_info["user_key"]."'";
            $all_rooms = $objRoom->getRowsAssoc($where);
            $room_key_array = array();
            foreach ($all_rooms as $room) {
                $room_key_array[] = $room["room_key"];
            }
        } else {
            $room_info = $this->session->get("room_info");
            $room_key_array = array_keys($room_info);
        }
*/
        $rules = array("room_key" => array("required" => true),
                       "sequence_key" => array("numeric"   => true));

        $check_obj = new EZValidator($request);
        foreach($rules as $field => $rules) {
            $check_obj->check($field, $rules);
        }
        if (EZValidator::isError($check_obj)) {
            $this->logger2->warn(array("parameter error", $check_obj));
            return $this->display('error.t.html');
        }
        $where = "meeting_ticket='".mysql_real_escape_string($request["id"])."' AND is_deleted=0";
        $meeting_info     = $obj_Meeting->getRow( $where );

        ( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") &&
            $member_info["room_key"] != $meeting_info["room_key"] ) ?
            $this->session->set( "display_deleteButton", false ) :
            $this->session->set( "display_deleteButton", true );
        //set parameters into session
        if ( ! $meeting_info ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$meeting_info);
            $this->_error = "MEETING_TICKET_ERROR";
        }
        //$this->session->remove("ondemand");
        $this->session->set( "meeting_key", $meeting_info["meeting_key"] );
        $this->session->set( "sequence_key", $request["sequence_key"] );
        $this->session->set( "log_meeting_key", $meeting_info["meeting_key"] );
        $this->session->set( "log_sequence_key", $request["sequence_key"] );
        $this->session->set( "log_type", $request["mode"] );

        // 録画GW情報取得
        require_once 'classes/core/dbi/MeetingSequence.dbi.php';
        $objMeetingSequence = new DBI_MeetingSequence( $this->get_dsn() );
        $where = "meeting_sequence_key = '".addslashes($request["sequence_key"])."'";
        if ($meeting_sequence_info = $objMeetingSequence->getRow($where)) {
            $this->session->set("display_deleteButton", ($meeting_sequence_info["record_status"] != "convert") ? true : false);
        }

        //assign values
        $this->template->assign('charset', $this->config->get( "GLOBAL", "html_output_char" ) );
        $this->template->assign('locale' , $this->get_language() );

        // service info
        $frame["service_info"] = $this->get_message("SERVICE_INFO");
        $this->template->assign("meeting_version", $user_info["meeting_version"]);
        $this->template->assign("__frame", $frame);
        $this->template->assign("api_url", N2MY_BASE_URL);
        $this->template->assign("auth_key", N2MY_FLASH_API_AUTH_KEY );
        $this->template->assign("server_dsn_key", $this->get_dsn_key() );
        $this->template->assign("meeting_key", $meeting_info['meeting_key']);
        $this->template->assign("core_session_key", $meeting_info["meeting_session_id"] );
        $max_tmp_file_size = $this->config->get("N2MY", "max_tmp_file_size") ? $this->config->get("N2MY", "max_tmp_file_size") : 50000000;
        $this->template->assign("maxTmpFileSize", $max_tmp_file_size );

        //display template
        $log_fl_ver = $this->request->get("log_fl_ver");
        $this->session->set("log_fl_ver", $log_fl_ver);
        // FMSサーバにファイルがあるかの確認(なければS3から取得)
        $obj_FmsService = new DBI_FmsServer(N2MY_MDB_DSN);
        $where = "server_key = ".$meeting_sequence_info["server_key"];
        $fms_server_info = $obj_FmsService->getRow($where);
        $fms_app_name = $this->config->get("CORE", "app_name");
        $fms_address = $fms_server_info["local_address"] ? $fms_server_info["local_address"] : $fms_server_info["server_address"];

        //再生日時登録
        $where = "meeting_sequence_key = '".addslashes($request["sequence_key"])."'";
        $data = array("last_play_datetime" => date("Y-m-d H:i:s"));
        $objMeetingSequence->update($data, $where);
        switch ( $request["mode"] ) {
            case 'log_video':
                // FMSサーバにファイルがあるかの確認(なければS3から取得)
                // 録画データの場合は両方必要
                $url = "http://".$fms_address.":".$fms_server_info["server_port"]."/fms_api.php" .
                    "?app_name=" .urlencode($fms_app_name).
                    "&method=dir_check".
                    "&dir=".$meeting_info["fms_path"].
                    "&meeting_sequence_key=".$request["sequence_key"].
                    "&meeting_key=".$meeting_info["meeting_key"].
                    "&meeting_id=".$meeting_info["meeting_session_id"].
                    "&type=video";
                $this->logger2->debug($url);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
                curl_setopt($ch, CURLOPT_TIMEOUT, 500);
                $result = curl_exec($ch);
                curl_close($ch);
                if($result == "-1" && $meeting_sequence_info["has_recorded_video"] == 1){
                    // 会議記録ファイルの取得(streamsファイル)
                    $url = "http://".$fms_address.":".$fms_server_info["server_port"]."/fms_api.php" .
                        "?app_name=" .urlencode($fms_app_name).
                        "&method=download_tar".
                        "&dir=".$meeting_info["fms_path"].
                        "&type=video".
                        "&meeting_sequence_key=".$request["sequence_key"].
                        "&meeting_key=".$meeting_info["meeting_key"].
                        "&meeting_id=".$meeting_info["meeting_session_id"];
                    $this->logger2->debug($url);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 500);
                    $dl_result = curl_exec($ch);
                    $this->logger2->info("FOR S3 DATA");
                    curl_close($ch);
                }

                // FMSサーバにファイルがあるかの確認(なければS3から取得)
                $url = "http://".$fms_address.":".$fms_server_info["server_port"]."/fms_api.php" .
                        "?app_name=" .urlencode($fms_app_name).
                        "&method=dir_check".
                        "&dir=".$meeting_info["fms_path"].
                        "&meeting_sequence_key=".$request["sequence_key"].
                        "&meeting_key=".$meeting_info["meeting_key"].
                        "&meeting_id=".$meeting_info["meeting_session_id"].
                        "&type=minutes";
                $this->logger2->info($url);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
                curl_setopt($ch, CURLOPT_TIMEOUT, 500);
                $result = curl_exec($ch);
                curl_close($ch);
                if($result == "-1" && $meeting_sequence_info["has_recorded_video"] == 1){
                    // 会議記録ファイルの取得(sharedobjectsファイル)
                    $url = "http://".$fms_address.":".$fms_server_info["server_port"]."/fms_api.php" .
                            "?app_name=" .urlencode($fms_app_name).
                            "&method=download_tar".
                            "&dir=".$meeting_info["fms_path"].
                            "&type=minutes".
                            "&meeting_sequence_key=".$request["sequence_key"].
                            "&meeting_key=".$meeting_info["meeting_key"].
                            "&meeting_id=".$meeting_info["meeting_session_id"];
                    $this->logger2->debug($url);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 500);
                    $dl_result = curl_exec($ch);
                    $this->logger2->info("FOR S3 DATA");
                    curl_close($ch);
                }

                // 操作ログ
                $this->add_operation_log('meetinglog_play_video', array(
                    'room_key'      => $meeting_info['room_key'],
                    'meeting_name'  => $meeting_info['meeting_name']
                    ));
                if ($log_fl_ver == "as2") {
                    $this->display( "core/meeting_log/video_base.t.html" );
                } else {
                    $this->display( "core/meeting_log/video_base_as3.t.html" );
                }
                break;

            default:
                // FMSサーバにファイルがあるかの確認(なければS3から取得)
                $url = "http://".$fms_address.":".$fms_server_info["server_port"]."/fms_api.php" .
                        "?app_name=" .urlencode($fms_app_name).
                        "&method=dir_check".
                        "&dir=".$meeting_info["fms_path"].
                        "&meeting_sequence_key=".$request["sequence_key"].
                        "&meeting_key=".$meeting_info["meeting_key"].
                        "&meeting_id=".$meeting_info["meeting_session_id"].
                        "&type=minutes";
                $this->logger2->info($url);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
                curl_setopt($ch, CURLOPT_TIMEOUT, 500);
                $result = curl_exec($ch);
                curl_close($ch);
                if($result == "-1" && $meeting_sequence_info["has_recorded_minutes"] == 1 ){
                    // 会議記録ファイルの取得(sharedobjectsファイル)
                    $url = "http://".$fms_address.":".$fms_server_info["server_port"]."/fms_api.php" .
                            "?app_name=" .urlencode($fms_app_name).
                            "&method=download_tar".
                            "&dir=".$meeting_info["fms_path"].
                            "&type=minutes".
                            "&meeting_sequence_key=".$request["sequence_key"].
                            "&meeting_key=".$meeting_info["meeting_key"].
                            "&meeting_id=".$meeting_info["meeting_session_id"];
                    $this->logger2->debug($url);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 500);
                    $dl_result = curl_exec($ch);
                    $this->logger2->info("FOR S3 DATA");
                    curl_close($ch);
                }
                // 操作ログ
                $this->add_operation_log('meetinglog_play_minutes', array(
                    'room_key'      => $meeting_info['room_key'],
                    'meeting_name'  => $meeting_info['meeting_name']
                    ));
                if ($log_fl_ver == "as2") {
                    $this->display( "core/meeting_log/minutes_base.t.html" );
                } else {

                	$custom = $this->config->get('SMARTY_DIR','custom');

                	$custom_favicon_path = sprintf( '/custom/%s/favicon.ico', $custom );
                	if ( file_exists( N2MY_APP_DIR . '/htdocs' . $custom_favicon_path ) )
                		$this->template->assign('custom_favicon_path', $custom_favicon_path);

                	$logo_file_path = sprintf('/custom/%s/lang/common/flash/Logo.swf', $custom);
                	if ( !file_exists( N2MY_APP_DIR . '/htdocs' . $logo_file_path ) )
                		$logo_file_path = sprintf( '/lang/%s/flash/Logo.swf', parent::get_language() );
                	$this->template->assign('logo_file_path', $logo_file_path);

                	$this->display( "core/meeting_log/minutes_base_as3.t.html" );
                }
                break;
        }
    }

    function action_log_display() {
        //assign values
        $log_type = $this->session->get("log_type");
        $this->template->assign('charset', $this->config->get( "GLOBAL", "html_output_char" ) );
        $this->template->assign('locale' , $this->get_language() );

        // service info
        $frame["service_info"] = $this->get_message("SERVICE_INFO");
        $this->template->assign("__frame", $frame);

        //display template
        switch ( $log_type ) {
            case 'log_video':
                // 操作ログ
                $this->add_operation_log('meetinglog_play_video', array(
                    'room_key'      => $this->session->get("log_room_key"),
                    'meeting_name'  => $this->session->get("log_meeting_name")
                    ));
                if ($this->request->get("as_ver") == "as2") {
                    $this->display( "core/meeting_log/video_base.t.html" );
                } else {
                    $this->display( "core/meeting_log/video_base_as3.t.html" );
                }
                break;

            default:
                // 操作ログ
                $this->add_operation_log('meetinglog_play_minutes', array(
                    'room_key'      => $this->session->get("log_room_key"),
                    'meeting_name'  => $this->session->get("log_meeting_name")
                    ));
                $this->display( "core/meeting_log/minutes_base.t.html" );
                break;
        }
    }
}

$main =& new Service_LogPlayer();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
