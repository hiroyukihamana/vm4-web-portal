<?php
require_once ('classes/AppFrame.class.php');
require_once ('lib/EZLib/EZMail/EZSmtp.class.php');
require_once ('lib/EZLib/EZHTML/EZValidator.class.php');
require_once ('lib/EZLib/EZCore/EZConfig.class.php');
class AppInquire extends AppFrame {

    function init() {
        $this -> _name_space = md5(__FILE__);
    }

    function default_view() {
        $this -> action_inquire_userID();
    }

    function action_inquire_userID($message = null) {
        $template = "user/inquire/index.t.html";
        $back_flag = $this -> request -> get('back');
        if ($back_flag || $message) {
            $this -> template -> assign("message", $message);
            $inquire_info = $this -> session -> get('inquire_info');
            $this -> template -> assign('inquire_info', $inquire_info);
        }
        $this -> display($template);
    }

    function action_inquire_userID_confirm() {
        $this -> set_submit_key($this -> _name_space);
        $template = "user/inquire/confirm.t.html";
        $request = $this -> request -> getAll();
        $this -> session -> set('inquire_info', $request);
        $message = $this -> validateRequest($request);
        if ($message) {
            $this -> action_inquire_userID($message);
            exit();
        }
        $this -> template -> assign('inquire_info', $request);
        $this -> display($template);
    }

    function action_inquire_userID_complete() {
        if (!$this -> check_submit_key($this -> _name_space)) {
            $this -> logger -> warn(__FUNCTION__ . "#duplicate", __FILE__, __LINE__);
            header("Location: ?action_inquire_userID&back=1");
            exit ;
        }
        $inquire_info = $this -> session -> get('inquire_info');
        $this -> action_send_mail($inquire_info);
        $configUtil = EZConfig::getInstance(N2MY_CONFIG_FILE);
        $url = $configUtil -> get('SALESFORCE', 'curl_target');
        if ($url) {
            $this -> postLead($inquire_info, $url);
        }
        $this -> session -> remove('inquire_info');
        $template = "user/inquire/complete.t.html";
        $this -> display($template);
    }

    function action_send_mail($inquire_info) {
        $custom = $this -> getCustomDir();
        $frame["lang"] = $this -> _lang;
        if (isset($custom) && !empty($this -> _service_info)) {
            $frame["service_info"] = $this -> get_service_info("SERVICE_INFO");
        } else {
            $frame["service_info"] = $this -> get_message("SERVICE_INFO");
        }
        $mail = new EZSmtp(null, $this -> _lang, "UTF-8");
        $this -> template -> assign("__frame", $frame);
        $this -> template -> assign('inquire_info', $inquire_info);
        $mail -> setTo($inquire_info['email']);
        $mail -> setFrom(USER_ASK_FROM);
        $mail -> setReturnPath(NOREPLY_ADDRESS);
        $mail -> setSubject(ASK_SUBJECT_FOR_USER);
        $mail -> setBody($this -> fetch("common/mail_template/meeting/inquire/userID.t.txt"));
        $mail -> send();
        $mail = new EZSmtp(null, "", "UTF-8");
        switch ($inquire_info['custom']) {
            case 'sales' :
                $mail -> setSubject(REISSUE_USERID_SALES);
                break;
            case 'paperless' :
                $mail -> setSubject(REISSUE_USERID_DOCS);
                break;
            default :
                $mail -> setSubject(REISSUE_USERID_MEETING);
                break;
        }
        $mail -> setTo(USER_ASK_TO);
        $mail -> setFrom(USER_ASK_FROM);
        $mail -> setReturnPath(NOREPLY_ADDRESS);
        $mail -> setBody($this -> fetch("common/mail_template/meeting/inquire/userID_admin.t.txt"));
        $mail -> send();
    }

    function validateRequest($request) {
        $message;
        //氏名検証
        if (!$request['family_name'] || !$request['given_name']) {
            $message .= '<li>' . INQUIRE_UID_NAME_NULL . '</li>';
        } elseif ((mb_strlen($request['family_name']) > 50) || mb_strlen($request['given_name']) > 50 || (mb_strlen($request['family_name']) + mb_strlen($request['given_name'])) > 50) {
            $message .= '<li>' . INQUIRE_UID_NAME_LENGTH . '</li>';
        }
        //氏名カナ検証
        $lang = $this -> session -> get('lang');
        if (!$lang) {
            $lang = $_COOKIE['lang'];
        }
        if ($lang == 'ja') {
            if (!$request['family_name_kana'] || !$request['given_name_kana']) {
                $message .= '<li>' . INQUIRE_UID_NAMEKANA_NULL . '</li>';
            } elseif ((mb_strlen($request['family_name_kana']) > 50) || mb_strlen($request['given_name_kana']) > 50 || (mb_strlen($request['family_name_kana']) + mb_strlen($request['given_name_kana'])) > 50) {
                $message .= '<li>' . INQUIRE_UID_NAMEKANA_LENGTH . '</li>';
            }
        }
        //メール検証
        if (!$request['email']) {
            $message .= '<li>' . INQUIRE_UID_EMAIL_NULL . '</li>';
        } elseif (mb_strlen($request['email']) > 255) {
            $message .= '<li>' . INQUIRE_UID_EMAIL_INVALID . '</li>';
        } elseif (!EZValidator::valid_email($request['email'])) {
            $message .= '<li>' . INQUIRE_UID_EMAIL_INVALID . '</li>';
        }
        if (!$request['email_confirm']) {
            $message .= '<li>' . INQUIRE_UID_EMAIL_CONFIRM_NULL . '</li>';
        } elseif (mb_strlen($request['email_confirm']) > 255) {
            $message .= '<li>' . INQUIRE_UID_EMAIL_CONFIRM_INVALID . '</li>';
        } elseif (!EZValidator::valid_email($request['email_confirm'])) {
            $message .= '<li>' . INQUIRE_UID_EMAIL_CONFIRM_INVALID . '</li>';
        }
        if (strcmp($request['email'], $request['email_confirm']) !== 0) {
            $message .= '<li>' . INQUIRE_UID_EMAIL_INCONSISTENT . '</li>';
        }
        //電話番号検証
        if (!$request['phone']) {
            $message .= '<li>' . PW_RESET_NULL_PHONE . '</li>';
        } elseif (mb_strlen($request['phone']) > 50) {
            $message .= '<li>' . PW_RESET_ERROR_PHONE . '</li>';
        }
        //会社名検証
        if (!$request['company']) {
            $message .= '<li>' . INQUIRE_UID_COMPANY_NULL . '</li>';
        } elseif (mb_strlen($request['company']) > 255) {
            $message .= '<li>' . INQUIRE_UID_COMPANY_LENGTH . '</li>';
        }
        //部署名検証
        if (!$request['division']) {
            $message .= '<li>' . INQUIRE_UID_DIVISION_NULL . '</li>';
        } elseif (mb_strlen($request['division']) > 255) {
            $message .= '<li>' . INQUIRE_UID_DIVISION_LENGTH . '</li>';
        }
        //スタッフ検証
        if (mb_strlen($request['staff']) > 255) {
            $message .= '<li>' . INQUIRE_UID_STAFF_LENGTH . '</li>';
        }
        return $message;
    }

    /**
     * Salesforceまで送信する
     */
    function postLead($inquire_info, $url) {
        $ch = curl_init();
        require_once ('lib/EZLib/EZUtil/EZDate.class.php');
        $time = date('Y/m/d H:i:s', EZDate::getLocateTime(time(), N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE));
        $company = $inquire_info['company'];
        switch ($inquire_info['custom']) {
            case 'sales' :
                $subject = REISSUE_USERID_SALES;
                break;
            case 'paperless' :
                $subject = REISSUE_USERID_DOCS;
                break;
            case 'meeting5lite' :
                $subject = REISSUE_USERID_MEETING_V5LITE;
                break;
            default :
                $subject = REISSUE_USERID_MEETING;
                break;
        }
        $remarks = $this -> fetchRemarks($inquire_info);
        $params = sprintf("subject=%s&", $subject) . 
                  sprintf("00N10000002D5bI=%s&", $time) . 
                  sprintf("company=%s&", urlencode($company)) . 
                  sprintf("00N10000002DWsN=%s&", urlencode($remarks)) . 
                  sprintf("00N10000001cqTo=%s&", '1') . 
                  sprintf("orgid=%s", "00D10000000IYt2");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, mb_convert_encoding($params, 'utf-8', 'auto'));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($ch);
        if (!$result) {
            $this -> logger2 -> warn($inquire_info);
            curl_close($ch);
            return false;
        }
        curl_close($ch);
        return true;
    }

    /**
     * 担当者備考欄の編集について.
     *
     */
    function fetchRemarks($request) {
        $inquire_info = array();
        if ($request['company']) {
            $inquire_info['company'] = $request['company'];
        }
        if ($request['division']) {
            $inquire_info['division'] = $request['division'];
        }
        if ($request['family_name']) {
            $inquire_info['family_name'] = $request['family_name'];
        }
        if ($request['given_name']) {
            $inquire_info['given_name'] = $request['given_name'];
        }
        if ($request['family_name_kana']) {
            $inquire_info['family_name_kana'] = $request['family_name_kana'];
        }
        if ($request['given_name_kana']) {
            $inquire_info['given_name_kana'] = $request['given_name_kana'];
        }
        if ($request['email']) {
            $inquire_info['email'] = $request['email'];
        }
        if ($request['email_confirm']) {
            $inquire_info['email_confirm'] = $request['email_confirm'];
        }
        if ($request['phone']) {
            $inquire_info['phone'] = $request['phone'];
        }
        if ($request['staff']) {
            $inquire_info['staff'] = $request['staff'];
        }
        $template = 'common/user/inquire/salesforce.t.txt';
        $this -> template -> assign('inquire_info', $inquire_info);
        return $this -> fetch($template);
    }

}

$main = new AppInquire();
$main -> execute();
?>
