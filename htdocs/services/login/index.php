<?php
require_once ('classes/AppFrame.class.php');
require_once ('classes/N2MY_Auth.class.php');
require_once ('classes/mgm/MGM_Auth.class.php');
require_once ('classes/vcubeid/vcubeIdCore.class.php');

class AppLogin extends AppFrame {

    private $obj_Auth = null;
    var $obj_vcubeId = null;
    var $serverName = null;
    var $consumerKey = null;
    var $wsseId = null;
    var $wssePw = null;
    var $domain = null;
    var $isSSL = null;
    var $isSha1 = null;

    function init() {
        $this->obj_Auth = new N2MY_Auth($this->get_dsn());
        $serverName = $_SERVER["SERVER_NAME"];
        if ($serverName == $this->config->get('VCUBE_ONE','meeting_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
        } else if($serverName == $this->config->get('VCUBE_ONE','document_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_document_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_doc_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_doc_wssePw');
        } else if($serverName == $this->config->get('VCUBE_ONE','sales_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_sales_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_sls_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_sls_wssePw');
        }else{
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
        }
        $this->domain = $this->config->get('VCUBE_ONE','vcubeid_domain');
        $this->isSSL = $this->config->get('VCUBE_ONE','vcube_isSSL') ? $this->config->get('VCUBE_ONE','isSSL') : false;
        $this->isSha1 = $this->config->get('VCUBE_ONE','vcube_isSha1') ? $this->config->get('VCUBE_ONE','isSha1') : false;
        $this->obj_vcubeId = new vcubeIdCore($this->domain, $this->wsseId, $this->wssePw, $this->isSSL, $this->isSha1);
    }

    function action_showTop($message = "") {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // 言語設定
        $user_id    = $this->request->get("user_id");
        $country       = $this->request->get("country", $this->request->getCookie("country"));
        $selected_country_key = $this->request->getCookie("selected_country_key");
//        $time_zone = $this->request->getCookie("time_zone") ? $this->request->getCookie("time_zone") : N2MY_USER_TIMEZONE;
        $time_zone     = $this->request->get("time_zone", $this->request->getCookie("time_zone") ? $this->request->getCookie("time_zone") : "");
        // タイムゾーンのセキュリティ強化
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
        }
        $user_station     = $this->request->get("user_station", isset($_COOKIE["personal_station"]) ? $_COOKIE["personal_station"] : "");
        $remote_addr   =  $_SERVER["REMOTE_ADDR"];
        $cookie_remote_addr   = $this->request->getCookie("remote_addr");
        if ($cookie_remote_addr != $remote_addr) {
            $selected_country_key = "auto";
        }
        // 地域コード取得
        $country_list = $this->get_country_list();
        // エラーメッセージ
        $this->template->assign("message", $message);
        // 設定
        $this->template->assign("country", $country);
        $this->template->assign("selected_country_key", $selected_country_key);
        $this->template->assign("time_zone", $time_zone);
        $this->template->assign("user_id", $user_id);
        $this->template->assign("user_station", $user_station);
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
        // はちまき描画しない
        $this->template->assign('is_not_display_portal_header', true);

        $this->display('user/login/index.t.html');
    }

    function action_showONELoginTop($message = ""){
        $vcube_one_members = $this->session->get('vcube_one_members');
        if(!$vcube_one_members){
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
            return $this->action_showTop($message);
        }
        // はちまき描画しない
        $this->template->assign('is_not_display_portal_header', true);
        $this->template->assign('vcube_one_members', $vcube_one_members);
        $this->display('user/login/vcubeid_select.t.html');
    }


    /**
     * ログイン処理
     */
    function action_showLogin() {
    	$this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->session->removeAll();
        session_regenerate_id(true);
        $request = $this->request->getAll();
        // パラメタ
        $user_id      = $this->request->get("user_id");
        $user_pw      = $this->request->get("user_password");
        $lang         = $this->request->get("lang");
        $time_zone    = htmlspecialchars($this->request->get("time_zone"));
        $_country_key = $this->request->get("country","auto");
        //サーバー取得
        $obj_MGMClass = new MGM_AuthClass($this->get_auth_dsn() );
        $user_info = $obj_MGMClass->getUserInfoById($user_id);
        if ($user_info){
            if (!$server_info = $obj_MGMClass->getServerInfo($user_info["server_key"])){
                return $this->action_server_down();
            } else {
                $this->session->set( "server_info", $server_info );
            }
        }

        if(!$server_info){
            $obj_server = new N2MY_DB($this->get_auth_dsn(), 'db_server');
            $where = sprintf("server_status = 1 AND host_name = '%s'" , $this->get_dsn_key());
            $server_info = $obj_server->getRow($where);
            if(!$server_info){
                return $this->action_server_down();
            }
            $this->session->set( "server_info", $server_info );
        }

        // ログインチェック
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $obj_Auth = new N2MY_Auth($this->get_dsn());
        $login_info = $obj_Auth -> check($user_id, $user_pw);
        require_once ("classes/dbi/member.dbi.php");
        $obj_member = new MemberTable($this->get_dsn());
        $_member_info = $obj_member->getRow("vcube_one_member_id = '" . mysql_real_escape_string($user_id) . "'");
        if($login_info){
            if($login_info['member_info'] && $_member_info){
                $res = $this -> validateVcubeId($user_id, $user_pw);
                $this->logger2->info($res);
                if($res){
                    $_member = array(
                        'user_id'      => $login_info['user_info']['user_id'],
                        'member_id'    => $login_info['member_info']['member_id'],
                        'company_name' => $login_info['user_info']['user_company_name'],
                        'is_portal_header'   => false,
                    );
                    $res['contracts'][] = $_member;
                    $this -> session -> set('vIdAuthToken', $res['vIdAuthToken']);
                    $this -> session -> set('vcube_one_members', $res['contracts']);
                    $this -> session -> set('lang', $lang);
                    $this -> session -> set('time_zone', $time_zone);
                    $this -> session -> set('country_key', $_country_key);
                    $one_url = $this -> get_redirect_url("services/login/index.php?action_showONELoginTop");
                    header("Location: " . $one_url);
                    exit;
                }
            }
        }else{
            if($_member_info){
                $res = $this -> validateVcubeId($user_id, $user_pw);
                $this->logger2->info($res);
                if($res){
                    if(count($res['contracts']) > 1){
                        $this -> session -> set('vIdAuthToken', $res['vIdAuthToken']);
                    	$this -> session -> set('vcube_one_members', $res['contracts']);
                        $this -> session -> set('lang', $lang);
                        $this -> session -> set('time_zone', $time_zone);
                        $this -> session -> set('country_key', $_country_key);
                        $one_url = $this -> get_redirect_url("services/login/index.php?action_showONELoginTop");
                        header("Location: " . $one_url);
                        exit;
                    }else{
                        $member_id  = $res['contracts'][0]["member_id"];
                        $role       = $res['contracts'][0]["role"];
                        if($res['contracts'][0]['is_portal_header']){
                        	$vIdAuthToken = $res['vIdAuthToken'];
                        	$contract_id = $res['contracts'][0]['contract_id'];
                        }
                        $login_info = $obj_Auth -> checkVcubeId($member_id);
                    }
                }
            }
        }
        if($login_info){
            if ($login_info["member_info"]["member_type"] == 'centre' || $login_info["member_info"]["member_type"] == 'terminal') {
                $this->session->remove('server_info');
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
                return $this->action_showTop($message);
            }
            require_once('classes/mgm/dbi/custom_domain_service.dbi.php');
            $obj_CustomDomainService      = new CustomDomainServiceTable( N2MY_MDB_DSN );
            $biz_info = $obj_CustomDomainService->getRow("service_name = 'biz' AND status = 1");
            $web_info = parse_url(N2MY_BASE_URL);
            if ($biz_info["domain"] != $web_info["host"] && $login_info['user_info']['service_name'] == 'biz'){
                $this->session->remove('server_info');
                $this->logger2->warn(array($biz_info["domain"], $web_info["host"]));
                $message .= "<li>".LOGIN_ERROR_BIZ . "</li>";
                return $this->action_showTop($message);
            }
            $this->session->set('login', '1');
            if ($custom = $this->config->get('SMARTY_DIR','custom')) {
                $login_info["user_info"]["custom"] = $custom;
            }
            //セールスフラグをチェック
            $this->session->set('service_mode', 'meeting');
            if($login_info["user_info"]["use_sales"]) {
                if ($login_info["member_info"]["use_sales"] == 1 && $login_info["user_info"]["account_model"] == "member") {
                    $meeting_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "meeting");
                    $sales_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "sales");
                } else if($login_info["member_info"]["use_sales"] == 1 && $login_info["user_info"]["account_model"] != "member") {
                    $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                    $sales_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "sales");
                } else if($login_info["member_info"]["use_sales"] == 0 && $login_info["member_info"] && $login_info["user_info"]["account_model"] != "member") {
                    $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                } else {
                    $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                    $sales_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "sales");
                }
                //モードの設定
                if (!$meeting_room_count) {
                    $this->session->set("service_mode","sales");
                } elseif(!$sales_room_count) {
                    $this->logger2->info($login_info["user_info"]["custom"]);
                    if ($login_info["user_info"]["custom"] == "sales") {
                        $login_info["user_info"]["custom"] = "";
                    }
                } else {
                    if ($login_info["user_info"]["custom"] == "sales") {
                        $this->session->set("service_mode","sales");
                    }
                    $this->session->set("use_mode_change","1");
                }
                $this->session->set("meeting_room_count", $meeting_room_count);
                $this->session->set("sales_room_count", $sales_room_count);
            } elseif ($login_info["user_info"]["custom"] == "sales") {
                $login_info["user_info"]["custom"] = "";
            }
            $login_info["user_info"]["login_url"] = N2MY_BASE_URL;
            $this->session->set('user_info', $login_info["user_info"]);
            $this->session->set('member_info', $login_info["member_info"]);
        }else{
            $this->session->remove('server_info');
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
            return $this->action_showTop($message);
        }

        /**
         * 接続可能なipか確認
         * とりあえずはオプション扱いではなく
         * 自分のkeyとひもづくiplistがあれば契約しているとみなし
         * 接続元のipと照らし合わせる
         */
         if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
            $this->session->removeAll();
            $message["title"] = $this->get_message("WHITELIST", "title");
            $message["body"] = $this->get_message("WHITELIST", "body");
            $this->template->assign('message', $message);
            $this->display('common.t.html');
            exit;
         }

        //MaxMindでIPから所在地を検索
        $remote_addr =  $_SERVER["REMOTE_ADDR"];
        $result_country_key = $this->_get_country_key($_country_key);
        if (!$result_country_key) {
            // 地域コードの不正な指定を防ぐ
            $country_list = $this->get_country_list();
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
                $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    $this->session->set('country_key', $country_row["country_key"]);
                    $result_country_key = $_country_key;
                }
            }
        }else {
            $this->session->set('country_key', $result_country_key);
        }
        $this->session->set('selected_country_key', $_country_key);
        // 利用言語設定 指定した言語が利用できない場合は優先言語を指定する
        if($login_info["user_info"]["lang_allow"]){
            $user_lang_list = split("," , $login_info["user_info"]["lang_allow"]);
            if(!in_array($lang , $user_lang_list)){
                $lang = $user_lang_list[0];
            }
        }
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$time_zone);
        }
        // クッキー
        $limit = time() + 365 * 24 * 3600;
        setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("country", $result_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("selected_country_key", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("remote_addr", $remote_addr,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        if ($request["user_station"]) {
            setcookie("personal_station", $request["user_station"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        } else {
            setcookie("personal_station", "",  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }
        //memberでログインの際は名前とメールをを強制的に上書き
        if( $login_info["member_info"]["member_name"] ){
            setcookie("personal_name", $login_info["member_info"]["member_name"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
//            setcookie("personal_email", $login_info["member_info"]["member_email"], $limit, N2MY_COOKIE_PATH);
        }

        // ポータル連携用情報保持
        parent::setVidInfo( $login_info['member_info']['vcube_one_member_id'], $vIdAuthToken, $contract_id, $lang, $time_zone);

        // セッション
        $this->session->set("lang", $lang);
        $this->session->set('time_zone', $time_zone);
        if($role){
            $this->session->set('role', $role);
        }
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());

        //メンバー課金利用の場合ユーザーキーからプラン及びオプションをもってくる
        if( $login_info["user_info"]["account_model"] == "member" || $login_info["user_info"]["account_model"] == "centre" ){
            require_once("classes/N2MY_IndividualAccount.class.php");
            $objIndividualAccount = new N2MY_IndividualAccount( $this->get_dsn(), $this->get_auth_dsn() );
            $login_info["user_info"]["user_key"];
            $planInfo = $objIndividualAccount->getPlan( $login_info["user_info"]["user_key"] );
            $this->session->set( 'plan_info', $planInfo );
        }
        //　メインメニューに遷移
        $this->logger2->info($user_id.",".$_country_key.",".$result_country_key, "login");
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, array(
            "server_info" => $server_info,
            "login_info" => $login_info));
        $url = $this->get_redirect_url("services/index.php");
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__,$url);
        if ($login_info["user_info"]["intra_fms"]){
            require_once("classes/dbi/user_fms_server.dbi.php");
            $objFmsServer = new UserFmsServerTable($this->get_dsn());
            //複数契約は後ほど
            $serverInfo = $objFmsServer->getRow(sprintf("user_key='%s'", $login_info["user_info"]["user_key"]));
            $this->template->assign("serverInfo", $serverInfo);
            $url = N2MY_BASE_URL."services/login/index.php";
            $this->template->assign("url", $url);
            $this->template->assign("actionName", "action_distinction");
            $this->display("user/login/check_fms.t.html");
        } else {
            if (!$this->session->get("meeting_room_count") || !$this->session->get("sales_room_count")) {
                $this->session->remove("use_mode_change");
            }
//            //member かつ　ネオジャパンサービスの人は自分の部屋をもってくる
//            if ( $login_info["member_info"] && ($login_info["user_info"]["account_model"] == "member" || $login_info["user_info"]["account_model"] == "centre")) {
//                $rooms = $obj_N2MY_Account->getOwnRoom( $login_info["member_info"]["room_key"], $login_info["user_info"]["user_key"] );
//            } else {
//                $rooms = $obj_N2MY_Account->getRoomList( $login_info["user_info"]["user_key"] );
//            }
//            $this->session->set('room_info', $rooms);
            header("Location: ".$url);
        }
        //操作ログ追加
        $this->add_operation_log("login");
        exit;
    }

	/**
	 * VCUBE ONE MEMBER ログイン処理
	 */
    function action_showONELogin(){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $lang         = $this->session->get("lang");
        $time_zone    = $this->session->get("time_zone");
        $_country_key = $this->session->get("country_key");
        $vcube_one_members = $this->session->get('vcube_one_members');
        $index   = $this->request->get("user") - 1;
        $user_id = $vcube_one_members[$index]['member_id'];
        $role    = $vcube_one_members[$index]['role'];
        $is_portal_header = $vcube_one_members[$index]['is_portal_header'];
        if($is_portal_header){
        	$vIdAuthToken = $this->session->get('vIdAuthToken');
        	$contract_id = $vcube_one_members[$index]['contract_id'];
        }
        $backUrl = $this->session->get("backUrl");
        $vid_login = $this->session->get("vcubeid_login");
        $this->session->removeAll();
        session_regenerate_id(true);
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if ( ! $user_info = $obj_MGMClass->getUserInfoById( $user_id ) ){
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
            if($backUrl){
                header("Location:".$backUrl);
                exit;
            }
            return $this->action_showTop($message);
        }
        if ( ! $server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] ) ){
            return $this->action_server_down();
        } else {
            $this->session->set( "server_info", $server_info );
        }
        // ログインチェック
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $obj_Auth = new N2MY_Auth( $this->get_dsn() );
        if ($login_info = $obj_Auth->checkVcubeId($user_id)) {
            if ($login_info["member_info"]["member_type"] == 'centre' || $login_info["member_info"]["member_type"] == 'terminal') {
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
                if($backUrl){
                    header("Location:".$backUrl);
                    exit;
                }
                return $this->action_showTop($message);
            }
            if ($login_info['user_info']['service_name'] == 'biz'){
                $message .= "<li>".LOGIN_ERROR_BIZ . "</li>";
                if($backUrl){
                    header("Location:".$backUrl);
                    exit;
                }
                return $this->action_showTop($message);
            }
            $this->session->set('login', '1');
            if($vid_login){
                $this->session->set('vcubeid_login', '1');
            }
            if ($custom = $this->config->get('SMARTY_DIR','custom')) {
                $login_info["user_info"]["custom"] = $custom;
            }
            //セールスフラグをチェック
            $this->session->set('service_mode', 'meeting');
            if($login_info["user_info"]["use_sales"]) {
                if ($login_info["member_info"]["use_sales"] == 1 && $login_info["user_info"]["account_model"] == "member") {
                    $meeting_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "meeting");
                    $sales_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "sales");
                } else if($login_info["member_info"]["use_sales"] == 1 && $login_info["user_info"]["account_model"] != "member") {
                    $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                    $sales_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "sales");
                } else if($login_info["member_info"]["use_sales"] == 0 && $login_info["member_info"] && $login_info["user_info"]["account_model"] != "member") {
                    $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                } else {
                    $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                    $sales_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "sales");
                }
                //モードの設定
                if (!$meeting_room_count) {
                    $this->session->set("service_mode","sales");
                } elseif(!$sales_room_count) {
                    $this->session->set("service_mode","meeting");
                    $this->logger2->info($login_info["user_info"]["custom"]);
                    if ($login_info["user_info"]["custom"] == "sales") {
                    	$login_info["user_info"]["custom"] = "";
                    }
                } else {
                    if ($login_info["user_info"]["custom"] == "sales") {
                        $this->session->set("service_mode","sales");
                    } else {
                        $this->session->set("service_mode","meeting");
                    }
                    $this->session->set("use_mode_change","1");
                }
                $this->session->set("meeting_room_count", $meeting_room_count);
                $this->session->set("sales_room_count", $sales_room_count);
            } elseif ($login_info["user_info"]["custom"] == "sales") {
                $login_info["user_info"]["custom"] = "";
            }
            $login_info["user_info"]["login_url"] = N2MY_BASE_URL;
            $this->session->set('user_info', $login_info["user_info"]);
            $this->session->set('member_info', $login_info["member_info"]);
        } else {
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
            return $this->action_showTop($message);
        }
        /**
         * 接続可能なipか確認
         * とりあえずはオプション扱いではなく
         * 自分のkeyとひもづくiplistがあれば契約しているとみなし
         * 接続元のipと照らし合わせる
         */
         if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
             $this->session->removeAll();
            $message["title"] = $this->get_message("WHITELIST", "title");
            $message["body"] = $this->get_message("WHITELIST", "body");
            $this->template->assign('message', $message);
            $this->display('common.t.html');
            exit;
         }
        //MaxMindでIPから所在地を検索
        $remote_addr =  $_SERVER["REMOTE_ADDR"];
        $result_country_key = $this->_get_country_key($_country_key);
        if (!$result_country_key) {
            // 地域コードの不正な指定を防ぐ
            $country_list = $this->get_country_list();
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
                $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    $this->session->set('country_key', $country_row["country_key"]);
                    $result_country_key = $_country_key;
                }
            }
        }else {
            $this->session->set('country_key', $result_country_key);
        }
        $this->session->set('selected_country_key', $_country_key);
        // 利用言語設定 指定した言語が利用できない場合は優先言語を指定する
        if($login_info["user_info"]["lang_allow"]){
            $user_lang_list = split("," , $login_info["user_info"]["lang_allow"]);
            if(!in_array($lang , $user_lang_list)){
                $lang = $user_lang_list[0];
            }
        }
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$time_zone);
        }
        // クッキー
        $limit = time() + 365 * 24 * 3600;
        setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("country", $result_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("selected_country_key", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("remote_addr", $remote_addr,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        if ($request["user_station"]) {
            setcookie("personal_station", $request["user_station"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        } else {
            setcookie("personal_station", "",  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }
        //memberでログインの際は名前とメールをを強制的に上書き
        if( $login_info["member_info"]["member_name"] ){
            setcookie("personal_name", $login_info["member_info"]["member_name"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }

        // ポータル連携用情報保持
        parent::setVidInfo( $login_info["member_info"]["vcube_one_member_id"], $vIdAuthToken, $contract_id, $lang, $time_zone);

        // セッション
        if($role){
        	$this->session->set('role', $role);
        }
        $this->session->set("lang", $lang);
        $this->session->set('time_zone', $time_zone);
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());

        //メンバー課金利用の場合ユーザーキーからプラン及びオプションをもってくる
        if( $login_info["user_info"]["account_model"] == "member" || $login_info["user_info"]["account_model"] == "centre" ){
            require_once("classes/N2MY_IndividualAccount.class.php");
            $objIndividualAccount = new N2MY_IndividualAccount( $this->get_dsn(), $this->get_auth_dsn() );
            $login_info["user_info"]["user_key"];
            $planInfo = $objIndividualAccount->getPlan( $login_info["user_info"]["user_key"] );
            $this->session->set( 'plan_info', $planInfo );
        }
        //　メインメニューに遷移
        $this->logger2->info($user_id.",".$_country_key.",".$result_country_key, "login");
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, array(
            "server_info" => $server_info,
            "login_info" => $login_info));
        $url = $this->get_redirect_url("services/index.php");
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__,$url);
        if ($login_info["user_info"]["intra_fms"]){
            require_once("classes/dbi/user_fms_server.dbi.php");
            $objFmsServer = new UserFmsServerTable($this->get_dsn());
            //複数契約は後ほど
            $serverInfo = $objFmsServer->getRow(sprintf("user_key='%s'", $login_info["user_info"]["user_key"]));
            $this->template->assign("serverInfo", $serverInfo);
            $url = N2MY_BASE_URL."services/login/index.php";
            $this->template->assign("url", $url);
            $this->template->assign("actionName", "action_distinction");
            $this->display("user/login/check_fms.t.html");
        } else {
            if (!$this->session->get("meeting_room_count") || !$this->session->get("sales_room_count")) {
                $this->session->remove("use_mode_change");
            }
            header("Location: ".$url);
        }
        //操作ログ追加
        $this->add_operation_log("login");
        exit;
    }

    /**
     * intraFMS契約ユーザーのクライアント、intraFMS間のネットワークチェック結果
     */
    public function action_distinction()
    {
        $result = "true" == $this->request->get("result") ? 1 : 0;    //    trueなら1をセット
        $this->logger2->info($result, "intracheck");
        $this->session->set("avairable_intra_fms", $result);

        $userInfo = $this->session->get('user_info');
        $member_info = $this->session->get('member_info');

        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $roomList = ( $member_info && ($userInfo["account_model"] == "member" || $userInfo["account_model"] == "centre")) ?
            $obj_N2MY_Account->getOwnRoom($member_info["member_key"], $userInfo["user_key"] ):
            $obj_N2MY_Account->getRoomList($userInfo["user_key"], $result);
        $rooms = array();
        foreach($roomList as $roomKey => $roomInfo){
            //表示可能な部屋のみ抽出
            if (1 != $roomInfo["displayRoom"]) $rooms[$roomKey] = $roomInfo;
        }

        $this->session->set('room_info', $rooms);
        header("Location: /services/index.php");
        exit;
    }

    function default_view() {
        $this->logger->trace(__FUNCTION__ . "#start", __FILE__, __LINE__);
        // 自動ログイン機能
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$_COOKIE);
        if ($this->config->get("N2MY", "auto_login") == 1) {
            $this->request->set("user_id", $this->config->get("N2MY", "login_user"));
            $this->request->set("user_password", $this->config->get("N2MY", "login_password"));
            // 言語
            $lang = $this->request->get("lang", $this->request->getCookie("lang", substr($this->get_language(), 0, 2)));
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$lang);
            if (!$lang) {
                // デフォルト指定
                $lang = substr($this->_lang, 0, 2);
            }
            $this->request->set("lang", $lang);
            // 地域
            $country = $this->request->get("country", $this->request->getCookie("country"));
            if (!$country) {
                // デフォルト指定
                $country_list = $this->get_country_list();
                $country_keys = array_keys($country_list);
                $lang = $country_keys[0];
            }
            $this->request->set("country", $country);
            // タイムゾーン
            $time_zone = $this->request->get("time_zone", $this->request->getCookie("time_zone"));
            if (!$time_zone) {
                // デフォルト指定
                $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
            }
            // タイムゾーンのセキュリティ強化
            $timezone_list = $this->get_timezone_list();
            if (!array_key_exists($time_zone, $timezone_list)) {
                $timezone_keys = array_keys($timezone_list);
                $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
            }
            $this->request->set("time_zone", $time_zone);
            $this->action_showLogin();
        } else {
            $this->action_showTop();
        }
        $this->logger->trace(__FUNCTION__ . "#end", __FILE__, __LINE__);
    }

    private function action_server_down()
    {
        $this->render_fault();
    }


    private function render_fault()
    {
        echo "server down";
    }

    function action_set_env() {
        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;
        // 地域コード
        $_country_key = $this->request->get("country_key");
        $request = $this->request->getAll();
        if ($_country_key) {
            $country_list = $this->get_country_list();
            // 改ざんされた場合の対応
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
                $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    setcookie("country", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
                }
            }
        }
        // 言語コード
        $lang = $this->request->get("lang");
        if ($lang) {
            setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $this->session->set("lang", $lang);
        }
        // タイムゾーン
        $time_zone = $this->request->get("time_zone");
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$time_zone);
        }
        if (is_numeric($time_zone)) {
            setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }
        header("Location: index.php");
    }

    function getToken($id , $pw){
        $vcubeid = $this->obj_vcubeId;
        $token = $vcubeid->getToken($this->consumerKey, $id, sha1($pw));
        return $token;
    }

    function getMemberId($vIdAuthToken , $vcubeId){
        $vcubeid = $this -> obj_vcubeId;
        // ステータス確認
        $response_id = $vcubeid -> ssologin($this -> consumerKey, $vIdAuthToken, $vcubeId);
        $contracts = $response_id['contracts']['contract'];
        if(!array_key_exists(0, $contracts)){
            $tmp = $contracts;
            unset($contracts);
            $contracts[] = $tmp;
        }
        $_contracts = array();
        foreach ($contracts as $contract){
            $xml = (array)simplexml_load_string($contract['settintXml']);
            $xml_role = (array)$xml['role'];
            $_contracts[] = array(
                'user_id'      => $contract['meeting']['userId'],
                'member_id'    => $contract['meeting']['memberId'],
                'company_name' => $contract['name'],
                'role'         => $xml_role['permission'],
            	'is_portal_header' => ( $contract['one']['header'] === "true" ),
            	'contract_id' => $contract['id'],
            );
        }
        return array(
            'result'    => $response_id['result'],
            'contracts' => $_contracts,
        );
    }

    /**
     * V-cube ID認証
     */
    function validateVcubeId($user_id, $user_pw){
        if(!$user_id || !$user_pw){
            return false;
        }
        $token = $this -> getToken($user_id, $user_pw);
        $this->logger2->info($token);
        if(!$token['result']){
        return false;
    }
        $res = $this -> getMemberId($token["vIdAuthToken"] , $user_id);
        if(!$res['result']){
            return false;
        }
        $res['vIdAuthToken'] = $token['vIdAuthToken'];
        return $res;
    }
}

$main = new AppLogin();
$main->execute();
?>
