<?php
require_once ('classes/AppFrame.class.php');
require_once ('classes/N2MY_Auth.class.php');
require_once ('classes/mgm/MGM_Auth.class.php');
require_once ('classes/vcubeid/vcubeIdCore.class.php');

class AppLoginVcubeID extends AppFrame {

    private $obj_Auth = null;
    var $obj_vcubeId = null;
    var $serverName = null;
    var $consumerKey = null;
    var $wsseId = null;
    var $wssePw = null;
    var $domain = null;
    var $isSSL = null;
    var $isSha1 = null;

    function init() {

        $this->obj_Auth = new N2MY_Auth($this->get_dsn());
        $serverName = $_SERVER["SERVER_NAME"];
        if ($serverName == $this->config->get('VCUBE_ONE','meeting_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
        } else if($serverName == $this->config->get('VCUBE_ONE','document_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_document_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_doc_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_doc_wssePw');
        } else if($serverName == $this->config->get('VCUBE_ONE','sales_server_name')){
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_sales_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_sls_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_sls_wssePw');
        }else{
            $this->consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
            $this->wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
            $this->wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
        }
        $this->domain = $this->config->get('VCUBE_ONE','vcubeid_domain');
        $this->isSSL = $this->config->get('VCUBE_ONE','vcube_isSSL') ? $this->config->get('VCUBE_ONE','isSSL') : false;
        $this->isSha1 = $this->config->get('VCUBE_ONE','vcube_isSha1') ? $this->config->get('VCUBE_ONE','isSha1') : false;
        $this->obj_vcubeId = new vcubeIdCore($this->domain, $this->wsseId, $this->wssePw, $this->isSSL, $this->isSha1);

    }

    function default_view() {
        /*
        $id = "hamana@vcube.co.jp";
        $pw = "hogehoge1";
        $vcubeid = $this->obj_vcubeId;
        $token = $this->getToken($id, sha1($pw));
        var_dump($token);
        $sso = $this->obj_vcubeId->ssologin($this->consumerKey , $token["vIdAuthToken"] , "hamana@vcube.co.jp" );
        var_dump ($sso);
        */
        $this->action_showTop();
    }

    function action_showTop($message = null){
        $user_id    = $this->request->get("user_id");
        $country       = $this->request->get("country", $this->request->getCookie("country"));
        $selected_country_key = $this->request->getCookie("selected_country_key");
        //        $time_zone = $this->request->getCookie("time_zone") ? $this->request->getCookie("time_zone") : N2MY_USER_TIMEZONE;
        $time_zone     = $this->request->get("time_zone", $this->request->getCookie("time_zone") ? $this->request->getCookie("time_zone") : "");
        // タイムゾーンのセキュリティ強化
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
        }
        // 地域コード取得
        $country_list = $this->get_country_list();

        // エラーメッセージ
        $this->template->assign("message", $message);
        // 設定
        $this->template->assign("country", $country);
        $this->template->assign("selected_country_key", $selected_country_key);
        $this->template->assign("time_zone", $time_zone);
        $this->template->assign("user_id", $user_id);
        $this->display("user/login/login_vcubeid.t.html");
    }

    function getToken($id , $pw){
        $vcubeid = $this->obj_vcubeId;
        $token = $vcubeid->getToken($this->consumerKey, $id, sha1($pw));
        $this->logger2->info($token);
        return $token;
    }

    function getMemberId($vIdAuthToken , $vcubeId){

        $vcubeid = $this->obj_vcubeId;
        // ステータス確認
        $response_id = $vcubeid->ssologin($this->consumerKey, $vIdAuthToken, $vcubeId);
        $this->logger2->info($response_id);
        $res["result"] = $response_id["result"];
        $res["user_id"]    = $response_id["contracts"]["contract"]["meeting"]["userId"];
        $res["member_id"]    = $response_id["contracts"]["contract"]["meeting"]["memberId"];
        $xml = (array)simplexml_load_string($response_id["contracts"]["contract"]["settintXml"]);
        $res["role"] = $xml["userrole"];
        return $res;
    }

    function action_login() {
        // 最初のトークン取得
        $request = $this->request->getAll();
        $id = $request["user_id"];
        $pw = $request["user_password"];
        $token = $this->getToken($id, $pw);
        if($token["result"] != true){
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
            return $this->action_showTop($message);
        }

        //  認証行いステータスを取得
        $res = $this->getMemberId($token["vIdAuthToken"] , $token["consumeruserUserId"]);
        if($res["result"] != true){
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
            return $this->action_showTop($message);
        }


        // 戻り値にuser_idとmemberIDがあるはず
        $user_id = $res["user_id"];
        $member_id = $res["member_id"];
        $role = $res["role"];

        $lang         = $this->request->get("lang");
        $time_zone    = htmlspecialchars($this->request->get("time_zone"));
        // メンバーでログイン処理

        $this->session->removeAll();
        session_regenerate_id(true);
        // ログインチェック
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $login_info = $this->obj_Auth->checkVcubeId($member_id);

//        var_dump($login_info);
        if($login_info){
            if ($login_info["member_info"]["member_type"] == 'centre' || $login_info["member_info"]["member_type"] == 'terminal') {
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
                return $this->action_showTop($message);
            }
            if ($login_info['user_info']['service_name'] == 'biz'){
                $message .= "<li>".LOGIN_ERROR_BIZ . "</li>";
                return $this->action_showTop($message);
            }
            $this->session->set('login', '1');
            if ($custom = $this->config->get('SMARTY_DIR','custom')) {
                $login_info["user_info"]["custom"] = $custom;
            }
            //セールスフラグをチェック
            if($login_info["user_info"]["use_sales"]) {
                if ($login_info["member_info"]["use_sales"] == 1 && $login_info["user_info"]["account_model"] == "member") {
                    $meeting_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "meeting");
                    $sales_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "sales");
                } else if($login_info["member_info"]["use_sales"] == 1 && $login_info["user_info"]["account_model"] != "member") {
                    $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                    $sales_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "sales");
                } else if($login_info["member_info"]["use_sales"] == 0 && $login_info["member_info"] && $login_info["user_info"]["account_model"] != "member") {
                    $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                } else {
                    $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                    $sales_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "sales");
                }
                //モードの設定
                if (!$meeting_room_count) {
                    $this->session->set("service_mode","sales");
                } elseif(!$sales_room_count) {
                    $this->session->set("service_mode","meeting");
                    $this->logger2->info($login_info["user_info"]["custom"]);
                    if ($login_info["user_info"]["custom"] == "sales") {
                        $login_info["user_info"]["custom"] = "";
                    }
                } else {
                    if ($login_info["user_info"]["custom"] == "sales") {
                        $this->session->set("service_mode","sales");
                    } else {
                        $this->session->set("service_mode","meeting");
                    }
                    $this->session->set("use_mode_change","1");
                }
                $this->session->set("meeting_room_count", $meeting_room_count);
                $this->session->set("sales_room_count", $sales_room_count);
            } elseif ($login_info["user_info"]["custom"] == "sales") {
                $login_info["user_info"]["custom"] = "";
            }
            $login_info["user_info"]["login_url"] = N2MY_BASE_URL;
            $this->session->set('user_info', $login_info["user_info"]);
            $this->session->set('member_info', $login_info["member_info"]);
        }else {
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_WIDTH . "</li>";
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER_PASSWORD . "</li>";
            return $this->action_showTop($message);
        }
        /**
         * 接続可能なipか確認
         * とりあえずはオプション扱いではなく
         * 自分のkeyとひもづくiplistがあれば契約しているとみなし
         * 接続元のipと照らし合わせる
         */
        if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
            $this->session->removeAll();
            $message["title"] = $this->get_message("WHITELIST", "title");
            $message["body"] = $this->get_message("WHITELIST", "body");
            $this->template->assign('message', $message);
            $this->display('common.t.html');
            exit;
        }

        //MaxMindでIPから所在地を検索
        $remote_addr =  $_SERVER["REMOTE_ADDR"];
        $result_country_key = $this->_get_country_key($_country_key);
        if (!$result_country_key) {
            // 地域コードの不正な指定を防ぐ
            $country_list = $this->get_country_list();
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
                $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    $this->session->set('country_key', $country_row["country_key"]);
                    $result_country_key = $_country_key;
                }
            }
        }else {
            $this->session->set('country_key', $result_country_key);
        }
        $this->session->set('selected_country_key', $_country_key);
        // 利用言語設定 指定した言語が利用できない場合は優先言語を指定する
        if($login_info["user_info"]["lang_allow"]){
            $user_lang_list = split("," , $login_info["user_info"]["lang_allow"]);
            if(!in_array($lang , $user_lang_list)){
                $lang = $user_lang_list[0];
            }
        }
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$time_zone);
        }
        // クッキー
        $limit = time() + 365 * 24 * 3600;
        setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("country", $result_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("selected_country_key", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("remote_addr", $remote_addr,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        if ($request["user_station"]) {
            setcookie("personal_station", $request["user_station"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        } else {
            setcookie("personal_station", "",  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }
        //memberでログインの際は名前とメールをを強制的に上書き
        if( $login_info["member_info"]["member_name"] ){
            setcookie("personal_name", $login_info["member_info"]["member_name"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            //            setcookie("personal_email", $login_info["member_info"]["member_email"], $limit, N2MY_COOKIE_PATH);
        }

        // セッション
        $this->session->set("lang", $lang);
        $this->session->set('time_zone', $time_zone);
        $this->session->set('role', $role);
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());

        //メンバー課金利用の場合ユーザーキーからプラン及びオプションをもってくる
        if( $login_info["user_info"]["account_model"] == "member" || $login_info["user_info"]["account_model"] == "centre" ){
            require_once("classes/N2MY_IndividualAccount.class.php");
            $objIndividualAccount = new N2MY_IndividualAccount( $this->get_dsn(), $this->get_auth_dsn() );
            $login_info["user_info"]["user_key"];
            $planInfo = $objIndividualAccount->getPlan( $login_info["user_info"]["user_key"] );
            $this->session->set( 'plan_info', $planInfo );
        }
        //　メインメニューに遷移
        $this->logger2->info($user_id.",".$_country_key.",".$result_country_key, "login");
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, array(
                "server_info" => $server_info,
                "login_info" => $login_info));
        $url = $this->get_redirect_url("services/index.php");
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__,$url);
        if ($login_info["user_info"]["intra_fms"]){
            require_once("classes/dbi/user_fms_server.dbi.php");
            $objFmsServer = new UserFmsServerTable($this->get_dsn());
            //複数契約は後ほど
            $serverInfo = $objFmsServer->getRow(sprintf("user_key='%s'", $login_info["user_info"]["user_key"]));
            $this->template->assign("serverInfo", $serverInfo);
            $url = N2MY_BASE_URL."services/login/index.php";
            $this->template->assign("url", $url);
            $this->template->assign("actionName", "action_distinction");
            $this->display("user/login/check_fms.t.html");
        } else {
            $rooms = $this->get_room_list();
            if (!$this->session->get("meeting_room_count") || !$this->session->get("sales_room_count")) {
                $this->session->remove("use_mode_change");
            }
            header("Location: ".$url);
        }
        //操作ログ追加
        $this->add_operation_log("login");
        exit;

    }
}

$main = new AppLoginVcubeID();
$main->execute();