<?php
require_once ('classes/AppFrame.class.php');
require_once ('classes/N2MY_Auth.class.php');
require_once ('classes/mgm/MGM_Auth.class.php');
require_once ('classes/mgm/dbi/sastik_account_translation.dbi.php');
require_once ('classes/mgm/dbi/sastik_account_host.dbi.php');
require_once ('lib/sastik/common.php');

class AppLogin extends AppFrame {

    private $obj_Auth = null;

    function init() {
      $res = $this->sastik_auth();
      $this->logger2->debug($res);

      //成功時の処理ここから
      $this->action_login($res["memberId"]);
      exit;
    }

    //Header情報称号
    function sastik_auth(){
        $headers = getallheaders();
        if ($headers["X-SASTIK-ACCOUNT"] && $headers["X-SASTIK-ASID"]
            && $headers["X-SASTIK-OBJECT-ID"]  && $headers["X-SASTIK-OSID"]) {

            if($headers["Referer"]) {
                $account_translation_table = new SastikAccountTranslationTable($this->get_auth_dsn());
                $account_host_table = new SastikAccountHostTable($this->get_auth_dsn());

                $member_id = false;

                $where = "account_id = '" . addslashes($headers["X-SASTIK-ACCOUNT"]) . "'";
                $where .= ' AND is_deleted = 0';
                $rows = $account_translation_table->getRowsAssoc($where);

            	foreach($rows as $r) {
              	    $account_id = $r['account_id'];

               	    $where = "account_host_key = '" . addslashes($r['account_host_key']) . "'";
               	    $where .=  " AND account_host = '" . addslashes($headers["Referer"]) . "'";
               	    $row = $account_host_table->getRowsAssoc($where);

                    if(count($row) > 0) {
                        $user_id = $row[0]['user_id'];
                        $member_id = generateMemberID($user_id, $account_id);
                        break;
                    }
               	}

                if($member_id === false) {
                    $this->logger2->warn($headers, "can't find member ID");
                    $this->_error();
                    exit;
                }

                $result = array("memberId" => $member_id);
            }
            else {
                $this->logger2->warn($headers, "referer error");
                $this->_error();
                exit;
            }
        } else {
            $this->logger2->warn($headers, "login header error");
            $this->_error();
            exit;
        }
        return $result;
    }

    /**
     * ログイン処理
     */
    private function action_login($member_id) {
        $this->session->removeAll();
        session_regenerate_id(true);
        // パラメタ
        $lang         = $this->request->get("lang", "ja");
        $time_zone    = htmlspecialchars($this->request->get("time_zone", "9"));
        $_country_key = $this->request->get("country", "jp");
        //サーバー取得
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if ( ! $user_info = $obj_MGMClass->getUserInfoById( $member_id ) ){
            $this->logger2->warn($member_id,"get user info");
            //エラーページへ遷移
            $this->_error();
            exit;
        }
        if ( ! $server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] ) ){
           $this->logger2->warn($member_id, "error get server info");
           //エラーページへ遷移
           $this->_error();
           exit;
        } else {
            $this->session->set( "server_info", $server_info );
        }

        // ログインチェック
        $obj_Auth = new N2MY_Auth( $this->get_dsn() );
        if ($login_info = $obj_Auth->checkVcubeId($member_id)) {
            $this->session->set('login', '1');
            if ($custom = $this->config->get('SMARTY_DIR','custom')) {
                $login_info["user_info"]["custom"] = $custom;
            }
            $login_info["user_info"]["login_url"] = N2MY_BASE_URL;
            $this->session->set('user_info', $login_info["user_info"]);
            $this->session->set('member_info', $login_info["member_info"]);
        } else {
            $this->logger2->info($member_id, "login error");
            //エラーページへ遷移
            $this->_error();
            exit;
        }

        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        /**
         * 接続可能なipか確認
         * とりあえずはオプション扱いではなく
         * 自分のkeyとひもづくiplistがあれば契約しているとみなし
         * 接続元のipと照らし合わせる
         */
         if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
            $this->session->removeAll();
            //エラーページへ遷移
            $this->_error();
            exit;
         }

        // 地域コードの不正な指定を防ぐ
        $country_list = $this->get_country_list();
        if (!array_key_exists($_country_key, $country_list)) {
            $country_keys = array_keys($country_list);
            $_country_key = $country_keys[0];
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
        }
        foreach ($country_list as $country_key => $country_row) {
            if ($_country_key == $country_key) {
                $this->session->set('country_key', $country_row["country_key"]);
            }
        }
        // クッキー
        $limit = time() + 365 * 24 * 3600;
        setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("country", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        //memberでログインの際は名前とメールをを強制的に上書き
        if( $login_info["member_info"]["member_name"] ){
            setcookie("personal_name", $login_info["member_info"]["member_name"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }

        // セッション
        $this->session->set("lang", $lang);
        $this->session->set('time_zone', $time_zone);
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());

        //メンバー課金利用の場合ユーザーキーからプラン及びオプションをもってくる
        if( $login_info["user_info"]["account_model"] == "member" || $login_info["user_info"]["account_model"] == "centre" ){
            require_once("classes/N2MY_IndividualAccount.class.php");
            $objIndividualAccount = new N2MY_IndividualAccount( $this->get_dsn(), $this->get_auth_dsn() );
            $login_info["user_info"]["user_key"];
            $planInfo = $objIndividualAccount->getPlan( $login_info["user_info"]["user_key"] );
            $this->session->set( 'plan_info', $planInfo );
        }
        //　メインメニューに遷移
        $url = $this->get_redirect_url("services/index.php");
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__,$url);
        if ($login_info["user_info"]["intra_fms"]){
            require_once("classes/dbi/user_fms_server.dbi.php");
            $objFmsServer = new UserFmsServerTable($this->get_dsn());
            //複数契約は後ほど
            $serverInfo = $objFmsServer->getRow(sprintf("user_key='%s'", $login_info["user_info"]["user_key"]));
            $this->template->assign("serverInfo", $serverInfo);
            $url = N2MY_BASE_URL."services/login/index.php";
            $this->template->assign("url", $url);
            $this->template->assign("actionName", "action_distinction");
            $this->display("user/login/check_fms.t.html");
        } else {
            $rooms = $this->get_room_list();
            header("Location: ".$url);
        }
        //操作ログ追加
        $this->add_operation_log("login");
        exit;
    }

    private function _error () {
        $message = array(
        "title" => $this->get_message("VCUBE_ID", "login_error_title"),
        "text" => $this->get_message("VCUBE_ID", "login_error_text"),
        "back_url" => "javascript:window.close();",
        "back_url_label" => $this->get_message("VCUBE_ID", "back_url_label")
        );
        $this->template->assign("message", $message);
        return $this->display('user/message.t.html');
    }
}

$main = new AppLogin();
$main->execute();
?>
