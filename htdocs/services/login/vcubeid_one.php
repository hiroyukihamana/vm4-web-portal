<?php
require_once ('classes/AppFrame.class.php');
require_once ('classes/N2MY_Auth.class.php');
require_once ('classes/mgm/MGM_Auth.class.php');

class AppVIDLogin extends AppFrame {

    private $obj_Auth = null;

    function init() {
        header('Pragma: no-cache');
        header('Cache-Control: no-cache');
    }

    function default_view(){
        $res = $this->vcubeId_auth();

        if($res["result"] ===  false){  //なにかおかしかったらそれなりのところに飛ばす
            $backUrl = $res["backUrl"]? $res["backUrl"]: N2MY_BASE_URL."/services/login";
            header("Location:".$backUrl);
            exit;
        } else {
            $id = $res["member_id"];
            $backUrl = $res["backUrl"]? $res["backUrl"]: N2MY_BASE_URL."/services/login";
            $role = $res["role"];
            if($res["is_portal_header"]){
            	$vIdAuthToken = $res["vIdAuthToken"];
            	$contract_id = $res['contract_id'];
            }
        }
        if(!$id){
            $this->action_select_user($res["contract"], $backUrl);
            exit;
        }

        $this->logger2->debug($res);
        $this->action_login($id, $backUrl, $role, $vIdAuthToken, $contract_id);
        exit;
    }

    function vcubeId_auth(){
      $serverName = $_SERVER["SERVER_NAME"];
      if ($serverName == $this->config->get('VCUBE_ONE','meeting_server_name')){
          $consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
          $wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
          $wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
      } else if($serverName == $this->config->get('VCUBE_ONE','document_server_name')){
          $consumerKey = $this->config->get('VCUBE_ONE','one_document_consumer_key');
          $wsseId = $this->config->get('VCUBE_ONE','vcubeid_doc_wsseId');
          $wssePw = $this->config->get('VCUBE_ONE','vcubeid_doc_wssePw');
      } else if($serverName == $this->config->get('VCUBE_ONE','sales_server_name')){
          $consumerKey = $this->config->get('VCUBE_ONE','one_sales_consumer_key');
          $wsseId = $this->config->get('VCUBE_ONE','vcubeid_sls_wsseId');
          $wssePw = $this->config->get('VCUBE_ONE','vcubeid_sls_wssePw');
      }else{
          $consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
          $wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
          $wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
      }
      $domain = $this->config->get('VCUBE_ONE','vcubeid_domain');
      $isSSL = $this->config->get('VCUBE_ONE','vcube_isSSL') ? $this->config->get('VCUBE_ONE','isSSL') : false;
      $isSha1 = $this->config->get('VCUBE_ONE','vcube_isSha1') ? $this->config->get('VCUBE_ONE','isSha1') : false;
      if (!$domain || !$wsseId || !$wssePw || !$consumerKey) {
          $this->logger2->error("parametter error");
          $res["result"]  = false;
          $res["backUrl"] = $_REQUEST["backUrl"]? $_REQUEST["backUrl"]: "";
          return $res;
      }


        $vIdAuthToken = $this->request->get("vIdAuthToken");
        require_once ('classes/vcubeid/vcubeIdCore.class.php');

        $vcubeid = new vcubeIdCore($domain, $wsseId, $wssePw, $isSSL, $isSha1);
        $this->logger2->debug(array($domain, $wsseId, $wssePw, $isSSL, $isSha1, $consumerKey, $vIdAuthToken));
        $response = $vcubeid->existToken($consumerKey, $vIdAuthToken);
        $this->logger2->debug($response, 'One_existToken');

        if($response["result"] === true){
            $res["result"]    = true;
            $res["vcubeId"]  = $response["vcubeId"];
        }else{
            $res["result"]    = false;
            $res["backUrl"]   = $_REQUEST["backUrl"]? $_REQUEST["backUrl"]: "";
            return $res;
        }


        // ステータス確認
        $response_id = $vcubeid->ssologin($consumerKey, $vIdAuthToken, $res["vcubeId"]);
        $this->logger2->debug($response_id, 'ssologin response');
        if($response_id["result"] === true){
        	$contract = $response_id['contracts']['contract'];
        	$contract_id = $this->request->get('cid');
        	$this->logger2->debug($contract_id, 'debug request cid');
        	if(array_key_exists(0, $contract) && $contract_id){
        		//複数契約
        		foreach ($contract as $_contract){
        			if( $_contract['id'] == $contract_id){
        				$contract = $_contract;
        				break;
        			}
        		}
        	}

			$res ["result"] = true;
			$res ["contract"] = $contract;
			$res ["user_id"] = $contract ["meeting"] ["userId"];
			$res ["member_id"] = $contract ["meeting"] ["memberId"];
			$res ['is_portal_header'] = ($contract ["one"] ["header"] === "true");
			$res ['contract_id'] = $contract ['id'];
			$xml = (array) simplexml_load_string( $contract ["settintXml"] );
			$xml_role = (array) $xml ['role'];
			$res ["role"] = $xml_role ["permission"];
        }else{
            $res["result"]    = false;
            $res["vcubeId"]   =  $res["vcubeId"];
        }
        $res["vIdAuthToken"]   =  $vIdAuthToken;
        $res["backUrl"]   = $_REQUEST["backUrl"]? $_REQUEST["backUrl"]: "";
        return $res;
    }

    public function action_select_user($contracts , $backUrl){
        foreach($contracts as $contract){
            $xml = (array)simplexml_load_string($contract["settintXml"]);
            $role = (array)($xml["role"]);
            $contract["meeting"]["role"] = $role["permission"];
            $contract["meeting"]["company_name"] = $contract["name"];

            $vcube_info["member_id"] = $contract["meeting"]["memberId"];
            $vcube_info["user_id"] = $contract["meeting"]["userId"];
            $vcube_info["role"] = $role["permission"];
            $vcube_info["company_name"] = $contract["name"];
            $login_info_list[] = $vcube_info;
        }
        $this->session->set('vcube_one_members', $login_info_list);

        if(!$login_info_list){
            $this->logger2->warn("error no user info");
            header("Location:".$backUrl);
            exit;
        }
        $this->session->set('backUrl', $backUrl);
        $this->session->set('lang', $this->request->get("lang"));
        $this->session->set('vcubeid_login', '1');
        $this->template->assign("vcube_one_members", $login_info_list);
        $selectUrl = $this -> get_redirect_url("services/login/vcubeid_one.php?action_display_select_user");
        header("Location: " . $selectUrl);
        exit;
        //$this->display('user/login/vcubeid_select.t.html');
        //var_dump($login_info_list);
    }

    public function action_display_select_user(){
        $vcube_one_members = $this->session->get('vcube_one_members');
        if(!$vcube_one_members){
            $this->logger2->warn("error no user info");
            $backUrl = $this->session->get('backUrl');
            header("Location:".$backUrl);
            exit;
        }
        $this->template->assign('is_not_display_portal_header', true);
        $this->template->assign('vcube_one_members', $vcube_one_members);
        $this->display('user/login/vcubeid_select.t.html');
    }
    /**
     * ログイン処理
     */
    public function action_login($member_id = null , $backUrl = null , $role = null, $vIdAuthToken = null, $contract_id = null) {

        if($member_id == null){
            $member_id = $this->request->get("member_id");
        }

        if($backUrl == null){
            $backUrl = $this->session->get("backUrl");
        }

        if($role == null){
            $role = $this->request->get("role");
        }

        $lang = $this->request->get("lang");
        if($lang){
            $lang = $this->session->get("lang");
        }

        $this->session->removeAll();
        session_regenerate_id(true);
        // パラメタ
        $lang         = $this->request->get("lang", "ja");
        $time_zone    = htmlspecialchars($this->request->get("time_zone"));
        if ($time_zone === null) {
        	$time_zone    = $this->request->getCookie("time_zone", "9");
        }
        $_country_key = $this->request->get("country", "auto");

        //サーバー取得
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if ( ! $user_info = $obj_MGMClass->getUserInfoById( $member_id ) ){
            $this->logger2->warn($member_id,"get user info");
            header("Location:".$backUrl);
            exit;
        }
        if ( ! $server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] ) ){
           $this->logger2->warn($member_id, "error get server info");
           header("Location:".$backUrl);
           exit;
        } else {
            $this->session->set( "server_info", $server_info );
        }

        // ログインチェック
        $obj_Auth = new N2MY_Auth( $this->get_dsn() );
        if ($login_info = $obj_Auth->checkVcubeId($member_id)) {
            $this->session->set('login', '1');
            $this->session->set('vcubeid_login', '1');
            if ($custom = $this->config->get('SMARTY_DIR','custom')) {
                $login_info["user_info"]["custom"] = $custom;
            }
            $login_info["user_info"]["login_url"] = N2MY_BASE_URL;
            $this->session->set('user_info', $login_info["user_info"]);
            $this->session->set('member_info', $login_info["member_info"]);
        } else {
            $this->logger2->info($member_id, "login error");
            header("Location:".$backUrl);
            exit;
        }

        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());

        if ($custom = $this->config->get('SMARTY_DIR','custom')) {
            $login_info["user_info"]["custom"] = $custom;
        }
        //セールスフラグをチェック
        if($login_info["user_info"]["use_sales"]) {
            if ($login_info["member_info"]["use_sales"] == 1 && $login_info["user_info"]["account_model"] == "member") {
                $meeting_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "meeting");
                $sales_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "sales");
            } else if($login_info["member_info"]["use_sales"] == 1 && $login_info["user_info"]["account_model"] != "member") {
                $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                $sales_room_count = $obj_N2MY_Account->getOwnRoomCount($login_info["member_info"]["member_key"], $login_info["user_info"]['user_key'], "sales");
            } else if($login_info["member_info"]["use_sales"] == 0 && $login_info["member_info"] && $login_info["user_info"]["account_model"] != "member") {
                $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
            } else {
                $meeting_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "meeting");
                $sales_room_count = $obj_N2MY_Account->getRoomCount($login_info["user_info"]['user_key'], "sales");
            }
            //モードの設定
            if (!$meeting_room_count) {
                $this->session->set("service_mode","sales");
            } elseif(!$sales_room_count) {
                $this->session->set("service_mode","meeting");
                $this->logger2->info($login_info["user_info"]["custom"]);
                if ($login_info["user_info"]["custom"] == "sales") {
                    $login_info["user_info"]["custom"] = "";
                }
            } else {
                if ($login_info["user_info"]["custom"] == "sales") {
                    $this->session->set("service_mode","sales");
                } else {
                    $this->session->set("service_mode","meeting");
                }
                $this->session->set("use_mode_change","1");
            }
            $this->session->set("meeting_room_count", $meeting_room_count);
            $this->session->set("sales_room_count", $sales_room_count);
        } elseif ($login_info["user_info"]["custom"] == "sales") {
            $login_info["user_info"]["custom"] = "";
        }

        /**
         * 接続可能なipか確認
         * とりあえずはオプション扱いではなく
         * 自分のkeyとひもづくiplistがあれば契約しているとみなし
         * 接続元のipと照らし合わせる
         */
        if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
            $this->session->removeAll();
            header("Location:".$backUrl);
            exit;
        }
        //MaxMindでIPから所在地を検索
        $remote_addr =  $_SERVER["REMOTE_ADDR"];
        $result_country_key = $this->_get_country_key($_country_key);
        if (!$result_country_key) {
            // 地域コードの不正な指定を防ぐ
            $country_list = $this->get_country_list();
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
                $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    $this->session->set('country_key', $country_row["country_key"]);
                    $result_country_key = $_country_key;
                }
            }
        }else {
            $this->session->set('country_key', $result_country_key);
        }
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$time_zone);
        }
        // クッキー
        $limit = time() + 365 * 24 * 3600;
        setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("country", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        //memberでログインの際は名前とメールをを強制的に上書き
        if( $login_info["member_info"]["member_name"] ){
            setcookie("personal_name", $login_info["member_info"]["member_name"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }

        // ポータル連携用情報保持
        parent::setVidInfo( $login_info["member_info"]["vcube_one_member_id"], $vIdAuthToken, $contract_id, $lang, $time_zone);

        // セッション
        $this->session->set("lang", $lang);
        $this->session->set('time_zone', $time_zone);
        $this->session->set('role', $role);
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());

        //メンバー課金利用の場合ユーザーキーからプラン及びオプションをもってくる
        if( $login_info["user_info"]["account_model"] == "member" || $login_info["user_info"]["account_model"] == "centre" ){
            require_once("classes/N2MY_IndividualAccount.class.php");
            $objIndividualAccount = new N2MY_IndividualAccount( $this->get_dsn(), $this->get_auth_dsn() );
            $login_info["user_info"]["user_key"];
            $planInfo = $objIndividualAccount->getPlan( $login_info["user_info"]["user_key"] );
            $this->session->set( 'plan_info', $planInfo );
        }
        //　メインメニューに遷移
        $url = $this->get_redirect_url("services/index.php");
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__,$url);
        if ($login_info["user_info"]["intra_fms"]){
            require_once("classes/dbi/user_fms_server.dbi.php");
            $objFmsServer = new UserFmsServerTable($this->get_dsn());
            //複数契約は後ほど
            $serverInfo = $objFmsServer->getRow(sprintf("user_key='%s'", $login_info["user_info"]["user_key"]));
            $this->template->assign("serverInfo", $serverInfo);
            $url = N2MY_BASE_URL."services/login/index.php";
            $this->template->assign("url", $url);
            $this->template->assign("actionName", "action_distinction");
            $this->display("user/login/check_fms.t.html");
        } else {
            header("Location: ".$url);
        }
        //操作ログ追加
        $this->add_operation_log("login");
        exit;
    }

    private function _error () {
        $message = array(
        "title" => $this->get_message("VCUBE_ID", "login_error_title"),
        "text" => $this->get_message("VCUBE_ID", "login_error_text"),
        "back_url" => "javascript:window.close();",
        "back_url_label" => $this->get_message("VCUBE_ID", "back_url_label")
        );
        $this->template->assign("message", $message);
        return $this->display('user/message.t.html');
    }

}

$main = new AppVIDLogin();
$main->execute();
