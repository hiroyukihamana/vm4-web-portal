<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once ("classes/dbi/reservation.dbi.php");
require_once ("classes/dbi/room.dbi.php");
require_once ('lib/EZLib/EZUtil/EZDate.class.php');
require_once ("classes/AppFrame.class.php");
require_once ("classes/N2MY_Reservation.class.php");
require_once ("classes/core/Core_Meeting.class.php");
require_once ("classes/core/dbi/Meeting.dbi.php");
//require_once ("classes/mgm/MGM_auth.class.php");

class AppCoreApi extends AppFrame {

    var $obj_Reservation = null;
    var $obj_Room = null;

    var $_name_space = null;
    private $dsn;

    function init() {
        if ($dsn_key = $this->request->get("serverDsnKey")) {
            $this->dsn = $this->get_dsn_value($dsn_key);
        } else {
            $this->dsn = $this->get_dsn();
        }
        $this->obj_Reservation = new ReservationTable( $this->dsn );
        $this->obj_Room = new RoomTable( $this->dsn );
        $this->_name_space = md5(__FILE__);
    }

    function action_flashvers() {
        $objFlash = new N2MY_DB($this->get_dsn(), "flash_version");
        $request = $this->request->getAll();
        $data = array(
            "user_id" => $request["user_id"],
            "flash_version" => $request["flash_version"],
            "create_datetime" => date("Y-m-d H:i:s"),
            );
        $ret = $objFlash->add($data);
        if (DB::isError($ret)) {
            print "0";
        } else {
            print "1";
        }
    }

    function action_presence2_start_meeting() {
//        $room_id    = $this->request->get(N2MY_SESS_NAME);
        $room_id    = $this->request->get('room_id');
        $name       = $this->request->get('name');
        if(!$room_id){
          $room_id = $this->session->get('presence_room_id');
        }else{
          $this->session->set('presence_room_id', $room_id);
          $this->session->set("view_room_key", $room_id);
        }

        if(!$name){
          $name = $this->session->get('presence_name');
        }else{
          $this->session->set('presence_name', $name);
        }
        $submit_key = md5(microtime());
        $knocker_build = md5("knockerbuild_submit_key");
        $this->session->set("knockerbuild_submit_key",$submit_key,$knocker_build);
/*
 *      // パラメタを隠蔽したい場合はこっちを使用してリダイレクトしてください
        $this->session->set('presence_room_id', $room_id);
        $this->session->set('presence_name', $name);
        header("Content-Type: text/xml; charset=UTF-8;");
        print '<?xml version="1.0" encoding="UTF-8"?>';
        print '<result>';
        print '<status>1</status>';
        print '<data>';
        print '<url>'.N2MY_BASE_URL.'services/api.php?action_presence2_enter=</url>';
        print '</data>';
        print '</result>';
*/
        $limit_time = time() + 365 * 24 * 3600;
        setcookie("personal_name", $name, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $room_info = $obj_N2MY_Account->getRoomInfo($room_id);
        $room_info["room_info"]['participant_num'] = 0;
        $room_info["room_info"]['audience_num'] = 0;
        $room_info["room_info"]['whiteboard_num'] = 0;
        $room_info["room_info"]['reserve_count'] = 0;

        require_once("classes/dbi/ordered_service_option.dbi.php");
        $this->obj_OrderedOption = new OrderedServiceOptionTable($this->get_dsn());
        $where_option = "room_key = '".$room_id."'".
            " AND ordered_service_option_status = 1";
        $room_info["option_count"] = $this->obj_OrderedOption->numRows($where_option);

        $display_option_count = 0;
        foreach ($room_info["options" ] as $option)
        {
          if($option)
            $display_option_count++;
        }
        if($room_info["room_info"]["max_audience_seat"] > 0)
          $display_option_count++;
        $room_info["display_option_count"] = $display_option_count;
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$room_info["display_option_count"]);
        $this->logger2->debug($room_info);
        $this->template->assign('room', $room_info);
        $user_info = $this->session->get("user_info");
        if ($user_info["intra_fms"]) {
            require_once("classes/dbi/user_fms_server.dbi.php");
            $obj_UserFmsServer = new UserFmsServerTable($this->get_dsn());
            $serverInfo = $obj_UserFmsServer->getRow(sprintf("user_key=%d", $user_info["user_key"]));
            $this->template->assign('server', $serverInfo["server_address"]);
        } else {
            $this->template->assign('server', $this->config->get("N2MY","fms_server"));
        }
        // 部屋一覧
        $this->template->assign('room_str', $room_id);
        $this->template->assign("hash", md5($_SERVER["HTTP_HOST"]."/".$room_id));
        $this->template->assign('serverDsnKey', $this->get_dsn_key() );
        $this->template->assign('use_clip_share', $user_info['use_clip_share']);
        $this->template->assign( 'displaySize', $_COOKIE["displaySize"] );

        $this->template->assign("knockerbuild_submit_key",$submit_key);
        $lang = $this->get_language();
        $this->_set_template_dir($lang);
        //言語KEY
        $lang_key = substr($lang,0,2);
        if($lang_key == "zh")
          $lang_key = str_replace("_", "-", strtolower($lang));
        setcookie("lang", $lang_key, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);

        $_country_key = $_COOKIE["country"] ? $_COOKIE["country"] : "auto";
        $selected_country_key = $_COOKIE["selected_country_key"] ? $_COOKIE["selected_country_key"] : $_country_key;
        $this->session->set('selected_country_key', $selected_country_key);
        $this->session->set('country_key', $_country_key);

        $this->session->set('presence', 1);
        $this->session->set('login', 1);    // ログイン
        $this->session->set("login_type", 'presence');

        return $this->display('user/start_presence2.t.html');
    }

    function action_presence2_enter() {
        $submit_key = md5(microtime());
        $knocker_build = md5("knockerbuild_submit_key");
        $this->session->set("knockerbuild_submit_key",$submit_key,$knocker_build);
        $room_id    = $this->session->get("presence_room_id");
        $name       = $this->session->get("presence_name");
        $limit_time = time() + 365 * 24 * 3600;
        setcookie("personal_name", $name, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        $room_info = $this->get_room_info($room_id);
        $this->template->assign('room', $room_info);
        $user_info = $this->session->get("user_info");
        if ($user_info["intra_fms"]) {
            require_once("classes/dbi/user_fms_server.dbi.php");
            $obj_UserFmsServer = new UserFmsServerTable($this->get_dsn());
            $serverInfo = $obj_UserFmsServer->getRow(sprintf("user_key=%d", $user_info["user_key"]));
            $this->template->assign('server', $serverInfo["server_address"]);
        } else {
            $this->template->assign('server', $this->config->get("N2MY","fms_server"));
        }
        $this->template->assign('c', $this->request->get("c"));
        // 部屋一覧
        $this->template->assign('room_str', $room_id);
        $this->template->assign("hash", md5($_SERVER["HTTP_HOST"]."/".$room_id));
        $this->template->assign('serverDsnKey', $this->get_dsn_key() );
        $this->template->assign("knockerbuild_submit_key",$submit_key);

        $lang = $this->get_language();
        $this->_set_template_dir($lang);
        return $this->display('user/start_presence2.t.html');
    }

    function action_disconnect_type() {
        $type = $this->request->get('type');
        if ($type) {
            $this->logger2->debug($type);
            $this->session->set('reload_type', $type);
        }
    }

    function action_user_agent() {
        require_once("classes/mgm/dbi/user_agent.dbi.php");
        $user_agent = new MgmUserAgentTable(N2MY_MDB_DSN);
        $request     = $this->request->getAll();
        $ticket      = $this->session->get("user_agent_ticket");
        $this->session->remove("user_agent_ticket");
        // ワンタイム
        if (!$request["ticket"] || ($request["ticket"] != $ticket)) {
            $this->logger2->debug(array($request["ticket"], $ticket));
            print "0";
            return false;
        }
        if (!$server_info = $this->session->get("server_info")) {
            $this->logger2->warn("no server info");
            return false;
        }
        require_once "lib/EZLib/EZUtil/EZUserAgent.class.php";
        $objUserAgent = EZUserAgent::getInstance();
        $browser_info = $objUserAgent->getBrowser();
        if (!$user_info = $this->session->get("user_info")) {
            require_once("classes/dbi/meeting.dbi.php");
            $server_info = $this->session->get("server_info");
            $dsn = $server_info["dsn"];
            $objMeeting = new MeetingTable($dsn);
            $where = "meeting_key = '".$this->session->get("meeting_key")."'";
            $user_key = $objMeeting->getOne($where, "user_key");
            if ($user_key) {
                require_once("classes/dbi/user.dbi.php");
                $objUser = new UserTable($dsn);
                $where = "user_key = '".$user_key."'";
                $user_info = $objUser->getRow($where);
            }
        }
        $login_type = $this->session->get("login_type");
        $user_agent_info = array(
            "db_key"            => $server_info["host_name"] ,
            "user_id"           => $user_info["user_id"],
            "invoice_flg"       => $user_info["invoice_flg"],
            "meeting_key"       => $this->session->get("meeting_key"),
            "participant_key"   => $this->session->get("participant_key"),
            "appli_type"        => $this->session->get("meeting_type"),
            "appli_mode"        => ($user_info["user_status"] == 2) ? "trial" : "general",
            "appli_role"        => ($login_type) ? $login_type : "default",
            "appli_version"     => $request["appli_version"],
            "user_agent"        => $_SERVER["HTTP_USER_AGENT"],
            "os"                => $browser_info["platform"],
            "browser"           => $browser_info["parent"],
            "flash"             => $request["flash"],
            "protcol"           => $request["protocol"],
            "port"              => is_numeric($request["port"]) ? $request["port"] : "",
            "ip"                => $_SERVER["REMOTE_ADDR"],
            "host"              => gethostbyaddr($_SERVER["REMOTE_ADDR"]),
            "monitor_size"      => $request["monitor_size"],
            "monitor_color"     => $request["monitor_color"],
            "camera"            => $request["camera"],
            "mic"               => $request["mic"],
            "speed"             => is_numeric($request["speed"]) ? $request["speed"] : ""
        );
        $error = $user_agent->check($user_agent_info);
        if (EZValidator::isError($error)) {
            $this->logger2->warn(array($user_agent_info,$error));
            print "0";
        } else {
            $ret = $user_agent->add($user_agent_info);
            print "1";
        }
    }

    function action_api() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * 会議中のユーザ一覧を取得する
     */
    function action_room_userlist() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // パラメタ取得
        $room_key = $this->get_room_key();
        $meeting_key = $this->request->get("meeting_key");
        // meeting_keyの指定が無い場合は、最後に開始したmeeging_keyを取得
        if (!$meeting_key) {
            $meeting = new Meeting();
            $meeting_info = $meeting->serviceLogin($room_key);
            $meeting_key = $meeting_info["meeting_key"];
            $this->logger->trace(__FUNCTION__."#meeting_key", __FILE__, __LINE__, array($room_key, $meeting_key));
        }
        // 会議参加者一覧を取得
        $query = array(
            "meeting_ticket" => $meeting_key,
            "provider_id" => $this->provider_id,
            "provider_password" => $this->provider_pw,
        );
        $url = $this->get_core_url("api/meeting/MeetingParticipants.php", $query);
        $this->logger->trace(__FUNCTION__."#url", __FILE__, __LINE__, array($query,$url));
        $output = $this->wget($url);
        $logs = array(
            "room_key" => $room_key,
            "meeting_key" => $meeting_key,
            "list" => $output,
            );
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $logs);
        header("Content-Type: text/xml; charset=UTF-8");
        $this->nocache_header();
        print $output;
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__, $output);
    }

    function action_room_status() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // パラメタ取得
        $room_key = $this->get_room_key();
        $meeting_key = $this->request->get("meeting_key");
        // meeting_keyの指定が無い場合は、最後に開始したmeeging_keyを取得
        if (!$meeting_key) {
            $meeting = new Meeting();
            $meeting_info = $meeting->serviceLogin($room_key);
            $meeting_key = $meeting_info["meeting_key"];
            $this->logger->trace(__FUNCTION__."#meeting_key", __FILE__, __LINE__, array($room_key, $meeting_key));
        }
        // 会議参加者一覧を取得
        $query = array(
            "meeting_ticket" => $meeting_key,
            "provider_id" => $this->provider_id,
            "provider_password" => $this->provider_pw,
        );
        $url = $this->get_core_url("api/meeting/MeetingStatus.php", $query);
        $output = $this->wget($url);
        $output = str_replace("</status>", "</status><meeting_key>".$meeting_key."</meeting_key>", $output);
        $logs = array(
            "room_key" => $room_key,
            "meeting_key" => $meeting_key,
            "status" => $output,
            );
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $logs);
        header("Content-Type: text/xml; charset=UTF-8");
        $this->nocache_header();
        print $output;
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__, $output);
    }

    /**
     * 指定部屋の状態を取得
     */
    function action_all_status() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $_rooms = $this->request->get("rooms");
        $reservations = array();
        // 部屋と会議キーを指定
        if (is_array($_rooms)) {
            foreach ($_rooms as $room_key => $meeting_key) {
                // 契約した部屋のみ取得可
                $query["rooms[".$room_key."]"] = $meeting_key;
                // 予約情報取得
//                $reservations[$room_key] = $this->_get_reservation_list($room_key);
            }
        // 部屋のみ指定（現在の状態）
        } else {
            $rooms = explode(",", $_rooms);
            $where = "room_key in ('".join("','", $rooms)."')";
            $rooms = $this->obj_Room->getRowsAssoc($where, null, null, null, "room_key, meeting_key");
            if ( DB::isError( $rooms ) ) {
                $this->logger->error( $rs->getUserInfo() );
                return $rooms;
            }
            foreach( $rooms as $key => $val ){
                $query[$val["room_key"]] = $val["meeting_key"];
//                $reservation_list = $this->_get_reservation_list( $val["room_key"] );
            }
        }
        // 指定会議の予約情報取得
        $obj_CoreMeeting = new Core_Meeting( $this->dsn );
        $room_list = $obj_CoreMeeting->getMeetingStatus( $query );
        $list = array();
        foreach( $room_list as $key => $room){
            $participants = array();
            foreach($room["participants"] as $participant) {
                // 余計な情報は公開しない
                $participants[] = array(
                    "use_count"             => $participant["use_count"],
                    "participant_type_key"  => $participant["participant_type_key"],
                    "participant_name"      => addslashes($participant["participant_name"]),
                    "participant_type_name" => addslashes($participant["participant_type_name"]),
                    );
            }
            $room["participants"] = $participants;
            $room["reservation_list"] = $this->_get_reservation_list( $room["room_key"] );
            //会議延長を判別
          if(!empty($room["meeting_ticket"]))
          {
            $where = "meeting_key ='".$room["meeting_ticket"]."'";
            $rowcurrentMeeting = $this->obj_Reservation->getRow($where);
          }
          if(!empty($rowcurrentMeeting["reservation_endtime"]) &&  $rowcurrentMeeting["reservation_endtime"] < date("Y-m-d H:i:s")) {
            //予約会議
            $meeting_extend["session"] = $rowcurrentMeeting["reservation_session"];
            $meeting_extend["name"] = $rowcurrentMeeting["reservation_name"];
            $meeting_extend["start"] = $this->makeDate($rowcurrentMeeting["reservation_starttime"]);
            $meeting_extend["end"] = $this->makeDate($rowcurrentMeeting["reservation_endtime"]);
            $room["reservation_list"]["extend"] = $meeting_extend;
          }else if(!empty($room["reservation_list"]["now"]) && $room["reservation_list"]["now"]["meeting_key"] != $room["meeting_ticket"]) {
            //臨時会議
            $meeting_extend["session"] = "";
            $meeting_extend["name"] = "臨時会議";
            $meeting_extend["start"] = "";
            $meeting_extend["end"] = "";
            $room["reservation_list"]["extend"] = $meeting_extend;
            }
            $list[] = $room;
        }

        $this->logger->debug(__FUNCTION__."#url", __FILE__, __LINE__, array($list, $this->_get_passage_time()));
        $this->nocache_header();
        // エンティティ処理
        array_walk_recursive($list, create_function('&$val, $key', '$val = htmlspecialchars($val);'));
        if ($this->request->get('output') == 'xml') {
            require_once("lib/pear/XML/Serializer.php");
            $serializer = new XML_Serializer();
            $serializer->setOption('mode','simplexml');
            $serializer->setOption('encoding','UTF-8');
            $serializer->setOption('addDecl','ture');
            $serializer->setOption('rootName','room_status');
            $serializer->serialize(array("rooms" => $list));
            $output = $serializer->getSerializedData();
            header("Content-Type: text/xml; charset=UTF-8");
            print $output;
        } else {
            header("Content-Type: text/javascript; charset=UTF-8");
            print json_encode($list);
        }
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__, $room_list);
    }

    /**
     * Staffページの状態を取得（セールス）
     */
    function action_outbound_status() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $_rooms = $this->request->get("rooms");
        // 部屋と会議キーを指定
        if (is_array($_rooms)) {
            foreach ($_rooms as $room_key => $meeting_key) {
                // 契約した部屋のみ取得可
                $query["rooms[".$room_key."]"] = $meeting_key;
                // 予約情報取得
                $reservations[$room_key] = $this->_get_reservation_list($room_key);
            }
        // 部屋のみ指定（現在の状態）
        } else {
            $roomkeys = explode(",", $_rooms);
            $rooms = array();
            foreach($roomkeys as $room_key) {
                $rooms[] = mysql_real_escape_string($room_key);
            }
            $where = "room_key in ('".join("','", $rooms)."')";
            $rooms = $this->obj_Room->getRowsAssoc($where, null, null, null, "room_key, meeting_key");
            if ( DB::isError( $rooms ) ) {
                $this->logger->error( $rs->getUserInfo() );
                return $rooms;
            }
            foreach( $rooms as $key => $val ){
                $query[$val["room_key"]] = $val["meeting_key"];
            }
        }
        // 指定会議の予約情報取得
        $obj_CoreMeeting = new Core_Meeting( $this->dsn );
        $room_list = $obj_CoreMeeting->getMeetingStatus( $query );
        $list = array();
        foreach( $room_list as $key => $room){
            $participants = array();
            $room["staff"] = 0;
            $room["customer"] = 0;
            $room["manager"] = 0;
            foreach($room["participants"] as $participant) {
                switch ($participant["participant_type_name"]) {
                case "staff":
                case "whiteboard_staff":
                    $room["staff"]++;
                    if (1 == $participant["is_away"]) {
                        $room["is_away"] = 1;
                    }
                    break;
                case "customer":
                case "whiteboard_customer":
                    $room["customer"]++;
                    break;
                case "manager":
                    $room["manager"]++;
                    break;
                }
                $participants[] = array(
                    "participant_name"      => $participant["participant_name"],
                    "participant_type_name" => $participant["participant_type_name"],
                    );
            }
            //情報量を減らすため、participantsはいれない
            $room["participants"] = "";
            //入室者がnormalが一人でis_awayだったらstatusを0にする。
            if (1 == $room["staff"] && 0 == $room["customer"] && 1 == $room["is_away"]) {
                $room["status"] = "0";
            }
            if ($room["status"] == 1 && $room["is_reserved"] == 1) {
                $room["reservation_list"] = $this->_get_reservation_list( $room["room_key"] );
            } else {
                $room["reservation_list"]["now"] ="";
            }
            $list[] = $room;
        }
        $this->logger->debug(__FUNCTION__."#url", __FILE__, __LINE__, array($list, $this->_get_passage_time()));
        header("Content-Type: text/javascript; charset=UTF-8");
        $this->nocache_header();
        // エンティティ処理
        array_walk_recursive($list, create_function('&$val, $key', '$val = htmlspecialchars($val);'));
        print json_encode($list);
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__, $room_list);
    }

    /*
     * TOPページの部屋選択部分用の情報を取得する
     */
    function action_getTopPageRoomInfo(){
      $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
      $_rooms = $this->request->get("rooms");
      $user_key = $this->request->get("user_key");
      $pcount_flag = $this->request->get("pcount_flag");
    // 部屋の情報取得
      $list = $this->get_all_status($_rooms);
      if($pcount_flag == 1){
    // 全会議室の参加人数取得
        $values["pcount"] = $this->_getRoomsPcount($user_key);
        $list[] = $values;
      }
      $this->logger->debug(__FUNCTION__."#url", __FILE__, __LINE__, array($list, $this->_get_passage_time()));
      $this->nocache_header();
      // エンティティ処理
      array_walk_recursive($list, create_function('&$val, $key', '$val = htmlspecialchars($val);'));
      if ($this->request->get('output') == 'xml') {
        require_once("lib/pear/XML/Serializer.php");
        $serializer = new XML_Serializer();
        $serializer->setOption('mode','simplexml');
        $serializer->setOption('encoding','UTF-8');
        $serializer->setOption('addDecl','ture');
        $serializer->setOption('rootName','room_status');
        $serializer->serialize(array("rooms" => $list));
        $output = $serializer->getSerializedData();
        header("Content-Type: text/xml; charset=UTF-8");
        print $output;
      } else {
        header("Content-Type: text/javascript; charset=UTF-8");
        print json_encode($list);
      }
      $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__, $room_list);
    }

    /*
     * 会議室の参加者取得
     */
    function _getRoomsPcount($user_key){

      // 会議室情報取得
      $where = sprintf( "user_key='%s'", addslashes($user_key));
      $rooms = $this->obj_Room->getRowsAssoc($where, null, null, null, "room_key,room_name, meeting_key");
      require_once('classes/core/dbi/Participant.dbi.php');
      $obj_Participant    = new DBI_Participant( $this->dsn );
      $obj_Meeting        = new DBI_Meeting( $this->dsn );
      $room_pcounts = array();
      foreach ($rooms as $room){
        $val = array();
        // ミーティング情報取得
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_ticket='%s'", addslashes($room["meeting_key"]) ) );
        $val["room_key"] = $room["room_key"];
        $val["room_name"] = $room["room_name"];
        // 会議中のみ人数取得
        if($meeting_info["is_active"]){
          $val["pcount"] = $obj_Participant->getCount($meeting_info["meeting_key"]);
        }
      else{
        $val["pcount"] = 0;
      }
        $room_pcounts[] = $val;
      }
      return $room_pcounts;


    }

    /*
     * 会議室の参加者取得 JSONデータにして返す
    */
    function action_getRoomsPcount(){
      header("Content-Type: text/javascript; charset=UTF-8");
      $user_info = $this->session->get('user_info');
      print json_encode($this->_getRoomsPcount($user_info["user_key"]));
    }

  /*
   * 部屋情報取得
   */
    function action_getRoom(){
      $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);

      // セッション切れだった場合はエラー
    if(!$this->session->has('user_info')){
        return false;
      }

      $user_info = $this->session->get('user_info');
      $member_info = $this->session->get('member_info');
      $avairable_intra_fms = $this->session->get("avairable_intra_fms");    // 1: 利用可能, 0: 利用不可能（非表示）

      $rooms = $this->get_room_list();
      if (!$selectDisplaySize = $this->session->get("selectedDisplaySize")) {
        $displaySize = $_COOKIE["displaySize"];
        $selectedDisplaySize = array();
        foreach ($rooms as $room) {
          $selectDisplaySize[$room["room_info"]["room_key"]] = $displaySize;
        }
        $this->session->set("selectedDisplaySize",$selectDisplaySize);
      }
      //表示する部屋を選択
      $room_key = $this->request->get("room_key") ? $this->request->get("room_key") : $this->session->get("view_room_key");
      $this->logger2->debug($room_key);
      if ($room_key) {
        $room = $rooms[$room_key];
        $this->session->set("view_room_key", $room_key);
      } else {
        //初期表示時は一番最初の部屋を表示
        $room_keys = array_keys($rooms);
        $room = $rooms[$room_keys[0]];
        $room_key = $room_keys[0];
        $this->session->set("view_room_key", $room_keys[0]);
      }
      $room["displaySize"] = $selectDisplaySize[$room_key];
      // 部屋一覧取得
      // メンバー課金
      if ( $member_info && ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre" || $user_info["account_model"] == "free") ) {
        $total_room_count = 1;
      } else {
        //            $rooms = array_slice($rooms, $offset, $page_cnt, true);
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $total_room_count = $obj_N2MY_Account->getRoomCount($user_info['user_key']);
        $display_option_list = $obj_N2MY_Account->getDisplayOptionList($room_key);
        $room_list = array();
        foreach($rooms as $_room_key => $_room){
          $room_list[$_room_key] = $_room["room_info"]["room_name"];
        }
      }

      $isContractShare = 0;
      // 部屋状態取得
      // デスクトップシェアリング
      if ($room["options"]['desktop_share']) {
        $isContractShare = 1;
      }
      $room["room_info"]['participant_num'] = 0;
      $room["room_info"]['audience_num'] = 0;
      $room["room_info"]['whiteboard_num'] = 0;
      $room["room_info"]['reserve_count'] = 0;
      $room["room_info"]['reserves'] = array();

      $filetype = array();
      if ($room["room_info"]["whiteboard_filetype"]) {
        $whiteboard_filetype = unserialize($room["room_info"]["whiteboard_filetype"]);
        $filetype = array();
        if ($whiteboard_filetype["document"]) {
          foreach($whiteboard_filetype["document"] as $val) {
            $filetype[] = $val;
          }
        }
        if ($whiteboard_filetype["image"]) {
          foreach($whiteboard_filetype["image"] as $val) {
            $filetype[] = $val;
          }
        }
      }
      $room["room_info"]["whiteboard_filetype"] = join(",", $filetype);
      //オプション契約数チェック
      require_once("classes/dbi/ordered_service_option.dbi.php");
      $this->obj_OrderedOption = new OrderedServiceOptionTable($this->get_dsn());
      $where_option = "room_key = '".$room_key."'".
          " AND ordered_service_option_status = 1";
      $room["option_count"] = $this->obj_OrderedOption->numRows($where_option);
      //メインページに表示されるオプション数を計算
      $display_option_count = 0;
      foreach ($display_option_list as $option)
      {
        if($option)
          $display_option_count++;
      }

      $room["display_option_count"] = $display_option_count;
      // all_statusはテキストにして返す
      $room_status = $this->get_all_status($room_key);
	  $participants = $room_status[0]['participants'];
	  if($participants){
	  	$_participants = array();
		$_participant = array();
	  	foreach ($participants as $participant) {
	  		foreach ($participant as $key => $value) {
  				$_participant[$key] = htmlspecialchars($value);
		  	}
		  	$_participants[] = $_participant;
	  	}
		$room_status[0]['participants'] = $_participants;
	  }
      // $room["all_status"] = json_encode($this->get_all_status($room_key));
      $room['all_status'] = json_encode($room_status);
      $json_value = json_encode($room);
      header("Content-Type: text/javascript; charset=UTF-8");
      print $json_value;
    }

    /*
     * 部屋のステータス取得
     */
    function get_all_status($select_room_key) {
      $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
      $_rooms = $select_room_key;
      $reservations = array();
      // 部屋と会議キーを指定
      if (is_array($_rooms)) {
        foreach ($_rooms as $room_key => $meeting_key) {
          // 契約した部屋のみ取得可
          $query["rooms[".$room_key."]"] = $meeting_key;
          // 予約情報取得
          //                $reservations[$room_key] = $this->_get_reservation_list($room_key);
        }
        // 部屋のみ指定（現在の状態）
      } else {
        $rooms = explode(",", $_rooms);
        $where = "room_key in ('".join("','", $rooms)."')";
        $rooms = $this->obj_Room->getRowsAssoc($where, null, null, null, "room_key, meeting_key");
        if ( DB::isError( $rooms ) ) {
          $this->logger->error( $rs->getUserInfo() );
          return $rooms;
        }
        foreach( $rooms as $key => $val ){
          $query[$val["room_key"]] = $val["meeting_key"];
          //                $reservation_list = $this->_get_reservation_list( $val["room_key"] );
        }
      }
      // 指定会議の予約情報取得
      $obj_CoreMeeting = new Core_Meeting( $this->dsn );
      $room_list = $obj_CoreMeeting->getMeetingStatus( $query );
      $list = array();
      foreach( $room_list as $key => $room){
        $participants = array();
        foreach($room["participants"] as $participant) {
          // 余計な情報は公開しない
          $participants[] = array(
              "use_count"             => $participant["use_count"],
              "participant_type_key"  => $participant["participant_type_key"],
              "participant_name"      => addslashes($participant["participant_name"]),
              "participant_type_name" => addslashes($participant["participant_type_name"]),
          );
        }
        $room["participants"] = $participants;
        $room["reservation_list"] = $this->_get_reservation_list( $room["room_key"] );
        //会議延長を判別
        if(!empty($room["meeting_ticket"]))
        {
          $where = "meeting_key ='".$room["meeting_ticket"]."'";
          $rowcurrentMeeting = $this->obj_Reservation->getRow($where);
        }
        if(!empty($rowcurrentMeeting["reservation_endtime"]) &&  $rowcurrentMeeting["reservation_endtime"] < date("Y-m-d H:i:s")) {
          //予約会議
          $meeting_extend["session"] = $rowcurrentMeeting["reservation_session"];
          $meeting_extend["name"] = $rowcurrentMeeting["reservation_name"];
          $meeting_extend["start"] = $this->makeDate($rowcurrentMeeting["reservation_starttime"]);
          $meeting_extend["end"] = $this->makeDate($rowcurrentMeeting["reservation_endtime"]);
          $meeting_extend["organizer_name"] =($rowcurrentMeeting["organizer_name"]);
          $room["reservation_list"]["extend"] = $meeting_extend;
        }else if(!empty($room["reservation_list"]["now"]) && $room["reservation_list"]["now"]["meeting_key"] != $room["meeting_ticket"]) {
          //臨時会議
          $meeting_extend["session"] = "";
          $meeting_extend["name"] = "臨時会議";
          $meeting_extend["start"] = "";
          $meeting_extend["end"] = "";
          $meeting_extend["organizer_name"] =($rowcurrentMeeting["organizer_name"]);
          $room["reservation_list"]["extend"] = $meeting_extend;
        }
        $list[] = $room;
      }
    return  $list;
    }

    private function _get_reservation_list( $room_key ) {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // パラメタ
        $start      = $this->request->get("start", date("Y-m-d H:i:s"));
        $end        = $this->request->get("end", "");
        $sort        = array(  $this->request->get("sort_key", "reservation_starttime") => $this->request->get("sort_type", "asc") );
        $limit      = $this->request->get("limit", RESERVATION_MAX_VIEW);
        $offset     = $this->request->get("offset", null);
        // 部屋情報取得
        $room["room_key"] = $room_key;
        $this->template->assign("room", $room);
        // 現在の予約件数
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime <= '".date('Y-m-d H:i:s')."'".
                " AND reservation_endtime > '".date('Y-m-d H:i:s')."'";
        $rowNowMeeting = $this->obj_Reservation->getRow( $where );
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$rowNowMeeting);
        $now = array();
        if ($rowNowMeeting) {
            $now = array(
                "meeting_key" => $rowNowMeeting["meeting_key"],
                "session" => $rowNowMeeting["reservation_session"],
                "name" => $rowNowMeeting["reservation_name"],
                "organizer_name" => $rowNowMeeting["organizer_name"],
                "start" => $this->makeDate( $rowNowMeeting["reservation_starttime"] ),
                "end" => $this->makeDate( $rowNowMeeting["reservation_endtime"] ),
                "max_port" => $rowNowMeeting["max_port"],
                );
        }
//        $this->template->assign("now", $now);
        // 予約情報取得
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime > '".$start."'";
        $count = $this->obj_Reservation->numRows($where);
        $this->template->assign("count", $count);
        // 予約情報取得
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime >= '".$start."'";
        if ($end != "") {
            $where .= " AND reservation_endtime < '".$end."'";
        }
        $reservation_list = $this->obj_Reservation->getRowsAssoc($where, $sort, $limit, $offset);
        $reservations = array();
        $reservations["now"] = $now;

        //10分前データ追加
        $reservation10 = $this->_get10MinutesReservation( $room_key );
        if( $reservation10 ){
            $reservations["minutes10"] = array(
                    "meeting_key" => $reservation10["meeting_key"],
                    "session" => $reservation10["reservation_session"],
                    "name" => $reservation10["reservation_name"],
                    "organizer_name" => $reservation10["organizer_name"],
                    "start" => $this->makeDate( $reservation10["reservation_starttime"] ),
                    "end" => $this->makeDate( $reservation10["reservation_endtime"] )
                    );
        }
        if ($reservation_list) {
            foreach($reservation_list as $_key => $row) {
                // 開始前
                $reservations["list"][] = array(
                    "room_key"    =>    $room_key,
                    "meeting_key" => $row["meeting_key"],
                    "session" => $row["reservation_session"],
                    "name" => $row["reservation_name"],
                    "organizer_name" => $row["organizer_name"],
                    "start" => $this->makeDate( $row["reservation_starttime"] ),
                    "end" => $this->makeDate( $row["reservation_endtime"] )
                    );
            }
        }
        return $reservations;
    }

    private function _get10MinutesReservation( $room_key )
    {
        // 予約情報取得
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime <= '" . date("Y-m-d H:i:00", time() + 10 * 60 ). "'" .
                " AND reservation_starttime > '" . date("Y-m-d H:i:00"). "'";
        $sort = array( "reservation_starttime" => "ASC" );
        return $this->obj_Reservation->getRow($where, "", $sort );
    }

    function action_reservation_list() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // パラメタ
        $room_key   = $this->get_room_key();
        $start      = $this->request->get("start", date("Y-m-d H:i:s"));
        $end        = $this->request->get("end", "");
        $sort_key   = $this->request->get("sort_key", "reservation_starttime");
        $sort_type  = $this->request->get("sort_type", "asc");
        $limit      = $this->request->get("limit", null);
        $offset     = $this->request->get("offset", null);
        // 部屋情報取得
        $room_table = new RoomTable($this->get_dsn());
        $room["room_key"] = $room_key;
        $this->template->assign("room", $room);
        // 現在の予約件数
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime <= '".date('Y-m-d H:i:s')."'".
                " AND reservation_endtime > '".date('Y-m-d H:i:s')."'";
        $_rowNowMeeting = $this->obj_Reservation->getList($where);
        $rowNowMeeting = $_rowNowMeeting[0];
        $now = array();
        if ($rowNowMeeting) {
            $now = array(
                "meeting_key" => $rowNowMeeting["meeting_key"],
                "session" => $rowNowMeeting["reservation_session"],
                "name" => $rowNowMeeting["reservation_name"],
                "start" => $rowNowMeeting["reservation_starttime"],
                "end" => $rowNowMeeting["reservation_endtime"]
                );
        }
        $this->template->assign("now", $now);
        // 予約情報取得
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime > '".$start."'";
        $count = $this->obj_Reservation->numRows($where);
        $this->template->assign("count", $count);
        // 予約情報取得
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime >= '".$start."'";
        if ($end != "") {
            $where .= " AND reservation_endtime < '".$end."'";
        }
        $rowReservation = $this->obj_Reservation->getList($where, $sort_key, $sort_type, $limit, $offset);
        $reservations = array();
        if ($rowReservation) {
            foreach($rowReservation as $_key => $row) {
                // 開始前
                $reservations[] = array(
                    "meeting_key" => $row["meeting_key"],
                    "session" => $row["reservation_session"],
                    "name" => $row["reservation_name"],
                    "start" => $row["reservation_starttime"],
                    "end" => $row["reservation_endtime"]
                    );
            }
        }
        $this->template->assign("reservations", $reservations);
        $output = $this->fetch('user/reservation/reservation.t.xml');
        header("Content-Type: text/xml; charset=UTF-8");
        $this->nocache_header();
        print $output;
        $this->logger->trace(__FUNCTION__."#end",__FILE__,__LINE__);
    }

    /**
     * 会議終了
     */
    function action_stop_meeting($meeting_ticket = null) {
        $obj_CoreMeeting = new Core_Meeting( $this->dsn );
        $objMeeting      = new DBI_Meeting( $this->dsn );
        if($meeting_ticket == null){
            $meeting_ticket = $this->request->get("meeting_ticket");
        }
        if (!$meeting_ticket) {
            $this->logger2->warn($meeting_ticket);
            return false;
        }
        $where = "meeting_ticket = '".addslashes($meeting_ticket)."'";
        $meeting_key = $objMeeting->getOne($where, "meeting_key");
        if (!$meeting_key) {
            $this->logger2->warn($meeting_key);
            return false;
        }
        $obj_CoreMeeting->stopMeeting($meeting_key);
    }

    /**
     * 会議強制終了
     */
    function action_force_stop_meeting(){
        // TODO
        $password = $this->request->get("admin_pw");
        $room_key =  $this->request->get("room_key");
        $user_info = $this->session->get('user_info');
        $is_one_admin = $this->request->get('is_one_admin');
        // 認証
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        require_once 'classes/dbi/user.dbi.php';
        $objMeeting = new DBI_Meeting( $this->dsn );
        $user_db = new UserTable($this->get_dsn());
        $password_flag = true;
        if(!$is_one_admin){
            $admin_password = $user_db->getOne('user_key = ' . $user_info["user_key"] , "user_admin_password");
            $db_admin_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $admin_password);
            if( 0 != strcmp( $db_admin_password, $password ) ){
                $this->logger2->info("password_miss", "force_stop_meeting");
                $res = array("reslut" => "0");
                $password_flag = false;
            }
        }
        if($password_flag){
            $where = "is_active = 1 AND is_reserved = 0 AND room_key = '".addslashes($room_key)."'";
            $meeting_ticket = $objMeeting->getOne($where, "meeting_ticket");
            $this->action_stop_meeting($meeting_ticket);
            $this->logger2->info($meeting_ticket, "force_stop_meeting");
            $res = array("reslut" => "1");
        }
        $json_value = json_encode($res);
        header("Content-Type: text/javascript; charset=UTF-8");
        print $json_value;
    }

    /**
     * イントラfms
     * fmsmove開始処理
     */
    public function action_start_fms_move() {
        $request = $this->request->getAll();
        if (!$request["meeting_key"] || !$request["meeting_sequence_key"]) {
            $this->logger2->warn($request);
            return false;
        } else {
            require_once("classes/dbi/fms_watch_status.dbi.php");
            require_once("classes/core/dbi/MeetingSequence.dbi.php");

            //sequenceinfo
            $objMeetingSequence = new DBI_MeetingSequence($this->dsn);
            $sequenceInfo = $objMeetingSequence->getRow(sprintf("meeting_sequence_key='%s'", $request["meeting_sequence_key"]));
            /**
             * 議事録、映像のどちらかが存在した場合
             * 移動準備
             */
            if (1 == $sequenceInfo["has_recorded_minutes"] ||
                1 == $sequenceInfo["has_recorded_video"]) {

                $objFmsWatch = new FmsWatchTable($this->dsn);
                //fms_move開始
                $data = array(
                            "meeting_key"            => $request["meeting_key"],
                            "meeting_sequence_key"   => $request["meeting_sequence_key"],
                            "status"                 => 1,
                            "registtime"             => date("Y-m-d H:i:s"),
                            "starttime"              => date("Y-m-d H:i:s")
                            );
                $fmsWatchId = $objFmsWatch->add($data);
            } else {
                $fmsWatchId = 0;
            }
            $data = array("meetingSequenceInfo"=>$sequenceInfo, "fmsWatchId"=>$fmsWatchId);
            $this->logger2->debug($data);
            print serialize($data);
        }
    }

    /**
     * イントラfms
     * fmsmove終了処理
     */
    public function action_result_fms_move() {
        $request = $this->request->getAll();
        if (!$request["fmsWatchId"]) {
            $this->logger2->warn($request);
            print 0;
        } else {
            require_once("classes/dbi/fms_watch_status.dbi.php");

            $objFmsWatch = new FmsWatchTable($this->dsn);
            //fms_move開始
            $data = array(
                        "status"    => $request["result"],
                        "endtime"   => date("Y-m-d H:i:s")
                        );
            $where = sprintf("watch_status_key='%s'", $request["fmsWatchId"]);
            $fmsmsWatchId = $objFmsWatch->update($data, $where);
            print 1;
        }
    }

    /**
     * イントラfms利用時
     */
    public function action_stop_meeting_by_fmsweb() {
    	// $this->logger2->info('sleep');sleep(15);// debug code

        $request = $this->request->getAll();
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $request);
        $obj_CoreMeeting = new Core_Meeting( $this->dsn );
        if (!$request["meeting_key"] || !$request["meeting_sequence_key"]) {
            $this->logger2->warn($request);
            print 0;
        } else {
            require_once("classes/core/dbi/MeetingSequence.dbi.php");
            $objMeetingSequence = new DBI_MeetingSequence($this->dsn);
            $where = "meeting_sequence_key = ".$request["meeting_sequence_key"];
            $data = array(
                        "meeting_size_used" => 0,
                        "status"            => 0
                        );
            $objMeetingSequence->update($data, $where);

            $dsn = $this->get_dsn($this->dsn);
            require_once("classes/dbi/fms_watch_status.dbi.php");
            $objFmsWatch = new FmsWatchTable($dsn);

            // 重複チェック
            $where = sprintf( 'meeting_key=%d AND meeting_sequence_key=%d', $request['meeting_key'], $request['meeting_sequence_key'] );
            $watch_status_key = $objFmsWatch->getOne( $where, 'watch_status_key' );
            // $this->logger2->info($watch_status_key, 'debug $watch_status_key');
            if ( !$watch_status_key ) {
            	$fmsData = array(
            			"meeting_key"           => $request["meeting_key"],
            			"meeting_sequence_key"  => $request["meeting_sequence_key"],
            			"status"                => 1,
            			"registtime"            => date("Y-m-d H:i:s")
            	);
            	$objFmsWatch->add($fmsData);
            	$watch_status_key = $objFmsWatch->getOne($where, 'watch_status_key');
            }
            $this->logger2->debug(array($fmsData, $fmsKey));
            print $watch_status_key;
        }
    }

    /**
     * スケジュール一覧を整形してHTMLで出力
     */
    function action_schedule_list() {
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $_GET);
        // 開始時間
        $room_key   = $this->get_room_key();
        $s_year     = $this->request->get("s_year");
        $s_month    = $this->request->get("s_month");
        $s_day      = $this->request->get("s_day");
        $days       = $this->request->get("days", 1);
        $reservation_place       = $this->request->get("reservation_place", N2MY_SERVER_TIMEZONE);
        $start_datetime = mktime(0,0,0,$s_month, $s_day, $s_year);
        // 終了時間
        $end_datetime = $start_datetime + ($days * 3600 * 24);
        return $this->render_schedule_list($room_key, $start_datetime, $end_datetime, $reservation_place);
    }

    public function action_move_fms()
    {
        $request = $this->request->getAll();
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $request);
        // DNSキー
        $dsn = $this->get_dsn($request["server_dsn_key"]);
        require_once("classes/dbi/fms_watch_status.dbi.php");
        $objFmsWatch = new FmsWatchTable($dsn);
        $where = sprintf("watch_status_key=%d AND status = 0", $request["fms_watch_key"]);
        $fmsWatchInfo = $objFmsWatch->getRow($where);
        $this->logger->debug("fmsWatchInfo",__FILE__,__LINE__,$fmsWatchInfo);

        $data = array(
                    "status"=> 1,
                    "starttime"=> date("Y-m-d H:i:s"));
        $where = sprintf("watch_status_key=%d", $request["fms_watch_key"]);
        $objFmsWatch->update($data, $where);

        if (!$fmsWatchInfo || DB::isError($fmsWatchInfo)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__,$fmsWatchInfo->getUserInfo());
            exit;
        }

        // FMSサーバ取得
        $obj_Meeting = new DBI_Meeting($dsn);
        // FMS PATH取得
        $where = "meeting_key = '".$fmsWatchInfo["meeting_key"]."'";
        $fms_path = $obj_Meeting->getOne($where, "fms_path");
        $meetingInfo = $obj_Meeting->getRow($where);
        if ($meetingInfo) {}
        // FMS SERVER取得
        $where = "meeting_key = '".$fmsWatchInfo["meeting_key"]."'" .
                " AND meeting_sequence_key = '".$fmsWatchInfo["meeting_sequence_key"]."'";
                //映像、議事録フラグ
        $obj_MeetingSequence = new DBI_MeetingSequence($this->dsn);
        $meetingSequenceInfo = $obj_MeetingSequence->getRow($where);
        $obj_FmsService = new DBI_FmsServer(N2MY_MDB_DSN);
        $where = "server_key = ".$meetingSequenceInfo["server_key"];
        $fms_server_info = $obj_FmsService->getRow($where);
        $fms_app_name = $this->config->get("CORE", "app_name");
        $fms_address = $fms_server_info["local_address"] ? $fms_server_info["local_address"] : $fms_server_info["server_address"];
        $meeting_id = $obj_Meeting->getMeetingID($fmsWatchInfo["meeting_key"]);
        $url = "http://".$fms_address.":".$fms_server_info["server_port"]."/fms_move.php" .
            "?app_name=" .urlencode($fms_app_name).
            "&meeting_key=".$meetingInfo["fms_path"].$meeting_id.
            "&sequence_key=".$fmsWatchInfo["meeting_sequence_key"].
            "&stream=".$meetingSequenceInfo["has_recorded_video"].
            "&sharedobject=".$meetingSequenceInfo["has_recorded_minutes"].
            "&file_list=".$request["file_list"];

        $this->logger->info(__FUNCTION__, __FILE__, __LINE__,$url);
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 650,
            CURLOPT_TIMEOUT => 650,
            );
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        //戻り値 成功 0, エラー 1
        $ret = @unserialize($ret);
        $this->logger->info("status",__FILE__,__LINE__,$ret);
        //成功 2, 失敗 9
        if (("0" == $ret["stream"])
            && ("0" == $ret["sharedobject"])) {
            $status = 2;
        } else {
            $status = 9;
            $this->logger->error("status",__FILE__,__LINE__,array("stream" => $ret["stream"], "sharedobject" => $ret["sharedobject"]));
            //データの移動に失敗した場合はメールを送信
            $mail_subject = "会議記録の移動に失敗しました";
            $mail_body = "会議記録の移動に失敗しました" ."\n\n" .
                "■ サイト: ".N2MY_BASE_URL."\n" .
                "■ FMSサーバー: ".$fms_server_info["server_address"]."\n".
                "■ ミーティングキー: ".$fmsWatchInfo["meeting_key"]."\n".
                "■ ステータス: ".print_r($ret, true). "\n" .
                "■ FUNCTION: ".__FUNCTION__."\n".
                "■ URL: ".$url."\n".
                "■ 結果: ".print_r($ret, true)."\n";
            if (defined("N2MY_LOG_ALERT_FROM") && defined("N2MY_LOG_ALERT_TO") && N2MY_LOG_ALERT_FROM && N2MY_LOG_ALERT_TO) {
                $this->sendReport(N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body);
            }
            $this->logger2->warn(array(N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body));
        }
        $data = array(
                    "status"=> $status,
                    "endtime"=> date("Y-m-d H:i:s"));
        $where = sprintf("watch_status_key=%d", $request["fms_watch_key"]);
        $objFmsWatch->update($data, $where);
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__,$data);
    }

    public function action_get_fmssize()
    {
        $request = $this->request->getAll();
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $request);
        // DNSキー
        $dsn = $this->get_dsn($request["server_dsn_key"]);
        require_once("classes/dbi/fms_watch_status.dbi.php");
        $objFmsWatch = new FmsWatchTable($dsn);
        $where = sprintf("watch_status_key=%d AND status = 0", $request["fms_watch_key"]);
        $fmsWatchInfo = $objFmsWatch->getRow($where);

        $data = array(
                    "status"=> 1,
                    "starttime"=> date("Y-m-d H:i:s"));
        $where = sprintf("watch_status_key=%d", $request["fms_watch_key"]);
        $objFmsWatch->update($data, $where);

        if (!$fmsWatchInfo || DB::isError($fmsWatchInfo)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__,$fmsWatchInfo->getUserInfo());
            exit;
        }
        // FMSサーバ取得
        $obj_Meeting = new DBI_Meeting($dsn);
        // FMS PATH取得
        $where = "meeting_key = '".$fmsWatchInfo["meeting_key"]."'";
        $meeting_info = $obj_Meeting->getRow($where, "meeting_session_id,fms_path");
        $fms_path = $meeting_info['fms_path'];
        // FMS SERVER取得
        $where = "meeting_key = '".$fmsWatchInfo["meeting_key"]."'" .
                " AND meeting_sequence_key = '".$fmsWatchInfo["meeting_sequence_key"]."'";
                //映像、議事録フラグ
        $obj_MeetingSequence = new DBI_MeetingSequence($this->dsn);
        $meetingSequenceInfo = $obj_MeetingSequence->getRow($where);
        $obj_FmsService = new DBI_FmsServer(N2MY_MDB_DSN);
        $where = "server_key = ".$meetingSequenceInfo["server_key"];
        $fms_server_info = $obj_FmsService->getRow($where);
        $fms_app_name = $this->config->get("CORE", "app_name");
        $fms_address = $fms_server_info["local_address"] ? $fms_server_info["local_address"] : $fms_server_info["server_address"];
        $meeting_id = $obj_Meeting->getMeetingID($fmsWatchInfo["meeting_key"]);
        $url = "http://".$fms_address.":".$fms_server_info["server_port"]."/fms_data.php" .
            "?app_name=" .urlencode($fms_app_name).
            "&meeting_key=".$fms_path.$meeting_id.
            "&sequence_key=".$fmsWatchInfo["meeting_sequence_key"].
            "&stream=".$meetingSequenceInfo["has_recorded_video"].
            "&sharedobject=".$meetingSequenceInfo["has_recorded_minutes"].
            "&moveflg=".$request["moveflg"].
            "&file_list=".$request["file_list"];

        $this->logger->info(__FUNCTION__, __FILE__, __LINE__,$url);
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 650,
            CURLOPT_TIMEOUT => 650,
            );
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        //戻り値 成功 0, エラー 1
        $ret = @unserialize($ret);
        $this->logger->info("status",__FILE__,__LINE__,$ret);
        //成功 2, 失敗 9
        if (("0" == $ret["stream"])
            && ("0" == $ret["sharedobject"])) {
            $status = 2;
        } else {
            $status = 9;
            $this->logger->error("status",__FILE__,__LINE__,array("meeting_key" => $fmsWatchInfo["meeting_key"], "stream" => $ret["stream"], "sharedobject" => $ret["sharedobject"]));
            //データの移動に失敗した場合はメールを送信
            $mail_subject = "会議記録の移動に失敗しました";
            $mail_body = "会議記録の移動に失敗しました" ."\n\n" .
                "■ サイト: ".N2MY_BASE_URL."\n" .
                "■ FMSサーバー: ".$fms_server_info["server_address"]."\n".
                "■ ミーティングキー: ".$fmsWatchInfo["meeting_key"]."\n".
                "■ ステータス: ".print_r($ret, true). "\n".
                "■ FUNCTION: ".__FUNCTION__."\n".
                "■ URL: ".$url."\n".
                "■ 結果: ".print_r($ret, true)."\n";
            if (defined("N2MY_LOG_ALERT_FROM") && defined("N2MY_LOG_ALERT_TO") && N2MY_LOG_ALERT_FROM && N2MY_LOG_ALERT_TO) {
                $this->sendReport(N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body);
            }
            $this->logger2->warn(array(N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body));
        }
        $data = array(
                    "status"=> $status,
                    "endtime"=> date("Y-m-d H:i:s"));
        $where = sprintf("watch_status_key=%d", $request["fms_watch_key"]);
        $objFmsWatch->update($data, $where);
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__,$data);
        $where = "meeting_sequence_key = ".$fmsWatchInfo["meeting_sequence_key"];
        $data = array(
                    "meeting_size_used" => $ret["stream_size"] + $ret["sharedobject_size"],
                    "status"            => 0
                    );
        $obj_MeetingSequence->update($data, $where);

        //会議が終了していたら全容量を取得
        $where = "meeting_key = '".$fmsWatchInfo["meeting_key"]."'";
        $meeting_info = $obj_Meeting->getRow($where);
        if ("0" == $meeting_info["is_active"] && ($meeting_info["meeting_use_minute"] > 0 || "presen" == $meeting_info["meeting_type"])) {
            // 全FMSの容量を取得
            $where = "meeting_key = ".$fmsWatchInfo["meeting_key"].
                " AND (has_recorded_minutes = 1 OR has_recorded_video = 1)";
            // 現在の状態を取得(各FMSの容量および、利用フラグを取得)
            $meeting_sequence_status = $obj_MeetingSequence->getStatus($meeting_info["meeting_key"]);
            // アップロードした資料の容量を取得
            if ($meeting_sequence_status["has_recorded_minutes"] || $meeting_sequence_status["has_recorded_video"]) {
                $obj_CoreMeeting = new Core_Meeting( $this->dsn );
                $document_size = $obj_CoreMeeting->getDocumentSize($meeting_info["meeting_key"]);
                $meeting_sequence_status["meeting_size_used"] += $document_size;
            }

            //全体の利用サイズを更新
            $where = "meeting_key = ".$meeting_info["meeting_key"];
            $obj_Meeting->update($meeting_sequence_status, $where);

            $this->logger2->debug(array(
            "meeting_key"   => $meeting_info["meeting_key"],
            "meeting_info"  => $meeting_sequence_status,
            "document_size" => $document_size)
            );
        }
    }

    public function action_get_meeting_sequence_key()
    {
        $meeting_key = $this->request->get("meeting_key");
        if(!$meeting_key){
          $this->logger2->error( $meeting_key, " parameter error");
          return false;
        }
        $obj_MeetingSequence = new DBI_MeetingSequence($this->dsn);
        $meeting_sequence_key = $obj_MeetingSequence->getOne("meeting_key = ".addslashes($meeting_key), "meeting_sequence_key", array("meeting_sequence_key" => "desc"));
        $this->logger2->info(array($meeting_key, $meeting_sequence_key));
        print $meeting_sequence_key;
    }

    public function action_set_fmssize_by_fms()
    {
        $request = $this->request->getAll();
        $this->logger2->debug($request);
        $dsn = $this->get_dsn($request["server_dsn_key"]);
        $obj_Meeting         = new DBI_Meeting($dsn);
        $obj_MeetingSequence = new DBI_MeetingSequence($this->dsn);

        if(!$request['meeting_sequence_key']){
          $this->logger2->error( $request, "parameter error");
          return false;
        }

        require_once("classes/dbi/fms_watch_status.dbi.php");
        $objFmsWatch = new FmsWatchTable($dsn);
        $where = sprintf('meeting_key=%d AND meeting_sequence_key=%d', $request['meeting_key'], $request['meeting_sequence_key']);
        $data = array(
            "status"    => 1,
            "starttime" => date("Y-m-d H:i:s")
            );
        $objFmsWatch->update($data, $where);
        if (("0" == $request["stream_err"])
            && ("0" == $request["sharedobject_err"])) {
            $status = 2;
        } else {
            $status = 9;
            $server_key = $obj_MeetingSequence->getOne("meeting_sequence_key = '".addslashes($request['meeting_sequence_key'])."'", 'server_key');
            if ($server_key) {
                require_once("classes/mgm/dbi/FmsServer.dbi.php");
                $obj_FmsServer = new DBI_FmsServer(N2MY_MDB_DSN);
                $fms_server_info = $obj_FmsServer->getRow("server_key = ".$server_key);
            }
//            $this->logger->error("status",__FILE__,__LINE__,array("meeting_key" => $fmsWatchInfo["meeting_key"], "stream" => $ret["stream"], "sharedobject" => $ret["sharedobject"]));
            //データの移動に失敗した場合はメールを送信
            $mail_subject = "会議記録の移動に失敗しました";
            $mail_body = "会議記録の移動に失敗しました" ."\n\n" .
                "■ URL: ".N2MY_BASE_URL."\n" .
                "■ FMSサーバー: ".$fms_server_info["server_address"]."\n".
                "■ ミーティングキー: ".$request["meeting_key"]."\n".
                "■ シーケンスキー: ".$request["meeting_sequence_key"]."\n";
            if (defined("N2MY_LOG_ALERT_FROM") && defined("N2MY_LOG_ALERT_TO") && N2MY_LOG_ALERT_FROM && N2MY_LOG_ALERT_TO) {
                $this->sendReport(N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body);
            }
        }
        $data = array(
                "status"    => $status,
                "endtime"   => date("Y-m-d H:i:s"));
        $objFmsWatch->update($data, $where);
        $this->logger2->debug(array($where, $data));
        // 共有メモサイズ取得
        require_once("classes/dbi/shared_file.dbi.php");
        $objSharedFile = new SharedFileTable($dsn);
        $sharedFileSize = $objSharedFile->getSequenceFileSize($request["meeting_sequence_key"]);

        $where = "meeting_sequence_key = ".$request["meeting_sequence_key"];
        $data = array(
                    "meeting_size_used" => $request["stream_size"] + $request["sharedobject_size"] + $sharedFileSize,
                    "status"            => 0
                    );
        $obj_MeetingSequence->update($data, $where);
        $this->logger2->debug(array($where, $data));
        //会議が終了していたら全容量を取得
        $where = "meeting_key = '".$request["meeting_key"]."'";
        $meeting_info = $obj_Meeting->getRow($where);
        if ("0" == $meeting_info["is_active"] && ($meeting_info["meeting_use_minute"] > 0 || "presen" == $meeting_info["meeting_type"])) {
            // 全FMSの容量を取得
            $where = "meeting_key = ".$meeting_info["meeting_key"].
                " AND (has_recorded_minutes = 1 OR has_recorded_video = 1)";
            // 現在の状態を取得(各FMSの容量および、利用フラグを取得)
            $meeting_sequence_status = $obj_MeetingSequence->getStatus($meeting_info["meeting_key"]);
            // アップロードした資料の容量を取得
            if ($meeting_sequence_status["has_recorded_minutes"] || $meeting_sequence_status["has_recorded_video"]) {
                $obj_CoreMeeting = new Core_Meeting( $this->dsn );
                $document_size = $obj_CoreMeeting->getDocumentSize($meeting_info["meeting_key"]);
                $meeting_sequence_status["meeting_size_used"] += $document_size;
            }

            //全体の利用サイズを更新
            $where = "meeting_key = ".$meeting_info["meeting_key"];
            $obj_Meeting->update($meeting_sequence_status, $where);

            $this->logger2->debug(array(
            "meeting_key"   => $meeting_info["meeting_key"],
            "meeting_info"  => $meeting_sequence_status,
            "document_size" => $document_size)
            );
        }
    }

    /*
     * S3へのアップ作業エラーメール
     */
    public function action_send_S3_errorMail(){
        $request = $this->request->getAll();
        $dsn = $this->get_dsn($request["server_dsn_key"]);
        $obj_Meeting         = new DBI_Meeting($dsn);
        $obj_MeetingSequence = new DBI_MeetingSequence($this->dsn);
        $server_key = $obj_MeetingSequence->getOne("meeting_sequence_key = '".addslashes($request['meeting_sequence_key'])."'", 'server_key');
        if ($server_key) {
            require_once("classes/mgm/dbi/FmsServer.dbi.php");
            $obj_FmsServer = new DBI_FmsServer(N2MY_MDB_DSN);
            $fms_server_info = $obj_FmsServer->getRow("server_key = ".$server_key);
        }
        $mail_subject = "S3アップに失敗しました";
        $mail_body = "S3アップに失敗しました" ."\n\n" .
                "■ URL: ".N2MY_BASE_URL."\n" .
                "■ FMSサーバー: ".$fms_server_info["server_address"]."\n".
                "■ ミーティングキー: ".$request["meeting_key"]."\n".
                "■ シーケンスキー: ".$request["meeting_sequence_key"]."\n";
        if (defined("N2MY_LOG_ALERT_FROM") && defined("N2MY_LOG_ALERT_TO") && N2MY_LOG_ALERT_FROM && N2MY_LOG_ALERT_TO) {
            $this->sendReport(N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body);
        }
        $this->logger2->warn($mail_body);
    }

    public function action_send_error(){
    	$request = $this->request->getAll();
    	switch ( $request['action'] ) {
    		case 'faild_add_fms_watch_status':
    			$mail_subject = "Faild add fms_watch_status";
    			$mail_body = 'URL: ' . N2MY_BASE_URL . "\n" .
    					'meeting_key: ' . $request['meeting_key'] . "\n" .
    					'sequence_key: ' . $request['meeting_sequence_key'] . "\n\n" .
    					'fms_watch_status の 追加に失敗しました' . "\n" .
    					'会議記録が 作成中 で止まっている可能性があります' . "\n" .
    					'fms_watch_status を 確認して下さい' . "\n" .
    					'会議記録移動：' . ( $request['is_move_fms_data'] ? '成功' : '失敗' ) . "\n";
    			if ( defined( 'N2MY_LOG_ALERT_FROM' ) && defined( 'N2MY_LOG_ALERT_TO' ) && N2MY_LOG_ALERT_FROM && N2MY_LOG_ALERT_TO ) {
    				parent::sendReport( N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body );
    			}
    			$this->logger2->warn( $mail_body, $mail_subject );
    			break;
    		default:
    			$this->logger2->error( $request );
    	}
    }



    /*
    public function action_issue_certificate()
    {
        $certificate = substr(md5(uniqid(rand(), true)), 1, 16);
        $oneTime = array(
                    "certificate"    =>    $certificate,
                    "createtime"    =>    time());
        $this->session->set("oneTime", $oneTime);
        header("Content-type: application/x-javascript");
        echo json_encode("true");
    }
    */

    public function action_auth_echo_cancel()
    {
        $oneTime = $this->session->get("oneTime");
        $this->session->remove("oneTime");
        $ttl = time() - $oneTime["createtime"];
        $sess_id = $this->request->get("sess_id");
        $result = false;
        if ($oneTime["certificate"] == $sess_id) {
            $str = $sess_id.N2MY_IDENTITY_KEY;
            $result = sha1($str);
        }

        echo $result;
        $this->logger2->debug(array($str, $result, $sess_id));
    }

    function action_record_callback() {
        $body = file_get_contents('php://input');
/*
$body = <<<EOM
<?xml version="1.0" encoding="utf-8"?>
<response>
<subject> error record </subject>
<queue_key>dbcd7dc3c86a1a5d61fdafcccd29fa635c9054ed</queue_key>
<status>1</status>
<error>
    <code>E0700</code>
    <message>system error </message>
</error>
</response>
EOM;
//*/
        $xml = new EZXML();
        $data = $xml->openXML($body);
        $this->logger2->info($body);
        if (!$data) {
            $this->logger2->warn($body);
        } else {
            if (!$record_id = $data["response"]["queue_key"][0]["_data"]) {
                $this->logger2->warn($data);
                return false;
            }
            // キューKEYで認証DBを通す
            require_once 'classes/mgm/MGM_Auth.class.php';
            require_once 'classes/N2MY_MeetingLog.class.php';
            require_once 'classes/dbi/meeting.dbi.php';
            require_once 'classes/core/Core_Meeting.class.php';
            $objMgmRelationTable = new MGM_AuthClass(N2MY_MDB_DSN);
            if (!$server_info = $objMgmRelationTable->getRelationDsn($record_id, "record_gw")) {
                $this->logger2->warn($body);
                return false;
            }
            // 録画GW情報取得
            $objMeetingSequence = new DBI_MeetingSequence( $server_info["dsn"] );
            $where = "record_id = '".addslashes($record_id)."'".
                " AND record_status = 'convert'".
                " AND has_recorded_video = 1";
            if (!$meeting_sequence_info = $objMeetingSequence->getRow($where)) {
                $this->logger2->warn($where);
                return false;
            }
            $this->logger2->debug($meeting_sequence_info);
             // 会議情報取得
            $where = "meeting_key = '".$meeting_sequence_info["meeting_key"]."'".
                " AND is_deleted = 0";
            $obj_Meeting = new MeetingTable( $server_info["dsn"] );
            $meeting_info = $obj_Meeting->getRow($where);
            $meeting_session_id = $meeting_info["meeting_session_id"];
            // 録画生成成功
            if ($data["response"]["status"][0]["_data"]) {
                // 自動会議データ削除
                if ($meeting_sequence_info["record_end_proc"] == "delete") {
                    // ロックがかかっている
                    if ($meeting_info["is_locked"]) {
                        $this->logger2->warn($meeting_session_id, "削除指定がされていたが、保護中のため削除不可");
                    } else {
                        $this->logger2->info($meeting_sequence_info, "録画データ生成後、会議データを自動削除");
                        $obj_CoreMeeting = new Core_Meeting( $server_info["dsn"], N2MY_MDB_DSN );
                        $obj_CoreMeeting->deleteMinute($meeting_sequence_info["meeting_sequence_key"]);
                        $obj_CoreMeeting->deleteVideo($meeting_sequence_info["meeting_sequence_key"]);
                        $obj_CoreMeeting->deleteMeetingTblMinute($meeting_sequence_info["meeting_key"]);
                        $obj_CoreMeeting->deleteMeetingTblVideo($meeting_sequence_info["meeting_key"]);
                    }
                }
                // シーケンス情報変更
                $meeting_sequence_data = array(
                    "record_status"     => "complete",
                    "record_result"     => "",
                    "record_end_proc"   => "",
                    );
            // 録画生成失敗
            } else {
                // エラー時は変換結果を返す
                $err_cd  = $data["response"]["error"][0]["code"][0]["_data"];
                $err_msg = $data["response"]["error"][0]["message"][0]["_data"];
                // シーケンス情報変更
                $meeting_sequence_data = array(
                    "record_status"     => "error",
                    "record_result"     => $err_cd,
                    "record_end_proc"   => "",
                    "record_filepath"   => "",
                    );
            }
            $this->logger2->debug(array($meeting_sequence_data, $where));
            $where = "meeting_sequence_key = ".$meeting_sequence_info["meeting_sequence_key"];
            $result = $objMeetingSequence->update($meeting_sequence_data, $where);
            if (DB::isError($result)) {
                $this->logger2->error($result->getUserInfo());
                return false;
            }
            if ($data["response"]["status"][0]["_data"]) {
                $meetingLog = new N2MY_MeetingLog($server_info["dsn"]);
                $meeting_size = $meetingLog->getMeetingSize($meeting_session_id);
            }
            // ワンタイムURL破棄
            $objMgmRelationTable->deleteRelationData($record_id, "record_gw");
        }
    }

    /**
     * デフォルトページ
     */
    function default_view() {
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__);
        print "";
        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);
    }

    /**
     * 指定日のスケジュール一覧
     */
    function render_schedule_list($room_key, $start_datetime, $end_datetime = null, $reservation_place = N2MY_SERVER_TIMEZONE) {
        $target_date = $start_datetime;
        if ($end_datetime) {
            $next_date = $end_datetime;
        } else {
            $next_date = $target_date + (3600 * 24);
        }
        $_start_date = date("Y-m-d 00:00:00", $target_date);
        $_end_date = date("Y-m-d 00:00:00", $next_date);
        // TimeZone指定
        $EZDate = new EZDate();
        $_start_date = $EZDate->getLocateTime($_start_date, $this->obj_Reservation->serverTimeZone, $reservation_place);
        $_start_date = date("Y-m-d H:i:s", $_start_date);
        $_end_date = $EZDate->getLocateTime($_end_date, $this->obj_Reservation->serverTimeZone, $reservation_place);
        $_end_date = date("Y-m-d H:i:s", $_end_date);
        $obj_Reservation = new N2MY_Reservation($this->get_dsn());
        $options = array(
            "limit" => 25,
            "offset" => 0,
            "start_time" => $_start_date,
            "end_time" => $_end_date,
        );
        $resevation_list = $obj_Reservation->getList($room_key, $options, $reservation_place);
        $this->logger2->debug($resevation_list);
        $total = $obj_Reservation->getCount($room_key, $options);
        $rest = $total - 10;
        $this->template->assign("list", $resevation_list);
        $this->template->assign("start_datetime", $start_datetime);
        $this->template->assign("end_datetime", $end_datetime - (3600 * 24));
        $this->template->assign("timezone", $reservation_place);
        $this->template->assign("total", $total);
        $this->template->assign("rest", $rest);
        $this->display("user/reservation/schedule.t.html");
    }

    /**
     * 新インバウンドページの状態を取得（セールス）
     */
    function action_get_inbound_status() {
      $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
      $this->logger->debug("start start",__FILE__,__LINE__,$this->request->getAll());
      $user_key = $this->request->get("key");
      $inbound_id = $this->request->get("id");
      if (!$user_key || !$inbound_id) {
          return false;
      }
      $obj_CoreMeeting = new Core_Meeting( $this->dsn );
      $room_list = $obj_CoreMeeting->getMeetingStatusInbound( $user_key, $inbound_id);
      $this->logger->debug("room_list",__FILE__,__LINE__,$room_list);
      $list = array();
      if ($room_list) {
        foreach( $room_list as $key => $room){
          $participants = array();
          $room["normal"] = 0;
          $room["customer"] = 0;
          $room["manager"] = 0;
          foreach($room["participants"] as $participant) {
            switch ($participant["participant_type_name"]) {
              case "normal":
              case "staff":
                $room["normal"]++;
                if (1 == $participant["is_away"]) {
                  $room["is_away"] = 1;
                }
                break;
              case "customer":
                $room["customer"]++;
                break;
              case "manager":
                $room["manager"]++;
                break;
            }
            $participants[] = array(
                "participant_name"      => $participant["participant_name"],
                "participant_type_name" => $participant["participant_type_name"],
            );
          }
          //情報量を減らすため、participantsはいれない
          $room["participants"] = "";
          //入室者がnormalが一人でis_awayだったらstatusを0にする。
          if (1 == $room["normal"] && 0 == $room["customer"] && 1 == $room["is_away"]) {
            $room["status"] = "0";
          }
          //$this->logger->info("room",__FILE__,__LINE__,$room["room_key"]);
          //予約確認
          if ($room["status"] == 1 && $room["is_reserved"] == 1) {
            $room["status"] = "0";
          }
          $list[] = $room;
        }
        // エンティティ処理
        array_walk_recursive($list, create_function('&$val, $key', '$val = htmlspecialchars($val);'));
      } else {
        $list = "";
      }
      $this->logger->debug(__FUNCTION__."#url", __FILE__, __LINE__, array($list, $this->_get_passage_time()));
      header("Content-Type: text/javascript; charset=UTF-8");
      $this->nocache_header();
      print json_encode($list);
      $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__, $room_list);
    }

    /**
     *会議終了後再入室（セールス）
     */
    function action_reenter_meeting()
    {
        // 最後に実行されたMeetingKeyを取得
        $room_key = $this->request->get("room_key");
        $meeting_ticket = $this->request->get("meeting_ticket");
        $this->action_stop_meeting($meeting_ticket);
        $_room_info = $this->session->get("room_info");
        $room_options = $_room_info[$room_key]["options"];
        $this->logger2->debug($room_options);
        if ($room_options["whiteboard"]) {
            $type = "whiteboard_staff";
        } else {
            $type = "staff";
        }

        $redirect_url = N2MY_BASE_URL ."services/index.php?action_meeting_start&room_key=".$room_key."&is_narrow=0&type=".$type."&fl_ver=as3";
        header("Location: ".$redirect_url);
        exit;
    }

    function nocache_header() {
        // 日付が過去
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        // 常に修正されている
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        // HTTP/1.1
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        // HTTP/1.0
        header("Pragma: no-cache");
    }

    private function makeDate( $date )
    {
        return preg_replace( "#-#", "/", $date );
    }

    private function _get_dsn( $server_dsn_key )
    {
        static $server_list;
        if (!$server_list) {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        }
        $dsn = $server_list["SERVER_LIST"][$server_dsn_key];
        if (!$dsn) {
            $this->logger2->error($server_dsn_key, "DSN_KEY取得エラー");
            $dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
        }
        return $dsn;
    }

}

$main =& new AppCoreApi();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
