<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:oneda                                                        |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//

require_once('config/config.inc.php');
require_once("classes/dbi/news.dbi.php");
require_once("classes/AppFrame.class.php");

class AppMaintenance extends AppFrame {

    var $newsTable = null;

    function init () {
        $this->newsTable = new NewsTable($this->get_auth_dsn(),
            DB_NEWS_TYPE_MAINTENANCE,
            $this->_lang);
    }

    function auth() {
        $this->checkAuth();
    }

    /**
     *
     */
    function action_desc(){
        if ($id = $_REQUEST['id']) {
            $dsn = $this->config->get("GLOBAL", "dsn");
            if($ret = $this->newsTable->getNewsDesc(mysql_real_escape_string($id))){
                $news = $ret->fetchRow(DB_FETCHMODE_ASSOC);
                $title = $news['news_title'];
                $date = ereg_replace('-', '.', $news['news_date']);
                $contents = nl2br($news['news_contents']);

                $this->template->assign('contents', $contents);
                $this->template->assign('title', $title);
                $this->template->assign('date', $date);
                $this->display("user/maintenance/maintenance_desc.t.html");
            } else {
                header("Location: index.html");
                exit();
            }
        } else {
            header("Location: index.html");
            exit();
        }
        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);

    }

    /**
     *
     */
    function default_view() {
        if ($ret = $this->newsTable->getReleaseList($this->_lang)){
            $news = array();
            while($news_temp = $ret->fetchRow(DB_FETCHMODE_ASSOC)){
                $news_temp['news_date'] = ereg_replace('-', '.', $news_temp['news_date']);
                array_push($news, $news_temp);
            }
            $this->template->assign('news', $news);
            $this->display('user/maintenance/index.t.html');
        }

    }

    /**
     *
     */
    function action_list(){
        if($ret = $this->newsTable->getReleaseList($this->_lang)){//1-Japanese 2-English
            $news = array();
            for($i=0; $i< 5; $i++){
                if($news_temp = $ret->fetchRow(DB_FETCHMODE_ASSOC)){
                    $news_temp['news_date'] = ereg_replace('-', '.', $news_temp['news_date']);
                    array_push($news, $news_temp);
                }
            }
            $this->template->assign('news', $news);
            $this->display('front/news_index.t.html');
        }
    }
}

$main =& new AppMaintenance();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>