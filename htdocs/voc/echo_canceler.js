var installedFlg = false;	// global flag
function loadObject()
{
	installedFlg = true;
	document.write('<object classid="clsid:6B539D58-9EF5-4147-AF29-A9789239C24F" codebase="/voc/vrmsaecc.ocx" id="veControl" width="1" height="1"></object>');
}

// 1: 成功, 1以外: 失敗

function thisMovie(movieName) {
    if (navigator.appName.indexOf("Microsoft") != -1) {
        return window[movieName];
    } else {
        return document[movieName];
    }
}

/**
 * 
 * @param result
 * @return
 * 成功: 1
 * 失敗: エラーコード -10
 */
function authResultCallBack(result)
{
	document.getElementById("vrms").authResultCallback(result);
}

function AuthVE(apiUrl, id)
{
    //var veControl = document.getElementById( "veControl" );
	/*
    if (installedFlg == true) {
    	veControl.AuthVE(apiUrl, id);
    } else {
    	authResultCallBack(-10);
    }
    */
    try {
    	var obj = new ActiveXObject("vrmsaecc.VeControl");
    	veControl.AuthVE(apiUrl, id);
    } catch (e) {
    	authResultCallBack(-10);
    }
}

function DeAuthVE()
{
	return Boolean(veControl.DeAuthVE());
}

// 0: success, not 0: errorcode
function StartVE(recordingDeviceName)
{
	var deviceNum = veControl.GetNumRecordingDevs();
	if (1 == deviceNum) {
		recordingDeviceID = 0;
	} else {
		var rateArray = new Array();
		for (var i = 0; i < deviceNum; i++) {
			rateArray.push(checkSimilarRate(recordingDeviceName, veControl.GetRecordingDeviceName(i)));
		}
		recordingDeviceID = getMaxIndex(rateArray);
	}
	return veControl.StartVE(recordingDeviceID);
}

function getMaxIndex(array)
{
	var max = 0;
	for (var i = 1; i < array.length; i++) {
		max = (array[max] > array[i]) ? max : i;
	}
	return max;
}

function checkSimilarRate(str1, str2)
{
	var str1 = str1.replace(/ /g,"");
	var str2 = str2.replace(/ /g,"");
	var length = Math.max(str1.length, str2.length);
	var match = 0;
	var i, j;
	for (i = 0, j = i + 1; i < length; i++) {
		if (str1.substr(i, j) == str2.substr(i, j)) match ++; 
	}
	return rate = match / length * 100;
}

function StopVE()
{
	return Boolean(veControl.StopVE());
}

function IsStarted()
{
	return Boolean(veControl.IsStarted());
}

function GetNumRecordingDevs()
{
	alert(veControl.GetNumRecordingDevs());
}

function GetRecordingDeviceName()
{
	var arr = new Array();
	for (var i = 0; i < veControl.GetNumRecordingDevs(); i++) {
		arr.push(veControl.GetRecordingDeviceName(i));
	}
	alert(arr.join("\n"));
}

function GetVersion()
{
	return veControl.GetVersion();
}

function IsAuthenticated()
{
	return Boolean(veControl.IsAuthenticated());
}
