<?php

require_once("htdocs/ives/McuTypes.php");
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/resolve/Resolver.php");

$classmap = array(
  'conference'            => 'Conference',
  'onConferenceCreated'   => 'OnConferenceCreatedRequest',
  'onConferenceDestroyed' => 'OnConferenceDestroyedRequest'
);

class ConferenceMngrListner {
	function __construct() {
		$this->logger = EZLogger2::getInstance();
		$this->listener = Resolver::resolveByServerIP();
	}

	function onConferenceCreated($request)
	{
		if ($this->listener)
			$this->listener->conferenceCreated($request);
	}

	function onConferenceDestroyed($request)
	{
		if ($this->listener)
			$this->listener->conferenceDestroyed($request);
	}
}

$main			= new ConferenceMngrListner();
$configProxy 	= new McuConfigProxy();
$conferenceMngrListener = sprintf($configProxy->get("conference_mngr_listener"), $configProxy->get("soap_domain"));

$soapServer = @new SoapServer($conferenceMngrListener, array('classmap' => $classmap));
$soapServer->setClass('ConferenceMngrListner');
$soapServer->handle();
