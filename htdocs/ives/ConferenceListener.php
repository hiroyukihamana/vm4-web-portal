<?php

require_once("htdocs/ives/McuTypes.php");
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/resolve/Resolver.php");

$classmap = array(
	'participant'               => 'Participant',
	'profile'                   => 'Profile',
	'onConferenceEnded'         => 'OnConferenceEndedRequest',
	'onConferenceInited'		=> 'OnConferenceInitedRequest',
	'onParticipantCreated'      => 'OnParticipantCreatedRequest',
	'onParticipantStateChanged' => 'OnParticipantStateChangedRequest',
	'onParticipantDestroyed'    => 'OnParticipantDestroyedRequest',
	'onDocSharingStatus'          => 'OnDocSharingStatusRequest',
	'onRecordingStatus'           => 'OnRecordingStatusRequest'
);

class ConferenceListner {
	function __construct() {
		$this->logger = EZLogger2::getInstance($config);
		$this->listener = Resolver::resolveByServerIP();
		if (!$this->listener) return;
	}

	function onConferenceEnded($request)
	{
		if ($this->listener)
			$this->listener->conferenceEnded($request);
	}

	function onConferenceInited($request)
	{
		if ($this->listener)
			$this->listener->conferenceInite($request);
	}

	function onParticipantCreated($request)
	{
		if ($this->listener)
			$this->listener->participantCreated($request);
	}

	function onParticipantStateChanged($request)
	{
		if ($this->listener)
				$this->listener->participantStateChanged($request);
	}

	function onParticipantDestroyed($request)
	{
		if ($this->listener)
			$this->listener->participantDestroyed($request);
	}

	function onDocSharingStatus($request)
	{
		if ($this->listener)
			$this->listener->onDocSharingStatus($request);
	}

	function onRecordingStatus($request)
	{
		if ($this->listener)
			$this->listener->onRecordingStatus($request);
	}
}

$configProxy	= new McuConfigProxy();

$conferenceListener = sprintf($configProxy->get("conference_listener"), $configProxy->get("soap_domain"));
$soapServer = new SoapServer($conferenceListener, array('classmap' => $classmap));
$soapServer->setClass('ConferenceListner');
$soapServer->handle();
