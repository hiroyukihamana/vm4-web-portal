<?php

class Conference
{
  public $compType;
  public $did;
  public $id;
  public $name;
  public $numActParticipants;
  public $numSlots;
  public $size;
  public $slots;
  public $timestamp;
}
class OnConferenceCreatedRequest
{
  public $confId;
  public $conf;
}
class OnConferenceDestroyedRequest
{
  public $confId;
}

class OnConferenceEndedRequest
{
  public $confId;
}

class Participant
{
  public $audioMuted;
  public $audioSupported;
  public $id;
  public $name;
  public $profile;
  public $state;
  public $textMuted;
  public $textSupported;
  public $type;
  public $videoMuted;
  public $videoSupported;
}
class Profile
{
  public $uid;
  public $name;
  public $videoBitrate;
  public $videoBitrateStrict;
  public $videoFPS;
  public $videoSize;
  public $videoQmin;
  public $videoQmax;
  public $intraPeriod;
}
class OnParticipantCreatedRequest
{
  public $confId;
  public $part;
}
class OnParticipantDestroyedRequest
{
  public $confId;
  public $partId;
}
class OnParticipantStateChangedRequest
{
  public $confId;
  public $data;
  public $partId;
  public $state;
}

class OnConferenceInitedRequest
{
}

class OnDocSharingStatusRequest
{
  public $confId;
  public $partId;
  public $status;
  public $reason;
}

class OnRecordingStatusRequest
{
  public $confId;
  public $filename;
  public $state;
}