<?php

require_once("classes/N2MY_Meeting.class.php");
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/polycom/Polycom.class.php");

try {
	$logger = EZLogger2::getInstance();
	$logger->info($_REQUEST);
	
	$guestDid = $_REQUEST["guestDid"];
	$did = $_REQUEST["confDid"];

	$configProxy 	= new McuConfigProxy();
	$dsn = $configProxy->getDsn();
	
	if (!$guestDid || !$did) {
		throw new Exception("irregal access.");
	}
	
	$polycom		= new PolycomClass($dsn);
	$objN2MYMeeting = new N2MY_Meeting($dsn);
	
	$reservedMeeting= $polycom->getMeetingByTemporaryAddress($guestDid);
	$targetMeeting	= $objN2MYMeeting->getMeeting($reservedMeeting["room_key"], "invite", $reservedMeeting["meeting_ticket"]);
	
	if (!$targetMeeting) {
		throw new Exception("no conference you can participate.");
	}
	else if (strtotime($targetMeeting["start_time"]) > time()) {
		throw new Exception("not in session yet.");
	}
	else if ($targetMeeting["meeting_key"] != $reservedMeeting["meeting_ticket"]) {
		throw new Exception("you cannot participate this conference.");
	}
	else if ($reservedMeeting["is_blocked"])
		throw new Exception("blocked meeting. meeting_key: ".$targetMeeting["meeting_key"]);
	

	$reservation = $polycom->getReservationInfo($targetMeeting["meeting_key"]);
	if (!$reservation)
		throw new Exception("check reservation info.");
	else if ($reservation["reservation_pw"] && 1 == $reservation["reservation_pw_type"])
		throw new Exception("meeting set password.");
	else {
		$logger->info("temporary did: ".$guestDid." accepted");
		echo 1;
	}
		
}
catch (Exception $e) {
	$logger->warn($e." guestdid: ".$guestDid);
	echo 0;
}
