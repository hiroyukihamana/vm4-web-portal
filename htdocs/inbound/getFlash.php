<?php
require_once("classes/AppFrame.class.php");
class AppPaGetFlash extends AppFrame {

    function init() {
    }

    function auth() {
    }

    function default_view(){
        $this->request->set("inbound_lang", $this->request->get("lang"));
        $this->display("inbound/getFlash.t.html");
    }

}
$main = new AppPaGetFlash();
$main->execute();
?>
