<?php
/*
 * Created on 2008/02/14
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");
require_once("config/config.inc.php");

class N2MY_Meeting_Resevation_API extends N2MY_Api
{
    var $_err = null;
    var $_ssl_mode = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
        $this->_name_space = md5(__FILE__);
        $this->_ssl_mode = $this->config->get("N2MY", "ssl_mode", "http");
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
    }

    /**
     * 予約一覧（検索）
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_limit 検索開始日 (デフォルト null)
     * @param datetime end_limit 検索終了日 (デフォルト null)
     * @param string sort_key ソート基準 (デフォルト 予約会議開始時間)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @param int limit 表示件数 (デフォルト null)
     * @param int offset 開始件数 (デフォルト null)
     * @return string $output 会議キー、予約キー、会議名、開始時間、終了時間をxmlで出力
     */
    function action_get_reservation_list()
    {
        require_once("classes/dbi/room.dbi.php");
        require_once("classes/dbi/reservation.dbi.php");

        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        $room_key         = $this->request->get("room_key");
        $reservation_name = htmlspecialchars($this->request->get("reservation_name", ""));
        $start_limit      = $this->request->get("start_limit", "");
        $end_limit        = $this->request->get("end_limit", "");
        $sort_key         = $this->request->get("sort_key", "reservation_starttime");
        $sort_type        = $this->request->get("sort_type", "asc");
        $limit            = $this->request->get("limit", N2MY_API_GET_LIST_LIMIT);
        $offset           = $this->request->get("offset", null);
        $output_type      = $this->request->get("output_type");
        $time_zone        = $this->session->get("time_zone");
        // 部屋情報取得
        $room_info = $this->getRoominfo($room_key);
        $status = 0;
        $this->logger->debug("room_key", __FILE__, __LINE__,$room_key);
        // 検索
        $reservation_obj = new ReservationTable($this->get_dsn(), $time_zone);
        $where = "room_key LIKE '$room_key'" .
                " AND reservation_status = 1";
        if ($start_limit && $end_limit) {
            $where .= " AND (" .
                    "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                    " OR ( reservation_endtime between '$start_limit' AND '$end_limit' )" .
                    " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_endtime )" .
                    " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_endtime ) )";
        }
        if ($reservation_name) {
            $where .= " AND reservation_name LIKE '%$reservation_name%'";
        }
        $reservations = $reservation_obj->getList($where, $sort_key, $sort_type, $limit, $offset);
        if ($reservations) {
            // 一覧表示
            foreach($reservations as $value){
                $reservation_data[] = array(
                    "meeting_key"           => $value["meeting_key"],
                    "reservation_pw"        => $value["reservation_pw"],
                    "reservation_name"      => $value["reservation_name"],
                    "reservation_key"       => $value["reservation_key"],
                    "reservation_session"   => $value["reservation_session"],
                    "reservation_starttime" => $value["reservation_starttime"],
                    "reservation_endtime"   => $value["reservation_endtime"],
                );
            }
            $status = 1;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "reservations" => $reservation_data,
                    ),
        );
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__,$reservations);
        $output = $this->output($result, $output_type);
        return true;
    }

    /**
     * 予約内容詳細（予約、招待者、事前資料）
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return string $output 予約キー、会議室名、会議名、開始時間、終了時間、パスワード、招待者、資料名をxmlで出力
     */
    function action_get_reservation_detail()
    {
        require_once ("classes/N2MY_Reservation.class.php");
        require_once("classes/dbi/reservation.dbi.php");
        require_once("classes/dbi/reservation_user.dbi.php");
        require_once("classes/dbi/room.dbi.php");

        $core_url    = $this->config->get("CORE","base_url");
        $room_key = $this->request->get("room_key");
        $reservation_session = $this->request->get("reservation_session");
        $provider_id = $this->config->get("CORE","provider_id");

        $reservation     = new N2MY_Reservation($this->get_dsn());
        $reservation_obj  = new ReservationTable($this->get_dsn());
        $reservation_user = new ReservationUserTable($this->get_dsn());
        // 予約情報
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        // 会議予約情報取得

        $where = "reservation_session = '".$reservation_session."'";
        $row = $reservation_obj->getRow($where);
        $this->logger->debug("data",__FILE__,__LINE__,$row);
        if ($row) {
            $status = 1;
        } else {
            $result = array(
                "status" => 0,
                "data" => array(
                        "reservation_session"       => $reservation_session,
                        ),
            );
            $output = $this->output($result);
            return true;
        }
        $room_info = $this->getRoominfo($room_key);
        // 参加者情報取得
        $user_list = $reservation_user->getUserList($row["reservation_key"]);
        $this->logger->debug("user_list",__FILE__,__LINE__,$user_list);

        // 事前アップロードファイル取得
        $documents = $reservation->getDocument($provider_id, $room_key, $row["meeting_key"]);
        $this->logger->debug("doc",__FILE__,__LINE__,$documents);

        // 出力
        $result = array(
            "status" => $status,
            "data" => array(
                    "reservation_key"       => $row["reservation_key"],
                    "room_name"             => $room_info["room_name"],
                    "reservation_name"      => $row["reservation_name"],
                    "reservation_starttime" => $row["reservation_starttime"],
                    "reservation_endtime"   => $row["reservation_endtime"],
                    "reservation_pw"        => $row["reservation_pw"],
                    "users"                 => $user_list,
                    "documents"             => $documents,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 予約追加
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_time 開始日時
     * @param datetime end_time 終了日時
     * @param string r_user_name 参加者名 (デフォルト null)
     * @param string r_user_email 参加者メールアドレス (デフォルト null)
     * @param string r_user_lang 参加者言語 (デフォルト null)
     * @param string r_user_timezone 参加者タイムゾーン (デフォルト null)
     * @return boolean
     */
    function action_add_reservation()
    {
        require_once ("classes/dbi/reservation.dbi.php");
        require_once ("classes/dbi/room.dbi.php");
        require_once ("classes/dbi/meeting.dbi.php");
        require_once ("classes/N2MY_Reservation.class.php");
        require_once ("classes/N2MY_Account.class.php");

        $provider_id  = $this->config->get("CORE","provider_id");
        $provider_pw  = $this->config->get("CORE","provider_pw");
        $core_url     = $this->config->get("CORE","base_url");
        $lang         = $this->session->get("lang");
        $country_key  = $this->session->get("country");
        $user_info    = $this->session->get("user_info");

        $reservation     = new N2MY_Reservation($this->get_dsn());
        $reservation_obj = new ReservationTable($this->get_dsn());
        $meeting_table   = new MeetingTable($this->get_dsn());
        $account         = new N2MY_Account($this->get_dsn());

        // 入力値を取得
        $reservation_info = $this->request->getAll();
        if (!$reservation_info["reservation_timezone"]) {
            $reservation_info["reservation_timezone"]    = $this->session->get("time_zone");
        }
        $room_key = $reservation_info["room_key"];
        $room_info = $this->getRoominfo($room_key);

        if ($reservation_info["reservation_starttime"] && $reservation_info["reservation_endtime"]) {
            $reservation_info["start_unix_time"] = EZDate::getLocateTime($reservation_info["reservation_starttime"], N2MY_SERVER_TIMEZONE, $reservation_info["reservation_timezone"]);
            $reservation_info["end_unix_time"]   = EZDate::getLocateTime($reservation_info["reservation_endtime"], N2MY_SERVER_TIMEZONE, $reservation_info["reservation_timezone"]);

            $ret = $reservation_obj->getListBetween($reservation_info["room_key"], date("Y-m-d H:i:s", $reservation_info["start_unix_time"]), date("Y-m-d H:i:s", $reservation_info["end_unix_time"]));
            $this->logger->debug("ret", __FILE__, __LINE__, $ret);
            if (count($ret) > 0) {
                $this->logger->error(__FUNCTION__."#already exist", __FILE__, __LINE__,
                    array(
                        "room_key"   => $room_key,
                        "start_time" => date("Y-m-d H:i:s", $reservation_info["start_unix_time"]),
                        "end_time" => date("Y-m-d H:i:s", $reservation_info["end_unix_time"]))
                );
                $result = array(
                    "status" => 0,
                    "data" => array(
                            "start_time" => date("Y-m-d H:i:s", $reservation_info["start_unix_time"]),
                            "end_time" => date("Y-m-d H:i:s", $reservation_info["end_unix_time"])
                            ),
                );
                $output = $this->output($result);
                exit;
            }
        } else {
            $result = array(
                "status" => 0,
                "data" => array(
                        "start_time" => $reservation_info["reservation_starttime"],
                        "end_time" => $reservation_info["reservation_endtime"]
                        ),
            );
            $output = $this->output($result);
            exit;
        }
        $this->logger->info("time",__FILE__,__LINE__,$reservation_info["reservation_starttime"]."end".$reservation_info["reservation_endtime"]);

        // MeetingKey発行
        $meeting_key = $meeting_table->create_meeting_key($room_key);
        $this->logger->debug(__FUNCTION__."#meeting_key", __FILE__, __LINE__, $meeting_key);
        $reservation_info["meeting_key"] = $meeting_key;
        $reservation_info["reservation_place"] = $this->session->get("country");
        // 予約セッション作成
        $reservation_info["reservation_session"] = md5(uniqid(rand(), true));
        // 予約名を変換
        $reservation_info["reservation_name"] = htmlspecialchars($reservation_info["reservation_name"]);
        // 予約会議登録
        $where = array(
             "room_key"               => $room_key,
             "meeting_key"            => $meeting_key,
             "reservation_name"       => $reservation_info["reservation_name"],
             "reservation_place"      => $reservation_info["reservation_timezone"],
             "reservation_starttime"  => date("Y-m-d H:i:s",$reservation_info["start_unix_time"]),
             "reservation_endtime"    => date("Y-m-d H:i:s",$reservation_info["end_unix_time"]),
             "reservation_pw"         => $reservation_info["reservation_pw"],
             "reservation_session"    => $reservation_info["reservation_session"],
             "reservation_status"     => "1",
             "reservation_registtime" => date("Y-m-d H:i:s"),
             );
        $ret = $reservation_obj->add($where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            $result = array(
                "status" => 0,
                "data" => array(
                        "room_key" => $room_key,
                        ),
            );
            $output = $this->output($result);
            return true;
        }

        // パラメタ作成
        $meeting_name = $reservation_info["reservation_name"];
        // 部屋の契約情報
        $country_key = $user_info["country_key"];
        $this->logger->trace("user_info",__FILE__,__LINE__,$user_info);
        $service_info = $account->getRoomInfo($room_key);

        // レイアウトタイプ取得
        $room_name = $room_info["room_name"];
        if ($room_info["room_layout_type"]) {
            $layout_type = $room_info["room_layout_type"];
        } else {
            $layout_type = "1";
        }
        // 記録容量
        $meeting_rec_size = $user_info["max_storage_size"] * (1024 * 1024);
        // CoreAPIの呼び出し
        $param = array(
            "provider_id"               => $provider_id,
            "provider_password"         => $provider_pw,
            "room_key"                  => $room_key,
            "layout_key"                => $layout_type,
            "meeting_ticket"            => $meeting_key,
            "meeting_room_name"         => $room_name,
            "meeting_country"           => $country_key,
            "meeting_name"              => $meeting_name,
            "meeting_tag"               => $room_key,
            "meeting_max_audience"      => $service_info["room_seat_max_audience_seat"],
            "meeting_max_seat"          => $service_info["room_seat_max_seat"],
            "meeting_size_recordable"   => $meeting_rec_size,
            "meeting_size_uploadable"   => "",
            "meeting_start_datetime"    => date('Y-m-d H:i:s',$reservation_info["start_unix_time"]),
            "meeting_stop_datetime"     => date('Y-m-d H:i:s',$reservation_info["end_unix_time"]),
            "ssl"                       => $service_info["meeting_ssl"],
            "hispec"                    => $service_info["high_quality"],
            "mobile"                    => $service_info["mobile_phone_number"],
            "h323"                      => $service_info["h323_number"],
            "sharing"                   => $service_info["desktop_share"],
        );
        $url = $this->get_core_url("api/meeting/MeetingCreate.php", $param);
        $create_xml = $this->api_execute($url);
        $core_session_key = $create_xml["connect"]["session"][0]["_data"];
        $this->logger->info(__FUNCTION__."#core_session_key", __FILE__, __LINE__,
                array(
                    "params"            => $param,
                    "url"               => $url,
                    "core_session_key"  => $core_session_key,
                )
            );
        $result = array(
            "status" => 1,
            "data" => array(
                    "reservation_key" => $ret,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 予約変更
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string meeting_name 会議名
     * @param datetime start_time 変更後開始時間
     * @param datetime end_time 変更後終了時間
     * @return boolean
     */
    function action_update_reservation()
    {
        require_once ("classes/N2MY_Reservation.class.php");
        require_once("classes/dbi/reservation.dbi.php");

        $room_key             = $this->request->get("room_key");
        $reservation_session  = $this->request->get("reservation_session");
        $room_name            = $this->request->get("reservation_name");
        $update_starttime     = $this->request->get("reservation_starttime");
        $update_endtime       = $this->request->get("reservation_endtime");

        $provider_id          = $this->config->get("CORE","provider_id");
        $provider_pw          = $this->config->get("CORE","provider_pw");
        $core_url             = $this->config->get("CORE","base_url");

        $this->session->remove('reservation_info');
        $reservation     = new N2MY_Reservation($this->get_dsn());

        $reservation_obj = new ReservationTable($this->get_dsn());
        $where = "reservation_session = '".$reservation_session."'";
        $row = $reservation_obj->getRow($where);

        if ($row) {
            // 変更内容をセッションに登録
            if ($room_name) {
                $row["reservation_name"]      = htmlspecialchars($room_name);
            }
            if ($update_starttime) {
                $row["reservation_starttime"] = $update_starttime;
            }
            if ($update_endtime) {
                $row["reservation_endtime"]   = $update_endtime;
            }
            $this->logger->info("reservation_data",__FILE__,__LINE__,$row);

            $row["start_unix_time"] = EZDate::getLocateTime($row["reservation_starttime"], $reservation->serverTimeZone, $row["reservation_place"]);
            $row["end_unix_time"]   = EZDate::getLocateTime($row["reservation_endtime"], $reservation->serverTimeZone, $row["reservation_place"]);

            $ret = $reservation_obj->getListBetween($row["room_key"], date("Y-m-d H:i:s", $row["start_unix_time"]), date("Y-m-d H:i:s", $row["end_unix_time"]), $row["reservation_key"]);
            $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $ret);
            if (count($ret) > 0) {
                $this->logger->error(__FUNCTION__."#already exist", __FILE__, __LINE__,
                    array(
                        "room_key"   => $room_key,
                        "start_time" => date("Y-m-d H:i:s", $row["start_unix_time"]),
                        "end_time" => date("Y-m-d H:i:s", $row["end_unix_time"]))
                );
                $result = array(
                    "status" => 0,
                    "data" => array(
                            "start_time" => $update_starttime,
                            "end_time" => $update_endtime
                            ),
                );
                $output = $this->output($result);
                return true;
            }

            // 予約内容更新
            $where = "reservation_session = '".$reservation_session."'";
            $data = array(
                 "reservation_name"       => $row["reservation_name"],
                 "reservation_starttime"  => date("Y-m-d H:i:s",$row["start_unix_time"]),
                 "reservation_endtime"    => date("Y-m-d H:i:s",$row["end_unix_time"]),
                 "reservation_updatetime" => date("Y-m-d H:i:s"),
                 );
            $this->logger->info("data",__FILE__,__LINE__,$data);
            $ret = $reservation_obj->update($data, $where);
            $status = 1;
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                $status = 0;
            }

            // CoreAPIの呼び出し
            $param = array(
                "provider_id"               => $provider_id,
                "provider_password"         => $provider_pw,
                "room_key"                  => $row["room_key"],
                "meeting_ticket"            => $row["meeting_key"],
                "meeting_name"              => $row["reservation_name"],
                "meeting_start_datetime"    => date("Y-m-d H:i:s",$row["start_unix_time"]),
                "meeting_stop_datetime"     => date("Y-m-d H:i:s",$row["end_unix_time"]),
                );
            $url = $this->get_core_url("api/meeting/MeetingReservationUpdate.php", $param);
            $xml = $this->api_execute($url);
            $this->logger->info(__FUNCTION__."#core_session_key", __FILE__, __LINE__,
                array(
                    "params" => $param,
                    "url" => $url)
            );
            $result = array(
                "status" => $status,
                "data" => array(
                        "reservation_session" => $reservation_session,
                        ),
            );
            $output = $this->output($result);
            return true;
        }
        $result = array(
            "status" => 0,
            "data" => array(
                    "reservation_session" => $reservation_session,
                    ),
        );
        $output = $this->output($result);
        return true;

    }

    /**
     * 予約削除
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return boolean
     */
    function action_delete_reservation()
    {
        require_once ("classes/N2MY_Reservation.class.php");
        require_once("classes/dbi/reservation.dbi.php");

        $room_key            = $this->request->get("room_key");
        $reservation_session = $this->request->get("reservation_session");
        $reservation_pw      = $this->request->get("reservation_pw");
        $provider_id         = $this->config->get("CORE","provider_id");
        $provider_pw         = $this->config->get("CORE","provider_pw");
        $core_url            = $this->config->get("CORE","base_url");

        $this->session->remove('reservation_info');
        $reservation     = new N2MY_Reservation($this->get_dsn());
        $reservation_obj = new ReservationTable($this->get_dsn());
        $where = "reservation_session = '".$reservation_session."'";
        $row = $reservation_obj->getRow($where);
        if ($row) {
            // パスワードチェック
            if ($row["reservation_pw"]) {
                if ($row["reservation_pw"] == $reservation_pw) {
                    $status = 1;
                } else if ($row["reservation_pw"] != $reservation_pw || !$reservation_pw) {
                    $result = array(
                        "status" => 0,
                        "data" => array(
                                "reservation_session"       => $reservation_session,
                                ),
                    );
                    $output = $this->output($result);
                    exit;
                }
            }
            $this->logger->info("row",__FILE__,__LINE__,$row);
            $ret = $reservation_obj->cancel($reservation_session);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                $status = 0;
            }

            // 開始後
            if (strtotime($row["reservation_starttime"]) < time()) {
                $this->logger->info(__FUNCTION__."#会議正常終了", __FILE__, __LINE__);
                $param = array(
                    "provider_id"               => $provider_id,
                    "provider_password"         => $provider_pw,
                    "room_key"                  => $row["room_key"],
                    "meeting_ticket"            => $row["meeting_key"],
                    );
                $url = $this->get_core_url("api/meeting/MeetingCreate.php", $param);
                $create_xml = $this->api_execute($url);
                $core_session_key = $create_xml["connect"]["session"][0]["_data"];
                // CoreAPIの呼び出し
                $param = array(
                    "provider_id"           => $provider_id,
                    "provider_password"     => $provider_pw,
                    "meeting_session_id"    => $core_session_key,
                    );
                $url = $this->get_core_url("api/meeting/MeetingStop.php", $param);
            } else {
                $this->logger->info(__FUNCTION__."#会議前のため消去", __FILE__, __LINE__);
                // CoreAPIの呼び出し
                $param = array(
                    "provider_id"       => $provider_id,
                    "provider_password" => $provider_pw,
                    "room_key"          => $row["room_key"],
                    "meeting_ticket"    => $row["meeting_key"],
                    );
                $url = $this->get_core_url("api/meeting/MeetingReservationCancel.php", $param);
            }
            $xml = $this->api_execute($url);
            $this->logger->info(__FUNCTION__."#core_session_key", __FILE__, __LINE__,
                array(
                    "params" => $param,
                    "url" => $url,
                    "xml" => $xml,
                    )
            );
            $result = array(
                "status" => 1,
                "data" => array(
                        "reservation_session" => $reservation_session,
                        ),
            );
            $output = $this->output($result);
            return true;
        }
        $result = array(
            "status" => 0,
            "data" => array(
                    "reservation_session" => $reservation_session,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 招待者一覧
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return string $output 名前、メールアドレス、言語、タイムゾーンをxmlで出力
     */
    function action_get_invite()
    {
        require_once("classes/dbi/reservation.dbi.php");
        require_once("classes/dbi/reservation_user.dbi.php");

        $reservation_obj = new ReservationTable($this->get_dsn());
        $reservation_user = new ReservationUserTable($this->get_dsn());

        $room_key = $this->request->get("room_key");
        $reservation_session = $this->request->get("reservation_session");
        // 会議予約情報取得
        $where = "reservation_session = '".$reservation_session."'";
        $row = $reservation_obj->getRow($where);
        if ($row) {
            // 参加者情報取得
            $user_list = $reservation_user->getUserList($row["reservation_key"]);
            $result = array(
                "status" => 1,
                "data" => array(
                        "reservation_key"       => $row["reservation_key"],
                        "users"                 => $user_list,
                        ),
            );
            $output = $this->output($result);
            return true;
        }
        $result = array(
            "status" => 0,
            "data" => array(
                    "reservation_session"       => $reservation_session,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 招待者追加
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string r_user_name 参加者名
     * @param string r_user_email 参加者メールアドレス
     * @param string r_user_lang 参加者言語
     * @param string r_user_timezone 参加者タイムゾーン
     * @return boolean
     */
    function action_add_invite()
    {
        require_once("classes/dbi/reservation.dbi.php");
        require_once("classes/dbi/reservation_user.dbi.php");

        $lang = $this->session->get("lang");
        $request_data = $this->request->getAll();

        $reservation_obj = new ReservationTable($this->get_dsn());
        $reservation_user = new ReservationUserTable($this->get_dsn());

        $this->session->remove('reservation_info');
        $where = "reservation_session = '".$request_data["reservation_session"]."'";
        $row = $reservation_obj->getRow($where);
        $this->session->set('reservation_info', $row);
        // パスワードチェック
        if ($row) {
            if ($row["reservation_pw"]) {
                if ($row["reservation_pw"] == $request_data["pw"]) {
                    $status = 1;
                } else if ($row["reservation_pw"] != $request_data["pw"] || !$request_data["pw"]) {
                    $result = array(
                        "status" => 0,
                        "data" => array(
                                "reservation_key"       => $row["reservation_key"],
                                ),
                    );
                    $output = $this->output($result);
                    exit;
                }
            }
            //招待者の人数取得
            $count = "0";
            foreach ($request_data as $key => $value) {
                $strsub = substr($key, 0, -1);
                if ($strsub == "r_user_name_") {
                    $count++;
                }
            }
            $this->logger->info("count",__FILE__,__LINE__,$count);

            for ($i = 0; $i < $count; $i++) {
                $r_user_name_key = "r_user_name_" . $i;
                $r_user_email_key = "r_user_email_" . $i;
                $r_user_place_key = "r_user_timezone_" . $i;
                $r_user_lang_key = "r_user_lang_" . $i;
                $r_user_type_key = "r_user_type_" . $i;
                $r_user_audience_key = "r_user_audience_" . $i;

                if ($request_data["$r_user_name_key"]) {
                    $user = array (
                        'r_user_name'     => htmlspecialchars($request_data[$r_user_name_key]),
                        'r_user_email'    => $request_data[$r_user_email_key],
                        'r_user_place'    => $request_data[$r_user_place_key],
                        'r_user_lang'     => ($request_data[$r_user_lang_key]) ? $request_data[$r_user_lang_key] : $lang,
                        'r_user_audience' => $request_data[$r_user_audience_key]
                    );
                    $user_list[] = $user;
                }
            }
            if ($user_list) {
               for ($i = 0; $i < count($user_list); $i++) {
                   $data = array(
                        "reservation_key"   => $row["reservation_key"],
                        "r_user_name"       => $user_list[$i]["r_user_name"],
                        "r_user_email"      => $user_list[$i]["r_user_email"],
                        "r_user_place"      => $user_list[$i]["r_user_place"],
                        "r_user_lang"       => $user_list[$i]["r_user_lang"],
                        "r_user_audience"   => $user_list[$i]["r_user_audience"],
                        "r_user_session"    => substr(md5(uniqid(rand(), 1)), 0, 24),
                        "r_user_status"     => 1,
                        "r_user_registtime" => date("Y-m-d H:i:s")
                    );
                    $this->logger->info("add",__FILE__,__LINE__,$data);
                    $ret[$i] = $reservation_user->add($data);
                    $status = 1;
                    if (DB::isError($ret[$i])) {
                        $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret[$i]->getUserInfo());
                        $status = 0;
                    }

                }
            }
            $result = array(
                "status" => $status,
                "data" => array(
                        "r_user_key"       => $ret,
                        ),
            );
            $output = $this->output($result);
            return true;
        }
        $result = array(
            "status" => 0,
            "data" => array(
                    "reservation_session"       => $request_data["reservation_session"],
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 招待者削除
     *
     * @param string room_key ルームキー
     * @param string reservation_session
     * @param int r_user_key 参加者キー
     * @return boolean
     */
    function action_delete_invite()
    {
        require_once("classes/dbi/reservation.dbi.php");
        require_once("classes/dbi/reservation_user.dbi.php");

        $core_url            = $this->config->get("CORE","base_url");

        $reservation_obj = new ReservationTable($this->get_dsn());
        $reservation_user = new ReservationUserTable($this->get_dsn());

        $room_key            = $this->request->get("room_key");
        $reservation_session = $this->request->get("reservation_session");
        $reservation_pw      = $this->request->get("reservation_pw");
        $r_user_session      = $this->request->get("r_user_session");

        $this->session->remove('reservation_info');
        $where = "reservation_session = '".$reservation_session."'";
        $row = $reservation_obj->getRow($where);
        if ($row) {
            // パスワードチェック
            if ($row["reservation_pw"]) {
                if ($row["reservation_pw"] == $reservation_pw) {
                    $status = 1;
                } else if ($row["reservation_pw"] != $reservation_pw || !$reservation_pw) {
                    $result = array(
                        "status" => 0,
                        "data" => array(
                                "reservation_key"       => $row["reservation_key"],
                                ),
                    );
                    $output = $this->output($result);
                    exit;
                }
            }

            $where = "r_user_session ='$r_user_session'";
            $r_user_info = $reservation_user->getRow($where);
            if (DB::isError($r_user_info)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$r_user_info->getUserInfo());
                $status = 0;
            }
            if ($r_user_info) {
                $where = "r_user_session ='$r_user_session'";
                $data = array(
                    "r_user_status" => 0,
                    "r_user_updatetime" => date("Y-m-d H:i:s"),
                );
                $ret = $reservation_user->update($data, $where);
                $status = 1;
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                    $status = 0;
                }
            } else {
                $status = 0;
            }
            $result = array(
                "status" => $status,
                "data" => array(
                        "reservation_session"       => $reservation_session,
                        ),
            );
            $output = $this->output($result);
            return true;
        }
        $result = array(
            "status" => 0,
            "data" => array(
                    "reservation_session"       => $reservation_session,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 部屋情報取得
     */
    function getRoominfo($room_key) {
        require_once("classes/dbi/room.dbi.php");

        $room_obj = new RoomTable($this->get_dsn());
        $room_info = $room_obj->get_detail($room_key);
        if (!$room_info) {
            $result = array(
                "status" => 0,
                "data" => array(
                        "room_key" => $room_key,
                        ),
            );
            $output = $this->output($result);
            exit;
        }
    }
}
$main = new N2MY_Meeting_Resevation_API();
$main->execute();