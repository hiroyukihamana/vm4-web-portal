<?php
/*
 * Created on 2008/03/04
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");

class N2MY_Meeting_Account_API extends N2MY_API
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
    }

    /**
     * ユーザーログインパスワードのみ変更
     *
     * @param
     */
    function action_update_user_loginpassword ()
    {
        require_once("classes/dbi/user.dbi.php");
        $userTable    = new UserTable($this->get_dsn());

        $user_id = $this->request->get("user_id");
        $new_password = $this->request->get("user_password");
        if (!$user_id || !$new_password) {
            $status = 0;
        } else if ($this->_isExist($user_id)) {
            $where = "user_id = '".mysql_real_escape_string($user_id)."'";
            $data = array(
                "user_password" => $new_password,
                "user_updatetime" => date("Y-m-d H:i:s")
                );
            $ret = $userTable->update($data, $where);
            $status = 1;
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                $status = 0;
            }
        } else {
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "user_id" => $user_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * ユーザーアドミンパスワードのみ変更
     *
     * @param
     */
    function action_update_user_adminpassword ()
    {
        require_once("classes/dbi/user.dbi.php");
        $userTable    = new UserTable($this->get_dsn());

        $user_id = $this->request->get("user_id");
        $new_admin_password = $this->request->get("user_admin_password");

        if (!$user_id || !$new_admin_password) {
            $status = 0;
        } else if ($this->_isExist($user_id)) {
            $where = "user_id = '".mysql_real_escape_string($user_id)."'";
            $data = array(
                "user_admin_password" => $new_admin_password,
                "user_updatetime" => date("Y-m-d H:i:s")
                );
            $ret = $userTable->update($data, $where);
            $status = 1;
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                $status = 0;
            }
        } else {
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "user_id" => $user_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * メンバー一覧取得
     *
     * @param
     */
    function action_get_member_list ()
    {
        require_once("classes/dbi/user.dbi.php");
        require_once("classes/dbi/member.dbi.php");

        $userTable    = new UserTable($this->get_dsn());
        $memberTable  = new MemberTable($this->get_dsn());

        $user_id = $this->request->get("user_id");

        $where = "user_id = '".mysql_real_escape_string($user_id)."'";
        $user_info = $userTable->getRow($where);
        $status = 0;
        $member_info = "";
        if ($user_info) {
            $where = "user_key = ".addslashes($user_info["user_key"]).
                     " AND member_status = 0";
            $member_info = $memberTable->getRowsAssoc($where);
            $status = 1;
            if (!$member_info) {
                $status = 0;
            }
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "member" => $member_info,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * メンバーアカウント追加
     *
     * @param
     */
    function action_add_member ()
    {
        require_once("classes/dbi/user.dbi.php");
        require_once("classes/dbi/member.dbi.php");
        $userTable    = new UserTable($this->get_dsn());
        $memberTable    = new MemberTable($this->get_dsn());

        $user_id = $this->request->get("user_id");

        $where = "user_id = '".mysql_real_escape_string($user_id)."'";
        $ret = $userTable->select($where);
        $user_info = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $status = 0;
        $ret = "";
        if ($user_info) {
            $member_data["user_key"]                  = $user_info["user_key"];
            $member_data["member_id"]                 = $this->request->get("member_id");
            $member_data["member_pass"]               = $this->request->get("member_password");
            $member_data["member_name"]               = $this->request->get("member_name");
            $member_data["member_group"]              = $this->request->get("member_group");
            $member_data["member_status"]             = "0";

            if ($this->_isExist($member_data["member_id"])) {
                $this->logger->error(__FUNCTION__."#already exist!",__FILE__,__LINE__,$member_data["member_id"]);
                $result = array(
                    "status" => 0,
                    "data"   => array(
                            "member_id" => $member_data["member_id"],
                            ),
                );
                $output = $this->output($result);
                return true;
            } else {
                $ret = $memberTable->add($member_data);
                $status = 1;
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                    $status = 0;
                }
            }
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "member_key" => $ret,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * メンバーアカウント修正
     *
     * @param
     */
    function action_update_member ()
    {
        require_once("classes/dbi/member.dbi.php");
        $memberTable    = new MemberTable($this->get_dsn());

        $member_id = $this->request->get("member_id");

        $where = "member_id = '".mysql_real_escape_string($member_id)."'";
        $ret = $memberTable->select($where);
        $member_info = $ret->fetchRow(DB_FETCHMODE_ASSOC);

        if ($member_info) {
            $member_data["member_pass"]    = $this->request->get("member_password", $member_info["member_pass"]);
            $member_data["member_name"]    = $this->request->get("member_name", $member_info["member_name"]);
            $member_data["member_group"]   = $this->request->get("member_group", $member_info["member_group"]);

            $ret = $memberTable->update($member_data, $where);
            $status = 1;
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                $status = 0;
            }
        } else {
            $this->logger->error(__FUNCTION__."#not member_info!",__FILE__,__LINE__,$member_id);
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "member_id" => $member_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * メンバーアカウント停止
     *
     * @param
     */
    function action_delete_member ()
    {
        require_once("classes/dbi/member.dbi.php");
        $memberTable    = new MemberTable($this->get_dsn());

        $member_id = $this->request->get("member_id");

        $where = "member_id = '".mysql_real_escape_string($member_id)."'";
        $ret = $memberTable->select($where);
        $member_info = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        if ($member_info) {
            $data = array(
                "member_status" => "-1",
                );
            $ret = $memberTable->update($data, $where);
            $status = 1;
        } else {
            $status = 0;
        }
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "member_id" => $member_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * メンバーグループ取得
     *
     * @param
     */
    function action_get_member_group ()
    {
        require_once("classes/dbi/user.dbi.php");
        require_once("classes/dbi/member_group.dbi.php");

        $userTable    = new UserTable($this->get_dsn());
        $groupTable   = new MemberGroupTable($this->get_dsn());

        $user_id = $this->request->get("user_id");

        $where = "user_id = '".mysql_real_escape_string($user_id)."'";
        $user_info = $userTable->getRow($where);

        if ($user_info) {
            $where = "user_key = ".addslashes($user_info["user_key"]);
            $group_info = $groupTable->getRowsAssoc($where);
            $this->logger->info("user",__FILE__,__LINE__,$group_info);
            $status = 1;
        }
        if (!$user_info || !$group_info) {
            $group_info = "";
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "group" => $group_info,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * メンバーグループ追加
     *
     * @param
     */
    function action_add_member_group ()
    {
        require_once("classes/dbi/user.dbi.php");
        require_once("classes/dbi/member_group.dbi.php");

        $userTable    = new UserTable($this->get_dsn());
        $groupTable   = new MemberGroupTable($this->get_dsn());

        $user_id = $this->request->get("user_id");
        $group_name = $this->request->get("group_name");

        $where = "user_id = '".mysql_real_escape_string($user_id)."'";
        $ret = $userTable->select($where);
        $user_info = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $status = 0;
        if ($user_info && $group_name) {
            $where = "user_key = ".addslashes($user_info["user_key"]).
                     " AND member_group_name = '".addslashes($group_name)."'";

            if ($groupTable->numRows($where) > 0) {
                $this->logger->error(__FUNCTION__."#alredy exist!",__FILE__,__LINE__,$group_name);
                $result = array(
                    "status" => 0,
                    "data"   => array(
                            "member_group_name" => $group_name,
                            ),
                );
                $output = $this->output($result);
                exit;
            } else {
                $data = array(
                    "user_key" => $user_info["user_key"],
                    "member_group_name" => $group_name,
                );
                $ret = $groupTable->add($data);
                $status = 1;
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                    $status = 0;
                }
                $result = array(
                    "status" => $status,
                    "data"   => array(
                            "member_group_key" => $ret,
                            ),
                );
                $output = $this->output($result);
                exit;
            }
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "user_id" => $user_id,
                    "member_group_name" => $group_name,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * メンバーグループ名修正
     *
     * @param
     */
    function action_update_member_group ()
    {
        require_once("classes/dbi/user.dbi.php");
        require_once("classes/dbi/member_group.dbi.php");

        $userTable    = new UserTable($this->get_dsn());
        $groupTable   = new MemberGroupTable($this->get_dsn());

        $user_id = $this->request->get("user_id");
        $member_group_key = $this->request->get("member_group_key");
        $group_name = $this->request->get("group_name");

        $where = "user_id = '".mysql_real_escape_string($user_id)."'";
        $ret = $userTable->select($where);
        $user_info = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $status = 0;
        if ($user_info && $member_group_key && $group_name) {
            $where = "user_key = ".addslashes($user_info["user_key"]).
                     " AND member_group_name = '".addslashes($group_name)."'";
            if ($groupTable->numRows($where) > 0) {
                $this->logger->error(__FUNCTION__."#alredy exist!",__FILE__,__LINE__,$group_name);
                $status = 0;
            } else {
                $where = "member_group_key = ".addslashes($member_group_key);
                $data = array(
                    "member_group_name" => $group_name,
                );
                $ret = $groupTable->update($data, $where);
                $status = 1;
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                    $status = 0;
                }
            }
        }

        $result = array(
            "status" => $status,
            "data"   => array(
                    "user_id" => $user_id,
                    "member_group_name" => $group_name,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * メンバーグループ削除
     *
     * @param
     */
    function action_delete_member_group ()
    {
        require_once("classes/dbi/user.dbi.php");
        require_once("classes/dbi/member_group.dbi.php");

        $userTable    = new UserTable($this->get_dsn());
        $groupTable   = new MemberGroupTable($this->get_dsn());

        $user_id = $this->request->get("user_id");
        $member_group_key = $this->request->get("member_group_key");

        $where = "user_id = '".$user_id."'";
        $ret = $userTable->select($where);
        $user_info = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $status = 0;
        if ($user_info && $member_group_key) {
            $where = "member_group_key = ".addslashes($member_group_key);
            $data = array(
                "user_key" => "0",
            );
            $ret = $groupTable->update($data, $where);
            $status = 1;
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                $status = 0;
            }
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "user_id" => $user_id,
                    "member_group_key" => $member_group_key,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 部屋一覧取得
     *
     * @param string user_id ユーザーID
     * @return
     */
    function action_get_roomlist()
    {
        require_once("classes/dbi/room.dbi.php");
        require_once("classes/N2MY_Account.class.php");

        $user_info = $this->session->get("user_info");

        $account    = new N2MY_Account($this->get_dsn());
        $room_table = new RoomTable($this->get_dsn());

        $status = 0;
        // 部屋一覧取得
        $where = "user_key = ".$user_info["user_key"].
                 " AND room_status = 1";
        $rooms = $room_table->getRowsAssoc($where, "room_key", "ASC", null, null);
        if (DB::isError($rooms)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$rooms->getUserInfo());
            $result = array(
                "status" => 0,
                "data" => array(
                        "user_id" => $user_info["user_id"],
                        ),
            );
            $output = $this->output($result);
            return true;
        }
        $this->logger->info("rooms",__FILE__,__LINE__,$rooms);
        if ($rooms) {
            $status = 1;
            foreach ($rooms as $value) {
                $room_option = $account->getRoomOptionList($value["room_key"]);
                $room_keys["room"][] = array(
                    "room_key"            => $value["room_key"],
                    "room_name"           => $value["room_name"],
                    "meeting_ssl"         => $room_option["meeting_ssl"],
                    "desktop_share"       => $room_option["desktop_share"],
                    "high_quality"        => $room_option["high_quality"],
                    "mobile_phone_number" => $room_option["mobile_phone_number"],
                    "h323_number"         => $room_option["h323_number"],
                    "hdd_extention"       => $room_option["hdd_extention"],
                );

            }
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                 "rooms" => $room_keys,
            ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 部屋名変更
     *
     * @param
     */
    function action_update_room ()
    {
        require_once("classes/dbi/room.dbi.php");

        $roomTable = new RoomTable($this->get_dsn());

        $room_key  = $this->request->get("room_key");
        $room_name = $this->request->get("room_name");

        $where = "room_key = '".$room_key."'";
        $room_info = $roomTable->getRow($where);
        $status = 0;
        if ($room_info) {
            $status = "1";
            $data = array(
                "room_name" => $room_name,
            );
            $ret = $roomTable->update($data, $where);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                $status = 0;
            }
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "room_key" => $room_key,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /*
     * id からそのユーザーID、メンバーIDが存在するかチェック
     */
    function _isExist($id){
        require_once("classes/dbi/user.dbi.php");
        require_once("classes/dbi/member.dbi.php");

        //member とuser のテーブルをチェック
        $userTable    = new UserTable($this->get_dsn());
        $memberTable  = new MemberTable($this->get_dsn());

        $where1 = "user_id = '".addslashes($id)."'";
        $where2 = "member_id = '".addslashes($id)."'";
        if ($userTable->numRows($where1) > 0 || $memberTable->numRows($where2) > 0){
            return true;
        } else {
            return false;
        }
    }
}
$main = new N2MY_Meeting_Account_API();
$main->execute();