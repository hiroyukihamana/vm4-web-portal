<?php
require_once("classes/N2MY_Api.class.php");

class N2MY_Meeting_Auth_API extends N2MY_Api
{
    var $session_id = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    function auth()
    {
    }

    /**
     * ログイン処理
     *
     * @param string id ユーザーID
     * @param string pw ユーザーパスワード
     * @return string $output テンプレートを表示
     */
    function action_login()
    {
        require_once("classes/N2MY_Auth.class.php");
        $login = new N2MY_Auth($this->get_dsn());

        $id          = $this->request->get("id");
        $pw          = $this->request->get("pw");
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get("time_zone", "9");
        $enc         = $this->request->get("enc", null);
        $output_type = $this->request->get("output_type");
        $user_type = "";
        // encordタイプに併せてパスワードエンコード
        if ($enc == "md5") {
            $pw = md5($pw);
        } else if ($enc == "sha1") {
            $pw = sha1($pw);
        }

        // ユーザ確認
        $user_info = $login->check($id, $pw, $enc);
        $status = 0;
        $session_id = "";
        // メンバー
        if (isset($user_info["member_id"])) {
            $status = 1;
            $user_info["user_info"] = array(
                "user_key"  => $user_info["user_key"],
                "user_id"   => $user_info["member_id"],
                "user_name" => $user_info["member_name"],
                );
        } elseif ($user_info) {
            $status = 1;
            $user_info["user_info"] = array(
                "user_key" => $user_info["user_key"],
                "user_id" => $user_info["user_id"],
                "user_name" => "",
                );
        }
        // 認証成功時
        if ($status == 1) {
            // セッションスタート
            $session = EZSession::getInstance();
            // ID を取得
            $session_id = session_id();
            // ユーザ情報配置
            if  ($output_type) {
                $session->set("output_type", $output_type);
            }
            $session->set("login", "1");
            $session->set("user_info", $user_info);
            $session->set("lang", $lang);
            $session->set("country", $country);
            $session->set("time_zone", $time_zone);
        }
        $this->logger->trace(__FUNCTION__."#login user!", __FILE__, __LINE__, $user_info);
        $result = array(
            "status" => $status,
            "data" => array(
                    "session" => $session_id
                    ),
            );
        $output = $this->output($result);
        return true;
    }

    /**
     * ログアウト処理
     *
     * @param
     * @return
     */
    function action_logout()
    {
        $this->checkAuthorization();
        $status = 0;
        if (session_destroy()) {
            $status = 1;
        }
        $result = array(
            "status" => $status,
            );
        $output = $this->output($result);

    }

    function action_show() {

    }
}
$main = new N2MY_Meeting_Auth_API();
$main->execute();