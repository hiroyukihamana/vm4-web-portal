<?php
/*
 * Created on 2008/01/31
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");

class N2MY_Meeting_API extends N2MY_Api
{

    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
    }

    /**
     * 会議キー取得
     *
     * @param boolean $header_flg ヘッダを出力
     * @return string $output  会議キー、会議名をxmlで出力
     */
    function action_create_meeting($header_flg = true)
    {
        require_once("classes/N2MY_Meeting.class.php");

        $room_key    = $this->request->get("room_key");
        $provider_id = $this->config->get("CORE","provider_id");
        $provider_pw = $this->config->get("CORE","provider_pw");
        $core_url    = $this->config->get("CORE","base_url");
        $meeting = new N2MY_Meeting($this->get_dsn(), $provider_id, $provider_pw, $core_url);
        $output_type = $this->request->get("output_type");
        // 現在利用可能な会議情報取得
        $now_meeting  = $meeting->getNowMeeting($room_key);
        $this->logger->info("now",__FILE__,__LINE__,$now_meeting);
        $meeting_key  = $now_meeting["meeting_key"];
        $meeting_name = $now_meeting["meeting_name"];
        // センタサーバ指定
        $country_id = "jp";
        if (isset($extension["locale"])) {
            switch ($extension["locale"]) {
            case "jp" :
            case "us" :
            case "cn" :
                $country_id = $extension["locale"];
                break;
            }
        }
        // オプション指定
        $option = array(
            "meeting_name" => $meeting_name,
            "start_time"   => $now_meeting["start_time"],
            "end_time"     => $now_meeting["end_time"],
            "country_id"   => $country_id,
        );
        $core_meeting_key = $meeting->createMeeting($room_key, $meeting_key, $option);

        $now_meeting["meeting_key"] = $core_meeting_key;
        $status = 1;
        if (!$now_meeting["meeting_key"]) {
            $status = 0;
        }
        $now_meeting["meeting_password"] = ($now_meeting["meeting_password"]) ? md5($now_meeting["meeting_password"]) : "";
        $meeting_info = array();
        $meeting_info[$core_meeting_key] = $now_meeting;
        $this->session->set("meeting_info", $meeting_info);

        $result = array(
            "status" => $status,
            "data"   => array(
                "meeting_key"  => $now_meeting["meeting_key"],
                "meeting_name" => $meeting_name,
            ),
        );
        $output = $this->output($result, $output_type);
        return true;
    }

    /**
     * 会議開始（会議キーを指定して開始）
     *
     * @param boolean $header_flg ヘッダを出力
     * @param string type 参加タイプ (デフォルト normal)
     * @param int meeting_key 会議キー
     * @param string name ユーザー名
     * @param string narrow narrow (デフォルト 0)
     * @param string lang 言語 (デフォルト ja)
     * @param string  skin_type レイアウトタイプ (デフォルト null)
     * @param string role 通常、招待者
     * @return string $output urlをxmlで出力
     */
    function action_start_meeting_with_key($header_flg = true)
    {
        require_once("classes/N2MY_Meeting.class.php");

        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        // 出力タイプ指定
        $output_type = $this->request->get("output_type");
        $meeting_key = $this->request->get("meeting_key");
        $type = $this->request->get("type", "normal");
        // ユーザ情報を整形
        $user_info = $this->session->get("user_info");
        // リクエストパラメタで上書き可能
        $user_info["name"]     = $this->request->get("name", $user_info["user_info"]["user_name"]);
        $user_info["narrow"]   = $this->request->get("narrow", 0);
        $user_info["lang"]     = $this->request->get("lang", "ja");
        $user_info["skin_type"]= $this->request->get("skin_type", "");
        $user_info["role"]     = $this->request->get("role");
        // 会議開始URL取得
        $provider_id = $this->config->get("CORE","provider_id");
        $provider_pw = $this->config->get("CORE","provider_pw");
        $core_url    = $this->config->get("CORE","base_url");
        $redirect_url = "";

        if ($meeting_key) {
            $status = 1;
            $meeting = new N2MY_Meeting($this->get_dsn(), $provider_id, $provider_pw, $core_url);
            $url = $meeting->startMeetingUrl($meeting_key, $type, $user_info);
            $this->logger->debug(__FUNCTION__, __FILE__, __LINE__,array(
                $meeting_key,
                $type,
                $user_info,
                "url" => $url));
            $meeting_info = $this->session->get("meeting_info");
            $meeting_info[$meeting_key]["start_url"] = $url;
            $this->session->set("meeting_info", $meeting_info);

            $redirect_url = "?action_auth_start&meeting_key=".$meeting_key."&".N2MY_SESS_NAME."=".session_id();
        } else {
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                 "url" => urlencode($redirect_url),
            ),
        );
        $output = $this->output($result, $output_type);
        return true;
    }

    /**
     * 会議開始（会議を作成して、そのまま開始）
     *
     * @param
     * @return 会議データを作成し、リダイレクトで会議室に入室
     */
    function action_start_meeting()
    {
        $output_type = $this->request->get("output_type");
        if (!$output_type) {
            $output_type = $this->session->get("output_type");
        }
        // 会議作成
        ob_start();
        $this->action_create_meeting();
        $content = ob_get_contents();
        ob_end_clean();
        $this->logger->debug("content",__FILE__,__LINE__,$content);

        // 取得したデータを配列に変換
        $api = new N2MY_Api();
        if ($content) {
            $data = $api->opendata($content, $output_type);
        } else {
            return false;
        }
        $this->logger->info("content",__FILE__,__LINE__,$data);
        // meeting_key取得
        if ($output_type == "json" || $output_type == "php") {
            $meeting_key = $data["data"]["meeting_key"];
        } else {
            $meeting_key = $data["result"]["data"]["meeting_key"];
        }
        // 会議開始URL生成
        $this->request->set("meeting_key", $meeting_key); // 会議キーを指定
        ob_start();
        $this->action_start_meeting_with_key();
        $content = ob_get_contents();
        ob_end_clean();
        // 取得したデータを配列に変換
        if ($content) {
            $data = $api->opendata($content, $output_type);
        } else {
            return false;
        }
        if (!$output_type) {
            $output_type = $this->session->get("output_type");
        }
        // meeting_key取得
        if ($output_type == "json" || $output_type == "php") {
            $redirect_url = $data["data"]["url"];
        } else {
            $redirect_url = $data["result"]["data"]["url"];
        }
        // そのままリダイレクト
        $hostname = $_SERVER["SERVER_NAME"];
        $port = "";
        // SSL対応
        if (isset ($_SERVER["HTTPS"])) {
            $schema = "https";
            // デフォルトポートであれば不要
            if ($_SERVER["SERVER_PORT"] != "443") {
                $port = ":" . $_SERVER["SERVER_PORT"];
            }
        } else {
            $schema = "http";
            // デフォルトポートであれば不要
            if ($_SERVER["SERVER_PORT"] != "80") {
                $port = ":" . $_SERVER["SERVER_PORT"];
            }
        }
        $url = $schema . "://" . $hostname . $port . $_SERVER["SCRIPT_NAME"].urldecode($redirect_url);
        $this->logger->info(__FUNCTION__."#redirect_dir", __FILE__, __LINE__, $url);
        header("Location: ".$url);
    }

    /**
     * 認証会議への入室
     *
     * @param string meeting_key 会議キー
     * @param string n2my_session n2myのセッションID
     * @param string reservation_pw 予約パスワード
     * @return パスワードがあっていれば入室、間違っていれば再度認証画面に遷移
     */
    function action_auth_start()
    {
        $meeting_key = $this->request->get("meeting_key");
        $session_id  = $this->request->get(N2MY_SESS_NAME);
        $meeting_data = $this->session->get("meeting_info");
        $this->logger->info("session",__FILE__,__LINE__,$meeting_data);
        if ($meeting_key) {
            $meeting_info = $meeting_data[$meeting_key];
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__, $meeting_info);
            // パスワードあり
            if ($meeting_info["meeting_password"]) {
                $password = $this->request->get("reservation_pw");
                $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array($password, md5($password)));
                // 入力パスワード同じならOK
                if (md5($password) == $meeting_info["meeting_password"]) {
                    $url = $meeting_info["start_url"];
                    $this->logger->info(__FUNCTION__."#予約パスワード付き", __FILE__, __LINE__, $url);
                    header("Location: ".$url);
                    exit();
                } else {
                    $api = new N2MY_Api();
                    $result = array(
                        "status" => "0",
                        "data"   => array(
                            "meeting_key"  => $meeting_key,
                        ),
                    );
                    $output = $api->output($result);
                    print $output;
                }
            } else {
                // パスワード無し／予約なし
                $url = $meeting_info["start_url"];
                $this->logger->info(__FUNCTION__."#予約無し", __FILE__, __LINE__, $url);
                header("Location: ".$url);
                exit();
            }
        }
    }
}
$main = new N2MY_Meeting_API();
$main->execute();