<?php
/*
 * Created on 2008/02/06
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");

class N2MY_MeetingLog_API extends N2MY_Api
{
    var $session_id = null;
    var $meetingLog = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
    }

    /**
     * 会議記録一覧（検索）
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param string user_name 参加者名 (デフォルト null)
     * @param datetime start_limit 検索開始日 (デフォルト null)
     * @param datetime end_limit 検索終了日 (デフォルト null)
     * @param int offset 開始件数 (デフォルト null)
     * @param int limit 表示件数 (デフォルト null)
     * @param string sort_key ソート基準 (デフォルト 会議開始時間)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @return $string $output 会議キー、会議名、日時、参加者、サイズ、映像有無、議事録有無
     */
    function action_get_meetinglog_list()
    {

        $room_key     = $this->request->get("room_key");
        $meeting_name = htmlspecialchars($this->request->get("meeting_name"));
        $user_name    = htmlspecialchars($this->request->get("user_name"));
        $start_limit  = $this->request->get("start_limit");
        $end_limit    = $this->request->get("end_limit");
        $offset       = $this->request->get("offset");
        $limit        = $this->request->get("limit", N2MY_API_GET_LIST_LIMIT);
        $sort_key     = $this->request->get("sort_key");
        $sort_type    = $this->request->get("sort_type");
        $output_type  = $this->request->get("output_type");
        $provider_id  = $this->config->get("CORE","provider_id");
        $provider_pw  = $this->config->get("CORE","provider_pw");
        $status = 0;

        $query = array(
            "provider_id"       => $provider_id,
            "provider_password" => $provider_pw,
            "room_key"          => $room_key,
            "title"             => $meeting_name,
            "participant"       => $user_name,
            "start_datetime"    => $start_limit,
            "end_datetime"      => $end_limit,
            "limit"             => $limit,
            "offset"            => $offset,
            "sort_key"          => $sort_key,
            "sort_type"         => $sort_type,
        );
        $url = $this->get_core_url("api/meeting/MeetingInfoList.php", $query);
        $result = $this->wget($url);
        $log_list_xml = $this->opendata($result, "xml");
        $this->logger->info("log_list_xml",__FILE__,__LINE__,$log_list_xml);
        $log_list = array();
        if (isset($log_list_xml["result"]["Meeting"][0])) {
            $status = 1;
            foreach($log_list_xml["result"]["Meeting"] as $_key1 => $meeting) {
                $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, $meeting);
                $log_data["meeting_key"]         = $meeting["ticket"];
                $log_data["meeting_session_id"]  = $meeting["meetingSessionId"];
                $log_data["meeting_name"]        = $meeting["Title"];
                $log_data["meetinglog_password"] = $meeting["Password"];
                $log_data["meetinglog_protect"]  = $meeting["Protect"];
                $log_data["date"]                = $meeting["date"];
                $mult = pow(10, 2);// 小数点2桁表示
                $log_data["filesize"]            = round(($meeting["Filesize"] / (1024 * 1024)) * $mult) / $mult;
                $log_data["video_log"]           = $meeting["VideoLog"];
                $log_data["meeting_log"]         = $meeting["MinutesLog"];
                $log_data["version"]             = $meeting["Version"];

                $users = array();
                $_users = isset($meeting["Users"]["User"]) ? $meeting["Users"]["User"] : array();
                $user_list = array();
                foreach($_users as $_key2 => $user) {
                    if ($user["TYPE"] != "audience" && $user["NAME"] != "") {
                        $user_list["user"][] = array(
                            "key" => $user["key"],
                            "type" => $user["TYPE"],
                            "name" => $user["NAME"]
                            );
                    }
                }
                $log_data["user_list"] = $user_list;

                $sequences = array();
                if($log_data["video_log"]||$log_data["meeting_log"]){
                       $sequences=array();
                    if(!$log_data["version"] ){
                        $sequences["sequence"] = array(
                            "meeting_sequence_key"  => $meeting["meeting_sequences"]["meeting_sequence"]["key"],
                            "meeting_size_used"     => $meeting["meeting_sequences"]["meeting_sequence"]["SIZE"],
                            "has_recorded_minutes"  => $log_data["video_log"],
                            "has_recorded_video"    => $log_data["meeting_log"]
                        );
                    } else if( $meeting["meeting_sequences"]["meeting_sequence"]["VIDEO"] || $meeting["meeting_sequences"]["meeting_sequence"]["MINUTES"] ){
                        $sequences["sequence"] = array(
                            "meeting_sequence_key"  => $meeting["meeting_sequences"]["meeting_sequence"]["key"],
                            "meeting_size_used"     => $meeting["meeting_sequences"]["meeting_sequence"]["SIZE"],
                            "has_recorded_minutes"  => $meeting["meeting_sequences"]["meeting_sequence"]["MINUTES"],
                            "has_recorded_video"    => $meeting["meeting_sequences"]["meeting_sequence"]["VIDEO"]
                        );
                    }
                    $log_data["sequences"] = $sequences;
                }
                $this->logger->info(__FUNCTION__, __FILE__, __LINE__, $log_data);
                $log_list[] = $log_data;
            }
        } else if (isset($log_list_xml["result"]["Meeting"]["id"])) {
            $status = 1;
            $meeting = $log_list_xml["result"]["Meeting"];
            $log_data["meeting_key"]             = $meeting["ticket"];
            $log_data["meeting_session_id"]  = $meeting["meetingSessionId"];
            $log_data["meeting_name"]        = $meeting["Title"];
            $log_data["meetinglog_password"] = $meeting["Password"];
            $log_data["meetinglog_protect"]  = $meeting["Protect"];
            $log_data["date"]                = $meeting["date"];
            $mult = pow(10, 2);// 小数点2桁表示
            $log_data["filesize"]            = round(($meeting["Filesize"] / (1024 * 1024)) * $mult) / $mult;
            $log_data["video_log"]           = $meeting["VideoLog"];
            $log_data["meeting_log"]         = $meeting["MinutesLog"];
            $log_data["version"]             = $meeting["Version"];

            $users = array();
            $_users = isset($meeting["Users"]["User"]) ? $meeting["Users"]["User"] : array();
            $user_list = array();
            foreach($_users as $_key2 => $user) {
                if ($user["TYPE"] != "audience" && $user["NAME"] != "") {
                    $user_list["user"][] = array(
                        "key" => $user["key"],
                        "type" => $user["TYPE"],
                        "name" => $user["NAME"]
                        );
                }
            }
            $log_data["user_list"] = $user_list;
            $sequences = array();
            if($log_data["video_log"]||$log_data["meeting_log"]){
                   $sequences=array();
                if(!$log_data["version"] ){
                    $sequences["sequence"] = array(
                        "meeting_sequence_key"  => $meeting["meeting_sequences"]["meeting_sequence"]["key"],
                        "meeting_size_used"     => $meeting["meeting_sequences"]["meeting_sequence"]["SIZE"],
                        "has_recorded_minutes"  => $log_data["video_log"],
                        "has_recorded_video"    => $log_data["meeting_log"]
                    );
                } else if( $meeting["meeting_sequences"]["meeting_sequence"]["VIDEO"] || $meeting["meeting_sequences"]["meeting_sequence"]["MINUTES"] ){
                    $sequences["sequence"] = array(
                        "meeting_sequence_key"  => $meeting["meeting_sequences"]["meeting_sequence"]["key"],
                        "meeting_size_used"     => $meeting["meeting_sequences"]["meeting_sequence"]["SIZE"],
                        "has_recorded_minutes"  => $meeting["meeting_sequences"]["meeting_sequence"]["MINUTES"],
                        "has_recorded_video"    => $meeting["meeting_sequences"]["meeting_sequence"]["VIDEO"]
                    );
                }
                $log_data["sequences"] = $sequences;
            }
            $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, $log_data);
            $log_list[] = $log_data;
        } else {
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "meeting" => $log_list,
                    ),
        );
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, $result);
        $output = $this->output($result, $output_type);
        return true;
    }
    /**
     * 会議記録（ログ自体）削除
     *
     * @param string room_key ルームキー
     * @param string meeting_key 会議キー
     * @return boolean
     */
    function action_delete_meetinglog()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        // room_key、meeting_session_id取得
        $room_key           = $this->request->get("room_key");
        $meeting_session_id = $this->request->get("meeting_session_id");
        $provider_id        = $this->config->get("CORE","provider_id");
        $provider_pw        = $this->config->get("CORE","provider_pw");
        $core_url           = $this->config->get("CORE","base_url");

        $meetingLog = new N2MY_MeetingLog($core_url, $provider_id, $provider_pw);
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array(
            "provider_id" => $provider_id,
            "provider_pw" => $provider_pw,
            "meeting_session_id" => $meeting_session_id,
        ));
        if ($room_key && $meeting_session_id) {
            $ret = $meetingLog->delete($room_key, $meeting_session_id);
            $status = 1;
        } else {
            $this->logger->error(__FUNCTION__."#delete meetinglog ERROR!",__FILE__,__LINE__,$room_key);
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "room_key" => $room_key,
                    "meeting_session_id" => $meeting_session_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 会議記録パスワード設定
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @param string meetinglog_password パスワード
     * @return boolean
     */
    function action_set_meetinglog_password()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        // room_key、meeting_session_id取得
        $room_key            = $this->request->get("room_key");
        $meeting_session_id  = $this->request->get("meeting_session_id");
        $log_password        = $this->request->get("log_password");
        $provider_id         = $this->config->get("CORE","provider_id");
        $provider_pw         = $this->config->get("CORE","provider_pw");
        $core_url            = $this->config->get("CORE","base_url");

        $meetingLog = new N2MY_MeetingLog($core_url, $provider_id, $provider_pw);
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array(
            "meeting_session_id" => $meeting_session_id,
            "log_password"       => $log_password,
        ));
        if ($room_key && $meeting_session_id) {
            $meetingLog->setPassword($room_key, $meeting_session_id, $log_password);
            $status = 1;
        } else {
            $this->logger->error(__FUNCTION__."#set meetinglog ERROR!",__FILE__,__LINE__,$room_key);
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "room_key" => $room_key,
                    "meeting_session_id" => $meeting_session_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 会議記録パスワード削除
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_unset_meetinglog_password()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        // room_key、meeting_session_id取得
        $room_key           = $this->request->get("room_key");
        $meeting_session_id = $this->request->get("meeting_session_id");
        $log_password       = $this->request->get("log_password");
        $provider_id        = $this->config->get("CORE","provider_id");
        $provider_pw        = $this->config->get("CORE","provider_pw");
        $core_url           = $this->config->get("CORE","base_url");

        $meetingLog = new N2MY_MeetingLog($core_url, $provider_id, $provider_pw);
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array(
            "meeting_session_id" => $meeting_session_id,
        ));
        if ($room_key && $meeting_session_id) {
            $meetingLog->unsetPassword($room_key, $meeting_session_id);
            $status = 1;
        } else {
            $this->logger->error(__FUNCTION__."#unset meetinglog password ERROR!",__FILE__,__LINE__,$room_key);
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "room_key" => $room_key,
                    "meeting_session_id" => $meeting_session_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 会議記録保護設定
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_set_meetinglog_protect()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        // room_key、meeting_session_id取得
        $room_key           = $this->request->get("room_key");
        $meeting_session_id = $this->request->get("meeting_session_id");
        $log_password       = $this->request->get("log_password");
        $provider_id        = $this->config->get("CORE","provider_id");
        $provider_pw        = $this->config->get("CORE","provider_pw");
        $core_url           = $this->config->get("CORE","base_url");

        $meetingLog = new N2MY_MeetingLog($core_url, $provider_id, $provider_pw);
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array(
            "meeting_session_id" => $meeting_session_id,
            "log_password"       => $log_password,
        ));
        if ($room_key && $meeting_session_id) {
            $meetingLog->setProtect($room_key, $meeting_session_id, $log_password);
            $status = 1;
        } else {
            $this->logger->error(__FUNCTION__."#set meetinglog protect ERROR!",__FILE__,__LINE__,$room_key);
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "room_key" => $room_key,
                    "meeting_session_id" => $meeting_session_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 会議記録保護解除
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_unset_meetinglog_protect()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        // room_key、meeting_session_id取得
        $room_key           = $this->request->get("room_key");
        $meeting_session_id = $this->request->get("meeting_session_id");
        $log_password       = $this->request->get("log_password");
        $provider_id        = $this->config->get("CORE","provider_id");
        $provider_pw        = $this->config->get("CORE","provider_pw");
        $core_url           = $this->config->get("CORE","base_url");

        $meetingLog = new N2MY_MeetingLog($core_url, $provider_id, $provider_pw);
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array(
            "meeting_session_id" => $meeting_session_id,
        ));
        if ($room_key && $meeting_session_id) {
            $meetingLog->unsetProtect($room_key, $meeting_session_id);
            $status = 1;
        } else {
            $this->logger->error(__FUNCTION__."#unset meetinglog protect ERROR!",__FILE__,__LINE__,$room_key);
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "room_key" => $room_key,
                    "meeting_session_id" => $meeting_session_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 会議記録再生（映像）
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_play_logplayer()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        $meeting_key  = $this->request->get("meeting_key");
        $sequence_key = $this->request->get("sequence_key");
        $provider_id  = $this->config->get("CORE","provider_id");
        $provider_pw  = $this->config->get("CORE","provider_pw");
        // 認証処理

        if ($meeting_key && $sequence_key) {
            $param = array(
                "action_set_param"      => "",
                "provider_id"           => $provider_id,
                "provider_password"     => $provider_pw,
                "meeting_ticket"        => $meeting_key,
                "sequence_key"          => $sequence_key,
                "locale"                => $this->session->get("lang"),
                "mode"                  => 'log_video'
                );
            $this->logger->debug(__FUNCTION__."#param", __FILE__, __LINE__, $param);
            $url = $this->get_core_url("/service/meeting_log/play.php", $param);
            $session_id = $this->wget($url);
            $this->logger->trace("session_id",__FILE__,__LINE__,$session_id);
            $session_id = $session_id;
            $param = array(
                "action_play"     => "",
                "session_id"      => $session_id
                );
            $url = $this->get_core_url("/service/meeting_log/play.php", $param);
            $this->logger->info(__FUNCTION__."#url", __FILE__, __LINE__, $url);
            header("Location: ".$url."&cachebuster=".time());
        }
        $result = array(
            "status" => 0,
            "data" => array(
                    "meeting_key"  => $meeting_key,
                    "sequence_key" => $sequence_key,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 会議記録再生（議事録）
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_play_lastlogplayer()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        $meeting_key  = $this->request->get("meeting_key");
        $sequence_key = $this->request->get("sequence_key");
        $provider_id  = $this->config->get("CORE","provider_id");
        $provider_pw  = $this->config->get("CORE","provider_pw");
        // 認証処理

        if ($meeting_key && $sequence_key) {
            $param = array(
                "action_set_param"      => "",
                "provider_id"           => $provider_id,
                "provider_password"     => $provider_pw,
                "meeting_ticket"        => $meeting_key,
                "sequence_key"          => $sequence_key,
                "locale"                => $this->session->get("lang"),
                "mode"                  => 'log_minutes'
                );
            $this->logger->debug(__FUNCTION__."#param", __FILE__, __LINE__, $param);
            $url = $this->get_core_url("/service/meeting_log/play.php", $param);
            $session_id = $this->wget($url);
            $this->logger->trace("session_id",__FILE__,__LINE__,$session_id);
            $session_id = $session_id;
            $param = array(
                "action_play"     => "",
                "session_id"      => $session_id
                );
            $url = $this->get_core_url("/service/meeting_log/play.php", $param);
            $this->logger->info(__FUNCTION__."#url", __FILE__, __LINE__, $url);
            header("Location: ".$url."&cachebuster=".time());
        }
        $result = array(
            "status" => 0,
            "data" => array(
                    "meeting_key"  => $meeting_key,
                    "sequence_key" => $sequence_key,
                    ),
        );
        $output = $this->output($result);
        return true;
    }
}
$main = new N2MY_MeetingLog_API();
$main->execute();