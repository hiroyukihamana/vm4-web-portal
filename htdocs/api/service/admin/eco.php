<?php
require_once("classes/AppFrame.class.php");
require_once("classes/mgm/dbi/meeting_date_log.dbi.php");
require_once("classes/N2MY_DBI.class.php");

class AppBatch extends AppFrame {

    var $core_dsn;
    var $objMeeting;
    var $objDateLog;

    function init() {
        $this->objDateLog = new MeetingDateLogTable( N2MY_MDB_DSN );
    }

    function default_view() {
        $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            $objMeeting = new N2MY_DB( $dsn );
            $date = $this->request->get("date", date("Y-m-d", strtotime("-1 day")));
            $sql = "SELECT DATE_FORMAT(actual_end_datetime, '%Y-%m-%d') as log_date" .
                    ", user.user_id as user_id" .
                    ", room_key" .
                    ", SUM(eco_move) AS eco_move" .
                    ", SUM(eco_fare) AS eco_fare" .
                    ", SUM(eco_co2)  AS eco_co2" .
                    ", SUM(eco_time) AS eco_time" .
                    " FROM meeting, user" .
                    " WHERE actual_end_datetime like '".addslashes($date)."%'" .
                    " AND meeting.user_key = user.user_key".
                    " AND use_flg = 1".
                    " GROUP BY DATE_FORMAT(actual_end_datetime, '%Y-%m-%d')" .
                    ",room_key";
            $rs = $objMeeting->_conn->query($sql);
            if (DB::isError($rs)) {
                $this->logger2->error($rs->getMessage());
                return false;
            } else {
                while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $user_id = $row["user_id"];
                    $room_key = $row["room_key"];
                    $data = $row;
                    $this->logger2->info($data);
                    $this->update($row["log_date"], $user_id, $room_key, $data);
                }
            }
        }
        print "ok";
    }

    /**
     * 日付、ユーザ、部屋毎に記録する
     */
    function update($date, $user_id, $room_key, $data) {
        $where = "log_date = '".addslashes($date)."'" .
                " AND user_id = '".addslashes($user_id)."'" .
                " AND room_key = '".addslashes($room_key)."'";
        // すでにデータが存在する場合
        if ($this->objDateLog->numRows($where) > 0) {
            // 更新
            $this->logger2->debug(array($where, $data));
            $ret = $this->objDateLog->update($data, $where);
        } else {
            // 追加
            $this->logger2->debug(array($where, $data));
            //$data["create_datetime"] = date("Y-m-d H:i:s");
            $ret = $this->objDateLog->add($data);
        }
        if (DB::isError($ret)) {
            $this->logger2->error($ret);
        }
    }

}

$main = new AppBatch;
$main->execute();
?>