<?php
/*
 * Created on 2008/03/04
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");

class N2MY_Admin_Account_API extends N2MY_API
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorizationAdmin();
    }

    /**
     * ユーザーアカウント追加
     *
     * @param
     */
    function action_add_user ()
    {
        require_once("classes/dbi/user.dbi.php");
        $userTable    = new UserTable($this->get_dsn());

        $user_data["reseller_key"]            = $this->request->get("reseller_key");
        $user_data["country_key"]             = $this->request->get("country_key" , "1");
        $user_data["user_id"]                 = $this->request->get("user_id");
        $user_data["user_password"]           = $this->request->get("user_password");
        $user_data["user_admin_password"]     = $this->request->get("user_admin_password");
        $user_data["user_company_name"]       = $this->request->get("user_company_name");
        $user_data["user_company_postnumber"] = $this->request->get("user_company_postnumber", "");
        $user_data["user_company_address"]    = $this->request->get("user_company_address", "");
        $user_data["user_company_phone"]      = $this->request->get("user_company_phone", "");
        $user_data["user_company_fax"]        = $this->request->get("user_company_fax", "");
        $user_data["user_staff_firstname"]    = $this->request->get("user_staff_firstname", "");
        $user_data["user_staff_lastname"]     = $this->request->get("user_staff_lastname", "");
        $user_data["user_staff_email"]        = $this->request->get("user_staff_email", "");
        $user_data["user_status"]             = "1";
        $user_data["user_delete_status"]      = "0";
        $user_data["user_starttime"]          = $this->request->get("user_starttime", date("Y-m-d H:i:s"));
        $user_data["user_registtime"]         = $this->request->get("user_registtime", date("Y-m-d H:i:s"));
        $status = 0;
        $this->logger->info("data",__FILE__,__LINE__,$user_data);
        if ($user_data["user_id"] && $user_data["user_password"] && $user_data["user_admin_password"]) {
            if ($this->_isExist($user_data["user_id"])) {
                $this->logger->error(__FUNCTION__."#already exist!",__FILE__,__LINE__,$user_data["user_id"]);
                $result = array(
                    "status" => 0,
                    "data"   => array(
                            "user_id" => $user_data["user_id"],
                            ),
                );
                $output = $this->output($result);
                return true;
            } else {
                $ret = $userTable->add($user_data);
                $status = 1;
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                    $status = 0;
                }
            }
        } else {
            $ret = "";
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "user_key" => $ret,
                    ),
        );
        $output = $this->output($result);
        return true;

    }

    /**
     * ユーザー情報詳細
     *
     * @param
     */
    function action_get_user_detail ()
    {
        require_once("classes/dbi/user.dbi.php");
        $userTable    = new UserTable($this->get_dsn());

        $user_id = $this->request->get("user_id");

        $where = "user_id = '".mysql_real_escape_string($user_id)."'";
        $ret = $userTable->select($where);
        $user_info = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $status = 1;
        if (!$user_info) {
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "user_info" => $user_info,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * ユーザーアカウント情報変更
     *
     * @param
     */
    function action_update_user ()
    {
        require_once("classes/dbi/user.dbi.php");
        $userTable    = new UserTable($this->get_dsn());

        $user_id = $this->request->get("user_id");

        $where = "user_id = '".mysql_real_escape_string($user_id)."'";
        $ret = $userTable->select($where);
        $user_info = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $status = 0;
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        if ($user_info) {
            if ($this->request->get("user_admin_password")) {
                $user_info["user_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $this->request->get("user_password"));
            }
            if ($this->request->get("user_admin_password")) {
                $user_info["user_admin_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $this->request->get("user_admin_password"));
            }
            $user_data["reseller_key"]            = $this->request->get("reseller_key" , $user_info["reseller_key"]);
            $user_data["country_key"]             = $this->request->get("country_key" , $user_info["country_key"]);
            $user_data["user_password"]           = $user_info["user_password"];
            $user_data["user_admin_password"]     = $user_info["user_admin_password"];
            $user_data["user_company_name"]       = $this->request->get("user_company_name" , $user_info["user_company_name"]);
            $user_data["user_company_postnumber"] = $this->request->get("user_company_postnumber" , $user_info["user_company_postnumber"]);
            $user_data["user_company_address"]    = $this->request->get("user_company_address" , $user_info["user_company_address"]);
            $user_data["user_company_phone"]      = $this->request->get("user_company_phone" , $user_info["user_company_phone"]);
            $user_data["user_company_fax"]        = $this->request->get("user_company_fax" , $user_info["user_company_fax"]);
            $user_data["user_staff_firstname"]    = $this->request->get("user_staff_firstname" , $user_info["user_staff_firstname"]);
            $user_data["user_staff_lastname"]     = $this->request->get("user_staff_lastname" , $user_info["user_staff_lastname"]);
            $user_data["user_staff_email"]        = $this->request->get("user_staff_email" , $user_info["user_staff_email"]);

            $ret = $userTable->update($user_data, $where);
            $status = 1;
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                $status = 0;
            }
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "user_id" => $user_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * ユーザーアカウント停止
     *
     * @param
     */
    function action_delete_user ()
    {
        require_once("classes/dbi/user.dbi.php");
        $userTable    = new UserTable($this->get_dsn());

        $user_id = $this->request->get("user_id");
        if ($this->_isExist($user_id)) {
            $where = "user_id = '".mysql_real_escape_string($user_id)."'";
            $data = array(
                "user_delete_status" => "2",
                "user_deletetime" => date("Y-m-d H:i:s"),
                );
            $ret = $userTable->update($data, $where);
            $status = 1;
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                $status = 0;
            }
        } else {
            $status = 0;
        }
        $result = array(
            "status" => $status,
            "data"   => array(
                    "user_id" => $user_id,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * ミーティングオプション追加
     *
     * @param string room_key 部屋キー
     * @param string service_option_key オプションキー
     * @param string ordered_service_option_status オプションステータス
     * @return string $output  部屋キー、部屋名をxmlで出力
     */
   function action_add_option($header_flg = true)
    {
        require_once("classes/dbi/service_option.dbi.php");
        require_once("classes/dbi/ordered_service_option.dbi.php");
        require_once("classes/dbi/room.dbi.php");

        $roomTable = new RoomTable($this->get_dsn());

        $room_key     = $this->request->get("room_key");
        $service_option_key                = $this->request->get("service_option_key");
        $ordered_service_option_status     = "1";
        $ordered_service_option_registtime = date("Y-m-d H:i:s");
        $status = 1;

        // バリデーションチェック
        $_check_param = array("room_key"                           => $room_key,
                               "service_option_key"                 => $service_option_key,
                               "ordered_service_option_status"      => $ordered_service_option_status,
        );
        $_rules = array(
                "room_key" => array( "required" => true, ),
                "service_option_key" => array( "required" => true, ),
                "ordered_service_option_status" => array( "required" => true, ),
        );
        $validation_obj = new EZValidator($_check_param);
        foreach($_rules as $field => $rules) {
            $validation_obj->check($field, $rules);
        }

        if (EZValidator::isError($validation_obj)) {
            // パラメータ不備
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # option param check error!! ");
            $status = 0;
            $_ordered_service_option_key   = " # option param check error!! ";
        }else{
            // パラメータ正常
            // 部屋が存在するかチェック
            $where = "room_key = '".$room_key."'";
            $room_info = $roomTable->getRow($where);
            if ($room_info) {
                // ミーティングオプション情報登録
                $_ordered_service_option_dto = array();
                $_ordered_service_option_dto = array("ordered_service_option_key"         => "",
                                                     "room_key"                           => $room_key,
                                                     "service_option_key"                 => $service_option_key,
                                                     "ordered_service_option_status"      => $ordered_service_option_status,
                                                     "ordered_service_option_registtime"  => $ordered_service_option_registtime,
                );
                $ordered_service_option   = new OrderedServiceOptionTable($this->get_dsn());
                $_ordered_service_option_key = $ordered_service_option->add($_ordered_service_option_dto);
                if (PEAR::isError($_ordered_service_option_key)) {
                    $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # ordered_service_option insert error!! ");
                    $status = 0;
                    $_ordered_service_option_key   = "";
                }else{
                    $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # ordered_service_option insert success!! ");
                }
            } else {
                $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room not found!! ");
                $status = 0;
                $_ordered_service_option_key   = "";
            }
        }

        $result = array(
            "status" => $status,
            "data" => array(
                    "ordered_service_option_key" => $_ordered_service_option_key,
                    ),
            );
        $output = $this->output($result);
        return true;

    }

    /**
     * ミーティングオプション削除
     *
     * @param string reseller_key リセラーキー
     * @return boolean
     */
    function action_delete_option()
    {
        require_once("classes/dbi/service_option.dbi.php");
        require_once("classes/dbi/ordered_service_option.dbi.php");
        // サービスオプションテータス更新（1 ==> 0）
        $ordered_service_option_key = $this->request->get("ordered_service_option_key");
        $ordered_service_option_deletetime = date("Y-m-d H:i:s");
        $status             = 1;

        // バリデーションチェック
        $_check_param = array("ordered_service_option_key"          => $ordered_service_option_key,
        );
        $_rules = array(
                "ordered_service_option_key" => array( "required" => true, ),
        );
        $validation_obj = new EZValidator($_check_param);
        foreach($_rules as $field => $rules) {
            $validation_obj->check($field, $rules);
        }

        if (EZValidator::isError($validation_obj)) {
            // パラメータ不備
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # option param check error!! ");
            $status = 0;
            $_ordered_service_option_key   = " # option param check error!! ";
        }else{
            // パラメータ正常
            // ordered_service_option
            $_ordered_service_option_dto = array();
            $_ordered_service_option_dto = array(
                                           "ordered_service_option_status"     => "0",
                                           "ordered_service_option_deletetime" => $ordered_service_option_deletetime,
                                           );
            $_where        = " ordered_service_option_key = '". addslashes($ordered_service_option_key). "'";
            // ミーティングオプション情報削除
            $ordered_service_option   = new OrderedServiceOptionTable($this->get_dsn());
            $_message = $ordered_service_option->update($_ordered_service_option_dto, $_where);
            if (PEAR::isError($_message)) {
                $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # reseller_info update error!! ");
                $status = 0;
                $_ordered_service_option_key   = " # reseller_info update error!! ";
            }else{
                $_ordered_service_option_key   = " # ordered_service_option update successful!! ";
            }
        }
        $result = array(
                    "status" => $status,
                    "data" => array(
                        "ordered_service_option_key" => $_ordered_service_option_key
                    ),
                  );
        $output = $this->output($result);
        return true;
    }

    /**
     * 部屋追加（ 部屋情報、部屋プラン情報、部屋座席情報 ）
     *
     * @param boolean $header_flg ヘッダを出力
     * @return string $output  部屋キー、部屋名をxmlで出力
     */
    function action_add_room($header_flg = true)
    {
        require_once("classes/dbi/room.dbi.php");
        require_once("classes/dbi/room_plan.dbi.php");
        require_once("classes/dbi/room_seat.dbi.php");

        // パラメータ受け取り
        $room_key                    = $this->request->get("room_key");
        $user_key                    = $this->request->get("user_key");
        $room_name                   = $this->request->get("room_name");
        $room_status                 = $this->request->get("room_status");
        $service_key                 = $this->request->get("service_key");
        $room_plan_yearly            = $this->request->get("room_plan_yearly");
        if ($room_plan_yearly == "1") {
            $room_plan_yearly_starttime  = $this->request->get("room_plan_starttime");
        }else{
            $room_plan_yearly_starttime  = "0000-00-00 00:00:00";
        }
        $room_plan_starttime         = $this->request->get("room_plan_starttime");
        $room_plan_status            = $this->request->get("room_plan_status");
        $room_seat_max_seat          = $this->request->get("room_seat_max_seat");
        $room_seat_max_audience_seat = $this->request->get("room_seat_max_audience_seat");
        $room_registtime             = date("Y-m-d H:i:s");
        $room_plan_registtime        = date("Y-m-d H:i:s");
        $room_seat_registtime        = date("Y-m-d H:i:s");
        $status = 1;

        // バリデーションチェック
        $_check_param = array("room_key"                    => $room_key,
                              "user_key"                    => $user_key,
                              "room_name"                   => $room_name,
                              "room_status"                 => $room_status,
                              "service_key"                 => $service_key,
                              "room_plan_yearly"            => $room_plan_yearly,
                              "room_plan_starttime"         => $room_plan_starttime,
                              "room_plan_status"            => $room_plan_status,
                              "room_seat_max_seat"          => $room_seat_max_seat,
                              "room_seat_max_audience_seat" => $room_seat_max_audience_seat,
        );
        $_rules = array(
                "room_key"                    => array( "required" => true, ),
                "user_key"                    => array( "required" => true, ),
                "room_name"                   => array( "required" => true, ),
                "room_status"                 => array( "required" => true, ),
                "service_key"                 => array( "required" => true, ),
                "room_plan_yearly"            => array( "required" => true, ),
                "room_plan_starttime"         => array( "required" => true, ),
                "room_plan_status"            => array( "required" => true, ),
                "room_seat_max_seat"          => array( "required" => true, ),
                "room_seat_max_audience_seat" => array( "required" => true, ),
        );
        $validation_obj = new EZValidator($_check_param);
        foreach($_rules as $field => $rules) {
            $validation_obj->check($field, $rules);
        }

        $room = new RoomTable($this->get_dsn());
        $where = "room_key = '".$room_key."'";
        $room_data = $room->getRow($where);

        $_room_key   = "";
        $_room_plan_key = "";
        $_room_seat_key = "";
        if (EZValidator::isError($validation_obj)) {
            // パラメータ不備
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room param check error!! ");
            $_room_key   = $room_key;
            $status = 0;
        } else if ($room_data) {
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room_key already exist!! ");
            $_room_key   = $room_key;
            $status = 0;
        } else {
            // パラメータ正常
            // room
            $_room_dto = array();
            $_room_dto = array(
                               "room_key"           => $room_key,
                               "user_key"           => $user_key,
                               "room_name"          => $room_name,
                               "room_status"        => $room_status,
                               "room_registtime"    => $room_registtime);
            $_room_key = $room->add($_room_dto);
            if (DB::isError($_room_key)) {
                $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room insert error!! ");
                $status = 0;
            }else{
                $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room insert success!! ");
                $_room_key   = $room_key;
                // room_plan
                $_room_plan_dto = array();
                $_room_plan_dto = array("room_plan_key"              => "",
                                        "room_key"                   => $room_key,
                                        "service_key"                => $service_key,
                                        "room_plan_yearly"           => $room_plan_yearly,
                                        "room_plan_yearly_starttime" => $room_plan_yearly_starttime,
                                        "room_plan_starttime"        => $room_plan_starttime,
                                        "room_plan_status"           => $room_plan_status,
                                        "room_plan_registtime"       => $room_plan_registtime,
                );
                $room_plan = new RoomPlanTable($this->get_dsn());
                // room_seat
                $_room_plan_key = $room_plan->add($_room_plan_dto);
                if (DB::isError($_room_plan_key)) {
                    $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room_plan insert error!! ");
                    $status = 0;
                }else{
                    $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room_plan insert success!! ");
                    // room_seat
                    $_room_seat_dto = array();
                    $_room_seat_dto = array("room_seat_key"               => "",
                                            "room_key"                    => $room_key,
                                            "room_seat_max_seat"          => $room_seat_max_seat,
                                            "room_seat_max_audience_seat" => $room_seat_max_audience_seat,
                                            "room_seat_registtime"        => $room_seat_registtime);
                    $room_seat = new RoomSeatTable($this->get_dsn());
                    // create record
                    $_room_seat_key = $room_seat->add($_room_seat_dto);
                    if (DB::isError($_room_seat_key)) {
                        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room_seat insert error!! ");
                        $status = 0;
                    }else{
                        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room_seat insert success!! ");
                    }
                }
            }
        }
        $result = array(
                    "status" => $status,
                    "data"   => array(
                        "room_key"      => $_room_key,
                        "room_plan_key" => $_room_plan_key,
                        "room_seat_key" => $_room_seat_key,
                    ),
                  );
        $output = $this->output($result);
        return true;

    }

    /**
     * 部屋削除
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return boolean
     */
    function action_delete_room()
    {
        require_once("classes/dbi/room.dbi.php");
        // サービスオプションテータス更新（1 ==> 0）
        $room_key           = $this->request->get("room_key");
        $room_deletetime    = date("Y-m-d H:i:s");
        $status             = 1;

        // バリデーションチェック
        $_check_param = array("room_key" => $room_key,
        );
        $_rules = array(
                "room_key" => array( "required" => true, ),
        );
        $validation_obj = new EZValidator($_check_param);
        foreach($_rules as $field => $rules) {
            $validation_obj->check($field, $rules);
        }
        if (EZValidator::isError($validation_obj)) {
            // パラメータ不備
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room param check error!! ");
            $status = 0;
            $_room_key   = "";
        }else{
            // パラメータ正常
            // room
            $_room_dto = array();
            $_room_dto = array("room_status" => "0",
                               "room_deletetime" => $room_deletetime,
            );
            $_where        = " room_key = '". addslashes($room_key). "'";
            // 部屋情報削除
            $room   = new RoomTable($this->get_dsn());
            $_message = $room->update($_room_dto, $_where);
            if (PEAR::isError($_message)) {
                $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # room update error!! ");
                $status = 0;
                $_room_key   = " # room update error!! ";
            }else{
                $_room_key   = " # room update successful!! ";
            }
        }
        $result = array(
                    "status" => $status,
                    "data" => array(
                    "room_key" => $_room_key
                    ),
                  );
        $output = $this->output($result);
        return true;
    }

    /*
     * id からそのユーザーID、メンバーIDが存在するかチェック
     */
    function _isExist($id){
        require_once("classes/dbi/user.dbi.php");
        require_once("classes/dbi/member.dbi.php");

        //member とuser のテーブルをチェック
        $userTable    = new UserTable($this->get_dsn());
        $memberTable  = new MemberTable($this->get_dsn());
        $where1 = "user_id = '".addslashes($id)."'";
        $where2 = "member_id = '".addslashes($id)."'";
        if ($userTable->numRows($where1) > 0 || $memberTable->numRows($where2) > 0){
            return true;
        } else {
            return false;
        }
    }
}
$main = new N2MY_Admin_Account_API();
$main->execute();