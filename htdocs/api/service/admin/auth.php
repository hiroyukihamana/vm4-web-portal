<?php
require_once("classes/N2MY_Api.class.php");

class N2MY_Admin_Auth_API extends N2MY_Api
{
    var $session_id = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    function auth()
    {
    }

    /**
     * ログイン処理
     *
     * @param string id ユーザーID
     * @param string pw ユーザーパスワード
     * @return string $output テンプレートを表示
     */
    function action_login()
    {
//        require_once("classes/N2MY_Auth.class.php");
//        $login = new N2MY_Auth($this->get_dsn());

        $id          = $this->request->get("id");
        $pw          = $this->request->get("pw");
        $enc         = $this->request->get("enc", null);
        $output_type = $this->request->get("output_type");
        $user_type = "";
        // encordタイプに併せてパスワードエンコード
        if ($enc == "md5") {
            $pw = md5($pw);
            $admin_pw = md5($this->config->get("N2MY_API", "admin_login_pw"));
        } else if ($enc == "sha1") {
            $pw = sha1($pw);
            $admin_pw = sha1($this->config->get("N2MY_API", "admin_login_pw"));
        }

        // ユーザ確認
        if (($id == $this->config->get("N2MY_API", "admin_login_id")) &&
            ($pw == $admin_pw)) {
            $status = 1;
            // セッションスタート
            $session = EZSession::getInstance();
            // ID を取得
            $session_id = session_id();
            // ユーザ情報配置
            if  ($output_type) {
                $session->set("output_type", $output_type);
            }
            $session->set("admin_login", "1");
        } else {
            $status = 0;
            $session_id = "";
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "session" => $session_id
                    ),
            );
        $output = $this->output($result);
        return true;
    }

    /**
     * ログアウト処理
     *
     * @param
     * @return
     */
    function action_logout()
    {
        $this->checkAuthorizationAdmin();
        $status = 0;
        if (session_destroy()) {
            $status = 1;
        }
        $result = array(
            "status" => $status,
            );
        $output = $this->output($result);

    }

    function action_show() {

    }
}
$main = new N2MY_Admin_Auth_API();
$main->execute();