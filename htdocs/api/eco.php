<?php
require_once("classes/AppFrame.class.php");
require_once("classes/core/dbi/Meeting.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once("lib/EZLib/EZMail/EZSmtp.class.php");
require_once("lib/EZLib/Transit.class.php");

class TransitTest extends AppFrame {

    var $obj_Transit = null;

    function init() {
    }

    function auth() {
    }

    function default_view() {
        $uri = $this->config->get('N2MY', 'transit_uri');
        if (!$uri) {
            return false;
        }
        $request = $this->request->getAll();
        if (!$request["meeting_key"] || !$request["server_key"]) {
            $this->logger2->warn($request);
            print "Parameter error";
            return false;
        }
        $this->obj_Transit = Transit::factory($uri);
        $exec_time = microtime(true);
        $meeting_key = $request["meeting_key"];
        $station_list = array();

        $dsn = $this->get_dsn( $request["server_key"] );
        $objMeeting = new DBI_Meeting( $dsn );
        $meeting_data = $objMeeting->getInfo($meeting_key);
        $meeting_info = $meeting_data[0];
        if ($meeting_info["meeting_use_minute"] == 0) {
            return false;
        }
        // 参加者一覧を取得
        $objParticipant = new DBI_Participant( $dsn );
        $where = "meeting_key = ".addslashes($meeting_key).
            " AND participant_name IS NOT NULL";
//            " AND use_count > 0";
        $sort = array( "update_datetime"=>"asc");
        $participant_list = $objParticipant->getRowsAssoc($where, $sort, null, 0, "participant_session_id,participant_name,participant_email,participant_station,use_count");
        // 参加者の最後に入室したステータスだけ取得
        foreach ($participant_list as $row) {
            $mail_list[$row["participant_session_id"]] = array(
                "name"      => $row["participant_name"],
                "email"     => $row["participant_email"],
                "station"   => $row["participant_station"],
                "use_count" => $mail_list[$row["participant_session_id"]]["use_count"] + $row["use_count"],
                );
        }
        // 駅一覧取得
        $client_co2 = 0;
        foreach($mail_list as $session_id => $participant_info) {
            if ($participant_info["station"]) {
                $station_list[] = $participant_info["station"];
//                $this->logger2->info(array($session_id, $participant_info["station"], $participant_info["use_count"]));
            }
            $client_co2 += $participant_info["use_count"] * ( defined("ECO_CLIENT_CO2") ? ECO_CLIENT_CO2 : 2.053628 );
        }
        $end_point = $this->request->get("end_point");
        // 目的地が指定されている場合
        $ret = $this->obj_Transit->getMultiStationRoute($station_list, $end_point, "time");
        $short_route = $ret["shortest_route"];
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
            "station_list" => $station_list,
            "mail_list" => $mail_list));
        $short_route["client_co2"] = $client_co2;
        $data = array(
            "eco_move" => $short_route["total"]["move"] * 2,
            "eco_time" => $short_route["total"]["time"] * 2,
            "eco_fare" => $short_route["total"]["fare"] * 2,
            "eco_co2"  => $short_route["total"]["co2"] * 2,
            "eco_info" => serialize($short_route),
            );
        $log = $data;
        unset($log["eco_info"]);
        $data["eco_co2"] = $data["eco_co2"] - $client_co2;
        $this->logger2->debug(array(
            "participant" => $participant_list,
            "transit_result" => $log,
            "client_co2" => $client_co2));
//        $this->logger2->info($data);
        // 統計情報を更新
        $where = "meeting_key = ".addslashes($meeting_key);
        $objMeeting->update($data, $where);
        // リアルタイム集計に変更
        $date = substr($meeting_info["actual_end_datetime"], 0, 10);
        $sql = "SELECT DATE_FORMAT(actual_end_datetime, '%Y-%m-%d') as log_date" .
            ", SUM(eco_move) AS eco_move" .
            ", SUM(eco_fare) AS eco_fare" .
            ", SUM(eco_co2)  AS eco_co2" .
            ", SUM(eco_time) AS eco_time" .
            " FROM meeting" .
            " WHERE room_key = '".$meeting_info["room_key"]."'".
            " AND actual_end_datetime like '".addslashes($date)."%'" .
            " AND use_flg = 1".
            " GROUP BY DATE_FORMAT(actual_end_datetime, '%Y-%m-%d')" .
            ",room_key";
        $eco_data = $objMeeting->_conn->getRow($sql, DB_FETCHMODE_ASSOC);
        $this->logger2->debug(array($sql, $eco_data));
        if (DB::isError($eco_data)) {
            $this->logger2->error($eco_data->getUserInfo());
        } else {
            $sql = "SELECT user_id" .
                    " FROM user, room" .
                    " WHERE user.user_key = room.user_key" .
                    " AND room_key = '".addslashes($meeting_info["room_key"])."'";
            $user_id = $objMeeting->_conn->getOne($sql);
            if (DB::isError($user_id)) {
                $htis->logger2->error($user_id->getUserInfo());
                return false;
            }
            $eco_data["user_id"]    = $user_id;
            $eco_data["log_date"]   = $date;
            $eco_data["room_key"]   = $meeting_info["room_key"];
            if (!$eco_data) {
                $eco_data = array(
                    "eco_move"  => 0,
                    "eco_fare"  => 0,
                    "eco_co2"   => 0,
                    "eco_time"  => 0,
                );
            }
            $where = "log_date = '".addslashes($date)."'" .
                    " AND user_id = '".addslashes($eco_data["user_id"])."'" .
                    " AND room_key = '".addslashes($eco_data["room_key"])."'";
            // すでにデータが存在する場合
            require_once("classes/mgm/dbi/meeting_date_log.dbi.php");
            $objDateLog = new MeetingDateLogTable(N2MY_MDB_DSN);
            if ($objDateLog->numRows($where) > 0) {
                // 更新
                $ret = $objDateLog->update($eco_data, $where);
            } else {
                // 追加
                $ret = $objDateLog->add($eco_data);
            }
            if (DB::isError($ret)) {
                $this->logger2->error(array($eco_data, $ret->getUserInfo()));
            }
        }
        // メール送信
        $report_mail = $this->request->get("mail");  // 特に指定がなければメールを送信する。
        $where = "meeting_key = ".$meeting_key;
        $_meeting_info = $objMeeting->getRowsAssoc($where);
        if ($_meeting_info && $report_mail) {
            $meeting_info = $_meeting_info[0];
            $total_time = number_format($meeting_info["eco_time"] / 60, 1);
            $mail_from = ADMIN_INQUIRY_FROM;
            require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
            $lang = EZLanguage::getLangCd("ja");
            $objSendMail = new EZSmtp(null, $lang, "UTF-8");
            $mail_subject = "ウェブテレビ会議  ECOメーターのお知らせ";
            $routes = $short_route;
            foreach($mail_list as $key => $participant) {
                $name    = $participant["name"];
                $mail_to = $participant["email"];
                $station = $participant["station"];
                if ($station && $mail_to && ($this->_valid_email($mail_to))) {
                    if ($routes["end"] == $station) {
                        $co2  = 0;
                        $time = 0;
                        $fare = 0;
                    } else {
                        $co2  = $routes["start"][$station]["summary"]["move"]["co2"] * 2;
                        $time = number_format($routes["start"][$station]["summary"]["move"]["minute"] * 2 / 60, 1);
                        $fare = $routes["start"][$station]["summary"]["fare"]["fareunit"]["unit"] * 2;
                    }
                    $co2_view = $this->co2_format($co2);
                    $co2_total_view = $this->co2_format($meeting_info["eco_co2"]);
                    $client_co2_view = $this->co2_format($client_co2);

                    $fare_view = number_format($fare);
                    $fare_total_view = number_format($meeting_info["eco_fare"]);

                    $mail_body = <<<EOM
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
ECOレポート
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
ウェブテレビ会議システム "V-CUBE ミーティング" を
ご利用頂きまして誠にありがとうございます。

ECOレポートをお送りいたします。
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
　□ 会議名
　　 {$meeting_info["meeting_name"]}

　□ 開催日時
　　 {$meeting_info["actual_start_datetime"]} (GMT +9)
　　 ～ {$meeting_info["actual_end_datetime"]} (GMT +9)

　□ ECOメーター

　　{$name} 様が、
　　ウェブテレビ会議参加したことにより
　　削減できたCO2、時間、交通費は以下の通りです。


EOM;

                    if ($station) {
                        $mail_body .= <<<EOM
　　▼ {$name} 様 ( {$station} => {$routes["end"]} )
　　・CO2: {$co2_view}
　　・時間: {$time} 時間
　　・交通費: {$fare_view} 円


EOM;
                    } else {
                        $mail_body .= <<<EOM
　　▼ {$name} 様 ( 集合駅: {$routes["end"]} )
　　・CO2: - g
　　・時間: - 時間
　　・交通費: - 円


EOM;
                    }

                    $mail_body .= <<<EOM
　　▼ 会議全体
　　・CO2: {$co2_total_view}
　　・時間: {$total_time} 時間
　　・交通費: {$fare_total_view} 円
─────────────────────────────────────
　□ ECOメーターの設定方法
　　　メインページの個人設定から「現在の最寄駅」を設定してください。
　　　会議終了後にECOレポートを受け取りたい方は、「ECOレポートを
　　　受け取る」にチェックをいれて、送り先のメールアドレスを設定し
　　　てください。
─────────────────────────────────────
CO2の排出量の目安
風呂だき1回　　　　　　約20ｇ
シャワー1分　　　　　　約74ｇ
車のアイドリング5分　　約65ｇ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
EOM;

//　　・会議で消費したCO2: {$client_co2_view}
                    $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array(
                        $mail_from, $mail_to, $mail_subject, $mail_body
                    ));
                    $objSendMail->setFrom(N2MY_MAIL_FROM);
                    $objSendMail->setReturnPath($mail_from);
                    $objSendMail->setTo($mail_to);
                    $objSendMail->setSubject($mail_subject);
                    $objSendMail->setBody($mail_body);
                    $result = $objSendMail->send();
                }
            }
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,microtime(true) - $exec_time);
        }
    }

    function co2_format($co2) {
        $s = array('g', 'Kg', 't', 'kt', 'Mt', 'Gt');
        if ($co2 === 0) {
            $e = 0;
            $co2_view = "0.00 ".$s[0];
        } else {
            if ($co2 > 0) {
                $e = floor(log($co2)/log(1000));
                $co2_view = sprintf('%.2f '.$s[$e], ($co2/pow(1000, floor($e))));
            } else {
                // マイナス
                $co2 = $co2 * -1;
                $e = floor(log($co2)/log(1000));
                $co2_view = sprintf('-%.2f '.$s[$e], ($co2/pow(1000, floor($e))));
            }
        }
        return $co2_view;
    }

    function _valid_email($email) {
        $strict = true;
        $regex = $strict ? '/^([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})$/i' : '/^([*+!.&#$|\'\\%\/0-9a-z^_`{}=?~:-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})$/i';
        if (preg_match($regex, trim($email), $matches)) {
            return array($matches[1], $matches[2]);
        } else {
            return false;
        }
    }

    public function get_dsn( $server_dsn_key )
    {
        static $server_list;
        if (!$server_list) {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        }
        return $server_list["SERVER_LIST"][$server_dsn_key];
    }
}

$main = new TransitTest();
$main->execute();
?>
