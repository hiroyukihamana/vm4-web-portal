<?php
/*
 * Created on 2008/02/06
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");
require_once("classes/N2MY_MeetingLog.class.php");
require_once("config/config.inc.php");
require_once ("lib/EZLib/EZUtil/EZDate.class.php");

class N2MY_MeetingLog_API extends N2MY_Api
{
    var $session_id = null;
    var $meetingLog = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
        $this->meetingLog = new N2MY_MeetingLog($this->get_dsn());
    }

    /**
     * 会議記録一覧（検索）
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param string user_name 参加者名 (デフォルト null)
     * @param datetime start_limit 検索開始日 (デフォルト null)
     * @param datetime end_limit 検索終了日 (デフォルト null)
     * @param int offset 開始件数 (デフォルト null)
     * @param int limit 表示件数 (デフォルト null)
     * @param string sort_key ソート基準 (デフォルト 会議開始時間)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @return $string $output 会議キー、会議名、日時、参加者、サイズ、映像有無、議事録有無
     */
    function action_get_list()
    {
        $room_key        = $this->request->get(API_ROOM_ID);
        $meeting_name    = htmlspecialchars($this->request->get("meeting_name"));
        $user_name       = htmlspecialchars($this->request->get("user_name"));
        $_wk_start_limit = $this->request->get("start_limit");
        $_wk_end_limit   = $this->request->get("end_limit");
        $member_id       = $this->request->get("member_id");
        $offset          = $this->request->get("offset", 0);
        $limit           = $this->request->get("limit", N2MY_API_GET_LIST_LIMIT);
        $sort_key        = $this->request->get("sort_key", "actual_start_datetime");
        $sort_type       = $this->request->get("sort_type");
        $output_type     = $this->request->get("output_type");
        $is_publish      = $this->request->get("is_publish");
        $is_reserved     = $this->request->get("is_reserved");
        $is_locked       = $this->request->get("is_locked");
        $has_minutes     = $this->request->get("has_minutes");
        $has_video       = $this->request->get("has_video");
        $status = 0;
        // タイムスタンプ変換
        $_tz_before = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $_tz_after = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        if (is_numeric($_wk_start_limit)) {
            $_ts_local = EZDate::getLocateTime($_wk_start_limit, $_tz_after, $_tz_before);
            $start_limit = date("Y-m-d H:i:s", $_ts_local);
        } else {
            $start_limit = $_wk_start_limit;
        }
        if (is_numeric($_wk_end_limit)) {
            $_ts_local = EZDate::getLocateTime($_wk_end_limit, $_tz_after, $_tz_before);
            $end_limit = date("Y-m-d H:i:s", $_ts_local);
        } else {
            $end_limit = $_wk_end_limit;
        }
        //
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "allow" => array_keys($room_info),
                ),
//            "title"     => array(
//                "maxlen"   => 64,
//                ),
//            "participant"     => array(
//                "maxlen"   => 64,
//                ),
            "start_datetime"     => array(
                "datetime"   => true,
                ),
            "end_datetime"     => array(
                "datetime"   => true,
                ),
            "limit"     => array(
                "numeric"   => true,
                ),
            "offset"     => array(
                "numeric"   => true,
                ),
            "sort_type" => array(
                "allow" => array("meeting_name", "meeting_size_used", "meeting_start_date","meeting_end_date","meeting_use_minute")
                ),
            "sort_type" => array(
                "allow" => array("asc", "desc")
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェックデータ編集
        $chack_data = array(
            "room_key"         => $room_key,
//            "title"             => $meeting_name,
//            "participant"       => $user_name,
            "start_datetime"    => $start_limit,
            "end_datetime"      => $end_limit,
            "limit"             => $limit,
            "offset"            => $offset,
            "sort_key"          => $sort_key,
            "sort_type"         => $sort_type,
            "output_type"       => $output_type,
        );
        $this->logger2->info($chack_data);
        // チェック
        $err_obj = $this->error_check($chack_data, $rules);
        if ($member_id) {
            require_once('classes/dbi/member.dbi.php');
            $objMember = new MemberTable($this->get_dsn());
            $where = "user_key = ".$user_info["user_key"].
                " AND member_status = 0" .
                " AND member_id = '".addslashes($member_id)."'";
            $this->logger2->info($where);
            if (!$member_key = $objMember->getOne($where, 'member_key')) {
                $err_obj->set_error("member_id", "230100");
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        if ($limit === "0") {
            $this->logger2->info($limit);
            $limit = null;
            $offset = null;
        }
        switch ($sort_key) {
            case "meeting_start_date":
                $sort_key = "actual_start_datetime";
                break;
            case "meeting_end_date":
                $sort_key = "actual_end_datetime";
                break;
        }
        $query = array(
            "user_key"          => $user_info["user_key"],
            "room_key"          => $room_key,
            "member_key"        => $member_key,
            "title"             => $meeting_name,
            "participant"       => $user_name,
            "start_datetime"    => $start_limit,
            "end_datetime"      => $end_limit,
            "limit"             => $limit,
            "offset"            => $offset,
            "sort_key"          => $sort_key,
            "sort_type"         => $sort_type,
            "is_publish"        => $is_publish,
            "is_reserved"       => $is_reserved,
            "is_locked"         => $is_locked,
            "has_minutes"       => $has_minutes,
            "has_video"         => $has_video,
        );
        $list = $this->meetingLog->getList($query);

        foreach ($list as $_key => $_val) {
            require_once("classes/dbi/fms_watch_status.dbi.php");
            $objFmsWatch  = new FmsWatchTable($this->get_dsn());
            $where        = sprintf("meeting_key=%d", $_val["meeting_key"]);
            $fmsWatchInfo = $objFmsWatch->getRow($where);
            $lists[$_key] = array(
                API_ROOM_ID                => $_val['room_key'],
                API_MEETING_ID             => $_val['meeting_session_id'],
                'meeting_name'             => $_val['meeting_name'],
                'meeting_size_used'        => $_val['meeting_size_used'],
                'meeting_log_password'     => $_val['meeting_log_password'] = $_val['meeting_log_password'] ? 1 : 0,
                'is_locked'                => $_val['is_locked'],
                'is_reserved'              => $_val['is_reserved'],
                'is_publish'               => $_val['is_publish'],
                API_MEETINGLOG_MINUTES     => $_val['has_recorded_minutes'],
                API_MEETINGLOG_VIDEO       => $_val['has_recorded_video'],
                'meeting_start_date'       => strtotime($_val['actual_start_datetime']),
                'meeting_end_date'         => strtotime($_val['actual_end_datetime']),
                'meeting_use_minute'       => $_val['meeting_use_minute'],
                'eco_move'                 => $_val['eco_move'],
                'eco_time'                 => $_val['eco_time'],
                'eco_fare'                 => $_val['eco_fare'],
                'eco_co2'                  => $_val['eco_co2'],
                'now_creating'              => ($fmsWatchInfo["status"] == "1") ? "1" : "0",
            );
            // 参加者情報
            foreach ($_val['participants'] as $_key_p => $_val_p) {
                $lists[$_key]['participants']['participant'][$_key_p] = array(
                    "participant_id"            => $_val_p["participant_session_id"],
                    "participant_name"          => $_val_p["participant_name"],
                    "participant_station"       => $_val_p["participant_station"],
                    "participant_start_date"    => ($_val_p["uptime_start"]) ? strtotime($_val_p["uptime_start"]) : "",
                    "participant_end_date"      => ($_val_p["uptime_end"]) ? strtotime($_val_p["uptime_end"]) : "",
                    "participant_type"          => $_val_p["participant_type_name"],
                    "participant_mode"          => $_val_p["participant_mode_name"],
                    "member_id"                 => $this->_get_member_id($user_info["user_key"], $_val_p["member_key"]),
                    "use_count"                 => $_val_p["use_count"],
                );
            }
            // meeting_sequences
            foreach ($_val['meeting_sequences'] as $_key_ms => $val_ms) {
                $lists[$_key]['meeting_sequences']['meeting_sequence'][$_key_ms] = array(
                    'meeting_sequence_key'    => $val_ms['meeting_sequence_key'],
                    'meeting_size_used'       => $val_ms['meeting_size_used'],
                    API_MEETINGLOG_MINUTES    => $val_ms['has_recorded_minutes'],
                    API_MEETINGLOG_VIDEO      => $val_ms['has_recorded_video']
                );
            }
        }

        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $summary = $obj_CoreMeeting->getSummary($query);
        $count = $summary["meeting_count"];
//        $count = $this->meetingLog->getCount($query);
        $this->logger2->info($count);
        $data = array(
            "count" => $count,
            "meetinglogs"  => array("meetinglog" => $lists),
            );
        return $this->output($data);
    }

    function action_get_info()
    {
        $room_key        = $this->request->get(API_ROOM_ID);
        $meeting_name    = htmlspecialchars($this->request->get("meeting_name"));
        $user_name       = htmlspecialchars($this->request->get("user_name"));
        $_wk_start_limit = $this->request->get("start_limit");
        $_wk_end_limit   = $this->request->get("end_limit");
        $member_id       = $this->request->get("member_id");
        $output_type     = $this->request->get("output_type");
        $is_publish      = $this->request->get("is_publish");
        $is_reserved     = $this->request->get("is_reserved");
        $is_locked       = $this->request->get("is_locked");
        $has_minutes     = $this->request->get("has_minutes");
        $has_video       = $this->request->get("has_video");
        $status = 0;
        // タイムスタンプ変換
        $_tz_before = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $_tz_after = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        if (is_numeric($_wk_start_limit)) {
            $_ts_local = EZDate::getLocateTime($_wk_start_limit, $_tz_after, $_tz_before);
            $start_limit = date("Y-m-d H:i:s", $_ts_local);
        } else {
            $start_limit = $_wk_start_limit;
        }
        if (is_numeric($_wk_end_limit)) {
            $_ts_local = EZDate::getLocateTime($_wk_end_limit, $_tz_after, $_tz_before);
            $end_limit = date("Y-m-d H:i:s", $_ts_local);
        } else {
            $end_limit = $_wk_end_limit;
        }
        //
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "allow" => array_keys($room_info),
                ),
//            "title"     => array(
//                "maxlen"   => 64,
//                ),
//            "participant"     => array(
//                "maxlen"   => 64,
//                ),
            "start_datetime"     => array(
                "datetime"   => true,
                ),
            "end_datetime"     => array(
                "datetime"   => true,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェックデータ編集
        $chack_data = array(
            "room_key"         => $room_key,
//            "title"             => $meeting_name,
//            "participant"       => $user_name,
            "start_datetime"    => $start_limit,
            "end_datetime"      => $end_limit,
            "output_type"       => $output_type,
        );
        $this->logger2->info($chack_data);
        // チェック
        $err_obj = $this->error_check($chack_data, $rules);
        if ($member_id) {
            require_once('classes/dbi/member.dbi.php');
            $objMember = new MemberTable($this->get_dsn());
            $where = "user_key = ".$user_info["user_key"].
                " AND member_status = 0" .
                " AND member_id = '".addslashes($member_id)."'";
            $this->logger2->info($where);
            if (!$member_key = $objMember->getOne($where, 'member_key')) {
                $err_obj->set_error("member_id", "230200");
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $query = array(
            "user_key"          => $user_info["user_key"],
            "room_key"          => $room_key,
            "member_key"        => $member_key,
            "title"             => $meeting_name,
            "participant"       => $user_name,
            "start_datetime"    => $start_limit,
            "end_datetime"      => $end_limit,
            "is_publish"        => $is_publish,
            "is_reserved"       => $is_reserved,
            "is_locked"         => $is_locked,
            "has_minutes"       => $has_minutes,
            "has_video"         => $has_video,
        );
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $summary = $obj_CoreMeeting->getSummary($query);
        $count = $summary["meeting_count"];
        $data = array(
            "count"         => $count,
            "total_time"    => $summary["total_use_time"],
            "total_size"    => $summary["total_size_used"],
            );
        return $this->output($data);
    }

    function action_get_detail() {
        require_once("classes/dbi/file_cabinet.dbi.php");
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $obj_Meeting = new MeetingTable( $this->get_dsn() );
        $objParticipant = new DBI_Participant( $this->get_dsn() );
        $objFileCabinet = new FileCabinetTable( $this->get_dsn() );
        $objMeetingSequence = new DBI_MeetingSequence( $this->get_dsn() );

        $meeting_session_id = $this->request->get(API_MEETING_ID);
        $meeting_pw = $this->request->get("password");
        $user_info = $this->session->get("user_info");
        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        $where = "meeting_session_id = '".addslashes($meeting_session_id)."'";
        if (!$meetingInfo = $obj_Meeting->getRow($where)) {
            $err_obj->set_error(API_MEETING_ID, "230002");
        } else {
            // ログインユーザ以外の予約
            if ($meetingInfo["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_MEETING_ID, "230002");
            }
        }
        // 映像、議事録再生時にパスワードが聞かれるのでもんだない？
        if ($meetingInfo["meeting_log_password"]) {
            if($meeting_pw != $meetingInfo["meeting_log_password"]) {
                $err_obj->set_error("password", "equal");
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
//        $meetingInfo["meeting_size_used"] = round($meetingInfo["meeting_size_used"] / (1024 * 1024));
        $meetingInfo["participantList"] = $objParticipant->getList($meetingInfo["meeting_key"], false);
        $_server_time_zone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE);
        $meeting_info = array(
            API_ROOM_ID                 => $meetingInfo["room_key"],
            API_MEETING_ID              => $meetingInfo["meeting_session_id"],
            'meeting_name'              => $meetingInfo["meeting_name"],
            'meeting_size_used'         => round($meetingInfo["meeting_size_used"] / (1024 * 1024)),
            'meeting_log_password'      => $meetingInfo["meeting_log_password"] ? 1 : 0 ,
            'is_locked'                 => $meetingInfo["is_locked"],
            'is_reserved'               => $meetingInfo["is_reserved"],
            'is_publish'                => $meetingInfo["is_publish"],
            API_MEETINGLOG_MINUTES      => $meetingInfo["has_recorded_minutes"],
            API_MEETINGLOG_VIDEO        => $meetingInfo["has_recorded_video"],
            'meeting_start_date'        => strtotime($meetingInfo["actual_start_datetime"]),
            'meeting_end_date'          => strtotime($meetingInfo["actual_end_datetime"]),
            'meeting_use_minute'        => $meetingInfo["meeting_use_minute"],
            'eco_move'                  => $meetingInfo["eco_move"],
            'eco_time'                  => $meetingInfo["eco_time"],
            'eco_fare'                  => $meetingInfo["eco_fare"],
            'eco_co2'                   => $meetingInfo["eco_co2"]
        );
        foreach($meetingInfo["participantList"] as $p_key => $participant) {
             $meeting_info["participants"]["participant"][$p_key] = array(
                API_PARTICIPANT_ID           => $participant["participant_session_id"],
                'participant_name'           => $participant["participant_name"],
                'participant_email'          => $participant["participant_email"],
                'participant_station'        => $participant["participant_station"],
                'use_count'                  => $participant["use_count"],
                'mode'                       => $participant["participant_mode_name"],
                'type'                       => $participant["participant_type_name"],
                API_PARTICIPANT_START        => strtotime($participant["uptime_start"]),
                API_PARTICIPANT_END          => strtotime($participant["uptime_end"])
            );
        }
        unset($meetingInfo["participantList"]);
        //sequeceList
        $where = "meeting_key = ".$meetingInfo["meeting_key"];
        $sequeceList = $objMeetingSequence->getRowsAssoc($where, array("meeting_sequence_key"=>"desc") );

        foreach ($sequeceList as $i => $sequece) {
            $videoURL = NULL;
            $minuteURL = NULL;
            if ($meetingInfo["is_publish"]) {
                if ($sequeceList[$i]["has_recorded_video"]) {
                    $data = sprintf("%s,log_video,%s,%s", $meetingInfo["meeting_key"], $this->session->get("lang"), $sequece["meeting_sequence_key"]);
                    $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $data);
                    $encrypted_data = str_replace("/", "-", $encrypted_data);
                    $encrypted_data = str_replace("+", "_", $encrypted_data);
                    $videoURL = sprintf( "%sondemand/%s", N2MY_BASE_URL, $encrypted_data);
                }
                if ($sequeceList[$i]["has_recorded_minutes"]) {
                    $data = sprintf("%s,log_minute,%s,%s", $meetingInfo["meeting_key"], $this->session->get("lang"), $sequece["meeting_sequence_key"]);
                    $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $data);
                    $encrypted_data = str_replace("/", "-", $encrypted_data);
                    $encrypted_data = str_replace("+", "_", $encrypted_data);
                    $minuteURL = sprintf( "%sminute/%s", N2MY_BASE_URL, $encrypted_data);
                }
            }
            $meeting_info["meeting_sequences"]["meeting_sequence"][$i] = array(
                'meeting_sequence_key'  => $sequeceList[$i]["meeting_sequence_key"],
                'meeting_size_used'     => $sequeceList[$i]["meeting_size_used"],
                'video_url'             => $videoURL,
                'minute_url'            => $minuteURL,
                API_MEETINGLOG_MINUTES   => $sequeceList[$i]["has_recorded_minutes"],
                API_MEETINGLOG_VIDEO     => $sequeceList[$i]["has_recorded_video"]
             );
        }
        //cabinet
        $where = "meeting_key = '".$meetingInfo["meeting_key"]."'" .
            " AND status = 1";
        $cabinets = $objFileCabinet->getRowsAssoc( $where, array( "cabinet_id"=>"asc" ) );
        foreach($cabinets as $c_key => $val) {
            $meeting_info["cabinets"]["cabinet"][$c_key] = array(
                'cabinet_id'    => $cabinets[$c_key]["cabinet_key"],
                'size'          => $cabinets[$c_key]["size"],
                'file_name'     => $cabinets[$c_key]["file_name"]
             );
        }
        return $this->output($meeting_info);
    }

    /**
     * 会議記録再生（映像）
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_video_player() {
        require_once("classes/N2MY_MeetingLog.class.php");
        $objMeeting         = new MeetingTable($this->get_dsn());
        $objMeetingSequence = new DBI_MeetingSequence($this->get_dsn());
        $meeting_key  = $this->request->get(API_MEETING_ID);
        $sequence_key = $this->request->get("sequence_id");
        $lang         = $this->request->get("lang", $this->session->get("lang"));
        $where = "meeting_session_id = '".addslashes($meeting_key)."'";
        if (!$meeting_info = $objMeeting->getRow($where)) {
            return $this->display_error("100", "PARAMETER_ERROR");
        }
        $where = "meeting_key = '".$meeting_info['meeting_key']."'".
            " AND meeting_sequence_key = '".addslashes($sequence_key)."'" .
            " AND has_recorded_video = 1";
        if (!$meeting_sequence_info = $objMeetingSequence->getRow($where)) {
            $this->logger2->info($where);
            return $this->display_error("100", "PARAMETER_ERROR");
        }
        $one_time_key = uniqid();
        $info = array(
            'limit'         => time() + 60,
            "id"            => $meeting_info['meeting_ticket'],
            "sequence_key"  => $sequence_key,
            "locale"        => $lang,
            "mode"          => 'log_video'
        );
        $this->session->set($one_time_key, $info);
        $param = array(
            'action_log_player' => '',
            N2MY_SESSION => session_id(),
            'id' => $one_time_key,
        );
        $url = $this->get_core_url($_SERVER['SCRIPT_NAME'], $param);
        $data = array(
            "url"  => $url,
            );
        $this->logger2->info(array($info, $data));
        $this->add_operation_log('meetinglog_play_video', array(
            'room_key'      => $meeting_info['room_key'],
            'meeting_name'  => $meeting_info['meeting_name']
        ));
        return $this->output($data);
    }

    /**
     * 会議記録再生（議事録）
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_minute_player() {
        require_once("classes/N2MY_MeetingLog.class.php");
        $objMeeting         = new MeetingTable($this->get_dsn());
        $objMeetingSequence = new DBI_MeetingSequence($this->get_dsn());
        $meeting_key  = $this->request->get(API_MEETING_ID);
        $lang         = $this->request->get("lang", $this->session->get("lang"));
        $sequence_key = $this->request->get("sequence_id");
        $where = "meeting_session_id = '".addslashes($meeting_key)."'";
        if (!$meeting_info = $objMeeting->getRow($where)) {
            return $this->display_error("100", "PARAMETER_ERROR");
        }
        $where = "meeting_key = '".$meeting_info['meeting_key']."'".
            " AND meeting_sequence_key = '".addslashes($sequence_key)."'" .
            " AND has_recorded_minutes = 1";
        if (!$meeting_sequence_info = $objMeetingSequence->getRow($where)) {
            $this->logger2->info($where);
            return $this->display_error("100", "PARAMETER_ERROR");
        }
        $redirect_param = array(
            );
        $one_time_key = uniqid();
        $info = array(
            'limit'         => time() + 300,
            "id"            => $meeting_info['meeting_ticket'],
            "sequence_key"  => $sequence_key,
            "locale"        => $lang,
            "mode"          => 'log_minute'
        );
        $this->session->set($one_time_key, $info);
        $param = array(
            'action_log_player' => '',
            N2MY_SESSION => session_id(),
            'id' => $one_time_key,
        );
        $url = $this->get_core_url($_SERVER['SCRIPT_NAME'], $param);
        $data = array(
            "url"  => $url,
            );
        $this->logger2->info(array($info, $data));
        $this->add_operation_log('meetinglog_play_minutes', array(
            'room_key'      => $meeting_info['room_key'],
            'meeting_name'  => $meeting_info['meeting_name']
        ));
        return $this->output($data);
    }

    function action_log_player() {
        require_once ("lib/EZLib/EZHTML/EZTemplate.class.php");
        $this->template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
        $id = $this->request->get('id');
        $param = $this->session->get($id);
        $this->logger2->info($param);
        if ($param && time() < $param['limit']) {
            $request = $param;
            $obj_Meeting  = new DBI_Meeting( $this->get_dsn() );
            $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_ticket='%s' AND is_deleted=0", $request["id"] ) );
            $member_info = $this->session->get( "member_info" );
            $user_info = $this->session->get( "user_info");
            if ( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") && $member_info["room_key"] != $meeting_info["room_key"] ) {
                $this->session->set( "display_deleteButton", false );
            } else {
                $this->session->set( "display_deleteButton", true );
            }
            //set parameters into session
            if ( ! $meeting_info ) {
                $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$meeting_info);
                $this->_error = "MEETING_TICKET_ERROR";
            }
            $this->session->remove("ondemand");
            $this->session->set( "meeting_key", $meeting_info["meeting_key"] );
            $this->session->set( "sequence_key", $request["sequence_key"] );
            $this->session->set( "log_meeting_key", $meeting_info["meeting_key"] );
            $this->session->set( "log_sequence_key", $request["sequence_key"] );
            $this->session->set( "log_type", $request["mode"] );
            // 録画GW情報取得
            require_once 'classes/core/dbi/MeetingSequence.dbi.php';
            $objMeetingSequence = new DBI_MeetingSequence( $this->get_dsn() );
            $where = "meeting_sequence_key = '".addslashes($request["sequence_key"])."'";
            if ($meeting_sequence_info = $objMeetingSequence->getRow($where)) {
                $this->session->set("display_deleteButton", ($meeting_sequence_info["record_status"] != "convert") ? true : false);
            }
            //assign values
            $this->template->assign('charset', $this->config->get( "GLOBAL", "html_output_char" ) );
            $this->template->assign('locale' , $this->get_language() );
            // service info
            $frame["service_info"] = $this->get_message("SERVICE_INFO");
            $this->template->assign("__frame", $frame);
            $this->template->assign("api_url", N2MY_BASE_URL);
            $this->template->assign("session_id", session_id());

            //display template
            $log_fl_ver = $this->request->get("log_fl_ver");
            $this->session->set("log_fl_ver", $log_fl_ver);
            //display template
            $log_fl_ver = $this->request->get("log_fl_ver");
            $this->session->set("log_fl_ver", $log_fl_ver);
            switch ( $request["mode"] ) {
                case 'log_video':
                    if ($log_fl_ver == "as2") {
                        $this->display( "core/meeting_log/video_base.t.html", 'common');
                    } else {
                        $this->display( "core/meeting_log/video_base_as3.t.html", 'common');
                    }
                    break;
                default:
                    $this->display( "core/meeting_log/minutes_base.t.html", 'common');
                    break;
            }
        } else {
            $message["title"] = '会議ログ再生エラー';
            $message["body"] = '会議ログの再生有効期限を過ぎています';
            $this->session->remove($id);
            $this->template->assign('message', $message);
            return $this->display('common.t.html', 'common');
        }
    }

    function action_eco_update() {
        if ($dsn_key = $this->request->get("serverDsnKey", N2MY_DEFAULT_DB)) {
            $this->_dsn = $this->get_dsn_value($dsn_key);
        } else {
            $this->_dsn = $this->get_dsn();
        }
        $this->obj_Eco = new N2MY_Eco( $this->get_dsn(), N2MY_MDB_DSN );
        require_once('lib/EZLib/Transit.class.php');
        $uri = $this->config->get("N2MY", "transit_uri");
        $transit_log = realpath(dirname(__FILE__)."/../../../var/")."/transit.log";
        $Transit = Transit::factory( $uri, $transit_log );
        $meeting_id = $this->request->get(API_MEETING_ID);
        $station_list   = $this->request->get("station");
        $endpoint       = $this->request->get("endpoint");
        $participants   = $this->request->get("participants");
        foreach($participants as $key => $participant) {
            $participants[$key] = $participant;
            $participants[$key]["participant_station"] = $station_list[$participant["participant_key"]];
        }
        $this->request->set("participants", $participants);
        $this->request->set("end_point", $endpoint);
        // 入力チェック
        $err = array();
        foreach($station_list as $key => $station) {
            if (!$station) {
                if ($endpoint == $key) {
                    $err[$key] = true;
                }
            } else {
                $ret = $Transit->getStationList($station, false, 50);
                if (!in_array($station, $ret)) {
                    $err[$key] = true;
                }
            }
        }
        if ($err) {
            return $this->render_eco_edit_form($err);
        }
        // 参加者の拠点情報を更新
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $objParticipant = new DBI_Participant($this->get_dsn());
        $where = "meeting_session_id = '".addslashes($meeting_id)."'";
        $meeting_info = $obj_Meeting->getRow($where);
        $this->logger2->info($station_list);
        if (!DB::isError($meeting_info)) {
            foreach($station_list as $participant_key => $station) {
                $where = "meeting_key = ".$meeting_info["meeting_key"].
                    " AND participant_key = '".addslashes($participant_key)."'";
                $data = array(
                    "participant_station" => $station,
                    "update_datetime"     => date("Y-m-d H:i:s"),
                );
                $objParticipant->update($data, $where);
            }
            // 経路検索結果取得
            $this->obj_Eco->createReport($meeting_info["meeting_key"], $station_list[$endpoint], false, false);
        }
        // 結果画面表示
        $this->action_meeting_log();
    }

}
$main = new N2MY_MeetingLog_API();
$main->execute();