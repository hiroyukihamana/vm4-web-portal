<?php
/*
 * Created on 2008/01/31
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");

class N2MY_Meeting_API extends N2MY_Api
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        $this->_name_space = md5(__FILE__);
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        if ("action_invite_start" != $this->getActionName()) {
            $this->checkAuthorization();
        }
    }

    function action_invite_start() {
        require_once 'classes/mgm/MGM_Auth.class.php';
        require_once 'classes/N2MY_Auth.class.php';
        require_once 'classes/N2MY_Account.class.php';
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        require_once 'classes/N2MY_Meeting.class.php';
        // DB選択
        $request = $this->request->getAll();
        $meeting_id  = $request[API_MEETING_ID];
        $output_type = $this->request->get("output_type");
        // パラメタチェック
        $rules = array(
            API_MEETING_ID => array(
                "required" => true,
                "valid_integer" => true,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $dsn_info = $obj_MGMClass->getRelationDsn($meeting_id, "meeting");
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "ミーティングキーが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "meeting_session_id = '".addslashes($meeting_id)."'" .
            " AND is_active = 1" .
            " AND is_deleted = 0";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "ミーティングキーが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        if ($meeting_info["is_reserved"] &&
            ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
             (strtotime($meeting_info['meeting_stop_datetime'])) < time())
            )
        ) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "予約時間内ではありません",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // ユーザー
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$meeting_info["user_key"];
        $user_info = $objUser->getRow($where);
        if (!$user_info) {
            return $this->display_error("101", "User not found", $this->get_error_info($err_obj));
        }
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $user_info["user_id"];
        $pw          = $user_info["user_password"];
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $enc         = $this->request->get("enc", null);
        $rules = array(
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
                ),
            API_TIME_ZONE     => array(
                "allow" => array_keys($this->get_timezone_list()),
                ),
        );
        $request = $this->request->getAll();
        $err_obj = $this->error_check($request, $rules);
//        if (!$user_info = $obj_MGMClass->getUserInfoById( $id ) ){
//            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
//        } else {
//            if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
//                $err_obj->set_error("", "SELECT_USER_ERROR", $id);
//                $this->display_error("1", "SELECT_SERVER_ERROR");
//            }
//        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $login = new N2MY_Auth($server_info["dsn"]);
            $user_type = "";
            // ユーザ確認
            $login_info = $login->check($id, $pw, $enc);
            if (!$login_info) {
                return $this->display_error("1", "USER_ID_PASS_INVALID");
            } else {
                // セッションスタート
                $session = EZSession::getInstance();
                // ID を取得
                $session_id = session_id();
                // ユーザ情報配置
                if  ($output_type) {
                    $session->set("output_type", $output_type);
                }
                $session->set("login", "1");
                $session->set("lang", $lang);
                $session->set("country_key", $country);
                $session->set("time_zone", $time_zone);
                $session->set("dsn_key", $server_info["host_name"]);
                $session->set("server_info", $server_info);
                // ユーザー情報
                $user_info = $login_info["user_info"];
                $session->set("user_info", $login_info["user_info"]);
                $type = "user";
                // 使用可能な部屋
                $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
                $rooms = $obj_N2MY_Account->getOwnRoomByRoomKey( $meeting_info["room_key"], $user_info["user_key"]);
                // 既存のセッションデータにも書き込む
                $session->set('room_info', $rooms);
                // ユーザごとのオプションを取得
                $type        = $this->request->get("type", "normal");
                $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
                // ホワイトボードアップロードメールアドレス
                $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $meeting_info["room_key"]."@".$this->config->get("N2MY", "mail_wb_host") : "";
                $user_options = array(
                    "user_key"              => $user_info["user_key"],
                    "name"                  => $this->request->get("name"),
                    "narrow"                => $this->request->get("is_narrow"),
                    "lang"                  => $this->request->get("lang"),
                    "skin_type"             => $this->request->get("skin_type", ""),
                    "mode"                  => $mode,
                    "role"                  => $this->request->get("role"),
                    "mail_upload_address"   => $mail_upload_address,
                    "account_model"         => $user_info["account_model"],
                    );
                // 会議入室
                $obj_N2MY_Meeting = new N2MY_Meeting($server_info["dsn"]);
                $meetingDetail = $obj_N2MY_Meeting->startMeeting($meeting_id, $type, $user_options);
                //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
                $session->set("room_key", $meeting_info["room_key"] );
                $session->set("type", $meetingDetail["participant_type_name"] );
                $session->set("meeting_key", $meetingDetail["meeting_key"] );
                $session->set("participant_key", $meetingDetail["participant_key"] );
                $session->set("participant_name", $this->request->get("name"));
                $session->set("mail_upload_address", $mail_upload_address );
                $session->set("fl_ver", $this->request->get("flash_version", "as2"));
                $session->set("reload_type", 'normal');
                $session->set( "meeting_version", $user_info["meeting_version"] );
                // 会議入室
                $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESSION."=".session_id();
                if ($meeting_info["meeting_password"]) {
                    $this->session->set("redirect_url", $url, $this->_name_space);
                    $url = N2MY_BASE_URL."services/" .
                            "?action_login=" .
                            "&".N2MY_SESSION."=".session_id().
                            "&reservation_session=".$meeting_info["reservation_session"].
                            "&ns=".$this->_name_space;
                }
                $data = array(
                    "url"           => $url,
                    API_MEETING_ID  => $meeting_id,
                    "meeting_name"  => $meetingDetail["meeting_name"],
                );
                return $this->output($data);
            }
        }
    }

    /**
     * 会議キー取得
     *
     * @param boolean $header_flg ヘッダを出力
     * @return string $output  会議キー、会議名をxmlで出力
     */
    function action_create()
    {
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        require_once( "classes/N2MY_Meeting.class.php" );
        $obj_N2MYMeeting = new N2MY_Meeting( $this->get_dsn() );
        $room_key = $this->request->get(API_ROOM_ID);
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        // チェックルール編集
        $rules = array(
            API_ROOM_ID     => array(
                "required" => true,
                "allow"    => array_keys($room_info),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            API_ROOM_ID    => $room_key,
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $now_meeting = $obj_N2MYMeeting->getNowMeeting( $room_key );
        $meeting_key = $now_meeting["meeting_key"];
        $meeting_name = $now_meeting["meeting_name"];
        $meeting_password = $now_meeting["meeting_password"];
        // センタサーバ指定
        $country_id = "jp";
        if (isset($extension["locale"])) {
            switch ($extension["locale"]) {
            case "jp" :
            case "us" :
            case "cn" :
                $country_id = $extension["locale"];
                break;
            }
        }
        // オプション指定
        $option = array(
            "user_key"     => $user_info["user_key"],
            "meeting_name" => $meeting_name,
            "start_time"   => $now_meeting["start_time"],
            "end_time"     => $now_meeting["end_time"],
            "country_id"   => $country_id,
        );
        $core_meeting_key = $obj_N2MYMeeting->createMeeting($room_key, $meeting_key, $option);
        $data = array(
            API_MEETING_ID  => $this->ticket_to_session($now_meeting["meeting_key"]),
            "meeting_name" => $meeting_name,
        );
        // TODO : 作成できたか出来ないかのチェックは不要？
        return $this->output($data);
    }

    /**
     * 会議開始（会議を作成して、そのまま開始）
     *
     * @param
     * @return 会議データを作成し、リダイレクトで会議室に入室
     */
    function action_start() {
        require_once("classes/N2MY_Meeting.class.php");
        require_once("classes/dbi/room.dbi.php");
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $objRoom = new RoomTable($this->get_dsn());
        if ($meeting_ticket = $this->session->get('invited_meeting_ticket')) {
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND meeting_ticket = '".addslashes($meeting_ticket)."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
            $meeting_key = $obj_Meeting->getOne($where, 'meeting_session_id');
        } else {
            $meeting_key = $this->request->get(API_MEETING_ID);
        }
        $this->logger2->info($meeting_key);
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
                "allow" => array_keys($this->session->get("room_info")),
                ),
            "flash_version" => array(
                "allow" => array("as2", "as3"),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
            "screen_mode"  => array(
                "allow"    => array('normal', 'wide'),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");
        // ユーザ情報
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );
        //member課金を利用していてメンバー以外が会議を利用しようとした場合
        if (($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") && !$member_info ) {
            $err_obj->set_error(API_ROOM_ID, "210100");
            return $this->display_error(100, "Parameter error");
        }
        // 会議の指定が有る場合
        $meeting_info = array();
        if ($meeting_key) {
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND meeting_session_id = '".addslashes($meeting_key)."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            $this->logger2->info($meeting_info);
            // 予約で時間外
            if (!$meeting_info) {
                $err_obj->set_error(API_MEETING_ID, "210101");
                return $this->display_error("101", "対象の会議が存在しないか、既に終了しています", $this->get_error_info($err_obj));
            } else {
                $obj_Participant = new DBI_Participant($this->get_dsn());
                $pcount = $obj_Participant->numRows("meeting_key = ".$meeting_info['meeting_key']." AND is_active = 1");
                if ($meeting_info["is_reserved"] &&
                    ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
                     (strtotime($meeting_info['meeting_stop_datetime'])) < time()) &&
                     $pcount == 0
                    )
                ) {
                    $err_obj->set_error(API_MEETING_ID, "210102");
                    return $this->display_error("101", "時間外の予約です", $this->get_error_info($err_obj));
                } else {
                    // 直前の会議取得
                    $last_meeting_key = $objRoom->getOne("room_key='".addslashes($room_key)."'", 'meeting_key');
                    // 異なる会議
                    if ($meeting_info['meeting_ticket'] != $last_meeting_key) {
                        // 開催中
                        $ret = $obj_N2MY_Meeting->getMeetingStatus($room_key, $last_meeting_key);
                        $err_obj->set_error(API_MEETING_ID, "210103");
                        if ($ret['status'] == '1') {
                            return $this->display_error("101", "前回会議が開催中です", $this->get_error_info($err_obj));
                        }
                    }
                }
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info($meeting_info['meeting_ticket']);
        // 会議情報取得
        if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_info['meeting_ticket'])) {
            $err_obj->set_error(API_ROOM_ID, "210004");
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $meeting_key = $meeting_info["meeting_key"];
        $country_key = $this->session->get("country_key");
        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();
        if (!array_key_exists($country_key, $country_list)) {
            $this->logger2->info($country_list);
            $_country_data = array_shift($country_list);
            $country_key = $_country_data["country_key"];
        }
        // オプション指定
        $options = array(
            "meeting_name" => $meeting_info["meeting_name"],
            "user_key"     => $user_info["user_key"],
            "start_time"   => $meeting_info["start_time"],
            "end_time"     => $meeting_info["end_time"],
            "country_id"   => $country_key,
            "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);

        //会議内からの招待メールのため会議作成毎にmgm_relationにmeeting_session_idを追加
        require_once("classes/mgm/dbi/relation.dbi.php");
        $objMgmRelationTable = new MgmRelationTable(
                                    $this->get_auth_dsn());
        $resultRelation = $objMgmRelationTable->getRow(sprintf("relation_key='%s'", $core_session));
        if (!$resultRelation) {
            $data = array(
                        "relation_key"  => $core_session,
                        "user_key"      => $user_info["user_id"],
                        "relation_type" => "meeting",
                        "status"        => 1,
                    );
            $add_data = $objMgmRelationTable->add($data);
        }

        // ユーザごとのオプションを取得
        $type        = $this->request->get("type", "normal");
        if ($is_audience = $this->session->get('invited_meeting_audience')) {
            switch($type) {
                case 'normal':
                    $type = 'audience';
                    break;
                case 'multicamera':
                    $type = 'multicamera_audience';
                    break;
                case 'whiteboard':
                    $type = 'whiteboard_audience';
                    break;
                case 'invisible_wb':
                    $type = 'invisible_wb_audience';
                    break;
            }
        }
//        $mode = "";
//        if ($meeting_info['is_trial']) {
        //ユーザーがトライアルである場合
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";

        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";
        $user_options = array(
            "id"                  => $member_info["member_key"],
            "name"                => $this->request->get("name", $member_info["member_name"]),
            "participant_email"   => $this->request->get("station", $member_info["member_station"]) ? $member_info["member_email"] : "",
            "participant_station" => $this->request->get("station", $member_info["member_station"]),
            "participant_country" => $country_key,
            "member_key"          => $member_info["member_key"],
            "narrow"              => $this->request->get("is_narrow"),
            "lang"                => $this->session->get("lang"),
            "skin_type"           => $this->request->get("skin_type", ""),
            "mode"                => $mode,
            "role"                => $login_type,
            "mail_upload_address" => $mail_upload_address,
            "account_model"       => $user_info["account_model"],
            );
        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);
        //centreまたはterminalでログインの場合はマルチカメラと資料共有のオプションを確認
        if ($login_type == "centre" || $login_type == "terminal") {
           $_room_info = $this->session->get("room_info");
           $room_options = $_room_info[$room_key]["options"];
           if ($room_options["multicamera"] && $type == "normal") {
                $type = "multicamera";
           } else if ($room_options["multicamera"] && $type == "audience") {
               $type = "multicamera_audience";
           } else if ($room_options["whiteboard"] && $login_type == "terminal" && $type == "normal") {
               $type = "whiteboard";
           } else if ($room_options["whiteboard"] && $login_type == "terminal" && $type == "audience") {
               $type = "whiteboard_audience";
           }
        }
        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set( "room_key", $room_key );
        $this->session->set( "type", $meetingDetail["participant_type_name"] );
        $this->session->set( "meeting_type", $type );
        $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set( "participant_key", $meetingDetail["participant_key"] );
        $this->session->set( "participant_name", $this->request->get("name", $member_info["member_name"]) );
        $this->session->set( "mail_upload_address", $mail_upload_address );
        $this->session->set( "fl_ver", $this->request->get("flash_version", "as2") );
        $this->session->set( "reload_type", 'normal');
        $this->session->set( "meeting_version", $user_info["meeting_version"] );
        // 画面サイズ
        $screen_mode = $this->request->get("screen_mode", "normal");
        switch ($screen_mode) {
            case 'normal' :
                $display_size = '4to3';
                break;
            case 'wide' :
                $display_size = '16to9';
                break;
            default :
                $display_size = '4to3';
                break;
        }
        $this->session->set( "display_size", $display_size);
        // 会議入室
        $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESS_NAME."=".session_id();
        if ($meeting_info["meeting_password"]) {
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".mysql_real_escape_string($user_info["user_key"])." AND reservation_session = '".mysql_real_escape_string($meeting_info["reservation_session"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            if ($reservation_info["reservation_pw_type"] == "1") {
                $this->session->set("redirect_url", $url, $this->_name_space);
                $url = N2MY_BASE_URL."services/" .
                        "?action_login=" .
                        "&".N2MY_SESS_NAME."=".session_id().
                        "&reservation_session=".$meeting_info["reservation_session"].
                        "&ns=".$this->_name_space;
            }
        }
        $data = array(
            "url"               => $url,
            API_MEETING_ID      => $this->ticket_to_session($meetingDetail["meeting_ticket"]),
            "meeting_name"      => $meetingDetail["meeting_name"],
//            "meeting_sequence_key"  => $meetingDetail["meeting_sequence_key"],
//            "participant_key"       => $meetingDetail["participant_key"],
//            "participant_id"        => $meetingDetail["participant_session_id"],
        );
        return $this->output($data);
    }

    /**
     *
     */
    function action_stop() {
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $password = $this->request->get("password");
        $user_info = $this->session->get("user_info");
//        $meeting_session = $this->request->get(API_MEETING_ID);
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
                "allow" => array_keys($this->session->get("room_info")),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // 部屋で直前に行われた会議を取得（今後、１部屋複数会議を実現した場合この機能は廃止になる可能性有り）
        require_once("classes/dbi/room.dbi.php");
        $objRoom = new RoomTable($this->get_dsn());
        $where = "room_key='".addslashes($room_key)."'" .
                " AND room_status = 1";
        $meeting_ticket = $objRoom->getOne($where, "meeting_key");
        $meeting_session = $this->ticket_to_session($meeting_ticket);
        require_once ( "classes/core/Core_Meeting.class.php" );
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $meeting_db = new N2MY_DB($this->get_dsn(), "meeting");
        $where = "meeting_session_id = '".addslashes($meeting_session)."'";
        $meeting_info = $meeting_db->getRow($where);
        // 予約があれば予約も終了
        if ($meeting_info["is_reserved"]) {
            require_once ("classes/dbi/reservation.dbi.php");
            $reservation_obj = new ReservationTable($this->get_dsn());
            $reservation_info = $reservation_obj->getRow("meeting_key = '".addslashes($meeting_ticket)."'");
//            // パスワードチェック
//            if ($reservation_info["reservation_pw"]) {
//                if (($password != $reservation_info["reservation_pw"]) && ($password != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "pw_equal");
//                }
//            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // 直前の会議が終了しているなら、エラーを返したいが、エラーというわけでもないので保留とする。
        if (!$meeting_info['is_active']) {
            $data = array(
                API_MEETING_ID           => "",
            );
        } else {
            // 予約があれば予約も終了
            if ($meeting_info["is_reserved"]) {
                $reservation_obj->cancel($reservation_info["reservation_session"]);
            }
            $obj_CoreMeeting->stopMeeting($meeting_info["meeting_key"]);
            $obj_Meeting = new DBI_Meeting($this->get_dsn());
            $where = "meeting_session_id = '".addslashes($meeting_session)."'" .
                " AND is_deleted = 0";
            $data = $obj_Meeting->getRow($where);
            $data = array(
                API_MEETING_ID           => $data['meeting_session_id'],
            );
        }
        return $this->output($data);
    }

    /**
     * ノッカー
     */
    function action_knocker() {
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
                "allow" => array_keys($this->session->get("room_info")),
                ),
            "message" => array(
                "required" => true,
                "maxlen" => 100,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        require_once("lib/EZLib/EZUtil/EZRtmp.class.php");
        require_once("classes/dbi/room.dbi.php");
        $objRoom        = new RoomTable($this->get_dsn());
        $obj_Meetng     = new MeetingTable($this->get_dsn());
        $obj_FmsServer  = new DBI_FmsServer( N2MY_MDB_DSN );
        // 部屋から直前の会議してい
        $where = "room_key = '".addslashes($room_key)."'" .
                " AND room_status = 1";
        $meeting_ticket = $objRoom->getOne($where, "meeting_key");
        $meeting_session = $this->ticket_to_session($meeting_ticket);
        $rtmp = new EZRtmp();
        // 会議のステータス取得
        $where = "meeting_session_id = '".addslashes($meeting_session)."'";
        $meeting_info = $obj_Meetng->getRow($where);
        // サーバ情報
        $server_info = $obj_FmsServer->getRow( sprintf("server_key=%d", $meeting_info["server_key"] ));
        $msg = addslashes($request["message"]);
        $msg = "'".$msg."'";
        $meeting_id = $obj_Meetng->getMeetingID($meeting_info["meeting_key"]);
        $uri = "rtmp://".$server_info["server_address"]."/".$this->config->get( "CORE", "app_name" )."/".$meeting_info["fms_path"].$meeting_id;
        $cmd .= "\"h = RTMPHack.new; " .
                "h.connection_uri = '" . $uri . "';" .
                "h.connection_args = [0x310, {:target => 'ALL', :message => ".$msg. "}];" .
                "h.method_name = '';" .
                "h.execute\"";
        $this->logger2->info($cmd);
        $rtmp->run($cmd);
        $data = array();
        return $this->output($data);
    }

    /**
     * 資料追加
     *
     */
    function action_wb_upload() {
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $password = $this->request->get("password");
        $file = $_FILES["file"];
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
                "allow" => array_keys($this->session->get("room_info")),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        if (!$file) {
            $err_obj->set_error("file", "required");
        } else {
            if (!EZValidator::valid_file_ext($file["name"], split(",", $this->config->get("N2MY", "convert_format")))) {
                $err_obj->set_error("file", "file_ext", split(",", $this->config->get("N2MY", "convert_format")));
            }
        }
        require_once("lib/EZLib/EZUtil/EZString.class.php");
        require_once( "classes/dbi/meeting.dbi.php" );
        require_once("classes/dbi/room.dbi.php");
        $objRoom = new RoomTable($this->get_dsn());
        $where = "room_key='".addslashes($room_key)."'" .
                " AND room_status = 1";
        $meeting_ticket = $objRoom->getOne($where, "meeting_key");
        $meeting_session = $this->ticket_to_session($meeting_ticket);
        $user_info = $this->session->get("user_info");
        // 会議のステータス取得
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $where = "meeting_session_id = '".addslashes($meeting_session)."'";
        $meeting_info = $obj_Meetng->getRow($where);
        // 予約があれば予約も終了
        if ($meeting_info["is_reserved"]) {
            require_once ("classes/dbi/reservation.dbi.php");
            $reservation_obj = new ReservationTable($this->get_dsn());
            $reservation_info = $reservation_obj->getRow("meeting_key = '".addslashes($meeting_ticket)."'");
//            // パスワードチェック
//            if ($reservation_info["reservation_pw"]) {
//                if (($password != $reservation_info["reservation_pw"]) && ($password != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "pw_equal");
//                }
//            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info(array($meeting_info, $obj_Meetng->_conn->last_query));
        $meeting_key = $meeting_info["meeting_key"];
        $document_path = $user_info["user_id"]."/".$meeting_info["room_key"]."/";

        $file = $_FILES["file"];
        $dir = $this->get_work_dir();
        $string = new EZString();
        $document_id = $string->create_id();
        $fileinfo = pathinfo($file["name"]);
        $tmp_name = $dir."/".$document_id.".".$fileinfo["extension"];
        move_uploaded_file($file["tmp_name"], $tmp_name);
        $name = ($name) ? $name : $fileinfo["basename"];
        // 部屋一覧取得
        $url = N2MY_LOCAL_URL."/api/document/";
        $post_data = array(
            "action_wb_upload" => "1",
            "db_host"       => $this->get_dsn_key(),
            "meeting_key"   => $meeting_key,
            "document_path" => $document_path,
            "Filedata"      => "@".$tmp_name,
            );
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 10,
            );
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        $this->logger2->info(array($option, $ret));
        $data = array("document_id" => $ret);
        return $this->output($data);
    }

}
$main = new N2MY_Meeting_API();
$main->execute();
