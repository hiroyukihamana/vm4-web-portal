<?php
/*
 * Created on 2008/02/14
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("lib/EZLib/EZUtil/EZString.class.php");
require_once("classes/N2MY_Api.class.php");

class N2MY_Meeting_Document_API extends N2MY_API
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
        $this->_name_space = md5(__FILE__);
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
    }

    /**
     * 資料追加
     *
     * @param string room_key ルームキー
     * @param string file ファイル情報
     * @return boolean
     */
    function action_add()
    {
        $meeting_ticket = $this->request->get("meeting_ticket");
        $user_info = $this->session->get("user_info");
        // 会議のステータス取得
        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $where = "meeting_ticket = '".addslashes($meeting_ticket)."'";
        $meeting_info = $obj_Meetng->getRow($where);
        $this->logger2->info(array($meeting_info, $obj_Meetng->_conn->last_query));
        $meeting_key = $meeting_info["meeting_key"];
        $document_path = $user_info["user_id"]."/".$meeting_info["room_key"]."/";

        $file = $_FILES["file"];
        $dir = $this->get_work_dir();
        $string = new EZString();
        $document_id = $string->create_id();
        $fileinfo = pathinfo($file["name"]);
        $tmp_name = $dir."/".$document_id.".".$fileinfo["extension"];
        move_uploaded_file($file["tmp_name"], $tmp_name);
        $name = ($name) ? $name : $fileinfo["basename"];

        // 部屋一覧取得
        $url = N2MY_LOCAL_URL."/api/document/";
        $post_data = array(
            "action_wb_upload" => "1",
            "db_host"       => $this->get_dsn_key(),
            "meeting_key"   => $meeting_key,
            "document_path" => $document_path,
            "Filedata"      => "@".$tmp_name,
            );
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 10,
            );
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        $this->logger2->info(array($option, $ret));
        return $this->output();
    }

}
$main = new N2MY_Meeting_Document_API();
$main->execute();