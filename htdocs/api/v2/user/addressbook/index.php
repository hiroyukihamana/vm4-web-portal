<?php
/*
 * Created on 2008/02/06
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");
require_once('classes/dbi/address_book.dbi.php');
require_once('classes/dbi/member.dbi.php');
require_once("config/config.inc.php");

class N2MY_AddressBook_API extends N2MY_Api
{
    var $session_id = null;
    var $meetingLog = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init() {
        // SSL対応
        header('Pragma:');
        $this->logger =& EZLogger2::getInstance();
        $this->_name_space = md5(__FILE__);
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth() {
        $this->checkAuthorization();
        $this->objAddressBook = new AddressBookTable($this->get_dsn());
        $this->objAddressBook->max_address_cnt = $this->config->get("N2MY", "address_book_count", 500);
        $this->objMember = new MemberTable($this->get_dsn());
    }

    function action_get_list() {

        $sort_key  = $this->request->get("sort_key", "name");
        $sort_type = $this->request->get("sort_type", "asc");
        $offset    = $this->request->get("offset", 0);
        $limit     = $this->request->get("limit", 20);
        $keyword   = $this->request->get("keyword");
        $member    = $this->request->get("member", null);
        $output_type = $this->request->get("output_type", null);
        $rule_output = $this->get_output_type_list();
        $rule_output[100] = 100;

        // チェックルール編集
        $rules = array(
            "limit"       => array(
                "integer" => true,
                ),
            "offset"      => array(
                "integer" => true,
                ),
            "sort_key"    => array(
                "allow"   => array("name", "name_kana", "email"),
                ),
            "sort_type"   => array(
                "allow"   => array("asc", "desc"),
                ),
            "member"      => array(
                "allow"   => array("0", "1"),
                ),
            "keyword"     => array(
                "maxlen"  => 64,
                ),
            "output_type" => array(
                "allow"   => $rule_output,
                ),
        );
        // チェックデータ編集
        $chack_data = array(
            "limit"       => $limit,
            "offset"      => $offset,
            "sort_key"    => $sort_key,
            "sort_type"   => $sort_type,
            "member"      => $member,
            "keyword"     => $keyword,
            "output_type" => $output_type,
        );
        // チェック
        $err_obj = $this->error_check($chack_data, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $user_info = $this->session->get("user_info");
        $where = "is_deleted = 0" .
            " AND user_key = ".$user_info["user_key"];
        if ($keyword) {
            $where .= " AND (name like '%".addslashes($keyword)."%'" .
                    " OR name_kana like '%".addslashes($keyword)."%'".
                    " OR email like '%".addslashes($keyword)."%')";
        }
        if ($member_info = $this->session->get("member_info")) {
            if (!$member) {
                $where .= " AND member_key = ".$member_info["member_key"];
            }
        } else {
            $where .= " AND member_key IS NULL";
        }
        if ($limit === "0") {
            $this->logger2->info($limit);
            $limit = null;
            $offset = null;
        }
        $rows = $this->objAddressBook->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        if (DB::isError($rows)) {
            $this->logger2->error($rows->getUserInfo());
        }
        $adressBook = array();
        foreach ( $rows as $_key => $_val) {
            $adress_book = array(
                API_ADDRESS_ID      => $rows[$_key]['address_book_key'],
                'name'              => $rows[$_key]['name'],
                'name_kana'         => $rows[$_key]['name_kana'],
                'email'             => $rows[$_key]['email'],
                'lang'              => $rows[$_key]['lang'],
                'timezone'          => $rows[$_key]['timezone']
             );
            // 現行のCentreアプリと言語の互換性を持たせる。
            if($adress_book['lang'] == 'zh-cn') {
                $adress_book['lang'] = 'zh';
            }
              $adressBook[] = $adress_book;

//            $rows[$_key][API_ADDRESS_ID] = $rows[$_key]['address_book_key'];
//            unset($rows[$_key]['address_book_key']);
//            unset($rows[$_key]['user_key']);
//            unset($rows[$_key]['member_key']);
//            unset($rows[$_key]['memo']);
//            unset($rows[$_key]['is_deleted']);
//            unset($rows[$_key]['create_datetime']);
//            unset($rows[$_key]['update_datetime']);
//            unset($rows[$_key]['addition']);
        }

        $count = $this->objAddressBook->numRows($where);
        $data = array(
            "count" => $count,
            "addresses" => array("address" => $adressBook),
        );
        return $this->output($data);
    }

    function action_get_count() {
        $keyword   = $this->request->get("keyword");
        $member    = $this->request->get("member", null);
        $output_type = $this->request->get("output_type", null);
        $rule_output = $this->get_output_type_list();
        $rule_output[100] = 100;

        // チェックルール編集
        $rules = array(
            "limit"       => array(
                "integer" => true,
                ),
            "offset"      => array(
                "integer" => true,
                ),
            "sort_key"    => array(
                "allow"   => array("name", "name_kana", "email"),
                ),
            "sort_type"   => array(
                "allow"   => array("asc", "desc"),
                ),
            "member"      => array(
                "allow"   => array("0", "1"),
                ),
            "keyword"     => array(
                "maxlen"  => 64,
                ),
            "output_type" => array(
                "allow"   => $rule_output,
                ),
        );
        // チェックデータ編集
        $chack_data = array(
            "member"      => $member,
            "keyword"     => $keyword,
            "output_type" => $output_type,
        );
        // チェック
        $err_obj = $this->error_check($chack_data, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $user_info = $this->session->get("user_info");
        $where = "is_deleted = 0" .
            " AND user_key = ".$user_info["user_key"];
        if ($keyword) {
            $where .= " AND (name like '%".addslashes($keyword)."%'" .
                    " OR name_kana like '%".addslashes($keyword)."%'".
                    " OR email like '%".addslashes($keyword)."%')";
        }
        if ($member_info = $this->session->get("member_info")) {
            if (!$member) {
                $where .= " AND member_key = ".$member_info["member_key"];
            }
        } else {
            $where .= " AND member_key IS NULL";
        }
        $count = $this->objAddressBook->numRows($where);
        $data = array(
            "count" => $count,
        );
        return $this->output($data);
    }

/*
    function action_get_member() {
        $sort_key  = $this->request->get("sort_key", "member_name");
        $sort_type = $this->request->get("sort_type", "asc");
        $offset      = $this->request->get("offset", 1);
        $limit     = $this->request->get("limit", 20);
        $keyword   = $this->request->get("keyword");
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );

        $offset    = ($limit * ($offset - 1));
        // 条件
        $where = " WHERE member_status = 0" .
                " AND member.user_key = ".$user_info["user_key"];
        if ($keyword) {
            $where .= " AND (member_name like '%".addslashes($keyword)."%'" .
                    " OR member_name_kana like '%".addslashes($keyword)."%'".
                    " OR member_email like '%".addslashes($keyword)."%')";
        }

        //member課金
        if ( $user_info["account_model"] == "member" && $member_info ) {
            $where = sprintf( "%s AND member.member_key != '%s'", $where, $member_info["member_key"] );
        }

        // 件数
        $sql = "SELECT count(*) FROM member" .
                " LEFT JOIN member_group" .
                " ON (member.user_key = member_group.user_key".
                " AND member.member_group = member_group.member_group_key)";
        $sql .= $where;
        $count = $this->objMember->_conn->getOne($sql);
        if (DB::isError($count)) {
            print $count->getUserInfo();
        }
        // データ取得
        $sql = "SELECT member.*, member_group_name FROM member".
                " LEFT JOIN member_group" .
                " ON (member.user_key = member_group.user_key".
                " AND member.member_group = member_group.member_group_key)";
        $sql .= $where;
        $sql .= " ORDER BY ";
        if (strtolower($sort_type) == "desc") {
            $sql .= $sort_key." DESC";
        } else {
            $sql .= $sort_key." ASC";
        }
        $rs = $this->objMember->_conn->limitQuery($sql, $offset, $limit);
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        $data = array(
            "count" => $count,
            "addresses" => $rows,
        );
        return $this->output($data);
    }
*/

    function action_add() {
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        $data = array(
            "name"      => $this->request->get("name"),
            "name_kana" => $this->request->get("name_kana"),
            "email"     => $this->request->get("email"),
            "timezone"  => $this->request->get(API_TIME_ZONE, $this->session->get('time_zone')),
            "lang"      => $this->request->get("lang"),
            "user_key"  => $user_info["user_key"],
        );
        // zh -> zh-cnに変換。
        if($data['lang'] == 'zh') {
            $data['lang'] = EZLanguage::getLang(EZLanguage::getLangCd($data['lang']));
        }
        // メンバー
        if ($member_info) {
            $data["member_key"] = $member_info["member_key"];
        }
        // 入力チェック
        $check_rules = array(
            "name" => array(
                "required" => true,
                "maxlen" => 50,
                "regex" => "/^[^\\r\\n]*$/D",
            ),
            "name_kana" => array(
                "maxlen" => 50,
                "regex" => "/^[^\\r\\n]*$/D",
            ),
            "email" => array(
                "required" => true,
                "email" => true,
                "maxlen" => 50,
                "regex" => "/^[^\\r\\n]*$/D",
                    ),
            API_TIME_ZONE => array(
                "allow" => array_keys($this->get_timezone_list()),
                ),
            "lang" => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $objCheck =& $this->objAddressBook->check($data, $check_rules);
//        $this->logger2->info(array($data, $check_rules, array_keys($this->get_timezone_list())));
        if (EZValidator::isError($objCheck)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($objCheck));
        } else {
            // 登録件数チェック
            $insert_id = $this->objAddressBook->add($data);
            $data = array(API_ADDRESS_ID => $insert_id);
            return $this->output($data);
        }
    }

    function action_update() {
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        $request = $this->request->getAll();
        // zh -> zh-cnに変換。
        if($request['lang'] == 'zh') {
            $request['lang'] = EZLanguage::getLang(EZLanguage::getLangCd($request['lang']));
        }
        $request["user_key"] = $user_info["user_key"];
        // メンバー
        if ($member_info) {
            $data["member_key"] = $member_info["member_key"];
        }
        // 入力チェック
        $check_rules = array(
            API_ADDRESS_ID => array(
                "required" => true,
                "integer" => true,
                ),
            "name" => array(
                "required" => true,
                "maxlen" => 50,
                "regex" => "/^[^\\r\\n]*$/D",
                ),
            "name_kana" => array(
                "maxlen" => 50,
                "regex" => "/^[^\\r\\n]*$/D",
                ),
            "email" => array(
                "required" => true,
                "email" => true,
                "maxlen" => 50,
                "regex" => "/^[^\\r\\n]*$/D",
                ),
            API_TIME_ZONE => array(
                "required" => true,
                "allow" => array_keys($this->get_timezone_list()),
                ),
            "lang" => array(
                "required" => true,
                "allow" => array_keys($this->get_language_list()),
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $objCheck =& $this->error_check($request, $check_rules);
        $address_id = $this->request->get(API_ADDRESS_ID);
        $where = "address_book_key = ".addslashes($address_id).
            " AND user_key = ".addslashes($user_info["user_key"]);
        if ($member_info["member_key"]) {
            $where .= " AND member_key = ".addslashes($member_info["member_key"]);
        } else {
            $where .= " AND member_key IS NULL";
        }
        // 予約会議登録
        if (!EZValidator::isError($objCheck)) {
            if (!$address_book_data = $this->objAddressBook->getRow($where)) {
                $objCheck->set_error(API_ADDRESS_ID, "240400");
            }
        }
        if (EZValidator::isError($objCheck)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($objCheck));
        } else {
            $data = array(
                "name"      => $request["name"],
                "name_kana" => $request["name_kana"],
                "email"     => $request["email"],
                "timezone"  => $request[API_TIME_ZONE],
                "lang"      => $request["lang"],
                "user_key"  => $user_info["user_key"],
            );
            $this->logger2->info(array($data, $address_book_data, $where));
            $ret = $this->objAddressBook->update($data, $where);
            if (DB::isError($ret)) {
                $this->logger2->info($ret->getUserInfo());
            }
            return $this->output();
        }
    }

    function action_delete() {
        $user_info = $this->session->get("user_info");
        $key = $this->request->get(API_ADDRESS_ID);
        // チェックルール編集
        $rules = array(
            API_ADDRESS_ID     => array(
                "integer"    => true,
                "required"    => true,
                ),
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            API_ADDRESS_ID => $key,
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        $where = "is_deleted = 0".
            " AND address_book_key = ".addslashes($key).
            " AND user_key = ".$user_info["user_key"];
        if ($member_info = $this->session->get("member_info")) {
            $where .= " AND member_key = ".$member_info["member_key"];
        }
        // 予約会議登録
        if (!EZValidator::isError($err_obj)) {
            if (!$address_book_data = $this->objAddressBook->getRow($where)) {
                $err_obj->set_error(API_ADDRESS_ID, "240500");
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $data = array(
            "is_deleted" => 1,
            "update_datetime" => date("Y-m-d H:i:s"),
            );
        $this->objAddressBook->update($data, $where);
        return $this->output();
    }
}

$main = new N2MY_AddressBook_API();
$main->execute();