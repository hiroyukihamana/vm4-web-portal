<?php
/*
 * Created on 2008/02/06
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");
require_once('classes/dbi/address_book.dbi.php');
require_once('classes/dbi/member.dbi.php');
require_once("config/config.inc.php");

class N2MY_Eco_API extends N2MY_Api
{
    var $session_id = null;
    var $meetingLog = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init() {
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth() {
        $this->checkAuthorization();
    }

    function action_get_station_list() {
        $request = $this->request->getAll();
        require_once('lib/EZLib/Transit.class.php');
        $station = $this->request->get("station");
        $rules = array(
            "station" => array(
                "required" => true,
            ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $uri = $this->config->get("N2MY", "transit_uri");
            $transit_log = realpath(dirname(__FILE__)."/../../../var/")."/transit.log";
            $Transit = Transit::factory( $uri, $transit_log );
            $ret1 = $Transit->getStationList($station, false, 50);
            $results['station'] = array();
            foreach( $ret1 as $key => $value ){
                $results['station'][] = $value;
            }
            $data = array("stations" => $results);
            return $this->output($data);
        }
    }

    function action_get_transit() {
        require_once('lib/EZLib/Transit.class.php');
        $request = $this->request->getAll();
        $station_list = $this->request->get("station_list");
        $end_place    = $this->request->get("end_place");
        $sort         = $this->request->get("sort", "time");
        $detail       = $this->request->get("detail", "0");
        $uri = $this->config->get("N2MY", "transit_uri");
        $transit_log = realpath(dirname(__FILE__)."/../../../var/")."/transit.log";
        $Transit = Transit::factory($uri, $transit_log );
        $rules = array(
            "station_list" => array(
                "required" => true,
            ),
            "sort" => array(
                "allow" => array("time", "fare", "transit", "co2"),
            ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!is_array($station_list)) {
            $station_list = split(",", $station_list);
        }
        $stations = array_unique($station_list);
        if (count($stations) < 2) {
            $err_obj->set_error("station_list", "minitem", 2);
        } else {
            foreach($stations as $station) {
                $ret1 = $Transit->getStationList($station, false, 50);
                if (count($ret1) != 1) {
                    $err_obj->set_error("station_list", "not_equal", $ret1);
                    break;
                }
            }
        }
        if ($end_place) {
            $ret1 = $Transit->getStationList($end_place, false, 50);
            if (count($ret1) != 1) {
                $err_obj->set_error("end_place", "not_equal", $ret1);
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            // ２拠点以上
            $ret = $Transit->getMultiStationRoute($station_list, $end_place, $sort, $detail);
            return $this->output($ret);
        }
    }
}

$main = new N2MY_Eco_API();
$main->execute();
?>
