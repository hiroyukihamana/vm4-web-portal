<?php
/*
 * Created on 2008/02/14
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");
require_once("classes/N2MY_Reservation.class.php");
require_once("config/config.inc.php");

class N2MY_Meeting_Resevation_API extends N2MY_Api
{
    var $_err = null;
    var $_ssl_mode = null;

    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
        $this->objReservation = new N2MY_Reservation($this->get_dsn(), $this->session->get("dsn_key"));
    }

    /**
     * 予約一覧（検索）
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_limit 検索開始日 (デフォルト null)
     * @param datetime end_limit 検索終了日 (デフォルト null)
     * @param string sort_key ソート基準 (デフォルト 予約会議開始時間)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @param int limit 表示件数 (デフォルト null)
     * @param int offset 開始件数 (デフォルト null)
     * @return string $output 会議キー、予約キー、会議名、開始時間、終了時間をxmlで出力
     */
    function action_get_list()
    {
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        $request = $this->request->getAll();
        $room_key         = $this->request->get(API_ROOM_ID, "");
        $reservation_name = $this->request->get("reservation_name", "");
        $_wk_start_limit  = $this->request->get("start_limit");
        $_wk_end_limit    = $this->request->get("end_limit");
        $member_id        = $this->request->get("member_id");
        $status           = $this->request->get("status");
        $limit            = $this->request->get("limit", N2MY_API_GET_LIST_LIMIT);
        $offset           = $this->request->get("offset", 0);
        $sort_key         = $this->request->get("sort_key", "reservation_starttime");
        $sort_type        = $this->request->get("sort_type", "asc");
        // タイムスタンプ変換
        $time_zone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $_tz_before = $time_zone;
        $_tz_after = $time_zone;
        if (is_numeric($_wk_start_limit)) {
            $_ts_local = EZDate::getLocateTime($_wk_start_limit, $_tz_after, $_tz_before);
            $start_limit = date("Y-m-d H:i:s", $_ts_local);
        } else {
            $start_limit = $_wk_start_limit;
        }
        if (is_numeric($_wk_end_limit)) {
            $_ts_local = EZDate::getLocateTime($_wk_end_limit, $_tz_after, $_tz_before);
            $end_limit = date("Y-m-d H:i:s", $_ts_local);
        } else {
            $end_limit = $_wk_end_limit;
        }
        $request["start_limit"] = $start_limit;
        $request["end_limit"] = $end_limit;
        $this->logger2->debug($request);
        $rules = array(
            API_ROOM_ID => array(
                "allow" => array_keys($room_info),
                ),
            "start_limit" => array(
                "datetime" => true,
                ),
            "end_limit" => array(
                "datetime" => true,
                ),
            "status" => array(
                "allow" => array('wait', 'now', 'valid', 'end'),
                ),
            "limit" => array(
                "integer" => true,
                ),
            "offset" => array(
                "integer" => true,
                ),
            "sort_key" => array(
                "allow" => array("reservation_name", "reservation_starttime"),
                ),
            "sort_type" => array(
                "allow" => array("asc", "desc")
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if ($member_id) {
            require_once('classes/dbi/member.dbi.php');
            $objMember = new MemberTable($this->get_dsn());
            $where = "user_key = ".$user_info["user_key"].
                " AND member_status = 0" .
                " AND member_id = '".addslashes($member_id)."'";
            if (!$member_key = $objMember->getOne($where, 'member_key')) {
                $err_obj->set_error("member_id", "220100");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            if ($limit === "0") {
                $this->logger2->info($limit);
                $limit = null;
                $offset = null;
            }
            //
            $options = array(
                "user_key"         => $user_info["user_key"],
                "room_key"         => $room_key,
                "reservation_name" => $reservation_name,
                "start_time"       => $start_limit,
                "end_time"         => $end_limit,
                "member_key"       => $member_key,
                "status"           => $status,
                "limit"            => $limit,
                "offset"           => $offset,
                "sort_key"         => $sort_key,
                "sort_type"        => $sort_type,
                );
            $reservations = $this->objReservation->getList($room_key, $options, $time_zone);
            $count = $this->objReservation->getCount($room_key, $options);
            $reservationsList = array();
            if ($reservations) {
                // 一覧表示
                foreach ($reservations as $_key => $_val) {
                    $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $reservations[$_key]["reservation_session"]);
                    $encrypted_data = str_replace("/", "-", $encrypted_data);
                    $encrypted_data = str_replace("+", "_", $encrypted_data);
                    $reservations_list = array(
                          'reservation_name'        => $reservations[$_key]["reservation_name"],
//                          'reservation_pw'          => $reservations[$_key]["reservation_pw"] = ($reservations[$_key]["reservation_pw"]) ? 1 : 0 ,
                          'reservation_pw'          => $reservations[$_key]["reservation_pw"] = $reservations[$_key]["reservation_pw"],
                          'status'                  => $reservations[$_key]["status"],
                          'room_id'                 => $reservations[$_key]["room_key"],
                          'room_name'               => $room_info[$reservations[$_key]["room_key"]]["room_info"]["room_name"],
                          'sender'                  => $reservations[$_key]["sender_name"],
                          'sender_mail'             => $reservations[$_key]["sender_email"],
                          'mail_body'               => $reservations[$_key]["reservation_info"],
                          API_ROOM_ID               => $reservations[$_key]["room_key"],
                          API_MEETING_ID            => $this->ticket_to_session($reservations[$_key]["meeting_key"]),
                          API_RESERVATION_ID        => $reservations[$_key]["reservation_session"],
                          API_RESERVATION_START     => $reservations[$_key]["reservation_starttime"],
                          API_RESERVATION_END       => $reservations[$_key]["reservation_endtime"],
                          'url'                     => N2MY_BASE_URL."y/".$encrypted_data
                    );
                    $reservationsList[] = $reservations_list;
                }
            }
            $data = array(
                "count" => $count,
                "reservations"  => array("reservation" => $reservationsList),
                );
            return $this->output($data);
        }
    }

    function action_get_count() {
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        $request = $this->request->getAll();
        $room_key         = $this->request->get(API_ROOM_ID, "");
        $reservation_name = $this->request->get("reservation_name", "");
        $_wk_start_limit  = $this->request->get("start_limit");
        $_wk_end_limit    = $this->request->get("end_limit");
        $member_id        = $this->request->get("member_id");
        $status           = $this->request->get("status");
        // タイムスタンプ変換
        $time_zone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $_tz_before = $time_zone;
        $_tz_after = $time_zone;
        if (is_numeric($_wk_start_limit)) {
            $_ts_local = EZDate::getLocateTime($_wk_start_limit, $_tz_after, $_tz_before);
            $start_limit = date("Y-m-d H:i:s", $_ts_local);
        } else {
            $start_limit = $_wk_start_limit;
        }
        if (is_numeric($_wk_end_limit)) {
            $_ts_local = EZDate::getLocateTime($_wk_end_limit, $_tz_after, $_tz_before);
            $end_limit = date("Y-m-d H:i:s", $_ts_local);
        } else {
            $end_limit = $_wk_end_limit;
        }
        $request["start_limit"] = $start_limit;
        $request["end_limit"] = $end_limit;
        $this->logger2->info($request);
        $rules = array(
            API_ROOM_ID => array(
                "allow" => array_keys($room_info),
                ),
            "start_limit" => array(
                "datetime" => true,
                ),
            "end_limit" => array(
                "datetime" => true,
                ),
            "status" => array(
                "allow" => array('wait', 'now', 'end'),
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if ($member_id) {
            require_once('classes/dbi/member.dbi.php');
            $objMember = new MemberTable($this->get_dsn());
            $where = "user_key = ".$user_info["user_key"].
                " AND member_status = 0" .
                " AND member_id = '".addslashes($member_id)."'";
            $this->logger2->info($where);
            if (!$member_key = $objMember->getOne($where, 'member_key')) {
                $err_obj->set_error("member_id", "220200");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            //
            $options = array(
                "user_key"         => $user_info["user_key"],
                "room_key"         => $room_key,
                "reservation_name" => $reservation_name,
                "start_time"       => $start_limit,
                "end_time"         => $end_limit,
                "member_key"       => $member_key,
                "status"           => $status,
                );
            $count = $this->objReservation->getCount($room_key, $options);
            $data = array(
                "count" => $count,
                );
            return $this->output($data);
        }
    }

    /**
     * 予約内容詳細（予約、招待者、事前資料）
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return string $output 予約キー、会議室名、会議名、開始時間、終了時間、パスワード、招待者、資料名をxmlで出力
     */
    function action_get_detail()
    {
        $objMember = new MemberTable($this->get_dsn());
        $request = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $password               = $this->request->get("password");
        $user_info              = $this->session->get("user_info");
        $room_info              = $this->session->get("room_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "220300");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "220300");
            }
        }
//        // パスワードチェック
//        if ($info["reservation_pw"]) {
//            if (($password != $info["reservation_pw"]) && ($password != $user_info["user_admin_password"])) {
//                $err_obj->set_error("password", "220012");
//            }
//        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $meeting_key = $reservation_data["info"]["meeting_key"];
            $info = array(
                'reservation_name'      => $reservation_data["info"]["reservation_name"],
                'reservation_pw'        => $reservation_data["info"]["reservation_pw"],
                'status'                => $reservation_data["info"]["status"],
                'sender'                => $reservation_data["info"]["sender"],
                'sender_mail'           => $reservation_data["info"]["sender_mail"],
                'mail_body'             => $reservation_data["info"]["mail_body"],
                API_ROOM_ID             => $reservation_data["info"]["room_key"],
                'room_name'             => $room_info[$reservation_data["info"]["room_key"]]["room_info"]["room_name"],
                API_MEETING_ID          => $this->ticket_to_session($reservation_data["info"]["meeting_key"]),
                API_RESERVATION_ID      => $reservation_data["info"]["reservation_session"],
                API_RESERVATION_START   => EZDate::getLocateTime($reservation_data["info"]["reservation_starttime"], N2MY_SERVER_TIMEZONE, $reservation_data["info"]["reservation_place"]),
                API_RESERVATION_END     => EZDate::getLocateTime($reservation_data["info"]["reservation_endtime"], N2MY_SERVER_TIMEZONE, $reservation_data["info"]["reservation_place"]),
            );
            $reservation_data["info"] = $info;
            unset($reservation_data["guest_flg"]);
            $country_key = $this->session->get("country_key");

            foreach ($reservation_data["guests"] as $_key => $guest) {
                $member_id = '';
                // メンバーキーで検索
                if ($guest["member_key"]) {
                    $where = "user_key = ".$user_info["user_key"].
                        " AND member_status = 0".
                        " AND member_key = '".addslashes($guest["member_key"])."'";
                    $member_id = $objMember->getOne($where, "member_id");
                }
                $guests[] = array(
                    'member_id'  => $member_id,
                    'name'       => $guest["name"],
                    'email'      => $guest["email"],
                    'timezone'   => $guest["timezone"],
                    'lang'       => $guest["lang"],
                    'type'       => ($guest["type"] == 1) ? "audience" : "" ,
                    API_GUEST_ID => $guest["r_user_session"],
                    'invite_url' => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"]
                 );

//                $guests[] = $guest;
//                $guest[API_GUEST_ID] = $guest["r_user_session"];
//                $guest["invite_url"] = N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"];
//                unset($guest["r_user_session"]);
//                $guest["type"] = ($guest["type"] == 1) ? "audience" : "";

            }

            unset($reservation_data["guests"]);
            $reservation_data["guests"]["guest"] = $guests;

            foreach ($reservation_data["documents"] as $_key => $_val) {
                switch ($_val["status"]) {
                    case "-1":  $status = "error";      break;
                    case "0":   $status = "uploading";  break;
                    case "1":   $status = "pending";    break;
                    case "2":   $status = "done";       break;
                    case "3":   $status = "done";       break;
                    default:    $status = "error";      break;
                }
                $documents[] = array(
                    'document_id'   => $_val["document_id"],
                    'name'          => $_val["name"],
                    'type'          => $_val["type"],
                    'category'      => $_val["category"],
                    'status'        => $status,
                    'index'         => $_val["index"],
                    'sort'          => $_val["sort"]
                 );
//                unset($_val["create_datetime"]);
//                unset($_val["update_datetime"]);
                //$documents[] = $_val;
            }
            unset($reservation_data["documents"]);
            require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
            $reservation_data["documents"]["document"] = $documents;
            $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $reservation_session);
            $encrypted_data = str_replace("/", "-", $encrypted_data);
            $encrypted_data = str_replace("+", "_", $encrypted_data);
            $reservation_data["info"]["url"] = N2MY_BASE_URL."y/".$encrypted_data;

            $obj_Meeting  = new DBI_Meeting($this->get_dsn());
            $meeting_data = $obj_Meeting->getRow(sprintf("meeting_ticket='%s'",$meeting_key));
            $teleconf_flg = ($meeting_data["tc_type"]=="pgi")? 1:0;
            if($teleconf_flg){
                $pgi_info = array(
                        "pgi_info" => array(
                                "teleconf_flg"          => $teleconf_flg,
                                "use_pgi_dialin"        => $meeting_data["use_pgi_dialin"],
                                "use_pgi_dialin_free"   => $meeting_data["use_pgi_dialin_free"],
                                "use_pgi_dialin_lo_call"=> $meeting_data["use_pgi_dialin_lo_call"],
                                "use_pgi_dialout"       => $meeting_data["use_pgi_dialout"],
                                "pgi_m_pass_code"       => $meeting_data["pgi_m_pass_code"],
                                "pgi_numbers_url"       => N2MY_BASE_URL."p/".$meeting_data["meeting_session_id"]),);
            }else{
                $pgi_info = array(
                        "pgi_info" => array(
                                "teleconf_flg"          => $teleconf_flg,),);
            }
            $reservation_data = array_merge($reservation_data,$pgi_info);
            $data = array(
                "reservation_info"  => $reservation_data,
                );
            return $this->output($data);
        }
    }

    /**
     * 予約追加
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_time 開始日時
     * @param datetime end_time 終了日時
     * @param string r_user_name 参加者名 (デフォルト null)
     * @param string r_user_email 参加者メールアドレス (デフォルト null)
     * @param string r_user_lang 参加者言語 (デフォルト null)
     * @param string r_user_timezone 参加者タイムゾーン (デフォルト null)
     * @return boolean
     */
    function action_add()
    {
        $request = $this->request->getAll();
        $user_info = $this->session->get("user_info");
        $user_key  = $user_info["user_key"];
        $room_key                   = $this->request->get(API_ROOM_ID);
        $reservation_name           = $this->request->get("name");
        $reservation_place          = $this->request->get(API_TIME_ZONE, $this->session->get('time_zone')); // セッションの値から取得
        $_wk_reservation_starttime  = $this->request->get("start");
        $_wk_reservation_endtime    = $this->request->get("end");
        $reservation_pw             = $this->request->get("password");
        $reservation_pw_type        = $this->request->get("password_type", 0);
        $sender_name                = $this->request->get("sender_name");
        $sender_email               = $this->request->get("sender_email");
        $reservation_info           = $this->request->get("info");
        $_guests                    = $this->request->get("guests");
        $send_mail                  = $this->request->get("send_mail", 1);
        $is_multicamera             = $this->request->get("is_multicamera", 1);
        $is_telephone               = $this->request->get("is_telephone", 1);
        $is_h323                    = $this->request->get("is_h323", 1);
        $is_mobile_phone            = $this->request->get("is_mobile_phone", 1);
        $is_high_quality            = $this->request->get("is_high_quality", 1);
        $is_desktop_share           = $this->request->get("is_desktop_share", 1);
        $is_ssl                     = $this->request->get("is_ssl", 1);
        $is_invite                  = $this->request->get("is_invite", 1);
        $is_record                  = $this->request->get("is_record", 1);
        $is_cabinet                 = $this->request->get("is_cabinet", 1);
        $teleconf_flg               = $this->request->get('teleconf_flg', 0);
        $use_pgi_dialin             = $this->request->get('use_pgi_dialin', 0);
        $use_pgi_dialin_free        = $this->request->get('use_pgi_dialin_free', 0);
        $use_pgi_dialin_lo_call     = $this->request->get('use_pgi_dialin_lo_call', 0);
        $use_pgi_dialout            = $this->request->get('use_pgi_dialout', 0);
        // メンバーログインで差出人が未入力の場合は自動補完
        if ($member_info = $this->session->get('member_info')) {
            $this->logger2->info($member_info);
            // メンバー又は、
            // ログインタイプ取得
            $login_type = $this->session->get("login_type");
            if ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre" || $login_type == "terminal") {
                // 名前
                if ($request['sender_name'] == "") {
                    $sender_name = $member_info['member_name'];
                    $request['member_name'] = $sender_name;
                }
                // メール
                if ($request['sender_email'] == "") {
                    $sender_email = $member_info['member_email'];
                    $request['sender_email'] = $sender_email;
                    if (($user_info["account_model"] == "centre" || $login_type == "terminal") && !$sender_email) {
                        $sender_email = $this->config->get("N2MY", "noreply_address")."@".$this->config->get("N2MY", "mail_wb_host");
                        $request['sender_email'] = $sender_email;
                    }
                }
            }
        }
        // 部屋のオプション情報取得
        $_room_info = $this->session->get("room_info");
        $room_keys = array_keys($_room_info);
        $room_info = $_room_info[$room_key];
//        $this->logger2->info($room_info);
        // オプションが無いものは無視
        if ($room_info['options']['multicamera'] == 0) {
            $is_multicamera = 0;
        }
        if ($room_info['options']['telephone'] == 0) {
            $is_telephone = 0;
        }
        if ($room_info['options']['h323_number'] == 0) {
            $is_h323 = 0;
        }
        if ($room_info['options']['mobile_phone_number'] == 0) {
            $is_mobile_phone = 0;
        }
        if ($room_info['options']['high_quality'] == 0) {
            $is_high_quality = 0;
        }
        if ($room_info['options']['desktop_share'] == 0) {
            $is_desktop_share = 0;
        }
        if ($room_info['options']['meeting_ssl'] == 0) {
            $is_ssl = 0;
        }
        if (!$is_invite) {
            $is_invite = 1;
        }
        if (!$is_record) {
            $is_record = 1;
        }
        if (!$is_cabinet) {
            $is_cabinet = 1;
        }
        // 時差変換
        $server_timezone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        if ($_wk_reservation_starttime) {
            if (is_numeric($_wk_reservation_starttime)) {
                $start_ts = $_wk_reservation_starttime;
            } else {
                $start_ts = EZDate::getLocateTime($_wk_reservation_starttime, $server_timezone, $reservation_place);
            }
//            $request['start'] = date("Y-m-d H:i:s", $_ts_local);
            $request['start'] = EZDate::getZoneDate("Y-m-d H:i:s", $reservation_place, $start_ts);
            $reservation_starttime = $request['start'];
        }
        if ($_wk_reservation_endtime) {
            if (is_numeric($_wk_reservation_endtime)) {
                $end_ts = $_wk_reservation_endtime;
            } else {
                $end_ts = EZDate::getLocateTime($_wk_reservation_endtime, $server_timezone, $reservation_place);
            }
//            $request['end'] = date("Y-m-d H:i:s", $_ts_local);
            $request['end'] = EZDate::getZoneDate("Y-m-d H:i:s", $reservation_place, $end_ts);
            $reservation_endtime = $request['end'];
        }
        // 登録内容
        $reservation_data["info"] = array(
            "user_key"               => $user_key,
            "room_key"               => $room_key,
            "reservation_name"       => $reservation_name,
            "reservation_place"      => $reservation_place,
            "reservation_starttime"  => $reservation_starttime,
            "reservation_endtime"    => $reservation_endtime,
            "sender"                 => $sender_name,
            "sender_mail"            => $sender_email,
            "mail_body"              => $reservation_info,
            "room_option"            => $room_info['options'],
            "is_multicamera"         => $is_multicamera,
            "is_telephone"           => $is_telephone,
            "is_h323"                => $is_h323,
            "is_mobile_phone"        => $is_mobile_phone,
            "is_high_quality"        => $is_high_quality,
            "is_desktop_share"       => $is_desktop_share,
            "is_ssl"                 => $is_ssl,
            "is_invite"              => $is_invite,
            "is_record"              => $is_record,
            "is_cabinet"             => $is_cabinet,
            );
        if (!$this->config->get("IGNORE_MENU", "teleconference") && $room_info['options']['teleconference']){
            if ($teleconf_flg) {
                $reservation_data["info"]["tc_type"] = "pgi";
            } else {
                $reservation_data["info"]["tc_type"] = "voip";
            }
            if ($reservation_data["info"]['tc_type'] == 'pgi') {
                if (!$use_pgi_dialin && !$use_pgi_dialin_free && !$use_pgi_dialin_lo_call && !$use_pgi_dialout) {
                    return $this->display_error(100, "Please select the dial method for the teleconferencing.");
                }else{
                    $reservation_data['info']['use_pgi_dialin']         = $use_pgi_dialin;
                    $reservation_data['info']['use_pgi_dialin_free']    = $use_pgi_dialin_free;
                    $reservation_data['info']['use_pgi_dialout']        = $use_pgi_dialout;
                    $reservation_data['info']['use_pgi_dialin_lo_call'] = $use_pgi_dialin_lo_call;
                }
            }else{
                $reservation_data['info']['use_pgi_dialin']         = $room_info['room_info']['use_pgi_dialin'];
                $reservation_data['info']['use_pgi_dialin_free']    = $room_info['room_info']['use_pgi_dialin_free'];
                $reservation_data['info']['use_pgi_dialout']        = $room_info['room_info']['use_pgi_dialout'];
                $reservation_data['info']['use_pgi_dialin_lo_call'] = $room_info['room_info']['use_pgi_dialin_lo_call'];
            }
        }
        // 予約会議登録
        if ($reservation_pw) {
            $reservation_data["pw_flg"] = 1;
            $reservation_data["info"]["reservation_pw"] = $reservation_pw;
            $reservation_data["info"]["reservation_pw_type"] = ($reservation_pw_type == 1) ? 2 : 1;
        }
        // 入力チェック
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
                "allow" => array_keys($this->session->get("room_info")),
            ),
            "name" => array(
                "required" => true,
                "maxlen" => 100,
            ),
            "start" => array(
                "required" => true,
                "datetime" => true,
            ),
            "end" => array(
                "required" => true,
                "datetime" => true,
            ),
            API_TIME_ZONE => array(
                "allow" =>  array_keys($this->get_timezone_list()),
                ),
            "sender_name" => array(
                "maxlen" => 50,
            ),
            "sender_email" => array(
                "email" => true,
            ),
            "info" => array(
                "maxlen" => 10000,
            ),
            "password" => array(
                "minlen" => 6,
                "maxlen" => 16,
                "alnum" => true,
            ),
            "password_type" => array(
                "allow" => array("0", "1")
            ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
            ),
            "is_multicamera" => array(
                "allow" => array("0", "1")
            ),
            "is_telephone" => array(
                "allow" => array("0", "1")
            ),
            "is_h323" => array(
                "allow" => array("0", "1")
            ),
            "is_mobile_phone" => array(
                "allow" => array("0", "1")
            ),
            "is_high_quality" => array(
                "allow" => array("0", "1")
            ),
            "is_desktop_share" => array(
                "allow" => array("0", "1")
            ),
            "is_ssl" => array(
                "allow" => array("0", "1")
            ),
            "is_invite" => array(
                "allow" => array("0", "1")
            ),
            "is_record" => array(
                "allow" => array("0", "1")
            ),
            "is_cabinet" => array(
                "allow" => array("0", "1")
            ),
        );
        $err_obj = $this->error_check($request, $rules);
        // 開始、終了時間をチェック
        $limit_start_time = time() - (2 * 24 * 3600);
        $limit_end_time = time() + (367 * 24 * 3600);
        if (strtotime($request["start"]) <= $limit_start_time) {
            $err_obj->set_error("start", "220400");
        }
        // 開始日が指定範囲外
        if (strtotime($request["end"]) >= $limit_end_time) {
            $err_obj->set_error("end", "220401");
        }
        // 日付が逆
        if (strtotime($request["start"]) >= strtotime($request["end"])) {
            $err_obj->set_error("end", "220402");
        }
        // 終了時間が現時刻より前だった場合のエラー処理
        if ($end_ts < time()) {
            $err_obj->set_error("end", "220403");
        }
        // 重複一覧
        $start_dt = EZDate::getZoneDate("Y-m-d H:i:s", $server_timezone, $start_ts);
        $end_dt = EZDate::getZoneDate("Y-m-d H:i:s", $server_timezone, $end_ts);
        require_once("classes/dbi/reservation.dbi.php");
        $obj_Reservation = new ReservationTable($this->get_dsn());
        $where = "room_key = '".addslashes($request[API_ROOM_ID])."'" .
                " AND reservation_status = 1" .
                " AND reservation_endtime != '".$start_dt."'" .
                " AND reservation_starttime != '".$end_dt."'" .
                " AND (" .
                "( reservation_starttime BETWEEN '".$start_dt."' AND '".$end_dt."' )" .
                " OR ( reservation_endtime BETWEEN '".$start_dt."' AND '".$end_dt."' )" .
                " OR ( '".$start_dt."' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '".$end_dt."' BETWEEN reservation_starttime AND reservation_endtime ) )";
        $duplicate_list = $obj_Reservation->getList($where, array("reservation_starttime" => "asc"));
        if ($duplicate_list) {
//            $this->logger2->info($duplicate_list);
            $err_obj->set_error("start", "220404");
        }
        // 参加者更新
        if ($_guests) {
            // メンバー
            $objMember = new MemberTable($this->get_dsn());
            // 本当は直接招待者のみ取得したいが、N2MY_Reservation::getParticipantListが、reservation_keyで取得している。こちらを変更する必要有り
            $error_info = array();
            $timezone_list = array_keys($this->get_timezone_list());
            $timezone_list[] = 100;
            $guest_rules = array(
                "name"     => array(
                    "required" => true,
                    "maxlen" => 50,
                    ),
                "email"     => array(
//                    "required" => true,
                    "email" => true,
                    ),
                "timezone"     => array(
                    "allow" =>  $timezone_list,
                    ),
                "lang"     => array(
                    "allow" => array_keys($this->get_language_list()),
                    ),
                "type"    => array(
                    "allow" => array("audience"),
                    ),
                );
            if ($user_info["account_model"] == "member") {
                $guest_rules["email"]["required"] == true;
            }
            // 一括登録対応
            $guests = array();
            $guests_error = array();
            foreach($_guests as $key => $guest) {
                // zh -> zh-cnに変換。
                if($guest['lang'] == 'zh') {
                    $guest['lang'] = EZLanguage::getLang(EZLanguage::getLangCd($guest['lang']));
                }
                $type = ($type == "audience") ? 1 : "";
                // メンバーID指定時はメンバー情報から参照
                $member_key = null;
                if ($guest['member_id']) {
                    $where = "user_key = ".$user_info["user_key"].
                        " AND member_status = 0" .
                        " AND member_id = '".addslashes($guest['member_id'])."'";
                    if ($member_info = $objMember->getRow($where)) {
                        $member_key = $member_info["member_key"];
                        $guest["name"]      = $member_info["member_name"];
                        $guest["email"]     = $member_info["member_email"];
                        $guest["timezone"]  = ($guest["timezone"])  ? $guest["timezone"] : $member_info["timezone"];
                        $guest["lang"]      = ($guest["lang"])      ? $guest["lang"]     : $member_info["lang"];

                    }
                }
                // 入力チェック
                $guest_validator = new EZValidator($guest);
                foreach($guest_rules as $field => $rule) {
                    $guest_validator->check($field, $rule);
                }
                // メンバーID指定があるが、存在しない
                if ($guest['member_id'] && !$member_info) {
                    $guest_validator->set_error("member_id", "220405");
                }
                //
                if (EZValidator::isError($guest_validator)) {
                    $errors = $this->get_error_info($guest_validator);
                    foreach($errors['errors'] as $error){
                        $guests_error[] = array(
                        'key'        => $key,
                        'field'      => $error['field'],
                        'err_cd'     => $error['err_cd'],
                        'err_msg'    => $error['err_msg'],
                        );
                    }
                }
                // メールアドレスがないユーザーが招待者と見なさない
                if ($guest["name"]) {
                    $guests[$key] = array(
                        "member_key"      => $member_key,
                        "name"            => $guest["name"],
                        "email"           => $guest["email"],
                        "timezone"        => ($guest["timezone"]) ? $guest["timezone"] : 100,
                        "lang"            => ($guest["lang"]) ? $guest["lang"] : $this->session->get("lang"),
                        "type"            => $guest["type"],
                        "r_organizer_flg" => 0,
                    );
                }
            }
            // 入力エラー
            if ($guests_error) {
                $err_obj->set_error("guests", "220408");
            } else {
                // 人数制限
                $normal_num = 0;
                $audience_num = 0;
                // 入力チェック
                foreach ($guests as $key => $guest) {
                    $guests[$key]["type"] = ($guest["type"] == "audience") ? 1 : "";
                    if ($guest["email"]) {
                        if ($guests[$key]["type"] == 1) {
                            $audience_num++;
                        } else {
                            $normal_num++;
                        }
                    }
                }
                $reservation_data["guest_flg"] = 1;
                $reservation_data["send_mail"] = $send_mail;
                $reservation_data["guests"] = $guests;
                // 通常人数確認
                if ($room_info["room_info"]["max_seat"] < $normal_num) {
                    $err_obj->set_error("guests", "220406", $room_info["room_info"]["max_seat"]);
                // オーディエンス確認
                } elseif ($room_info["room_info"]["max_audience_seat"] < $audience_num) {
                    $err_obj->set_error("guests", "220407", $room_info["room_info"]["max_audience_seat"]);
                }
            }
        }
        // 入力エラー
        if (EZValidator::isError($err_obj)) {
            $error_info = $this->get_error_info($err_obj);
            if ($guests_error) {
                foreach($error_info['errors'] as $key => $error) {
                    if ($error['field'] == 'guests') {
                        $error_info['errors'][$key]['detail'] = $guests_error;
                    }
                }
            }
            return $this->display_error(100, "PARAMETER_ERROR", $error_info);
        }
        $result = $this->objReservation->add($reservation_data);
        $guests = $this->_format_guests($result["guests"]);

        $reservation_id = $result["reservation_session"];
        $meeting_ticket = $this->objReservation->obj_Reservation->getOne("reservation_session = '".addslashes($reservation_id)."'", "meeting_key");
        $meeting_id = $this->ticket_to_session($meeting_ticket);

        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $result["reservation_session"]);
        $encrypted_data = str_replace("/", "-", $encrypted_data);
        $encrypted_data = str_replace("+", "_", $encrypted_data);
        $data = array(
            API_RESERVATION_ID  => $result["reservation_session"],
            API_MEETING_ID      => $meeting_id,
            "url" => N2MY_BASE_URL."y/".$encrypted_data,
            "guests" => $guests,
            );
        return $this->output($data);
    }

    /**
     * 予約変更
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string meeting_name 会議名
     * @param datetime start_time 変更後開始時間
     * @param datetime end_time 変更後終了時間
     * @return boolean
     */
    function action_update()
    {
        $request = $this->request->getAll();
        $user_info = $this->session->get("user_info");
        $_room_info = $this->session->get("room_info");
        $user_key  = $user_info["user_key"];
        $reservation_session        = $this->request->get(API_RESERVATION_ID);
        $reservation_name           = $this->request->get("name");
        $reservation_place          = $this->request->get(API_TIME_ZONE, $this->session->get('time_zone')); // セッションの値から取得
        $_wk_reservation_starttime  = $this->request->get("start");
        $_wk_reservation_endtime    = $this->request->get("end");
        //$password                   = $this->request->get("password");
        $change_password            = $this->request->get("change_password", 0);
        $reservation_pw             = $this->request->get("new_password");
        $reservation_pw_type        = $this->request->get("password_type", 0);
        $sender_name                = $this->request->get("sender_name");
        $sender_email               = $this->request->get("sender_email");
        $reservation_info           = $this->request->get("info");
        $_guests                    = $this->request->get("guests");
        $guest_flg                  = $this->request->get("guest_flg", 1);
        $send_mail                  = $this->request->get("send_mail", 1);
        $teleconf_flg               = $this->request->get('teleconf_flg', 0);
        $use_pgi_dialin             = $this->request->get('use_pgi_dialin', 0);
        $use_pgi_dialin_free        = $this->request->get('use_pgi_dialin_free', 0);
        $use_pgi_dialin_lo_call     = $this->request->get('use_pgi_dialin_lo_call', 0);
        $use_pgi_dialout            = $this->request->get('use_pgi_dialout', 0);
        // メンバーログインで差出人が未入力の場合は自動補完
        if ($member_info = $this->session->get('member_info')) {
            $this->logger->info($member_info);
            // メンバー又は、
            if ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") {
                // 名前
                if ($request['sender_name'] == "") {
                    $sender_name = $member_info['member_name'];
                    $request['member_name'] = $sender_name;
                }
                // メール
                if ($request['sender_email'] == "") {
                    $sender_email = $member_info['member_email'];
                    $request['sender_email'] = $sender_email;
                }
            }
        }
        // 時差変換
        $server_timezone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        if ($_wk_reservation_starttime) {
            if (is_numeric($_wk_reservation_starttime)) {
                $start_ts = $_wk_reservation_starttime;
            } else {
                $start_ts = EZDate::getLocateTime($_wk_reservation_starttime, $server_timezone, $reservation_place);
            }
//            $request['start'] = date("Y-m-d H:i:s", $_ts_local);
            $request['start'] = EZDate::getZoneDate("Y-m-d H:i:s", $reservation_place, $start_ts);
            $reservation_starttime = $request['start'];
        }
$this->logger2->info(array($_wk_reservation_starttime, $reservation_place, $server_timezone, $reservation_starttime, date("Y-m-d H:i:s", $start_ts)));
        if ($_wk_reservation_endtime) {
            if (is_numeric($_wk_reservation_endtime)) {
                $end_ts = $_wk_reservation_endtime;
            } else {
                $end_ts = EZDate::getLocateTime($_wk_reservation_endtime, $server_timezone, $reservation_place);
            }
//            $request['end'] = date("Y-m-d H:i:s", $_ts_local);
            $request['end'] = EZDate::getZoneDate("Y-m-d H:i:s", $reservation_place, $end_ts);
            $reservation_endtime = $request['end'];
        }
$this->logger2->info(array($_wk_reservation_endtime, $reservation_place, $server_timezone, $reservation_endtime, date("Y-m-d H:i:s", $end_ts)));

//        if ($request['start']) {
//            $_tz_before = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
//            if (is_numeric($request['start'])) {
//                $_ts_local = $request['start'];
//            } else {
//                $_ts_local = EZDate::getLocateTime($request['start'], $reservation_place, $_tz_before);
//            }
//            $request['start'] = date("Y-m-d H:i:s", $_ts_local);
//            $reservation_starttime = $request['start'];
//        }
//        if ($request['end']) {
//            if (is_numeric($request['end'])) {
//                $_ts_local = $request['end'];
//            } else {
//                $_ts_local = EZDate::getLocateTime($request['end'], $reservation_place, $_tz_before);
//            }
//            $request['end'] = date("Y-m-d H:i:s", $_ts_local);
//            $reservation_endtime = $request['end'];
//        }
        //
        $reservation_data["info"] = array(
            "reservation_session"    => $reservation_session,
            "reservation_name"       => $reservation_name,
            "reservation_place"      => $reservation_place,
            "reservation_starttime"  => $reservation_starttime,
            "reservation_endtime"    => $reservation_endtime,
            "sender"                 => $sender_name,
            "sender_mail"            => $sender_email,
            "mail_body"              => $reservation_info,
            );

        $reservation_info = $this->objReservation->getDetail($reservation_session);
        require_once ("classes/dbi/room.dbi.php");
        $room_obj = new RoomTable($this->get_dsn());
        $use_teleconf = $room_obj->getRow("room_key = '".$reservation_info["info"]["room_key"]."'", "use_teleconf");

        if (!$this->config->get("IGNORE_MENU", "teleconference") && $use_teleconf){
            if ($teleconf_flg) {
                $reservation_data["info"]["tc_type"] = "pgi";
            } else {
                $reservation_data["info"]["tc_type"] = "voip";
            }
            if ($reservation_data["info"]['tc_type'] == 'pgi') {
                if (!$use_pgi_dialin && !$use_pgi_dialin_free && !$use_pgi_dialin_lo_call && !$use_pgi_dialout) {
                    return $this->display_error(100, "Please select the dial method for the teleconferencing.");
                }else{
                    $reservation_data['info']['use_pgi_dialin']         = $use_pgi_dialin;
                    $reservation_data['info']['use_pgi_dialin_free']    = $use_pgi_dialin_free;
                    $reservation_data['info']['use_pgi_dialout']        = $use_pgi_dialout;
                    $reservation_data['info']['use_pgi_dialin_lo_call'] = $use_pgi_dialin_lo_call;
                }
            }else{
                $reservation_data['info']['use_pgi_dialin']         = $room_info['room_info']['use_pgi_dialin'];
                $reservation_data['info']['use_pgi_dialin_free']    = $room_info['room_info']['use_pgi_dialin_free'];
                $reservation_data['info']['use_pgi_dialout']        = $room_info['room_info']['use_pgi_dialout'];
                $reservation_data['info']['use_pgi_dialin_lo_call'] = $room_info['room_info']['use_pgi_dialin_lo_call'];
            }
        }
        // 入力チェック
        $rules = array(
            "name" => array(
                "required" => true,
                "maxlen" => 100,
            ),
            "start" => array(
                "required" => true,
                "datetime" => true,
            ),
            "end" => array(
                "required" => true,
                "datetime" => true,
            ),
            API_TIME_ZONE => array(
                "allow" =>  array_keys($this->get_timezone_list()),
                ),
            "sender_name" => array(
                "maxlen" => 50,
            ),
            "sender_email" => array(
                "email" => true,
            ),
            "info" => array(
                "maxlen" => 10000,
            ),
            "new_password" => array(
                "minlen" => 6,
                "maxlen" => 16,
                "alnum" => true,
            ),
            "password_type" => array(
                "allow" => array("0", "1")
            ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
            ),
            "is_multicamera" => array(
                "allow" => array("0", "1")
            ),
            "is_telephone" => array(
                "allow" => array("0", "1")
            ),
            "is_h323" => array(
                "allow" => array("0", "1")
            ),
            "is_mobile_phone" => array(
                "allow" => array("0", "1")
            ),
            "is_high_quality" => array(
                "allow" => array("0", "1")
            ),
            "is_desktop_share" => array(
                "allow" => array("0", "1")
            ),
            "is_ssl" => array(
                "allow" => array("0", "1")
            ),
            "is_invite" => array(
                "allow" => array("0", "1")
            ),
            "is_record" => array(
                "allow" => array("0", "1")
            ),
            "is_cabinet" => array(
                "allow" => array("0", "1")
            ),
        );
        $err_obj = $this->error_check($request, $rules);
        // 開始、終了時間をチェック
        $limit_start_time = time() - (2 * 24 * 3600);
        $limit_end_time = time() + (367 * 24 * 3600);
        if (strtotime($request["start"]) <= $limit_start_time) {
            $err_obj->set_error("start", "220501");
        }
        // 開始日が指定範囲外
        if (strtotime($request["end"]) >= $limit_end_time) {
            $err_obj->set_error("end", "220502");
        }
        // 日付が逆
        if (strtotime($request["start"]) >= strtotime($request["end"])) {
            $err_obj->set_error("end", "220503");
        }
        // 終了時間が現時刻より前だった場合のエラー処理
$this->logger2->info(array($end_ts, time()));
        if ($end_ts < time()) {
//        if (strtotime($request["end"]) < time() ) {
            $err_obj->set_error("end", "220504");
        }
        // 予約会議登録
        if (!$_reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "220500");
        } else {
            // 重複一覧
            $start_dt = EZDate::getZoneDate("Y-m-d H:i:s", $server_timezone, $start_ts);
            $end_dt = EZDate::getZoneDate("Y-m-d H:i:s", $server_timezone, $end_ts);
            require_once("classes/dbi/reservation.dbi.php");
            $obj_Reservation = new ReservationTable($this->get_dsn());
            $where = "room_key = '".addslashes($_reservation_data["info"]["room_key"])."'" .
                " AND reservation_status = 1" .
                " AND reservation_endtime != '".$start_dt."'" .
                " AND reservation_starttime != '".$end_dt."'" .
                " AND (" .
                "( reservation_starttime between '".$start_dt."' AND '".$end_dt."' )" .
                " OR ( reservation_endtime between '".$start_dt."' AND '".$end_dt."' )" .
                " OR ( '".$start_dt."' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '".$end_dt."' BETWEEN reservation_starttime AND reservation_endtime ) )".
                " AND reservation_session != '" . $request[API_RESERVATION_ID]."'";
//            $this->logger2->info($where);
            $duplicate_list = $obj_Reservation->getList($where, array("reservation_starttime" => "asc"));
            if ($duplicate_list) {
                $this->logger2->info($duplicate_list);
                $err_obj->set_error("start", "220505");
            }
            $reservation_data["info"]["room_key"] = $_reservation_data["info"]["room_key"];
            // パスワード
            switch ($change_password) {
                // 変更なし
                case 0:
                    $reservation_data["pw_flg"] = ($_reservation_data["info"]["reservation_pw"]) ? 1 : 0;
                    $reservation_data["info"]["reservation_pw"] = $_reservation_data["info"]["reservation_pw"];
                    break;
                // 変更あり
                case 1:
                    if ($reservation_pw) {
                        $reservation_data["pw_flg"] = 1;
                        $reservation_data["info"]["reservation_pw"] = $reservation_pw;
                    } else {
                        $reservation_data["pw_flg"] = 0;
                        $reservation_data["info"]["reservation_pw"] = "";
                    }
                    break;
            }
            $reservation_data["info"]["reservation_pw_type"] = ($reservation_pw_type == 1) ? 2 : 1;
//            // パスワードチェック
//            if ($_reservation_data["info"]["reservation_pw"]) {
//                if (($password != $_reservation_data["info"]["reservation_pw"]) && ($reservation_pw != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "220012");
//                }
//            }
            // ログインユーザ以外の予約
            if ($_reservation_data["info"]["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "220500");
            }
            $reservation_data["info"]["meeting_key"] = $_reservation_data["info"]["meeting_key"];
        }
        $reservation_data["info"]["reservation_key"] = $_reservation_data["info"]["reservation_key"];
        // 参加者更新
        $reservation_data["guest_flg"] = $guest_flg;
        $guests = array();
        $guests_error = array();
        if ($_guests) {
            // メンバー
            $objMember = new MemberTable($this->get_dsn());
            $error_info = array();
            $timezone_list = array_keys($this->get_timezone_list());
            $timezone_list[] = 100;
            $guest_rules = array(
                "name"     => array(
                    "required" => true,
                    "maxlen" => 50,
                    ),
                "email"     => array(
//                    "required" => true,
                    "email" => true,
                    ),
                "timezone"     => array(
                    "allow" =>  $timezone_list,
                    ),
                "lang"     => array(
                    "allow" => array_keys($this->get_language_list()),
                    ),
                "type"    => array(
                    "allow" => array("audience"),
                    ),
                );
            if ($user_info["account_model"] == "member") {
                $guest_rules["email"]["required"] == true;
            }
            // 一括登録対応
            foreach($_guests as $key => $guest) {
                $type = ($type == "audience") ? 1 : "";
                // メンバーID指定時はメンバー情報から参照
                $member_key = null;
                if ($guest['member_id']) {
                    $where = "user_key = ".$user_info["user_key"].
                        " AND member_status = 0" .
                        " AND member_id = '".addslashes($guest['member_id'])."'";
                    if ($member_info = $objMember->getRow($where)) {
                        $member_key = $member_info["member_key"];
                        $guest["name"]      = $member_info["member_name"];
                        $guest["email"]     = $member_info["member_email"];
                        $guest["timezone"]  = ($guest["timezone"])  ? $guest["timezone"] : $member_info["timezone"];
                        $guest["lang"]      = ($guest["lang"])      ? $guest["lang"]     : $member_info["lang"];
                    }
                }
                $guests[$key] = array(
                    "reservation_key" => $_reservation_data["info"]["reservation_key"],
                    "member_key"      => $member_key,
                    "name"            => $guest["name"],
                    "email"           => $guest["email"],
                    "timezone"        => ($guest["timezone"]) ? $guest["timezone"] : 100,
                    "lang"            => ($guest["lang"]) ? $guest["lang"] : $this->session->get("lang"),
                    "type"            => $guest["type"],
                    "r_user_session"  => $guest["guest_id"],
                    "r_organizer_flg" => 0,
                );
                // メールアドレスがないユーザーが招待者と見なさない
                $guest_validator = new EZValidator($guest);
                foreach($guest_rules as $field => $rule) {
                    $guest_validator->check($field, $rule);
                }
                // メンバーID指定があるが、存在しない
                if ($guest['member_id'] && !$member_info) {
                    $guest_validator->set_error("member_id", "220405");
                }
                if (EZValidator::isError($guest_validator)) {
                    $errors = $this->get_error_info($guest_validator);
                    foreach($errors['errors'] as $error){
                        $guests_error[] = array(
                        'key'        => $key,
                        'field'      => $error['field'],
                        'err_cd'     => $error['err_cd'],
                        'err_msg'    => $error['err_msg'],
                        );
                    }
                }
            }
            // 人数制限
            $normal_num = 0;
            $audience_num = 0;
            // 入力チェック
            foreach ($guests as $key => $guest) {
                $guests[$key]["type"] = ($guest["type"] == "audience") ? 1 : "";
                if ($guest["email"]) {
                    if ($guests[$key]["type"] == 1) {
                        $audience_num++;
                    } else {
                        $normal_num++;
                    }
                }
            }
            // 入力エラー
            if ($guests_error) {
                $err_obj->set_error("guests", "220409");
            } else {
                $room_info = $_room_info[$_reservation_data["info"]["room_key"]];
                if ($room_info["room_info"]["max_seat"] < $normal_num) {
                    $err_obj->set_error("guests", "220507", $room_info["room_info"]["max_seat"]);
                }
                if ($room_info["room_info"]["max_audience_seat"] < $audience_num) {
                    $err_obj->set_error("guests", "220508", $room_info["room_info"]["max_audience_seat"]);
                }
            }
        }
        $reservation_data["guests"] = $guests;
        $reservation_data["send_mail"] = $send_mail;
        // エラー確認
        if (EZValidator::isError($err_obj)) {
            $error_info = $this->get_error_info($err_obj);
            if ($guests_error) {
                foreach($error_info['errors'] as $key => $error) {
                    if ($error['field'] == 'guests') {
                        $error_info['errors'][$key]['detail'] = $guests_error;
                    }
                }
            }
            return $this->display_error(100, "PARAMETER_ERROR", $error_info);
        } else {
            $result = $this->objReservation->update($reservation_data);
            $guests = $this->_format_guests($result["guests"]);
            return $this->output($guests);
        }
    }

    /**
     * 予約削除
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return boolean
     */
    function action_delete()
    {
        $request = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $password               = $this->request->get("password");
        $user_info              = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "221100");
        } else {
            $info = $reservation_data["info"];
//            // パスワードチェック
//            if ($info["reservation_pw"]) {
//                if (($password != $info["reservation_pw"]) && ($reservation_data != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "220012");
//                }
//            }
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "221100");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $result = $this->objReservation->cancel($reservation_session);
            return $this->output();
        }
    }

    /**
     * 招待者一覧
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return string $output 名前、メールアドレス、言語、タイムゾーンをxmlで出力
     */
    function action_get_invite()
    {
        $request = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $password               = $this->request->get("password");
        $user_info              = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "220002");
        } else {
            $info = $reservation_data["info"];
//            // パスワードチェック
//            if ($info["reservation_pw"]) {
//                if (($password != $info["reservation_pw"]) && ($reservation_data != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "220012");
//                }
//            }
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "220002");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            // 本当は直接招待者のみ取得したいが、N2MY_Reservation::getParticipantListが、reservation_keyで取得している。こちらを変更する必要有り
            $country_key = $this->session->get("country_key");
            foreach ($reservation_data["guests"] as $key => $guest) {
                $guest["invite_url"] = N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"];
                $guest[API_GUEST_ID] = $guest["r_user_session"];
                unset($guest["r_user_session"]);
                $guests[] = $guest;
            }
            $data["guests"]["guest"] = $guests;
            return $this->output($data);
        }
    }

    /**
     * 招待者追加
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string r_user_name 参加者名
     * @param string r_user_email 参加者メールアドレス
     * @param string r_user_lang 参加者言語
     * @param string r_user_timezone 参加者タイムゾーン
     * @return boolean
     */
    function action_add_invite()
    {
        $request  = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $password               = $this->request->get("password");
        $send_mail              = $this->request->get("send_mail", 1);
        $sender_name            = $this->request->get("sender_name");
        $sender_email           = $this->request->get("sender_email");
        $body                   = $this->request->get("info");
        $member_id              = $this->request->get("member_id");
        $name                   = $this->request->get("name");
        $email                  = $this->request->get("email");
        $type                   = $this->request->get("type");
        $timezone               = $this->request->get("timezone");
        $lang                   = $this->request->get("lang");
        $user_info              = $this->session->get("user_info");
        // メンバーログインで差出人が未入力の場合は自動補完
        if ($member_info = $this->session->get('member_info')) {
            $this->logger->info($member_info);
            // メンバー又は、
            if ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") {
                // 名前
                if ($request['sender_name'] == "") {
                    $sender_name = $member_info['member_name'];
                    $request['member_name'] = $sender_name;
                }
                // メール
                if ($request['sender_email'] == "") {
                    $sender_email = $member_info['member_email'];
                    $request['sender_email'] = $sender_email;
                }
            }
        }
        // 本当は直接招待者のみ取得したいが、N2MY_Reservation::getParticipantListが、reservation_keyで取得している。こちらを変更する必要有り
        $error_info = array();
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $timezone_list = array_keys($this->get_timezone_list());
        $timezone_list[] = 100;
        $guest_rules = array(
            "name"     => array(
                "required" => true,
                "maxlen" => 50,
                ),
            "email"     => array(
//                "required" => true,
                "email" => true,
                ),
            "timezone"     => array(
                "allow" =>  $timezone_list,
                ),
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "type"    => array(
                "allow" => array("audience"),
                ),
            );
        if ($user_info["account_model"] == "member") {
            $guest_rules["email"]["required"] == true;
        }
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "220600");
        } else {
            $info = $reservation_data["info"];
//            // パスワードチェック
//            if ($info["reservation_pw"]) {
//                if (($password != $info["reservation_pw"]) && ($reservation_data != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "220012");
//                }
//            }
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "220600");
            }
        }
        if (EZValidator::isError($err_obj)) {
            $error_info[] = $this->get_error_info($err_obj);
        }
        // メンバー
        $objMember = new MemberTable($this->get_dsn());
        // 一括登録対応
        $guests = array();
        if (is_array($name)) {
            foreach($name as $_key => $_val) {
                $type = ($type == "audience") ? 1 : "";
                // メンバーID指定時はメンバー情報から参照
                $member_key = null;
                if ($member_id[$_key]) {
                    $where = "user_key = ".$user_info["user_key"].
                        " AND member_status = 0" .
                        " AND member_id = '".addslashes($member_id[$_key])."'";
                    if (!$member_info = $objMember->getRow($where)) {
                        $err_obj->set_error("member_id", "220601");
                    } else {
                        $member_key     = $member_info["member_key"];
                        $name           = $member_info["member_name"];
                        $email          = $member_info["member_email"];
                        $timezone       = ($timezone[$_key])  ? $timezone[$_key] : $member_info["timezone"];
                        $lang           = ($lang[$_key])      ? $lang[$_key]     : $member_info["lang"];
                    }
                }
                // メールアドレスがないユーザーが招待者と見なさない
                if ($email[$_key]) {
                    $guests[$_key] = array(
                        "reservation_key" => $info["reservation_key"],
                        "name"            => $name[$_key],
                        "email"           => $email[$_key],
                        "timezone"        => ($timezone[$_key]) ? $timezone[$_key] : 100,
                        "lang"            => ($lang[$_key]) ? $lang[$_key] : $this->session->get("lang"),
                        "type"            => $type[$_key],
                        "r_user_session"  => $this->objReservation->getInviteId(),
                        "r_organizer_flg" => 0,
                    );
                }
            }
        } else {
            $type = ($type == "audience") ? 1 : "";
            // メンバーID指定時はメンバー情報から参照
            $member_key = null;
            if ($member_id) {
                $where = "user_key = ".$user_info["user_key"].
                    " AND member_status = 0" .
                    " AND member_id = '".addslashes($member_id)."'";
                if (!$member_info = $objMember->getRow($where)) {
                    $err_obj->set_error("member_id", "220601");
                } else {
                    $member_key = $member_info["member_key"];
                    $name       = $member_info["member_name"];
                    $email      = $member_info["member_email"];
                    $timezone   = ($timezone)  ? $timezone : $member_info["timezone"];
                    $lang       = ($lang)      ? $lang     : $member_info["lang"];
                }
            }
            $guests[] = array(
                "reservation_key" => $info["reservation_key"],
                "name"            => $name,
                "email"           => $email,
                "timezone"        => ($timezone) ? $timezone : 100,
                "lang"            => ($lang) ? $lang : $this->session->get("lang"),
                "type"            => $type,
                "r_user_session"  => $this->objReservation->getInviteId(),
                "r_organizer_flg" => 0,
            );
        }
        // 入力チェック
        foreach ($guests as $key => $guest) {
            $guest_err_obj = $this->error_check($guest, $guest_rules);
            if (EZValidator::isError($guest_err_obj)) {
                $error_info[] = $this->get_error_info($guest_err_obj);
            }
        }
        if ($error_info) {
            return $this->display_error(100, "PARAMETER_ERROR", $error_info);
        }
        $country_key = $this->session->get("country_key");
        foreach ($guests as $key => $guest) {
            if ($send_mail) {
                $info['sender']      = $sender_name;
                $info['sender_mail'] = $sender_email;
                $info['mail_body']   = $body;
            }
            $guest = $this->objReservation->participant_mail($guest, $info, "create", $send_mail);
            $invited[] = array(
                API_GUEST_ID => $guest["r_user_session"],
                "invite_url"   => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
            );
        }
        $data["guests"]["guest"] = $invited;
        return $this->output($data);
    }

    /**
     * 招待者削除
     *
     * @param string room_key ルームキー
     * @param string reservation_session
     * @param int r_user_key 参加者キー
     * @return boolean
     */
    function action_delete_invite()
    {
        $request  = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $password               = $this->request->get("password");
        $invite_id              = $this->request->get(API_GUEST_ID);
        $user_info              = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            API_GUEST_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        // 予約が存在しない
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "220700");
        } else {
            $info = $reservation_data["info"];
//            // パスワードチェック
//            if ($info["reservation_pw"]) {
//                if (($password != $info["reservation_pw"]) && ($reservation_data != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "220012");
//                }
//            }
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "220700");
            } else {
                $where = "reservation_key = " .$info["reservation_key"].
                        " AND r_user_session = '".addslashes($invite_id)."'";
                // 招待者IDが存在しない
                if (!$guest = $this->objReservation->obj_ReservationUser->getRow($where)) {
                    $err_obj->set_error(API_RESERVATION_ID, "220701");
                }
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $guest["lang"] = $this->request->get("lang", $this->session->get("lang"));
            $this->logger2->info(array($info, $guest));
            $this->objReservation->participant_mail($guest, $info, "deletelist");
            return $this->output();
        }
    }

    /**
     * 指定された招待者一覧で更新
     */
    private function _update_invite() {

    }

    /**
     * 予約の資料追加
     */
    function action_add_document() {
        $request = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $password               = $this->request->get("password");
        $name                   = $this->request->get("name");
        $file                   = $_FILES["file"];
        $user_info              = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$file) {
            $err_obj->set_error("file", "required");
        } else {
            if (!EZValidator::valid_file_ext($file["name"], split(",", $this->config->get("N2MY", "convert_format")))) {
                $err_obj->set_error("file", "file_ext", split(",", $this->config->get("N2MY", "convert_format")));
            }
        }
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "220800");
        } else {
            $info = $reservation_data["info"];
//            // パスワードチェック
//            if ($info["reservation_pw"]) {
//                if (($password != $info["reservation_pw"]) && ($reservation_data != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "220012");
//                }
//            }
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "220800");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $dir = $this->get_work_dir();
            $string = new EZString();
            $document_id = $string->create_id();
            $fileinfo = pathinfo($file["name"]);
            $tmp_name = $dir."/".$document_id.".".$fileinfo["extension"];
            move_uploaded_file($file["tmp_name"], $tmp_name);
            $name = ($name) ? $name : $fileinfo["basename"];
            $reservation_data = $this->objReservation->getDetail($reservation_session);
            $meeting_ticket = $reservation_data["info"]["meeting_key"];
            $document_id = $this->objReservation->addDocument($meeting_ticket, $tmp_name, $name);
            $data = array("document_id" => $document_id);
            return $this->output($data);
        }
    }

    /**
     * 更新
     */
    function action_update_document() {
        $request = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $password               = $this->request->get("password");
        $document_id            = $this->request->get("document_id");
        $user_info              = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "document_id" => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "220900");
        } else {
            $info = $reservation_data["info"];
//            // パスワードチェック
//            if ($info["reservation_pw"]) {
//                if (($password != $info["reservation_pw"]) && ($reservation_data != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "220012");
//                }
//            }
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "220900");
            }
            if (!array_key_exists($document_id, $reservation_data["documents"])) {
                $err_obj->set_error("document_id", "220901");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $data = array(
                "name" => $this->request->get("name"),
                "sort" => $this->request->get("sort"),
                );
            $this->objReservation->updateDocument($document_id, $data);
            return $this->output();
        }
    }

    /**
     * 削除
     */
    function action_delete_document() {
        $request = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $password               = $this->request->get("password");
        $document_id            = $this->request->get("document_id");
        $user_info              = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "document_id" => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "221000");
        } else {
            $info = $reservation_data["info"];
//            // パスワードチェック
//            if ($info["reservation_pw"]) {
//                if (($password != $info["reservation_pw"]) && ($reservation_data != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "220012");
//                }
//            }
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "221000");
            }
            if (!array_key_exists($document_id, $reservation_data["documents"])) {
                $err_obj->set_error("document_id", "221001");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $this->objReservation->deleteDocument($document_id);
            return $this->output();
        }
    }

    /**
     * 一覧
     */
    function action_get_document() {
        $request = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $password               = $this->request->get("password");
        $user_info              = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "220002");
        } else {
            $info = $reservation_data["info"];
//            // パスワードチェック
//            if ($info["reservation_pw"]) {
//                if (($password != $info["reservation_pw"]) && ($reservation_data != $user_info["user_admin_password"])) {
//                    $err_obj->set_error("password", "220012");
//                }
//            }
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "220002");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $document_list = array();
            if ($reservation_data["documents"]) {
                foreach ($reservation_data["documents"] as $_document) {
                    switch ($_document["status"]) {
                        case DOCUMENT_STATUS_WAIT :    $_document["status"] = "uploading"; break;
                        case DOCUMENT_STATUS_DO :      $_document["status"] = "pending"; break;
                        case DOCUMENT_STATUS_SUCCESS : $_document["status"] = "done"; break;
                        case DOCUMENT_STATUS_DELETE :  $_document["status"] = "delete"; break;
                        case DOCUMENT_STATUS_ERROR :
                        default :                      $_document["status"] = "error"; break;
                    }
                    unset($_document["create_datetime"]);
                    unset($_document["update_datetime"]);
                    $document_list[] = $_document;
                }
            }
            $data["documents"]["document"] = $document_list;
            return $this->output($data);
        }
    }

    // パスワード確認
    function action_confirm_password() {
        $request = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $password               = $this->request->get("password");
        $user_info              = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "221200");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "221200");
            }
            // パスワードチェック
            require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
            if ($info["reservation_pw"]) {
                if (($password != $info["reservation_pw"]) && ($reservation_data != EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_admin_password"]))) {
                    $err_obj->set_error("password", "221201");
                }
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            return $this->output();
        }
    }

    /*
     *電話会議連携の情報確認
    */
    function action_get_pgi(){
        $request   = $this->request->getAll();
        $reservation_session    = $this->request->get(API_RESERVATION_ID);
        $rules = array(
                API_RESERVATION_ID => array(
                        "required" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            //該当予約が無ければエラー
            $err_obj->set_error(API_RESERVATION_ID, "220002");
        }else{
            //電話連携関連の情報を収集しマージ
            require_once ("classes/dbi/room.dbi.php");
            $obj_Meeting  = new DBI_Meeting($this->get_dsn());
            $meeting_data = $obj_Meeting->getRow(sprintf("meeting_ticket='%s'", $reservation_data["info"]['meeting_key']));
            $teleconf_flg = ($meeting_data["tc_type"]=="pgi")? 1:0;
            if ($meeting_data['pgi_api_status'] == 'complete') {
                require_once("classes/pgi/PGiPhoneNumber.class.php");
                if ($meeting_data['use_pgi_dialin'] || $meeting_data['use_pgi_dialin_free']|| $meeting_data['use_pgi_dialin_lo_call'])  {
                    $main_phone_number = unserialize($meeting_data['pgi_phone_numbers']);
                    $main_phone_number = PGiPhoneNumber::toMultilangLocationName($main_phone_number, $this->_lang);
                    if (!$meeting_data['use_pgi_dialin']){
                        $main_phone_number = PGiPhoneNumber::extractFreeMain($main_phone_number);
                    } else {
                        $main_phone_number = PGiPhoneNumber::extractLocalMain($main_phone_number);
                    }
                }
            }
            if($teleconf_flg){
                $data = array(
                        "room_info"        => array("room_id"               => $meeting_data["room_key"],
                                                    "meeting_session_id"    => $meeting_data["meeting_session_id"],),
                        "main_phone_number"=> $main_phone_number,
                        "pgi_phone_numbers"=> unserialize($meeting_data['pgi_phone_numbers']),);
            }else{
                $pgi_info = array(
                        "pgi_info" => array(
                                "teleconf_flg"          => $teleconf_flg,),);
            }
        }
        if(EZValidator::isError($err_obj)){
            $this->logger2->info("test");
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }else{
            return $this->output($data);
        }
    }

    function action_preview_document() {
        require_once("classes/N2MY_Document.class.php");
        require_once("classes/dbi/meeting.dbi.php");
        $objDocument = new N2MY_Document($this->get_dsn());
        $objMeeting  = new MeetingTable($this->get_dsn());

        $request     = $this->request->getAll();
        $document_id = $this->request->get("document_id");
        $page        = $this->request->get("page", 1);
        $user_info   = $this->session->get("user_info");

        // 入力チェック
        $rules = array(
            "document_id" => array(
                "required" => true
                ),
            "page" => array(
                "required" => true,
                "integer" => true,
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        $documentInfo = $objDocument->getDetail( $document_id );
        if (!$documentInfo) {
            $err_obj->set_error("document_id", "220011");
        } else {
            $where = sprintf( "meeting_key='%s' AND is_deleted=0", $documentInfo["meeting_key"] );
            $document_user_key = $objMeeting->getOne($where, "user_key");
            if ($document_user_key != $user_info["user_key"]) {
                $err_obj->set_error("document_id", "220011");
            }
        }
        if (EZValidator::isError($err_obj)) {
            // エラー
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            // ファイル情報取得
            $fileInfo = $objDocument->getPreviewFileInfo( $documentInfo, $page );
            //resize
            $max_width = 480;
            $max_height = 320;
            $percent = 0.5;

            list($width, $height) = getimagesize($fileInfo["targetImage"]);
            $x_ratio = $max_width / $width;
            $y_ratio = $max_height / $height;

            if( ($width <= $max_width) && ($height <= $max_height) ){
                $tn_width = $width;
                $tn_height = $height;
            } elseif (($x_ratio * $height) < $max_height) {
                $tn_height = ceil($x_ratio * $height);
                $tn_width = $max_width;
            } else {
                $tn_width = ceil($y_ratio * $width);
                $tn_height = $max_height;
            }
            header('Content-type: image/jpeg');
            // 再サンプル
            $image_p = imagecreatetruecolor($tn_width, $tn_height);
            $image = imagecreatefromjpeg($fileInfo["targetImage"]);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
            imagejpeg($image_p, null, 100);
        }
    }

    function _format_guests($_guests) {
        $guests = array();
        if ($_guests) {
            $country_key = $this->session->get("country_key");
            foreach ($_guests as $key => $guest) {
                $guests[] = array(
                    "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key["country_key"].'&lang='.$guest["lang"],
                    API_GUEST_ID => $guest["r_user_session"],
                    );
            }
        }
        return $guests;
    }
}
$main = new N2MY_Meeting_Resevation_API();
$main->execute();
