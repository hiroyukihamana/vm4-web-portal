<?php

require_once("classes/N2MY_MeetingLog.class.php");
require_once("classes/N2MY_Api.class.php");

class N2MY_MeetingAdminMeetinglogAPI extends N2MY_Api
{

var $obj_MeetingLog = null;
var $obj_Meeting    = null;

    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    function auth()
    {
        $this->checkAuthorizationAdmin();
        $this->obj_MeetingLog = new N2MY_MeetingLog($this->get_dsn());
        $this->obj_Meeting    = new DBI_Meeting($this->get_dsn());
        // ログイン以外の場合は認証させる
    }

    /**
     * オンデマンド設定
     */
    function action_set_ondemand() {
        $meeting_id = $this->request->get("meeting_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "meeting_id" => $meeting_id,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "user_key = '".$user_info["user_key"]."'" .
                " AND meeting_session_id = '".addslashes($meeting_id)."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where);
        if (!$meeting_info) {
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        $this->obj_MeetingLog->controlPublish($meeting_info["room_key"], $meeting_id, "0");
        $this->add_operation_log('meetinglog_public', array(
        		'room_key'      => $meeting_info["room_key"],
        		'meeting_name'  => $meeting_info["meeting_name"]
        ));
        return $this->output();
    }

    /**
     * オンデマンド解除
     */
    function action_unset_ondemand() {
        $meeting_id = $this->request->get("meeting_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "meeting_id" => $meeting_id,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "user_key = '".$user_info["user_key"]."'" .
                " AND meeting_session_id = '".addslashes($meeting_id)."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where);
        if (!$meeting_info) {
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        $this->obj_MeetingLog->controlPublish($meeting_info["room_key"], $meeting_id, "1");
        $this->add_operation_log('meetinglog_unpublic', array(
        		'room_key'      => $meeting_info["room_key"],
        		'meeting_name'  => $meeting_info["meeting_name"]
        ));
        return $this->output();
    }

    /**
     * 保護設定
     */
    function action_set_protect() {
        $meeting_id = $this->request->get("meeting_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "meeting_id" => $meeting_id,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "user_key = '".$user_info["user_key"]."'" .
                " AND meeting_session_id = '".addslashes($meeting_id)."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where);
        if (!$meeting_info) {
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        $this->obj_MeetingLog->setProtect($meeting_info["room_key"], $meeting_id);
        $this->add_operation_log('meetinglog_lock', array(
        		'room_key'      => $meeting_info["room_key"],
        		'meeting_name'  => $meeting_info["meeting_name"]
        ));
        return $this->output();
    }

    /**
     * 保護解除
     */
    function action_unset_protect() {
        $meeting_id = $this->request->get("meeting_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "meeting_id" => $meeting_id,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "user_key = '".$user_info["user_key"]."'" .
                " AND meeting_session_id = '".addslashes($meeting_id)."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where);
        if (!$meeting_info) {
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        $this->obj_MeetingLog->unsetProtect($meeting_info["room_key"], $meeting_id);
        $this->add_operation_log('meetinglog_unlock', array(
        		'room_key'      => $meeting_info["room_key"],
        		'meeting_name'  => $meeting_info["meeting_name"]
        ));
        return $this->output();
    }

    /**
     * 映像削除
     */
    function action_delete_video() {
        $meeting_id = $this->request->get("meeting_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "meeting_id" => $meeting_id,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "user_key = '".$user_info["user_key"]."'" .
                " AND meeting_session_id = '".addslashes($meeting_id)."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where);
        if (!$meeting_info) {
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        // 契約した部屋か確認
        $this->obj_MeetingLog->deleteVideo($meeting_id);
        $this->add_operation_log('meetinglog_delete_video', array(
        		'room_key'      => $meeting_info["room_key"],
        		'meeting_name'  => $meeting_info["meeting_name"]
        ));
        return $this->output();
    }

    /**
     * 議事録削除
     */
    function action_delete_minutes() {
        $meeting_id = $this->request->get("meeting_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "meeting_id" => $meeting_id,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "user_key = '".$user_info["user_key"]."'" .
                " AND meeting_session_id = '".addslashes($meeting_id)."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where);
        if (!$meeting_info) {
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        // 契約した部屋か確認
        $this->obj_MeetingLog->deleteMinutes($meeting_id);
        $this->add_operation_log('meetinglog_delete_minutes', array(
        		'room_key'      => $meeting_info["room_key"],
        		'meeting_name'  => $meeting_info["meeting_name"]
        ));
        return $this->output();
    }

    /**
     * 削除
     */
    function action_delete() {
        $meeting_id = $this->request->get("meeting_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "meeting_id" => $meeting_id,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "user_key = '".$user_info["user_key"]."'" .
                " AND meeting_session_id = '".addslashes($meeting_id)."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where);
        if (!$meeting_info) {
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        // 契約した部屋か確認
        $this->obj_MeetingLog->delete($meeting_info["room_key"], $meeting_id);
        $this->add_operation_log('meetinglog_delete', array(
        		'room_key'      => $meeting_info["room_key"],
        		'meeting_name'  => $meeting_info["meeting_name"]
        ));
        return $this->output();
    }

    /**
     * パスワード設定
     */
    function action_set_password() {
        $meeting_id = $this->request->get("meeting_id");
        $user_info = $this->session->get("user_info");
        $password = $this->request->get("pw");
        $check_data = array(
            "meeting_id" => $meeting_id,
            "pw" => $password,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
            "pw" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "user_key = '".$user_info["user_key"]."'" .
                " AND meeting_session_id = '".addslashes($meeting_id)."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where);
        if (!$meeting_info) {
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        // 契約した部屋か確認
        $this->obj_MeetingLog->setPassword($meeting_info["room_key"], $meeting_id, $password);
        $this->add_operation_log('meetinglog_set_password', array(
        		'room_key'      => $meeting_info["room_key"],
        		'meeting_name'  => $meeting_info["meeting_name"]
        ));
        return $this->output();
    }

    /**
     * パスワード解除
     */
    function action_unset_password() {
        $meeting_id = $this->request->get("meeting_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "meeting_id" => $meeting_id,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "user_key = '".$user_info["user_key"]."'" .
                " AND meeting_session_id = '".addslashes($meeting_id)."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where);
        if (!$meeting_info) {
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        // 契約した部屋か確認
        $this->obj_MeetingLog->setPassword($meeting_info["room_key"], $meeting_id, "");
        $this->add_operation_log('meetinglog_unset_password', array(
        		'room_key'      => $meeting_info["room_key"],
        		'meeting_name'  => $meeting_info["meeting_name"]
        ));
        return $this->output();
    }

    /**
     * キャビネットダウンロード
     */
    function action_download_cabinet() {
        //* ファイルキャビネットのポリシーが決まっていないので一時凍結
        $userInfo = $this->session->get("user_info");
        $request = $this->request->getAll();
        $cabinetInfo = $this->objFileCabinet->getRow(sprintf("cabinet_id='%s'", $request["cabinet_id"]));
        $meetingInfo = $this->objMeeting->getRow(sprintf("meeting_key='%s'", $cabinetInfo["meeting_key"]), null, null, 1, 0, "user_key");
        $this->logger2->info(array($meetingInfo, $userInfo, $cabinetInfo));
        if ($meetingInfo["user_key"] == $userInfo["user_key"] &&
            1 == $cabinetInfo["status"]) {
            $fullpath = $this->objFileUpload->getFullPath( $cabinetInfo["file_cabinet_path"] );
            $this->objFileUpload->checkFile( $fullpath, $cabinetInfo["tmp_name"] );
            $targetFile = sprintf( "%s%s", $fullpath, $cabinetInfo["tmp_name"] );
            $content_length = filesize( $targetFile );
            $useragent = getenv("HTTP_USER_AGENT");

            $cabinetInfo["file_name"] = str_replace(" ","+",$cabinetInfo["file_name"]);

            // ie
            if( ereg( "MSIE", $useragent ) ) {
                $fileName = mb_convert_encoding( $cabinetInfo["file_name"] , "SJIS-win" );
                Header("Content-Disposition: attachment; filename=" . $fileName );
                Header('Pragma: private');
                Header('Cache-Control: private');
                Header("Content-Type: application/octet-stream-dummy");
            } else {
                if ( ereg( "Safari", $useragent ) ) {
                    //Safariの場合は全角文字が全て化けるので、何かしら固定のファイル名にして回避
                    $cabinetInfo["file_name"] = "";
                }
                Header("Content-Disposition: attachment; filename=" . $cabinetInfo["file_name"] );
                Header("Content-Type: application/octet-stream; name=" . $cabinetInfo["file_name"] );
            }
            Header("Content-Length: ". $content_length );
            readfile($targetFile);
            exit;
        } else
            $this->logger2->error(array($meetingInfo, $userInfo, $cabinetInfo));
            $this->render_fault();

        $meeting_id = $this->request->get("meeting_id");
        //*/
        $this->output();
    }

    /**
     * ECOメーター情報取得
     */
    function action_get_eco_info() {
        $meeting_id = $this->request->get("meeting_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "meeting_id" => $meeting_id,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $objParticipant = new DBI_Participant($this->get_dsn());
        $where = "user_key = '".$user_info["user_key"]."'" .
                " AND meeting_session_id = '".addslashes($meeting_id)."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where, "meeting_key, eco_co2, eco_move, eco_time, eco_fare, eco_info");
        if (!$meeting_info) {
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        $this->logger2->info(array($where, $meeting_info));
        $eco_info = ($meeting_info["eco_info"]) ? unserialize($meeting_info["eco_info"]) : array();
        $participant_where = "meeting_key = '".addslashes($meeting_info["meeting_key"])."'" .
                " AND is_active = 0".
                " AND use_count > 0";
        $participant_list = $objParticipant->getRowsAssoc($participant_where, null, null, null, "participant_id, participant_name, participant_station, use_count");
        $data = array(
            "eco_move" => $meeting_info["eco_move"],
            "eco_co2"  => $meeting_info["eco_co2"],
            "eco_fare" => $meeting_info["eco_fare"],
            "eco_time" => $meeting_info["eco_time"],
            "participants" => array(
                "participant" => $participant_list,
                ),
            );
        return $this->output($data);
    }

    /**
     * ECOメーター情報設定
     */
    function action_set_eco_info() {
        require_once('lib/EZLib/Transit.class.php');
        $uri = $this->config->get("N2MY", "transit_uri");
        $transit_log = realpath(dirname(__FILE__)."/../../../var/")."/transit.log";
        $Transit = Transit::factory( $uri, $transit_log );
        $meeting_id     = $this->request->get("meeting_id");
        $station_list   = $this->request->get("station");
        $endpoint       = $this->request->get("endpoint");
        $participants   = $this->session->get("participants");
        foreach($participants as $key => $participant) {
            $participants[$key] = $participant;
            $participants[$key]["participant_station"] = $station_list[$participant["participant_key"]];
        }
        $this->session->set("participants", $participants);
        $this->session->set("end_point", $endpoint);
        // 入力チェック
        $err = array();
        foreach($station_list as $key => $station) {
            if (!$station) {
                if ($endpoint == $key) {
                    $err[$key] = true;
                }
            } else {
                $ret = $Transit->getStationList($station, false, 50);
                if (!in_array($station, $ret)) {
                    $err[$key] = true;
                }
            }
        }
        if ($err) {
            return $this->render_eco_edit_form($err);
        }
        // 参加者の拠点情報を更新
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        $this->obj_Meeting = new MeetingTable($this->get_dsn());
        $objParticipant = new DBI_Participant($this->get_dsn());
        $where = "meeting_session_id = '".addslashes($meeting_id)."'";
        $meeting_info = $this->obj_Meeting->getRow($where);
        $this->logger2->info($station_list);
        if (!DB::isError($meeting_info)) {
            foreach($station_list as $participant_key => $station) {
                $where = "meeting_key = ".$meeting_info["meeting_key"].
                    " AND participant_key = '".addslashes($participant_key)."'";
                $data = array(
                    "participant_station" => $station,
                    "update_datetime"     => date("Y-m-d H:i:s"),
                );
                $objParticipant->update($data, $where);
            }
            // 経路検索結果取得
            $this->obj_Eco->createReport($meeting_info["meeting_key"], $station_list[$endpoint], false, true);
        }
        // 結果画面表示
        return $this->output();
    }

}
$main = new N2MY_MeetingAdminMeetinglogAPI();
$main->execute();