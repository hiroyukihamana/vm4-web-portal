<?php
require_once('classes/mgm/dbi/custom_domain_service.dbi.php');
require_once("classes/AppFrame.class.php");

class API_CdnAcceptor extends AppFrame
{
    function init() {
        $this->dsn = $this->get_dsn();
    }

    function default_view() {
        $this->render_default();
    }

    function render_default() {
        $obj_CustomDomainService      = new CustomDomainServiceTable( N2MY_MDB_DSN );
        $server_list = $obj_CustomDomainService->getRowsAssoc("status = 1");
        $web = parse_url(N2MY_BASE_URL);
        $cdn_list[] = $web["host"];
        foreach ($server_list as $server) {
            $cdn_list[] = $server["domain"];
        }
        $content_delivery_list = $this->config->getAll("CONTENT_DELIVERY_HOST");
        foreach ($content_delivery_list as $server) {
            $cdn_list[] = $server;
        }
        $this->template->assign( "cdn_list", $cdn_list );
        $this->display( "core/api/meeting_config/meeting/cdn_accepter.t.xml");
    }
}

$main = new API_CdnAcceptor();
$main->execute();