<?php
set_time_limit(0);
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Document.class.php");
require_once("lib/EZLib/EZUtil/EZRtmp.class.php");
require_once("lib/EZLib/EZUtil/EZFile.class.php");

class DocumentConverter extends AppFrame {

    var $_document_path = "";
    var $_process_kill_time = 60;
    var $doc_conf = array();
    var $doc_dir = null;
    var $dsn = null;
    var $db_host = null;
    var $obj_Document = null;
    // マルチスレッド対応（予定）
    var $process_no = null;

    /**
     * 初期化
     */
    function init() {
        $this->doc_conf = $this->config->getAll("DOCUMENT");
        $this->timeout = ($this->doc_conf["timeout"]) ? $this->doc_conf["timeout"] : 240;
//        $db_host = $this->request->get("db_host");
        $db_host = $_REQUEST["action_wb_upload"] ? $_REQUEST["db_host"] : $this->request->get("db_host");
        if ($db_host) {
            $this->dsn = $this->get_dsn_value($db_host);
            $this->db_host = $db_host;
        } else {
            die("error!!");
        }
        $this->obj_Document = new N2MY_Document($this->dsn, N2MY_DOC_DIR);
        if( $_REQUEST["action_wb_upload"] ){
            $this->action_wb_upload();exit;
        }
    }

    /**
     * 資料ID取得
     */
    function action_negotiate() {
        $document_id = $this->obj_Document->createID();
        print '<?xml version="1.0" encoding="UTF-8"?>'."\n";
        print '<response api_error="">'."\n";
        print '<document_id>'.$document_id.'</document_id>'."\n";
        print '</response>';
    }

    /**
     * 会議内資料アップロード用の対応
     */
    function action_wb_upload() {
        $path = trim( $_REQUEST["document_path"]);
        $meeting_session_id = $_REQUEST["meeting_key"];
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass(N2MY_MDB_DSN);
        $meeting_key = $obj_MGMClass->getMeetingID($meeting_session_id);
        if (!$path && $meeting_key) {
            require_once("classes/core/dbi/Meeting.dbi.php");
            require_once("classes/dbi/user.dbi.php");
            $objMeeting = new DBI_Meeting($this->dsn);
            $objUser = new UserTable($this->dsn);
            $where = "meeting_key = ".$meeting_key;
            $meeting_info = $objMeeting->getRow($where, "meeting_key,user_key,room_key");
            $where = "user_key = ".$meeting_info["user_key"];
            $user_id = $objUser->getOne($where, "user_id");
            $path = $user_id."/".$meeting_info["room_key"]."/";
        }
        $participant_id = $_REQUEST["participant_id"];
        $document_id = $_REQUEST["document_id"];
        $file = isset($_FILES["Filedata"]) ? $_FILES["Filedata"] : null;
        // 資料IDの指定がなければ作成(会議内の資料張り込み以外はこれを利用する)
        if (!$document_id) {
            $document_id = $this->obj_Document->createID();
        }
        $path .= "wb_".str_pad($meeting_key, 10, 0, STR_PAD_LEFT);
        $file["document_id"] = $document_id;
        $file["meeting_key"] = $meeting_key ? $meeting_key : 0;
        $file["label"]       = $_REQUEST["name"] ? $_REQUEST["name"] : $file["name"];
        $files[] = $file;
        // オプション(コールバックURLで資料アップロード)
        $ext = $_REQUEST["ext"] ? $_REQUEST["ext"] : "jpg";
        $format = $_REQUEST["format"] ? $_REQUEST["format"] : "bitmap";
        $option = array(
            "format"  => $format,
            "version" => $_REQUEST["version"] ? $_REQUEST["version"] : "as2",
            "ext"     => $ext,
            "x"       => $_REQUEST["x"] ? $_REQUEST["x"] : $this->config->get("DOCUMENT", "max_x", 640),
            "y"       => $_REQUEST["y"] ? $_REQUEST["y"] : $this->config->get("DOCUMENT", "max_y", 480),
            "success_url" => N2MY_LOCAL_URL."api/document/index.php" .
                "?action_dispatch=" .
                "&document_id=" .$document_id.
                "&db_host=".$this->db_host.
                "&meeting_key=".$meeting_key.
                "&participant_id=".$participant_id,
            "error_url" => N2MY_LOCAL_URL."api/document/index.php" .
                "?action_destruct=" .
                "&document_id=" .$document_id.
                "&db_host=".$this->db_host.
                "&meeting_key=".$meeting_key.
                "&participant_id=".$participant_id,
            "at_once" => 1,         // 会議中のモノから開始する
            );
        $option["is_storage"] = 0;
        $mode = "mail" == $_REQUEST["mode"] ? "mail" : "meeting";
        $this->_accept($files, $path, $mode, $option, $participant_id);
        print $document_id;
    }

    /**
     * ファイル受付
     */
    function action_accept() {
        $file = isset($_FILES["file"]) ? $_FILES["file"] : null;
        $path = trim($this->request->get("document_path"));
        if (is_array($file["name"])) {
            $names = $this->request->get("name");
            for($i = 0; $i < count($file["name"]); $i++) {
                $error = $file["error"][$i];
                if ($error === 0) {
                    $files[] = array(
                        "name"        => $file["name"][$i],
                        "type"        => $file["type"][$i],
                        "tmp_name"    => $file["tmp_name"][$i],
                        "error"       => $file["error"][$i],
                        "size"        => $file["size"][$i],
                        "label"        => isset($names[$i]) ? $names[$i] : $file["name"][$i],
                        "document_id" => $this->obj_Document->createID(),
                        "meeting_key" => $this->request->get("meeting_key", 0),
                    );
                }
            }
        } else {
            $file["document_id"] = $this->obj_Document->createID();
            $file["meeting_key"] = $this->request->get("meeting_key", 0);
            $file["label"]       = $this->request->get("name", $file["label"]);
            $files[] = $file;
        }
        $ext = $this->request->get("ext", "jpg");
        $format = $this->request->get("format", "bitmap");
        $option = array(
            "ext"    => $ext,
            "format" => $format,
            "version" => $this->request->get("version", "as2"),
            "sort"   => $this->request->get("sort"),
            "x"      => $this->request->get("x", $this->config->get("DOCUMENT", "max_x", 640)),
            "y"      => $this->request->get("y", $this->config->get("DOCUMENT", "max_y", 480)),
            );
        if( $this->request->get("db_data") == "storage"){
            $mode = "storage";
            $option["is_storage"] = 1;
        }else{
            $mode = "pre";
            $option["is_storage"] = 0;
        }
        $this->_accept($files, $path, $mode, $option);
        print $file["document_id"];
    }

    private function _accept($files, $path, $mode, $option, $participant_id=null) {
        // 不正なパスは不可
        if (@ereg("[^0-9a-zA-Z\_\/\-.]", $path)) {
            $this->logger2->warn($path);
            return false;
        } else {
            $path = ereg_replace("\/{1,}", '/', $path);
        }
        if (substr($path,-1,1) != "/") {
            $path .= "/";
        }
        // 資料変換リスト
        $this->logger2->debug(array(
            "path"   => $path,
            "files"  => $files,
            "option" => $option,
            ));
        foreach($files as $key => $file) {
            // 入力チェック
            if($this->obj_Document->check($file)) {
                $meeting_key = $file["meeting_key"];
                $document_id = $file["document_id"];
                $file_info = $this->obj_Document->file_info($file["name"]);
                // 画像、もしくはPDFの場合は無視される
//                if ("image" == $file_info["type"] || $file_info["extension"] == "pdf") {
                if ("image" == $file_info["type"]) {
                    $option["format"] = "bitmap";
                }
                if ("vector" == $option["format"]) {
                    $option["ext"] = "swf";
                }

                // 資料か、画像
                if ($file_info["type"] == "document" || $file_info["type"] == "image") {
                    // 一時保存
                    $tmp_file = N2MY_DOC_DIR.$path.$document_id.".".$file_info["extension"];
                    $tmp_dir = dirname($tmp_file);
                    if (!file_exists($tmp_dir)) {
                        EZFile::mkdir_r($tmp_dir, 0777);
                        chmod($tmp_dir, 0777);
                    }
                    // ファイルを移動
                    move_uploaded_file($file["tmp_name"], $tmp_file);
                    // 資料追加
                    $this->obj_Document->add($document_id, $path, $tmp_file, $mode, $file["label"], $meeting_key, $participant_id, $option["format"], $option["version"], $option["sort"],$option["is_storage"]);
                    // 変換の優先度
                    if ($option["at_once"]) {
                        $priority = 1; // 会議中の資料
                    } else {
                        if ($file_info["type"] == "image") {
                            $priority = 2; // 予約の画像
                        } else {
                            $priority = 3; // 予約の資料
                        }
                    }
                    // 資料変換開始
                    $this->convertDocument($path, $document_id, $file_info["extension"], $priority, $option);
                } else {
                }
            } else {
                $this->logger2->warn($file);
            }
        }
    }

    /**
     * ドキュメントファイル
     */
    function convertDocument($dir, $document_id, $extension, $priority, $opt) {
        $this->logger2->debug(array($dir, $document_id, $extension));
        // キューに入れる
        $this->obj_Document->addQueue($this->db_host, $dir, $document_id, $extension, $priority, $opt);
        if ($this->doc_conf["do_not_use_cron_convert"]) {
            $url = N2MY_LOCAL_URL.'api/document/index.php' .
                    '?action_convert=' .
                    '&db_host='.$this->db_host;
            // 変換中ならそのまま何もしない
            if ($jobStatus = $this->obj_Document->getActiveProcess()) {
                $passage_time = time() - strtotime($jobStatus["update_datetime"]);
                // ジョブが何もしないまま一定時間経過したら、何も動いてないとみなし、新たなジョブを作成
                if ($passage_time > $this->_process_kill_time) {
                    $this->logger2->warn(array($passage_time, time(), strtotime($jobStatus["update_datetime"]), date("Y-m-d H:i:s"), $jobStatus, $passage_time));
                    $this->obj_Document->stop($jobStatus["no"]);
                    $this->obj_Document->start($url);
                }
            } else {
                // 変換処理開始
                $this->obj_Document->start($url);
            }
        }
    }

    /**
     * 別プロセスとしてリクエストを発行し、キューにたまった資料を順次変換する
     */
    function action_convert($reset = true) {
        static $jobStatus;
        static $count = 0;
        // 初回のみ
        if (!$jobStatus) {
            $jobStatus = $this->obj_Document->getActiveProcess();
            $this->process_no = $jobStatus["no"];
        }
        // 次のドキュメント
        if ($reset) {
            $count = 0;
        }
        // キューからデータ取得
        $queue = $this->obj_Document->getQueue();
        if (isset($queue["document_id"])) {
            $document_id = $queue["document_id"];
            $input_file = N2MY_DOC_DIR.$queue["document_path"].$document_id.".".$queue["extension"];
            $file_info = $this->obj_Document->file_info($input_file);
            if ($file_info["type"] == "image") {
                // 画像
                $option = unserialize($queue["addition"]);
                $output_file = N2MY_DOC_DIR.$queue["document_path"]."/".$document_id."/".$document_id.".".$option["ext"];
                $ret = $this->obj_Document->convertImage($input_file, $output_file, $option);
                if ($ret) {
                    // 1枚の場合はファイル名をあわせる
                    if ($ret == 1) {
                        $_output_file = N2MY_DOC_DIR.$queue["document_path"].$document_id."/".$document_id."_1.".$option["ext"];
                        rename($output_file, $_output_file);
                    }
                    // 変換完了後にステータス変更
                    $this->obj_Document->successful($document_id, $ret);
                } else {
                    $this->obj_Document->setStatus($document_id, DOCUMENT_STATUS_ERROR);
                }
                // 再帰処理
                $this->action_convert();
            // 資料変換を通知
            } elseif ($file_info["type"] == "document") {
                $url_params = parse_url(N2MY_BASE_URL);
                $host_name = $url_params["host"];
                $port = ($url_params["port"]) ? $url_params["port"] :80;
                $rtmp = new EZRtmp();
                $fms_list = split(",",$this->doc_conf["fms"]);
                // 接続可能なFMSに送信
                foreach($fms_list as $fms) {
                    $document_app = sprintf($this->doc_conf["rtmp"], $fms, $this->doc_conf["appli"]);
                    $cmd = "\"".
                           "h = RTMPHack.new; " .
                           " h.connection_uri = '".$document_app."';" .
                           " h.connection_args = ['web'];" .
                           " h.method_name = 'startConvert';" .
                           " h.method_args = ['".$document_id."'" .
                           ", '".$this->doc_conf["user"]."@".$_SERVER["SERVER_NAME"].":".N2MY_DOC_DIR.$queue["document_path"]."/'" .
                           ", '".$this->doc_conf["user"]."@".$_SERVER["SERVER_NAME"].":".N2MY_DOC_DIR.$queue["document_path"]."/'" .
                           ", '".N2MY_LOCAL_URL."/api/document/status.php?action_dispatch'" .
                           ", '".$queue["extension"]."'" .
                           ", '".$queue["document_format"]."'" .
                           ", '".$queue["document_version"]."'" .
                           "];".
                           " h.execute;".
                           "\"";
                           $this->logger2->info($cmd);
                    $result = $rtmp->run($cmd);
                    list($active, $max_connection) = split("/", $result);
                    // n/x の形式でない場合はエラーとする。
                    if ($result && $active > 0 && $max_connection > 0) {
                        break;
                    } else {
                        $this->logger2->warn(array($fms, $result), "DOCクライアントが接続されていない");
                    }
                }
                // 変換通知成功
                if ($active > 0) {
                    sleep(2);
                    $this->action_convert();
                    $this->watchStatus($document_id, true);
                // 待機中
                } elseif ($count < 60) {
                    $this->logger2->info($document_id." - ".$count, "待機中のDOCクライアントが存在しないため、そのまま待機");
                    $count = $count + 1;
                    // 全てが待機中
                    if ( 1 == $count) {
                        $mail_subject = "DOCクライアントがすべて利用中のため、待機";
                        $mail_body = "DOCクライアントがすべて利用中のため、待機になりました。" ."\n\n" .
                            "■ URL: ".N2MY_BASE_URL."\n" .
                            "■ FMS サーバ: ".print_r($fms_list, true). "\n" .
                            "■ アプリケーション名: ".$document_app. "\n" .
                            "■ 資料ID: ".$document_id;
                        if (defined("N2MY_LOG_ALERT_FROM") && defined("N2MY_LOG_ALERT_TO") && N2MY_LOG_ALERT_FROM && N2MY_LOG_ALERT_TO) {
                            $this->sendReport(N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body);
                        }
                        $this->logger2->warn(array(N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body));
                    }
                    // キューを再取得
                    $document_info = $this->obj_Document->getStatus($document_id);
                    if ($document_info["status"] !== DOCUMENT_STATUS_DELETE) {
                        $this->obj_Document->setStatus($document_id, 0);
                        sleep(2);
                        $this->action_convert(false);
                    }
                // 120秒を経過しても変換できない場合はエラーとし、次のキューを変換
                } else {
                    $this->obj_Document->setStatus($document_id, DOCUMENT_STATUS_ERROR);
                    $semaphore_file = N2MY_APP_DIR."/var/doc_error";
                    if (!file_exists($semaphore_file)) {
                        touch($semaphore_file);
                        $mail_subject = "待機中のDOCクライアントが存在しない。";
                        $mail_body = "資料変換機が接続されていません。" ."\n" .
                            "至急、以下のFMS、および設定を確認してください。" ."\n\n" .
                            "■ URL: ".N2MY_BASE_URL."\n" .
                            "■ FMS サーバ: ".print_r($fms_list, true). "\n" .
                            "■ アプリケーション名: ".$document_app. "\n" .
                            "■ 資料ID: ".$document_id;
                        if (defined("N2MY_ALERT_FROM") && defined("N2MY_ALERT_TO") && N2MY_ALERT_FROM && N2MY_ALERT_TO) {
                            $this->sendReport(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body);
                        }
                        $this->logger2->warn(array(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body));
                    }
                    $this->action_convert();
                }
            }
        } else {
            // 終了処理
            $this->obj_Document->stop($this->process_no);
        }
    }

    /**
     * 変換中のドキュメントの状態監視
     */
    function watchStatus($document_id, $reset = false) {
        static $count = 1;
        // 次のドキュメント
        if ($reset) {
            $count = 0;
        }
        // 処理時間を更新
        $this->obj_Document->keepAlive($this->process_no);
        // n秒間隔で状態確認
        $step = 2;
        $document = $this->obj_Document->getStatus($document_id);
        $this->logger2->debug($document_id);
        // 正常終了
        if ($document["status"] == DOCUMENT_STATUS_SUCCESS) {
            $this->logger2->debug(array(N2MY_DOC_DIR.$document["document_path"].$document_id.".".$document["extension"], "successful! : ".($count * $step. " sec")));
            return true;
        // エラー、もしくは一定時間処理の経過時間を過ぎた場合は失敗とみなす
        } else if ($document["status"] == DOCUMENT_STATUS_ERROR || $count > ($this->timeout / $step)) {
            $this->logger2->warn(array("convert error! : ".$document_id, $document));
            return false;
        }
        sleep($step);
        $count = $count + 1;
        $this->watchStatus($document_id);
    }

    // action_deleteのエイリアス
    function action_cancel() {
        $this->action_delete();
    }

    /**
     * ドキュメントの削除
     */
    function action_delete() {
        $document_id = $this->request->get("document_id");
        if (!is_array($document_id)) {
            $document_ids[] = $document_id;
        } else {
            $document_ids = $document_id;
        }
        foreach ($document_ids as $document_id) {
            $this->obj_Document->delete($document_id);
        }
    }

    /**
     * 一覧取得
     */
    function action_list() {
        $path = $this->request->get("document_path");
        $rows = $this->obj_Document->getList($path);
        print "<pre>";
        var_dump($rows);
    }

    /**
     * 通常は何もなし
     */
    function default_view() {
        $message = "Document Converter";
        $this->logger2->info($message);
        echo $message;
    }

    function action_destruct() {
        return 1;
        /*
        $meeting_key = $this->request->get("meeting_key");
        $document_id = $this->request->get("document_id");
        $participant_id = $this->request->get("participant_id");
        $document = $this->obj_Document->getDetail($document_id);
        $this->logger2->info(array($meeting_key,$document_id,$participant_id,$document));
        require_once("classes/core/dbi/Meeting.dbi.php");
        $obj_Meeting = new DBI_Meeting($this->dsn);
        $where = "meeting_key = ".$meeting_key.
            " AND is_active = 1";
        $meeting_info = $obj_Meeting->getRow($where);
        if ($meeting_info) {
            // サーバ情報
            if (1 == $meeting_info["intra_fms"]) {
                require_once("classes/dbi/intra_fms.dbi.php");
                $objUserFmsServer    = new DBI_IntraFmsServer($this->dsn);
                $server_info = $objUserFmsServer->getRow(sprintf("fms_key=%d", $meeting_info["server_key"]));
            } else if (0 == $meeting_info["intra_fms"]) {
            require_once("classes/mgm/dbi/FmsServer.dbi.php");
            $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
            $server_info = $obj_FmsServer->getRow( sprintf("server_key=%d", $meeting_info["server_key"] ));
            }
//            require_once("classes/mgm/dbi/FmsServer.dbi.php");
//            $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
//            $server_info = $obj_FmsServer->getRow( sprintf("server_key=%d", $meeting_info["server_key"] ));
            $rtmp = new EZRtmp();
            // 画像貼り付け
            $rtmp_uri = "rtmp://".$server_info["server_address"]."/".$this->config->get("CORE", "app_name")."/".$meeting_info["fms_path"].$meeting_info["meeting_key"];     // RTMP URL
            $status = $document["document_status"];
            // アップロードしたユーザに通知
            if ($participant_id) {
                // 通知
                $cmd = "\"".
                    "h = RTMPHack.new; " .
                    " h.connection_uri = '".$rtmp_uri."';" .
                    " h.connection_args = [0x320];" .
                    " h.method_name = 'callTargetClientFunction';" .
                    " h.method_args = ['".$participant_id."', 'onDocumentInformation', 'error', {:code=>'".$status."'}];" .
                    " h.execute;".
                    "\"";
                $rtmp->run($cmd);
            }
        }
        */
    }

    /**
     * 利用中の会議に資料を貼り付ける
     */
    function action_dispatch() {
        return 1;
        /*
        $meeting_key = $this->request->get("meeting_key");
        $document_id = $this->request->get("document_id");
        $participant_id = $this->request->get("participant_id");
        $document = $this->obj_Document->getDetail($document_id);
        $this->logger2->debug(array($meeting_key,$document_id,$participant_id,$document));

        if ($document["document_status"] == DOCUMENT_STATUS_SUCCESS) {
            require_once("classes/core/dbi/Meeting.dbi.php");
            $obj_Meeting = new DBI_Meeting($this->dsn);
            $where = "meeting_key = ".$meeting_key.
                " AND is_active = 1";
            $meeting_info = $obj_Meeting->getRow($where);
            if ($meeting_info) {
                // サーバ情報
                if (1 == $meeting_info["intra_fms"]) {
                    require_once("classes/dbi/intra_fms.dbi.php");
                    $objUserFmsServer    = new DBI_IntraFmsServer($this->dsn);
                    $server_info = $objUserFmsServer->getRow(sprintf("fms_key=%d", $meeting_info["server_key"]));
                } else if (0 == $meeting_info["intra_fms"]) {
                    require_once("classes/mgm/dbi/FmsServer.dbi.php");
                    $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
                    $server_info = $obj_FmsServer->getRow( sprintf("server_key=%d", $meeting_info["server_key"] ));
                }
//                require_once("classes/mgm/dbi/FmsServer.dbi.php");
//                $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
//                $server_info = $obj_FmsServer->getRow( sprintf("server_key=%d", $meeting_info["server_key"] ));
                $base_url = N2MY_BASE_URL."docs/".$document["document_path"].$document_id."/".$document_id;
                $label = $document["document_name"];
                $rtmp = new EZRtmp();
                // 画像貼り付け
                $rtmp_uri = "rtmp://".$server_info["server_address"]."/".$this->config->get("CORE", "app_name")."/".$meeting_info["fms_path"].$meeting_info["meeting_key"];     // RTMP URL
                // アップロードしたユーザに通知
                if ($participant_id) {
                    // 画像貼り付け
                    $cmd = "\"".
                        "h = RTMPHack.new; " .
                        " h.connection_uri = '".$rtmp_uri."';" .
                        " h.connection_args = [0x320];" .
                        " h.method_name = 'addWbPagesByRuby';" .
                        " h.method_args = ['".$base_url."', '".$label."', '".$document['document_index']."'];" .
                        " h.execute;".
                        "\"";
                    $rtmp->run($cmd);
                    // 通知
                    $cmd = "\"".
                        "h = RTMPHack.new; " .
                        " h.connection_uri = '".$rtmp_uri."';" .
                        " h.connection_args = [0x320];" .
                        " h.method_name = 'callTargetClientFunction';" .
                        " h.method_args = ['".$participant_id."', 'onDocumentInformation', 'finishUpload',0];" .
                        " h.execute;".
                        "\"";
                    $rtmp->run($cmd);
                } else {
                    // メール資料貼り込み
                    $cmd = "\"".
                        "h = RTMPHack.new; " .
                        " h.connection_uri = '".$rtmp_uri."';" .
                        " h.connection_args = [0x320];" .
                        " h.method_name = 'addWbPagesByEmail';" .
                        " h.method_args = ['".$base_url."', '".$label."', '".$document['document_index']."'];" .
                        " h.execute;".
                        "\"";
                    $rtmp->run($cmd);
                }
            }
        }
        */
    }
}

$main = new DocumentConverter();
$main->execute();
