<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Api.class.php");
require_once("lib/EZLib/EZHTML/EZValidator.class.php");

class N2MY_Inbound_API extends N2MY_Api
{
    var $session_id = null;

    function init()
    {
        $this->_name_space = md5(__FILE__);
    }

    function action_random_start() {
        $request = $this->request->getAll();
        $rules = array(
            "inbound_id" => array(
                "required" => true,
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
        	$message["title"] = "PARAMETER_ERROR";
        	$message["body"] = $this->get_error_info($err_obj);
        	return $this->render_valid($message);
        }
        $inbound_id = $request["inbound_id"];
        require_once("classes/mgm/MGM_Auth.class.php");
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if ( ! $server_info = $obj_MGMClass->getRelationDsn( $inbound_id, "inbound_id" ) ){
            $message["title"] = "PARAMETER_ERROR";
            $message["body"] = INBOUND_ID_ERROR;
            return $this->render_valid($message);
        }

        require_once("classes/dbi/inbound.dbi.php");
        $obj_Inbound = new InboundTable( $server_info["dsn"] );
        $where = "inbound_id = '".addslashes($inbound_id)."'";
        $inbound_info = $obj_Inbound->getRow($where);
        if (!$inbound_info) {
        	$message["title"] = "PARAMETER_ERROR";
        	$message["body"] = INBOUND_ID_ERROR;
        	return $this->render_valid($message);
        }

        $name = $request["name"]?$request["name"]:"";

        // モバイル判別
        if (strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod") || strpos($_SERVER['HTTP_USER_AGENT'],"iPad")) {
        	$base_url = parse_url(N2MY_BASE_URL);
        	$referer = $_SERVER["HTTP_REFERER"];
        	$url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://info?inbound_id=".$inbound_id."&entrypoint=".$base_url["host"]."&callbackurl=".urlencode($referer)."&name=".$name;
        	$this->logger2->info($url);
        	header("Location: ".$url);
        	exit;
        }

        $lang = $request["lang"]?$request["lang"]:$this->get_language();
        // ユーザー
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$inbound_info["user_key"];
        $user_info = $objUser->getRow($where);
        $this->logger->debug("inbound_info",__FILE__,__LINE__,$inbound_info);

        $room_key = $this->get_random_roomkey($user_info["user_key"], $inbound_id, $server_info["dsn"]);
        $this->logger2->info($room_key);
        if (!$room_key) {
        	$err_obj->set_error($inbound_id, "MEETING_INVALID");
        	$message["title"] = INBOUND_ERROR_TITLE;
        	$message["body"] = "NO available room";
        	return $this->render_valid($message);
        }

        $narrow = $this->request->get("is_narrow");
        $type = "customer";

        require_once("classes/N2MY_Meeting.class.php");
        $obj_N2MY_Meeting = new N2MY_Meeting($server_info["dsn"]);
        $obj_Room = new RoomTable( $server_info["dsn"] );
        $where = "room_key = '".addslashes($room_key)."'";
        $room_info = $obj_Room->getRow($where);

        // 最後に実行されたMeetingKeyを取得
        $last_meeting_key = $room_info["meeting_key"];
        $this->logger->debug(__FUNCTION__."last_meeting_key", __FILE__, __LINE__, $last_meeting_key);

        // ステータス確認
        $rooms[$room_key] = $last_meeting_key;
        $last_meeting_status = $this->api_room_status($rooms, $server_info["dsn"]);
        if ($last_meeting_status == "1") {
        	// 会議情報取得
        	if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key)) {
        		$message["title"] = INBOUND_ERROR_TITLE;
        		$message["body"] = INBOUND_MEETING_ERROR;
        		return $this->render_valid($message);
        	}
        }
        $this->logger2->debug($room_key);
        $meeting_key = $meeting_info["meeting_key"];
        // オプション指定
        $options = array(
        		"meeting_name" => $meeting_info["meeting_name"],
        		"start_time"   => $meeting_info["start_time"],
        		"end_time"     => $meeting_info["end_time"],
        		"country_id"   => "jp",
        		"password"     => $meeting_info["meeting_password"],
        		"display_size" => $inbound_info["display_size"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);

        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";
        if (!$name && $inbound_info["customer_info_flg"] == 2) {
            $name = $inbound_info["customer_name"];
        }
        $user_options = array(
        		"name"      => $name,
        		"participant_email"         => "",
        		"participant_station"       => "",
        		"narrow"    => $this->request->get("is_narrow"),
        		"lang"      => $lang,
        		"skin_type" => $this->request->get("skin_type", ""),
        		"mode"      => "",//"general",
        		"role"      => $type,//"default",
        		"mail_upload_address"       => $mail_upload_address,
        );
        $tag = $this->request->get("tag");
        if($tag){
            $user_options["participant_tag"] = $tag;
        }

        if(mb_strlen($tag) > 255){

            $err_msg["errors"][] = array(
                    "field"   => "tag",
                    "err_cd"   => "maxlen",
                    "err_msg" => "The tag max length is 255.",
            );
            $err_obj->set_error("tag","tag error" ,$err_msg);
            return $this->display_error(100, "PARAMETER_ERROR",$err_msg);
        }

        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options, $customer_type = "customer", $inbound_id);
        $this->logger2->info($meetingDetail);
        if ( !$meetingDetail ) {
        	$message["title"] = INBOUND_ERROR_TITLE;
        	$message["body"] = INBOUND_MEETING_ERROR;
        	return $this->render_valid($message);
        }
        $session = EZSession::getInstance();
        // ID を取得
        $session_id = session_id();
        $session->set( "room_key", $room_key );
        $session->set( "type", $meetingDetail["participant_type_name"] );
        $session->set( "meeting_type", $type );
        $session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $session->set( "participant_key", $meetingDetail["participant_key"] );
        $session->set( "participant_name", $name );
        $session->set( "mail_upload_address", $mail_upload_address );
        $session->set( "reload_type", 'normal');
        $session->set( "fl_ver", "as3" );
        $session->set( "display_size", "16to9" );
        $session->set( "meeting_version", $user_info["meeting_version"] );
        $session->set( "user_info", $user_info );

        // 参加者情報
        $session->set( "user_agent_ticket", uniqid());

        $url = N2MY_BASE_URL."inbound/?action_meeting_display&n2my_session=".$session_id;
        if($room_info["room_password"]){
        	$session->set("redirect_url", $url, $session_id);
        	$url = N2MY_BASE_URL."inbound/" .
        			"?action_room_auth_login=" .
        			"&n2my_session=".$session_id.
        			"&room_key=".$room_key.
        			"&ns=".$session_id;
        }

        $data = array(
            "url"      => $url,
            "meeting_id"    => $meetingDetail["meeting_key"],
        );
        $this->session = $session;
        return $this->output($data);
    }

    function get_random_roomkey($user_key, $inbound_id, $dsn)
    {
        require_once ("classes/core/Core_Meeting.class.php");
        require_once("classes/dbi/user.dbi.php");
        $obj_User = new UserTable( $dsn );
        $where_user = "user_key = ".$user_key.
                      " AND user_status = 1".
                      " AND user_delete_status = 0";
        $user_info = $obj_User->getRow($where_user);
        $obj_CoreMeeting = new Core_Meeting( $dsn );
        $room_status = $obj_CoreMeeting->getMeetingStatusInbound( $user_info["user_id"], $inbound_id);
        $this->logger->debug("room_status",__FILE__,__LINE__,$room_status);

        if ($room_status) {
            foreach($room_status as $room) {
                if ("1" == $room["status"] && "1" == $room["pcount"] && !$room["is_reserved"] ) {
                    $this->logger->trace("room_list",__FILE__,__LINE__,$room);
                    $room_list[] = $room["room_key"];
                }
            }
            //指定秒数以内に再入室した部屋を優先
            return $room_list[0];
        }
        return false;
    }

    function api_room_status($rooms, $dsn) {
        require_once ("classes/core/Core_Meeting.class.php");
        $obj_CoreMeeting = new Core_Meeting( $dsn );
        $room_list = $obj_CoreMeeting->getMeetingStatus( $rooms );
        $status = $room_list[0]["status"];
        $this->logger2->debug($status);
        return $status;
    }

    function error_check($data, $rules) {
        $check_obj = new EZValidator($data);
        foreach($rules as $field => $rule) {
            $check_obj->check($field, $rule);
        }
        return $check_obj;
    }

    private function render_valid($message = null)
    {
        $err_msg["errors"][] = array(
                "field"   => "inbound_id",
                "err_cd"  => "meeting",
                "err_msg" => $message["body"],
                );
        return $this->display_error("100", $message["title"], $err_msg);
    }
}

$main = new N2MY_Inbound_API();
$main->execute();
