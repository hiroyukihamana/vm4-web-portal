<?php
/*
 * Created on 2008/02/14
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");
require_once("classes/N2MY_Reservation.class.php");
require_once("classes/dbi/reservation_user.dbi.php");
require_once("classes/N2MY_Meeting.class.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once("config/config.inc.php");

class N2MY_Meeting_Resevation_API extends N2MY_Api
{
    var $_err = null;
    var $_ssl_mode = null;
    var $obj_MeetingLog = null;
    var $obj_Participant = null;

    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
        $this->objReservation = new N2MY_Reservation($this->get_dsn(), $this->session->get("dsn_key"));
        $this->objReservationUser = new ReservationUserTable($this->get_dsn());
        $this->obj_Meeting    = new DBI_Meeting($this->get_dsn());
        $this->obj_Participant    = new DBI_Participant($this->get_dsn());

    }

    /**
     * 予約一覧（検索）
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_limit 検索開始日 (デフォルト null)
     * @param datetime end_limit 検索終了日 (デフォルト null)
     * @param string sort_key ソート基準 (デフォルト 予約会議開始時間)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @param int limit 表示件数 (デフォルト null)
     * @param int offset 開始件数 (デフォルト null)
     * @return string $output 会議キー、予約キー、会議名、開始時間、終了時間をxmlで出力
     */
    function action_get_list()
    {
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        $request   = $this->request->getAll();
        $room_key         = $this->request->get(API_ROOM_ID, "");
        $reservation_name = $this->request->get("reservation_name", "");
        $_wk_start_limit  = $this->request->get("start_limit");
        $_wk_end_limit    = $this->request->get("end_limit");
        $_wk_updatetime   = $this->request->get("updatetime");
        $limit            = $this->request->get("limit", N2MY_API_GET_LIST_LIMIT);
        $page             = $this->request->get("page", 1);
        $sort_key         = $this->request->get("sort_key", "reservation_starttime");
        $sort_type        = $this->request->get("sort_type", "asc");
        // タイムスタンプ変換
        $server_time_zone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $user_time_zone   = $this->session->get("time_zone"); // セッションの値から取得
        if ($_wk_start_limit) {
            if (is_numeric($_wk_start_limit)) {
                $_ts_local = $_wk_start_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_start_limit, $user_time_zone, $server_time_zone);
            }
            $start_limit = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone, $_ts_local);
        }
        $this->logger2->debug(array($_wk_start_limit, $user_time_zone, $server_time_zone, $start_limit, date("Y-m-d H:i:s", $_ts_local)));
        if ($_wk_end_limit) {
            if (is_numeric($_wk_end_limit)) {
                $_ts_local = $_wk_end_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_end_limit, $user_time_zone, $server_time_zone);
            }
            $end_limit = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone, $_ts_local);
        }
        $this->logger2->debug(array($_wk_end_limit, $user_time_zone, $server_time_zone, $end_limit, date("Y-m-d H:i:s", $_ts_local)));
        if ($_wk_updatetime) {
            if (is_numeric($_wk_updatetime)) {
                $_ts_local = $_wk_updatetime;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_updatetime, $user_time_zone, $server_time_zone);
            }
            $updatetime = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone, $_ts_local);
        }
        $this->logger2->debug(array($_wk_updatetime, $user_time_zone, $server_time_zone, $updatetime, date("Y-m-d H:i:s", $_ts_local)));
        $request["start_limit"] = $start_limit;
        $request["end_limit"] = $end_limit;
        $request["updatetime"] = $updatetime;
        //
        $options = array(
            "user_key"         => $user_info["user_key"],
            "room_key"         => $room_key,
            "reservation_name" => $reservation_name,
            "start_time"       => $start_limit,
            "end_time"         => $end_limit,
            "updatetime"      => $updatetime,
            "limit"            => $limit,
            "page"             => $page,
            "sort_key"         => $sort_key,
            "sort_type"        => $sort_type,
            );
        $rules = array(
            API_ROOM_ID => array(
                "allow" => array_keys($room_info),
                ),
            "start_limit" => array(
                "datetime" => true,
                ),
            "end_limit" => array(
                "datetime" => true,
                ),
            "updatetime" => array(
                "datetime" => true,
                ),
            "limit" => array(
                "integer" => true,
                ),
            "page" => array(
                "integer" => true,
                ),
            "sort_key" => array(
                "allow" => array("reservation_name", "reservation_starttime"),
                ),
            "sort_type" => array(
                "allow" => array("asc", "desc")
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $offset           = ($limit * ($page - 1));
            $options["offset"] = $offset;
            unset($options["page"]);
            if($user_info["is_one_time_meeting"] == 1){
              $reservations = $this->objReservation->getList_for_ONE($room_key, $options, $server_time_zone);
              $count = $this->objReservation->getCount_for_ONE($room_key, $options);
            }else{
                    $reservations = $this->objReservation->getList($room_key, $options, $server_time_zone);
                    $count = $this->objReservation->getCount($room_key, $options);
            }
            $reservationsList = array();
            if ($reservations) {
                // 一覧表示
                foreach ($reservations as $_key => $_val) {
                    $reservations_list = array(
                          'reservation_name'        => $reservations[$_key]["reservation_name"],
                          'reservation_pw'          => $reservations[$_key]["reservation_pw"] = ($reservations[$_key]["reservation_pw"]) ? 1 : 0 ,
                          'status'                  => $reservations[$_key]["status"],
                          'room_id'                 => $reservations[$_key]["room_key"],
                          API_RESERVATION_ID        => $reservations[$_key]["reservation_session"],
                          API_MEETING_ID            => $this->ticket_to_session($reservations[$_key]["meeting_key"]),
                          API_RESERVATION_START     => $reservations[$_key]["reservation_starttime"],
                          API_RESERVATION_END       => $reservations[$_key]["reservation_endtime"],
                          "reservation_registtime"  => $reservations[$_key]["reservation_registtime"],
                          "reservation_updatetime"  => $reservations[$_key]["reservation_updatetime"]
                    );
                    $reservationsList[] = $reservations_list;
                }
            }
            $data = array(
                "count" => $count,
                "reservations"  => array("reservation" => $reservationsList),
                );
            return $this->output($data);
        }
    }

    /**
     * 予約内容詳細（予約、招待者、事前資料）
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return string $output 予約キー、会議室名、会議名、開始時間、終了時間、パスワード、招待者、資料名をxmlで出力
     */
    function action_get_detail()
    {
        $request = $this->request->getAll();
        $reservation_session   = $this->request->get(API_RESERVATION_ID);
        $reservation_pw   = $this->request->get("password");
        $time_zone = $this->session->get("timezone");
        $user_info = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            }
        }
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        if ($info["reservation_pw"]) {
            if (($reservation_pw != $info["reservation_pw"]) && ($reservation_pw != EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_admin_password"]))) {
                $err_obj->set_error("password", "pw_equal");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
          $where = "reservation_key = ".$reservation_data["info"]["reservation_key"]. " AND r_organizer_flg=1";
          $organizer_info           = $this->objReservationUser->getRow($where);
          $organizer                = array(
              'name'         => $reservation_data["organizer"]["name"],
              'email'        => $reservation_data["organizer"]["email"],
              'timezone'     => $reservation_data["organizer"]["timezone"],
              'lang'          => $reservation_data["organizer"]["lang"]);
            $reservation_data["organizer"] = $organizer;
      $_authority = $this->_format_authority($reservation_data["info"]["reservation_key"]);
      $authority = array(
        'name'           => $reservation_data['authority']['name'],
        'email'          => $reservation_data['authority']['email'],
        'timezone'       => $reservation_data['authority']['timezone'],
        'lang'           => $reservation_data['authority']['lang'],
        'presenter_url'  => $_authority['presenter_url']);
      $reservation_data['presenter'] = $authority;
      unset($reservation_data['authority']);

            require_once("classes/dbi/meeting.dbi.php");
            $objMeeting = new MeetingTable($this->get_dsn());
            $pin_cd     = $objMeeting->getOne("meeting_ticket = '".addslashes($reservation_data["info"]["meeting_key"])."'", "pin_cd");
            $room_info  = $this->session->get("room_info");
            $_room_info = $room_info[$reservation_data["info"]["room_key"]];
            $info = array(
                'reservation_name'      => $reservation_data["info"]["reservation_name"],
                'reservation_pw'        => ($reservation_data["info"]["reservation_pw"]) ? 1 : 0 ,
                'max_port'              => $reservation_data["info"]["max_port"],
                'status'                => $reservation_data["info"]["status"],
                'sender'                => $reservation_data["info"]["sender"],
                'sender_mail'           => $reservation_data["info"]["sender_mail"],
                'mail_body'             => $reservation_data["info"]["mail_body"],
                'is_reminder'           => $reservation_data["info"]["is_reminder_flg"],
                'mail_type'                => $reservation_data["info"]["mail_type"],
              'is_convert_wb_to_pdf'  => $reservation_data["info"]["is_convert_wb_to_pdf"],
              'is_rec_flg'            => $reservation_data["info"]["is_rec_flg"],
              'is_cabinet_flg'        => $reservation_data["info"]["is_cabinet_flg"],
              'is_desktop_share'      => $reservation_data["info"]["is_desktop_share"],
              'is_invite_flg'         => $reservation_data["info"]["is_invite_flg"],
                'pin_cd'                => $pin_cd,
                API_ROOM_ID             => $reservation_data["info"]["room_key"],
                API_MEETING_ID          => $this->ticket_to_session($reservation_data["info"]["meeting_key"]),
                API_RESERVATION_ID      => $reservation_data["info"]["reservation_session"],
                API_RESERVATION_START   => strtotime($reservation_data["info"]["reservation_starttime"]),
                API_RESERVATION_END     => strtotime($reservation_data["info"]["reservation_endtime"])
              );
              $reservation_data["info"] = $info;
              if (!$_room_info["use_sales_option"]) {
                  $info["url"] = $reservation_data["info"]["url"];
              }

            unset($reservation_data["guest_flg"]);

            $country_key = $this->session->get("country_key");

            foreach ($reservation_data["guests"] as $_key => $guest) {
                if ($_room_info["use_sales_option"] && $guest["member_key"]) {
                    $guest_type = "staff";
                } else {
                    if ($_room_info["use_sales_option"] &&
                             ! $guest["member_key"]) {
                        $guest_type = "customer";
                    } else {
                        $guest_type = ($guest["type"] == 1) ? "audience" : "";
                    }
                }
                $guests[] = array(
                    'name'       => $guest["name"],
                    'email'      => $guest["email"],
                    'timezone'   => $guest["timezone"],
                    'lang'       => $guest["lang"],
                    API_GUEST_ID => $guest["r_user_session"],
                    'type' => $guest_type,
                     'invite_url' => N2MY_BASE_URL . "r/" .
                                 $guest["r_user_session"] . '&c=' . $country_key .
                                 '&lang=' . $guest["lang"]
                 );
            }

            unset($reservation_data["guests"]);
            $reservation_data["guests"]["guest"] = $guests;

            foreach ($reservation_data["documents"] as $_key => $_val) {
                $documents[] = array(
                    'document_id'   => $_val["document_id"],
                    'name'          => $_val["name"],
                    'type'          => $_val["type"],
                    'category'      => $_val["category"],
                    'status'        => $_val["status"],
                    'index'         => $_val["index"],
                    'sort'          => $_val["sort"]
                 );
            }

            unset($reservation_data["documents"]);
            require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
            $reservation_data["documents"]["document"] = $documents;
            $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $reservation_session);
            $encrypted_data = str_replace("/", "-", $encrypted_data);
            $encrypted_data = str_replace("+", "_", $encrypted_data);
            $reservation_data["info"]["url"] = N2MY_BASE_URL."y/".$encrypted_data;
            $data = array(
                "reservation_info"  => $reservation_data,
                );
            return $this->output($data);
        }
    }

    function action_get_detail_sales()
    {
      $this->action_get_detail();
    }


    function get_value($str){
        if(strlen($str) == 0){
            return 1;
        }else{
            if($str == 0){
                return 0;
            }else{
                return 1;
            }
        }
    }
      
 
// 下記コメントアウト箇所はDEVのみの更新（Rev33682）
// STGには反映されていないため、必要かの判断ができないのでSTG側を正としリファレンス等をあわせる。
/*
    function action_get_detail_sales()
    {
        $request = $this->request->getAll();
        $reservation_session   = $this->request->get(API_RESERVATION_ID);
        $reservation_pw   = $this->request->get("password");
        $time_zone = $this->session->get("timezone");
        $user_info = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
                "output_type" => array(
                    "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            }
        }
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        if ($info["reservation_pw"]) {
            if (($reservation_pw != $info["reservation_pw"]) && ($reservation_pw != EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_admin_password"]))) {
                $err_obj->set_error("password", "pw_equal");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            require_once("classes/dbi/meeting.dbi.php");
            $objMeeting = new MeetingTable($this->get_dsn());
            $pin_cd     = $objMeeting->getOne("meeting_ticket = '".addslashes($reservation_data["info"]["meeting_key"])."'", "pin_cd");
            $room_info  = $this->session->get("room_info");
            $_room_info = $room_info[$reservation_data["info"]["room_key"]];
            $info = array(
                'reservation_name'      => $reservation_data["info"]["reservation_name"],
                'reservation_pw'        => ($reservation_data["info"]["reservation_pw"]) ? 1 : 0 ,
                'status'                => $reservation_data["info"]["status"],
                'sender'                => $reservation_data["info"]["sender"],
                'sender_mail'           => $reservation_data["info"]["sender_mail"],
                'mail_body'             => $reservation_data["info"]["mail_body"],
                'is_reminder'           => $reservation_data["info"]["is_reminder_flg"],
                'mail_type'             => $reservation_data["info"]["mail_type"],
                'is_convert_wb_to_pdf'  => $reservation_data["info"]["is_convert_wb_to_pdf"],
                'is_rec_flg'            => $reservation_data["info"]["is_rec_flg"],
                'is_cabinet_flg'        => $reservation_data["info"]["is_cabinet_flg"],
                'is_desktop_share'      => $reservation_data["info"]["is_desktop_share"],
                'is_invite_flg'         => $reservation_data["info"]["is_invite_flg"],
                'pin_cd'                => $pin_cd,
                API_ROOM_ID             => $reservation_data["info"]["room_key"],
                API_MEETING_ID          => $this->ticket_to_session($reservation_data["info"]["meeting_key"]),
                API_RESERVATION_ID      => $reservation_data["info"]["reservation_session"],
                API_RESERVATION_START   => strtotime($reservation_data["info"]["reservation_starttime"]),
                API_RESERVATION_END     => strtotime($reservation_data["info"]["reservation_endtime"])
              );
              $reservation_data["info"] = $info;
              if (!$_room_info["use_sales_option"]) {
                  $info["url"] = $reservation_data["info"]["url"];
              }

            unset($reservation_data["guest_flg"]);

            $country_key = $this->session->get("country_key");

            if($reservation_data["staff"]) {
              //$reservation_data["customer"] = $reservation_data["guest"];
              $reservation_data["customer"] = array (
                  "name" => $reservation_data["guest"]["name"],
                  "email" => $reservation_data["guest"]["email"],
                  "timezone" => $reservation_data["guest"]["timezone"],
                  "lang" => $reservation_data["guest"]["lang"],
                  API_GUEST_ID => $reservation_data["guest"]["r_user_session"],
                  "invite_url" => N2MY_BASE_URL."r/".$reservation_data["guest"]["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
              );
              $staff = array (
                  "member_key" => $reservation_data["staff"]["member_key"],
                  "name" => $reservation_data["staff"]["name"],
                  "email" => $reservation_data["staff"]["email"],
                  "timezone" => $reservation_data["staff"]["timezone"],
                  "lang" => $reservation_data["staff"]["lang"],
                  API_GUEST_ID => $reservation_data["staff"]["r_user_session"],
                  "invite_url" => N2MY_BASE_URL."r/".$reservation_data["staff"]["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],

              );

              if($reservation_data["is_manager"]){
                $manager = array (
                  "name" => $reservation_data["manager"]["name"],
                  "email" => $reservation_data["manager"]["email"],
                  "timezone" => $reservation_data["manager"]["timezone"],
                  "lang" => $reservation_data["manager"]["lang"],
                  API_GUEST_ID => $reservation_data["manager"]["r_user_session"],
                  "invite_url" => N2MY_BASE_URL."r/".$reservation_data["manager"]["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],

                );
              }

              unset($reservation_data["guest"]);
              unset($reservation_data["staff"]);
              $reservation_data["staff"] = $staff;
              $reservation_data["manager"] = $manager;
            } else {
              foreach ($reservation_data["guests"] as $_key => $guest) {
                if($guest["member_key"]) {
                  $reservation_data["staff"] = array (
                      "member_key" => $guest["member_key"],
                      "name" => $guest["name"],
                      "email" => $guest["email"],
                      "timezone" => $guest["timezone"],
                      "lang" => $guest["lang"],
                      API_GUEST_ID => $guest["r_user_session"],
                      "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
                  );
                } else {
                  $reservation_data["customer"] = array (
                      "name" => $guest["name"],
                      "email" => $guest["email"],
                      "timezone" => $guest["timezone"],
                      "lang" => $guest["lang"],
                      API_GUEST_ID => $guest["r_user_session"],
                      "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
                  );
                }
                if($reservation_data["is_manager"]){
                    $reservation_data["manager"] = array (
                                    "member_key" => $guest["member_key"],
                                    "name" => $guest["name"],
                                    "email" => $guest["email"],
                                    "timezone" => $guest["timezone"],
                                    "lang" => $guest["lang"],
                                    API_GUEST_ID => $guest["r_user_session"],
                                    "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
                    );
                }
              }
            }

            unset($reservation_data["guests"]);
            unset($reservation_data["documents_dsp"]);
              $this->logger2->info($reservation_data);
//             $reservation_data["organizer"] = ;

            foreach ($reservation_data["documents"] as $_key => $_val) {
                $documents[] = array(
                    'document_id'   => $_val["document_id"],
                    'name'          => $_val["name"],
                    'type'          => $_val["type"],
                    'category'      => $_val["category"],
                    'status'        => $_val["status"],
                    'index'         => $_val["index"],
                    'sort'          => $_val["sort"]
                 );
            }

            unset($reservation_data["documents"]);
            require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
            $reservation_data["documents"]["document"] = $documents;
            $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $reservation_session);
            $encrypted_data = str_replace("/", "-", $encrypted_data);
            $encrypted_data = str_replace("+", "_", $encrypted_data);
            $reservation_data["info"]["url"] = N2MY_BASE_URL."y/".$encrypted_data;
            $data = array(
                "reservation_info"  => $reservation_data,
                );
            return $this->output($data);
        }
    }
*/

    
    /**
     * vid ログインの場合、push を行うために、tokenの 死活判定を行う
     * @see MTGVFOUR-1748
     * @see htdocs/services/reservation/index.php::isActiveVidToken
     * @return boolean
     */
    private function isActiveVidToken(){
    
    	if ( !$this->session->get( 'vid_info' )
    			|| !$this->session->get('service_mode') == 'meeting'
    			|| !$this->config->get( 'VCUBE_PORTAL', 'is_pool_reservation' )
    	) {
    		// push を 行わないので、チェック不要
    		return true;
    	}
    	require_once 'classes/vcubeid/vcubeIdCore.class.php';
    	$domain = $this->config->get('VCUBE_ONE','vcubeid_domain');
    	$isSSL = $this->config->get('VCUBE_ONE','vcube_isSSL') ?: false;
    	$isSha1 = $this->config->get('VCUBE_ONE','vcube_isSha1') ?: false;
    	switch ( $_SERVER["SERVER_NAME"] ) {
    		case $this->config->get('VCUBE_ONE','meeting_server_name') :
    			$consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
    			$wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
    			$wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
    			break;
    		case $this->config->get('VCUBE_ONE','document_server_name') :
    			$consumerKey = $this->config->get('VCUBE_ONE','one_document_consumer_key');
    			$wsseId = $this->config->get('VCUBE_ONE','vcubeid_doc_wsseId');
    			$wssePw = $this->config->get('VCUBE_ONE','vcubeid_doc_wssePw');
    			break;
    		case $this->config->get('VCUBE_ONE','sales_server_name') :
    			$consumerKey = $this->config->get('VCUBE_ONE','one_sales_consumer_key');
    			$wsseId = $this->config->get('VCUBE_ONE','vcubeid_sls_wsseId');
    			$wssePw = $this->config->get('VCUBE_ONE','vcubeid_sls_wssePw');
    			break;
    		default :
    			$consumerKey = $this->config->get('VCUBE_ONE','one_meeting_consumer_key');
    			$wsseId = $this->config->get('VCUBE_ONE','vcubeid_mtg_wsseId');
    			$wssePw = $this->config->get('VCUBE_ONE','vcubeid_mtg_wssePw');
    			break;
    	}
    	if (!$domain || !$wsseId || !$wssePw || !$consumerKey) {
    		// 定義不足により解決不能だが、ログインできているで通らないはず
    		$this->logger2->error("parametter error");
    		return false;
    	}
    	$obj_vid = new vcubeIdCore($domain, $wsseId, $wssePw, $isSSL, $isSha1);
    
    	$vid_info = $this->session->get( 'vid_info' );
    	$exist = $obj_vid->existToken( $consumerKey, $vid_info['auth_token'] );
    	$this->logger2->debug( array( 'vid_info'=>$vid_info, 'response'=>$exist ), 'check existToken for reservation_push');
    
    	// 非活性のため偽
    	if( !$exist || !$exist['result'] ){
    		$this->logger2->warn( array( 'vid_info'=>$vid_info, 'response'=>$exist ), 'vid session dose not exist' );
    		return false;
    	}
    
    	// 活性確認済みのため真
    	return true;
    }

    /**
     * 予約追加
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_time 開始日時
     * @param datetime end_time 終了日時
     * @param string r_user_name 参加者名 (デフォルト null)
     * @param string r_user_email 参加者メールアドレス (デフォルト null)
     * @param string r_user_lang 参加者言語 (デフォルト null)
     * @param string r_user_timezone 参加者タイムゾーン (デフォルト null)
     * @return boolean
     */
    function action_add()
    {
        
    	if ( !$this->isActiveVidToken() ) {
    		// 活性が確認できなければセッションエラー
    		$this->session->clear();
    		parent::display_error(1, 'Login failed / Invalid auth token', $_REQUEST[N2MY_SESS_NAME]);
    		exit();
    	}
        
        $user_info               = $this->session->get("user_info");
        $member_info             = $this->session->get("member_info");
        $user_key                = $user_info["user_key"];
        $room_key                = $this->request->get(API_ROOM_ID);
        $max_port                = $this->request->get("max_port");
        $reservation_name        = $this->request->get("name");
        $reservation_place       = $this->session->get("time_zone"); // セッションの値から取得
        $_starttime              = $this->request->get("start");
        $_endtime                = $this->request->get("end");
        $reservation_pw          = $this->request->get("password");
        $reservation_pw_type     = $this->request->get("password_type") ? $this->request->get("password_type") : 1;
        $sender_name             = $this->request->get("sender_name");
        $sender_email            = $this->request->get("sender_email");
        $reservation_info        = $this->request->get("info");
        $_guests                 = $this->request->get("guests");
        $send_mail               = $this->request->get("send_mail", 1);
        $organizer_flag          = in_array($this->request->get("organizer_flag"), array(0, 1, 2)) ? $this->request->get("organizer_flag") : 0;
        $authority_flag          = in_array($this->request->get("presenter_flag"), array(0, 1)) ? $this->request->get("presenter_flag") : 0;
        $teleconf_flg            = in_array($this->request->get('teleconf_flg'), array(0,1)) ? $this->request->get('teleconf_flg') : 0;
        $use_pgi_dialin          = in_array($this->request->get('use_pgi_dialin'), array(0,1)) ? $this->request->get('use_pgi_dialin') : 0;
        $use_pgi_dialin_free     = in_array($this->request->get('use_pgi_dialin_free'), array(0,1)) ? $this->request->get('use_pgi_dialin_free') : 0;
        $use_pgi_dialin_lo_call  = in_array($this->request->get('use_pgi_dialin_lo_call'), array(0,1)) ? $this->request->get('use_pgi_dialin_lo_call') : 0;
        $use_pgi_dialout         = in_array($this->request->get('use_pgi_dialout'), array(0,1)) ? $this->request->get('use_pgi_dialout') : 0;
        // タイプによってroomlist更新
        if($user_info["is_one_time_meeting"] == 1 &&  $this->session->get("service_mode") != "sales"){
            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"] , null , true);
            $this->session->set("room_info" ,$rooms);
        }
        /*2014-01-14 追加
                予約追加で「リマインダー」と「メールHTML化」追加
        */
        $mail_type               = $this->request->get("mail_type");
        $is_reminder_flg         = $this->request->get("is_reminder");
        $is_convert_wb_to_pdf    = $this->get_value($this->request->get("is_convert_wb_to_pdf"));
        $is_rec_flg              = $this->get_value($this->request->get("is_rec"));
        $is_cabinet_flg          = $this->get_value($this->request->get("is_cabinet"));
        $is_desktop_share        = $this->get_value($this->request->get("is_desktop_share"));
        $is_invite_flg           = $this->get_value($this->request->get("is_invite"));
        if($is_convert_wb_to_pdf == 0 || $is_rec_flg == 0 || $is_cabinet_flg == 0 || $is_desktop_share == 0 || $is_invite_flg == 0){
          $is_limited_function = 1;
        }else{
          $is_limited_function = 0;
        }
        // 時差変換
        if ($_starttime) {
            if (is_numeric($_starttime)) {
                $reservation_starttime = EZDate::getZoneDate("Y-m-d H:i:s", $reservation_place, $_starttime);
            } else {
                $reservation_starttime = $_starttime;
            }
        }
        if ($_endtime) {
            if (is_numeric($_endtime)) {
                $reservation_endtime = EZDate::getZoneDate("Y-m-d H:i:s", $reservation_place, $_endtime);
            } else {
                $reservation_endtime = $_endtime;
            }
        }
        $reservation_data["info"] = array(
            "user_key"               => $user_key,
            "member_key"             => $member_info["member_key"],
            "room_key"               => $room_key,
            "max_port"               => $_guests["manager"]["is_manager"] ? "3" : $max_port ,
            "reservation_name"       => $reservation_name,
            "reservation_place"      => $reservation_place,
            "reservation_starttime"  => $reservation_starttime,
            "reservation_endtime"    => $reservation_endtime,
            "sender"                 => $sender_name,
            "sender_mail"            => $sender_email,
            "mail_body"              => $reservation_info,
            "mail_type"              => $mail_type,
            "is_reminder_flg"        => $is_reminder_flg,
            "is_convert_wb_to_pdf"   => $is_convert_wb_to_pdf,
            "is_rec_flg"             => $is_rec_flg,
            "is_cabinet_flg"         => $is_cabinet_flg,
            "is_desktop_share"       => $is_desktop_share,
            "is_invite_flg"          => $is_invite_flg,
            "is_limited_function"    => $is_limited_function,
            "is_organizer_flg"       => $organizer_flag,
            "is_authority_flg"       => $authority_flag
            );
        $room_info  = $this->session->get("room_info");
        $room_keys  = array_keys($room_info);
        $_room_info = $room_info[$room_key];

        if($_room_info["room_info"]["use_sales_option"] && ($this->session->get("role") == "10" || $member_info["use_ss_watcher"])){
            // 監視権限ありの場合、他の部屋は予約対象外
            require_once("classes/dbi/member_room_relation.dbi.php");
            $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
            $room_relation = $objRoomRelation->getRoomRelation($member_info["member_key"]);
            if( $room_relation["room_key"] != $room_key){
               return $this->display_error(100, "not member room relation ");
            }
            $room_keys  = $room_relation["room_key"];
        }else{
            $room_keys  = array_keys($room_info);
        }
                                    
        if (!$this->config->get("IGNORE_MENU", "teleconference") && $_room_info['options']['teleconference']){
            if ($teleconf_flg) {
                if(!$_room_info['room_info']['use_teleconf']){
                    return $this->display_error(100, 'teleconference not allowed.');
                }else{
                    if(!$_room_info['room_info']['use_pgi_dialin'] && $use_pgi_dialin){
                        return $this->display_error(100, 'use_pgi_dialin not allowed.');
                    }
                    if(!$_room_info['room_info']['use_pgi_dialin_free'] && $use_pgi_dialin_free){
                        return $this->display_error(100, 'use_pgi_dialin_free not allowed.');
                    }
                    if(!$_room_info['room_info']['use_pgi_dialout'] && $use_pgi_dialout){
                        return $this->display_error(100, 'use_pgi_dialout not allowed.');
                    }
                    if(!$_room_info['room_info']['use_pgi_dialin_lo_call'] && $use_pgi_dialin_lo_call){
                        return $this->display_error(100, 'use_pgi_dialin_lo_call not allowed.');
                    }
                    $reservation_data["info"]["tc_type"] = "pgi";
                }
            } else {
                $reservation_data["info"]["tc_type"] = "voip";
            }
            if ($reservation_data["info"]['tc_type'] == 'pgi') {
                if (!$use_pgi_dialin && !$use_pgi_dialin_free && !$use_pgi_dialin_lo_call && !$use_pgi_dialout) {
                    return $this->display_error(100, "Please select the dial method for the teleconferencing.");
                }else{
                    $reservation_data['info']['use_pgi_dialin']         = $use_pgi_dialin;
                    $reservation_data['info']['use_pgi_dialin_free']    = $use_pgi_dialin_free;
                    $reservation_data['info']['use_pgi_dialout']        = $use_pgi_dialout;
                    $reservation_data['info']['use_pgi_dialin_lo_call'] = $use_pgi_dialin_lo_call;
                }
            }else{
                $reservation_data['info']['use_pgi_dialin']         = $_room_info['room_info']['use_pgi_dialin'];
                $reservation_data['info']['use_pgi_dialin_free']    = $_room_info['room_info']['use_pgi_dialin_free'];
                $reservation_data['info']['use_pgi_dialout']        = $_room_info['room_info']['use_pgi_dialout'];
                $reservation_data['info']['use_pgi_dialin_lo_call'] = $_room_info['room_info']['use_pgi_dialin_lo_call'];
            }
        }
        // 予約会議登録
        if($organizer_flag == '2'){
            $_organizer['name'] = $sender_name;
            $_organizer['email']= $sender_email;
            $_organizer['r_organizer_flg'] = 1;
            $_organizer['timezone'] = 100;
            $_organizer['lang'] = $this->session->get("lang");
        }elseif($organizer_flag == '1'){
            $_organizer = $this->request->get('organizer');
            $_organizer["timezone"]         = (@is_numeric($_organizer["timezone"])) ? $_organizer["timezone"] : 100;
            $_organizer["lang"]             = $_organizer["lang"] ? $_organizer["lang"] : $this->session->get("lang");
            $_organizer['r_organizer_flg']  = 1;
        }
        $reservation_data['organizer'] = $_organizer;
        if($authority_flag == '1'){
            $_authority = $this->request->get('presenter');
            $_authority['timezone']         = (@is_numeric($_authority['timezone'])) ? $_authority['timezone'] : 100;
            $_authority['lang']             = $_authority['lang'] ? $_authority['lang'] : $this->session->get("lang");
            $_authority['r_organizer_flg']  = 0;
            $_authority['r_user_authority'] = 1;
        }
        $reservation_data['authority'] = $_authority;
        if ($reservation_pw) {
            $reservation_data["pw_flg"] = 1;
            $reservation_data["info"]["reservation_pw"] = $reservation_pw;
            if($reservation_pw_type == 1){
                $reservation_data["info"]["reservation_pw_type"] = 1;
            }else{
                $reservation_data["info"]["reservation_pw_type"] = 2;
            }
        }
        if ($_room_info['options']['multicamera'] > 0) {
            $reservation_data["info"]["is_multicamera"] = 1;
        }
        $err_obj = $this->action_check($reservation_data);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // 参加者更新
        $timezone_list = array_keys($this->get_timezone_list());
        $timezone_list[] = 100;
        $guest_rules = array(
            "name"     => array("required" => true,
                                "maxlen"   => 50),
            "email"    => array("required" => true,
                                "email"    => true,),
            "timezone" => array("allow"    => $timezone_list,),
            "lang"     => array("allow"    => array_keys($this->get_language_list())));

        if ($_guests) {
            // 本当は直接招待者のみ取得したいが、N2MY_Reservation::getParticipantListが、reservation_keyで取得している。こちらを変更する必要有り
            $error_info = array();
            if ($_room_info["room_info"]["use_sales_option"]) {
                $guest_rules["type"] = array("allow"    => array("staff,customer"));
                require_once("classes/dbi/member_room_relation.dbi.php");
                $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
                $where = "room_key = '".addslashes($room_key)."'";
                $room_relation = $objRoomRelation->getRow($where);
                $staff_count = 0;
                $customer_count = 0;
            } else {
                $guest_rules["type"] = array("allow"    => array("audience"));
            }

            // 一括登録対応
            $guests = array();
            if ($_room_info["room_info"]["use_sales_option"]) {
                if ($_guests["staff"]) {
                    //古いAPI利用者用対応(zhの場合はzh-cnに変更)
                    if ($_guests["staff"]["lang"] == "zh") {
                        $_guests["staff"]["lang"] = "zh-cn";
                    }
                    $guests["staff"] = array(
                        "name"     => $_guests["staff"]["name"],
                        "email"    => $_guests["staff"]["email"],
                        "timezone" => (@is_numeric($_guests["staff"]["timezone"])) ? $_guests["staff"]["timezone"] : 100,
                        "lang"     => ($_guests["staff"]["lang"]) ? $_guests["staff"]["lang"] : $this->session->get("lang"),
                        "type"     => "",
                        "r_organizer_flg"     => 0,
                        "member_key" => $room_relation["member_key"],
                    );
                } else if (!$_guests["staff"]) {
                  $err_obj->set_error(API_GUEST_ID, "RESERVATION_GUEST_STAFF_INVALID");
                }
                if ($_guests["customer"]) {
                    //古いAPI利用者用対応(zhの場合はzh-cnに変更)
                    if ($_guests["customer"]["lang"] == "zh") {
                        $_guests["customer"]["lang"] = "zh-cn";
                    }
                    $guests["customer"] = array(
                        "name"     => $_guests["customer"]["name"],
                        "email"    => $_guests["customer"]["email"],
                        "timezone" => (@is_numeric($_guests["customer"]["timezone"])) ? $_guests["customer"]["timezone"] : 100,
                        "lang"     => ($_guests["customer"]["lang"]) ? $_guests["customer"]["lang"] : $this->session->get("lang"),
                        "type"     => "",
                        "r_organizer_flg"     => 0,
                    );
                } else if (!$_guests["customer"]) {
                    $err_obj->set_error(API_GUEST_ID, "RESERVATION_GUEST_CUSTOMER_INVALID");
                }
                
                // 監視者を参加させる場合、名前とメールは必須項目とする
                if ($_guests["manager"]["is_manager"]) {
                    if (!$_guests["manager"]["name"]) {
                        $err_obj->set_error(API_MANAGER_ID, "RESERVATION_MANAGER_INVALID");
                    }
                    if (!$_guests["manager"]["email"]) {
                        $err_obj->set_error(API_MANAGER_MAIL, "RESERVATION_MANAGER_INVALID");
                    }
                    //古いAPI利用者用対応(zhの場合はzh-cnに変更)
                    if ($_guests["manager"]["lang"] == "zh") {
                        $_guests["manager"]["lang"] = "zh-cn";
                    }
                    $guests["manager"] = array(
                                    "name"     => $_guests["manager"]["name"],
                                    "email"    => $_guests["manager"]["email"],
                                    "timezone" => (@is_numeric($_guests["manager"]["timezone"])) ? $_guests["manager"]["timezone"] : 100,
                                    "lang"     => ($_guests["manager"]["lang"]) ? $_guests["manager"]["lang"] : $this->session->get("lang"),
                                    "type"     => "",
                                    "r_organizer_flg"     => 0,
                    );
                }
            } else {
                foreach ($_guests as $key => $guest) {
                    $type = ($type == "audience") ? 1 : "";
                    // メールアドレスがないユーザーが招待者と見なさない
                    if ($guest["email"]) {
                        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
                        if ($guest["lang"] == "zh") {
                            $guest["lang"] = "zh-cn";
                        }
                        $guests[$key] = array(
                            "name"     => $guest["name"],
                            "email"    => $guest["email"],
                            "timezone" => (@is_numeric($guest["timezone"])) ? $guest["timezone"] : 100,
                            "lang"     => ($guest["lang"]) ? $guest["lang"] : $this->session->get("lang"),
                            "type"     => $guest["type"],
                            "r_organizer_flg"     => 0,
                        );
                    }
                }
            }
            // 入力チェック
            foreach ($guests as $key => $guest) {
                $guest_err_obj = $this->error_check($guest, $guest_rules);
                if (EZValidator::isError($guest_err_obj)) {
                    $error_info[] = $this->get_error_info($guest_err_obj);
                } else {
                    $guests[$key]["type"] = ($guest["type"] == "audience") ? 1 : "";
                }
                if($guest['type'] == "audience" && $_room_info['room_info']['max_audience_seat'] <= 0){
                    return $this->display_error(100, "PARAMETER_ERROR", "Room Max Audience Seat is 0");
                }
            }
            if ($error_info) {
                $err_obj->set_error("guests", "regex");
            }
            $reservation_data["guest_flg"] = 1;
            $reservation_data["guests"] = $guests;
        } else if(!$_guests && $_room_info["room_info"]["use_sales_option"]) {
            $err_obj->set_error(API_GUEST_ID, "RESERVATION_GUEST_INVALID");
        }

        $reservation_data["send_mail"] = $send_mail;
        if($_authority){
          $authority_err_obj = $this->error_check($_authority, $guest_rules);
          if (EZValidator::isError($authority_err_obj)) {
              $error_info[] = $this->get_error_info($authority_err_obj);
              $err_obj->set_error("authority", "regex");
          }
        }
        if($_organizer){
          $orginizer_err_obj = $this->error_check($_organizer, $guest_rules);
          if (EZValidator::isError($orginizer_err_obj)) {
              $error_info[] = $this->get_error_info($orginizer_err_obj);
              $err_obj->set_error("organizer", "regex");
          }
        }
        
        // エラー確認
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        if($_room_info["room_info"]["use_sales_option"]){
            $reservation_data["staff"] = $reservation_data["guests"]["staff"];
            $reservation_data["guest"] = $reservation_data["guests"]["customer"];
            if($_guests["manager"]["is_manager"]){
                $reservation_data["manager"] = $reservation_data["guests"]["manager"];
                if($user_info["use_port_plan"] == "1" && $user_info["entry_mode"] !="0"){
                    // ポート制御かつ監視者ありのため３人で予約
                    $reservation_data["info"]["max_port"] = "3";
                }
                $reservation_data["is_manager"] = "1";
            }
            unset($reservation_data["guests"]);
        }
        // リマインダー用 BASE_URL
        $reservation_data["info"]["reservation_url"] = N2MY_BASE_URL;
        $result = $this->objReservation->add($reservation_data);
        $guests = $this->_format_guests($result["guests"],$_room_info["room_info"]["use_sales_option"]);
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $result["reservation_session"]);
        $encrypted_data = str_replace("/", "-", $encrypted_data);
        $encrypted_data = str_replace("+", "_", $encrypted_data);
        $data = array(
            API_RESERVATION_ID => $result["reservation_session"],
            "url"       => N2MY_BASE_URL."y/".$encrypted_data,
            "pin_cd"    => $result["pin_cd"],
            "guests"    => $guests,
            "presenter" => $this->_format_authority($result["reservation_key"]),
        );
        if($_room_info["room_info"]["is_one_time_meeting"]){
            //one time だったら部屋のライフタイムを伸ばす
            $this->update_room_life_time($_room_info["room_info"] , $reservation_endtime);
        }
        return $this->output($data);
    }

    function action_add_sales() {
      $this->action_add();
    }

    // one time meeting用life timeを更新する
    function update_room_life_time($room_info,$life_time){
        //one time だったら部屋のライフタイムを伸ばす
        require_once ("classes/dbi/room.dbi.php");
        $obj_room = new RoomTable($this->get_dsn());
        $room_where = "room_key = '" . $room_info["room_key"] . "'";
        $room_data = array(
                "life_date_time" => $life_time
        );
        $obj_room->update($room_data, $room_where);
    }

    /**
     * 予約変更
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string meeting_name 会議名
     * @param datetime start_time 変更後開始時間
     * @param datetime end_time 変更後終了時間
     * @return boolean
     */
    function action_update(){
    	
    	if ( !$this->isActiveVidToken() ) {
    		// 活性が確認できなければセッションエラー
    		$this->session->clear();
    		parent::display_error(1, 'Login failed / Invalid auth token', $_REQUEST[N2MY_SESS_NAME]);
    		exit();
    	}
    	
        $user_info               = $this->session->get("user_info");
        $member_info             = $this->session->get("member_info");
        $user_key                = $user_info["user_key"];
        $room_key                = $this->request->get(API_ROOM_ID);
        $reservation_session     = $this->request->get(API_RESERVATION_ID);
        $reservation_name        = $this->request->get("name");
        $reservation_place       = $this->session->get("time_zone"); // セッションの値から取得
        $_starttime              = $this->request->get("start");
        $_endtime                = $this->request->get("end");
        $max_port                = $this->request->get("max_port");
        $reservation_pw          = $this->request->get("password");
        $sender_name             = $this->request->get("sender_name");
        $sender_email            = $this->request->get("sender_email");
        $reservation_info        = $this->request->get("info");
        $_guests                 = $this->request->get("guests");
        $guest_flg               = $this->request->get("guest_flg", 1);
        $send_mail               = $this->request->get("send_mail", 1);
        $mail_type               = $this->request->get("mail_type");
        $is_reminder_flg         = $this->request->get("is_reminder");
        $mail_send_type          = $this->request->get("mail_send_type");
        $is_convert_wb_to_pdf    = $this->get_value($this->request->get("is_convert_wb_to_pdf"));
        $is_rec_flg              = $this->get_value($this->request->get("is_rec"));
        $is_cabinet_flg          = $this->get_value($this->request->get("is_cabinet"));
        $is_desktop_share        = $this->get_value($this->request->get("is_desktop_share"));
        $is_invite_flg           = $this->get_value($this->request->get("is_invite"));
        $organizer_flag          = in_array($this->request->get("organizer_flag"), array(0, 1, 2)) ? $this->request->get("organizer_flag") : 0;
        $authority_flag          = in_array($this->request->get("presenter_flag"), array(0, 1)) ? $this->request->get("presenter_flag") : 0;
        $teleconf_flg            = in_array($this->request->get('teleconf_flg'), array(0,1)) ? $this->request->get('teleconf_flg') : 0;
        $use_pgi_dialin          = in_array($this->request->get('use_pgi_dialin'), array(0,1)) ? $this->request->get('use_pgi_dialin') : 0;
        $use_pgi_dialin_free     = in_array($this->request->get('use_pgi_dialin_free'), array(0,1)) ? $this->request->get('use_pgi_dialin_free') : 0;
        $use_pgi_dialin_lo_call  = in_array($this->request->get('use_pgi_dialin_lo_call'), array(0,1)) ? $this->request->get('use_pgi_dialin_lo_call') : 0;
        $use_pgi_dialout         = in_array($this->request->get('use_pgi_dialout'), array(0,1)) ? $this->request->get('use_pgi_dialout') : 0;
        $reservation_data = $this->objReservation->getDetail($reservation_session);
        if(date("Y-m-d H:i:s") > $reservation_data["info"]["reservation_endtime"]){
            $reservation_data = $this->objReservation->getDetail($reservation_session);
            $meeting_ticket = $reservation_data["info"]["meeting_key"];
            $where = "meeting_ticket = '".$meeting_ticket."'";
            $meeting_data = $this->obj_Meeting->getRow($where);
            // 会議終了判断
            if ($meeting_data["is_active"] == 0) {
                return $this->display_error(1,ROOM_STATUS_ERROR, "Is not active meeting");
            }else{
                $where = " meeting_key = '".$meeting_data["meeting_key"]."'" . " AND is_active = '1' ";
                $participant_data = $this->obj_Participant->getRow($where);
                // is_activeが更新されていない場合を考慮し、入室者の存在でも判断を行う
                if($participant_data["is_active"] == null){
                    return $this->display_error(1,ROOM_STATUS_ERROR, "Is not active meeting");
                }
            }
        }
        // タイプによってroomlist更新
        if($user_info["is_one_time_meeting"] == 1 &&  $this->session->get("service_mode") != "sales"){
            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"] , null , true);
            $this->session->set("room_info" ,$rooms);
        }elseif($this->session->get("service_mode") == "sales" && ($member_info['use_ss_watcher'] || $this->session->get("role") == "10")){
            // manager権限を持つ場合、全部屋を取得しているので、紐付いている部屋だけを取り直す
            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $rooms = $obj_N2MY_Account->getOwnRoom( $member_info["member_key"], $user_info["user_key"], "sales" );
            $this->session->set("room_info" ,$rooms);
        }
        // 時差変換
        if ($_starttime) {
            if (is_numeric($_starttime)) {
                $reservation_starttime = EZDate::getZoneDate("Y-m-d H:i:s", $reservation_place, $_starttime);
            } else {
                $reservation_starttime = $_starttime;
            }
        }
        if ($_endtime) {
            if (is_numeric($_endtime)) {
                $reservation_endtime = EZDate::getZoneDate("Y-m-d H:i:s", $reservation_place, $_endtime);
            } else {
                $reservation_endtime = $_endtime;
            }
        }
        $room_info  = $this->session->get("room_info");
        if($room_info["use_sales_option"] && ($this->session->get("role") == "10" || $member_info["use_ss_watcher"])){
            // 監視権限ありの場合、他の部屋は予約対象外
            require_once("classes/dbi/member_room_relation.dbi.php");
            $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
            $room_relation = $objRoomRelation->getRoomRelation($member_info["member_key"]);
            if( $room_relation["room_key"] != $room_key){
               return $this->display_error(100, "not member room relation ");
            }
            $room_keys  = $room_relation["room_key"];
        }else{
            $room_keys  = array_keys($room_info);
        }
        $_room_info = $room_info[$room_key];
        if($is_convert_wb_to_pdf == 0 || $is_rec_flg == 0 || $is_cabinet_flg == 0 || $is_desktop_share == 0 || $is_invite_flg == 0){
            $is_limited_function = 1;
        }else{
            $is_limited_function = 0;
        }
        $reservation_data["info"] = array(
            "room_key"               => $room_key,
            "reservation_session"    => $reservation_session,
            "reservation_name"       => $reservation_name,
            "reservation_place"      => $reservation_place,
            "reservation_starttime"  => $reservation_starttime,
            "reservation_endtime"    => $reservation_endtime,
            "max_port"               => $max_port,
            "sender"                 => $sender_name,
            "sender_mail"            => $sender_email,
            "mail_body"              => $reservation_info,
            "user_key"               => $user_key,
            "mail_type"              => $mail_type,
            "is_reminder_flg"        => $is_reminder_flg,
            "is_convert_wb_to_pdf"   => $is_convert_wb_to_pdf,
            "is_rec_flg"             => $is_rec_flg,
            "is_cabinet_flg"         => $is_cabinet_flg,
            "is_desktop_share"       => $is_desktop_share,
            "is_invite_flg"          => $is_invite_flg,
            "is_limited_function"    => $is_limited_function,
            "is_organizer_flg"       => $organizer_flag,
            "is_authority_flg"       => $authority_flag
            );
        if (!$this->config->get("IGNORE_MENU", "teleconference") && $_room_info['options']['teleconference']){
            if ($teleconf_flg) {
                if(!$_room_info['room_info']['use_teleconf']){
                    return $this->display_error(100, 'teleconference not allowed.');
                }else{
                    if(!$_room_info['room_info']['use_pgi_dialin'] && $use_pgi_dialin){
                        return $this->display_error(100, 'use_pgi_dialin not allowed.');
                    }
                    if(!$_room_info['room_info']['use_pgi_dialin_free'] && $use_pgi_dialin_free){
                        return $this->display_error(100, 'use_pgi_dialin_free not allowed.');
                    }
                    if(!$_room_info['room_info']['use_pgi_dialout'] && $use_pgi_dialout){
                        return $this->display_error(100, 'use_pgi_dialout not allowed.');
                    }
                    if(!$_room_info['room_info']['use_pgi_dialin_lo_call'] && $use_pgi_dialin_lo_call){
                        return $this->display_error(100, 'use_pgi_dialin_lo_call not allowed.');
                    }
                    $reservation_data["info"]["tc_type"] = "pgi";
                }
            } else {
                $reservation_data["info"]["tc_type"] = "voip";
            }
            if ($reservation_data["info"]['tc_type'] == 'pgi') {
                if (!$use_pgi_dialin && !$use_pgi_dialin_free && !$use_pgi_dialin_lo_call && !$use_pgi_dialout) {
                    return $this->display_error(100, "Please select the dial method for the teleconferencing.");
                }else{
                    $reservation_data['info']['use_pgi_dialin']         = $use_pgi_dialin;
                    $reservation_data['info']['use_pgi_dialin_free']    = $use_pgi_dialin_free;
                    $reservation_data['info']['use_pgi_dialout']        = $use_pgi_dialout;
                    $reservation_data['info']['use_pgi_dialin_lo_call'] = $use_pgi_dialin_lo_call;
                }
            }else{
                $reservation_data['info']['use_pgi_dialin']         = $_room_info['room_info']['use_pgi_dialin'];
                $reservation_data['info']['use_pgi_dialin_free']    = $_room_info['room_info']['use_pgi_dialin_free'];
                $reservation_data['info']['use_pgi_dialout']        = $_room_info['room_info']['use_pgi_dialout'];
                $reservation_data['info']['use_pgi_dialin_lo_call'] = $_room_info['room_info']['use_pgi_dialin_lo_call'];
            }
        }
        if($organizer_flag == '2'){
            $_organizer['name'] = $sender_name;
            $_organizer['email']= $sender_email;
            $_organizer['r_organizer_flg'] = 1;
            $_organizer['timezone'] = 100;
            $_organizer['lang'] = $this->session->get("lang");
        }elseif($organizer_flag == '1'){
            $_organizer = $this->request->get('organizer');
            $_organizer["timezone"]         = (@is_numeric($_organizer["timezone"])) ? $_organizer["timezone"] : 100;
            $_organizer["lang"]             = $_organizer["lang"] ? $_organizer["lang"] : $this->session->get("lang");
            $_organizer['r_organizer_flg']  = 1;
        }
        $reservation_data['organizer'] = $_organizer;
        if($authority_flag == '1'){
            $_authority = $this->request->get('presenter');
            $_authority['timezone']         = (@is_numeric($_authority['timezone'])) ? $_authority['timezone'] : 100;
            $_authority['lang']             = $_authority['lang'] ? $_authority['lang'] : $this->session->get("lang");
            $_authority['r_organizer_flg']  = 0;
            $_authority['r_user_authority'] = 1;
        }
        $reservation_data['authority'] = $_authority;
        // 予約会議登録
        if ($reservation_pw) {
            $reservation_data["pw_flg"] = 1;
            $reservation_data["info"]["reservation_pw"] = $reservation_pw;
        }
        $err_obj = $this->action_check($reservation_data);
        if (!$_reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            // ログインユーザ以外の予約
            if ($_reservation_data["info"]["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            }
            $reservation_data["info"]["meeting_key"] = $_reservation_data["info"]["meeting_key"];
        }
        if ($_reservation_data["info"]["room_key"] != $reservation_data["info"]["room_key"]) {
            $err_obj->set_error("room_key", "ROOMKEY_INVALID");
        }
        if (@$_reservation_data["info"]["is_multicamera"] > 0) {
            $reservation_data["info"]["is_multicamera"] = 1;
        }
        $reservation_data["info"]["reservation_key"] = $_reservation_data["info"]["reservation_key"];
        // 参加者更新
        $reservation_data["guest_flg"] = $guest_flg;
        $timezone_list = array_keys($this->get_timezone_list());
        $timezone_list[] = 100;
        $guest_rules = array(
            "name"     => array("required" => true,
                                "maxlen"   => 50),
            "email"    => array("required" => true,
                                "email"    => true),
            "timezone" => array("allow"    => $timezone_list,),
            "lang"     => array("allow"    => array_keys($this->get_language_list())),
            );
        $guests = array();
        if ($_guests) {
            $error_info = array();
            if ($_room_info["room_info"]["use_sales_option"]) {
                $guest_rules["type"] = array("allow"    => array("staff,customer"));
                require_once("classes/dbi/member_room_relation.dbi.php");
                $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
                $where = "room_key = '".addslashes($room_key)."'";
                $room_relation = $objRoomRelation->getRow($where);
            } else {
                $guest_rules["type"] = array("allow"    => array("audience"));
            }
            if ($_room_info["room_info"]["use_sales_option"]) {
              if ($_guests["staff"]) {
                //古いAPI利用者用対応(zhの場合はzh-cnに変更)
                if ($_guests["staff"]["lang"] == "zh") {
                  $_guests["staff"]["lang"] = "zh-cn";
                }
                $guests["staff"] = array(
                    "r_user_session" => $_guests["staff"]["guest_id"],
                    "name"     => $_guests["staff"]["name"],
                    "email"    => $_guests["staff"]["email"],
                    "timezone" => (@is_numeric($_guests["staff"]["timezone"])) ? $_guests["staff"]["timezone"] : 100,
                    "lang"     => ($_guests["staff"]["lang"]) ? $_guests["staff"]["lang"] : $this->session->get("lang"),
                    "type"     => "",
                    "r_organizer_flg"     => 0,
                    "member_key" => $room_relation["member_key"],
                );
              } else if (!$_guests["staff"]) {
                $err_obj->set_error(API_GUEST_ID, "RESERVATION_GUEST_STAFF_INVALID");
              }
              if ($_guests["customer"]) {
                //古いAPI利用者用対応(zhの場合はzh-cnに変更)
                if ($_guests["customer"]["lang"] == "zh") {
                  $_guests["customer"]["lang"] = "zh-cn";
                }
                $guests["customer"] = array(
                    "r_user_session" => $_guests["customer"]["guest_id"],
                    "name"     => $_guests["customer"]["name"],
                    "email"    => $_guests["customer"]["email"],
                    "timezone" => (@is_numeric($_guests["customer"]["timezone"])) ? $_guests["customer"]["timezone"] : 100,
                    "lang"     => ($_guests["customer"]["lang"]) ? $_guests["customer"]["lang"] : $this->session->get("lang"),
                    "type"     => "",
                    "r_organizer_flg"     => 0,
                );
              } else if (!$_guests["customer"]) {
                $err_obj->set_error(API_GUEST_ID, "RESERVATION_GUEST_CUSTOMER_INVALID");
              }

              if ($_guests["manager"]["is_manager"]) {
                  if (!$_guests["manager"]["name"]) {
                      $err_obj->set_error(API_MANAGER_ID, "RESERVATION_MANAGER_INVALID");
                  }
                  if (!$_guests["manager"]["email"]) {
                      $err_obj->set_error(API_MANAGER_MAIL, "RESERVATION_MANAGER_INVALID");
                  }
                  //古いAPI利用者用対応(zhの場合はzh-cnに変更)
                  if ($_guests["manager"]["lang"] == "zh") {
                      $_guests["manager"]["lang"] = "zh-cn";
                  }
                  $guests["manager"] = array(
                                  "r_user_session" => $_guests["manager"]["guest_id"],
                                  "name"     => $_guests["manager"]["name"],
                                  "email"    => $_guests["manager"]["email"],
                                  "timezone" => (@is_numeric($_guests["manager"]["timezone"])) ? $_guests["manager"]["timezone"] : 100,
                                  "lang"     => ($_guests["manager"]["lang"]) ? $_guests["manager"]["lang"] : $this->session->get("lang"),
                                  "type"     => "",
                                  "r_organizer_flg"     => 0,
                  );
              }
              
              if ($_guests["manager"]["is_manager"]) {
                  if (!$_guests["manager"]["name"]) {
                      $err_obj->set_error(API_MANAGER_ID, "RESERVATION_MANAGER_INVALID");
                  }
                  if (!$_guests["manager"]["email"]) {
                      $err_obj->set_error(API_MANAGER_MAIL, "RESERVATION_MANAGER_INVALID");
                  }
                  //古いAPI利用者用対応(zhの場合はzh-cnに変更)
                  if ($_guests["manager"]["lang"] == "zh") {
                      $_guests["manager"]["lang"] = "zh-cn";
                  }
                  $guests["manager"] = array(
                                  "r_user_session" => $_guests["manager"]["guest_id"],
                                  "name"     => $_guests["manager"]["name"],
                                  "email"    => $_guests["manager"]["email"],
                                  "timezone" => (@is_numeric($_guests["manager"]["timezone"])) ? $_guests["manager"]["timezone"] : 100,
                                  "lang"     => ($_guests["manager"]["lang"]) ? $_guests["manager"]["lang"] : $this->session->get("lang"),
                                  "type"     => "",
                                  "r_manager_flg"     => 1,
                                  "r_organizer_flg"     => 0,
                  );
                  // 監視者を含め予約のため、最大参加者数を3に変更
                  $reservation_data["info"]["max_port"] = "3";
              }else{
                  // 会議開始後マネージャが入室した場合は値を変更しない
                  if($_reservation_data["info"]["is_manager"] == "2"){
                        $reservation_data["info"]["is_manager"] = "2";
                        $reservation_data["info"]["max_port"] = "3";
                  }else{
                      if($user_info["use_port_plan"] == "1" && $user_info["entry_mode"] !="0"){
                          $reservation_data["info"]["max_port"] = "2";
                      }
                      $managerList = $this->objReservationUser->getUserList($reservation_data["info"]["reservation_key"],"true");
                      if($managerList){
                          // 監視者を含め予約を行った場合、対   象の監視者を無効にする
                          $managerList = $this->objReservationUser->managerCancel($reservation_data["info"]["reservation_key"],"true");
                      }
                  }
              }
            } else {
              // 一括登録対応
              foreach($_guests as $key => $guest) {
                  // メールアドレスがないユーザーが招待者と見なさない
                  if ($guest["email"]) {
                      $type = ($type == "audience") ? 1 : "";
                      //古いAPI利用者用対応(zhの場合はzh-cnに変更)
                      if ($guest["lang"] == "zh") {
                          $guest["lang"] = "zh-cn";
                      }
                      $guests[$key] = array(
                          "reservation_key" => $_reservation_data["info"]["reservation_key"],
                          "name"            => $guest["name"],
                          "email"           => $guest["email"],
                          "timezone"        => (is_numeric($guest["timezone"])) ? $guest["timezone"] : 100,
                          "lang"            => ($guest["lang"]) ? $guest["lang"] : $this->session->get("lang"),
                          "type"            => $guest["type"],
                          "r_user_session"  => $guest["guest_id"],
                          "r_organizer_flg" => 0,
                      );
                  }
              }
            }
            $this->logger2->warn($guests);
            // 入力チェック
            foreach ($guests as $key => $guest) {
                $guest_err_obj = $this->error_check($guest, $guest_rules);
                if (EZValidator::isError($guest_err_obj)) {
                    $error_info[] = $this->get_error_info($guest_err_obj);
                } else {
                    $guests[$key]["type"] = ($guest["type"] == "audience") ? 1 : "";
                }
                if($guest['type'] == "audience" && $_room_info['room_info']['max_audience_seat'] <= 0){
                    return $this->display_error(100, "PARAMETER_ERROR", "Room Max Audience Seat is 0");
                }
            }
            if ($error_info) {
                $err_obj->set_error("guests", "regex");
            }
        }
        $reservation_data["guests"]    = $guests;
        $reservation_data["send_mail"] = $send_mail;
        if($send_mail){
          if($mail_send_type == 2){
            $reservation_data["mail_send_type"] = 2;
          }else{
            $reservation_data["mail_send_type"] = 1;
          }
        }else{
          $reservation_data["mail_send_type"] = 0;
        }
        if($_authority){
            $authority_err_obj = $this->error_check($_authority, $guest_rules);
            if (EZValidator::isError($authority_err_obj)) {
                  $error_info[] = $this->get_error_info($authority_err_obj);
                $err_obj->set_error("authority", "regex");
            }
        }
        if($_organizer){
          $orginizer_err_obj = $this->error_check($_organizer, $guest_rules);
          if (EZValidator::isError($orginizer_err_obj)) {
                  $error_info[] = $this->get_error_info($orginizer_err_obj);
                  $err_obj->set_error("organizer", "regex");
              }
        }
        require_once("classes/dbi/meeting.dbi.php");
        $objMeeting = new MeetingTable($this->get_dsn());
        $pin_cd     = $objMeeting->getOne("meeting_ticket = '".addslashes($reservation_data["info"]["meeting_key"])."'", "pin_cd");
        // エラー確認
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
          if($_room_info["room_info"]["use_sales_option"]){
            $reservation_data["staff"] = $reservation_data["guests"]["staff"];
            $reservation_data["guest"] = $reservation_data["guests"]["customer"];
            if($_guests["manager"]["is_manager"]){
                $reservation_data["manager"] = $reservation_data["guests"]["manager"];
                if($user_info["use_port_plan"] == "1" && $user_info["entry_mode"] !="0"){
                    $reservation_data["info"]["max_port"] = "3";
                }
                $reservation_data["is_manager"] = "1";
             }elseif ($_reservation_data["info"]["is_manager"] == "2"){
                $reservation_data["is_manager"] = "2";
             }else{
                $reservation_data["is_manager"] = "0";
             }
          unset($reservation_data["guests"]);
          }
            $result = $this->objReservation->update($reservation_data);
            $guests = $this->_format_guests($result["guests"],$_room_info["room_info"]["use_sales_option"]);
            if($_room_info["room_info"]["is_one_time_meeting"]){
                //one time だったら部屋のライフタイムを伸ばす
                $this->update_room_life_time($_room_info["room_info"] , $reservation_endtime);
            }
            if ($this->request->get("output_type") == "php"){
                $guests["pin_cd"] = $pin_cd;
                $_output = $guests;
            } else {
                $_output["guest"] = $guests;
                $_output["presenter"] = $this->_format_authority($reservation_data["info"]["reservation_key"]);
                $_output["pin_cd"] = $pin_cd;
            }
            return $this->output($_output);
        }
    }

    function action_update_sales() {
      $this->action_update();
    }

    /**
     * 予約入力チェック
     */
    function action_check($data) {
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        // 入力チェック
        $rules = array(
            "room_key"              => array("required" => true,
                                             "allow"    => array_keys($room_info)),
            "reservation_name"      => array("required" => true,
                                             "maxlen"   => 100),
            "reservation_starttime" => array("required" => true,
                                             "datetime" => true),
            "reservation_endtime"   => array("required" => true,
                                             "datetime" => true),
            "reservation_place"     => array("allow"    =>  array_keys($this->get_timezone_list())),
            "sender"                => array("maxlen"   => 50),
            "sender_email"          => array("email"    => true),
            "info"                  => array("maxlen"   => 10000),
            "reservation_pw"        => array("range"    => array("6", "16")),
            "output_type"           => array("allow"    => $this->get_output_type_list()),
        );
        $param = $data["info"];
        $err_obj = $this->error_check($param, $rules);

        // 開始、終了時間をチェック
        $limit_start_time = time() - (2 * 24 * 3600);
        $limit_end_time   = time() + (367 * 24 * 3600);
        if (strtotime($param["reservation_starttime"]) <= $limit_start_time) {
            $err_obj->set_error("reservation_starttime", "RESERVATION_STARTTIME_INVALID");
        }
        if (strtotime($param["reservation_endtime"])   >= $limit_end_time) {
            $err_obj->set_error("reservation_endtime", "RESERVATION_ENDTIME_INVALID");
        }
        if (strtotime($param["reservation_starttime"]) >= strtotime($param["reservation_endtime"])) {
            $err_obj->set_error("reservation_endtime", "RESERVATION_ERROR_INVALIDTIME");
        }

        // 終了時間が現時刻より前だった場合のエラー処理
        $svr_timezone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $_starttime = EZDate::getLocateTime($param['reservation_starttime'], $svr_timezone, $param['reservation_place']);
        $_endtime   = EZDate::getLocateTime($param['reservation_endtime'],   $svr_timezone, $param['reservation_place']);
        if ($_endtime < time()) {
            $err_obj->set_error("reservation_endtime", "RESERVATION_ENDTIME_INVALID2");
        }
        $endtime    = date('Y-m-d H:i:s', $_endtime);
        $starttime  = date('Y-m-d H:i:s', $_starttime);

        // 参加人数上限確認
        if($user_info["use_port_plan"] == "1" && $user_info["entry_mode"] == "1"){
            $reservation_data["info"] = $param;
            $check_maxport_over_flg = $this->objReservation->check($room_key, $reservation_data);

            if($check_maxport_over_flg || $param["max_port"] < "2"){
                $err_obj->set_error("max_port", "RESERVATION_ERROR_OVERPORT");
            }
        }

        // 重複一覧
        require_once("classes/dbi/reservation.dbi.php");
        $obj_Reservation = new ReservationTable($this->get_dsn());

        $where = "room_key = '".addslashes($param["room_key"])."'" .
                " AND reservation_status = 1" .
                " AND reservation_endtime != '".$starttime."'" .
                " AND reservation_starttime != '".$endtime."'" .
                " AND (" .
                "( reservation_starttime between '".$starttime."' AND '".$endtime."' )" .
                " OR ( reservation_endtime between '".$starttime."' AND '".$endtime."' )" .
                " OR ( '".$starttime."' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '".$endtime."' BETWEEN reservation_starttime AND reservation_endtime ) )";

        if ($param["reservation_session"]) {
            $where .= " AND reservation_session != '" . $param["reservation_session"]."'";
        }

        $duplicate_list = $obj_Reservation->getList($where, array("reservation_starttime" => "asc"));
        if ($duplicate_list) {
            $this->logger2->info($duplicate_list);
            $err_obj->set_error("reservation_starttime", "RESERVATION_ERROR_TIME");
        }
        return $err_obj;
    }

    /**
     * 予約削除
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return boolean
     */
    function action_delete()
    {
        
    	if ( !$this->isActiveVidToken() ) {
    		// 活性が確認できなければセッションエラー
    		$this->session->clear();
    		parent::display_error(1, 'Login failed / Invalid auth token', $_REQUEST[N2MY_SESS_NAME]);
    		exit();
    	}
    	
    	$request = $this->request->getAll();
        $reservation_session   = $this->request->get(API_RESERVATION_ID);
        $user_info = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $result = $this->objReservation->cancel($reservation_session);
            return $this->output();
        }
    }

    /**
     * 招待者一覧
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return string $output 名前、メールアドレス、言語、タイムゾーンをxmlで出力
     */
    function action_get_invite()
    {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $user_info           = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            // 本当は直接招待者のみ取得したいが、N2MY_Reservation::getParticipantListが、reservation_keyで取得している。こちらを変更する必要有り
            $country_key = $this->session->get("country_key");
            foreach ($reservation_data["guests"] as $key => $guest) {
                $guest["invite_url"] = N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"];
                $guest[API_GUEST_ID] = $guest["r_user_session"];
                unset($guest["r_user_session"]);
                $guests[] = $guest;
            }
            $data["guests"]["guest"] = $guests;
            return $this->output($data);
        }
    }
    /**
     * 招待者一覧(セールス)
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return string $output 名前、メールアドレス、言語、タイムゾーンをxmlで出力
     */
    function action_get_invite_sales()
    {
      $request = $this->request->getAll();
      $reservation_session = $this->request->get(API_RESERVATION_ID);
      $user_info           = $this->session->get("user_info");
      $rules = array(
          API_RESERVATION_ID => array(
              "required" => true
          ),
          "output_type" => array(
              "allow" => $this->get_output_type_list(),
          ),
      );
      $err_obj = $this->error_check($request, $rules);
      if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
        $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
      } else {
        $info = $reservation_data["info"];
        // ログインユーザ以外の予約
        if ($info["user_key"] != $user_info["user_key"]) {
          $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        }
      }
      if (EZValidator::isError($err_obj)) {
        return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
      } else {

        /* 
　　　  * 下記ソース箇所はDEVのみコメントアウトがない状態。更新はRev33682で実施されており、マージ漏れと予想。
        * ただし、以下の処理を行われると、ゲストIDとinvite_urlがNullとなる。
        * 影響度は小さいため、こちらはコメントアウト（DEV側の処理）として更新とする
        *
        *　ここから

        // 本当は直接招待者のみ取得したいが、N2MY_Reservation::getParticipantListが、reservation_keyで取得している。こちらを変更する必要有り
         $country_key = $this->session->get("country_key");
         $reservation_data["guest"]["invite_url"] = N2MY_BASE_URL."r/".$reservation_data["guest"]["r_user_session"].'&c='.$country_key.'&lang='.$reservation_data["guest"]["lang"];
         $reservation_data["guest"][API_GUEST_ID] = $reservation_data["guest"]["r_user_session"];
         unset($reservation_data["guest"]["r_user_session"]);
         $data["guest"] = $reservation_data["guest"];
         $reservation_data["staff"]["invite_url"] = N2MY_BASE_URL."r/".$reservation_data["staff"]["r_user_session"].'&c='.$country_key.'&lang='.$reservation_data["staff"]["lang"];
         $reservation_data["staff"][API_GUEST_ID] = $reservation_data["staff"]["r_user_session"];
         unset($reservation_data["staff"]["r_user_session"]);
         $data["staff"] = $reservation_data["staff"];
　　　　　　　*
　　　　　　　* ここまで
　　　　　　　*/
        if($reservation_data["staff"]) {
          //$data["customer"] = $reservation_data["guest"];
          //$data["staff"] = $reservation_data["staff"];
          $data["customer"] = array (
              "name" => $reservation_data["guest"]["name"],
              "email" => $reservation_data["guest"]["email"],
              "timezone" => $reservation_data["guest"]["timezone"],
              "lang" => $reservation_data["guest"]["lang"],
              "invite_url" => N2MY_BASE_URL."r/".$reservation_data["guest"]["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
              API_GUEST_ID => $reservation_data["guest"]["r_user_session"],
          );
          $data["staff"] = array (
              "member_key" => $reservation_data["staff"]["member_key"],
              "name" => $reservation_data["staff"]["name"],
              "email" => $reservation_data["staff"]["email"],
              "timezone" => $reservation_data["staff"]["timezone"],
              "lang" => $reservation_data["staff"]["lang"],
              "invite_url" => N2MY_BASE_URL."r/".$reservation_data["staff"]["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
              API_GUEST_ID => $reservation_data["staff"]["r_user_session"],
          );
          if($reservation_data["manager"]) {
              $data["manager"] = array (
              "name" => $reservation_data["manager"]["name"],
              "email" => $reservation_data["manager"]["email"],
              "timezone" => $reservation_data["manager"]["timezone"],
              "lang" => $reservation_data["manager"]["lang"],
              "invite_url" => N2MY_BASE_URL."r/".$reservation_data["manager"]["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
              API_GUEST_ID => $reservation_data["manager"]["r_user_session"],
              );
           }
        } else {
          foreach($reservation_data["guests"] as $guest) {
                      if($guest["member_key"]) {
              $data["staff"] = array (
                  "member_key" => $guest["member_key"],
                  "name" => $guest["name"],
                  "email" => $guest["email"],
                  "timezone" => $guest["timezone"],
                  "lang" => $guest["lang"],
                  API_GUEST_ID => $guest["r_user_session"],
                  "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],

              );
            } else {
              $data["customer"] = array (
                  "name" => $guest["name"],
                  "email" => $guest["email"],
                  "timezone" => $guest["timezone"],
                  "lang" => $guest["lang"],
                  API_GUEST_ID => $guest["r_user_session"],
                  "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
              );
            }
          }
        }
        return $this->output($data);
      }
    }

    function action_get_invited_list_by_member_id()
    {
        $request = $this->request->getAll();
        $user_info = $this->session->get("user_info");
        $member_id = $request["member_id"];
        $rules = array(
            "member_id"    => array("required" => true),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once 'classes/dbi/member.dbi.php';
        $obj_Member = new MemberTable($this->get_dsn());
        $where = "member_id = '".addslashes($member_id)."'" .
            " AND member_status = 0";
        $invitee_member_info = $obj_Member->getRow($where);
        if (!$invitee_member_info) {
            $err_msg["errors"][] = array(
                "field"   => "member_id",
                "err_cd"  => "invalid",
                "err_msg" => "メンバーIDが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        $_wk_start_limit  = $this->request->get("start_limit");
        $_wk_end_limit    = $this->request->get("end_limit");
//        $limit            = $this->request->get("limit", N2MY_API_GET_LIST_LIMIT);
//        $page             = $this->request->get("page", 1);
//        $sort_key         = $this->request->get("sort_key", "reservation_starttime");
//        $sort_type        = $this->request->get("sort_type", "asc");
        if ($_wk_start_limit && $_wk_end_limit) {
            // タイムスタンプ変換
            $server_time_zone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
            $user_time_zone   = $this->session->get("time_zone"); // セッションの値から取得
            if ($_wk_start_limit) {
                if (is_numeric($_wk_start_limit)) {
                    $_ts_local = $_wk_start_limit;
                } else {
                    $_ts_local = EZDate::getLocateTime($_wk_start_limit, $user_time_zone, $server_time_zone);
                }
                $start_limit = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone, $_ts_local);
            }
            $this->logger2->debug(array($_wk_start_limit, $user_time_zone, $server_time_zone, $start_limit, date("Y-m-d H:i:s", $_ts_local)));
            if ($_wk_end_limit) {
                if (is_numeric($_wk_end_limit)) {
                    $_ts_local = $_wk_end_limit;
                } else {
                    $_ts_local = EZDate::getLocateTime($_wk_end_limit, $user_time_zone, $server_time_zone);
                }
                $end_limit = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone, $_ts_local);
            }
        } else {
            $start_limit = date("Y-m-d 00:00:00");
            $end_limit = date("Y-m-d 23:59:59");
        }

        require_once( "classes/dbi/meeting_invitation_user.dbi.php" );
        $obj_MeetingInvitationUser = new MeetingInvitationUserTable( $this->get_dsn() );
        $where = "invitee_member_id = '".htmlspecialchars($invitee_member_info["member_id"])."'".
                 " AND status = 1";
        $invited_list = $obj_MeetingInvitationUser->getRowsAssoc($where);
        require_once("classes/dbi/meeting.dbi.php");
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $reservations = array();
        foreach ($invited_list as $invited) {
            $where = "meeting_key = ".$invited["meeting_key"];
            $meeting = $obj_Meeting->getRow($where);
            $where = "member_id = '".addslashes($invited["sender_member_id"])."'" .
                     " AND member_status = 0";
            $sender_member_info = $obj_Member->getRow($where);
            $meeting_starttime = "";
            if ($meeting["meeting_start_datetime"]) {
                $meeting_starttime = EZDate::getLocateTime($meeting["meeting_start_datetime"], $this->session->get('time_zone'), N2MY_SERVER_TIMEZONE);
            }
            $meeting_stoptime = "";
            if ($meeting["meeting_stop_datetime"]) {
                $meeting_stoptime = EZDate::getLocateTime($meeting["meeting_stop_datetime"], $this->session->get('time_zone'), N2MY_SERVER_TIMEZONE);
            }
            $reservations[] = array("meeting_key" => $meeting["meeting_key"],
                            API_MEETING_ID => $meeting["meeting_session_id"],
                            "meeting_name" => $meeting["meeting_name"],
                            API_ROOM_ID => $meeting["room_key"],
                            "start_datetime" => $meeting_starttime,
                            "end_datetime" => $meeting_stoptime,
                            "sender_member_name" => $sender_member_info["member_name"],
                            "sender_member_id" => $sender_member_info["member_id"],
                            "invitee_member_name" => $invitee_member_info["member_name"],
                            "invitee_member_id" => $invitee_member_info["member_id"],);
        }

        //予約会議から
        require_once ("classes/N2MY_Reservation.class.php");
        $obj_N2MY_Reservation = new N2MY_Reservation($this->get_dsn());
        $options = array("start_time" => $start_limit,
                         "end_time" => $end_limit,);
        $reservation_list = $obj_N2MY_Reservation->getInvitedMeetingList( $invitee_member_info, $options, $this->session->get('time_zone') );
        $this->logger2->debug($reservation_list);
        if ($reservation_list) {
            // 一覧表示
            foreach ($reservation_list as $reservation) {
            	if($reservation['status'] == 'end'){
            		continue;
            	}
                $where = "meeting_ticket = '".$reservation["meeting_key"]."'";
                $meeting = $obj_Meeting->getRow($where);
                if ($meeting["is_active"] == "1") {
                    $where = "member_key = '".addslashes($reservation["member_key"])."'" .
                             " AND member_status = 0";
                    $sender_member_info = $obj_Member->getRow($where);
                    $reservations[] = array("meeting_key" => $meeting["meeting_key"],
                                    API_MEETING_ID => $meeting["meeting_session_id"],
                                    "meeting_name" => $meeting["meeting_name"],
                                    API_ROOM_ID => $meeting["room_key"],
                                    "start_datetime" => $reservation["reservation_starttime"],
                                    "end_datetime" => $reservation["reservation_endtime"],
                                    "sender_member_name" => $sender_member_info["member_name"] ? $sender_member_info["member_name"] : $reservation["sender_name"],
                                    "sender_member_id" => $sender_member_info["member_id"] ? $sender_member_info["member_id"] : "",
                                    "invitee_member_name" => $invitee_member_info["member_name"],
                                    "invitee_member_id" => $invitee_member_info["member_id"],);
                }
            }
        }

        $data = array(
            "count" => count($reservations),
            "meetings"  => array("meeting" => $reservations),
            );
        return $this->output($data);

    }

    /**
     * 招待者追加
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string r_user_name 参加者名
     * @param string r_user_email 参加者メールアドレス
     * @param string r_user_lang 参加者言語
     * @param string r_user_timezone 参加者タイムゾーン
     * @return boolean
     */
    function action_add_invite()
    {
        $request  = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $send_mail           = $this->request->get("send_mail", 1);
        $name                = $this->request->get("name");
        $email               = $this->request->get("email");
        $type                = $this->request->get("type");
        $timezone            = $this->request->get("timezone");
        $lang                = $this->request->get("lang");
        $user_info           = $this->session->get("user_info");
        // 本当は直接招待者のみ取得したいが、N2MY_Reservation::getParticipantListが、reservation_keyで取得している。こちらを変更する必要有り
        $error_info = array();
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $timezone_list = array_keys($this->get_timezone_list());
        $timezone_list[] = 100;
        $guest_rules = array(
            "name"     => array(
                "required" => true,
                "maxlen" => 50,
                ),
            "email"     => array(
                "required" => true,
                "email" => true,
                ),
            "timezone"     => array(
                "allow" =>  $timezone_list,
                ),
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "type"    => array(
                "allow" => array("audience"),
                ),
            );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            $error_info[] = $this->get_error_info($err_obj);
        }
        // 一括登録対応
        $room_info  = $this->session->get("room_info");
        $_room_info = $room_info[$reservation_data['info']['room_key']];
        $guests = array();
        if (is_array($name)) {
            foreach($name as $_key => $_val) {
                $type = ($type == "audience") ? $type : "";
                // メールアドレスがないユーザーが招待者と見なさない
                if ($email[$_key]) {
                    $guests[$_key] = array(
                        "reservation_key" => $info["reservation_key"],
                        "name"            => $name[$_key],
                        "email"           => $email[$_key],
                        "timezone"        => ($timezone[$_key]) ? $timezone[$_key] : 100,
                        "lang"            => ($lang[$_key]) ? $lang[$_key] : $this->session->get("lang"),
                        "type"            => $type[$_key],
                        "r_user_session"  => $this->objReservation->getInviteId(),
                        "r_organizer_flg" => 0,
                    );
                }
            }
        } else {
            $type = ($type == "audience") ? $type : "";
            $guests[] = array(
                "reservation_key" => $info["reservation_key"],
                "name"            => $name,
                "email"           => $email,
                "timezone"        => ($timezone) ? $timezone : 100,
                "lang"            => ($lang) ? $lang : $this->session->get("lang"),
                "type"            => $type,
                "r_user_session"  => $this->objReservation->getInviteId(),
                "r_organizer_flg" => 0,
            );
        }
        // 入力チェック
        foreach ($guests as $key => $guest) {
            $guest_err_obj = $this->error_check($guest, $guest_rules);
            if (EZValidator::isError($guest_err_obj)) {
                $error_info[] = $this->get_error_info($guest_err_obj);
            }
        }
        if ($error_info) {
            return $this->display_error(100, "PARAMETER_ERROR", $error_info);
        }
        $country_key = $this->session->get("country_key");
        foreach ($guests as $key => $guest) {
            if ($guest['type'] == 'audience') {
                if($_room_info['room_info']['max_audience_seat'] <= 0){
                    return $this->display_error(100, "PARAMETER_ERROR", "Room Max Audience Seat is 0");
                }
                $guest['type'] = 1;
            }
            $guest = $this->objReservation->participant_mail($guest, $info, "create", $send_mail);
            $invited[] = array(
                API_GUEST_ID => $guest["r_user_session"],
                "invite_url"   => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
            );
        }
        require_once("classes/dbi/meeting.dbi.php");
        $objMeeting = new MeetingTable($this->get_dsn());
        $pin_cd     = $objMeeting->getOne("meeting_ticket = '".addslashes($reservation_data["info"]["meeting_key"])."'", "pin_cd");
        $data["pin_cd"] = $pin_cd;
        $data["guests"]["guest"] = $invited;
        return $this->output($data);
    }

    /**
     * 招待者削除
     *
     * @param string room_key ルームキー
     * @param string reservation_session
     * @param int r_user_key 参加者キー
     * @return boolean
     */
    function action_delete_invite()
    {
        $request  = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $invite_id           = $this->request->get(API_GUEST_ID);
        $user_info = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            API_GUEST_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        // 予約が存在しない
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            } else {
                $where = "reservation_key = " .$info["reservation_key"].
                        " AND r_user_session = '".addslashes($invite_id)."'";
                // 招待者IDが存在しない
                if (!$guest = $this->objReservationUser->getRow($where)) {
                    $err_obj->set_error(API_RESERVATION_ID, "INVITE_SESSION_INVALID");
                }
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            //$guest["lang"] = $this->request->get("lang", $this->session->get("lang"));
            $_guest["reservation_key"] = $guest["reservation_key"];
            $_guest["name"] = $guest["r_user_name"];
            $_guest["email"] = $guest["r_user_email"];
            $_guest["timezone"] = $guest["r_user_place"];
            $_guest["lang"] = $this->request->get("lang", $this->session->get("lang"));
            $_guest["type"] = $guest["r_user_audience"];
            $_guest["r_user_session"] = $guest["r_user_session"];
            $_guest["r_organizer_flg"] = $guest["r_organizer_flg"];
            $this->logger2->info(array($info, $_guest));
            $send_mail = true;
            $this->objReservation->participant_mail($_guest, $info, "deletelist", $send_mail);
            return $this->output();
        }
    }

    /**
     * 指定された招待者一覧で更新
     */
    private function _update_invite() {

    }

    /**
     * 予約の資料追加
     */
    function action_add_document() {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $name = $this->request->get("name");
        $file = $_FILES["file"];
        $user_info   = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$file) {
            $err_obj->set_error("file", "required");
        } else {
            if (!EZValidator::valid_file_ext($file["name"], split(",", $this->config->get("N2MY", "convert_format")))) {
                $err_obj->set_error("file", "file_ext", split(",", $this->config->get("N2MY", "convert_format")));
            }
        }
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $dir = $this->get_work_dir();
            $string = new EZString();
            $document_id = $string->create_id();
            $fileinfo = pathinfo($file["name"]);
            $tmp_name = $dir."/".$document_id.".".$fileinfo["extension"];
            move_uploaded_file($file["tmp_name"], $tmp_name);
            $name = ($name) ? $name : $fileinfo["basename"];
            $reservation_data = $this->objReservation->getDetail($reservation_session);
            $meeting_ticket = $reservation_data["info"]["meeting_key"];
            $format = $this->request->get('format');
            if($format != "bitmap" && $format != "vector"){
                $format = "bitmap";
            }
            $document_id = $this->objReservation->addDocument($meeting_ticket, $tmp_name, $name, $format);
            $data = array(
                "document_id" => $document_id,
                );
            unlink($tmp_name);
            return $this->output($data);
        }
    }

    /**
     * 予約の資料追加(ストレージ)
     */
    function action_add_document_storage() {
      $reservation_session = $this->request->get(API_RESERVATION_ID);
      $name = $this->request->get("name");
      //パラメータをチェック
      $request = $this->request->getAll();
      $user_info = $this->session->get("user_info");
      $rules = array(
          API_RESERVATION_ID => array(
              "required" => true
          ),
          "output_type" => array(
              "allow" => $this->get_output_type_list(),
          ),
      );
      $err_obj = $this->error_check($request, $rules);
      //$reservation_sessionでミーティングチケットを取る
      if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
        $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
      } else {
        $info = $reservation_data["info"];
        // ログインユーザ以外の予約
        if ($info["user_key"] != $user_info["user_key"]) {
          $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        }
      }
      $this->logger->info($reservation_data);
      //$document_idあるいはclip_keyでストレージファイル情報を取る
      require_once ("classes/N2MY_Storage.class.php");
        $obj_Storage = new N2MY_Storage($this->get_dsn());
        if (!$file_info = $obj_Storage->getStorageInfoByDocumentID($request["document_id"],$user_info["user_key"])) {
          $err_obj->set_error("document_id", "this file does not exist.");
        } else {
          if($file_info["status"] != "2") {
            $err_obj->set_error("document_id", "this file is prepaing...");
          }
        }
      if (EZValidator::isError($err_obj)) {
        return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
      } else {
        if($file_info["clip_key"]) {
          //$this->logger->info("ok",$file_info["clip_key"]);
          //重複か確認
          $reservation_clips = $this->getMeetingClips($reservation_data['info']['meeting_key']);
          if($reservation_clips) {
            foreach($reservation_clips as $clip) {
              $this->logger->info("clip name", $clip["clip_key"]);
              if($clip["clip_key"] == $file_info["clip_key"]) {
                $err_obj->set_error("document_id", "this video has been added.");
                return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
              }
            }
          }
          //追加
          $file_info["name"] = $name ? $name:$file_info["file_name"];
          $clips[] = $file_info;
          require_once ("classes/N2MY_Reservation.class.php");
          $obj_Reservation = new N2MY_Reservation($this->get_dsn());
          $obj_Reservation->addClipRelations($reservation_data['info']['meeting_key'], $clips, $user_info["user_key"]);
          $data["doucment_id"] = $file_info["clip_key"];

        } elseif($file_info["document_id"]) {
          require_once ("classes/N2MY_Reservation.class.php");
          $obj_Reservation = new N2MY_Reservation($this->get_dsn());
          $file_info["name"] = $name ? $name:$file_info["file_name"];
          $obj_Reservation->_addStorageFile($file_info, $reservation_data["info"]["room_key"], $reservation_data["info"]["meeting_key"], $user_info["user_id"]);
          require_once("classes/core/dbi/Document.dbi.php");
          $obj_Document = new DBI_Document($this->get_dsn());
          $where = "document_path like '%".$reservation_data["info"]["meeting_key"]."%'";
          $sort = array("document_key" => "desc");
          $data["doucment_id"] = $obj_Document->getOne($where, "document_id", $sort);
        }
        return $this->output($data);
      }
    }

    private function getMeetingClips($meeting_ticket) {
      require_once ("classes/dbi/meeting.dbi.php");
      $meeting_table = new MeetingTable($this->get_dsn());
      $meeting_key   = $meeting_table->findMeetingKeyByMeetingTicket($meeting_ticket);
      require_once ("classes/dbi/meeting_clip.dbi.php");
      $meeting_clip_table = new MeetingClipTable($this->get_dsn());
      return $meeting_clip_table->findByMeetingKey($meeting_key);
    }

    /**
     * 更新
     */
    function action_update_document() {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $document_id = $this->request->get("document_id");
        $user_info   = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "document_id" => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            }
            if(substr($document_id, 0, 3) === 'MC_') {
              $clip_files = $this->getMeetingClips($reservation_data['info']['meeting_key']);
              if(!$clip_files) {
                $err_obj->set_error("document_id", "DOCUMENT_INVALID");
              } else {
                $clip_key = 0;
                foreach($clip_files as $clip) {
                  if($document_id == $clip["clip_key"]) {
                    $clip_key = $clip["meeting_clip_key"];
                    break;
                  }
                }
                if($clip_key == 0) {
                  $err_obj->set_error("document_id", "CLIP_INVALID");
                }
              }
            } else {
              if (!array_key_exists($document_id, $reservation_data["documents"])) {
                $err_obj->set_error("document_id", "DOCUMENT_INVALID");
              }
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
          if(substr($document_id, 0, 3) === 'MC_') {
            require_once ("classes/dbi/meeting_clip.dbi.php");
            $obj_meetingClip = new MeetingClipTable($this->get_dsn());
            $where = sprintf( "meeting_clip_key='%s' AND is_deleted=0", $clip_key );
            $data = array(
                    "meeting_clip_name" => $this->request->get("name"),
                    "updatetime" => date("Y-m-d H:i:s"),
                );
            $obj_meetingClip->update($data, $where);
          } else {
            $data = array(
                "name" => $this->request->get("name"),
                "sort" => $this->request->get("sort"),
            );
            $this->objReservation->updateDocument($document_id, $data);
          }

            return $this->output();
        }
    }

    /**
     * 削除
     */
    function action_delete_document() {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $document_id = $this->request->get("document_id");
        $user_info   = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "document_id" => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            }
            if(substr($document_id, 0, 3) === 'MC_') {
              $clip_files = $this->getMeetingClips($reservation_data['info']['meeting_key']);
              if(!$clip_files) {
                $err_obj->set_error("document_id", "DOCUMENT_INVALID");
              } else {
                $clip_key = 0;
                foreach($clip_files as $clip) {
                  if($document_id == $clip["clip_key"]) {
                    $clip_key = $clip["meeting_clip_key"];
                    break;
                  }
                }
                if($clip_key == 0) {
                  $err_obj->set_error("document_id", "CLIP_INVALID");
                }
              }
            } else {
              if (!array_key_exists($document_id, $reservation_data["documents"])) {
                $err_obj->set_error("document_id", "DOCUMENT_INVALID");
              }
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
          if(substr($document_id, 0, 3) === 'MC_') {
            require_once ("classes/dbi/meeting_clip.dbi.php");
            $obj_meetingClip = new MeetingClipTable($this->get_dsn());
            $where = sprintf( "meeting_clip_key='%s' AND is_deleted=0", $clip_key );
            $data = array(
                    "is_deleted" => "1",
                    "updatetime" => date("Y-m-d H:i:s"),
                );
            $obj_meetingClip->update($data, $where);
          } else {
            //require_once ("classes/N2MY_Document.class.php");
            //$obj_N2MYDocument = new N2MY_Document($this->get_dsn());
            $document_info =  $reservation_data["documents"][$document_id];//$obj_N2MYDocument->getDetail($document_id);
            $this->logger2->debug($document_info);
            $this->objReservation->deleteDocument($document_info);
          }
            return $this->output();
        }
    }

    /**
     * 一覧
     */
    function action_get_document() {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $user_info           = $this->session->get("user_info");
        $rules = array(
            API_RESERVATION_ID => array(
                "required" => true
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $document_list = array();
            if ($reservation_data["documents"]) {
                foreach ($reservation_data["documents"] as $_document) {
                    switch ($_document["status"]) {
                        case DOCUMENT_STATUS_WAIT :    $_document["status"] = "uploading"; break;
                        case DOCUMENT_STATUS_DO :      $_document["status"] = "pending"; break;
                        case DOCUMENT_STATUS_SUCCESS : $_document["status"] = "done"; break;
                        case DOCUMENT_STATUS_DELETE :  $_document["status"] = "delete"; break;
                        case DOCUMENT_STATUS_ERROR :
                        default :                      $_document["status"] = "error"; break;
                    }
                    unset($_document["create_datetime"]);
                    unset($_document["update_datetime"]);
                    $document_list[] = $_document;
                }
            }
            $data["documents"]["document"] = $document_list;

            $clip_files = $this->getMeetingClips($reservation_data['info']['meeting_key']);
            if($clip_files) {
              foreach($clip_files as $clip) {
                $clip_data["meeting_clip_name"] = $clip["meeting_clip_name"];
                $clip_data["clip_key"] = $clip["clip_key"];
                $data["clips"]["clip"][] = $clip_data;
              }
            }
            return $this->output($data);
        }
    }

    function action_preview_document() {
        require_once("classes/N2MY_Document.class.php");
        require_once("classes/dbi/meeting.dbi.php");
        $objDocument = new N2MY_Document($this->get_dsn());
        $objMeeting  = new MeetingTable($this->get_dsn());

        $request     = $this->request->getAll();
        $document_id = $this->request->get("document_id");
        $page        = $this->request->get("page", 1);
        $user_info   = $this->session->get("user_info");

        // 入力チェック
        $rules = array(
            "document_id" => array(
                "required" => true
                ),
            "page" => array(
                "required" => true,
                "integer" => true,
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        $documentInfo = $objDocument->getDetail( $document_id );
        if (!$documentInfo) {
            $err_obj->set_error("document_id", "NOT_FOUND");
        } else {
            $where = sprintf( "meeting_key='%s' AND is_deleted=0", $documentInfo["meeting_key"] );
            $document_user_key = $objMeeting->getOne($where, "user_key");
            if ($document_user_key != $user_info["user_key"]) {
                $err_obj->set_error("document_id", "DOCUMENT_ID_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            // エラー
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            // ファイル情報取得
            $fileInfo = $objDocument->getPreviewFileInfo( $documentInfo, $page );
            //resize
            $max_width = 480;
            $max_height = 320;
            $percent = 0.5;

            list($width, $height) = getimagesize($fileInfo["targetImage"]);
            $x_ratio = $max_width / $width;
            $y_ratio = $max_height / $height;

            if( ($width <= $max_width) && ($height <= $max_height) ){
                $tn_width = $width;
                $tn_height = $height;
            } elseif (($x_ratio * $height) < $max_height) {
                $tn_height = ceil($x_ratio * $height);
                $tn_width = $max_width;
            } else {
                $tn_width = ceil($y_ratio * $width);
                $tn_height = $max_height;
            }
            header('Content-type: image/jpeg');
            // 再サンプル
            $image_p = imagecreatetruecolor($tn_width, $tn_height);
            $image = imagecreatefromjpeg($fileInfo["targetImage"]);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
            imagejpeg($image_p, null, 100);
        }
    }

    function _format_guests($_guests,$is_sales = null) {
        $guests = array();
        if ($_guests) {
          if($is_sales){
            foreach ($_guests as $key => $guest) {
              $this->logger2->info($guest);
              if($guest["member_key"]){
                $staff = array(
                    "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key["country_key"].'&lang='.$guest["lang"],
                    API_GUEST_ID => $guest["r_user_session"],
                );
                $guests["staff"] = $staff;
              }elseif($guest["is_manager"]) {
                $manager = array(
                  "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key["country_key"].'&lang='.$guest["lang"],
                  API_GUEST_ID => $guest["r_user_session"],
                );
                $guests["manager"] = $manager;
              }else{
                $customer = array(
                  "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key["country_key"].'&lang='.$guest["lang"],
                  API_GUEST_ID => $guest["r_user_session"],
                );
                $guests["customer"] = $customer;
              }
            }
          }else {
            $country_key = $this->session->get("country_key");
            foreach ($_guests as $key => $guest) {
              if($guest["r_organizer_flg"] == 0 && $guest["r_user_authority"] == 0){
                $guests[] = array(
                    "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
                    API_GUEST_ID => $guest["r_user_session"],
                );
              }
            }
          }
        }
        return $guests;
    }

  function _format_authority($reservation_key){
    $where = sprintf("reservation_key = '%s' AND r_user_authority = 1 AND r_user_status = 1", $reservation_key);
    $authority_info = $this->objReservationUser->getRowsAssoc($where);
    $authority = array();
    if($authority_info){
      $country_key = $this->session->get("country_key");
      $authority = array(
        "presenter_url"   => N2MY_BASE_URL."r/".$authority_info[0]["r_user_session"]."&c=".$country_key."&lang=".$authority_info[0]["r_user_lang"],
      );
    }
    return $authority;
  }

}
$main = new N2MY_Meeting_Resevation_API();
$main->execute();
