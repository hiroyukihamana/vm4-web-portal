<?php
/*
 * Created on 2008/01/31
*
* To change the template for this generated file go to
* Window - Preferences - PHPeclipse - PHP - Code Templates
*/
require_once("classes/N2MY_Api.class.php");

class N2MY_Meeting_API extends N2MY_Api
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        $this->_name_space = md5(__FILE__);
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        if ("action_invite_start" != $this->getActionName() && "action_meeting_display" != $this->getActionName()) {
            $this->checkAuthorization();
        }
    }

    function action_invite_start() {
        require_once 'classes/mgm/MGM_Auth.class.php';
        require_once 'classes/N2MY_Auth.class.php';
        require_once 'classes/N2MY_Account.class.php';
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        require_once 'classes/N2MY_Meeting.class.php';

        // DB選択
        $request = $this->request->getAll();
        $meeting_id  = $request[API_MEETING_ID];
        $output_type = $this->request->get("output_type");
        // パラメタチェック
        $rules = array(
                        API_MEETING_ID => array(
                                        "required" => true,
                                        "valid_integer" => true,
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $dsn_info = $obj_MGMClass->getRelationDsn($meeting_id, "meeting");
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "meeting_session_id = '".addslashes($meeting_id)."'" .
                        " AND is_active = 1" .
                        " AND is_deleted = 0";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        if ($meeting_info["is_reserved"] &&
        ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
                        (strtotime($meeting_info['meeting_stop_datetime'])) < time())
        )
        ) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "予約時間内ではありません",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // ユーザー
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$meeting_info["user_key"];
        $user_info = $objUser->getRow($where);
        if (!$user_info) {
            return $this->display_error("101", "User not found", $this->get_error_info($err_obj));
        }
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $user_info["user_id"];
        $pw          = $user_info["user_password"];
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $enc         = $this->request->get("enc", null);
        $rules = array(
                        "lang"     => array(
                                        "allow" => array_keys($this->get_language_list()),
                        ),
                        "country"     => array(
                                        "allow" => array_keys($this->get_country_list()),
                        ),
                        API_TIME_ZONE     => array(
                                        "allow" => array_keys($this->get_timezone_list()),
                        ),
        );
        $request = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $err_obj = $this->error_check($request, $rules);
        if (!$user_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
                $err_obj->set_error("", "SELECT_USER_ERROR", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $login = new N2MY_Auth($server_info["dsn"]);
            $user_type = "";
            // ユーザ確認
            $login_info = $login->check($id, $pw, $enc);
            if (!$login_info) {
                return $this->display_error("1", "USER_ID_PASS_INVALID");
            } else {
                // セッションスタート
                $session = EZSession::getInstance();
                // ID を取得
                $session_id = session_id();
                // ユーザ情報配置
                if  ($output_type) {
                    $session->set("output_type", $output_type);
                }
                $session->set("login", "1");
                $session->set("lang", $lang);
                $session->set("country_key", $country);
                $session->set("time_zone", $time_zone);
                $session->set("dsn_key", $server_info["host_name"]);
                $session->set("server_info", $server_info);
                // ユーザー情報
                $user_info = $login_info["user_info"];
                $session->set("user_info", $login_info["user_info"]);
                $type = "user";
                // 使用可能な部屋
                $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
                $rooms = $obj_N2MY_Account->getOwnRoomByRoomKey( $meeting_info["room_key"], $user_info["user_key"]);
                // 既存のセッションデータにも書き込む
                $session->set('room_info', $rooms);
                // ユーザごとのオプションを取得
                $type        = $this->request->get("type", "normal");
                $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
                // ホワイトボードアップロードメールアドレス
                $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $meeting_info["room_key"]."@".$this->config->get("N2MY", "mail_wb_host") : "";
                $user_options = array(
                                "user_key"              => $user_info["user_key"],
                                "name"                  => $this->request->get("name"),
                                "narrow"                => $this->request->get("is_narrow"),
                                "lang"                  => $this->request->get("lang"),
                                "skin_type"             => $this->request->get("skin_type", ""),
                                "mode"                  => $mode,
                                "role"                  => $this->request->get("role"),
                                "mail_upload_address"   => $mail_upload_address,
                                "account_model"         => $user_info["account_model"],
                );
                // 会議入室
                $obj_N2MY_Meeting = new N2MY_Meeting($server_info["dsn"]);
                $meetingDetail = $obj_N2MY_Meeting->startMeeting($meeting_id, $type, $user_options);
                //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
                $session->set("room_key", $meeting_info["room_key"] );
                $session->set("type", $meetingDetail["participant_type_name"] );
                $session->set("meeting_key", $meetingDetail["meeting_key"] );
                $session->set("participant_key", $meetingDetail["participant_key"] );
                $session->set("participant_name", $this->request->get("name"));
                $session->set("mail_upload_address", $mail_upload_address );
                $session->set("fl_ver", $this->request->get("flash_version", "as2"));
                $session->set("reload_type", 'normal');
                $session->set( "meeting_version", $user_info["meeting_version"] );
                $this->session->set( "user_agent_ticket", uniqid());
                // 会議入室
                $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESSION."=".session_id();
                if ($meeting_info["meeting_password"]) {
                    $this->session->set("redirect_url", $url, $this->_name_space);
                    $url = N2MY_BASE_URL."services/" .
                                    "?action_login=" .
                                    "&".N2MY_SESSION."=".session_id().
                                    "&reservation_session=".$meeting_info["reservation_session"].
                                    "&ns=".$this->_name_space;
                }
                $data = array(
                                "url"           => $url,
                                API_MEETING_ID  => $meeting_id,
                                "meeting_name"  => $meetingDetail["meeting_name"],
                );
                return $this->output($data);
            }
        }
    }


    /**
     * セールス用 入室API
     * @return array
     */
    function action_inbound_start(){
        require_once 'classes/mgm/MGM_Auth.class.php';
        require_once 'classes/N2MY_Auth.class.php';
        require_once 'classes/N2MY_Account.class.php';
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        require_once 'classes/N2MY_Meeting.class.php';
        // DB選択
        $request = $this->request->getAll();
        $meeting_id  = $request[API_MEETING_ID];
        $output_type = $this->request->get("output_type");
        // パラメタチェック
        $rules = array(
                        API_MEETING_ID => array(
                                        "required" => true,
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $dsn_info = $obj_MGMClass->getRelationDsn($meeting_id, "meeting");
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "meeting_session_id = '".addslashes($meeting_id)."'" .
                        " AND is_active = 1" .
                        " AND is_deleted = 0";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        } else {
            require_once('classes/core/dbi/Participant.dbi.php');
            $obj_Participant    = new DBI_Participant( $server_info["dsn"] );
            $pcount      = $obj_Participant->getCount($meeting_info["meeting_key"]);
            if ($pcount != 1) {
                $err_msg["errors"][] = array(
                                "field"   => API_MEETING_ID,
                                "err_cd"  => "invalid",
                                "err_msg" => "MEETING_INVALID",
                );
                return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
            }
        }
        if ($meeting_info["is_reserved"] &&
        ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
                        (strtotime($meeting_info['meeting_stop_datetime'])) < time())
        )
        ) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "予約時間内ではありません",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // ユーザー
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$meeting_info["user_key"];
        $user_info = $objUser->getRow($where);
        if (!$user_info) {
            return $this->display_error("101", "User not found", $this->get_error_info($err_obj));
        }
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $user_info["user_id"];
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $account_model = $user_info["meeting_version"];
        $rules = array(
                        "lang"     => array(
                                        "allow" => array_keys($this->get_language_list()),
                        ),
                        "country"     => array(
                                        "allow" => array_keys($this->get_country_list()),
                        ),
                        API_TIME_ZONE     => array(
                                        "allow" => array_keys($this->get_timezone_list()),
                        ),
        );
        $request = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $err_obj = $this->error_check($request, $rules);
        if (!$mgm_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $mgm_info["server_key"] )){
                $err_obj->set_error("", "SELECT_USER_ERROR", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        // セッションスタート
        $this->session->set("lang", $lang);
        $this->session->set("country_key", $country);
        $this->session->set("time_zone", $time_zone);
        $this->session->set("dsn_key", $server_info["host_name"]);
        $this->session->set("server_info", $server_info);

        // ユーザー情報
        $this->session->set("user_info", $user_info);
        // 使用可能な部屋
        $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
        $rooms = $obj_N2MY_Account->getOwnRoomByRoomKey( $meeting_info["room_key"], $user_info["user_key"]);
        // 既存のセッションデータにも書き込む
        $this->session->set('room_info', $rooms);
        // ユーザごとのオプションを取得
        $type = $this->request->get("type", "customer");
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $meeting_info["room_key"]."@".$this->config->get("N2MY", "mail_wb_host") : "";
        $user_options = array(
                        "user_key"              => $user_info["user_key"],
                        "name"                  => $this->request->get("name"),
                        "narrow"                => $this->request->get("is_narrow"),
                        "lang"                  => $this->request->get("lang"),
                        "skin_type"             => $this->request->get("skin_type", ""),
                        "mode"                  => $mode,
                        "role"                  => $this->request->get("role"),
                        "mail_upload_address"   => $mail_upload_address,
                        "account_model"         => $user_info["account_model"],
        );
        // 会議入室
        $obj_N2MY_Meeting = new N2MY_Meeting($server_info["dsn"]);
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($meeting_id, $type, $user_options);

        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set("room_key", $meeting_info["room_key"] );
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $where = "room_key = '".$meeting_info["room_key"]."'";
        $relation_info = $objRoomRelation->getRow($where);
        require_once("classes/dbi/member.dbi.php");
        $objMember = new MemberTable($this->get_dsn());
        $where = "member_key = ".$relation_info["member_key"];
        $member_info = $objMember->getRow($where);
        $this->session->set( "member_info", $member_info );

        $this->session->set("type", $meetingDetail["participant_type_name"] );
        $this->session->set("meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set("participant_key", $meetingDetail["participant_key"] );
        $this->session->set("participant_name", $this->request->get("name"));
        $this->session->set("mail_upload_address", $mail_upload_address );
        $this->session->set("fl_ver", $this->request->get("flash_version", "as3"));
        $this->session->set("reload_type", 'customer');
        $this->session->set( "meeting_version", $account_model );
        $this->session->set( "meeting_type", "customer" );
        $screen_mode = $this->request->get("screen_mode", "wide");
        $this->session->set( "user_agent_ticket", uniqid());
        $display_size = '16to9';
        $this->session->set( "display_size", $display_size);
        // 会議入室
        $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESSION."=".session_id();
        if ($meeting_info["meeting_password"]) {
            $this->session->set("redirect_url", $url, $this->_name_space);
            $url = N2MY_BASE_URL."services/" .
                            "?action_login=" .
                            "&".N2MY_SESSION."=".session_id().
                            "&reservation_session=".$meeting_info["reservation_session"].
                            "&ns=".$this->_name_space;
        }
        $data = array(
                        "url"           => $url,
                        API_MEETING_ID  => $meeting_id,
                        "meeting_name"  => $meetingDetail["meeting_name"],
                        //                "meeting_version" => $session->get( "meeting_version" ),
        );
        return $this->output($data);


    }

    function action_inbound_enter(){
        require_once 'classes/mgm/MGM_Auth.class.php';
        require_once 'classes/N2MY_Auth.class.php';
        require_once 'classes/N2MY_Account.class.php';
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        require_once 'classes/N2MY_Meeting.class.php';
        // DB選択
        $request = $this->request->getAll();
        $meeting_id  = $request[API_MEETING_ID];
        $output_type = $this->request->get("output_type");
        // パラメタチェック
        $rules = array(
                        API_MEETING_ID => array(
                                        "required" => true,
                        ),
                        "mobile"  => array(
                                        "allow"    => array('1', '0'),
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $dsn_info = $obj_MGMClass->getRelationDsn($meeting_id, "meeting");
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "meeting_session_id = '".addslashes($meeting_id)."'" .
                        " AND is_active = 1" .
                        " AND is_deleted = 0";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        } else {
            require_once('classes/core/dbi/Participant.dbi.php');
            $obj_Participant    = new DBI_Participant( $server_info["dsn"] );
            $pcount      = $obj_Participant->getCount($meeting_info["meeting_key"]);
            if ($pcount != 1) {
                $err_msg["errors"][] = array(
                                "field"   => API_MEETING_ID,
                                "err_cd"  => "invalid",
                                "err_msg" => "MEETING_INVALID",
                );
                return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
            }
        }

        // ユーザー
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$meeting_info["user_key"];
        $user_info = $objUser->getRow($where);
        if (!$user_info) {
            return $this->display_error("101", "User not found", $this->get_error_info($err_obj));
        }

        // モバイル判別
        $mobile_flg = $this->request->get("mobile");
        if ($mobile_flg || strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod") || strpos($_SERVER['HTTP_USER_AGENT'],"iPad")) {
            require_once("classes/dbi/inbound.dbi.php");
            $obj_Inbound = new InboundTable( $server_info["dsn"] );
            $where = "user_key = '".addslashes($user_info["user_key"])."'";
            $inbound_info = $obj_Inbound->getRow($where);
            $base_url = parse_url(N2MY_BASE_URL);
            $referer = $_SERVER["HTTP_REFERER"];
            $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://info?inbound_id=".$inbound_info["inbound_id"]."&entrypoint=".$base_url["host"]."&callbackurl=".urlencode($referer)."&name=".$this->request->get("name");
            $this->logger2->info($url);
            $data = array(
                            "url"               => $url,
                            API_MEETING_ID      => $this->ticket_to_session($meeting_info["meeting_ticket"]),
                            "meeting_name"      => $meeting_info["meeting_name"],
                            "password_flg"      => 0,
                            "need_login_flg"    => 0,
            );
            return $this->output($data);
            exit;
        }

        if ($meeting_info["is_reserved"] &&
        ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
                        (strtotime($meeting_info['meeting_stop_datetime'])) < time())
        )
        ) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "予約時間内ではありません",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $user_info["user_id"];
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $account_model = $user_info["meeting_version"];
        $rules = array(
                        "lang"     => array(
                                        "allow" => array_keys($this->get_language_list()),
                        ),
                        "country"     => array(
                                        "allow" => array_keys($this->get_country_list()),
                        ),
                        API_TIME_ZONE     => array(
                                        "allow" => array_keys($this->get_timezone_list()),
                        ),
        );
        $request = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $err_obj = $this->error_check($request, $rules);
        if (!$mgm_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $mgm_info["server_key"] )){
                $err_obj->set_error("", "SELECT_USER_ERROR", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        // セッションスタート
        $this->session->set("lang", $lang);
        $this->session->set("country_key", $country);
        $this->session->set("time_zone", $time_zone);
        $this->session->set("dsn_key", $server_info["host_name"]);
        $this->session->set("server_info", $server_info);

        // ユーザー情報
        $this->session->set("user_info", $user_info);
        // 使用可能な部屋
        $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
        $rooms = $obj_N2MY_Account->getOwnRoomByRoomKey( $meeting_info["room_key"], $user_info["user_key"]);
        // 既存のセッションデータにも書き込む
        $this->session->set('room_info', $rooms);
        // ユーザごとのオプションを取得
        $type = $this->request->get("type", "customer");
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $meeting_info["room_key"]."@".$this->config->get("N2MY", "mail_wb_host") : "";

        $user_options = array(
                        "user_key"              => $user_info["user_key"],
                        "name"                  => $this->request->get("name"),
                        "narrow"                => $this->request->get("is_narrow"),
                        "lang"                  => $this->request->get("lang"),
                        "skin_type"             => $this->request->get("skin_type", ""),
                        "mode"                  => $mode,
                        "role"                  => $this->request->get("role"),
                        "mail_upload_address"   => $mail_upload_address,
                        "account_model"         => $user_info["account_model"],
        );
        // 会議入室
        $obj_N2MY_Meeting = new N2MY_Meeting($server_info["dsn"]);
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($meeting_id, $type, $user_options);

        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set("room_key", $meeting_info["room_key"] );
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $where = "room_key = '".$meeting_info["room_key"]."'";
        $relation_info = $objRoomRelation->getRow($where);
        require_once("classes/dbi/member.dbi.php");
        $objMember = new MemberTable($this->get_dsn());
        $where = "member_key = ".$relation_info["member_key"];
        $member_info = $objMember->getRow($where);

        $this->session->set( "member_info", $member_info );

        $this->session->set("type", $meetingDetail["participant_type_name"] );
        $this->session->set("meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set("participant_key", $meetingDetail["participant_key"] );
        $this->session->set("participant_name", $this->request->get("name"));
        $this->session->set("mail_upload_address", $mail_upload_address );
        $this->session->set("fl_ver", $this->request->get("flash_version", "as3"));
        $this->session->set("reload_type", 'customer');
        $this->session->set( "meeting_version", $account_model );
        $this->session->set( "meeting_type", "customer" );
        $screen_mode = $this->request->get("screen_mode", "wide");
        $this->session->set( "user_agent_ticket", uniqid());
        $display_size = '16to9';
        $this->session->set( "display_size", $display_size);
        // 会議入室
        $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESSION."=".session_id();
        if ($meeting_info["meeting_password"]) {
            $this->session->set("redirect_url", $url, $this->_name_space);
            $url = N2MY_BASE_URL."services/" .
                            "?action_login=" .
                            "&".N2MY_SESSION."=".session_id().
                            "&reservation_session=".$meeting_info["reservation_session"].
                            "&ns=".$this->_name_space;
        }
        $data = array(
                        "url"           => $url,
                        API_MEETING_ID  => $meeting_id,
                        "meeting_name"  => $meetingDetail["meeting_name"],
                        //                "meeting_version" => $session->get( "meeting_version" ),
        );
        return $this->output($data);

    }

    /**
     * 会議キー取得
     *
     * @param boolean $header_flg ヘッダを出力
     * @return string $output  会議キー、会議名をxmlで出力
     */
    function action_create()
    {
        if(!$this->request->get(API_ROOM_ID)){
            return $this->action_create_on_time_meeting();
        }
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        require_once( "classes/N2MY_Meeting.class.php" );
        $obj_N2MYMeeting = new N2MY_Meeting( $this->get_dsn() );
        $room_key = $this->request->get(API_ROOM_ID);
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        $this->session->remove("meeting_auth_flg");
        // チェックルール編集
        $rules = array(
                        API_ROOM_ID     => array(
                                        "required" => true,
                                        "allow"    => array_keys($room_info),
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
                        API_ROOM_ID    => $room_key,
                        "output_type" => $this->request->get("output_type", null),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $now_meeting = $obj_N2MYMeeting->getNowMeeting( $room_key );
        $meeting_key = $now_meeting["meeting_key"];
        $meeting_name = $now_meeting["meeting_name"];
        $meeting_password = $now_meeting["meeting_password"];
        // センタサーバ指定
        $country_id = "jp";
        if (isset($extension["locale"])) {
            switch ($extension["locale"]) {
                case "jp" :
                case "us" :
                case "cn" :
                    $country_id = $extension["locale"];
                    break;
            }
        }
        // オプション指定
        $option = array(
                        "user_key"     => $user_info["user_key"],
                        "meeting_name" => $meeting_name,
                        "start_time"   => $now_meeting["start_time"],
                        "end_time"     => $now_meeting["end_time"],
                        "country_id"   => $country_id,
        );
        $password_flg = "0";
        if ($now_meeting["meeting_password"] ) {
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".mysql_real_escape_string($user_info["user_key"])." AND reservation_session = '".mysql_real_escape_string($now_meeting["reservation_session"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            $this->logger->debug("reservation_info",__FILE__,__LINE__,$reservation_info);
            if ($reservation_info["reservation_pw_type"] == "1") {
                $password_flg = "1";
            }
        }
        $core_meeting_key = $obj_N2MYMeeting->createMeeting($room_key, $meeting_key, $option);
        if ($core_meeting_key) {
            $obj_Meeting = new DBI_Meeting($this->get_dsn());
            $where = "meeting_session_id = '".addslashes($core_meeting_key)."'".
                            " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
        } else {
            $err_obj->set_error(0, "MEETING_INVALID");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info(array($core_meeting_key,$meeting_info));

        $data = array(
                        API_MEETING_ID  => $this->ticket_to_session($now_meeting["meeting_key"]),
                        "meeting_name" => $meeting_name,
                        "password_flg" => $password_flg,
                        "pin_cd"       => $meeting_info["pin_cd"],
        );
        // TODO : 作成できたか出来ないかのチェックは不要？
        return $this->output($data);
    }

    /*
     * one time meeting作成
    * */
    function action_create_on_time_meeting(){

        $user_info = $this->session->get("user_info");
        // one_time_meetingのみ有効
        if($user_info["is_one_time_meeting"] != 1){
            // チェックルール編集
            $rules = array(
                            API_ROOM_ID     => array(
                                            "required" => true,
                            ),
                            "output_type"  => array(
                                            "allow"    => $this->get_output_type_list(),
                            ),
            );
            // チェックデータ（リクエスト）編集
            $request = array(
                            API_ROOM_ID    => $room_key,
                            "output_type" => $this->request->get("output_type", null),
            );
            // チェック
            $err_obj = $this->error_check($request, $rules);
            $err_obj->set_error(0, "USER_MEETING_TYPE_ERROR");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once( "classes/N2MY_Meeting.class.php" );
        $obj_N2MYMeeting = new N2MY_Meeting( $this->get_dsn() );
        $now_meeting = $obj_N2MYMeeting->oneTimeMeeting( $user_info["user_id"] );
        $meeting_ticket = $now_meeting["meeting_ticket"];
        $meeting_name = $now_meeting["meeting_name"];
        $meeting_password = $now_meeting["meeting_password"];
        // オプション指定
        $option = array(
                        "user_key"     => $user_info["user_key"],
                        "meeting_name" => $meeting_name,
                        "start_time"   => $now_meeting["start_time"],
                        "end_time"     => $now_meeting["end_time"],
                        "country_id"   => $country_id,
        );

        $core_meeting_key = $obj_N2MYMeeting->createMeeting(null, $meeting_ticket, $option);
        if ($core_meeting_key) {
            $obj_Meeting = new DBI_Meeting($this->get_dsn());
            $where = "meeting_session_id = '".addslashes($core_meeting_key)."'".
                            " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
        } else {
            $err_obj->set_error(0, "MEETING_INVALID");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info(array($core_meeting_key,$meeting_info));

        $data = array(
                        API_MEETING_ID  => $this->ticket_to_session($now_meeting["meeting_ticket"]),
                        "meeting_name" => $meeting_name,
                        "password_flg" => $password_flg,
                        "pin_cd"       => $meeting_info["pin_cd"],
        );
        // TODO : 作成できたか出来ないかのチェックは不要？
        return $this->output($data);
    }

    //会議認証
    function action_auth() {
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        require_once( "classes/N2MY_Meeting.class.php" );
        $obj_N2MYMeeting = new N2MY_Meeting( $this->get_dsn() );
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $meeting_session_id = $this->request->get(API_MEETING_ID);
        $password = $this->request->get("password");
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        $enc = $this->request->get("enc", "md5");
        // チェックルール編集
        $rules = array(
                        API_MEETING_ID     => array(
                                        "required" => true,
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
                        API_MEETING_ID    => $meeting_session_id,
                        "output_type" => $this->request->get("output_type", null),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "meeting_session_id = '".addslashes($meeting_session_id)."'" .
                        " AND is_active = 1" .
                        " AND is_deleted = 0";
        $meeting_info = $obj_Meeting->getRow($where);
        if (!$meetingInfo = $obj_Meeting->getRow($where)) {
            $err_obj->set_error(API_MEETING_ID, "MEETING_KEY_INVALID");
        } else {
            // ログインユーザ以外の予約
            if ($meetingInfo["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_MEETING_ID, "MEETING_KEY_INVALID");
            }
        }

        //予約情報からパスワード判定
        if ($meeting_info["is_reserved"]) {
            require_once ("classes/dbi/reservation.dbi.php");
            $reservation_obj = new ReservationTable($this->get_dsn());
            $reservation_info = $reservation_obj->getRow("meeting_key = '".addslashes($meetingInfo["meeting_ticket"])."'");
            if ($enc == "sha1") {
                $reservation_pass = sha1($reservation_info["reservation_pw"]);
            } else {
                $reservation_pass = md5($reservation_info["reservation_pw"]);
            }
            if (($reservation_info["reservation_pw"] === "") || ($reservation_pass === $password)) {
                $this->session->set("meeting_auth_flg", "1");
                return $this->output();
            } else {
                $err_obj->set_error("password", "PASSWORD_INVALID");
            }
        } else {
            $err_obj->set_error(API_MEETING_ID, "MEETING_KEY_INVALID");
        }
        return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
    }

    //会議認証


    //V-CUBE ID利用者ログイン用とメンバー課金での招待認証
    function action_auth_login () {
        require_once ('classes/N2MY_Auth.class.php');
        require_once ('classes/mgm/MGM_Auth.class.php');

        $meeting_session_id = $this->request->get(API_MEETING_ID);

        $id      = $this->request->get("id");
        $pw      = $this->request->get("pw");
        $pw_encode      = $this->request->get("enc");
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $obj_Auth = new N2MY_Auth( $this->get_dsn() );
        $rules = array(
                        API_MEETING_ID     => array(
                                        "required" => true,
                        ),
                        "id"     => array(
                                        "required" => true
                        ),
                        "pw"     => array(
                                        "required" => true
                        ),
                        "enc" => array(
                                        "allow" => array("md5", "sha1"),
                        ),
                        "output_type" => array(
                                        "allow" => $this->get_output_type_list(),
                        ),
        );
        $request = array(
                        API_MEETING_ID    => $meeting_session_id,
                        "id"    => $id,
                        "pw"    => $pw,
                        "output_type" => $this->request->get("output_type", null),
        );
        $err_obj = $this->error_check($request, $rules);
        $where = "meeting_session_id = '".addslashes($meeting_session_id)."'" .
                        " AND is_active = 1" .
                        " AND is_deleted = 0";
        require_once("classes/N2MY_Meeting.class.php");
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        if (!$meetingInfo = $obj_Meeting->getRow($where)) {
            $err_obj->set_error(API_MEETING_ID, "MEETING_KEY_INVALID");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        if ($meetingInfo["is_reserved"]) {
            $password_flg = "0";
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".mysql_real_escape_string($meetingInfo["user_key"])." AND meeting_key = '".mysql_real_escape_string($meetingInfo["meeting_ticket"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            $this->logger->info("reservation_info",__FILE__,__LINE__,$reservation_info);
            if ($reservation_info["reservation_pw_type"] == "1") {
                $password_flg = "1";
            }
        }

        if ($login_info = $obj_Auth->check($id, $pw, $pw_encode)) {
            $this->logger2->debug($login_info);
            $user_info = $this->session->get("user_info");
            if($user_info["account_model"] == "free"){
                // vCubeID でのログイン
                if ($login_info["member_info"]["member_type"] != 'free' ) {
                    $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
                    return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
                }
                $this->session->set("member_info", $login_info["member_info"]);
            } else if ($user_info["account_model"] == "member"){
                // member課金メンバーのログイン
                if(!$obj_Auth->checkMember($id, $pw, $user_info["user_key"], $pw_encode, "", $user_info["external_member_invitation_flg"])){
                    $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
                    return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
                }
                $this->session->set("member_info", $login_info["member_info"]);
                // BIZ用の認証をいれる
                if($user_info["service_name"] == "biz"){
                    // bizを使っているユーザーはservice_nameがbiz同士じゃないと入室出来ない
                    if($user_info["service_name"] != $login_info["user_info"]["service_name"]){
                        $err_obj->set_error("id", "NOT_BIZ_ERROR", $id);
                        return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
                    }
                }
            } else {
                $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
                return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
            }
        } else {
            $this->logger2->info($request);
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $data = array(
                        "password_flg"      => $password_flg,
                        API_MEETING_ID      => $meeting_session_id,
        );
        $this->session->set("meeting_auth_flg", "1");
        return $this->output($data);
    }

    /**
     * 会議開始（会議を作成して、そのまま開始）
     *
     * @param
     * @return 会議データを作成し、リダイレクトで会議室に入室
     */
    function action_start() {

        if(!$this->request->get(API_ROOM_ID)){
            //return $this->action_start_on_time_meeting();
        }
        $this->session->remove("meeting_auth_flg");
        require_once("classes/N2MY_Meeting.class.php");
        require_once("classes/dbi/room.dbi.php");
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        if($request['type'] == 'authority'){
            $is_authority = 1;
        }
        $type        = $this->request->get("type", "normal");
        $room_key = $this->request->get(API_ROOM_ID);
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $objRoom = new RoomTable($this->get_dsn());
        if ($meeting_ticket = $this->session->get('invited_meeting_ticket')) {
            $where = "room_key = '".addslashes($room_key)."'" .
                            " AND meeting_ticket = '".addslashes($meeting_ticket)."'" .
                            " AND is_active = 1" .
                            " AND is_deleted = 0";
            $meeting_key = $obj_Meeting->getOne($where, 'meeting_session_id');
            if(!$meeting_key){
                $this->logger2->warn("MEETING_INVALID meeting_ticket:".$meeting_ticket);
                $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                return $this->display_error("101", "対象の会議が存在しないか、既に終了しています", $this->get_error_info($err_obj));
            }
        } else {
            $meeting_key = $this->request->get(API_MEETING_ID);
        }
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // ユーザ情報
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );
        if($user_info["is_one_time_meeting"] == 1){
            if($request["type"] != "customer" && $request["type"] != "staff"  && $request["type"] != "manager" ){ // 20140926暫定対応
                require_once("classes/N2MY_Account.class.php");
                $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
                $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"] , $room_service_mode , true);
                $this->session->set("room_info" ,$rooms);
            }
        }

        // manager権限をもつユーザのみ部屋再取得を実施。
        if($this->session->get("service_mode") == "sales" && ($member_info['use_ss_watcher'] || $this->session->get("role") == "10")){
            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());

            // 入室TypeにStaffを宣言したあとmanagerを指定すると正常な部屋リストではない、対策として再度Sales部屋一覧を取得
            if($request["type"] == "manager" ){                
                $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"] , "sales" , true);
                $this->session->set("room_info" ,$rooms);            
            }

            // 全部屋を取得している状態のため、別部屋にもStaffで入れる、対応としてstaff指定時は紐付いている部屋だけを取り直す
            if($request["type"] == "staff"){
                $rooms = $obj_N2MY_Account->getOwnRoom( $member_info["member_key"], $user_info["user_key"], "sales" );
                $this->session->set("room_info" ,$rooms);            
            }
        }

        // チェックルール編集
        if ($user_info["use_stb"]) {
            $room_rule = array("required" => true);
        } else {
            $room_rule = array(
                            "required" => true,
                            "allow" => array_keys($this->session->get("room_info")),
            );
        }
        $rules = array(
                        API_ROOM_ID => $room_rule,
                        "flash_version" => array(
                                        "allow" => array("as2", "as3"),
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
                        "screen_mode"  => array(
                                        "allow"    => array('normal', 'wide'),
                        ),
                        "url_encrypt"  => array(
                                        "allow"    => array(1, 0),
                        ),
                        "onetime_url"  => array(
                                        "allow"    => array(1, 0),
                        ),
                        "lang"         => array(
                                        "allow"    => array_keys($this->get_language_list()),
                        ),
                        "country"      => array(
                                        "allow"    => array_keys($this->get_country_list()),
                        ),
                        API_TIME_ZONE  => array(
                                        "allow"    => array_keys($this->get_timezone_list()),
                        ),
                        "tag"          => array(
                                        "maxlen"   => 255,
                        ) // ,
                        // 緊急対応
                        //            "type"         => array(
                                        //                "allow"    => array("normal", "audience", "whiteboard", "authority", "staff", "customer")
                                        //                )
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        //audience許せない部屋
        $room_info = $objRoom->getRow("room_key='".mysql_real_escape_string($room_key)."'");
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");
        //STBの契約の部屋の場合は他のSTBタイプの部屋に入室可能な処理をいれる
        if (!$room_info) {
            $err_obj->set_error(0, "ROOM_ID_INVALID");
            return $this->display_error(0, "ROOM_ID_INVALID");
        }
        if ($room_info["use_sales_option"] && ($login_type == "normal" || !$login_type )) {
            $login_type = "staff";
            $this->session->set("service_mode", "sales");
        } else if ($room_info["use_sales_option"] && ($login_type != "staff" && $login_type == "invitedGuest" )) {
            $login_type = "customer";
            $this->session->set("service_mode", "sales");
        } else {
            $this->session->set("service_mode", "meeting");
        }
        // 緊急対応
        //         require_once ("classes/N2MY_Account.class.php");
        //         $obj_N2MY_Account = new N2MY_Account($this -> get_dsn());
        //         $room_options = $obj_N2MY_Account->getRoomOptionList($room_key);
        //         if($this->session->get('service_mode') == 'sales'){
        //             if($type == 'normal' || $type == 'audience' || $type == 'whitboard' || $type == 'authority'){
        //                 return $this->display_error("100", "PARAMETER_ERROR", "TYPE IS NOT ALLOWED");
        //             }
        //         }else{
        //             if($type == 'staff' || $type == 'customer'){
        //                 return $this->display_error("100", "PARAMETER_ERROR", "TYPE IS NOT ALLOWED");
        //             }
        //         }
        //         if(($room_info['max_audience_seat'] == 0  || $room_options['whiteboard']) && $type == "audience"){
        //             return $this->display_error("100", "PARAMETER_ERROR", "TYPE IS NOT ALLOWED");
        //         }
        //         if($room_info['max_whiteboard_seat'] == 0 && $type == 'whiteboard'){
        //             return $this->display_error("100", "PARAMETER_ERROR", "TYPE IS NOT ALLOWED");
        //         }
        // 緊急対応
        //        if($room_options['whiteboard'] && $type != 'whiteboard'){
        //            return $this->display_error("100", "PARAMETER_ERROR", "TYPE IS NOT ALLOWED");
        //        }
        // 会議の指定が有る場合
        $meeting_info = array();
        if ($meeting_key) {
            $where = "room_key = '".addslashes($room_key)."'" .
                            " AND meeting_session_id = '".addslashes($meeting_key)."'" .
                            " AND is_active = 1" .
                            " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            $this->logger2->info($meeting_info);
            // 予約で時間外
            if (!$meeting_info) {
                $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                return $this->display_error("101", "対象の会議が存在しないか、既に終了しています", $this->get_error_info($err_obj));
            } else {
                if ($meeting_info["is_reserved"] &&
                ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
                                (strtotime($meeting_info['meeting_stop_datetime'])) < time())
                )
                ) {
                    $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                    return $this->display_error("101", "時間外の予約です", $this->get_error_info($err_obj));
                } else {
                    // 直前の会議取得
                    $last_meeting_key = $objRoom->getOne("room_key='".addslashes($room_key)."'", 'meeting_key');
                    // 異なる会議
                    if ($meeting_info['meeting_ticket'] != $last_meeting_key) {
                        // 開催中
                        $ret = $obj_N2MY_Meeting->getMeetingStatus($room_key, $last_meeting_key);
                        if ($ret['status'] == '1') {
                            return $this->display_error("101", "前回会議が開催中です", $this->get_error_info($err_obj));
                        }
                    }
                }
            }
        }

        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // 会議情報取得
        if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_info['meeting_ticket'])) {
            $err_obj->set_error(0, "MEETING_INVALID");
            //            return $this->display_error(0, "MEETING_INVALID");
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        //ポート課金の場合は人数チェック
        if ($user_info["max_connect_participant"] > 0 && $user_info["use_port_plan"]) {
            require_once('classes/core/dbi/Participant.dbi.php');
            $obj_Participant     = new DBI_Participant( $this->get_dsn() );
            // 現在の参加者数（電話を抜いた数）
            $nowParticipantNumber = $obj_Participant->getCountExceptPgi( $user_info["user_key"] );

            // ポート制御時の入室制御
            if($user_info["entry_mode"] == "1"){
            $now = date("Y-m-d H:i:s");
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND reservation_status    = '1'" .
                    " AND reservation_starttime <= '" . $now . "'" .
                    " AND reservation_endtime   > '" . $now . "'";

            require_once ("classes/dbi/reservation.dbi.php");
            $reservation_obj = new ReservationTable($this->get_dsn());
            $reservation_info_port = $reservation_obj->getRow($where);

            require_once( "classes/dbi/meeting.dbi.php" );
            $obj_Meeting = new MeetingTable($this->get_dsn());
            $where = "meeting_ticket = '".$reservation_info_port["meeting_key"]."'".
                    " AND is_active = 1";
                    
            $rowNowMeetingInfo = $obj_Meeting->getRow($where);
            $nowParticipantNumberTargetRoom = $obj_Participant->getCountExceptPgi($user_info["user_key"]);

            // ダミーデータ作成
            $invite_url = N2MY_BASE_URL."services/invite.php?action_error_url";
            $data_dummy = array(
                "url"               => $invite_url,
                 API_MEETING_ID      => "DUMMY_MEETING_ID",
                "meeting_name"      => "",
                "password_flg"      => "",
                "need_login_flg"    => "0",
            );

            if(!$reservation_info_port){
                ////////////////////////////////
                // モバイル側は本処理を通る際エラーとなるが、アプリの制御が必要になるので一旦この状態でコミット
                ////////////////////////////////
                //$err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                //return $this->display_error("102", "Meeting Not Active", $this->get_error_info($err_obj));
                return $this->output($data_dummy);
            }
                
            // 会議室の参加可能人数チェック
            if ($nowParticipantNumberTargetRoom >= $reservation_info_port["max_port"]) {

                // managerの場合、契約ポート数に空きがあれば参加可能
                require_once("classes/N2MY_Reservation.class.php");
                $obj_N2MY_Reservation = new N2MY_Reservation($this->get_dsn());
                $start_time = strtotime("now");
                $reservation_now_maxport = $obj_N2MY_Reservation->checkMaxUsePort($start_time, $start_time);
               
                // 現在の時間で予約されている会議の最大参加者数をチェック、1人も入る枠なければ入室不可
                if($request["type"] == "manager" && $reservation_info_port["max_port"] == "2" && $user_info["max_connect_participant"] != $reservation_now_maxport){
                        $this->logger2->info("ss_watcher Enter a room -> meeting_key : ". $now_meeting_key );
                    }else{
                        //$err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                        //return $this->display_error("102", "Max Port Connect Participant", $this->get_error_info($err_obj));
                        return $this->output($data_dummy);
                    }
                }
            }

            // 契約ポート数チェック
            if ($nowParticipantNumber >= $user_info["max_connect_participant"]) {
                $where = "meeting_ticket = '".$meeting_info["meeting_key"]."'".
                         " AND is_active = 1";
                $rowNowMeeting = $obj_Meeting->getRow($where);
                require_once('classes/dbi/max_connect_log.dbi.php');
                $obj_MaxConnectLog     = new MaxConnectLog( $this->get_dsn() );
                $data = array(
                        "user_key"      => $user_info["user_key"],
                        "meeting_key"   => $rowNowMeeting["meeting_key"],
                        "room_key"      => $room_key,
                        "type"          => "port_plan",
                        "connect_count" => $nowParticipantNumber,
                );
                $obj_MaxConnectLog->add($data);
                $err_obj->set_error(API_MEETING_ID, "MEETING_MAX_CONNECTION");
                return $this->display_error("102", "Max Connect Participant", $this->get_error_info($err_obj));
             }
        }
      
        $meeting_key = $meeting_info["meeting_key"];
        // $country_key = $this->session->get("country_key");
        // $country_list = $this->get_country_list();
        // if (!array_key_exists($country_key, $country_list)) {
        // $this->logger2->info($country_list);
        // $_country_data = array_shift($country_list);
        // $country_key = $_country_data["country_key"];
        // }


        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_key = $this->request->get('country');
        if (!$country_key) {
            $country_key = $this->session->get('country_key');
        }
        if ($country_key == "auto") {
            //validate ip
            if(isset($location)){
                require_once ('lib/pear/Net/IPv4.php');
                $ipv4 = new Net_IPv4();
                if($ipv4 -> check_ip($location)){
                    $country_key = $this->detectCountryByIPAddress($location);
                }
            } else {
                $result_country_key = $this->_get_country_key($country_key);
                $country_key = $result_country_key;
            }
        }
        $this->session->set('country_key', $country_key);
        $this->session->set('selected_country_key', $country_key);
        // オプション指定
        $options = array(
                        "meeting_name" => $meeting_info["meeting_name"],
                        "user_key"     => $user_info["user_key"],
                        "start_time"   => $meeting_info["start_time"],
                        "end_time"     => $meeting_info["end_time"],
                        "country_id"   => $country_key,
                        "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);

        //会議内からの招待メールのため会議作成毎にmgm_relationにmeeting_session_idを追加
        require_once("classes/mgm/dbi/relation.dbi.php");
        $objMgmRelationTable = new MgmRelationTable(
                        $this->get_auth_dsn());
        $resultRelation = $objMgmRelationTable->getRow(sprintf("relation_key='%s'", $core_session));
        if (!$resultRelation) {
            $data = array(
                            "relation_key"  => $core_session,
                            "user_key"      => $user_info["user_id"],
                            "relation_type" => "meeting",
                            "status"        => 1,
            );
            $add_data = $objMgmRelationTable->add($data);
        }

        $_room_info = $this->session->get("room_info");
        $room_options = $_room_info[$room_key]["options"];
        // ユーザごとのオプションを取得
        if($type == 'authority'){
            $type = 'normal';
        }
        if ($room_info["use_sales_option"] && ($type == "normal" || $type = "staff") && $login_type != "customer") {
            if ($room_options["whiteboard"]) {
                $type = "whiteboard_staff";
            } else {
                $type = "staff";
            }
        } else if ($room_info["use_sales_option"] && ($login_type != "staff" && $login_type == "customer" )) {
            if ($room_options["whiteboard"]) {
                $type = "whiteboard_customer";
            } else {
                $type = "customer";
            }
            $this->session->set("service_mode", "sales");
        }
        if ($is_audience = $this->session->get('invited_meeting_audience')) {
            switch($type) {
                case 'normal':
                    $type = 'audience';
                    break;
                case 'multicamera':
                    $type = 'multicamera_audience';
                    break;
                case 'whiteboard':
                    $type = 'whiteboard_audience';
                    break;
                case 'invisible_wb':
                    $type = 'invisible_wb_audience';
                    break;
            }
        }
        //        $mode = "";
        //        if ($meeting_info['is_trial']) {
        //ユーザーがトライアルである場合
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        //participant登録持はinvitedGuestのタイプはないため、inviteに変更する
        if ($login_type == "invitedGuest") {
            $login_type = "invite";
        }
        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";
        $name = $this->request->get("name", $member_info["member_name"]);
        if ($this->session->get("contact_name") && $type == "customer") {
            $name = $this->session->get("contact_name");
        }
        $user_options = array(
                        "user_key"            => $user_info["user_key"],
                        "id"                  => $member_info["member_key"],
                        "name"                => $name,
                        "participant_email"   => $member_info["member_email"],
                        "participant_station" => $this->request->get("station", $member_info["member_station"]),
                        "participant_country" => $country_key,
                        "member_key"          => $member_info["member_key"],
                        "narrow"              => $this->request->get("is_narrow"),
                        "lang"                => $this->request->get("lang", $this->session->get("lang")),
                        "skin_type"           => $this->request->get("skin_type", ""),
                        "mode"                => $mode,
                        "role"                => $login_type,
                        "mail_upload_address" => $mail_upload_address,
                        "account_model"       => $user_info["account_model"],
        );
        $tag = $this->request->get("tag");
        if($tag){
            $user_options["participant_tag"] = $tag;
        }

        // 監視者での入室
        if($request["type"] == "manager"){
            // 管理者権限をもち、STAFF＋カスタマー参加状態の場合入室可能
            if( $member_info['use_ss_watcher'] || $this->session->get("role") == "10" ) {
                require_once('classes/core/dbi/Participant.dbi.php');
                require_once( "classes/dbi/meeting.dbi.php" );
                $obj_Meeting = new MeetingTable($this->get_dsn());
                $obj_Participant     = new DBI_Participant( $this->get_dsn() );
               
                $where = "meeting_ticket = '".$meeting_info["meeting_key"]."'".
                         " AND is_active = 1";
                $checkNowMeeting = $obj_Meeting->getRow($where);
                $pcount      = $obj_Participant->getCount($checkNowMeeting["meeting_key"]);
                if($pcount != "2"){
                    // 条件未達成のため入室させない
                    $err_obj->set_error(API_MEETING_ID, "NOT_ROOM_ENTER");
                    return $this->display_error("102", "staff and customer not enter a room", $this->get_error_info($err_obj));
                }
            }else{
                $err_obj->set_error(API_MEETING_ID, "NOT_MANAGER_ACCOUNT");
                return $this->display_error("102", "Not Manager", $this->get_error_info($err_obj));
            }
            if($user_info['use_port_plan'] && $user_info['entry_mode'] && $reservation_info_port["max_port"] == 2){
                // ポート制御モード時限定：2人予約部屋で監視者参加の場合、ポート数は3となる
                require_once("classes/N2MY_Reservation.class.php");
                $this->obj_N2MY_Reservation = new N2MY_Reservation($this->get_dsn());
                $this->obj_N2MY_Reservation->addManagerPort( $meeting_key );
            }
            if($request["type"] == "manager"){
                $request["type"] = "ss_watcher";
            }
            $type = $request["type"];
        }
        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);

        // 先に入室しているpolycom clientが存在する場合 participant record にmeeting_keyを追加更新
        /*
         if ($meetingDetail["ives_conference_id"] && $meetingDetail["ives_did"]) {
        require_once("classes/polycom/Polycom.class.php");
        $polycom = new PolycomClass($this->get_dsn());
        $polycom->addMeetingKey2PolycomParticipant($meetingDetail["meeting_key"], $meetingDetail["ives_conference_id"], $meetingDetail["ives_did"]);
        }
        */

        //マルチカメラと資料共有対応
        if ($type == "normal" && $room_options["multicamera"]) {
            $type = "multicamera";
        } else if ($type == "normal" && $room_options["whiteboard"]) {
            $type = "whiteboard";
        } else if ($type == "audience" && $room_options["multicamera"]) {
            $type = "multicamera_audience";
        } else if ($type == "audience" && $room_options["whiteboard"]) {
            $type = "whiteboard_audience";
        }
        // 言語が指定されていた場合に比較し、変更があれば、セッションを書き換える
        if($this->request->get('lang') && $this->session->get('lang') != $this->request->get('lang')){
            $this->session->set('lang', $this->request->get('lang'));
        }
        $this->session->set( "room_key", $room_key );
        $this->session->set( "type", $meetingDetail["participant_type_name"] );
        $this->session->set( "meeting_type", $type );
        $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set( "participant_key", $meetingDetail["participant_key"] );
        $this->session->set( "participant_name", $this->request->get("name", $member_info["member_name"]) );
        $this->session->set( "mail_upload_address", $mail_upload_address );
        $this->session->set( "fl_ver", $this->request->get("flash_version", "as2") );
        $this->session->set( "reload_type", 'normal');
        $this->session->set( "meeting_version", $user_info["meeting_version"] );
        // 画面サイズ
        $screen_mode = $this->request->get("screen_mode", "normal");
        $fl_ver = $this->request->get("fl_ver", "as3");
        if ($room_info["use_sales_option"]) {
            $screen_mode = "wide";
            $fl_ver = "as3";
            $this->session->set( "fl_ver", "as3");
        }
        //アクティブスピーカー対応
        if($room_info["active_speaker_mode_use_flg"]){
            $screen_mode = "wide";
            $fl_ver = "as3";
            $this->session->set( "fl_ver", "as3");
        }
        if($is_authority || $type == "whiteboard" ){
            $fl_ver = "as3";
            $this->session->set( "fl_ver", "as3");
        }
        switch ($screen_mode) {
            case 'normal' :
                $display_size = '4to3';
                break;
            case 'wide' :
                $display_size = '16to9';
                break;
            default :
                $display_size = '4to3';
                break;
        }
        $this->session->set( "display_size", $display_size);
        // 会議入室
        $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESS_NAME."=".session_id();

        //$this->logger2->info($this->session->getAll());
        //onetime_url
        if($this->request->get("onetime_url")) {
            //add new onetime_url data
            require_once ("classes/dbi/onetime_url.dbi.php");
            $obj_OnetimeUrl = new OnetimeUrlTable($this->get_dsn());
            $ontime_session_id = substr(md5(uniqid(rand(), 1)), 0, 24);
            $country = $this->session->get("country_key");
            $add_onetime_url_data = array(
                            "onetime_session_id" => $ontime_session_id,
                            "user_key" => $user_info["user_key"],
                            "meeting_key" => $meetingDetail["meeting_key"],
                            "meeting_type" => $type,
                            "participant_key" => $meetingDetail["participant_key"],
                            "participant_name" => $this->request->get("name", $member_info["member_name"]),
                            "participant_type_name" => $meetingDetail["participant_type_name"],
                            "participant_lang" => $this->request->get("lang", $this->session->get("lang")),
                            "participant_place" => $country,
                            "participant_timezone" => $this->request->get(API_TIME_ZONE, $this->session->get("time_zone")),
                            "fl_ver" => $fl_ver,
                            "display_size" => $display_size,
                            "tag" => $this->request->get("tag", ""),
                            "is_used_flg" => 0,
            );
            $obj_OnetimeUrl->add($add_onetime_url_data);
            //make new url
            $url = N2MY_BASE_URL."o/".$ontime_session_id;
        }
        if (($this->request->get("contact_name") && $this->request->get("contact_email")) || $request["url_encrypt"]) {
            require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
            $key = N2MY_ENCRYPT_KEY;
            $iv = N2MY_ENCRYPT_IV;
            $encrypted_data = EZEncrypt::encrypt($key, $iv, session_id());
            $encrypted_data = str_replace("/", "-", $encrypted_data);
            $encrypted_data = str_replace("+", "_", $encrypted_data);
            $url = N2MY_BASE_URL."direct/meeting/".$encrypted_data;
        }
        if ($meeting_info["meeting_password"]) {
            $password_flg = "0";
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".mysql_real_escape_string($user_info["user_key"])." AND reservation_session = '".mysql_real_escape_string($meeting_info["reservation_session"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            $this->logger->debug("reservation_info",__FILE__,__LINE__,$reservation_info);
            if ($reservation_info["reservation_pw_type"] == "1") {
                $password_flg = "1";
                $this->session->set("redirect_url", $url, $this->_name_space);
                $url = N2MY_BASE_URL."services/" .
                                "?action_login=" .
                                "&".N2MY_SESS_NAME."=".session_id().
                                "&reservation_session=".$meeting_info["reservation_session"].
                                "&ns=".$this->_name_space;
            }
        }
        //Freeの場合はログインが必要なようにflgで渡す(ログインフラグがある時はパスワードフラグは認証時に渡す)
        $need_login_flg = 0;
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $member_info = $this->session->get( "member_info" );
        $relation_list = "";
        if ($member_info) {
            $where = "member_key = ".$member_info["member_key"].
            " AND room_key = '".$room_key."'";
            $relation_list = $objRoomRelation->getRowsAssoc($where);
        }
        //自分の部屋の場合はログイン不要
        if ( ($user_info["account_model"] == "member"  || $user_info["account_model"] == "free") && !$relation_list && !$user_info["external_user_invitation_flg"]) {
            $need_login_flg = 1;
            $password_flg   = "0";
        }
        //モバイルの場合はユーザーエージェント登録
        if (strpos($_SERVER['HTTP_USER_AGENT'], "iOS;") || strpos($_SERVER['HTTP_USER_AGENT'], "iPad;") || strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod")) {
            $this->add_user_agent();
        } else {
            $this->session->set( "user_agent_ticket", uniqid());
        }
        //SFDC対応。入室処理と同時に招待メールを送付。
        if ($this->request->get("contact_name") && $this->request->get("contact_email")) {
            require_once("classes/dbi/meeting.dbi.php");
            $obj_Meetng = new MeetingTable($this->get_dsn());
            // 会議のステータス取得
            $where = "meeting_ticket = '".addslashes($meeting_key)."'";
            $meeting_data = $obj_Meetng->getRow($where);
            require_once "classes/dbi/meeting_invitation.dbi.php";
            $objMeetingInvitationTable = new MeetingInvitationTable($this->get_dsn());
            if (!$invitationData = $objMeetingInvitationTable->getRow(
                            sprintf("meeting_key='%s' AND status = 1", $meeting_data["meeting_key"]),
                            "*",
                            array("invitation_key"=>"desc"))) {
                $user_session_id = md5(uniqid(rand(), 1));
                $audience_session_id = md5(uniqid(rand(), 1));
                $invitationData = array(
                                "meeting_key" => $meeting_data["meeting_key"],
                                "meeting_session_id" => $meeting_data["meeting_session_id"],
                                "meeting_ticket" => $meeting_data["meeting_ticket"],
                                "user_session_id" => $user_session_id,
                                "audience_session_id" => $audience_session_id,
                                "contact_name" => $this->request->get("contact_name"),
                                "status" => 1
                );
                $this->logger2->debug($invitationData);
                $objMeetingInvitationTable->add($invitationData);
            }
            require_once("classes/N2MY_Invitation.class.php");
            $obj_Invitation = new N2MY_Invitation($this->get_dsn());
            $lang = $this->session->get("lang") ? $this->session->get("lang") : "en";
            $obj_Invitation->sendInvitationMail($meetingDetail["meeting_key"], $this->request->get("contact_email"), $lang, $userType="normal", $reservationPassword=null, $this->request->get("contact_name"));
        }
        if($is_authority){
            $url .= "&authority=1";
        }
        $data = array(
                        "url"               => $url,
                        API_MEETING_ID      => $this->ticket_to_session($meetingDetail["meeting_ticket"]),
                        "meeting_name"      => $meetingDetail["meeting_name"],
                        "password_flg"      => $password_flg,
                        "need_login_flg"    => $need_login_flg,
                        //            "meeting_sequence_key"  => $meetingDetail["meeting_sequence_key"],
        //            "participant_key"       => $meetingDetail["participant_key"],
        //            "participant_id"        => $meetingDetail["participant_session_id"],
        );
        return $this->output($data);
    }

    //on_time_meeting用会議開始
    function action_start_on_time_meeting(){

        $this->session->remove("meeting_auth_flg");
        require_once("classes/N2MY_Meeting.class.php");
        require_once("classes/dbi/room.dbi.php");
        // ユーザ情報
        $user_info = $this->session->get("user_info");

        if($user_info["is_one_time_meeting"] != 1){
            // エラー処理
            return $this->display_error("100", "PARAMETER_ERROR", "meeting_type_error");
        }
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        $room_key = $user_info["user_id"];
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $objRoom = new RoomTable($this->get_dsn());
        if ($meeting_ticket = $this->session->get('invited_meeting_ticket')) {
            $where = "room_key = '".addslashes($room_key)."'" .
                            " AND meeting_ticket = '".addslashes($meeting_ticket)."'" .
                            " AND is_active = 1" .
                            " AND is_deleted = 0";
            $meeting_key = $obj_Meeting->getOne($where, 'meeting_session_id');
        } else {
            $meeting_key = $this->request->get(API_MEETING_ID);
        }
        $this->logger2->info($meeting_key);
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // チェックルール編集
        $rules = array(
                        "meeting_id"    => array(
                                        "required" => true,
                        ),
                        "flash_version" => array(
                                        "allow"    => array("as2", "as3"),
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
                        "screen_mode"  => array(
                                        "allow"    => array('normal', 'wide'),
                        ),
                        "url_encrypt"  => array(
                                        "allow"    => array(1, 0),
                        ),
                        "onetime_url"  => array(
                                        "allow"    => array(1, 0),
                        ),
                        "lang"         => array(
                        "allow"    => array_keys($this->get_language_list()),
                        ),
                        "country"     => array(
                        "allow"    => array_keys($this->get_country_list()),
                        ),
                        API_TIME_ZONE  => array(
                        "allow"    => array_keys($this->get_timezone_list()),
                        ),
                        "tag"          => array(
                        "minlen"   => 3,
                        "maxlen"   => 255,
                        "regex"    => "/^[0-9a-zA-Z\._-]+$/",
                        ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());

        $_room_info = $obj_N2MY_Account->getUserRoomInfo($user_info["user_key"]);
        $room_info = $_room_info["room_info"];
        $this->logger2->info($room_info);
        if ($room_info["use_sales_option"] && ($login_type == "normal" || !$login_type )) {
            $login_type = "staff";
            $this->session->set("service_mode", "sales");
        } else if ($room_info["use_sales_option"] && ($login_type != "staff" && $login_type == "invitedGuest" )) {
            $login_type = "customer";
            $this->session->set("service_mode", "sales");
        } else {
            $this->session->set("service_mode", "meeting");
        }
        $member_info = $this->session->get( "member_info" );
        //member課金を利用していてメンバー以外が会議を利用しようとした場合
        if ( $user_info["account_model"] == "member" && !$member_info ) {
            //            $err_obj->set_error(0, "MEMBER_INVALID");
            //            return $this->display_error(0, "MEMBER_INVALID");
        }
        // 会議の指定が有る場合
        $meeting_info = array();
        if ($meeting_key) {
            $where = "room_key = '".addslashes($room_key)."'" .
                            " AND meeting_session_id = '".addslashes($meeting_key)."'" .
                            " AND is_active = 1" .
                            " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            $this->logger2->info($meeting_info);
            // 予約で時間外
            if (!$meeting_info) {
                $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                return $this->display_error("101", "対象の会議が存在しないか、既に終了しています", $this->get_error_info($err_obj));
            } else {
                if ($meeting_info["is_reserved"] &&
                ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
                                (strtotime($meeting_info['meeting_stop_datetime'])) < time())
                )
                ) {
                    $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                    return $this->display_error("101", "時間外の予約です", $this->get_error_info($err_obj));
                } else {
                    // 直前の会議取得
                    $last_meeting_key = $objRoom->getOne("room_key='".addslashes($room_key)."'", 'meeting_key');
                }
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info($meeting_info['meeting_ticket']);

        //ポート課金の場合は人数チェック
        if ($user_info["max_connect_participant"] > 0 && $user_info["use_port_plan"]) {
            require_once('classes/core/dbi/Participant.dbi.php');
            $obj_Participant     = new DBI_Participant( $this->get_dsn() );
            $nowParticipantNumber = $obj_Participant->numRows('user_key = '.$user_info["user_key"].' AND is_active = 1');
                if ($nowParticipantNumber >= $user_info["max_connect_participant"]) {
                require_once( "classes/dbi/meeting.dbi.php" );
                $obj_Meeting = new MeetingTable($this->get_dsn());
                $where = "meeting_ticket = '".$meeting_info["meeting_key"]."'".
                                " AND is_active = 1";
                $rowNowMeeting = $obj_Meeting->getRow($where);
                require_once('classes/dbi/max_connect_log.dbi.php');
                $obj_MaxConnectLog     = new MaxConnectLog( $this->get_dsn() );
                $data = array(
                                "user_key"      => $user_info["user_key"],
                                "meeting_key"   => $rowNowMeeting["meeting_key"],
                                "room_key"      => $room_key,
                                "type"          => "port_plan",
                                "connect_count" => $nowParticipantNumber,
                );
                $obj_MaxConnectLog->add($data);
                if (!$this->check_one_account()) {
                    $err_obj->set_error(API_MEETING_ID, "MEETING_MAX_CONNECTION");
                    return $this->display_error("102", "Max Connect Participant", $this->get_error_info($err_obj));
                }
            }
        }
        $meeting_key = $meeting_info["meeting_ticket"];
        $country_key = $this->session->get("country_key");
        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();
        if (!array_key_exists($country_key, $country_list)) {
            $this->logger2->info($country_list);
            $_country_data = array_shift($country_list);
            $country_key = $_country_data["country_key"];
        }
        // オプション指定
        $options = array(
                        "meeting_name" => $meeting_info["meeting_name"],
                        "user_key"     => $user_info["user_key"],
                        "start_time"   => $meeting_info["start_time"],
                        "end_time"     => $meeting_info["end_time"],
                        "country_id"   => $country_key,
                        "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting(null, $meeting_key, $options);
        //会議内からの招待メールのため会議作成毎にmgm_relationにmeeting_session_idを追加
        require_once("classes/mgm/dbi/relation.dbi.php");
        $objMgmRelationTable = new MgmRelationTable($this->get_auth_dsn());
        $resultRelation = $objMgmRelationTable->getRow(sprintf("relation_key='%s'", $core_session));
        if (!$resultRelation) {
            $data = array(
                            "relation_key"  => $core_session,
                            "user_key"      => $user_info["user_id"],
                            "relation_type" => "meeting",
                            "status"        => 1,
            );
            $add_data = $objMgmRelationTable->add($data);
        }

        $room_options = $_room_info["options"];
        // ユーザごとのオプションを取得
        $type        = $this->request->get("type", "normal");
        if ($room_info["use_sales_option"] && ($type == "normal" || $type = "staff") && $login_type != "customer") {
            if ($room_options["whiteboard"]) {
                $type = "whiteboard_staff";
            } else {
                $type = "staff";
            }
        } else if ($room_info["use_sales_option"] && ($login_type != "staff" && $login_type == "customer" )) {
            if ($room_options["whiteboard"]) {
                $type = "whiteboard_customer";
            } else {
                $type = "customer";
            }
            $this->session->set("service_mode", "sales");
        }
        if ($is_audience = $this->session->get('invited_meeting_audience')) {
            switch($type) {
                case 'normal':
                    $type = 'audience';
                    break;
                case 'multicamera':
                    $type = 'multicamera_audience';
                    break;
                case 'whiteboard':
                    $type = 'whiteboard_audience';
                    break;
                case 'invisible_wb':
                    $type = 'invisible_wb_audience';
                    break;
            }
        }
        //        $mode = "";
        //        if ($meeting_info['is_trial']) {
        //ユーザーがトライアルである場合
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        //participant登録持はinvitedGuestのタイプはないため、inviteに変更する
        if ($login_type == "invitedGuest") {
            $login_type = "invite";
        }
        // ホワイトボードアップロードメールアドレス ワンタイムの時はメール張り込みはなし
        $mail_upload_address = "";
        $name = $this->request->get("name", $member_info["member_name"]);
        if ($this->session->get("contact_name") && $type == "customer") {
            $name = $this->session->get("contact_name");
        }
        $user_options = array(
                        "user_key"            => $user_info["user_key"],
                        "id"                  => $member_info["member_key"],
                        "name"                => $name,
                        "participant_email"   => $member_info["member_email"],
                        "participant_station" => $this->request->get("station", $member_info["member_station"]),
                        "participant_country" => $country_key,
                        "member_key"          => $member_info["member_key"],
                        "narrow"              => $this->request->get("is_narrow"),
                        "lang"                => $this->request->get("lang", $this->session->get("lang")),
                        "skin_type"           => $this->request->get("skin_type", ""),
                        "mode"                => $mode,
                        "role"                => $login_type,
                        "mail_upload_address" => $mail_upload_address,
                        "account_model"       => $user_info["account_model"],
        );
        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);

        // 先に入室しているpolycom clientが存在する場合 participant record にmeeting_keyを追加更新
        /*
         if ($meetingDetail["ives_conference_id"] && $meetingDetail["ives_did"]) {
        require_once("classes/polycom/Polycom.class.php");
        $polycom = new PolycomClass($this->get_dsn());
        $polycom->addMeetingKey2PolycomParticipant($meetingDetail["meeting_key"], $meetingDetail["ives_conference_id"], $meetingDetail["ives_did"]);
        }
        */

        //マルチカメラと資料共有対応
        if ($type == "normal" && $room_options["multicamera"]) {
            $type = "multicamera";
        } else if ($type == "normal" && $room_options["whiteboard"]) {
            $type = "whiteboard";
        } else if ($type == "audience" && $room_options["multicamera"]) {
            $type = "multicamera_audience";
        } else if ($type == "audience" && $room_options["whiteboard"]) {
            $type = "whiteboard_audience";
        }
        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set( "room_key", $room_key );
        $this->session->set( "type", $meetingDetail["participant_type_name"] );
        $this->session->set( "meeting_type", $type );
        $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set( "participant_key", $meetingDetail["participant_key"] );
        $this->session->set( "participant_name", $this->request->get("name", $member_info["member_name"]) );
        $this->session->set( "mail_upload_address", $mail_upload_address );
        $this->session->set( "fl_ver", $this->request->get("flash_version", "as2") );
        $this->session->set( "reload_type", 'normal');
        $this->session->set( "meeting_version", $user_info["meeting_version"] );
        // 画面サイズ
        $screen_mode = $this->request->get("screen_mode", "normal");
        if ($room_info["use_sales_option"]) {
            $screen_mode = "wide";
            $this->session->set( "fl_ver", "as3");
        }
        switch ($screen_mode) {
            case 'normal' :
                $display_size = '4to3';
                break;
            case 'wide' :
                $display_size = '16to9';
                break;
            default :
                $display_size = '4to3';
                break;
        }
        $this->session->set( "display_size", $display_size);
        // 会議入室
        $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESS_NAME."=".session_id();

        //$this->logger2->info($this->session->getAll());
        //onetime_url
        if($this->request->get("onetime_url")) {
            //add new onetime_url data
            require_once ("classes/dbi/onetime_url.dbi.php");
            $obj_OnetimeUrl = new OnetimeUrlTable($this->get_dsn());
            $ontime_session_id = substr(md5(uniqid(rand(), 1)), 0, 24);
            $add_onetime_url_data = array(
                            "onetime_session_id" => $ontime_session_id,
                            "user_key" => $user_info["user_key"],
                            "meeting_key" => $meetingDetail["meeting_key"],
                            "meeting_type" => $type,
                            "participant_key" => $meetingDetail["participant_key"],
                            "participant_name" => $this->request->get("name", $member_info["member_name"]),
                            "participant_type_name" => $meetingDetail["participant_type_name"],
                            "participant_lang" => $this->request->get("lang", $this->session->get("lang")),
                            "participant_place" => $this->request->get("country", $this->session->get("country_key")),
                            "participant_timezone" => $this->request->get(API_TIME_ZONE, $this->session->get("time_zone")),
                            "fl_ver" => $this->request->get("flash_version", "as2"),
                            "display_size" => $display_size,
                            "tag" => $this->request->get("tag", ""),
                            "is_used_flg" => 0,
            );
            $obj_OnetimeUrl->add($add_onetime_url_data);
            //make new url
            $url = N2MY_BASE_URL."o/".$ontime_session_id;
        }

        if (($this->request->get("contact_name") && $this->request->get("contact_email")) || $request["url_encrypt"]) {
            require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
            $key = N2MY_ENCRYPT_KEY;
            $iv = N2MY_ENCRYPT_IV;
            $encrypted_data = EZEncrypt::encrypt($key, $iv, session_id());
            $encrypted_data = str_replace("/", "-", $encrypted_data);
            $encrypted_data = str_replace("+", "_", $encrypted_data);
            $url = N2MY_BASE_URL."direct/meeting/".$encrypted_data;
        }
        if ($meeting_info["meeting_password"]) {

            $this->session->set("redirect_url", $url, $this->_name_space);
            $url = N2MY_BASE_URL."services/" .
                            "?action_login=" .
                            "&".N2MY_SESS_NAME."=".session_id().
                            "&reservation_session=".$meeting_info["reservation_session"].
                            "&ns=".$this->_name_space;

            $password_flg = "0";
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".mysql_real_escape_string($user_info["user_key"])." AND reservation_session = '".mysql_real_escape_string($meeting_info["reservation_session"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            $this->logger->debug("reservation_info",__FILE__,__LINE__,$reservation_info);
            if ($reservation_info["reservation_pw_type"] == "1") {
                $password_flg = "1";
            }
        }
        //Freeの場合はログインが必要なようにflgで渡す(ログインフラグがある時はパスワードフラグは認証時に渡す)
        $need_login_flg = 0;
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $member_info = $this->session->get( "member_info" );
        $relation_list = "";
        if ($member_info) {
            $where = "member_key = ".$member_info["member_key"].
            " AND room_key = '".$room_key."'";
            $relation_list = $objRoomRelation->getRowsAssoc($where);
        }
        //自分の部屋の場合はログイン不要
        if ( ($user_info["account_model"] == "member"  || $user_info["account_model"] == "free") && !$relation_list && !$user_info["external_user_invitation_flg"]) {
            $need_login_flg = 1;
            $password_flg   = "0";
        }
        //モバイルの場合はユーザーエージェント登録
        if (strpos($_SERVER['HTTP_USER_AGENT'], "iOS;") || strpos($_SERVER['HTTP_USER_AGENT'], "iPad;") || strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod")) {
            $this->add_user_agent();
        } else {
            $this->session->set( "user_agent_ticket", uniqid());
        }
        //SFDC対応。入室処理と同時に招待メールを送付。
        if ($this->request->get("contact_name") && $this->request->get("contact_email")) {
            require_once("classes/dbi/meeting.dbi.php");
            $obj_Meetng = new MeetingTable($this->get_dsn());
            // 会議のステータス取得
            $where = "meeting_ticket = '".addslashes($meeting_key)."'";
            $meeting_data = $obj_Meetng->getRow($where);
            require_once "classes/dbi/meeting_invitation.dbi.php";
            $objMeetingInvitationTable = new MeetingInvitationTable($this->get_dsn());
            if (!$invitationData = $objMeetingInvitationTable->getRow(
                            sprintf("meeting_key='%s' AND status = 1", $meeting_data["meeting_key"]),
                            "*",
                            array("invitation_key"=>"desc"))) {
                $user_session_id = md5(uniqid(rand(), 1));
                $audience_session_id = md5(uniqid(rand(), 1));
                $invitationData = array(
                                "meeting_key" => $meeting_data["meeting_key"],
                                "meeting_session_id" => $meeting_data["meeting_session_id"],
                                "meeting_ticket" => $meeting_data["meeting_ticket"],
                                "user_session_id" => $user_session_id,
                                "audience_session_id" => $audience_session_id,
                                "contact_name" => $this->request->get("contact_name"),
                                "status" => 1
                );
                $this->logger2->debug($invitationData);
                $objMeetingInvitationTable->add($invitationData);
            }
            require_once("classes/N2MY_Invitation.class.php");
            $obj_Invitation = new N2MY_Invitation($this->get_dsn());
            $lang = $this->session->get("lang") ? $this->session->get("lang") : "en";
            $obj_Invitation->sendInvitationMail($meetingDetail["meeting_key"], $this->request->get("contact_email"), $lang, $userType="normal", $reservationPassword=null, $this->request->get("contact_name"));
        }
        $data = array(
                        "url"               => $url,
                        API_MEETING_ID      => $this->ticket_to_session($meetingDetail["meeting_ticket"]),
                        "meeting_name"      => $meetingDetail["meeting_name"],
                        "password_flg"      => $password_flg,
                        "need_login_flg"    => $need_login_flg,
                        //            "meeting_sequence_key"  => $meetingDetail["meeting_sequence_key"],
        //            "participant_key"       => $meetingDetail["participant_key"],
        //            "participant_id"        => $meetingDetail["participant_session_id"],
        );
        return $this->output($data);
    }

    /**
     * 会議開始。Mobaile用
     *
     * @param
     * @return 会議データを作成し、Mobile用のURLを返す
     */
    function action_start_mobile() {
        $this->session->remove("meeting_auth_flg");
        require_once("classes/N2MY_Meeting.class.php");
        require_once("classes/dbi/room.dbi.php");
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $objRoom = new RoomTable($this->get_dsn());
        if ($meeting_ticket = $this->session->get('invited_meeting_ticket')) {
            $where = "room_key = '".addslashes($room_key)."'" .
                            " AND meeting_ticket = '".addslashes($meeting_ticket)."'" .
                            " AND is_active = 1" .
                            " AND is_deleted = 0";
            $meeting_key = $obj_Meeting->getOne($where, 'meeting_session_id');
        } else {
            $meeting_key = $this->request->get(API_MEETING_ID);
        }
        $this->logger2->info($meeting_key);
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());

        // チェックルール編集
        $rules = array(
                        API_ROOM_ID => array(
                                        "required" => true,
                                        "allow" => array_keys($this->session->get("room_info")),
                        ),
                        "flash_version" => array(
                                        "allow" => array("as2", "as3"),
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
                        "screen_mode"  => array(
                                        "allow"    => array('normal', 'wide'),
                        ),
                        "mobile"  => array(
                                        "allow"    => array('1', '0'),
                        ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");
        // ユーザ情報
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );
        //member課金を利用していてメンバー以外が会議を利用しようとした場合
        if ( $user_info["account_model"] == "member" && !$member_info ) {
            //            $err_obj->set_error(0, "MEMBER_INVALID");
            //            return $this->display_error(0, "MEMBER_INVALID");
        }
        // 会議の指定が有る場合
        $meeting_info = array();
        if ($meeting_key) {
            $where = "room_key = '".addslashes($room_key)."'" .
                            " AND meeting_session_id = '".addslashes($meeting_key)."'" .
                            " AND is_active = 1" .
                            " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            $this->logger2->info($meeting_info);
            // 予約で時間外
            if (!$meeting_info) {
                $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                return $this->display_error("101", "対象の会議が存在しないか、既に終了しています", $this->get_error_info($err_obj));
            } else {
                if ($meeting_info["is_reserved"] &&
                ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
                                (strtotime($meeting_info['meeting_stop_datetime'])) < time())
                )
                ) {
                    $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                    return $this->display_error("101", "時間外の予約です", $this->get_error_info($err_obj));
                } else {
                    // 直前の会議取得
                    $last_meeting_key = $objRoom->getOne("room_key='".addslashes($room_key)."'", 'meeting_key');
                    // 異なる会議
                    if ($meeting_info['meeting_ticket'] != $last_meeting_key) {
                        // 開催中
                        $ret = $obj_N2MY_Meeting->getMeetingStatus($room_key, $last_meeting_key);
                        if ($ret['status'] == '1') {
                            return $this->display_error("101", "前回会議が開催中です", $this->get_error_info($err_obj));
                        }
                    }
                }
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info($meeting_info['meeting_ticket']);
        // 会議情報取得
        if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_info['meeting_ticket'])) {
            $err_obj->set_error(0, "MEETING_INVALID");
            //            return $this->display_error(0, "MEETING_INVALID");
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $meeting_key = $meeting_info["meeting_key"];
        $country_key = $this->session->get("country_key");
        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();
        if (!array_key_exists($country_key, $country_list)) {
            $this->logger2->info($country_list);
            $_country_data = array_shift($country_list);
            $country_key = $_country_data["country_key"];
        }

        // オプション指定
        $options = array(
                        "meeting_name" => $meeting_info["meeting_name"],
                        "user_key"     => $user_info["user_key"],
                        "start_time"   => $meeting_info["start_time"],
                        "end_time"     => $meeting_info["end_time"],
                        "country_id"   => $country_key,
                        "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);

        //会議内からの招待メールのため会議作成毎にmgm_relationにmeeting_session_idを追加
        require_once("classes/mgm/dbi/relation.dbi.php");
        $objMgmRelationTable = new MgmRelationTable(
                        $this->get_auth_dsn());
        $resultRelation = $objMgmRelationTable->getRow(sprintf("relation_key='%s'", $core_session));
        if (!$resultRelation) {
            $data = array(
                            "relation_key"  => $core_session,
                            "user_key"      => $user_info["user_id"],
                            "relation_type" => "meeting",
                            "status"        => 1,
            );
            $add_data = $objMgmRelationTable->add($data);
        }

        // ユーザごとのオプションを取得
        $type        = $this->request->get("type", "normal");
        if ($is_audience = $this->session->get('invited_meeting_audience')) {
            switch($type) {
                case 'normal':
                    $type = 'audience';
                    break;
                case 'multicamera':
                    $type = 'multicamera_audience';
                    break;
                case 'whiteboard':
                    $type = 'whiteboard_audience';
                    break;
                case 'invisible_wb':
                    $type = 'invisible_wb_audience';
                    break;
            }
        }
        //        $mode = "";
        //        if ($meeting_info['is_trial']) {
        //ユーザーがトライアルである場合
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        //participant登録持はinvitedGuestのタイプはないため、inviteに変更する
        if ($login_type == "invitedGuest") {
            $login_type = "invite";
        }

        //Freeの場合はログインが必要なようにflgで渡す(ログインフラグがある時はパスワードフラグは認証時に渡す)
        $need_login_flg = 0;
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $member_info = $this->session->get( "member_info" );
        $relation_list = "";
        if ($member_info) {
            $where = "member_key = ".$member_info["member_key"].
            " AND room_key = '".$room_key."'";
            $relation_list = $objRoomRelation->getRowsAssoc($where);
        }
        //自分の部屋の場合はログイン不要
        if ( ($user_info["account_model"] == "member"  || $user_info["account_model"] == "free") && !$relation_list) {
            $need_login_flg = 1;
            $password_flg   = "0";
        }

        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $where = "meeting_session_id = '".addslashes($core_session)."'" .
                        " AND is_deleted = 0";
        $meeting_info = $obj_Meeting->getRow($where);
        if (!$meeting_info) {
            $message["title"] = MEETING_START_ERROR_TITLE;
            $message["body"] = MEETING_START_ERROR_BODY;
            return $this->render_valid($message);
        }
        $this->session->set("mobile_start_flg", true);
        $url    = $this->config->get("N2MY", "mobile_protcol", 'vcube-meeting')."://join?session=".session_id()."&room_id=".$meeting_info["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meeting_info['pin_cd'];

        if ($meeting_info["meeting_password"]) {
            $password_flg = "0";
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".addslashes($user_info["user_key"])." AND reservation_session = '".addslashes($meeting_info["reservation_session"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            $this->logger->debug("reservation_info",__FILE__,__LINE__,$reservation_info);
            if ($reservation_info["reservation_pw_type"] == "1") {
                $password_flg = "1";
            }
        }
        $data = array(
                        "url"               => $url,
                        API_MEETING_ID      => $this->ticket_to_session($meeting_info["meeting_ticket"]),
                        "meeting_name"      => $meeting_info["meeting_name"],
                        "password_flg"      => $password_flg,
                        "need_login_flg"    => $need_login_flg,
        );
        return $this->output($data);
    }

    function action_meeting_display() {
        $id = $this->request->get("id");
        if (!$id) {
            $this->logger2->warn("id parameter error");
            return false;
        }
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $key = N2MY_ENCRYPT_KEY;
        $iv = N2MY_ENCRYPT_IV;
        $id = str_replace("-", "/", $id);
        $id = str_replace(' ', '+', $id);
        $id = str_replace('_', '+', $id);
        $decryptdata = EZEncrypt::decrypt($key, $iv, $id);
        $this->logger2->info($decryptdata);
        $this->checkAuthorizationMeeting($decryptdata);

        $type = $this->session->get( "type" );
        require_once ("lib/EZLib/EZHTML/EZTemplate.class.php");
        $this->template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
        $this->template->assign('charset', "UTF-8" );

        // 会議情報取得
        $user_info = $this->session->get("user_info");
        $meeting_key = $this->session->get("meeting_key");
        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $where = "meeting_key = '".addslashes($meeting_key)."'" .
                        " AND user_key = '".$user_info["user_key"]."'" .
                        " AND is_active = 1" .
                        " AND is_deleted = 0";
        $meeting_info = $obj_Meetng->getRow($where);
        $this->template->assign('meeting_info' , $meeting_info );
        require_once("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable($this->get_dsn());
        $room_info = $obj_Room->getRow("room_key = '".addslashes($meeting_info['room_key'])."'");
        $this->template->assign("room_info" , $room_info);

        //Skinsの選択を追加
        $skinName = $user_info["user_logo"] ? "SkinsFuji" : "Skins";
        $this->template->assign('skinName' , $skinName );
        $this->template->assign('locale' , $this->get_language() );

        $meeting_version = $this->session->get("meeting_version");
        if ($meeting_version >= "4.7.0.0") {
            $version = "4.7.0.0";
        } else {
            $version = "4.6.5.0";
        }
        $this->template->assign('meeting_version' , $version );

        $content_delivery_base_url = $this->get_content_delivery_base_url();
        $this->template->assign("content_delivery_base_url", $content_delivery_base_url);

        // eco canceler
        $certificate = substr(md5(uniqid(rand(), true)), 1, 16);
        $oneTime = array(
                        "certificate"    =>    $certificate,
                        "createtime"    =>    time());
        $this->session->set("oneTime", $oneTime);
        $this->template->assign('certificateId', $certificate );
        $this->template->assign('apiUrl', '/services/api.php?action_issue_certificate');

        // UserAgent更新
        $this->template->assign('user_agent_ticket' , $this->session->get("user_agent_ticket") );
        // ディスプレイサイズ指定
        $this->template->assign('display_size', htmlspecialchars($this->session->get("display_size"), ENT_QUOTES));
        if ($this->session->get("fl_ver") == "as3") {
            $this->template->assign('meeting_type' , htmlspecialchars($this->session->get("meeting_type"),ENT_QUOTES) );
            $this->template->assign('participant_name' , $this->session->get("participant_name") );
            $this->template->assign('ignore_staff_reenter_alert' , $room_info["ignore_staff_reenter_alert"] );
            // V-Cube Meetingのバージョンが違う場合、キャッシュしたデータを使わせない。
            $version_file = N2MY_APP_DIR."version.dat";
            if ($fp = fopen($version_file, "r")) {
                $version = trim(fgets($fp));
            }
            $this->template->assign('cachebuster' , $version);
            $this->display_encrypt( sprintf( 'core/%s/meeting_as3.t.html', $type ) );
        } else {
            $this->display_encrypt( sprintf( 'core/%s/meeting_base.t.html', $type ) );
        }
    }

    private function get_content_delivery_base_url() {
        $protocol = '';
        $content_delivery_base_url = '';
        $country_key = $this->session->get("country_key");

        if($this->config->get("CONTENT_DELIVERY", "is_enabled", false)) {
            $country_key = $this->session->get("country_key");
            // 中国からの入室の場合、データの参照元をCDNからではなくBravからに設定する。
            $content_delivery_host = $this->config->get("CONTENT_DELIVERY_HOST", $country_key, "");
            if($content_delivery_host) {
                if(strlen($content_delivery_host)) {
                    $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
                }
                $content_delivery_base_url = $protocol . $content_delivery_host;
                $meeting_file_dir = $this->config->get("CONTENT_DELIVERY", "meeting_file_dir", "");
                if(strlen($meeting_file_dir)) {
                    $content_delivery_base_url .= '/' . $meeting_file_dir;
                }
            }
        }
        return $content_delivery_base_url;
    }

    /**
     * 会議開始。Mobaileの場合はアプリにリダイレクト
     *
     * @param
     * @return 会議データを作成し、リダイレクトで会議室に入室
     */
    function action_enter() {
        $this->session->remove("meeting_auth_flg");
        require_once("classes/N2MY_Meeting.class.php");
        require_once("classes/dbi/room.dbi.php");
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $objRoom = new RoomTable($this->get_dsn());
        if ($meeting_ticket = $this->session->get('invited_meeting_ticket')) {
            $where = "room_key = '".addslashes($room_key)."'" .
                            " AND meeting_ticket = '".addslashes($meeting_ticket)."'" .
                            " AND is_active = 1" .
                            " AND is_deleted = 0";
            $meeting_key = $obj_Meeting->getOne($where, 'meeting_session_id');
        } else {
            $meeting_key = $this->request->get(API_MEETING_ID);
        }
        $this->logger2->info($meeting_key);
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // チェックルール編集
        $rules = array(
                        API_ROOM_ID => array(
                                        "required" => true,
                                        "allow" => array_keys($this->session->get("room_info")),
                        ),
                        "flash_version" => array(
                                        "allow" => array("as2", "as3"),
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
                        "screen_mode"  => array(
                                        "allow"    => array('normal', 'wide'),
                        ),
                        "mobile"  => array(
                                        "allow"    => array('1', '0'),
                        ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");
        // ユーザ情報
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );
        //member課金を利用していてメンバー以外が会議を利用しようとした場合
        if ( $user_info["account_model"] == "member" && !$member_info ) {
            //            $err_obj->set_error(0, "MEMBER_INVALID");
            //            return $this->display_error(0, "MEMBER_INVALID");
        }
        // 会議の指定が有る場合
        $meeting_info = array();
        if ($meeting_key) {
            $where = "room_key = '".addslashes($room_key)."'" .
                            " AND meeting_session_id = '".addslashes($meeting_key)."'" .
                            " AND is_active = 1" .
                            " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            $this->logger2->info($meeting_info);
            // 予約で時間外
            if (!$meeting_info) {
                $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                return $this->display_error("101", "対象の会議が存在しないか、既に終了しています", $this->get_error_info($err_obj));
            } else {
                if ($meeting_info["is_reserved"] &&
                ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
                                (strtotime($meeting_info['meeting_stop_datetime'])) < time())
                )
                ) {
                    $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                    return $this->display_error("101", "時間外の予約です", $this->get_error_info($err_obj));
                } else {
                    // 直前の会議取得
                    $last_meeting_key = $objRoom->getOne("room_key='".addslashes($room_key)."'", 'meeting_key');
                    // 異なる会議
                    if ($meeting_info['meeting_ticket'] != $last_meeting_key) {
                        // 開催中
                        $ret = $obj_N2MY_Meeting->getMeetingStatus($room_key, $last_meeting_key);
                        if ($ret['status'] == '1') {
                            return $this->display_error("101", "前回会議が開催中です", $this->get_error_info($err_obj));
                        }
                    }
                }
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info($meeting_info['meeting_ticket']);
        // 会議情報取得
        if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_info['meeting_ticket'])) {
            $err_obj->set_error(0, "MEETING_INVALID");
            //            return $this->display_error(0, "MEETING_INVALID");
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $meeting_key = $meeting_info["meeting_key"];
        $country_key = $this->session->get("country_key");
        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();
        if (!array_key_exists($country_key, $country_list)) {
            $this->logger2->info($country_list);
            $_country_data = array_shift($country_list);
            $country_key = $_country_data["country_key"];
        }
        // オプション指定
        $options = array(
                        "meeting_name" => $meeting_info["meeting_name"],
                        "user_key"     => $user_info["user_key"],
                        "start_time"   => $meeting_info["start_time"],
                        "end_time"     => $meeting_info["end_time"],
                        "country_id"   => $country_key,
                        "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);

        //会議内からの招待メールのため会議作成毎にmgm_relationにmeeting_session_idを追加
        require_once("classes/mgm/dbi/relation.dbi.php");
        $objMgmRelationTable = new MgmRelationTable(
                        $this->get_auth_dsn());
        $resultRelation = $objMgmRelationTable->getRow(sprintf("relation_key='%s'", $core_session));
        if (!$resultRelation) {
            $data = array(
                            "relation_key"  => $core_session,
                            "user_key"      => $user_info["user_id"],
                            "relation_type" => "meeting",
                            "status"        => 1,
            );
            $add_data = $objMgmRelationTable->add($data);
        }

        // ユーザごとのオプションを取得
        $type        = $this->request->get("type", "normal");
        if ($is_audience = $this->session->get('invited_meeting_audience')) {
            switch($type) {
                case 'normal':
                    $type = 'audience';
                    break;
                case 'multicamera':
                    $type = 'multicamera_audience';
                    break;
                case 'whiteboard':
                    $type = 'whiteboard_audience';
                    break;
                case 'invisible_wb':
                    $type = 'invisible_wb_audience';
                    break;
            }
        }
        //        $mode = "";
        //        if ($meeting_info['is_trial']) {
        //ユーザーがトライアルである場合
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        //participant登録持はinvitedGuestのタイプはないため、inviteに変更する
        if ($login_type == "invitedGuest") {
            $login_type = "invite";
        }

        //Freeの場合はログインが必要なようにflgで渡す(ログインフラグがある時はパスワードフラグは認証時に渡す)
        $need_login_flg = 0;
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $member_info = $this->session->get( "member_info" );
        $relation_list = "";
        if ($member_info) {
            $where = "member_key = ".$member_info["member_key"].
            " AND room_key = '".$room_key."'";
            $relation_list = $objRoomRelation->getRowsAssoc($where);
        }
        //自分の部屋の場合はログイン不要
        if ( ($user_info["account_model"] == "member"  || $user_info["account_model"] == "free") && !$relation_list) {
            $need_login_flg = 1;
            $password_flg   = "0";
        }

        /* ブラウザ以外のデバイス */
        //モバイル判別
        if (strpos($_SERVER['HTTP_USER_AGENT'], "iPad;") || strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod")) {
            require_once( "classes/dbi/meeting.dbi.php" );
            $obj_Meeting = new MeetingTable($this->get_dsn());
            $where = "meeting_session_id = '".addslashes($core_session)."'" .
                            " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            if (!$meeting_info) {
                $message["title"] = MEETING_START_ERROR_TITLE;
                $message["body"] = MEETING_START_ERROR_BODY;
                return $this->render_valid($message);
            }
            $url    = $this->config->get("N2MY", "mobile_protcol", 'vcube-meeting')."://join?session=".session_id()."&room_id=".$meeting_info["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meeting_info['pin_cd'];
            $data = array(
                            "url"               => $url,
                            API_MEETING_ID      => $this->ticket_to_session($meeting_info["meeting_ticket"]),
                            "meeting_name"      => $meeting_info["meeting_name"],
                            "password_flg"      => $password_flg,
                            "need_login_flg"    => $need_login_flg,
            );
            return $this->output($data);
            exit();
        }

        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";
        $user_options = array(
                        "id"                  => $member_info["member_key"],
                        "name"                => $this->request->get("name", $member_info["member_name"]),
                        "participant_email"   => $member_info["member_email"],
                        "participant_station" => $this->request->get("station", $member_info["member_station"]),
                        "participant_country" => $country_key,
                        "member_key"          => $member_info["member_key"],
                        "narrow"              => $this->request->get("is_narrow"),
                        "lang"                => $this->session->get("lang"),
                        "skin_type"           => $this->request->get("skin_type", ""),
                        "mode"                => $mode,
                        "role"                => $login_type,
                        "mail_upload_address" => $mail_upload_address,
                        "account_model"       => $user_info["account_model"],
        );
        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);

        $mobile_flg = $this->request->get("mobile");
        $_room_info = $this->session->get("room_info");
        $room_options = $_room_info[$room_key]["options"];

        // 先に入室しているpolycom clientが存在する場合 participant record にmeeting_keyを追加更新
        if ($meetingDetail["ives_conference_id"] && $meetingDetail["ives_did"]) {
            require_once("classes/polycom/Polycom.class.php");
            $polycom = new PolycomClass($this->get_dsn());
            $polycom->addMeetingKey2PolycomParticipant($meetingDetail["meeting_key"], $meetingDetail["ives_conference_id"], $meetingDetail["ives_did"]);
        }

        //マルチカメラと資料共有対応
        if ($type == "normal" && $room_options["multicamera"]) {
            $type = "multicamera";
        } else if ($type == "normal" && $room_options["whiteboard"]) {
            $type = "whiteboard";
        } else if ($type == "audience" && $room_options["multicamera"]) {
            $type = "multicamera_audience";
        } else if ($type == "audience" && $room_options["whiteboard"]) {
            $type = "whiteboard_audience";
        }
        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set( "room_key", $room_key );
        $this->session->set( "type", $meetingDetail["participant_type_name"] );
        $this->session->set( "meeting_type", $type );
        $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set( "participant_key", $meetingDetail["participant_key"] );
        $this->session->set( "participant_name", $this->request->get("name", $member_info["member_name"]) );
        $this->session->set( "mail_upload_address", $mail_upload_address );
        $this->session->set( "fl_ver", $this->request->get("flash_version", "as2") );
        $this->session->set( "reload_type", 'normal');
        $this->session->set( "meeting_version", $user_info["meeting_version"] );
        $this->session->set( "user_agent_ticket", uniqid());
        // 画面サイズ
        $screen_mode = $this->request->get("screen_mode", "normal");
        switch ($screen_mode) {
            case 'normal' :
                $display_size = '4to3';
                break;
            case 'wide' :
                $display_size = '16to9';
                break;
            default :
                $display_size = '4to3';
                break;
        }
        $this->session->set( "display_size", $display_size);
        // 会議入室
        $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESS_NAME."=".session_id();
        if ($meeting_info["meeting_password"]) {

            $this->session->set("redirect_url", $url, $this->_name_space);
            $url = N2MY_BASE_URL."services/" .
                            "?action_login=" .
                            "&".N2MY_SESS_NAME."=".session_id().
                            "&reservation_session=".$meeting_info["reservation_session"].
                            "&ns=".$this->_name_space;

            $password_flg = "0";
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".addslashes($user_info["user_key"])." AND reservation_session = '".addslashes($meeting_info["reservation_session"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            $this->logger->debug("reservation_info",__FILE__,__LINE__,$reservation_info);
            if ($reservation_info["reservation_pw_type"] == "1") {
                $password_flg = "1";
            }
        }
        if ($this->request->get("callbackurl")) {
            $where = "room_key = '".addslashes($room_key)."'" .
                            " AND meeting_key = '".addslashes($meetingDetail["meeting_key"])."'" .
                            " AND is_active = 1" .
                            " AND is_deleted = 0";
            $meeting_data = $obj_Meeting->getRow($where, "room_addition");
            if ($meeting_info["room_addition"]) {
                $addition = unserialize($meeting_data["room_addition"]);
            }
            $addition["callbackurl"] = urldecode($this->request->get("callbackurl"));
            $update_data["room_addition"] = serialize($addition);
            $obj_Meeting->update($update_data, $where);
        }
        $data = array(
                        "url"               => $url,
                        API_MEETING_ID      => $this->ticket_to_session($meetingDetail["meeting_ticket"]),
                        "meeting_name"      => $meetingDetail["meeting_name"],
                        "password_flg"      => $password_flg,
                        "need_login_flg"    => $need_login_flg,
                        //            "meeting_sequence_key"  => $meetingDetail["meeting_sequence_key"],
        //            "participant_key"       => $meetingDetail["participant_key"],
        //            "participant_id"        => $meetingDetail["participant_session_id"],
        );
        return $this->output($data);
    }

    function action_get_invite_url() {
        require_once 'classes/mgm/MGM_Auth.class.php';
        require_once 'classes/N2MY_Auth.class.php';
        require_once 'classes/N2MY_Account.class.php';
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        require_once 'classes/N2MY_Meeting.class.php';

        // DB選択
        $request = $this->request->getAll();
        $meeting_id  = $request[API_MEETING_ID];
        $output_type = $this->request->get("output_type");

        // ダミーの内容を渡しGATEは正常とする
        if($meeting_id == "DUMMY_MEETING_ID"){
            $invite_url = N2MY_BASE_URL."services/invite.php?action_error_url";
            $data_dummy = array(
                            API_MEETING_ID  => "DUMMY_MEETING_ID",
                            "user_invite_url"           => $invite_url,
                            "authority_invite_url"      => $invite_url,
            );

            return $this->output($data_dummy);
        }

        // パラメタチェック
        $rules = array(
                        API_MEETING_ID => array(
                                        "required" => true,
                                        "valid_integer" => true,
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $dsn_info = $obj_MGMClass->getRelationDsn($meeting_id, "meeting");
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "meeting_session_id = '".addslashes($meeting_id)."'" .
                        " AND is_active = 1" .
                        " AND is_deleted = 0";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        require_once("classes/dbi/room.dbi.php");
        $objRoom  = new RoomTable($this->get_dsn());
        $roomInfo = $objRoom->getRow(sprintf('room_key = "%s"', mysql_real_escape_string($meeting_info['room_key'])));
        if(!$roomInfo['is_invite_button']){
            return $this->display_error("100", "PARAMETER_ERROR", "THE SENDING EMAIL FUNCTION IS NOT ALLOWED");
        }
        if ($meeting_info["is_reserved"] == "1") {
            $objReservation    = new ReservationTable($this->get_dsn());
            $where = "reservation_status = 1 AND meeting_key = '".mysql_real_escape_string($meeting_info["meeting_ticket"])."'";
            $reservationInfo = $objReservation->getRow($where);
            if(!$reservationInfo['is_invite_flg']){
                return $this->display_error("100", "PARAMETER_ERROR", "THE SENDING EMAIL FUNCTION IS NOT ALLOWED");
            }
        }
        //会議内からの招待メールのためユーザー、オーディエンス用のハッシュ値を生成
        require_once "classes/N2MY_Invitation.class.php";
        $objInvitation = new N2MY_Invitation($server_info["dsn"]);

        require_once "classes/dbi/meeting_invitation.dbi.php";
        $objMeetingInvitationTable = new MeetingInvitationTable($server_info["dsn"]);
        $where = sprintf("meeting_key='%s' AND status = 1", $meeting_info["meeting_key"]);
        $invitationData = $objMeetingInvitationTable->getRow($where , "*", array("invitation_key"=>"desc"));
        if (!$invitationData) {
            $user_session_id = md5(uniqid(rand(), 1));
            $audience_session_id = md5(uniqid(rand(), 1));
            $authority_session_id = md5(uniqid(rand(), 1));
            $invitationData = array(
                            "meeting_key" => $meeting_info["meeting_key"],
                            "meeting_session_id" => $meeting_info["meeting_session_id"],
                            "user_session_id" => $user_session_id,
                            "audience_session_id" => $audience_session_id,
                            "authority_session_id"=> $authority_session_id,
                            "status" => 1
            );
            if ($meeting_info["is_reserved"])
                $invitationData["meeting_ticket"] = $meeting_info["meeting_ticket"];
            $objMeetingInvitationTable->add($invitationData);
        }
        $lang = $this->session->get("lang") ? $this->session->get("lang") : "en";
        $invite_url = N2MY_BASE_URL."g/".$lang."/".$meeting_info["meeting_session_id"]."/";
        $data = array(
                        API_MEETING_ID  => $meeting_id,
                        "user_invite_url"           => $invite_url.$invitationData["user_session_id"],
                        "authority_invite_url"      => $invite_url.$invitationData["authority_session_id"],
        );
        if($roomInfo['max_audience_seat'] > 0){
            $data['audience_invite_url'] = $invite_url.$invitationData["audience_session_id"];
        }
        return $this->output($data);
    }

    public function action_send_invitation_mail()
    {
        require_once("classes/core/dbi/Meeting.dbi.php");
        require_once("classes/dbi/reservation.dbi.php");
        // パラメタチェック
        $meeting_session_id = $this->request->get(API_MEETING_ID);
        $user_type          = $this->request->get("user_type");
        $email_address      = $this->request->get("email_address");
        $output_type        = $this->request->get("output_type");
        if(!EZValidator::valid_email($email_address)){
            return $this->display_error("100", "PARAMETER_ERROR", "EMAIL ERROR");
        }
        $rules = array(
                        API_MEETING_ID => array(
                                        "required" => true,
                                        "valid_integer" => true,
                        ),
                        'user_type'    => array(
                                        "required" => true,
                                        "allow"    => array("audience", "normal", "authority")
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        $err_obj = $this->error_check($this->request->getAll(), $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $objMeeting = new DBI_Meeting($this->get_dsn());
        $meetingInfo= $objMeeting->getRow(sprintf("meeting_session_id = '%s' AND is_deleted=0 AND is_active = 1", mysql_real_escape_string($meeting_session_id)));
        if(!$meetingInfo){
            return $this->display_error("100", "PARAMETER_ERROR", "MEETING ID ERROR");
        }
        require_once("classes/dbi/room.dbi.php");
        $objRoom  = new RoomTable($this->get_dsn());
        $roomInfo = $objRoom->getRow(sprintf('room_key = "%s"', mysql_real_escape_string($meetingInfo['room_key'])));
        if(!$roomInfo['is_invite_button']){
            return $this->display_error("100", "PARAMETER_ERROR", "THE SENDING EMAIL FUNCTION IS NOT ALLOWED");
        }
        if($roomInfo['max_audience_seat'] == 0 && $user_type == "audience"){
            return $this->display_error("100", "PARAMETER_ERROR", "USER TYPE IS NOT VALID");
        }
        if ($meetingInfo["is_reserved"] == "1") {
            $objReservation    = new ReservationTable($this->get_dsn());
            $where = "reservation_status = 1 AND meeting_key = '".mysql_real_escape_string($meetingInfo["meeting_ticket"])."'";
            $reservationInfo = $objReservation->getRow($where);
            if(!$reservationInfo['is_invite_flg']){
                return $this->display_error("100", "PARAMETER_ERROR", "THE SENDING EMAIL FUNCTION IS NOT ALLOWED");
            }
            if($reservationInfo["reservation_pw_type"] == 2 || $reservationInfo["reservation_pw"] == "")
            {
                $reservationPassword  = "";
            } else {
                $reservationPassword = $reservationInfo["reservation_pw"];
            }
        } else {
            $reservationPassword  = "";
        }
        $lang = $this->session->get("lang");
        if(!$lang){
            $lang = 'ja';
        }
        require_once "classes/dbi/meeting_invitation.dbi.php";
        $objMeetingInvitationTable = new MeetingInvitationTable($this->get_dsn());
        $invitationData = $objMeetingInvitationTable->getRow(sprintf("meeting_key='%s' AND status = 1", $meetingInfo["meeting_key"]) , "*", array("invitation_key"=>"desc"));
        if (!$invitationData) {
            $user_session_id      = md5(uniqid(rand(), 1));
            $audience_session_id  = md5(uniqid(rand(), 1));
            $authority_session_id = md5(uniqid(rand(), 1));
            $invitationData = array(
                            "status"               => 1,
                            "meeting_key"          => $meetingInfo["meeting_key"],
                            "meeting_session_id"   => $meetingInfo["meeting_session_id"],
                            "user_session_id"      => $user_session_id,
                            "audience_session_id"  => $audience_session_id,
                            "authority_session_id" => $authority_session_id,
            );
            if ($meetingInfo["is_reserved"]){
                $invitationData["meeting_ticket"] = $meetingInfo["meeting_ticket"];
            }
            $objMeetingInvitationTable->add($invitationData);
        }
        require_once ("classes/N2MY_Invitation.class.php");
        $objInvidation = new N2MY_Invitation($this->get_dsn());
        $result = $objInvidation->sendInvitationMail($meetingInfo['meeting_key'], $email_address, $lang, $user_type, $reservationPassword);
        if(!$result){
            return $this->display_error("100", "PARAMETER_ERROR");
        }
        $invitation_url = $objInvidation->getInvitationMailUrl($meetingInfo["meeting_session_id"], $lang, $user_type);
        $data = array(
                        API_MEETING_ID  => $meeting_session_id,
                        "email_address" => $email_address,
                        "invitation_url"=> $invitation_url,
        );
        return $this->output($data);
    }

    /**
     *
     */
    function action_stop() {
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $meeting_session = $this->request->get(API_MEETING_ID);
        // ユーザ情報
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );
        // ワンタイムミーティングだったら会議室一覧を更新
        if($user_info["is_one_time_meeting"] == 1 &&  $this->session->get("service_mode") != "sales"){
            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"] , null , true);
            $this->session->set("room_info" ,$rooms);
        }

        // チェックルール編集
        $rules = array(
                        API_ROOM_ID => array(
                                        "required" => true,
                                        "allow" => array_keys($this->session->get("room_info")),
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        if (!$meeting_session) {
            // 部屋で直前に行われた会議を取得（今後、１部屋複数会議を実現した場合この機能は廃止になる可能性有り）
            require_once("classes/dbi/room.dbi.php");
            $objRoom = new RoomTable($this->get_dsn());
            $where = "room_key='".addslashes($room_key)."'" .
                            " AND room_status = 1";
            $meeting_ticket = $objRoom->getOne($where, "meeting_key");
            $meeting_session = $this->ticket_to_session($meeting_ticket);
        } else {
            $meeting_ticket = $this->session_to_ticket($meeting_session);
        }
        if(!$meeting_ticket){
            $err_msg["errors"][] = array(
                            "field"   => API_ROOM_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "部屋キーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        require_once ( "classes/core/Core_Meeting.class.php" );
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $meeting_db = new N2MY_DB($this->get_dsn(), "meeting");
        $where = "meeting_session_id = '".addslashes($meeting_session)."'";
        $meeting_info = $meeting_db->getRow($where);
        // 予約があれば予約も終了
        if ($meeting_info["is_reserved"]) {
            require_once ("classes/dbi/reservation.dbi.php");
            $reservation_obj = new ReservationTable($this->get_dsn());
            $reservation_info = $reservation_obj->getRow("meeting_key = '".addslashes($meeting_ticket)."'");
            $reservation_obj->cancel($reservation_info["reservation_session"]);
        }
        $obj_CoreMeeting->stopMeeting($meeting_info["meeting_key"]);
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $where = "meeting_session_id = '".addslashes($meeting_session)."'" .
                        " AND is_deleted = 0";
        $data = $obj_Meeting->getRow($where);

        $data = array(
                        API_ROOM_ID              => $data['room_key'],
                        API_MEETING_ID           => $data['meeting_session_id'],
                        'meeting_name'           => $data['meeting_name'],
                        'meeting_size_used'      => $data['meeting_size_used'],
                        'is_locked'              => $data['is_locked'],
                        'is_reserved'            => $data['is_reserved'],
                        'is_publish'             => $data['is_publish'],
                        'meeting_use_minute'     => $data['meeting_use_minute'],
                        'eco_move'               => $data['eco_move'],
                        'eco_time'               => $data['eco_time'],
                        'eco_fare'               => $data['eco_fare'],
                        'eco_co2'                => $data['eco_co2'],
                        'meeting_start_date'     => strtotime($data['actual_start_datetime']),
                        'meeting_end_date'       => strtotime($data['actual_end_datetime']),
                        API_MEETINGLOG_MINUTES   => $data['has_recorded_minutes'],
                        API_MEETINGLOG_VIDEO     => $data['has_recorded_video']
        );
        //        unset($data['meeting_key']);
        //        unset($data['fms_path']);
        //        unset($data['layout_key']);
        //        unset($data['server_key']);
        //        unset($data['sharing_server_key']);
        //        unset($data['user_key']);
        //        unset($data['meeting_ticket']);
        //        unset($data['meeting_country']);
        //        unset($data['meeting_room_name']);
        //        unset($data['meeting_tag']);
        //        unset($data['member_keys']);
        //        unset($data['participant_names']);
        //        unset($data['meeting_max_audience']);
        //        unset($data['meeting_max_extendable']);
        //        unset($data['meeting_max_seat']);
        //        unset($data['meeting_size_recordable']);
        //        unset($data['meeting_size_uploadable']);
        //        unset($data['meeting_log_owner']);
        //        unset($data['meeting_log_password']);
        //        unset($data['meeting_start_datetime']);
        //        unset($data['meeting_stop_datetime']);
        //        unset($data['meeting_extended_counter']);
        //        unset($data['is_active']);
        //        unset($data['is_blocked']);
        //        unset($data['is_extended']);
        //        unset($data['version']);
        //        unset($data['create_datetime']);
        //        unset($data['update_datetime']);
        //        unset($data['room_addition']);
        //        unset($data['is_deleted']);
        //        unset($data['use_flg']);
        //        unset($data['eco_info']);
        //        unset($data['meeting_keys']);
        //        unset($data['meeting_names']);
        //        //
        //        $data['meeting_start_date'] = strtotime($data['actual_start_datetime']);
        //        unset($data['actual_start_datetime']);
        //        $data['meeting_end_date'] = strtotime($data['actual_end_datetime']);
        //        unset($data['actual_end_datetime']);
        //        $data[API_MEETINGLOG_MINUTES] = $data["has_recorded_minutes"];
        //        unset($data["has_recorded_minutes"]);
        //        $data[API_MEETINGLOG_VIDEO] = $data["has_recorded_video"];
        //        unset($data["has_recorded_video"]);
        //        // 統一
        //        $data[API_ROOM_ID] = $data['room_key'];
        //        unset($data['room_key']);
        //        $data[API_MEETING_ID] = $data['meeting_session_id'];
        //        unset($data['meeting_session_id']);

        return $this->output($data);
    }

    /**
     *  強制終了
     */
    function action_force_stop() {
        $user_info   = $this->session->get("user_info");
        if(!$user_info['force_stop_meeting_flg']){
            return $this->display_error("100", "PARAMETER_ERROR", 'FORCE STOP MEETING IS NOT ALLOWED');
        }
        $room_key = $this->request->get(API_ROOM_ID);
        $member_info = $this->session->get('member_info');
        $rules = array(
                        API_ROOM_ID    => array(
                                        "required" => true,
                                        "allow"    => array_keys($this->session->get("room_info")),
                        ),
                        'output_type'  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        // チェック
        if($this->check_one_account() && $member_info){
            $rules[API_ROOM_ID]['allow'] = array_keys($this->getRoomListInWhiteList($member_info['member_key']));
        }
        $err_obj = $this->error_check($this->request->getAll(), $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        //パスワード判断
        if($this->check_one_account()){
            $role = $this->session->get('role');
            if($role != '10' && $member_info){
                $this->logger2->info("admin_user_required", "force_stop_meeting");
                $err_msg["errors"][] = array(
                                "field"   => "ADMIN",
                                "err_cd"  => "INVALID",
                                "err_msg" => "admin_user_required",
                );
                return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
            }
        }else{
            $admin_pw = $this->request->get('admin_pw');
            require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
            require_once ('classes/dbi/user.dbi.php');
            $objUser = new UserTable($this->get_dsn());
            $admin_password = $objUser -> getOne('user_key = ' . $user_info["user_key"] , "user_admin_password");
            $db_admin_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $admin_password);
            if( 0 != strcmp($db_admin_password, $admin_pw)){
                $this->logger2->info("admin_password_miss", "force_stop_meeting");
                $err_msg["errors"][] = array(
                                "field"   => "ADMIN_PASSWORD",
                                "err_cd"  => "INVALID",
                                "err_msg" => "ADMIN_PASSWORD",
                );
                return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
            }
        }
        //処理開始
        require_once("classes/core/dbi/Meeting.dbi.php");
        $objMeeting = new DBI_Meeting($this->get_dsn());
        $where = sprintf('is_active = 1 AND is_reserved = 0 AND room_key = "%s"', mysql_real_escape_string($room_key));
        $meeting_key = $objMeeting -> getOne($where, "meeting_key");
        if($meeting_key == null){
            $this->logger2->info("meeting does not exist", "force_stop_meeting");
            $err_msg["errors"][] = array(
                            "field"   => "MEETING_KEY",
                            "err_cd"  => "INVALID",
                            "err_msg" => "MEETING_KEY INVALID",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        require_once("classes/core/Core_Meeting.class.php");
        $objCoreMeeting = new Core_Meeting($this->get_dsn());
        $objCoreMeeting -> stopMeeting($meeting_key);
        $data = $objMeeting->getRow('meeting_key = "' . mysql_real_escape_string($meeting_key) . '"');
        return $this->output(array(
                        API_ROOM_ID              => $data['room_key'],
                        API_MEETING_ID           => $data['meeting_session_id'],
                        'meeting_name'           => $data['meeting_name'],
                        'meeting_use_minute'     => $data['meeting_use_minute'],
                        'meeting_start_date'     => strtotime($data['actual_start_datetime']),
                        'meeting_end_date'       => strtotime($data['actual_end_datetime']),
        ));
    }

    /**
     * ノッカー
     */
    function action_knocker() {
        $request = $this->request->getAll();
        $meeting_ticket = $this->request->get(API_MEETING_ID);
        // チェックルール編集
        $rules = array(
                        API_MEETING_ID => array(
                                        "required" => true,
                        ),
                        "message" => array(
                                        "required" => true,
                                        "maxlen" => 100,
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        require_once("lib/EZLib/EZUtil/EZRtmp.class.php");
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
        $rtmp = new EZRtmp();
        // 会議のステータス取得
        $where = "meeting_session_id = '".addslashes($meeting_ticket)."'";
        $meeting_info = $obj_Meetng->getRow($where);
        // サーバ情報
        $server_info = $obj_FmsServer->getRow( sprintf("server_key=%d", $meeting_info["server_key"] ));
        $msg = addslashes($request["message"]);
        $msg = "'".$msg."'";
        $meeting_id = $obj_Meetng->getMeetingID($meeting_info["meeting_key"]);
        $uri = "rtmp://".$server_info["server_address"]."/".$this->config->get( "CORE", "app_name" )."/".$meeting_info["fms_path"].$meeting_id;
        $cmd .= "\"h = RTMPHack.new; " .
                        "h.connection_uri = '" . $uri . "';" .
                        "h.connection_args = [0x310, {:target => 'ALL', :message => ".$msg. "}];" .
                        "h.method_name = '';" .
                        "h.execute\"";
        $this->logger2->info($cmd);
        $rtmp->run($cmd);
        $data = array();
        return $this->output($data);
    }

    /**
     * 資料追加
     *
     */
    function action_wb_upload() {
        $request = $this->request->getAll();
        $meeting_ticket = $this->request->get(API_MEETING_ID);
        require_once ("classes/dbi/meeting.dbi.php");
        $objMeetng = new MeetingTable($this->get_dsn());
        $meeting_info = $objMeetng->findByMeetingSessionId($meeting_ticket);
        $file = $_FILES["file"];
        // チェックルール編集
        $rules = array(
                        API_MEETING_ID => array(
                                        "required" => true,
                        ),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        if (!$file) {
            $err_obj->set_error("file", "required");
        } else {
            require_once ("classes/N2MY_Document.class.php");
            $objDocument = new N2MY_Document($this->get_dsn());
            $support_document_list = $objDocument->getSupportFormat($meeting_info['room_key']);
            $file_param = pathinfo($file['name']);
            $file_extension = strtolower($file_param["extension"]);
            if (!in_array($file_extension, $support_document_list['support_ext_list'])) {
                $this->logger2->info(array($file_param["extension"], $support_document_list['support_ext_list']));
                $err_obj->set_error("file", "filetype");
            } else {
                // 画像
                require_once ("classes/dbi/room.dbi.php");
                $objRoom = new RoomTable($this->get_dsn());
                $room_info = $objRoom->get_detail($meeting_info['room_key']);
                if (in_array($file_extension, $support_document_list['image_ext_list'])) {
                    $maxsize = ($room_info["whiteboard_size"] <= 5) ? $room_info["whiteboard_size"] : 5;
                    // 資料
                } else {
                    $maxsize = ($room_info["whiteboard_size"] <= 20) ? $room_info["whiteboard_size"] : 20;
                }
                if ($file['size'] > $maxsize * 1024 * 1024) {
                    $err_obj->set_error("file", "maxfilesize");
                }
                // 主に upload_max_filesize 以上の大きさのファイルがアップロードされた場合
                else if ($file["error"] == UPLOAD_ERR_INI_SIZE) {
                    $err_obj->set_error("file", "maxfilesize");
                }
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            $this->logger2->info($this->get_error_info($err_obj));
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once("lib/EZLib/EZUtil/EZString.class.php");
        $user_info = $this->session->get("user_info");
        $meeting_key = $meeting_info["meeting_key"];
        $document_path = $user_info["user_id"]."/".$meeting_info["room_key"]."/";

        $file = $_FILES["file"];
        $dir = $this->get_work_dir();
        $string = new EZString();
        $document_id = $string->create_id();
        $fileinfo = pathinfo($file["name"]);
        $tmp_name = $dir."/".$document_id.".".$fileinfo["extension"];
        move_uploaded_file($file["tmp_name"], $tmp_name);
        $name = ($name) ? $name : $fileinfo["basename"];
        $format = $this->request->get('format');
        if($format != "bitmap" && $format != "vector"){
            $format = "bitmap";
        }
        // 部屋一覧取得
        $url = N2MY_LOCAL_URL."/api/document/";
        $post_data = array(
                        "action_wb_upload" => "1",
                        "db_host"       => $this->get_dsn_key(),
                        "meeting_key"   => $meeting_key,
                        "document_path" => $document_path,
                        "Filedata"      => "@".$tmp_name,
                        "format"        => $format,
        );
        $ch = curl_init();
        $option = array(
                        CURLOPT_URL => $url,
                        CURLOPT_POST => 1,
                        CURLOPT_POSTFIELDS => $post_data,
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_CONNECTTIMEOUT => 10,
                        CURLOPT_TIMEOUT => 10,
        );
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        unlink($tmp_name);
        $this->logger2->info(array($option, $ret));
        if(!$ret){
            return $this->display_error("100", "PARAMETER_ERROR", "FILE UPLOAD FAILED");
        }else{
            $data = array(
                            "document_id" => $ret,
            );
            return $this->output($data);
        }
    }

    function action_add_room_one_time_meeting(){
        $request = $this->request->getAll();
        // チェックルール編集
        $rules = array(
                        "output_type"  => array(
                                        "allow"        => $this->get_output_type_list(),
                        ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        $user_info = $this->session->get("user_info");
        if($user_info["is_one_time_meeting"] != 1){
            // エラー処理
            return $this->display_error("100", "PARAMETER_ERROR", "user_type_error");
        }

        require_once( "classes/N2MY_Meeting.class.php" );
        $obj_N2MYMeeting = new N2MY_Meeting( $this->get_dsn() );
        $room_count = $obj_N2MYMeeting->getOneTimeMeetingRoomCount($user_info["user_key"]);
        // 部屋上限
        $max_room_count = $this->config->get('VCUBE_GATE', 'max_one_time_room_count');
        if(!$max_room_count){
            $max_room_count = 100;
        }
        if($room_count > $max_room_count){
            // one time meeting部屋は100まで
            return $this->display_error("100", "PARAMETER_ERROR", "room_count_max_over");
        }
        $room_key = $obj_N2MYMeeting->createOneTimeMeetingRoom($user_info["user_key"]);
        if(!$room_key){
            return $this->display_error("100", "PARAMETER_ERROR", "user_plan_error");
        }
        $data = array(
                        "room_id" => $room_key
        );
        return $this->output($data);
    }

    function action_invite_member()
    {
        $request = $this->request->getAll();
        $meeting_id  = $request[API_MEETING_ID];
        $member_id  = $request["member_id"];
        // パラメタチェック
        $rules = array(
                        API_MEETING_ID => array(
                                        "required" => true,
                        ),
                        "member_id"    => array("required" => true),
                        "output_type"  => array(
                                        "allow"    => $this->get_output_type_list(),
                        ),
        );
        $member_info = $this->session->get("member_info");
        if (!$member_info) {
            $err_obj->set_error("member error", "not member login");
        }
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once 'classes/mgm/MGM_Auth.class.php';
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $dsn_info = $obj_MGMClass->getRelationDsn($meeting_id, "meeting");
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        require_once 'classes/dbi/meeting.dbi.php';
        $obj_Meeting = new MeetingTable($server_info["dsn"]);
        $where = "meeting_session_id = '".addslashes($meeting_id)."'" .
                        " AND is_active = 1" .
                        " AND is_deleted = 0";
        $meeting_info = $obj_Meeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                            "field"   => API_MEETING_ID,
                            "err_cd"  => "invalid",
                            "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        //招待されるメンバー情報取得
        require_once("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable($this->get_dsn());
        $room_info = $obj_Room->getRow("room_key='".addslashes($meeting_info["room_key"])."'");

        require_once 'classes/dbi/member.dbi.php';
        $obj_Member = new MemberTable($server_info["dsn"]);
        $where = "member_id = '".addslashes($member_id)."'" .
                        " AND member_status = 0";
        if ($room_info["use_stb_option"]) {
            $where .= " AND use_stb = 1";
        }
        $invite_member_info = $obj_Member->getRow($where);
        if (!$invite_member_info) {
            $err_msg["errors"][] = array(
                            "field"   => "member_id",
                            "err_cd"  => "invalid",
                            "err_msg" => "メンバーIDが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        require_once( "classes/dbi/meeting_invitation_user.dbi.php" );
        $obj_MeetingInvitationUser = new MeetingInvitationUserTable( $server_info["dsn"] );
        //すでに招待済みか確認
        $where = "meeting_session_id = '".$meeting_info["meeting_session_id"]."'".
                        " AND invitee_member_id = '".htmlspecialchars($invite_member_info["member_id"])."'".
                        " AND status = 1";
        $invite_coount = $obj_MeetingInvitationUser->numRows($where);
        if ($invite_coount > 0) {
            $this->logger2->info($invite_member_info, "already invited");
        } else {
            $data = array("meeting_key" => $meeting_info["meeting_key"],
                            "meeting_session_id" => $meeting_info["meeting_session_id"],
                            "sender_member_id" => $member_info["member_id"],
                            "invitee_member_id" => $invite_member_info["member_id"],
                            "status" => 1,
            );
            $obj_MeetingInvitationUser->add($data);
        }
        $data = array();
        return $this->output($data);
    }

    function action_delete_invite_member() {
        $request  = $this->request->getAll();
        $meeting_session_id = $this->request->get(API_MEETING_ID);
        $member_id           = $this->request->get("member_id");
        $user_info = $this->session->get("user_info");
        $rules = array(
                        API_MEETING_ID => array(
                                        "required" => true
                        ),
                        "member_id" => array(
                                        "required" => true
                        ),
                        "output_type" => array(
                                        "allow" => $this->get_output_type_list(),
                        ),
        );
        $err_obj = $this->error_check($request, $rules);
        // 予約が存在しない
        require_once ("classes/dbi/meeting.dbi.php");
        $obj_Meeting = new MeetingTable($this->get_dsn());
        if (!$meeting_info   = $obj_Meeting->getRow("meeting_session_id = '".htmlspecialchars($meeting_session_id)."'")) {
            $err_obj->set_error(API_MEETING_ID, "MEETING_SESSION_INVALID");
        }
        require_once 'classes/dbi/member.dbi.php';
        $obj_Member = new MemberTable($this->get_dsn());
        $where = "member_id = '".addslashes($member_id)."'";
        $invitee_member_info = $obj_Member->getRow($where);
        if (!$invitee_member_info) {
            $err_msg["errors"][] = array(
                            "field"   => "member_id",
                            "err_cd"  => "invalid",
                            "err_msg" => "メンバーIDが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once( "classes/dbi/meeting_invitation_user.dbi.php" );
        $obj_MeetingInvitationUser = new MeetingInvitationUserTable( $this->get_dsn() );
        //招待済みか確認
        $where = "meeting_session_id = '".$meeting_info["meeting_session_id"]."'".
                        " AND invitee_member_id = '".htmlspecialchars($member_id)."'".
                        " AND status = 1";
        $invite_info = $obj_MeetingInvitationUser->getRow($where);
        if (!$invite_info) {
            $err_msg["errors"][] = array(
                            "field"   => "member_id",
                            "err_cd"  => "not invited",
                            "err_msg" => "メンバーIDが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        $where = "meeting_invitation_user_key = ".$invite_info["meeting_invitation_user_key"];
        $update_data = array("status" => "0");
        $obj_MeetingInvitationUser->update($update_data, $where);
        $where = "meeting_session_id = '".$meeting_info["meeting_session_id"]."'".
                        " AND invitee_member_id = '".htmlspecialchars($member_id)."'";
        $invite_info = $obj_MeetingInvitationUser->getRow($where);
        $data = array();
        return $this->output($data);
    }

}
$main = new N2MY_Meeting_API();
$main->execute();
