<?php
require_once("classes/N2MY_Api.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/mail_doc_conv.dbi.php");

class N2MY_MeetingAdminRoomAPI extends N2MY_Api
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        header('Pragma:');
    }

    function auth()
    {
        $this->checkAuthorizationAdmin();
        $this->obj_Room = new RoomTable($this->get_dsn());
        $user_info = $this->session->get("user_info");
        $this->obj_MailWB = new MailDocConvet($this->get_dsn(), $user_info["user_key"]);
    }

    /**
     * 設定
     */
    function action_update()
    {
        $room_key       = $this->request->get("room_id");
        $request = $this->request->getAll();
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "room_id"                               => $request["room_id"],
            "room_name"                             => $request["room_name"],
            "default_h264_use_flg"                  => $request["default_h264_use_flg"],
            "prohibit_extend_meeting_flg"           => $request["prohibit_extend_meeting_flg"],
            "disable_rec_flg"                       => $request["disable_rec_flg"],
            "whiteboard_page"                       => $request["whiteboard_page"],
            "whiteboard_size"                       => $request["whiteboard_size"],
            "is_wb_no"                              => $request["is_wb_no"],
            "is_convert_wb_to_pdf"                  => $request["is_convert_wb_to_pdf"],
            "is_personal_wb"                        => $request["is_personal_wb"],
            "mail_wb_upload"                        => $request["mail_wb_upload"],
            "cabinet"                               => $request["cabinet"],
            "cabinet_size"                          => $request["cabinet_size"],
            "use_teleconf"                          => $request["use_teleconf"],
            "use_pgi_dialin"                        => $request["use_pgi_dialin"],
            "use_pgi_dialin_free"                   => $request["use_pgi_dialin_free"],
            "use_pgi_dialout"                       => $request["use_pgi_dialout"],
            "is_device_skip"                        => $request["is_device_skip"],
            "default_layout_4to3"                   => $request["default_layout_4to3"],
            "default_layout_16to9"                  => $request["default_layout_16to9"],
            "default_microphone_mute"               => $request["default_microphone_mute"],
            "default_camera_mute"                   => $request["default_camera_mute"],
            "default_agc_use_flg"                   => $request["default_agc_use_flg"],
            "is_auto_transceiver"                   => $request["is_auto_transceiver"],
        	"transceiver_number"                   	=> $request["transceiver_number"],
            "is_auto_voice_priority"                => $request["is_auto_voice_priority"],
            "is_netspeed_check"                     => $request["is_netspeed_check"],
            "is_participant_notifier"               => $request["is_participant_notifier"],
            "audience_chat_flg"                     => $request["audience_chat_flg"],
            "rec_gw_convert_type"                   => $request["rec_gw_convert_type"],
            "rec_gw_userpage_setting_flg"           => $request["rec_gw_userpage_setting_flg"],
            "customer_camera_use_flg"               => $request["customer_camera_use_flg"],
            "customer_camera_status"                => $request["customer_camera_status"],
            "customer_sharing_button_hide_status"   => $request["customer_sharing_button_hide_status"],
            "wb_allow_flg"                          => $request["wb_allow_flg"],
            "chat_allow_flg"                        => $request["chat_allow_flg"],
            "print_allow_flg"                       => $request["print_allow_flg"],

        );
        $rules = array(
            "room_id" => array(
                "required" => true,
            ),
            "room_name" => array(
                //"required" => true
                "maxlen"   => 20,
            ),
            "default_h264_use_flg" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "prohibit_extend_meeting_flg" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "disable_rec_flg" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "whiteboard_page" => array(
                //"required" => true,
                "allow" => array("1", "3", "5", "10", "15", "25", "50", "75", "100"),
            ),
            "whiteboard_size" => array(
                //"required" => true,
                "allow" => array("1", "3", "5", "10", "15", "20"),
            ),
            "is_wb_no" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "is_convert_wb_to_pdf" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "is_personal_wb" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "mail_wb_upload" => array(
                //"required" => true,
                "allow" => array("all", "select", "deny"),
            ),
            "cabinet" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "cabinet_size" => array(
                //"required" => true,
                "allow" => array("1", "3", "5", "10", "15", "20"),
            ),
            "use_teleconf" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "use_pgi_dialin" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "use_pgi_dialin_free" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "use_pgi_dialout" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "is_device_skip" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "default_layout_4to3" => array(
                //"required" => true,
                "allow" => array("normal", "largeWhiteboard", "small", "video", "onlyWhiteboard"),
            ),
            "default_layout_16to9" => array(
                //"required" => true,
                "allow" => array("normal", "onlyWhiteboard", "small", "video", "video,small"),
            ),
            "default_microphone_mute" => array(
                //"required" => true,
                "allow" => array("fixedon", "on", "off"),
            ),
            "default_camera_mute" => array(
                //"required" => true,
                "allow" => array("fixedon", "on", "off"),
            ),
            "default_agc_use_flg" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "is_auto_transceiver" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "transceiver_number" => array(
                //"required" => true,
                "minval" => "2",
                "maxval" => "11",
        		),
            "is_auto_voice_priority" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "is_netspeed_check" => array(
                //"required" => true,
                "allow" => array("ftth", "adsl", "catv", "isdn", "mobile"),
            ),
            "is_participant_notifier" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "audience_chat_flg" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "rec_gw_convert_type" => array(
                //"required" => true,
                "allow" => array("mov", "wmv"),
            ),
            "rec_gw_userpage_setting_flg" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "customer_camera_use_flg" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "customer_camera_status" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "customer_sharing_button_hide_status" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "wb_allow_flg" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "chat_allow_flg" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
            "print_allow_flg" => array(
                //"required" => true,
                "allow" => array("0", "1"),
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        
        if ($request["whiteboard_filetype_image"]) {
        	$data["whiteboard_filetype"]["image"] = explode(",", $request["whiteboard_filetype_image"]);
        	$allow_type = array("bmp","gif","jpg","png","tif","eps","psd");
        	if($error = $this->check_in_type_list($data["whiteboard_filetype"]["image"],$allow_type)){
        		$err_obj->set_error("whiteboard_filetype_image", $error." is not allowed");
        	}
        }
        if ($request["whiteboard_filetype_document"]) {
        	$data["whiteboard_filetype"]["document"] = explode(",", $request["whiteboard_filetype_document"]);
        	$allow_type = array("word","excel","ppt","vsd","pdf");
        	if($error = $this->check_in_type_list($data["whiteboard_filetype"]["document"],$allow_type)){
        		$err_obj->set_error("whiteboard_filetype_document", $error." is not allowed");
        	}
        }
        if ($request["cabinet_filetype"]) {
        	$data["cabinet_filetype"] = explode(",", $request["cabinet_filetype"]);
            foreach ($data["cabinet_filetype"] as $ext) {
                if (ereg("[^0-9a-z]", $ext)) {
                    $err_obj->set_error("cabinet_filetype", "alnum", $ext);
                    break;
                }
            }
        }
        $protocol = explode(",", $request["rtmp_protocol"]);
        if ($request["rtmp_protocol"]) {
        	$allow_type = array("rtmp:1935","rtmp:8080","rtmps-tls:443","rtmpt:80","rtmpt:8080","rtmp:80","rtmps:443");
        	if($error = $this->check_in_type_list($protocol,$allow_type)){
        		$err_obj->set_error("rtmp_protocol", $error." is not allowed");
        	}
        }
        $rooms_info = $this->session->get("room_info");
        if (!$rooms_info[$room_key]) {
            $err_obj->set_error(API_ROOM_ID, "ROOM_NOTFOUND", $room_key);
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $room_info = $rooms_info[$room_key];
       
        $update_data = array(
        	"room_name"                             => $request["room_name"]?$request["room_name"]:$room_info["room_info"]["room_name"],
            "default_h264_use_flg"                  => $request["default_h264_use_flg"]?$request["default_h264_use_flg"]:$room_info["room_info"]["default_h264_use_flg"],
            "prohibit_extend_meeting_flg"           => $request["prohibit_extend_meeting_flg"]?$request["prohibit_extend_meeting_flg"]:$room_info["room_info"]["prohibit_extend_meeting_flg"],
            "disable_rec_flg"                       => $request["disable_rec_flg"]?$request["disable_rec_flg"]:$room_info["room_info"]["disable_rec_flg"],
            "whiteboard_page"                       => $request["whiteboard_page"]?$request["whiteboard_page"]:$room_info["room_info"]["whiteboard_page"],
            "whiteboard_size"                       => $request["whiteboard_size"]?$request["whiteboard_size"]:$room_info["room_info"]["whiteboard_size"],
            "is_wb_no"                              => $request["is_wb_no"]?$request["is_wb_no"]:$room_info["room_info"]["is_wb_no"],
            "is_convert_wb_to_pdf"                  => $request["is_convert_wb_to_pdf"]?$request["is_convert_wb_to_pdf"]:$room_info["room_info"]["is_convert_wb_to_pdf"],
            "is_personal_wb"                        => $request["is_personal_wb"]?$request["is_personal_wb"]:$room_info["room_info"]["is_personal_wb"],
            "mfp"                                   => $request["mail_wb_upload"]?$request["mail_wb_upload"]:$room_info["room_info"]["mfp"],
            "cabinet"                               => $request["cabinet"]?$request["cabinet"]:$room_info["room_info"]["cabinet"],
            "cabinet_size"                          => $request["cabinet_size"]?$request["cabinet_size"]:$room_info["room_info"]["cabinet_size"],
            "use_teleconf"                          => $request["use_teleconf"]?$request["use_teleconf"]:$room_info["room_info"]["use_teleconf"],
            "use_pgi_dialin"                        => $request["use_pgi_dialin"]?$request["use_pgi_dialin"]:$room_info["room_info"]["use_pgi_dialin"],
            "use_pgi_dialin_free"                   => $request["use_pgi_dialin_free"]?$request["use_pgi_dialin_free"]:$room_info["room_info"]["use_pgi_dialin_free"],
            "use_pgi_dialout"                       => $request["use_pgi_dialout"]?$request["use_pgi_dialout"]:$room_info["room_info"]["use_pgi_dialout"],
            "is_device_skip"                        => $request["is_device_skip"]?$request["is_device_skip"]:$room_info["room_info"]["is_device_skip"],
            "rtmp_protocol"                         => $request["rtmp_protocol"],
            "default_layout_4to3"                   => $request["default_layout_4to3"]?$request["default_layout_4to3"]:$room_info["room_info"]["default_layout_4to3"],
            "default_layout_16to9"                  => $request["default_layout_16to9"]?$request["default_layout_16to9"]:$room_info["room_info"]["default_layout_16to9"],
            "default_microphone_mute"               => $request["default_microphone_mute"]?$request["default_microphone_mute"]:$room_info["room_info"]["default_microphone_mute"],
            "default_camera_mute"                   => $request["default_camera_mute"]?$request["default_camera_mute"]:$room_info["room_info"]["default_camera_mute"],
            "default_agc_use_flg"                   => $request["default_agc_use_flg"]?$request["default_agc_use_flg"]:$room_info["room_info"]["default_agc_use_flg"],
            "is_auto_transceiver"                   => $request["is_auto_transceiver"]?$request["is_auto_transceiver"]:$room_info["room_info"]["is_auto_transceiver"],
            "transceiver_number"				 	=> $request["transceiver_number"]?$request["transceiver_number"]:$room_info["room_info"]["transceiver_number"],
        	"is_auto_voice_priority"                => $request["is_auto_voice_priority"]?$request["is_auto_voice_priority"]:$room_info["room_info"]["is_auto_voice_priority"],
            "is_netspeed_check"                     => $request["is_netspeed_check"],
            "is_participant_notifier"               => $request["is_participant_notifier"]?$request["is_participant_notifier"]:$room_info["room_info"]["is_participant_notifier"],
            "audience_chat_flg"                     => $request["audience_chat_flg"]?$request["audience_chat_flg"]:$room_info["room_info"]["audience_chat_flg"],
            "rec_gw_convert_type"                   => $request["rec_gw_convert_type"]?$request["rec_gw_convert_type"]:$room_info["room_info"]["rec_gw_convert_type"],
            "rec_gw_userpage_setting_flg"           => $request["rec_gw_userpage_setting_flg"]?$request["rec_gw_userpage_setting_flg"]:$room_info["room_info"]["rec_gw_userpage_setting_flg"],
            "customer_camera_use_flg"               => $request["customer_camera_use_flg"]?$request["customer_camera_use_flg"]:$room_info["room_info"]["customer_camera_use_flg"],
            "customer_camera_status"                => $request["customer_camera_status"]?$request["customer_camera_status"]:$room_info["room_info"]["customer_camera_status"],
            "customer_sharing_button_hide_status"   => $request["customer_sharing_button_hide_status"]?$request["customer_sharing_button_hide_status"]:$room_info["room_info"]["customer_sharing_button_hide_status"],
            "wb_allow_flg"                          => $request["wb_allow_flg"]?$request["wb_allow_flg"]:$room_info["room_info"]["wb_allow_flg"],
            "chat_allow_flg"                        => $request["chat_allow_flg"]?$request["chat_allow_flg"]:$room_info["room_info"]["chat_allow_flg"],
            "print_allow_flg"                       => $request["print_allow_flg"]?$request["print_allow_flg"]:$room_info["room_info"]["print_allow_flg"],
        );
        $update_data["cabinet_filetype"] = serialize($data["cabinet_filetype"]);
        $update_data["whiteboard_filetype"] = serialize($data["whiteboard_filetype"]);//isset($request["whiteboard_filetype"])?serialize($data["whiteboard_filetype"]):$room_info["whiteboard_filetype"];
        //機能制限ついて
        if(!$room_info["options"]["h264"]) {
        	unset($update_data["default_h264_use_flg"]);
        }
        if(!$room_info["options"]["whiteboard"]) {
        	unset($update_data["disable_rec_flg"]);
        	unset($update_data["default_layout_4to3"]);
        	unset($update_data["default_layout_16to9"]);
        }
        if(!$room_info["room_info"]["use_sales_option"]) {
        	unset($update_data["is_convert_wb_to_pdf"]);
        	unset($update_data["default_layout_4to3"]);
        	unset($update_data["customer_camera_use_flg"]);
        	unset($update_data["customer_camera_status"]);
        	unset($update_data["customer_sharing_button_hide_status"]);
        	unset($update_data["wb_allow_flg"]);
        	unset($update_data["chat_allow_flg"]);
        	unset($update_data["print_allow_flg"]);
        } else {
        	if($update_data["default_layout_16to9"]!="onlyWhiteboard" && $update_data["default_layout_16to9"]!="normal" ) {
        		$err_obj->set_error("default_layout_16to9", "sales room can only set as normal or onlyWhiteboard");
        	}
        	unset($update_data["is_convert_wb_to_pdf"]);
        	
        }
        if($room_info["room_info"]["max_audience_seat"] <= 0) {
        	unset($update_data["audience_chat_flg"]);
        }
        if(!$room_info["options"]["record_gw"]) {
        	unset($update_data["rec_gw_convert_type"]);
        	unset($update_data["rec_gw_userpage_setting_flg"]);
        }
        if(!$room_info["options"]["teleconference"]) {
        	unset($update_data["use_teleconf"]);
        	unset($update_data["use_pgi_dialin"]);
        	unset($update_data["use_pgi_dialin_free"]);
        	unset($update_data["use_pgi_dialout"]);
        } else {
        	if ($update_data['use_teleconf'] && !$update_data['use_pgi_dialin'] && !$update_data['use_pgi_dialin_free'] && !$update_data['use_pgi_dialout']) {
        		$err_obj->set_error("teleconf", "minitem", 1);
        	}
        }
        $this->logger2->info($update_data);
        $where = "room_key = '".addslashes($room_key)."'";
        $this->obj_Room->update($update_data, $where);
        $this->add_operation_log('setup_room', array( 'room_key' => $room_key));
        return $this->output();
    }

    function check_in_type_list($data, $allow_type) {        
        $forbidden_data = "";
        foreach ($data as $value) {
        	$this->logger->info($value);
            if(!in_array($value, $allow_type)) {
               if( $forbidden_data == "") {
               	$forbidden_data = $value;
               } else {
               	$forbidden_data .= ", ".$value;
               }
            }
        }
        return $forbidden_data;
    }

    /**
     *
     */
    function action_get_wb_mail() {
        $rows = $this->obj_MailWB->getList();
        $wb_mail_list = array();
        foreach ($rows as $row) {
            $wb_mail_list[] = array(
                "wb_mail_id" => $row["uniq_key"],
                "name" => $row["name"],
                "email" => $row["mail_address"],
            );
        }
        return $this->output(array("wb_mail_list" => $wb_mail_list));
    }

    function action_add_wb_mail() {
        $name           = $this->request->get("name");
        $email          = $this->request->get("email");
        $check_data = array(
            "name" => $name,
            "email" => $email,
        );
        $rules = array(
            "name" => array(
                "required" => true
            ),
            "email" => array(
                "required" => true,
            	"regex" => '/^([*+!.&#$|\'\\%\/0-9a-zA-Z^_`{}=?~:-]+)?@((?:[-a-zA-Z0-9]+\.)+[a-zA-Z]{2,6})$/',
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $data = array(
            "name"             => $name,
            "mail_address"     => $email
            );
        if (!$result = $this->obj_MailWB->add($data)) {
            return $this->display_error(1000, "DB Error", $this->get_error_info($err_obj));
        }
        $data = array(
            "wb_mail_id" => $result
        );
        $this->add_operation_log('mail_wb_upload_address_add', array('mail' => $email));
        return $this->output($data);
    }

    function action_update_wb_mail() {
        $wb_mail_key    = $this->request->get("wb_mail_id");
        $name           = $this->request->get("name");
        $email          = $this->request->get("email");
        $check_data = array(
            "wb_mail_key" => $wb_mail_key,
            "name" => $name,
            "email" => $email,
        );
        $rules = array(
            "wb_mail_key" => array(
                "required" => true
            ),
            "name" => array(
                "required" => true
            ),
            "email" => array(
                "required" => true,
            	"email" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $data = array(
            "name"             => $name,
            "mail_address"     => $email
            );
        $where = "uniq_key = '".addslashes($wb_mail_key)."'";
        $result = $this->obj_MailWB->update($data, $where);
        $this->add_operation_log('mail_wb_upload_address_update', array('mail' => $email));
        return $this->output();
    }

    function action_delete_wb_mail() {
        $wb_mail_key    = $this->request->get("wb_mail_id");
        $check_data = array(
            "wb_mail_key" => $wb_mail_key,
        );
        $rules = array(
            "wb_mail_key" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "uniq_key = '".addslashes($wb_mail_key)."'";
        $mail = $this->obj_MailWB->getOne($where, "mail_address");
        $result = $this->obj_MailWB->delete($where);
        $this->add_operation_log('mail_wb_upload_address_delete', array('mail' => $mail));
        return $this->output();
    }

    function action_sort_up() {
        $room_key       = $this->request->get("room_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "room_id" => $room_key,
        );
        $rules = array(
            "room_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->obj_Room->up_sort($user_info["user_key"], $room_key);
        return $this->output();
    }

    function action_sort_down() {
        $room_key       = $this->request->get("room_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "room_id" => $room_key,
        );
        $rules = array(
            "room_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->obj_Room->down_sort($user_info["user_key"], $room_key);
        return $this->output();
    }

}
$main = new N2MY_MeetingAdminRoomAPI();
$main->execute();