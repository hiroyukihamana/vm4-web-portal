<?php
require_once("classes/N2MY_Api.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/room.dbi.php");
require_once("config/config.inc.php");

class N2MY_Customer_API extends N2MY_Api
{
    var $session_id = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
    }

    /**
    * ログイン処理
     *
     * @param number outbound_id スタッフID
     * @return string $output テンプレートを表示
     */
    function action_login()
    {
        $output_type = $this->request->get("output_type");
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $outbound_id = $this->request->get("outbound_id");
        // DB選択
        $rules = array(
            "outbound_id"     => array(
                "required" => true
                ),
        		"lang" => array(
        		    "allow" => array_keys($this->get_language_list()),
        		),
        		"country"     => array(
        		    "allow" => array_keys($this->get_country_list()),
        		),
        		API_TIME_ZONE => array(
        		    "allow" => array_keys($this->get_timezone_list()),
        		),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),	
        );
        $request = $this->request->getAll();
        $err_obj = $this->error_check($request, $rules);

        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
        	$obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        	if ( ! $server_info = $obj_MGMClass->getRelationDsn( $outbound_id, "outbound" ) ){
        		$err_obj->set_error("", "SELECT_USER_ERROR", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR");
        	}
        	$obj_Room = new RoomTable( $server_info["dsn"] );
        	$where = "outbound_id = '".$outbound_id."'".
        			" AND room_status != -1";
        	$room_info = $obj_Room->getRow($where);
        	require_once("classes/N2MY_Account.class.php");
        	$obj_N2MY_Account = new N2MY_Account($server_info["dsn"] );
        	$room_info = $obj_N2MY_Account->getRoomInfo( $room_info["room_key"] );
        	$this->logger2->info($room_info);
        	//memberとのリレーションチェック
        	require_once("classes/dbi/member_room_relation.dbi.php");
        	$objRoomRelation = new MemberRoomRelationTable($server_info["dsn"] );
        	$where = "room_key = '".addslashes($room_info["room_info"]["room_key"])."'";
        	$room_relation = $objRoomRelation->getRow($where);
        	if ($room_info && $room_relation) {
        		$obj_Member = new MemberTable( $server_info["dsn"]  );
        		$where = "outbound_id = '".addslashes($outbound_id)."'".
        				" AND member_key = ".$room_relation["member_key"];
        		$member_info = $obj_Member->getRow($where);
        		//$this->logger->debug("room_info",__FILE__,__LINE__,$room_info);
        		if($member_info){
        			$addition = unserialize($room_info["room_info"]["addition"]);
        			$room_info["room_info"]["flicli"] = $addition["flicli"];
        			$room_info["room_info"]["email"] = $addition["email"];
        			$room_info["room_info"]["title"] = $addition["title"];
        			$room_info["room_info"]["room_image"] = $addition["room_image"];
        			$room_info["room_info"]["outbound"] = $member_info["outbound_id"];
        		}else{
        			return $this->display_error("1", "Login failed /Invalid Outbound ID");
        		}
        	}else{
        		return $this->display_error("1", "Login failed /Invalid Outbound ID");
        	}
        	
        	// セッションスタート
        	$session = EZSession::getInstance();
        	$session_id = session_id();
        	$session->set("login", "1");
            if  ($output_type) {
                $session->set("output_type", $output_type);
            }
            
            $country_key = $this->request->get("country", "jp");
            if ($country_key == "auto") {
            	$result_country_key = $this->_get_country_key($country_key);
            	$country_key = $result_country_key;
            }
            // 地域が特定できない場合は利用可能な地域をデフォルトとする
            $country_list = $this->get_country_list();
             
            if (!array_key_exists($country_key, $country_list)) {
            	$_country_data = array_shift($country_list);
            	$country_key = $_country_data["country_key"];
            }
            $session->set("country_key", $country_key);
            
            $session->set("login_customer", "1");
            $session->set("lang", $lang);
            $session->set("country_key", $country_key);
            $session->set("time_zone", $time_zone);
            $session->set('room_info', $room_info);
            $session->set("dsn_key", $server_info["host_name"]);
            $session->set("server_info", $server_info);
            // ユーザー情報
            $session->set("login_user_type", "customer");
            // 部屋情報
            // ユーザー情報整形
            $profile =  array (
                "room_id"    => $room_info["room_info"]["room_key"],
                "room_name"  => $room_info["room_info"]["room_name"],
                "tag"        => $room_info["room_info"]["flicli"],
                "image_path" => $room_info["room_info"]["room_image"],
                "email"      => $room_info["room_info"]["email"],
                "title"      => $room_info["room_info"]["title"],
                "comment"    => $room_info["room_info"]["comment"],
            );
           //$room_data[$room_info["room_key"]] = $room_info;
            // 戻り値
            $data = array(
                "session" => $session_id,
                "profile" => $profile,
                );
            $this->session = $session;
            $this->logger2->info($this->session);
            return $this->output($data);
        }
    }
}
$main = new N2MY_Customer_API();
$main->execute();