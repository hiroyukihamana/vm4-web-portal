<?php
/*
 * Created on 2008/01/31
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");

class N2MY_Customer_Meeting_API extends N2MY_Api
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        $this->_name_space = md5(__FILE__);
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorizationCustomer();
    }

    /**
     * 会議開始（会議を作成して、そのまま開始）
     *
     * @param
     * @return 会議データを作成し、リダイレクトで会議室に入室
     */
    function action_start() {
        require_once("classes/N2MY_Meeting.class.php");
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $meeting_key = $this->request->get(API_MEETING_ID);
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());

        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
            //    "allow" => array_keys($this->session->get("room_info")),
                ),
            "flash_version" => array(
                "allow" => array("as2", "as3"),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $room_info = $this->session->get("room_info");
        $request["room_id"] = $room_info["room_info"]["room_key"];
        $room_key = $request["room_id"];
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // ログインタイプ取得
        $login_type = $this->session->get("login_type")?$this->session->get("login_type"):"customer";
        // ユーザ情報
        $this->logger2->info( $this->session->get("room_info") );
        $obj_User = new UserTable($this->get_dsn());
        $user_info = $obj_User->getRow("user_key = ".$room_info["room_info"]["user_key"]);
        $this->session->set("user_info", $user_info);

        //資料共有部屋か確認
        if ($room_info["options"]["whiteboard"]) {
        	$login_type = "whiteboard_customer";
        }

        //地域情報
        $country_key = $this->session->get("country_key");
        /*
        if ($country_key == "auto") {
        	$result_country_key = $this->_get_country_key($country_key);
        	$country_key = $result_country_key;
        }

        if (!$country_key) {
        	$country_key = $this->session->get("country_key");
        }
        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();

        if (!array_key_exists($country_key, $country_list)) {
        	$_country_data = array_shift($country_list);
        	$country_key = $_country_data["country_key"];
        }
        $this->session->set("country_key", $country_key);*/
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        // 会議の指定が有る場合
        if ($meeting_key) {
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND meeting_session_id = '".addslashes($meeting_key)."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            $this->logger2->info($meeting_info);
        }

        //#li#meeting_info 二回目取ります？

        // 会議情報取得
        if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_key)) {
            $err_obj->set_error(0, "MEETING_INVALID");
        }
        $this->logger2->info($meeting_info);
        // チェック判定
        $meeting_key = $meeting_info["meeting_key"];
        $obj_CoreMeeting = new Core_Meeting( $this->get_dsn() );
        $meeting_status = $obj_CoreMeeting->getMeetingStatus( array( $room_key => $meeting_key) );
        if ("0" == $meeting_status[0]["pcount"]) {
           $err_msg["errors"][] = array(
                "field"   => "inbound_id",
                "err_cd"  => "meeting",
                "err_msg" => "No staff in the room.",
                );
                return $this->display_error("1", ROOM_STATUS_ERROR, $err_msg);
        } else {
            $room_status["normal"] = 0;
            $room_status["customer"] = 0;
            $room_status["manager"] = 0;
            foreach($meeting_status[0]["participants"] as $participant) {
                switch ($participant["participant_type_name"]) {
                case "staff":
                    $room_status["normal"]++;
                    break;
                case "customer":
                    $room_status["customer"]++;
                    break;
                }
            }
            if ("1" == $room_status["normal"] && "0" == $room_status["customer"]) {
                // 地域が特定できない場合は利用可能な地域をデフォルトとする
                // オプション指定
                $options = array(
                    "meeting_name" => $meeting_info["meeting_name"],
                    "user_key"     => $user_info["user_key"],
                    "start_time"   => $meeting_info["start_time"],
                    "end_time"     => $meeting_info["end_time"],
                    "country_id"   => $country_key,
                    "password"     => $meeting_info["meeting_password"]
                );
                // 現在の部屋のオプションで会議更新
                $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);
                // ユーザごとのオプションを取得
                $type        = $this->request->get("type", "customer");
                //ユーザーがトライアルである場合
                $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
                // ホワイトボードアップロードメールアドレス
                $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";
                $user_options = array(
                    "user_key"                  => $user_info["user_key"],
                    "name"                => $this->request->get("name", ""),
                    "participant_email"   => $member_info["member_email"],
                    "participant_station" => $this->request->get("station", $member_info["member_station"]),
                    "narrow"              => $this->request->get("is_narrow"),
                    "lang"                => $this->session->get("lang"),
                    "skin_type"           => $this->request->get("skin_type", ""),
                    "mode"                => $mode,
                    "role"                => $login_type,
                    "mail_upload_address" => $mail_upload_address,
                    "account_model"       => $user_info["account_model"],
                    );
                // 確認
                $tag = $this->request->get("tag");
                if($tag){
                    $user_options["participant_tag"] = $tag;
                }
                if(mb_strlen($tag) > 255){
                    $err_msg["errors"][] = array(
                            "field"   => "tag",
                            "err_cd"   => "maxlen",
                            "err_msg" => "The tag max length is 255.",
                    );
                    $err_obj->set_error("tag","tag error" ,$err_msg);
                    return $this->display_error(100, "PARAMETER_ERROR",$err_msg);
                }
                // 会議入室
                $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);
                //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
                $this->session->set( "room_key", $room_key );
                $this->session->set( "type", $meetingDetail["participant_type_name"] );
                $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
                $this->session->set( "participant_key", $meetingDetail["participant_key"] );
                $this->session->set( "mail_upload_address", $mail_upload_address );
                $this->session->set( "fl_ver", $this->request->get("flash_version", "as3") );
                $this->session->set( "meeting_version", $user_info["meeting_version"] );
                $this->session->set( "display_size", "16to9");
                $this->session->set( "user_agent_ticket", uniqid());
                $this->session->set("login_type", 'customer');
                $this->session->set("service_mode", 'sales');
                $this->session->set( "reload_type", 'normal');
                $this->session->set( "meeting_type", 'customer');
                // 会議入室
                $url = N2MY_BASE_URL."outbound/?action_meeting_display=&".N2MY_SESS_NAME."=".session_id();
                if ($room_info["room_info"]["room_password"]) {
                    $this->session->set("redirect_url", $url, session_id());
                    $url = N2MY_BASE_URL."outbound/" .
                        "?action_room_auth_login=" .
                        "&".N2MY_SESS_NAME."=".session_id().
                        "&room_key=".$room_key.
                        "&ns=".session_id();
                }
                if($meetingDetail['meeting_key']){
                    $_meetingDetail = $obj_Meeting->getRow('meeting_key = ' . $meetingDetail['meeting_key']);
                }
                $data = array(
                    "url"      => $url,
                    "meeting_id"    => $_meetingDetail["meeting_session_id"],
                );
                return $this->output($data);
            } else {
                $err_msg["errors"][] = array(
                    "field"   => "inbound_id",
                    "err_cd"  => "meeting",
                    "err_msg" => "Room is busy now.",
                );
                return $this->display_error("1", ROOM_STATUS_ERROR, $err_msg);
            }
        }
    }
    
    public function action_start_mobile(){
        require_once("classes/N2MY_Meeting.class.php");
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $meeting_key = $this->request->get(API_MEETING_ID);
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());

        $this->logger2->info($this->session);

        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
            //    "allow" => array_keys($this->session->get("room_info")),
                ),
            "flash_version" => array(
                "allow" => array("as2", "as3"),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $room_info = $this->session->get("room_info");
        $request["room_id"] = $room_info["room_info"]["room_key"];
        $room_key = $request["room_id"];
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // ログインタイプ取得
        $login_type = $this->session->get("login_type")?$this->session->get("login_type"):"customer";
        // ユーザ情報
        $this->logger2->info( $this->session->get("room_info") );
        $obj_User = new UserTable($this->get_dsn());
        $user_info = $obj_User->getRow("user_key = ".$room_info["room_info"]["user_key"]);
        $this->session->set("user_info", $user_info);

        //資料共有部屋か確認
        if ($room_info["options"]["whiteboard"]) {
        	$login_type = "whiteboard_customer";
        }

        //地域情報
        $country_key = $this->session->get("country_key");
        /*
        if ($country_key == "auto") {
        	$result_country_key = $this->_get_country_key($country_key);
        	$country_key = $result_country_key;
        }

        if (!$country_key) {
        	$country_key = $this->session->get("country_key");
        }
        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();

        if (!array_key_exists($country_key, $country_list)) {
        	$_country_data = array_shift($country_list);
        	$country_key = $_country_data["country_key"];
        }
        $this->session->set("country_key", $country_key);*/

        // 会議情報取得
        if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_key)) {
            $err_obj->set_error(0, "MEETING_INVALID");
        }
        $this->logger2->info($meeting_info);
        // チェック判定
        $meeting_key = $meeting_info["meeting_key"];
        $obj_CoreMeeting = new Core_Meeting( $this->get_dsn() );
        $meeting_status = $obj_CoreMeeting->getMeetingStatus( array( $room_key => $meeting_key) );
        $this->logger2->info($meeting_status);
        if ("0" == $meeting_status[0]["pcount"]) {
           $err_msg["errors"][] = array(
                "field"   => "inbound_id",
                "err_cd"  => "meeting",
                "err_msg" => "No staff in the room.",
                );
                return $this->display_error("1", ROOM_STATUS_ERROR, $err_msg);
        } else {
            $room_status["normal"] = 0;
            $room_status["customer"] = 0;
            $room_status["manager"] = 0;
            foreach($meeting_status[0]["participants"] as $participant) {
                switch ($participant["participant_type_name"]) {
                case "staff":
                    $room_status["normal"]++;
                    break;
                case "customer":
                    $room_status["customer"]++;
                    break;
                }
            }
            if ("1" == $room_status["normal"] && "0" == $room_status["customer"]) {
                // 地域が特定できない場合は利用可能な地域をデフォルトとする
                // オプション指定
                $options = array(
                    "meeting_name" => $meeting_info["meeting_name"],
                    "user_key"     => $user_info["user_key"],
                    "start_time"   => $meeting_info["start_time"],
                    "end_time"     => $meeting_info["end_time"],
                    "country_id"   => $country_key,
                    "password"     => $meeting_info["meeting_password"]
                );
                // 現在の部屋のオプションで会議更新
                $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);

                // ユーザごとのオプションを取得
                $type        = $this->request->get("type", "customer");
                //ユーザーがトライアルである場合
                $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";

                // ホワイトボードアップロードメールアドレス
                $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";
                $user_options = array(
                    "user_key"                  => $user_info["user_key"],
                    "name"                => $this->request->get("name", ""),
                    "participant_email"   => $member_info["member_email"],
                    "participant_station" => $this->request->get("station", $member_info["member_station"]),
                    "narrow"              => $this->request->get("is_narrow"),
                    "lang"                => $this->session->get("lang"),
                    "skin_type"           => $this->request->get("skin_type", ""),
                    "mode"                => $mode,
                    "role"                => $login_type,
                    "mail_upload_address" => $mail_upload_address,
                    "account_model"       => $user_info["account_model"],
                    );
                // 確認
                $tag = $this->request->get("tag");
                if($tag){
                    $user_options["participant_tag"] = $tag;
                }

                if(mb_strlen($tag) > 255){

                    $err_msg["errors"][] = array(
                            "field"   => "tag",
                            "err_cd"   => "maxlen",
                            "err_msg" => "The tag max length is 255.",
                    );
                    $err_obj->set_error("tag","tag error" ,$err_msg);
                    return $this->display_error(100, "PARAMETER_ERROR",$err_msg);
                }

                //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
                $this->session->set( "room_key", $room_key );
                $this->session->set( "mail_upload_address", $mail_upload_address );
                $this->session->set( "fl_ver", $this->request->get("flash_version", "as3") );
                $this->session->set( "meeting_version", $user_info["meeting_version"] );
                $this->session->set( "display_size", "16to9");
                $this->session->set( "user_agent_ticket", uniqid());
                $this->session->set("login_type", 'customer');
                $this->session->set("service_mode", 'sales');
                $this->session->set( "reload_type", 'normal');
                $this->session->set( "meeting_type", 'customer');
                // 会議入室
                
                require_once( "classes/dbi/meeting.dbi.php" );
                $obj_Meetng = new MeetingTable($this->get_dsn());
                $where = "meeting_ticket = '" . $meeting_key . "'" .
                		" AND user_key = '".$user_info["user_key"]."'" .
                		" AND is_active = 1" .
                		" AND is_deleted = 0";
                $meeting_data = $obj_Meetng->getRow($where);
                $this->logger2->debug($where);
                $outbound_url = N2MY_BASE_URL."outbound/".$room_info["room_info"]["outbound_id"];
                $url = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://join?session=".session_id()."&room_id=".$meeting_data["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meeting_data['pin_cd']."&callbackurl=".urlencode($outbound_url);
                $data = array(
                		"url"      => $url,
                		"meeting_id"    => $meetingDetail["meeting_key"],
                );
                return $this->output($data);
            } else {
                $err_msg["errors"][] = array(
                "field"   => "inbound_id",
                "err_cd"  => "meeting",
                "err_msg" => "Room is busy now.",
                );
                return $this->display_error("1", ROOM_STATUS_ERROR, $err_msg);
            }
        }
    }

    function action_meeting_display() {
        $type = $this->session->get( "type" );
        $this->template->assign('charset', "UTF-8" );
        $this->template->assign('locale' , $this->session->get("lang") );
        $this->display( sprintf( 'core/%s/meeting_base.t.html', $type ) );
    }

    function action_send_message() {
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
                "allow" => array_keys($this->session->get("room_info")),
                ),
            "company" => array(
                "required" => true,
                "maxlen"   => 30,
                ),
            "email" => array(
                "required" => true,
                "email"    => true,
                ),
            "name" => array(
                "required" => true,
                "maxlen"   => 20,
                ),
            "message" => array(
                "required" => true,
                "maxlen"   => 200,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $room_info = $this->session->get("room_info");

        //メール送信
        if ($room_info[$room_key]["email"] && $this->config->get("N2MY", "message_mail") != 1) {
            require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
            $mail = new EZSmtp(null, "ja", "UTF-8");
            $lang = $this->session->get("lang", "ja");

            $body = $this->fetch( sprintf( '%s/mail/vc/message.t.txt', $lang));
            $mail->setFrom(USER_ASK_FROM, $request['name']);
            $mail->setSubject(OUTBOUND_MESSAGE_MAIL_SUBJECT);
            $mail->setReturnPath($request['email']);
            $mail->setTo($room_info["email"]);
            $mail->setBody($body);
            $mail->send();
        }

        //DBに情報を追加
        $message_data = array();
        $message_data['user_key']         = $room_info[$room_key]['user_key'];
        $message_data['room_key']         = $room_info[$room_key]['room_key'];
        $message_data['customer_name']    = $request['name'];
        $message_data['customer_company'] = $request['company'];
        $message_data['customer_email']   = $request['email'];
        $message_data['customer_comment'] = $request['message'];
        $message_data['customer_ip']      = $_SERVER[REMOTE_ADDR];
        $message_data['status']           = 1;
        $this->logger->trace("message_data",__FILE__,__LINE__,$message_data);
        require_once ("classes/dbi/customer_message.dbi.php");
        $obj_CustomerMessage = new CustomerMessageTable($this->get_dsn());
        $obj_CustomerMessage->add($message_data);

        $data = array();
        return $this->output($data);

    }

}
$main = new N2MY_Customer_Meeting_API();
$main->execute();
