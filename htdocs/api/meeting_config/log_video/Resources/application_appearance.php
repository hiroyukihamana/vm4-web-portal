<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// | 株式会社ブイキューブ                                                 |
// | N2MY_CORE API                                                        |
// |                                                                      |
// |【画面ＩＤ】                                                          |
// |【画面名称】                                                          |
// |                                                                      |
// +----------------------------------------------------------------------+
// | OS     Red Hat Enterprise Linux AS release 3 (Taroon)                |
// | Apache version Apache/1.3.34 (Unix)                                  |
// | MySQL  version 4.0.22                                                |
// | PHP    version 4.3.11 (cli)                                          |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 V-cube, Inc.                                      |
// +----------------------------------------------------------------------+
// | Authors: Masayuki IIno <iino@vcube.co.jp>                            |
// |        :                                                             |
// +----------------------------------------------------------------------+
//
// $Id$

require_once('webapp/classes/AppFrame.class.php');

/**
 * 【API_ApplicationAppearance】
 *
 * <pre>
 * サブクラスによるフロントエンドの機能実装。
 * フレームワーククラスを継承するものとする。
 * </pre>
 *
 * @see    EZCore/EZFrame
 * @access public
 * @author Masayuki IIno <iino@vcube.co.jp>
 */
class API_ApplicationAppearance extends AppFrame
{
// ------------------------------------------------------------------------

// {{{ -- vars

    /**
     * テンプレートファイル
     *
     * @access public
     * @var    string
     */
    var $_tpl = 'api/meeting_config/log_video/resources/application_appearance.t.xml';

// }}}

// ------------------------------------------------------------------------

// {{{ -- API_ApplicationAppearance()

    /**
     * コンストラクタ
     *
     * @access public
     * @param
     * @return
     */
    function API_ApplicationAppearance()
    {
        //call super_class' constructor
        parent::AppFrame();
    }

// }}}
// {{{ -- _API_ApplicationAppearance()

    /**
     * デストラクタ
     *
     * @access public
     * @param
     * @return
     */
    function _API_ApplicationAppearance()
    {
    }

// }}}

// ------------------------------------------------------------------------

// {{{ -- init()

    /**
     * 初期化処理
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return
     */
    function init(&$factory)
    {
        //debug trace
        $factory->logger->trace(__CLASS__, __FILE__, __LINE__, __FUNCTION__);
    }

// }}}
// {{{ -- default_view()

    /**
     * デフォルト処理
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return
     */
    function default_view(&$factory)
    {
        //call action
        $result = $this->action_default($factory);
        if (PEAR::isError($result)) {
            return $result;
        }
    }

// }}}

// ------------------------------------------------------------------------

// {{{ -- action_default()

    /**
     * [API] デフォルト
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return
     */
    function action_default(&$factory)
    {
        //call renderer
        $renderer = $this->render_default($factory);
        if (PEAR::isError($renderer)) {
            return $renderer;
        }
    }

// }}}

// ------------------------------------------------------------------------

// {{{ -- render_default()

    /**
     * [renderer] アプリケーション -アピアランス設定情報を提供する
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return
     */
    function render_default(&$factory)
    {
        //debug trace
        $factory->logger->debug(__FUNCTION__." called.", __FILE__, __LINE__);

        //display xml_template
        $factory->tpl->display_xml($this->_tpl);
    }

// }}}

// ------------------------------------------------------------------------
}

$main =& new API_ApplicationAppearance();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
