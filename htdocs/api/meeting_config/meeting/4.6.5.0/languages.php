<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/AppFrame.class.php");

class API_Languages extends AppFrame
{
    function default_view()
    {
        $this->render_default();
    }

    private function render_default() {
        $this->display( "core/api/meeting_config/meeting/resources/languages.t.xml" );
    }
}

$main =& new API_Languages();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
