<?php
require_once "classes/AppFrame.class.php";
require_once "classes/dbi/user.dbi.php";
require_once "classes/dbi/meeting.dbi.php";
require_once "lib/EZLib/EZHTML/EZValidator.class.php";

class AppMeetingLayout extends AppFrame {

    function default_view() {
        $request = $this->request->getAll();
        $this->logger2->debug($request);
        $validator = new EZValidator($request);
        $rules = array(
            "seat" => array(
                "allow" => array('20'),
                ),
            "type" => array(
                "allow" => array('normal', 'audience', 'multicamera', 'multicamera_audience', 'whiteboard', 'whiteboard_audience', 'staff', 'ss_watcher', 'customer', 'whiteboard_staff', 'whiteboard_customer'),
                ),
            "display_size" => array(
                "allow" => array('4to3', '16to9'),
                ),
        );
        foreach($rules as $field => $rule) {
            $validator->check($field, $rule);
        }
        // とりあえず警告
        if (EZValidator::isError($validator)) {
            $this->logger2->error($this->get_error_info($validator));
            return false;
        }
        $obj_User       = new UserTable($this->get_dsn());
        $obj_Meeting    = new MeetingTable($this->get_dsn());
        $meeting_key    = $this->session->get('meeting_key');
        $meeting_info   = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted = 0", $meeting_key ) );
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $room_info      = $obj_N2MY_Account->getRoomInfo($meeting_info["room_key"]);
        $user_info      = $obj_User->getRow("user_key = '".$meeting_info["user_key"]."'");
        if($meeting_info["is_reserved"]) {
            //予約会議情報
            $this->logger2->info($meeting_info["meeting_ticket"]);
            require_once('classes/dbi/reservation.dbi.php');
            $objReservation = new ReservationTable($this->get_dsn());
            $reservationInfo = $objReservation->getRow(sprintf("meeting_key='%s'", $meeting_info["meeting_ticket"]));
            $customer_sharing_button_hide_status = $reservationInfo["is_customer_desktop_share_flg"];
        } else {
            $customer_sharing_button_hide_status = $room_info["room_info"]["customer_sharing_button_hide_status"];
        }
        $active_speaker = "";
        if($request["type"] == "normal" && ($room_info["room_info"]["active_speaker_mode_only_flg"] || $room_info["room_info"]["active_speaker_mode_use_flg"])){
            $active_speaker = "ActiveSpeaker";
        }

        $this->logger2->debug(array($user_info, $room_info, $meeting_info));
        $this->template->assign('user_info', $user_info);
        $this->template->assign('room_info', $room_info);
        $this->template->assign('meeting_info', $meeting_info);
        $this->template->assign('customer_sharing_button_hide_status', $customer_sharing_button_hide_status);
        $this->display("core/meeting/layout/".$request["type"]."/layout".$request["seat"]."_".$request["display_size"].$active_speaker.".t.xml");
    }
}
$main = new AppMeetingLayout();
$main->execute();
?>
