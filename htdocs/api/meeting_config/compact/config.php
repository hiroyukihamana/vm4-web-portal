<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once('classes/core/dbi/DataCenter.dbi.php');
require_once('classes/core/dbi/Document.dbi.php');
require_once('classes/core/dbi/Meeting.dbi.php');
require_once('classes/core/dbi/MeetingOptions.dbi.php');
require_once('classes/core/dbi/MeetingSequence.dbi.php');
require_once('classes/core/dbi/Participant.dbi.php');
require_once('classes/core/dbi/Options.dbi.php');
require_once('classes/dbi/reservation.dbi.php');
require_once('classes/dbi/room.dbi.php');
require_once('classes/mgm/dbi/FmsServer.dbi.php');
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Document.class.php");
require_once "lib/EZLib/EZUtil/EZMath.class.php";

class API_Config extends AppFrame
{
    function init() {
        $this->dsn = $this->get_dsn();
    }

    function default_view() {
        $this->render_default();
    }

    function render_default() {
        $obj_Datacenter      = new DBI_DataCenter( N2MY_MDB_DSN );
        $obj_Document        = new DBI_Document( $this->dsn );
        $obj_Meeting         = new DBI_Meeting( $this->dsn );
        $obj_MeetingSequence = new DBI_MeetingSequence( $this->dsn );
        $obj_Participant     = new DBI_Participant( $this->dsn );
        $obj_FmsServer       = new DBI_FmsServer( N2MY_MDB_DSN );
        $obj_MeetingOptions  = new DBI_MeetingOptions( $this->dsn );
        $obj_Options         = new DBI_Options( N2MY_MDB_DSN );
        $obj_Room            = new RoomTable( $this->dsn );

        $session         = $this->session->getAll();
        $meeting_key     = $session["meeting_key"];
        $participant_key = $session["participant_key"];

        if (!$meeting_key || !$participant_key) {
            $this->logger->error("missing parameters.", __FILE__, __LINE__);
            return;
        }

        $config_info      = $this->config->getAll('CONFIG_HOST');
        $meeting_info     = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", $meeting_key ) );
        $country = $this->session->get("country_key");
        if (!$meeting_info["intra_fms"]) {
            $datacenter_info = $obj_Datacenter->getRowsAssoc(null, array("datacenter_key" => "ASC"));
            $server_info      = $obj_FmsServer->getRow(sprintf( "server_key='%s'", $meeting_info["server_key"]));
            // 再生時にFMSに接続できない場合
            if ($server_info["server_key"] > 1000 && $country != "cn" ) {
                $fms_address = $server_info["server_address"];
            } else {
                $fms_address = $server_info["local_address"] ? $server_info["local_address"] : $server_info["server_address"];
            }
            $this->logger2->info($participant_key." : ".$fms_address);
            if (!$obj_FmsServer->isActive($fms_address)) {
                $nowParticipantNumber = $obj_Participant->numRows('meeting_key = '.$meeting_info["meeting_key"].' AND is_active = 1');
                $this->logger2->info($nowParticipantNumber, '現在の参加人数');
                if ($nowParticipantNumber == 0) {
                    $this->logger2->warn($server_info, "FMS接続障害(自動切換え)");
                    require_once("classes/dbi/fms_deny.dbi.php");
                    try {
                        $objFmsDeny = new FmsDenyTable( $this->dsn );
                        $_fms_deny_keys = $objFmsDeny->findByUserKey($meeting_info["user_key"], null, null, 0, "fms_server_key");
                    } catch (Exception $e) {
                        $this->logger2->warn("自動切換え失敗");
                        return;
                    }
                    $fms_deny_keys = array();
                    if ($_fms_deny_keys) {
                        foreach ($_fms_deny_keys as $fms_key) {
                            $fms_deny_keys[] = $fms_key["fms_server_key"];
                        }
                    }
                    if ($_server_key = $obj_FmsServer->getKeyByDatacenterKey($server_info["datacenter_key"], null, null, false, $fms_deny_keys)) {
                        require_once("classes/core/Core_Meeting.class.php");
                        $obj_CoreMeeting = new Core_Meeting($this->dsn, N2MY_MDB_DSN );
                        $obj_CoreMeeting->changeFMS($meeting_key, $_server_key);
                        $server_key = $_server_key;
                        $server_info = $obj_FmsServer->getRow( sprintf( "server_key='%s'", $_server_key ));
                        $this->logger2->info("自動切換え成功");
                    } else {
                        $this->logger2->warn("自動切換え失敗");
                    }
                }
            }
        } else {
            require_once("classes/dbi/user_fms_server.dbi.php");
            $objUserFmsServer       = new UserFmsServerTable($this->dsn);
            $server_info = $objUserFmsServer->getRow(sprintf("fms_key='%s'", $meeting_info['server_key']));
            $server_info["datacenter_key"] = $server_info["fms_key"];
            $datacenter_info[] = array(
                                "datacenter_key"=>$server_info["fms_key"],
                                "datacenter_name_ja"=>$server_info["server_address"],
                                "datacenter_name_en"=>$server_info["server_address"]);
        }
        $participant_info = $obj_Participant->getDetail( sprintf( "participant_key='%s'", $participant_key ) );
        $server_key       = $meeting_info["server_key"];
        $sequence_info    = $obj_MeetingSequence->getRow(sprintf( "meeting_key='%s'", $meeting_key ), null, array( "meeting_sequence_key" => "desc"));

        $file_info = array();
        $obj_N2MYDocument = new N2MY_Document($this->dsn);
        $where = "meeting_key = ".$meeting_key.
            " AND document_mode = 'pre'".
            " AND document_status = '".DOCUMENT_STATUS_SUCCESS."'";
        $document_list = $obj_N2MYDocument->getList($where);
        $this->logger2->debug($document_list);

        //set values for permission
        switch ($participant_info['participant_type_name']) {
            case 'normal':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x110";
                $participant_info['participant_permission_user'] = "0x000000000000003FFFFFFFFFFFFFFFFF";
                break;
            case 'audience':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x120";
                $participant_info['participant_permission_user'] = "0x0000000000000000000000000DB3C020";
                break;
            case 'document':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x120";
                $participant_info['participant_permission_user'] = "0x00000000000000000000000000130020";
                break;
            case 'phone':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x310";
                $participant_info['participant_permission_user'] = "";
                break;
            case 'h323':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x320";
                $participant_info['participant_permission_user'] = "";
                break;
            case 'compact':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x110";
                $participant_info['participant_permission_user'] = "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                break;
            case 'invisible_wb':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x150";
                $participant_info['participant_permission_user'] = "";
                break;
            default:
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x110";
                $participant_info['participant_permission_user'] = "0x000000000000003FFFFFFFFFFFFFFFFF";
                break;
        }
        switch ($participant_info['participant_mode_name']) {
            case 'trial':
                $participant_info['is_trial']             = 'true';
                $participant_info['use_welcome_message']  = 'true';
                break;
            default:
                $participant_info['is_trial']             = 'false';
                $participant_info['use_welcome_message']  = 'false';
                break;
        }

        switch ($participant_info['participant_role_name']) {
            case 'invite':
                $participant_info['is_guest']     = 'true';
                if($participant_info['use_welcome_message'] == 'true'){
                    $participant_info['use_welcome_message']  = 'true';
                }else{
                    $participant_info['use_welcome_message']  = 'false';
                }
                break;
            default:
                $participant_info['is_guest']             = 'false';
                if($participant_info['use_welcome_message'] == 'true'){
                    $participant_info['use_welcome_message']  = 'true';
                }else{
                    $participant_info['use_welcome_message']  = 'false';
                }
                break;
        }

        //create values for meeting_reservation
        $meeting_start_datetime = null;
        if (isset($meeting_info['meeting_start_datetime']) && $meeting_info['meeting_start_datetime']) {
            $meeting_info['meeting_start_datetime'] = strtotime($meeting_info['meeting_start_datetime']);
        }

        $meeting_stop_datetime = null;
        if (isset($meeting_info['meeting_stop_datetime'])  && $meeting_info['meeting_stop_datetime']) {
            $meeting_info['meeting_stop_datetime']  = strtotime($meeting_info['meeting_stop_datetime']);
        }

        //sharing
        $shringSring = "";
        //ignore or intra利用時は利用不可能
        $shringSring .= $this->config->get('IGNORE_MENU','tool_sharing2_app') || 1 == $meeting_info["intra_fms"] ? 0 : 1;
        $shringSring .= $this->config->get('IGNORE_MENU','tool_sharing3_app') ? 0 : 1;
        $this->template->assign("availableSharingVersion", $shringSring);

        //set additional values
        $meeting_info['meeting_publish_id']       = substr(md5(uniqid(rand(), true)), 1, 16);

        // one time for voc
        $oneTime = array(
                    "certificate"    =>    $meeting_info['meeting_publish_id'],
                    "createtime"    =>    time());
        $this->session->set("oneTime", $oneTime);

        $app_name = $this->config->get('CORE', 'app_name', 'Vrms');
        $meeting_info['meeting_application_name'] = $app_name;
        if (isset($meeting_info['meeting_log_password']) && $meeting_info['meeting_log_password']) {
            $meeting_info['meeting_log_password'] = 'true';
        } else {
            $meeting_info['meeting_log_password'] = 'false';
        }

        if (isset($meeting_info['is_reserved'])   && $meeting_info['is_reserved']   == 1) {
            $meeting_info['is_reserved']   = 'true';
        } else {
            $meeting_info['is_reserved']   = 'false';
        }

        if (isset($participant_info['is_narrowband']) && $participant_info['is_narrowband'] == 1) {
            $participant_info['is_narrowband'] = 'true';
        } else {
            $participant_info['is_narrowband'] = 'false';
        }

        // オプション情報
        $option_list = $obj_Options->getList();
        $where = "meeting_key = ".$meeting_key;
        $meeting_option_list = $obj_MeetingOptions->getRowsAssoc($where);
        foreach($meeting_option_list as $key => $row) {
            $option_name = $option_list[$row["option_key"]]["option_name"];
            $meeting_option[$option_name] = $row["meeting_option_quantity"];
        }

        // ユーザー情報取得
        $_user_info = $this->session->get("user_info");
        require_once 'classes/dbi/user.dbi.php';
        $objUser = new UserTable($this->get_dsn());
        $where = "user_key = ". $_user_info["user_key"];
        $user_info = $objUser->getRow($where);
        // 部屋の情報取得
        $where = "room_key = '". addslashes($meeting_info["room_key"])."'";
        $room_info = $obj_Room->getRow($where);
        // 予約会議だった場合パスワードの有無をチェック
        if ("true" == $meeting_info["is_reserved"]) {
            $objReservation = new ReservationTable($this->dsn);
            $reservationInfo = $objReservation->getRow(sprintf("meeting_key='%s'", $meeting_info["meeting_ticket"]));
            $this->logger2->info($reservationInfo);
            $this->template->assign("reservation_info", $reservationInfo);
            $this->template->assign("reservationPassword", $reservationInfo["reservation_pw"]);
            //マルチカメラ
            $meeting_option["multicamera"] = $reservationInfo["is_multicamera"];
            // 機能制限
            if ($reservationInfo["is_limited_function"]) {
                //音声通話
                if (!$reservationInfo["is_telephone"]) $meeting_option["telephone"] = 0;
                //H.323
                if (!$reservationInfo["is_h323"]) $meeting_option["h323"] = 0;
                //携帯
                if (!$reservationInfo["is_mobile_phone"]) $meeting_option["mobile"] = 0;
                //高画質
                if (!$reservationInfo["is_high_quality"]) $meeting_option["hispec"] = 0;
                //共有
                $meeting_option["sharing"] = $reservationInfo["is_desktop_share"];
                //暗号化
                if ($meeting_option["ssl"] == 1 && $reservationInfo["is_meeting_ssl"] == 0) {
                    // 暗号化で契約した状態で、暗号化を外す
                    $room_info["rtmp_protocol"] = "";
                }
                $meeting_option["ssl"] = $reservationInfo["is_meeting_ssl"];
                //会議室内招待
                if (!$reservationInfo["is_invite_flg"]) {
                    $this->logger2->info($participant_info['participant_permission_user']);
                    $mask = "0xFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission & $mask_bin);
                    $this->logger2->info($participant_info['participant_permission_user']);
                }
                //録画
                if (!$reservationInfo["is_rec_flg"]) {
                    $this->logger2->info($participant_info['participant_permission_user']);
                    $mask = "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission & $mask_bin);
                    $this->logger2->info($participant_info['participant_permission_user']);
                }
                //キャビネット
                $room_info["cabinet"] = $reservationInfo["is_cabinet_flg"];
                // ホワイトボードページ番号
                //$room_info["is_wb_no"] = $reservationInfo["is_wb_no_flg"];
            } else if ($meeting_option["multicamera"]) {
                //マルチカメラ時排他制御
                $meeting_option["telephone"] = "0";
                $meeting_option["smartphone"] = "0";
                $meeting_option["h323"] = "0";
                $meeting_option["mobile"] = "0";
            }
        } else if ($meeting_option["multicamera"]) {
            //マルチカメラ時排他制御
            $meeting_option["telephone"] = "0";
            $meeting_option["smartphone"] = "0";
            $meeting_option["h323"] = "0";
            $meeting_option["mobile"] = "0";
        }
        if ($this->session->get("display_size") == "16to9") {
            $room_info["default_layout"] = $room_info["default_layout_16to9"];
        } else {
            $room_info["default_layout"] = $room_info["default_layout_4to3"];
        }
        // キャビネット無効化
        if ($room_info["cabinet"] == "0") {
            $this->logger2->info($participant_info['participant_permission_user']);
            $mask = "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFF";
            $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
            $mask_bin = EZMath::hexbin($mask);
            $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission & $mask_bin);
            $this->logger2->info($participant_info['participant_permission_user']);
        }
        $participant_info['reload_type'] = $this->session->get('reload_type');

        if ($room_info["cabinet_filetype"]) {
            $cabinet_filetype = @unserialize($room_info["cabinet_filetype"]);
            if (is_array($cabinet_filetype)) {
                $room_info["cabinet_filetype"] = join(",", $cabinet_filetype);
            } else {
                $room_info["cabinet_filetype"] = "";
            }
        }
        // 回線速度設定
        $netspeed_check = $this->get_message("NET_SPEED_LIST", $room_info["is_netspeed_check"]);
        $room_info["is_netspeed_check"] = ($netspeed_check) ? substr($netspeed_check, 0, strpos($netspeed_check, ":")) : "";
        // 自動トランシーバー切り替え人数
        if ($room_info["is_auto_transceiver"]) {
            if (!$room_info["transceiver_number"]) $room_info["transceiver_number"] = 11;
        } else {
            $room_info["transceiver_number"] = 100;
        }
        $room_info["is_auto_transceiver"] = false;
        //assign values
//        $this->template->assign("c" , $config_info);
        $this->template->assign("d" , $datacenter_info);
        $this->template->assign("document_list" , $document_list);
        $this->template->assign("m" , $meeting_info);
        $this->template->assign("mo", $meeting_option);
        $this->template->assign("p" , $participant_info);
        $this->template->assign("s" , $server_info);
        $this->template->assign("fms_address" , $fms_address);
        // 対応フォーマット取得
        require_once ("classes/N2MY_Document.class.php");
        $objDocument = new N2MY_Document($this->get_dsn());
        $support_document_list = $objDocument->getSupportFormat($meeting_info["room_key"]);
        $room_info["document_type_list"]    = join(",", $support_document_list['document_ext_list']);
        $room_info["image_type_list"]       = join(",", $support_document_list['image_ext_list']);
        $this->logger2->info(array(
            "document_type_list" => $room_info["document_type_list"],
            "image_type_list"    => $room_info["image_type_list"]
            ));
        $user_info["addition"] = unserialize($user_info["addition"]);
        $this->logger2->debug(array(
            "user_info" => $user_info,
            "room_info" => $room_info,
            "reservation_info" => $reservationInfo
            ));
        $this->template->assign("user_info" , $user_info);
        $this->template->assign("room_info" , $room_info);
        $extension = array();
        $total_seat = $meeting_info["meeting_max_seat"] + $meeting_info["meeting_max_audience"];
        $bandwidth_level = ceil($total_seat / 10);
        $extension["maxRoomBandwidth"] = $bandwidth_level * 2048;
        if ($meeting_option["hispec"] > 0) {
            $hispecBandWidth = $meeting_option["hispec"] * 2048;
        } else {
            $hispecBandWidth = 2048;
        }
        $extension["maxHighQualityRoomBandwidth"] = $extension["maxRoomBandwidth"] + $hispecBandWidth;
        $extension["maxUserBandwidth"] = 256;
        if ($meeting_option["minimumBandwidth80"]) {
            $extension["minUserBandwidth"] = 80;
        } else {
            $extension["minUserBandwidth"] = 64;
        }
        $extension["maxHighQualityUserBandwidth"] = 512;
        //CabinetのPATH
        $dateInfo = date_parse( $meeting_info["create_datetime"] );
        $cabinet_path = sprintf( "%04d%02d/%02d/", $dateInfo["year"], $dateInfo["month"], $dateInfo["day"]);
        $this->template->assign("cabinet_path", $cabinet_path);
        //$this->logger->info(__FUNCTION__,__FILE__,__LINE__,array($meeting_info, $extension));
        // オーディエンス契約数に応じて帯域を変更
        $this->template->assign("extension", $extension);
        //server host key
        $this->template->assign("server_dsn_key", $this->get_dsn_key() );
        //シークエンスキー
        $this->template->assign("meeting_sequence_key", $sequence_info["meeting_sequence_key"]);
        // セッションの値
        $this->template->assign("session", $session);
        // ユーザごとの同時接続ユーザー数制限値取得
        $this->template->assign("user_limit", isset($user_info["meeting_max_connecct"]) ? $user_info["meeting_max_connecct"] : "");

        // HTTP
        if ($_SERVER["SERVER_PORT"] == 80) {
            $http_schema = "http";
            $http_port = "80";
        } elseif ($_SERVER["SERVER_PORT"] == 443) {
            $http_schema = "https";
            $http_port = "443";
        } else {
            $http_schema = "http";
            $http_port = $_SERVER['SERVER_PORT'];
        }
        $base_url = $http_schema."://".$_SERVER["SERVER_NAME"].":".$http_port."/";
        $this->template->assign("base_url", $base_url);

        //会議内からの招待メールのためユーザー、オーディエンス用のハッシュ値を生成
        require_once "classes/N2MY_Invitation.class.php";
        $objInvitation = new N2MY_Invitation($this->dsn);

        require_once "classes/dbi/meeting_invitation.dbi.php";
        $objMeetingInvitationTable = new MeetingInvitationTable($this->dsn);
        if (!$invitationData = $objMeetingInvitationTable->getRow(
                                                            sprintf("meeting_key='%s' AND status = 1", $meeting_key),
                                                            "*",
                                                            array("invitation_key"=>"desc"))) {
            $user_session_id = md5(uniqid(rand(), 1));
            $audience_session_id = md5(uniqid(rand(), 1));
            $invitationData = array(
                                "meeting_key" => $meeting_key,
                                "meeting_session_id" => $meeting_info["meeting_session_id"],
                                "user_session_id" => $user_session_id,
                                "audience_session_id" => $audience_session_id,
                                "status" => 1
                                );
            if ($meeting_info["is_reserved"])
                $invitationData["meeting_ticket"] = $meeting_info["meeting_ticket"];
            $objMeetingInvitationTable->add($invitationData);
        }
        $this->template->assign("typeNormal", $invitationData["user_session_id"]);
        $this->template->assign("typeAudience", $invitationData["audience_session_id"]);

        $telephone_data = array();
        if ($meeting_option["telephone"]) {
            $tel_no = $this->config->getAll("TELEPHONE");
            $location_list = $this->get_message("TELEPHONE");
            foreach($location_list as $key => $location) {
                $telephone_data[$key]["name"] = $location;
                $telephone_data[$key]["list"] = split(",", $tel_no[$key]);
            }
        }
        // echo cancel
        $this->template->assign('enableEchoCancel', !$this->config->get("IGNORE_MENU", "echo_canceler"));
        if (strpos($_SERVER["HTTP_USER_AGENT"], "Windows") !== false) {
             if (strpos($_SERVER["HTTP_USER_AGENT"], "MSIE") !== false) {
                $this->template->assign('enableActiveX', 1);
             }
        }

        $this->template->assign("lang", $this->session->get("lang"));
        // ミーティングID
        $meeting_id = $obj_Meeting->getMeetingID($meeting_key);
        $this->template->assign("meeting_id", $meeting_id);
        $this->template->assign("telephone_data", $telephone_data);
        // highPriorityMicrophoneNames
        $this->template->assign("highPriorityMicrophoneNames", $this->config->get('ECHO_CANCEL', 'highPriorityMicrophoneNames'));
        // excludeEchoCancelNames
        $this->template->assign("excludeEchoCancelNames", $this->config->get('ECHO_CANCEL', 'excludeEchoCancelNames'));
        //display xml_template
        $this->display( sprintf( "core/api/meeting_config/meeting/resources/config_%s.t.xml", $participant_info['participant_type_name'] ) );
    }

}

$main = new API_Config();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
