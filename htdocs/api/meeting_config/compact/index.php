<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/AppFrame.class.php");

class API_LoadConfig extends AppFrame
{
    function default_view() {
        $this->render_default();
    }

    private function render_default(){
        $this->template->assign("pathToLibrary"                     , "/lang/".$this->_lang."/flash/compact/Lib.swf" );
        $this->template->assign("pathToApplicationAppearanceDataXML", "application_appearance.php");
        $this->template->assign("pathToConfigDataXML"               , "config.php");
        $this->template->assign("pathToCoreAppearanceDataXML"       , "core_appearance.php");
        $this->template->assign("pathTolanguageXML"                 , "/lang/".$this->_lang."/flash/language.xml" );
        $this->template->assign("pathToLayoutXML"                   , "layout.php");
        $this->display( "core/api/meeting_config/meeting/project.t.xml" );
    }

}

$main =& new API_LoadConfig();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
