<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_Account.class.php");
require_once("classes/AppFrame.class.php");
require_once("classes/core/dbi/DataCenter.dbi.php");
require_once("classes/core/dbi/Meeting.dbi.php");
require_once("classes/core/dbi/MeetingSequence.dbi.php");
require_once("classes/core/dbi/MeetingOptions.dbi.php");
require_once("classes/mgm/dbi/FmsServer.dbi.php");

class API_Config extends AppFrame
{
    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth() {
        $this->checkAuth();
    }

    function default_view()
    {
        $obj_Datacenter      = new DBI_DataCenter( N2MY_MDB_DSN );
        $obj_Meeting         = new DBI_Meeting( $this->get_dsn() );
        $obj_MeetingSequence = new DBI_MeetingSequence( $this->get_dsn() );
        $obj_MeetingOptions  = new DBI_MeetingOptions( $this->get_dsn() );
        $obj_FmsServer       = new DBI_FmsServer( N2MY_MDB_DSN );
        $session = $this->session->getAll();

        $meeting_key     = $session["meeting_key"];
        $sequence_key = $session["sequence_key"];
        $this->template->assign("sequence_key" , $sequence_key);

        if ( ! $meeting_key ) {
            $this->logger->error("missing parameters.", __FILE__, __LINE__);
            return;
        }

        //get data from object
        $datacenter_list       = $obj_Datacenter->getRowsAssoc( null, array( "datacenter_key" => "ASC" ) );
        $meeting_info          = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", $meeting_key ) );
        $meeting_sequence_info = $obj_MeetingSequence->getRow( sprintf( "meeting_sequence_key='%s'", $sequence_key ), null, array( "meeting_sequence_key" => "desc"));
        $server_key            = $meeting_sequence_info["server_key"];
        if (!$meeting_info["intra_fms"]) {
            $server_info           = $obj_FmsServer->getRow( sprintf( "server_key='%s'", $server_key ));
            // 再生時にFMSに接続できない場合
            $fms_address = $server_info["local_address"] ? $server_info["local_address"] : $server_info["server_address"];
            if (!$obj_FmsServer->isActive($fms_address)) {
                $this->logger2->warn($server_info, "FMS接続障害(自動切換え)");
                require_once("classes/dbi/fms_deny.dbi.php");
                try {
                    $objFmsDeny = new FmsDenyTable( $this->dsn );
                    $_fms_deny_keys = $objFmsDeny->findByUserKey($meeting_info["user_key"], null, null, 0, "fms_server_key");
                } catch (Exception $e) {
                    $this->logger2->warn("自動切換え失敗");
                    return;
                }
                $fms_deny_keys = array();
                if ($_fms_deny_keys) {
                    foreach ($_fms_deny_keys as $fms_key) {
                        $fms_deny_keys[] = $fms_key["fms_server_key"];
                    }
                }
                $server_key  = $obj_FmsServer->getKeyByDatacenterKey($server_info["datacenter_key"], null, null, false, $fms_deny_keys);
                $server_info = $obj_FmsServer->getRow( sprintf( "server_key='%s'", $server_key ));
            }
        } else {
            require_once("classes/dbi/user_fms_server.dbi.php");
            $objUserFmsServer       = new UserFmsServerTable($this->get_dsn());
            $server_info = $objUserFmsServer->getRow(sprintf("fms_key='%s'", $server_key));
        }
        $meeting_options_list = array();
        $m_options = $obj_MeetingOptions->getList( $meeting_key);
        if ($m_options) {
            foreach ($m_options as $val) {
                $option_name  = $val['option_name'];
                $option_value = $val['meeting_option_quantity'];
                if ($option_value) {
                    $option_value = 'true';
                }
                $meeting_options_list[$option_name] = $option_value;
            }
        }

        //create values for meeting_reservation
        $meeting_start_datetime = null;
        if (isset($meeting_info['meeting_start_datetime']) && $meeting_info['meeting_start_datetime']) {
            $meeting_info['meeting_start_datetime'] = strtotime($meeting_info['meeting_start_datetime']);
        }

        $meeting_stop_datetime = null;
        if (isset($meeting_info['meeting_stop_datetime'])  && $meeting_info['meeting_stop_datetime']) {
            $meeting_info['meeting_stop_datetime']  = strtotime($meeting_info['meeting_stop_datetime']);
        }

        //set additional values
        $meeting_info['meeting_publish_id']       = substr(md5(uniqid(rand(), true)), 1, 16);
        $app_name = $this->config->get('ONDEMAND', 'app_name');
        $meeting_info['meeting_application_name'] = $app_name;

        if (isset($meeting_info['meeting_log_password']) && $meeting_info['meeting_log_password']) {
            $meeting_info['meeting_log_password'] = 'true';
        } else {
            $meeting_info['meeting_log_password'] = 'false';
        }

        if (isset($meeting_info['is_reserved'])   && $meeting_info['is_reserved']   == 1) {
            $meeting_info['is_reserved']   = 'true';
        } else {
            $meeting_info['is_reserved']   = 'false';
        }

        //set participant information
        $participant_info = array();
        $participant_info['participant_type']            = "0x110";
        $participant_info['participant_permission_user'] = "0x0000000000000003FFFFFFFFFFFFFFFF";

        // HTTP
        if ($_SERVER["SERVER_PORT"] == 80) {
            $http_schema = "http";
            $http_port = "80";
        } elseif ($_SERVER["SERVER_PORT"] == 443) {
            $http_schema = "https";
            $http_port = "443";
        } else {
            $http_schema = "http";
            $http_port = $_SERVER['SERVER_PORT'];
        }
        // 部屋の情報取得
        require_once('classes/dbi/room.dbi.php');
        $obj_Room = new RoomTable( $this->get_dsn() );
        $where = "room_key = '". addslashes($meeting_info["room_key"])."'";
        $room_info = $obj_Room->getRow($where);
        //CabinetのPATH
        $dateInfo = date_parse( $meeting_info["create_datetime"] );
        $cabinet_path = sprintf( "%04d%02d/%02d/", $dateInfo["year"], $dateInfo["month"], $dateInfo["day"]);
        $this->template->assign("is_ondemand", $this->session->get("ondemand"));
        $this->template->assign("cabinet_path", $cabinet_path);
        $this->template->assign("d" , $datacenter_list);
        $this->template->assign("m" , $meeting_info);
        $this->template->assign("mo", $meeting_options_list);
        $this->template->assign("p" , $participant_info);
        $this->template->assign("s" , $server_info);
        $this->template->assign("server_dsn_key", $this->get_dsn_key() );
        $this->template->assign("room_info" , $room_info);
        // HTTP
        if ($_SERVER["SERVER_PORT"] == 80) {
            $http_schema = "http";
            $http_port = "80";
        } elseif ($_SERVER["SERVER_PORT"] == 443) {
            $http_schema = "https";
            $http_port = "443";
        } else {
            $http_schema = "http";
            $http_port = $_SERVER['SERVER_PORT'];
        }
         // ミーティングID
        $meeting_id = $obj_Meeting->getMeetingID($meeting_key);
        $this->template->assign("meeting_id", $meeting_id);
         // ベースURL
        $base_url = $http_schema."://".$_SERVER["SERVER_NAME"].":".$http_port."/";
        $this->template->assign("base_url", $base_url);
        // 録画GW
        $this->template->assign("record_gw", $this->session->get("record_gw"));
        //display xml_template
        $this->display( "core/api/meeting_config/meeting/resources/config_log_minutes.t.xml" );
    }
}

$main =& new API_Config();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
