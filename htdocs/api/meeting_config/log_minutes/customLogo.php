<?php
/**
 * Created by PhpStorm.
 * User: Chen
 * Date: 2015/11/04
 * Time: 15:37
 */

require_once("classes/N2MY_Account.class.php");
require_once("classes/AppFrame.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/sales_setting_image.dbi.php");
class API_customLogo extends AppFrame
{
    var $salesSettingTable = null;
    var $user_table = null;

    function init()
    {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
        $this->salesSettingTable = new SalesSettingImageTable($this->get_dsn());
        $this->user_table = new UserTable($this->get_dsn());
    }

    function auth()
    {
//        $this->checkAuth();
    }

    function default_view()
    {
        $imageID = $this->request->get("image_id");
        $image = $this->salesSettingTable->getRow(sprintf('status = 1 AND image_id = "%s"', mysql_real_escape_string($imageID)));
        if (!$image) {
            $this->logger2->warn( __FILE__, __LINE__, "image doesn't exist");
            return false;
        }

        $userInfo = $this->user_table->find( $image["user_key"]);

        if(!$userInfo) {
            $this->logger2->warn("user doesn't exist");
        }

        $mimes = array(
            'jpg' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'gif' => 'image/gif',
            'png' => 'image/png'
        );
        $file = N2MY_APP_DIR . 'salesSetting/' . $userInfo['user_id'] . '/' . $image['type'] . '/' . $image['image_id'] . '.' . $image['extension'];
        header('content-type: ' . $mimes[$image['extension']]);
        header('content-disposition: inline; filename="' . $image['image_id'] . '";');
        readfile($file);
    }

}

$main =& new API_customLogo();
$main->execute();
