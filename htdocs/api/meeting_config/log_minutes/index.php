<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_Account.class.php");
require_once("classes/AppFrame.class.php");

class API_LoadConfig extends AppFrame
{
    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth() {
        $this->checkAuth();
    }

    function default_view()
    {
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__);
        $log_fl_ver = $this->session->get("log_fl_ver");
        //assgn values
        $this->template->assign("pathToLibrary"                     , "/lang/".$this->_lang."/flash/log_minutes/Lib.swf" );
        $this->template->assign("pathToApplicationAppearanceDataXML", "application_appearance.php");
        $this->template->assign("pathToConfigDataXML"               , "config.php");
        $this->template->assign("pathToCoreAppearanceDataXML"       , "core_appearance.php");
        $this->template->assign("pathTolanguageXML"                 , "/lang/".$this->_lang."/flash/log_minutes/language.xml" );
        if ($log_fl_ver == "as3") {
            $this->template->assign("pathToLayoutXML"               , "layout_as3.php");
        }else{
            $this->template->assign("pathToLayoutXML"               , "layout.php");
        }

        $this->display( "core/api/meeting_config/meeting/project.t.xml" );
    }
}

$main =& new API_LoadConfig();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
