<?php
/**
 * Created by PhpStorm.
 * User: Chen
 * Date: 2015/10/26
 * Time: 19:10
 */

require_once("classes/N2MY_Account.class.php");
require_once("classes/AppFrame.class.php");

class API_LayoutAS3 extends AppFrame
{
    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth() {
        $this->checkAuth();
    }

    function default_view()
    {
        $this->template->assign( "display_deleteButton", $this->session->get( "display_deleteButton" ) );
        $this->display( "core/api/meeting_config/meeting/resources/layout_for_log_minutes_as3.t.xml" );
    }

}

$main =& new API_LayoutAS3();
$main->execute();
