<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_Account.class.php");
require_once("classes/AppFrame.class.php");

class API_Layout extends AppFrame
{
    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth() {
        $this->checkAuth();
    }

    function default_view()
    {
        $this->template->assign( "display_deleteButton", $this->session->get( "display_deleteButton" ) );
        $this->display( "core/api/meeting_config/meeting/resources/layout_for_wb_preview.t.xml" );
    }

}

$main =& new API_Layout();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
