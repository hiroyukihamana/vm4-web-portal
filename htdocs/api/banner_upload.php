<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// | 株式会社ブイキューブ                                                 |
// | N2MY_CORE API                                                        |
// |                                                                      |
// |【画面ＩＤ】                                                          |
// |【画面名称】                                                          |
// |                                                                      |
// +----------------------------------------------------------------------+
// | OS     Red Hat Enterprise Linux AS release 3 (Taroon)                |
// | Apache version Apache/1.3.34 (Unix)                                  |
// | MySQL  version 4.0.22                                                |
// | PHP    version 4.3.11 (cli)                                          |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 V-cube, Inc.                                      |
// +----------------------------------------------------------------------+
// | Authors: Hiroshi Oneda <oneda@vcube.com>                            |
// |        :                                                             |
// +----------------------------------------------------------------------+
//
// $Id$

require_once('webapp/classes/AppFrame.class.php');
require_once('webapp/classes/dbi/Meeting.dbi.php');
require_once('webapp/classes/dbi/Provider.dbi.php');

/**
 * 【banner_upload】
 *
 * <pre>
 * サブクラスによるフロントエンドの機能実装。
 * フレームワーククラスを継承するものとする。
 * </pre>
 *
 * @see    EZCore/EZFrame
 * @access public
 * @author Hiroshi Oneda <oneda@vcube.com>
 */
class banner_upload extends AppFrame
{
// ------------------------------------------------------------------------

// {{{ -- vars

    /**
     * エラーメッセージ
     *
     * @access public
     * @var    string
     */
     var $_error = null;

    /**
     * [DBI オブジェクト] : 会議
     *
     * @access public
     * @var    object
     */
     var $_obj_m = null;

    /**
     * [DBI オブジェクト] : プロバイダー
     *
     * @access public
     * @var    object
     */
     var $_obj_p = null;

    /**
     * プロバイダーKEY
     *
     * @access public
     * @var    integer
     */
     var $_provider_key = null;

    /**
     * ステータス
     *
     * @access public
     * @var    integer
     */
     var $_status = 0;

    /**
     * テンプレートファイル
     *
     * @access public
     * @var    string
     */
     var $_tpl = 'api/banner_upload.t.xml';

// }}}

// ------------------------------------------------------------------------

// {{{ -- banner_upload()

    /**
     * コンストラクタ
     *
     * @access public
     * @param  
     * @return 
     */
    function banner_upload()
    {
        //call super_class' constructor
        parent::AppFrame();
    }

// }}}
// {{{ -- banner_upload()

    /**
     * デストラクタ
     *
     * @access public
     * @param  
     * @return 
     */
    function _banner_upload()
    {
    }

// }}}

// ------------------------------------------------------------------------

// {{{ -- init()

    /**
     * 初期化処理
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return 
     */
    function init(&$factory)
    {
        //debug trace
        $factory->logger->error(__CLASS__, __FILE__, __LINE__, __FUNCTION__);

        //---------------------------------------------------------------------
        //create object [ Meeting ]
        $this->_obj_m =& new Meeting();

        //create object [ Provider ]
        $this->_obj_p =& new Provider();
    }

// }}}
// {{{ -- auth()

    /**
     * 認証処理
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return 
     */
    function auth(&$factory)
    {
        //get parameters from request
        $provider_id       = $factory->request->get("provider_id"      , null);
        $provider_password = $factory->request->get("provider_password", null);
$factory->logger->error(__CLASS__, __FILE__, __LINE__, $provider_id);
$factory->logger->error(__CLASS__, __FILE__, __LINE__, $provider_password);
        //provider authentication
        $provider_key  = $this->_obj_p->getKeyByAuth($provider_id, $provider_password);

        if (!$provider_key) {
            $this->_error = "PROVIDER_AUTH_ERROR";

            $renderer = $this->render_default($factory);
            if (PEAR::isError($renderer)) {
                return $renderer;
            }
            return false;
        }

        //set provider_key
        $this->_provider_key = $provider_key;
    }

// }}}
// {{{ -- prepare()

    /**
     * 準備処理
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return 
     */
    function prepare(&$factory)
    {
    }

// }}}
// {{{ -- default_view()

    /**
     * デフォルト処理
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return 
     */
    function default_view(&$factory)
    {
    	$factory->logger->error(__CLASS__, __FILE__, __LINE__, $factory);
        //call action
        $action = $this->action_default($factory);
        if (PEAR::isError($action)) {
            return $action;
        }
    }

// }}}

// ------------------------------------------------------------------------

// {{{ -- action_default()

    /**
     * [API] 会議予約更新処理
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return 
     */
    function action_default(&$factory)
    {
    	$factory->logger->error(__CLASS__, __FILE__, __LINE__, $factory);
        //get parameters from request
        $params = array(
                        "provider_id"            => null,
                        "provider_password"      => null,
                        //"room_key"               => null,
                        "meeting_ticket"         => null,
                        "banner_url"            => null,
                        //"meeting_name"           => null,
                        //"meeting_start_datetime" => null,
                        //"meeting_stop_datetime"  => null
                       );

        $d = array();
        foreach ($params as $idx => $val) {
            $d[$idx] = $factory->request->get($idx, $val);
            if ($d[$idx] == "") {
                $d[$idx] = $val;
            }
        }
		$factory->logger->error(__CLASS__, __FILE__, __LINE__, $d);
        //---------------------------------------------------------------------
        //get meeting_key
        $meeting_key = $this->_obj_m->getKeyByTicket($this->_provider_key, $d['meeting_ticket']);

		$banner_name = $meeting_key.".jpg";
    	if($_FILES){
			print_r($_FILES);
			$name       = $_FILES["file"]["name"];
	        $type       = $_FILES["file"]["type"];
	        $tmp_name   = $_FILES["file"]["tmp_name"];
	        $error      = $_FILES["file"]["error"];
	        $size       = $_FILES["file"]["size"];
	        list ($width, $height, $type, $attr) = getimagesize($tmp_name);
	        print "width".$width."height".$height;
			$upfile = '../banners/'.$banner_name;
	        if (!move_uploaded_file($tmp_name, $upfile)) {
	        	print "error upload file";
	        }
		}
		$banner_size = $width."-".$height;
        //update meeting reservation
        $factory->logger->error(__CLASS__, __FILE__, __LINE__, $meeting_key.$d['banner_url'].$banner_size);
        $is_updated  = $this->_obj_m->banner_upload($meeting_key, $d['banner_url'], $banner_size);

        //set status
        if ($is_updated) {
            $this->_status = "1";
        }

        //---------------------------------------------------------------------
        //call renderer
        $renderer = $this->render_default($factory);
        if (PEAR::isError($renderer)) {
            return $renderer;
        }
    }

// }}}

// ------------------------------------------------------------------------

// {{{ -- render_default()

    /**
     * [renderer] デフォルト
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return 
     */
    function render_default(&$factory)
    {
        //assign values
        $factory->tpl->assign('api_class', __CLASS__);
        $factory->tpl->assign('api_error', $this->_error);

        $factory->tpl->assign('status'   , $this->_status);

        //display xml_template
        $factory->tpl->display_xml($this->_tpl);
    }

// }}}

// ------------------------------------------------------------------------
}

$main =& new banner_upload();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
