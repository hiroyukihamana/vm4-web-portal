<?php
require_once ("classes/N2MY_Api.class.php");
require_once ("classes/N2MY_DBI.class.php");
require_once ("classes/dbi/user.dbi.php");
require_once ("classes/dbi/room.dbi.php");
require_once ("classes/dbi/member.dbi.php");
require_once ("classes/dbi/member_group.dbi.php");
require_once ("lib/EZLib/EZHTML/EZValidator.class.php");
require_once ("lib/EZLib/EZUtil/EZEncrypt.class.php");
require_once ("classes/dbi/sales_room_queue.dbi.php");/* MTGVFOUR-2215 */

class ONE_ADMIN_MEMBER_API extends N2MY_Api {

    var $obj_user = null;
    var $obj_room = null;
    var $obj_member = null;
    var $obj_member_group = null;
    var $obj_auth_user = null;
    var $obj_server = null;
    var $account_dsn = null;
    var $obj_SalesRoomQueue = null;/* MTGVFOUR-2215 */
    var $is_SalesRoomQadd   = null;/* MTGVFOUR-2215 */

    function init() {
        // SSL対応
        header('Pragma:');
    }

    function auth() {
        $this -> checkAuthorizationOne();
        $this -> account_dsn = $this -> config -> get('GLOBAL', 'auth_dsn');
        $this -> obj_user = new UserTable($this -> get_dsn());
        $this -> obj_room = new RoomTable($this -> get_dsn());
        $this -> obj_member = new MemberTable($this -> get_dsn());
        $this -> obj_member_group = new MemberGroupTable($this -> get_dsn());
        $this -> obj_auth_user = new N2MY_DB($this -> account_dsn, 'user');
        $this -> obj_server = new N2MY_DB($this -> account_dsn, 'db_server');
        $this -> obj_SalesRoomQueue = new SalesRoomQueue($this->get_dsn()); /* MTGVFOUR-2215 */
        $this -> is_SalesRoomQadd = $this -> config -> get('N2MY', 'is_SalesRoomQadd');/* MTGVFOUR-2215 */
    }

    function action_delete_member() {
        $request = $this -> request -> getAll();
        $rules = array(
            'n2my_session' => array('required' => true),
            'member_xml' => array('required' => true),
            'output_type' => array('allow' => $this -> get_output_type_list())
        );
        $err_obj = $this -> error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this -> display_error("100", "PARAMETER_ERROR", $this -> get_error_info($err_obj));
        }
        $member_xml = $this->one_get_xml(rawurldecode($request['member_xml']));
        if (!$member_xml) {
            return $this -> display_error("100", "PARAMETER_ERROR", "XML_ERROR");
        }
        //turn SimpleXML Object into String
        $member_data = json_decode(json_encode($member_xml), true);
        $members = $member_data['member'];
        if (!array_key_exists(0, $members)) {
            $tmp = $members;
            unset($members);
            $members[] = $tmp;
        }
        $data = array();
        if ($members) {
            $correct_ret = array();
            $error_ret = array();
            foreach ($members as $member) {
                $ret = $this -> delete_member($member);
                if ($ret['status']) {
                    $correct_ret[] = array('member_id' => $ret['member_id'], );
                } else {
                    $error_ret['errors'][] = array(
                        'error_member_id' => $member['member_id'],
                        'error_msg' => $ret['error_msg']
                    );
                }
            }
            $data['count'] = count($members);
            if ($error_ret) {
                $data['error_members'] = $error_ret;
            }
            if ($correct_ret) {
                $data['members'] = $correct_ret;
            } else {
                $this -> display_error(100, "PARAMETER_ERROR", $error_ret);
            }
        }
        $this -> logger2 -> info($data);
        return parent::one_output($data);
    }

    private function delete_member($info) {
        if (!$info['user_id'] || !$info['vcube_id']) {
            return array(
                'status' => 0,
                'error_msg' => 'USER_ID OR VCUBE_ID CANNOT BE NULL'
            );
        }
        $where = "user_id = '" . mysql_real_escape_string($info['user_id']) . "'";
        $user_info = $this -> obj_user -> getRow($where);
        if (!$user_info) {
            $error_msg[] = 'USER DOES NOT EXIST';
            $ret = array(
                'status' => 0,
                'error_msg' => $error_msg
            );
            return $ret;
        }
        $where = "member_status = 0 AND user_key = '" . $user_info['user_key'] . "' AND vcube_one_member_id = '" . mysql_real_escape_string($info['vcube_id']) . "'";
        if ($info['member_id']) {
            $where .= " AND member_id = '" . mysql_real_escape_string($info['member_id']) . "'";
        }
        $member_info = $this -> obj_member -> getRow($where);
        if (!$member_info) {
            $error_msg[] = 'MEMBER DOES NOT EXIST';
            $ret = array(
                'status' => 0,
                'error_msg' => $error_msg
            );
            return $ret;
        }

        require_once 'classes/N2MY_Account.class.php';
        $obj_n2my_account = new N2MY_Account( $this->get_dsn() );
        $obj_n2my_account->deleteMember( $member_info['member_key'] );

        return array(
            'status' => 1,
            'member_id' => $member_info['member_id']
        );
    }

    function action_edit_member(){
        $member_list = $this -> read_xml();
        if($member_list){
            $error_ret   = array();
            $correct_ret = array();
            foreach ($member_list as $member) {
                $ret = $this -> edit_member($member);
                if ($ret['status']) {
                    unset($ret['status']);
                    $correct_ret[] = $ret;
                } else {
                    $error_ret['errors'][] = array(
                        'error_member_id' => $member['member_id'],
                        'error_msg' => $ret['error_msg']
                    );
                }
            }
            $data['count'] = count($member_list);
            if ($error_ret) {
                $data['error_members'] = $error_ret;
            }
            if ($correct_ret) {
                $data['members'] = $correct_ret;
            } else {
                $this -> display_error(100, "PARAMETER_ERROR", $error_ret);
            }
        }
        return parent::one_output($data);
    }

    function edit_member($info){
        $this->logger2->info($info);
        if (!$info['user_id'] || !$info['member_id']) {
            return array(
                'status'    => 0,
                'error_msg' => 'USER_ID OR MEMBER_ID CANNOT BE NULL'
            );
        }
        $where = "user_status = 1 AND user_id = '" . mysql_real_escape_string($info['user_id']) . "'";
        $user_info = $this -> obj_user -> getRow($where);
        if (!$user_info) {
            $ret = array(
                'status'    => 0,
                'error_msg' => 'USER DOES NOT EXIST'
            );
            return $ret;
        }
        $where = "member_status = 0 AND user_key = '" . $user_info['user_key'] . "' AND member_id = '" . mysql_real_escape_string($user_info['user_id'] . '_' . $info['member_id']) . "'";
        $member_info = $this -> obj_member -> getRow($where);
        if (!$member_info) {
            $ret = array(
                'status'    => 0,
                'error_msg' => 'MEMBER DOES NOT EXIST'
            );
            return $ret;
        }
        $error_msg = array();
        $data = array();
        if ($info['member_name']) {
            if (mb_strlen($info['member_name']) > 256){
                $error_msg[] = 'MEMBER NAME LENGTH INVALID';
            }else{
                $data['member_name'] = $info['member_name'];
            }
        }
        if ($info['email']) {
            if (!EZValidator::valid_email($info['email'])) {
                $error_msg[] = 'MEMBER EMAIL INVALID';
            }else{
                $data['member_email'] = $info['email'];
            }
        }
        if ($info['member_password']) {
            if (!preg_match('/^[!-\[\]-~@._=`]{8,128}$/', $info['member_password'])) {
                $error_msg[] = 'MEMBER PASSWORD INVALID';
            }elseif (!preg_match('/[[:alpha:]]+/', $info['member_password']) || preg_match('/^[[:alpha:]]+$/', $info['member_password'])) {
                $error_msg[] = 'MEMBER PASSWORD INVALID';
            }else{
                $data['member_pass'] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $info['member_password']);
            }
        }
        if ($info['lang']) {
            $lang_list = $this -> get_language_list();
            if (!array_key_exists($info['lang'], $lang_list)) {
                $error_msg[] = 'LANGUAGE INVALID';
            }else{
                $data['lang'] = $info['lang'];
            }
        }
        if ($info['timezone']) {
            $timezone_list = $this -> get_timezone_list();
            if (!array_key_exists($info['timezone'], $timezone_list)) {
                $error_msg[] = 'TIMEZONE INVALID';
            }else{
                $data['timezone'] = $info['timezone'];
            }
        }
        if ($info['group_name']) {
            $data['member_group'] = $this -> get_member_group_key($info['group_name'], $user_info['user_key'], true);
        }
        // if (strlen($info['use_sales']) != 0){
            // if (is_numeric($info['use_sales']) && in_array($info['use_sales'], array(1,0))){
                // $data['use_sales'] = $info['use_sales'];
            // }else{
                // $error_msg[] = 'SALES OPTION INVALID';
            // }
        // }
        if ($error_msg) {
            $ret = array(
                'status' => 0,
                'error_msg' => $error_msg
            );
            return $ret;
        }
        //MEMBER INFO UPDATE
        $this -> logger2 -> info($data);
        $this -> obj_member -> update($data, $where);
        // if ($data['use_sales']) {
            // if (!$member_info['use_sales']) {
                // $this -> add_sales_room($member_info['member_key']);
            // }
        // } else {
            // if ($member_info['use_sales']) {
                // $obj_member_room_relation = new N2MY_DB($this -> get_dsn(), 'member_room_relation');
                // $room_key = $obj_member_room_relation -> getOne('member_key = ' . $member_info['member_key'], 'room_key');
                // if ($room_key) {
                    // $where = "room_key = '" . mysql_real_escape_string($room_key) . "'";
                    // $obj_member_room_relation -> remove($where);
                    // $data = array(
                        // 'room_status'     => 0,
                        // 'room_deletetime' => date("Y-m-d H:i:s")
                    // );
                    // $this -> obj_room -> update($data, $where);
                    // $obj_room_plan = new N2MY_DB($this -> get_dsn(), 'room_plan');
                    // $data = array(
                        // 'room_plan_status'     => 0,
                        // 'room_plan_updatetime' => date("Y-m-d H:i:s")
                    // );
                    // $obj_room_plan -> update($data, $where);
                    // $obj_relation = new N2MY_DB($this -> account_dsn, 'relation');
                    // $where = "relation_key = '" . mysql_real_escape_string($room_key) . "'";
                    // $data = array(
                        // 'status' => 0
                    // );
                    // $obj_relation -> update($data, $where);
                // }
            // }
        // }
        $_member_info = $this -> obj_member -> getRow('member_key = ' . $member_info['member_key']);
        return array(
            'status'          => 1,
            'user_id'         => $user_info['user_id'],
            'member_id'       => $_member_info['vcube_one_member_id'],
            'member_name'     => $_member_info['member_name'],
            'member_password' => $info['member_password'],
            'email'           => $_member_info['member_email'],
            'lang'            => $_member_info['lang'],
            'timezone'        => $_member_info['timezone'],
            'group_name'      => $_member_info['member_group'],
            'use_sales'       => $_member_info['use_sales']
        );
    }

    function action_add_member() {
        $request = $this -> request -> getAll();
        // パラメタチェック
        $rules = array(
            'n2my_session' => array('required' => true),
            'member_xml' => array('required' => true),
            'output_type' => array('allow' => $this -> get_output_type_list())
        );
        $err_obj = $this -> error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this -> display_error("100", "PARAMETER_ERROR", $this -> get_error_info($err_obj));
        }
        //$request['member_xml'] = preg_replace('#&(?![a-z]{1,6};)#i', '&#038;', $request['member_xml']);
        $member_xml = $this->one_get_xml(rawurldecode($request['member_xml']));
        if (!$member_xml) {
            return $this -> display_error("100", "PARAMETER_ERROR", "XML_ERROR");
        }
        //turn SimpleXML Object into String
        $member_data = json_decode(json_encode($member_xml), true);
        $members = $member_data['member'];
        if (!array_key_exists(0, $members)) {
            $tmp = $members;
            unset($members);
            $members[] = $tmp;
        }
        $data = array();
        if ($members) {
            $correct_ret = array();
            $error_ret = array();
            foreach ($members as $member) {
                $ret = $this -> add_member($member);
                if ($ret['status']) {
                    $correct_ret[] = array(
                        'member_key' => $ret['member_key'],
                        'member_id' => $ret['member_id']
                    );
                } else {
                    $error_ret['errors'][] = array(
                        'error_member_id' => $member['member_id'],
                        'error_msg' => $ret['error_msg']
                    );
                }
            }
            $data['count'] = count($members);
            if ($error_ret) {
                $data['error_members'] = $error_ret;
            }
            if ($correct_ret) {
                $data['members'] = $correct_ret;
            } else {
                $this -> display_error(100, "PARAMETER_ERROR", $error_ret);
            }
        }
        $this -> logger2 -> info($data);
        return parent::one_output($data);
    }

    function add_member($member_info) {
        $error_msg = array();
        $where = sprintf('user_id = "%s"', addslashes($member_info['user_id']));
        $user_info = $this -> obj_user -> getRow($where);
        if (!$user_info) {
            $error_msg[] = 'USER DOES NOT EXIST';
            $ret = array(
                'status' => 0,
                'error_msg' => $error_msg
            );
            return $ret;
        }
        $member_id_original = $member_info['member_id'];
        $member_info['user_key'] = $user_info['user_key'];
        $member_info['member_id'] = $user_info['user_id'] . '_' . $member_info['member_id'];
        if ($user_info['member_id_format_flg'] == 0) {
            if (!EZValidator::valid_vcube_id($member_id_original)) {
                $error_msg[] = 'MEMBER ID INVALID';
            } elseif (mb_strlen($member_id_original) > 255) {
                $error_msg[] = 'MEMBER ID LENGTH INVALID';
            }
        } else {
            $regex_rule = '/^[[:alnum:]!#$%&\'*+\-\/=?^_`{|}~.@]{8,255}$/';
            $valid_result = EZValidator::valid_regex($member_id_original, $regex_rule);
            if ($valid_result == false) {
                $error_msg[] = 'MEMBER ID INVALID';
            }
        }
        if ($this -> is_member_exist($member_info['member_id'])) {
            $error_msg[] = 'MEMBER ID ALREADY EXISTS';
        }
        if (!$member_info['member_name']) {
            $error_msg[] = 'MEMBER NAME CANNOT BE NULL';
        } elseif (mb_strlen($member_info['member_name']) > 256) {
            $error_msg[] = 'MEMBER NAME LENGTH INVALID';
        }
        //メンバーメアドは非必要になる
        if ($member_info['email'] && !EZValidator::valid_email($member_info['email'])) {
            $error_msg[] = 'MEMBER EMAIL INVALID';
        }
        if ($member_info['member_password']) {
            if (!preg_match('/^[!-\[\]-~@._=`]{8,128}$/', $member_info['member_password'])) {
                $error_msg[] = 'MEMBER PASSWORD INVALID';
            }
            if (!preg_match('/[[:alpha:]]+/', $member_info['member_password']) || preg_match('/^[[:alpha:]]+$/', $member_info['member_password'])) {
                $error_msg[] = 'MEMBER PASSWORD INVALID';
            }
        } else {
            $member_info['member_password'] = $this -> create_id() . rand(2, 9);
        }
        if ($user_info['max_member_count'] != 0 && $user_info["account_plan"] == "one_id_host") {
            $where = "member_status > -1 AND user_key = " . $user_info['user_key'];
            $member_cnt = $this -> obj_member -> numRows($where);
            if ($user_info['max_member_count'] <= $member_cnt) {
                $error_msg[] = 'MEMBER COUNT INVALID';
            }
        }
        if ($error_msg) {
            $ret = array(
                'status' => 0,
                'error_msg' => $error_msg
            );
            return $ret;
        }

        if (!$member_info['lang'] && (is_array($member_info['lang']) || strlen($member_info['lang']) == 0)) {
            $member_info['lang'] = 'ja';
        } else {
            $lang_list = $this -> get_language_list();
            if (!array_key_exists($member_info['lang'], $lang_list)) {
                $member_info['lang'] = 'ja';
            }
        }
        if (!$member_info['timezone'] && (is_array($member_info['timezone']) || strlen($member_info['timezone']) == 0)) {
            $member_info['timezone'] = 9;
        } else {
            $timezone_list = $this -> get_timezone_list();
            if (!array_key_exists($member_info['timezone'], $timezone_list)) {
                $member_info['timezone'] = 9;
            }
        }
        if (!$member_info['use_sales']) {
            if (is_array($member_info['use_sales']) || strlen($member_info['use_sales']) == 0) {
                $member_info['use_sales'] = 1;
            } else {
                $member_info['use_sales'] = 0;
            }
        } else {
            $member_info['use_sales'] = 1;
        }

        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $member_data = array(
            'user_key' => $user_info['user_key'],
            'member_id' => $member_info['member_id'],
            'member_name' => $member_info['member_name'],
            'member_pass' => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_info['member_password']),
            'member_email' => $member_info['email'],
            'member_group' => $this -> get_member_group_key($member_info['group_name'], $user_info['user_key'], true),
            'timezone' => $member_info['timezone'],
            'lang' => $member_info['lang'],
            'use_sales' => $member_info['use_sales'],
            'use_shared_storage' => 1,
            'vcube_one_member_id' => $member_id_original,
            'create_datetime' => date("Y-m-d H:i:s"),
            'update_datetime' => date("Y-m-d H:i:s")
        );
        $this -> logger2 -> info($member_data);
        $_ret = $this -> obj_member -> add($member_data);
        if (DB::isError($_ret)) {
            $ret = array(
                'status' => 0,
                'error_msg' => 'DB ERROR'
            );
            return $ret;
        }
        $server_info = $this -> obj_server -> getRowsAssoc('server_status = 1');
        $auth_member = array(
            'user_id' => $member_info['member_id'],
            'server_key' => $server_info[0]['server_key'],
            'user_registtime' => date("Y-m-d H:i:s")
        );
        $_ret = $this -> obj_auth_user -> add($auth_member);
        if (DB::isError($_ret)) {
            $ret = array(
                'status' => 0,
                'error_msg' => 'DB ERROR'
            );
            return $ret;
        }
        $where = sprintf("user_key = %s AND member_id = '%s'", $member_info['user_key'], mysql_real_escape_string($member_info['member_id']));
        $_member_info = $this -> obj_member -> getRow($where);
        //部屋情報登録
        if ($member_info['use_sales']) {
/* MTGVFOUR-2215--> */
            if( $this->is_SalesRoomQadd ) {
$this -> logger2 -> info('>>>>> SALES ROOM QUEUE ADDED < new root >');
                $_ret = $this -> obj_SalesRoomQueue->addQmember($_member_info['member_key']);
                if( DB::isError($_ret) ) {
                    $ret = array(
                        'status' => 0,
                        'error_msg' => 'DB ERROR'
                    );
                    return $ret;
                }
             } else {
                $_ret = $this -> add_sales_room($_member_info['member_key']);
                if (!$_ret) {
                    $ret = array(
                        'status' => 0,
                        'error_msg' => 'DB ERROR'
                    );
                    return $ret;
                }
            }
/* <--MTGVFOUR-2215 */
        }
        $ret = array(
            'status' => 1,
            'member_key' => $_member_info['member_key'],
            'member_id' => $_member_info['member_id']
        );
        return $ret;
    }

    /**
     * ADD SALES ROOM
     */
    function add_sales_room($member_key) {
        $where = 'member_key = ' . $member_key;
        $member_info = $this -> obj_member -> getRow($where);
        $where = 'user_key = ' . $member_info['user_key'];
        $user_info = $this -> obj_user -> getRow($where);
        $count = $this -> obj_room -> numRows($where);
        $room_key = $user_info['user_id'] . '-' . ($count + 1) . '-' . substr(md5(uniqid(time(), true)), 0, 4);
        $room_sort = $count + 1;
        //部屋登録
        $room_data = array(
            'room_key' => $room_key,
            'room_name' => $member_info['member_name'],
            'user_key' => $user_info['user_key'],
            'room_sort' => $room_sort,
            'room_status' => "1",
            'use_teleconf' => "0",
            'use_pgi_dialin' => "0",
            'use_pgi_dialin_free' => "0",
            'use_pgi_dialin_lo_call' => "0",
            'use_pgi_dialout' => "0",
            'use_sales_option' => "1",
            'max_ss_watcher_seat' => "1",
            'room_registtime' => date("Y-m-d H:i:s")
        );
        $ret = $this -> obj_room -> add($room_data);
        if (DB::isError($ret)) {
            return false;
        }
        $obj_relation = new N2MY_DB($this -> account_dsn, "relation");
        $relation_data = array(
            'relation_key' => $room_key,
            'relation_type' => "mfp",
            'user_key' => $user_info["user_id"],
            'status' => "1",
            'create_datetime' => date("Y-m-d H:i:s")
        );
        $ret = $obj_relation -> add($relation_data);
        require_once ("classes/dbi/inbound.dbi.php");
        $obj_inboound = new InboundTable($this -> get_dsn());
        $inbound_count = $obj_inboound -> numRows($where);
        if ($inbound_count == 0 || !$inbound_count) {
            $count = 1;
            while ($count > 0) {
                // 念のため8ケタにそろえる
                $inbound_id = substr($this -> create_id(), 0, 8);
                $inbound_where = "inbound_id = '" . $inbound_id . "'";
                $count = $obj_inboound -> numRows($inbound_where);
            }
            // 登録
            $inbound_data = array(
                "inbound_id" => $inbound_id,
                "user_key" => $user_info['user_key'],
                "status" => 1,
                "create_datetime" => date("Y-m-d H:i:s"),
                "update_datetime" => date("Y-m-d H:i:s")
            );
            $ret = $obj_inboound -> add($inbound_data);
            if (DB::isError($ret)) {
                return false;
            }
            //relation追加
            $relation_data = array(
                "relation_key" => $inbound_id,
                "relation_type" => "inbound_id",
                "user_key" => $user_info["user_id"],
                "status" => 1,
                "create_datetime" => date("Y-m-d H:i:s")
            );
            $obj_relation -> add($relation_data);
        }
        require_once ("classes/dbi/outbound.dbi.php");
        $obj_outbound = new OutboundTable($this -> get_dsn());
        $outbound_info = $obj_outbound -> getRow($where);
        if (!$outbound_info) {
            $outbound_data = array(
                "user_key" => $user_info['user_key'],
                "status" => 1,
                "create_datetime" => date("Y-m-d H:i:s"),
                "update_datetime" => date("Y-m-d H:i:s")
            );
            $ret = $obj_outbound -> add($outbound_data);
            if (DB::isError($ret)) {
                return false;
            }
        }
        $count = 1;
        while ($count > 0) {
            $outbound_id = $this -> create_id();
            $where = "outbound_id = '" . addslashes($outbound_id) . "'";
            $count = $this -> obj_member -> numRows($where);
        }
        $outbound_where = "user_key = " . $user_info['user_key'];
        $outbound_info = $obj_outbound -> getRow($outbound_where);
        $outbound_key = sprintf("%03d", $outbound_info["outbound_key"]);
        $outbound_where = "outbound_id != '' AND user_key = " . $user_info['user_key'];
        $sales_member_count = $this -> obj_member -> numRows($outbound_where);
        $outbound_sort = $sales_member_count + 1;
        $outbound_sort = sprintf("%03d", $outbound_sort);
        $outbound_number_id = $outbound_key . $outbound_sort;
        $member_update_data = array(
            "outbound_id" => $outbound_id,
            "outbound_number_id" => $outbound_number_id,
            "use_sales" => 1
        );
        $where_member = "member_key = " . $member_key;
        $this -> obj_member -> update($member_update_data, $where_member);
        //部屋のアウトバウンドIDも更新
        $where_room = "room_key = '" . addslashes($room_key) . "'";
        $room_update_data = array(
            "outbound_id" => $outbound_id
        );
        $add_room = $this -> obj_room -> update($room_update_data, $where_room);
        $relation_data = array(
            "relation_key" => $outbound_id,
            "relation_type" => "outbound",
            "user_key" => $member_info["member_id"],
            "status" => "1",
            "create_datetime" => date("Y-m-d H:i:s")
        );
        $obj_relation -> add($relation_data);
        $relation_data_number = array(
            "relation_key" => $outbound_number_id,
            "relation_type" => "outbound_number",
            "user_key" => $member_info["member_id"],
            "status" => "1",
            "create_datetime" => date("Y-m-d H:i:s")
        );
        $obj_relation -> add($relation_data_number);
        //部屋のリレーション追加
        require_once ("classes/dbi/member_room_relation.dbi.php");
        $obj_member_room_relation = new MemberRoomRelationTable($this -> get_dsn());
        $member_room_relation_data = array(
            "member_key" => $member_info["member_key"],
            "room_key" => $room_key,
            "create_datetime" => date("Y-m-d H:i:s")
        );
        $ret = $obj_member_room_relation -> add($member_room_relation_data);
        if (DB::isError($ret)) {
            return false;
        }

        $service_key = 118;
        $room_plan_data = array(
            'room_key' => $room_key,
            'room_plan_status' => 1,
            'service_key' => $service_key,
            'contract_month_number' => 0,
            'discount_rate' => 0,
            'room_plan_starttime' => date("Y-m-d 00:00:00"),
            'room_plan_endtime' => "0000-00-00 00:00:00",
            'room_plan_registtime' => date("Y-m-d H:i:s")
        );
        require_once ("classes/dbi/room_plan.dbi.php");
        $obj_room_plan = new RoomPlanTable($this -> get_dsn());
        $ret = $obj_room_plan -> add($room_plan_data);
        if (DB::isError($ret)) {
            return false;
        }
        require_once ("classes/dbi/service.dbi.php");
        $obj_service = new ServiceTable($this -> account_dsn);
        $where = 'service_key = ' . $service_key;
        $service_info = $obj_service -> getRow($where);
        require_once ("classes/dbi/ordered_service_option.dbi.php");
        $obj_ordered_service_option = new OrderedServiceOptionTable($this -> get_dsn());
        $where = sprintf("room_key = '%s' AND service_option_key = 7 AND ordered_service_option_status = 1", addslashes($room_key));
        $audience_count = $obj_ordered_service_option -> numRows($where);
        if ($audience_count > 0) {
            $max_audience = $audience_count . "0";
        } else {
            $max_audience = $service_info["max_audience_seat"];
        }
        $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
        $document_ext = $this->config->getAll('DOCUMENT_FILES');
        if(array_key_exists("xdw", $document_ext)){
            unset($document_ext["xdw"]);
        }
        $whiteboard_filetype["document"] = array_keys($document_ext);
        $whiteboard_filetype["image"]    = array_keys($image_ext);
        $where = "room_key = '" . mysql_real_escape_string($room_key) . "'";
        $room_seat = array(
            "max_seat" => 2,
            "max_audience_seat" => $max_audience,
            "max_whiteboard_seat" => $service_info["max_whiteboard_seat"],
            "max_guest_seat" => $service_info["max_guest_seat"],
            "max_guest_seat_flg" => $service_info["max_guest_seat_flg"],
            "extend_seat_flg" => $service_info["extend_seat_flg"],
            "extend_max_seat" => $service_info["extend_max_seat"],
            "extend_max_audience_seat" => $service_info["extend_max_audience_seat"],
            "meeting_limit_time" => $service_info["meeting_limit_time"],
            "ignore_staff_reenter_alert" => $service_info["ignore_staff_reenter_alert"],
            "default_camera_size" => $service_info["default_camera_size"],
            "hd_flg" => $service_info["hd_flg"],
            "disable_rec_flg" => $service_info["disable_rec_flg"],
            "active_speaker_mode_only_flg" => 0,
            "active_speaker_mode_use_flg" => 0,
            "max_room_bandwidth" => $service_info["max_room_bandwidth"],
            "max_user_bandwidth" => $service_info["max_user_bandwidth"],
            "min_user_bandwidth" => $service_info["min_user_bandwidth"],
            "whiteboard_page" => $service_info["whiteboard_page"] ? $service_info["whiteboard_page"] : 100,
            "whiteboard_size" => $service_info["whiteboard_size"] ? $service_info["whiteboard_size"] : 20,
            "cabinet_size" => $service_info["cabinet_size"] ? $service_info["cabinet_size"] : 20,
            "customer_sharing_button_hide_status" => 1,
            "default_agc_use_flg" => 1,
            "default_layout_16to9" => "normal",
            "whiteboard_filetype" => serialize($whiteboard_filetype),
            "room_updatetime" => date("Y-m-d H:i:s")
        );
        $ret = $this -> obj_room -> update($room_seat, $where);
        if (DB::isError($ret)) {
            return false;
        }
        //PC画面共有と高画質オプション付与
        //プランのオプション情報取得
        $add_options = array();
        if ($service_info["meeting_ssl"] == 1) {
            $add_options[] = "2";
        }
        if ($service_info["desktop_share"] == 1) {
            $add_options[] = "3";
        }
        if ($service_info["high_quality"] == 1) {
            $add_options[] = "5";
        }
        if ($service_info["mobile_phone"] > 0) {
            $add_options[] = "6";
        }
        if ($service_info["h323_client"] > 0) {
            $add_options[] = "8";
        }
        //hdd_extentionは数値分登録
        if ($service_info["hdd_extention"] > 0) {
            $add_options[] = "4";
        }
        // ホワイトボードプラン
        if ($service_info["whiteboard"] == 1) {
            $add_options[] = "16";
        }
        // マルチカメラ
        if ($service_info["multicamera"] == 1) {
            $add_options[] = "18";
        }
        // 電話連携
        if ($service_info["telephone"] == 1) {
            $add_options[] = "19";
        }
        // 録画GW
        if ($service_info["record_gw"] == 1) {
            $add_options[] = "20";
        }
        // スマートフォン
        if ($service_info["smartphone"] == 1) {
            $add_options[] = "21";
        }
        // 資料共有映像再生許可
        if ($service_info["whiteboard_video"] == 1) {
            $add_options[] = "22";
        }
        // PGi連携
        // if ($service_info["teleconference"] == 1) {
            // $add_options[] = "23";
        // }
        // 最低帯域アップ
        if ($service_info["video_conference"] == 1) {
            $add_options[] = "24";
        }
        // 最低帯域アップ
        if ($service_info["minimumBandwidth80"] == 1) {
            $add_options[] = "25";
        }
        // h264
        if ($service_info["h264"] == 1) {
            $add_options[] = "26";
        }
        // global_link
        // ユーザーにGLオプションがあればGLをつける
        require_once ("classes/dbi/user_service_option.dbi.php");
        $obj_user_option = new UserServiceOptionTable($this->get_dsn());
        $user_option_info = $obj_user_option -> getRow("service_option_key = 30 AND user_service_option_status =1 AND user_key = " . $user_info['user_key']);

        if ($service_info["global_link"] == 1 || $user_option_info) {
            $add_options[] = "30";
        }


        //オプション登録
        if ($add_options) {
            foreach ($add_options as $option_key) {
                $where = "ordered_service_option_status = 1 AND room_key = '" . $room_key . "' AND service_option_key = " . $option_key;
                $count = $obj_ordered_service_option -> numRows($where);
                if ($count == 0 || "4" == $option_key || "6" == $option_key || "8" == $option_key) {
                    if ("4" == $option_key) {
                        for ($num = 1; $num <= $service_info["hdd_extention"]; $num++) {
                            $this -> add_room_options($room_key, $option_key, null);
                        }
                    } else if ("6" == $option_key) {
                        for ($num = 1; $num <= $service_info["mobile_phone"]; $num++) {
                            $this -> add_room_options($room_key, $option_key, null);
                        }
                    } else if ("8" == $option_key) {
                        for ($num = 1; $num <= $service_info["h323_client"]; $num++) {
                            $this -> add_room_options($room_key, $option_key, null);
                        }
                    } else {
                        $this -> add_room_options($room_key, $option_key, null);
                    }
                }
            }
        }
        $this -> logger2 -> info('SALES ROOM ADDED SUCCESSFULLY');
        return true;
    }

    function add_room_options($room_key, $service_option_key, $option_start_time) {
        require_once ("classes/dbi/ordered_service_option.dbi.php");
        $obj_ordered_service_option = new OrderedServiceOptionTable($this -> get_dsn());
        if (!$option_start_time) {
            $option_start_time = date("Y-m-d H:i:s");
        }
        if ($service_option_key == 23) {// teleconference
            require_once ("classes/dbi/pgi_setting.dbi.php");
            $obj_pgi_setting = new PGiSettingTable($this -> get_dsn());
            $month_first_day = substr($option_start_time, 0, 7);
            $pgi_setting = $obj_pgi_setting -> findEnablleAtYM($room_key, $month_first_day);
            if (count($pgi_setting) <= 0) {
                exit ;
            }
        }
        $today = date("Y-m-d 23:59:59");
        if (strtotime($option_start_time) >= strtotime($today)) {
            $status = "2";
        } else {
            $status = "1";
        }
        $data = array(
            "room_key" => $room_key,
            "service_option_key" => $service_option_key,
            "ordered_service_option_status" => $status,
            "ordered_service_option_starttime" => $option_start_time,
            "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
            "ordered_service_option_deletetime" => "0000-00-00 00:00:00"
        );
        if ($service_option_key == 7) {
            $obj_room_plan = new N2MY_DB($this -> get_dsn(), "room_plan");
            $where = "room_plan_status = 1 AND room_key = '" . mysql_real_escape_string($room_key) . "'";
            $room_plan = $obj_room_plan -> getRow($where);
            if (!$room_plan) {
                $where = "room_plan_status = 2 AND room_key = '" . mysql_real_escape_string($room_key) . "'";
                $room_plan_reserve = $obj_room_plan -> getRow($where);
                if ($room_plan_reserve) {
                    $obj_ordered_service_option -> add($data);
                }
            } else {
                $obj_ordered_service_option -> add($data);
                $where_count = "ordered_service_option_status = 1 AND room_key = '" . mysql_real_escape_string($room_key) . "' AND service_option_key = " . $service_option_key;
                $audience_count = $obj_ordered_service_option -> numRows($where_count);
                if ($audience_count > 0) {
                    $seat_data["max_seat"] = "9";
                    $audience_seat = $audience_count . "0";
                } else {
                    $seat_data["max_seat"] = "10";
                    $audience_seat = "0";
                }
                $seat_data["max_audience_seat"] = $audience_seat;
                $where_room = "room_key = '" . mysql_real_escape_string($room_key) . "'";
                $room_data = $this -> obj_room -> update($seat_data, $where_room);
            }
        } else {
            $obj_ordered_service_option -> add($data);
            if ($service_option_key == 23) {// teleconference
                $teleconf_data = array(
                    'use_teleconf' => 0,
                    'use_pgi_dialin' => 0,
                    'use_pgi_dialin_free' => 0,
                    'use_pgi_dialin_lo_call' => 0,
                    'use_pgi_dialout' => 0
                );
                $where_room = "room_key = '" . mysql_real_escape_string($room_key) . "'";
                $this -> obj_room -> update($teleconf_data, $where_room);
            }
        }
        // スマートフォン連携時に電話連携がなければ自動でセットする
        if ($service_option_key == 21) {
            $where = "room_key = '" . mysql_real_escape_string($room_key) . "' AND service_option_key = 19 AND ordered_service_option_status = 1";
            $where_count = $obj_ordered_service_option -> numRows($where);
            if ($where_count == 0) {
                $data2 = $data;
                $data2["service_option_key"] = 19;
                $obj_ordered_service_option -> add($data2);
            }
        }
        //テレビ会議連携の場合は人数を9に変更
        if ($service_option_key == 24) {
            require_once ("classes/dbi/ives_setting.dbi.php");
            $obj_ives_setting = new IvesSettingTable($this -> get_dsn());
            $res = $obj_ives_setting -> findLatestByRoomKey($room_key);
            if (PEAR::isError($res) || !isset($res["use_active_speaker"])) {
                exit ;
            }
            // ActiveSpeakerがオンの時は、人数変更はスキップ。
            if ($res["use_active_speaker"] != 1) {
                $seat_data = array(
                    "max_seat" => 9
                );
                $where = "room_key = '" . mysql_real_escape_string($room_key) . "'";
                $this -> obj_room -> update($seat_data, $where);
            }
        }
    }

    private function read_xml(){
        $request = $this -> request -> getAll();
        $rules = array(
            'n2my_session' => array('required' => true),
            'member_xml'   => array('required' => true),
            'output_type'  => array('allow' => $this -> get_output_type_list())
        );
        $err_obj = $this -> error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this -> display_error("100", "PARAMETER_ERROR", $this -> get_error_info($err_obj));
        }
        $member_xml = $this->one_get_xml(rawurldecode($request['member_xml']));
        if (!$member_xml) {
            return $this -> display_error("100", "PARAMETER_ERROR", "XML_ERROR");
        }
        //turn SimpleXML Object into String
        $member_data = json_decode(json_encode($member_xml), true);
        $members = $member_data['member'];
        if (!array_key_exists(0, $members)) {
            $tmp = $members;
            unset($members);
            $members[] = $tmp;
        }
        return $members;
    }

    private function is_member_exist($id) {
        //member, user, 認証DBのuser のテーブルをチェック
        if ($this -> obj_user -> numRows(sprintf("user_id='%s'", mysql_real_escape_string($id))) > 0 || $this -> obj_member -> numRows(sprintf("member_id='%s'", mysql_real_escape_string($id))) > 0 || $this -> obj_auth_user -> numRows(sprintf("user_id='%s'", mysql_real_escape_string($id))) > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function get_member_group_key($group_name, $user_key, $isCreate = false) {
        if (!$group_name || !$user_key) {
            return 0;
        }
        $where = sprintf("member_group_name='%s' AND user_key = '%s'", $group_name, $user_key);
        $group_key = $this -> obj_member_group -> getOne($where, "member_group_key");
        if ($group_key) {
            return $group_key;
        } elseif ($isCreate) {
            //add group
            $data = array();
            $data["member_group_name"] = trim($group_name);
            $data["user_key"] = $user_key;
            $key = $this -> obj_member_group -> add($data);
            return $key;
        } else {
            return 0;
        }

    }

    /**
     * 乱数ID発行
     */
    private function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a . $b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this -> dec2any($c);
    }

    /**
     * 数値から62進数のID発行
     */
    private function dec2any($num, $base = 57, $index = false) {
        if (!$base) {
            $base = strlen($index);
        } else if (!$index) {
            $index = substr('23456789ab' . 'cdefghijkm' . 'nopqrstuvw' . 'xyzABCDEFG' . 'HJKLMNPQRS' . 'TUVWXYZ', 0, $base);
        }
        $out = "";
        for ($t = floor(log10($num) / log10($base)); $t >= 0; $t--) {
            $a = floor($num / pow($base, $t));
            $out = $out . substr($index, $a, 1);
            $num = $num - ($a * pow($base, $t));
        }
        return $out;
    }

}

$main = new ONE_ADMIN_MEMBER_API();
$main -> execute();
?>