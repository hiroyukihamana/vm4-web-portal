<?php
require_once("classes/N2MY_Api.class.php");
class N2MY_Meeting_Auth_API extends N2MY_Api
{
    var $session_id = null;
    var $api_version = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
        //$this->getApiInterface();
    }

    function auth()
    {
        // ログイン以外の場合は認証させる
     }

    /**
     * API情報を返す
     */
    function action_get_info() {
        $this->logger2->info();
        $data = $this->getApiVersion();
        return $this->output($data);
    }

    function action_login(){
        $request = $this -> request -> getAll();
        // パラメタチェック
        $rules = array(
                'output_type'  => array('allow' => $this->get_output_type_list())
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this -> display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once("classes/mgm/MGM_Auth.class.php");
        require_once("classes/N2MY_Auth.class.php");
        require_once("classes/mgm/dbi/api_auth.dbi.php");
        // パラメタ取得
        $api_key     = $this->request->get("api_id");
        $id          = $this->request->get("id");
        $pw          = $this->request->get("pw");
        $output_type = $this->request->get("output_type");
        $req_data = array(
                "api_id" => $api_key,
                "id" => $id,
                "pw" => $pw,
                );
        $this->logger2->info($req_data);
        $session = EZSession::getInstance();
        $session->removeAll();
        // ID を取得
        $session_id = session_id();
        // ユーザ情報配置
        if  ($output_type) {
            $session->set("output_type", $output_type);
        }

        $this->session = $session;

        $objApiAuth = new ApiAuthTable(N2MY_MDB_DSN);
        $where = "api_key = '".$api_key."'" .
                " AND status = 1";
        $secret = $objApiAuth->getOne($where, 'secret');
        if(!$secret){
            $this->logger2->warn("api_key is Invalidity");
            return $this->display_error("1", "failed");
        }

//$this->add_operation_log("login");
//$test = hash('sha256', $pw);

        $config_id =  $this->config->get("ONE_ACCOUNT" , "id");
        $config_pw =  hash('sha256', $config_id.hash('sha256',$this->config->get("ONE_ACCOUNT" , "pw")).$secret);
        $data = array(
                "session" => $session_id,
        );
        if(!$config_id || !$config_pw){
            $this->logger2->warn("id & pw is Invalidity");
            return $this->display_error("1", "failed");
        }
//var_dump($pw);
        if($config_id == $id && $pw === $config_pw){
            $session->set("one_login", "1");
        }else{
            $this->logger2->warn("pw is Un Match");
            return $this->display_error("1", "failed");
        }

        $this->logger2->info("ONE Login Success");
        //server infoを取る
        $obj_MGMClass = new MGM_AuthClass( N2MY_MDB_DSN );
        $obj_DBserver = new MgmServerTable(N2MY_MDB_DSN);
        $where = sprintf( "host_name='%s' AND server_status=1", $this->get_dsn_key() );
        $server_key = $obj_DBserver->getOne($where, "server_key");
        if (!$server_info = $obj_MGMClass->getServerInfo( $server_key )){
        	$err_obj->set_error("", "SELECT_USER_ERROR", $id);
        	return $this->display_error("1", "SELECT_SERVER_ERROR");
        }
        $session->set("server_info", $server_info);

        return $this->output($data);
    }


}
$main = new N2MY_Meeting_Auth_API();
$main->execute();
