<?php
require_once("classes/N2MY_Api.class.php");
require_once("classes/ONE_Account.class.php");

class N2MY_MeetingAdminUserAPI extends N2MY_Api
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    var $obj_ONEAccount = null;

    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    function auth()
    {
        $this->checkAuthorizationOne();
        $this->obj_ONEAccount = new ONE_Account($this->get_dsn());
    }

    function action_add_user() {
        $request = $this->request->getAll();
        // パラメタチェック
        $rules = array(
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
            ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $user_data = (array)$this->one_get_xml(rawurldecode($this->request->get("user_xml")));
        $result = $this->obj_ONEAccount->add_one_user($user_data, $this->request->get('n2my_session'));
        if($result["status"] == "panameter_error") {
        $err_list = $this->get_error_info($result["err_obj"]);
        return $this->display_error(100, "PARAMETER_ERROR", $err_list);
        } else if($result["status"] == "error") {
        return $this->display_error(1000, "DATABASE_ERROR", "ADD FAILED, PLEASE TRY AGAIN.");
        } else {
            $data = array(
                "user_key" => $result["user_info"]["user_key"],
                "user_id" => $result["user_info"]["user_id"],
                "user_password" => $result["user_info"]["user_password"],
                "user_admin_password" => $result["user_info"]["user_admin_password"],
            );
        }
        return parent::one_output($data);
    }

    function action_edit_user() {
        $request = $this->request->getAll();
        // パラメタチェック
        $rules = array(
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
            ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $user_data = (array)$this->one_get_xml(rawurldecode($this->request->get("user_xml")));
        $this->logger2->info($user_data);
        $result = $this->obj_ONEAccount->edit_one_user($user_data, $this->request->get('n2my_session'));
        if($result["status"] == "panameter_error") {
            $err_list = $this->get_error_info($result["err_obj"]);
            return $this->display_error(100, "PARAMETER_ERROR", $err_list);
        } else if($result["status"] == "error") {
            return $this->display_error(1000, "DATABASE_ERROR", "ADD FAILED, PLEASE TRY AGAIN.");
        }
        return parent::one_output();
    }

    /**
     * user_id が 存在するか否か
     */
    public function action_check_user() {
        $request = $this->request->getAll();
        $rules = array (
            'user_id' => array (
                'required' => true
            ),
            'output_type' => array (
                'allow' => $this->get_output_type_list()
            )
        );
        $err_obj = parent::error_check( $request, $rules );
        if (EZValidator::isError( $err_obj )) {
            return parent::display_error( "100", "PARAMETER_ERROR", $this->get_error_info( $err_obj ) );
        }

        require_once 'classes/mgm/dbi/user.dbi.php';
        $dbi_mgm_user = new MgmUserTable( N2MY_MDB_DSN );
        $registered = 0 == $dbi_mgm_user->numRows( sprintf('user_id="%s"', addslashes($request ['user_id']) ) ) ? 0 : 1;
        return parent::one_output( array('registered' => $registered) );
    }

    /**
     * 指定された One契約している user_id を削除する
     */
    public function action_delete_user(){
        $request = $this->request->getAll();
        $rules = array (
            'user_id' => array (
                'required' => true
            ),
            'output_type' => array (
                'allow' => $this->get_output_type_list()
            )
        );
        $err_obj = parent::error_check( $request, $rules );
        if (EZValidator::isError( $err_obj )) {
            return parent::display_error( "100", "PARAMETER_ERROR", $this->get_error_info( $err_obj ) );
        }
        $user_id = $request ['user_id'];
        require_once ('classes/dbi/user.dbi.php');
        $obj_user = new UserTable( $this->get_dsn() );
        $where = sprintf( 'user_id = "%s"', addslashes( $user_id ) );
        $user_info = $obj_user->getRow( $where );
        if (! $user_info || !$this->obj_ONEAccount->is_one_account($user_info["account_plan"])) {
            return parent::display_error( "100", "user_id is not registed" );
        }
        $user_key = $user_info ['user_key'];
        $update = array ();
        $delete_user_id = sprintf( "%s_delete_%s", $user_id, date( "YmdHi" ) );
        $update ['user_id'] = $delete_user_id;
        $update ['user_delete_status'] = 2;
        $update ['user_deletetime'] = date( "Y-m-d H:i:s" );
        $obj_user->update( $update, sprintf( 'user_key = %d', $user_key ) );
        $obj_mgmUser = new MgmUserTable( N2MY_MDB_DSN );
        $obj_mgmUser->update( array (
            'user_id' => $delete_user_id
        ), sprintf( 'user_id="%s"', mysql_real_escape_string($user_id) ) );

        // MTGVFOUR-2335 one_user_queue に キューをためる場合
        $config = EZConfig::getInstance();
        if($config->get("N2MY","is_one_user_queue") == 1) {
            $this->logger2->info("Delete One User Add Queue. User_Key=" . $user_key);
            $ret = $this->obj_ONEAccount->userAddQueue($user_key,2);
            return parent::one_output();
        }
        
        require_once ('classes/dbi/member.dbi.php');
        $obj_member = new MemberTable( $this->get_dsn() );
        $member_array = $obj_member->getMemberList( $user_key );
        require_once 'classes/N2MY_Account.class.php';
        $obj_n2my_account = new N2MY_Account( $this->get_dsn() );
        foreach ( (array) $member_array as $member_info ) {
            $obj_n2my_account->deleteMember( $member_info['member_key'] );
        }
        return parent::one_output();
    }

    function action_set_room_mcu_server(){
        $request = $this->request->getAll();
        require_once("classes/mgm/dbi/mcu_server.dbi.php");
        $mcuServerTable   = new McuServerTable(N2MY_MDB_DSN);
        $mcu_host = $mcuServerTable->getActiveServerAddress($request["ives_mcu_server_key"]);
        if(!$mcu_host) {
            $msg = "No MCU server available where key is '" . $request["ives_mcu_server_key"] . "'.";
            $this->logger2->warn($msg);
            return false;
        }
        require_once("classes/dbi/room.dbi.php");
        $roomTable = new RoomTable($this->get_dsn());
        $roomInfos  = $roomTable->getRowsAssoc(sprintf( "room_status = 1 AND use_sales_option = 0 AND user_key='%s' AND room_name LIKE 'room%%'", $request["user_key"]));
        foreach ($roomInfos as $roomInfo){
            $accountInfo = $this->obj_ONEAccount->createSipAccountWithRetry($roomInfo, $request["ives_profile_id"]);
            if(!$accountInfo) {
                $msg = "Failed to create a SIP account.".$request["ives_profile_id"];
                $this->logger2->error($msg);
                return false;
            }
            require_once("classes/polycom/Polycom.class.php");
            $polycom = new PolycomClass($this->get_dsn());
            $polycom->setWsdlDomain($mcu_host);
            $did = $polycom->createDid();
            $vad = ($request["ives_use_active_speaker"] == 1) ? true : false;
            $result = $this->obj_ONEAccount->createAdHocConference($roomInfo, $did, $vad, $mcu_host);
            require_once ('classes/dbi/user.dbi.php');
            $userTable = new UserTable($this->get_dsn());
            $userInfo  = $userTable->getRow('user_key = ' . $roomInfo['user_key'], 'user_key, user_id, user_company_name');
            if(!$result){
                //$this->logger2->warn($data,"#IVES DB ERROR!");
                $mail_from = N2MY_ALERT_FROM;
                $mail_to   = N2MY_ALERT_TO;
                $roomInfo = array(
                    'room_key' => $roomInfo['room_key'],
                    'room_name'=> $roomInfo['room_name'],
                );
                $mail_body = "MCUと連携が失敗しました。\nログファイルから内容を確認して下さい。\n\n" . N2MY_LOCAL_URL. $_SERVER['SCRIPT_NAME'] .'?'.
                             __FUNCTION__ . "\n-----------------\n". print_r(array('request' => $request, 'user_info'=> $userInfo, 'room_info' => $roomInfo), true);
                $mail_subject = '部屋へMCUサーバーは連携失敗しました。';
                $this->sendReport($mail_from, $mail_to, $mail_subject, $mail_body);
                return false;
            }
            $data = array(
                'room_key'           => $roomInfo['room_key'],
                'mcu_server_key'     => $request["ives_mcu_server_key"],
                'user_key'           => $userInfo["user_key"],
                'ives_did'           => $did,
                'sip_uid'            => $accountInfo["uid"],
                'client_id'          => $accountInfo["name"],
                'client_pw'          => $accountInfo["secret"],
                'profile_id'         => $request["ives_profile_id"],
                'num_profiles'       => ($roomInfo["max_seat"] > 20) ? 20 : $roomInfo["max_seat"],
                'use_active_speaker' => $request["ives_use_active_speaker"],
                'mcu_hd_flg'         => $request["ives_use_hd_profile"],
                'startdate'          => date("Y-m-d H:i:s"),
                'registtime'         => date("Y-m-d H:i:s")
            );
            require_once("classes/dbi/ives_setting.dbi.php");
            $ivesSettingTable = new IvesSettingTable($this->get_dsn());
            $res = $ivesSettingTable->add($data);
            if (!DB::isError($res)) {
                $data = array(
                    "room_key"                          => $roomInfo['room_key'],
                    "user_service_option_key"           => 0,
                    "service_option_key"                => 24,
                    "ordered_service_option_status"     => 1,
                    "ordered_service_option_registtime" => date("Y-m-d H:i:s"),
                    "ordered_service_option_starttime"  => date("Y-m-d H:i:s"),
                );
                require_once("classes/dbi/ordered_service_option.dbi.php");
                $orderServiceTable = new OrderedServiceOptionTable($this->get_dsn());
                $orderServiceTable->add($data);
            }else{
                $this->logger2->error('IVES OPTION ADD FAILED');
                return false;
            }
        }
    }

}
$main = new N2MY_MeetingAdminUserAPI();
$main->execute();