<?php

require_once("classes/N2MY_DBI.class.php");
require_once "classes/dbi/room.dbi.php";
require_once('classes/mgm/MGM_Auth.class.php');
require_once('lib/EZLib/EZCore/EZLogger2.class.php');
require_once('lib/EZLib/EZHTML/EZValidator.class.php');
require_once('lib/EZLib/EZUtil/EZEncrypt.class.php');

class vcubeId {
   var $auth_dsn    = null;
   var $dsn         = null;
   var $server_info = null;
   var $user_info   = null;

  //private $purgeLimit = Constants::PURGE_LIMIT;
  public function __construct($auth_dsn, $userId){


    $this->logger2 = EZLogger2::getInstance($this->config);

    $this->auth_dsn = $auth_dsn;

    $this->logger2->info($this->dsn);

  }

  //初期設定
  private function init($user_id = null)
  {
    try{
      $obj_MGMClass = new MGM_AuthClass( $this->auth_dsn);

      $this->user_info  = $obj_MGMClass->getUserInfoById($user_id);
      $this->server_info      = $obj_MGMClass->getServerInfo( $this->user_info["server_key"]);
       //dsn
      $this->dsn    = $this->server_info["dsn"];

      if(!$this->dsn || $this->user_info["vcubeid_add_use_flg"]){
        return false;
      }

      return true;
    }catch(Exception $e){
      $this->logger2->error($e->getMessage());
      return false;
    }

  }

  public function testFunc($user_id, $consumerKey, $redirectUrl)
  {
    $this->logger2->info("testFunc API start");
    try{
      if(!$this->init($user_id)){
        $this->logger2->error("user not found");
        throw new Exception("user not Found");
      }

      $this->logger2->info($user_id);
      $this->logger2->info($consumerKey);
      $this->logger2->info($redirectUrl);


      $ret["result"] = true;
      $ret["user_id"]     = $user_id;
      $ret["consumerKey"] = $consumerKey;
      $ret["redirectUrl"] = $redirectUrl;

    }catch(Exception $e){
      $ret["result"]      = false;
      $ret["user_id"]     = $user_id;
      $ret["consumerKey"] = $consumerKey;
      $ret["message"]     = $e->getMessage();
    }
    return $ret;
  }


  //メンバ追加
  public function addMember(
                            $userId = null,
                            $id = null,
                            $pw = null,
                            $name = null,
                            $nameKana = null,
                            $email = null,
                            $statusKey = 0,
                            $type = "",
                            $groupKey = 0,
                            $roomKey = null,
                            $timezone = null,
                            $lang = null
                          )
  {
    $this->logger2->info("add Member API start");
    $this->logger2->info($id);
    try{
      if(!$this->init($userId)){
        $this->logger2->error("user not found");
        throw new Exception("user not Found");
      }

      //詰め替え
      $registDate["userId"]          = $userId;
      $registDate["memberId"]        = $id;
      $registDate["memberPw"]        = $pw;
      $registDate["memberName"]      = $name;
      $registDate["memberNameKana"]  = $nameKana;
      $registDate["memberMail"]      = $email;
      $registDate["memberStatusKey"] = $statusKey;
      $registDate["memberType"]      = $type;
      $registDate["memberStatusKey"] = $groupKey;
      $registDate["roomKey"]         = $roomKey;
      $registDate["timezone"]        = $timezone;
      $registDate["lang"]            = $lang;

      //必要情報取得
      $user_db = new N2MY_DB( $this->dsn, "user" );
      $user = $user_db->getRow("user_id='".mysql_real_escape_string($this->user_info["user_id"])."'");

      $this->logger2->info($user);

      $member_db = new N2MY_DB($this->dsn, "member");
      $member_list = $member_db->getRowsAssoc( sprintf( "user_key ='%s' and member_status > -1", $user['user_key'] ) );


      //バリデーション
      $ret = $this->memberValidation($registDate, $user, $member_list);
      $this->logger2->info($ret);
      if($ret["result"] === false){
        return $ret;
      }

      //追加実行
      $ret = $this->addMemberFunc($registDate, $user, $member_list);

      return $ret;

    }catch(Exception $e){
      $this->logger2->error($e->getMessage());
      $ret["result"]   = false;
      $ret["userId"]   = $userId;
      $ret["memberId"] = $id;
      $ret["message"]  = $e->getMessage();
      return $ret;
    }

  }

  private function memberValidation($data, $user, $member_list){
    $this->logger2->info("member validation");
    //バリデート
    $message = array();

    //メンバーID
    if (!preg_match('/^[[:alnum:]@+._-]{3,255}$/', $data["memberId"])) {
    $this->logger2->info($data["memberId"]);
        $message[] = USER_ERROR_ID_LENGTH.' ';
    } elseif($this->_isExistMember($data["memberId"])){
        $message[] = MEMBER_ERROR_ID_EXIST.' ';
    }

    //member password
    if (!$data["memberPw"]) {
        $message[] = MEMBER_ERROR_PASS . ' ';
    //ユーザーパスワードをチェック
    } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $data["memberPw"])) {
        $message[] = USER_ERROR_PASS_INVALID_01. ' ';
    } elseif (!preg_match('/[[:alpha:]]+/',$data["memberPw"]) || preg_match('/^[[:alpha:]]+$/',$data["memberPw"])) {
        $message[] = USER_ERROR_PASS_INVALID_02. ' ';
    }

    //メンバー名
    if(!$data["memberName"]){
       $message[] = MEMBER_ERROR_NAME.' ';

    } elseif (mb_strlen($data["memberName"]) > 30) {
       $message[] = MEMBER_ERROR_NAME_LENGTH.' ';
    }

    //メンバー名カナ
    if (mb_strlen($data["menberNameKana"]) > 30) {
       $message[] = MEMBER_ERROR_NAME_KANA_LENGTH.' ';
    }


    // メンバー課金の場合のみ必須
    //メンバーメイル
    if($user["account_model"] == "member" && !$data["memberMail"]){
      $message[] = MEMBER_ERROR_EMAIL.' ';
    }

    if ($data["memberMail"] != "") {
      if(!EZValidator::valid_email($data["memberMail"])){
        $message[] = MEMBER_ERROR_EMAIL_INVALID.' ';
      }
    }


    //メンバー数上限チェック
    if( ($user["account_model"] == "member" || $user["account_model"] == "centre") &&
      $user["max_member_count"] <= count( $member_list ) ) {
        $message[] = MEMBER_ERROR_OVER_COUNT.' ';
    }

    if(count($message) == 0){
      $ret["result"]   = true;
      $ret["userId"]   = $data["userId"];
      $ret["memberId"] = $data["memberId"];
      $ret["message"]  = $message;
    }else{
      $ret["result"]   = false;
      $ret["userId"]   = $data["userId"];
      $ret["memberId"] = $data["memberId"];
      $ret["message"]  = $message;
    }


    return $ret;
    //バリデートend
  }

  protected function addMemberFunc( $data, $user, $member_list)
  {
   $this->logger2->info("add member fuction");
   //メンバー登録

   try{
     $user_key = $user['user_key'];

     $temp = array(
         "user_key"         => $user['user_key'],
         "member_id"        => $data["memberId"],
         "member_pass"      => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $data["memberPw"]),
         "member_email"     => $data["memberMail"],
         "member_name"      => $data["memberName"],
         "member_name_kana" => $data["memberNameKana"],
         "member_status"    => (($data["memberStatusKey"]) ? $data["memberStatusKey"] : 0),
         "member_type"      => (($data["memberType"]) ? $data["memberType"] : ""),
         "member_group"     => (($data["memberGroupKey"]) ? $data["memberGroupKey"] : 0),
         "room_key"         => $data["roomKey"],
         "timezone"         => $data["timezone"],
         "lang"             => $data["lang"],
         "create_datetime"  => date("Y-m-d H:i:s"),
         "update_datetime"  => date("Y-m-d H:i:s"),
     );

     $member_db = new N2MY_DB($this->dsn, "member");
     $ret = $member_db->add( $temp );
     if (DB::isError($ret)) {
         $this->logger2->error(__FILE__.__LINE__.":".__FUNCTION__."#DB ERROR!");
         $this->logger2->error($temp);
         throw new Exception("database error");
     } else {
         $this->logger2->info(__FUNCTION__."#add member successful!");
     }

     $member_info = $member_db->getRow("member_id = '".addslashes($data["memberId"])."'");

     //認証サーバーへ追加
     $obj_MgmUserTable = new N2MY_DB( $this->auth_dsn, "user" );
     $auth_data = array(
         "user_id"     => $data["memberId"],
         "server_key"  => $this->user_info["server_key"]
         );
     $result = $obj_MgmUserTable->add( $auth_data );
     if (DB::isError($result)) {
         $this->logger2->error(__FILE__.__LINE__.":".__FUNCTION__."#DB ERROR!");
         throw new Exception("database error");

     } else {
         $this->logger2->info(__FUNCTION__."#add member successful!");
     }

     $ret["result"]   = true;
     $ret["userId"]   = $data["userId"];
     $ret["memberId"] = $data["memberId"];
     $ret["message"]  = "add member complete";
   }catch(Exception $e){

     $ret["result"]   = false;
     $ret["userId"]   = $data["userId"];
     $ret["memberId"] = $data["memberId"];
     $ret["message"]  = $e->getMessage();

     return $ret;
   }

   return $ret;
  }

  public function getRoom(
                          $userId,
                          $memberId
                          )
  {
    $this->logger2->info("get Room API start");
    try{

      if(!$this->init($userId)){
        $this->logger2->error("user not found");
        throw new Exception("user not Found");
      }

      //ユーザー情報
      $where = "user_id='".addslashes($this->user_info["user_id"])."'";
      $user_db   = new N2MY_DB($this->dsn, "user");
      $user_data = $user_db->getRow($where);

      if($memberId){  //メンバーIDでしぼる
        //メンバー情報
        $where = "member_id='".mysql_real_escape_string($memberId)."'";
        $user_db   = new N2MY_DB($this->dsn, "member");
        $member_data = $user_db->getRow($where);

        if(!$member_data){
          $this->logger2->info("member not found");
          throw new Exception("member not Found");
        }
        //メンバーの部屋情報
        $room_db = new N2MY_DB($this->dsn, "room");
        $rSql  = "SELECT ";
        $rSql .= "member.member_id, ";
        $rSql .= "room.*, ";
        $rSql .= "room_plan.* ";
        $rSql .= "FROM ";
        $rSql .= "member, member_room_relation, room LEFT JOIN room_plan on ( room.room_key = room_plan.room_key ) ";
        $rSql .= "WHERE ";
        $rSql .= "member.member_id = '".mysql_real_escape_string($memberId)."' ";
        $rSql .= "AND member.member_key = member_room_relation.member_key ";
        $rSql .= "AND member_room_relation.room_key = room.room_key ";

        $this->logger2->info($rSql);

        $rs = $room_db->_conn->query($rSql);

      }else{  //ユーザーID全体
        //部屋情報
        $room_db = new N2MY_DB($this->dsn, "room");
        $rSql  = "SELECT ";
        $rSql .= "user.user_id, ";
        $rSql .= "room.*, ";
        $rSql .= "room_plan.* ";
        $rSql .= "FROM ";
        $rSql .= "user, room LEFT JOIN room_plan on ( room.room_key = room_plan.room_key ) ";
        $rSql .= "WHERE ";
        $rSql .= "user.user_id = '".mysql_real_escape_string($userId)."' ";
        $rSql .= "AND user.user_key = room.user_key ";

        $this->logger2->info($rSql);

        $rs = $room_db->_conn->query($rSql);
      }

      $osOption_db = new N2MY_DB($this->dsn, "ordered_service_option");

      $rooms = array();
      while( $room = $rs->fetchRow( DB_FETCHMODE_ASSOC ) ){
         //サービス詳細取得
         $this->initServices();
         foreach($this->services as $service){
          if($room["service_key"] == $service["service_key"]){
            $room["service"] = $service;
            break;
          }
         }

         //order_service_option
         $osOptions = $osOption_db->getRowsAssoc(sprintf("room_key = '%s' ", $room["room_key"]), array(), null, 0, "ordered_service_option_key,service_option_key");

         $tmpOsOptions = array();
         foreach($osOptions as $osOption){
          foreach($this->sOptions as $sOption){
           if($osOption["service_option_key"] == $sOption["service_option_key"]){
             $osOption["service_option"] = $sOption;
             break;
           }
          }

          $tmpOsOptions[] = $osOption;
         }

         $room["ordered_serivce_option"] = $tmpOsOptions;

         $rooms[] = $room;
      }


      $ret["result"]  = true;
      $ret["userId"]   = $userId;
      $ret["memberId"] = $memberId;
      $ret["rooms"]   = $rooms;
      $ret["message"] = "success";

      return $ret;

    }catch(Exception $e){
      $this->logger2->error($e->getMessage());
      $ret["result"]   = false;
      $ret["userId"]   = $userId;
      $ret["memberId"] = $memberId;
      $ret["message"]  = $e->getMessage();
      return $ret;
    }


  }

  public function addRoom(
                          $userId,
                          $roomName            = null,
                          $serviceKey          = null,
                          $discountRate        = 0,
                          $roomPlanYearly      = null,
                          $roomPlanStarttime   = null,
                          $roomPlanEndtime     = null,
                          $contractMonthNumber = null
                          )
  {
    $this->logger2->info("add Room API start");

    try{

      if(!$this->init($userId)){
        $this->logger2->error("user not found");
        throw new Exception("user not Found");
      }

      $data["userId"]              = $userId;
      $data["roomName"]            = $roomName;
      $data["serviceKey"]          = $serviceKey;
      $data["discountRate"]        = $discountRate;
      $data["roomPlanYearly"]      = $roomPlanYearly;
      $data["roomStarttime"]       = $roomPlanStarttime;
      $data["roomPlanEndtime"]     = $roomPlanEndtime;
      $data["contractMonthNumber"] = $contractMonthNumber;

      //validation
      $ret = $this->roomValidation($data);
      if($ret["result"]  === false){
        return $ret;
      }else{
        //成形されたデータに上書き
        $data = $ret["data"];
      }
      //部屋登録
      $ret = $this->addRoomFunc($data);

      return $ret;

    }catch(Exception $e){
      $this->logger2->error($e->getMessage());
      $ret["result"]  = false;
      $ret["userId"]  = $userId;
      $ret["roomKey"]  = "";
      $ret["message"] = $e->getMessage();
      return $ret;
    }


  }

  private function roomValidation($data){
    //バリデート
    $this->logger2->info("add Room Validation");
    if (!$data["serviceKey"]) {
        $message[] = "serviceKeyを入力してください。";
    }else{
      $service_db = new N2MY_DB($this->auth_dsn, "service");
      $service_info = $service_db->getRow("service_key = ".$data["serviceKey"]);
      if(!$service_info){
        $message[] = "存在しないserviceKeyです。";
      }
    }


    if (!$data["roomName"]) {
        $message[] = "部屋名を入力してください。";
    }
    if ( !$data["discountRate"]) {
      $data["discountRate"] = 0;
    } else if (!is_numeric($data["discountRate"])) {
      $message[] = "割引率は数値で入力してください。";
    }
    if ($data["roomPlanYearly"]) {
      if (!$data["roomPlanEndtime"]) {
        $data["roomPlanEndtime"] = "0000-00-00 00:00:00";
      }
      if (!$data["contractMonthNumber"]) {
        $message[] = "契約期間（月数）を選択してください。";
      }
    } else {
      $data["roomPlanEndtime"] = "0000-00-00 00:00:00";
      $data["contractMonthNumber"] = "0";
    }

    if (!$data["roomStarttime"]) {
        $data["roomStarttime"] = date("Y-m-d 00:00:00");
    }

    //バリデーションエラー

    if ( count($message) > 0 ) {
      $ret["result"]   = false;
      $ret["userId"]   = $data["userId"];
      $ret["roomKey"]  = "";
      $ret["data"]     = $data;
      $ret["message"]  = $message;

    }else{
      $ret["result"]   = ture;
      $ret["userId"]   = $data["userId"];
      $ret["roomKey"]  = "";
      $ret["data"]     = $data;
      $ret["message"]  = $message;
    }

    return $ret;
  }

  protected function addRoomFunc($data)
  {
    $this->logger2->info("add Room Func start");

    try{


      //ユーザー情報
      $where = "user_id='".addslashes($this->user_info["user_id"])."'";
      $user_db   = new N2MY_DB($this->dsn, "user");
      $user_data = $user_db->getRow($where);

      //部屋情報
      $where = "user_key='".addslashes($user_data["user_key"])."'";
      $room_db = new RoomTable($this->dsn);
      $count   = $room_db->numRows($where);

      $room_key = $user_data["user_id"]."-".($count + 1)."-".substr(md5(uniqid(time(), true)), 0,4);
      $room_sort = $count + 1;

      //部屋登録
      $room_data = array (
          "room_key"            => $room_key,
          "room_name"           => $data["roomName"],
          "user_key"            => $user_data["user_key"],
          "room_sort"           => $room_sort,
          "room_status"         => "1",
          "use_teleconf"        => "1",
          "use_pgi_dialin"      => "1",
          "use_pgi_dialin_free" => "1",
          "use_pgi_dialout"     => "1",
          "room_registtime"     => date("Y-m-d H:i:s"),
          );
      $add_room = $room_db->add($room_data);
      if (DB::isError($add_room)) {
          $this->logger2->error(__FILE__.__LINE__.":".__FUNCTION__."#DB ERROR!");
          throw new Exception("database error");
      }

      //account.relation 登録
      $relation_db = new N2MY_DB($this->auth_dsn, "relation");
      $relation_data = array(
          "relation_key"  => $room_key,
          "relation_type" => "mfp",
          "user_key"      => $this->user_info["user_id"],
          "status"        => "1",
          "create_datetime" => date("Y-m-d H:i:s"),
          );

      $relation_db->add($relation_data);


      //ルームプラン登録
      $planData = array();
      $planData["service_key"]           = $data["serviceKey"];
      $planData["room_starttime"]        = $data["roomStarttime"];
      $planData["room_plan_endtime"]     = $data["roomPlanEndtime"];
      $planData["discount_rate"]         = $data["discountRate"];
      $planData["contract_month_number"] = $data["contractMonthNumber"];

      $this->addRoomPlan($room_key, $planData);

      $ret["result"]   = true;
      $ret["userId"]   = $data["userId"];
      $ret["roomKey"]  = $room_key;
      $ret["message"]  = "complete";

    }catch(Exception $e){
      $ret["result"]   = false;
      $ret["userId"]   = $data["userId"];
      $ret["roomKey"]  = $room_key;
      $ret["message"]  = $e->getFile().":".$e->getLine()." ".$e->getMessage();

    }


    return $ret;
  }


  public function addMemberAndRoom(
                                   $userId = null,
                                   $id = null,
                                   $pw = null,
                                   $name = null,
                                   $nameKana = null,
                                   $email = null,
                                   $statusKey = 0,
                                   $type = "",
                                   $groupKey = 0,
                                   $timezone = null,
                                   $lang = null,
                                   $roomName,
                                   $serviceKey,
                                   $discountRate,
                                   $roomPlanYearly,
                                   $roomPlanStarttime,
                                   $roomPlanEndtime,
                                   $contractMonthNumber
                                   )
  {
    $this->logger2->info("add MemberAndRoom API start");

    try{

      if(!$this->init($userId)){
        $this->logger2->error("user not found");
        throw new Exception("user not Found");
      }

      //必要情報取得
      $user_db = new N2MY_DB( $this->dsn, "user" );
      $user = $user_db->getRow("user_id='".mysql_real_escape_string($this->user_info["user_id"])."'");

      $this->logger2->info($user);

      $member_db = new N2MY_DB($this->dsn, "member");
      $member_list = $member_db->getRowsAssoc( sprintf( "user_key ='%s' and member_status > -1", $user['user_key'] ) );

      $roomData["userId"]              = $userId;
      $roomData["roomName"]            = $roomName;
      $roomData["serviceKey"]          = $serviceKey;
      $roomData["discountRate"]        = $discountRate;
      $roomData["roomPlanYearly"]      = $roomPlanYearly;
      $roomData["roomStarttime"]       = $roomPlanStarttime;
      $roomData["roomPlanEndtime"]     = $roomPlanEndtime;
      $roomData["contractMonthNumber"] = $contractMonthNumber;


      $memberData["userId"]          = $userId;
      $memberData["memberId"]        = $id;
      $memberData["memberPw"]        = $pw;
      $memberData["memberName"]      = $name;
      $memberData["memberNameKana"]  = $nameKana;
      $memberData["memberMail"]      = $email;
      $memberData["memberStatusKey"] = $statusKey;
      $memberData["memberType"]      = $type;
      $memberData["memberStatusKey"] = $groupKey;
      $memberData["roomKey"]         = "";
      $memberData["timezone"]        = $timezone;
      $memberData["lang"]            = $lang;
      $this->logger2->info($memberData);
      //validation
      //room
      $ret = $this->roomValidation($roomData);

      if($ret["result"]  === false){
        return $ret;
      }else{
        //成形されたデータに上書き
        $roomData = $ret["data"];
      }

      //member
      $ret = $this->memberValidation($memberData, $user, $member_list);
      if($ret["result"] === false){
        return $ret;
      }

      //部屋追加
      $rRet = $this->addRoomFunc($roomData);

      if($rRet["result"] == false){
        $this->logger2->info("add room error");
        throw new Exception("add room error");
      }

      //メンバー追加
      $mRet = $this->addMemberFunc($memberData, $user, $member_list);
      if($mRet["result"] == false){
        throw new Exception("add member error");
      }
      $this->logger2->info("add member complete");

      //member_room_relation
      $mrrRet = $this->addMemberRoomRelationFunc($userId, $memberData["memberId"], $rRet["roomKey"]);

      if($mrrRet["result"] == false){
        throw new Exception("add relation error");
      }

      $this->logger2->info("add relation complete");

      $ret["result"]   = true;
      $ret["userId"]   = $userId;
      $ret["memberId"] = $id;
      $ret["massage"]  = "complete";


    }catch(Exception $e){

      $this->logger2->error($e->getMessage());

      $ret["result"]   = false;
      $ret["userId"]   = $userId;
      $ret["memberId"] = $id;
      $ret["massage"]  = $e->getMessage();
    }

    return $ret;
  }

  public function addMemberRoomRelation(
                                   $userId    = null,
                                   $memberId  = null,
                                   $roomKey   = null
                                   )
  {
    $this->logger2->info("addMemberRoomRelation API start");

    try{

      if(!$this->init($userId)){
        $this->logger2->error("user not found");
        throw new Exception("user not Found");
      }

      //data.user取得
      $where = "user_id='".addslashes($this->user_info["user_id"])."'";
      $user_db   = new N2MY_DB($this->dsn, "user");
      $user_data = $user_db->getRow($where);

      //バリデーション
      $ret = $this->memberRoomRelationValidation($user_data["user_key"], $memberId, $roomKey);
      $this->logger2->info($ret);
      if($ret["result"] === false){
        return $ret;
      }

      //リレーション追加
      $ret = $this->addMemberRoomRelationFunc($user_data["user_key"], $memberId, $roomKey);

      return $ret;
    }catch(Exception $e){
      $this->logger2->error($e->getMessage());
      $ret["result"]  = false;
      $ret["userId"]  = $userId;
      $ret["memberRoomRelationId"]  = "";
      $ret["message"] = $e->getMessage();
      return $ret;
    }


  }

  public function memberRoomRelationValidation(
                                   $user_key    = null,
                                   $memberId  = null,
                                   $roomKey   = null
                                   )
  {

    //メンバーキーが有効か
    if(is_null($memberId)){
      $message[] = "memberKey は必須です。";
    }elseif(!$this->member_exists($user_key, $memberId)){
      $message[] = "memberKey が登録されてない、または無効です";
    }

    //ルームキーが有効か
    if(is_null($roomKey)){
      $message[] = "memberKey は必須です。";
    }elseif(!$this->room_exists($user_key, $roomKey)){
      $message[] = "roomKey が登録されてない、または無効です";
    }

    if(count($message) > 0){
      $ret["result"]  = false;
      $ret["userId"]  = $userId;
      $ret["memberRoomRelationId"]  = "";
      $ret["message"] = $message;
    }else{
      $ret["result"]  = true;
      $ret["userId"]  = $userId;
      $ret["memberRoomRelationId"]  = "";
      $ret["message"] = $message;
    }

    return $ret;

  }


  protected function addMemberRoomRelationFunc($user_key, $memberId = null, $roomKey = null)
  {
    //バリデーション
    $message = array();



    try{


      //member_key取得
      $member_db = new N2MY_DB($this->dsn, "member");
      $member = $member_db->getRow( sprintf("user_key = '%s' AND member_id='%s'", $user_key, mysql_real_escape_string($memberId) ) );

      $mrRation_db = new N2MY_DB($this->dsn, "member_room_relation");
      $mrr = $mrRation_db->getRow(sprintf("member_key = '%s' AND room_key = '%s'", $member["member_key"], $roomKey));
      if(!$mrr){

        $relationData = array();
        $relationData["member_key"]      = $member["member_key"];
        $relationData["room_key"]        = $roomKey;
        $relationData["create_datetime"] = date("Y-m-d H:i:s");


        $res = $mrRation_db->add($relationData);

        if (PEAR::isError($res)) {
            $this->logger2->info($res->getMessage());
            $this->logger2->error(__FILE__.__LINE__.":".__FUNCTION__."#DB ERROR!");
            throw new Exception("database error");
        }

      }

      $ret["result"]   = true;
      $ret["userId"]   = $userId;
      $ret["memberId"] = $memberId;
      $ret["roomKey"]  = $roomKey;
      $ret["message"]  = "complete";


    }catch(Exception $e){
      $ret["result"]   = false;
      $ret["userId"]   = $userId;
      $ret["memberId"] = $memberId;
      $ret["roomKey"]  = $roomKey;
      $ret["message"]  = $e->getFile().":".$e->getLine()." ".$e->getMessage();
    }

    return $ret;
  }

  /**
   * ルームプラン登録（プランに合わせて人数、オプションも登録）
   */
  function addRoomPlan($room_key, $data) {

    $this->logger2->info("add room plan start");

    $service_key = $data["service_key"];
    $start_time = $data["room_starttime"];
    $end_time = $data["room_plan_endtime"];
    $discount_rate = $data["discount_rate"];
    $contract_month_number = $data["contract_month_number"];
    $today = date("Y-m-d 23:59:59");

    if (strtotime($start_time) <= strtotime($today)) {
    $this->logger2->info("add room plan start1");
        //プラン登録
        $room_plan_db = new N2MY_DB($this->dsn, "room_plan");
        $room_plan = array (
            "room_key" => $room_key,
            "service_key" => $service_key,
            "room_plan_status" => "1",
            "room_plan_starttime" => $start_time,
            "room_plan_endtime" => $end_time,
            "discount_rate" => $discount_rate,
            "contract_month_number" => $contract_month_number,
            "room_plan_registtime" => date("Y-m-d H:i:s"),
            );
        $add_plan = $room_plan_db->add($room_plan);
        if (DB::isError($add_plan)) {
            $this->logger2->info( $add_plan->getUserInfo());
        }
        $service_db = new N2MY_DB($this->auth_dsn, "service");
        $service_info = $service_db->getRow("service_key = ".$service_key);
        $this->logger2->info($service_key);

        //シート登録
        $room_db = new N2MY_DB($this->dsn, "room");

        //オーディエンスオプション数で人数変更
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable($this->dsn);
        if ($service_info["max_audience_seat"] == "0") {
            $where = "room_key = '".addslashes($room_key)."'".
                     " AND service_option_key = 7".
                     " AND ordered_service_option_status = 1";
            $audience_count = $ordered_service_option->numRows($where);

            $this->logger2->trace("audience",__FILE__,__LINE__,$audience_count);

            if ($audience_count > 0 ) {
                $max_audience = $audience_count."0";
            } else {
                $max_audience = $service_info["max_audience_seat"];
            }
        } else {
            $max_audience = $service_info["max_audience_seat"];
        }

        $max_whiteboard = $service_info["max_whiteboard_seat"];
        $where = "room_key = '".addslashes($room_key)."'";
        $room_seat = array (
            "max_seat" => $service_info["max_seat"],
            "max_audience_seat" => $max_audience,
            "max_whiteboard_seat" => $max_whiteboard,
            "meeting_limit_time" => $service_info["meeting_limit_time"],
            "ignore_staff_reenter_alert" => $service_info["ignore_staff_reenter_alert"],
            "default_camera_size" => $service_info["default_camera_size"],
            "hd_flg"                => $service_info["hd_flg"],
            "disable_rec_flg"       => $service_info["disable_rec_flg"],
            "room_updatetime" => date("Y-m-d H:i:s"),
        );
        if ($service_info["service_name"] == "Paperless_Free" || $service_info["service_name"] == "Meeting_Free") {
            $room_seat["whiteboard_page"] = "10";
            $room_seat["whiteboard_size"] = "2";
            $whiteboard_filetype = array(
                "document" => array("ppt","pdf"),
                "image"    => array("jpg"),
            );
            $room_seat["whiteboard_filetype"] =serialize($whiteboard_filetype);
        }
        $add_seat = $room_db->update($room_seat, $where);

        //プランのオプション情報取得
        $add_options = array();
        if ($service_info["meeting_ssl"] == 1) {
            $add_options[] = "2";
        }
        if ($service_info["desktop_share"] == 1) {
            $add_options[] = "3";
        }
        if ($service_info["high_quality"] == 1) {
            $add_options[] = "5";
        }
        if ($service_info["mobile_phone"] > 0) {
            $add_options[] = "6";
        }
        if ($service_info["h323_client"] > 0) {
            $add_options[] = "8";
        }
        //hdd_extentionは数値分登録
        if ($service_info["hdd_extention"] > 0) {
            $add_options[] = "4";
        }
        // ホワイトボードプラン
        if ($service_info["whiteboard"] == 1) {
            $add_options[] = "16";
        }
        // マルチカメラ
        if ($service_info["multicamera"] == 1) {
            $add_options[] = "18";
        }
        // 電話連携
        if ($service_info["telephone"] == 1) {
            $add_options[] = "19";
        }
        // 録画GW
        if ($service_info["record_gw"] == 1) {
            $add_options[] = "20";
        }
        // スマートフォン
        if ($service_info["smartphone"] == 1) {
            $add_options[] = "21";
        }
        // 資料共有映像再生許可
        if ($service_info["whiteboard_video"] == 1) {
            $add_options[] = "22";
        }
        //オプション登録
        if ($add_options) {
            foreach ($add_options as $value) {
                $where = "room_key = '".addslashes($room_key)."'".
                         " AND service_option_key = ".$value.
                         " AND ordered_service_option_status = 1";
                $count = $ordered_service_option->numRows($where);
                $this->logger2->debug($count);
                if ($count == 0 || "4" == $value || "6" == $value || "8" == $value) {
                    if ("4" == $value) {
                        $this->logger2->info($value);
                        for ($num = 1; $num <= $service_info["hdd_extention"]; $num++ ) {
                            $this->addRoomOption($room_key, $value, null);
                        }
                    } else if ("6" == $value) {
                        $this->logger2->info($value);
                        for ($num = 1; $num <= $service_info["mobile_phone"]; $num++ ) {
                            $this->addRoomOption($room_key, $value, null);
                        }
                    } else if ("8" == $value) {
                        $this->logger2->info($value);
                        for ($num = 1; $num <= $service_info["h323_client"]; $num++ ) {
                            $this->addRoomOption($room_key, $value, null);
                        }
                    } else {
                        $this->addRoomOption($room_key, $value, null);
                    }
                }
            }
            if (DB::isError($add_seat)) {
                $this->logger2->info( $add_seat->getUserInfo());
            }
        }
        //プレミアムプランへの変更時はオーディエンスオプションを停止
        if ($service_info["max_audience"] != 0) {
            $where = "room_key = '".addslashes($room_key)."'".
                         " AND service_option_key = 7".
                         " AND ordered_service_option_status = 1";
            $data = array (
                "ordered_service_option_status" => "0"
            );
            $oudience_option = $ordered_service_option->update($data, $where);
        }
    } else {
        //プラン登録
        $room_plan_db = new N2MY_DB($this->dsn, "room_plan");
            $room_plan = array (
            "room_key" => $room_key,
            "service_key" => $service_key,
            "room_plan_status" => "2",
            "room_plan_starttime" => $start_time,
            "room_plan_endtime" => $end_time,
            "discount_rate" => $discount_rate,
            "contract_month_number" => $contract_month_number,
            "room_plan_registtime" => date("Y-m-d H:i:s"),
        );

        $add_plan = $room_plan_db->add($room_plan);

        if (DB::isError($add_plan)) {
            $this->logger2->info( $add_plan->getUserInfo());
        }
    }

    $this->logger2->info("addRoomPlan end");
    return true;
  }


  //オプション登録
  function addRoomOption($room_key, $service_option_key, $option_start_time) {
      require_once("classes/dbi/ordered_service_option.dbi.php");
      $ordered_service_option = new OrderedServiceOptionTable($this->dsn);
      $this->logger2->trace("start_time",__FILE__,__LINE__,$option_start_time);
      if (!$option_start_time) {
          $option_start_time = date("Y-m-d H:i:s");
      }

      if ($service_option_key == 23) {// teleconference
          require_once("classes/dbi/pgi_setting.dbi.php");
          $pgi_setting_table = new PGiSettingTable($this->dsn);

          $month_first_day = substr($option_start_time ,0 ,7);
          $pgi_setting = $pgi_setting_table->findEnablleAtYM($room_key, $month_first_day);
          if (count($pgi_setting) <= 0) {
               $this->action_room_detail('', 'teleconferenceのオプションを付与するには、'.$month_first_day.'以降のPGi設定を作成する必要があります');
               exit;
          }
      }

      $today = date("Y-m-d 23:59:59");
      if (strtotime($option_start_time) >= strtotime($today))  {
          $status = "2";
      } else {
          $status = "1";
      }
      $data = array(
          "room_key" => $room_key,
          "service_option_key" => $service_option_key,
          "ordered_service_option_status" => $status,
          "ordered_service_option_starttime" => $option_start_time,
          "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
          "ordered_service_option_deletetime" => "0000-00-00 00:00:00",
          );

      //オーディエンスオプションの際は部屋の人数変更
      if ($service_option_key == 7) {
          $where = "room_key = '".addslashes($room_key)."'".
                   " AND room_plan_status = 1";
          $room_plan_db = new N2MY_DB($this->dsn, "room_plan");
          $room_plan = $room_plan_db->getRow($where);
          if ($room_plan["service_key"] == "45" || $room_plan["service_key"] == "46" || $room_plan["service_key"] == "47") {
              $this->logger2->error("not add audience");
          } else if (!$room_plan) {
              //プランが無かった場合は予約があるか確認
              $where = "room_key = '".addslashes($room_key)."'".
                   " AND room_plan_status = 2";
              $room_plan_reserve = $room_plan_db->getRow($where);
              if ($room_plan_reserve) {
                  $ordered_service_option->add($data);
              }
          } else {
              $ordered_service_option->add($data);
              $where_coount = "room_key = '".addslashes($room_key)."'".
                   " AND service_option_key = ".addslashes($service_option_key).
                   " AND ordered_service_option_status = 1";
              $audience_count = $ordered_service_option->numRows($where_coount);
              if ($audience_count > 0) {
                  $seat_data["max_seat"] = "9";
                  $audience_seat = $audience_count."0";
                  $this->logger2->info($audience_seat);
              } else {
                  $seat_data["max_seat"] = "10";
                  $audience_seat = "0";
              }
              $seat_data["max_audience_seat"] = $audience_seat;
              $where_room = "room_key = '".addslashes($room_key)."'";
              $room_db = new N2MY_DB($this->dsn, "room");
              $room_data = $room_db->update($seat_data, $where_room);
          }

      } else {
          $ordered_service_option->add($data);
          if ($service_option_key == 23) {// teleconference
              $room_db       = new N2MY_DB($this->dsn, "room");
              $teleconf_data = array('use_teleconf'=>1, 'use_pgi_dialin'=>1, 'use_pgi_dialin_free'=>1, 'use_pgi_dialout'=>1);
              $room_db->update($teleconf_data , "room_key = '".addslashes($room_key)."'");
          }
      }

      // スマートフォン連携時に電話連携がなければ自動でセットする
      if ($service_option_key == 21) {
          $where = "room_key = '".addslashes($room_key)."'".
               " AND service_option_key = 19".
               " AND ordered_service_option_status = 1";
          $count = $ordered_service_option->numRows($where);
          if ($count == 0) {
              $data2 = $data;
              $data2["service_option_key"] = 19;
              $ordered_service_option->add($data2);
          }
      }

      return true;
  }

  //メンバーが有効か
  private function member_exists ($user_key, $memberId){
    $ret = false;
    $member_db = new N2MY_DB($this->dsn, "member");
    $memberCnt = $member_db->numRows( sprintf("user_key = '%s' AND member_id='%s'", $user_key, mysql_real_escape_string($memberId) ) );
    $this->logger2->info(sprintf("user_key = '%s' AND member_id='%s'", $user_key, $memberId ));

    if($memberCnt > 0){
      $ret  = true;
    }
    $this->logger2->info($memberCnt);

    return $ret;
  }


  //部屋が有効か
  private function room_exists($user_key, $roomKey){
    $ret  = false;
    $room_db = new N2MY_DB($this->dsn, "room");
    $roomCnt = $room_db->numRows( sprintf("user_key = '%s'  AND room_key = '%s'", $user_key, $roomKey ) );

    if($roomCnt > 0){
      $ret  = true;
    }

    return $ret;
  }  /**
   * id からそのユーザーが存在するかチェック
   */
  private function _isExistMember($id){
    //member, user, 認証DBのuser のテーブルをチェック
    $user_db = new N2MY_DB($this->dsn, "user");
    $member_db = new N2MY_DB($this->dsn, "member");
    $id = mysql_real_escape_string($id);
    if ( $user_db->numRows( sprintf("user_id='%s'", $id ) ) > 0 ||
         $member_db->numRows( sprintf("member_id='%s'", $id ) )  > 0 ||
         $user_db->numRows( sprintf("user_id='%s'", $id ) )  > 0 ){
      return true;
    } else {
      return false;
    }
  }

  public function initServices(){
    $service_db = new N2MY_DB($this->auth_dsn, "service");
    $this->services = $service_db->getRowsAssoc("", array(), null, 0, "service_key,service_name");

    $sOption_db = new N2MY_DB($this->auth_dsn, "service_option");
    $this->sOptions = $sOption_db->getRowsAssoc("", array(), null, 0, "service_option_key,service_option_name");

  }
}