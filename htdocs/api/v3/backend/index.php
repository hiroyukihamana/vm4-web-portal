<?php
require_once("classes/N2MY_Api.class.php");
require_once("classes/mgm/MGM_Auth.class.php");

class N2MY_Meeting_API extends N2MY_Api
{
    var $session_id = null;
    var $api_version = null;
    var $vcubeId_path = null;
    var $wsdl_path = null;
    var $soap_options = null;
    
    //dsn情報
    var $auth_dsn = null;
    var $dsn      = null; 
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        //強制的にaction_indexをたたくようにします
        $_REQUEST["action_index"] = "";
        
        //soapの設定
        $this->vcubeId_path = dirname(__FILE__).DIRECTORY_SEPARATOR."vcubeId.class.php";
        $this->wsdl_path    = dirname(__FILE__).DIRECTORY_SEPARATOR."vcubeId.wsdl";
        
        
        require_once $this->vcubeId_path;

        if( !defined("PHP_EOL") ){
          define("PHP_EOL", "\n");
        }

        /** WSDL cache false */
        ini_set("soap.wsdl_cache_enabled", "0");

        $this->soap_options = array(  "encoding" => "UTF-8" , "soap_version" => SOAP_1_2);
        //soapの設定--end
        
        
        //dsn情報取得
        $this->auth_dsn = $this->get_auth_dsn();
        
        // SSL対応
        header('Pragma:');
        //$this->getApiInterface();
    }
    
    
    
    function auth()
    {
        // ログイン以外の場合は認証させる
    }
    
    function action_index(){
      $this->logger2->info("API START");
      
      //userId の取得
      $soapXml = file_get_contents('php://input');
      $this->logger2->info("xml => ".$soapXml);
      
      $xml = simplexml_load_string($soapXml);
      
      
      
      //foreach($xml->xpath("//userId") as $item){
      //  $userId = (string)$item;
      //}
      $userId = "vuzer";
      
      $this->logger2->info("user_id => ".$userId);
      
      require_once 'classes/mgm/MGM_Auth.class.php';
      
      $server = new SoapServer($this->wsdl_path, $this->soap_options);
      
      $server->setClass("vcubeId", $this->get_auth_dsn(), $userId);
      $server->handle();
      
    }
    
    

}
$main = new N2MY_Meeting_API();
$main->execute();
