google.load("language", "1");
$(function () {
    $("#fix_button").click(function() {
        $("#fix_form").submit();
    });
    $('#checkall').click(function() {
        $('.row_select').attr('checked', this.checked);
    });
//    $("#split_form").hide();
});

function edit_form(define_no) {
    location.href = '?action_edit_form=&define_no='+define_no;
}

function edit(myForm) {
    if (confirm("変更しますか")) {
        myForm._action.name = "action_edit";
        myForm.submit();
    }
}

function translate(id, from, to) {
    var subject = $('#from_'+id);
    var text = subject.val();
    google.language.translate(text, from, to, function(result) {
        if (result.error) {
            alert("Error:" + result.message);
            return;
        }
        $('#to_'+id+'_'+to).val(result.translation);
        change_text(id, to);
//          alert(result.translation);
    });
}

function change_text(id, to) {
    var subject = $('#to_'+id+'_'+to);
    subject.css("background-color", "#ff66aa");
}

function togglea(id) {
    $(".form_"+id).toggle("slow");
}

function text_reset(id, to) {
    $('#to_'+id+'_'+to).css("background-color", "#ffffff");
}

function edit_lang(id, to) {
    var subject = $('#to_'+id+'_'+to);
    var text = subject.val();
    var param = {
        action_edit_translator: "",
        id: id,
        lang: to,
        word: text
    }
    $.post("index.php", param, function(data){
        if (data == "ok") {
            text_reset(id, to);
        } else {
            alert(data);
        }
    });
}

function add_form(define_no) {
    location.href = '?action_add_form=&define_no='+define_no;
}

function add(myForm) {
    if (confirm("追加しますか")) {
        myForm._action.name = "action_add";
        myForm.submit();
    }
}

function data_split() {
    $("#split_form").show("slow");
}

function check_edit(myForm, flg) {
    if (confirm("選択した項目を変更しますか")) {
        myForm._action.name = "action_select_edit";
        myForm.flg.value = flg;
        myForm.submit();
    }
}

function jump(page, action) {
    var url = '?page='+page;
    if (action) {
        url = url+"&"+action;
    }
    location.href = url;
}