#!/bin/sh

db_host="localhost"
db_port="3306"
db_user=""
db_pass=""
db_name_web="n2my_web"
db_name_core="n2my_core"

#mysql_bin=/usr/local/mysql/bin/mysqldump
mysql_bin=/usr/local/mysql5/bin/mysqldump

ymd=`date '+%Y%m%d'`

web_backup_dir=${HOME}/expire/n2my_web/${ymd}
core_backup_dir=${HOME}/expire/n2my_core/${ymd}

mkdir -p ${web_backup_dir}
mkdir -p ${core_backup_dir}

echo "DB BACKUP"
echo ""
echo [${db_name_web}]
$mysql_bin -h ${db_host} -P ${db_port} -u ${db_user} --password=${db_pass} ${db_name_web} | gzip > ${web_backup_dir}/sql.zip
echo ${web_backup_dir}/sql.zip
echo "== successful =="

echo ""
echo [${db_name_core}]
$mysql_bin -h ${db_host} -P ${db_port} -u ${db_user} --password=${db_pass} ${db_name_core} | gzip > ${core_backup_dir}/sql.zip
echo ${core_backup_dir}/sql.zip
echo "== successful =="