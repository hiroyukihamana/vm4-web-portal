<?php
header('Content-Type: text/html; charset=UTF-8');

require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/ONE_Account.class.php");


class AppOneUserQueue extends AppFrame {

    const TIME_OUT = 600;             //通常実行時タイムアウト(10分)
    const LIMIT_NUMBER = 10;
    const PID_FILE_DEF = '/bin/batch/oneUserQueue';

    private $mypid;
    private $nowTime;
    private $timecheck;
    private $obj_ONE_Account;
    private $result_log_dir;
    private $options;
    private $option_list = array();
    private $opt_flg = 0;
    private $help_text = "
---------------------------------------------------------------------
- 使用法  :  one_user_quere.php option1 【option2】 ( option3 【option4】)
- 使用例  :  one_user_quere.php -dlt 10 -memberLimit 10
---------------------------------------------------------------------
-dlt 【num】                 :  一度の起動で処理するキューの数を指定し、num分の数を最大にして処理をする
                             :  num に all を指定した場合は保留状態を除く全件を対象にする
                             :  num に forceAll を指定した場合は保留状態を含む全件を対象にする
( -memberLimit 【num】)      :  (追加オプション) num分のメンバー数を最大にして実行する
                    ※保留状態は対象にしない、基本cronではこれに数字をセットして使用する
-upd 【num】                 :  一度の起動で処理するキューの数を指定し、num分の数を最大にして処理をする
                             :  num に all を指定した場合は保留状態を除く全件を対象にする
                             :  num に forceAll を指定した場合は保留状態を含む全件を対象にする
                    ※保留状態は対象にしない、基本cronではこれに数字をセットして使用する
-def 【num】                 :  一度の起動で処理するキューの数を指定し、num分の数を最大にして処理をする
                             :  num に all を指定した場合は保留状態を除く全件を対象にする
                             :  num に forceAll を指定した場合は保留状態を含む全件を対象にする
( -memberLimit 【num】)      :  (追加オプション) num分のメンバー数を最大にして実行する(削除に対してのみ有効)
                    ※保留状態は対象にしない、基本cronではこれに数字をセットして使用する
-addQueueDlt 【user_key】    :  ユーザーにひもづくメンバーが存在した場合キューに削除モードで登録する
-addQueueUpd 【user_key】    :  ユーザーにひもづくメンバーが存在した場合キューに更新モードで登録する
-p 【one_user_queue_key】    :  対象のキューキーがあれば保留状態にする
-cancel【one_user_queue_key】:  対象のユーザーのキューを削除
-setQueueDltMode【one_user_queue_key】
                             :  対象のキューキーが登録されていた場合にモードを削除に更新する
-forceSelf【one_user_queue_key】
                             :  対象のキューキーがあれば処理を実行する
-allCancelDlt                :  削除系の全てのキューを削除する
-allCancelUpd                :  更新系の全てのキューを削除する
-allCancelDef                :  全てのキューを削除する
-l 【num】                   :  num分だけキューのリストを表示
-ld【num】                   :  num分だけ削除モードキューのリストを表示
-lu【num】                   :  num分だけ更新モードキューのリストを表示
-h                           :  help コマンド使い方を表示\n";

    function init() {
        $this->obj_ONE_Account = new ONE_Account( parent::get_dsn() );
        $this->timecheck = $this->obj_ONE_Account->getMidnightRuntimeInfo();
    }
    
    /*
     * 引数格納、夜間有効時間取得
     */
    function __construct($argv) {
        parent::__construct();
        $this->options = $argv;
        array_shift($this->options);
        $this->nowTime = date("H:i:s");
    }

    /*
     * メイン処理
     */
    function default_view() {
        $this->log_pre();
        $this->set_option();
        $this->check_option();
        $this->print_reference();
        $this->check_duplicate();
        $this->run_processing();
    }

    /*
     * 引数を各オプション別に振り分ける
     * 上書きの可能性のある Mode は 重複した場合のみ -1 設定にする。
     */
    function set_option() {
        $mode = 0;
        for( $opt_cnt = 0 ; $opt_cnt < count($this->options); $opt_cnt++){
            // 各オプション毎に設定値を振り分ける
            switch( $this->options[$opt_cnt] ) {
                //ヘルプ表示
                case "-h":
                    $this->option_list["help"] = true;
                    break;
                //キューリスト表示(全モード)
                case "-l":
                    $this->option_list["queue_list"] = $this->options[ $opt_cnt +1 ];
                    $this->option_list["mode"] = null;
                    $opt_cnt++;
                    break;
                //キューリスト表示(削除モードのみ)
                case "-ld":
                    $this->option_list["queue_list"] = $this->options[ $opt_cnt +1 ];
                    $this->option_list["mode"] = 2;
                    $opt_cnt++;
                    break;
                //キューリスト表示(更新モードのみ)
                case "-lu":
                    $this->option_list["queue_list"] = $this->options[ $opt_cnt +1 ];
                    $this->option_list["mode"] = 0;
                    $opt_cnt++;
                    break;
                //キュー保留
                case "-p":
                    $this->option_list["pend_queue"] = $this->options[ $opt_cnt +1 ];
                    $this->opt_flg++;
                    $opt_cnt++;
                    $this->option_list["mode"] = null;
                    break;
                //モード(不正時は-1を格納)
                //削除 = 2
                case "-dlt":
                    $mode++;
                //追加 = 1
                case "-add":
                    $mode++;
                //更新 = 0
                case "-upd":
                    if(!array_key_exists("mode",$this->option_list)){
                        $this->option_list["mode"] = $mode;
                    } else {
                        $this->option_list["mode"] = -1;
                    }
                    $this->option_list["limit_queue"] = $this->options[ $opt_cnt +1 ];
                    $opt_cnt++;
                    break;
                //両モード指定 = null
                case "-def":
                    if(!array_key_exists("mode",$this->option_list)){
                        $this->option_list["mode"] = null;
                    } else {
                        $this->option_list["mode"] = -1;
                    }
                    $this->option_list["limit_queue"] = $this->options[ $opt_cnt +1 ];
                    $opt_cnt++;
                    break;
                //キュー追加(削除)
                case "-addQueueDlt":
                    $this->option_list["add_queue"] = $this->options[ $opt_cnt +1 ];
                    $this->opt_flg++;
                    $opt_cnt++;
                    $this->option_list["mode"] = 2;
                    break;
                //キュー追加(更新)
                case "-addQueueUpd":
                    $this->option_list["add_queue"] = $this->options[ $opt_cnt +1 ];
                    $this->opt_flg++;
                    $opt_cnt++;
                    $this->option_list["mode"] = 0;
                    break;
                //キュー更新(更新->削除)
                case "-setQueueDltMode":
                    $this->option_list["set_delete_mode"] = $this->options[ $opt_cnt +1 ];
                    $this->opt_flg++;
                    $opt_cnt++;
                    $this->option_list["mode"] = null;
                    break;
                //キュー削除
                case "-cancel":
                    $this->option_list["cancel"] = $this->options[ $opt_cnt +1 ];
                    $this->opt_flg++;
                    $opt_cnt++;
                    $this->option_list["mode"] = null;
                    break;
                //キュー全件削除(全)
                case "-allCancelDef":
                    $this->opt_flg++;
                    $this->option_list["all_cancel"] = true;
                    $this->option_list["mode"] = null;
                    break;
                //キュー全件削除(削除)
                case "-allCancelDlt":
                    $this->opt_flg++;
                    $this->option_list["all_cancel"] = true;
                    $this->option_list["mode"] = 2;
                    break;
                //キュー全件削除(削除)
                case "-allCancelUpd":
                    $this->opt_flg++;
                    $this->option_list["all_cancel"] = true;
                    $this->option_list["mode"] = 0;
                    break;
                //削除時１度に削除する最大
                case "-memberLimit":
                    $this->option_list["member_limit"] = $this->options[ $opt_cnt +1 ];
                    $opt_cnt++;
                    break;
                //1キュー強制実行
                case "-forceSelf":
                    $this->option_list["force_self"] = $this->options[ $opt_cnt +1 ];
                    $this->opt_flg++;
                    $opt_cnt++;
                    break;
                //オプション不正
                default:
                    echo "Option not valid error :" . $this->options[ $opt_cnt ] . "\n";
                    $this->option_list["unknown_option"] = $this->options[ $opt_cnt ];
                    $this->log_writer("option error");
                    exit;
                    break;
            }
        }
        rerurn;
    }


    /*
     * オプションの整合性確認
     * 各オプションが正しく設定されているか確認する。
     */
    function check_option() {
        /*******************************/
        /* 基本オプションチェック      */
        /*******************************/
        // 複合可能オプション指定チェック
        if( $this->opt_flg > 1 ) {
            echo "Option not valid error\n";
            $this->log_writer("option error");
            exit;
            break;
        }
        // Modeオプション不正だった場合はエラー応答
        if (array_key_exists("mode",$this->option_list)){
            if( $this->option_list["mode"] < 0 ){
                echo "Mode select Error\n";
                $this->log_writer("mode select error");
                exit;
            }
        }

        /*******************************/
        /* 各実行オプションチェック    */
        /*******************************/
        // キュー追加オプション
        if(array_key_exists("add_queue",$this->option_list) && !ctype_digit($this->option_list["add_queue"])){
            echo "add queue option error:" .$this->option_list["add_queue"]. "\n";
            $this->log_writer("add queue option error");
            exit;
        }
        // キュー更新オプション
        if(array_key_exists("set_delete_mode",$this->option_list) && !ctype_digit($this->option_list["set_delete_mode"])){
            echo "update queue option error:" . $this->option_list["set_delete_mode"] . "\n";
            $this->log_writer("update queue option error");
            exit;
        }
        // キュー一時停止オプション
        if(array_key_exists("pend_queue",$this->option_list) && !ctype_digit($this->option_list["pend_queue"])){
            echo "pend queue option error:". $this->option_list["pend_queue"] . "\n";
            $this->log_writer("pend queue option error");
            exit;
        }
        // キューキャンセルオプション
        if(array_key_exists("cancel",$this->option_list) && !ctype_digit($this->option_list["cancel"])){
            echo "cancel queue option error:" . $this->option_list["cancel"] . "\n";
            $this->log_writer("cancel queue option error");
            exit;
        }
        // ユーザー指定実行オプション
        if(array_key_exists("force_self",$this->option_list) && !ctype_digit($this->option_list["force_self"])){
            echo "self queue option error:" . $this->option_list["force_self"] . "\n";
            $this->log_writer("self queue option error");
            exit;
        }
        // キュー実行件数オプション
        if(array_key_exists("limit_queue",$this->option_list)){
            if(!ctype_digit($this->option_list["limit_queue"])){
                if($this->option_list["limit_queue"]=="all") {
                     $this->option_list["all"] = true;
                     unset($this->option_list["limit_queue"]);
                } else if ($this->option_list["limit_queue"]=="forceAll") {
                     $this->option_list["force_all"] = true;
                     unset($this->option_list["limit_queue"]);
                } else {
                    echo "limit queue option error:" . $this->option_list["limit_queue"] . "\n";
                    $this->log_writer("limit queue option error");
                    exit;
                }
            } else {
                //0指定された場合はエラー
                if( $this->option_list["limit_queue"] < 1) {
                    echo "limit queue option error:" . $this->option_list["limit_queue"] . "\n";
                    $this->log_writer("limit queue option error");
                    exit;
                }
            }
        }
        // メンバー削除件数オプション
        if (array_key_exists("member_limit",$this->option_list)){
            if ($this->option_list["mode"]===null || $this->option_list["mode"]==2) {
                if(!ctype_digit($this->option_list["member_limit"])){
                    echo "member limit table option error: " . $this->option_list["member_limit"] . "\n";
                    $this->log_writer("member limit table option error");
                    exit;
                } else {
                    //0指定された場合はエラー
                    if( $this->option_list["member_limit"] < 1) {
                        echo "member limit table option error:" . $this->option_list["member_limit"] ."\n";
                        $this->log_writer("member limit table option error");
                        exit;
                    }
                }
            } else {
                echo "member limit table option error:" . $this->option_list["member_limit"] . "\n";
                $this->log_writer("member limit table option error");
                exit;
            }
        }
        // キューリスト表示オプション
        if( array_key_exists("queue_list",$this->option_list) && !ctype_digit($this->option_list["queue_list"])) {
            //数値以外が来た場合はキューリミット指定なし
            $this->option_list["queue_list"] = null;
        }
        array_values($this->option_list);
        return;
    }



    /*
     * ヘルプ表示、リスト表示を行う
     */
    function print_reference(){
        //ヘルプ表示指定 or オプション指定なしの場合はヘルプ表示
        if(array_key_exists("help",$this->option_list) || count($this->options)== 0) {
            echo($this->help_text);
            $this->log_writer();
            exit;
        //リスト表示オプション指定
        } else if(array_key_exists("queue_list",$this->option_list)) {
            $result = $this->obj_ONE_Account->userListQueue($this->option_list["queue_list"],$this->option_list["mode"],true);
            if($result[0] == false){
                $this->log_writer($result[1]);
            } else {
                foreach($result as $res_line) {
                    echo "-------------------------------------\n";
                    echo "one_user_queue_key  :" . $res_line["one_user_queue_key"] . "\n";
                    echo "user_key            :" . $res_line["user_key"] . "\n";
                    if($res_line["mode"] == "0") {
                        $mode_str = "[Update]";
                    } else if($res_line["mode"] == "1") {
                        $mode_str = "[Add]";
                    } else if($res_line["mode"] == "2") {
                        $mode_str = "[Delete]";
                    }
                    echo "mode                :" . $res_line["mode"] . " " . $mode_str . "\n";
                    echo "error_count         :" . $res_line["error_count"] . "\n";
                    echo "pend                :" . $res_line["pend"] . "\n";
                    echo "create_detatime     :" . $res_line["create_detatime"] . "\n";
                    echo "update_datetime     :" . $res_line["update_datetime"] . "\n";
                }
                $this->log_writer();
            }
            exit;
        }
    }

    /*
     * メイン処理
     * 更新と削除処理の同時起動は可能 / 両方同時処理(Mode指定なし)及びその他の場合は多重起動不可
     */
    function run_processing(){
        //夜間実行メールフラグ
        $is_send_report = false;
        // キュー追加
        if(array_key_exists("add_queue",$this->option_list)){
            $result = $this->obj_ONE_Account->userAddQueue($this->option_list["add_queue"],$this->option_list["mode"]);
        // キュー更新
        } else if(array_key_exists("set_delete_mode",$this->option_list)){
            $result = $this->obj_ONE_Account->userQueueSetDeleteMode($this->option_list["set_delete_mode"]);
        //キュー保留
        } else if(array_key_exists("pend_queue",$this->option_list)){
            $result = $this->obj_ONE_Account->userPendSetQueue($this->option_list["pend_queue"]);
        //キュー削除
        } else if(array_key_exists("cancel",$this->option_list)){
            $result = $this->obj_ONE_Account->userQueueCancel($this->option_list["cancel"]);
        //キュー全件削除
        } else if(array_key_exists("all_cancel",$this->option_list)){
            $result = $this->obj_ONE_Account->userAllQueueCancel($this->option_list["mode"]);
        // 1件指定強制処理
        } else if(array_key_exists("force_self",$this->option_list)){
            $result = $this->obj_ONE_Account->userExecFromUserQueuekey($this->option_list["force_self"]);
        //全件処理(夜間限定のMode)
        } else if($this->option_list["all"] || $this->option_list["force_all"]) {
            if ($this->timecheck["is_run"]) {
                $is_send_report = true;
            }
            $result = $this->obj_ONE_Account->userAllExecFromQueue($this->option_list["force_all"],$this->option_list["mode"],$this->option_list["member_limit"]);
        //通常処理
        } else {
            // キューリミット指定なしの場合はデフォルト設定
            if(!$this->option_list["limit_queue"]){
                $this->option_list["limit_queue"] = self::LIMIT_NUMBER;
            }
            $result = $this->obj_ONE_Account->userExecFromQueue($this->option_list["limit_queue"],$this->option_list["mode"],$this->option_list["member_limit"]);
        }

        //リザルト確認
        if($result[0]===false){
            $this->log_writer($result[1]);
            //夜間実行時は結果を報告
            //異常発生時はメールが送信されない可能性もある
            if($is_send_report) {
                $mail_subject = "one_user_queue 夜間作業停止";
                $mail_body = "one_user_queueが異常発生により夜間処理を停止しました" ."\n\n" . "■ Pid: ". $this->mypid ."\n";
                if (defined("N2MY_ALERT_FROM") && defined("N2MY_ALERT_TO") && N2MY_ALERT_FROM && N2MY_ALERT_TO) {
                    $this->sendReport(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body);
                }
            }
        } else{
            $this->log_writer();
            //夜間実行時は結果を報告
            if($is_send_report) {
                $mail_subject = "one_user_queue 夜間作業終了";
                $mail_body = "one_user_queueが夜間処理を終了しました" ."\n\n" . "■ Pid: ". $this->mypid ."\n";
                if (defined("N2MY_CRON_FROM") && defined("N2MY_CRON_TO") && N2MY_CRON_FROM && N2MY_CRON_TO) {
                    $this->sendReport(N2MY_CRON_FROM, N2MY_CRON_TO, $mail_subject, $mail_body);
                }
            }
        }
    }

    /*
     * 多重起動制御
     * 削除モード、更新モードは各1プロセスごとに実行可能
     * 両方モードは、削除、更新どちらかのどちらか片方でも実行していたら実行しない
     * 両方モード中は、完全起動制限
     */
    function check_duplicate(){
        $_status = "";
        // 各Mode別pidファイル名
        $pidFile_def  = N2MY_APP_DIR. self::PID_FILE_DEF . '.pid';
        $pidFile_upd  = N2MY_APP_DIR. self::PID_FILE_DEF . '_upd.pid';
        $pidFile_dlt  = N2MY_APP_DIR. self::PID_FILE_DEF . '_dlt.pid';

        //起動プロセス確認
        //全Mode処理
        if(file_exists($pidFile_def)) {
            $_pid = trim(file_get_contents($pidFile_def));
            $str_cmd_line = system("ps ww -p {$_pid} ", $_status);
            $mailFile = N2MY_APP_DIR.'/bin/batch/mailSend_'.$_pid;
            $mail_flag = file_exists($mailFile);
            //プロセスが終了していた場合
            if($_status){
                unlink($pidFile_def);
                if($mail_flag){
                    unlink($mailFile);
                }
            //プロセスが実行状態の場合
            } else {
                $file_time = filemtime($pidFile_def);
                $time = time() - $file_time;
                // 夜間実行タイムアウトの時間( 夜間時間 - ( pid作成時間 - 夜間開始時間) + 10分 )
                if(strpos($str_cmd_line,"all") !== false || strpos($str_cmd_line,"forceAll") !== false ) {
                    $is_all = true;
                } else {
                    $is_all = false;
                }
                if($is_all) {
                    $time_interval = $file_time - $this->timecheck[start_time];
                    $timeout = $this->timecheck["time_duration"]  - $time_interval + self::TIME_OUT;
                } else {
                    $timeout = self::TIME_OUT;
                }
                //タイムアウトの場合排他を解除する
                if ($time >= $timeout) {
                    $this->sendPidTimeoutMail($pidFile_def);
                    unlink($pidFile_def);
                    if($mail_flag){
                        unlink($mailFile);
                    }
                } else {
                    $this->log_writer("Multiple start-up : def");
                    echo("全モード実行プロセス起動中のため実行を中止しました。\n");
                    $this->logger2->warn("全モード実行プロセス起動中のため実行を中止しました。 time:".$time);
                    exit;
                }
            }
        }
        //更新モードの場合
        if( $this->option_list["mode"]===0 || $this->option_list["mode"]===null ) {
            if(file_exists($pidFile_upd)) {
                $_pid = trim(file_get_contents($pidFile_upd));
                $str_cmd_line = system("ps ww -p {$_pid} ", $_status);
                $mailFile = N2MY_APP_DIR.'/bin/batch/mailSend_'.$_pid;
                $mail_flag = file_exists($mailFile);
                //プロセスが終了していた場合
                if($_status){
                    unlink($pidFile_upd);
                    if($mail_flag){
                        unlink($mailFile);
                    }
                } else {
                    $file_time = filemtime($pidFile_upd);
                    $time = time() - $file_time;
                    // 夜間実行タイムアウトの時間( 夜間時間 - ( pid作成時間 - 夜間開始時間) + 10分 )
                    if(strpos($str_cmd_line,"all") !== false || strpos($str_cmd_line,"forceAll") !== false ) {
                        $is_all = true;
                    } else {
                        $is_all = false;
                    }
                    if($is_all) {
                        $time_interval = $file_time - $this->timecheck[start_time];
                        $timeout = $this->timecheck["time_duration"]  - $time_interval + self::TIME_OUT;
                    } else {
                        $timeout = self::TIME_OUT;
                    }
                    //タイムアウトの場合排他を解除する
                    if ($time >= $timeout) {
                        $this->sendPidTimeoutMail($pidFile_upd);
                        unlink($pidFile_upd);
                        if($mail_flag){
                            unlink($mailFile);
                        }
                    } else {
                        $this->log_writer("Multiple start-up : upd");
                        echo("更新モード実行プロセス実行中ため実行を中止しました。\n");
                        $this->logger2->warn("更新モード実行プロセス実行中ため実行を中止しました。 time:".$time);
                        exit;
                    }
                }
            }
        }
        //削除モードの場合
        if( $this->option_list["mode"]===2 || $this->option_list["mode"]===null ) {
            if(file_exists($pidFile_dlt)) {
                $_pid = trim(file_get_contents($pidFile_dlt));
                $str_cmd_line = system("ps ww -p {$_pid} ", $_status);
                $mailFile = N2MY_APP_DIR.'/bin/batch/mailSend_'.$_pid;
                $mail_flag = file_exists($mailFile);
                //プロセスが終了していた場合
                if($_status){
                    unlink($pidFile_dlt);
                    if($mail_flag){
                        unlink($mailFile);
                    }
                } else {
                    $file_time = filemtime($pidFile_dlt);
                    $time = time() - $file_time;
                    // 夜間実行タイムアウトの時間( 夜間時間 - ( pid作成時間 - 夜間開始時間) + 10分 )
                    if(strpos($str_cmd_line,"all") !== false || strpos($str_cmd_line,"forceAll") !== false ) {
                        $is_all = true;
                    } else {
                        $is_all = false;
                    }
                    if($is_all) {
                        $time_interval = $file_time - $this->timecheck[start_time];
                        $timeout = $this->timecheck["time_duration"]  - $time_interval + self::TIME_OUT;
                    } else {
                        $timeout = self::TIME_OUT;
                    }
                    //タイムアウトの場合排他を解除する
                    if ($time >= $timeout) {
                        $this->sendPidTimeoutMail($pidFile_dlt);
                        unlink($pidFile_dlt);
                        if($mail_flag){
                            unlink($mailFile);
                        }
                    } else {
                        $this->log_writer("Multiple start-up : dlt");
                        echo("削除モード実行プロセス実行中のため実行を中止しました。\n");
                        $this->logger2->warn("削除モード実行プロセス実行中のため実行を中止しました。 time:".$time);
                        exit;
                    }
                }
            }
        }
        //排他設定
        $this->logger2->trace(array($_pid,getmypid()));
        //実行オプションモード別pidファイル作成
        if($this->option_list["mode"]===2){
            $pidFile = $pidFile_dlt;
        } else if($this->option_list["mode"]===0){
            $pidFile = $pidFile_upd;
        } else {
            $pidFile = $pidFile_def;
        }
        system('echo '.getmypid().' > '.$pidFile, $_status);
        $this->mypid = getmypid();
        if($_status){
            //メールを飛ばす
            $this->logger2->error("one_user_queue.phpはPIDファイルの作成に失敗しました。");
            exit;
        }
    }

    /*
     * タイムアウトメール送信処理
     */
    private function sendPidTimeoutMail($pidFile)
    {
        $_pid = trim(file_get_contents($pidFile));
        $this->_log ( "# time_out" ,  $this->result_log_dir);
        $this->logger2->warn("one_user_queue処理がハングしました。");
        $mail_subject = "one_user_queue処理にてタイムアウトが発生しました";
        $mail_body = "one_user_queue処理にてタイムアウトが発生したため、多重起動制限を解除しました" ."\n\n" .
                    "■File:" .$pidFile . "\n■ Pid: ".$_pid."\n";
        if (defined("N2MY_ALERT_FROM") && defined("N2MY_ALERT_TO") && N2MY_ALERT_FROM && N2MY_ALERT_TO) {
            $this->sendReport(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body);
        }
    }

    /*
     * ログ出力前提作業
     */
    private function log_pre(){
        $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $this->result_log_dir = $log_dir;
        return $result_log_dir;
    }

    /*
     * ログへの書き込み
     */
    private function log_writer( $messege=null ){
        $time = date("m/d/H:i:s");
        if ( $messege ) {
            // 異常系
            $log_messege = "# error    date:" . $time . " message: [ " . $messege . " ]";
            $this->_log ( $log_messege  ,  $this->result_log_dir);
        } else {
            // 正常系
            $log_messege = "# succcess date:" . $time;
            $this->_log ( $log_messege ,  $this->result_log_dir);
        }
    }

    /*
     * ログ書き込み
     */
    private function _log($string , $result_log_dir){
        $log_prefix = "one_user_queue";
        $option_str = "\n << OPTION_LIST [option_name] : [value] >>\n";
        $log_file = sprintf("%s/%s_%s.log", $result_log_dir, $log_prefix, date('Ym', time()));
        $fp = fopen($log_file, "a");

        //ログ吐き出し用にオプション整理
        if($this->option_list) {
            foreach( $this->option_list as $key => $value ){
                $option_str .= " [ " . $key . " ] => [ " . $value . " ]\n";
            }
            $string .= $option_str;
        }
        if($fp){
            fwrite($fp,$string."\n");
        }
        fclose($fp);
    }

    /*
     * メール送信
     */
    function sendReport($mail_from, $mail_to, $mail_subject, $mail_body) {
        require_once("lib/EZLib/EZMail/EZSmtp.class.php");
        // 入力チェック
        if (!$mail_from || !$mail_to || !$mail_subject || !$mail_body) {
            return false;
        }
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang = EZLanguage::getLangCd("ja");
        $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
        $obj_SendMail->setFrom($mail_from);
        $obj_SendMail->setReturnPath($mail_from);
        $obj_SendMail->setTo($mail_to);
        $obj_SendMail->setSubject($mail_subject);
        $obj_SendMail->setBody($mail_body);
        // メール送信
        $result = $obj_SendMail->send();
        $this->logger2->info(array($mail_from, $mail_to, $mail_subject, $mail_body));
    }
    


}
$main = new AppOneUserQueue($argv);
$main->execute();
