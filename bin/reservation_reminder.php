#!/usr/local/bin/php
<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Reservation.class.php");
require_once("classes/dbi/reservation.dbi.php");

class AppReservationReminder extends AppFrame {

    var $_mail_flg = 1;
    var $dsn = null;
    var $N2MY_Reservation = null;
    var $obj_Reservation = null;

    function __destruct() {
        //unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
    }

    function init() {
        $this->dsn = $this->get_dsn();
        $this->N2MY_Reservation = new N2MY_Reservation($this->dsn);
        $this->obj_Reservation     = new ReservationTable($this->dsn);
    }

    function default_view() {
        $reminder_list = $this->N2MY_Reservation->getReminderList();
        var_dump($reminder_list);
        foreach ($reminder_list as $value){
            $info = $value;
            $info["base_url"] = $value["reservation_url"];
            $info["sender_mail"] = $value["sender_email"];
            $info["sender"] = $value["sender_name"];
            $info["mail_body"] = $value["reservation_info"];
            $info["guest_url_format"] = $value["user_info"]["guest_url_format"];
            //カスタムチェック
            $info["custom"] = $value["reservation_custom"];
            $account_model = $value["user_info"]["account_model"];
            $maillist = array();
            foreach ($value["guests"] as $guest){
                $guest["reservation_key"] = $value["reservation_key"];
                $guest["country_key"] = $value["meeting_country"];
                $maillist[] = $this->N2MY_Reservation->participant_mail($guest , $info , "reminder" , true ,$value["user_info"]);
            }
            $this->N2MY_Reservation->sendMailToOwner($info , "reminder" , $value["sender_lang"] , $maillist);
            $where = "reservation_key = " . $value["reservation_key"];
            $data["is_reminder_send_flg"] = 1;
            $this->obj_Reservation->update($data, $where);
        }
    }


}
$main = new AppReservationReminder();
$main->execute();
