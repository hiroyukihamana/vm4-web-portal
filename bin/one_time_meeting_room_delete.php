#!/usr/local/bin/php
<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Reservation.class.php");
require_once("classes/dbi/reservation.dbi.php");
require_once("classes/core/Core_Meeting.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");

class AppOneTimeMeetingRoomDelete extends AppFrame {

    var $_mail_flg = 1;
    var $dsn = null;
    var $N2MY_Reservation = null;
    var $obj_Reservation = null;
    var $obj_core_meeting = null;
    var $obj_room = null;
    var $obj_Meeting = null;
    var $obj_Participant = null;

    function __destruct() {
        //unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
    }

    function init() {
        $this->dsn = $this->get_dsn();
        $this->N2MY_Reservation = new N2MY_Reservation($this->dsn);
        $this->obj_Reservation     = new ReservationTable($this->dsn);
        $this->obj_core_meeting     = new Core_Meeting($this->dsn);
        $this->obj_room     = new RoomTable($this->dsn);
        $this->obj_Meeting = new DBI_Meeting($this->dsn);
        $this->obj_Participant = new DBI_Participant($this->dsn);
    }

    /*
     * one time meeting用 会議室を削除する
     */
    function default_view() {
        $now =  date('Y-m-d H:i:s');
        // 削除対象はlife_date_timeが実行時より前の会議室
        $where = "is_one_time_meeting = 1 AND life_date_time < '" . $now . "'";
        $delete_room_list =  $this->obj_room->getRowsAssoc($where);
        foreach($delete_room_list as $room_info){
            // $meeintg_info初期化
            $meeintg_info = null;
            // 会議中の会議室は削除対象にならないため確認
            $meeting_where = "room_key = '".addslashes($room_info["room_key"])."' AND is_active = 1 AND is_deleted = 0";
            $meeintg_info = $this->obj_Meeting->getRow($meeting_where);
            if($meeintg_info){
                //開催中だが参加者がいない場合は削除
                $participant_count = count($this->obj_Participant->getList($meeintg_info["meeting_key"],true));
                if ( $participant_count == 0 ){
                    $this->obj_core_meeting->deleteOnetimeMeetingRoom($room_info);
                    var_dump($room_info);
                }
            } else {
                $this->obj_core_meeting->deleteOnetimeMeetingRoom($room_info);
                var_dump($room_info);
            }
        }
    }


}
$main = new AppOneTimeMeetingRoomDelete();
$main->execute();
