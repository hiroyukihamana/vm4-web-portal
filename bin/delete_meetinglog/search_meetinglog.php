#!/usr/local/bin/php
<?php
require_once("common.php");
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");

class AppMeetinglogSearch extends AppFrame {
    var $dsn = null;
    var $account_dsn = null;

    function init() {
        $this->dsn = $this->get_dsn();
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
    }

    function default_view(){
        if($user_id){
            //指定ユーザの会議記録情報をテキストに落とす。
            $delete_list_flg   = $this->search_meetinglog_record_user($user_id);
        }else{
            //削除ユーザのユーザIDをテキストに落とす。
            $user_key_list_flg = $this->search_meetinglog_document();
            //削除対象会議記情報をテキストに落とす。
            $delete_list_flg   = $this->search_meetinglog_record();
        }
        print(memory_get_peak_usage());
    }
    /* 指定したユーザの会議記録情報を検索しテキストに格納
     */
    private function search_meetinglog_record_user($user_id){
        $delete_list = null;
        $user_db     = new N2MY_DB($this->get_dsn(),"user AS u ,meeting AS m ,meeting_sequence AS ms");
        $where       = sprintf("u.user_id = '%s' AND u.user_key = m.user_key AND m.meeting_key = ms.meeting_key AND (ms.has_recorded_video = 1 OR ms.has_recorded_minutes = 1)",$user_id);
        $columns     = sprintf("u.user_id, m.fms_path, m.meeting_session_id,ms.meeting_sequence_key");
        $list = $user_db->getRowsAssoc($where,array(),null,0,$columns);
        foreach($list as $key => $user_data){
            $delete_list .= $user_data["user_id"]."/".$user_data["fms_path"].$user_data["meeting_session_id"]."/".$user_data["meeting_sequence_key"]."\n";
        }
        $delete_list_flg = ($delete_list) ? 1 : 0;
        if($delete_list_flg){
            if(file_exists(DELETE_RECORD_LIST))unlink(DELETE_RECORD_LIST);
            touch(DELETE_RECORD_LIST);
            $file = file_get_contents(DELETE_RECORD_LIST).$delete_list;
            file_put_contents(DELETE_RECORD_LIST, $file);
        }
        return $delete_list_flg;
    }

    /* 削除ユーザのuser_keyを検索しテキストに格納
     * 削除対象条件
     * 1.削除ユーザ
     * 2.指定した期間に削除されたユーザ
     */
     private function search_meetinglog_document(){
        $user_key_list = null;
        $data_db   = new N2MY_DB($this->get_dsn(),"user");
        $where     = sprintf("user_delete_status = 2 AND (user_deletetime >= '%s' AND user_deletetime <= '%s')",DELETE_DOCUMENT_START,DELETE_DOCUMENT_END);
        $user_list = $data_db->getRowsAssoc($where,array(),null,0,"user_key");
        foreach($user_list as $key => $user_data){
            $user_key_list .= $user_data["user_key"]."\n";
        }
        $user_key_list_flg = ($user_key_list) ? 1 : 0;
        if($user_key_list_flg){
            if(file_exists(DELETE_DOCUMENT_LIST))unlink(DELETE_DOCUMENT_LIST);
            touch(DELETE_DOCUMENT_LIST);
            $file = file_get_contents(DELETE_DOCUMENT_LIST).$user_key_list;
            file_put_contents(DELETE_DOCUMENT_LIST, $file);
        }
        return $user_key_list_flg;
    }

    /* 削除ユーザの会議記録情報を検索しテキストに格納
     * 削除対象条件
     * 1.削除ユーザ
     * 2.has_recorded_minutesとhas_recorded_videoのどちらかが存在する
     * 3.指定したFMSサーバ
     * 4.指定した期間に作成されたシーケンス
     *
     * テキスト格納方式
     * user_key/fms_path/meeting_session_id/meeting_sequence_key
     */
    private function search_meetinglog_record(){
        $delete_list     = null;
        $server_key_list = array();
        $server = new N2MY_DB($this->account_dsn, "fms_server");
        $where  = sprintf("server_address LIKE '%s'",FMS_SERVER_PATTERN);
        $server_list = $server->getRowsAssoc($where,array(),null,0,"server_key");
        foreach($server_list as $_key => $server_info)
            array_push($server_key_list, print_r($server_info["server_key"],true));
        $user_db     = new N2MY_DB($this->get_dsn(),"user AS u ,meeting AS m ,meeting_sequence AS ms");
        $where       = sprintf("u.user_delete_status = 2 AND u.user_key = m.user_key AND m.meeting_key = ms.meeting_key AND (ms.has_recorded_video = 1 OR ms.has_recorded_minutes = 1)");
        $columns     = sprintf("u.user_id, m.fms_path, m.meeting_session_id,ms.meeting_sequence_key,ms.server_key");
        $list = $user_db->getRowsAssoc($where,array(),null,0,$columns);
        foreach($list as $key => $user_data){
            if(in_array($user_data["server_key"],$server_key_list))
                $delete_list .= $user_data["user_id"]."/".$user_data["fms_path"].$user_data["meeting_session_id"]."/".$user_data["meeting_sequence_key"]."\n";
        }
        $delete_list_flg = ($delete_list) ? 1 : 0;
        if($delete_list_flg){
            if(file_exists(DELETE_RECORD_LIST))unlink(DELETE_RECORD_LIST);
            touch(DELETE_RECORD_LIST);
            $file = file_get_contents(DELETE_RECORD_LIST).$delete_list;
            file_put_contents(DELETE_RECORD_LIST, $file);
        }
        return $delete_list_flg;
    }
}
$main =& new AppMeetinglogSearch();
$main->execute();