#!/usr/local/bin/php
<?php
require_once("common.php");
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");

class AppStreamDelete extends AppFrame{
    var $dsn = null;
    var $account_dsn = null;

    function init(){
        $this->dsn = $this->get_dsn();
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
    }

    function default_view(){
        $fms_address = FMS_ADDRESS_101;
        $fms_port    = FMS_PORT;
        $log_file = sprintf("%s_%s.log", STREAM_DELETE_LOG, date('Ymd', time()));
        $fp = fopen($log_file, "a");

        $exec = file_exists(DELETE_RECORD_LIST);
        if( !$exec ) {
            $this->_log($fp,">>>[error] input file not exists! --> exit!!");
            return 0;
        }
        $fms_app_name  = $this->config->get("CORE","app_name");

        $lines = file(DELETE_RECORD_LIST);
        $line_cnt = 0;
        $user_db = new N2MY_DB($this->get_dsn(), "user");
        $meet_db = new N2MY_DB($this->get_dsn(), "meeting");
        $seq_db  = new N2MY_DB($this->get_dsn(), "meeting_sequence");
        $FMS_db  = new N2MY_DB($this->account_dsn, "fms_server");
        foreach ($lines as $line_num => $line) {

            $buf = explode("/", $line);

            $buf_userKey = $buf[0];
            $buf_fmsKey  = $buf[1]."/".$buf[2]."/".$buf[3]."/".$buf[4]."/";
            $buf_seqKey  = $buf[5];

            $where    = "user_key = '".addslashes($buf_userKey)."'";
            $user_row = $user_db->getRow($where);
            $user_id  = $user_row["user_id"];
            $del_flg  = $user_row["user_delete_status"];

            if( $del_flg != 2 ) {
                $this->_log($fp,"user:".$buf_userKey."(".$user_id.")[NG] delete_status!=2");
                continue;
            }

            $where      = "meeting_sequence_key  = '".addslashes($buf_seqKey)."'";
            $seq_row    = $seq_db->getRow($where);
            $server_key = $seq_row["server_key"];

            $where       = "server_key = '".addslashes($server_key)."'";
            $FMS_row     = $FMS_db->getRow($where);
            $server_name = $FMS_row["server_address"];

            if( !stristr($server_name,FMS_SERVER) ) {
                $this->_log($fp,"user:".$buf_userKey."(".$user_id.")[NG] FMS server not nakano");
                continue;
            }

            $url = "http://".$fms_address.":".$fms_port."/fms_sequence_delete.php" .
                   "?app_name=".$fms_app_name.
                   "&key=".$buf_fmsKey.
                   "&sequence_key=".$buf_seqKey;
            $this->_log($fp,"URL for FMS server:".$url);
            $ch = curl_init();
            $option = array(
                CURLOPT_URL            => $url ,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT        => 300,
                );
            curl_setopt_array($ch, $option);
            $result = curl_exec($ch);

            if( $result ) {
                $data = array("has_recorded_minutes" => 0 , "has_recorded_video" => 0 , "meeting_size_used" => 0);
                $seq_db->update($data, "meeting_sequence_key = ".$buf_seqKey);
                $this->_log($fp,"meeting_sequence_key:".$buf_seqKey."[OK] success --> db update");
            } else {
                $this->_log($fp,"meeting_sequence_key:".$buf_seqKey."[NG] ");
            }
/***
            $line_cnt++;
            if( $line_cnt > 99 ) {
                break;
            }
***/
            time_nanosleep(0, ZZZ_TIME);

        }
        fclose($fp);
    }
    private function _log($fp,$string) {
        if ($fp) {
            fwrite($fp, $string . "\n");
        }
    }
}

$main =& new AppStreamDelete();
$main->execute();
?>
