#!/usr/local/bin/php
<?php
require_once("common.php");
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");

class AppDocsDelete extends AppFrame{
    var $dsn = null;
    var $account_dsn = null;

    function init(){
        $this->dsn = $this->get_dsn();
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $fms_app_name  = $this->config->get("CORE","app_name");
    }

    function default_view(){
        $docs_dir    = DOCS_DIR;
        $docs_dir_bk = DOCS_DIR_BK;
        $log_file = sprintf("%s_%s.log", DOCS_DELETE_LOG, date('Ymd', time()));
        $fp = fopen($log_file, "a");

        $exec = file_exists(DELETE_DOCUMENT_LIST);
        if( !$exec ) {
            $this->_log($fp,">>>[error] input file not exists! --> exit!!");
            return 0;
        }

        $lines = file(DELETE_DOCUMENT_LIST);
        $line_cnt = 0;
        $user_db = new N2MY_DB($this->get_dsn(), "user");
    
        foreach ($lines as $line_num => $line) {
    
            $buf = explode("\n", $line);
    
            $buf_userKey = $buf[0];
            $where    = "user_key = '".addslashes($buf_userKey)."'";
            $user_row = $user_db->getRow($where);
            $user_id  = $user_row["user_id"];
            $del_flg  = $user_row["user_delete_status"];
            if( $del_flg != 2 ) {
                $this->_log($fp,"user:".$buf_userKey."(".$user_id.")[NG] delete_status!=2");
                continue;
            }

            $dir    = $docs_dir   .$user_id;
            if( $dir === $docs_dir ) {
                $this->_log($fp,"dir:".$dir."=".$docs_dir."[NG] user_id empty!");
                continue;
            }

            $dir_bk = $docs_dir_bk.$user_id;

            if(!file_exists($dir)) {
                $this->_log($fp,"user:".$buf_userKey."(".$user_id.")[NG] directory not exists!");
            } else {
                if(!file_exists($docs_dir_bk)) {
                    $this->prv_mkdir_r($docs_dir_bk,0777);
                }
                $cmd = "mv ".$dir." ".$dir_bk;
                system($cmd,$ret);
                if($ret){
                    $this->_log($fp,"user:".$buf_userKey."(".$user_id.")[NG] directory mv aborted!");
                } else {
                    $this->_log($fp,"user:".$buf_userKey."(".$user_id.")[OK] success!");
                }
            }

/****    
            $line_cnt++;
            if( $line_cnt > 99 ) {
                break;
            }
****/

            time_nanosleep(0, ZZZ_TIME);

    
        }
        fclose($fp);
    }

    private function prv_mkdir_r($dirName, $rights=0777){
        $dirs = explode('/', $dirName);
        $dir='';
        foreach ($dirs as $part) {
            $dir.=$part.'/';
            if (!is_dir($dir) && strlen($dir)>0)
                mkdir($dir, $rights);
        }
    }

    private function _log($fp,$string) {
        if ($fp) {
            fwrite($fp, $string . "\n");
        }
    }

}

$main =& new AppDocsDelete();
$main->execute();
?>
