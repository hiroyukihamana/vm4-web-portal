<?php
    //個人ホワイトボード：コピー処理スキップフラグ(0:処理実施、1:処理スキップ)
    define('STORAGE_COPY_SKIP_FLG'  ,"0");

    //FMSサーバ検索条件
    define('FMS_SERVER'  ,"nakano");

    //FMSサーバ検索条件(前後の%は変更しない
    define('FMS_SERVER_PATTERN'  ,"%"."nakano"."%");

    //削除予定会議記録の情報リストファイル名
    define('DELETE_RECORD_LIST'  ,dirname(__FILE__)."/record_delete_list.txt");

    //削除予定ドキュメントの情報リストファイル名
    define('DELETE_DOCUMENT_LIST'  ,dirname(__FILE__)."/user_key_list.txt");

    //現在時間より何ヶ月前の情報を収集するか (1=1ヶ月前、2=ヶ月前
    define('DELETE_RECORD_TIME'  ,"-"."1");

    //削除する会議記録を抽出する期間(Y-m-d h:i:s
    define('DELETE_RECORD_START',   "2013-01-01 00:00:00");
    define('DELETE_RECORD_END'  ,   "2016-01-01 00:00:00");

    //削除するドキュメントを抽出する期間(Y-m-d h:i:s
    define('DELETE_DOCUMENT_START', "2013-01-01 00:00:00");
    define('DELETE_DOCUMENT_END'  , "2016-01-01 00:00:00");

    //FMSサーバ
    define('FMS_ADDRESS_101',"nakano101.fcs.nice2meet.us");
    define('FMS_PORT',"8081");

    //docsフォルダ
    define('DOCS_DIR'  ,"/home/storage/meeting.nice2meet.us/htdocs/docs/");
    define('DOCS_DIR_BK'  ,"/home/storage/meeting.nice2meet.us/htdocs_bk/docs/");
    define('ONE_DAY_COPY_DIR'  ,"/home/mtg-user/docs_bk_20160325/");

    //sleep time(msec)
    define('ZZZ_TIME',"0");

    //ログファイル：stream
    define('STREAM_DELETE_LOG'  ,dirname(__FILE__)."/stream_delete_result");

    //ログファイル：docs
    define('DOCS_DELETE_LOG'  ,dirname(__FILE__)."/docs_delete_result");

    //ログファイル：days
    define('ONE_DAY_COPY_LOG'  ,dirname(__FILE__)."/oneday_copy_result");

    //メッセージ
    define('USE_MEM'      ,"\nMemory usage:%s\n");
    define('DOC_START'    ,"\n-> Document search start\n");
    define('DOC_NOT_DEL'  ,"\n>>>There is no document to be deleted <<<\n");
    define('DOC_END'      ,"\n-> Document search end\n");
    define('REC_START'    ,"\n-> Record search start\n");
    define('REC_NOT_DEL'  ,"\n>>>There is no Record to be deleted <<<\n");
    define('REC_END'      ,"\n-> Record search end\n");
    define('NOT_DEL'      ,"\n>>> There is no document and record to be deleted <<<\n");
    define('DEL_CNT'      ,"\nCount:%s\n");
    define('NOT_USER_ID'  ,"\n>>> Error -> Please specify the user_id <<<\n\n".
                           "<Help>\n".
                           "~/php delete_meetinglog.php all                           -> Delete all record and document\n".
                           "~/php delete_meetinglog.php portion [user_id]             -> Delete a record of the specified user\n".
                           "~/php delete_meetinglog.php list ~/record_delete_list.txt -> Delete a record of the specified list\n".
                           "~/php delete_meetinglog.php list ~/user_key_list.txt      -> Delete a document of the specified list\n".
                           "~/php delete_meetinglog.php search                        -> search of record and document\n");
    define('NOT_OPTION'   ,"\n>>> Error -> Please specify the option <<<\n\n".
                           "<Help>\n".
                           "~/php delete_meetinglog.php all                           -> Delete all record and document\n".
                           "~/php delete_meetinglog.php portion [user_id]             -> Delete a record of the specified user\n".
                           "~/php delete_meetinglog.php list ~/record_delete_list.txt -> Delete a record of the specified list\n".
                           "~/php delete_meetinglog.php list ~/user_key_list.txt      -> Delete a document of the specified list\n".
                           "~/php delete_meetinglog.php search                        -> search of record and document\n");
    define('NOT_LIST'     ,"\n>>> Error -> Please specify the listname <<<\n\n".
                           "<Help>\n".
                           "~/php delete_meetinglog.php all                           -> Delete all record and document\n".
                           "~/php delete_meetinglog.php portion [user_id]             -> Delete a record of the specified user\n".
                           "~/php delete_meetinglog.php list ~/record_delete_list.txt -> Delete a record of the specified list\n".
                           "~/php delete_meetinglog.php list ~/user_key_list.txt      -> Delete a document of the specified list\n".
                           "~/php delete_meetinglog.php search                        -> search of record and document\n");
    define('DEL_START'    ,"\n>>> Delete start <<<\n");
    define('DEL_END'      ,"\n>>> Delete end <<<\n");
    define('SEA_COM'      ,"\n>>> Search completed <<<\n");
    define('DEL_CHE'      ,"\nDo you really want to delete this?".
                           "\nyes/no:");
    define('DEL_CAN'      ,"\nStop processing\n");
    define('DEL_STA'      ,"\nStart processing\n");
    define('RE_ENT'       ,"\nPlease enter it again\n");
    define('COL_SEL'      ,"\nPlease selected a collection target".
                           "\ndocument(doc) or record(rec) or all:");
    
?>
