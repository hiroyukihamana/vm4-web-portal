#!/usr/local/bin/php
<?php
$arg_num=$argc;
$arg_yyyymmdd=$argv[1];
require_once("common.php");
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");

class App1dayDocsMove extends AppFrame{
    var $dsn = null;
    var $account_dsn = null;

    function init(){
        $this->dsn = $this->get_dsn();
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
    }

    function default_view(){
        if( ( $GLOBALS["arg_num"] < 2 ) |
            ( mb_strlen($GLOBALS["arg_yyyymmdd"]) != 10 ) ) {
            echo ">>>>> usage : oneday_docs_copy.php ".'"yyyy-mm-dd"'."\n";
            return;
        }

        $p_yy = mb_substr($GLOBALS["arg_yyyymmdd"], 0, 4);
        $p_mm = mb_substr($GLOBALS["arg_yyyymmdd"], 5, 2);
        $p_dd = mb_substr($GLOBALS["arg_yyyymmdd"], 8, 2);
        if( !checkdate( $p_mm, $p_dd, $p_yy ) ) {
            echo ">>>>> Errors in the input date\n";
            return;
        }

        $st_datetime = $p_yy."-".$p_mm."-".$p_dd . " 00:00:00";
        $ed_datetime = $p_yy."-".$p_mm."-".$p_dd . " 23:59:59";
        
        $docs_dir    = N2MY_DOCUMENT_ROOT ."docs/";
        $docs_dir_bk = ONE_DAY_COPY_DIR;

        $log_file    = sprintf("%s_%s.log", ONE_DAY_COPY_LOG, $GLOBALS["arg_yyyymmdd"] );
        $fp          = fopen($log_file, "a");
        $this->_log($fp,"[START]:oneday_docs_copy.php ".$GLOBALS["arg_yyyymmdd"]);

        $docs_db     = new N2MY_DB($this->get_dsn(), "document");
   
        $where       = sprintf("document_status = 2 AND (update_datetime >= '%s' AND update_datetime <= '%s')",$st_datetime,$ed_datetime);
        $docs_rows   = $docs_db->getRowsAssoc($where,array(),null,0,"document_path,document_id");
        foreach ($docs_rows as $docs_row) {
            $docs_path  = $docs_row["document_path"];
            if( $docs_path === null ) {
                $this->_log($fp,"[docs]:[NG] document_path null!");
                continue;
            }
            $docs_id    = $docs_row["document_id"];
            if( $docs_id === null ) {
                $this->_log($fp,"[docs]:[NG] document_id null!");
                continue;
            }

            $dir    = $docs_dir .$docs_path .$docs_id;
            if( $dir === $docs_dir ) {
                $this->_log($fp,"[docs]:".$dir."=".$docs_dir."[NG] document_path/id empty!");
                continue;
            }

            $dir_bk = $docs_dir_bk .$docs_path;

            if(!file_exists($dir)) {
                $this->_log($fp,"[docs]:".$dir."[NG] directory not exists!");
            } else {
                if(file_exists($dir_bk.$docs_id)) {
                    $this->_log($fp,"[docs]:".$dir_bk.$docs_id."[exists] file copy skip!");
                    continue;
                }
                if(!file_exists($dir_bk)) {
                    $this->prv_mkdir_r($dir_bk,0777);
                }
                
                $cmd = "cp -r ";
                $cmd = $cmd .$dir." ".$dir_bk;
                system($cmd,$ret);
                if($ret){
                    $this->_log($fp,"[docs]:".$dir."[NG] directory copy aborted!");
                } else {
                    $this->_log($fp,"[docs]:".$dir."[OK] success!");
                }
            }
        }

        if( STORAGE_COPY_SKIP_FLG === "0" ) {
            $storage_dir    = N2MY_DOCUMENT_ROOT ."docs/";
            $storage_dir_bk = ONE_DAY_COPY_DIR;

            $storage_db = new N2MY_DB($this->get_dsn(), "storage_file");
    
            $where     = sprintf("status = 2 AND extension = '%s' AND category = '%s'AND (create_datetime >= '%s' AND create_datetime <= '%s')","wbs","personal_white",$st_datetime,$ed_datetime);
            $storage_rows = $storage_db->getRowsAssoc($where,array(),null,0,"file_path,file_name");
            foreach ($storage_rows as $storage_row) {
                $storage_filepath = $storage_row["file_path"];
                if( $storage_filepath === null ) {
                    $this->_log($fp,"[storage]:[NG] storage_filepath null!");
                    continue;
                }
                $storage_filename = $storage_row["file_name"];
                if( $storage_filename === null ) {
                    $this->_log($fp,"[storage]:[NG] storage_filename null!");
                    continue;
                }

                $dir    = $storage_dir .$storage_filepath .$storage_filename;
                if( $dir === $storage_dir ) {
                    $this->_log($fp,"[storage]:".$dir."=".$storage_dir."[NG] file_path/name empty!");
                    continue;
                }

                $dir_bk = $storage_dir_bk .$storage_filepath;
                if(!file_exists($dir)) {
                    $this->_log($fp,"[storage]:".$dir."[NG] directory not exists!");
                } else {
                    if(!file_exists($dir_bk)) {
                        $this->prv_mkdir_r($dir_bk,0777);
                    }
                    if(file_exists($dir_bk.$storage_filename)) {
                        $this->_log($fp,"[storage]:".$dir_bk.$storage_filename."[exists] file copy skip!");
                        continue;
                    }
                    $ret = copy( $dir, $dir_bk.$storage_filename );
                    if(!$ret){
                        $this->_log($fp,"[storage]:".$dir."[NG] file copy aborted!");
                    } else {
                        $this->_log($fp,"[storage]:".$dir."[OK] success!");
                    }
                }
            }
        } else {
            $this->_log($fp,"[storage]:STORAGE_COPY_SKIP_FLG is ON ... no exec");
        }

        $this->_log($fp,"[END]:oneday_docs_copy.php ".$GLOBALS["arg_yyyymmdd"]);
        fclose($fp);

    }

    private function prv_mkdir_r($dirName, $rights=0777){
        $dirs = explode('/', $dirName);
        $dir='';
        foreach ($dirs as $part) {
            $dir.=$part.'/';
            if (!is_dir($dir) && strlen($dir)>0)
                mkdir($dir, $rights);
        }
    }

    private function _log($fp,$string) {
        if ($fp) {
            fwrite($fp, $string . "\n");
        }
    }

}

$main =& new App1dayDocsMove();
$main->execute();
?>
