#!/usr/local/bin/php
<?php
require_once("common.php");
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");

class AppMeetinglogSearch extends AppFrame {
    var $dsn = null;
    var $account_dsn = null;

    function init() {
        $this->dsn = $this->get_dsn();
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
    }

    function default_view(){
        global $argv;
        $type    = $argv[1];
        if($type == "all"){
            //全データ削除するか最終確認
            while(1){
                echo DEL_CHE;
                $line = trim(fgets(STDIN));
                if($line == "yes") {
                    echo DEL_STA;
                    break;
                }else if($line == "no"){
                    echo DEL_CAN;
                    return;
                }else{
                    echo RE_ENT;
                }
            }
            //前月分のバックアップを削除
            if(file_exists(DOCS_DIR_BK))system($cmd = "rm -rf ".DOCS_DIR_BK);
            //削除ユーザのユーザIDを収集
            $user_key_list = $this->search_meetinglog_document();
            //削除ユーザの会議記録を収集
            $record_list = $this->search_meetinglog_record_all();
            //削除対象が無いもしくは検索のみの場合処理終了
            if(!$user_key_list && !$record_list){
                echo NOT_DEL;
                return;
            }
         }else if($type == "portion"){
            $user_id = $argv[2];
            if($user_id){
                //削除ユーザのユーザIDを収集
                $user_key_list = $this->search_meetinglog_document($user_id);
                //指定ユーザの会議記録を収集
                $record_list   = $this->search_meetinglog_record_portion($user_id);
             }else{
                echo NOT_USER_ID;
                return;
             }
         }else if($type == "list"){
             //リスト指定削除
             $list = $argv[2];
             if($list == DELETE_DOCUMENT_LIST){
                 echo DEL_START;
                 $this->docs_delete($list);
                 echo DEL_END;
                 return;
             }else if($list == DELETE_RECORD_LIST){
                 echo DEL_START;
                 $this->stream_delete($list);
                 echo DEL_END;
                 return;
             }else{
                 echo NOT_LIST;
                 return;
             }
         }elseif($type == "search"){
            while(1){
                echo COL_SEL;
                $line = trim(fgets(STDIN));
                if($line == "document" || $line == "doc") {
                    //ユーザIDを収集
                    $user_key_list = $this->search_meetinglog_document();
                    echo sprintf(USE_MEM,memory_get_peak_usage());
                    return;
                }else if($line == "record" || $line == "rec"){
                    //会議記録を収集
                    $record_list = $this->search_meetinglog_record_all();
                    echo sprintf(USE_MEM,memory_get_peak_usage());
                    return;
                }else if($line == "all"){
                    //ユーザIDを収集
                    $user_key_list = $this->search_meetinglog_document();
                    //会議記録を収集
                    $record_list = $this->search_meetinglog_record_all();
                    //情報無い場合エラー出力
                    if(!$user_key_list && !$record_list){
                        echo NOT_DEL;
                        echo sprintf(USE_MEM,memory_get_peak_usage());
                        return;
                    }
                    echo sprintf(USE_MEM,memory_get_peak_usage());
                    return;
                }else{
                    echo RE_ENT;
                }
            }
        }else if(!$type){
            echo NOT_OPTION;
            return;
        }
        if($user_key_list or $record_list)echo DEL_START;
        //削除対象が存在する場合Docの削除を実施
        if($user_key_list)$this->docs_delete();
        //削除対象が存在する場合会議記録の削除を実施
        if($record_list)$this->stream_delete();
        if($user_key_list or $record_list)echo DEL_END;
        echo sprintf(USE_MEM,memory_get_peak_usage());
    }

    /* 指定ユーザの会議記録情報を検索しテキストに格納
     */
    private function search_meetinglog_record_portion($user_id = null){
        echo REC_START;
        $record_list = null;
        $cnt         = null;
        $data_db     = new N2MY_DB($this->get_dsn(),"user");
        $where   = sprintf("user_id = '%s'",$user_id);
        $user_data = $data_db->getRow($where,"user_delete_status");
        if($user_data["user_delete_status"] == 2){
            $user_db = new N2MY_DB($this->get_dsn(),"user AS u ,meeting AS m ,meeting_sequence AS ms");
            $where   = "u.user_id = '".$user_id."' ".
                       "AND u.user_key = m.user_key ".
                       "AND m.meeting_key = ms.meeting_key ".
                       "AND (ms.has_recorded_video = 1 OR ms.has_recorded_minutes = 1)";
            $columns = "u.user_id, m.fms_path, m.meeting_session_id,ms.meeting_sequence_key,ms.server_key";
            $list    = $user_db->getRowsAssoc($where,array(),null,0,$columns);
            foreach($list as $key => $user_data){
                $record_list .= $user_data["user_id"]."/".$user_data["fms_path"].$user_data["meeting_session_id"]."/".$user_data["meeting_sequence_key"]."/".$user_data["server_key"]."\n";
                $cnt++;
            }
        }
        if($cnt)echo  sprintf(DEL_CNT,$cnt);
        $delete_flg = ($record_list) ? 1 : 0;
        if($delete_flg){
            if(file_exists(DELETE_RECORD_LIST))unlink(DELETE_RECORD_LIST);
            touch(DELETE_RECORD_LIST);
            $file = file_get_contents(DELETE_RECORD_LIST).$record_list;
            file_put_contents(DELETE_RECORD_LIST, $file);
        }
        $rec = (!$record_list) ? REC_NOT_DEL : REC_END;
        echo $rec;
        return $record_list;
    }

    /* 会議記録情報を検索しテキストに格納
     * 削除対象条件
     * 1.削除ユーザ
     * 2.has_recorded_minutesとhas_recorded_videoのどちらかが存在する
     * 4.指定した期間に作成されたシーケンス
     */
    private function search_meetinglog_record_all(){
        echo REC_START;
        $record_list = null;
        $cnt         = null;
        $user_db = new N2MY_DB($this->get_dsn(),"user AS u ,meeting AS m ,meeting_sequence AS ms");
        $where   = "u.user_delete_status = 2 ".
                   "AND u.user_key = m.user_key ".
                   "AND m.meeting_key = ms.meeting_key ".
                   "AND (ms.has_recorded_video = 1 OR ms.has_recorded_minutes = 1) ".
                   "AND ms.create_datetime BETWEEN '".date("Y-m-d h:i:s",strtotime(DELETE_RECORD_TIME."month"))."'".
                   "AND '".date("Y-m-d h:i:s")."'";
        $columns = "u.user_id, m.fms_path, m.meeting_session_id,ms.meeting_sequence_key,ms.server_key";
        $list    = $user_db->getRowsAssoc($where,array(),null,0,$columns);
        foreach($list as $key => $user_data){
            $record_list .= $user_data["user_id"]."/".$user_data["fms_path"].$user_data["meeting_session_id"]."/".$user_data["meeting_sequence_key"]."/".$user_data["server_key"]."\n";
            $cnt++;
        }
        if($cnt)echo  sprintf(DEL_CNT,$cnt);
        $delete_flg = ($record_list) ? 1 : 0;
        if($delete_flg){
            if(file_exists(DELETE_RECORD_LIST))unlink(DELETE_RECORD_LIST);
            touch(DELETE_RECORD_LIST);
            $file = file_get_contents(DELETE_RECORD_LIST).$record_list;
            file_put_contents(DELETE_RECORD_LIST, $file);
        }
        $rec = (!$record_list) ? REC_NOT_DEL : REC_END;
        echo $rec;
        return $record_list;
    }

    /* 削除ユーザのuser_keyを検索しテキストに格納
     * 削除対象条件
     * 1.削除ユーザ
     * 2.指定した期間に削除されたユーザ
     */
    private function search_meetinglog_document($user_id =null){
        echo DOC_START;
        $user_key_list = null;
        $cnt           = null;
        $data_db   = new N2MY_DB($this->get_dsn(),"user");
        $where     = "user_delete_status = 2 ".
                     "AND (user_deletetime >= '".date("Y-m-d h:i:s", strtotime(DELETE_RECORD_TIME."month"))."'".
                     "AND user_deletetime <= '".date("Y-m-d h:i:s")."')";
        if( $user_id ){
            $where = "user_id = '".addslashes($user_id)."'"." AND " . $where;
        }
        $user_list = $data_db->getRowsAssoc($where,array(),null,0,"user_key");
        foreach($user_list as $key => $user_data){
            $user_key_list .= $user_data["user_key"]."\n";
            $cnt++;
        }
        if($cnt) echo  sprintf(DEL_CNT,$cnt);
        $delete_flg = ($user_key_list) ? 1 : 0;
        if($delete_flg){
            if(file_exists(DELETE_DOCUMENT_LIST))unlink(DELETE_DOCUMENT_LIST);
            touch(DELETE_DOCUMENT_LIST);
            $file = file_get_contents(DELETE_DOCUMENT_LIST).$user_key_list;
            file_put_contents(DELETE_DOCUMENT_LIST, $file);
        }
        $doc = (!$user_key_list) ? DOC_NOT_DEL : DOC_END;
        echo $doc;
        return $user_key_list;
    }

    /*Docsバックアップ
     */
    private function docs_delete($list = null){
        $docs_dir    = DOCS_DIR;
        $docs_dir_bk = DOCS_DIR_BK;
        $log_file = sprintf("%s_%s.log", DOCS_DELETE_LOG, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        $user_db = new N2MY_DB($this->get_dsn(), "user");

        if($list){
            $exec = file_exists($list);
        }else{
            $exec = file_exists(DELETE_DOCUMENT_LIST);
        }
        if( !$exec ) {
            $this->_log($fp,">>>[error] input file not exists! --> exit!!");
            fclose($fp);
            return 0;
        }

        $user_key_list = file(DELETE_DOCUMENT_LIST);
        foreach ($user_key_list as $key => $user_data){
            $buf = explode("\n", $user_data);
            $user_key = $buf[0];
            $where    = sprintf("user_key  = %s",$user_key);
            $user_row = $user_db->getRow($where,"user_id");
            $user_id  = $user_row["user_id"];
            $dir      = $docs_dir.$user_id;
            $dir_bk   = $docs_dir_bk.$user_id;

            if(!file_exists($dir)){
                $this->_log($fp,"user:".$user_key."(".$user_id.")[NG] directory not exists!");
            }else{
                if(!file_exists($docs_dir_bk))$this->prv_mkdir_r($docs_dir_bk,0777);
                $cmd = "mv ".$dir." ".$dir_bk;
                system($cmd,$ret);
                if($ret){
                    $this->_log($fp,"user:".$user_key."(".$user_id.")[NG] directory mv aborted!");
                } else {
                    $this->_log($fp,"user:".$user_key."(".$user_id.")[OK] success!");
                }
            }
            time_nanosleep(0, ZZZ_TIME);
        }
        fclose($fp);
    }

    /*会議記録削除
    */
    private function stream_delete($list = null){
        $log_file = sprintf("%s_%s.log", STREAM_DELETE_LOG, date('Ymd', time()));
        $fp = fopen($log_file, "a");

        if($list){
            $exec = file_exists($list);
        }else{
            $exec = file_exists(DELETE_RECORD_LIST);
        }
        if( !$exec ) {
            $this->_log($fp,">>>[error] input file not exists! --> exit!!");
            fclose($fp);
            return 0;
        }
        $server  = new N2MY_DB($this->account_dsn, "fms_server");
        $seq_db  = new N2MY_DB($this->get_dsn(), "meeting_sequence");
        $fms_app_name  = $this->config->get("CORE","app_name");
        $lines = file(DELETE_RECORD_LIST);
        foreach ($lines as $line_num => $line) {

            $buf = explode("/", $line);

            $buf_userKey = $buf[0];
            $buf_fmsKey  = $buf[1]."/".$buf[2]."/".$buf[3]."/".$buf[4]."/";
            $buf_seqKey  = $buf[5];
            $buf_svrkey  = $buf[6];

            $where   = sprintf("server_key ='%s'", $buf_svrkey);
            $columns = sprintf("server_address, server_port");
            $list    = $server->getRowsAssoc($where,array(),null,0,$columns);
            foreach($list as $_key => $server_info){
                $fms_address = $server_info["server_address"];
                $fms_port    = $server_info["server_port"];
            }
            $url = "http://".$fms_address.":".$fms_port."/fms_sequence_delete.php" .
                    "?app_name=".$fms_app_name.
                    "&key=".$buf_fmsKey.
                    "&sequence_key=".(int)$buf_seqKey;
            $this->_log($fp,"URL for FMS server:".$url);
            $ch = curl_init();
            $option = array(
                    CURLOPT_URL            => $url ,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_CONNECTTIMEOUT => 10,
                    CURLOPT_TIMEOUT        => 300,
            );
            curl_setopt_array($ch, $option);
            $result = curl_exec($ch);
            if($result){
                $data = array("has_recorded_minutes" => 0 , "has_recorded_video" => 0 , "meeting_size_used" => 0);
                $seq_db->update($data, "meeting_sequence_key = ".$buf_seqKey);
                $this->_log($fp,"meeting_sequence_key:".$buf_seqKey."[OK] success --> db update");
            } else {
                $this->_log($fp,"meeting_sequence_key:".$buf_seqKey."[NG] ");
            }
        }
        fclose($fp);
    }

    private function prv_mkdir_r($dirName, $rights=0777){
        $dirs = explode('/', $dirName);
        $dir='';
        foreach ($dirs as $part) {
            $dir.=$part.'/';
            if (!is_dir($dir) && strlen($dir)>0)
                mkdir($dir, $rights);
        }
    }

    private function _log($fp,$string) {
        if ($fp) {
            fwrite($fp, $string . "\n");
        }
    }

}
$main =& new AppMeetinglogSearch();
$main->execute();
