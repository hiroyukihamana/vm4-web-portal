#!/usr/local/bin/php
<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("lib/EZLib/EZUtil/EZRtmp.class.php");
require_once("lib/EZLib/EZUtil/EZFile.class.php");
require_once("classes/core/dbi/ConvertFile.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/N2MY_Document.class.php");
require_once("classes/mcu/resolve/Resolver.php");


class AppConvertDocument extends AppFrame {

    var $_mail_flg = 1;
    function __destruct() {
    unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
  }
    function init() {
        $this->dsn = $this->get_dsn();
        $this->obj_ConvertFile = new DBI_ConvertFile(N2MY_MDB_DSN);
        $this->obj_N2MY_Document = new N2MY_Document($this->dsn);
        $this->doc_conf = $this->config->getAll("DOCUMENT");
    }

    function default_view() {
        //多重起動処理
        $_status = "";
        $pidFile = N2MY_APP_DIR.'/bin/batch/sendEdContents.pid';
        if(file_exists($pidFile)){
            $_pid = trim(file_get_contents($pidFile));
            system("ps {$_pid} ", $_status);
            $mailFile = N2MY_APP_DIR.'/bin/batch/mailSend_'.$_pid;
            $mail_flag = file_exists($mailFile);
            if($_status){
                unlink($pidFile);
                if($mail_flag){
                    unlink($mailFile);
                }
            } else {
                $file_time = filemtime($pidFile);
                $time = time() - $file_time;
                if ($time > 120 && !$mail_flag) {
                    $this->logger2->warn("変換処理中のため、資料変換処理の多重起動を中止しました。 time:".$time);
                    $mail_subject = "ミーティング資料変換処理に120秒以上がかかっております：".N2MY_LOCAL_URL;
                    $mail_body = "ミーティング資料変換処理に120秒以上がかかっております" ."\n\n" .
                                "■ Pid: ".$_pid."\n";
                    if (defined("N2MY_ERROR_FROM") && defined("N2MY_ERROR_TO") && N2MY_ERROR_FROM && N2MY_ERROR_TO) {
                        $this->sendReport(N2MY_ERROR_FROM, N2MY_ERROR_TO, $mail_subject, $mail_body);
                    }
                    system('echo '.$_pid.' > '.$mailFile, $_status);
                    if($_status){
                        $this->logger2->error("mailSendファイルの作成に失敗しました。");
                    }
                    exit;
                } else if ($time < 600) {
                    $this->logger2->warn("変換処理中のため、資料変換処理の多重起動を中止しました。 time:".$time);
                    exit;
                } else {
                    $this->logger2->error("convert.phpがハングしました。");
                    $mail_subject = "ミーティング資料変換でタイムアウトが発生しました（600秒経過）";
                    $mail_body = "ミーティング資料変換でタイムアウトが発生（600秒経過）したため、新規プロセスで変換実行しました。" ."\n\n" .
                                "■ Pid: ".$_pid."\n";
                    if (defined("N2MY_ALERT_FROM") && defined("N2MY_ALERT_TO") && N2MY_ALERT_FROM && N2MY_ALERT_TO) {
                        $this->sendReport(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body);
                    }
                    if($mail_flag){
                        unlink($mailFile);
                    }
                }
            }
        }
        $this->logger2->trace(array($_pid,getmypid()));
        system('echo '.getmypid().' > '.$pidFile, $_status);
        if($_status){
            //メールを飛ばす　
            $this->logger2->error("sendEdContents.phpはPIDファイルの作成に失敗しました。");
            exit;
        }

        $where = "status = 0";
        $sort = array(
            "priority" => "asc",
            "create_datetime" => "asc",
        );
        $convetFile = $this->obj_ConvertFile->getRow($where, "*", $sort);
        $this->logger2->debug($convetFile);
        if (DB::isError($convetFile)) {
            $this->logger2->error($convetFile->getUserInfo());
            exit;
        } else if (!$convetFile) {
            //変換ファイルがないのでexit
      $this->logger->debug("not convert file");
            exit;
        }
        $document_id = $convetFile["document_id"];
        $input_file = N2MY_DOC_DIR.$convetFile["document_path"].$document_id.".".$convetFile["extension"];
        $file_info = $this->obj_N2MY_Document->file_info($input_file);
        if ($file_info["type"] == "image") {
            $this->logger2->info("startConvert : ".$document_id);
            $this->obj_N2MY_Document->setStatus($document_id, 1);
            // 画像
            $option = unserialize($convetFile["addition"]);
            $output_file = N2MY_DOC_DIR.$convetFile["document_path"]."/".$document_id."/".$document_id.".".$option["ext"];
            $ret = $this->obj_N2MY_Document->convertImage($input_file, $output_file, $option);
            if ($ret) {
                // 1枚の場合はファイル名をあわせる
                if ($ret == 1) {
                    $_output_file = N2MY_DOC_DIR.$convetFile["document_path"].$document_id."/".$document_id."_1.".$option["ext"];
                    rename($output_file, $_output_file);
                }
                // 変換完了後にステータス変更
                $this->obj_N2MY_Document->successful($document_id, $ret);
            } else {
                $this->obj_N2MY_Document->setStatus($document_id, DOCUMENT_STATUS_ERROR);
            }
        // 資料変換を通知
        } elseif ($file_info["type"] == "document") {
            $url_params = parse_url(N2MY_LOCAL_URL);
            $host_name = $url_params["host"];
            $port = ($url_params["port"]) ? $url_params["port"] :80;
            $rtmp = new EZRtmp();
            $fms_list = split(",",$this->doc_conf["fms"]);
            // 接続可能なFMSに送信
            $documentRecord = $this->obj_N2MY_Document->getDetail($convetFile["document_id"]);
            $meetingObj = new MeetingTable($this->dsn);
            $meetingInfo = $meetingObj->findActiveMeetingByMeetingKey($documentRecord["meeting_key"]);
            $format = (($convetFile["document_format"] == "vector") && Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"])) ? "hybrid" : $convetFile["document_format"];
          foreach($fms_list as $fms) {
                $document_app = sprintf($this->doc_conf["rtmp"], $fms, $this->doc_conf["appli"]);
                
                $cmd = "\"".
                       "h = RTMPHack.new; " .
                       " h.connection_uri = '".$document_app."';" .
                       " h.connection_args = ['web'];" .
                       " h.method_name = 'startConvert';" .
                       " h.method_args = ['".$document_id."'" .
                       ", '".$this->doc_conf["user"]."@".$host_name.":".N2MY_DOC_DIR.$convetFile["document_path"]."/'" .
                       ", '".$this->doc_conf["user"]."@".$host_name.":".N2MY_DOC_DIR.$convetFile["document_path"]."/'" .
                       ", '".N2MY_LOCAL_URL."/api/document/status.php?action_dispatch'" .
                       ", '".$convetFile["extension"]."'" .
                       ", '".$format."'" .
                       ", '".$convetFile["document_version"]."'" .
                       "];".
                       " h.execute;".
                       "\"";
                       $this->logger2->info($cmd);
                $result = $rtmp->run($cmd);
                list($active, $max_connection) = split("/", $result);
                // n/x の形式でない場合はエラーとする。
                $this->logger2->info(array($result,$active,$max_connection));
                if ($active > 0 && $max_connection > 0) {
                    break;
                } else {
                    $this->logger2->warn(array($fms, $result), "DOCクライアントが接続されていない");
                }
            }
            // 変換通知成功
            if ($active > 0) {
                $this->logger2->info("startConvert : ".$document_id);
                $this->obj_N2MY_Document->setStatus($document_id, 1);
                exit;
            } else {
                if ($this->_mail_flg) {
                    $this->logger2->info($document_id." - ", "待機中のDOCクライアントが存在しないため、そのまま待機");
                    $mail_subject = "DOCクライアントがすべて利用中のため、待機";
                    $mail_body = "DOCクライアントがすべて利用中のため、待機になりました。" ."\n\n" .
                        "■ URL: ".$host_name."\n" .
                        "■ FMS サーバ: ".print_r($fms_list, true). "\n" .
                        "■ アプリケーション名: ".$document_app. "\n" .
                        "■ 資料ID: ".$document_id;
                    if (defined("N2MY_LOG_ALERT_FROM") && defined("N2MY_LOG_ALERT_TO") && N2MY_LOG_ALERT_FROM && N2MY_LOG_ALERT_TO) {
                        $this->sendReport(N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body);
                    }
                    $this->logger2->warn(array(N2MY_LOG_ALERT_FROM, N2MY_LOG_ALERT_TO, $mail_subject, $mail_body));
                }

            }
        }
    }

    function sendReport($mail_from, $mail_to, $mail_subject, $mail_body) {
        require_once("lib/EZLib/EZMail/EZSmtp.class.php");
        // 入力チェック
        if (!$mail_from || !$mail_to || !$mail_subject || !$mail_body) {
            return false;
        }
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang = EZLanguage::getLangCd("ja");
        $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
        $obj_SendMail->setFrom($mail_from);
        $obj_SendMail->setReturnPath($mail_from);
        $obj_SendMail->setTo($mail_to);
        $obj_SendMail->setSubject($mail_subject);
        $obj_SendMail->setBody($mail_body);
        // メール送信
        $result = $obj_SendMail->send();
        $this->logger2->info(array($mail_from, $mail_to, $mail_subject, $mail_body));
    }
}
$main = new AppConvertDocument();
$main->execute();
