<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/ONE_Account.class.php");


class AppConvertDocument extends AppFrame {

    const TIME_OUT = 600;
    const LIMIT_NUMBER = 10;

    private $obj_ONE_Account;
    private $member_key;
    private $limit_number;
    private $result_log_dir;
    private $option;
    private $num;
    private $help_text = "
---------------------------------------------------------------------
- 使用法  :  sales_room_quere.php option1 【option2】
---------------------------------------------------------------------
-n 【num】               	:     キューを処理する数を指定し、num分の数を最大にして処理をする(デフォルト10)
					※保留状態は対象にしない基本cronではこれに数字をセットして使用する
-all                     	:     全件処理 ※保留状態も対象
-m 【member_key】        	:     キューに対象メンバーキーが存在した場合追加保留状態であっても追加する
-cancel  【member_key】  	:     対象のメンバーのキューを削除
-allCancel               	:     全てキューを削除する
-h                       	:     help コマンド使い方を表示
-p 【member_key】        	:     対象のメンバーキーがあれば保留状態にする
-a 【member_key】        	:     キューに追加する 
					※同じメンバーキーは追加出来ない 
					※存在しないメンバーキー、userにセールスオプションがなければ追加しない
-forceAdd  【member_key】	:     キューにありなし関係なく対象のメンバーキーにセールス部屋を追加 
					※ただしセールス部屋は一つだけしか持たないのは変わらない 
					※userのセールスフラグは確認する
-l 【num】               	:     num分だけキューのリストを表示(デフォルト10) \n";
    

    function init() {
        $this->obj_ONE_Account = new ONE_Account( parent::get_dsn() );
    }
    
    /*
    * 引数格納と数値チェック
    * member_key指定に限り数値ではない場合は処理は実施せず、エラー内容をコンソールに表示
    */
    function __construct($argv) {
        parent::__construct();
    	$this->option = $argv[1];
    	$this->num = $argv[2];    	
    }

    // main
    function default_view() {
    	$this->log_pre();
        $this->check_number();
    	$this->check_option();
		$this->check_duplicate();
    	$this->handling_option();
    }  

    /*
    * 数値チェック
    * member_key指定に限り数値ではない場合は処理は実施せず、エラー内容をコンソールに表示
    */
    function check_number(){
    	switch ($this->option) {
    		case "-n":
    		case "-l":
        		if($this->num && ctype_digit($this->num)){
    	   	    	$this->limit_number = $this->num;    		    	
    		    }else{
        	    	//echo("指定した数は数値ではないため10に置き換えて実行します\n");
    	   	    	$this->limit_number = self:: LIMIT_NUMBER;
    		    }
                break;    
            case "-m":
            case "-cancel":
            case "-p":
            case "-a":
            case "-forceAdd":
          		$this->member_key = $this->num;
               	if(!$this->num || !ctype_digit($this->num)){
         		   	echo("指定した menber_key に不備があるため実行できません\n");
		    	    $this->log_writer("member_key error");
            	}
            	break;
        }
    }

    /**
     * チェック系のため、多重起動許容
     */
    function check_option(){
        switch ($this->option) {
        case "-h":
            echo($this->help_text);
            $this->log_writer();
            break;
        case "-l":
       	    $result = $this->obj_ONE_Account->listQueue($this->limit_number);
            if($result[0] == false){
    	        $this->log_writer($result[1]);
                break;
            } else {
                foreach($result as $res_line) {
                    echo "-------------------------------------\n";
                    echo "queue_key       :" . $res_line["sales_room_queue_key"] . "\n";
                    echo "member_key      :" . $res_line["member_key"] . "\n";
                    echo "error_count     :" . $res_line["error_count"] . "\n";
                    echo "pend            :" . $res_line["pend"] . "\n";
                    echo "create_detatime :" . $res_line["create_detatime"] . "\n";
                    echo "update_datetime :" . $res_line["update_datetime"] . "\n";
                }
                $this->log_writer();
            }
            break;
        }
    }

    /**
	 * メイン処理のため、多重起動不可
	 */
    function handling_option(){

        switch ($this->option) {
        case "-n":
            $result = $this->obj_ONE_Account->numExecFromQueue($this->limit_number);
            break;               	
        case "-all":
            $result = $this->obj_ONE_Account->allExecFromQueue();
            break;               	
        case "-m":
            $result = $this->obj_ONE_Account->selExecFromQueue($this->member_key);
            break;               	
        case "-cancel":
            $result = $this->obj_ONE_Account->selQueueCancel($this->member_key);
            break;               	
        case "-allCancel":
            $result = $this->obj_ONE_Account->allQueueCancel();
            break;               	
        case "-p":
            $result = $this->obj_ONE_Account->pendSetQueue($this->member_key);
            break;               	
        case "-a":
            $result = $this->obj_ONE_Account->addQueue($this->member_key);
            break;               	
        case "-forceAdd":
            $result = $this->obj_ONE_Account->forceAddRoom($this->member_key);
            break;               	
        default:
            echo("オプション不備のため実行できません\n");
    	    $this->log_writer("option error");
        }

        if($result[0] == false){
    	    $this->log_writer($result[1]);
        } else{
    	    $this->log_writer($messege);
    	}
    }

    function check_duplicate(){
        $_status = "";
        $pidFile = N2MY_APP_DIR.'/bin/batch/salesRoomQuere.pid';
        if(file_exists($pidFile)){
            $_pid = trim(file_get_contents($pidFile));
            system("ps {$_pid} ", $_status);
            $mailFile = N2MY_APP_DIR.'/bin/batch/mailSend_'.$_pid;
            $mail_flag = file_exists($mailFile);
            if($_status){ 
                unlink($pidFile);
                if($mail_flag){
                    unlink($mailFile);
                }
            } else {
                $file_time = filemtime($pidFile);
                $time = time() - $file_time;
                if ($time < self:: TIME_OUT) {
         	    	$this->log_writer("Multiple start-up");
         	    	echo("多重起動のため実行を中止しました。");
                    $this->logger2->warn("多重起動のため実行を中止しました。 time:".$time);
                    exit;
                } else {
                    $this->_log ( "# time_out" ,  $this->result_log_dir);
                    $this->logger2->warn("sales_room_queue処理がハングしました。");
                    $mail_subject = "sales_room_queue処理にてタイムアウトが発生しました（600秒経過）";
                    $mail_body = "sales_room_queue処理にてタイムアウトが発生（600秒経過）したため、新規プロセスで実行しました。" ."\n\n" .
                                "■ Pid: ".$_pid."\n";
                    if (defined("N2MY_ALERT_FROM") && defined("N2MY_ALERT_TO") && N2MY_ALERT_FROM && N2MY_ALERT_TO) {
                        $this->sendReport(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body);
                    }
                    if($mail_flag){
                        unlink($mailFile);
                    }
                }
            }
        }
        $this->logger2->trace(array($_pid,getmypid()));
        system('echo '.getmypid().' > '.$pidFile, $_status);
        if($_status){
            //メールを飛ばす　
            $this->logger2->error("sales_room_queue.phpはPIDファイルの作成に失敗しました。");
            exit;
        }

    }


    // ログ出力前提作業
    private function log_pre(){
        $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $this->result_log_dir = $log_dir;
        return $result_log_dir;
    }

    /*
     * ログへの書き込み
     */
    private function log_writer( $messege=null ){
	    $time = date("m/d/H:i:s");
        if ( $messege ) {
     	    $log_messege = "# error    date:" . $time . " option:" . $this->option . " num:" . $this->limit_number . " member_key:" .$this->member_key .  " messege:" . $messege;
            $this->_log ( $log_messege  ,  $this->result_log_dir);
        } else {
        	// 正常系
     	    $log_messege = "# succcess date:" . $time . " option:" . $this->option . " num:" . $this->limit_number  . " member_key:" .$this->member_key . " messege:" . $messege;
            $this->_log ( $log_messege ,  $this->result_log_dir);
        }
        exit();
    }

    private function _log($string , $result_log_dir){
        $log_prefix = "sales_room_quere";
        $log_file = sprintf("%s/%s_%s.log", $result_log_dir, $log_prefix, date('Ym', time()));
        $fp = fopen($log_file, "a");
        if($fp){
            fwrite($fp, $string."\n");
        }
        fclose($fp);
    }

    function sendReport($mail_from, $mail_to, $mail_subject, $mail_body) {
        require_once("lib/EZLib/EZMail/EZSmtp.class.php");
        // 入力チェック
        if (!$mail_from || !$mail_to || !$mail_subject || !$mail_body) {
            return false;
        }
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang = EZLanguage::getLangCd("ja");
        $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
        $obj_SendMail->setFrom($mail_from);
        $obj_SendMail->setReturnPath($mail_from);
        $obj_SendMail->setTo($mail_to);
        $obj_SendMail->setSubject($mail_subject);
        $obj_SendMail->setBody($mail_body);
        // メール送信
        $result = $obj_SendMail->send();
        $this->logger2->info(array($mail_from, $mail_to, $mail_subject, $mail_body));
    }
}
$main = new AppConvertDocument($argv);
$main->execute();
