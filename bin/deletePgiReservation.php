#!/usr/local/bin/php
<?php
date_default_timezone_set('Asia/Tokyo');
$_tz = (int)(substr( date( 'O' ), 0, 3));
define("N2MY_SERVER_TIMEZONE", $_tz);

require_once("set_env.php");
require_once 'classes/AppFrame.class.php';
require_once 'classes/N2MY_PGi_Client.class.php';
require_once 'classes/core/dbi/Meeting.dbi.php';
require_once "classes/dbi/pgi_setting.dbi.php";
require_once "classes/pgi/PGiSystem.class.php";
require_once("classes/pgi/config/PGiConfig.php");
require_once "classes/mgm/dbi/pgi_session.dbi.php";

class AppPgiCron extends AppFrame {

    var $pgiConfig;
    private $pgi_client;
    //{{{ init
    function init()
    {
        $this->pgiConfig = PGiConfig::getInstance();
    }

    //}}}
    //{{{default_view
    function default_view()
    {
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $yesterday = date("Y-m-d 23:59:59", strtotime("-1 Day"));
        $where = "is_active = 0 AND pgi_conference_status = 1".
                 " AND pgi_api_status = 'complete'".
                 " AND pgi_service_name = 'GlobalMeet2'".
                 " AND update_datetime < '".$yesterday."'";
        $columns = "meeting_key,meeting_ticket,room_key,pgi_api_status,pgi_conference_id";
        
        $meetings = $obj_Meeting->getRowsAssoc($where, array("meeting_key" => "desc"), null, 0, $columns);
        $this->logger2->debug($meetings);
        
        if( 0 == count( $meetings ) ) {
        	$this->logger2->info( 'delete pgi reservation target is empty.' );
        	return;
        }
        
        $_log = array();
        $_count = array();
        foreach ($meetings as $meeting) {
        	$setting  = new PGiSettingTable($this->get_dsn());
            $where = sprintf("room_key = '%s'", mysql_real_escape_string($meeting["room_key"])).
                     " AND is_deleted = 0";
            $setting = $setting->getRow($where);
            $system  = PGiSystem::findBySystemKey($setting['system_key']);
            $this->logger2->debug($setting);
            if ( !$setting) {
            	//$this->logger2->warn($meeting, "pgi setting error");
            	$this->updateMeeting($meeting['meeting_ticket'], array('pgi_api_status' => 'delete-failed') );
            	$_log['failed']['pgi_setting'][ $meeting['room_key'] ][] = $meeting['meeting_key'];
            	$_count['failed']++;
            	continue;
            }
            
            $result = $this->deletePGIResevationOfMeeting($meeting, $setting, $system);
            if( !$result ){
            	$_log['failed']['call_delete'][ $meeting['room_key'] ][] = $meeting['meeting_key'];
            	$_count['failed']++;
            } else {
            	$_log['success'][ $meeting['room_key'] ][] = $meeting['meeting_key'];
            	$_count['success']++;
            }
            $this->logger2->debug( json_encode( array( $meeting['room_key']=>$meeting['meeting_key'], 'count' => $_count ) ), 'debug loop' );
            // break;
            sleep(1);
        }
        $this->logger2->info( json_encode( array( 'count' => $_count, 'result' => $_log ) ), 'delete pgi reserv result.' );
        if( $_log['failed'] ) {
        	$this->logger2->warn( json_encode( array( 'count' => $_count, 'result' => $_log ) ), 'delete pgi reserv result.' );
        }
        //exit;
    }
    //}}}
    //{{{ deletePGIResevationOfMeeting
    private function deletePGIResevationOfMeeting($meeting, $setting, $pgi_system)
    {
        $this->updateMeeting($meeting['meeting_ticket'], array('pgi_api_status' => 'delete-wait'));

        $system  = PGiSystem::findBySystemKey($setting['system_key']);
        $setting["service_name"] = (string)$system->serviceName;
        
        $result = $this->deletePGIResevation($setting, $meeting['pgi_conference_id'], $pgi_system);
        $data = array();
        if($result){
        	$data = array('pgi_api_status' => 'complete',
        			'pgi_conference_status' => 0);
        } else {
        	$data = array('pgi_api_status' => 'delete-failed');
        }
        $this->updateMeeting($meeting['meeting_ticket'], $data);

        return $result ? true : false;
    }
    
    private function deletePGIResevation($pgi_setting, $pgi_conference_id, $pgi_system)
    {
        $token = $this->checkPGiLogin((string)$pgi_system->webID, (string)$pgi_system->webPW, $pgi_system, $pgi_setting);
        $this->pgi_client = $this->createPGIClient($pgi_system, $this->pgiConfig->get("PGI", "reservation_url"));
        $params = array('token'          => $token,
                        'client_id'             => $pgi_setting["pgi_client_id"],
                        'client_pw'             => $pgi_setting["pgi_client_pw"],
                        'pass_code_type' => 'Random10DigitStrong',
                        'service_name'   => (string)$pgi_system->serviceName,
                        'conf_id'        => $pgi_conference_id,
                        'pgi_services_url'     => $this->pgiConfig->get("PGI", "pgi_services_url"),
                        'pgi_schemas_client_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_client_url"),
                        'pgi_schemas_common_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_common_url"),
                        'pgi_schemas_reservation_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_reservation_url"),
                        'pgi_serialization_arrays_url'     => $this->pgiConfig->get("PGI", "pgi_serialization_arrays_url"));
        $headers['SOAPAction'] = '"'.$this->pgiConfig->get("PGI", 'reservation_delete_soapaction').'"';
        $headers['Content-Type'] = "text/xml; charset=utf-8";
        $res        = $this->pgi_client->call('delete_reservation', $params, $headers);
        $this->logger2->debug($res);
        $error_msg = $res['ReservationDeactivateResponse'][0]['ReservationDeactivateResult'][0]['Errors'][0]["ApiError"][0];
        if ($error_msg) {
        	$this->logger2->warn( json_encode( array('request' => $params, 'response' => $res) ), 'failed delete pgi reserv.' );
        	return false;
        }
        return true;
    }
    
    function checkPGiLogin ($web_id, $web_pw ,$pgi_system, $pgi_setting) 
    {
        if (!$web_id || !$web_pw || !$pgi_system || !$pgi_setting){
            throw new Exception ("pgi login parameter failed ");
        }
        $this->logger2->debug($pgi_system);
        $obj_PGiSession        = new PGiSessionTable(N2MY_MDB_DSN);
        $where = "system_key = '".mysql_real_escape_string((string)$pgi_system->key)."'".
                 " AND company_id = ".mysql_real_escape_string($pgi_setting["company_id"])."";
        $session_data = $obj_PGiSession->getRow($where);
        if (!$session_data) {
            $add_data = array("system_key" => mysql_real_escape_string((string)$pgi_system->key),
                              "company_id" => mysql_real_escape_string($pgi_setting["company_id"]));
            $obj_PGiSession->add($add_data);
        }
        
        $session_expire_time = "";
        if ($session_data["pgi_session_token"]) {
            $session_expire_time = date("Y-m-d H:i:s", strtotime($session_data["update_datetime"]." 10 hour"  ));
        }
        
        if ($pgi_setting["admin_client_id"]) {
            $admin_client_id = $pgi_setting["admin_client_id"];
            $admin_client_pw = $pgi_setting["admin_client_pw"];
        } else {
            $admin_client_id = (string)$pgi_system->adminClientID;
            $admin_client_pw = (string)$pgi_system->adminClientPW;
        }
        $this->logger2->debug($session_data);
        if (!$session_expire_time || $session_expire_time < date("Y-m-d H:i:s") ) {
            $token = $this->loginPGI($admin_client_id, $admin_client_pw, $web_id, $web_pw);
            $data = array("pgi_session_token" => $token);
            $obj_PGiSession->update($data, $where);
        } else {
            $token = $session_data["pgi_session_token"];
        }
        if (!$token){
            throw new Exception ("pgi login failed");
        }
        return $token;
    }
    
    //Login PGi
    private function loginPGI($pgi_client_id, $pgi_client_pw, $pgi_web_id, $pgi_web_pw)
    {
        $this->pgi_client = new N2MY_PGi_Client($this->pgiConfig->get("PGI", "logon_url"),
                                       (string)$pgi_web_id ,(string)$pgi_web_pw);
        $params = array('admin_client_id' => $pgi_client_id,
                        'admin_client_pw' => $pgi_client_pw,
                        'web_id'    => $pgi_web_id,
                        'web_pw'    => $pgi_web_pw,
                        'pgi_security_url'     => $this->pgiConfig->get("PGI", "pgi_security_url"),
                        'pgi_schemas_security_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_security_url"),
                        'pgi_schemas_common_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_common_url"));
                        $this->logger2->debug($params);
        $headers['SOAPAction'] = '"'.$this->pgiConfig->get("PGI", 'login_soapaction').'"';
        $headers['Content-Type'] = "text/xml; charset=utf-8";
        $res    = $this->pgi_client->call('login', $params, $headers);
        $attr = $res['LogOnResponse'][0]['LogOnResult'][0]['a:Token'][0]["_data"];
        return $attr;
    }

    //}}}
    //{{{ updateMeeting
    private function updateMeeting($meeting_key, $data)
    {
        $meeting        = new DBI_Meeting($this->get_dsn());
        $where          = sprintf("meeting_ticket='%s'", mysql_real_escape_string($meeting_key));
        $res = $meeting->update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("update meeting failed PEAR said : ".$res->getUserInfo());
        }
    }
    //}}}
    // php 5.3以下でDateTImeのdiffが使えなかったので実装
    //{{{dateDiff
    function dateDiff($start, $end)
    {
        $dayTime = 60 * 60 * 24;

        $startTime = strtotime($start->format('Y-m-d H:i:s'));
        $endTime   = strtotime($end->format('Y-m-d H:i:s'));

        return floor(($endTime - $startTime) / $dayTime);
    }
    // }}}
    // {{{minuteDiff
    function minuteDiff($start, $end)
    {
        $dayTime = 60 * 60 * 24;

        $startTime = strtotime($start->format('Y-m-d H:i:s'));
        $endTime   = strtotime($end->format('Y-m-d H:i:s'));

        $diffSec = $endTime - $startTime;
        if ($diffSec <= $dayTime){
            return $diffSec / 60;
        }

        return $diffSec % $dayTime / 60;
    }
    //}}}

    //}}}
    // {{{ createPGIClient
    private function createPGIClient($pgi_system, $url)
    {
        return new N2MY_PGi_Client($url,(string)$pgi_system->webID ,(string)$pgi_system->webPW);
    }
}

$main = new AppPgiCron();
$main->execute();
