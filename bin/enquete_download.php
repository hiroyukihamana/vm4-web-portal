<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Questionnaire.class.php");
require_once("classes/mgm/dbi/user_agent.dbi.php");
require_once("classes/dbi/answersheet.dbi.php");
require_once("classes/dbi/answer.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/answer_branch.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once("classes/core/dbi/Meeting.dbi.php");

class Answersheet extends AppFrame {
    var $_dsn = null;
    var $account_dsn = null;

    function init() {
        // DBサーバ情報取得
        if (file_exists(sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ))) {
            $server_list = parse_ini_file( sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
        } else {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        }
    }

    function default_view() {
      $this->action_answer_log_csv();
    }


    function action_answer_log_csv() {

        //前日
        //$date = date("Y-m-d",strtotime("-1 day"));
        $date = "2012-03-21";

        //入室者取得
        $obj_UserAgent = new MgmUserAgentTable($this->account_dsn);
        $where_agent = "create_datetime >= '".$date." 00:00:00'".
                 " AND create_datetime <= '".$date." 23:59:59'";
        $user_count = $obj_UserAgent->numRows($where_agent);
        if (DB::isError($user_count)) {
            $this->logger2->error($user_count->getUserInfo());
            return false;
        }

        //アンケート回答者数
        $obj_Answersheet = new AnswersheetTable($this->_dsn);
        $where = "created_datetime >= '".$date." 00:00:00'".
                 " AND created_datetime <= '".$date." 23:59:59'";
        $answer_count = $obj_Answersheet->numRows($where);
        if (DB::isError($answer_count)) {
            $this->logger2->error($answer_count->getUserInfo());
            return false;
        }

        //項目
        $menu = array("満足度,相手の映像が表示されない,自分の映像が表示されない,映像の品質が悪い,自分の音声が取得できない,相手の音声が聞こえない,音が頻繁に途切れる,音声が遅延する,音声がエコーする,音声がハウリングする,機能（ボタン）が多く操作に迷う,デザインがわかりにくい（使いにくい）箇所がある。,デザインについてのコメント,突然切断される,デスクトップ共有が開始出来ない,デスクトップ共有が閲覧出来ない,全体的に動作が遅い,ホワイトボード操作に不具合があった,不具合についてのコメント,利用頻度,WEB会議のタイプ,会議参加した方,その他ご意見・ご要望,入室方法,名前,フリガナ,会社名,メールアドレス,電話番号,ユーザーID,部屋名,回答日時,入室日時,会議名,参加者名");

        //質問
        $obj_Questionnaire = new N2MY_Questionnaire($this->get_dsn());
        $question_list = $obj_Questionnaire->getQuestionnaire("meeting_end", $this->request->get("lang","ja"));

        //出力したい答え。
        $obj_Answer = new AnswerTable($this->_dsn);
        $sql = "select answersheet.answersheet_id,answer.question_id,answer_branch.question_branch_id,answer.comment from answer
                LEFT JOIN answersheet as answersheet ON answersheet.answersheet_id = answer.answersheet_id
                LEFT JOIN answer_branch as answer_branch ON answer_branch.answer_id = answer.answer_id
                where answersheet.created_datetime >= '".$date." 00:00:00'".
                "AND answersheet.created_datetime <= '".$date." 23:59:59'".
                "GROUP BY answer.answer_id";


        $answer_result = $obj_Answer->_conn->query( $sql );
        $results = array();

        while ( $answer_row = $answer_result->fetchRow( DB_FETCHMODE_ASSOC ) ){
            foreach ($question_list as  $_q) {
                //選択肢の文言取得
                if (is_array($_q["branch"])) {
                    foreach($_q["branch"] as $_qb) {
                        if ($answer_row["question_branch_id"] == $_qb["question_branch_id"]){
                            $answer_row["branch_lang"] = $_qb["text"];
                        }
                    }
                }
                if ($_q["question_id"] == 1) {
                    if($_q["question_id"] == $answer_row["question_id"]) {
                        $results[$answer_row["answersheet_id"]][$answer_row["question_branch_id"]] = $answer_row["branch_lang"];
                    }
                } elseif ($_q["question_id"] == 2 || $_q["question_id"] == 3) {
                    foreach ($_q["branch"] as $_q2_3) {
                        if ($answer_row["question_branch_id"] == $_q2_3["question_branch_id"]) {
                                $results[$answer_row["answersheet_id"]][$_q2_3["question_branch_id"]] = "T";
                        } elseif (!$results[$answer_row["answersheet_id"]][$_q2_3["question_branch_id"]]) {
                                $results[$answer_row["answersheet_id"]][$_q2_3["question_branch_id"]] = "F";
                        }
                    }

                } elseif ($_q["question_id"] == 4) {
                    foreach ($_q["branch"] as $_q4) {
                        if ($answer_row["question_branch_id"] == $_q4["question_branch_id"]) {
                                $results[$answer_row["answersheet_id"]][$_q4["question_branch_id"]] = "T";
                        } elseif (!$results[$answer_row["answersheet_id"]][$_q4["question_branch_id"]]) {
                                $results[$answer_row["answersheet_id"]][$_q4["question_branch_id"]] = "F";
                        }
                    }
                    if (isset($answer_row["comment"]) && $answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"]) {
                        $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = "";
                    }


                } elseif ($_q["question_id"] == 5) {
                    foreach ($_q["branch"] as $_q5) {
                        if ($answer_row["question_branch_id"] == $_q5["question_branch_id"]) {
                                $results[$answer_row["answersheet_id"]][$_q5["question_branch_id"]] = "T";
                        } elseif (!$results[$answer_row["answersheet_id"]][$_q5["question_branch_id"]]) {
                                $results[$answer_row["answersheet_id"]][$_q5["question_branch_id"]] = "F";
                        }
                    }
                    if (isset($answer_row["comment"]) && $answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"]) {
                        $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = "";
                    }

                } elseif ($_q["question_id"] == 6) {
                    if($answer_row["question_id"] == 6) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["branch_lang"];
                    }
                    if(!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif ($_q["question_id"] == 7) {
                    if($answer_row["question_id"] == 7) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["branch_lang"];
                    }

                    if(!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 8) {
                    if($answer_row["question_id"] ==8) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["branch_lang"];
                    }

                    if(!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 9) {
                    if (isset($answer_row["comment"]) && $answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"]) {
                        $results[$answer_row["answersheet_id"]][$_q["question_id"]."_comment"] = "";
                    }

                } elseif($_q["question_id"] == 10) {
                    if($answer_row["question_id"] == 10) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["branch_lang"];
                    }
                    if(!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 11) {
                    if ($answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 12) {
                    if ($answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 13) {
                    if ($answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 14) {
                    if ($answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }

                } elseif($_q["question_id"] == 15) {
                    if ($answer_row["question_id"] == $_q["question_id"]) {
                         $results[$answer_row["answersheet_id"]][$_q["question_id"]] = $answer_row["comment"];
                    } elseif (!$results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]]) {
                        $results[$answer_row["answersheet_id"]]["q_".$_q["question_id"]] = "";
                    }
                }
            }

        }

        //アンケート項目以外の必要項目
        $obj_AnswerSheet = new AnswerSheetTable($this->_dsn);
        $sql2 = "select answersheet.answersheet_id,user.user_id,meeting.meeting_room_name,answersheet.created_datetime,participant.uptime_start,meeting.meeting_name,participant.participant_name from answersheet
                LEFT JOIN participant as participant ON participant.participant_key = answersheet.participant_key
                LEFT JOIN meeting as meeting ON meeting.meeting_key = participant.meeting_key
                LEFT JOIN user as user ON user.user_key = meeting.user_key
                where answersheet.created_datetime >= '".$date." 00:00:00'".
                "AND answersheet.created_datetime <= '".$date." 23:59:59'";

        $other_result = $obj_AnswerSheet->_conn->query( $sql2 );
        while($other_row = $other_result->fetchRow( DB_FETCHMODE_ASSOC )) {
            foreach($results as $keys => $anses) {
                if ($other_row["answersheet_id"] == $keys) {
                    $results[$keys]["user_id"] = $other_row["user_id"];
                    $results[$keys]["room_name"] = $other_row["meeting_room_name"];
                    $results[$keys]["create"] = $other_row["created_datetime"];
                    $results[$keys]["uptime"] = $other_row["uptime_start"];
                    $results[$keys]["m_name"] = $other_row["meeting_name"];
                    $results[$keys]["p_name"] = $other_row["participant_name"];
                }
            }
        }


        //まとめ
        $csv_ddata = "";
        foreach ($results as $result) {
            $csv_data .= join(",", $result)."\n";
        }

        ob_start();
        $this->template->assign('user_count',$user_count);
        $this->template->assign('answer_count',$answer_count);
        $this->template->assign('start_date',$date);
        $this->template->assign('end_date',$date);
        $this->template->assign('menu',$menu);
        $this->template->assign('csv_data',$csv_data);
        $this->template->display('common/admin_tool/enquete_list/answersheet.t.csv');
        $contents = ob_get_contents();
        if ($this->_lang == "ja_JP") {
            $contents = mb_convert_encoding($contents, 'SJIS', 'UTF-8');
        }
        ob_clean();


        //書き込み
        $csv_file = N2MY_DOC_DIR."enqueteLog/".$date.".csv";
        $fp = fopen($csv_file, 'ab');
        fwrite($fp, $contents);
        fclose($fp);

        //保存
        $file = file_get_contents($csv_file);

    }


    //1週間後削除
    function action_answer_log_delete() {
        $dir = dir( N2MY_DOC_DIR."enqueteLog/" );
        while (false !== ($filename = $dir->read())) {
            if(is_file($filename)) {
                $timestamp= filectime($filename);
                if(mktime() > $timestamp + 60*60*24*7) {
                    unlink($filename);
                }
            }
        }
    }

}
$main = new Answersheet();
$main->execute();
