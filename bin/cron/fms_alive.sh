#!/bin/sh

_log() {
	msg=$1
	exectime=`date +"[%Y-%m-%d %H:%M:%S]"`
	echo "${exectime} ${msg}"
}

fms_alive() {
	for a in $(seq 1 5)
	do
		alive=`ruby \
		-r /home/project/meeting.nice2meet.us/lib/ruby/rtmp.rb -e \
		"h = RTMPHack.new; h.connection_uri = 'rtmp://kddi02.fcs.nice2meet.us/meeting'; h.connection_args = [0x900]; h.method_name = 'aliveCheck'; h.execute;"`
		if [ $alive = "alive" ]
		then
			return 1
		fi
		_log "retry"
	done
	return 0
	exit
}


_log "start"
fms_alive
if [ $? = 1 ]
then
	echo "ok"
else
	echo "ng"
fi

_log "end"
