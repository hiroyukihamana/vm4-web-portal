<?php
ini_set("memory_limit", "512M");
require_once ("set_env.php");
require_once ("classes/AppFrame.class.php");
require_once ("classes/N2MY_DBI.class.php");
require_once ("classes/dbi/user.dbi.php");


/**
 * セールスインバウンドを使えるように、リレーションテーブルのuser_key値を直す。
 */

class Patch_4995 extends AppFrame {

    var $obj_relation = null;
    var $obj_user = null;

    function init() {
    	$this->obj_relation = new N2MY_DB($this -> get_auth_dsn(), "relation");
    	$this->obj_user = new UserTable($this -> get_dsn());
    	
    }

    function default_view() {
        $where_relationList = 'relation_type = "inbound_id" AND status =1 AND create_datetime >= "2014-08-20 00:00:00"';
        $inboundRelationList = $this->obj_relation->getRowsAssoc($where_relationList);
        foreach ($inboundRelationList as $inboundRelation) {
        	if(is_numeric($inboundRelation["user_key"])) {
        		$where_userInfo = 'user_key="'.$inboundRelation["user_key"].'" AND account_plan="one"';
        		if($userInfo = $this->obj_user->getRow($where_userInfo)) {
        			$data_updateRelation = array("user_key" => $userInfo["user_id"]);
        			$where_updateRelation = "relation_key='".$inboundRelation["relation_key"]."'";
        			$this->logger2->info($data_updateRelation, $where_updateRelation);
        			$rs = $this->obj_relation->update($data_updateRelation, $where_updateRelation);
        			if (DB::isError($rs)) {
        			    $this->logger2->error(array("where" => $where_updateRelation,"data" => $data_updateRelation),"caution! sales inbound repair failed.");
        			}
        		}
        		
        	}
        }
       
    }

    private function _log($string) {
        $config = parse_ini_file(sprintf("%sconfig/config.ini", N2MY_APP_DIR), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $log_prefix = "4995_patch";
        $log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        if ($fp) {
            fwrite($fp, $string . "\n");
        }
        fclose($fp);
    }

}

$main = new Patch_4995();
$main -> execute();
?>