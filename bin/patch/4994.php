<?php
ini_set("memory_limit", "512M");
require_once ("set_env.php");
require_once ("classes/AppFrame.class.php");
require_once ("classes/dbi/member.dbi.php");
require_once ("classes/dbi/room.dbi.php");
require_once ("classes/dbi/room_plan.dbi.php");
require_once ("classes/dbi/member_room_relation.dbi.php");
require_once ("classes/dbi/ordered_service_option.dbi.php");
require_once ("classes/dbi/service.dbi.php");

/**
 ・Oneユーザーのセールス部屋のプランを118(Oneセールスプラン)に変更
 ・オプションや会議室設定もプランに合わせて変更する
 */

class Patch_4994 extends AppFrame {

    var $obj_room = null;
    var $obj_member = null;
    var $obj_room_plan = null;
    var $obj_member_room_relation = null;
    var $obj_ordered_service_option = null;
    var $obj_service = null;

    function init() {
        $this->obj_room = new RoomTable($this->get_dsn());
        $this->obj_member = new MemberTable($this->get_dsn());
        $this->obj_room_plan = new RoomPlanTable($this->get_dsn());
        $this->obj_member_room_relation = new MemberRoomRelationTable($this->get_dsn());
        $this->obj_ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
        $this->obj_service = new ServiceTable($this->config->get('GLOBAL', 'auth_dsn'));
    }

    function default_view() {
        $where = 'member_status = 0 AND vcube_one_member_id IS NOT NULL';
        $members = $this->obj_member->getRowsAssoc($where);
        if ($members) {
            $result = array();
            $service_key = 118;
            $where = 'service_key = ' . $service_key;
            $service_info = $this->obj_service->getRow($where);
            foreach ($members as $member) {
                $where = 'member_key = ' . $member['member_key'];
                $room_key = $this->obj_member_room_relation->getOne($where, 'room_key');
                if ($room_key) {
                    $where = "room_plan_status = 1 AND service_key = 89 AND room_key = '" . mysql_real_escape_string($room_key) . "'";
                    if ($this->obj_room_plan->numRows($where) >= 1) {
                        $data = array(
                            'room_plan_status'     => 0, 
                            'room_plan_updatetime' => date("Y-m-d H:i:s")
                        );
                        $this->obj_room_plan->update($data, $where);
                        $room_plan_data = array(
                            'room_key'              => $room_key,
                            'room_plan_status'      => 1,
                            'service_key'           => $service_key,
                            'discount_rate'         => 0,
                            'contract_month_number' => 0,
                            'room_plan_starttime'   => date("Y-m-d 00:00:00"),
                            'room_plan_endtime'     => '0000-00-00 00:00:00',
                            'room_plan_registtime'  => date("Y-m-d H:i:s")
                        );
                        $this->obj_room_plan->add($room_plan_data);
                        $room_seat = array(
                            'max_seat'                     => $service_info['max_seat'],
                            'max_audience_seat'            => $service_info['max_audience_seat'],
                            'max_whiteboard_seat'          => $service_info['max_whiteboard_seat'],
                            'max_guest_seat'               => $service_info['max_guest_seat'],
                            'max_guest_seat_flg'           => $service_info['max_guest_seat_flg'],
                            'extend_seat_flg'              => $service_info['extend_seat_flg'],
                            'extend_max_seat'              => $service_info['extend_max_seat'],
                            'extend_max_audience_seat'     => $service_info['extend_max_audience_seat'],
                            'meeting_limit_time'           => $service_info['meeting_limit_time'],
                            'ignore_staff_reenter_alert'   => $service_info['ignore_staff_reenter_alert'],
                            'default_camera_size'          => $service_info['default_camera_size'],
                            'hd_flg'                       => $service_info['hd_flg'],
                            'disable_rec_flg'              => $service_info['disable_rec_flg'],
                            'active_speaker_mode_only_flg' => $service_info['active_speaker_mode_only_flg'],
                            'active_speaker_mode_use_flg'  => $service_info['active_speaker_mode_only_flg'],
                            'max_room_bandwidth'           => $service_info['max_room_bandwidth'],
                            'max_user_bandwidth'           => $service_info['max_user_bandwidth'],
                            'min_user_bandwidth'           => $service_info['min_user_bandwidth'],
                            'whiteboard_page'              => $service_info['whiteboard_page'],
                            'whiteboard_size'              => $service_info['whiteboard_size'],
                            'cabinet_size'                 => 0,
                            'room_updatetime'              => date("Y-m-d H:i:s")
                        );
                        $where = "room_key = '" . mysql_real_escape_string($room_key) . "'";
                        $this->obj_room->update($room_seat, $where);
                        $service_options = array(
                            'desktop_share' => 3,
                            'high_quality'  => 5,
                            'h264'          => 26
                        );
                        foreach ($service_options as $service_option_name => $service_option_key) {
                            $where = "ordered_service_option_status = 1 AND room_key = '" . mysql_real_escape_string($room_key) . "' AND service_option_key = " . $service_option_key;
                            if ($this->obj_ordered_service_option->numRows($where) == 0) {
                            	$data = array(
                                    'room_key'                          => $room_key,
                                    'service_option_key'                => $service_option_key,
                                    'ordered_service_option_status'     => 1,
                                    'ordered_service_option_starttime'  => date("Y-m-d H:i:s"),
                                    'ordered_service_option_registtime' => date("Y-m-d 00:00:00"),
                                    'ordered_service_option_deletetime' => '0000-00-00 00:00:00'
                                );
                                $this->obj_ordered_service_option->add($data);
                            }
                        }
                        $result[] = $member['member_id'];
                    }
                }
            }
            foreach ($result as $_result) {
                $this->_log('Member ID : ' . $_result . ' UPDATE SUCCEED');
            }
        }
    }

    private function _log($string) {
        $config = parse_ini_file(sprintf("%sconfig/config.ini", N2MY_APP_DIR), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $log_prefix = "4994_patch";
        $log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        if ($fp) {
            fwrite($fp, $string . "\n");
        }
        fclose($fp);
    }

}

$main = new Patch_4994();
$main -> execute();
?>