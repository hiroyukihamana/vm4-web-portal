<?php
ini_set("memory_limit", "512M");
require_once ("set_env.php");
require_once ("classes/AppFrame.class.php");
require_once ("classes/dbi/user.dbi.php");
require_once ("classes/dbi/member.dbi.php");
require_once ("classes/N2MY_Account.class.php");

class Patch_49123 extends AppFrame {

    var $obj_user = null;

    function init() {
        $this -> objUser = new UserTable($this -> get_dsn());
        $this -> objMember = new MemberTable($this -> get_dsn());
        $this -> objN2MYAccount = new N2MY_Account($this -> get_dsn());
    }

    function default_view() {
        $where = "user_delete_status = 2 AND user_deletetime > '2015-08-27 00:00:00' AND account_plan ='one'";
        $deletedUsers = $this -> objUser -> getRowsAssoc($where);
        if ($deletedUsers) {
            $memberList = null;
            foreach ($deletedUsers as $user) {
                $where = "member_status = 0 AND user_key = " . $user['user_key'];
                $memberList = $this -> objMember -> getRowsAssoc($where);
                if ($memberList) {
                    foreach ($memberList as $member) {
                        $ret = $this -> objN2MYAccount -> deleteMember($member['member_key']);
                        if ($ret) {
                            $this -> _log('UPDATE SUCCEEDED : ' . $member['member_key']);
                        } else {
                            $this -> _log('UPDATE FAILED : ' . $member['member_key']);
                        }
                    }
                }
            }
        }

    }

    private function _log($string) {
        $config = parse_ini_file(sprintf("%sconfig/config.ini", N2MY_APP_DIR), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $log_prefix = "4.9.12.3_patch";
        $log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        if ($fp) {
            fwrite($fp, $string . "\n");
        }
        fclose($fp);
    }

}

$main = new Patch_49123();
$main -> execute();
?>