<?php
ini_set("memory_limit","512M");
require_once("set_env.php");
require_once ("classes/AppFrame.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/pgi_setting.dbi.php");
require_once("classes/dbi/pgi_rate.dbi.php");

class Patch_4985 extends AppFrame {

    var $dsn = null;
    var $obj_room = null;
    var $obj_pgiSetting = null;
    var $obj_pgiRate = null;

    function __destruct() {
        //unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
    }

    function init() {
        $this->dsn = $this->get_dsn();
        $this->obj_room     = new RoomTable($this->dsn);
        $this->obj_pgiSetting     = new PGiSettingTable($this->dsn);
        $this->obj_pgiRate     = new PGiRateTable($this->dsn);
    }
    function default_view() {
        //追加必要なPGi System Key
        $systemKeys = array("VCUBE HYBRID PRODUCTION" => array("area4" => "30" , "navidial"=> "15"),
                            "VCUBE HYBRID AGENCY PRODUCTION" =>  array("area4" => "27" , "navidial"=> "12"),
                            "PGi HYBRID PRODUCTION" =>  array("area4" => "30" , "navidial"=> "15"));
        
        foreach ($systemKeys as $key => $value) {
            $where = "system_key = '".mysql_escape_string($key)."'";
            $pgi_setting_list = $this->obj_pgiSetting->getRowsAssoc($where);
            
            foreach ($pgi_setting_list as $pgi_setting) {
                $this->add_data($pgi_setting["pgi_setting_key"], $value["area4"], "gm_dialin_local_area4");
                $this->add_data($pgi_setting["pgi_setting_key"], $value["navidial"], "gm_dialin_navidial");
            }
        }

    }

    private function add_data ($pgi_setting_key, $value, $type) {
        $where = "pgi_setting_key = ".mysql_escape_string($pgi_setting_key).
                 " AND is_deleted = 0".
                 " AND rate_type =  '".$type."'";
        $pgi_rate = $this->obj_pgiRate->getRow($where);
        if ($pgi_setting_key && !$pgi_rate) {
            $data = array("pgi_setting_key" => mysql_escape_string($pgi_setting_key),
                           "rate" => $value,
                           "rate_type" => $type,
                           "is_deleted" => "0",
                           "registtime" => date("Y-m-d H:i:s")
                           );
            $this->obj_pgiRate->add($data);
        }
    }
    
    private function _log($string){
        $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $log_prefix = "4985_patch";
        $log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        if($fp){
            fwrite($fp, $string."\n");
        }
        fclose($fp);
    }

}
$main = new Patch_4985();
$main->execute();