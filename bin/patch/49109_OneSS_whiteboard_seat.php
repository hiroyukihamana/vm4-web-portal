<?php
ini_set( 'memory_limit', '512M' );
require_once 'set_env.php';

require_once 'classes/AppFrame.class.php';
require_once 'classes/dbi/service.dbi.php';

require_once 'classes/dbi/user.dbi.php';
require_once 'classes/dbi/user_plan.dbi.php';
require_once 'classes/dbi/room.dbi.php';
require_once 'classes/dbi/room_plan.dbi.php';
require_once 'classes/dbi/ordered_service_option.dbi.php';

/**
 * [MTGVFOUR-602] One＿セールス部屋のプランを変更（対象：既存） - JIRA
 *
 * @link https://vcube2192.atlassian.net/browse/MTGVFOUR-602
 *      
 */

class Patch_49109 extends AppFrame {
	
	// 該当room_list洗い出し詳細出力
	var $is_debug = false;
	
	// 対象外 room_key 定義
	var $skip_room_keys = array(
		
	);
	
	// 固定値定義
	// `use_one_plan` = 1
	var $old_service_keys = array ( 
			111, // グローバルプラン 電話連携あり
			112, // グローバルプラン 電話連携あり(代理店)
			113, // グローバルプラン 電話連携なし
			114, // ローカルルプラン 電話連携あり
			115, // ローカルルプラン 電話連携あり(代理店)
			116, // ローカルルプラン 電話連携なし
			// 118,//Oneセールスプラン
			119, // トライアルグローバルプラン　電話連携あり
			120, // トライアルグローバルプラン　電話連携なし
			121, // トライアルローカルルプラン　電話連携あり
			122, //トライアルローカルルプラン　電話連携なし
	);
	var $new_service_key = 118; // Oneセールスプラン
	
	// 部屋上限帯域
	var $old_max_room_bandwidth = 6144; 
	var $new_max_room_bandwidth = 2048; 
	// WBユーザ
	var $old_max_whiteboard_seat = 10; 
	var $new_max_whiteboard_seat = 0; 
	
	// 付いていて良いオプション
	var $old_option_keys = array (
			3, // desktop_share
			//5,//high_quality
			26, //h264
			30 //GlobalLink
	);
	var $add_option_key = 5;
	
	// dbi object
	var $objUser;
	var $objRoom;
	var $objRoomPlan;
	var $objOrderdServiceOption;
	
	var $log_file;

	function init() {
		$config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true );
		$log_dir = $config["LOGGER"]["log_dir"];
		$log_prefix = "49109_patch";
		$this->log_file = sprintf( "%s/%s_%s.log", $log_dir, $log_prefix, date( 'Ymd', time() ) );
		$this->info( $this->log_file, 'output log file' );
		
		$dsn = $this->get_dsn();
		$this->objUser = new UserTable( $dsn );
		$this->objRoom = new RoomTable( $dsn );
		$this->objRoomPlan = new RoomPlanTable( $dsn );
		$this->objOrderdServiceOption = new OrderedServiceOptionTable( $dsn );
	}
	
	// main
	function default_view() {
		$this->_log();
		$this->_log();
		$this->debug( 'php start' );
		$this->main();
		$this->info( 'php end' );
	}

	private function main() {
		
		$where = sprintf('max_whiteboard_seat = %d AND use_sales_option = 1 AND room_status = 1', $this->old_max_whiteboard_seat);
		$room_keys = $this->objRoom->getCol( $where, 'room_key' );
		
		foreach ( $room_keys as $i => $room_key ) {
			if ( in_array( $room_key, $this->skip_room_keys ) ) {
				$this->_log( 'skip', $room_key );
				$this->info( $room_key, 'skip_room_key' );
				unset( $room_keys[$i] );
				continue;
			}
		}
		
		if ( ! $room_keys ) {
			$this->info( '> target room is empty' );
			$this->_log( '> target room is empty' );
			return;
		}
		
		$room_list = array ();
		$user_list = array ();
		
		foreach ( $room_keys as $room_key ) {
			$this->debug( $room_key, 'room_key' );
			
			$where = sprintf( 'room_key = "%s"', addslashes( $room_key ) );
			$room_info = $room_list[$room_key] = $this->objRoom->getRow( $where );
			
			$user_key = $room_info['user_key'];
			if ( ! $user_list[$user_key] ) {
				$this->debug( $user_key, 'user_key' );
				$user_list[$user_key] = $this->objUser->getRow( sprintf( 'user_key = %d', $user_key ) );
			}
		}
		
		$this->_log( '[before data] ' );
		$this->_log( 'no', 'user_key', 'is_one', 'room_key', 'service_key', 'options', 'is_error' );
		$service_list = array ();
		$option_list = array ();
		$count = 0;
		$errors = array();
		foreach ( $room_list as $room_key => $room_info ) {
			$is_error = false;
			$user_key = $room_info['user_key'];
			$user_info = $user_list[$user_key];
			
			$service_key = $this->objRoomPlan->getOne( sprintf( 'room_key = "%s" AND room_plan_status = 1', addslashes( $room_key ) ), 'service_key' );
			if(! in_array($service_key, $this->old_service_keys)){
				$is_error = true;
			}
			++ $service_list[$service_key];
			
			$option_keys = $this->objOrderdServiceOption->getCol( sprintf( 'room_key = "%s" AND ordered_service_option_status = 1', addslashes( $room_key ) ), 'service_option_key' );
			foreach ( $option_keys as $option_key ) {
				if(! in_array($option_key, $this->old_option_keys)){
					$is_error = true;
				}
				++ $option_list[$option_key];
			}
			
			// 確認用
			$is_one = $user_info['account_plan'] === 'one';
			$max_whiteboard_seat = $room_info['max_whiteboard_seat'];
			$max_room_bandwidth = $room_info['max_room_bandwidth'];
			if ( ! $is_one
				|| $max_whiteboard_seat != $this->old_max_whiteboard_seat
				|| $max_room_bandwidth != $this->old_max_room_bandwidth
			) {
				$is_error = true;
			}
			
			if($is_error){
				$errors[] = $room_key;
			}
			$this->_log( ++ $count, $user_key, $is_one, $room_key, $service_key, join( ':', $option_keys ), $is_error );
		}
		
		$this->info( count( $room_list ), 'modify room_key count' );
		$this->info( count( $user_list ), 'user_key count' );
		$this->info( $service_list, 'remove service keys' );
		$this->info( $option_list, 'option keys' );
		
		$this->debug( array_keys( $user_list ), 'user_list count' );
		
		if ( $errors ) {
			$this->info( '> error' );
			$this->_log( '> error' );
			foreach ($errors as $room_key){
				$this->_log(sprintf('"%s"', $room_key), '');
				$this->info($room_key, 'error_room_key');
			}
			$this->_log( '> error' );
			$this->info( '> error' );
		}
		
		if ( $this->is_debug ) {
			$this->_log( '[exit debug=TRUE] ' );
			$this->info( 'please modify $this->is_debug = FALSE' );
			$this->info( 'exit' );
			exit();
		}
		
		echo 'please check data of output_file' . "\n";
		echo 'is execute patch ? ( YES / no ) : ' . "\n";
		echo 'input : ';
		ob_flush();
		$input = trim( fgets( STDIN ) );
		if ( $input !== 'YES' ) {
			$this->_log( '[exit input!=YES] ' );
			$this->info( 'please input YES then execute patch' );
			exit();
		}
		
		// 実処理開始
		$this->_log();
		$this->_log( '[execute patch start] ' );
		$this->info( 'execute patch start' );
		
		// $count, $room_key, $status_room, $status_room_plan, $ordered_service_option_key
		$this->_log( 'no', 'room_key', 'upRoom', 'upRoomPlan', 'ordered_service_option_key' );
		$count = 0;
		foreach ( $room_list as $room_key => $room_info ) {
			
			// service_key を 118 に変更
			{
				$data = array (
						'service_key' => $this->new_service_key,
						'room_plan_updatetime' => date( 'Y-m-d H:i:s' )
				);
				$where = sprintf( 'room_key = "%s" AND room_plan_status = 1', addslashes( $room_key ) );
				$status_room_plan = $this->objRoomPlan->update( $data, $where );
				if ( DB::isError( $status_room_plan ) ) {
					$this->logger2->error( $status_room_plan->getMessage() );
					$status_room_plan = '[!ERROR!]';
				} else {
					$status_room_plan = 1;
				}
			}
			
			// high_quality option 登録
			{
				$data = array (
						'room_key' => addslashes( $room_key ),
						'service_option_key' => $this->add_option_key,
						'ordered_service_option_status' => 1,
						'ordered_service_option_starttime' => date( 'Y-m-d H:i:s' ),
						'ordered_service_option_registtime' => date( 'Y-m-d 00:00:00' ),
						'ordered_service_option_deletetime' => '0000-00-00 00:00:00'
				);
				$ordered_service_option_key = $this->objOrderdServiceOption->add( $data );
				if ( DB::isError( $ordered_service_option_key ) ) {
					$this->logger2->error( $ordered_service_option_key->getMessage() );
					$ordered_service_option_key = '[!ERROR!]';
				}
			}
			
			// WBseat・帯域 変更
			{
				$data = array (
						'max_whiteboard_seat' => $this->new_max_whiteboard_seat,
						'max_room_bandwidth' => $this->new_max_room_bandwidth,
						'room_updatetime' => date( 'Y-m-d H:i:s' )
				);
				$where = sprintf( 'room_key = "%s" AND room_status = 1', addslashes( $room_key ) );
				$status_room = $this->objRoom->update( $data, $where );
				if ( DB::isError( $status_room ) ) {
					$this->logger2->error( $status_room->getMessage() );
					$status_room = '[!ERROR!]';
				} else {
					$status_room = 1;
				}
			}
			
			// ログ出力
			{
				$this->_log( ++ $count, $room_key, $status_room, $status_room_plan, $ordered_service_option_key );
			}
		}
	
	}

	private function _log($msg = '') {
		switch (true){
			case $msg === '':
				break;
			case func_num_args() == 1:
				$msg .= " " . date( 'Y-m-d H:i:s', time() );
				break;
			case $args = func_get_args() :
				$msg = join( ",", $args );
				break;
		}
		
		$fp = fopen( $this->log_file, "a" );
		if ( $fp ) {
			fwrite( $fp, $msg . "\n" );
		}
		fclose( $fp );
	}

	private function info($msg, $tag = null) {
		if ( ! is_string( $msg ) ) {
			$msg = var_export( $msg, true );
		}
		if ( $tag ) {
			$msg = sprintf( '%s : %s', $tag, $msg );
		}
		
		$msg .= "\n";
		echo $msg;
		ob_flush();
	}

	private function debug($msg, $tag = null) {
		if ( ! $this->is_debug ) {
			return;
		}
		if ( ! is_string( $msg ) ) {
			$msg = var_export( $msg, true );
		}
		if ( $tag ) {
			$msg = sprintf( '%s : %s', $tag, $msg );
		}
		echo "[ debug ] " . $msg . "\n";
		ob_flush();
	}

}

$main = new Patch_49109();
$main->execute();
?>