<?php
ini_set("memory_limit","512M");
require_once("set_env.php");
require_once ("classes/AppFrame.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/dbi/clip.dbi.php");
require_once("classes/core/dbi/Storage.dbi.php");
require_once("classes/dbi/meeting_clip.dbi.php");

class Patch_4900 extends AppFrame {

    var $dsn = null;
    var $obj_user = null;
    var $obj_room = null;
    var $obj_clip = null;
    var $obj_N2MY_Account = null;
    var $objStorage = null;
    var $clipTable = null;
    var $obj_meeting_clip = null;

    function __destruct() {
        //unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
    }

    function init() {
        $this->dsn = $this->get_dsn();
        $this->obj_user = new UserTable($this->dsn);
        $this->obj_room     = new roomTable($this->dsn);
        $this->obj_clip     = new clipTable($this->dsn);
        $this->obj_N2MY_Account = new N2MY_Account($this->dsn);
        $this->objStorage      = new DBI_StorageFile($this->dsn);
        $this->clipTable = new ClipTable($this->dsn);
        $this->obj_meeting_clip = new MeetingClipTable($this->dsn);
    }
    function default_view() {
/*
        // 普通ユーザー用処理
        $where = "user_status = 1 AND (account_model = '' OR account_model IS NULL) ";
        // ユーザーリスト取得
        $user_list = $this->obj_user->getRowsAssoc($where);
        $this->_log("[room user](user_id(user_key)=room_count*(500+500)+option_count*1000+clip_size)\n");
        $user_list_count = count($user_list);
        // オプション数格納用
        $option = array();
        for($i = 0;  $i < $user_list_count; $i++){
            $option[$i] = 0;
            // ストレージ初期値
            $storage = $user_list[$i]["clip_share_storage_size"];
            $where = "room_status = 1 AND user_key = " . $user_list[$i]["user_key"];
            // 会議室一覧取得
            $user_list[$i]["room_list"] = $this->obj_room->getRowsAssoc($where);
            $room_count = count($user_list[$i]["room_list"]);
            $storage += ($room_count * (STORAGE_ROOM_SIZE + STORAGE_USER_SIZE));
            // オプション取得
            for($z = 0; $z < $room_count; $z++){
                $user_list[$i]["room_list"][$z]["options"] = $this->obj_N2MY_Account->getRoomOptionList($user_list[$i]["room_list"][$z]["room_key"]);
                $storage += ($user_list[$i]["room_list"][$z]["options"]["hdd_extention"] * STORAGE_OPTION_SIZE);
                // ストレージ容量を増やすオプション数を保存
                if($user_list[$i]["room_list"][$z]["options"]["hdd_extention"] >= 1){
                    $option[$i] += $user_list[$i]["room_list"][$z]["options"]["hdd_extention"];
                }
            }
            $update_data = array(
                   "max_storage_size" => $storage
                    );
            $update_where = "user_key = " . $user_list[$i]["user_key"];
            $this->obj_user->update($update_data,$update_where);
            $this->_log($user_list[$i]["user_id"]."(".$user_list[$i]["user_key"].")=".$room_count."*(".STORAGE_ROOM_SIZE."+".STORAGE_USER_SIZE.")+".$option[$i]."*".STORAGE_OPTION_SIZE ."+".$user_list[$i]["clip_share_storage_size"]. "=".$storage."M");
            echo($user_list[$i]["user_key"]."=".$room_count."*(".STORAGE_ROOM_SIZE."+".STORAGE_USER_SIZE.")+".$option[$i]."*".STORAGE_OPTION_SIZE ."+".$user_list[$i]["clip_share_storage_size"]. "=".$storage."M\n");
        }

        // フリーユーザー用処理
        $update_where = " account_model = 'free' ";
        $update_data = array(
                "max_storage_size" => 0
        );

        $this->obj_user->update($update_data,$update_where);
        //トライアルアカウント
        $update_where = " user_status = 2 ";
        $update_data = array(
                "max_storage_size" => 0
        );

        $this->obj_user->update($update_data,$update_where);

        // センターとメンバー課金制アカウント
        $where = "user_status = 1 AND (account_model = 'member' or account_model = 'centre')";
        // ユーザーリスト取得
        $member_user_list = $this->obj_user->getRowsAssoc($where);
        $user_list_count = count($member_user_list);
        // オプション数格納用
        $option = array();
        $this->_log("\n[member & centre user](user_id(user_key)=option_count*500+room_count*500+clip_size)\n");
        echo("member & centre user \n");
        for($i = 0;  $i < $user_list_count; $i++){
            $option[$i] = 0;
            // ストレージ初期値
            $storage = $member_user_list[$i]["clip_share_storage_size"];
            $where = "room_status = 1 AND user_key = " . $member_user_list[$i]["user_key"];
            // 会議室一覧取得
            $member_user_list[$i]["room_list"] = $this->obj_room->getRowsAssoc($where);
            $room_count = count($member_user_list[$i]["room_list"]);
            // オプション取得
            for($z = 0; $z < $room_count; $z++){
                $member_user_list[$i]["room_list"][$z]["options"] = $this->obj_N2MY_Account->getRoomOptionList($member_user_list[$i]["room_list"][$z]["room_key"]);
                if($member_user_list[$i]["room_list"][$z]["options"]["hdd_extention"]){
                    $storage += ($member_user_list[$i]["room_list"][$z]["options"]["hdd_extention"] * STORAGE_ROOM_SIZE) + STORAGE_USER_SIZE;
                }
                // ストレージ容量を増やすオプション数を保存
                if($member_user_list[$i]["room_list"][$z]["options"]["hdd_extention"] >= 1){
                    $option[$i] += $member_user_list[$i]["room_list"][$z]["options"]["hdd_extention"];
                }
            }
            $update_data = array(
                    "max_storage_size" => $storage
            );
            $update_where = "user_key = " . $member_user_list[$i]["user_key"];
            $this->obj_user->update($update_data,$update_where);
            if($option[$i]){
                echo($member_user_list[$i]["user_key"]."=".$option[$i]."*".STORAGE_ROOM_SIZE ."+".$room_count."*".STORAGE_ROOM_SIZE."+".$member_user_list[$i]["clip_share_storage_size"]. "=".$storage."M\n");
                $this->_log($member_user_list[$i]["user_id"]."(".$member_user_list[$i]["user_key"].")=".$option[$i]."*".STORAGE_ROOM_SIZE ."+".$room_count."*".STORAGE_ROOM_SIZE."+".$member_user_list[$i]["clip_share_storage_size"]. "=".$storage."M");
            }else{
                echo($member_user_list[$i]["user_key"]."=".$option[$i]."*".STORAGE_ROOM_SIZE ."+".$room_count."*0+".$member_user_list[$i]["clip_share_storage_size"]. "=".$storage."M\n");
                $this->_log($member_user_list[$i]["user_id"]."(".$member_user_list[$i]["user_key"].")=".$option[$i]."*".STORAGE_ROOM_SIZE ."+".$room_count."*0+".$member_user_list[$i]["clip_share_storage_size"]. "=".$storage."M");
            }
        }
*/
        // 動画データをストレージへコピー
        $clip_list = $this->obj_clip->getRowsAssoc("clip_status = 2 AND is_deleted = 0");
        foreach($clip_list as $clip){
            $add_data = array(
                    "document_id"            => null,
                    "clip_key"               => $clip["clip_key"],
                    "personal_white_id"      => null,
                    "user_key"               => $clip["user_key"],
                    "member_key"             => 0,
                    "owner"                  => 0,
                    "file_path"              => "none",
                    "file_name"              => $clip["title"],
                    "description"            => $clip["description"],
                    "user_file_name"         => $clip["title"],
                    "file_size"              => $clip["flv_filesize"],
                    "format"                 => "clip",
                    "category"               => "video",
                    "extension"              => "flv",
                    "version"                => "as2",
                    "status"                 => 2,
                    "storage_folder_key"     => 0,
                    "last_update_member_key" => 0,
                    "create_datetime"        => date("Y-m-d H:i:s"),
                    "update_datetime"        => date("Y-m-d H:i:s")
            );
            $this->objStorage->add($add_data);
        }

        // ストレージとクリップミーティングと紐付
        $meeting_clip_list = $this->obj_meeting_clip->getRowsAssoc("is_deleted = 0");
       // var_dump($clip_list);
        foreach($meeting_clip_list as $meeting_clip){
            $update_data = array(
                    'storage_file_key' => $this->objStorage->getOne("clip_key = '" .$meeting_clip["clip_key"]."'" , "storage_file_key"),
            );
            $update_where = "meeting_clip_key = " . $meeting_clip["meeting_clip_key"];
            $this->obj_meeting_clip->update($update_data , $update_where);
        }

    }

    private function _log($string){
        $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $log_prefix = "4900_patch";
        $log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        if($fp){
            fwrite($fp, $string."\n");
        }
        fclose($fp);
    }

}
$main = new Patch_4900();
$main->execute();