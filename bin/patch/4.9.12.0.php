<?php
ini_set("memory_limit", "512M");
require_once ("set_env.php");
require_once ("classes/AppFrame.class.php");
require_once ("classes/dbi/user.dbi.php");

class Patch_49120 extends AppFrame {

    var $obj_user = null;

    function init() {
        $this -> obj_user = new UserTable($this->get_dsn());
    }

    function default_view() {
        $_where = 'account_plan = "one" AND user_delete_status != 1';
        $users = $this -> obj_user -> getCol($_where, 'user_key');
        foreach ($users as $user_key) {
            $where = "user_key = " . $user_key;
            $data = array(
                'member_id_format_flg' => "1",
            );
            $ret = $this -> obj_user -> update($data, $where);
            if (DB::isError($ret)) {
                $this -> _log('UPDATE FAILED : ' . $user_key);
            } else {
                $this -> _log('UPDATE SUCCEEDED : ' . $user_key);
            }
        }
    }

    private function _log($string) {
        $config = parse_ini_file(sprintf("%sconfig/config.ini", N2MY_APP_DIR), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $log_prefix = "4.9.12.0_patch";
        $log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        if ($fp) {
            fwrite($fp, $string . "\n");
        }
        fclose($fp);
    }

}

$main = new Patch_49120();
$main -> execute();
?>