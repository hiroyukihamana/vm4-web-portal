<?php
/**
 * Created by PhpStorm.
 * User: Chen
 * Date: 2015/10/15
 * Time: 15:23
 */

ini_set("memory_limit", "512M");
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/dbi/meeting.dbi.php");

class Patch_49125 extends AppFrame
{

    protected $meetingTable = null;

    function init()
    {
        $this->meetingTable = new MeetingTable($this->get_dsn());
    }

    function default_view()
    {
        $meetingKeys = $this->getONETimeMeeting();
        if ($meetingKeys) {
            $updateData = array(
                'is_one_time_meeting' => 1,
                'update_datetime' => date('Y-m-d H:i:s'),
            );
            $condition = null;
            foreach ($meetingKeys as $index => $meetingKey) {
                $condition = 'meeting_key = ' . $meetingKey['meeting_key'];
                $result = $this->meetingTable->update($updateData, $condition);
                if (DB::isError($result)) {
                    $this->_log('meeting table update failed, meeting key is : ' . $meetingKey['meeting_key']);
                } else {
                    $this->_log('meeting table update succeeded, meeting key is : ' . $meetingKey['meeting_key']);
                }
            }
        }
    }

    private function getONETimeMeeting()
    {
        $rows = array();
        $sqlQuery = 'SELECT meeting_key FROM meeting m ' .
            'WHERE m.create_datetime >= "2015-09-01 00:00:00" ' .
            'AND m.room_key NOT IN (SELECT room_key FROM room r WHERE r.is_one_time_meeting = 0)';
        $result = $this->meetingTable->_conn->query($sqlQuery);
        if (DB::isError($result)) {
            $this->logger->info(__FUNCTION__ . " # DBError", __FILE__, __LINE__, $sqlQuery);
        } else {
            while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
                $rows[] = $row;
            }
        }
        return $rows;
    }

    private function _log($string)
    {
        $config = parse_ini_file(sprintf("%sconfig/config.ini", N2MY_APP_DIR), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $log_prefix = "4.9.12.5_patch";
        $log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        if ($fp) {
            fwrite($fp, $string . "\n");
        }
        fclose($fp);
    }

}

$main = new Patch_49125();
$main->execute();
?>