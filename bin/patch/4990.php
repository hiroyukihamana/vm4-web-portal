<?php
ini_set("memory_limit", "512M");
require_once ("set_env.php");
require_once ("classes/AppFrame.class.php");
require_once ("classes/dbi/room.dbi.php");
require_once ("classes/dbi/user.dbi.php");
require_once ("classes/dbi/member.dbi.php");
require_once ("classes/dbi/member_room_relation.dbi.php");

/**
 * インバウンドスタッフ選択箇所と、インバウンドスタッフ追加の一覧取得箇所で、
 * メンバーの削除ステータスを見ていないので、
 * 有効になっているスタッフのみ取得するようになる
 */

class Patch_4990 extends AppFrame {

    var $obj_user = null;
    var $obj_room = null;
    var $obj_member = null;
    var $obj_member_room_relation = null;

    function init() {
        $this -> obj_user = new UserTable($this -> get_dsn());
        $this -> obj_room = new RoomTable($this -> get_dsn());
        $this -> obj_member = new MemberTable($this -> get_dsn());
        $this -> obj_member_room_relation = new MemberRoomRelationTable($this -> get_dsn());
    }

    function default_view() {
        $this -> delete_redundant_rooms();
    }

    function delete_redundant_rooms() {
        $where = 'user_status = 1 AND use_sales = 1';
        $users = $this -> obj_user -> getRowsAssoc($where);
        if ($users) {
            foreach ($users as $user) {
                $where = 'member_status = -1 AND use_sales = 1 AND user_key = ' . $user['user_key'];
                $members = $this -> obj_member -> getRowsAssoc($where);
                if ($members) {
                    foreach ($members as $member) {
                        if ($member) {
                            $where = 'member_key = ' . $member['member_key'];
                            $member_room_relation = $this -> obj_member_room_relation -> getRow($where);
                            if ($member_room_relation) {
                                $where = 'room_key = "' . mysql_real_escape_string($member_room_relation['room_key']) . '"';
                                $room = $this -> obj_room -> getRow($where);
                                if ($room && $room['room_status']) {
                                    $data = array('room_status' => 0);
                                    $ret = $this -> obj_room -> update($data, $where);
                                    $error_msg = "Room " . $room['room_key'];
                                    if (DB::isError($ret)) {
                                        $error_msg .= " delete failed\n";
                                    } else {
                                        $error_msg .= " delete successfully\n";
                                    }
                                    $this -> _log($error_msg);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function _log($string) {
        $config = parse_ini_file(sprintf("%sconfig/config.ini", N2MY_APP_DIR), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $log_prefix = "4990_patch";
        $log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        if ($fp) {
            fwrite($fp, $string . "\n");
        }
        fclose($fp);
    }

}

$main = new Patch_4990();
$main -> execute();
?>