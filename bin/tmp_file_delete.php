#!/usr/local/bin/php
<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/dbi/tmp_file.dbi.php");


class AppTmpFileDelete extends AppFrame {

    var $dsn = null;
    var $obj_tmp_file = null;


    function __destruct() {
    }

    function init() {
        $this->dsn = $this->get_dsn();
        $this->obj_tmp_file = new TmpFiletTable(  $this->dsn );
    }

    /*
     * 本日より前のデータは全て削除する(物理的データも)
     * 毎日05:00:00に実行させる
     */
    function default_view() {
        $now =  date('Y-m-d 00:00:00');
        $where = "status = 1 AND create_datetime < '" . $now . "'";
        $delete_list = $this->obj_tmp_file->getRowsAssoc($where);
        foreach($delete_list as $tmp_file_info){
            $file_path = N2MY_APP_DIR . $tmp_file_info["path"].$tmp_file_info["file_name"];
            if(file_exists($file_path)){
                unlink($file_path);
            }
        }
        $data = array(
                "status" => 0
                );
        $this->obj_tmp_file->update($data , $where);
        //var_dump($delete_list);
    }


}
$main = new AppTmpFileDelete();
$main->execute();
