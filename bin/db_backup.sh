#!/bin/sh

db_host=""
db_port="3306"
db_user="n2my"
db_pass=""
db_name_mgm=""
db_name_data=""

mysql_bin=/usr/local/mysql/bin/mysqldump

ymd=`date '+%Y%m%d'`

mgm_backup_dir=${HOME}/meeting_backup/db/${ymd}/${db_name_mgm}
data_backup_dir=${HOME}/meeting_backup/db/${ymd}/${db_name_data}

mkdir -p ${mgm_backup_dir}
mkdir -p ${data_backup_dir}

echo "DB BACKUP"
echo ""
echo [${db_name_mgm}]
$mysql_bin -h ${db_host} --max_allowed_packet=80M -P ${db_port} -u ${db_user} --password=${db_pass} ${db_name_mgm} | gzip > ${mgm_backup_dir}/sql.zip
echo ${mgm_backup_dir}/sql.zip
echo "== successful =="

echo ""
echo [${db_name_data}]
$mysql_bin -h ${db_host} --max_allowed_packet=1G -P ${db_port} -u ${db_user} --password=${db_pass} ${db_name_data} | gzip > ${data_backup_dir}/sql.zip
echo ${data_backup_dir}/sql.zip
echo "== successful =="

