<?php
require_once 'set_env.php';
require_once 'lib/EZLib/EZCore/EZConfig.class.php';
require_once 'classes/AppFrame.class.php';
require_once 'classes/vcubeid/portalCore.class.php';
require_once 'classes/dbi/meeting.dbi.php';
require_once 'classes/dbi/reservation.dbi.php';
require_once 'classes/dbi/reservation_push.dbi.php';

class CronReservationPush extends AppFrame {
	
	private $dbi_user;
	private $dbi_meeting;
	private $dbi_reservation;
	private $dbi_reservation_push;
	
	// 対象制御パラメータ
	private $opt;
	private $is_echo;
	
	public function init() {
		$this->dbi_user = new UserTable( parent::get_dsn() );
		$this->dbi_meeting = new MeetingTable( parent::get_dsn() );
		$this->dbi_reservation = new ReservationTable( parent::get_dsn() );
		$this->dbi_reservation_push = new ReservationPush( parent::get_dsn() );
	}
	
	public function setOpt( $opt = array( 'ALL' => true) ) {
		
		$this->is_echo = !array_key_exists('no-echo', $opt);
		
		// fix opt
		switch ( true ) {
			case array_key_exists( 'ALL', $opt ) : // 検索フィルタ無し
				$this->opt = array( 'ALL' => true );
				break;
			case array_key_exists( 'push_key', $opt) : // レコード指定
				$this->opt = array( 'push_key' => $opt['push_key'] );
				break;
			case array_key_exists('reservation_key', $opt) : // レコード指定
				$this->opt = array( 'reservation_key' => $opt['reservation_key'] );
				break;
			case array_key_exists( 'offset', $opt ) : // use config.ini
				$offset = EZConfig::getInstance()->get( 'VCUBE_PORTAL', 'push_cron_offset' );
				$params = explode( '-', preg_replace( '/^-/', '', $offset ) );
				$opt = array();
				foreach( $params as $param ) {
					list( $key, $val ) = explode( ' ', $param );
					$opt[$key] = (int)$val;
				}
				// break; config.ini の内容も validate する
			default : // validate
				foreach( $opt as $key => &$val ) {
					if ( !is_numeric( $key ) ) {
						$opt = false;
						break;
					}
					// 数値以外は0指定に落とす
					if ( !is_numeric( $val ) ) $val = 0;
				}
				
				if ( $opt ) $this->opt = $opt;
				else{
					// 検索フィルタが false のため ALL指定 と同じ挙動となる
					$this->logger2->warn($this->opt, 'argument is warn');
				}
		}
	}
	
	// main
	public function default_view() {
		
		// 起動可否チェック
		$is_push_reservation = EZConfig::getInstance()->get( 'VCUBE_PORTAL', 'is_push_reservation' );
		if ( !$is_push_reservation ){
			$this->_echo('is_push_reservation of config.ini is not true.');
			return false;
		}
		
		// 多重起動チェック
		if ( !$this->checkPid( N2MY_APP_DIR . '/bin/batch/' . __CLASS__ . '.pid' ) ){
			$this->_echo('Error: duplicate execute.');
			return false;
		}
		
		$push_list = $this->dbi_reservation_push->getList( $this->opt );
		
		// 処理対象無し
		if ( count( $push_list ) < 1 ){
			$this->_echo('target is empty.');
			return true;
		}
		
		// 送信データ確認
		if ( $this->is_echo ) {
			$columns = array('reservation_push_key', 'reservation_key', 'error_count', 'is_pend');
			echo implode(',', $columns) . PHP_EOL;
			foreach ($push_list as $row){
				$data = array();
				foreach( $columns as $name ){
					$data[] = $row[$name];
				}
				echo implode( ',', $data ) . PHP_EOL;
			}
			echo 'is send ' . count($push_list) . ' rows?' . PHP_EOL;
			echo 'input [Y/n] (default n) : ';
			ob_flush();
			$input = trim( fgets( STDIN ) );
			if ( $input !== 'Y' ) {
				echo 'input is not Y. exit.' . PHP_EOL;
				return true;
			}
		}
		
		$is_error = false;
		foreach( $push_list as $push_info ) {
			$objPortalCore = new PortalCore( $push_info['vcube_id'], $push_info['auth_token'], $push_info['contract_id'] );
			
			$status = false;
			switch ( $push_info['function'] ) {
				case ReservationPush::MODE_ADD :
					$status = $objPortalCore->addReservation( $this->getPortalPushData( $push_info['reservation_key'] ) );
					break;
				case ReservationPush::MODE_UPDATE :
					$status = $objPortalCore->updateReservation( $this->getPortalPushData( $push_info['reservation_key'] ) );
					break;
				case ReservationPush::MODE_DELETE :
					$meeting_ticket = $this->dbi_reservation->getOne( sprintf( 'reservation_key=%d', $push_info['reservation_key'] ), 'meeting_key' );
					$meeting_id = $this->dbi_meeting->getOne( sprintf( 'meeting_ticket="%s"', $meeting_ticket ), 'meeting_session_id' );
					$status = $objPortalCore->deleteReservation( $meeting_id );
					break;
			}
			
			if ( $status ) {
				$this->dbi_reservation_push->remove( $push_info['reservation_push_key'] );
				continue;
			}
			
			// API not success
			$is_error = true;
			break;
		}
		
		// 正常終了
		if ( !$is_error ){
			$this->_echo('success: push ' . count($push_list) . ' rows.');
			return true;
		}
		
		$max_push_error_count = EZConfig::getInstance()->get( 'VCUBE_PORTAL', 'max_push_error_count', 3 );
		$pend_list = $this->dbi_reservation_push->checkErrorCount( $max_push_error_count );
		if ( !$pend_list ) {
			$this->_echo('Error: response_code is not 200. please check portal_push.log');
			return false;
		}
		
		// アラート
		$subject = 'alert reservation_push pending';
		$body = array();
		$body[] = sprintf('予約情報ポータルプッシュが %d回 失敗し、送信を断念しました。', $max_push_error_count);
		$body[] = 'db_table::reservation_push 及び portal_push.log を確認し、再送信して下さい。';
		$body[] = '';		
		$body[] = print_r( array(
				'HOSTNAME' => php_uname('n'),
				'N2MY_APP_DIR' => N2MY_APP_DIR,
		), true );
		
		$list = array();
		foreach( $pend_list as $row ) {
			// 認証情報なのでメールには載せない
			unset( $row['vcube_id'] );
			unset( $row['auth_token'] );
			unset( $row['contract_id'] );
			$function = &$row['function'];
			switch ( $function ) {
				case ReservationPush::MODE_ADD :
					$function = 'Add';
					break;
				case ReservationPush::MODE_UPDATE :
					$function = 'Update';
					break;
				case ReservationPush::MODE_DELETE :
					$function = 'Delete';
					break;
			}
			$list[] = $row;
		}
		$body[] = print_r( array(
				'pend_list' => $list 
		), true );
		
		$body = implode(PHP_EOL, $body) . PHP_EOL;
		if ( defined( 'N2MY_ERROR_FROM' ) && defined( 'N2MY_ERROR_TO' ) ) {
			$this->sendReport( N2MY_ERROR_FROM, N2MY_ERROR_TO, $subject, $body );
		}
		$this->logger2->warn($body, $subject);
		$this->_echo('Error: error_count over. please check portal_push.log and n2my.err.log');
		return false;
	}
	
	private function _echo( $msg ) {
		if ( $this->is_echo ) echo $msg . PHP_EOL;
		$this->logger2->info($msg);
	}
	
	/**
	 * 多重起動防止用 pid 管理
	 *
	 * @param unknown $pid_path        	
	 * @return boolean
	 */
	private function checkPid( $pid_path ) {
		if ( !is_dir( dirname( $pid_path ) ) ) mkdir( dirname( $pid_path ), 0777, true );
		
		$_retval = $pid = $sid = false;
		// check keeping pid
		if ( file_exists( $pid_path ) ) {
			list( $pid, $sid ) = explode( ',', trim( file_get_contents( $pid_path ) ) );
			exec( sprintf( 'ps %d', $pid ), $null, $_retval );
		}
		
		// 既存プロセスなし
		if ( $_retval || !file_exists( $pid_path ) ) {
			// 旧プロセスのセッションファイル破棄
			if ( $sid ) unlink( N2MY_APP_DIR . '/tmp/sess_' . $sid );
			// 自身のプロセスを保持
			$command = sprintf( 'echo %d,%s > %s', getmypid(), session_id(), $pid_path );
			exec( $command, $null, $_retval );
			if ( $_retval ) {
				$this->logger2->error( $command, 'touch pid failed' );
				exit();
			}
			return true;
		}
		
		// 多重起動検知
		// keep this session_id?
		
		$time = time() - filemtime( $pid_path );
		$alert = EZConfig::getInstance()->get('VCUBE_PORTAL', 'alert_cron_process_sec');
		if ( 0 < $alert && $alert < $time ) {
			
			// 通知間隔制御
			$is_send = dirname( $pid_path ) . __CLASS__ . '.mail';
			if ( file_exists( $is_send ) ) { // 通知済み
				// 再通知判定
				if ( $alert < time() - filemtime( $is_send ) ) exec( sprintf('touch %s', $is_send) );
				else $is_send = false;
			} else {
				exec( sprintf( 'touch %s', $is_send ) );
			}
			
			if ( $is_send && defined( 'N2MY_ERROR_FROM' ) && defined( 'N2MY_ERROR_TO' ) ){
				$subject = 'alert cron reservation_push process hung up';
				$body = array();
				$body[] = sprintf( '%s の処理に %d 秒以上かかっています。', __FILE__, $time );
				$body[] = sprintf( 'pid: %d のプロセス 及び db_table:reservation_push を確認してください。', $pid );
				$body[] = print_r( array(
						'HOSTNAME' => php_uname('n'),
						'N2MY_APP_DIR' => N2MY_APP_DIR,
				), true );
				$body[] = '対応が完了したら以下のコマンドを実行してください。';
				$body[] =  sprintf('rm %s', realpath($is_send) );
				$body = implode(PHP_EOL, $body) . PHP_EOL;
				$this->sendReport( N2MY_ERROR_FROM, N2MY_ERROR_TO, $subject, $body );
			}
			
		}
		
		$this->logger2->warn( array(
				'既存プロセスの処理時間' => $time,
				'既存プロセスのPID' => $pid
		), '多重起動検知' );
		
		return false;
	}
	
	/**
	 * 求められている予約情報に整形
	 *
	 * @param int $reservation_key        	
	 * @return array
	 */
	private function getPortalPushData( $reservation_key ) {
		$reservation_info = $this->dbi_reservation->getRow( sprintf( 'reservation_key=%d', $reservation_key ) );
		
		$now = strtotime( 'now' );
		$start = strtotime( $reservation_info['reservation_starttime'] );
		$end = strtotime( $reservation_info['reservation_endtime'] );
		$status = false;
		switch ( true ) {
			case $start < $now && $now < $end :
				$status = 'now'; // 開催中
				break;
			case $end < $now :
				$status = 'end'; // 終了
				break;
			default :
				$status = 'wait'; // 開催待
				break;
		}
		
		$meeting_info = $this->dbi_meeting->getRow(sprintf( 'meeting_ticket="%s"', $reservation_info['meeting_key'] ));
		$meeting_id = $meeting_info['meeting_session_id'];
		$user_id = $this->dbi_user->getOne(sprintf('user_key=%d', $meeting_info['user_key']), 'user_id');
		
		
		$return = array();
		// TODO escape reservation_name
		$return['reservation_name'] = $reservation_info['reservation_name'];
		$return['reservation_pw'] = $reservation_info['reservation_pw'] ? 1 : 0;
		$return['status'] = $status;
		$return['user_id'] = $user_id;
		$return['room_id'] = $reservation_info['room_key'];
		$return['reservation_id'] = $reservation_info['reservation_session'];
		$return['meeting_id'] = $meeting_id;
		$return['reservation_start_date'] = $start;
		$return['reservation_end_date'] = $end;
		return $return;
	}
}

// if ( realpath( $_SERVER['SCRIPT_FILENAME'] ) !== __FILE__ ) return;

// argv validate
$opt = getopt( '0::1::2::3::4::5::6::7::8::9::h', array(
		'ALL',
		'offset',
		'no-echo',
		'push_key:',
		'reservation_key:',
) );

if( array_key_exists( 'no-echo', $opt ) ){
	switch (true){
		case array_key_exists('ALL', $opt):
		case array_key_exists('offset', $opt):
		case array_key_exists('push_key', $opt):
		case array_key_exists('reservation_key', $opt):
			break;
		default: 
			$opt = false;
			break;
	}
}

if ( !$opt || array_key_exists( 'h', $opt ) ) {
	$echo = array();
	$echo[] = 'Usage: ' . basename( __FILE__ ) . ' --ALL';
	$echo[] = '       ' . basename( __FILE__ ) . ' --offset';
	$echo[] = '       ' . basename( __FILE__ ) . ' [-N[=minutes]]...';
	$echo[] = '       ' . basename( __FILE__ ) . ' --push_key [reservation_push_key]';
	$echo[] = '       ' . basename( __FILE__ ) . ' --reservation_key [reservation_key]';
	$echo[] = '';
	$echo[] = '  -h            Thes help.';
	$echo[] = '  --ALL         Push send ALL the node. (include `is_pend`)';
	$echo[] = '  --offset      Push send using "push_cron_offset" in config.ini';
	$echo[] = '  --no-echo     Ignore echo stdout. (only used by crontab)';
	$echo[] = '  -N=<minutes>  Push send `error_count` = N AND `update_datetime` < now - <minites>.';
	$echo[] = '                N is allow only between 0 and 9 integer.';
	$echo[] = '                If <minutes> is empty then minutes is 0.';
	$echo[] = '                e.g  -0 -1=15 -2=30 -3=60 ...';
	$echo[] = '  --push_key K ';
	$echo[] = '  --reservation_key K';
	$echo[] = '                Push send *_key = K. (include `is_pend`)';
	echo implode( PHP_EOL, $echo ) . PHP_EOL;
	return;
}
// var_dump( $opt, $argc ); return;
$main = new CronReservationPush();
$main->setOpt( $opt );
$main->execute();



