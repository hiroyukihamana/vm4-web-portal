#!/usr/local/bin/php
<?php
require_once 'set_env.php';
require_once 'classes/AppFrame.class.php';
require_once 'classes/N2MY_Storage.class.php';
require_once 'classes/dbi/storage_physical_manage.dbi.php';

class DeleteStoragePhysical extends AppFrame {
	
	private $obj_n2my_storage;
	private $obj_storage_physical;
	
	function init() {
		$this->obj_n2my_storage = new N2MY_Storage( parent::get_dsn() );
		$this->obj_storage_physical = new StoragePhysicalManageTable( parent::get_dsn() );
	}
	
	function default_view() {
		$span = $this->config->get( 'STORAGE', 'phsical_delete_span_day', 0 );
		if ( $span < 1 ) {
			$this->logger2->error( array (
					'margin_phsical_delete' => var_export( $span, true )
			),'please set phsical_delete_margin more than 1' );
			exit();
		}
		
		$storage_list = $this->obj_storage_physical->getUntreatedStorageFileList( $span );
		foreach ( $storage_list as $physical_info ) {
			if ( $this->obj_n2my_storage->deletePhysical( $physical_info['storage_file_key'] ) ) {
				$this->obj_storage_physical->setSuccess( $physical_info['storage_physical_manage_key'] );
			} else {
				$this->obj_storage_physical->setError( $physical_info['storage_physical_manage_key'] );
			}
		}
	}

}
$main = new DeleteStoragePhysical();
$main->execute();
