<?php

function generateMemberID($user_id, $account_id) {
    $glue = '-';
    
    // Sanitize
    $account_id = str_replace('%', '', urlencode($account_id));
    
    return $user_id . $glue . $account_id;
}

?>

