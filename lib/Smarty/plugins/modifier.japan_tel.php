<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * Type:     modifier<br>
 * Name:     nl2br<br>
 * Date:     Feb 26, 2003
 * Purpose:  convert \r\n, \r or \n to <<br>>
 * Input:<br>
 *         - contents = contents to replace
 *         - preceed_test = if true, includes preceeding break tags
 *           in replacement
 * Example:  {$text|nl2br}
 * @link http://smarty.php.net/manual/en/language.modifier.nl2br.php
 *          nl2br (Smarty online manual)
 * @version  1.0
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return string
 */
function smarty_modifier_japan_tel($string)
{
    if (!preg_match('/\+81\ \(0\)/', $string) && preg_match('/\+81 /', $string)) {
        $result = preg_replace('/\+81 /','+81 (0)',$string);
    } else if (preg_match('/^81-/', $string)) {
        $result = preg_replace('/^81-/','+81 (0)',$string);
        $result = preg_replace('/-/',' ',$result);
    } else {
        $result = preg_replace('/-/',' ',$string);
    }
    return $result;
}
/* vim: set expandtab: */

?>
