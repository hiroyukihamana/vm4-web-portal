<?php

/**
 * @author Yuki Asano <y-asano@vcube.co.jp>
 * @copyright Vcube, Inc. All Rights Reserved.
 * @version 0.1
 */

class ExecCommand
{
  public $output;
  public $return;
  public $response;
  public $timeout;
  public $expectReturn;
  private $isError;
  private $cmd;
  private $args = array();

  //{{{ __construct
  /*
   * @param $timeout Integer - second.
   * @param $expectReturn String - expect result value.
   * @throws Exception Invalid timeout property. It's only allowed integer.
   * @throws Exception Invalid expectReturn property.
   */
  public function __construct( $timeout, $expectReturn )
  {
    if ( !is_int( $timeout ) )
      throw new Exception( "Invalid timeout property. It's only allowed integer." );
    $this->timeout = $timeout;

    if ( is_null( $expectReturn ) )
      throw new Exception( "Invaid expectReturn property." );

    $this->expectReturn = ("null" == $expectReturn) ? null : $expectReturn;
  }
  //}}}
  //{{{ isError
  /*
   * @param $valid Integer - command return value.
   */
  public function isError( $valid=0 )
  {
    return ($valid != $this->return);
  }
  //}}}
  //{{{ getResults
  public function getResults()
  {
    return array( "command"  => $this->cmd,
                  "output"   => $this->output,
                  "return"   => $this->return,
                  "response" => $this->response);
  }
  //}}}
  //{{{ getResultString
  /*
   * @param $delimiter mix - A result string delimiter.
   */
  public function getResultString( $delimiter="\n" )
  {
    $p = array("command: %s", "output: %s", "return: %s", "response: %s");
    return sprintf ( implode( $delimiter , $p ),
                     $this->cmd,
                     print_r($this->output, true),
                     $this->return,
                     $this->response);
  }
  //}}}
  //{{{ setTrustedArgument();
  /*
   * @param $arg String - Non escape command argument string.
   */
  public function setTrustedArgument( $arg )
  {
    $this->args[] = $arg;
  }
  //}}}
  //{{{ setTrustedArguments();
  /*
   * @param $arg Array - Non escape command argument array.
   */
  public function setTrustedArguments( Array $args )
  {
    foreach ( $args as $arg )
      $this->setTrustedArgument( $arg );
  }
  //}}}
  //{{{ setArgument();
  /*
   * @param $arg String - Non escape command argument string.
   */
  public function setArgument( $arg )
  {
    $this->args[] = escapeshellarg( $arg );
  }
  //}}}
  //{{{ setArguments();
  /*
   * @param $arg Array - Non escape command argument array.
   */
  public function setArguments( Array $args )
  {
    foreach ( $args as $arg )
      $this->setArgument( $arg );
  }
  //}}}
  //{{{ execute
  /*
   * @param $cmd String - command name.
   * @throws Exception Return value was not a result of the expectation. You should make sure the request sequence.
   */
  public function execute( $cmd )
  {
    $limit = ini_get( "max_execution_time" );

    set_time_limit( $this->timeout );

    $this->cmd = escapeshellcmd( $cmd );
    if ( $this->args && is_array( $this->args ) )
      $this->cmd .= " " . implode( " ", $this->args );
    $this->response = exec( $this->cmd , $this->output, $this->return );

    set_time_limit( $limit );
    $this->args = array();
    if ( $this->expectReturn != $this->return )
        throw new Exception( "Return value was not a result of the expectation. You should make sure the request sequence.\n".$this->cmd."\n".print_r($this->response, true));
  }
  //}}}
}
