<?php
//同期元バケット
define("BASE_BUCKET",   "");
//同期先バケット
define("BACKUP_BUCKET", "");
//S3のaccess_log保存先ディレクトリ
define("ACCESS_LOG_DIR_PATH", "/home/project/S3BucketSync/S3Log");
//同期処理ログ
define("LOG_FILE_PATH", "/home/project/S3BucketSync/logs/sync-".date("Y-m-d").".log");