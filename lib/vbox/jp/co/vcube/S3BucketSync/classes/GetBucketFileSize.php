<?php
require_once __dir__.'/../lib/php/AWS/sdk/sdk.class.php';

class GetBucketFileSize
{
    private $bucket;

    public function __construct($bucket)
    {
        $this->bucket = $bucket;
    }

    public function getAllSize()
    {
        $s3 = new AmazonS3();
        return $s3->get_bucket_filesize($this->bucket, false);
    }
}