<?php
require_once __DIR__.'/../lib/php/AWS/sdk/sdk.class.php';
require_once __DIR__.'/GetBucketFileSize.php';
require_once __DIR__.'/LogFileLoader.php';
require_once __DIR__.'/Logger.php';

class S3BucketSync
{
    /**
     * @var AmazonS3
     * 10Mのファイルで約10秒
     */
    //private $s3;
    private $baseBucket;
    private $backupBucket;
    private $input;
    private $excludePath;
    private $accessLogDirPath;
    private $bucketSizeCheckHour = 6;//毎時稼動するので容量チェックは一日一回にする
    private $dataFile = 'allList.txt';
    private $logger;

    public function __construct($argv)
    {
        //$this->s3                = new AmazonS3();
        //$this->s3->use_ssl       = false;
        $this->baseBucket        = BASE_BUCKET;
        $this->backupBucket      = BACKUP_BUCKET;
        $this->input             = $argv;
        $this->excludePath       = explode(',', EXCLUDE_PATH);
        $this->accessLogDirPath  = ACCESS_LOG_DIR_PATH;
        $this->logger            = new Logger(LOG_FILE_PATH);
    }

    public function run()
    {
        set_time_limit(0);

        $this->logger->info("Start.");

        if( in_array('all', $this->input) ){
            $res = $this->allSync();
        }else{
            $res = $this->sync();
        }

        //サイズ取得は重いAPIなので一日一回だけにしておく
        if( in_array('all', $this->input) || date('H') == $this->bucketSizeCheckHour ){
            $getBucketFileSize = new GetBucketFileSize($this->baseBucket);
            $masterSize = $getBucketFileSize->getAllSize();

            $getBucketFileSize = new GetBucketFileSize($this->backupBucket);
            $backupSize = $getBucketFileSize->getAllSize();

            $this->logger->info("Master Size:".$masterSize);
            $this->logger->info("Backup Size:".$backupSize);
        }

        $this->logger->info("End.");
    }

    private function allSync()
    {
        $this->logger->info("Start All Sync.");

        $listFile = __DIR__."/../data/".$this->dataFile;
        unlink($listFile);

        $s3 = new AmazonS3();
        $s3->use_ssl = false;

        $this->logger->info("Listing Start.");

        $list = $s3->list_objects($this->baseBucket);
        while(count($list->body->Contents) > 0){
            foreach($list->body->Contents as $file){
                $line = sprintf("%s,%s\n", $file->Key, $file->ETag);
                file_put_contents($listFile, $line, FILE_APPEND | LOCK_EX);
            }
            $list = $s3->list_objects($this->baseBucket, array('marker'=>$file->Key));
        }
        $this->logger->info("Listing End.");

        $this->logger->info("Copy Start.");

        $fp = fopen($listFile, 'r');
        while (!feof($fp)) {

            $line = trim(fgets($fp));

            if(!$line) continue;

            list($fileKey,$etag) = explode(',',$line);
            $dst = $s3->get_object_headers($this->backupBucket, $fileKey);

            if( $dst->status == '404' || $etag != $dst->header['etag'] ){
                $this->copy($fileKey);
            }else{
                $this->logger->info("Skip:".$fileKey);
            }
        }
        fclose($fp);

        $this->logger->info("Copy end.");
    }

    private function sync()
    {
        $this->logger->info("Start Sync.");

        $logFileLoader = new LogFileLoader($this->accessLogDirPath);

        $targetObject = $logFileLoader->getUpdateLog($this->excludePath);

        if( $targetObject === false ){
            $this->logger->err("AccessLog dir not found. " . $this->accessLogDirPath);
            return false;
        }

        if( isset($targetObject['put']) && count($targetObject['put']) > 0 ){
            foreach($targetObject['put'] as $key){
                $this->copy($key);
            }
        }

        if( isset($targetObject['delete']) && count($targetObject['delete']) > 0 ){
            foreach($targetObject['delete'] as $key){
                $this->delete($key);
            }
        }


    }

    private function copy($key)
    {

        $this->logger->info("Copy:".$key." from:".$this->baseBucket." -> to:".$this->backupBucket);

        //dry-run
        if( in_array('dry-run', $this->input) ) return;

        $source = array('bucket'=>$this->baseBucket,   'filename'=>$key);
        $dest   = array('bucket'=>$this->backupBucket, 'filename'=>$key);

        $s3 = new AmazonS3();
        $s3->use_ssl = false;
        $response = $s3->copy_object($source, $dest);

        if($response->status != '200'){
            $this->logger->err("Object Not Found:".$key);
            return false;
        }

        $acl = $this->getSrcAcl($key);
        $response = $s3->set_object_acl($this->backupBucket, $key, $acl);

        if($response->status != '200'){
            $this->logger->err("Set ACL Error:".$key);
            return false;
        }
    }

    private function delete($key)
    {

        $this->logger->info("Delete:".$key." Bucket:".$this->backupBucket);

        //dry-run
        if( in_array('dry-run', $this->input) ) return;

        $s3 = new AmazonS3();
        $s3->use_ssl = false;
        $response = $s3->delete_object($this->backupBucket, $key);
        if($response->status != '200'){
            $this->logger->err("Object Not Found:".$key);
            return false;
        }
    }

    private function getSrcAcl($key)
    {
        $s3 = new AmazonS3();
        $s3->use_ssl = false;
        $response = $s3->get_object_acl($this->baseBucket, $key);

        $acl = array();
        if( count($response->body->AccessControlList->Grant) > 0 ){
            foreach($response->body->AccessControlList->Grant as $v){
                if( $v->Grantee->ID ){
                    $acl[] = array('id' => (String)$v->Grantee->ID,
                                   'permission' => (String)$v->Permission);
                }
                if( $v->Grantee->URI ){
                    $acl[] = array('id' => (String)$v->Grantee->URI,
                                   'permission' => (String)$v->Permission);
                }
            }
        }

        return $acl;
    }
}