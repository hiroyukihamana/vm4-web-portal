<?php

class Logger{

    private $logFilePath;

    public function __construct($logFilePath)
    {
        $this->logFilePath = $logFilePath;
    }

    public function info($message)
    {
        $str = sprintf("[%s] [INFO] %s\n", date("Y-m-d H:i:s"), $message);
        $this->put($str);
    }

    public function err($message)
    {
        $str = sprintf("[%s] [ERR] %s\n", date("Y-m-d H:i:s"), $message);
        $this->put($str);
    }

    private function put($str)
    {
        file_put_contents($this->logFilePath, $str, FILE_APPEND | LOCK_EX);
        echo $str;
    }

}