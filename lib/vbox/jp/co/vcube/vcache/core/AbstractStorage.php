<?php

/**
 * @author Yuki Asano <y-asano@vcube.co.jp>
 * @copyright Vcube, Inc. All Rights Reserved.
 * @version 0.1
 */

abstract class AbstractStorage
{

  /**
   * @param $target Object - LocalStorage instance that inherited AbstractStorage.
   */
  abstract public function copyTo( $target );

  /**
  * @param $target Object - RemoteStorage instance that inherited AbstractStorage.
  */
  abstract public function uploadTo( $target );

  /**
  * @param $target Object - LocalStorage instance that inherited AbstractStorage.
  */
  abstract public function downloadTo( $target );
  /**
  * @param $target Object - LocalStorage instance that inherited AbstractStorage.
  */
  abstract public function upload( $target );

  /**
  * @param $target Object - LocalStorage instance that inherited AbstractStorage.
  */
  abstract public function download( $target );

  /**
  * @param $target Object - RemoteStorage instance that inherited AbstractStorage.
  */
//  abstract public function upload( $target );

  /**
   * @param $target Object - LocalStorage instance that inherited AbstractStorage.
   */
//  abstract public function download( $target );

  /**
   * $this filepath move to $target filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   */
  abstract public function moveTo( $target );

  /**
   * Create specified directory.
   */
  abstract public function makeDir();

  /**
   * Return the exists filelist array without dot-files(`.` and `..`).
   */
  abstract public function filelist();

  /**
   * Delete a file.
   */
  abstract public function delete();

  /**
   * Verify a file exists.
   */
  abstract public function exists();

  /** @const TRANSFER_MODE_SCP Integer - A transfer mode for scp. */
  const TRANSFER_MODE_SCP   = "scp";

  /** @const TRANSFER_MODE_SFTP Integer - A transfer mode for sftp. */
  const TRANSFER_MODE_SFTP  = "sftp";

  /** @const TRANSFER_MODE_RSYNC Integer - A transfer mode for rsync. */
  const TRANSFER_MODE_RSYNC = "rsync";

  /** @const TRANSFER_MODE_S3CMD Integer - A transfer mode for amazon s3cmd. */
  const TRANSFER_MODE_S3CMD = "s3cmd";

  /** @const TRANSFER_MODE_S3SYNC Integer - A transfer mode for amazon s3sync. */
  const TRANSFER_MODE_S3SYNC = "s3sync";

  const TYPE_FILE = "file";
  const TYPE_DIR  = "dir";
  const TYPE_LINK = "link";
  const TYPE_UNKNOWN = "none";

  /** @public $ignore_exists Boolean - A ignore the existence of a file before using setFilepath method.  */
  public  $ignore_exists = false;

  /** @param $unescape Boolean - The filepath unescape. */
  public  $unescape = false;

  private $_filepath;
  private $_dirname;
  private $_basename;
  private $_extension;
  private $_filename;
  private $_filesize;
  private $_atime;
  private $_mtime;
  private $_ctime;
  private $_file_type;

  
  /**
   * @param $name String - Access property name.
   */
  public function __get( $name )
  {
    if ( property_exists($this, "_".$name) )
      return $this->{"_".$name};
    return null;
  }

  protected function isMountStorage()
  {
    if ( is_subclass_of( $this, 'MountStorage' ) )
        return true;
    return false;
  }

  protected function isRemoteStorage()
  {
    if ( is_subclass_of( $this, 'RemoteStorage' ) )
        return true;
    return false;
  }

  /**
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @param $caller String - caller method name.
   */
  protected function validateCopyTo( $target )
  {
    if ( $this->isRemoteStorage() && $target->isRemoteStorage() )
      throw new Exception( "The RemoteStorage objects can not allowed." );

    return true;
  }

  /**
  * @param $target Object - Storage instance that inherited AbstractStorage.
  * @param $caller String - caller method name.
  */
  protected function validateUploadTo( $target )
  {
    $this->validateCopyTo( $target );
    if (! ( $this->isMountStorage() && $target->isRemoteStorage()) )
      throw new Exception( "UploadTo method is allowed only from LocalStorage to RemoteStorage." );

    return true;
  }

  /**
  * @param $target Object - Storage instance that inherited AbstractStorage.
  * @param $caller String - caller method name.
  */
  protected function validateDownloadTo( $target )
  {
    $this->validateCopyTo( $target );
    if (! ($this->isRemoteStorage()  && $target->isMountStorage()))
      throw new Exception( "DownloadTo method is allowed only from RemoteStorage to LocalStorage." );

    return true;
  }

  /**
   * @param $filepath String - A target filepath string.
   */
  private function isValidFilepath( $filepath )
  {
    $pattern = '|\.{2,}|';
    if ( preg_match($pattern, $filepath) )
      return false;
    return true;
  }

  /**
   * @param $filepath String - A target filepath string.
   * @param $unescape Boolean - The filepath unescape.
   */
  public function setFilepath( $filepath, $unescape=false )
  {
    $config = $this->getStorageConfig();

    $filepath = $config->root_dir . $filepath;

    if ( $this->isValidFilepath( $filepath ) == false )
      throw new Exception( "Invalid filepath pattern. - ". $filepath );

    $this->_filepath = $filepath;
    $this->unescape  = $unescape;

    if ( $this->ignore_exists == false ) {
      if ( $this->exists() == false )
        throw new Exception( "The file or directory was not found. Make sure filepath or use `ignore_exists` property.- ". $filepath );
      else
        $this->setFileinfo( $filepath );
    }
  }

  /**
   * @param $filepath String - A target filepath string.
   */
  protected function setFileinfo( $filepath )
  {
    $stat = stat( $filepath );

    $this->_filesize = $stat["size"];
    $this->_atime = $stat["atime"];
    $this->_mtime = $stat["mtime"];
    $this->_ctime = $stat["ctime"];

    $info = pathinfo( $filepath );
    $this->_dirname   = @$info['dirname'];
    $this->_basename  = @$info['basename'];
    $this->_extension = @$info['extension'];
    $this->_filename  = @$info['filename'];
    if(is_link( $filepath ) == true) {
        $this->_file_type = self::TYPE_LINK;
    } elseif(is_file( $filepath)) {
        $this->_file_type = self::TYPE_FILE;
    } elseif(is_dir($filepath)) {
        $this->_file_type = self::TYPE_DIR;
    } else {
        $this->_file_type = self::TYPE_UNKNOWN;
    }
  }


}
