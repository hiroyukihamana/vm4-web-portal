<?php

class TransferOption
{
  private $transfer_mode;

  public $first_argument = "";
  public $end_argument = "";
  public $timeout = 0;
  public $expect_return = "";
  public $async_transfer = false;

  public function __construct( $transfer_mode )
  {
    $method = "set".ucfirst($transfer_mode)."Option";
    if ( method_exists( __CLASS__, $method ) ) {
      $this->{$method}( $transfer_mode );
    } else {
      throw new Exception( "Invalid transfer_mode. - ".$transfer_mode );
    }
    $this->transfer_mode = $transfer_mode;
  }

  public function getTransferMode()
  {
    return $this->transfer_mode;
  }

  public function setSftpOption()
  {
    $this->timeout = 200;
  }

  public function setRsyncOption()
  {
    $this->timeout = 150;
  }

  public function setScpOption()
  {
    $this->timeout = 100;
    $this->expect_return = 0;
    $this->first_argument = "-Crp";
  }

  public function setS3cmdOption()
  {
    $this->timeout = 1200;
    $this->expect_return = 0;
    $this->first_argument = " -v ";
    $this->end_argument = "";
    $this->async_transfer = true;
  }

  public function setS3syncOption()
  {
    $this->timeout = 1200;
    $this->expect_return = 0;
    $this->first_argument = " -rv ";
  }
  
}
