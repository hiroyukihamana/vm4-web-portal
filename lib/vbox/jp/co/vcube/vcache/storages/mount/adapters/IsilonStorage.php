<?php

/**
 * @author Yuki Asano <y-asano@vcube.co.jp>
 * @copyright Vcube, Inc. All Rights Reserved.
 * @version 0.1
 */

require_once( "jp/co/vcube/vcache/storages/mount/MountStorage.php" );

abstract class IsilonStorage extends MountStorage
{

  /**
   * The abstract method for each storage config.
   */
  abstract public function getStorageConfig();

  /** @public $transfer_option TransferOption - Transfer option object. */
  public $transfer_option;

  /** @public $default_transfer_mode Integer - Default transfer mode. Default value is MountStorage::TRANSFER_MODE_SCP */
  public $default_transfer_mode = MountStorage::TRANSFER_MODE_SCP;


}
