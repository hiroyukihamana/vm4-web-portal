#!/usr/bin/env ruby

require File.dirname(__FILE__) + '/amf_marshal'

module RTMP
	module Constants
		SO_EVENT_CONNECT = 1
		SO_EVENT_DISCONNECT = 2
		SO_EVENT_SET_ATTRIBUTE = 3
		SO_EVENT_UPDATE_DATA = 4
		SO_EVENT_UPDATE_ATTRIBUTE = 5
		SO_EVENT_SEND_MESSAGE = 6
		SO_EVENT_STATUS = 7
		SO_EVENT_CLEAR_DATA = 8
		SO_EVENT_DELETE_DATA = 9
		SO_EVENT_DELETE_ATTRIBUTE = 10
		SO_EVENT_INITIAL_DATA = 11
	end
    class SharedObjectEvent

	    def initialize (name, version, type, key=nil, value=nil)
			@name = name
			@version = version
		    @type = type
			@key = key
			@value = value
		end
		attr_accessor :name, :version, :type, :key, :value
	end

	class SharedObjectMessage
		def initialize (name, version, persistence, message_type, key=nil, value=nil)
			@name = name
			@version = version
			@persistence = persistence
			@message_type = message_type
			@key = key
			@value = value
		end
		def _amf_load (t_data)
			n_data = StringIO.new t_data
			@name = n_data.read(n_data.read(2).unpack('n')[0])
			puts @name
			@version = n_data.read(4).unpack('N')[0]
			puts @version
			@persistence = (n_data.read(4).unpack('N')[0] & 0x2) == 2
			puts @persistence
			
			@events = Array.new

			#events_data = Array.new
			#n_data.read().split([0, 0, 0, 0].pack('C*')).each {|elem| events_data << elem if elem.length>0 }

			#events_data.each {|elem|
			until n_data.eof? do
				event_type = n_data.read(1).unpack('c')[0]
				case event_type
				when 0x00
					n_data.read(3)
				when Constants::SO_EVENT_INITIAL_DATA, Constants::SO_EVENT_CLEAR_DATA
					@events << SharedObjectEvent.new(@_name, @version, event_type)
					puts "event_type: " + event_type.to_s
				when Constants::SO_EVENT_UPDATE_DATA
					each_data_size = n_data.read(4).unpack('N')[0]
					each_data = StringIO.new(n_data.read(each_data_size))
					key = AMF::Marshal.read_string(each_data)
					val = AMF::Marshal.load(each_data)

					@events << SharedObjectEvent.new(@_name, @version, event_type, key, val)
					puts "event_type: " + event_type.to_s + ", key: " + key.to_s + ", val: " + val.to_s
				when Constants::SO_EVENT_UPDATE_ATTRIBUTE, Constants::SO_EVENT_DELETE_DATA
			        event_data_size = n_data.read(4).unpack('N')[0]
					key = AMF::Marshal.read_string(n_data)
					@events << SharedObjectEvent.new(@name, @version, event_type, key)
					puts "event_type: " + event_type.to_s + ", key: " + key.to_s
				else
					@events << SharedObjectEvent.new(@name, @version, event_type)
				    puts "unexpected so event type: " + event_type.to_s
				end
			end

		end
		def serialize
			data = ''
			data << [@name.length].pack('n')
			data << @name
			data << [@version].pack('N')
			data << [@persistence ? 2 : 0].pack('N')
			data << [0].pack('N')
			data << [@message_type].pack('c')

			case @message_type
			when Constants::SO_EVENT_CONNECT, Constants::SO_EVENT_DISCONNECT
				data << [0].pack('N')
			when Constants::SO_EVENT_SET_ATTRIBUTE
				key = AMF::Marshal.write_string(@key)
				value = AMF::Marshal.dump(@value)
				data << [key.length + value.length].pack('N')
				data << key
				data << value
			end

			data
		end
		def to_s
			t_str = "name : "<<@name.to_s<<"\n"
			t_str << "version : "<<@version.to_s<<"\n"
			t_str << "persistence : \n"<<@persistence.to_s<<"\n"
			t_str << "message_type : \n"<<@message_type.to_s<<"\n"
		end
		attr_writer :name,:version,:persistence,:message_type,:key,:value,:events
		attr_reader :name,:version,:persistence,:message_type,:key,:value,:events
	end
	
	module SharedObjectMessageExtension
		def SharedObjectMessageExtension.extend_object(obj)
			if obj.respond_to? :register_datatype
				super
				obj.register_datatype(19) do |t_data|
					n_func = SharedObjectMessage.allocate
					n_func._amf_load t_data
					n_func
				end
			else
				raise "this object doesn't have the \"register_datatype\" method"
			end
		
		end
	end
end
