#!/usr/bin/env ruby

require File.dirname(__FILE__) + '/shared_object_message'

module RTMP

	class SharedObject
	    @@instances = []

		attr_reader :data, :uri, :name, :persistence, :version
		attr_writer :version

		def self.get_remote(name, uri, persistence=false)

			so = @@instances.find {|elem|
				elem.uri == uri &&
				elem.name == name &&
				elem.persistence == persistence
			}

			if so.nil?
			    so = SharedObject.new(name, uri, persistence)
				@@instances << so
			end

            so
		end

		def initialize(name, uri, persistence=false)
			@name = name
			@uri = uri
			@persistence = persistence
			@net_connection = nil
			@data = nil
			@version = 0
		end

		def connect(net_connection)
			@data = SharedObjectData.new(self)
		    @net_connection = net_connection
			@net_connection.send_shared_object_message(SharedObjectMessage.new(@name, 0, @persistence, 0x01))
		end

		def disconnect
			@net_connection.send_shared_object_message(SharedObjectMessage.new(@name, 0, @persistence, 0x02))
		    @net_connection = nil
		end

		def send_message(attr_name, value)
			puts 'send_message'
			@net_connection.send_shared_object_message(SharedObjectMessage.new(@name, @version, @persistence, 0x03, attr_name, value))
		end

		def update_data(attr_name, value)
			unless @data.nil?
				@data.write_attribute_no_event(attr_name, value)
			end
		end

		alias clear disconnect
		alias close disconnect

	end

	class SharedObjectData

		def initialize(owner)
			@owner = owner
			@attributes = Hash.new
		end

		def [](attr_name)
			read_attribute(attr_name)
		end

		def []=(attr_name, value)
			write_attribute(attr_name, value)
		end

		def write_attribute_no_event(attr_name, value)
			attr_name = attr_name.to_s
			@attributes[attr_name] = value
		end

		private

		def attribute?(attr_name)
			read_attribute(attr_name)
		end
		def attribute=(attr_name, value)
			write_attribute(attr_name, value)
		end

		def read_attribute(attr_name)
			attr_name = attr_name.to_s
			@attributes[attr_name]
		end

		def write_attribute(attr_name, value)
			attr_name = attr_name.to_s
			@attributes[attr_name] = value
			@owner.send_message(attr_name, value)
		end

	    def method_missing(method_id, *args, &block)
		    method_name = method_id.to_s
			if md = /\=$/.match(method_name)
			    attr_name = md.pre_match
				__send__("attribute=", attr_name, *args, &block)
			else
				__send__("attribute?", method_name, *args, &block)
			end
		end
	end
end

