<?php
############################################################################
#    Copyright (C) 2003/2004 by yannick connan                             #
#    yannick@dazzlebox.com                                                 #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU Library General Public License as       #
#    published by the Free Software Foundation; either version 2 of the    #
#    License, or (at your option) any later version.                       #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU Library General Public     #
#    License along with this program; if not, write to the                 #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################
class Ping {
	function Ping ($value,$response) {
		$this->value = $value;
		$this->response = $response;
	}
	function _load ($t_data) {
		$this->response = unpack('n', substr($t_data,0,2));
		$this->value = unpack('N',substr($t_data,2,4));
		if ($this->response == 6) {
			$this->response = false;
        } else {
			$this->response = true;
		}
	}
	function serialize() {
		if ($this->response) {
			$num = 7;
        } else {
			$num = 6;
		}
		return pack('nN', $num, $this->value);
	}
	function to_s() {
		$t_str = "value : ".$this->value.to_s."\n";
		$t_str .= "response : ".$this->response."\n";
        return $t_str;
	}

    // getter
    function getVar($varname) {
        if (isset($this->$varname)) {
            return $this->$varname;
        }
    }
    
    // setter
    function setVar($varname, $value) {
        if (isset($this->$varname)) {
            $this->$varname = $value;
        }
    }
}

class ClientBandwidth {
	function ClientBandwidth ($value,$mist=null) {
		$this->value = $value;
		$this->mist = $mist;
	}
	function _load ($t_data) {
		$this->value = unpack('N', substr($t_data, 0, 4));
		$this->mist = unpack('C', substr($t_data,4,1));
	}
	function serialize() {
		return pack('NC', $this->value, $this->mist);
	}
	function to_s() {
		$t_str = "value : ".$this->value.to_s."\n";
		$t_str .= "mist : ".$this->mist.to_s."\n";
        return $t_str;
	}
    // getter
    function getVar($varname) {
        if (isset($this->$varname)) {
            return $this->$varname;
        }
    }
    
    // setter
    function setVar($varname, $value) {
        if (isset($this->$varname)) {
            $this->$varname = $value;
        }
    }
}


class ServerBandwidth {
	function ServerBandwidth ($value) {
		$this->value = $value;
	}
	function _load ($t_data) {
		$this->value = unpack('N', substr($t_data, 0, 4));
	}
	function serialize() {
		return pack('N', $this->value);
	}
	function to_s() {
		$t_str = "value : ".$this->value.to_s."\n";
		return $t_str;
	}
    // getter
    function getVar($varname) {
        if (isset($this->$varname)) {
            return $this->$varname;
        }
    }
    
    // setter
    function setVar($varname, $value) {
        if (isset($this->$varname)) {
            $this->$varname = $value;
        }
    }
}

class Mistery {
	function Mistery ($value) {
		$this->value = $value;
	}
	function _load ($t_data) {
		$this->value = unpack('N', substr($t_data,0,4));
	}
	function serialize() {
		return pack('N', $this->value);
	}
	function to_s() {
		$t_str = "value : ".$this->value.to_s."\n";
		return $t_str;
	}
    // getter
    function getVar($varname) {
        if (isset($this->$varname)) {
            return $this->$varname;
        }
    }
    
    // setter
    function setVar($varname, $value) {
        if (isset($this->$varname)) {
            $this->$varname = $value;
        }
    }
}

module BandwidthExtension {
	function BandwidthExtension.extend_object(obj) {
		if obj.respond_to? :register_datatype
			super
			obj.register_datatype(5) do |t_data|
				n_func = ServerBandwidth.allocate
				n_func._load t_data
				n_func
			}
			obj.register_datatype(6) do |t_data|
				n_func = ClientBandwidth.allocate
				n_func._load t_data
				n_func
			}
			obj.register_datatype(3) do |t_data|
				n_func = Mistery.allocate
				n_func._load t_data
				n_func
			}
			obj.register_datatype(4) do |t_data|
				n_func = Ping.allocate
				n_func._load t_data
				n_func
			}
		else
			raise "this object doesn't have the \"register_datatype\" method"
		}
	
	}
}