<?php
############################################################################
#    Copyright (C) 2003/2004 by yannick connan                             #
#    yannick@dazzlebox.com                                                 #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU Library General Public License as       #
#    published by the Free Software Foundation; either version 2 of the    #
#    License, or (at your option) any later version.                       #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU Library General Public     #
#    License along with this program; if not, write to the                 #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################
//require_once('./rtmp_packet.php');
require_once('function_call.php');
//require_once('./bandwidth_function.php');

class Frame {
    var $timer, $size, $_config, $data_type, $obj;
}

class NetConnection {

	function NetConnection() {
		$this->bytes_out = 0;
		$this->msg_out = 0;
		$this->frames_in = array();
		$this->frames_out = array();
		$this->writing_queue = array();
		$this->authorized = false;
		$this->on_connect_action = array();
		$this->on_error_action = array();
		$this->on_disconnect_action = array();
		$this->request_id = 1;
		$this->request_array = array();
		$this->bandwidth_in = null;
		$this->bandwith_out = null;
		$this->end_point = null;
		$this->last_timer_frames = null;
	}
	
	function get_last_timer($frame_num) {
		if (!$this->last_timer_frames) {
			$this->last_timer_frames = array();
		}
        list($utime, $time) = split(" ",mktime());
		if (!array_key_exists($frame_num, $this->last_timer_frames)) {
			$this->last_timer_frames[$frame_num] = floor($time * 1000 + $utime * 1000);
			$dif_time = 0;
        } else {
			$n_time = floor($time * 1000 + $utime * 1000);
			$dif_time = $n_time - $this->last_timer_frames[$frame_num];
			$this->last_timer_frames[$frame_num] = $n_time;
		}
		return $dif_time;
	}
	
	function connect($uri,$args) {
		$uri_arr = parse_url($uri);
		$this->uri = $uri;
		if ($uri_arr["scheme"] == 'rtmp') {
			if (!$uri_arr["port"]) {
				$this->port = '1935';
            } else {
				$this->port = $uri_arr["port"];
			}
			$this->host = $uri_arr["host"];
			$this->path = $uri_arr["path"];
			if ($this->path[0] == 0x2F) {
				$this->path = substr($this->path, 1);
			}
            /*
			while ($this->reading_thread) {
            */
                $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
                var_dump(array($this->socket, $this->host,$this->port));
                socket_connect($this->socket, $this->host,$this->port);
				socket_write($this->socket, "\03".str_pad("", 1536, "a"));
				socket_read ($this->socket, 1);
				$qq = socket_read($this->socket, 1536);
				socket_write($this->socket, $qq);
				socket_read ($this->socket, 1536);
                socket_close($this->socket);
				$fc = new FunctionCall("connect",1,$args);
				$fc->setVar("first_arg", array(
                    "app" => $this->path,
                    "flashVer" => "LNX 7.0.0",
                    "swfUrl"=>"http://toto.com/",
                    "tcUrl"=>$this->uri
                    ));
                    
				$t_amf = $fc->serialize();
				$l = $t_amf.length;
                /*
				if ($l >= 128) {
					$t_amf = pack("C", $t_amf[0..127]+[195])+$t_amf[128..-1];
				}
				$this->request_array[1] = Proc.new do |$f_obj|
					if ($f_obj.method == "_result") {
						if ($f_obj.args[0]['code'] == "NetConnection.Connect.Success") {
							$this->process_writing();
							$this->on_connect();
                        } else {
							$this->on_error($f_obj.args[0]);
							$this->disconnect();
						}
					} elseif ($f_obj.method == "_error") {
						$this->on_error($f_obj.args[0]);
						$this->disconnect();
					}
				}
				socket_write($this->socket, "\03"+setMediumInt(0)+setMediumInt(l)+[20].pack("C")+"\0\0\0\0"+$t_amf);
				$this->connected = true;
                /*
				while $this->connected
					begin
						$pack = get_packet
						#puts "$pack"
					rescue Exception
						$this->connected = false;
						$this->disconnect();
					else
						$pack.extend BandwidthExtension
						$pack.extend FunctionCallExtension
						case $pack.data_type
						when 20
							$t_func = $pack.parsed_data
							$t_func.request_id = $t_func.request_id.to_i
							case $t_func.method
							when "_result"
								$this->on_returned_result($t_func);
							when "onStatus"
								$this->on_returned_error($t_func);
							when "_error"
								$this->on_returned_error($t_func);
							else
							}
						when 3
						
						when 4
							if !$pack.parsed_data.response
								write_packet(RTMPPacket.new(2,get_last_timer(2),Ping.new($pack.parsed_data.value,true).serialize,4,0))
							}
						when 5
							$this->bandwidth_in = $pack.parsed_data.value
						when 6
							$this->bandwidth_out = $pack.parsed_data.value
							write_packet(RTMPPacket.new(2,get_last_timer(2),ServerBandwidth.new($this->bandwidth_out).serialize,5,0))
						else
						}
					}
				}
			}
            */
		}
	}

	function on_returned_result($func) {
        /*
		if $func.request_id != 0
			if $this->request_array.has_key? $func.request_id
				Thread.critical = true
				$t_handle = $this->request_array.delete($func.request_id)
				Thread.critical = false
				$t_handle.call($func)
			}
		}
        */
	}
	
	function on_returned_error($func) {
        /*
		if $func.request_id != 0
			if $this->request_array.has_key? $func.request_id
				Thread.critical = true
				$t_handle = $this->request_array.delete($func.request_id)
				Thread.critical = false
				$t_handle.call($func)
			}
		else
			on_error($func.args[0])
		}
        */
	}
	
	function process_request($func) {
        /*
		obj = $this->end_point
		meth_arr = method.split("/")
		begin
			until meth_arr.length <= 2
				m_name = meth_arr.shift.intern
				if obj.respond_to? m_name
					obj = obj.send(m_name)
				else
					raise StandardError, "property not found"
				}
			}
			if obj.respond_to? m_name
				res = obj.send(m_name,*$func.args)
			else
				raise StandardError, "method not found"
			}
		rescue Exception => err
			$t_func = FunctionCall.new("_error",$func.request_id,[{level=>"error",code=>"NetConnection.Call.Failed",description=>err.to_s}])
		else
			$t_func = FunctionCall.new("_result",$func.request_id,[res])
		}
		write_packet(RTMPPacket.new(3,get_last_timer(3),$t_func.serialize,20,0))
        */
	}
	
	function send_function($func,$frame=3,$obj=0,$action) {
        /*
		Thread.critical = true
			if action.null?
				r_id = 0
			else
				$this->request_id += 1
				
				r_id = $this->request_id
				$this->request_array[r_id] = action
			}
		Thread.critical = false
		$func.request_id = r_id
		write_packet(RTMPPacket.new($frame,get_last_timer($frame),$func.serialize,20,obj))
        */
	}
	
	function call($meth,$args,$action) {
        /*
		act = action
		send_function(FunctionCall.new(meth.to_s,0,$args)) do |obj|
			act.call(obj.args[0])
		}
        */
	}
	
	function on_connect($action) {
        /*
		if action.null?
			$this->on_connect_action.call()
		else
			$this->on_connect_action = action
		}
        */
        return false;
	}
	
	function on_error($err=null,$action) {
        /*
		if action.null?
			$this->on_error_action.call(err)
		else
			$this->on_error_action = action
		}
        */
        return false;
	}
	
	function on_disconnect($action) {
        /*
		if action.null?
			$this->on_disconnect_action.call()
		else
			$this->on_disconnect_action = action
		}
        */
        return false;
	}
	
	function get_packet() {
        /*
			$f_byte = $this->socket.read(1);
			$first_number = unpack("C", f_byte);
			#puts first_number/16
			$packet_type = $first_number >> 6
			$frame_number = $first_number & 0x3F
			if ($frame_number == 0) {
				$frame_number = unpack("C", $this->socket.read(1));
            } elseif ($frame_number == 1) {
				$frame_number = unpack("n", $this->socket.read(2));
			}
			if ! $this->frames_in.has_key? $frame_number
				$this->frames_in[$frame_number] = new Frame(0,0,0,0);
				if $packet_type != 0
					raise StandardError, "packet error"
				}
			}
			case $packet_type
			when 0
				$this->frames_in[$frame_number]->timer = getMediumInt()
				$this->frames_in[$frame_number].size = getMediumInt()
				$this->frames_in[$frame_number].data_type = $this->socket.read(1).unpack("C")[0]
				$this->frames_in[$frame_number].obj = $this->socket.read(4).unpack("N")[0]
			when 1
				$this->frames_in[$frame_number]->timer = getMediumInt()
				$this->frames_in[$frame_number].size = getMediumInt()
				$this->frames_in[$frame_number].data_type = $this->socket.read(1).unpack("C")[0]
			when 2
				$this->frames_in[$frame_number]->timer = getMediumInt()
			when 3
			
			else

			}
			$packet_str = $this->socket.read($this->frames_in[$frame_number].size)
			return RTMPPacket.new(	$frame_number,
						$this->frames_in[$frame_number]->timer,
						$packet_str,
						$this->frames_in[$frame_number].data_type,
						$this->frames_in[$frame_number].obj)
        */
	}
	function getMediumInt() {
        /*
		num_array = $this->socket.read(3).unpack("C*")
		num = num_array[0]<< 16 ^ num_array[1]<< 8 ^ num_array[2]
		return num
        */
	}
	function setMediumInt($num) {
        /*
		return [num].pack("N")[1,3]
        */
	}

	function process_writing() {
        /*
		$this->writing_thread = Thread.new do
			begin
				while $this->connected do
					$t_packet,to_close = $this->writing_queue.pop
					$t_packet.size = $t_packet.data.length
					if ! $this->frames_out.has_key? $t_packet.frame
						$this->frames_out[$t_packet.frame] = Frame.new(0,0,0,0)
						$packet_type = 0
					else
						$t_frame = $this->frames_out[$t_packet.frame]
						$packet_mask = 0
						if ($t_frame.obj == $t_packet.obj)
							$packet_mask += 1 
						}
						if ($t_frame.data_type == $t_packet.data_type) and ($t_frame.size == $t_packet.size)
							$packet_mask += 1 
							if ($t_frame->timer == $t_packet.timer)
								$packet_mask += 1 
							}
						}
					}
					$t_frame = $this->frames_out[$t_packet.frame]
					$t_f = $t_packet.frame
					if $t_f >= 64 && $t_f <= 255
						$packet_str = "" << [($packet_type*64)].pack('C') << [$t_packet.frame].pack('C')
					} elseif ($t_f > 255
						$t_f = 1
						$packet_str = "" << [(($packet_type*64)+1)].pack('C') << [$t_packet.frame].pack('n')
					} elseif ($t_f < 2
						raise StandardError,"packet Error"
					else
						$packet_str = "" << [(($packet_type*64)+$t_packet.frame)].pack('C')
					}
					case $packet_type
					when 2
						$t_frame.timer = $t_packet.timer
						$packet_str << setMediumInt($t_packet.timer)
					when 1
						$t_frame.timer = $t_packet.timer
						$t_frame.data_type = $t_packet.data_type
						$t_frame.size = $t_packet.size
						$packet_str << setMediumInt($t_packet.timer)
						$packet_str << setMediumInt($t_packet.size)
						$packet_str << $t_packet.data_type
					when 0
						$t_frame.timer = $t_packet.timer
						$t_frame.data_type = $t_packet.data_type
						$t_frame.size = $t_packet.size
						$t_frame.obj = $t_packet.obj
						$packet_str << setMediumInt($t_packet.timer)
						$packet_str << setMediumInt($t_packet.size)
						$packet_str << $t_packet.data_type
						$packet_str << [$t_packet.obj].pack("N")
					else
					
					}
					$packet_str << $t_packet.data
					$this->socket.write($packet_str)
					#$this->bytes_out += $packet_str.length
					#$this->msg_out += 1
					if to_close
						$this->disconnect();
					}
				}
			rescue Exception => obj
				puts obj
				puts obj.backtrace
			}
		}
        */
	}
	function write_packet ($packet,$to_close=false) {
        /*
		if packet.class == RTMPPacket
			$this->writing_queue.push [packet,to_close]
		else
			raise "not a Packet !"
			}
		}
        */
    }
		
	function disconnect() {
        socket_close($this->socket);
        /*
		$this->connected = false
		if !$this->writing_thread.null?
			Thread.kill($this->writing_thread) if Thread.current != $this->writing_thread
		}
		Thread.kill($this->reading_thread) if Thread.current != $this->reading_thread
		$this->socket.close if not $this->socket.closed?
		on_disconnect()
		initialize()
        */
	}

    // getter
    function getVar($varname) {
        if (isset($this->$varname)) {
            return $this->$varname;
        }
    }
    
    // setter
    function setVar($varname, $value) {
        if (isset($this->$varname)) {
            $this->$varname = $value;
        }
    }
}