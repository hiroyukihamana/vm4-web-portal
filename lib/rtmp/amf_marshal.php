<?php
# AMF4R - AMF parser and RPC engine for Ruby
# Copyright 2002-2003  Yannick Connan <yannick@dazzlebox.com>
#
#This library is free software; you can redistribute it and/or
#modify it under the terms of the GNU Lesser General Public
#License as published by the Free Software Foundation; either
#version 2.1 of the License, or (at your option) any later version.
#
#This library is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#Lesser General Public License for more details.
#
#You should have received a copy of the GNU Lesser General Public
#License along with this library; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#/

define("AMF_NUMBER", 0);
define("AMF_BOOLEAN", 1);
define("AMF_STRING", 2);
define("AMF_HASH", 3);
define("AMF_NIL", 5);
define("AMF_UNDEF", 6);
define("AMF_LINK", 7);
define("AMF_ASSOC_ARRAY", 8);
define("AMF_END", 9);
define("AMF_ARRAY", 10);
define("AMF_DATE", 11);
define("AMF_LONG_STRING", 12);
define("AMF_UNDEF_OBJ", 13);
define("AMF_XML", 15);
define("AMF_CUSTOM", 16);

class AmfArgs {
    var $file,$links,$link_count,$to_return;
}

class AmfCustomObject {
    var $class_name,$properties;
}

class MarshalClass {
	
	function set_time_dif() {
        $time_dif = gmdate("U") - time();
		return $time_dif;
    }
	
	function MarshalClass() {
        //TIME_DIF = set_time_dif();
		$this->class_dumper_name_registry = null;
		$this->class_dumper_action_registry = array();
		$this->class_loader_registry = array();
    }
	
	function register_dumper($class_obj, $class_name = null, $action) {
		if ($class_name == null) {
			$class_name = get_class($class_obj);
        }
		$this->class_dumper_name_registry[$class_obj] = $class_name;
		$this->class_dumper_action_registry[$class_obj] = $action;
    }
	
    function delete(&$data, $item) {
        foreach($data as $key => $val) {
            if ($val == $item) {
                unset($data[$key]);
            }
        }
    }
    
	function unregister_dumper($class_obj) {
        $this->delete($this->class_dumper_name_registry, $class_obj);
        $this->delete($this->class_dumper_action_registry, $class_obj);
	}
	
	function register_loader($class_name,$action) {
		$this->class_load_registry[$class_name] = $action;
	}
	
	function unregister_loader($class_name) {
		$this->delete($this->class_load_registry, $class_name);
	}
	
	function get_args($arg) {
		if ($arg == null) {
			$t_str = "";
			$t_arg = new AmfArgs($t_str,array(),0,true);
        } elseif (method_exists($arg,"read")) {
			$t_arg = new AmfArgs($arg,array(),0,false);
        } else {
			$t_str = $arg;
			$t_arg = new AmfArgs($t_str,array(),0,true);
        }
		return $t_arg;
	}
	
	function dump($obj,$t_out=null) {
		$args = $this->get_args($t_out);
		$this->_dump($obj,$args);
		if ($args->to_return) {
			fseek($args->file, 0);
			return $args->file.read;
        } else {
			return true;
		}
	}

	function dump_array($obj,$t_out=null) {
		$args = $this->get_args($t_out);
		foreach($obj as $item) {
			$this->_dump($item,$args);
		}
		if ($args->to_return) {
			fseek($args->file, 0);
			return $args->file.read;
        } else {
			return true;
		}
	}

	function load($t_inner) {
		$args = $this->get_args($t_inner);
		$args->links = array();
		return _load($args);
	}
	
	function load_array($t_inner) {
		$args = $this->get_args($t_inner);
		$args->links = array();
		$arr = array();
		while (!feof($args->file)) {
			array_push($arr, _load($args));
		}
		return $arr;
	}
	
	function write_long($obj,$t_out=null) {
		$args = $this->get_args($t_out);
		$this->_write_long($obj,$args);
		if ($args->to_return) {
			fseek($args->file, 0);
			return $args->file.read;
        } else {
			return true;
		}
	}
	
	function write_short($obj,$t_out=null) {
		$args = $this->get_args($t_out);
		$this->_write_short($obj,$args);
		if ($args->to_return) {
			fseek($args->file, 0);
			return $args->file.read;
        } else {
			return true;
		}
	}
	
	function read_long($t_inner) {
		$args = $this->get_args($t_inner);
		return _read_long(args);
	}
	
	function read_short($t_inner) {
		$args = $this->get_args($t_inner);
		return _read_short($args);
	}
	
	function write_string($obj,$t_out=null) {
		$args = $this->get_args($t_out);
		$this->_write_string($obj,$args);
		if ($args->to_return) {
			fseek($args->file, 0);
			return $args->file.read;
        } else {
			return true;
		}
	}
	
	function read_string($t_inner) {
		$args = $this->get_args($t_inner);
		return _read_string($args);
	}
	
	function write_long_string($obj,$t_out=null) {
		$args = $this->get_args($t_out);
		$this->_write_long_string($obj,$args);
		if ($args->to_return) {
			fseek($args->file, 0);
			return $args->file.read;
        } else {
			return true;
		}
	}
	
	function read_long_string($t_inner) {
		$args = $this->get_args($t_inner);
		return _read_long_string($args);
	}
	
	function read_byte($t_inner) {
		$args = $this->get_args($t_inner);
		return _read_byte($args);
	}
	
	function write_byte($obj,$t_out=null) {
		$args = $this->get_args($t_out);
		$this->_write_byte($obj,$args);
		if ($args->to_return) {
			fseek($args->file, 0);
			return $args->file.read;
        } else {
			return true;
		}
	}
	
	private function setLink($obj,$args) {
		if (array_key_exists($obj->object_id, $args->links)) {
			$args->file.write(AMF_LINK);
			$this->_write_short($args->links[$obj.object_id],$args);
			return false;
        } else {
			$args->links[$obj.object_id] = $args->link_count;
			$args->link_count += 1;
			return true;
		}
	}
	function _write_byte($obj,$args) {
		$args->file.write(pack("c", $obj));
	}
	function _write_short($obj,$args) {
		$args->file.write(pack("n", $obj));
	}
	function _write_long($obj,$args) {
		$args->file.write(pack("N", $obj));
	}
	function _write_string($obj,$args) {
		$this->_write_short($obj.length,args);
		$args->file.write($obj);
	}
	function _write_long_string($obj,$args) {
		$this->_write_long($obj.length,$args);
		$args->file.write($obj);
	}
	
	function _dump($obj,$args) {
		switch (gettype($obj)) {
		case "string":
			if ($obj.length < 65535) {
				$this->_write_byte(AMF_STRING,$args);
				$this->_write_string($obj,$args);
            } else {
				$this->_write_byte(AMF_LONG_STRING,$args);
				$this->_write_long_string($obj,$args);
			}
            break;
		case Numeric:
			$this->_write_byte(AMF_NUMBER,$args);
			$args->file.write(pack("G", floatval($obj)));
            break;
		case "array":
			if ($this->setLink($obj,$args)) {
				$this->_write_byte(AMF_ARRAY,$args);
				$this->_write_long($obj.length,$args);
				foreach($obj as $arr_obj) {
					$this->_dump($arr_obj,$args);
				}
			}
            break;
        /* array と同じかなぁ
		case Hash:
			if ($this->setLink($obj,$args)) {
				$this->_write_byte(AMF_HASH,$args)
				foreach($obj as $h_key => $h_val) {
					$this->_write_string($h_key.to_s,$args);
					$this->_dump($h_val,$args);
				}
				$this->_write_byte(0,$args);
				$this->_write_byte(0,$args);
				$this->_write_byte(AMF_END,$args);
			}
            break;
        */
        /* Timeって何
		case Time:
			$this->_write_byte(AMF_DATE,$args);
			$args->file.write(pack("G", array($obj.to_f * 1000)));
			if (TIME_DIF <= 0) {
				$this->_write_short(-TIME_DIF,$args);
            } else {
				$this->_write_short(65535-TIME_DIF+1,$args);
			}
            break;*/
        case "boolean":
            $this->_write_byte(AMF_BOOLEAN,$args);
            $this->_write_byte(1,$args);
            break;
        /* boolean かな
		case TrueClass:
			$this->_write_byte(AMF_BOOLEAN,$args);
			$this->_write_byte(1,$args);
            break;
		case FalseClass:
			$this->_write_byte(AMF_BOOLEAN,$args);
			$this->_write_byte(0,$args);
            break;*/
		case "null":
			$this->_write_byte(AMF_NIL,$args);
            break;
		default:
			_write_obj($obj,$args);
		}
	}
	
	function _write_obj($obj,$args) {
		if (array_key_exists($obj->class, $this->class_dumper_action_registry)) {
			if ($this->setLink($obj,$args)) {
				$this->_write_byte(AMF_CUSTOM,$args);
				$this->_write_string($this->class_dumper_name_registry[get_class_methods($obj)],$args);
				$n_obj = $this->class_dumper_name_registry[get_class_methods($obj)].call($obj);
				foreach($n_obj as $h_key => $h_val) {
					$this->_write_string($h_key.to_s,$args);
					$this->_dump($h_val,$args);
				}
				$this->_write_byte(0,$args);
				$this->_write_byte(0,$args);
				$this->_write_byte(AMF_END,$args);
			}
		}
	}
	
	function _read_byte($args) {
		return unpack('c',$args->file.read(1));
	}
	
	function _read_short($args) {
		return unpack('n', $args->file.read(2));
	}
	
	function _read_long($args) {
		return unpack('N', $args->file.read(4));
	}
	
	function _read_string($args) {
		return $args->file.read(_read_short($args));
	}
	
	function _read_long_string($args) {
		return $args->file.read(_read_long($args));
	}
	
	function register($obj,$args) {
		$args->links.push($obj);
	}
	
	function _load($args) {
		$vv = _read_byte($args);
		switch ($vv) {
		case AMF_NUMBER:
			return unpack('G', fgets($args->file,8));
		case AMF_BOOLEAN:
			$t_b = _read_byte($args);
			if ($t_b == 0) {
				return false;
            } else {
				return true;
			}
		case AMF_STRING:
			return _read_string($args);
		case AMF_HASH:
			$t_hash = Hash.new
			register($t_hash,$args);
			do {
				$h_key = _read_string($args);
				$h_val = _load($args);
				if ($h_val == "end_obj") {
					break;
                } else {
					$t_hash[$h_key] = $h_val;
				}
			} while(0);
			return $t_hash;
            break;
		case AMF_NIL:
			return null;
            break;
		case AMF_UNDEF:
			return null;
		case AMF_LINK:
			return $args->links[_read_short($args)];
		case AMF_ASSOC_ARRAY:
			$t_hash = Hash.new
			register(t_hash,$args);
			$le = _read_long($args);
			do {
				$h_key = _read_string($args);
				$h_val = _load($args);
				if ($h_val == "end_obj") {
					break;
                } else {
					$t_hash[$h_key] = $h_val;
				}
			} while (0);
			return $t_hash;
		case AMF_END:
			return "end_obj";
		case AMF_ARRAY:
			$t_array = array();
			register($t_array,$args);
			$len = _read_long($args);
            for($i = 0; $i < $len; $i++) {
				$t_array.push(_load($args));
			}
			return $t_array;
		case AMF_DATE:
			$get_time = unpack('G', $args->file.read(8));
			$time_dif = _read_short($args);
			return Time.at($get_time/1000);
		case AMF_LONG_STRING:
			return _read_long_string($args);
		case AMF_UNDEF_OBJ:
			return null;
		case AMF_XML:
			$t_str = "";
			register($t_str,$args);
			$t_str .= _read_long_string($args);
			return $t_str;
		case AMF_CUSTOM:
			return _read_obj($args);
		default:
            throw new Exception(__FUNCTION__);
        }
	}
	
	function _read_obj($args) {
		$class_name = _read_string($args);
		$properties = array();
		do {
			$h_key = _read_string($args);
			$h_val = _load($args);
			if ($h_val == "end_obj") {
				break;
            } else {
				$t_hash[$h_key] = $h_val;
            }
		} while(0);
		
		$t_obj = new AmfCustomObject($class_name,$properties);
		
		if (array_key_exists($class_name, $this->class_loader_registry)) {
			$n_obj = $this->class_loader_registry[$class_name].call($t_obj);
        } else {
			$n_obj = $t_obj;
        }
		
		register($n_obj,$args);
		return n_obj;
    }
}
$Marshal = new MarshalClass();