<?php
############################################################################
#    Copyright (C) 2003/2004 by yannick connan                             #
#    yannick@dazzlebox.com                                                 #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU Library General Public License as       #
#    published by the Free Software Foundation; either version 2 of the    #
#    License, or (at your option) any later version.                       #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU Library General Public     #
#    License along with this program; if not, write to the                 #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

class RawData {
    var $data_type = null;
    var $data = null;
}

class RTMPPacket {
	function RTMPPacket ($frame,$timer,$t_data,$data_type,$obj) {
		$this->frame = $frame;
		$this->timer = $timer;
		$this->data_type = $data_type;
		$this->obj = $obj;
		$this->parsed = false;
		$this->parsed_data = null;
		$this->size = count($t_data);
		$this->data = $t_data;
		$this->types = array();
	}

    // getter
    function getVar($varname) {
        if (isset($this->$varname)) {
            return $this->$varname;
        }
    }
    
    // setter
    function setVar($varname, $value) {
        if (isset($this->$varname)) {
            $this->$varname = $value;
        }
    }

	function register_datatype ($data_type,&action)
		$this->types[$data_type] = action;
	}

	function parsed_data() {
		if (!$this->parsed) {
			if ($this->types.has_key? $this->data_type) {
				$this->parsed_data = $this->types[$this->data_type].call($this->data);
            } else {
				$this->parsed_data = new RawData($this->data_type,$this->data);
			}
			$this->parsed = true;
		}
		return $this->parsed_data;
	}
}
