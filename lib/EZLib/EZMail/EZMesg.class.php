<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | メッセージハンドリングクラス                                         |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors: Masayuki IIno <masayuki.iino@gmail.com>                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 Masayuki IIno all rights reserved.                |
// +----------------------------------------------------------------------+
//
// $Id: EZMesg.class.php,v 1.1 2006/01/23 01:25:49 iino Exp $

// ----------------------------   define  ---------------------------------

// {{{ -- define
// }}}

/**
 *【EZMesg】メッセージハンドリングクラス
 *
 * <b>説明</b><br />
 *
 * <code>
 * </code>
 *
 * @access     public
 * @author     Masayuki IIno <masayuki.iino@gmail.com>
 * @package    EZLib
 * @subpackage EZMail
 * @version    $Revision
 */
class EZMesg
{
// ----------------------------    vars   ---------------------------------

// {{{ -- vars

    /**
     * Bcc
     *
     * @access private
     * @var    array
     */
    var $_bcc = array();

    /**
     * メール本文
     *
     * @access private
     * @var    string
     */
    var $_body = null;

    /**
     * Cc
     *
     * @access private
     * @var    array
     */
    var $_cc = array();

    /**
     * Charset指定
     *
     * @access private
     * @var    string
     */
    var $_charset = "ISO-2022-JP";

    /**
     * Content-Type指定
     *
     * @access private
     * @var    string
     */
    var $_content_type = "text/plain";

    /**
     * 機種依存文字変換テーブル配列
     *
     * @access private
     * @var    array
     */
    var $_conv_map = array();

    /**
     * 整形済みメールデータ
     *
     * @access private
     * @var    string
     */
    var $_data = null;

    /**
     * From
     *
     * @access private
     * @var    string
     */
    var $_from = null;

    /**
     * Mail From
     *
     * @access private
     * @var    string
     */
    var $_mail_from = null;

    /**
     * Message-Id
     *
     * @access private
     * @var    string
     */
    var $_message_id = null;

    /**
     * MIME-Version
     *
     * @access private
     * @var    string
     */
    var $_mime_version = "1.0";

    /**
     * Reply-To
     *
     * @access private
     * @var    string
     */
    var $_reply_to = null;

    /**
     * Return-Path
     *
     * @access private
     * @var    string
     */
    var $_return_pah = "masayuki.iino@gmail.com";

    /**
     * メール件名
     *
     * @access private
     * @var    string
     */
    var $_subject = null;

    /**
     * To
     *
     * @access private
     * @var    array
     */
    var $_to = array();

    /**
     * X-Mailer
     *
     * @access private
     * @var    string
     */
    var $_xmailer = "EZMail-Sender ver.1.0 [ja]";

// }}}

// ----------------------------    init   ---------------------------------

// {{{ -- EZMesg()

    /**
     * コンストラクタ
     *
     * @access public
     * @param  
     * @return 
     */
    function EZMesg()
    {
    }

// }}}
// {{{ -- _EZMesg()

    /**
     * デストラクタ
     *
     * @access private
     * @param  
     * @return 
     */
    function _EZMesg()
    {
    }

// }}}

// ---------------------------- interface ---------------------------------

// ----------------------------  private  ---------------------------------

// {{{ -- _getBccAsString()

    /**
     * Bccのアドレスを文字列で取得
     *
     * @access public
     * @param  
     * @return string
     */
    function _getBccAsString()
    {
        return $this->_getAttribute('bcc', true);
    }

// }}}
// {{{ -- _getCcAsString()

    /**
     * Ccのアドレスを文字列で取得
     *
     * @access public
     * @param  
     * @return string
     */
    function _getCcAsString()
    {
        return $this->_getAttribute('cc', true);
    }

// }}}
// {{{ -- _getToAsString()

    /**
     * Toのアドレスを文字列で取得
     *
     * @access public
     * @param  
     * @return string
     */
    function _getToAsString()
    {
        return $this->_getAttribute('to', true);
    }

// }}}

// {{{ -- _getAttribute()

    /**
     * 送信先アドレスを取得
     *
     * @access public
     * @param  string  $field  フィールド   [ to | cc | bcc ]
     * @param  boolean $format フォーマット [ true = string | false = array ]
     * @return mixed
     */
    function _getAttribute($field = 'to', $format = false)
    {
        //get email address
        if ($field) {
            //get array
            $ra = array();
            if (preg_match("/^to$/i",  $field)) {
                $ra = $this->_to;
            }
            if (preg_match("/^cc$/i",  $field)) {
                $ra = $this->_cc;
            }
            if (preg_match("/^bcc$/i", $field)) {
                $ra = $this->_bcc;
            }

            //get values
            if ($format) {
                $str = null;
                if ($ra) {
                    $lines =  array();
                    foreach ($ra as $val) {
                        $line  = null;
                        $email = $val['email'];
                        $label = $val['label'];
                        if ($label) {
                            mb_language("ja");
                            $enc = (mb_detect_encoding( $label, "auto")) ? mb_detect_encoding($label, "auto")
                                                                         : "EUC-JP" ;

                            if (mb_strlen($label) > 15) {
                                $len = 30;
                                $lab = array();
                                $i   = 0;
                                while ($i <= $len) {
                                    $tmp = mb_substr($label, $i, 15, "EUC-JP");
                                    $tmp = mb_convert_encoding ($tmp, "JIS", $enc);
                                    $tmp = mb_encode_mimeheader($tmp, "JIS", "B" );
                                    $tmp = trim($tmp);

                                    if ($tmp) {
                                        array_push($lab, $tmp);
                                    }
                                    $i = $i + 15;
                                }
                                $label = implode(" ", $lab);
                            } else {
                                $label = mb_convert_encoding ($label, "JIS", $enc);
                                $label = mb_encode_mimeheader($label, "JIS", "B" );
                            }
                            $line  = sprintf("%s <%s>", $label, $email);
                        } else {
                            $line  = $email;
                        }
                        array_push($lines, $line);
                    }
                    $str = implode(", ", $lines);
                }
                return $str;
            } else {
                return $ra;
            }
        }
    }

// }}}
// {{{ -- _setAttribute()

    /**
     * 送信先アドレスを設定
     *
     * @access public
     * @param  string $field  フィールド   [ to | cc | bcc ]
     * @param  string $email  メールアドレス
     * @param  string $label  表示ラベル
     * @return 
     */
    function _setAttribute($field, $email, $label = null)
    {
        //set values
        if (preg_match("/^to$/i",  $field)) {
            $this->_to[]  = array("email" => $email,
                                  "label" => $label
                                 );
        }
        if (preg_match("/^cc$/i",  $field)) {
            $this->_cc[]  = array("email" => $email,
                                  "label" => $label
                                 );
        }
        if (preg_match("/^bcc$/i", $field)) {
            $this->_bcc[] = array("email" => $email,
                                  "label" => $label
                                 );
        }
    }

// }}}

// ----------------------------   public  ---------------------------------

// {{{ -- init()

    /**
     * 整形済みメールデータを初期化
     *
     * @access public
     * @param  
     * @return string
     */
    function init()
    {
        $this->_data = null;
    }

// }}}

// {{{ -- get()

    /**
     * 整形済みメールデータを取得
     *
     * @access public
     * @param  
     * @return string
     */
    function get()
    {
        return $this->_data;
    }

// }}}
// {{{ -- getBcc()

    /**
     * Bccのアドレスを配列で取得
     *
     * @access public
     * @param  
     * @return array
     */
    function getBcc()
    {
        return $this->_getAttribute('bcc', false);
    }

// }}}
// {{{ -- getBody()

    /**
     * メール本文を取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getBody()
    {
        if ($this->_body) {
            mb_language("ja");
            $enc  = (mb_detect_encoding($this->_body, "auto")) ? mb_detect_encoding($this->_body, "auto")
                                                               : "EUC-JP" ;
            $body = $this->_body;
            $body = mb_convert_kana(    $body, "KV" , $enc);
            $body = mb_convert_encoding($body, "JIS", $enc);

            return $body;
        }
    }

// }}}
// {{{ -- getCc()

    /**
     * Ccのアドレスを配列で取得
     *
     * @access public
     * @param  
     * @return array
     */
    function getCc()
    {
        return $this->_getAttribute('cc', false);
    }

// }}}
// {{{ -- getCharset()

    /**
     * Charsetを取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getCharset()
    {
        return $this->_charset;
    }

// }}}
// {{{ -- getContentType()

    /**
     * Content-Typeを取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getContentType()
    {
        return $this->_content_type;
    }

// }}}
// {{{ -- getFrom()

    /**
     * Fromを取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getFrom()
    {
        return $this->_from;
    }

// }}}
// {{{ -- getMailFrom()

    /**
     * Mail Fromを取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getMailFrom()
    {
        //return Mail From
        if ($this->_mail_from) {
            return $this->_mail_from;
        }

        //set Mail From
        if ($this->_from) {
            $this->setMailFrom($this->_from);
            return $this->_mail_from;
        }
    }

// }}}
// {{{ -- getMessageId()

    /**
     * Message-Idを取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getMessageId()
    {
        return $this->_message_id;
    }

// }}}
// {{{ -- getMimeVersion()

    /**
     * MIME-Versionを取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getMimeVersion()
    {
        return $this->_mime_version;
    }

// }}}
// {{{ -- getReplyTo()

    /**
     * Reply-Toを取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getReplyTo()
    {
        //return Reply-To
        if ($this->_reply_to) {
            return $this->_reply_to;
        }

        //set Reply-To
        if ($this->_mail_from) {
            $this->setReplyTo($this->_mail_from);
            return $this->_reply_to;
        }
        if ($this->_from) {
            $this->setReplyTo($this->_from);
            return $this->_reply_to;
        }
    }

// }}}
// {{{ -- getReturnPath()

    /**
     * Return-Pathを取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getReturnPath()
    {
        return $this->_return_path;
    }

// }}}
// {{{ -- getSubject()

    /**
     * メール件名を取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getSubject()
    {
        if ($this->_subject) {
            mb_language("ja");
            $enc     = (mb_detect_encoding($this->_subject, "auto")) ? mb_detect_encoding($this->_subject, "auto")
                                                                     : "EUC-JP" ;
            $subject = $this->_subject;
            $subject = mb_convert_kana(     $subject, "KV" , $enc);
            //$subject = mb_convert_encoding ($subject, "JIS", $enc);
            //$subject = mb_encode_mimeheader($subject, "JIS", "B" );

            if (mb_strlen($subject) > 15) {
                $len = mb_strlen($subject);
                $sub = array();
                $i   = 0;
                while ($i <= $len) {
                    $tmp = mb_substr($subject, $i, 15, "EUC-JP");
                    $tmp = mb_convert_encoding ($tmp, "JIS", $enc);
                    $tmp = mb_encode_mimeheader($tmp, "JIS", "B" );
                    $tmp = trim($tmp);

                    if ($tmp) {
                        array_push($sub, $tmp);
                    }
                    $i   = $i + 15;
                }
                $subject = implode("\r\n ", $sub);
            } else {
                $subject = mb_convert_encoding ($subject, "JIS", $enc);
                $subject = mb_encode_mimeheader($subject, "JIS", "B" );
            }

            return $subject;
        }
    }

// }}}
// {{{ -- getTo()

    /**
     * Toのアドレスを配列で取得
     *
     * @access public
     * @param  
     * @return array
     */
    function getTo()
    {
        return $this->_getAttribute('to', false);
    }

// }}}
// {{{ -- getXMailer()

    /**
     * X-Mailerを取得
     *
     * @access public
     * @param  
     * @return string
     */
    function getXMailer()
    {
        return $this->_xmailer;
    }

// }}}

// {{{ -- prepare()

    /**
     * 整形済みメールデータを設定
     *
     * @access public
     * @param   
     * @return string
     */
    function prepare()
    {
        static $id, $cnt, $date, $host;

        //get values
        if ($id == "") {
            $id   = md5(microtime());
            $cnt  = 1;
            $date = date("r");
            $host = isset($_SERVER['HOSTNAME']) ? $_SERVER['HOSTNAME'] : $_SERVER['SERVER_NAME'];
        }

        //create DATA
        $data  = "";
        $data .= sprintf("Message-ID: <%s_%s@%s>\r\n"      , $id, $cnt++, $host);
        $data .= sprintf("Date: %s\r\n"                    , $date);
        $data .= sprintf("From: %s\r\n"                    , $this->getFrom());
        $data .= sprintf("To: %s\r\n"                      , $this->_getToAsString());

        if ($this->_getCcAsString()) {
            $data .= sprintf("Cc: %s\r\n"                  , $this->_getCcAsString());
        }
        if ($this->_getBccAsString()) {
            $data .= sprintf("Bcc: %s\r\n"                 , $this->_getBccAsString());
        }

        $data .= sprintf("Reply-To: %s\r\n"                , $this->getReplyTo());
        $data .= sprintf("Subject: %s\r\n"                 , $this->getSubject());
        $data .= sprintf("MIME-Version: %s\r\n"            , $this->getMimeVersion());
        $data .= sprintf("Content-Type: %s; charset=%s\r\n", $this->getContentType(), $this->getCharset());
        $data .= "Content-Transfer-Encoding: 7bit\r\n";
        $data .= sprintf("X-Mailer: %s\r\n"                , $this->getXMailer());
        $data .= "\r\n";
        $data .= $this->getBody();

        //set DATA
        $this->_data = $data;

        return $this->_data;
    }

// }}}
// {{{ -- setBcc()

    /**
     * Bccに送信先アドレスを設定
     *
     * @access public
     * @param  string $email メールアドレス
     * @param  string $label 表示ラベル
     * @return 
     */
    function setBcc($email, $label = null)
    {
        $this->_setAttribute('bcc', $email, $label);
    }

// }}}
// {{{ -- setBody()

    /**
     * メール本文を設定
     *
     * @access public
     * @param  string $value メール本文
     * @return 
     */
    function setBody($value)
    {
        $this->_body = $value;
    }

// }}}
// {{{ -- setCc()

    /**
     * Ccに送信先アドレスを設定
     *
     * @access public
     * @param  string $email メールアドレス
     * @param  string $label 表示ラベル
     * @return 
     */
    function setCc($email, $label = null)
    {
        $this->_setAttribute('cc', $email, $label);
    }

// }}}
// {{{ -- setCharset()

    /**
     * Charsetを設定
     *
     * @access public
     * @param  string $value キャラセット
     * @return 
     */
    function setCharset($value)
    {
        $this->_charset = $value;
    }

// }}}
// {{{ -- setContentType()

    /**
     * Content-Typeを設定
     *
     * @access public
     * @param  string $value コンテントタイプ
     * @return 
     */
    function setContentType($value)
    {
        $this->_content_type = $value;
    }

// }}}
// {{{ -- setFrom()

    /**
     * Fromを設定
     *
     * @access public
     * @param  string $value メールアドレス
     * @return 
     */
    function setFrom($value)
    {
        $this->_from = $value;
    }

// }}}
// {{{ -- setMailFrom()

    /**
     * Mail Fromを設定
     *
     * @access public
     * @param  string $value メールアドレス
     * @return 
     */
    function setMailFrom($value)
    {
        $this->_mail_from = $value;
    }

// }}}
// {{{ -- setMessageId()

    /**
     * Message-Idを設定
     *
     * @access public
     * @param  string $value メッセージID
     * @return 
     */
    function setMessageId($value)
    {
        $this->_message_id = $value;
    }

// }}}
// {{{ -- setMimeVersion()

    /**
     * MIME-Versionを設定
     *
     * @access public
     * @param  string $value MIMEバージョン
     * @return 
     */
    function setMimeVersion($value)
    {
        $this->_mime_version = $value;
    }

// }}}
// {{{ -- setReplyTo()

    /**
     * Reply-Toにアドレスを設定
     *
     * @access public
     * @param  string $value メールアドレス
     * @return 
     */
    function setReplyTo($value)
    {
        $this->_reply_to = $value;
    }

// }}}
// {{{ -- setReturnPath()

    /**
     * Return-Pathを設定
     *
     * @access public
     * @param  string $value メールアドレス
     * @return 
     */
    function setReturnPath($value)
    {
        $this->_return_path = $value;
    }

// }}}
// {{{ -- setSubject()

    /**
     * メール件名を設定
     *
     * @access public
     * @param  string $subject メール件名
     * @return 
     */
    function setSubject($subject)
    {
        $this->_subject = $subject;
    }

// }}}
// {{{ -- setTo()

    /**
     * Toに送信先アドレスを設定
     *
     * @access public
     * @param  string $email メールアドレス
     * @param  string $label 表示ラベル
     * @return 
     */
    function setTo($email, $label = null)
    {
        $this->_setAttribute('to', $email, $label);
    }

// }}}
// {{{ -- setXMailer()

    /**
     * X-Mailerを設定
     *
     * @access public
     * @param  string $value XMailer
     * @return 
     */
    function setXMailer($value)
    {
        $this->_xmailer = $value;
    }

// }}}

// ----------------------------   option  ---------------------------------

// {{{ -- create_conv_map()

    /**
     * 機種依存文字変換テーブルを生成
     *
     * @access public
     * @param  
     * @return 
     */
    function create_conv_map()
    {
        //変換テーブル初期化
        $char_map = array();

        //半角カナ (0x8EA0 も一応定義していることに注意)
        //$map  = "";
        //$map .= "。 「 」 、 ・ ヲ ァ ィ ゥ ェ ォ ャ ュ ョ ッ ";
        //$map .= "ー ア イ ウ エ オ カ キ ク ケ コ サ シ ス セ ソ ";
        //$map .= "タ チ ツ テ ト ナ ニ ヌ ネ ノ ハ ヒ フ ヘ ホ マ ";
        //$map .= "ミ ム メ モ ヤ ユ ヨ ラ リ ル レ ロ ワ ン ゛ ゜";
        //$char_map['0x8EA0'] = explode(" ", $map);

        //13区
        $map = '';
        $map .= '(1) (2) (3) (4) (5) (6) (7) (8) (9) ';
        $map .= '(10) (11) (12) (13) (14) (15) (16) ';
        $map .= '(17) (18) (19) (20) I II III IV ';
        $map .= 'V VI VII VIII IX X . ミリ ';
        $map .= 'キロ センチ メートル グラム トン アール ヘクタール リットル ';
        $map .= 'ワット カロリー ドル セント パーセント ミリバール ページ mm ';
        $map .= 'cm km mg kg cc m^2 . . . . . . . . 平成 " ';
        $map .= ',, No. K.K. Tel (上) (中) (下) (左) ';
        $map .= '(右) (株) (有) (代) 明治 大正 昭和 ';
        $map .= '≒ ≡ ∫ ∫ Σ √ ⊥ ';
        $map .= '∠ Ｌ △ ∵ ∩ ∪';
        $char_map['0xADA1'] = explode(" ", $map);

        //89区
        $map  = "";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . .";
        $char_map['0xF9A1'] = explode(" ", $map);

        //90区
        $map  = "";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . .";
        $char_map['0xFAA1'] = explode(" ", $map);

        //91区
        $map  = "";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . .";
        $char_map['0xFBA1'] = explode(" ", $map);

        //92区
        $map  = "";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = ". . . . . . . . . . . . . . . . ";
        $map  = "i ii iii iv v vi vii viii ix x ￢ | ' ''";
        $char_map['0xFCA1'] = explode(" ", $map);

        //変換用テーブル作成
        $conv_map = array();

        foreach ($char_map as $c => $r) {
            $code = $c;
            foreach ($r as $v) {
                $char_code = pack('C*', $code/256, $code%256);

                //未定義領域か漢字領域。ゲタに変換。
                if ($v == '.') {
                    $v = "〓";
                }
                $conv_map[$char_code] = $v;
                $code++;
            }
        }
        $this->_conv_map = $conv_map;
    }

// }}}
// {{{ -- replace_special_chars()

    /**
     * 機種依存文字を変換した結果を取得
     *
     * @access public
     * @param  string $str 変換対象文字列
     * @return string
     */
    function replace_special_chars($str)
    {
        //変換テーブルがなければ生成
        if (!$this->_conv_map) {
            $this->create_conv_map();
        }

        //アスキーコード、EUCの2バイト、EUCの3バイトの正規表現
        $ascii       = '[\x00-\x7F]';
        $doubleBytes = '[\xA1-\xFE][\xA1-\xFE]';
        $tripleBytes = '\x8F[\xA1-\xFE][\xA1-\xFE]';

        // ----- 変換対象は -----
        //   半角カナ 8EA0-8EDF  
        //   13区     ADA1-ADFE  
        //   89区     F9A1-F9FE  
        //   90区     FAA1-FAFE  
        //   91区     FBA1-FBFE  
        //   92区     FCA1-FCFE  
        $pattern = '\x8E[\xA0-\xDF]|[\xAD\xAE\xF9\xFA\xFB\xFC][\xA1-\xFE]';
        $ptr     = "/(($ascii|$doubleBytes|$tripleBytes)*?($pattern))/";
        $rep     = "\${1}$this->_conv_map[bin2hex(\${3})]";
        //$str     = preg_replace($ptr, $rep, $str);
        //$str     = preg_replace("/(($ascii|$doubleBytes|$tripleBytes)*?($pattern))/", \$1\$this->_conv_map[\$3], $str);
        //$str     = preg_replace("/(($ascii|$doubleBytes|$tripleBytes)*?($pattern))/", $1$this->_conv_map[$3], $str);

        return $str;
    }

// }}}

}
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
