<?php
require_once("lib/pear/Mail.php");

class EZSmtp {

    var $to = "";
    var $bcc = "";
    var $from = "";
    var $return_path = "";
    var $subject = "";
    var $body = "";
    var $host = "";
    var $lang = "";
    var $file_path = "";
    var $file_name = "";
    var $internal_enc;
    var $timeout = 5;
    var $html_body = "";
    var $text_body = "";

    function EZSmtp($host = "", $lang = "", $internal_enc = "EUC-JP") {
        if ($host == "") {
            $host = ini_get("SMTP");
        }
        $this->internal_enc = $internal_enc;
        $this->lang = $lang;
        $this->host = $host;
    }

    /**
     * 送信先指定
     */
    function setTo($to, $name = "") {
        $this->to = $to;
        $this->to_name = $name;
    }

    /**
     * BCC指定
     */
    function setBcc($bcc, $name = "") {
        $this->bcc = $bcc;
        $this->bcc_name = $name;
    }
    /**
     * 送信元指定
     */
    function setFrom($from, $name = "") {
        $this->from = $from;
        $this->from_name = $name;
    }
    /**
     * 送信元指定
     */
    function setReturnPath($return_path) {
        $this->return_path = $return_path;
    }
    /**
     * 件名
     */
    function setSubject($subject) {
        $this->subject = $subject;
    }
    /**
     * 本文
     */
    function setBody($body) {
        $this->body = $body;
    }

    function setHtmlBody($body) {
        $this->html_body = $body;
    }

    function setTextBody($body) {
        $this->text_body = $body;
    }

    /**
     * 言語指定
     */
    function setLang($lang) {
        $this->lang = $lang;
    }


    /**
     *
     */
    function setFilePath($file_path) {
        $this->file_path = $file_path;
    }

    function setFileName($file_name) {
        $this->file_name = $file_name;
    }


    /**
     * いちいちセットするのが面倒になってきた
     */
    function send2($from, $to, $subject, $body, $bcc = null) {
        $this->setFrom($from);
        $this->setTo($to);
        if ($bcc) {
            $this->setBcc($bcc);
        }
        $this->setSubject($subject);
        $this->setBody($body);
        $this->send();
    }

    /**
     * 送信
     */
    function send($format = "null") {
        // 現在の設定
        $lang = mb_language();
        $enc = mb_internal_encoding();
        // 送信先
        $to         = $this->to;
        $bcc         = $this->bcc;
        $from       = $this->from;
        $subject    = $this->subject;
        $body       = $this->body;
        $return_path = $this->return_path ? $this->return_path : $from;
        $reply_to = $return_path;
        // 改行コードを\r\nに統一
        $body = str_replace("\r\n","\n",$body);
        $body = str_replace("\r","\r\n",$body);
        $body = str_replace("\n","\r\n",$body);
        // 指定言語対応
        switch ($this->lang) {
        // 日本語
        case "ja":
        case "ja_JP":
        case "en":
/*        case "en_US":
            $encode      = "JIS";
            $mime_encode = "ISO-2022-JP";
            $transfer_encoding = "7bit";
            mb_language("ja");
            require_once "lib/EZLib/EZUtil/EZString.class.php";
            $subject = EZString::convertDependenceChar($subject);
            $body    = EZString::convertDependenceChar($body);
            $subject = mb_convert_encoding($subject, $encode, $this->internal_enc);
            $body    = mb_convert_encoding($body, $encode, $this->internal_enc);
            if ($this->to_name) {
                $this->to_name = EZString::convertDependenceChar($this->to_name);
                $this->to_name = mb_convert_encoding($this->to_name, $encode, $this->internal_enc);
            }
            if ($this->from_name) {
                $this->from_name = EZString::convertDependenceChar($this->from_name);
                $this->from_name = mb_convert_encoding($this->from_name, $encode, $this->internal_enc);
            }
            break;*/
        // 中国語
//        case "zh_CN":
//            $encode      = "GB2312";
//            $mime_encode = "GB2312";
//            $transfer_encoding = "base64";
//            mb_language("uni");
//            require_once "lib/EZLib/EZUtil/EZString.class.php";
//            $subject = EZString::convertDependenceChar($subject);
//            $body    = EZString::convertDependenceChar($body);
//            $subject = mb_convert_encoding($subject, $encode, $this->internal_enc);
//            $body    = mb_convert_encoding($body, $encode, $this->internal_enc);
//            if ($this->to_name) {
//                $this->to_name = EZString::convertDependenceChar($this->to_name);
//                $this->to_name = mb_convert_encoding($this->to_name, $encode, $this->internal_enc);
//            }
//            if ($this->from_name) {
//                $this->from_name = EZString::convertDependenceChar($this->from_name);
//                $this->from_name = mb_convert_encoding($this->from_name, $encode, $this->internal_enc);
//            }
//            break;
        default :
            // UTF-8設定
            $encode      = "UTF-8";
            $mime_encode = "UTF-8";
            $transfer_encoding = "base64";
            mb_language("uni");
            $body = chunk_split(base64_encode($body));
        }
        mb_internal_encoding($encode);
        // 送信先
        if ($this->to_name) {
            $to   = $this->_encode_mimeheader($this->to_name, $mime_encode).' <'.$to.'>';
        }
        if ($this->from_name) {
            $from = $this->_encode_mimeheader($this->from_name, $mime_encode).' <'.$from.'>';
            $reply_to = $this->_encode_mimeheader($this->from_name, $mime_encode).' <'.$return_path.'>';
        }
        $subject    = mb_encode_mimeheader($subject, $mime_encode, "B");
        $params["host"] = $this->host;
        $params["timeout"] = $this->timeout;
        /*
        $params["auth"] = "";
        $params["username"] = "";
        $params["password"] = "";
        $params["port"] = "";
        $params["debug"] = false;
        */
        if (!($smtp = Mail::factory("smtp", $params))) {
            error_log("#Unable to instantiate Net_SMTP object".__FILE__.__LINE__);
            // 元に戻す
            mb_language($lang);
            mb_internal_encoding($enc);
            return false;
        }
        // ヘッダ
        $headers["MIME-Version"] = "1.0";
        if($format == "html")
        	$headers["Content-Type"] = ' text/html; charset="'.$mime_encode.'"';
        else
        	$headers["Content-Type"] = 'text/plain; charset="'.$mime_encode.'"';
        $headers["Content-Transfer-Encoding"] = $transfer_encoding;
        $headers["Content-Disposition"] = "inline";
        $headers["To"] = $to;
        $headers["Bcc"] = $bcc;
        $headers["From"] = $from;
        $headers["Reply-To"] = $reply_to;
//        $headers["Return-Path"] = $return_path;
        $headers["Subject"] = $subject;
        $to = $to.$bcc;
        $smtp->send($to, $headers, $body);
        // 元に戻す
        mb_language($lang);
        mb_internal_encoding($enc);
    }

    /*
     * HTMLメール送信
     */
    public function sendHtml($text_send_flag = 1){
        require_once("lib/pear/Mail/mime.php");
        // 現在の設定
        $lang = mb_language();
        $enc = mb_internal_encoding();
        // 送信先
        $to         = $this->to;
        $bcc         = $this->bcc;
        $from       = $this->from;
        $subject    = $this->subject;
        $html_body       = $this->html_body;
        $text_body       = $this->text_body;
        $return_path = $this->return_path ? $this->return_path : $from;
        $reply_to = $return_path;
        // 改行コードを\r\nに統一
        $html_body = str_replace("\r\n","\n",$html_body);
        $html_body = str_replace("\r","\r\n",$html_body);
        $html_body = str_replace("\n","\r\n",$html_body);
        $text_body = str_replace("\r\n","\n",$text_body);
        $text_body = str_replace("\r","\r\n",$text_body);
        $text_body = str_replace("\n","\r\n",$text_body);
        // 指定言語対応
        switch ($this->lang) {
            // 日本語
            case "ja":
            case "ja_JP":
            case "en":
            default :
            // UTF-8設定
            $encode      = "UTF-8";
            $mime_encode = "UTF-8";
            $transfer_encoding = "base64";
            mb_language("uni");
            //$html_body = chunk_split(base64_encode($html_body));
        }
        mb_internal_encoding($encode);
        // 送信先
        if ($this->to_name) {
            $to   = $this->_encode_mimeheader($this->to_name, $mime_encode).' <'.$to.'>';
        }
        if ($this->from_name) {
            $from = $this->_encode_mimeheader($this->from_name, $mime_encode).' <'.$from.'>';
            $reply_to = $this->_encode_mimeheader($this->from_name, $mime_encode).' <'.$return_path.'>';
        }
        $subject    = mb_encode_mimeheader($subject, $mime_encode, "B");
        $params["host"] = $this->host;
        $params["timeout"] = $this->timeout;

        if (!($smtp = Mail::factory("smtp", $params))) {
            error_log("#Unable to instantiate Net_SMTP object".__FILE__.__LINE__);
            // 元に戻す
            mb_language($lang);
            mb_internal_encoding($enc);
            return false;
        }

        $mime = new Mail_Mime("\n");
        if($text_send_flag != 0){
            // macのメーラー対策で文の最後に改行を入れる
            $mime -> setTxtBody($text_body."\r\n");
        }
        $mime -> setHTMLBody($html_body);

        $bodyParam = array(
                "head_charset" => "ISO-2022-JP",
                "text_charset" => "UTF-8",
                "text_encoding" => 'base64',
                "html_charset" => "UTF-8",
                "html_encoding" => 'base64',
        );
        $body = $mime -> get($bodyParam);

        // ヘッダ
        $headers["MIME-Version"] = "1.0";
        $headers["Content-Type"] = ' multipart/alternative; charset="'.$mime_encode.'"';
        $headers["Content-Transfer-Encoding"] = $transfer_encoding;
        $headers["To"] = $to;
        $headers["Bcc"] = $bcc;
        $headers["From"] = $from;
        $headers["Reply-To"] = $reply_to;
        //        $headers["Return-Path"] = $return_path;
        $headers["Subject"] = $subject;
        $headers = $mime -> headers($headers);
        $to = $to.$bcc;
        $smtp->send($to, $headers, $body);
        // 元に戻す
        mb_language($lang);
        mb_internal_encoding($enc);
    }

    /*
     * ファイルを添付してメール送信
     */
    public function sendAttachment(){
        require_once("lib/pear/Mail/mime.php");
        // 現在の設定
        $lang = mb_language();
        $enc = mb_internal_encoding();
        // 送信先
        $to         = $this->to;
        $bcc         = $this->bcc;
        $from       = $this->from;
        $subject    = $this->subject;
        $body       = $this->body;
        $return_path = $this->return_path ? $this->return_path : $from;
        $reply_to = $return_path;
        // 改行コードを\r\nに統一
        $body = str_replace("\r\n","\n",$body);
        $body = str_replace("\r","\r\n",$body);
        $body = str_replace("\n","\r\n",$body);
        // 指定言語対応
        switch ($this->lang) {
            // 日本語
            case "ja":
            case "ja_JP":
            case "en":
                /*        case "en_US":
                 $encode      = "JIS";
                 $mime_encode = "ISO-2022-JP";
                 $transfer_encoding = "7bit";
                 mb_language("ja");
                 require_once "lib/EZLib/EZUtil/EZString.class.php";
                 $subject = EZString::convertDependenceChar($subject);
                 $body    = EZString::convertDependenceChar($body);
                 $subject = mb_convert_encoding($subject, $encode, $this->internal_enc);
                 $body    = mb_convert_encoding($body, $encode, $this->internal_enc);
                 if ($this->to_name) {
                 $this->to_name = EZString::convertDependenceChar($this->to_name);
                 $this->to_name = mb_convert_encoding($this->to_name, $encode, $this->internal_enc);
                 }
                 if ($this->from_name) {
                 $this->from_name = EZString::convertDependenceChar($this->from_name);
                 $this->from_name = mb_convert_encoding($this->from_name, $encode, $this->internal_enc);
            }
            break;*/
            // 中国語
            //        case "zh_CN":
            //            $encode      = "GB2312";
            //            $mime_encode = "GB2312";
            //            $transfer_encoding = "base64";
            //            mb_language("uni");
            //            require_once "lib/EZLib/EZUtil/EZString.class.php";
            //            $subject = EZString::convertDependenceChar($subject);
            //            $body    = EZString::convertDependenceChar($body);
            //            $subject = mb_convert_encoding($subject, $encode, $this->internal_enc);
            //            $body    = mb_convert_encoding($body, $encode, $this->internal_enc);
            //            if ($this->to_name) {
            //                $this->to_name = EZString::convertDependenceChar($this->to_name);
            //                $this->to_name = mb_convert_encoding($this->to_name, $encode, $this->internal_enc);
            //            }
            //            if ($this->from_name) {
            //                $this->from_name = EZString::convertDependenceChar($this->from_name);
            //                $this->from_name = mb_convert_encoding($this->from_name, $encode, $this->internal_enc);
            //            }
            //            break;
            default :
            // UTF-8設定
            $encode      = "UTF-8";
            $mime_encode = "UTF-8";
            $transfer_encoding = "base64";
            mb_language("uni");
            //$body = chunk_split(base64_encode($body));
        }
        mb_internal_encoding($encode);
        // 送信先
        if ($this->to_name) {
            $to   = $this->_encode_mimeheader($this->to_name, $mime_encode).' <'.$to.'>';
        }
        if ($this->from_name) {
            $from = $this->_encode_mimeheader($this->from_name, $mime_encode).' <'.$from.'>';
            $reply_to = $this->_encode_mimeheader($this->from_name, $mime_encode).' <'.$return_path.'>';
        }
        $subject    = mb_encode_mimeheader($subject, $mime_encode, "B");
        $params["host"] = $this->host;
        $params["timeout"] = $this->timeout;
        /*
         $params["auth"] = "";
        $params["username"] = "";
        $params["password"] = "";
        $params["port"] = "";
        $params["debug"] = false;
        */
        if (!($smtp = Mail::factory("smtp", $params))) {
            error_log("#Unable to instantiate Net_SMTP object".__FILE__.__LINE__);
            // 元に戻す
            mb_language($lang);
            mb_internal_encoding($enc);
            return false;
        }

        $mime = new Mail_Mime("\n");
        // macのメーラー対策で文の最後に改行を入れる
        $mime -> setTxtBody($body."\r\n");
        $mime -> addAttachment($this->file_path, "text/plain" , $this->file_name);

        $bodyParam = array(
                "head_charset" => "ISO-2022-JP",
                "text_charset" => "UTF-8",
                "text_encoding" => 'base64'
        );
        $body = $mime -> get($bodyParam);

        // ヘッダ
        $headers["MIME-Version"] = "1.0";
        $headers["Content-Type"] = ' multipart/mixed; charset="'.$mime_encode.'"';
        $headers["Content-Transfer-Encoding"] = $transfer_encoding;
        $headers["To"] = $to;
        $headers["Bcc"] = $bcc;
        $headers["From"] = $from;
        $headers["Reply-To"] = $reply_to;
        //        $headers["Return-Path"] = $return_path;
        $headers["Subject"] = $subject;
        $headers = $mime -> headers($headers);
        $to = $to.$bcc;
        $smtp->send($to, $headers, $body);
        // 元に戻す
        mb_language($lang);
        mb_internal_encoding($enc);
    }

    private function _encode_mimeheader($string, $charset=null, $linefeed="\r\n") {
        if (!$charset)
            $charset = mb_internal_encoding();

        $start = "=?$charset?B?";
        $end = "?=";
        $encoded = '';

        /* Each line must have length <= 75, including $start and $end */
        $length = 75 - strlen($start) - strlen($end);
        /* Average multi-byte ratio */
        $ratio = mb_strlen($string, $charset) / strlen($string);
        /* Base64 has a 4:3 ratio */
        $magic = $avglength = floor(3 * $length * $ratio / 4);

        for ($i=0; $i <= mb_strlen($string, $charset); $i+=$magic) {
            $magic = $avglength;
            $offset = 0;
            /* Recalculate magic for each line to be 100% sure */
            do {
                $magic -= $offset;
                $chunk = mb_substr($string, $i, $magic, $charset);
                $chunk = base64_encode($chunk);
                $offset++;
            } while (strlen($chunk) > $length);
            if ($chunk)
                $encoded .= ' '.$start.$chunk.$end.$linefeed;
        }
        /* Chomp the first space and the last linefeed */
        $encoded = substr($encoded, 1, -strlen($linefeed));
        return $encoded;
    }
}
?>
