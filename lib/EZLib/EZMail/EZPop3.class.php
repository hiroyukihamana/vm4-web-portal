<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | POP3 ラッパークラス                                                  |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors: Masayuki IIno <masayuki.iino@gmail.com>                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 Masayuki IIno all rights reserved.                |
// +----------------------------------------------------------------------+
//
// $Id: EZPop3.class.php,v 1.1 2006/01/23 01:25:49 iino Exp $

require_once('Net/POP3.php');

// ----------------------------   define  ---------------------------------

// {{{ -- define
// }}}

/**
 *【EZPop3】POP3 ラッパークラス
 *
 * <b>説明</b><br />
 *
 * <code>
 * </code>
 *
 * @access     public
 * @author     Masayuki IIno <masayuki.iino@gmail.com>
 * @package    EZLib
 * @subpackage EZMail
 * @version    $Revision
 */
class EZPop3
{
// ----------------------------    vars   ---------------------------------

// {{{ -- vars

    /**
     * POP3オブジェクト
     *
     * @access private
     * @var    object Net_POP3
     */
    var $_pop3 = null;

    /**
     * 接続先ホスト名
     *
     * @access private
     * @var    string
     */
    var $_host = null;

    /**
     * POP3ポート番号
     *
     * @access private
     * @var    integer
     */
    var $_port = null;

// }}}

// ----------------------------    init   ---------------------------------

// {{{ -- EZPop3()

    /**
     * コンストラクタ
     *
     * @access public
     * @param  
     * @return 
     */
    function EZPop3()
    {
    }

// }}}
// {{{ -- _EZPop3()

    /**
     * デストラクタ
     *
     * @access private
     * @param  
     * @return 
     */
    function _EZPop3()
    {
    }

// }}}

// ---------------------------- interface ---------------------------------

// ----------------------------  private  ---------------------------------

// ----------------------------   public  ---------------------------------

// {{{ -- init()

    /**
     * 初期化メソッド
     *
     * @access public
     * @param  string  $host   接続先ホスト名
     * @param  integer $port   ポート番号
     * @param  string  $myhost 接続元ホスト名
     * @return 
     */
    function init($host = "localhost", $port = 25, $myhost = "localhost")
    {
        //set values
        $this->_host = $host;
        $this->_port = $port;

        //craete pop3 object
        $this->_pop3 =& new Net_POP3();

        if (PEAR::isError($this->_pop3)) {
            return $this->_pop3;
        }
    }

// }}}

// {{{ -- connect()

    /**
     * 接続処理
     *
     * @access public
     * @param  
     * @return 
     */
    function connect()
    {
    }

// }}}
// {{{ -- disconnect()

    /**
     * 切断処理
     *
     * @access public
     * @param  
     * @return 
     */
    function disconnect()
    {
    }

// }}}

// ----------------------------   option  ---------------------------------

}
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
