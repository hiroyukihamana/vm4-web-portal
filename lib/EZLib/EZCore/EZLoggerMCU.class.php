<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

define('EZLOGGER_MCU_LEVEL_FATAL', 1);
define('EZLOGGER_MCU_LEVEL_ERROR', 2);
define('EZLOGGER_MCU_LEVEL_WARN', 3);
define('EZLOGGER_MCU_LEVEL_INFO', 4);
define('EZLOGGER_MCU_LEVEL_DEBUG', 5);
define('EZLOGGER_MCU_LEVEL_TRACE', 6);

/**
 *【EZLogger】ロガークラス
 *
 * <b>使い方</b>：<br />
 *
 * 1. フレームワーク内でロガークラスを生成
 * <code>
 * $config =& new EZConfig();
 * $logger =& EZLogger::getInstance($config);
 * if (PEAR::isError($logger)) {
 *     trigger_error($logger->getMessage(), E_USER_ERROR);
 * }
 * $logger->open();
 * </code>
 *
 * 2. フレームワークのサブクラス、メソッド内では以下を実行
 * <code>
 * $factory->logger->logging_mode($msg, [ __FILE__, __LINE__, $info ]);
 *                   ~~~~~~~~~~~~
 * </code>
 * ※logging_mode = fatal | error | warn | info | debug | trace
 *
 * // ログ出力メソッドに渡す引数：<br />
 * // $msg     : ログショートメッセージ（もしくは、出力オブジェクト）  <br />
 * // __FILE__ : このまま記述すればよい（勝手にファイル名が挿入される）<br />
 * // __LINE__ : このまま記述すればよい（勝手にライン番号が挿入される）<br />
 * // $info    : ログメッセージの詳細 <br />
 *
 * @access     public
 * @author     Kiyomizu Hiroyuki
 * @package    EZLib
 * @subpackage EZCore
 * @version    $Revision
 */
class EZLoggerMCU {

    /**
     * ログファイルポインター
     *
     * @access private
     * @var    resource
     */
    var $_fp = null;
    var $_err_fp = null;

    /**
     * ログファイル名
     *
     * @access private
     * @var    string
     */
    var $_log_file = null;

    /**
     * ログファイル分割期間
     *
     * @access private
     * @var    string
     */
    var $_log_span = "Ym";

    var $_log_type = 0;

    /**
     * Loggerオブジェクト
     *
     * @access private
     * @var    object  EZLogger
     */
    var $_logger = null;
    var $_flg = false;
    var $_err_flg = false;

    function __destruct() {
        if ($this->_flg) {
            if ($this->_err_flg) {
                $result = fwrite($this->_err_fp, "\n\n\n");
            } else {
                $result = fwrite($this->_fp, "\n\n\n");
            }
        }
    }

    /**
     * ログ出力
     *
     * @access private
     * @param  string  $msg  ログショートメッセージ
     * @param  mixed   $info 詳細メッセージ or デバッグ変数
     * @return
     */
    function _write($info = null, $msg = null) {
        static $stmp;
        static $error_count;
        $this->_flg = true;
        //init log_message
        $log_msg = null;
        // 呼び出し元
        $bt = debug_backtrace();
        $line = $bt[1]["line"];
        $file = $bt[1]["file"];
        $func = $bt[1]["function"];
        $msg = "[".$func."] ". $msg;
        //create log_message
        $timestamp = date("Y-m-d H:i:s", time());
        $session_id = session_id();
        $ip = ($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "";
        if (!defined('_EZSESSION_NAMESPACE')) define('_EZSESSION_NAMESPACE', 'ezlib.class');
        $user_id = (isset ($_SESSION[_EZSESSION_NAMESPACE]["user_info"]["user_id"])) ? $_SESSION[_EZSESSION_NAMESPACE]["user_info"]["user_id"] : "";
        $log_msg = "=====================\n[" . $timestamp . "]" .
        " - " . $msg .
        " - " . $ip .
        " - " . $user_id . ":" . $session_id . "\n" .
        $file .
        " - " . $bt[2]["class"]." : ".$bt[2]["function"] .
        " - " . $line . "\n";
        $mail_body = $log_msg;
        // エラー時にはトレースログを出す
        if ($func == "error" || $func == "fatal") {
            $log_msg .= "[[  DEBUG TRACE  ]]\n";
            $_val = null;
            foreach($bt as $key => $val) {
                $function = isset($bt[$key + 1]["function"]) ? $bt[$key + 1]["function"] : "";
                $log_msg .= $val["file"]." - [ ".$function." ] - ".$val["line"]."\n";
                if ($val["args"]) {
                    $log_msg .= print_r($val["args"], true);
                }
            }
            $log_msg .= "[[  REQUEST  ]]\n";
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                $log_msg .= print_r($_GET, true);
            } else {
                $log_msg .= print_r($_POST, true);
            }
            $log_msg .= "[[  SERVER  ]]\n";
            $log_msg .= print_r($_SERVER, true);
            $log_msg .= "[[  SESSION  ]]\n";
            $log_msg .= print_r($_SESSION, true);
            $log_msg .= "--\n";
        }
        //create additional_message
        if (!is_null($info)) {
            if (is_string($info)) {
                $log_msg .= "--\n" . $info . " \n--\n";
            } else {
                $log_msg .= "--\n" . print_r($info, true) . "\n--\n";
            }
        }
        // エラー時にはメールでレポートを出す
        if ((N2MY_ERROR_NOTIFICATION == 1) && ($func == "error" || $func == "fatal")) {
            // 送信先指定あり
            if (defined("N2MY_ERROR_FROM") && defined("N2MY_ERROR_TO") && N2MY_ERROR_FROM && N2MY_ERROR_TO) {
                // SMTPクラスはシングルトンにした方がいいかも
                if (!isset($stmp)) {
                    require_once("lib/EZLib/EZMail/EZSmtp.class.php");
                    $smtp = new EZSmtp(null, "", "UTF-8");
                }
                // １分間で３回以上は報告しない
                $err_notification_limit = N2MY_APP_DIR."var/err_notification_limit";
                if (file_exists($err_notification_limit)) {
                    $t = filemtime($err_notification_limit);
                    if ((time() - $t) > 60) {
                        $count = 0;
                    // reset
                    } else {
                        $count = file_get_contents($err_notification_limit);
                    }
                //initialize
                } else {
                    $count = 0;
                }
                if ($count < 3) {
                    $count++;
                    file_put_contents($err_notification_limit, $count);
                    $mail_body = "ミーティングのシステムエラーを検出しました。\n" .
                            "ログファイルから内容を確認して下さい。\n\n".
                            N2MY_LOCAL_URL."\n".
                            $this->_error_log_file."\n\n".
                            $mail_body;
                    if ($msg) {
                        $mail_body .= "--\n".print_r($info, true);
                    } else {
                        $mail_body .= "--\n".$msg."\n--\n".print_r($info, true);
                    }
                    $smtp->send2(N2MY_ERROR_FROM, N2MY_ERROR_TO, "EZLib Notification [ ".$func." ] ".$_SERVER["SERVER_NAME"]. " - ". $bt[2]["class"]." : ".$bt[2]["function"] ."() - " . $line , $mail_body);
                    $error_count++;
                }
            }
        }
        if ($func == "warn" || $func == "error" || $func == "fatal") {
            $this->_err_flg = true;
            $result = fwrite($this->_err_fp, $log_msg);
        } else {
            $result = fwrite($this->_fp, $log_msg);
        }
        return $log_msg;
    }

    /**
     * 初期化
     *
     * @access public
     * @param  object  &$config コンフィグオブジェクト（参照渡し）
     * @return boolean
     */
    function init(& $config) {
        //set params
        $this->_log_level = $config->get('LOGGER', 'log_level', EZLOGGER_LEVEL_ERROR);
        $this->_log_type = $config->get('LOGGER', 'log_type', 1);
        $log_dir = $config->get('LOGGER', 'log_dir', null);
        $log_span = $config->get('LOGGER', 'log_span', 'Ym');
        $log_prefix = $config->get('LOGGER_MCU', 'log_prefix', 'mcu');
        //remove last "/" from log_dir
        if (preg_match("/.+\/$/i", $log_dir)) {
            $pos = strrpos($log_dir, "/");
            $log_dir = substr($log_dir, 0, $pos);
        }
        //set log_file
        $this->_log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date($log_span, time()));
        $this->_error_log_file = sprintf("%s/%s_%s.err.log", $log_dir, $log_prefix, date($log_span, time()));
    }

    /**
     * ログオープン
     *
     * @access public
     * @param
     * @return
     */
    function open() {
        //open log_file
        if (!is_resource($this->_fp)) {
            if (!file_exists($this->_log_file)) {
                $result = touch($this->_log_file);
                if ($result == false) {
                    $err_msg = sprintf("[fatal_error]: create log_file failed. - %s - %s", __FILE__, __LINE__);
                    trigger_error($err_msg, E_USER_ERROR);
                    exit;
                }
                chmod($this->_log_file, 0777);
            }
            $this->_fp = fopen($this->_log_file, "a");
        }
        if (!is_resource($this->_err_fp)) {
            if (!file_exists($this->_error_log_file)) {
                $result = touch($this->_error_log_file);
                if ($result == false) {
                    $err_msg = sprintf("[fatal_error]: create log_file failed. - %s - %s", __FILE__, __LINE__);
                    trigger_error($err_msg, E_USER_ERROR);
                    exit;
                }
                chmod($this->_error_log_file, 0777);
            }
            $this->_err_fp = fopen($this->_error_log_file, "a");
        }
    }

    /**
     * ログクローズ
     *
     * @access public
     * @param
     * @return
     */
    function close() {
        //write log
        $this->trace("Log closed.", __FILE__, __LINE__, null);
        //put blank_lines
        if ($this->_log_level < EZLOGGER_LEVEL_FATAL) {
            if (is_resource($this->_fp)) {
                $result = fwrite($this->_fp, "\n\n\n");
                if ($result == false) {
                    $err_msg = sprintf("[fatal_error]: write log failed. - %s - %s", __FILE__, __LINE__);
                    trigger_error($err_msg, E_USER_ERROR);
                    exit;
                }
            }
            if (is_resource($this->_err_fp)) {
                $result = fwrite($this->_err_fp, "\n\n\n");
                if ($result == false) {
                    $err_msg = sprintf("[fatal_error]: write log failed. - %s - %s", __FILE__, __LINE__);
                    trigger_error($err_msg, E_USER_ERROR);
                    exit;
                }
            }
        }
        $this->_EZLogger();
    }

    /**
     * ロガーインスタンスを取得
     *
     * @access public
     * @param  object &$config コンフィグオブジェクト（参照渡し）
     * @return object
     */
    public static function getInstance($config = null) {
        //set static variable
        static $instance;
        //return instance if already exists
        if (isset ($instance)) {
            return $instance;
        }
        //create logger_object
        $instance = new EZLoggerMCU();
        //init
        $obj = $instance->init($config);
        if (PEAR :: isError($obj)) {
            return $obj;
        }
        //return logger_object
        return $instance;
    }

    /**
     * ログ出力（Level: 致命的）
     *
     * @access public
     * @param  mixed   $info 詳細メッセージ or デバッグ変数
     * @param  string  $msg  ログショートメッセージ
     * @return boolean
     */
    public function fatal($info = null, $msg = null) {
        //check log_level
        if ($this->_log_level >= EZLOGGER_LEVEL_FATAL) {
            return $this->_write($info, $msg);
        }
        return true;
    }

    /**
     * ログ出力（Level: エラー）
     *
     * @access public
     * @param  mixed   $info 詳細メッセージ or デバッグ変数
     * @param  string  $msg  ログショートメッセージ
     * @return boolean
     */
    public function error($info = null, $msg = null) {
        //check log_level
        if ($this->_log_level >= EZLOGGER_LEVEL_ERROR) {
            return $this->_write($info, $msg);
        }
        return true;
    }

    /**
     * ログ出力（Level: 警告）
     *
     * @access public
     * @param  mixed   $info 詳細メッセージ or デバッグ変数
     * @param  string  $msg  ログショートメッセージ
     * @return boolean
     */
    public function warn($info = null, $msg = null) {
        //check log_level
        if ($this->_log_level >= EZLOGGER_LEVEL_WARN) {
            return $this->_write($info, $msg);
        }
        return true;
    }

    /**
     * ログ出力（Level: ユーザー任意のメッセージ）
     *
     * @access public
     * @param  mixed   $info 詳細メッセージ or デバッグ変数
     * @param  string  $msg  ログショートメッセージ
     * @return boolean
     */
    public function info($info = null, $msg = null) {
        //check log_level
        if ($this->_log_level >= EZLOGGER_LEVEL_INFO) {
            return $this->_write($info, $msg);
        }
        return true;
    }

    /**
     * ログ出力（Level: デバッグ）
     *
     * @access public
     * @param  mixed   $info 詳細メッセージ or デバッグ変数
     * @param  string  $msg  ログショートメッセージ
     * @return boolean
     */
    public function debug($info = null, $msg = null) {
        //check log_level
        if ($this->_log_level >= EZLOGGER_LEVEL_DEBUG) {
            $this->_write($info, $msg);
        }
        return true;
    }

    /**
     * ログ出力（Level: 通知）
     *
     * @access public
     * @param  mixed   $info 詳細メッセージ or デバッグ変数
     * @param  string  $msg  ログショートメッセージ
     * @return boolean
     */
    public function trace($info = null, $msg = null) {
        //check log_level
        if ($this->_log_level >= EZLOGGER_LEVEL_TRACE) {
            $this->_write($info, $msg);
        }
        return true;
    }
}