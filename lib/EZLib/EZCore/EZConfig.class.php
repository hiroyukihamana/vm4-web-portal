<?php
/**
 *【EZConfig】コンフィグクラス
 *
 * <b>説明</b><br />
 *
 * <code>
 * </code>
 *
 * @access     public
 * @package    EZLib
 * @subpackage EZCore
 * @version    $Revision
 */
class EZConfig
{

    private static $singleton;
    var $_config = array();

    function __construct($config_file) {
        //check config_file
        if (!file_exists($config_file)) {
            trigger_error("#config_file not found -> $config_file", E_USER_ERROR);
        }
        if (!is_readable($config_file)) {
            trigger_error("#config_file not readable -> $config_file", E_USER_ERROR);
        }
        //parse ini_file
        $config = parse_ini_file($config_file, true);

        //set config
        if ($config) {
            $this->_config =& $config;
        } else {
            trigger_error("#unable to parse_config_file -> $config_file", E_USER_ERROR);
        }
    }

    public static function getInstance($config_file = null) {
        if (EZConfig::$singleton) {
            return EZConfig::$singleton;
        }
        EZConfig::$singleton = new EZConfig($config_file);
        return EZConfig::$singleton;
    }

    /**
     * コンフィグパラメータを取得
     *
     * @access public
     * @param  string $group   グループ要素名
     * @param  string $key     要素名
     * @param  string $default デフォルト値
     * @return mixed
     */
    function get($group, $key, $default = null)
    {
        if (isset($this->_config[$group][$key])) {
            return $this->_config[$group][$key];
        } else {
            return $default;
        }
    }

    /**
     * コンフィグパラメータをまとめて取得
     *
     * @access public
     * @param  string $group   グループ要素名
     * @param  string $default デフォルト値
     * @return mixed
     */
    function getAll($group, $default = null)
    {
        if (isset($this->_config[$group])) {
            return $this->_config[$group];
        } else {
            return $default;
        }
    }

    /**
     * コンフィググループ名を取得
     *
     * @access public
     * @param  string $default デフォルト値
     * @return mixed
     */
    function getGroups($default = null)
    {
        $groups = array();
        $groups = array_keys($this->_config);

        if ($groups) {
            return $groups;
        } else {
            return $default;
        }
    }

    /**
     * コンフィググループ内の各要素名を取得
     *
     * @access public
     * @param  string $group   グループ要素名
     * @param  string $default デフォルト値
     * @return mixed
     */
    function getGroupKeys($group, $default = null)
    {
        if (isset($this->_config[$group])) {
            $sub_groups = array_keys($this->_config[$group]);
            if ($sub_groups) {
                return $sub_groups;
            } else {
                return $default;
            }
        } else {
            return $default;
        }
    }

    /**
     * コンフィグパラメータを設定
     *
     * @access public
     * @param  string  $group  グループ要素名
     * @param  string  $key    要素名
     * @param  mixed   $value  設定値
     * @param  boolean $create 要素が存在しなければ生成するフラグ [ true:生成する ]
     * @return
     */
    function set($group, $key, $value, $create = true)
    {
        if (isset($this->_config[$group][$key])) {
            $this->_config[$group][$key] = $value;
        } else {
            if ($create == false) {
                return;
            } else {
                $this->_config[$group][$key] = $value;
            }
        }
    }

    /**
     * コンフィグパラメータをまとめて設定
     *
     * @access public
     * @param  string  $group  グループ要素名
     * @param  array   $pair   設定する要素と値のペア配列
     * @param  boolean $create 要素が存在しなければ生成するフラグ [ true:生成する ]
     * @return
     */
    function setAll($group, $pair, $create = true)
    {
        if (!isset($this->_config[$group])) {
            if ($create == false) {
                return;
            }
        }

        if (!is_array($pair)) {
            return;
        }

        foreach ($pair as $key => $value) {
            $this->_config[$group][$key] = $value;
        }
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
