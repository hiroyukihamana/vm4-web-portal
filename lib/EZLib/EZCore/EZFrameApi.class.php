<?php
require_once('lib/pear/PEAR.php');
require_once('lib/EZLib/EZCore/EZConfig.class.php');
require_once('lib/EZLib/EZCore/EZLogger.class.php');
require_once('lib/EZLib/EZCore/EZLogger2.class.php');
require_once('lib/EZLib/EZRequest/EZRequest.class.php');

class EZFrameApi
{
    var $config = null;
    var $logger = null;

    function EZFrameApi() {
        $this->config = EZConfig::getInstance(N2MY_CONFIG_FILE);
        $this->logger = EZLogger::getInstance($this->config);
        $this->logger->open();
        $this->logger2 = EZLogger2::getInstance($this->config);
        $this->logger2->open();
        $this->request =& new EZApiRequest();
        $this->_get_passage_time();
    }

    function _init()
    {
        $this->init();
    }

    /**
     * 初期化処理
     */
    function init() {
    }

    /**
     * 認証処理
     */
    function auth() {
    }

    /**
     * 準備処理 [ action_xxxxx() 処理の直前に行う処理 ]
     */
    function prepare() {
    }

    /**
     * エラー処理
     * @param  mixed   $error   エラーメッセージ
     */
    function error($error = null) {
    }

    /**
     * 終了処理
     */
    function dispose() {
    }

    /**
     * デフォルト描画処理
     */
    function default_view() {
    }

    /**
     * 各処理の実行
     */
    function _execAction() {
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__);
        $action_name = $this->getActionName();
        $this->logger->trace(__FUNCTION__."#action_name",__FILE__,__LINE__,$action_name);
        return $this->$action_name();
    }

    public function getActionName() {
        static $action_name;
        if ($action_name) {
            return $action_name;
        }
        //get action_xxxxx from request
        $request_action = null;
        foreach ($_REQUEST as $key => $value) {
            if (substr($key, 0, 7) == 'action_') {
                $request_action = $key  ;
                break;
            }
        }
        $action_name = "default_view";
        //get class_methods
        $methods = get_class_methods($this);
        //set requested action_name
        foreach ($methods as $method) {
            if (strtolower($method) == strtolower($request_action)) {
                $action_name = $method;
            }
        }
        return $action_name;
    }

    /**
     * メインメソッド
     */
    function execute() {
        //execute actions
        $error = $this->_init();
        // アプリケーションの初期処理
        $this->init();
        if ($error == null) {
            $error = $this->auth();
            if ($error == null) {
                $error = $this->prepare();
                if ($error == null) {
                    $error = $this->_execAction();
                }
            }
        }
        //action on error
        if ($error) {
            //call error_handler method
            $this->error($error);
        }
        //dispose
        $this->dispose();
        $alert_time = $this->config->get("N2MY", "alert_time", 1);
        $alert_memory = $this->config->get("N2MY", "alert_memory", 1);
        $passage_time = $this->_get_passage_time();
        $memory_usege = memory_get_peak_usage();
        if (($passage_time > $alert_time && $alert_time > 0) || $memory_usege > $alert_memory * 1024 * 1024) {
            $this->logger2->warn(array(
                "path"          => $_SERVER['SCRIPT_NAME'],
                "request"       => $GLOBALS["HTTP_RAW_POST_DATA"] ? $GLOBALS["HTTP_RAW_POST_DATA"] : $this->request->getAll(),
                "passage_time"  => $passage_time,
                "memory_usage"  => number_format($memory_usege / 1024 / 1024, 2) . " MB",
                )
            );
        }
        //done
        exit();
    }

    /**
     * 経過時間取得
     */
    function _get_passage_time() {
        static $start_time;
        if (!isset($start_time)) {
            list($micro, $sec) = split(" ", microtime());
            $start_time = $micro + $sec - 0.0001;
        }
        list($micro, $sec) = split(" ", microtime());
        $passage_time = ($micro + $sec) - $start_time;
        return $passage_time;
    }
}

// 指定地がNULLの時、デフォルト値を参照するようにしたいが影響範囲が大きいのでクラスを拡張
class EZApiRequest extends EZRequest {
    function get($keys, $default = null)
    {
        if (is_array($keys)) {
            $ra = array();
            foreach ($keys as $k) {
                if (isset($this->_request[$k])) {
                    $ra[$k] = $this->_request[$k];
                }
            }
            return $ra;
        } else {
            if (is_null($this->_request[$keys]) || $this->_request[$keys] === "") {
                return $default;
            } else {
                return $this->_request[$keys];
            }
        }
    }
}


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */