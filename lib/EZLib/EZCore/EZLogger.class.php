<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

define('EZLOGGER_LEVEL_FATAL', 1);
define('EZLOGGER_LEVEL_ERROR', 2);
define('EZLOGGER_LEVEL_WARN' , 3);
define('EZLOGGER_LEVEL_INFO' , 4);
define('EZLOGGER_LEVEL_DEBUG', 5);
define('EZLOGGER_LEVEL_TRACE', 6);

/**
 *【EZLogger】ロガークラス
 *
 * <b>使い方</b>：<br />
 *
 * 1. フレームワーク内でロガークラスを生成
 * <code>
 * $config =& new EZConfig();
 * $logger =& EZLogger::getInstance($config);
 * if (PEAR::isError($logger)) {
 *     trigger_error($logger->getMessage(), E_USER_ERROR);
 * }
 * $logger->open();
 * </code>
 *
 * 2. フレームワークのサブクラス、メソッド内では以下を実行
 * <code>
 * $factory->logger->logging_mode($msg, [ __FILE__, __LINE__, $info ]);
 *                   ~~~~~~~~~~~~
 * </code>
 * ※logging_mode = fatal | error | warn | info | debug | trace
 *
 * // ログ出力メソッドに渡す引数：<br />
 * // $msg     : ログショートメッセージ（もしくは、出力オブジェクト）  <br />
 * // __FILE__ : このまま記述すればよい（勝手にファイル名が挿入される）<br />
 * // __LINE__ : このまま記述すればよい（勝手にライン番号が挿入される）<br />
 * // $info    : ログメッセージの詳細 <br />
 *
 * @access     public
 * @author     Masayuki IIno <masayuki.iino@gmail.com>
 * @package    EZLib
 * @subpackage EZCore
 * @version    $Revision
 */
class EZLogger
{
// ----------------------------    vars   ---------------------------------

// {{{ -- vars

  /**
   * ログファイルポインター
   *
   * @access private
   * @var    resource
   */
  var $_fp = null;

  /**
   * ログメッセージライン
   *
   * @access private
   * @var    stirng
   */
  var $_lines = null;

  /**
   * ログ出力ディレクトリ
   *
   * @access private
   * @var    string
   */
  var $_log_dir = null;

  /**
   * ログファイル名
   *
   * @access private
   * @var    string
   */
  var $_log_file = null;

  /**
   * ログレベル
   *
   * @access private
   * @var    integer
   */
  var $_log_level = EZLOGGER_LEVEL_ERROR;

  /**
   * ログファイル分割期間
   *
   * @access private
   * @var    string
   */
  var $_log_span = "Ym";

  /**
   * ログ日付フォーマット
   *
   * @access private
   * @var    string
   */
  var $_log_timestamp = "Y-m-d H:i:s";

  /**
   * Loggerオブジェクト
   *
   * @access private
   * @var    object  EZLogger
   */
  var $_logger = null;

  /**
   * コンストラクタ
   *
   * @access public
   * @param
   * @return
   */
  function EZLogger()
  {
  }

  /**
   * ログ出力
   *
   * @access private
   * @param  string  $msg  ログショートメッセージ
   * @param  string  $file 実行ファイル名
   * @param  integer $line 実行ライン番号
   * @param  mixed   $info 詳細メッセージ or デバッグ変数
   * @return
   */
  function _write($msg, $file = null, $line = null, $info = null)
  {
    //init log_message
    $log_msg = null;

    //create log_message
    $timestamp = date($this->_log_timestamp, time());
        $session_id = session_id();
        $ip = ($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "";
        $user_id = (isset($_SESSION[_EZSESSION_NAMESPACE]["user_info"]["user_id"])) ? $_SESSION[_EZSESSION_NAMESPACE]["user_info"]["user_id"] : "";
        $log_msg   = "=====================\n[".$timestamp."]" .
                " - ".$msg.
                " - ".$ip.
                " - ".$user_id.":".$session_id."\n".
                $file.
                " - ".$line."\n";

    //create additional_message
    if (!is_null($info)) {
        if (is_string($info)) {
            $log_msg .= "--\n".$info." \n--\n";
          } else {
            $log_msg .= "--\n".print_r($info, true)."\n--\n";
          }
    }
    $result = fwrite($this->_fp, $log_msg);
  }

  /**
   * 初期化
   *
   * @access public
   * @param  object  &$config コンフィグオブジェクト（参照渡し）
   * @return boolean
   */
  function init(&$config)
  {
    //set params
    $this->_log_dir       = $config->get('LOGGER', 'log_dir'      , null);
    $this->_log_level     = $config->get('LOGGER', 'log_level'    , EZLOGGER_LEVEL_ERROR);
    $this->_log_span      = $config->get('LOGGER', 'log_span'     , 'Ym');
    $this->_log_timestamp = $config->get('LOGGER', 'log_timestamp', 'Y-m-d H:i:s');
    $this->_log_prefix    = $config->get('LOGGER', 'log_prefix'   , 'n2my');

    //remove last "/" from log_dir
    if (preg_match("/.+\/$/i", $this->_log_dir)) {
      $pos            = strrpos($this->_log_dir, "/");
      $this->_log_dir =  substr($this->_log_dir, 0, $pos);
    }

    //set log_file
    $this->_log_file = sprintf("%s/%s_%s.log", $this->_log_dir, $this->_log_prefix, date($this->_log_span, time()));

    //init error
    $error =& $this->_error;
    $error =  null;

    if ($error) {
      return PEAR::raiseError($error);
    }
    return true;
  }

  /**
   * ログオープン
   *
   * @access public
   * @param
   * @return
   */
    function open()
    {
        //open log_file
        if (!is_resource($this->_fp)) {
            if (!file_exists($this->_log_file)) {
                $result = touch($this->_log_file);
                if ($result == false) {
                    $err_msg  = sprintf("[fatal_error]: create log_file failed. - %s - %s", __FILE__, __LINE__);
                    trigger_error($err_msg, E_USER_ERROR);
                    exit;
                }
                chmod($this->_log_file, 0777);
            }
            $this->_fp = fopen($this->_log_file, "a");
        }
    }

  /**
   * ログクローズ
   *
   * @access public
   * @param
   * @return
   */
  function close() {
    //write log
    $this->trace("Log closed.", __FILE__, __LINE__, null);
    //put blank_lines
    if ($this->_log_level < EZLOGGER_LEVEL_FATAL) {
        if (is_resource($this->_fp)) {
            $result = fwrite($this->_fp, "\n\n\n");
            if ($result == false) {
                  $err_msg  = sprintf("[fatal_error]: write log failed. - %s - %s", __FILE__, __LINE__);
                  trigger_error($err_msg, E_USER_ERROR);
                  exit;
              }
        }
    }

    $this->_EZLogger();
  }

  /**
   * ロガーインスタンスを取得
   *
   * @access public
   * @param  object &$config コンフィグオブジェクト（参照渡し）
   * @return object
   */
  function & getInstance($config = null)
  {
    //set static variable
    static $instance;
    //return instance if already exists
    if (isset($instance)) {
      return $instance;
    }

    //create logger_object
    $instance = new EZLogger();

    //init
    $obj = $instance->init($config);
    if (PEAR::isError($obj)) {
      return $obj;
    }

    //return logger_object
    return $instance;
  }

  /**
   * ログ出力（Level: 致命的）
   *
   * @access public
   * @param  string  $msg  ログショートメッセージ
   * @param  string  $file 実行ファイル名
   * @param  integer $line 実行ライン番号
   * @param  mixed   $info 詳細メッセージ or デバッグ変数
   * @return boolean
   */
  function fatal($msg, $file = null, $line = null, $info = null)
  {
    //check log_level
    if ($this->_log_level >= EZLOGGER_LEVEL_FATAL) {

      //write log
      $msg = sprintf("[fatal]: %s", $msg);
      $this->_write($msg, $file, $line, $info);
    }

    return true;
  }

  /**
   * ログ出力（Level: エラー）
   *
   * @access public
   * @param  string  $msg  ログショートメッセージ
   * @param  string  $file 実行ファイル名
   * @param  integer $line 実行ライン番号
   * @param  mixed   $info 詳細メッセージ or デバッグ変数
   * @return boolean
   */
  function error($msg, $file = null, $line = null, $info = null)
  {
    //check log_level
    if ($this->_log_level >= EZLOGGER_LEVEL_ERROR) {

      //write log
      $msg = sprintf("[error]: %s", $msg);
      $this->_write($msg, $file, $line, $info);
    }
    return true;
  }

  /**
   * ログ出力（Level: 警告）
   *
   * @access public
   * @param  string  $msg  ログショートメッセージ
   * @param  string  $file 実行ファイル名
   * @param  integer $line 実行ライン番号
   * @param  mixed   $info 詳細メッセージ or デバッグ変数
   * @return boolean
   */
  function warn($msg, $file = null, $line = null, $info = null)
  {
    //check log_level
    if ($this->_log_level >= EZLOGGER_LEVEL_WARN) {

      //write log
      $msg = sprintf("[warn]: %s", $msg);
      $this->_write($msg, $file, $line, $info);
    }

    return true;
  }

  /**
   * ログ出力（Level: ユーザー任意のメッセージ）
   *
   * @access public
   * @param  string  $msg  ログショートメッセージ
   * @param  string  $file 実行ファイル名
   * @param  integer $line 実行ライン番号
   * @param  mixed   $info 詳細メッセージ or デバッグ変数
   * @return boolean
   */
  function info($msg, $file = null, $line = null, $info = null)
  {
    //check log_level
    if ($this->_log_level >= EZLOGGER_LEVEL_INFO) {

      //write log
      $msg = sprintf("[info]: %s", $msg);
      $this->_write($msg, $file, $line, $info);
    }

    return true;
  }

  /**
   * ログ出力（Level: デバッグ）
   *
   * @access public
   * @param  string  $msg  ログショートメッセージ
   * @param  string  $file 実行ファイル名
   * @param  integer $line 実行ライン番号
   * @param  mixed   $info 詳細メッセージ or デバッグ変数
   * @return boolean
   */
  function debug($msg, $file = null, $line = null, $info = null)
  {
    //check log_level
    if ($this->_log_level >= EZLOGGER_LEVEL_DEBUG) {

      //write log
      $msg = sprintf("[debug]: %s", $msg);
      $this->_write($msg, $file, $line, $info);
    }

    return true;
  }

  /**
   * ログ出力（Level: 通知）
   *
   * @access public
   * @param  string  $msg  ログショートメッセージ
   * @param  string  $file 実行ファイル名
   * @param  integer $line 実行ライン番号
   * @param  mixed   $info 詳細メッセージ or デバッグ変数
   * @return boolean
   */
  function trace($msg, $file = null, $line = null, $info = null)
  {
    //check log_level
    if ($this->_log_level >= EZLOGGER_LEVEL_TRACE) {

      //write log
      $msg = sprintf("[trace]: %s", $msg);
      $this->_write($msg, $file, $line, $info);
    }

    return true;
  }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
