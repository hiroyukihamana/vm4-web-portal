<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
/**
 *【EZXML】XMLクラス
 *
 * <b>説明</b><br />
 *
 * <code>
 * </code>
 *
 * @access     public
 * @author     Masayuki IIno <iino@vcube.co.jp>
 * @package    EZLib
 * @subpackage EZXML
 * @version    $Revision
 */
class EZXML
{
    /**
     * CDATA結合文字列
     *
     * @access private
     * @var    string
     */
    var $_cdata_joint = null;

    /**
     * CDATAホワイトスペース削除フラグ
     *
     * @access private
     * @var    boolean
     */
    var $_cdata_trim = true;

    /**
     * [テンポラリー] データスタック
     *
     * @access private
     * @var    array
     */
    var $_data_stack = array();

    /**
     * XML文字エンコーディング
     *
     * @access private
     * @var    string
     */
    var $_enc_in = 'UTF-8';

    /**
     * PHP配列エンコーディング
     *
     * @access private
     * @var    string
     */
    var $_enc_out = 'eucjp-win';

    /**
     * ノードパターン
     *
     * @access private
     * @var    array
     */
    var $_nodes = array();

    /**
     * ノードスタック [テンポラリー]
     *
     * @access private
     * @var    array
     */
    var $_node_stack = array();

    /**
     * XMLルートノード名
     *
     * @access private
     * @var    string
     */
    var $_root = null;

    /**
     * ルートスタック [テンポラリー]
     *
     * @access private
     * @var    array
     */
    var $_root_stack = array();

    /**
     * XMLパーサーオブジェクト
     *
     * @access private
     * @var    resource
     */
    var $_parser = null;

    /**
     * XML2ARRAYデータスタック
     *
     * @access private
     * @var    array
     */
    var $_stack = array();

    /**
     * ルートノード直下のトップノード名
     *
     * @access private
     * @var    string
     */
    var $_top_node = null;

    /**
     * コンストラクタ
     *
     * @access public
     * @param
     * @return
     */
    function EZXML()
    {
    }

    /**
     * キャラクターデータハンドラー
     *
     * @access private
     * @param  integer &$parser XMLパーサー (参照渡し)
     * @param  string   $cdata  キャラクターデータ
     * @return
     */
    function _cDataHandler(&$parser, $cdata)
    {
        $current_node = null;
        if (!$this->_nodes) {
            $current_node = $this->_root;
        } else {
            $num = count($this->_nodes) - 1;
            $current_node = $this->_nodes[$num];
        }

        $stack =& $this->_data_stack;

        if (!isset($stack[$current_node])) {
            $stack[$current_node] = null;
        }

        if (trim($cdata) !== "") {
            if ($this->_cdata_trim) {
                $cdata = trim($cdata);
            }

            if (is_null($stack[$current_node])) {
                $stack[$current_node] = $cdata;
            } else {
                $str = "%s" . $this->_cdata_joint . "%s";
                $stack[$current_node] = sprintf($str, $stack[$current_node], $cdata);
            }
        }
    }

    /**
     * 終了要素ハンドラー
     *
     * @access private
     * @param  integer &$parser XMLパーサー (参照渡し)
     * @param  string   $node   ノード名
     * @return
     */
    function _endElementHandler(&$parser, $node)
    {
        //set data structure
        $nodes =& $this->_nodes;
        $stack =& $this->_node_stack;
        $cdata =& $this->_data_stack;

        //return at root node
        if ($node === $this->_root) {
            $this->_root_stack[sprintf("%s_data", $node)]= $cdata[$node];
            return;
        }

        //handle cData
        foreach ($nodes as $idx => $key) {
            if (!isset($stack[$key])) {
                $stack[$key] = array();
            }

            $num =  count($stack[$key]);
            $num = (count($stack[$key])) ? ($num - 1) : 0 ;

            if ($key === $node) {
                if (isset($cdata[$node])) {
                    $stack[$key][$num]['_data'] = $cdata[$node];
                    unset($cdata[$node]);
                } else {
                    $stack[$key][$num]['_data'] = null;
                }

                array_pop($this->_nodes);
                break;
            }

            $stack =& $stack[$key][$num];
        }
    }

    /**
     * XMLデータをパースする
     *
     * @access private
     * @param  string &$data xmlデータ (参照渡し)
     * @return array
     */
    function _parse(&$data)
    {
        //get xml parse result
        $result = xml_parse($this->_parser, $data);

        if ($result === false) {
            $errmsg = sprintf("xml_data parse failed. -> [%s][%s]", __LINE__, __FILE__);
            return PEAR::raiseError($errmsg);
        }

        //merge stack
        $this->_stack =& $this->_root_stack;

        if ($this->_node_stack) {
            foreach ($this->_node_stack as $idx => $val) {
                $this->_stack[$this->_root][$idx] = $val;
            }
        }

        return $this->_stack;
    }

    /**
     * XMLパーサーを生成する
     *
     * @access private
     * @param
     * @return integer
     */
    function _parserCreate()
    {
        //create xml_parser
        $this->_parser = xml_parser_create($this->_enc_in);

        //set xml_parser options
        if ($this->_parser !== false) {
            xml_parser_set_option(         $this->_parser, XML_OPTION_CASE_FOLDING, 0);
            xml_parser_set_option(         $this->_parser, XML_OPTION_TARGET_ENCODING, $this->_enc_in);
            xml_set_object(                $this->_parser, $this);
            xml_set_element_handler(       $this->_parser, '_startElementHandler', '_endElementHandler');
            xml_set_character_data_handler($this->_parser, '_cDataHandler');
        }

        return $this->_parser;
    }

    /**
     * 開始要素ハンドラー
     *
     * @access private
     * @param  integer &$parser XMLパーサー (参照渡し)
     * @param  string   $node   ノード名
     * @param  array    $attr   属性値
     * @return
     */
    function _startElementHandler(&$parser, $node, $attr)
    {
        //set root node
        if (!$this->_root) {
            $this->_root              = $node;
            $this->_root_stack[$node] = array();
            $this->_root_stack[sprintf("%s_attr", $node)]= $attr;
            return;
        }

        //set node list
        if (!$this->_nodes) {
            $this->_top_node = $node;
            array_push($this->_nodes, $node);
        }

        if (!in_array($node, $this->_nodes)) {
            array_push($this->_nodes, $node);
        }

        //set data structure
        $nodes =& $this->_nodes;
        $stack =& $this->_node_stack;

        foreach ($nodes as $idx => $key) {
            if ($key === $node) {
                $stack[$key][]['_attr'] = $attr;
                break;
            }

            if (!isset($stack[$key])) {
                $stack[$key] = array();
            }

            $num   =  count($stack[$key]);
            $num   =  (count($stack[$key])) ? ($num - 1) : 0 ;
            $stack =& $stack[$key][$num];
        }
    }

    /**
     * XMLをPHP配列データへ変換する
     *
     * @access public
     * @param  string &$xml         XMLデータ (参照渡し)
     * @param  string  $cdata_joint CDATA結合文字列
     * @param  boolean $cdata_trim  CDATAホワイトスペース削除フラグ [true: 削除する | false: 削除しない]
     * @return array
     */
    function openXML(&$xml, $cdata_joint = "\n", $cdata_trim = true)
    {
        //create xml_parser
        $parser = $this->_parserCreate();
        if ($parser === false) {
            $errmsg = sprintf("xml_parser create failed. -> [%s][%s]", __LINE__, __FILE__);
            return PEAR::raiseError($errmsg);
        }

        //set cdata_joint
        $this->_cdata_joint = $cdata_joint;

        //set cdata_trim
        $this->_cdata_trim  = $cdata_trim;

        //get php_array
        $xml_array = array();
        $xml_array = $this->_parse($xml);

        //free xml parser
        xml_parser_free($this->_parser);
        $this->_parser = null;

        return $xml_array;
    }

    /**
     * PHP配列データをXMLへ変換する
     *
     * @access public
     * @param  array   &$data      PHP配列データ (参照渡し)
     * @param  integer  $level     レベル
     * @param  string   $prior_key 前のキー
     * @return string
     */
    function saveXML_sample(&$data, $level = 0, $prior_key = null)
    {
        if ($level == 0) {
            ob_start();
            echo('<?xml version="1.0" ?>');
        }

        while (list($key, $value) = each($data)) {
            if (!strpos($key, ' attr') ) {
                if (is_array($value) && array_key_exists(0, $value)) {
                    XML_serialize($value, $level, $key);
                } else {
                    $tag = ($prior_key) ? $prior_key : $key;
                    echo str_repeat("\t", $level), '<', $tag;

                    if (array_key_exists("$key attr", $data)) {
                        while (list($attr_name, $attr_value) = each($data["$key attr"])) {
                            echo ' ', $attr_name, '="', htmlspecialchars($attr_value), '"';
                        }
                        reset($data["$key attr"]);
                    }

                    if (is_null($value)) {
                        echo(" />\n");
                    } elseif (!is_array($value)) {
                        echo '>', htmlspecialchars($value), "</$tag>\n";
                    } else {
                        echo ">\n", XML_serialize($value, $level + 1), str_repeat("\t", $level), "</$tag>\n";
                    }
                }
            }
        }

        reset($data);

        if ($level == 0) {
            $str = &ob_get_contents();
            ob_end_clean();
            return $str;
        }
    }

    /**
     * XMLファイル、及び出力データの文字コードを設定する
     *
     * @access public
     * @param  string $enc_out PHP配列エンコーディング
     * @param  string $enc_in  XML文字エンコーディング
     * @return
     */
    function setEncodes($enc_out = 'eucjp-win', $enc_in = 'UTF-8')
    {
        //set output encoding
        $ithis->_enc_out = $enc_out;

        //set input encoding
        $this->_enc_in   = $enc_in;
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
