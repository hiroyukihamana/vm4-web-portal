<?php
class EZXML2
{
    var $parser;
    var $error_code;
    var $error_string;
    var $current_line;
    var $current_column;
    var $data = array();
    var $datas = array();
    var $root = null;

    function parse($data)
    {
        $this->parser = xml_parser_create('UTF-8');
        xml_set_object($this->parser, $this);
        xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);
        xml_set_element_handler($this->parser, 'tag_open', 'tag_close');
        xml_set_character_data_handler($this->parser, 'cdata');
        if (!xml_parse($this->parser, $data)) {
            $xml_data = array();
            $this->error_code = xml_get_error_code($this->parser);
            $this->error_string = xml_error_string($this->error_code);
            $this->current_line = xml_get_current_line_number($this->parser);
            $this->current_column = xml_get_current_column_number($this->parser);
        } else {
            list($root, $val) = each($this->data['child']);
            $xml_data[$root] = $this->_formatter($val[0]['child']);
            $xml_data[$root."_attr"] = $val[0]['attribs'];
            $xml_data[$root."_data"] = $val[0]['data'];
        }
        xml_parser_free($this->parser);
        return $xml_data;
    }

    function tag_open($parser, $tag, $attribs)
    {
        $this->data['child'][$tag][] = array('data' => '', 'attribs' => $attribs, 'child' => array());
        $this->datas[] =& $this->data;
        $this->data =& $this->data['child'][$tag][count($this->data['child'][$tag])-1];
    }

    function cdata($parser, $cdata)
    {
        $this->data['data'] .= trim($cdata);
    }

    function tag_close($parser, $tag)
    {
        $this->data =& $this->datas[count($this->datas)-1];
        array_pop($this->datas);
    }

    function openXML($xml, $cdata_joint = "\n", $cdata_trim = true)
    {
        return $this->parse($xml);
    }

    function _formatter($data) {
        $response = array();
        foreach($data as $key => $val) {
            foreach($val as $node_key => $node_val) {
                if ($node_val['child']) {
                    $child_node = $this->_formatter($node_val['child']);
                    $response[$key][$node_key] = $child_node;
                }
                $response[$key][$node_key]['_attr'] = $node_val['attribs'];
                $response[$key][$node_key]['_data'] = $node_val['data'];
            }
        }
        return $response;
    }
}