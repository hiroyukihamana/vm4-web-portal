<?php
require_once('lib/pear/PEAR.php');
require_once('lib/Smarty/Smarty.class.php');

class EZTemplate extends Smarty
{
	function __construct($config) {
        $this->template_dir      = $config['template_dir'];
        $this->cache_dir         = $config['cache_dir'];
        $this->compile_dir       = $config['compile_dir'];
        $this->plugins_dir       = isset($config['plugins_dir']) ? $config['plugins_dir'] : null;
        $this->config_dir        = isset($config['config_dir']) ? $config['config_dir'] : null;
        $this->default_modifiers = array('default:""');
	}

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
