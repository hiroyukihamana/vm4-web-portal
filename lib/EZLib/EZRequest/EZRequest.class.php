<?php
/**
 *【EZRequest】リクエストクラス
 *
 * <b>説明</b><br />
 *
 * <code>
 * </code>
 *
 * @access     public
 * @package    EZLib
 * @subpackage EZRequest
 * @version    $Revision
 */
class EZRequest
{
    /**
     * リクエストメソッド
     *
     * @access private
     * @var    string
     */
    var $_method = null;

    /**
     * リクエストパラメータ
     *
     * @access private
     * @var    reference
     */
    var $_request = null;

    /**
     * コンストラクタ
     *
     * @access public
     * @param
     * @return
     */
    function EZRequest()
    {
        //set method
        $this->_setMethod();

        //init request
        if (preg_match("/^post$/i", $this->_method)) {
            $this->_request =& $_POST;
        } else {
            $this->_request =& $_GET;
        }

        //trim white_space
        foreach ($this->_request as $idx => $val) {
            if (is_string($val)) {
                $this->_request[$idx] = trim($val);
            } else {
                $this->_request[$idx] = $val;
            }
        }
    }

    /**
     * リクエストパラメータを取得（GET | POST）
     *
     * @access public
     * @param  mixed  $keys    要素名（要素名を配列で渡すことにより複数指定可能）
     * @param  string $default デフォルト値
     * @return mixed
     */
    function get($keys, $default = null)
    {
        if (is_array($keys)) {
            $ra = array();
            foreach ($keys as $k) {
                if (isset($this->_request[$k])) {
                    $ra[$k] = $this->_request[$k];
                }
            }
            return $ra;

        } else {
            if (isset($this->_request[$keys])) {
                return $this->_request[$keys];
            } else {
                return $default;
            }
        }
    }

    /**
     * リクエストパラメータを全て取得（GET | POST）
     *
     * @access public
     * @param
     * @return array
     */
    function getAll()
    {
        return $this->_request;
    }

    /**
     * リクエストパラメータの要素名をすべて取得（GET | POST）
     *
     * @access public
     * @param
     * @return array
     */
    function getKeys()
    {
        if ($this->_request) {
            return array_keys($this->_request);
        } else {
            return array();
        }
    }

    /**
     * リクエストパラメータに指定された要素名が存在するかチェック
     *
     * @access public
     * @param  string  $key 要素名
     * @return boolean
     */
    function has($key)
    {
        if (isset($this->_request[$key])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * リクエストパラメータを削除
     *
     * @access public
     * @param  string $key 要素名
     * @return
     */
    function remove($key)
    {
        if (isset($this->_request[$key])) {
            unset($this->_request[$key]);
        }
    }

    /**
     * リクエストパラメータを全て削除
     *
     * @access public
     * @param
     * @return
     */
    function removeAll()
    {
        $this->_request = array();
    }

    /**
     * リクエストパラメータを設定
     *
     * @access public
     * @param  string $key   要素名
     * @param  mixed  $value 値
     * @return
     */
    function set($key, $value)
    {
        if (is_string($value)) {
            $this->_request[$key] = trim($value);
        } else {
            $this->_request[$key] = $value;
        }
    }

    /**
     * リクエストパラメータをまとめて設定
     *
     * @access public
     * @param  array   &$data 要素名と値のペア配列（参照渡し）
     * @param  boolean  $mode モード（true：既存のパラメータに上書き | false：設定値で入れ替え）
     * @return
     */
    function setAll(&$data, $mode = false)
    {
        if ($mode == true) {
            $this->_request = $data;
        } else {
            $this->_request = array_merge($this->_request, $data);
        }

        //trim white_space
        foreach ($this->_request as $idx => $val) {
            if (is_string($val)) {
                $this->_request[$idx] = trim($val);
            } else {
                $this->_request[$idx] = $val;
            }
        }
    }

    function getCookie($key, $default = null) {
        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        } else {
            return $default;
        }
    }

    function setCookie($key, $value, $expire = null, $path = null, $domain = null) {
        if (!$expire) {
            $expire = time()+60*60*24*30;
        }
        setcookie($key, $value, $expire);
    }

    private function _setMethod()
    {
        //set request_method
        if (!$this->_method) {
            $this->_method = $_SERVER['REQUEST_METHOD'];
        }
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
