<?php
class Transit_val extends Transit_common {

    var $config = null;

    /**
     * 駅名検索
     *
     * @param string $station_name 駅名
     * @param boolean $suffix true:前後方一致、false:前方一致
     * @return array $station_list
     */
    function getStationList($station_name, $suffix = true, $max_length = null) {

        // 後方一致検索
        if ($suffix == true) {
            $station_name = "?".$station_name;
        }
        $param = array(
            "val_htmb" => "jcgi_station",
            "val_in_name" => $station_name,
            "val_stationonly" => 1,
            );
        $contents = $this->api_execute("exp.cgi", $param, "SJIS");
        $data = $this->_parse($contents, $param["val_htmb"]);
        $list = array();
        for($i = 1; $i <= $data["val_stn_cnt"]; $i++) {
            $name = $data["val_stn_name".$i];
            $list[$name] = $name;
            if (is_int($max_length) && ($i > $max_length)) break;
        }
        return $list;
    }

    /**
     * 出発駅、到着駅を指定して、検索したルートを返す。
     *
     * @param string $station_to 始点
     * @param string $station_from 終点
     * @option mixed $option オプション
     *        "output"    => "detail",                // 出力結果 => summary:経路無し / detail:経路有り(デフォルト) / dia:時間指定
     *        "date_mode" => "start",                 // 検索条件(outputにdia指定時のみ) => begin:始発 / start:出発時刻 / end:到着時刻(デフォルト) / last:終電
     *        "date_time" => date("Y-m-d H:i:s"),        // 検索日時(outputにdia指定時は時間指定が有効)
     *        "sort"      => "time",                    // 優先度 => time:時間(デフォルト) / fare:料金 / transit (乗り換え) / CO2
     *        "count"     => 1,                        // 取得件数(デフォルト5件)
     */
    function getStationRoute($station_to, $station_from, $_option = null) {
        $option = array(
            "output"    => ($_option["output"]) ? $_option["output"] : "detail",
            "date_mode" => ($_option["date_mode"]) ? $_option["date_mode"] : "start",
            "date_time" => ($_option["date_time"]) ? $_option["date_time"] : date("Y-m-d H:i:s"),
            "sort"      => ($_option["sort"]) ? $_option["sort"] : "time",
            "count"     => ($_option["count"]) ? $_option["count"] : 5,
            );
        $param = array(
            "val_from" => $station_to,
            "val_to"   => $station_from,
            );
        // 詳細検索
        switch ($option["output"]) {
            // 時間指定な経路詳細情報取得
            case "dia":
                $param["val_htmb"] = "jcgi_diadetails";
                $param["val_hour"] = date("h", strtotime($option["date_time"]));
                $param["val_minute"] = date("i", strtotime($option["date_time"]));
                switch ($option["date_mode"]) {
                    case "start" :
                        $param["val_searchtype"] = 1;
                        break;
                    case "end" :
                        $param["val_searchtype"] = 2;
                        break;
                    default :
                        $param["val_searchtype"] = 1;
                        break;
                }
                break;
            // 時間を指定しないで経路間の詳細取得
            case "detail":
                $param["val_htmb"] = "jcgi_details2";
                break;
            // サマリー取得
            default :
                $param["val_htmb"] = "jcgi_summary";
        }
        // 件数
        if ($option["count"]) {
            $param["val_max_result"] = $option["count"];
        }
        // ソート
        if ($option["sort"]) {
            switch($option["sort"]) {
                case "time":
                    $param["val_sorttype"] = 3;
                    break;
                case "fare":
                    $param["val_sorttype"] = 2;
                    break;
                case "co2":
                    $param["val_sorttype"] = 6;
                    break;
                default :
                    $param["val_sorttype"] = 3;
                    break;
            }
        }
        $param["val_co2mode"] = 1;
        $contents = $this->api_execute("exp.cgi", $param, "SJIS");
        $data = $this->_parse($contents, $param["val_htmb"]);
        $route_list = $this->_formatStationRoute($data);
        return $route_list;
    }

    /**
     * 複数拠点検索
     * 集合場所が指定されていない場合は最短ルート検索を検索
     * （ただし、その場合は拠点数に制限あり）
     *
     * @param array $station_list 駅コードリスト
     * @param string $end_place 集合地点
     * @param string $sort ソート順
     *
     */
    function getMultiStationRoute($station_list, $end_place = "", $sort = "time", $detail = false) {
        // 集合場所が決まっていない場合は、自動検索
        if (!$end_place) {
            return $this->getStationShortestRoute($station_list, $sort, $detail);
        } else {
            $exec_time = microtime(true);
            $exec_cnt   = 0;
            $total_move = 0;
            $total_time = 0;
            $total_fare = 0;
            $total_co2  = 0;
            // 検索条件
            $option = array(
                "sort"    => $sort,
                "count"    => 1,
                );
            $route_list = array();
            foreach($station_list as $start_place) {
                $move = 0;
                $time = 0;
                $fare = 0;
                $co2  = 0;
                // 検索済み
                if ($end_place == $start_place) {
                } elseif (isset($route_list[$end_place][$start_place])) {
                    $move = $route_list[$end_place][$start_place]["summary"]["move"]["move"];
                    $time = $route_list[$end_place][$start_place]["summary"]["move"]["minute"];
                    $fare = $route_list[$end_place][$start_place]["summary"]["fare"]["fareunit"]["unit"];
                    $co2  = $route_list[$end_place][$start_place]["summary"]["move"]["co2"];
                } else {
                    $route = $this->getStationRoute($start_place, $end_place, $option);
                    $this->logger->info(__FUNCTION__,__FILE__,__LINE__,microtime(true) - $exec_time);
                    $exec_cnt++;
                    $move = $route[0]["summary"]["move"]["move"];
                    $time = $route[0]["summary"]["move"]["minute"];
                    $fare = $route[0]["summary"]["fare"]["fareunit"]["unit"];
                    $co2  = $route[0]["summary"]["move"]["co2"];
                    $route_list[$end_place][$start_place] = $route[0];
                }
                // 統計情報
                $total_move += $move;
                $total_time += $time;
                $total_fare += $fare;
                $total_co2  += $co2;
            }
            $summary[$end_place] = array(
                "move" => $total_move,
                "time" => $total_time,
                "fare" => $total_fare,
                "co2"  => $total_co2
            );
            $short_route = array(
                "shortest_route" => array(
                    "total" => array(
                        "move" => $total_move,
                        "time" => $total_time,
                        "fare" => $total_fare,
                        "co2"  => $total_co2,
                        ),
                    "end"   => $end_place,
                    "start" => $route_list[$end_place]
                    )
            );
            $short_route["total"]["exec_time"] = microtime(true) - $exec_time;
            $short_route["total"]["exec_count"] = $exec_cnt;
            // せっかく全ルートを検索したので、全ルートを渡す
            if ($detail == true) {
                foreach($route_list as $end_place => $routes) {
                    $short_route["routes"][] = array(
                        "total" => $summary[$end_place],
                        "end"   => $end_place,
                        "start" => $routes
                    );
                }
            }
            return $short_route;
        }
    }


    /**
     * 複数拠点検索（人数設定あり）
     *
     *
     *
     * @param array $station_list 駅コードリスト
     * @param string $end_place 集合地点
     * @param string $sort ソート順
     *
     */
    function getMultiStationRouteWithNum($station_list, $end_place = "", $sort = "time", $detail = false) {
        // 集合場所が決まっていない場合は、自動検索
        if (!$end_place) {
            return $this->getStationShortestRoute($station_list, $sort, $detail);
        } else {
            $exec_time = microtime(true);
            $exec_cnt   = 0;
            $total_move = 0;
            $total_time = 0;
            $total_fare = 0;
            $total_co2  = 0;
            // 検索条件
            $option = array(
                "sort"    => $sort,
                "count"    => 1,
                );
            $route_list = array();
            //foreach($station_list as $start_place) {
            for($i = 0; $i < count($station_list); $i++) {
                $start_place = $station_list[$i]["name"];
                $num = $station_list[$i]["num"];
                $move = 0;
                $time = 0;
                $fare = 0;
                $co2  = 0;
                // 検索済み
                if ($end_place == $start_place) {
                } elseif (isset($route_list[$end_place][$start_place])) {
                    $move = $route_list[$end_place][$start_place]["summary"]["move"]["move"];
                    $time = $route_list[$end_place][$start_place]["summary"]["move"]["minute"];
                    $fare = $route_list[$end_place][$start_place]["summary"]["fare"]["fareunit"]["unit"];
                    $co2  = $route_list[$end_place][$start_place]["summary"]["move"]["co2"];
                } else {
                    $route = $this->getStationRoute($start_place, $end_place, $option);
//                    $this->logger->info(__FUNCTION__,__FILE__,__LINE__,microtime(true) - $exec_time);
                    $exec_cnt++;
                    $move = $route[0]["summary"]["move"]["move"];
                    $time = $route[0]["summary"]["move"]["minute"];
                    $fare = $route[0]["summary"]["fare"]["fareunit"]["unit"];
                    $co2  = $route[0]["summary"]["move"]["co2"];
                    $route_list[$end_place][$start_place] = $route[0];
                }
                // 統計情報
                $total_move += $move * $num;
                $total_time += $time * $num;
                $total_fare += $fare * $num;
                $total_co2  += $co2 * $num;
            }
            $summary[$end_place] = array(
                "move" => $total_move,
                "time" => $total_time,
                "fare" => $total_fare,
                "co2"  => $total_co2
            );
            $short_route = array(
                "shortest_route" => array(
                    "total" => array(
                        "move" => $total_move,
                        "time" => $total_time,
                        "fare" => $total_fare,
                        "co2"  => $total_co2,
                        ),
                    "end"   => $end_place,
                    "start" => $route_list[$end_place]
                    )
            );
            $short_route["total"]["exec_time"] = microtime(true) - $exec_time;
            $short_route["total"]["exec_count"] = $exec_cnt;
            // せっかく全ルートを検索したので、全ルートを渡す
            if ($detail == true) {
                foreach($route_list as $end_place => $routes) {
                    $short_route["routes"][] = array(
                        "total" => $summary[$end_place],
                        "end"   => $end_place,
                        "start" => $routes
                    );
                }
            }
            return $short_route;
        }
    }

    /**
     * 複数駅拠点間の最短集合場所、及び各拠点からのルート情報、料金・距離・時間の集計情報を取得する
     *
     * @param array $station_list 駅コードを配列で指定
     * @param string $sort ソート順 ( time:時間 / fare:料金 / co2:co2 / move: 距離 )
     * @param boolean $detail 全ルートの結果を返す
     * @return mixed array(
     *            // 最短ルート
     *            "short_route" => array(
     *                "total" => array(
     *                    "move"   => 総距離(Km)
     *                    "time" => 総時間(分)
     *                    "fare" => 総料金(円)
     *                    "co2"  => 総CO2(Kg)
     *                    ),
     *                "end_point" => 集合場所
     *                "routes" => 各拠点からのルート情報
     *                ),
     *            "routes" => 全てのルート情報
     *        )
     */
    function getStationShortestRoute($station_list, $sort = "time", $detail = false) {
        // 2拠点以上、50拠点以下
        if (count($station_list) > 1 && count($station_list) < 50) {
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$station_list);
            $exec_time = microtime(true);
            $shortest_value = 0;
            $exec_cnt = 0;
            $transit_list = array();
            $short_route = array();
            // 出発駅
            foreach ($station_list as $i => $end_place) {
                $total_move = 0;
                $total_time = 0;
                $total_fare = 0;
                $total_co2  = 0;
                // 終点が既に検証済みなら無視
                if (!$transit_list[$end_place])  {
                    // 到着駅
                    foreach ($station_list as $j => $start_place) {
                        $start_place = $station_list[$j];
                        $move = 0;
                        $time = 0;
                        $fare = 0;
                        $co2  = 0;
                        // 自分の駅は無視
                        if ($i != $j) {
                            // 同一駅名は無視
                            if ($end_place == $start_place) {
                                $transit_list[$end_place][$start_place] = array(
                                    "move" => $move,
                                    "time" => $time,
                                    "fare" => $fare,
                                    "co2"  => $co2,
                                );
                            // 検索済み
                            } elseif (isset($transit_list[$end_place][$start_place])) {
                                $move = $transit_list[$end_place][$start_place]["summary"]["move"]["move"];
                                $time = $transit_list[$end_place][$start_place]["summary"]["move"]["minute"];
                                $fare = $transit_list[$end_place][$start_place]["summary"]["fare"]["fareunit"]["unit"];
                                $co2  = $transit_list[$end_place][$start_place]["summary"]["move"]["co2"];
                                $transit_list[$end_place][$start_place]["summary"]["cnt"]++;
//                                $this->logger->info(__FUNCTION__,__FILE__,__LINE__, $transit_list[$end_place][$start_place]);
                            // 未検索
                            } else {
                                $option = array(
                                    "sort"    => $sort,
                                    "count"    => 1,
                                    );
                                $route = $this->getStationRoute($start_place, $end_place, $option);
//                                $this->logger->info(__FUNCTION__,__FILE__,__LINE__, array($start_place, $end_place, microtime(true) - $exec_time));
                                $exec_cnt++;
                                $move = $route[0]["summary"]["move"]["move"];
                                $time = $route[0]["summary"]["move"]["minute"];
                                $fare = $route[0]["summary"]["fare"]["fareunit"]["unit"];
                                $co2  = $route[0]["summary"]["move"]["co2"];
                                $route[0]["summary"]["cnt"] = 1;
                                $transit_list[$end_place][$start_place] = $route[0];
                            }
                            // 統計情報
                            $total_move += $move;
                            $total_time += $time;
                            $total_fare += $fare;
                            $total_co2  += $co2;
                        }
                        $summary[$end_place] = array(
                            "move" => $total_move,
                            "time" => $total_time,
                            "fare" => $total_fare,
                            "co2"  => $total_co2
                        );
                    }
                    switch ($sort) {
                        case "time":
                            $total_sort = $total_time;
                            break;
                        case "fare":
                            $total_sort = $total_fare;
                            break;
                        case "co2":
                            $total_sort = $total_co2;
                            break;
                        case "move":
                            $total_sort = $total_move;
                            break;
                        default:
                            $sort = "time";
                            $total_sort = $total_time;
                            break;
                    }
                    // 最短ルート
                    if (($shortest_value == 0) || ($shortest_value > $total_sort)) {
                        $shortest_value = $total_sort;
                        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array($shortest_value, $total_sort));
                        $short_route = array(
                            "shortest_route" => array(
                                "total" => $summary[$end_place],
                                "end"   => $end_place,
                                "start" => $transit_list[$end_place]
                                )
                        );
                    }
                }
            }
            // せっかく全ルートを検索したので、全ルートを渡す
            if ($detail == true) {
                foreach($transit_list as $end_place => $routes) {
                    $short_route["routes"][] = array(
                        "total" => $summary[$end_place],
                        "end"   => $end_place,
                        "start" => $routes
                        );
                }
            }
            $short_route["total"]["exec_time"] = microtime(true) - $exec_time;
            $short_route["total"]["exec_count"] = $exec_cnt;
            return $short_route;
        } else {
            return false;
        }
    }

    //
    /**
     * 経路検索のフォーマット整形
     *
     */
    private function _formatStationRoute($data) {
//        print "<pre>";
//        print_r($data);
        // 経路リスト生成
        $route_list = array();
        for($r = 1; $r <= $data["val_route_cnt"]; $r++) {
            $_r = "val_r".$r."_";
            $total_move = 0;
            $total_time = 0;
            $time = 0;
            $line = $_r."line_";
            for ($l = 1; $l <= $data[$line."cnt"]; $l++) {
                // 経路情報生成
                $from = array(
                    "name" => $data[$line."from_".$l],
                    );
                $to = array(
                    "name" => $data[$line."to_".$l],
                    );
                // 距離
                $move  = $data[$line."dist_".$l] / 10;
                $total_move += $move;
                // 時間
                $time = $data[$line."time_".$l];
                // co2
                $co2 = $data[$line."co2_".$l];
                $total_co2 += $co2;
                $move = array(
                    "minute" => $time,
                    "move" => $move,
                    "co2" => $co2,
                    );
                $fare = array(
                    "fareunit" => array("unit" => 0),
                );
                $route[$l] = array(
                    "from" => $from,
                    "to" => $to,
                    "move" => $move,
                    "fare" => $fare);
            }
            // 合計時間
            $total_time += $data["val_time_".$r];
            // 料金
            for($i = 1; $i <= $data[$_r."fsect_cnt"]; $i++) {
                $route[$data[$_r."fsect_from_".$i]]["fare"] = array(
                    "fareunit" => array(
                        "unit" => $data[$_r."fsect_fare_".$i]),
                );
            }
            for($i = 1; $i <= $data[$_r."csect_cnt"]; $i++) {
                $route[$data[$_r."csect_from_".$i]]["fare"]["fareunit"]["unit"] += $data[$_r."csect_surcharge_".$i."_fare_1"];
            }
            $_fare = $data["val_fare_".$r] + $data["val_surcharge_fare_".$r];
            $fare = array(
                "fareunit" => array("unit" => $_fare),
            );
            // 経路情報生成
            $from = array(
                "name" => $data[$line."from_1"],
                );
            $to = array(
                "name" => $data[$line."to_".$data[$line."cnt"]],
                );
            $move = array(
                "minute" => $total_time,
                "move" => $total_move,
                "co2" => $data["val_co2_".$r],
                "transit_cnt" => $data["val_transfer_".$r],
                );
            $summary = array(
                "from" => $from,
                "to"   => $to,
                "move" => $move,
                "fare" => $fare);
            $route_list[] =
                array(
                    "summary" => $summary,
                    "route" => $route
                    );
        }
        return $route_list;
    }

    /**
     * パースを共通化とログ出力
     */
    function _parse($contents, $type) {
        $params = split('&', $contents);
        foreach($params as $param) {
            list($key, $val) = split("=", $param);
            $data[$key] = $val;
        }
        // パラメタ取得エラー
        if ($contents == "" || $data["val_errcode"] != 0 || $data["val_connect_errcode"] != 0) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$data);
        } else {
            $this->setlog($type);
        }
        return $data;
    }
}