<?php
require_once("lib/pear/PEAR.php");
class Transit_common extends PEAR {

    var $config = null;
    var $logger = null;
    var $transit_log = null;

    /**
     * コンストラクタ
     */
    function __construct($url_parts, $log = null) {
        $this->logger = EZLogger::getInstance();
        $this->config = $url_parts;
        $this->transit_log    = $log;
    }

    function setlog($type) {
        if (is_file($this->transit_log)) {
            $transit_log = unserialize(file_get_contents($this->transit_log));
        }
        $ym = date("Y-m");
        if (isset($transit_log[$ym][$type])) {
            $transit_log[$ym][$type]++;
        } else {
            $transit_log[$ym][$type] = 1;
        }
        if ($this->transit_log) {
            file_put_contents($this->transit_log, serialize($transit_log));
        }
    }

    function getlog() {
        if (is_file($this->transit_log)) {
            $transit_log = unserialize(file_get_contents($this->transit_log));
        }
        return $transit_log;
    }

    /**
     * 駅名検索
     *
     * @param string $station_name 駅名
     * @param boolean $suffix true:前後方一致、false:前方一致
     * @return array $station_list
     */
    function getStationList($station_name, $suffix = true, $max_length = null) {
        return array();
    }

    /**
     * 出発駅、到着駅を指定して、検索したルートを返す。
     *
     * @param string $station_to 始点
     * @param string $station_from 終点
     * @option mixed $option オプション
     *        "output"    => "detail",                // 出力結果 => summary:経路無し / detail:経路有り(デフォルト) / dia:時間指定
     *        "date_mode" => "start",                 // 検索条件(outputにdia指定時のみ) => begin:始発 / start:出発時刻 / end:到着時刻(デフォルト) / last:終電
     *        "date_time" => date("Y-m-d H:i:s"),        // 検索日時(outputにdia指定時は時間指定が有効)
     *        "sort"      => "time",                    // 優先度 => time:時間(デフォルト) / fare:料金 / move:距離
     *        "count"     => 1,                        // 取得件数(デフォルト5件)
     */
    function getStationRoute($station_to, $station_from, $_option = null) {
        return array();
    }

    /**
     * 複数拠点検索
     * 集合場所が指定されていない場合は最短ルート検索を検索
     * （ただし、その場合は拠点数に制限あり）
     *
     * @param array $station_list 駅コードリスト
     * @param string $end_place 集合地点
     * @param string $sort ソート順
     *
     */
    function getMultiStationRoute($station_list, $end_place = "", $sort = "time") {
        return array();
    }

    /**
     * 複数駅拠点間の最短集合場所、及び各拠点からのルート情報、料金・距離・時間の集計情報を取得する
     *
     * @param array $station_list 駅コードを配列で指定
     * @param string $sort ソート順 ( time:時間 / fare:料金 / co2:co2 / move: 距離 )
     * @param boolean $detail 全ルートの結果を返す
     * @return mixed array(
     *            // 集計情報
     *            "total" => array(
     *                "km"   => 総距離(Km)
     *                "time" => 総時間(分)
     *                "fare" => 総料金(円)
     *                "co2"  => 総CO2(Kg)
     *                ),
     *            // 最短ルート
     *            "short_route" => array(
     *                "end_point" => 集合場所
     *                "routes" => 各拠点からのルート情報
     *                ),
     *            "routes" => 全てのルート情報
     *        )
     */
    function getStationShortestRoute($station_list, $sort = "time", $detail = false) {
        return array();
    }

    /**
     * APIを実行し、データ取得(でも、HTTPで有ることが前提になってます。SOAPとかいやだな・・・。)
     *
     */
    function api_execute($file, $param, $encode = "") {
        // クエリ生成
        $query_str = "";
        foreach ($param as $_key => $_value) {
            if ($encode) {
                $_value = mb_convert_encoding($_value, $encode, "UTF-8");
            }
            urlencode($_value);
            $query_str .= "&".$_key."=".urlencode($_value);
        }
        $url = "http://";
        $url .= $this->config["host"];
        if (isset($this->config["port"])) {
            $url .= ":".$this->config["port"];
        }
        if (isset($this->config["path"])) {
            $url .= $this->config["path"];
        }
        $url .= $file;
        if ($query_str) {
            $url .= "?".substr($query_str, 1);
        }
//        print $url;
        // 実行
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 10,
            );
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$url);
        curl_setopt_array($ch, $option);
        $contents = curl_exec($ch);
        if ($encode) {
            $contents = mb_convert_encoding($contents, "UTF-8", $encode);
        }
        curl_close($ch);
        return $contents;
    }
}
?>