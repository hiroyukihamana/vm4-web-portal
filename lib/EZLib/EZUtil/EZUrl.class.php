<?php
/*
 * Created on 2006/12/08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("lib/EZLib/EZCore/EZLogger.class.php");

class EZUrl {

    var $logger = null;

    function __construct()
    {
        $this->logger = EZLogger::getInstance();
    }

    /**
     *
     */
    function wget($url) {
        $_url = parse_url($url);
        if ($_url["scheme"] == "https") {
            $url = "http://";
            if ($_url["user"]) {
                $url .= $_url["user"].":";
            }
            if ($_url["pass"]) {
                $url .= $_url["pass"]."@";
            }
            $url .= $_url["host"].$_url["path"];
            if ($_url["query"]) {
                $url .= "?".$_url["query"];
            }
        }
        $contents = "";
        $this->logger->trace(__FUNCTION__."#wget url", __FILE__, __LINE__, $url);
        $fp = @fopen($url, "r");
        if ($fp == false) {
            $this->logger->error(__FUNCTION__."#URL doesn't exist.", __FILE__, __LINE__, $url);
            return false;
        } else {
            while (!feof($fp)) {
                $contents .= fread($fp, 1024);
            }
            fclose($fp);
        }
        return $contents;
    }
}
?>
