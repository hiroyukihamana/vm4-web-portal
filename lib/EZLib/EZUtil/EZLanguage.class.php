<?php
class EZLanguage {
    // 言語コードの順引き用
    private static $lang_list = array(
        "af" => "af",
        "ar" => "ar",
        "bg" => "bg",
        "ca" => "ca",
        "cs" => "cs",
        "cy" => "cy",
        "da" => "da",
        "de" => "de",
        "el" => "el",
        "en" => "en_US",
        "es" => "es",
        "et" => "et",
        "fa" => "fa",
        "fi" => "fi",
        "fr" => "fr_FR",
        "ga" => "ga",
        "gl" => "gl",
        "he" => "he",
        "hi" => "hi",
        "hr" => "hr",
        "hu" => "hu",
        "in" => "in_ID",
        "is" => "is",
        "it" => "it",
        "ja" => "ja_JP",
        "ko" => "ko_KR",
        "lt" => "lt",
        "lv" => "lv",
        "mk" => "mk",
        "ms" => "ms",
        "mt" => "mt",
        "nl" => "nl",
        "no" => "no",
        "pl" => "pl",
        "pt" => "pt",
        "ro" => "ro",
        "ru" => "ru",
        "sk" => "sk",
        "sl" => "sl",
        "sq" => "sq",
        "sr" => "sr",
        "sv" => "sv",
        "sw" => "sw",
        "th" => "th_TH",
        "tl" => "tl",
        "tr" => "tr",
        "uk" => "uk",
        "vi" => "vi",
        "yi" => "yi",
        "zh" => "zh_CN",
        "zh-cn" => "zh_CN",
        "zh-tw" => "zh_TW"
    );

    // 逆引き用
    private static $lang_cd_list;

    public static function getLangCd($lang)
    {
        return isset(self::$lang_list[$lang]) ? self::$lang_list[$lang] : "";
    }

    public static function getLang($lang_cd)
    {
        if(!isset(self::$lang_cd_list)) {
            self::$lang_cd_list = array_flip(self::$lang_list);
            self::$lang_cd_list["zh_CN"] = "zh-cn";
        }
        return isset(self::$lang_cd_list[$lang_cd]) ? self::$lang_cd_list[$lang_cd] : "";
    }
}