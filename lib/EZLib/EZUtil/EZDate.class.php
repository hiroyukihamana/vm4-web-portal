<?php
class EZDate
{
    function __construct() {

    }

    function getLocateTime($datetime, $setTimeZone, $nowTimeZone, $format = null)
    {
        if (!is_numeric($datetime)) {
            $datetime = strtotime($datetime);
        }
        $datetime = $datetime - ($nowTimeZone - $setTimeZone) * 3600;
        if ($format) {
            return date($format, $datetime);
        }
        return $datetime;
    }

    function getZoneDate($layout, $countryzone, $time)
    {
        if($countryzone >> 0){
            $zone = 3600 * $countryzone;
        } else {
            $zone = 0;
        }
        if(!$time) {
            $time = time();
        }
        $date = gmdate($layout, $time + $zone);
        return $date;
    }

}
?>