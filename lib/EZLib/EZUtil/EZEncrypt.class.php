<?php
class EZEncrypt
{
    function encrypt($key, $iv, $data)
    {
        $base64_data = base64_encode($data);
        $resource = mcrypt_module_open(MCRYPT_BLOWFISH, '',  MCRYPT_MODE_CBC, '');
        mcrypt_generic_init($resource, $key, $iv);
        $encrypted_data = mcrypt_generic($resource, $base64_data);
        mcrypt_generic_deinit($resource);

        mcrypt_module_close($resource);

        $encrypted_data_base64 = base64_encode($encrypted_data);
        return $encrypted_data_base64;
    }

    function decrypt($key, $iv, $encrypted_data_base64)
    {
        $encrypted_data = self::complementAddSlashes($encrypted_data_base64);
        $resource = mcrypt_module_open(MCRYPT_BLOWFISH, '',  MCRYPT_MODE_CBC, '');
        mcrypt_generic_init($resource, $key, $iv);
        $base64_decrypted_data = mdecrypt_generic($resource, $encrypted_data);
        mcrypt_generic_deinit($resource);
        $decrypted_data = base64_decode($base64_decrypted_data);

        mcrypt_module_close($resource);

        return $decrypted_data;
    }

    private static function complementAddSlashes($encrypted_data_base64) {
        for ($i = 0; $i < 3; $i++) {
            $return = base64_decode($encrypted_data_base64);
            if (!$return)
                $encrypted_data_base64 = "/".$encrypted_data_base64;
            else
                return $return;
        }
        return false;
    }
}
