<?php
//require_once("lib/EZLib/EZUtil/EZFileInfo.class.php");

class EZImage
{
    var $logger = null;
    private $image_magic_dir = "";
    var $finfo = null;

    function __construct($image_magic_dir = "") {
        $this->image_magic_dir = $image_magic_dir ? $image_magic_dir."/" : "";
        $this->logger = EZLogger::getInstance();
        $this->logger2 = EZLogger2::getInstance();
//        $this->finfo = new EZ_FileInfo();
    }

    /**
     * コマンド実行
     */
    function command_exec($command) {
        ob_start();
        $command = $this->image_magic_dir.$command;
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$command);
        passthru($command);
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    }

    /**
     * 画像情報
     */
    function info($image_path) {
        static $image_info;
        // 思ったより画像情報の抽出に時間がかかるので静的に持つ
        $hash = md5($image_path);
        if (isset($image_info[$hash])) {
            return $image_info[$hash];
        } else {
            $cmd = "identify ". escapeshellarg($image_path);
            $ret = $this->command_exec($cmd);
            // 取得エラー
            if (!$ret) {
                $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,$cmd);
                return false;
            }
            // 必要な情報だけ抽出
            $image_info[$hash] = null;
            $lines = split("\n" ,$ret);
            // 改行でページ
            foreach($lines as $line) {
                $_image_info = split(" ", $line);
                // サイズ情報
                if (isset($_image_info[2])) {
                    list($x, $y) = split("x", $_image_info[2]);
                    // フォーマット
                    if ($_image_info[1]) {
                        $image_info[$hash][] = array(
                            "path" => $_image_info[0],
                            "type" => $_image_info[1],
                            "size" => filesize($image_path),
                            "x" => $x,
                            "y" => $y,
                            );
                    }
                }
            }
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array($image_info[$hash]));
            if (count($image_info[$hash]) == 1) {
                return $image_info[$hash];
            } else {
                return $image_info[$hash];
            }
        }
    }

    /**
     * 画像の一括変換
     * 処理に時間がかかりますので注意してください。
     *
     * @param string $file ファイル名（ワイルドカード指定可）
     * @param array $options オプション（mogrifyのオプションそのままです）
     *
     */
    function mogrify($file, $options) {
        $cmd = "mogrify";
        foreach($options as $option => $value) {
            $cmd .= " -".$option." ".escapeshellarg($value);
        }
        $cmd .= " ".escapeshellarg($file);
        $this->logger2->debug($cmd);
        $ret = $this->command_exec($cmd);
    }

    /**
     * 画像変換
     * 変換後のフォーマットは拡張子で自動的に認識する
     *
     * @param string $path_from 変換元のファイルフルパス
     * @param string $path_from 変換先のファイルフルパス
     * @param integer $x_max 高さ
     * @param integer $y_max 幅
     */
    function convert($path_from, $path_to, $x_max = null, $y_max = null) {
        $this->logger2->debug(array($path_from, $path_to, $x_max, $y_max));
        // 画像の情報取得
        $path_parts = pathinfo($path_to);
        $dirname = $path_parts['dirname'];
        $filename = $path_parts['filename'];
        $ext = $path_parts['extension'];
        $image_info_arr = $this->info($path_from);
        if (!$image_info_arr) {
            $this->logger->info(__FUNCTION__."#no image", __FILE__, __LINE__, $path_from);
            return false;
        } else {
            $result = array();
            // ファイルがひとつだけ
            if (count($image_info_arr) == 1) {
                $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $image_info_arr);
                $result[] = $this->_convert($image_info_arr[0], $path_to, $x_max, $y_max);
            } else {
                $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $image_info_arr);
                $i = 1;
                foreach($image_info_arr as $image_info) {
                    $path_to = "{$dirname}/{$filename}_{$i}.{$ext}";
                    $result[] = $this->_convert($image_info, $path_to, $x_max, $y_max);
                    $i++;
                }
            }
            return $result;
        }
    }

    function _convert($image_info, $path_to, $x_max = null, $y_max = null) {
        $path_from = $image_info["path"];
        $x = $image_info["x"];
        $y = $image_info["y"];
        $new_x = $x;
        $new_y = $y;
        if (!($x > 0) || !($y > 0)) {
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__, array($image_info));
            return false;
        }
        // リサイズ
        if ($x_max || $y_max) {
            // 縦横比を保持してリサイズ
            if($x > $x_max || $y > $y_max){
                $x_rate = $x_max / $x;
                $y_rate = $y_max / $y;
                if( $x_rate > $y_rate ){
                    $new_x = intval( $x * $y_rate );
                    $new_y = intval( $y * $y_rate );
                } else {
                    $new_x = intval( $x * $x_rate );
                    $new_y = intval( $y * $x_rate );
                }
            }
        }
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, array(
            "from" => array(
                $path_from,
                "x" => $x,
                "y" => $y),
            "to" => array(
                $path_to,
                "x" => $new_x,
                "y" => $new_y)
                )
            );
        // イメージ生成
        $image_magic = true;
        // イメージマジックで変換
        if ($image_magic) {
            // テキストフォーマットならそのまま画像にする！！
            /*
            $type = $finfo->get_type($path_from);
            if ($type == "text/plain") {
                $cmd = "convert" .
                    " -resize ".$x_max."x".$y_max.
                    " ".escapeshellarg($path_from)." ".escapeshellarg($path_to);
                $ret = $this->command_exec($cmd);
            }
            */
            $cmd = "convert" .
                " -resize ".$new_x."x".$new_y.
                " -colorspace RGB".
//                " -quality 90".
                " ".escapeshellarg($path_from)." ".escapeshellarg($path_to);
            $ret = $this->command_exec($cmd);
        // imageMagickが無い場合PHPの標準ライブラリで扱えるフォーマットのみサポート
        /*
        } else {
            $ext = $this->finfo->get_extension($path_from);
            switch ($ext) {
            case "png" :
                $img_in = ImageCreateFromPNG($path_from);
                break;
            case "jpg" :
                $img_in = ImageCreateFromJPEG($path_from);
                break;
            case "gif" :
                $img_in = ImageCreateFromGIF($path_from);
                break;
            default :
            }
            $img_out = imagecreatetruecolor($new_x, $new_y);
            // フォーマット変換
            imagealphablending($img_out, false);
            ImageCopyResampled($img_out, $img_in, 0, 0, 0, 0, $new_x, $new_y, $x, $y );
            imagesavealpha($img_out, true);

            imagepng($img_out, $path_to);
            ImageDestroy($img_in);
            ImageDestroy($img_out);
            chmod($path_to, 0777);
        */
        }
        $filesize = filesize($path_to);
        // 変換に成功
        if (file_exists($path_to) && $filesize > 0) {
            $_img = pathinfo($path_to);
            return array(
                "path" => $path_to,
                "type" => $_img["extension"],
                "size" => $filesize,
                "x"    => $new_x,
                "y"    => $new_y,
                );
        } else {
            return false;
        }
    }

    function output($path) {
        $this->finfo->output($path);
    }
}
?>