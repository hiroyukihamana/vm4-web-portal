<?php
class EZCsv
{
    // ファイルポインタ
    var $fp = null;
    // デリミタ
    var $delim = ",";
    // CSVファイルのエンコード
    var $csv_encode = null;
    // 内部エンコード
    var $internal_enc = null;
    // ヘッダ有無
    var $is_head = null;
    // フィールド
    var $header = null;
    // パース時のダブルクオートの処理
    var $double_quote = null;

    /**
     * CSVファイルの定義をセット
     *
     * @param boolean $is_header
     * @param string $csv_encode
     * @param string $internal_encode
     * @param string $delim
     * @param boolean $double_quote
     */
    function __construct($is_header = false, $csv_encode = "SJIS", $internal_encode = "EUC-JP", $delim = ",", $double_quote = false) {
        $this->logger = EZLogger2::getInstance();
        $this->is_head = $is_header;
        $this->delim = $delim;
        $this->csv_encode = $csv_encode;
        $this->internal_enc = $internal_encode;
        $this->double_quote = $double_quote;
    }

    /**
     * ファイルを開く
     *
     * @param string $file
     * @param string $mode "r":読み込み専用、"w":書き込み専用
     */
    function open($file, $mode = "r") {
        if ($mode == "r") {
            if (file_exists($file)) {
                $this->fp = fopen($file, "r");
                if ($this->is_head == true) {
                    $line = fgets($this->fp, 4096);
                    $line = mb_convert_encoding($line, $this->internal_encode, $this->csv_encode);
                    $this->header = $this->_parse($line);
                }
                return true;
            } else {
                return false;
            }
        } elseif ($mode == "w") {
            $this->fp = fopen($file, "a+");
        }
    }

    /**
     * ヘッダの項目を取得
     */
    function getHeader() {
        return $this->header;
    }

    /**
     *
     */
    function setHeader($header) {
        $this->is_head = true;
        $this->header = $header;
    }

    /**
     * 次の行を取得
     */
    function getNext() {
        // 終端文字がきたらおしまい
        if (!feof($this->fp)) {
            $line = fgets($this->fp, 4096);
            if (trim($line) === "") {
                @fclose($this->fp);
                $this->fp = null;
                return null;
            }
            $line = mb_convert_encoding($line, $this->internal_encode, $this->csv_encode);
            $data = $this->_parse($line);
            // 値のダブルクオート処理
            if ($this->double_quote) {
                foreach($data as $key => $val) {
                    $data[$key] = str_replace('""', '"', $val);
                }
            }
            return $data;
        } else {
            @fclose($this->fp);
            $this->fp = null;
            return null;
        }
    }

    /**
     * 区切り文字で取得
     */
    function _parse($line, $reset = array()) {
        $pos = strpos($line, $this->delim);
        $fields = $reset;
        while ($pos !== false) {
            $val = substr($line, 0, $pos);
            $line = substr($line, $pos +1);
            // ダブルクォートで始まっている
            if (substr($val, 0, 1) == '"') {
                $len = strlen($val);
                // 値がダブルクォートで終わっていない場合
                if (substr($val, $len -1, 1) != '"') {
                    $_pos = strpos($line, '"');
                    // 次のダブルクォートを探す
                    if ($_pos !== false) {
                        $_val = substr($line, 0, $_pos);
                        $line = substr($line, $_pos +2);
                        $val = substr($val, 1).$this->delim.$_val;
                    }
                // ダブルクォートで終わっている
                } else {
                    $val = substr($val, 1, $len -2);
                }
            }
            if ($this->header && $this->is_head) {
                $fields[$this->header[count($fields)]] = $val;
            } else {
                $fields[] = $val;
            }
            $pos = strpos($line, $this->delim);
        }
        if (substr($line, 0, 1) == '"') {
            $_line = rtrim($line);
            if (substr($_line, strlen($_line) - 1, 1) == '"') {
                $line = substr($_line, 1, strlen($_line) - 2);
                if ($this->header && $this->is_head) {
                    $fields[$this->header[count($fields)]] = $line;
                } else {
                    $fields[] = $line;
                }
            } else {
                // 最後の列が終わらない場合、再帰的に次の行を読み込む
                if (!feof($this->fp)) {
                    $_line = fgets($this->fp, 4096);
                    $_line = mb_convert_encoding($_line, $this->internal_encode, $this->csv_encode);
                    if ($this->header && $this->is_head) {
                        $_flds = $this->_parse($line.$_line, $fields);
                    } else {
                        $_flds = $this->_parse($line.$_line);
                    }
                    $fields = array_merge($fields, $_flds);
                }
            }
        } else {
            $line = rtrim($line);
            if ($this->header && $this->is_head) {
                $fields[$this->header[count($fields)]] = $line;
            } else {
                $fields[] = $line;
            }
        }
        return $fields;
    }

    /**
     * ファイルを閉じる
     */
    function write($data = array()) {
        $_data = array();
        // ヘッダで指定された連想配列のキー順に出力(ヘッダに存在しない配列は出力しない)
        if ($this->is_head == true) {
            foreach($this->header as $_key => $_val) {
                // データ有り
                if (isset($data[$_key])) {
                    $val = $data[$_key];
                    if ($val === "") {
                        $_data[] = "";
                    // 改行を含むとNG
//                    } elseif (is_numeric($val)) {
                    } elseif (is_numeric($val) && preg_match('/[\r\n\s\t]/', $val) == 0) {
                        $_data[] = $val;
                    } else {
                        $val = str_replace('"', '""', $val);
                        if ($this->csv_encode != $this->internal_encode) {
                            $val = @mb_convert_encoding($val, $this->csv_encode, $this->internal_encode);
                        }
                        $_data[] = '"'.$val.'"';
                    }
                // データ無し時はカラムを埋める
                } else {
                    $_data[] = "";
                }
            }
        // 指定されたデータ順
        } else {
            foreach($data as $key => $val) {
                if ($val === "") {
                    $_data[] = "";
                } elseif (is_numeric($val)) {
                    $_data[] = $val;
                } else {
                    $val = str_replace('"', '""', $val);
                    if ($this->csv_encode != $this->internal_encode) {
                        $val = @mb_convert_encoding($val, $this->csv_encode, $this->internal_encode);
                    }
                    $_data[] = '"'.$val.'"';
                }
            }
        }
        $line = join($_data, $this->delim);
        $line .= "\r\n";
        return fwrite($this->fp, $line);
    }

    /**
     * ファイルを閉じる
     */
    function close() {
        if ($this->fp) {
            fclose($this->fp);
        }
    }
}
?>