<?php

class EZArray {

    function mergeRecursiveReplace( $array, $newValues ) {
        foreach ($newValues as $key => $value ) {
            if (is_array($value)) {
                if (!isset($array[$key])) {
                    $array[$key] = array();
                }
                $array[$key] = $this->mergeRecursiveReplace($array[$key], $value);
            } else {
                $array[$key] = $value;
            }
        }
        return $array;
    }
}
?>