<?php

/**
 *
 */
class EZMath {

    function __construct() {
    }

    /**
     * 2進数 -> 16進数変換
     *
     * 通常は base_convert関数を利用するがint(64)をこえるビットは変換できないため使用する
     */
    function binhex ($number) {
        $result = "";
        $bin = substr(chunk_split (strrev($number), 4,'-'), 0, -1);
        $temp = preg_split('[-]', $bin, -1, PREG_SPLIT_DELIM_CAPTURE);
        for ($i = count($temp)-1;$i >= 0;$i--) {
            $result = $result . base_convert(strrev($temp[$i]), 2, 16);
        }
        return "0x".strtoupper($result);
    }

    /**
     * 16進数 -> 2進数変換
     *
     * 通常は base_convert関数を利用するがint(64)をこえるビットは変換できないため使用する
     */
    function hexbin($hex, $padding = false)
    {
        // Validation
        $hex = preg_replace('/^(0x|X)?/i', '', $hex);
        $hex = preg_replace('/[[:blank:]]/', '', $hex);
        if(empty($hex))
        {
            $hex = '0';
        }
        if(!preg_match('/^[0-9A-F]*$/i', $hex))
        {
            trigger_error('Argument is not a hex', E_USER_WARNING);
            return false;
        }

        // Conversion
        $bin = '';
        $hex = array_reverse(str_split($hex));
        foreach($hex as $n)
        {
            $n = hexdec($n);
            for($i = 1; $i <= 8; $i <<= 1)
            {
                $bin .= ($i & $n)? '1' : '0';
            }
            if($padding)
            {
                $bin .= ' ';
            }
        }
        return ltrim(strrev($bin));
    }


    /**
     * n進数 -> x進数へ変換
     * $BIN = "01";
     * $DEC = "0123456789";
     * $HEX = "0123456789ABCDEF";
     * $ASCII = "";
     * for ($i = 97; $i < 122; $i++)
     * {
     *     $ASCII .= chr($i);
     * }
print custombase_convert_big("1111", $HEX, $BIN)."<br />";
print custombase_convert_big($e, $BIN, $ASCII)."<br />";
     */


    function custombase_convert_big ($numstring, $frombase, $tobase) {
        $from_count = strlen($frombase);
        $to_count = strlen($tobase);
        $length = strlen($numstring);
        $result = '';
        for ($i = 0; $i < $length; $i++)
        {
            $number[$i] = strpos($frombase, $numstring{$i});
        }
        do // Loop until whole number is converted
        {
            $divide = 0;
            $newlen = 0;
            for ($i = 0; $i < $length; $i++) // Perform division manually (which is why this works with big numbers)
            {
                $divide = $divide * $from_count + $number[$i];
                if ($divide >= $to_count)
                {
                    $number[$newlen++] = (int)($divide / $to_count);
                    $divide = $divide % $to_count;
                }
                elseif ($newlen > 0)
                {
                    $number[$newlen++] = 0;
                }
            }
            $length = $newlen;
            $result = $tobase{$divide} . $result; // Divide is basically $numstring % $to_count (i.e. the new character)
        }
        while ($newlen != 0);
        return $result;
    }

    function number_format($number, $digit = 1000, $unit = array('Byte', 'KB', 'MB', 'GB', 'TB', 'PB'), $decimals = 2) {
        if ($number == 0) {
            $e = 0;
            $number_view = "0 ".$unit[0];
        } else {
            if ($number > 0) {
                $e = floor(log($number)/log($digit));
                $number_view = sprintf('%.'.$decimals.'f '.$unit[$e], ($number/pow($digit, floor($e))));
            } else {
                // マイナス
                $number = $number * -1;
                $e = floor(log($number)/log($digit));
                $number_view = sprintf('-%.'.$decimals.'f '.$unit[$e], ($number/pow($digit, floor($e))));
            }
        }
        return $number_view;
    }

}
?>