<?php

class EZRtmp {

    var $ruby = "";
    var $rtmp = "";

    /**
     * @todo コマンドのエスケープ処理を出来ればクラスの中で実装したい
     */
    function __construct($ruby = null, $rtmp = null) {
        $this->logger = EZLogger2::getInstance();
        if ($ruby) {
            $this->ruby = $ruby;
        } else {
            $this->ruby = N2MY_RUBY_DIR;
        }
        $this->ruby .= "ruby";

        if ($ruby) {
            $this->rtmp = $rtmp;
        } else {
            $this->rtmp = N2MY_APP_DIR."lib/ruby/rtmp.rb";
        }
    }

    function run($command) {
        $cmd = $this->ruby.
            " -r ".$this->rtmp.
            " -e ".$command;
        $this->logger->debug($cmd);
        return exec($cmd);
    }
}