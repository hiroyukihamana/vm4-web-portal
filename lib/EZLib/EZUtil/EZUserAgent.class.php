<?php

class EZUserAgent {

    private static $browser_info_list;

    public function init($ini_path = null) {
        //iniファイルからデータを取得
        if (version_compare(phpversion(), '5.3.0', '>=')) {
            $this->browser_info_list = parse_ini_file($ini_path, true, INI_SCANNER_RAW);
        } else {
            $this->browser_info_list = parse_ini_file($ini_path, true);
        }
    }

    public static function getInstance() {
        //set static variable
        static $instance;
        //return instance if already exists
        if (isset ($instance)) {
            return $instance;
        }
        // create logger_object
        if (!$ini_path = ini_get('browscap')) {
            $ini_path = "lib/EZLib/EZUtil/php_browscap.ini";
        }
        $instance = new EZUserAgent($ini_path);
        //init
        $obj = $instance->init($ini_path);
        if (PEAR :: isError($obj)) {
            return $obj;
        }
        //return logger_object
        return $instance;
    }

    /**
     *  fnmatch の fatal error に対応(2013/3)
     */
    function getBrowser($user_agent = null) {
        //fnmatch の fatal error に対応。WINでPHP5.3以下ではarray()をかえす
        if (!defined('PHP_VERSION_ID')) {
            $version = explode('.', PHP_VERSION);
            $version = ($version[0] * 10000 + $version[1] * 100 + $version[2]);
        } else {
            $version = PHP_VERSION_ID;
        }
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' && $version < 50300) {
            return array();
            exit;
        }
        //引数が省略された場合のデフォルト値を設定
        if ($user_agent === null) {
            $user_agent = $_SERVER['HTTP_USER_AGENT'];
        }

        //マッチするパターンを探す
        foreach($this->browser_info_list as $pattern => $browser_info){
            if(fnmatch($pattern, $user_agent, FNM_CASEFOLD))
                $match_pattern_list[] = $pattern;
        }

        //もっともマッチするパターンを選択
        $best_score = PHP_INT_MAX;
        foreach($match_pattern_list as $pattern){
            if (mb_strlen($user_agent) <= 255 && mb_strlen($pattern) <= 255) {
                $score = @levenshtein($user_agent, $pattern);
            } else {
                $score = -1;
            }
            if($best_score > $score){
                $best_score = $score;
                $best_pattern = $pattern;
            }
        }

        //情報を取得
        $result = array();
        $regex = preg_replace(array("/\./","/\*/","/\?/"), array("\.",".*","."), $best_pattern);
        $result['browser_name_regex'] = strtolower("^{$regex}$");
        $result['browser_name_pattern'] = $best_pattern;
        $result += $this->browser_info_list[$best_pattern];

        //親を辿って、親の情報を取得
        $parent = @$result['Parent'];
        if (is_array($parent)) {
            while($parent){
                $result += (array)$this->browser_info_list[$parent];
                $parent = @$this->browser_info_list[$parent]['Parent'];
            }
        }

        //キーを全て小文字に変換
        $r = array();
        foreach($result as $key => $value){
            $r[strtolower($key)] = $value;
        }

        return $r;
    }
}
?>