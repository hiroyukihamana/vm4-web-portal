<?php
/**
 * DB関連の関数群
 */
class EZDB_Util {

    function __construct() {
    }

    function parse_query($data) {
        // set default values
        $timeout_passed = FALSE;
        $error = FALSE;
        $read_multiply = 1;
        $finished = FALSE;
        $offset = 0;
        $max_sql_len = 0;
        $file_to_unlink = '';
        $sql_query = '';
        $sql_query_disabled = FALSE;
        $go_sql = FALSE;
        $executed_queries = 0;
        $run_query = TRUE;
        $charset_conversion = FALSE;
        $reset_charset = FALSE;
        $bookmark_created = FALSE;
        // init values
        $sql_delimiter = ';';
        $sql = '';
        $start_pos = 0;
        $i = 0;
        // Append new data to buffer
        $buffer = $data;
        // Current length of our buffer
        $len = strlen($buffer);
        // Grab some SQL queries out of it
        while ($i < $len) {
            $found_delimiter = false;
            // Find first interesting character, several strpos seem to be faster than simple loop in php:
            //while (($i < $len) && (strpos('\'";#-/', $buffer[$i]) === FALSE)) $i++;
            //if ($i == $len) break;
            $oi = $i;
            $p1 = strpos($buffer, '\'', $i);
            if ($p1 === FALSE) {
                $p1 = 2147483647;
            }
            $p2 = strpos($buffer, '"', $i);
            if ($p2 === FALSE) {
                $p2 = 2147483647;
            }
            $p3 = strpos($buffer, $sql_delimiter, $i);
            if ($p3 === FALSE) {
                $p3 = 2147483647;
            } else {
                $found_delimiter = true;
            }
            $p4 = strpos($buffer, '#', $i);
            if ($p4 === FALSE) {
                $p4 = 2147483647;
            }
            $p5 = strpos($buffer, '--', $i);
            if ($p5 === FALSE || $p5 >= ($len -2) || $buffer[$p5 +2] > ' ') {
                $p5 = 2147483647;
            }
            $p6 = strpos($buffer, '/*', $i);
            if ($p6 === FALSE) {
                $p6 = 2147483647;
            }
            $p7 = strpos($buffer, '`', $i);
            if ($p7 === FALSE) {
                $p7 = 2147483647;
            }
            $i = min($p1, $p2, $p3, $p4, $p5, $p6, $p7);
            unset ($p1, $p2, $p3, $p4, $p5, $p6, $p7);
            if ($i == 2147483647) {
                $i = $oi;
                if (!$finished) {
                    break;
                }
                // at the end there might be some whitespace...
                if (trim($buffer) == '') {
                    $buffer = '';
                    $len = 0;
                    break;
                }
                // We hit end of query, go there!
                $i = strlen($buffer) - 1;
            }

            // Grab current character
            $ch = $buffer[$i];

            // Quotes
            if (!(strpos('\'"`', $ch) === FALSE)) {
                $quote = $ch;
                $endq = FALSE;
                while (!$endq) {
                    // Find next quote
                    $pos = strpos($buffer, $quote, $i +1);
                    // No quote? Too short string
                    if ($pos === FALSE) {
                        // We hit end of string => unclosed quote, but we handle it as end of query
                        if ($finished) {
                            $endq = TRUE;
                            $i = $len -1;
                        }
                        break;
                    }
                    // Was not the quote escaped?
                    $j = $pos -1;
                    while ($buffer[$j] == '\\')
                        $j--;
                    // Even count means it was not escaped
                    $endq = (((($pos -1) - $j) % 2) == 0);
                    // Skip the string
                    $i = $pos;
                }
                if (!$endq) {
                    break;
                }
                $i++;
                // Aren't we at the end?
                if ($finished && $i == $len) {
                    $i--;
                } else {
                    continue;
                }
            }

            // Not enough data to decide
            if ((($i == ($len -1) && ($ch == '-' || $ch == '/')) || ($i == ($len -2) && (($ch == '-' && $buffer[$i +1] == '-') || ($ch == '/' && $buffer[$i +1] == '*')))) && !$finished) {
                break;
            }

            // Comments
            if ($ch == '#' || ($i < ($len -1) && $ch == '-' && $buffer[$i +1] == '-' && (($i < ($len -2) && $buffer[$i +2] <= ' ') || ($i == ($len -1) && $finished))) || ($i < ($len -1) && $ch == '/' && $buffer[$i +1] == '*')) {
                // Copy current string to SQL
                if ($start_pos != $i) {
                    $sql .= substr($buffer, $start_pos, $i - $start_pos);
                }
                // Skip the rest
                $j = $i;
                $i = strpos($buffer, $ch == '/' ? '*/' : "\n", $i);
                // didn't we hit end of string?
                if ($i === FALSE) {
                    if ($finished) {
                        $i = $len -1;
                    } else {
                        break;
                    }
                }
                // Skip *
                if ($ch == '/') {
                    // Check for MySQL conditional comments and include them as-is
                    if ($buffer[$j +2] == '!') {
                        $comment = substr($buffer, $j +3, $i - $j -3);
                        if (preg_match('/^[0-9]{5}/', $comment, $version)) {
                            if ($version[0] <= PMA_MYSQL_INT_VERSION) {
                                $sql .= substr($comment, 5);
                            }
                        } else {
                            $sql .= $comment;
                        }
                    }
                    $i++;
                }
                // Skip last char
                $i++;
                // Next query part will start here
                $start_pos = $i;
                // Aren't we at the end?
                if ($i == $len) {
                    $i--;
                } else {
                    continue;
                }
            }

            // End of SQL
            if ($found_delimiter || ($finished && ($i == $len -1))) {
                $tmp_sql = $sql;
                if ($start_pos < $len) {
                    $length_to_grab = $i - $start_pos;
                    if (!$found_delimiter) {
                        $length_to_grab++;
                    }
                    $tmp_sql .= substr($buffer, $start_pos, $length_to_grab);
                    unset ($length_to_grab);
                }
                // Do not try to execute empty SQL
                if (!preg_match('/^([\s]*;)*$/', trim($tmp_sql))) {
                    $sql = $tmp_sql;
                    $querys[] = trim($tmp_sql);
                    //                    $this->PMA_importRunQuery($sql, substr($buffer, 0, $i + strlen($sql_delimiter)));
                    $buffer = substr($buffer, $i +strlen($sql_delimiter));
                    // Reset parser:
                    $len = strlen($buffer);
                    $sql = '';
                    $i = 0;
                    $start_pos = 0;
                    // Any chance we will get a complete query?
                    //if ((strpos($buffer, ';') === FALSE) && !$finished) {
                    if ((strpos($buffer, $sql_delimiter) === FALSE) && !$finished) {
                        break;
                    }
                } else {
                    $i++;
                    $start_pos = $i;
                }
            }
        } // End of parser loop
        return $querys;
    }

    /**
     * MySQLdump
     * この機能、EZFrameに欲しいな
     */
    function _mysqldump($mysql_database)
    {
        $sql="show tables;";
        $result= mysql_query($sql);
        if( $result)
        {
            while( $row= mysql_fetch_row($result))
            {
                $this->_mysqldump_table_structure($row[0]);
                if( isset($_REQUEST['sql_table_data']))
                {
                    $this->_mysqldump_table_data($row[0]);
                }
            }
        }
        else
        {
            echo "/* no tables in $mysql_database */\n";
        }
        mysql_free_result($result);
    }

    function _mysqldump_table_structure($table)
    {
        echo "/* Table structure for table `$table` */\n";
        if( isset($_REQUEST['sql_drop_table']))
        {
            echo "DROP TABLE IF EXISTS `$table`;\n\n";
        }
        if( isset($_REQUEST['sql_create_table']))
        {
            $sql="show create table `$table`; ";
            $result=mysql_query($sql);
            if( $result)
            {
                if($row= mysql_fetch_assoc($result))
                {
                    echo $row['Create Table'].";\n\n";
                }
            }
            mysql_free_result($result);
        }
    }

    function _mysqldump_table_data($table)
    {
        $sql="select * from `$table`;";
        $result = mysql_query($sql);
        if ( $result)
        {
            $num_rows= mysql_num_rows($result);
            $num_fields= mysql_num_fields($result);

            if ( $num_rows > 0)
            {
                echo "/* dumping data for table `$table` */\n";

                $field_type=array();
                $i=0;
                while( $i < $num_fields)
                {
                    $meta= mysql_fetch_field($result, $i);
                    array_push($field_type, $meta->type);
                    $i++;
                }

                //print_r( $field_type);
                echo "insert into `$table` values\n";
                $index=0;
                while( $row= mysql_fetch_row($result))
                {
                    echo "(";
                    for( $i=0; $i < $num_fields; $i++)
                    {
                        if( is_null( $row[$i]))
                            echo "null";
                        else
                        {
                            switch( $field_type[$i])
                            {
                                case 'int':
                                    echo $row[$i];
                                    break;
                                case 'string':
                                case 'blob' :
                                default:
                                    echo "'".mysql_real_escape_string($row[$i])."'";

                            }
                        }
                        if( $i < $num_fields-1)
                            echo ",";
                    }
                    echo ")";

                    if( $index < $num_rows-1)
                        echo ",";
                    else
                        echo ";";
                    echo "\n";

                    $index++;
                }
            }
        }
        mysql_free_result($result);
        echo "\n";
    }

}