<?php

class EZDBR {
    public static function connect($dsn) {
        return new EZDBR_mysql($dsn);
    }
}

class EZDBR_mysql{
    protected $DB = array();
    public function __construct($dsn) {
        $this->logger = EZLogger2::getInstance();
        if (is_array($dsn)) {
            $this->DB['master'] = DB::connect($dsn['master']);
            if (DB::isError($this->DB['master'])) {
            }
            $this->DB['slave'] = DB::connect($dsn['slave']);
        } else {
            $this->DB['master'] = DB::connect($dsn);
            $this->DB['slave'] = $this->DB['master'];
        }
    }

    /**
     * 参照系のみ
     */
    public function __call($method, $params) {
        if ($method == 'query' || $method == 'limitQuery' || $method == 'getOne') {
            if(preg_match('/^\s*SELECT/i', $params[0])) {
                return call_user_func_array(array($this->DB['slave'], $method), $params);
            }
        }
        return call_user_func_array(array($this->DB['master'], $method), $params);
    }
}

?>
