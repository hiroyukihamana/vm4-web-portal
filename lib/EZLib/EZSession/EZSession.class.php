<?php
define('_EZSESSION_NAMESPACE', 'ezlib.class');

class EZSession
{
    /**
     * セッションパラメータ
     *
     * @access private
     * @var    reference
     */
    var $_session = null;
    static $singleton;

    /**
     * コンストラクタ
     *
     * @access public
     * @param
     * @return
     */
    function __construct()
    {
        //session_start
        if (ini_get('session.auto_start') != 1) {
            session_start();
           //API以外からn2my_sessionを指定された場合にRegenerate
           if (!isset($_SESSION['initiated']) && !strpos($_SERVER["SCRIPT_FILENAME"], "/bin/convert.php")) {
               // session_destroy();
               session_regenerate_id(TRUE);
               $_SESSION['initiated'] = true;
           }
        }

        //create session reference
        $this->_session =& $_SESSION;
    }

    /**
     * シングルトンパターンを適用
     * ただし多用しないこと！
     */
    public static function getInstance() {
        if (EZSession::$singleton) {
            return EZSession::$singleton;
        }
        EZSession::$singleton = new EZSession();
        return EZSession::$singleton;
    }

    /**
     * 全てのセッションパラメータを取得
     *
     * @access public
     * @param
     * @return array
     */
    function all()
    {
        return $this->_session;
    }

    /**
     * 全てのセッションパラメータを完全に削除
     *
     * @access public
     * @param
     * @return
     */
    function clear()
    {
        $this->_session = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000
            ,$params["path"], $params["domain"]
            ,$params["secure"], $params["httponly"]);
        }
        session_destroy();
    }

    /**
     * セッションパラメータを取得
     *
     * @access public
     * @param  mixed  $keys    要素名（要素名を配列で渡すことにより複数指定可能）
     * @param  string $ns      疑似名前空間
     * @param  string $default デフォルト値
     * @return mixed
     */
    function get($keys, $ns = _EZSESSION_NAMESPACE, $default = null)
    {
        if (!$ns) {
            trigger_error(sprintf("#invalid session_namespace. - %s - %s", __FILE__, __LINE__), E_USER_ERROR);
        }

        if (is_array($keys)) {
            $ra = array();
            foreach ($keys as $k) {
                if (isset($this->_session[$ns][$k])) {
                    $ra[$k] = $this->_session[$ns][$k];
                }
            }
            return $ra;

        } else {
            if (isset($this->_session[$ns][$keys])) {
                return $this->_session[$ns][$keys];
            } else {
                return $default;
            }
        }

    }

    /**
     * セッションパラメータを全て取得
     *
     * @access public
     * @param  string $ns 疑似名前空間
     * @return array
     */
    function getAll($ns = _EZSESSION_NAMESPACE)
    {
        if (!isset($this->_session[$ns])) {
            $this->_session[$ns] = array();
        }

        return $this->_session[$ns];
    }

    /**
     * セッションパラメータ要素名を取得
     *
     * @access public
     * @param  string $ns 疑似名前空間
     * @return array
     */
    function getKeys($ns = _EZSESSION_NAMESPACE)
    {
        if (isset($this->_session[$ns])) {
            return array_keys($this->_session[$ns]);
        }

        return array();
    }

    /**
     * セッションパラメータに指定された要素名が存在するかチェック
     *
     * @access public
     * @param  string  $key 要素名
     * @param  string  $ns  疑似名前空間
     * @return boolean
     */
    function has($key, $ns = _EZSESSION_NAMESPACE)
    {
        if (isset($this->_session[$ns][$key])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * セッションパラメータを削除
     *
     * @access public
     * @param  string $key 要素名
     * @param  string $ns  疑似名前空間
     * @return
     */
    function remove($key, $ns = _EZSESSION_NAMESPACE)
    {
        if (isset($this->_session[$ns][$key])) {
            unset($this->_session[$ns][$key]);
        }
    }

    /**
     * セッションパラメータを全て削除
     *
     * @access public
     * @param  string $ns 疑似名前空間
     * @return
     */
    function removeAll($ns = _EZSESSION_NAMESPACE)
    {
        if (isset($this->_session[$ns])) {
            //$this->_session[$ns] = array();
            unset($this->_session[$ns]);
        }
    }

    /**
     * セッションパラメータを設定
     *
     * @access public
     * @param  string $key   要素名
     * @param  mixed  $value 値
     * @param  string $ns    疑似名前空間
     * @return
     */
    function set($key, $value, $ns = _EZSESSION_NAMESPACE)
    {
        if (is_string($value)) {
            $this->_session[$ns][$key] = trim($value);
        } else {
            $this->_session[$ns][$key] = $value;
        }
    }

    /**
     * セッションパラメータをまとめて設定
     *
     * @access public
     * @param  array   &$data 要素名と値のペア配列（参照渡し）
     * @param  string   $ns   疑似名前空間
     * @param  boolean  $mode モード（true：既存のパラメータに上書き | false：設定値で入れ替え）
     * @return
     */
    function setAll(&$data, $ns = _EZSESSION_NAMESPACE, $mode = false)
    {
        if ($mode == true) {
            $this->_session[$ns] = $data;
        } else {
            if (!isset($this->_session[$ns])) {
                $this->_session[$ns] = array();
            }
            $this->_session[$ns] = array_merge($this->_session[$ns], $data);
        }
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
