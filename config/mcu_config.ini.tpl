[MCU]
ignore_create_sip_account = 1

defaultId                               = "5099083"
defaultPw                               = "2GREO9yaQ0Yx"
defaultViewId                           = "flashViewId"
defaultViewPw                           = "2GREO9yaQ0Yx"

# mobile
smallId                                         = "smallId"
smallPw                                         = "6eAqzKgtC6"
smallViewId                                     = "smallViewId"
smallViewPw                                     = "6eAqzKgtC6"

# video conference
videoParticipantDomain      = "vcube"
videoParticipantGuestDomain = "vcubeguest"

# pgi client
telephoneId                                     = "telephoneId"
telephonePw                                     = "telephonePw"

# mcu to PGI sip client
mcuToPgiDid                                     = "7771119800"

soap_domain                 = "WEB Domain"

regist_conference           = "%s/ives/ConferenceMngrListener.php?wsdl"
callback_conference         = "%s/ives/ConferenceListener.php?wsdl"
create_client               = "http://%s/B2BAPI/controllerService?wsdl"

conference_listener         = "%s/ives/ConferenceListener_wsdl.php"
conference_listener_port    = "%s/ives/ConferenceListener.php"
conference_listener_xsd     = "%s/ives/ConferenceListener.xsd"
conference_mngr_listener    = "%s/ives/ConferenceMngrListener_wsdl.php"
conference_mngr_listener_port = "%s/ives/ConferenceMngrListener.php"
conference_mngr_listener_xsd= "%s/ives/ConferenceMngrListener.xsd"

conference_name             = "onpremise-meeting"
mixerId                     = "mixer@http://127.0.0.1:9090"
mixerId2                        = "mixer@http://127.0.0.1:9090"
profileId                   = "sip_profile"
polycomProfileId            = "sip_profile"
flashProfileId              = "sip_profile"
polycomHD720ProfileId           = "HD720"
mobileProfileId                         = "mobile_profile"

stbHDProfileId                          = "stb720p"
stbflashProfileId           = "stb720p"
size                        = 2
hd_size                     = 6

didLength                   = 10
guestWsdl                   = "http://%s/mcuGuestController/DidManagerWsService?wsdl"
guestPrefix                 = ""
guestDidLength              = 7

invitationGuestCallbackUrl  = "%s/ives/validate/validateInvitationGuestDid.php"
reservationGuestCallbackUrl = "%s/ives/validate/validateReservationGuestDid.php"
sipWsdl         = ""

backgroundImage = "/var/lib/mediaserver/mute.png"
telephoneClientImage      = "/var/lib/mediaserver/polycom_phone.png"

callout_sip_template            = "sip:%s@%s"
callout_callerId_template            = "sip:%s@s.%s"
callout_proxy           = "sip:%s"
callout_h323_template           = "sip:%s-%s@%s:5090"
callout_name                            = "__CALLEE__PARTICIPANT__"
callout_pgi_name                        = ""

document_sharing_upload_path    = "/upload/mcu/app"
document_sharing_no_image_path  = "shared/images/mcu/noimage.jpg"