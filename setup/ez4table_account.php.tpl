#!{N2MY_PHP_DIR}
<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("lib/EZLib/EZDB/EZDB.class.php");

class EZ4TableBuilder extends AppFrame {

    function init() {
        $db = new EZDB();
        $db->init($this->config->get("GLOBAL", "auth_dsn"));
        $this->tables = $db->_conn->getListOf("tables");
    }

    function action_create_xml() {
        foreach ($this->tables as $_key => $table_name) {
            echo $table_name;
            $db = new EZDB();
            $db->init($this->config->get("GLOBAL", "auth_dsn"), $table_name);
            $db->tableInfoBuilder("account");
        }
    }

    function action_create_table() {

    }

    function default_view() {
        echo <<<EOM
USAGE
    ez4table [MODE] [TABLES...]

OPTIONS
    MODE:
        create_xml : database table -> xml file

        create_table : xml file -> database table

    TABLES:
        table list
EOM;
    }

    /**
     *
     */
    function createTable() {
        foreach ($this->tables as $_key => $table_name) {
        }
    }
}
if ($argc >= 2) {
    $sub_cmd = $argv[1];
    $_REQUEST["action_".$sub_cmd] = "1";
    $table = isset($argv[2]) ? $argv[2] : "";
}
$main = new EZ4TableBuilder();
$main->execute();