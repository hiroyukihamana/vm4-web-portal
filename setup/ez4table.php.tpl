#!{N2MY_PHP_DIR}
<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("lib/EZLib/EZDB/EZDB.class.php");

class EZ4TableBuilder extends AppFrame {

    var $_dsn = null;

    function init() {
        $db = new EZDB();
        $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        foreach ($server_list["SERVER_LIST"] as $value) {
            $server[] .= $value;
        }
        $this->_dsn = $server[0];
        $db->init($this->_dsn);
        $this->tables = $db->_conn->getListOf("tables");
    }

    function action_create_xml() {
        $table = $_REQUEST["table"];

        error_log($this->_dsn);
        if ($table) {
            echo $table."\n";
            $db = new EZDB();
            $db->init($this->_dsn, $table);
            $db->tableInfoBuilder("data");
        } else {
            foreach ($this->tables as $_key => $table) {
                echo $table_name;
                $db = new EZDB();
                $db->init($this->_dsn, $table);
                $db->tableInfoBuilder("data");
            }
        }
    }

    function action_create_table() {

    }

    function default_view() {
        echo <<<EOM
USAGE
    ez4table [MODE] [TABLES...]

OPTIONS
    MODE:
        create_xml : database table -> xml file

        create_table : xml file -> database table

    TABLES:
        table list
EOM;
    }

    /**
     *
     */
    function createTable() {
        foreach ($this->tables as $_key => $table_name) {
        }
    }
}
if ($argc >= 2) {
    $sub_cmd = $argv[1];
    $_REQUEST["action_".$sub_cmd] = "1";
    $table = isset($argv[2]) ? $argv[2] : "";
    $_REQUEST["table"] = $table;
}
$main = new EZ4TableBuilder();
$main->execute();