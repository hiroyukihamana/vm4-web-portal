ALTER TABLE `user` ADD `intra_fms` TINYINT( 1 ) NOT NULL DEFAULT '0' COMMENT '0: 未契約, 1: 契約' AFTER `user_delete_status` ;

ALTER TABLE `user` ADD INDEX ( `intra_fms` ) ;

--
-- user_fms_server
--

CREATE TABLE `user_fms_server` (
  `fms_key` int(11) NOT NULL auto_increment,
  `user_key` int(11) NOT NULL,
  `server_address` varchar(64) NOT NULL,
  `server_port` int(10) NOT NULL,
  `server_count` int(10) NOT NULL default '0' COMMENT '利用回数',
  `server_priority` int(10) NOT NULL default '1' COMMENT '優先度',
  `error_count` int(11) NOT NULL default '0',
  `is_available` tinyint(1) NOT NULL default '1' COMMENT '0: 停止, 1: 有効',
  `registtime` datetime NOT NULL,
  `updatetime` datetime NOT NULL,
  PRIMARY KEY  (`fms_key`),
  KEY `status` (`is_available`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


--
-- meeting
--

ALTER TABLE `meeting` ADD `intra_fms` TINYINT( 1 ) NOT NULL DEFAULT '0' COMMENT '0: 共用fms, 1: intrafms' AFTER `is_reserved` ;


-- doc
ALTER TABLE `document` ADD `document_transmitted` TINYINT( 1 ) NOT NULL DEFAULT '1' COMMENT '1: 未送信, 2: 送信' AFTER `document_sort` ;

ALTER TABLE `fms_watch_status` CHANGE `starttime` `starttime` DATETIME NOT NULL;

ALTER TABLE `document` ADD `participant_id` INT( 10 ) NULL COMMENT '資料をアップロードしたparticipant_key' AFTER `document_id` ;
ALTER TABLE `document` ADD `document_transmitted` TINYINT( 1 ) NOT NULL DEFAULT '1' COMMENT '1: 未送信, 2: 送信' AFTER `document_sort` ;


-- ユーザー情報の備考が拡張項目と一緒になっている
ALTER TABLE `user` ADD `memo` text AFTER `addition` ;

-- 部屋のソート順
ALTER TABLE `room` ADD `room_sort` INT NOT NULL DEFAULT '1' AFTER `room_status` ;

-- キャビネット設定
ALTER TABLE `room` ADD `cabinet` TINYINT NOT NULL DEFAULT '1' AFTER `room_sort` ;

-- ファイル資料張り込み設定
ALTER TABLE `room` ADD `mfp` VARCHAR(10) DEFAULT 'all' AFTER `cabinet` ;

