-- MACアドレス
ALTER TABLE  `member` ADD  `mac_address` TEXT NULL COMMENT  'MACアドレス';
ALTER TABLE  `user` ADD `has_chat` INT( 1 )  NULL DEFAULT '0' COMMENT  '0:無し、1:有り' AFTER  `is_minutes_delete`;
ALTER TABLE  `user` ADD `is_cybozu_option` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'cybozu連係オプション' AFTER `invoice_flg`;
ALTER TABLE `participant` ADD INDEX ( `is_active` );
