-- 【Attention!】
-- アップデート対象のパッケージがver4.6.2.1以前の場合のみ適用してください。
-- ver4.6.2.1以降のパッケージはsetup時に作成されます。


-- user

ALTER TABLE  `user` ADD `use_clip_share` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '動画共有オプション' AFTER `is_cybozu_option`;
ALTER TABLE  `user` ADD `clip_share_storage_size` int(10) NOT NULL DEFAULT '0' COMMENT '動画共有利用容量(MB)' AFTER `use_clip_share`;


-- --------------------------------------------------------

--
-- テーブルの構造 `clip`
--

CREATE TABLE IF NOT EXISTS `clip` (
  `clip_id` int(11) NOT NULL auto_increment COMMENT 'プライマリキー',
  `clip_key` varchar(64) NOT NULL COMMENT 'クリップキー',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーキー',
  `title` varchar(128) NOT NULL COMMENT 'タイトル',
  `description` text NOT NULL COMMENT '説明',
  `upload_filesize` bigint(20) unsigned NOT NULL COMMENT '元ファイルサイズ',
  `flv_filesize` bigint(20) unsigned default NULL COMMENT 'flvのファイルサイズ',
  `storage_no` tinyint(3) unsigned NOT NULL COMMENT 'ストレージNO',
  `clip_status` tinyint(1) NOT NULL default '0' COMMENT '変換状況',
  `duration` float(12,2) NOT NULL COMMENT '動画再生時間',
  `is_deleted` tinyint(1) NOT NULL default '0' COMMENT '削除フラグ',
  `createtime` datetime NOT NULL COMMENT '作成時間',
  `updatetime` datetime NOT NULL COMMENT '更新時間',
  PRIMARY KEY  (`clip_id`),
  KEY `clip_key` (`clip_key`),
  KEY `user_key` (`user_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_clip`
--

CREATE TABLE IF NOT EXISTS `meeting_clip` (
  `meeting_clip_key` int(11) NOT NULL auto_increment COMMENT 'プライマリキー',
  `meeting_key` int(11) NOT NULL COMMENT 'ミーティングキー',
  `clip_key` varchar(64) NOT NULL COMMENT 'クリップキー',
  `is_deleted` tinyint(1) NOT NULL default '0' COMMENT '削除フラグ',
  `is_loaded` tinyint(1) NOT NULL default '0' COMMENT '会議での利用済みフラグ',
  `createtime` datetime default NULL COMMENT '作成日時',
  `updatetime` datetime default NULL COMMENT '更新日時',
  PRIMARY KEY  (`meeting_clip_key`),
  KEY `meeting_key` (`meeting_key`),
  KEY `clip_key` (`clip_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
