-- 課金対象グループ
ALTER TABLE `user` ADD `user_group` VARCHAR( 64 ) NULL COMMENT '課金対象のグループ' AFTER `user_id`;

-- ユーザーグループテーブル追加
CREATE TABLE `user_group` (
`group_id` VARCHAR( 64 ) NOT NULL COMMENT  'グループID',
`group_name` TEXT NOT NULL COMMENT  'グループ名',
`status` VARCHAR( 1 ) NOT NULL COMMENT  '0: 無効, 1: 有効',
`create_datetime` DATETIME NOT NULL COMMENT  '開始日時',
`update_datetime` DATETIME NOT NULL COMMENT  '更新日付'
) ENGINE = MYISAM;