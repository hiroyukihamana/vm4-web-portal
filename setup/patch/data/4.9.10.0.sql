/*VMTG-3261 V-CUBE One対応：会議室とメンバーの分類機能追加*/
CREATE TABLE IF NOT EXISTS `member_room_whitelist` (
  `member_room_whitelist_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `member_key` int(11) NOT NULL  COMMENT 'メンバーキー',
  `room_key` varchar(64) NOT NULL COMMENT '部屋キー',
  `whitelist_status` tinyint(4) NOT NULL DEFAULT '1',
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`member_room_whitelist_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `room` ADD `has_member_whitelist` tinyint(4) NOT NULL COMMENT 'タグ付けされているかフラグ' DEFAULT 0 AFTER  `is_one_time_meeting`;

ALTER TABLE `user_room_setting` ADD `has_member_whitelist` tinyint(4) NOT NULL COMMENT 'タグ付けされているかフラグ' DEFAULT 0 AFTER  `mobile_mix`;

/*VMTG-2464 （会議中）個人ホワイトボードの印刷、PDF化、拡大 #ソフトバンクBB*/
ALTER TABLE `room` ADD `is_convert_personal_wb_to_pdf` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '個人ホワイトボードPDF化：0:使用しない、1:使用する' AFTER `is_personal_wb`;
ALTER TABLE `user_room_setting` ADD `is_convert_personal_wb_to_pdf` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '個人ホワイトボードPDF化：0:使用しない、1:使用する' AFTER `is_personal_wb`;
