
-- 削除する時にデータを指定する為ユーザーがオプションを追加した時のキーを追加
ALTER TABLE `ordered_service_option` ADD `user_service_option_key` INT( 11 ) NOT NULL DEFAULT '0' COMMENT 'user_service_optionのキー（メンバー課金のみ）' AFTER `room_key` ;
ALTER TABLE `ordered_service_option` ADD INDEX ( `user_service_option_key` ) ;

CREATE TABLE `user_service_option` (
  `user_service_option_key` int(11) NOT NULL auto_increment,
  `user_key` varchar(64) NOT NULL default '0',
  `service_option_key` int(11) NOT NULL default '0',
  `user_service_option_status` int(11) NOT NULL default '1' COMMENT '停止中:0、利用中:1、開始待ち:2',
  `user_service_option_starttime` datetime default '0000-00-00 00:00:00' COMMENT '開始日',
  `user_service_option_registtime` datetime NOT NULL default '0000-00-00 00:00:00',
  `user_service_option_deletetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`user_service_option_key`),
  KEY `room_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

--
-- テーブルの構造 data.`user_plan`
--

CREATE TABLE `user_plan` (
  `user_plan_key` int(11) NOT NULL auto_increment,
  `user_key` varchar(64) NOT NULL,
  `service_key` int(11) NOT NULL default '0',
  `user_plan_yearly` tinyint(4) NOT NULL default '0',
  `user_plan_yearly_starttime` datetime NOT NULL default '0000-00-00 00:00:00',
  `user_plan_starttime` datetime NOT NULL default '0000-00-00 00:00:00',
  `user_plan_status` tinyint(1) NOT NULL default '1' COMMENT '停止中:0、利用中:1、開始待ち:2',
  `user_plan_registtime` datetime NOT NULL default '0000-00-00 00:00:00',
  `user_plan_updatetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`user_plan_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_member_use_log`
--

CREATE TABLE `meeting_member_use_log` (
  `log_no` int(11) NOT NULL auto_increment,
  `meeting_key` int(11) NOT NULL default '0',
  `user_key` int(11) NOT NULL,
  `member_key` int(11) NOT NULL,
  `participant_key` int(11) default NULL,
  `create_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`log_no`),
  KEY `meeting_key` (`meeting_key`),
  KEY `create_datetime` (`create_datetime`),
  KEY `user_key` (`user_key`),
  KEY `member_key` (`member_key`),
  KEY `participant_key` (`participant_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

 ALTER TABLE `user_plan` ADD INDEX ( `user_key` );


--
-- テーブルの構造 `member_usage_details`
--


CREATE TABLE IF NOT EXISTS `member_usage_details` (
  `user_key` int(11) default NULL,
  `user_id` varchar(64) default NULL,
  `user_company_name` text,
  `user_company_postnumber` text,
  `user_company_address` text,
  `user_company_phone` text,
  `user_company_fax` text,
  `user_starttime` datetime default NULL,
  `user_deletetime` datetime default NULL,
  `user_plan_yearly` tinyint(1) default NULL,
  `user_plan_yearly_starttime` datetime default NULL,
  `room_key` varchar(64) default NULL,
  `meeting_max_seat` int(11) default NULL,
  `service_name` text,
  `intro` tinyint(10) default NULL,
  `desktop_share` tinyint(1) default NULL,
  `hdd_extension` int(11) default NULL,
  `participant_key` int(11) default NULL,
  `participant_station` text,
  `meeting_key` varchar(128) default NULL,
  `meeting_room_name` text,
  `actual_start_datetime` datetime default NULL,
  `actual_end_datetime` datetime default NULL,
  `member_key` int(11) default NULL,
  `member_id` text,
  `member_name` text,
  `member_name_kana` text,
  `member_email` text,
  `uptime_start` datetime default NULL,
  `uptime_end` datetime default NULL,
  `use_count` int(11) default NULL,
  `create_datetime` datetime default NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
