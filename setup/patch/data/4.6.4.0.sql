ALTER TABLE  `participant` ADD  `teleconference_pin_cd` INT NOT NULL DEFAULT  '0' COMMENT  'PGi連携用Pin code' AFTER  `use_count`;
ALTER TABLE  `meeting` ADD  `pgi_service_name` VARCHAR( 64 ) NULL COMMENT  'PGi利用サービス名' AFTER  `pgi_api_status`;

--
-- テーブルの構造 `agency_relation_member`
--

CREATE TABLE `agency_relation_member` (
  `agency_relation_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `member_key` int(11) NOT NULL,
  PRIMARY KEY (`agency_relation_member_id`),
  KEY `agency_id` (`agency_id`),
  KEY `member_key` (`member_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `agency_relation_user`
--

CREATE TABLE `agency_relation_user` (
  `agency_relation_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `user_key` varchar(45) NOT NULL,
  PRIMARY KEY (`agency_relation_user_id`),
  KEY `agency_id` (`agency_id`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;