ALTER TABLE  `eventlog` ADD INDEX (  `meeting_key` );
ALTER TABLE  `eventlog` ADD INDEX (  `event_id` );
ALTER TABLE  `user` ADD  `user_whiteboard_advertise` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'ホワイトボード広告表示' AFTER  `use_port_plan`;
ALTER TABLE  `user_service_option` ADD `user_service_option_expiredatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT  '停止予定日';