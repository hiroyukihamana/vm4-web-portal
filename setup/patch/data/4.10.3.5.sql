/* MTGVFOUR-2213【One】VID登録数が多いマスターIDの情報変更を加えると、エラーが発生する */
ALTER TABLE `pgi_rate` ADD INDEX pgi_setting_key (`pgi_setting_key`);

/* MTGVFOUR-2215TO DO5 サブタスク【One】 メンバー追加部分ロジック変更 */
CREATE TABLE IF NOT EXISTS `sales_room_queue` (
  `sales_room_queue_key` INT(11) NOT NULL AUTO_INCREMENT,
  `member_key` INT(11) NOT NULL,
  `error_count` INT(11) NULL,
  `pend` TINYINT(1) NULL,
  `create_detatime` DATETIME NULL,
  `update_datetime` DATETIME NULL,
  PRIMARY KEY (`sales_room_queue_key`)
  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;