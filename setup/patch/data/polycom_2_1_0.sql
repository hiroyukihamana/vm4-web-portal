ALTER TABLE  `room` ADD  `mobile_mix` INT NOT NULL COMMENT  'MCUを利用したMobileMix機能の利用可／不可フラグ' AFTER  `meeting_limit_time`

ALTER TABLE  `ives_setting` ADD  `mcu_server_key` INT( 10 ) NOT NULL DEFAULT  '0' AFTER  `ives_setting_key` ,
ADD INDEX ( mcu_server_key );
ALTER TABLE  `ives_setting` CHANGE  `mcu_server_key`  `mcu_server_key` INT( 10 ) NOT NULL DEFAULT  '0' COMMENT  '数値が入力されている場合は指定されたMCUサーバーを利用する';

ALTER TABLE  `video_conference` ADD  `mcu_server_key` INT( 10 ) NOT NULL AFTER  `video_conference_key` ,
ADD INDEX ( mcu_server_key );

ALTER TABLE  `video_conference_participant` ADD  `media_mixer_key` INT( 10 ) NOT NULL AFTER  `video_conference_key` ,
ADD INDEX ( media_mixer_key );

ALTER TABLE  `video_conference` ADD  `media_mixer_key` INT( 10 ) NOT NULL AFTER  `mcu_server_key` ,
ADD INDEX ( media_mixer_key );

ALTER TABLE  `video_conference_participant` ADD  `participant_session_id` VARCHAR( 50 ) NOT NULL AFTER  `did` ,
ADD INDEX ( participant_session_id );

ALTER TABLE  `video_conference_participant` ADD  `participant_name` VARCHAR( 255 ) NOT NULL AFTER  `part_id` ,
ADD  `participant_type` VARCHAR( 30 ) NOT NULL AFTER  `participant_name` ,
ADD INDEX ( participant_type );

CREATE TABLE IF NOT EXISTS `conference_record` (
  `conference_record_key` int(11) NOT NULL auto_increment,
  `methodName` varchar(30) NOT NULL,
  `confId` varchar(50) default NULL,
  `partId` int(5) default NULL,
  `partName` varchar(255) default NULL,
  `partType` varchar(30) default NULL,
  `state` varchar(30) default NULL,
  `info` text,
  `createdate` varchar(30) NOT NULL,
  PRIMARY KEY  (`conference_record_key`),
  KEY `confId` (`confId`,`partId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
