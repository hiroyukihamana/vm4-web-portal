ALTER TABLE `document` ADD `document_format` VARCHAR( 64 ) NULL COMMENT '変換後のフォーマット' AFTER `document_name` ,
ADD `document_version` VARCHAR( 64 ) NULL COMMENT 'アニメーションのバージョン' AFTER `document_format`;

DROP TABLE `echo_canceler_log`;
CREATE TABLE IF NOT EXISTS `echo_canceler_log` (
 `meeting_key` int(10) NOT NULL,
 `participant_id` text NOT NULL,
 `machine_id` varchar(255) NOT NULL,
 `start_datetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
