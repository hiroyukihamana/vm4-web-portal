CREATE TABLE `datacenter_priority` (
`datacenter_priority_key` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`user_key` INT( 11 ) NOT NULL ,
`country` VARCHAR( 16 ) NOT NULL ,
`datacenter_key` INT( 11 ) NOT NULL ,
`sort` INT( 11 ) NOT NULL ,
`create_datetime` DATETIME NOT NULL ,
INDEX (  `user_key` ,  `datacenter_key` )
) ENGINE = MYISAM;

CREATE TABLE `datacenter_ignore` (
`datacenter_ignore_key` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`user_key` INT( 11 ) NOT NULL ,
`datacenter_key` INT( 11 ) NOT NULL ,
`create_datetime` DATETIME NOT NULL ,
INDEX (  `user_key` ,  `datacenter_key` )
) ENGINE = MYISAM;

ALTER TABLE `ordered_service_option` ADD `ordered_service_option_expiredatetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '停止予定日' AFTER `ordered_service_option_deletetime`;
ALTER TABLE `room` ADD `room_expiredatetime` DATETIME NULL DEFAULT '0000-00-00 00:00:00' COMMENT '停止予定日' AFTER `room_deletetime`;

