/*アドレス帳グループ機能*/
CREATE TABLE `address_book_group` (
`address_book_group_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'アドレスグループKEY',
`user_key` int(11) NOT NULL COMMENT  'ユーザーKEY',
`group_name` varchar(255) NOT NULL COMMENT  'グループ名',
`status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'ステータス 0:無効、1:有効',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`address_book_group_key`),
KEY `user_key` (`user_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE  `address_book` ADD `address_book_group_key` int(11) NOT NULL DEFAULT '0' COMMENT 'アドレスグループキー' AFTER `email`;