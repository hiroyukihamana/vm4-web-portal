CREATE TABLE `storage_folder` (
`storage_folder_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'フォルダKEY',
`user_key` int(11) NOT NULL COMMENT  'ユーザーKEY',
`member_key` int(11) NOT NULL COMMENT  'メンバーKEY',
`folder_name` varchar(255) NOT NULL COMMENT  'フォルダ名',
`parent_storage_folder_key` int(11) NOT NULL DEFAULT '0' COMMENT '所属フォルダ',
`path` text NOT NULL COMMENT  'パス',
`status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'ステータス 0:無効、1:有効',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`storage_folder_key`),
KEY `user_key` (`user_key`),
KEY `member_key` (`member_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE  `member` ADD  `outbound_number_id` VARCHAR(64) DEFAULT NULL COMMENT  'セールスアウトバウンドナンバー' AFTER  `outbound_id`;

CREATE TABLE `outbound` (
  `outbound_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL,
  `outbound_prefix` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`outbound_key`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
ALTER TABLE  `reservation` ADD `reservation_custom` VARCHAR(64) DEFAULT NULL COMMENT 'カスタマードメイン情報保存' AFTER  `reservation_url`;
ALTER TABLE  `room` ADD `use_extend_bandwidth` VARCHAR(64) DEFAULT NULL COMMENT '利用帯域追加機能' AFTER  `use_sales_option`;

/* 8361 サジェスト */
CREATE TABLE `meeting_suggest_log` (
`suggest_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'サジェストKEY',
`meeting_key` int(11) NOT NULL COMMENT  'ミーティングKEY',
`meeting_sequence_key` int(11) NOT NULL COMMENT  'ミーティングシーケンスKEY',
`user_key` int(11) NOT NULL COMMENT  'ユーザーキー',
`participant_key` int(10) NOT NULL COMMENT  '参加者KEY',
`user_name` varchar(255) NOT NULL COMMENT  '参加者名',
`level` varchar(11) NOT NULL COMMENT  'レベル',
`status_type` varchar(255) NOT NULL COMMENT  'サジェスト種類',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`suggest_key`),
KEY `meeting_key` (`meeting_key`),
KEY `meeting_sequence_key` (`meeting_sequence_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/* 部屋の人数臨時拡張 */
ALTER TABLE  `room` ADD  `extend_max_seat` INT( 11 ) NOT NULL AFTER  `max_guest_seat_flg`;
ALTER TABLE  `room` ADD  `extend_max_audience_seat` INT( 11 ) NOT NULL AFTER  `extend_max_seat`;
ALTER TABLE  `room` ADD  `extend_seat_flg` INT( 11 ) NOT NULL AFTER  `extend_max_audience_seat`;
ALTER TABLE  `room` ADD  `member_multi_room_flg` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `extend_seat_flg`;
ALTER TABLE  `max_connect_log` ADD  `type` VARCHAR( 64 ) NULL DEFAULT NULL AFTER  `room_key`;

ALTER TABLE  `participant` ADD `personal_wb_login_id` VARCHAR(64) DEFAULT NULL COMMENT '個人WB保存用ID' AFTER  `storage_login_id`;

/* VMTG-297 機能利用カウント */
CREATE TABLE `meeting_action_count_log` (
`meeting_action_count_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '機能カウントKEY',
`meeting_key` int(11) NOT NULL COMMENT  'ミーティングKEY',
`meeting_sequence_key` int(11) NOT NULL COMMENT  'ミーティングシーケンスKEY',
`action_type` varchar(255) NOT NULL COMMENT  '操作タイプ',
`count` int(10) NOT NULL COMMENT  '利用数',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`meeting_action_count_key`),
KEY `meeting_key` (`meeting_key`),
KEY `meeting_sequence_key` (`meeting_sequence_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

