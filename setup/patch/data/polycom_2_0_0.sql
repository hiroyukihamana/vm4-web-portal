ALTER TABLE `ives_setting` ADD COLUMN `profile_id` INT(10) NOT NULL COMMENT '課金タイプ, 社内テスト用: 281, デモ用: 321, 課金用: 323' AFTER `client_pw`;
ALTER TABLE `ives_setting` ADD COLUMN `num_profiles` INT(10) NOT NULL DEFAULT '0' COMMENT '課金タイプの申し込み数' AFTER `profile_id`;
ALTER TABLE `ives_setting` ADD COLUMN `use_active_speaker` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'アクティブスピーカー使用/不使用フラグ' AFTER `num_profiles`;
