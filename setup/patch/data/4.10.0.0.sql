CREATE TABLE IF NOT EXISTS `browser_title` (
  `browser_title_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーキー',
  `title` varchar(64) NOT NULL COMMENT 'タイトル内容',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`browser_title_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `sales_setting_image` (
  `sales_setting_image_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーキー',
  `image_id` varchar(50) NOT NULL COMMENT '画像ID',
  `type` varchar(10) NOT NULL COMMENT 'ヘッダー：header、カスタマイズ映像枠：seatLogo、入室前のロゴ：logo',
  `extension` varchar(10) NOT NULL COMMENT 'jpg, png, gif',
  `height` int(10) NOT NULL,
  `width` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`sales_setting_image_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `user` ADD `use_announcement` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'ホームページのお知らせバナー：0:使用しない、1:使用する' AFTER `use_port_plan`;

/* ポートアカウント判断 */
ALTER TABLE `user` ADD COLUMN `entry_mode` INT(11) NOT NULL DEFAULT '0' COMMENT '0:通常 1:ポート管理' AFTER `account_plan`;

/* ポート人数格納用 */
ALTER TABLE `reservation` ADD COLUMN `max_port` INT(11) NULL DEFAULT '0' AFTER `reservation_info`;

/* [MTGVFOUR-1188] 予約情報ポータルプッシュ機能 */
CREATE TABLE IF NOT EXISTS `reservation_push` (
  `reservation_push_key` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PRIMARY',
  `reservation_key` int(11) NOT NULL COMMENT 'FK',
  `vcube_id` varchar(255) NOT NULL COMMENT 'auth vid',
  `auth_token` varchar(255) NOT NULL COMMENT 'auth vid',
  `contract_id` int(11) NOT NULL COMMENT 'auth vid',
  `function` int(11) NOT NULL COMMENT '1:Add,2:Update,3:Delete',
  `error_count` int(11) NOT NULL DEFAULT 0,
  `is_pend` tinyint(1) NOT NULL DEFAULT 0,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`reservation_push_key`),
  INDEX reservation_key(`reservation_key`),
  INDEX error_count(`error_count`),
  INDEX update_datetime(`update_datetime`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

