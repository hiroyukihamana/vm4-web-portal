ALTER TABLE  `meeting` ADD  `temporary_did` VARCHAR( 50 ) NOT NULL AFTER  `ives_did` ,ADD INDEX ( temporary_did );

ALTER TABLE `reservation` DROP `polycom_temporary_address`;

ALTER TABLE  `meeting` CHANGE  `temporary_did`  `temporary_did` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

CREATE TABLE `video_conference_operation_log` (
`operation_key` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`operation_id` INT( 11 ) NOT NULL ,
`did` VARCHAR( 20 ) NOT NULL ,
`room_key` VARCHAR( 64 ) NOT NULL ,
`user_key` INT( 11 ) NOT NULL ,
`confid` VARCHAR( 64 ) NOT NULL ,
`result` BOOL NOT NULL ,
`operator` VARCHAR( 64 ) NOT NULL ,
`detail` TEXT NULL DEFAULT NULL ,
`datetime` DATETIME NOT NULL ,
INDEX (  `operation_id` )
) ENGINE = MYISAM;

ALTER TABLE  `room` ADD  `hd_flg` TINYINT NOT NULL DEFAULT  '0' COMMENT  'HDプランフラグ' AFTER  `disable_rec_flg`;
ALTER TABLE  `room` ADD  `default_h264_use_flg` TINYINT NOT NULL DEFAULT  '0' COMMENT  'h264利用フラグ' AFTER  `hd_flg`;
ALTER TABLE  `room` ADD  `default_agc_use_flg` TINYINT NOT NULL DEFAULT  '0' COMMENT  'AGC利用フラグ' AFTER  `default_h264_use_flg`;

ALTER TABLE  `ives_setting` CHANGE  `ives_did`  `ives_did` VARCHAR( 20 ) NOT NULL;

ALTER TABLE  `meeting` CHANGE  `ives_did`  `ives_did` VARCHAR( 20 ) NOT NULL;

ALTER TABLE  `participant` CHANGE  `ives_did`  `ives_did` VARCHAR( 20 ) NOT NULL;

ALTER TABLE  `video_conference` CHANGE  `did`  `did` VARCHAR( 20 ) NOT NULL;
