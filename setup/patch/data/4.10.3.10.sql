/* MTGVFOUR-2335 One 大量データ更新処理を別ロジック化 */
CREATE TABLE IF NOT EXISTS `one_user_queue` (
  `one_user_queue_key` INT(11) NOT NULL AUTO_INCREMENT,
  `user_key` INT(11) NOT NULL,
  `mode` INT(11) NOT NULL comment 'mode number => [ 0: update , 1: add , 2:delete ]',
  `error_count` INT(11) NULL,
  `pend` TINYINT(1) NULL,
  `create_detatime` DATETIME NULL,
  `update_datetime` DATETIME NULL,
  PRIMARY KEY (`one_user_queue_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;