
/* MTGVFOUR-1366 野良会議を考慮したポート制御 */
/*（即時会議の終了優先度格納） */

CREATE TABLE `entrymode_option` (
  `entrymode_option_key` INT(11) NULL AUTO_INCREMENT,
  `user_key` INT(11) NOT NULL,
  `target` VARCHAR(11) NOT NULL DEFAULT 'TIME',
  `sort` VARCHAR(4) NOT NULL DEFAULT 'DESC',
  `threshold` INT(1) NOT NULL DEFAULT '2',
  `entrymode_option_updatetime` DATETIME NULL,
  `create_datetime` DATETIME NULL,
  PRIMARY KEY (`entrymode_option_key`)
  );
