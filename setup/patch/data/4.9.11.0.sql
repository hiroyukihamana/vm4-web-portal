
ALTER TABLE `member` CHANGE `vcube_one_member_id` `vcube_one_member_id` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ONE MEMBER用カラム';
ALTER TABLE `member` ADD INDEX vcube_one_member_id ( `vcube_one_member_id` );

ALTER TABLE `fms_watch_status` ADD INDEX meeting_key ( `meeting_key` );
ALTER TABLE `fms_watch_status` ADD INDEX meeting_sequence_key ( `meeting_sequence_key` );
ALTER TABLE `member` ADD INDEX outbound_id ( `outbound_id` );
ALTER TABLE `member` ADD INDEX outbound_number_id ( `outbound_number_id` );
ALTER TABLE `member` ADD INDEX user_key ( `user_key` );
ALTER TABLE `document` ADD INDEX document_path ( `document_path` );
