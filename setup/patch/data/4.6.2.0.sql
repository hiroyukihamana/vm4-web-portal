ALTER TABLE  `user` ADD  `service_name` varchar(255) NOT NULL COMMENT  'custom_domain_key' AFTER  `has_chat`;
-- PGi
CREATE TABLE `pgi_setting` (
      `pgi_setting_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
      `room_key` varchar(64) NOT NULL COMMENT 'roomテーブルのプライマリキー',
      `system_key` varchar(32) DEFAULT NULL COMMENT 'pgiのシステムキー',
      `company_id` varchar(64) NOT NULL COMMENT 'CompanyID',
      `client_id` varchar(10) NOT NULL COMMENT 'ClientID',
      `client_pw` varchar(8) NOT NULL COMMENT 'ClientPW',
      `startdate` date DEFAULT NULL COMMENT '料金プランの開始月',
      `is_deleted` varchar(45) NOT NULL DEFAULT '0' COMMENT '',
      `registtime` datetime NOT NULL COMMENT '登録日',
      `updatetime` datetime DEFAULT NULL COMMENT '更新日',
      PRIMARY KEY (`pgi_setting_key`)
);
CREATE TABLE `pgi_rate` (
      `pgi_rate_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
      `pgi_setting_key` int(11) NOT NULL  COMMENT 'pgi_settingテーブルのキー',
      `rate` int(11) NOT NULL  COMMENT '料金',
      `rate_type` varchar(32) NOT NULL  COMMENT 'dnisの対応するキー',
      `is_deleted` varchar(45) NOT NULL DEFAULT '0' COMMENT '',
      `registtime` datetime NOT NULL COMMENT '登録日',
      `updatetime` datetime DEFAULT NULL COMMENT '更新日',
      PRIMARY KEY (`pgi_rate_key`)
);
--  room
ALTER TABLE  `room` ADD  `use_teleconf` tinyint(1) NOT NULL DEFAULT '0' COMMENT  '電話会議使用有無' AFTER `is_device_skip`;;
ALTER TABLE  `room` ADD  `use_pgi_dialin` tinyint(1) NOT NULL DEFAULT '0' COMMENT  'pgiダイヤルイン使用有無' AFTER `use_teleconf`;;
ALTER TABLE  `room` ADD  `use_pgi_dialin_free` tinyint(1) NOT NULL DEFAULT '0' COMMENT  'pgiダイヤルイン使用有無(フリーダイヤル)' AFTER `use_pgi_dialin`;
ALTER TABLE  `room` ADD  `use_pgi_dialout` tinyint(1) NOT NULL DEFAULT '0' COMMENT  'pgiダイヤルアウト(携帯)使用有無' AFTER `use_pgi_dialin_free`;

--  meeting
ALTER TABLE  `meeting` ADD  `pgi_setting_key` VARCHAR(20) NOT NULL COMMENT  'pgi_settingテーブルのプライマリキー' AFTER `eco_info`;
ALTER TABLE  `meeting` ADD  `pgi_conference_id` INT( 10 ) NOT NULL COMMENT  'PGI会議ID' AFTER `pgi_setting_key`;
ALTER TABLE  `meeting` ADD  `pgi_m_pass_code` VARCHAR( 15 ) NOT NULL COMMENT  '主催者用' AFTER `pgi_conference_id`;
ALTER TABLE  `meeting` ADD  `pgi_p_pass_code` VARCHAR( 15 ) NOT NULL COMMENT  '参加者用' AFTER `pgi_m_pass_code`;
ALTER TABLE  `meeting` ADD  `pgi_l_pass_code` VARCHAR( 15 ) NOT NULL COMMENT  'オーディエンス用' AFTER `pgi_p_pass_code`;
ALTER TABLE  `meeting` ADD  `pgi_phone_numbers` TEXT NOT NULL COMMENT  '複数保持可能（シリアライズ化）' AFTER `pgi_l_pass_code`;
ALTER TABLE  `meeting` ADD  `pgi_api_status` VARCHAR(15) NOT NULL COMMENT  'PGIのAPIアクセス管理' AFTER `pgi_phone_numbers`;
ALTER TABLE  `meeting` ADD  `tc_type` VARCHAR( 20 ) NOT NULL COMMENT  '音声通話タイプ  voip / teleconf / etc' AFTER `pgi_api_status`;
ALTER TABLE  `meeting` ADD  `tc_teleconf_note` TEXT NOT NULL COMMENT  'etc 時（自由入力）' AFTER `tc_type`;

ALTER TABLE  `meeting` ADD  `use_pgi_dialin` tinyint(1) NOT NULL DEFAULT '0' COMMENT  'pgiダイヤルイン使用有無' AFTER `tc_teleconf_note`;
ALTER TABLE  `meeting` ADD  `use_pgi_dialin_free` tinyint(1) NOT NULL DEFAULT '0' COMMENT  'pgiダイヤルイン使用有無(フリーダイヤル)' AFTER `use_pgi_dialin`;
ALTER TABLE  `meeting` ADD  `use_pgi_dialout` tinyint(1) NOT NULL DEFAULT '0' COMMENT  'pgiダイヤルアウト(携帯)使用有無' AFTER `use_pgi_dialin_free`;

--  user
ALTER TABLE  `user` ADD  `user_expiredatetime` DATETIME NOT NULL DEFAULT  '0000-00-00 00:00:00' COMMENT  '停止予定日' AFTER `user_updatetime`;