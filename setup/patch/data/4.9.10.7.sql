
ALTER TABLE  `ives_setting` ADD INDEX ( `room_key` );
ALTER TABLE `ordered_service_option` ADD INDEX(`room_key`);
ALTER TABLE `user` ADD INDEX(`user_id`);
ALTER TABLE `video_conference_participant` ADD INDEX(`meeting_key`);
ALTER TABLE `video_conference_participant` ADD INDEX(`video_conference_key`);
ALTER TABLE `video_conference_participant` ADD INDEX(`participant_key`);
ALTER TABLE `video_conference` ADD INDEX(`meeting_key`);
ALTER TABLE `video_conference` ADD INDEX(`room_key`);
