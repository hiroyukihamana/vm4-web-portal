-- 資料ユーザーの人数
ALTER TABLE  `room` ADD  `max_whiteboard_seat` INT NOT NULL DEFAULT  '0' COMMENT  'ホワイトボードユーザー' AFTER  `max_audience_seat`;
ALTER TABLE  `meeting` ADD  `meeting_max_whiteboard` INT NOT NULL DEFAULT  '0' COMMENT  'ホワイトボードユーザー' AFTER  `meeting_max_audience`;
