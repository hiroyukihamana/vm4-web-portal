--
-- メンバー部屋リレーションテーブル
--

CREATE TABLE  `member_room_relation` (
`member_room_relation_id` INT NOT NULL AUTO_INCREMENT ,
`member_key` INT NOT NULL ,
`room_key` VARCHAR( 64 ) NOT NULL ,
`create_datetime` DATETIME NOT NULL ,
PRIMARY KEY (  `member_room_relation_id` ) ,
INDEX (  `member_key` ,  `room_key` )
) ENGINE = MYISAM ;


ALTER TABLE  `user` ADD  `meeting_version` VARCHAR( 20 ) NULL DEFAULT NULL AFTER  `intra_fms`;
ALTER TABLE  `user` ADD  `meeting_limit_time` INT NOT NULL DEFAULT  '0' AFTER  `max_rec_size`;
ALTER TABLE  `room` ADD  `meeting_limit_time` INT NOT NULL DEFAULT  '0' AFTER  `rec_gw_userpage_setting_flg`;

--
-- IVeS関連
--

CREATE TABLE IF NOT EXISTS `ives_setting` (
`ives_setting_key` int(11) NOT NULL auto_increment COMMENT 'AUTO_INCREMENT',
`room_key` varchar(64) NOT NULL,
`ives_did` INT UNSIGNED NOT NULL,
`client_id` varchar(64) NOT NULL,
`client_pw` varchar(64) NOT NULL,
`startdate` datetime default NULL,
`is_deleted` tinyint(3) unsigned NOT NULL default '0',
`registtime` datetime NOT NULL,
`updatetime` datetime default NULL,
PRIMARY KEY  (`ives_setting_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
ALTER TABLE  `ives_setting` ADD  `sip_uid` INT NOT NULL COMMENT  'SIP用uid' AFTER  `ives_did`;

ALTER TABLE  `ives_setting` ADD  `user_key` INT( 11 ) NOT NULL AFTER  `ives_setting_key` ,ADD INDEX ( user_key );

ALTER TABLE `meeting` ADD `ives_did` INT unsigned NOT NULL;
ALTER TABLE `meeting` ADD `ives_conference_id` varchar(64) NOT NULL;
ALTER TABLE `meeting` ADD `ives_api_status` TINYINT( 11 ) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'default:0, create:1, complate:2, delete:3';
ALTER TABLE  `meeting` ADD  `need_fms_version` VARCHAR( 20 ) NOT NULL AFTER  `eco_info`;

ALTER TABLE `participant` ADD `ives_did` INT unsigned NOT NULL;
ALTER TABLE `participant` ADD `ives_conference_id` varchar(64) NOT NULL;
ALTER TABLE `participant` ADD `ives_part_id` INT unsigned NOT NULL;

ALTER TABLE  `reservation` ADD  `polycom_temporary_address` VARCHAR( 64 ) NOT NULL AFTER  `is_wb_no_flg` ,
ADD INDEX ( polycom_temporary_address );

CREATE TABLE IF NOT EXISTS `video_conference` (
  `video_conference_key` int(11) NOT NULL auto_increment,
  `user_key` int(11) NOT NULL,
  `room_key` varchar(64) NOT NULL,
  `conference_id` varchar(64) NOT NULL,
  `did` int(10) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `createtime` datetime NOT NULL,
  `starttime` datetime NOT NULL,
  `updatetime` datetime NOT NULL,
  PRIMARY KEY  (`video_conference_key`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

ALTER TABLE `room` ADD `default_microphone_mute` VARCHAR( 20 ) NOT NULL DEFAULT 'off' COMMENT 'マイクのデフォルトmuteフラグ' AFTER `default_layout_16to9`;
ALTER TABLE `room` ADD `default_camera_mute` VARCHAR( 20 ) NOT NULL DEFAULT 'off' COMMENT 'カメラのデフォルトmuteフラグ' AFTER `default_microphone_mute`;
ALTER TABLE  `room` ADD  `default_camera_size` VARCHAR( 64 ) NULL DEFAULT NULL COMMENT 'カメラのデフォルトサイズ' AFTER  `default_camera_mute`;
ALTER TABLE  `room` ADD  `disable_rec_flg` TINYINT NOT NULL DEFAULT  '0' COMMENT  '録画不可フラグ' AFTER  `default_camera_size`;

ALTER TABLE `room`  ADD `max_room_bandwidth` INT NOT NULL DEFAULT '2048' COMMENT '部屋上限帯域' AFTER `room_sort`,  ADD `max_user_bandwidth` INT NOT NULL DEFAULT '256' COMMENT '拠点上限帯域' AFTER `max_room_bandwidth`,  ADD `min_user_bandwidth` INT NOT NULL DEFAULT '30' COMMENT '拠点下限限帯域' AFTER `max_user_bandwidth`;

ALTER TABLE  `meeting` ADD  `need_fms_version` VARCHAR( 20 ) NOT NULL COMMENT  '必要FMSバージョン' AFTER  `eco_info`;
ALTER TABLE  `meeting` ADD INDEX (  `user_key` );
