ALTER TABLE  `participant` ADD  `participant_tag` VARCHAR( 255 ) NULL DEFAULT NULL COMMENT  'API連携用識別Tag' AFTER  `participant_skin_type` ,ADD INDEX ( participant_tag );
ALTER TABLE `ordered_service_option` ADD INDEX(`room_key`);
ALTER TABLE `user` ADD INDEX(`user_id`);
ALTER TABLE `video_conference_participant` ADD INDEX(`meeting_key`);
ALTER TABLE `video_conference_participant` ADD INDEX(`video_conference_key`);
ALTER TABLE `video_conference_participant` ADD INDEX(`participant_key`);
ALTER TABLE `video_conference` ADD INDEX(`meeting_key`);
ALTER TABLE `video_conference` ADD INDEX(`room_key`);