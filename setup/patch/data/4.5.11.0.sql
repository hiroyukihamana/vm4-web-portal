-- シーケンステーブルの入退出時刻を記録
ALTER TABLE `meeting_sequence` ADD `start_datetime` DATETIME NULL DEFAULT NULL COMMENT '開始日時' AFTER `has_recorded_video`;
ALTER TABLE `meeting_sequence` ADD `end_datetime` DATETIME NULL DEFAULT NULL COMMENT '更新日時' AFTER `start_datetime`;
