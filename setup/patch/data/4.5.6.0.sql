-- デフォルトレイアウト
ALTER TABLE `room` CHANGE `default_layout` `default_layout_4to3` VARCHAR( 20 ) DEFAULT NULL COMMENT '4:3デフォルトレイアウト';
ALTER TABLE `room` ADD `default_layout_16to9` VARCHAR( 20 ) DEFAULT NULL COMMENT '16:9デフォルトレイアウト' AFTER `default_layout_4to3`;

-- 部屋の設定
ALTER TABLE `room` ADD `is_wb_no` INT(1) NOT NULL DEFAULT '1' COMMENT  'ホワイトボードページ番号表示' AFTER `is_netspeed_check`;
ALTER TABLE `room` ADD `is_device_skip` INT(1) NOT NULL DEFAULT '0' COMMENT  '入室時のデバイス選択スキップ' AFTER `is_wb_no`;

-- ページ番号の非表示設定（予約）
ALTER TABLE `reservation` ADD `is_wb_no_flg` TINYINT( 4 ) DEFAULT '1' AFTER `is_cabinet_flg`;

-- 操作ログ
ALTER TABLE `operation_log` ADD `session_id` varchar(200) DEFAULT NULL COMMENT 'セッションID' AFTER `member_id`;

-- グループ課金
ALTER TABLE `user_group` ADD `basic_fee` BIGINT NOT NULL COMMENT '基本料金' AFTER `group_name`;
ALTER TABLE `user_group` ADD `running_fee` INT NOT NULL COMMENT '追加料金(分)' AFTER `basic_fee`;
ALTER TABLE `user_group` ADD `limit_fee` BIGINT NOT NULL COMMENT '上限料金' AFTER `running_fee`;
ALTER TABLE `user_group` ADD `free_time` INT NOT NULL COMMENT '無料通話時間(分)' AFTER `limit_fee`;

-- ユーザグループのインデックス
ALTER TABLE  `user` ADD INDEX (  `user_group` );
