ALTER TABLE  `room` ADD  `mobile_4x4_layout` INT( 1 ) NOT NULL DEFAULT  '0' AFTER  `mobile_mix` ,
ADD INDEX ( 4x4layout );

ALTER TABLE  `video_conference_participant` ADD  `media_mixer_key` INT( 10 ) NOT NULL AFTER  `video_conference_key` ,
ADD INDEX ( media_mixer_key );

ALTER TABLE  `video_conference` ADD  `media_mixer_key` INT( 10 ) NOT NULL AFTER  `mcu_server_key` ,
ADD INDEX ( media_mixer_key );

ALTER TABLE  `video_conference_participant` ADD  `participant_session_id` VARCHAR( 50 ) NOT NULL AFTER  `did` ,
ADD INDEX ( participant_session_id );
