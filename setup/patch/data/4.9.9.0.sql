/*アクティブスピーカーモード*/
ALTER TABLE  `room` ADD `active_speaker_mode_only_flg` tinyint(4) NOT NULL COMMENT '強制アクティブスピーカーモードフラグ(1だとactive_speaker_mode_use_flgが切り替えが出来ずONになる)' DEFAULT 0 AFTER  `print_allow_flg`;
ALTER TABLE  `room` ADD `active_speaker_mode_use_flg` tinyint(4) NOT NULL COMMENT 'アクティブスピーカーモード' DEFAULT 0 AFTER  `active_speaker_mode_only_flg`;
ALTER TABLE  `room` ADD `active_speaker_mode_user_count` integer NULL COMMENT 'アクティブスピーカーモード同時表示拠点数' DEFAULT 0 AFTER  `active_speaker_mode_use_flg`;
ALTER TABLE  `room` ADD `active_speaker_mode_speaker_count` integer NULL COMMENT 'アクティブスピーカーモード同時発言拠点数' DEFAULT 0 AFTER  `active_speaker_mode_use_flg`;
ALTER TABLE  `room` ADD `active_speaker_default_layout_4to3` varchar(20) NULL COMMENT '4:3アクティブスピーカーモードデフォルトレイアウト'  AFTER  `active_speaker_mode_user_count`;
ALTER TABLE  `room` ADD `active_speaker_default_layout_16to9` varchar(20) NULL COMMENT '16:9アクティブスピーカーモードデフォルトレイアウト'  AFTER  `active_speaker_default_layout_4to3`;


/*VMTG-2093 開発＿【カラム追加】電話会議＿ローコール*/
ALTER TABLE  `room` ADD `use_pgi_dialin_lo_call` tinyint(4) NOT NULL COMMENT 'ダイヤルイン ローコール使用有無' DEFAULT 0 AFTER  `use_pgi_dialin_free`;
ALTER TABLE  `meeting` ADD `use_pgi_dialin_lo_call` tinyint(4) NOT NULL COMMENT 'ダイヤルイン ローコール使用有無' DEFAULT 0 AFTER  `use_pgi_dialin_free`;
UPDATE `room` SET use_pgi_dialin_lo_call = 1 WHERE `use_pgi_dialin` = 1 AND `use_teleconf` = 1 AND `room_status` = 1;

/* カラム名変更 */
ALTER TABLE user CHANGE teleconference_flg promotion_flg tinyint(4) NOT NULL COMMENT 'プロモーションモーダル画面表示フラグ' DEFAULT 0;

/*MCU HD対応用管理者ページでの切り替え機能*/
ALTER TABLE `ives_setting` ADD `mcu_hd_flg` tinyint(1) NOT NULL COMMENT 'MCU HD FlAG' DEFAULT 0 AFTER `use_active_speaker`;

/*PGi 会議室内ボタンにログを仕込み*/
ALTER TABLE `meeting_function_count_log` ADD `dial_in_button` int(4) DEFAULT '0' COMMENT 'ダイヤルイン' AFTER `telephony_button`;
ALTER TABLE `meeting_function_count_log` ADD `dial_out_button` int(4) DEFAULT '0' COMMENT 'ダイヤルアウト' AFTER `dial_in_button`;
ALTER TABLE `meeting_function_count_log` ADD `dial_out_call_button` int(4) DEFAULT '0' COMMENT '発信' AFTER `dial_out_button`;
ALTER TABLE `meeting_function_count_log` ADD `eject_telephony_user_button` int(4) DEFAULT '0' COMMENT '退室' AFTER `dial_out_call_button`;
ALTER TABLE `meeting_function_count_log` ADD `eject_all_telephony_user_button` int(4) DEFAULT '0' COMMENT '全員退室' AFTER `eject_telephony_user_button`;

/* 以下ONE関係のSQL */
/* userテーブルカラム追加  */
ALTER TABLE  `user` ADD `is_add_room` tinyint(4) DEFAULT '0' COMMENT '会議室をユーザーが追加/削除出来るユーザーフラグ' AFTER  `support_info_type`;
ALTER TABLE  `user` ADD `is_one_time_meeting` tinyint(4) DEFAULT '0' COMMENT '会議室なくても会議を行えるユーザーフラグ' AFTER  `is_add_room`;
ALTER TABLE  `user` ADD `account_plan` varchar(20) DEFAULT NULL COMMENT '登録したアカウントプランを記録するため' AFTER  `account_model`;
/* ONE TIME MEETING用 カラムをroomテーブルに追加 */
ALTER TABLE  `room` ADD `is_one_time_meeting` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ワンタイムミーティング用の会議室フラグ' AFTER  `meeting_limit_time`;
ALTER TABLE  `room` ADD `life_date_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT 'ワンタイムミーティング用の会議室削除されるまでの時間' AFTER  `room_expiredatetime`;
ALTER TABLE  `meeting` ADD `is_one_time_meeting` tinyint(4) COMMENT 'ワンタイムミーティングフラグ' DEFAULT 0 ;
/* ONE MEMBER 用　カラムを追加 */
ALTER TABLE  `member` ADD `vcube_one_member_id` text DEFAULT NULL COMMENT 'ONE MEMBER用カラム' AFTER  `outbound_number_id`;

/* user_room_setting 内容はroomテーブルとほぼ同じ */
CREATE TABLE IF NOT EXISTS `user_room_setting` (
  `user_room_setting_key` int(11) NOT NULL AUTO_INCREMENT,
  `max_seat` int(11) DEFAULT '0' COMMENT '最大参加者数',
  `max_audience_seat` int(11) DEFAULT '0' COMMENT '最大オーディエンス数',
  `max_whiteboard_seat` int(11) NOT NULL DEFAULT '0' COMMENT 'ホワイトボードユーザー',
  `max_guest_seat` int(11) DEFAULT NULL COMMENT '最大招待者数',
  `max_guest_seat_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '最大招待者設定フラグ 1:ON 0:OFF',
  `extend_max_seat` int(11) NOT NULL,
  `extend_max_audience_seat` int(11) NOT NULL,
  `extend_seat_flg` int(11) NOT NULL,
  `user_key` int(11) NOT NULL DEFAULT '0',
  `room_name` text,
  `room_layout_type` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `max_room_bandwidth` int(11) NOT NULL DEFAULT '2048' COMMENT '部屋上限帯域',
  `max_user_bandwidth` int(11) NOT NULL DEFAULT '256' COMMENT '拠点上限帯域',
  `min_user_bandwidth` int(11) NOT NULL DEFAULT '30' COMMENT '拠点下限限帯域',
  `cabinet` tinyint(4) NOT NULL DEFAULT '1',
  `max_shared_memo_size` int(11) DEFAULT '100000' COMMENT '共有メモ最大サイズ',
  `mfp` varchar(10) DEFAULT 'all',
  `default_layout_4to3` varchar(20) DEFAULT NULL COMMENT '4:3デフォルトレイアウト',
  `default_layout_16to9` varchar(20) DEFAULT NULL COMMENT '16:9デフォルトレイアウト',
  `default_microphone_mute` varchar(20) NOT NULL DEFAULT 'off' COMMENT 'カメラ・マイクのデフォルトmuteフラグ',
  `default_camera_mute` varchar(20) NOT NULL DEFAULT 'off' COMMENT 'カメラのデフォルトmuteフラグ',
  `default_camera_size` varchar(64) DEFAULT NULL COMMENT 'カメラのデフォルトサイズ',
  `default_auto_rec_use_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '自動録画フラグ',
  `disable_rec_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '録画不可フラグ',
  `is_convert_wb_to_pdf` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'WBのPDF化デフォルトフラグ',
  `is_personal_wb` tinyint(4) NOT NULL DEFAULT '1' COMMENT '個人ホワイトボード：0:使用しない、1:使用する',
  `invited_limit_time` int(11) NOT NULL DEFAULT '0' COMMENT '時間制限を適用する場合は分をいれる',
  `hd_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'HDプランフラグ',
  `default_h264_use_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'h264利用フラグ',
  `default_agc_use_flg` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'AGC利用フラグ',
  `use_sound_codec` varchar(64) DEFAULT NULL COMMENT '利用sound codec',
  `whiteboard_page` int(11) DEFAULT '100' COMMENT '資料枚数',
  `whiteboard_size` int(11) DEFAULT '20' COMMENT '資料サイズ',
  `whiteboard_filetype` text COMMENT '資料ファイルタイプ',
  `cabinet_size` int(11) DEFAULT '20' COMMENT 'キャビネットサイズ',
  `cabinet_filetype` text COMMENT '資料ファイルタイプ',
  `is_auto_transceiver` int(1) DEFAULT '0' COMMENT 'トランシーバーモード',
  `transceiver_number` int(11) NOT NULL COMMENT '切り替え拠点数',
  `is_auto_voice_priority` int(1) DEFAULT '0' COMMENT '音声優先モード',
  `is_netspeed_check` varchar(10) DEFAULT '' COMMENT '回戦速度チェック',
  `is_wb_no` int(1) NOT NULL DEFAULT '1' COMMENT 'ホワイトボードページ番号表示',
  `is_device_skip` int(1) NOT NULL DEFAULT '0' COMMENT '入室時のデバイス選択スキップ',
  `is_participant_notifier` tinyint(4) NOT NULL DEFAULT '1' COMMENT '参加者の入退室状況バルーン 0:非表示、1:表示',
  `use_teleconf` tinyint(1) NOT NULL DEFAULT '0' COMMENT '電話会議使用有無',
  `use_pgi_dialin` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pgiダイヤルイン使用有無',
  `use_pgi_dialin_free` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pgiダイヤルイン使用有無(フリーダイヤル)',
  `use_pgi_dialin_lo_call` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ダイヤルイン ローコール使用有無',
  `use_pgi_dialout` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pgiダイヤルアウト(携帯)使用有無',
  `use_sales_option` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'セールスオプション利用フラグ',
  `use_extend_bandwidth` varchar(64) DEFAULT NULL COMMENT '利用帯域追加機能',
  `prohibit_extend_meeting_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '会議延長禁止フラグ',
  `audience_chat_flg` tinyint(1) DEFAULT '0' COMMENT 'オーディエンスのチャットフラグ',
  `outbound_id` varchar(64) DEFAULT NULL COMMENT 'アウトバウンドURL利用ID',
  `inbound_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'インバウンド利用フラグ',
  `inbound_id` varchar(255) DEFAULT NULL COMMENT 'インバウンドID',
  `reenter_flg` tinyint(1) DEFAULT '0' COMMENT 'インバウンド優先再入室グラグ',
  `wb_allow_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ホワイトボード操作の初期設定フラグ',
  `chat_allow_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'チャット操作の初期設定フラグ',
  `print_allow_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '印刷操作の初期設定フラグ',
  `active_speaker_mode_only_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '強制アクティブスピーカーモードフラグ(1だとactive_speaker_mode_use_flgが切り替えが出来ずONになる)',
  `active_speaker_mode_use_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'アクティブスピーカーモード',
  `active_speaker_mode_speaker_count` int(11) DEFAULT '0' COMMENT 'アクティブスピーカーモード同時発言拠点数',
  `active_speaker_mode_user_count` int(11) DEFAULT '0' COMMENT 'アクティブスピーカーモード同時表示拠点数',
  `active_speaker_default_layout_4to3` varchar(20) DEFAULT NULL COMMENT '4:3アクティブスピーカーモードデフォルトレイアウト',
  `active_speaker_default_layout_16to9` varchar(20) DEFAULT NULL COMMENT '16:9アクティブスピーカーモードデフォルトレイアウト',
  `customer_camera_use_flg` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'お客様側カメラの利用0:禁止、1:許可',
  `customer_camera_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'お客様側映像 0:非表示、1:表示',
  `customer_sharing_button_hide_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'お客様側デスクトップ共有ボタン',
  `ignore_staff_reenter_alert` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'スタッフ再入室アラート非表示フラグ',
  `room_image` int(11) NOT NULL DEFAULT '0',
  `room_password` varchar(16) DEFAULT NULL,
  `addition` text,
  `comment` text COMMENT 'アウトバウンドページ',
  `rtmp_protocol` text COMMENT 'RTMPプロトコル',
  `rec_gw_convert_type` varchar(64) NOT NULL DEFAULT 'mov' COMMENT '録画GW変換形式',
  `rec_gw_userpage_setting_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '録画GWをユーザーページで利用可否フラグ',
  `meeting_limit_time` int(11) NOT NULL DEFAULT '0',
  `mobile_mix` int(11) NOT NULL DEFAULT '1' COMMENT 'MCUを利用したMobileMix機能の利用可／不可フラグ',
  `create_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `delete_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `expire_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_room_setting_key`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/* user_ives_setting 内容はives_settinテーブルとほぼ同じ(room_keyがない) */
CREATE TABLE IF NOT EXISTS `user_ives_setting` (
  `user_ives_setting_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'AUTO_INCREMENT',
  `mcu_server_key` int(3) NOT NULL,
  `user_key` int(11) NOT NULL,
  `ives_did` varchar(20) NOT NULL,
  `sip_uid` int(11) NOT NULL COMMENT 'SIP用uid',
  `client_id` varchar(64) NOT NULL,
  `client_pw` varchar(64) NOT NULL,
  `profile_id` int(10) NOT NULL COMMENT '課金タイプ, 社内テスト用: 281, デモ用: 321, 課金用: 323',
  `num_profiles` int(10) NOT NULL DEFAULT '0' COMMENT '課金タイプの申し込み数',
  `use_active_speaker` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'アクティブスピーカー使用/不使用フラグ',
  `mcu_hd_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'MCU HD FlAG',
  `ignore_video_conference_address` tinyint(1) NOT NULL DEFAULT '0',
  `startdate` datetime DEFAULT NULL,
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`user_ives_setting_key`),
  KEY `user_key` (`user_key`),
  KEY `mcu_server_key` (`mcu_server_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


/* ここから下はONE PGI関係の設定 */
/* ユーザーにも持たせる */
CREATE TABLE IF NOT EXISTS `user_pgi_setting` (
  `user_pgi_setting_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `user_key` int(11) NOT NULL COMMENT 'userキー',
  `system_key` varchar(32) DEFAULT NULL COMMENT 'pgiのシステムキー',
  `company_id` varchar(64) NOT NULL COMMENT 'CompanyID',
  `client_id` varchar(10) NOT NULL COMMENT 'ClientID',
  `client_pw` varchar(8) NOT NULL COMMENT 'ClientPW',
  `admin_client_id` varchar(64) DEFAULT NULL,
  `admin_client_pw` varchar(64) DEFAULT NULL,
  `startdate` date DEFAULT NULL COMMENT '料金プランの開始月',
  `is_deleted` varchar(45) NOT NULL DEFAULT '0',
  `registtime` datetime NOT NULL COMMENT '登録日',
  `updatetime` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`user_pgi_setting_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `user_pgi_rate` (
  `user_pgi_rate_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `user_pgi_setting_key` int(11) NOT NULL COMMENT 'user_pgi_settingテーブルのキー',
  `rate` int(11) NOT NULL COMMENT '料金',
  `rate_type` varchar(32) NOT NULL COMMENT 'dnisの対応するキー',
  `is_deleted` varchar(45) NOT NULL DEFAULT '0',
  `registtime` datetime NOT NULL COMMENT '登録日',
  `updatetime` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`user_pgi_rate_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

/* STB用 */
ALTER TABLE  `user` ADD  `use_stb` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'STB利用フラグ' AFTER  `use_sales`;
ALTER TABLE  `room` ADD  `use_stb_option` INT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'STB利用フラグ' AFTER  `use_sales_option`;
ALTER TABLE  `member` ADD  `use_stb` INT( 11 ) NOT NULL DEFAULT  '0' COMMENT  'STB利用フラグ' AFTER  `use_sales`;
ALTER TABLE  `meeting` ADD  `use_stb_option` INT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'STB利用フラグ' AFTER  `use_sales_option`;
CREATE TABLE `meeting_invitation_user` (
  `meeting_invitation_user_key` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_key` int(11) NOT NULL COMMENT '会議キー',
  `meeting_session_id` varchar(255) NOT NULL COMMENT '会議session ID',
  `sender_member_id` varchar(255) DEFAULT NULL COMMENT '送信者のmember_id',
  `invitee_member_id` varchar(255) DEFAULT NULL COMMENT '招待されたのEmail',
  `invitee_email` varchar(255) DEFAULT NULL COMMENT '招待されたのmember_id',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: 無効, 1: 有効',
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`meeting_invitation_user_key`),
  KEY `meeting_key` (`meeting_key`),
  KEY `meeting_session_id` (`meeting_session_id`),
  KEY `sender_member_id` (`sender_member_id`),
  KEY `invitee_member_id` (`invitee_member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE  `pgi_setting` CHANGE  `client_pw`  `client_pw` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT  'ClientPW';