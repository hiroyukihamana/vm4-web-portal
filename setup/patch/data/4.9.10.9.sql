
/* [MTGVFOUR-530] 会議内招待ボタン非表示 */
ALTER TABLE `room` ADD `is_invite_button` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '会議内招待：0:使用しない、1:使用する' AFTER `is_convert_personal_wb_to_pdf`;
ALTER TABLE `user_room_setting` ADD `is_invite_button` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '会議内招待：0:使用しない、1:使用する' AFTER `is_convert_personal_wb_to_pdf`;
