ALTER TABLE  `user` ADD  `lang_allow` varchar(100) NULL COMMENT  '言語許可リスト(「,」区切りで指定nullの場合は全許可)' AFTER  `meeting_version`;
ALTER TABLE  `room` ADD  `max_guest_seat` int(11) NULL COMMENT  '最大招待者数' AFTER  `max_whiteboard_seat`;
ALTER TABLE  `room` ADD  `max_guest_seat_flg` tinyint(4) NOT NULL DEFAULT 0 COMMENT  '最大招待者設定フラグ 1:ON 0:OFF' AFTER  `max_guest_seat`;

ALTER TABLE `participant` ADD `user_key` int(11) NULL AFTER  `meeting_key`;
ALTER TABLE `participant` ADD KEY `user_key` (`user_key`);