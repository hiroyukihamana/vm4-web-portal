--
-- テーブルの構造 `fms_deny`
--

CREATE TABLE `fms_deny` (
  `fms_deny_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'KEY',
  `user_key` int(11) NOT NULL,
  `fms_server_key` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: 有効, 0: 無効',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`fms_deny_key`),
  KEY `user_key` (`user_key`),
  KEY `fms_server_key` (`fms_server_key`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;