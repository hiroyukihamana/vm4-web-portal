/*電話連携デフォルト化についての事前準備*/
ALTER TABLE  `user` ADD `teleconference_flg` tinyint(4) NOT NULL COMMENT '電話連携使用可否フラグ' DEFAULT 0 AFTER  `invoice_flg`;

/* ワンタイムURL */
CREATE TABLE `onetime_url` (
`onetime_url_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ワンタイムURLKEY',
`onetime_session_id` varchar(64) NOT NULL COMMENT  'ワンタイムURLセッション',
`user_key` int(11) NOT NULL COMMENT  'ユーザーキー',
`meeting_key` int(11) NOT NULL COMMENT  '会議キー',
`meeting_type` varchar(64) NOT NULL COMMENT  '',
`participant_key` int(11) NOT NULL COMMENT  '',
`participant_name` varchar(255) DEFAULT NULL COMMENT  '',
`participant_type_name` varchar(255) DEFAULT NULL COMMENT  '',
`participant_lang` varchar(11) DEFAULT NULL COMMENT  '',
`participant_place` varchar(11) DEFAULT NULL COMMENT  '',
`participant_timezone` varchar(11) DEFAULT NULL COMMENT  '',
`fl_ver` varchar(64) NOT NULL COMMENT  '',
`display_size` varchar(64) NOT NULL COMMENT  '',
`tag` varchar(255) DEFAULT NULL COMMENT  '個人識別子',
`is_used_flg` int(11) NOT NULL DEFAULT 0 COMMENT  '利用したかとうかフラグ',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`use_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`onetime_url_key`),
KEY `user_key` (`user_key`),
KEY `meeting_key` (`meeting_key`),
KEY `participant_key` (`participant_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;