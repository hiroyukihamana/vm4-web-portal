ALTER TABLE  `meeting_suggest_log` CHANGE COLUMN `create_datetime` `suggest_time` DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00";
ALTER TABLE  `meeting_suggest_log` ADD `create_datetime` DATETIME NOT NULL COMMENT 'DBに入れる時間' DEFAULT  "0000-00-00 00:00:00" AFTER  `suggest_time`;

/* 外部ストレージ連携用API */
ALTER TABLE  `storage_file` ADD `external_flg` tinyint(4) NOT NULL COMMENT '外部ストレージサービス連携ファイルフラグ' DEFAULT  "0" AFTER  `last_update_member_key`;
ALTER TABLE  `storage_file` ADD `external_service_file_key` varchar(255)  NULL COMMENT '外部ストレージサービス固有キー' DEFAULT  null AFTER  `external_flg`;

ALTER TABLE  `storage_folder` ADD `external_flg` tinyint(4) NOT NULL COMMENT '外部ストレージサービス連携ファイルフラグ' DEFAULT  "0" AFTER  `status`;
ALTER TABLE  `storage_folder` ADD `external_service_folder_key` varchar(255)  NULL COMMENT '外部ストレージサービス固有キー' DEFAULT  null AFTER  `external_flg`;
ALTER TABLE  `storage_folder` ADD `external_service_account_key` varchar(255)  NULL COMMENT '外部ストレージサービスアカウント情報' DEFAULT  null AFTER  `external_service_folder_key`;
ALTER TABLE  `storage_folder` ADD `external_service_name` varchar(255) NULL COMMENT '外部ストレージ名' DEFAULT  null AFTER  `external_service_account_key`;

ALTER TABLE  `address_book` ADD INDEX ( `user_key` );
ALTER TABLE  `address_book` ADD INDEX ( `member_key` );
ALTER TABLE  `address_book` ADD INDEX ( `email` );