-- 請求月
ALTER TABLE `user` ADD `payment_terms` VARCHAR( 10 ) NULL DEFAULT 'post' COMMENT 'pre: 前請求 / on: 当月請求 / post: 後請求' AFTER `invoice_flg` ;

-- 部屋プラン
ALTER TABLE `room_plan` ADD `discount_rate` INT( 11 ) NOT NULL DEFAULT '0' COMMENT '割引率' AFTER `service_key` ;
ALTER TABLE `room_plan` ADD `contract_month_number` INT( 11 ) NOT NULL DEFAULT '0' COMMENT '契約期間（月数）' AFTER `discount_rate` ;
ALTER TABLE `room_plan` ADD `room_plan_endtime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'プラン終了日' AFTER `room_plan_starttime` ;

-- ユーザープラン
ALTER TABLE `user_plan` ADD `discount_rate` INT( 11 ) NOT NULL DEFAULT '0' COMMENT '割引率' AFTER `service_key`;
ALTER TABLE `user_plan` ADD `contract_month_number` INT( 11 ) NOT NULL DEFAULT '0' COMMENT '契約期間（月数）' AFTER `discount_rate` ;
ALTER TABLE `user_plan` ADD `user_plan_endtime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'プラン終了日' AFTER `user_plan_starttime` ;