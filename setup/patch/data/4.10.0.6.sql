/* MTGVFOUR-1315 【One】新プランのV4mtg対応 */
ALTER TABLE `user` ADD COLUMN `max_room_count` int(11) NOT NULL DEFAULT '0' COMMENT  '部屋の最大数' AFTER `max_member_count`;
