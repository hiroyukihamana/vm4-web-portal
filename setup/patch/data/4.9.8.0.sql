/*議長権*/
ALTER TABLE  `reservation` ADD `is_authority_flg` integer NOT NULL COMMENT '議長設定フラグ' DEFAULT 0 AFTER  `is_reminder_send_flg`;
ALTER TABLE  `reservation_user` ADD `r_user_authority` integer NOT NULL COMMENT '予約招待議長フラグ' DEFAULT 0 AFTER  `r_user_audience`;
ALTER TABLE  `meeting_function_count_log` ADD `authority_button` int(4) DEFAULT '0' COMMENT  '議長権フラグ' AFTER  `mcu_document_share_button`;
ALTER TABLE  `meeting_invitation` ADD `authority_session_id` varchar(255) DEFAULT NULL COMMENT  '招待議長セッションID' AFTER  `audience_session_id`;
