ALTER TABLE `ping_failed` ADD INDEX ( `p_session` ) ;
ALTER TABLE `participant` ADD INDEX ( `create_datetime` ) ;

-- 再入室時の状態
ALTER TABLE `ping_failed` ADD `reload_type` VARCHAR( 20 ) NOT NULL COMMENT 'リロードタイプ' AFTER `type`;
ALTER TABLE `ping_failed` ADD `p_login_time` DATETIME NOT NULL COMMENT '入室時間' AFTER `sequence_key`;
ALTER TABLE `ping_failed` ADD INDEX ( `reload_type` ) ;
