--
-- テーブルの構造 `ping_failed`
--

CREATE TABLE IF NOT EXISTS `ping_failed` (
  `ping_failed_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '連番',
  `type` varchar(20) NOT NULL COMMENT 'タイプ',
  `fms_key` int(11) NOT NULL COMMENT 'FMS',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーKEY',
  `meeting_key` int(11) NOT NULL COMMENT '会議キー',
  `sequence_key` int(11) NOT NULL COMMENT '会議シーケンス',
  `p_time` datetime NOT NULL COMMENT '時間',
  `p_session` varchar(255) NOT NULL COMMENT '参加者KEY',
  `p_protocol` varchar(10) NOT NULL COMMENT 'プロトコル',
  `p_port` int(11) NOT NULL COMMENT 'ポート',
  `p_number` int(11) NOT NULL COMMENT '参加人数',
  `p_count` int(11) NOT NULL COMMENT '連続失敗回数',
  `fms_bytes_in` int(11) DEFAULT NULL COMMENT '受信した合計バイト数',
  `fms_bytes_out` int(11) DEFAULT NULL COMMENT '送信した合計バイト数',
  `fms_msg_in` int(11) DEFAULT NULL COMMENT '受信RTMPメッセージ合計数',
  `fms_msg_out` int(11) DEFAULT NULL COMMENT '送信RTMPメッセージ合計数',
  `fms_msg_dropped` int(11) DEFAULT NULL COMMENT '欠落RTMPメッセージ合計数',
  `fms_ping_rtt` int(11) DEFAULT NULL COMMENT 'pingメッセージに応答するまでの時間',
  `fms_audio_queue_msgs` int(11) DEFAULT NULL COMMENT 'オーディオメッセージ数',
  `fms_video_queue_msgs` int(11) DEFAULT NULL COMMENT 'ビデオメッセージ数',
  `fms_so_queue_msgs` int(11) DEFAULT NULL COMMENT '共有オブジェクトメッセージ数',
  `fms_data_queue_msgs` int(11) DEFAULT NULL COMMENT 'データメッセージ数',
  `fms_dropped_audio_msgs` int(11) DEFAULT NULL COMMENT '欠落オーディオメッセージ数',
  `fms_dropped_video_msgs` int(11) DEFAULT NULL COMMENT '欠落ビデオメッセージ数',
  `fms_audio_queue_bytes` int(11) DEFAULT NULL COMMENT 'オーディオメッセージサイズ',
  `fms_video_queue_bytes` int(11) DEFAULT NULL COMMENT 'ビデオメッセージサイズ',
  `fms_so_queue_bytes` int(11) DEFAULT NULL COMMENT '共有オブジェクトメッセージサイズ',
  `fms_data_queue_bytes` int(11) DEFAULT NULL COMMENT 'データメッセージサイズ',
  `fms_dropped_audio_bytes` int(11) DEFAULT NULL COMMENT '欠落オーディオメッセージサイズ',
  `fms_dropped_video_bytes` int(11) DEFAULT NULL COMMENT '欠落ビデオメッセージサイズ',
  `fms_bw_out` int(11) DEFAULT NULL COMMENT '上り帯域幅',
  `fms_bw_in` int(11) DEFAULT NULL COMMENT '下り帯域幅',
  `fms_client_id` varchar(200) DEFAULT NULL COMMENT '固有のID',
  `create_datetime` datetime NOT NULL COMMENT '作成日',
  PRIMARY KEY (`ping_failed_id`),
  KEY `meeting_key` (`meeting_key`),
  KEY `sequence_key` (`sequence_key`),
  KEY `type` (`type`),
  KEY `fms_key` (`fms_key`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

ALTER TABLE `room` ADD `transceiver_number` INT NOT NULL COMMENT '切り替え拠点数' AFTER `is_auto_transceiver`;