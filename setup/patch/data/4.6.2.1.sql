CREATE TABLE `answersheet` (
  `answersheet_id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) NOT NULL,
  `participant_key` int(11) NOT NULL,
  `participant_name` varchar(64) DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY (`answersheet_id`),
  KEY `questionnaire_id` (`questionnaire_id`,`participant_key`)
) ENGINE=MyISAM;

CREATE TABLE `answer` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `answersheet_id` varchar(11) NOT NULL,
  `question_id` varchar(11) NOT NULL,
  `comment` text,
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY (`answer_id`),
  KEY `answersheet_id` (`answersheet_id`,`question_id`)
) ENGINE=MyISAM;

CREATE TABLE `answer_branch` (
  `answer_branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question_branch_id` int(11) NOT NULL,
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY (`answer_branch_id`),
  KEY `answer_id` (`answer_id`,`question_id`,`question_branch_id`)
) ENGINE=MyISAM;