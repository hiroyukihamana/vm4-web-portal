/* MTGVFOUR-827【S&S】Web_マネージャー機能の追加 */
/* セールスの管理者用：非One時に使用する、マネージャー判断用のカラム追加 */
ALTER TABLE `member` ADD COLUMN  `use_ss_watcher` TINYINT(1) NULL DEFAULT '0'  COMMENT  'S&S 監視者権限格納' AFTER `use_shared_storage`;

/* セールスの管理者用：マネージャー用のシートカラム追加 */
ALTER TABLE `room` ADD COLUMN `max_ss_watcher_seat` INT(11) NULL DEFAULT '0' COMMENT  'S&S 監視者用' AFTER `max_whiteboard_seat`;

/* セールスの管理者用：過去に作られた部屋にマネージャ入室可とする反映 */
UPDATE `room` SET `max_ss_watcher_seat`='1' WHERE `use_sales_option`='1' AND room_status = '1';

/* セールスの管理者用：ポート制御設定時、予約した際に監視者を含める場合フラグを立てる */
ALTER TABLE `reservation` ADD COLUMN `is_manager` INT(11) NULL DEFAULT 0 COMMENT 'S&S 監視者予約判断 0->予約時監視者含めない、1→予約時監視者含める、2→予約時監視者含めないが、会議中に入室した場合' AFTER `is_organizer_flg`;

/* セールスの管理者用：ポート制御設定時、予約した際に監視者を含める場合情報を保存する */
ALTER TABLE `reservation_user` ADD COLUMN `r_manager_flg` INT(11) NULL DEFAULT 0 COMMENT 'S&S 監視者情報' AFTER `r_organizer_flg`;
