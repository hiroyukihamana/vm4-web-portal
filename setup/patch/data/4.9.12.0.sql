ALTER TABLE `pw_reset` ADD `email` varchar(255) AFTER `name`;
ALTER TABLE `pw_reset` ADD `phone` varchar(50) AFTER `email`;
ALTER TABLE `member` CHANGE  `member_id`  `member_id` VARCHAR( 340 ) CHARACTER SET utf8 NOT NULL;

/*MTGVFOUR-1008 Web_個人ホワイトボードのファイル単位での取り込み・保存機能のご要望*/
CREATE TABLE IF NOT EXISTS `tmp_file` (
  `tmp_file_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `tmp_file_id` varchar(50) NOT NULL  COMMENT 'ファイルID',
  `path` varchar(255) NOT NULL COMMENT 'ファイルパス',
  `file_name` varchar(50) NOT NULL COMMENT 'ファイル名',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ステータス、1:DL可能、0:削除済み',
  `memo` varchar(255) NOT NULL COMMENT 'メモ',
  `dl_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'DLフラグ 1:DL済み、0:未DL',
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`tmp_file_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

/* [MTGVFOUR-203] メンバー削除時の個人ストレージデータの削除処理 */
CREATE TABLE IF NOT EXISTS `storage_physical_manage` (
  `storage_physical_manage_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PRIMARY',
  `storage_file_key` int(11) NOT NULL COMMENT 'FK',
  `member_key` int(11) NOT NULL COMMENT 'FK',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0:UNTREATED, 1:SUCCESS,-1:ERROR',
  `is_pending` tinyint(4) NOT NULL DEFAULT 0,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`storage_physical_manage_key`),
  INDEX storage_file_key(`storage_file_key`),
  INDEX member_key(`member_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

/* [MTGVFOUR-1116] セールスアカウントでのアクティブスピーカー変更処理 */
UPDATE `room` SET `active_speaker_mode_only_flg`='0', `active_speaker_mode_use_flg`='0', `room_updatetime` = now()
  WHERE `use_sales_option` = '1'
  AND `active_speaker_mode_only_flg` = '1'
  AND `max_seat` = '2';
  