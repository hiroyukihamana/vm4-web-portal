/* 8355 招待メールのHTML化 */
ALTER TABLE  `reservation` ADD  `mail_type` varchar(20) NULL COMMENT  'メール形式(html/text)nullの場合だとtext' AFTER `sender_lang`;
/* 8352 招待メール内のURL表示の変更機能 */
ALTER TABLE  `user` ADD  `guest_url_format` int(11) NOT NULL DEFAULT 0 COMMENT  '招待URLの表示方法:0:<>あり 1:<>なし' AFTER `lang_allow`;
ALTER TABLE  `user` ADD  `guest_mail_type` int(11) NOT NULL DEFAULT 0 COMMENT  '招待メールHTML化:0:<>利用する 1:<>利用しない' AFTER `guest_url_format`;
/* 8302 共有・個人ストレージ機能 */
ALTER TABLE  `user` ADD  `max_storage_size` int(11) NOT NULL DEFAULT 0 COMMENT  'ストレージサイズ(メガ単位)' AFTER  `lang_allow`;
ALTER TABLE  `member` ADD  `use_shared_storage` tinyint(1) NOT NULL DEFAULT 1 COMMENT  '共有ストレージ使用フラグ' AFTER  `member_group`;
ALTER TABLE  `meeting_clip` ADD  `storage_file_key` int(11) NULL COMMENT  'ストレージファイルキー' AFTER  `is_loaded`;
ALTER TABLE  `meeting_clip` ADD  `meeting_clip_name` varchar(255) NULL COMMENT  'ミーティング中でのファイル名' AFTER  `storage_file_key`;
ALTER TABLE  `participant` ADD  `storage_login_id` varchar(255) NULL COMMENT  'ストレージファイル用のキー' AFTER  `participant_session_id`;
ALTER TABLE  `document` ADD  `is_storage` tinyint(1) NOT NULL DEFAULT 0 COMMENT  'ストレージファイルかどうかのフラグ' AFTER  `document_transmitted`;

CREATE TABLE `storage_file` (
`storage_file_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ストレージファイルKEY',
`document_id` varchar(255) NULL COMMENT  'ドキュメントID',
`clip_key` varchar(255) NULL COMMENT  '動画ID',
`user_key` int(11) NOT NULL COMMENT  'ユーザーKEY',
`member_key` int(11) NOT NULL COMMENT  'メンバーKEY',
`owner` int(11) NOT NULL COMMENT  'オーナー',
`file_path` varchar(255) NOT NULL COMMENT  '変換後のファイルパス',
`file_name` varchar(255) NOT NULL COMMENT  '実ファイル名',
`user_file_name` varchar(255) NOT NULL COMMENT  'ユーザー表示用ファイル名',
`description` varchar(500) NULL COMMENT  '説明',
`file_size` bigint(20) NOT NULL COMMENT  '変換後のファイルサイズ',
`format` varchar(64) NOT NULL COMMENT  '変換後フォーマット',
`category` varchar(64) NOT NULL COMMENT  'カテゴリー',
`extension` varchar(64) NOT NULL COMMENT  '元ファイル拡張子',
`version` varchar(64) NOT NULL COMMENT  'アニメーションのバージョン',
`document_index` int(11) NOT NULL DEFAULT 1 COMMENT  'ページ数',
`status` int(11) NOT NULL COMMENT  'ステータス',
`storage_folder_key` int(11) NOT NULL COMMENT  '所属フォルダーKEY',
`last_update_member_key` int(11) NOT NULL COMMENT  'ラスト編集者(メンバー)',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`storage_file_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
ALTER TABLE  `storage_file` ADD INDEX (  `user_key` );
ALTER TABLE  `storage_file` ADD INDEX (  `member_key` );
ALTER TABLE  `storage_file` ADD INDEX (  `document_id` );
ALTER TABLE  `storage_file` ADD INDEX (  `clip_key` );

ALTER TABLE  `storage_file` ADD  `personal_white_id` varchar(64) NULL COMMENT  '個人ホワイトボードID' AFTER  `clip_key`;

CREATE TABLE `storage_favorite_file` (
`storage_file_key` int(11) NOT NULL COMMENT 'ストレージファイルKEY',
`user_key` int(11) NOT NULL COMMENT  'ユーザーKEY',
`member_key` int(11) NOT NULL COMMENT  'メンバーKEY',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
KEY `storage_file_key` (`storage_file_key`),
KEY `user_key` (`user_key`),
KEY `member_key` (`member_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/* 8361 アンケート機能 */
ALTER TABLE  `meeting` ADD  `has_quick_vote` tinyint(1) NOT NULL DEFAULT 0 COMMENT  '投票保持フラグ' AFTER  `has_shared_memo`;
ALTER TABLE  `meeting_sequence` ADD  `has_quick_vote` tinyint(1) NOT NULL DEFAULT 0 COMMENT  '投票保持フラグ' AFTER  `has_shared_memo`;

CREATE TABLE `quick_vote_log` (
`quick_vote_log_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'アンケート結果KEY',
`meeting_key` int(11) NOT NULL COMMENT  'ミーティングKEY',
`meeting_sequence_key` int(11) NOT NULL COMMENT  'ミーティングシーケンスKEY',
`number` int(11) NOT NULL COMMENT  'アンケート番号',
`participant_key` int(10) NOT NULL COMMENT  '参加者KEY',
`participant_name` varchar(255) NOT NULL COMMENT  '参加者名',
`result` varchar(20) NOT NULL COMMENT  '投票結果',
`fcs_quick_vote_id` varchar(60) NOT NULL COMMENT  'fms側識別ID',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`quick_vote_log_key`),
KEY `meeting_key` (`meeting_key`),
KEY `meeting_sequence_key` (`meeting_sequence_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/* 8356 セールス機能 */
ALTER TABLE  `user` ADD  `use_sales` TINYINT NOT NULL DEFAULT  '0' COMMENT  'セールス機能利用フラグ' AFTER  `use_clip_share`;
ALTER TABLE  `meeting` ADD  `use_sales_option` TINYINT NOT NULL DEFAULT  '0' COMMENT  'セールスオプション利用フラグ' AFTER  `use_pgi_dialout`;
ALTER TABLE  `meeting` ADD  `inbound_flg` tinyint(4) NOT NULL DEFAULT  '0' COMMENT  'インバウンド利用フラグ' AFTER  `use_sales_option`;
ALTER TABLE  `meeting` ADD  `inbound_id` varchar(255) DEFAULT NULL COMMENT 'インバウンドID' AFTER  `inbound_flg`;
ALTER TABLE  `room` ADD `use_sales_option` TINYINT NOT NULL DEFAULT  '0' COMMENT  'セールスオプション利用フラグ' AFTER  `use_pgi_dialout`;
ALTER TABLE  `room` ADD `inbound_flg` tinyint(4) NOT NULL DEFAULT  '0' COMMENT  'インバウンド利用フラグ' AFTER  `use_sales_option`;
ALTER TABLE  `room` ADD `inbound_id` varchar(255) DEFAULT NULL COMMENT 'インバウンドID' AFTER  `inbound_flg`;
ALTER TABLE  `room` ADD `default_auto_rec_use_flg` TINYINT NOT NULL DEFAULT  '0' COMMENT  '自動録画フラグ' AFTER  `default_camera_size`;
ALTER TABLE  `room` ADD `reenter_flg` tinyint(1) DEFAULT '0' COMMENT 'インバウンド優先再入室グラグ' AFTER  `inbound_id`;
ALTER TABLE  `room` ADD `wb_allow_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ホワイトボード操作の初期設定フラグ' AFTER  `reenter_flg`;
ALTER TABLE  `room` ADD `chat_allow_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'チャット操作の初期設定フラグ' AFTER  `wb_allow_flg`;
ALTER TABLE  `room` ADD `print_allow_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '印刷操作の初期設定フラグ' AFTER  `chat_allow_flg`;
ALTER TABLE  `room` ADD `customer_camera_use_flg` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'お客様側カメラの利用0:禁止、1:許可' AFTER  `print_allow_flg`;
ALTER TABLE  `room` ADD `room_image` int(11) NOT NULL DEFAULT '0' AFTER  `customer_camera_use_flg`;
ALTER TABLE  `room` ADD `room_password` varchar(16) DEFAULT NULL AFTER  `room_image`;
ALTER TABLE  `room` ADD `addition` text DEFAULT NULL COMMENT  'セールス用設定保存カラム' AFTER  `room_password`;
ALTER TABLE  `room` ADD `comment` text DEFAULT NULL COMMENT  'アウトバウンドページ' AFTER  `addition`;
ALTER TABLE  `room` ADD `outbound_id` varchar(64) DEFAULT NULL COMMENT  'アウトバウンドURL利用ID' AFTER  `use_sales_option`;
ALTER TABLE  `room` ADD `customer_sharing_button_hide_status` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'お客様側デスクトップ共有ボタン' AFTER  `customer_camera_use_flg`;
ALTER TABLE  `participant` ADD  `is_away` TINYINT NOT NULL DEFAULT  '0' COMMENT  '一時退室フラグ' AFTER  `is_narrowband`;
ALTER TABLE  `member` ADD  `use_sales` TINYINT NOT NULL DEFAULT  '0' COMMENT  'セールス機能利用フラグ' AFTER  `mac_address`;
ALTER TABLE  `member` ADD  `outbound_id` varchar(64) DEFAULT NULL COMMENT  'アウトバウンドURL利用ID' AFTER  `use_sales`;
ALTER TABLE  `reservation` ADD  `is_customer_desktop_share_flg` TINYINT(4) DEFAULT '0' COMMENT  'カスタマーPC共有 0:使用しない、1:使用する' AFTER  `is_desktop_share`;
ALTER TABLE  `reservation` ADD  `is_auto_rec_flg` TINYINT(4) DEFAULT '0' COMMENT  '自動録画 0:使用しない、1:使用する' AFTER  `is_rec_flg`;
ALTER TABLE  `reservation` ADD  `is_customer_camera_flg` TINYINT(4) DEFAULT '0' COMMENT  'お客様カメラ利用 0:使用しない、1:使用する' AFTER  `is_cabinet_flg`;
ALTER TABLE  `reservation` ADD  `is_wb_allow_flg` TINYINT(4) DEFAULT '0' COMMENT  'WB操作許可 0:使用しない、1:使用する' AFTER  `is_customer_camera_flg`;
ALTER TABLE  `reservation` ADD  `is_chat_allow_flg` TINYINT(4) DEFAULT '0' COMMENT  'チャット操作許可 0:使用しない、1:使用する' AFTER  `is_wb_allow_flg`;
ALTER TABLE  `reservation` ADD  `is_print_allow_flg` TINYINT(4) DEFAULT '0' COMMENT  '印刷操作許可 0:使用しない、1:使用する' AFTER  `is_chat_allow_flg`;
ALTER TABLE  `meeting_clip` ADD  `room_key` varchar(64) NULL COMMENT  '会議室キー(主にセールスで使用する)' AFTER  `is_loaded`;

CREATE TABLE IF NOT EXISTS `inbound` (
  `inbound_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'インバウンドキー',
  `inbound_id` varchar(255) NOT NULL COMMENT 'インバウンド用のID',
  `inbound_name` varchar(255) DEFAULT NULL COMMENT '名前',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーキー',
  `reenter_time` int(11) NOT NULL DEFAULT '0' COMMENT '再入室時間（秒）',
  `design_type` varchar(32) NOT NULL DEFAULT 'normal' COMMENT 'normal：通常デザイン、original：オリジナル、custom：カスタマイズ',
  `design_key` int(11) NOT NULL DEFAULT '1' COMMENT 'ボタンのタイプ 0:オリジナル使用、100:カスタマイズ',
  `user_option_key` int(11) NOT NULL DEFAULT '0' COMMENT 'ユーザーオプションキー',
  `customer_info_flg` int(11) NOT NULL DEFAULT '0' COMMENT '0:表示、1:非表示、2:非表示、名前固定',
  `customer_name` varchar(64) DEFAULT NULL COMMENT '固定する場合のお客様名',
  `inbound_sort` int(11) NOT NULL DEFAULT '1' COMMENT 'ソート',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'ステータス',
  `addition` text COMMENT 'その他',
  PRIMARY KEY (`inbound_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;
ALTER TABLE  `inbound` ADD  `create_datetime` DATETIME NULL DEFAULT NULL AFTER  `addition` ,
ADD  `update_datetime` DATETIME NULL DEFAULT NULL AFTER  `create_datetime`;

CREATE TABLE IF NOT EXISTS `inbound_log` (
  `inbound_log_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL,
  `inbound_id` varchar(255) NOT NULL COMMENT 'インバウンドID',
  `room_key` varchar(255) NOT NULL,
  `meeting_key` int(11) DEFAULT NULL,
  `participant_key` int(11) DEFAULT NULL,
  `participant_session_id` varchar(255) DEFAULT NULL,
  `http_referer` text,
  `remote_addr` text,
  `http_user_agent` text,
  `event` varchar(64) NOT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`inbound_log_key`),
  KEY `user_key` (`user_key`),
  KEY `room_key` (`room_key`),
  KEY `meeting_key` (`meeting_key`),
  KEY `user_key_2` (`user_key`),
  KEY `room_key_2` (`room_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `customer_message` (
  `no` int(11) NOT NULL auto_increment,
  `user_key` int(11) NOT NULL default '0',
  `room_key` varchar(64) NOT NULL default '',
  `customer_name` text NOT NULL,
  `customer_company` text NOT NULL,
  `customer_tel` text NOT NULL,
  `customer_email` text NOT NULL,
  `customer_ip` text NOT NULL,
  `customer_comment` text NOT NULL,
  `addition` text NOT NULL,
  `status` int(11) NOT NULL default '1',
  `send_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  KEY `no` (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/* ポート課金 */
ALTER TABLE  `user` ADD  `max_connect_participant` INT( 11 ) NULL DEFAULT NULL COMMENT  '同時接続数上限' AFTER  `max_rec_size`;
ALTER TABLE  `participant` ADD  `fms_server_key` INT( 11 ) NULL DEFAULT NULL COMMENT  '利用FMSサーバーキー' AFTER  `member_key`,
 ADD  `participant_country` VARCHAR( 32 ) NULL DEFAULT NULL COMMENT  '所在地' AFTER  `participant_locale`;
ALTER TABLE  `user` ADD  `use_port_plan` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'ポート課金フラグ' AFTER  `use_sales`;


ALTER TABLE  `eventlog` ADD  `room_key` VARCHAR( 255 ) NULL DEFAULT NULL AFTER  `participant_type`;

CREATE TABLE  `max_connect_log` (
`max_connect_log_key` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`user_key` INT( 11 ) NOT NULL ,
`meeting_key` INT( 10 ) NULL ,
`room_key` VARCHAR( 255 ) NULL ,
`connect_count` INT NOT NULL ,
`create_datetime` DATETIME NOT NULL ,
PRIMARY KEY (  `max_connect_log_key` ) ,
INDEX (  `user_key` ,  `meeting_key` ,  `room_key` )
) ENGINE = MYISAM ;