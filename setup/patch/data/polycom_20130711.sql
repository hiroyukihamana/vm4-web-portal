CREATE TABLE IF NOT EXISTS `conference_record` (
  `conference_record_key` int(11) NOT NULL auto_increment,
  `methodName` varchar(30) NOT NULL,
  `confId` varchar(50) default NULL,
  `partId` int(5) default NULL,
  `partName` varchar(255) default NULL,
  `partType` varchar(30) default NULL,
  `state` varchar(30) default NULL,
  `info` text,
  `createdate` varchar(30) NOT NULL,
  PRIMARY KEY  (`conference_record_key`),
  KEY `confId` (`confId`,`partId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE  `ives_setting` ADD  `mcu_server_key` INT( 10 ) NOT NULL AFTER  `ives_setting_key` ,
ADD INDEX ( mcu_server_key );

ALTER TABLE  `room` ADD  `mobile_mix` INT( 11 ) NOT NULL DEFAULT  '1' AFTER  `meeting_limit_time` ,
ADD INDEX ( mobile_mix );

ALTER TABLE  `video_conference` ADD  `mcu_server_key` INT( 10 ) NOT NULL AFTER  `video_conference_key` ,
ADD INDEX ( mcu_server_key );
