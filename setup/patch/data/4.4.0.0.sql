-- メンバーキーの検索用索引追加（メンバーIDは変更されると追えなくなる）
ALTER TABLE `meeting` ADD `member_keys` TEXT NULL COMMENT 'メンバーキー一覧' AFTER `meeting_use_minute`;

-- 参加者名一覧（改行区切り）
ALTER TABLE `meeting` ADD `participant_names` TEXT NULL COMMENT '参加者名一覧' AFTER `member_keys`;

-- 議事録の公開
ALTER TABLE `meeting` ADD `is_publish` TINYINT( 1 ) NOT NULL DEFAULT '0' COMMENT '0: 非公開、1: 公開中' AFTER `is_reserved` ;


ALTER TABLE `meeting_sequence` ADD `status` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `has_recorded_video` ;


--
-- テーブルの構造 `file_cabinet`
--

CREATE TABLE IF NOT EXISTS `file_cabinet` (
  `cabinet_id` int(11) NOT NULL auto_increment,
  `meeting_key` int(10) NOT NULL,
  `tmp_name` text character set utf8 NOT NULL,
  `type` text NOT NULL,
  `size` int(11) NOT NULL,
  `file_name` text character set utf8 NOT NULL,
  `file_cabinet_path` text character set utf8 NOT NULL,
  `status` tinyint(1) NOT NULL default '1',
  `registtime` datetime NOT NULL,
  `updatetime` datetime default NULL,
  PRIMARY KEY  (`cabinet_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


--
-- テーブルの構造 `fms_watch_status`
--

CREATE TABLE `fms_watch_status` (
  `watch_status_key` int(11) NOT NULL auto_increment,
  `meeting_key` int(10) NOT NULL,
  `meeting_sequence_key` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL default '0' COMMENT '0: wait, 1: 実行中, 2: 成功, 9: 失敗',
  `registtime` datetime NOT NULL,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  PRIMARY KEY  (`watch_status_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
