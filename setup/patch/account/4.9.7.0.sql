/* BIZ案件 カスタムドメイン追加 */
INSERT INTO `custom_domain_service` (`service_name`, `domain`, `status`, `create_datetime`, `update_datetime`) VALUES('biz', 'biz.nice2meet.us', 1, '2014-03-13 00:00:00', '2014-03-13 00:00:00');
ALTER TABLE  `service` ADD `user_service_name` varchar(64) NULL COMMENT 'custom_domain_serviceの提供サービス名' AFTER `lang_allow`;
ALTER TABLE  `service` ADD `external_user_invitation_flg` tinyint NOT NULL COMMENT 'ID制の外部招待許可フラグ' DEFAULT 0 AFTER `user_service_name`;
ALTER TABLE  `service` ADD `external_member_invitation_flg` tinyint NOT NULL COMMENT 'ID制別ユーザーに紐ずくメンバーの招待フラグ' DEFAULT 0 AFTER  `external_user_invitation_flg`;
UPDATE  `service` SET  `user_service_name` =  'biz' , `external_member_invitation_flg` = 1 WHERE  `service`.`service_key` = 108 LIMIT 1 ;
