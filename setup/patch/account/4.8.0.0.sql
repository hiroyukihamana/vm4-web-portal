INSERT INTO `service` VALUES(89, 1, 'セールスプラン', 2, 0, 0, 0, 2048, 256, 80, '160x120', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 45000, 34900, 25, 1200, NOW(), 1, 0);
INSERT INTO options VALUES ('28',  'GlobalLink', NOW( ) , NULL);
INSERT INTO service_option VALUES ('30',  '1',  'GlobalLink',  '0',  '0',  '0');
ALTER TABLE  `fms_server` ADD  `protocol_port` VARCHAR( 255 ) NULL DEFAULT "rtmp:1935,rtmp:80,rtmp:8080,rtmps:443,rtmps-tls:443" COMMENT  'FMS利用プロトコルポート' AFTER  `server_priority`;
INSERT INTO  datacenter VALUES ('2001',  'Global Link',  'Global Link',  'Global Link',  'Global Link',  'Global Link',  'globallink',  '0',  '1',  '1', NOW( ) ,  '0000-00-00 00:00:00');
ALTER TABLE  `fms_server` ADD  `use_global_link` TINYINT NOT NULL DEFAULT  '0' COMMENT  'GlobalLinkで利用するか' AFTER  `is_ssl`;