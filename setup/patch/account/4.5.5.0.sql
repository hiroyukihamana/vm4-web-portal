INSERT INTO participant_type (`participant_type_key` ,`participant_type_name` ,`create_datetime` ,`update_datetime`) VALUES (10 , 'h323', '0000-00-00 00:00:00', NULL);
INSERT INTO participant_type (`participant_type_key` ,`participant_type_name` ,`create_datetime` ,`update_datetime`) VALUES (11 , 'h323ins', '0000-00-00 00:00:00', NULL);
INSERT INTO participant_type (`participant_type_key` ,`participant_type_name` ,`create_datetime` ,`update_datetime`) VALUES (12 , 'phone', '0000-00-00 00:00:00', NULL);
INSERT INTO participant_type (`participant_type_key` ,`participant_type_name` ,`create_datetime` ,`update_datetime`) VALUES (13 , 'unknown', '0000-00-00 00:00:00', NULL);

INSERT INTO `participant_mode` (`participant_mode_key` ,`participant_mode_name` ,`create_datetime` ,`update_datetime` )VALUES (10 ,'h323ins', '0000-00-00 00:00:00', NULL);
INSERT INTO `participant_mode` (`participant_mode_key` ,`participant_mode_name` ,`create_datetime` ,`update_datetime` )VALUES (11 ,'unknown', '0000-00-00 00:00:00', NULL);

INSERT INTO `participant_role` (`participant_role_key`, `participant_role_name`, `create_datetime`, `update_datetime`) VALUES ('3', 'unknown', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
