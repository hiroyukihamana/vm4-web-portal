--
-- テーブルの構造 `agency`
--

CREATE TABLE `agency` (
  `agency_id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_name` varchar(150) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `create_datetime` varchar(45) DEFAULT NULL,
  `delete_datetime` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`agency_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE  `datacenter` ADD  `datacenter_name_cn` TEXT NOT NULL AFTER  `datacenter_name_en`;