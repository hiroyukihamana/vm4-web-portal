-- service

INSERT INTO `service` (`service_key`, `country_key`, `service_name`, `service_initial_charge`, `service_charge`, `service_additional_charge`, `service_freetime`, `service_registtime`, `service_status`) VALUES (48, 1, 'おてがるプラン', 0, 0, 0, 0, '2009-02-10 00:00:00', 1);
INSERT INTO `service` (`service_key`, `country_key`, `service_name`, `service_initial_charge`, `service_charge`, `service_additional_charge`, `service_freetime`, `service_registtime`, `service_status`) VALUES (49, 1, '使い放題プラン', 0, 0, 0, 0, '2009-02-10 00:00:00', 1);

-- service_optin

INSERT INTO `service_option` (`service_option_key`, `country_key`, `service_option_name`, `service_option_price`, `service_option_init_price`, `service_option_running_price`) VALUES (14, 1, '導入サポート', 0, 0, 0);


-- phpMyAdmin SQL Dump
-- version 2.10.2
-- http://www.phpmyadmin.net
--
-- ホスト: localhost
-- 生成時間: 2009 年 5 月 12 日 21:05
-- サーバのバージョン: 5.0.41
-- PHP のバージョン: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- データベース: `meeting_trunk_mgm`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `eco_simulator_log`
--

CREATE TABLE `eco_simulator_log` (
  `id` int(11) NOT NULL auto_increment,
  `endpoint` text NOT NULL,
  `startpoint` text NOT NULL,
  `move` double NOT NULL,
  `time` bigint(20) NOT NULL,
  `fare` bigint(20) NOT NULL,
  `co2` double NOT NULL,
  `num_trips` int(11) NOT NULL,
  `num_expances` INT( 11 ) NOT NULL DEFAULT '0',
  `entrytime` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
