/* アクティブスピーカー */
ALTER TABLE  `service` ADD `active_speaker_mode_only_flg` tinyint(4) NOT NULL COMMENT '強制アクティブスピーカーモードフラグ' DEFAULT 0 AFTER  `external_member_invitation_flg`;


ALTER TABLE  `service` ADD `guest_url_format` int(11) NOT NULL DEFAULT '0' COMMENT '招待URLの表示方法:0:<>あり 1:<>なし' AFTER  `active_speaker_mode_only_flg`;
UPDATE `service` SET `guest_url_format` = '1' WHERE `service`.`service_key` =108;

/* 以下ONE関係のSQL */
/*ユーザーサービスプランにユーザー設定を追加*/
ALTER TABLE  `service` ADD `max_storage_size` int(11) NOT NULL DEFAULT '0' COMMENT 'ユーザー最大容量' AFTER  `add_storage_size`;
ALTER TABLE  `service` ADD `max_member_count` tinyint(11) NOT NULL DEFAULT '0' COMMENT 'ユーザー最大メンバー数' AFTER  `lang_allow`;
ALTER TABLE  `service` ADD `port` tinyint(11) NOT NULL DEFAULT '0' COMMENT 'ユーザー最大メンバー数' AFTER  `max_member_count`;
ALTER TABLE  `service` ADD `pgi_setting_key` varchar(64) NOT NULL DEFAULT '' COMMENT 'PGiプランキー' AFTER  `teleconference`;
ALTER TABLE  `service` ADD `use_one_plan` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'VCUBE ONE利用フラグ' AFTER  `use_sales_plan`;
ALTER TABLE  `service` ADD `active_speaker_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'アクティブスピーカー利用フラグ' AFTER  `disable_rec_flg`;
ALTER TABLE  `service` ADD `active_speaker_user_count` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'アクティブスピーカー最大映像表示数' AFTER  `active_speaker_flg`;

ALTER TABLE  `service` CHANGE COLUMN `max_guest_seat` `max_guest_seat` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE  `service` CHANGE COLUMN `max_guest_seat_flg` `max_guest_seat_flg` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE  `service` CHANGE COLUMN `extend_max_seat` `extend_max_seat` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE  `service` CHANGE COLUMN `extend_max_audience_seat` `extend_max_audience_seat` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE  `service` CHANGE COLUMN `extend_seat_flg` `extend_seat_flg` INT(11) NOT NULL DEFAULT '0'; 


/*ONE用のサービスプラン追加*/
INSERT INTO `service` (`service_key`, `country_key`, `service_name`, `max_seat`, `max_audience_seat`, `max_whiteboard_seat`, `meeting_limit_time`, `max_room_bandwidth`, `max_user_bandwidth`, `min_user_bandwidth`, `default_camera_size`, `disable_rec_flg`, `active_speaker_mode_only_flg`, `active_speaker_flg`, `active_speaker_user_count`, `whiteboard_video`, `meeting_ssl`, `desktop_share`, `hdd_extention`, `high_quality`, `mobile_phone`, `h323_client`, `whiteboard`, `multicamera`, `telephone`, `smartphone`, `record_gw`, `h264`, `global_link`, `teleconference`, `pgi_setting_key`, `hd_flg`, `sales_flg`, `use_room_plan`, `use_member_plan`, `use_sales_plan`,`use_one_plan`, `whiteboard_page`, `whiteboard_size`, `document_filetype`, `image_filetype`, `cabinet_filetype`, `service_initial_charge`, `service_charge`, `service_additional_charge`, `service_freetime`, `service_registtime`, `service_status`, `service_expire_day`, `add_storage_size`, `member_storage_size`,`max_storage_size`,`max_member_count`,`port`) VALUES
(111, 1, 'グローバルプラン 電話連携あり', 10, 0, 10, 0, 6144, 512, 100, '640x480', 0, 1, 1, 4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 'VCUBE HYBRID PRODUCTION',0,0,0,0,0,1,100,20,'','','',45000,100000,100000,0,'2014-08-30 00:00:00',1,0,0,0,5000,0,50),
(112, 1, 'グローバルプラン 電話連携あり(代理店)', 10, 0, 10, 0, 6144, 512, 100, '640x480', 0, 1, 1, 4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 'VCUBE HYBRID AGENCY PRODUCTION',0,0,0,0,0,1,100,20,'','','',45000,100000,100000,0,'2014-08-30 00:00:00',1,0,0,0,5000,0,50),
(113, 1, 'グローバルプラン 電話連携なし', 10, 0, 10, 0, 6144, 512, 100, '640x480', 0, 1, 1, 4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, '',0,0,0,0,0,1,100,20,'','','',45000,100000,100000,0,'2014-08-30 00:00:00',1,0,0,0,5000,0,50),
(114, 1, 'ローカルルプラン 電話連携あり', 10, 0, 10, 0, 6144, 512, 100, '640x480', 0, 1, 1, 4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 'VCUBE HYBRID PRODUCTION',0,0,0,0,0,1,100,20,'','','',45000,100000,100000,0,'2014-08-30 00:00:00',1,0,0,0,5000,0,50),
(115, 1, 'ローカルルプラン 電話連携あり(代理店)', 10, 0, 10, 0, 6144, 512, 100, '640x480', 0, 1, 1, 4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 'VCUBE HYBRID AGENCY PRODUCTION',0,0,0,0,0,1,100,20,'','','',45000,100000,100000,0,'2014-08-30 00:00:00',1,0,0,0,5000,0,50),
(116, 1, 'ローカルルプラン 電話連携なし', 10, 0, 10, 0, 6144, 512, 100, '640x480', 0, 1, 1, 4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '',0,0,0,0,0,1,100,20,'','','',45000,100000,100000,0,'2014-08-30 00:00:00',1,0,0,0,5000,0,50);

/* ONE用APIキー */
INSERT INTO `api_auth` (`appli_name`, `appli_info`, `api_key`, `secret`, `status`, `create_datetime`, `update_datetime`) VALUES('VCUBE ONE', 'VCUBE ONE用APIキー', 'd034c790cf4d2c07a965a66e920d97e8465f3a5e754498de0d3d6c629f8a2152', 'e7e76c00e35ff5249c84c23ab2d0598749c7de0d925d9b1d8ac285df4fdedda4', '1', '2014-08-08 11:09:26', '2014-08-08 11:09:26');

/* STB用フラグ */
ALTER TABLE  `media_mixer` ADD  `stb_mix` INT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'STB用フラグ' AFTER  `mobile_mix`;
ALTER TABLE  `service` ADD `use_stb_plan` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'STB利用フラグ' AFTER  `use_one_plan`;
INSERT INTO `service` (`service_key`, `country_key`, `service_name`, `max_seat`, `max_audience_seat`, `max_whiteboard_seat`, `max_guest_seat`, `max_guest_seat_flg`, `extend_max_seat`, `extend_max_audience_seat`, `extend_seat_flg`, `meeting_limit_time`, `max_room_bandwidth`, `max_user_bandwidth`, `min_user_bandwidth`, `default_camera_size`, `disable_rec_flg`, `active_speaker_flg`, `active_speaker_user_count`, `whiteboard_video`, `meeting_ssl`, `desktop_share`, `hdd_extention`, `high_quality`, `mobile_phone`, `h323_client`, `whiteboard`, `multicamera`, `telephone`, `smartphone`, `record_gw`, `h264`, `global_link`, `teleconference`, `pgi_setting_key`, `hd_flg`, `bandwidth_up_flg`, `sales_flg`, `use_room_plan`, `use_member_plan`, `use_sales_plan`, `use_one_plan`, `use_stb_plan`, `whiteboard_page`, `whiteboard_size`, `document_filetype`, `image_filetype`, `cabinet_filetype`, `ignore_staff_reenter_alert`, `service_initial_charge`, `service_charge`, `service_additional_charge`, `service_freetime`, `service_registtime`, `service_status`, `service_expire_day`, `member_storage_size`, `add_storage_size`, `max_storage_size`, `lang_allow`, `max_member_count`, `port`, `user_service_name`, `external_user_invitation_flg`, `external_member_invitation_flg`, `active_speaker_mode_only_flg`, `guest_url_format`, `vcubeid_connect_service`) VALUES
(117, 1, 'STBプラン', 12, 0, 12, 1, 1, 0, 0, 0, 0, 10240, 1024, 150, '720p', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '1', 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2014-08-22 00:00:00', 1, 0, 1000, 1000, 0, NULL, 0, 0, NULL, 0, 0, 0, 0, NULL);
ALTER TABLE  `mcu_server` ADD  `use_stb` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'STBでの利用フラグ' AFTER  `sip_proxy_port`;