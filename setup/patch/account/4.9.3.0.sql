ALTER TABLE  `service` ADD  `bandwidth_up_flg` TINYINT(4) DEFAULT 0 COMMENT  '帯域追加フラグ' AFTER  `hd_flg`;
UPDATE `service` SET `bandwidth_up_flg` = 1 WHERE service_key IN (80,75,81,82,83,84,85,86,87,88,89,90,92,76,77,91,93);
/* 部屋の人数臨時拡張 */
ALTER TABLE  `service` ADD  `max_guest_seat` INT( 11 ) NOT NULL AFTER  `max_whiteboard_seat`;
ALTER TABLE  `service` ADD  `max_guest_seat_flg` INT( 11 ) NOT NULL AFTER  `max_guest_seat`;
ALTER TABLE  `service` ADD  `extend_max_seat` INT( 11 ) NOT NULL AFTER  `max_guest_seat_flg`;
ALTER TABLE  `service` ADD  `extend_max_audience_seat` INT( 11 ) NOT NULL AFTER  `extend_max_seat`;
ALTER TABLE  `service` ADD  `extend_seat_flg` INT( 11 ) NOT NULL AFTER  `extend_max_audience_seat`;