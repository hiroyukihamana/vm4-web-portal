-- user_agentテーブルにIndexを張る
ALTER TABLE `user_agent` ADD INDEX user_id(`user_id`);
ALTER TABLE `user_agent` ADD INDEX meeting_key(`meeting_key`);
ALTER TABLE `user_agent` ADD INDEX participant_key(`participant_key`);
