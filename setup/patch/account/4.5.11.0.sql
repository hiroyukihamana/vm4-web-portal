-- 告知機能
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'キー',
  `start_datetime` datetime NOT NULL COMMENT 'メンテナンス開始日時',
  `end_datetime` datetime NOT NULL COMMENT 'メンテナンス終了日時',
  `info` text NOT NULL COMMENT '情報',
  `url_ja` text NOT NULL COMMENT 'メンテナンスページ（日本語）',
  `url_en` text NOT NULL COMMENT 'メンテナンスページ（英語）',
  `url_zh` text NOT NULL COMMENT 'メンテナンスページ（中国語）',
  `default_lang` varchar(10) NOT NULL COMMENT '設定がない場合のデフォルト言語',
  `status` varchar(1) NOT NULL COMMENT 'ステータス',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='告知機能';
