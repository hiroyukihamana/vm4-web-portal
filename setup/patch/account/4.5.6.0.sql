-- 資料共有で映像受信
ALTER TABLE `service` ADD `whiteboard_video` INT( 1 ) NULL DEFAULT '0' COMMENT '資料共有ビデオ付き' AFTER `max_whiteboard_seat`;

INSERT INTO `options` (`option_key` ,`option_name` ,`create_datetime` ,`update_datetime`) VALUES
 (20, 'whiteboard_video', '0000-00-00 00:00:00', NULL);

INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`) VALUES
 (22 , '0', 'whiteboard_video', '5000', '0', '0');

-- 参加者タイプ
INSERT INTO `participant_type` ( `participant_type_key` , `participant_type_name` , `create_datetime` , `update_datetime` ) VALUES
(14, 'invisible_wb', NOW() , NULL);
