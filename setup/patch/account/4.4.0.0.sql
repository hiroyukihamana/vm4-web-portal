-- phpMyAdmin SQL Dump
-- version 2.10.2
-- http://www.phpmyadmin.net
--
-- ホスト: localhost
-- 生成時間: 2009 年 7 月 16 日 19:29
-- サーバのバージョン: 5.0.41
-- PHP のバージョン: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- データベース: `meeting_trunk_mgm`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `ip_whitelist`
--

CREATE TABLE `ip_whitelist` (
  `user_key` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL default '1' COMMENT '1: 有効, 9: 削除',
  `registtime` datetime NOT NULL,
  `updatetime` datetime default NULL,
  KEY `user_key` (`user_key`),
  KEY `ip` (`ip`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
