-- 【Attention!】
-- 既存のプランが設定されている場合は、4.5.0.0のパッチ(各種移行ツールの事。)で
-- 利用人数の更新を行なって下さい！
-- SIの場合は、導入時のプラン設定を見てから対応しましょう。
-- ただし、設定済みの部屋には関係しないので、途中でプラン変更しない限りは問題なしです。

-- New Trial
UPDATE service SET country_key = '1',service_name = 'New_Trial',service_initial_charge = '45000',max_seat = '10',max_audience_seat = '0',meeting_ssl = '0',desktop_share = '0',hdd_extention = '0',high_quality = '0',mobile_phone = '0',h323_client = '0',whiteboard = '0',service_charge = '9900',service_additional_charge = '190',service_freetime = '0',service_status = '1' WHERE service_key='41';
-- Entry
UPDATE service SET country_key = '1',service_name = 'Entry',service_initial_charge = '45000',max_seat = '10',max_audience_seat = '0',meeting_ssl = '0',desktop_share = '0',hdd_extention = '0',high_quality = '0',mobile_phone = '0',h323_client = '0',whiteboard = '0',service_charge = '34900',service_additional_charge = '25',service_freetime = '1200',service_status = '1' WHERE service_key='42';
-- Standerd
UPDATE service SET country_key = '1',service_name = 'Standard',service_initial_charge = '45000',max_seat = '10',max_audience_seat = '0',meeting_ssl = '0',desktop_share = '1',hdd_extention = '0',high_quality = '0',mobile_phone = '1',h323_client = '0',whiteboard = '0',service_charge = '79900',service_additional_charge = '0',service_freetime = '60000',service_status = '1' WHERE service_key='43';
-- 高画質
UPDATE service SET country_key = '1',service_name = 'High_Quality',service_initial_charge = '45000',max_seat = '10',max_audience_seat = '0',meeting_ssl = '0',desktop_share = '1',hdd_extention = '0',high_quality = '1',mobile_phone = '1',h323_client = '0',whiteboard = '0',service_charge = '119900',service_additional_charge = '0',service_freetime = '60000',service_status = '1' WHERE service_key='44';
-- Premium20
UPDATE service SET country_key = '1',service_name = 'Premium20',service_initial_charge = '45000',max_seat = '9',max_audience_seat = '11',meeting_ssl = '0',desktop_share = '1',hdd_extention = '0',high_quality = '0',mobile_phone = '1',h323_client = '0',whiteboard = '0',service_charge = '119900',service_additional_charge = '0',service_freetime = '60000',service_status = '1' WHERE service_key='45';
-- Premium30
UPDATE service SET country_key = '1',service_name = 'Premium30',service_initial_charge = '45000',max_seat = '9',max_audience_seat = '21',meeting_ssl = '0',desktop_share = '1',hdd_extention = '0',high_quality = '0',mobile_phone = '1',h323_client = '0',whiteboard = '0',service_charge = '149900',service_additional_charge = '0',service_freetime = '60000',service_status = '1' WHERE service_key='46';
-- Premium40
UPDATE service SET country_key = '1',service_name = 'Premium40',service_initial_charge = '45000',max_seat = '9',max_audience_seat = '31',meeting_ssl = '0',desktop_share = '1',hdd_extention = '0',high_quality = '0',mobile_phone = '1',h323_client = '0',whiteboard = '0',service_charge = '179900',service_additional_charge = '0',service_freetime = '60000',service_status = '1' WHERE service_key='47';
