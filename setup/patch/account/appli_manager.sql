--
-- テーブルの構造 `appli_file`
--

CREATE TABLE `appli_file` (
  `appli_file_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_type` varchar(64) NOT NULL COMMENT 'アップデーター:installer;アプリケーション:binary',
  `file_name` varchar(64) NOT NULL COMMENT 'ファイル名',
  `file_size` int(10) NOT NULL DEFAULT 0 COMMENT 'バイト/最大1GB',
  `version` varchar(64) NOT NULL COMMENT 'ファイルバージョン',
  `hash` varchar(64) NOT NULL DEFAULT 0 COMMENT 'ハッシュ',
  `is_staging` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'ステージングフラグ',
  `is_released` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'リリースフラグ',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '削除フラグ',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日時', 
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日時', 
  PRIMARY KEY (`appli_file_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;