CREATE TABLE `api_auth` (
  `api_no` int(11) NOT NULL AUTO_INCREMENT COMMENT '連番',
  `appli_name` varchar(100) DEFAULT NULL COMMENT 'アプリケーション名',
  `appli_info` text NOT NULL,
  `api_key` varchar(64) NOT NULL COMMENT 'API_KEY',
  `secret` varchar(64) NOT NULL COMMENT '秘密鍵',
  `status` varchar(1) NOT NULL COMMENT '0:無効, 1:有効',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`api_no`),
  UNIQUE KEY `api_key` (`api_key`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='API認証';


-- お手軽、使い放題の利用人数を指定
UPDATE `service` SET `max_seat` = '10' WHERE `service`.`service_key` =48;
UPDATE `service` SET `max_seat` = '10' WHERE `service`.`service_key` =49;