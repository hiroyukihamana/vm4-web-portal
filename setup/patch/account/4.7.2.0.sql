ALTER TABLE  `datacenter` ADD  `datacenter_name_tw` TEXT NOT NULL AFTER  `datacenter_name_cn` ,
ADD  `datacenter_name_fr` TEXT NOT NULL AFTER  `datacenter_name_tw`;
UPDATE faq_list SET faq_answer = 'はい。映像と音声は非公開の独自プロトコルで通信しており、テキストチャットやファイル共有などドキュメントの部分は標準でSSLで通信しておりますので、限りなく万全な設計になっております。また、オプションサービスであるSSL対応サービスをご利用頂くことで、全ての通信をSSL化することも可能です。' WHERE faq_key = 53;

UPDATE faq_data SET answer = 'はい。映像と音声は非公開の独自プロトコルで通信しており、テキストチャットやファイル共有などドキュメントの部分は標準でSSLで通信しておりますので、限りなく万全な設計になっております。また、オプションサービスであるSSL対応サービスをご利用頂くことで、全ての通信をSSL化することも可能です。' WHERE faq_key = 53 AND lang LIKE 'ja%';
UPDATE faq_data SET question = 'How secure is V-cube?',answer = 'V-cube application is highly secure.  V-cube uses the original protocol for video and audio, and SSL (Secured Socket Layer)for documents such as text chat and file sharing. It is optional to have SSL for all communication.' WHERE faq_key = 53 AND lang LIKE 'en%';
UPDATE faq_data SET question = 'Comment est sécurisé V-cube ?',answer = 'L\'application V-cube est hautement sécurisé. V-cube utilise des protocoles standards pour la vidéo et l\'audio, et le protocole SSL (Secured Socket Layer) pour les donnés tels que le chat texte et le partage de fichiers. L\'utilisation du protocole SSL pour toutes les communications est néanmoins optionnelle.' WHERE faq_key = 53 AND lang LIKE 'fr%';
UPDATE faq_data SET question = '安全性完善吗？',answer = '是的。视频和语音通过非公开的独立协议进行传输，文本聊天和文件共享等文档部分以标准SSL进行传输，因此在安全方面万无一失。此外，因为使用支持SSL的选项服务，所以全部传输也可以进行SSL化。' WHERE faq_key = 53 AND lang LIKE 'zh_CN';
UPDATE faq_data SET question = '安全性完善嗎？',answer = '是的。視頻和語音通過非公開的獨立協定進行傳輸，文本聊天和檔共用等文檔部分以標準SSL進行傳輸，因此在安全方面萬無一失。此外，因為使用支援SSL的選項服務，所以全部傳輸也可以進行SSL化。' WHERE faq_key = 53 AND lang LIKE 'zh_TW';

INSERT INTO `service` VALUES(80, 1, '新エントリー', 10, 0, 10, 0, 2048, 256, 80, '160x120', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 45000, 34900, 25, 1200, '2012-08-30 11:51:30', 1, 0);
INSERT INTO `service` VALUES(81, 1, '新Premium20', 9, 11, 10, 0, 4096, 256, 80, '160x120', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 45000, 119900, 0, 60000, '2012-08-30 11:51:30', 1, 0);
INSERT INTO `service` VALUES(82, 1, '新Premium30', 9, 21, 10, 0, 6144, 256, 80, '160x120', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 45000, 149900, 0, 60000, '2012-08-30 11:51:30', 1, 0);
INSERT INTO `service` VALUES(83, 1, '新Premium40', 9, 31, 10, 0, 8192, 256, 80, '160x120', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 45000, 179900, 0, 60000, '2012-08-30 11:51:30', 1, 0);
INSERT INTO `service` VALUES(84, 1, '新Premium50', 9, 41, 10, 0, 10240, 256, 80, '160x120', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 45000, 209900, 0, 60000, '2012-08-30 11:51:30', 1, 0);
INSERT INTO `service` VALUES(85, 1, '新同時2拠点（大塚商会）', 2, 0, 0, 0, 2048, 256, 80, '160x120', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 20000, 20000, 0, 60000, '2012-08-30 11:51:30', 1, 0);
INSERT INTO `service` VALUES(86, 1, '新スマート（大塚商会）', 3, 0, 0, 0, 2048, 256, 80, '160x120', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 30000, 30000, 0, 60000, '2012-08-30 11:51:30', 1, 0);
INSERT INTO `service` VALUES(87, 1, '新スマートプラス（大塚商会）', 5, 0, 0, 0, 2048, 256, 80, '160x120', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 50000, 50000, 0, 60000, '2012-08-30 11:51:30', 1, 0);
INSERT INTO `service` VALUES(88, 1, '新Premium20プラス', 20, 0, 10, 0, 4096, 256, 80, '160x120', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 45000, 129900, 0, 10000, '2012-08-30 11:51:30', 1, 0);

UPDATE service set max_room_bandwidth = 6144 WHERE service_key = 74;
UPDATE service set max_room_bandwidth = 3072 WHERE service_key = 75;
