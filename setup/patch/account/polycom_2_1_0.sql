ALTER TABLE  `mcu_server` ADD  `controller_port` INT( 4 ) NOT NULL DEFAULT  '8080' AFTER  `server_address` ,
ADD  `socket_port` INT( 4 ) NOT NULL DEFAULT  '1500' AFTER  `controller_port` ,
ADD  `sip_proxy_port` INT( 4 ) NOT NULL DEFAULT  '5060' AFTER  `socket_port`;

ALTER TABLE  `mcu_server` ADD  `ip` VARCHAR( 20 ) NOT NULL AFTER  `server_address` ,
ADD INDEX ( ip );

ALTER TABLE  `mcu_server` ADD  `number_domain` VARCHAR( 255 ) NOT NULL AFTER  `server_address`;