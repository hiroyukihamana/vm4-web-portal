-- IP制限
ALTER TABLE `ip_whitelist` DROP INDEX `user_key`;
ALTER TABLE `ip_whitelist` DROP INDEX `ip`;
ALTER TABLE `ip_whitelist` DROP INDEX `status`;

ALTER TABLE `ip_whitelist` CHANGE `registtime` `create_datetime` DATETIME NOT NULL COMMENT '作成日時';
ALTER TABLE `ip_whitelist` CHANGE `updatetime` `update_datetime` DATETIME NULL DEFAULT NULL COMMENT '更新日時';
ALTER TABLE `ip_whitelist` CHANGE `ip` `ip` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `ip_whitelist` ADD `ip_whitelist_key` INT NOT NULL AUTO_INCREMENT COMMENT 'KEY' FIRST , ADD PRIMARY KEY ( `ip_whitelist_key` );

ALTER TABLE `ip_whitelist` ADD INDEX ( `user_key` );
ALTER TABLE `ip_whitelist` ADD INDEX ( `ip` );
ALTER TABLE `ip_whitelist` ADD INDEX ( `status` );