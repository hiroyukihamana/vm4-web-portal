ALTER TABLE  `service` ADD  `whiteboard_page` INT NOT NULL DEFAULT  '0' COMMENT  'アップロード可能ページ数' AFTER  `hd_flg` ,
ADD  `whiteboard_size` INT NOT NULL DEFAULT  '0' COMMENT  'アップロード可能ファイルサイズ' AFTER  `whiteboard_page` ,
ADD  `document_filetype` VARCHAR( 255 ) NULL DEFAULT NULL COMMENT  'ップロード可能ドキュメント（拡張子をカンマ区切りで登録）' AFTER  `whiteboard_size` ,
ADD  `image_filetype` VARCHAR( 255 ) NULL DEFAULT NULL COMMENT  'ップロード可能画像（拡張子をカンマ区切り）' AFTER  `document_filetype`,
ADD  `cabinet_filetype` VARCHAR( 255 ) NULL COMMENT  '利用キャビネットファイルタイプ（カンマ区切り）' AFTER  `image_filetype`;