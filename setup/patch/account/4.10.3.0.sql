/* MTGVFOUR-827【S&S】Web_マネージャー機能の追加 */
/* 新type S&S 監視者として設定、participant_type_key=23以外ではTOP画面のあと○○人の表示が正しい数とならない状態になります*/

INSERT INTO `participant_type` (`participant_type_key`,`participant_type_name`,`create_datetime`) VALUES ('23','ss_watcher',now()) ;
