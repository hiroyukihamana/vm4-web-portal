--
-- テーブルの構造 `sastik_account_translation`
--

CREATE TABLE `sastik_account_translation` (
  `account_translation_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_host_key` int(10) unsigned NOT NULL, 
  `account_id` varchar(64) NOT NULL COMMENT 'アカウント名', 
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '削除フラグ', 
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日時', 
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日時', 
  PRIMARY KEY (`account_translation_key`), 
  KEY `account_id` (`account_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


--
-- テーブルの構造 `sastik_account_host`
--

CREATE TABLE `sastik_account_host` (
  `account_host_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) NOT NULL,
  `account_host` varchar(64) NOT NULL COMMENT '顧客サーバIP',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日時',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日時',
  PRIMARY KEY (`account_host_key`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

