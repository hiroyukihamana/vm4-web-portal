INSERT INTO `participant_type` (`participant_type_key`, `participant_type_name`, `create_datetime`, `update_datetime`) VALUES (16, 'staff', NOW(), NULL);
INSERT INTO `participant_type` (`participant_type_key`, `participant_type_name`, `create_datetime`, `update_datetime`) VALUES (17, 'customer', NOW(), NULL);

ALTER TABLE  `service` ADD  `sales_flg` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'セールスオプション' AFTER  `hd_flg` ,
ADD  `use_room_plan` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'ルームプラン利用フラグ' AFTER  `sales_flg` ,
ADD  `use_member_plan` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'ID制利用フラグ' AFTER  `use_room_plan` ,
ADD  `use_sales_plan` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  'セールスプラン利用フラグ' AFTER  `use_member_plan`;