ALTER TABLE  `datacenter` ADD  `status` INT( 4 ) NOT NULL DEFAULT  '1' COMMENT  'ステータス' AFTER  `datacenter_country`;
ALTER TABLE  `datacenter` ADD  `sort` INT NOT NULL DEFAULT '1' COMMENT  '優先順位' AFTER  `status`;

INSERT INTO `question_branch` (`question_branch_id` ,`question_id` ,`lang1` ,`lang2` ,`lang3` ,`lang4` ,`lang5` ,`sort` ,`created_datetime` ,`updated_datetime` ,`deleted_datetime`) VALUES (NULL ,  '3',  '音が頻繁に途切れる',  'Lose sound very often',  '经常听不到声音',  '',  '',  '3', NOW( ) ,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00');

UPDATE  `question_branch` SET  `sort` =  '4' WHERE  `question_branch`.`question_branch_id` =13 LIMIT 1 ;
UPDATE  `question_branch` SET  `sort` =  '5' WHERE  `question_branch`.`question_branch_id` =14 LIMIT 1 ;
UPDATE  `question_branch` SET  `sort` =  '6' WHERE  `question_branch`.`question_branch_id` =15 LIMIT 1 ;

UPDATE  `question_branch` SET  `sort` =  '3' WHERE  `question_branch`.`question_branch_id` =2;
UPDATE  `question_branch` SET  `sort` =  '2' WHERE  `question_branch`.`question_branch_id` =3;


UPDATE `question` SET `lang3` = '您对本次网络视频会议的使用满意吗？' WHERE `question`.`question_id` = 1;
UPDATE `question` SET `lang3` = '关于图像（可以多选）' WHERE `question`.`question_id` = 2;
UPDATE `question` SET `lang3` = '关于语音（可以多选）' WHERE `question`.`question_id` = 3;
UPDATE `question` SET `lang3` = '关于设计（可以多选）' WHERE `question`.`question_id` = 4;
UPDATE `question` SET `lang3` = '其他（可以多选）' WHERE `question`.`question_id` = 5;
UPDATE `question` SET `lang3` = '请选择您使用网络视频会议的频率。' WHERE `question`.`question_id` = 6;
UPDATE `question` SET `lang3` = '您这次的网络视频会议属于什么类型？' WHERE `question`.`question_id` = 7;
UPDATE `question` SET `lang3` = '这次会议的参会者身份是什么？（可以多选）' WHERE `question`.`question_id` = 8;
UPDATE `question` SET `lang3` = '请您填写其他的意见或建议。（自由阐述）' WHERE `question`.`question_id` = 9;


UPDATE  `question_branch` SET  `lang3` =  '非常满意' WHERE  `question_branch`.`question_branch_id` =1;
UPDATE  `question_branch` SET  `lang3` =  '比较满意' WHERE  `question_branch`.`question_branch_id` =2;
UPDATE  `question_branch` SET  `lang3` =  '满意' WHERE  `question_branch`.`question_branch_id` =3;
UPDATE  `question_branch` SET  `lang3` =  '都不是' WHERE  `question_branch`.`question_branch_id` =4;
UPDATE  `question_branch` SET  `lang3` =  '比较不满意' WHERE  `question_branch`.`question_branch_id` =5;
UPDATE  `question_branch` SET  `lang3` =  '不满意' WHERE  `question_branch`.`question_branch_id` =6;
UPDATE  `question_branch` SET  `lang3` =  '非常不满意' WHERE  `question_branch`.`question_branch_id` =7;

UPDATE  `question_branch` SET  `lang3` =  '未显示对方图像' WHERE  `question_branch`.`question_branch_id` =8;
UPDATE  `question_branch` SET  `lang3` =  '未显示我方图像' WHERE  `question_branch`.`question_branch_id` =9;
UPDATE  `question_branch` SET  `lang3` =  '图像质量差' WHERE  `question_branch`.`question_branch_id` =10;

UPDATE  `question_branch` SET  `lang3` =  '无法接通我方语音' WHERE  `question_branch`.`question_branch_id` =11;
UPDATE  `question_branch` SET  `lang3` =  '无法听到对方语音' WHERE  `question_branch`.`question_branch_id` =12;
UPDATE  `question_branch` SET  `lang3` =  '经常听不到声音' WHERE  `question_branch`.`question_branch_id` =42;
UPDATE  `question_branch` SET  `lang3` =  '语音延迟' WHERE  `question_branch`.`question_branch_id` =13;
UPDATE  `question_branch` SET  `lang3` =  '语音回响（听起来有回音）' WHERE  `question_branch`.`question_branch_id` =14;
UPDATE  `question_branch` SET  `lang3` =  '语音啸响（声音尖锐刺耳、大声作响）' WHERE  `question_branch`.`question_branch_id` =15;

UPDATE  `question_branch` SET  `lang3` =  '功能（按钮）太多令操作困惑' WHERE  `question_branch`.`question_branch_id` =16;
UPDATE  `question_branch` SET  `lang3` =  '有些部分设计难懂（不易使用）。（麻烦您填写详细内容）' WHERE  `question_branch`.`question_branch_id` =17;

UPDATE  `question_branch` SET  `lang3` =  '突然中断' WHERE  `question_branch`.`question_branch_id` =18;
UPDATE  `question_branch` SET  `lang3` =  '桌面共享无法开始' WHERE  `question_branch`.`question_branch_id` =19;
UPDATE  `question_branch` SET  `lang3` =  '桌面共享无法阅览' WHERE  `question_branch`.`question_branch_id` =20;
UPDATE  `question_branch` SET  `lang3` =  '整体反应慢' WHERE  `question_branch`.`question_branch_id` =21;
UPDATE  `question_branch` SET  `lang3` =  '白板操作有些不便（因为功能较多、麻烦您填写详细内容）' WHERE  `question_branch`.`question_branch_id` =22;

UPDATE  `question_branch` SET  `lang3` =  '每月1次' WHERE  `question_branch`.`question_branch_id` =23;
UPDATE  `question_branch` SET  `lang3` =  '每月2~3次' WHERE  `question_branch`.`question_branch_id` =24;
UPDATE  `question_branch` SET  `lang3` =  '每月4次（每周一次）' WHERE  `question_branch`.`question_branch_id` =25;
UPDATE  `question_branch` SET  `lang3` =  '每月4次以上' WHERE  `question_branch`.`question_branch_id` =26;
UPDATE  `question_branch` SET  `lang3` =  '不定期' WHERE  `question_branch`.`question_branch_id` =27;

UPDATE  `question_branch` SET  `lang3` =  '营业会议' WHERE  `question_branch`.`question_branch_id` =28;
UPDATE  `question_branch` SET  `lang3` =  '企划会议' WHERE  `question_branch`.`question_branch_id` =29;
UPDATE  `question_branch` SET  `lang3` =  '报告会议' WHERE  `question_branch`.`question_branch_id` =30;
UPDATE  `question_branch` SET  `lang3` =  '商业谈判' WHERE  `question_branch`.`question_branch_id` =31;
UPDATE  `question_branch` SET  `lang3` =  '培训•教育' WHERE  `question_branch`.`question_branch_id` =32;
UPDATE  `question_branch` SET  `lang3` =  '经营会议•董事会议' WHERE  `question_branch`.`question_branch_id` =33;
UPDATE  `question_branch` SET  `lang3` =  '其他' WHERE  `question_branch`.`question_branch_id` =34;

UPDATE  `question_branch` SET  `lang3` =  '经营者' WHERE  `question_branch`.`question_branch_id` =35;
UPDATE  `question_branch` SET  `lang3` =  '管理者' WHERE  `question_branch`.`question_branch_id` =36;
UPDATE  `question_branch` SET  `lang3` =  '负责人' WHERE  `question_branch`.`question_branch_id` =37;
UPDATE  `question_branch` SET  `lang3` =  '交易户' WHERE  `question_branch`.`question_branch_id` =38;
UPDATE  `question_branch` SET  `lang3` =  '顾客' WHERE  `question_branch`.`question_branch_id` =39;
UPDATE  `question_branch` SET  `lang3` =  '在家办公者' WHERE  `question_branch`.`question_branch_id` =40;
UPDATE  `question_branch` SET  `lang3` =  '其他' WHERE  `question_branch`.`question_branch_id` =41;
