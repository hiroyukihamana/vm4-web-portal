<?php
require_once 'classes/N2MY_Batch.class.php';
class N2MY_BATCH_4_5_1_4 extends N2MY_Batch {
    var $frame = null;

    /**
     * コンストラクタ
     */
    function __construct($frame) {
        $this->frame = $frame;
    }

    function getInfo() {
        $batch_info = <<<EOM
会議利用ユーザーの利用統計用データで、入室経路が招待メールや会議内呼び出しである場合に、
ユーザーIDが記録されていない。会議情報＞部屋＞ユーザで取得し、再設定する。
EOM;
        return $batch_info;
    }

    /**
     * 前処理
     */
    function prepare() {
    }

    /**
     * バックアップ
     */
    function backup() {
    }

    /**
     * ロールバック
     */
    function roleback() {
    }

    /**
     * 実行
     */
    function execute() {
        require_once 'classes/mgm/dbi/user_agent.dbi.php';
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        $dsn = $_SESSION["_adminauthsession"]["dsn"];
        $objUserAgent = new MgmUserAgentTable(N2MY_MDB_DSN);
        $objMeeting = new MeetingTable($dsn);
        $objUser    = new UserTable($dsn);
        // USER_ID が NULL の情報を取得
        $user_agent_list = $objUserAgent->getRowsAssoc("user_id IS NULL", null, null, 0, "user_agent_key, meeting_key", "user_agent_key");
        print '<table border=1 width="100%">';
        print "<tr>";
        print "<th>user_agent_key</th>";
        print "<th>meeting_key</th>";
        print "<th>user_id</th>";
        print "</tr>";
        foreach ($user_agent_list as $user_agent_key => $user_agent) {
            print "<tr>";
            print "<td>".htmlspecialchars($user_agent_key)."</td>";
            print "<td>".htmlspecialchars($user_agent["meeting_key"])."</td>";
            // ユーザーKEY取得
            $user_info = null;
            if ($user_key = $objMeeting->getOne("meeting_key = '".addslashes($user_agent["meeting_key"])."'", "user_key")) {
                // ユーザー情報
                if ($user_info = $objUser->getRow("user_key = '".addslashes($user_key)."'", "user_id, user_status")) {
                    $user_agent_info = array(
                        "user_id"    => $user_info["user_id"],
                        "appli_mode" => ($user_info["user_status"] == 2) ? "trial" : "general",
                        );
                    // 更新
                    $this->frame->logger2->info(array($user_agent_info, "user_agent_key = ". $user_agent_key));
                    $ret = $objUserAgent->update($user_agent_info, "user_agent_key = ". $user_agent_key);
                    if (DB::isError($ret)) {
                        $this->frame->logger2->error($ret->getUserInfo());
                        exit;
                    }
                }
            }
            print "<td>".htmlspecialchars($user_info["user_id"])."</td>";
            print "</tr>";
        }
        print "<table>";
        exit;
    }

    /**
     * 後処理
     */
    function destroy() {
    }
}
?>
