<?php
require_once 'classes/N2MY_Batch.class.php';
class N2MY_BATCH_4_3_1_0 extends N2MY_Batch {
    var $frame = null;

    function __construct($frame) {
        $this->frame = $frame;
    }

    function getInfo() {
        $batch_info = <<<EOM
4.3.1.0 以前の会議情報に会議に参加したメンバーID、及び参加者名を改行コード区切りで再設定する。
EOM;
        return $batch_info;
    }

    function prepare() {
    }

    function execute() {
        // 4.3.1の差分追加
        // タイムアウト無し
        set_time_limit(0);
        require_once('classes/core/dbi/Meeting.dbi.php');
        require_once('classes/core/dbi/Participant.dbi.php');
        $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            // DBサーバ一覧
            $obj_Meeting            = new DBI_Meeting( $dsn );
            $obj_Participant        = new DBI_Participant( $dsn );
            // 利用された会議（削除されたデータも対象）
            $where = "is_active = 0".
                " AND use_flg = 1";
            $meeting_keys = $obj_Meeting->getCol($where, "meeting_key");
            $this->frame->logger2->info($meeting_keys);
            foreach($meeting_keys as $meeting_key) {
                // 参加者情報を索引として登録
                $where = "meeting_key = ".$meeting_key.
                    " AND use_count > 0".
                    " AND is_active = 0";
                $participant_list = $obj_Participant->getRowsAssoc($where, null, null, null, "member_key,participant_name");
                $member_keys = array();
                $participant_names = array();
                foreach($participant_list as $_key => $participant) {
                    if ($participant["member_key"]) {
                        $member_keys[] = $participant["member_key"];
                    }
                    $participant_names[] = $participant["participant_name"];
                }
                $member_keys = array_unique($member_keys);
                $participant_names = array_unique($participant_names);
                // 改行コードできって検索しやすくする
                $participant_info["member_keys"] = join($member_keys,"\n");
                $participant_info["participant_names"] = join($participant_names,"\n");
                $this->frame->logger2->info(array($meeting_key, $participant_info));
                $where = "meeting_key = ".$meeting_key;
                $obj_Meeting->update($participant_info, $where);
            }
        }
        return true;
    }

    function destroy() {
    }
}
?>
