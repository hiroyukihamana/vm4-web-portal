<?php
require_once 'classes/N2MY_Batch.class.php';
class N2MY_BATCH_4_5_12_3 extends N2MY_Batch {
    var $frame = null;
    var $standard_plan_keys = array();
    var $paperless_plan     = array();

    function __construct($frame) {
        $this->frame = $frame;
        $this->standard_plan_keys = array(
            "43" => "Standard",
            "44" => "High_Quality",
            "45" => "Premium20",
            "46" => "Premium30",
            "47" => "Premium40",
            "50" => "Premium50",
            "51" => "Premium60",
            "52" => "Premium70",
            "53" => "Premium80",
            "54" => "Premium90",
            "55" => "Premium100",
            "56" => "資料共有プラン",
            "65" => "Premium20プラス",
            "66" => "Premium30プラス"
        );
        $this->paperless_plan = array("56");
    }

    function getInfo() {
        $batch_info = <<<EOM
スタンダード以上で、資料ユーザー数が設定されていない既存ユーザに資料ユーザーを10拠点追加する。
スタンダードプランのIDは、構築した環境によって異なるため以下設定に間違いが無いか必ず確認すること！
※現在は、ASPのデータを元に作成されています。


EOM;
        $batch_info .= var_export($this->standard_plan_keys, TRUE);
        return $batch_info;
    }

    function prepare() {
    }

    function execute() {
        set_time_limit(0);
        require_once('classes/dbi/room.dbi.php');
        require_once('classes/dbi/room_plan.dbi.php');
        $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        print "<table border=1>";
        print "<tr bgcolor='#407dc7'>";
        print "<th>room_key</th>";
        print "<th>room_name</th>";
        print "<th>plan</th>";
        print "<th>max_whiteboard_seat</th>";
        print "</tr>";
        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            // DBサーバ一覧
            $obj_Room       = new RoomTable( $dsn );
            $obj_RoomPlan   = new RoomPlanTable( $dsn );
            // 利用された会議（削除されたデータも対象）
            $target_date = date('Y-m-d');
            $where = " (room_plan_starttime >= '". $target_date ." 00:00:00' OR" .
                      " room_plan_starttime < '". $target_date ." 00:00:00')" .
                 " AND (room_plan_endtime IS NULL OR" .
                      " room_plan_endtime = '0000-00-00 00:00:00' OR" .
                      " room_plan_endtime > '". $target_date ." 00:00:00')" .
                 " AND (room_plan_status = 1 OR room_plan_status = 2)".
                 " AND service_key IN (".join(", ", array_keys($this->standard_plan_keys)).")";
            $room_keys = $obj_RoomPlan->getRowsAssoc($where, array('room_key' => "asc"), NULL, NULL, "room_key,service_key", "room_key");
            foreach($room_keys as $room_key => $plan_info) {
                $where = "room_key = '".addslashes($room_key)."'";
                $room_info = $obj_Room->getRow($where);
                if (in_array($plan_info['service_key'], $this->paperless_plan) && $room_info["max_whiteboard_seat"] < 20) {
                    $seat_num = 20;
                } elseif ($room_info["max_whiteboard_seat"] < 10) {
                    $seat_num = 10;
                } else {
                    $seat_num = 0;
                }
                // 更新
                if ($seat_num == 0) {
                    print '<tr bgcolor="#cccccc">';
                } else {
                    print '<tr>';
                    $this->frame->logger2->info(array($room_info, $plan_info, $seat_num));
                    $obj_Room->update(array("max_whiteboard_seat" => $seat_num), $where);
                }
                print "<td>".htmlspecialchars($room_key)."</td>";
                print "<td>".htmlspecialchars($room_info["room_name"])."</td>";
                print "<td>".htmlspecialchars($this->standard_plan_keys[$plan_info['service_key']])."</td>";
                print "<td>".$room_info["max_whiteboard_seat"]." > ".$seat_num."</td>";
                print "</tr>";
            }
        }
        print "<table>";
        exit();
    }

    function destroy() {
    }
}
?>
