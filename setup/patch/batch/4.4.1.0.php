<?php
require_once 'classes/N2MY_Batch.class.php';
class N2MY_BATCH_4_4_1_0 extends N2MY_Batch {
    var $frame = null;

    /**
     * コンストラクタ
     */
    function __construct($frame) {
        $this->frame = $frame;
    }

    function getInfo() {
        $batch_info = <<<EOM
ユーザー毎にメール資料貼り込み制限をつけていたが、部屋ごとに設定可能になったため
部屋ごとの設定を、ユーザーで設定していたデフォルト値で登録する。
EOM;
        return $batch_info;
    }

    /**
     * 前処理
     */
    function prepare() {
    }

    /**
     * バックアップ
     */
    function backup() {
    }

    /**
     * ロールバック
     */
    function roleback() {
    }

    /**
     * 実行
     */
    function execute() {
        require_once('classes/dbi/user.dbi.php');
        require_once('classes/dbi/room.dbi.php');
        $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            // 部屋一覧取得
            $where = "";
            $obj_User = new UserTable( $dsn );
            $obj_Room = new RoomTable( $dsn );
            $room_keys = $obj_Room->getRowsAssoc($where, array("user_key" => "ASC", "room_key" => "ASC"), null, 0, "user_key, room_key");
            $user_key = "";
            $sort = 1;
            foreach ($room_keys as $room_info) {
                if ($room_info["user_key"] != $user_key) {
                    $mfp = "all";
                    $where = "user_key = ".$room_info["user_key"];
                    $user_info = $obj_User->getRow($where);
                    if ($user_info["addition"]) {
                        if ($addition = @unserialize($user_info["addition"])) {
                            switch ($addition["mail_doc_conv"]) {
                                case "0":
                                    $mfp = "deny";
                                    break;
                                case "1":
                                    $mfp = "select";
                                    break;
                                case "2":
                                    $mfp = "all";
                                    break;
                            }
                        } else {
                            // メモ用の項目として使っていたらNGなので
                            $user_data = array(
                                "addition" => "",
                                "memo" => $user_info["addition"]
                                );
                            $this->frame->logger2->info(array($user_data, $where), "");
                            $obj_User->update($user_data, $where);
                        }
                    }
                    $sort = 1;
                }
                $where = "user_key = ".$room_info["user_key"].
                    " AND room_key = '".addslashes($room_info["room_key"])."'";
                $room_data = array(
                    "room_sort" => $sort,
                    "mfp" => $mfp
                    );
                $this->frame->logger2->info(array($room_data, $where), "room");
                $obj_Room->update($room_data, $where);
                $user_key = $room_info["user_key"];
                $sort++;
            }
        }
        return true;
    }

    /**
     * 後処理
     */
    function destroy() {
    }
}
?>
