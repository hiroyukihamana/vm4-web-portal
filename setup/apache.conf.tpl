#-------------------------
# バーチャルで環境を構築する場合
#-------------------------
NameVirtualHost xxx.xxx.xxx.xxx:80

<VirtualHost xxx.xxx.xxx.xxx:80>
    ServerAdmin sample@example.com
    DocumentRoot /home/project/meeting/htdocs
    ServerName meeting.example.com

    ErrorLog /home/project/meeting/logs/error_log
    SetEnvIf Request_URI "\.css" dontlog
    SetEnvIf Request_URI "\.gif" dontlog
    SetEnvIf Request_URI "\.ico" dontlog
    SetEnvIf Request_URI "\.jpg" dontlog
    SetEnvIf Request_URI "\.js"  dontlog
    SetEnvIf Request_URI "\.png" dontlog
    CustomLog "|/usr/local/apache/bin/rotatelogs \
       /home/project/meeting/logs/access_log.%Y%m%d 604800 540" common env=!dontlog
    ErrorDocument 404 /error.html

    <Directory /home/project/meeting/>
        Options ExecCGI FollowSymLinks MultiViews Includes
        SetEnv CONFIG_FILE /home/project/meeting/config/config.ini
        AllowOverride all
    </Directory>
</VirtualHost>

#-------------------------
# エイリアスで構築を構築する場合（相対パス周りが色々解決できてませんが・・・。）
#-------------------------
Alias /n2my/meeting/ "/home/project/meeting/htdocs/"
<Directory "/home/project/meeting/htdocs">
    Options ExecCGI FollowSymLinks MultiViews Includes
    SetEnv CONFIG_FILE /home/project/meeting/config/config.ini
    AllowOverride all
</Directory>
