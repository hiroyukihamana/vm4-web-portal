#!{N2MY_PHP_DIR}
<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Meeting.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/mail_doc_conv.dbi.php");
require_once("lib/pear/Mail/mimeDecode.php");

/* syslog 用エラーメッセージ */
define("ENOHEADERS",  "200: No valid mail header");
define("ENOMAILDATA", "201: No valid mail body");
define("ENOFROM",     "202: No valid from address");
define("EMULTIPLE",   "203: Multiple binary files");

/* Notice 回避 */
//$_SERVER['REQUEST_METHOD'] = "MAIL";

class AppMailUpload extends AppFrame {

    var $room_obj = null;

    /**
     * チェック
     */
    function check_message($data) {
        // エラーチェック
        $rules = array(
            "email" => array(
                "required" => true,
                ),
            );
        $check_obj = $this->room_obj->check($data, $rules);
        // 部屋の存在確認
        if ($data["account"]) {
            $room_row = $this->room_obj->getRow("room_key = '".addslashes($data["account"])."'");
            if (!$room_row) {
                $check_obj->set_error("room_key", "required", $data["account"]);
            } else {
                // 有効/無効
                $where = "user_key = ".$room_row["user_key"];
                $user_info = $this->user_obj->getRow($where);
                if (!$user_info) {
                    $this->logger2->warn($where);
                } else {
                    if ($user_info["addition"]) {
                        $addition = unserialize($user_info["addition"]);
                        $mail_doc_conv = isset($addition["mail_doc_conv"]) ? $addition["mail_doc_conv"] : 2;
                    } else {
                        $mail_doc_conv = 2;
                    }
                }
                switch ($mail_doc_conv) {
                case 1:
                    // 有効なリスト
                    $this->mail_doc_obj = new MailDocConvet($this->get_dsn(), $room_row["user_key"]);
                    $where = "is_active = 1";
                    $allowlist = $this->mail_doc_obj->getList($where, null, null, null, null, "mail_address");
                    $this->logger2->debug($allowlist);
                    $allow_flg = false;
                    foreach ($allowlist as $allow_mail) {
                        if (mb_ereg($allow_mail["mail_address"], $data["email"])) {
                             $allow_flg = true;
                             break;
                        }
                    }
                    if ($allow_flg == false) {
                         $check_obj->set_error("user_key", "disabled", $mail_doc_conv);
                    }
                    break;
                case 2:
                    $this->logger2->debug("allow all");
                    break;
                default :
                    $this->logger2->debug("deny all");
                    $check_obj->set_error("user_key", "disabled", $mail_doc_conv);
                    break;
                }
            }
        }
        $err_fields = $check_obj->error_fields();
        if ($err_fields) {
            $this->logger2->info(array($err_fields));
        }
        return $check_obj;
    }

    /**
     * 基本処理
     */
    function default_view() {
        /* MIME decode パラメタ */
        $flags = array(
            'include_bodies' => true,         /* body を取得する */
            'decode_bodies'  => false,        /* body を decode する */
            'decode_headers' => true,        /* header を decode する */
            'crlf' => '\n',                   /* 改行コード指定 */
            'input' => NULL                   /* 入力データ */
            );
        if ($stdin = fopen("php://stdin", "r")) {
            /* メイル本文の取得 */
            while(!feof($stdin)) {
                $flags['input'] .= fgets($stdin);
            }
            fclose($stdin);
            $mail_info = $this->procmail(Mail_mimeDecode::decode($flags));
            $data["name"] = (trim($mail_info["SUBJECT"])) ? $mail_info["SUBJECT"] : "(匿名)";
            $data["email"] = $mail_info["FROM"];
            $career = substr($mail_info["FROM"], strrpos($mail_info["FROM"], "@") + 1);
            $data["message"] = $mail_info["MESSAGE"];
            if ($mail_info["TO"]) {
                $data["account"] = substr($mail_info["TO"], 0, strpos($mail_info["TO"], "@"));
            }
            $obj_MGMAuthClass = new MGM_AuthClass(N2MY_MDB_DSN);
            $dsn_info = $obj_MGMAuthClass->getRelationDsn($data["account"], "mfp");
            $this->room_obj = new RoomTable($dsn_info["dsn"]);
            $this->user_obj = new UserTable($dsn_info["dsn"]);
            // チェック処理
            $check_obj = $this->check_message($data);
            if (EZValidator::isError($check_obj)) {
                $this->logger2->warn($check_obj);
                return false;
            }
            // 対象の部屋で会議が行われているかチェック
            $room_key = $data["account"];
            $now_row = $this->room_obj->getRow("room_key = '".addslashes($room_key)."'");
            $last_meeting_key = $now_row["meeting_key"];

            $obj_Meeting = new N2MY_Meeting($dsn_info["dsn"]);
            $last_meeting_info = $obj_Meeting->getMeetingStatus($room_key, $last_meeting_key);
            $this->logger2->debug(array(
                "room_key" => $room_key,
                "last_meeting_key" => $last_meeting_key,
                "last_meeting_info" => $last_meeting_info,
                ));
            // アップロード可能か確認
            if (($last_meeting_info["status"] == "0") || ($last_meeting_info["pcount"] == 0)) {
                $this->logger2->warn(array(
                    "last_meeting_status" => $last_meeting_info["status"],
                    "participant_count" => $last_meeting_info["pcount"]));
            } else {
                if ($mail_info["FILES"]) {
                    // 対応フォーマット取得
                    $convert_format = split(",", $this->config->get("N2MY", "convert_format"));
                    // 全ファイルを処理する
                    foreach ($mail_info["FILES"] as $file) {
                        // 一時ファイル
                        $tmp_file = N2MY_APP_DIR."tmp/whiteboard/".date("Ymd")."_".uniqid();
                        // 拡張子
                        $ext = strtolower(substr($file["FILENAME"], strrpos($file["FILENAME"], ".") + 1));
                        $tmp_file .= ".".$ext;
                        $type = $file["MIMETYPE"];
                        // 未対応のフォーマット
                        if (!in_array($ext, $convert_format)) {
                            $this->logger2->warn(array(
                                "allow_ext" => $convert_format,
                                "name" => $tmp_file,
                                "type" => $type,
                                "size" => filesize($tmp_file),
                                "extension" => $ext,
                                ));
                        // 会議資料アップロード
                        } else {
                            file_put_contents($tmp_file, base64_decode($file["BODY"]));
                            $meeting_key = $last_meeting_info["meeting_key"];
                            $this->wb_upload($dsn_info["host_name"], $meeting_key, $tmp_file);
                            unlink($tmp_file);
                        }
                    }
                }
            }
        }
    }

    /**
     * 資料アップロード
     *
     * @param string $db_host DBホストID
     * @param integer $meeting_key 会議キー
     * @param string $filename ファイル名(必ず拡張子付きにしてください)
     */
    function wb_upload($db_host, $meeting_key, $filename) {
        // プロバイダID
        $ch = @curl_init();
        $url = N2MY_BASE_URL."api/document/index.php";
        $ch = curl_init();
        $post_data = array(
            "action_wb_upload"  => "",
            "db_host"           => $db_host,
            "meeting_key"       => $meeting_key,
            "Filedata" 	        => "@".$filename,
            );
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 1,
            CURLOPT_TIMEOUT => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            );
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        $this->logger2->info($post_data, $ret);
    }

    /*
     * メイル本文の解析と必要な情報の取得
     *
     * @param    object    decode 済みメイルデータ
     * @return    array    API 通知データ
     */
    function procmail($st) {
        if(isset($st->headers)) {
            $headers = $this->procheader($st->headers);
            $bodys   = $this->procbody($st);
            // キャリア別に本文置換
            $headers["SUBJECT"] = $this->emoji_escape($headers["SUBJECT"]);
            $bodys["MESSAGE"]  = $this->emoji_escape($bodys["MESSAGE"]);
            return array_merge($headers, (($bodys) ? $bodys : array()));
        } else {
            $this->logger2->error(ENOHEADERS);
            return false;
        }
    }

    /**
     * 絵文字をエスケープ
     */
    function emoji_escape($str) {
        $original = mb_substitute_character();
        mb_substitute_character(0x3013);
        $str = str_replace("〓", "", mb_convert_encoding($str, 'UTF-8', 'JIS'));
        mb_substitute_character($original);
        return $str;
    }

    /*
     * ヘッダ情報の取得
     *
     * @param    object    decode 済みメイルヘッダ
     * @return    array    API 通知データ
     */
    function procheader($header) {
        return array(
            "FROM"    => isset($header['from']) ? $this->realaddr($header['from']) : NULL,
            "TO"      => isset($header['to']) ? $this->realaddr($header['to']) : NULL,
            "SUBJECT" => isset($header['subject']) ? mb_decode_mimeheader($header['subject']) : NULL,
            );

    }
    /*
     * メイル本文の取得
     *
     * @param    object    decode 済みメイル本文
     * @return    array    API 通知データ
     */
    function procbody($st) {
//        $this->logger2->info(__FUNCTION__,__FILE__,__LINE__, $st->parts);
        if (isset($st->parts)) {
            $bodys =& $st->parts;
            $this->logger2->debug($bodys);
            $mail = NULL;
            if(isset($bodys)) {
                foreach($bodys as $body) {
                    //$this->logger2->info(__FUNCTION__,__FILE__,__LINE__, $body);
                    /* content-type が text/plain ならメッセージとみなす */
                    if(preg_match("/text\/plain/i", $body->headers["content-type"])) {
                        $mail["MESSAGE"]  = $body->body;
                    // multipart/mix HTMLメールにも対応する？
                    } else if(preg_match("/multipart\/alternative/i", $body->headers["content-type"])) {
                        $_bodys = $body->parts;
                        foreach($_bodys as $_body) {
                            if(preg_match("/text\/plain/i", $_body->headers["content-type"])) {
                                $mail["MESSAGE"]  = $_body->body;
                                $this->logger2->info($mail["MESSAGE"]);
                                break;
                            }
                        }
                    } else {
                        /* name/name* が指定されていればファイル添付 */
                        if(isset($body->ctype_parameters["name"]) && strlen($body->ctype_parameters["name"])) {
                                /*
                                 * base64 以外の encode の可能性もあるので
                                 * 一度 decode したものを
                                 * 明示的に base64 で encode する
                                 */
                                $mail["FILES"][] = array(
                                    "FILENAME" => $body->ctype_parameters["name"],
                                    "MIMETYPE" => $body->ctype_primary."/".$body->ctype_secondary,
                                    "BODY"     => $body->body,
                                );
                        }
                    }
                }
            }
        } else {
            if(preg_match("!text/plain!", $st->headers['content-type'])) {
                $mail["MESSAGE"]  = $st->body;
            }
        }
       return($mail);
    }

    /*
     * メイルアドレスを正規化して addr のみを取得
     *
     * @param    string    修飾されたメイルアドレス(from/to)
     * @return    string    正規化したメイルアドレス
     */
    function realaddr($mail) {
        $mail = addslashes($mail);
        $mail = str_replace('"','',$mail);
        //署名付きの場合の処理を追加
        preg_match("/<.*>/",$mail,$str);
        if($str[0]!=""){
            $str=substr($str[0],1,strlen($str[0])-2);
            $mail = $str;
        }
        return $mail;
    }
}
$main = new AppMailUpload();
$main->execute();
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>