/*
VMTG Database Version: 4.9.9.3
Date: 2014-10-07 11:25:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for action
-- ----------------------------
DROP TABLE IF EXISTS `action`;
CREATE TABLE `action` (
  `action_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action_name` text NOT NULL,
  `action_detail` text,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`action_key`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of action
-- ----------------------------
INSERT INTO `action` VALUES ('1', 'upload', null, '2006-06-19 17:35:32', null);
INSERT INTO `action` VALUES ('2', 'filecabinet', null, '2006-06-19 17:35:32', null);
INSERT INTO `action` VALUES ('3', 'chat', null, '2006-06-19 17:35:32', null);
INSERT INTO `action` VALUES ('4', 'wboard', null, '2006-06-19 17:35:32', null);
INSERT INTO `action` VALUES ('5', 'sharing', null, '2006-06-19 17:35:32', null);
INSERT INTO `action` VALUES ('6', 'rec', null, '2006-06-19 17:35:32', null);

-- ----------------------------
-- Table structure for agency
-- ----------------------------
DROP TABLE IF EXISTS `agency`;
CREATE TABLE `agency` (
  `agency_id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_name` varchar(150) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `create_datetime` varchar(45) DEFAULT NULL,
  `delete_datetime` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`agency_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agency
-- ----------------------------

-- ----------------------------
-- Table structure for api_auth
-- ----------------------------
DROP TABLE IF EXISTS `api_auth`;
CREATE TABLE `api_auth` (
  `api_no` int(11) NOT NULL AUTO_INCREMENT COMMENT '連番',
  `appli_name` varchar(100) DEFAULT NULL COMMENT 'アプリケーション名',
  `appli_info` text NOT NULL,
  `api_key` varchar(64) NOT NULL COMMENT 'API_KEY',
  `secret` varchar(64) NOT NULL COMMENT '秘密鍵',
  `status` varchar(1) NOT NULL COMMENT '0:無効, 1:有効',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`api_no`),
  UNIQUE KEY `api_key` (`api_key`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='API認証';

-- ----------------------------
-- Records of api_auth
-- ----------------------------
INSERT INTO `api_auth` VALUES ('1', 'Centre', 'Centre', '2dec10f4df9bf6567c89bc75567842dc52c43d97873b4c337aa0295990d73695', '41a62e1dacf86088c7e2b82f136cd590bd9254455502044c34cd70fd0d8ead88', '1', '2010-11-22 12:01:09', '2010-11-22 12:01:09');
INSERT INTO `api_auth` VALUES ('2', 'PresenceAppli2', 'プレゼンスアプリ用', '8f46a64ca94d1e3e6827162b403b4de97bf7b9f32a02a74b245f0edca8fa1ce8', '57f8a550faf179c172312f11e554285cdd88ea765c5cff719114bf587f1d9edc', '1', '2011-02-18 14:22:23', '2011-02-18 14:22:23');
INSERT INTO `api_auth` VALUES ('3', 'VCUBE ONE', 'VCUBE ONE用APIキー', 'd034c790cf4d2c07a965a66e920d97e8465f3a5e754498de0d3d6c629f8a2152', 'e7e76c00e35ff5249c84c23ab2d0598749c7de0d925d9b1d8ac285df4fdedda4', '1', '2014-08-08 11:09:26', '2014-08-08 11:09:26');

-- ----------------------------
-- Table structure for appli_file
-- ----------------------------
DROP TABLE IF EXISTS `appli_file`;
CREATE TABLE `appli_file` (
  `appli_file_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_type` varchar(64) NOT NULL COMMENT 'アップデーター:installer;アプリケーション:binary',
  `file_name` varchar(64) NOT NULL COMMENT 'ファイル名',
  `file_size` int(10) NOT NULL DEFAULT '0' COMMENT 'バイト/最大1GB',
  `version` varchar(64) NOT NULL COMMENT 'ファイルバージョン',
  `hash` varchar(64) NOT NULL DEFAULT '0' COMMENT 'ハッシュ',
  `is_staging` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ステージングフラグ',
  `is_released` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'リリースフラグ',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日時',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日時',
  PRIMARY KEY (`appli_file_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of appli_file
-- ----------------------------

-- ----------------------------
-- Table structure for convert_file
-- ----------------------------
DROP TABLE IF EXISTS `convert_file`;
CREATE TABLE `convert_file` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `db_host` varchar(100) NOT NULL COMMENT 'DB名',
  `document_path` text NOT NULL COMMENT 'パス',
  `document_id` varchar(255) DEFAULT NULL,
  `document_format` varchar(64) DEFAULT NULL COMMENT '変換後のフォーマット',
  `document_version` varchar(64) DEFAULT NULL COMMENT 'アニメーションのバージョン',
  `extension` varchar(255) NOT NULL DEFAULT '',
  `status` varchar(255) NOT NULL DEFAULT '',
  `priority` int(11) DEFAULT NULL COMMENT '優先度',
  `addition` text NOT NULL COMMENT '拡張',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`no`),
  KEY `document_id` (`document_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of convert_file
-- ----------------------------

-- ----------------------------
-- Table structure for convert_process
-- ----------------------------
DROP TABLE IF EXISTS `convert_process`;
CREATE TABLE `convert_process` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) NOT NULL DEFAULT '',
  `status` varchar(255) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of convert_process
-- ----------------------------

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `country_key` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` text NOT NULL,
  `country_consumption_tax` double NOT NULL DEFAULT '0',
  `country_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`country_key`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('1', 'jp', '5', '2003-08-17 13:13:38');
INSERT INTO `country` VALUES ('2', 'us', '0', '2003-08-17 13:13:38');
INSERT INTO `country` VALUES ('3', 'sg', '5', '2004-01-31 06:51:32');

-- ----------------------------
-- Table structure for custom_domain_service
-- ----------------------------
DROP TABLE IF EXISTS `custom_domain_service`;
CREATE TABLE `custom_domain_service` (
  `custom_domain_service_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'カスタムドメインキー',
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '提供サービス名',
  `domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ドメイン名',
  `status` int(11) DEFAULT '1' COMMENT 'ステータス',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`custom_domain_service_key`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='カスタムドメイン用';

-- ----------------------------
-- Records of custom_domain_service
-- ----------------------------
INSERT INTO `custom_domain_service` VALUES ('1', 'paperless', 'paperless.nice2meet.us', '1', '2011-08-09 15:47:54', '2011-08-09 15:47:54');
INSERT INTO `custom_domain_service` VALUES ('2', 'pgi', 'pgi.nice2meet.us', '1', '2011-08-09 15:47:54', '2011-08-09 15:47:54');
INSERT INTO `custom_domain_service` VALUES ('3', 'tact', 'schoolie-gaia.nice2meet.us', '1', '2011-08-09 15:47:54', '2011-08-09 15:47:54');
INSERT INTO `custom_domain_service` VALUES ('4', 'biz', 'biz.nice2meet.us', '1', '2014-03-13 00:00:00', '2014-03-13 00:00:00');

-- ----------------------------
-- Table structure for datacenter
-- ----------------------------
DROP TABLE IF EXISTS `datacenter`;
CREATE TABLE `datacenter` (
  `datacenter_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datacenter_name_ja` text NOT NULL,
  `datacenter_name_en` text NOT NULL,
  `datacenter_name_cn` text NOT NULL,
  `datacenter_name_tw` text NOT NULL,
  `datacenter_name_fr` text NOT NULL,
  `datacenter_country` varchar(32) NOT NULL DEFAULT 'jp',
  `free_use_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'フリーで利用する場合は1',
  `status` int(4) NOT NULL DEFAULT '1' COMMENT 'ステータス',
  `sort` int(11) NOT NULL DEFAULT '1' COMMENT '優先順位',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`datacenter_key`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of datacenter
-- ----------------------------
INSERT INTO `datacenter` VALUES ('2001', 'Global Link', 'Global Link', 'Global Link', 'Global Link', 'Global Link', 'globallink', '0', '1', '1', '2014-10-06 16:06:59', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for db_server
-- ----------------------------
DROP TABLE IF EXISTS `db_server`;
CREATE TABLE `db_server` (
  `server_key` int(11) NOT NULL AUTO_INCREMENT,
  `host_name` varchar(100) NOT NULL,
  `dsn` text NOT NULL,
  `server_status` tinyint(1) NOT NULL DEFAULT '1',
  `server_registtime` datetime NOT NULL,
  `server_updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`server_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_server
-- ----------------------------

-- ----------------------------
-- Table structure for eco_simulator_log
-- ----------------------------
DROP TABLE IF EXISTS `eco_simulator_log`;
CREATE TABLE `eco_simulator_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endpoint` text NOT NULL,
  `startpoint` text NOT NULL,
  `move` double NOT NULL,
  `time` bigint(20) NOT NULL,
  `fare` bigint(20) NOT NULL,
  `co2` double NOT NULL,
  `num_trips` int(11) NOT NULL,
  `num_expances` int(11) NOT NULL DEFAULT '0',
  `entrytime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eco_simulator_log
-- ----------------------------

-- ----------------------------
-- Table structure for faq
-- ----------------------------
DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `faq_key` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category` int(11) NOT NULL DEFAULT '0',
  `faq_subcategory` int(11) NOT NULL DEFAULT '0',
  `faq_order` int(11) NOT NULL DEFAULT '0',
  `faq_question` text NOT NULL,
  `faq_answer` text NOT NULL,
  `faq_status` int(11) NOT NULL DEFAULT '0',
  `faq_lang` varchar(255) NOT NULL DEFAULT '0',
  `faq_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1: すべてのサービス, 2: 通常, 3: メンバー課金モデル',
  PRIMARY KEY (`faq_key`)
) ENGINE=MyISAM AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of faq
-- ----------------------------
INSERT INTO `faq` VALUES ('1', '1', '1', '1', '1契約（１会議室）につき10地点間での会議が可能ということですが、10地点以上で行うことは可能でしょうか？', 'オーディエンス機能（閲覧のみの機能）を利用することにより最大50拠点までテレビ会議に参加して頂くことが可能となります。', '2', '0', '1');
INSERT INTO `faq` VALUES ('2', '2', '1', '1', '料金の全般のしつもん', '料金の全般のこたえ', '2', '0', '1');
INSERT INTO `faq` VALUES ('3', '3', '2', '1', '環境のミーティングのしつもん', '環境のミーティングの答え', '2', '0', '1');
INSERT INTO `faq` VALUES ('4', '4', '3', '1', 'トラブルシューティングのセミナーしつもん', 'トラブルシューティングのセミナーのこたえ', '2', '0', '1');
INSERT INTO `faq` VALUES ('5', '5', '1', '10', '相手の声がノイズが多くてよく聞こえない', '1)相手はヘッドセットを使用していますか？\r\n\r\n→使用していない。\r\nヘッドセットをご利用頂いてください。\r\n\r\n→使用している（2へ進む）\r\n\r\n\r\n2)相手のヘッドセットのマイクが、音声集音タイプですか？\r\n\r\n→はい。\r\n相手のコンピュータに何か問題がある可能性がございます。PCの再起動のお願いをしてください。\r\n\r\n→いいえ。\r\n現在の物より高性能なヘッドセットをご利用頂けますようお勧めいたします。', '1', '0', '1');
INSERT INTO `faq` VALUES ('6', '5', '3', '1', 'まっくのせみなーのしつもん', 'まっくのせみなーのしつもん', '2', '0', '1');
INSERT INTO `faq` VALUES ('15', '1', '2', '9', 'ホワイトボードには、どのような資料が貼り付けられますか？', 'BMP、JPEG、GIF、PNG、TIFF形式画像や、Word、Excel、PowerPoint、PDF　などの資料の貼り付けが可能です。\r\n※動画ファイル、PowerPointのアニメーション機能には対応しておりません。', '1', '0', '1');
INSERT INTO `faq` VALUES ('16', '1', '2', '1', 'IDの管理はユーザ毎で行うのでしょうか？', 'いいえ。ログインＩＤの発行は基本的に１契約＝１会議室あたり１つとなります。\r\nお使いいただくユーザ総数は100人でも1000人でも問題ありません。座席数が10人の会議室を、１室ご利用するイメージでお考えいただければと思います。', '1', '0', '1');
INSERT INTO `faq` VALUES ('21', '1', '1', '2', '申し込みの前に、無料で試用することはできますか？', '実際にご利用頂けるテストアカウントを貸し出させて頂きますので、ウェブサイトからお申し込みください。', '2', '0', '1');
INSERT INTO `faq` VALUES ('18', '1', '3', '1', 'seminar questin', 'seminar answer', '2', '0', '1');
INSERT INTO `faq` VALUES ('19', '5', '2', '6', 'aaa', 'aaa', '2', '0', '1');
INSERT INTO `faq` VALUES ('20', '4', '1', '13', '3G対応の携帯電話を呼び出してもつながらない', '「N2MY Mobile」は非通知で発信を行なっています。\r\n携帯電話側が非通知着信を拒否する設定になっている場合は、許可する設定に変更を行なってください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('22', '2', '2', '1', 'test', 'test', '2', '0', '1');
INSERT INTO `faq` VALUES ('23', '1', '1', '3', '申し込みの前に、無料で試用することはできますか？', '実際にご利用頂ける「無料トライアルアカウント」を貸し出させて頂きますので、ウェブサイトからお申し込みください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('24', '1', '2', '2', '1契約（１会議室）につき10拠点間での会議が可能ということですが、10拠点以上で行うことは可能でしょうか？', 'オーディエンス機能（閲覧のみの機能）を利用することにより最大50拠点までテレビ会議に参加して頂くことが可能となります。', '1', '0', '1');
INSERT INTO `faq` VALUES ('25', '1', '2', '3', 'オーディエンス機能とはなんでしょうか？', 'オーディエンス（傍聴者）とは、テレビ会議を傍聴することができる会議参加者です。オーディエンス参加者は、通常参加者が会議を行っている様子を閲覧することができます。オーディエンス参加者が、通常参加者となって、行われている会議に対して意見等を述べることもできます。', '1', '0', '1');
INSERT INTO `faq` VALUES ('26', '1', '2', '4', '社外の人間と会議をしたいのですが、ID・パスワードを教えたくありません。', 'はい、可能です。「招待メール機能」をご利用頂くことで、他の会議参加者にIDとパスワードを教えることなくテレビ会議を行うことが可能となります。\r\n※招待メールを受け取る為のアドレスが必要です。', '1', '0', '1');
INSERT INTO `faq` VALUES ('27', '1', '2', '5', 'IDを持っていないユーザも参加することはできますか？', 'はい、可能です。招待メール機能によって参加頂けます。\r\n※招待メールを受け取る為のアドレスが必要です。', '1', '0', '1');
INSERT INTO `faq` VALUES ('28', '1', '2', '6', '1契約で複数の会議を同時に開催することはできますか？', 'いいえ、出来ません。\r\n別途、追加で会議室の契約ができますので、お問い合わせフォームからお問い合わせいただくか、サポートセンターにお問い合わせください。\r\nサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('29', '1', '3', '2', '海外でも利用できますか？', 'はい、可能です。ウェブベースでのテレビ会議システムであるため、インターネットをご利用することが可能であれば、どなたでも参加して頂くことが可能です。', '1', '0', '1');
INSERT INTO `faq` VALUES ('30', '1', '2', '10', 'ホワイトボードに貼り付ける資料の容量制限はありますか？', '1ファイルにつき、20Mまでアップロード可能です。また、1ファイルにつき20ページまでアップロード可能です。20Mまたは、20ページを超えるファイルは分割してアップロードしてください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('31', '1', '2', '7', '自分のPCで動いているアプリケーションの画面を表示・共有できますか？', 'はい、可能です。\r\nただし、N2MY Sharingへのオプション契約が必要となります。', '1', '0', '1');
INSERT INTO `faq` VALUES ('32', '1', '2', '8', 'ホワイトボードの内容をコピー＆ペーストすることはできますか？', 'いいえ、こちらの機能には現在対応しておりません。 ただし、「議事録を見る」機能で会議終了後でもホワイトボードの内容を見ることができます。 \r\nまた、会議中に「ファイルキャビネット」機能でお互いファイルの送受信を行うことができます。', '1', '0', '1');
INSERT INTO `faq` VALUES ('33', '1', '1', '4', 'IDとパスワードをなくしてしまったのですが、どうすればいいでしょうか？', 'お手数ですが、サポートセンターにお問い合わせください。\r\nサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('34', '1', '2', '11', '同時に何人、会議に参加できますか？', '通常サービスでは最大10人まで同時に会議に参加して頂くことが可能です。\r\nまた、オプション機能であるオーディエンス機能をご利用頂くことで最大50人まで同時に会議に参加して頂くことが可能となります。', '1', '0', '1');
INSERT INTO `faq` VALUES ('35', '1', '2', '12', '他社テレビ会議サービスとの違いについて教えてください。', 'nice to meet youが他社サービスと大きく異なる点は以下の5点となっております。\r\n\r\n・インストール不要。\r\n専用ソフトをインストールする必要がなくインターネットが使用出来ればご利用可能です。\r\n\r\n・環境を選ばないマルチプラットフォーム。\r\nWindows、Macでも利用できます。\r\n\r\n・H.323対応のテレビ会議システムにも対応しています。\r\n\r\n・ 携帯対応。テレビ電話機能を持つ携帯電話での参加が可能です。\r\n\r\n・プロキシ、Firewall対応。企業内ＬＡＮの設定にも自動的に対応するので、難しい設定は不要です。', '1', '0', '1');
INSERT INTO `faq` VALUES ('36', '1', '2', '13', 'nice to meet youを利用するために必要な機器などは何ですか？', 'ブロードバンド環境、パソコン、カメラ、ヘッドセットがあればすぐにnice to meet youをご利用頂けます。カメラがなくても参加可能です。', '1', '0', '1');
INSERT INTO `faq` VALUES ('37', '1', '1', '5', 'Macで利用できますか？', 'はい、可能です。 ただし、一部ツール、N2MY Sharing N2MY CheckerはWindows版しかございません。', '1', '0', '1');
INSERT INTO `faq` VALUES ('38', '1', '1', '6', 'インターネットは利用できないが、イントラネット上で利用したい。', '弊社サーバをお買いあげいただき、イントラネット上で運用する方法があります。費用やスケジュールなどについては、サポートセンターまでお問い合わせください。 \r\nサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('39', '1', '2', '14', 'サーバが落ちた場合はどのような対応をして頂けるのでしょうか？', 'センターが分散されているため、万が一サーバがダウンした場合でも、サービスに影響を与えないように設計されております。', '1', '0', '1');
INSERT INTO `faq` VALUES ('40', '1', '1', '7', 'カメラを貸し出して頂くことは可能でしょうか？', '原則、貸し出しはしておりませんが、お問い合せ頂ければ貸し出しをさせていただくことも可能となっております。 \r\nサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('41', '1', '2', '15', '会議室パックを導入前に試用してみたいのですが、貸し出してもらうことは可能でしょうか？', '会議室パッケージのお貸し出しにつきましては、一度カスタマーサポートセンターまでお問い合せください。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('42', '1', '2', '16', '既に以前購入したテレビ会議システムがあるのですが、それを利用することはできますか？', 'H.323対応のテレビ会議システムであればご利用頂けます。\r\nただし、オプションサービスであるH.323対応サービスをご契約して頂く必要がございます。', '1', '0', '1');
INSERT INTO `faq` VALUES ('43', '1', '2', '17', '無料トライアルアカウントで携帯電話接続（N2MY Mobile）も利用できますか？', '原則、ご利用頂けませんが、携帯電話接続（N2MY Mobile）のお試しをご希望のお客さまは、携帯電話接続機能の付いた無料トライアルアカウントを発行させていただきます。 \r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('44', '2', '2', '2', '初期費用はいくらですか？', 'いずれのプランも一律 47,250円（税込）となります。こちらの料金にはセッティング費用等の諸費用も含まれております。', '1', '0', '1');
INSERT INTO `faq` VALUES ('45', '2', '2', '3', '料金体系の詳細をおしえてください。', '月額10,395（税込）より各種プランをご用意しております。\r\nなお、課金はルームチャージ制となっておりますのでご利用人数に関係なく一律料金となっております。\r\n1つのアカウントで、オンライン上の会議室を１室お貸しするようなイメージになります。詳細は料金プラン一覧をご覧ください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('46', '2', '2', '4', '料金プランを入会後に変更する際に、禁則事項など何か規定はありますか？\r\n（入会後１ヶ月しないうちにプランを変更するなど）', '特に制限はございません。プランの変更につきましては月の途中でも変更は可能ですが、適用は翌月１日からとなります。（その月は現行のプランでのご利用となります）前払いのお客様は翌々月の変更となります。', '1', '0', '1');
INSERT INTO `faq` VALUES ('47', '2', '2', '5', '解約後に、再開する場合は、初期費用はかかるのでしょうか？', 'はい、再度設定いたしますので、初期費用が発生いたします。', '1', '0', '1');
INSERT INTO `faq` VALUES ('48', '2', '2', '6', '料金は「1室あたり」とありますが、利用ユーザー数に制限はあるのでしょうか？', 'ご利用ユーザー数に制限はございません。課金は会議室単位になりますので、何名様でご利用されても料金は変わりません。', '1', '0', '1');
INSERT INTO `faq` VALUES ('49', '2', '2', '2', '会議室パックの概算費用を教えてください。', '弊社担当者からお見積もりをさせて頂きますので、サポートセンターまでお問い合せください。\r\nサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('50', '3', '1', '2', 'ファイアウォール（リバースプロキシやNAT）からの接続に対応していますか？', 'はい。ただしプロキシ経由の接続の場合には、プロキシの種類によって、映像・音声の遅延が発生する場合がございます。　対応策がございますので、別途お問い合わせください。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('51', '3', '3', '1', '推奨はブロードバンド環境（上り128kbps、下り384kbps以上）とありますが、これ以下の回線速度の場合、どのような不都合が発生するのでしょうか？', '音声遅延や、映像遅延などが発生いたします。', '1', '0', '1');
INSERT INTO `faq` VALUES ('52', '3', '1', '1', 'PHS移動通信（AirH″などのモバイルカード）からの利用は可能ですか？', '弊社では、動作の保証をしておりませんが、利用する事は可能です。 \r\n「低速回線モード」、「音声優先モード」、「利用帯域設定」など、回線速度の低い環境に合わせた設定も可能です。\r\nただし、PHS環境（AirH\"など）の場合には、128kbps接続でもパケット処理などにより、実質的には64kbps以下の速度しか出ないケースが多い為、実質的な利用は困難なケースがございます。', '1', '0', '1');
INSERT INTO `faq` VALUES ('53', '1', '1', '8', 'セキュリティは万全なのでしょうか？', 'はい。映像と音声は非公開の独自プロトコルで通信しており、テキストチャットやファイル共有などドキュメントの部分は標準でSSLで通信しておりますので、限りなく万全な設計になっております。また、オプションサービスであるSSL対応サービスをご利用頂くことで、全ての通信をSSL化することも可能です。', '1', '0', '1');
INSERT INTO `faq` VALUES ('54', '3', '1', '2', '映像と音声のコーデックは何を使用していますか？', 'ビデオのエンコード・デコードは、Flashの機能により行われております。\r\nその機能で使用されているコーデックはSorenson Media社のSorenson Sparkコーデックです。', '1', '0', '1');
INSERT INTO `faq` VALUES ('55', '3', '1', '3', '使用しているプロトコルは何でしょうか？', 'Flash Media Serverで使用される、Adobe社独自のプロトコルRTMPを使用しております。\r\n※RTMPはFlash Media Serverで使用される、Adobe社独自のプロトコルです。\r\n詳細は、Adobe社のウェブページ（http://www.adobe.com/jp/products/flashmediaserver/）をご覧ください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('56', '3', '1', '4', '映像・音声・データのやりとりや、ログインＩＤなどは、どのようなセキュリティ対応をされているのか教えてください。', 'ビデオストリーム以外の全ての通信は、SSL通信を利用しております。\r\nビデオストリームは独自の通信を行っており、プロトコル仕様は公開されていないため現状でも安全ですが、オプション機能により、ビデオストリームのSSＬ化も可能です。', '1', '0', '1');
INSERT INTO `faq` VALUES ('57', '3', '1', '5', 'カメラは何が使えますか？推奨カメラなどはありますか？', 'USBカメラを利用することが可能です。\r\n詳細は、動作確認済みカメラをご覧ください。\r\nhttp://www.nice2meet.us/ja/tools/', '1', '0', '1');
INSERT INTO `faq` VALUES ('58', '1', '2', '19', '10拠点で利用する場合の必要帯域は？', '上り256Kbps、下り512Kbpsとなります。', '1', '0', '1');
INSERT INTO `faq` VALUES ('59', '1', '2', '20', 'フレームレートはいくつですか？', '最大15fpsとなっております。\r\n会議参加人数に合わせて、自動的に最適なフレームレートになります。', '1', '0', '1');
INSERT INTO `faq` VALUES ('60', '1', '1', '20', '最低利用期間など、利用期間に関する制限はありますか？', 'いいえ、制限は一切ございません。 \r\n契約期間の制限はございませんので、月単位のご利用が可能です。', '1', '0', '1');
INSERT INTO `faq` VALUES ('61', '1', '1', '21', '海外でも利用できますか？', 'はい、可能です。ウェブベースでのテレビ会議システムであるため、インターネットをご利用することが可能であれば、どなたでも参加して頂くことが可能です。', '1', '0', '1');
INSERT INTO `faq` VALUES ('62', '4', '1', '1', '回線速度を測定中のまま進まない', '回線速度が非常に遅い、あるいは回線を確保出来ていない可能性がございます。\r\n再度、入室していただきますと回線を再取得いたしますので、問題を解決する場合がございます。\r\n上記の方法で解決しない場合は、当社の推奨環境（回線、PCスペック）を満たしていない場合がございますので、弊社推奨環境をご確認ください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('63', '4', '1', '2', '回線速度を測定する画面でエラーが表示される（プロキシ）', '回線速度が非常に遅い、あるいは回線を確保出来ていない可能性がございます。\r\n再度、入室していただきますと回線を再取得いたしますので、問題を解決する場合がございます。\r\n上記の方法で解決しない場合は、当社の推奨環境（回線、PCスペック）を満たしていない場合がございますので、弊社推奨環境をご確認ください。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('64', '4', '1', '3', '毎回、会議中に「切断されました」となってしまう', '当システムでは、会議サーバに対して一定時間毎に「正常に会議が行われている」ことを通信しております。そのため、遅延が著しく発生している場合、サーバ側が接続していない状況と誤認し切断する場合がございます。会議では遅延を引き起こさなくても根本的な原因は遅延と同じになります。\r\n※プロキシが存在する場合や回線環境、PCスペックが当社製品の動作環境を下回っている可能性があります。', '1', '0', '1');
INSERT INTO `faq` VALUES ('65', '4', '1', '14', '映像の縦横比がおかしい', 'ご使用中のビデオデバイスによっては、映像取得サイズを変更した際に映像の縦横比が変更されたり、上下に黒い帯が入る可能性があります。デバイスの性能や設定等をご確認ください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('66', '4', '1', '4', '遅延がひどい', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)回線は何をお使いですか？\r\n\r\n→ISDN　or　ダイヤルアップ\r\n推奨の回線速度が維持できません。快適で安価なBフレッツをお勧めいたします。\r\n\r\n→ADSL／Bフレッツ／その他、光やケーブルテレビなど　（3へ進む）\r\n\r\n\r\n3)プロキシ経由の接続ですか？\r\n\r\n→プロキシ経由です。\r\nプロキシ経由の接続の場合、プロキシサーバの特性上、遅延が発生する可能性があります。ただし、1935ポートを外向きに開けていただければプロキシサーバを介さずに通信ができますので、遅延の回避が可能となります。詳細はネットワーク管理者にその旨ご相談ください。\r\n\r\n→プロキシ経由ではない（4へ進む）\r\n\r\n\r\n4)お使いのパソコンは、PentiumIIIの533MHz以上ですか？\r\n\r\n→いいえ。\r\n推奨のスペックを満たしておりません。映像などを処理するに当たり、ある程度のスペックが要求されます。大変恐縮ではございますが、推奨環境以上のパソコンでご利用ください。\r\n\r\n→はい。（5へ進む）\r\n\r\n\r\n5)何かほかのアプリケーションが起動していませんか？\r\n\r\n→はい。\r\nシステムのリソース不足の可能性がございますので、他のアプリケーションを終了してください。\r\n\r\n→いいえ。\r\nご利用のパソコン自体に搭載されておりますビデオチップが遅延の原因かと思われます。少し古いタイプやノートパソコンなどでは映像を見るに適したスペックになっていないことがあります。お手数ではございますが再度、別のパソコンでご利用ください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('67', '4', '1', '5', '自分の映像が表示されない', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)カメラが接続されていますか？\r\n\r\n→接続されていない。\r\nUSBカメラもしくは、IEEE1394のカメラを接続してください。\r\n\r\n→接続されている（3へ進む）\r\n\r\n\r\n3)カメラはOSに認識されていますか？\r\n\r\n→認識されていない。\r\nカメラのマニュアルに従い、ドライバをインストールするなどして、カメラを認識させてください。\r\n\r\n→認識されている。（4へ進む）\r\n\r\n\r\n4)「カメラ切り替え」ボタンを何度かクリックして映像が表示されますか？\r\n\r\n→表示される。\r\nカメラが複数台つながっている場合や、OSの状態によってはカメラ切り替えボタンを押さないとカメラが利用できない場合がございます。\r\n\r\n→表示されない。（5へ進む）\r\n\r\n\r\n5)「会議室に入室」ボタンを押した後に表示される小さなウィンドウで、「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n会議ウィンドウを閉じて、再度、会議室に入室してください。\r\nその際に、必ず「許可」をクリックするようにしてください。\r\n\r\n→クリックした。（6へ進む）\r\n\r\n\r\n6)ほかにカメラを使うアプリケーションは起動していませんか？\r\n\r\n→起動している。\r\nカメラを使う他のアプリケーションを終了し、会議ウィンドウを閉じて、再度、会議室に入室してください。\r\n\r\n→起動していない。\r\n対応していないカメラである可能性か、カメラが壊れている可能性がございます。また、パソコンの再起動を一度お試しください。カメラの不具合につきましては各メーカーへお問い合わせください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('68', '4', '1', '6', '相手の映像が表示されない', '1)相手の方にN2MY Checker2でご利用環境の診断を行って頂いてください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)相手がログインしていますか？\r\n\r\n→ログインしていない。\r\n相手の方へ会議室への入室のお願いをしてください。\r\n\r\n→ログインしている。（3へ進む）\r\n\r\n\r\n3)相手は相手自身の映像を相手のパソコン上で見えていますか？\r\n\r\n→見えていない。\r\n相手の映像が相手のPC上で表示されるように設定してください。\r\n（『自分の映像が表示されない』を参照）\r\n\r\n→見えている。（4へ進む）\r\n\r\n\r\n4)相手の名前は表示されていますか？\r\n\r\n→表示されている。\r\n何らかのシステムトラブルの可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示されていない。（5へ進む）\r\n\r\n\r\n5)チャットウィンドウに文字を打ち込んで送信を押した後、チャットウィンドウにその内容が表示されますか？\r\n\r\n→表示されない。\r\nお客様が正常に接続できていない可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示される。\r\n相手が正常に接続できていない可能性がございます。\r\n相手に会議室への再入室のお願いをしてください。（『自分の映像が表示されない』を参照）', '1', '0', '1');
INSERT INTO `faq` VALUES ('69', '4', '1', '7', '自分の音声が相手に届かない', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)パソコンにマイクは接続されていますか？\r\n\r\n→接続されていない。\r\nパソコンにマイクを接続し、会議室へ再入室してください。\r\n\r\n→接続されている。（3へ進む）\r\n\r\n\r\n3)会議開始後に表示される小さなウィンドウで「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n再度会議室に入室し直してください。その際に必ず「許可」をクリックしてください。\r\n\r\n→クリックした。（4へ進む）\r\n\r\n\r\n4)パソコンのマイクがミュートの設定になっていませんか？\r\n\r\n→なっている。\r\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。\r\n\r\n→なっていない。（5へ進む）\r\n\r\n\r\n5)[コントロールパネル]→[サウンドとオーディオデバイス]→[オーディオ]→[録音]→[音量]→[録音コントロール]の画面で「マイク」にチェックがはいっていますか？\r\n\r\n→入っていない。\r\n「マイク」にチェックをいれてください。\r\n\r\n→入っている。（6へ進む）\r\n\r\n\r\n6)マイク切り替えボタンをクリックして、相手に自分の音声が届くようになりますか？\r\n\r\n→届く。\r\nマイクが複数台つながっている場合や、OSの状態によってはマイク切り替えボタンを押さないとマイクが利用できない場合があります。\r\n\r\n→届かない。\r\nマイクが故障しているか、お使いのコンピュータでは使えないマイクの可能性があります。また、パソコンの再起動を一度お試しください。マイクの不具合につきましては各メーカーへお問い合わせください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('70', '4', '1', '8', '相手の音声が自分に届かない（音が出ない）', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)スピーカもしくはヘッドフォンはつながっていますか？\r\n\r\n→つながっていない。\r\nスピーカもしくはヘッドフォンを接続し、会議室へ再入室してください。\r\n\r\n→つながっている（3へ進む）\r\n\r\n\r\n3)パソコンのスピーカもしくはヘッドフォンがミュートになっていませんか？\r\n\r\n→ミュートになっている。\r\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。\r\n\r\n→ミュートになっていない（4へ進む）\r\n\r\n\r\n4)パソコン本体のボリュームがOFFになっていませんか？\r\n\r\n→なっている。\r\nボリュームをあげてください。（設定ではなく、パソコン本体のボリューム）\r\n\r\n→なっていない。\r\n相手の設定が正常でない可能性があります。\r\n相手側にFAQの『自分の音声が相手に届かない。』を確認してください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('71', '4', '1', '9', '音が小さいのですが、どうすればいいでしょうか？', 'ミュートもしくはボリュームが小さくなっていませんか？\r\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('72', '4', '1', '10', '自分の声がエコーしてしまいます。直し方はありますか？', '相手側がスピーカーを使っていませんか？\r\n\r\n→使用している。\r\nあなたの声を相手側のマイクが拾っています。ヘッドセットをご利用頂ければエコーは無くなります。相手側でスピーカーの使用がやむを得ない場合は、エコーキャンセラー付きのマイクが必要になります。\r\n参考：エコーキャンセラー付きマイク　ClearOne社　ACCUMICII\r\n\r\n→使用していない。\r\n相手のヘッドセットが安価な物だと、多少自分の声が戻ってくる可能性がございます。', '1', '0', '1');
INSERT INTO `faq` VALUES ('73', '4', '1', '11', '相手の声がノイズが多くてよく聞こえない', '1)相手はヘッドセットを使用していますか？\r\n\r\n→使用していない。\r\nヘッドセットをご利用頂いてください。\r\n\r\n→使用している（2へ進む）\r\n\r\n\r\n2)相手のヘッドセットのマイクが、音声集音タイプですか？\r\n\r\n→はい。\r\n相手のコンピュータに何か問題がある可能性がございます。PCの再起動のお願いをしてください。\r\n\r\n→いいえ。\r\n現在の物より高性能なヘッドセットをご利用頂けますようお勧めいたします。', '1', '0', '1');
INSERT INTO `faq` VALUES ('74', '4', '1', '12', 'クリックしてもページが表示されない（ポップアップ表示がブロックされてしまう）', '「Windows XP Service Pack 2」をご利用されている方は、ポップアップ表示がブロックされてしまいます。\r\nブラウザのツールバーより「ツール」を選択、メニューから「ポップアップブロック」→「ポップアップブロックの設定」を選択し、クリックします。\r\n「許可するWebサイトのアドレス」に『www.nice2meet.us』と入力し、「追加」ボタンをクリックします。\r\n「閉じる」ボタンをクリックし、設定ウインドウを閉じます。\r\n\r\n以上で設定は完了です。', '1', '0', '1');
INSERT INTO `faq` VALUES ('75', '2', '3', '1', '初期費用はいくらですか？', 'ご利用のプランによって異なりますので、カスタマーサポートセンターまでお問合せ下さい。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('76', '2', '3', '2', '料金体系の詳細をおしえてください。', '月額 29,900円（税別）から各種プランをご用意しております。\r\n1つのアカウントで、オンラインのセミナールームをを１つお貸しするようなイメージになるとお考えいただければと思います。\r\n詳細は料金プラン一覧をご覧ください。', '2', '0', '1');
INSERT INTO `faq` VALUES ('77', '2', '3', '3', '解約後に、再開する場合は、初期費用はかかるのでしょうか？', 'はい、再度設定しますので、初期費用は発生します。', '1', '0', '1');
INSERT INTO `faq` VALUES ('78', '1', '3', '3', '1契約で複数のセミナーを同時に開催することはできますか？', 'いいえ、出来ません。別途、追加でセミナールームの契約ができますので、お問い合わせフォームからお問い合わせいただくか、カスタマーサポートセンターにお問い合わせください。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('79', '5', '1', '1', '推奨のマイクなどありますか？', 'USBタイプの利用を推奨いたします。\r\nピンジャックタイプですとノイズが発生しやすくなります。\r\niSightや本体マイクでも会議可能ですが、ハウリングやエコーが発生しやすくなります。', '1', '0', '1');
INSERT INTO `faq` VALUES ('80', '5', '1', '2', '会議中に他の作業をしていると、会議の処理が遅いように感じるのですが？', '最前面での利用を推奨いたします。\r\nOSやブラウザの設定によっては、会議の画面がバックグランドに回った際の処理が遅くなるため、常に最前面で利用してください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('81', '5', '1', '3', '遅延がひどい', '1)回線は何をお使いですか？\r\n\r\n→ISDN　or　ダイヤルアップ\r\n推奨の回線速度が維持できません。快適で安価なBフレッツをお勧めいたします。\r\n\r\n→ADSL／Bフレッツ／その他、光やケーブルテレビなど（2へ進む）\r\n\r\n\r\n2)プロキシ経由の接続ですか？\r\n\r\n→プロキシ経由です。\r\nプロキシ経由の接続の場合、プロキシサーバの特性上、遅延が発生する可能性があります。ただし、1935ポートを外向きに開けていただければプロキシサーバを介さずに通信ができますので、遅延の回避が可能となります。詳細はネットワーク管理者にその旨ご相談ください。\r\n\r\n→プロキシ経由ではない（3へ進む）\r\n\r\n\r\n3)お使いのパソコンは推奨環境を満たしていますか？\r\n\r\n→いいえ。\r\n映像などを処理するに当たり、ある程度のスペックが要求されます。大変恐縮ではございますが、推奨環境以上のパソコンでご利用ください。\r\n\r\n→はい。（4へ進む）\r\n\r\n\r\n4)何かほかのアプリケーションが起動していませんか？\r\n\r\n→はい。\r\nシステムのリソース不足の可能性がございますので、他のアプリケーションを終了してください。\r\n\r\n→いいえ。\r\nご利用のパソコン自体に搭載されておりますビデオチップが遅延の原因かと思われます。少し古いタイプやノートパソコンなどでは映像を見るに適したスペックになっていないことがあります。お手数ではございますが再度、別のパソコンでご利用ください。(5へ進む)\r\n\r\n\r\n5)ウィンドウが最前面になっていない。\r\n\r\nMacは最前面のウィンドウの処理を優先にする為、ウィンドウがバックグラウンドに回ると極端に処理が遅くなる場合があります。', '1', '0', '1');
INSERT INTO `faq` VALUES ('82', '5', '1', '4', '自分の映像が表示されない', '1)カメラが接続されていますか？\r\n\r\n→接続されていない。\r\nUSBカメラもしくは、IEEE1394のカメラを接続してください。\r\n\r\n→接続されている（2へ進む）\r\n\r\n\r\n2)カメラはOSに認識されていますか？\r\n\r\n→認識されていない。\r\nカメラのマニュアルに従い、ドライバをインストールするなどして、カメラを認識させてください。\r\n\r\n→認識されている。（3へ進む）\r\n\r\n\r\n3)「カメラ切り替え」ボタンを何度かクリックして映像が表示されますか？　\r\n\r\n→表示される。\r\nカメラが複数台つながっている場合や、OSの状態によってはカメラ切り替えボタンを押さないとカメラが利用できない場合がございます。\r\n※Macでは標準でカメラドライバを持っている為、USBカメラ利用の場合は必ず切り替えを行う必要があります。\r\n\r\n→表示されない。（4へ進む）\r\n\r\n\r\n4)「会議室に入室」ボタンを押した後に表示される小さなウィンドウで、「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n会議ウィンドウを閉じて、再度、会議室に入室してください。\r\nその際に、必ず「許可」をクリックするようにしてください。\r\n\r\n→クリックした。（5へ進む）\r\n\r\n\r\n5)ほかにカメラを使うアプリケーションは起動していませんか？\r\n\r\n→起動している。\r\nカメラを使う他のアプリケーションを終了し、会議ウィンドウを閉じて、再度、会議室に入室してください。\r\n\r\n→起動していない。\r\n対応していないカメラである可能性か、カメラが壊れている可能性がございます。また、パソコンの再起動を一度お試しください。カメラの不具合につきましては各メーカーへお問い合わせください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('83', '5', '1', '5', '相手の映像が表示されない', '1)相手がログインしていますか？\r\n\r\n→ログインしていない。\r\n相手の方へ会議室への入室のお願いをしてください。\r\n\r\n→ログインしている。（2へ進む）\r\n\r\n\r\n2)相手は相手自身の映像を相手のパソコン上で見えていますか？\r\n\r\n→見えていない。\r\n相手の映像が相手のPC上で表示されるように設定してください。\r\n（『自分の映像が表示されない』を参照）\r\n\r\n→見えている。（3へ進む）\r\n\r\n\r\n3)相手の名前は表示されていますか？\r\n\r\n→表示されている。\r\n何らかのシステムトラブルの可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示されていない。（4へ進む）\r\n\r\n\r\n4)チャットウィンドウに文字を打ち込んで送信を押した後、チャットウィンドウにその内容が表示されますか？\r\n\r\n→表示されない。\r\nお客様が正常に接続できていない可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示される。\r\n相手が正常に接続できていない可能性がございます。\r\n相手に会議室への再入室のお願いをしてください。（『自分の映像が表示されない』を参照）', '1', '0', '1');
INSERT INTO `faq` VALUES ('84', '5', '1', '6', '自分の音声が相手に届かない', '1)パソコンにマイクは接続されていますか？\r\n\r\n→接続されていない。\r\nパソコンにマイクを接続し、会議室へ再入室してください。\r\n\r\n→接続されている。（2へ進む）\r\n\r\n\r\n2)会議開始後に表示される小さなウィンドウで「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n再度、会議室に入室し直してください。その際に必ず「許可」をクリックしてください。\r\n\r\n→クリックした。（3へ進む）\r\n\r\n\r\n3)パソコンのマイクがミュートの設定になっていませんか？\r\n\r\n→なっている。\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[入力]→[主音量]→[消音]にチェックを外してください。\r\n\r\n→なっていない。（4へ進む）\r\n\r\n\r\n4)マイク切り替えボタンを何度かクリックして、相手に自分の音声が届くようになりますか？\r\n\r\n→届く。\r\nMacでは、マイク切り替えボタンを押さないとマイクが利用できない場合があります。\r\n\r\n→届かない。\r\nマイクが故障しているか、お使いのコンピュータでは使えない可能性があります。また、パソコンの再起動を一度お試しください。マイクの不具合につきましては各メーカーへお問い合わせください。\r\nMacの音声入力はほとんどの場合、音声入力端子が使えない場合があります。当社ではUSBアダプタを必ずご使用になる事を推奨しております。', '1', '0', '1');
INSERT INTO `faq` VALUES ('85', '5', '1', '7', '相手の音声が自分に届かない（音が出ない）', '1)スピーカもしくはヘッドフォンはつながっていますか？\r\n\r\n→つながっていない。\r\nスピーカもしくはヘッドフォンを接続し、会議室へ再入室してください。\r\n\r\n→つながっている（2へ進む）\r\n\r\n\r\n2)パソコンのスピーカもしくはヘッドフォンがミュートになっていませんか？\r\n\r\n→ミュートになっている。\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[出力]→[主音量]→[消音]のチェックを外して、ボリュームを上げてください。\r\n\r\n→ミュートになっていない（3へ進む）\r\n\r\n\r\n3)パソコン本体のボリュームがOFFになっていませんか？\r\n\r\n→なっている。\r\nボリュームをあげてください。（設定ではなく、パソコン本体のボリューム）\r\n\r\n→なっていない。\r\n相手の設定が正常でない可能性があります。\r\n相手側にFAQの『自分の音声が相手に届かない。』の確認をお願いをしてください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('86', '5', '1', '8', '音が小さいのですが、どうすればいいでしょうか？', 'ミュートもしくはボリュームが小さくなっていませんか？\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[入力]→[入力音量]→[消音]のチェックを外して、ボリュームを上げてください。', '1', '0', '1');
INSERT INTO `faq` VALUES ('87', '5', '1', '9', '自分の声がエコーしてしまいます。直し方はありますか？', '相手側がスピーカーを使っていませんか？\r\n\r\n→使用している。\r\nあなたの声を相手側のマイクが拾っています。ヘッドセットをご利用頂ければエコーは無くなります。相手側でスピーカーの使用がやむを得ない場合は、エコーキャンセラー付きのマイクが必要になります。\r\n\r\n→使用していない。\r\n相手のヘッドセットが安価な物だと、多少自分の声が戻ってくる可能性がございます。', '1', '0', '1');
INSERT INTO `faq` VALUES ('148', '1', '2', '18', '無料トライアルアカウントでH.323TV会議システムへの接続（N2MY H.323）も利用できますか？', '原則、ご利用頂けませんが、お問い合せ頂ければH.323TV会議システムへの接続（N2MY H.323）をご利用頂ける無料トライアルアカウントを発行することも可能となっております。 カスタマーサポートセンターまでお問合せ下さい。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq` VALUES ('88', '1', '1', '100', 'Is it possible to connect more than 10 locations?', 'Yes, up to 49 locations can be connected simultaneously by using the optional “Audience” feature. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('89', '1', '1', '101', 'Is it possible to try the service before purchasing it?', 'Yes, you can sign up online for a 2 week trial account. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('90', '1', '1', '102', 'What should I do if I forget my ID and password?', 'Contact us by phone (0570-00-2192) or e-mail.  We can send you the information once we confirm your identity. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('91', '1', '1', '102', 'Can I use nice to meet you on a Mac?', 'Yes, but you can not use the ‘Sharing’ and ‘Checker’ features on a Mac. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('92', '1', '1', '103', 'Can I setup nice to meet you within an intranet?', 'Yes, you can purchase a server to run nice to meet you on your intranet for added security.  Please contact our sales team for more details. (0570-00-2192)', '1', '1', '1');
INSERT INTO `faq` VALUES ('93', '1', '1', '104', 'Can I rent a web camera? ', 'Normally we do not offer rental of web cameras, but we can make exceptions.  Please ask our sales team for more information.  (0570-00-2192) ', '1', '1', '1');
INSERT INTO `faq` VALUES ('94', '1', '1', '105', 'What kind of security is used for login-ID, video, voice and data communication?', 'SSL is used for all the communication except video and voice.  Video and voice are secured by our original protocol, which is proprietary.  N2MY SSL is an optional feature makes it possible to make all communication more secure. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('95', '1', '1', '106', 'How can I start my meeting without disclosing my ID & password?', 'By using the ‘INVITATION’ feature you can e-mail an invitation with an automatically generated hyperlink to all your guests.  This will allow your guests to have ‘one-click access’ into the room.  Not only can you secure your ID & password, but it’s hassle free for your guests.', '1', '1', '1');
INSERT INTO `faq` VALUES ('96', '1', '1', '107', 'Is it possible to participate in a meeting without an ID? ', 'Yes, it is possible to participate in a meeting using a hyperlink sent to you by ‘e-mail invitation’.   *An e-mail address is required to receive the invitation. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('97', '1', '2', '100', 'How many IDs and passwords do I get per contract?', 'One ID and password is issued per contract.  You may share the ID and password with as many users as you’d like. It may be easier for you to think of it this way; you are renting a ‘virtual room’ and you have the right to allow anyone to use it. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('98', '1', '2', '101', 'How can I start my meeting without disclosing my ID & password?', 'By using the ‘INVITATION’ feature you can e-mail an invitation with an automatically generated hyperlink to all your guests.  This will allow your guests to have ‘one-click access’ into the room.  Not only can you secure your ID & password, but it’s hassle free for your guests.', '1', '1', '1');
INSERT INTO `faq` VALUES ('99', '1', '2', '102', 'Is it possible to connect more than 10 locations?', 'Yes, up to 49 locations can be connected simultaneously by using the optional “Audience” feature.', '1', '1', '1');
INSERT INTO `faq` VALUES ('100', '1', '2', '103', 'Is it possible to participate in a meeting without an ID? ', 'Yes, it is possible to participate in a meeting using a hyperlink sent to you by ‘e-mail invitation’.   *An e-mail address is required to receive the invitation. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('101', '1', '2', '104', 'Can I have several meetings simultaneously with one account ID & password?', 'No, you can only have one meeting at a time per account ID & password.  Basically, the concept is that you are renting one ‘room’, therefore, you can only have one meeting at a time.  Please, contact the support center if you would like to create another ‘room’ or account ID & password. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('102', '1', '2', '105', 'Is there a minimum contract term?', 'No, there is no minimum contract term.  It is on a month-to-month basis.  ', '1', '1', '1');
INSERT INTO `faq` VALUES ('103', '1', '2', '106', 'Can I share files and applications running on my PC?', 'Yes, you can activate the desktop sharing feature and show the documents and applications running on your desktop.  This feature will also enable people to take control of your PC. N2MY Sharing is an optional feature. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('104', '1', '2', '107', 'Can I copy the contents of the whiteboard to a clipboard or vice versa?', 'No. There is only a File/picture upload on the whiteboard feature.\r\nYou can display files such as Word, Excel, PowerPoint, PDF, BMP, PNG, JPEG, and GIF on the whiteboard. \r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('105', '1', '2', '108', 'How many people can participate in a meeting at the same time?  ', 'You can have up to 10 people attend a meeting with video and voice.  If you use the ‘Audience’ feature you can have up to 49 locations connected at the same time. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('106', '1', '2', '109', 'Tell me the advantages of using V-cube meeting over other services.', 'There are 3 big differences: \r\n1. No need for installation\r\n2. Cross platform, you can use it on a Macintosh as well as on Windows PC.  It can also connect to H.323 compliant terminals.\r\n3. Bypass proxy and firewall.  No need for any changes in settings for company LAN.  \r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('107', '1', '1', '110', 'Do I need any special equipment for V-cube Meeting?', 'All you need is broadband internet connection, computer, web camera and a headset. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('108', '1', '2', '112', 'What happens if a server goes down, how do you respond to it?', 'In the event that a server goes down, we have backup servers in multiple locations that will keep our service running 24/7.  ', '1', '1', '1');
INSERT INTO `faq` VALUES ('109', '1', '2', '113', 'Can I try the conference room package before purchasing it?', 'Normally we do not offer a free trial for our conference room package, but we have made exceptions in the past with money back guarantee deposit.  Please ask our sales team for more information. (0570-00-2192) ', '1', '1', '1');
INSERT INTO `faq` VALUES ('110', '1', '2', '114', 'Can I connect V-cube Meeting to the web conferencing system I currently have?', 'If your current system uses an H.323 compliant terminal, the answer is yes.  But you must sign up for the H.323 optional service. Please contact our support team for more details.  ', '1', '1', '1');
INSERT INTO `faq` VALUES ('111', '1', '2', '115', 'Can I try the N2MY mobile service with a free trial account? ', 'Normally we do not offer a free trial for N2MY mobile, but we can make exceptions.  Please ask our sales team for more information.  (0570-00-2192) ', '1', '1', '1');
INSERT INTO `faq` VALUES ('112', '1', '2', '116', 'How much bandwidth do I need for a conference between 10 locations?', 'You need at least 256kbps upstream and 512kbps downstream.', '1', '1', '1');
INSERT INTO `faq` VALUES ('113', '1', '2', '117', 'How many frame rates does nice to meet you get?', 'It automatically changes depending on the number of participants connected. But the maximum frame rate you can get is 15fps.', '1', '1', '1');
INSERT INTO `faq` VALUES ('114', '2', '2', '100', 'Is there an initial cost?', 'Yes, there is a one-time activation fee of 45,000 Yen for any plan. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('115', '2', '2', '101', 'Tell me about conferencing package.', 'Please ask our support team to get an estimate.  (0570-00-2192) ', '1', '1', '1');
INSERT INTO `faq` VALUES ('116', '2', '2', '101', 'Please tell me the details about your pricing. ', 'We have several plans starting from 9,900 Yen (tax included) per month.  It is a flat fee no matter how many participants there are.  It is like renting a virtual conference room online with an account and you are free to use it as you like. See pricing details for more info.  ', '1', '1', '1');
INSERT INTO `faq` VALUES ('117', '2', '2', '102', 'Can I change the service plan?  Can I change the plan within one month?', 'You can change the service plan in the middle of the month.  However, the change will be effective until the first day of the following month.', '1', '1', '1');
INSERT INTO `faq` VALUES ('118', '2', '2', '104', 'Do I have to pay the initial cost again to re-start the service once I close an account?', 'Yes, we charge the initial setup cost of 45,000 Yen. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('119', '2', '2', '105', 'Is there a limitation on the number of users in a room?', 'This will depend on the type of price plan you purchased.  Please contact our support team to find out what the limitations are on each plan.', '1', '1', '1');
INSERT INTO `faq` VALUES ('120', '3', '1', '100', 'Can I use nice to meet you with PHS mobile? (AirH or P-in)', 'It is not guaranteed by company, but you can use it with PHS mobile.  It would be difficult to use it because of the network speed is usually lower than 64kbps.', '1', '1', '1');
INSERT INTO `faq` VALUES ('121', '3', '1', '101', 'Can I use V-cube’s service even if my PC is inside a firewall (reverse proxy or NAT)?', 'Yes, our service was designed to bypass firewalls. However, if you are connecting via a proxy server you may experience slower connection speeds.  Such delay can be avoided by opening port 1935 outward.  For more details please contact our support team.', '1', '1', '1');
INSERT INTO `faq` VALUES ('122', '3', '1', '102', 'What kind of video/voice codec is used? ', 'Flash is used for video encode/decode.  The codec used in the feature is the Sorenson Spark codec made by Sorenson Media.', '1', '1', '1');
INSERT INTO `faq` VALUES ('123', '3', '1', '103', 'What kind of protocols and ports are used?', 'RTMP is Macromedia\'s original protocol used in Flash Communication Server.  For more details, please refer to the Macromedia website.', '1', '1', '1');
INSERT INTO `faq` VALUES ('124', '3', '1', '104', 'What kind of security is used for login-ID, video, voice and data communication? ', 'SSL is used for all the communication except video and voice.  Video and voice are secured by our original protocol, which is proprietary.  N2MY SSL is an optional feature makes it possible to make all communication more secure. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('125', '3', '1', '105', 'Which web cameras do you recommend? ', 'Our system is easiest to use with a standard USB web cam. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('126', '4', '1', '100', 'I can not go into the meeting room because the program stops where it measures the connection speed.', 'This may occur because the internet connection speed is too slow or the performance of the PC is slow.  Please try logging in again.  If it occurs again, the PC may not satisfy minimum system requirements (internet connection or specification).  Please refer to our requirements for more details. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('127', '4', '1', '101', 'I see an error message when the PC connection speed is measured. （Proxy） ', 'You might not be able to use this service in your network environment.  There are a few possible reasons:\r\n1. Your network’s proxy server is old.\r\n2. The security setting of the network is too high.\r\nTo resolve this please ask our support team.  We can suggest the best way to accommodate your network environment.\r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('128', '4', '1', '102', 'I am disconnected during the meeting each time I use it. ', 'The system regularly reports to the server that there is a meeting taking place.  If the server does not get the report because of the delay, it sometimes disconnects.  The basic reason is delay. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('129', '4', '1', '103', 'There is a huge delay. ', '1) Check the environment of your PC using N2MY Checker.  \r\n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker.\r\n\r\n2) How is your PC Connected?\r\n→ISDN　or　Dial up\r\nThe speed of your PC connection is less than what we recommend.\r\n→ADSL／DSL／T-1（Go to 3）\r\n3)Is your connection through a proxy?\r\n→Yes.  It is through a proxy.\r\nIf you are connecting via a proxy server you may experience slower connection speeds. Such a delay can be avoided by opening port 1935 outward. Please talk to your IT department.\r\n→No.  It is not through a proxy. （Go to 4）\r\n4)Does your PC have a processor newer than a Pentium III 533 MHz?\r\n→No.\r\nYour PC is not fast enough.  Your must have a PC that meets or exceeds our minimum system requirements.\r\n→Yes.（Go to 5）\r\n5) Are other applications open?\r\n→Yes.\r\nClose other applications, the system may be out of resources. \r\n→No. \r\nIt seems that the video tip is the reason for the delay.  Your PC cannot see video images because it is too old or it is a notebook.  Please try using another PC.  \r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('130', '4', '1', '104', 'My picture cannot be seen. ', '1) Check the environment of your PC using N2MY Checker.  \r\n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker. \r\n2) Is there a web camera connected?\r\n→No.  \r\nConnect USB camera or IEEE 1934 camera.\r\n→Yes. （Go to 3） \r\n3) Does your OS recognize the camera?\r\n→No.\r\nMake your OS recognize the camera by installing the driver.\r\n→Yes.  （Go to 4） \r\n4) Can you see your picture when you click the “camera switch button” which is located under the frame where your picture is supposed to be?\r\n→Yes.\r\nSometimes you have to click the camera switch button to display your picture.\r\n→No.  （Go to 5） \r\n5)Did you click “approve” on the small window that pops up after clicking the meeting start button?\r\n→No.\r\nClose the meeting window and start it again.\r\n→Yes.  （Go to 6） \r\n6)Are other applications for the camera open?\r\n→Yes.\r\nClose the application for camera and click the meeting start button again.\r\n→No.\r\nThe camera might not be compatible, or it may be broken.  Try restarting your PC, if that does not work, contact the camera maker’s technical support. \r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('131', '4', '1', '105', 'I cannot see other participants’ pictures. ', '1) Check the environment of other participants’ PCs using N2MY Checker.  \r\n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker.\r\n2) Are they logged in? \r\n→No. Ask them to login. \r\n→Yes. （Go to 3） \r\n3) Can they see their own pictures on their PCs? \r\n→ No. they should set it up so they can see their pictures on their own PCs.  (Refer to above “Self picture can not be seen”.) \r\n→ Yes. （Go to 4） \r\n4) Can you see their names? \r\n→No. There might be some systematic trouble.  Try starting the meeting again. \r\n→Yes. （Go to 5） \r\n5) Can you see the text you type on the chat window?\r\n→No. You might not be connected.  Try starting the meeting again.\r\n→Yes.  Other participants might not be connected.  Try starting the meeting again.  (Refer to above “Self picture cannot be seen”.) \r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('132', '4', '1', '106', 'Other participants cannot hear my voice. ', '1) Check the environment of your PC using N2MY Checker.  \r\n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker.  \r\n2) Is a microphone connected to your PC? \r\n→No.  Connect a microphone to your PC. \r\n→Yes.  （Go to 3） \r\n3) Did you click “approve” on the small window which pops up after clicking the meeting start button? \r\n→No.  Close the meeting window and start it again.  You have to click “approve” on the small window. \r\n→Yes.  （Go to 4） \r\n4) Is the microphone mute is set up? \r\n→Yes. Uncheck the microphone mute and set the volume high on the Sounds and Audio Devices section of the control panel. \r\n→No.  （Go to 5） \r\n5)Go to control panel.>Find the sounds and audio devices.> Audio>Recording>Click the Volume>Is the box of microphone checked? \r\n→No.  Check the box of the microphone. \r\n→Yes.  （Go to 6） \r\n6)Can others hear you speaking when you click the microphone switch button? \r\n→Yes.  Sometimes you have to click the microphone switch button, especially when there are a few microphones connected to a PC or because of the state of the OS. \r\n→No.  The microphone might not be compatible, or it might be broken.  Try restarting you PC.  If that does not work, contact the camera maker’s technical support. \r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('133', '4', '1', '107', 'I cannot hear the other participants’ voices.   ', '1) Check the environment of your PC using N2MY Checker.  \r\n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker. \r\n2) Is either a speaker or a headphone connected to your PC?\r\n→No.  Connect either a speaker or a headphone to your PC.  \r\n→Yes.  (Go to 3)\r\n 3) Is the speaker or headphone mute set up? \r\n→Yes.  Uncheck the microphone mute and set the volume to high on the Sounds and Audio Devices section of control panel. \r\n→No.  (Go to 4) \r\n4) Is the volume of the PC off?\r\n→Yes.  Turn up the volume. (Be sure to turn up the PC volume.) \r\n→No.  The set up of other users might not be normal.  Ask them to check. (Refer to above \"Other participants cannot hear my voices\")\r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('134', '5', '1', '100', 'Which microphone can I use? Which do you recommend? ', 'We recommend using USB microphones.  (Normally there are no sound input terminals.  If there are, they cannot be used in most cases.)  You may experience howling or echo with iSight or computer microphone. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('135', '5', '1', '101', 'I feel a delay when opening other applications in the front. ', 'We recommend using our system in the front.  There may be delay when the window goes to the background since the processing order is from front to back on a Mac. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('136', '5', '1', '102', 'There is a huge delay.  ', '1) How is your PC Connected?\r\n→ISDN　or　Dial up\r\nThe speed of your PC connection is less than what we recommend.\r\n→ADSL／DSL／T-1（Go to 2）\r\n2)Is your connection through a proxy?\r\n→Yes.  It is through a proxy.  If you are connecting via a proxy server you may experience slower connection speeds. Such a delay can be avoided by opening port 1935 outward. Please talk to your IT department.\r\n→No.  It is not through a proxy. （Go to 3）\r\n3)Does your Mac meet our system requirements? \r\n→No.  Please use a computer that meets our system requirements. \r\n→Yes.  (Go to 4) \r\n4) Are other applications open?\r\n→Yes.  Close other applications, the system may be out of resources. \r\n→No.  It seems that the video tip is the reason for the delay.  Your PC cannot see video images because it is too old or it is a notebook.  Please try using another PC.  (Go to 5) \r\n5) The window is not in the front.  There may be delay when the window goes to the background since the processing order is from front to back on a Mac.\r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('137', '5', '1', '103', 'My picture cannot be seen. ', '1) Is a web camera connected?\r\n→No.  Connect USB camera or IEEE 1934 camera.\r\n→Yes. （Go to 2）\r\n2) Does your OS recognize the camera?\r\n→No.\r\nMake your OS recognize the camera by installing the driver.\r\n→Yes.  （Go to 3） \r\n3) Can you see your picture when you click the “camera switch button” which is located under the frame where your picture is supposed to be?\r\n→Yes.  Sometimes you have to click the camera switch button to display your picture.  *You have to click the button on Mac with USB camera since Mac has its own camera driver.\r\n→No.  （Go to 4） \r\n4)Did you click “approve” on the small window that pops up after clicking the meeting start button?\r\n→No.  Close the meeting window and start it again.\r\n→Yes.  （Go to 5） \r\n5)Are other applications for the camera open?\r\n→Yes.  Close the application for camera and click the meeting start button again.\r\n→No.\r\nThe camera might not be compatible, or it may be broken.  Try restarting your PC, if that does not work, contact the camera maker’s technical support.\r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('138', '5', '1', '104', 'I cannot see other participants’ pictures.  ', '1) Are they logged in? \r\n→No. Ask them to login. \r\n→Yes. （Go to 2） \r\n2) Can they see their own pictures on their PCs? \r\n→ No. They should set it up so they can see their pictures on their own PCs.  (Refer to above “Self picture can not be seen”.) \r\n→ Yes. （Go to 3） \r\n3) Can you see their names? \r\n→No. There might be some systematic trouble.  Try starting the meeting again. \r\n→Yes. （Go to 4） \r\n4) Can you see the text you type on the chat window?\r\n→No. You might not be connected.  Try starting the meeting again.\r\n→Yes.  Other participants might not be connected.  Try starting the meeting again.  (Refer to above “Self picture cannot be seen”.)\r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('139', '5', '1', '105', 'Other participants cannot hear my voice. ', '1) Is a microphone connected to your Mac? \r\n→No.  Connect a microphone to your Mac. \r\n→Yes.  （Go to 2） \r\n2) Did you click “approve” on the small window which pops up after clicking the meeting start button? →No.  Close the meeting window and start it again.  You have to click “approve” on the small window. \r\n→Yes.  （Go to 3） \r\n3) Is the microphone mute is set up? \r\n→Yes. Click on the Apple Menu and select System Preferences>Sound (in Hardware)>Output.  Uncheck the mute box and turn on the volume.\r\n→No.  （Go to 4） \r\n4) Can others hear you speaking when you click the microphone switch button? \r\n→Yes.  Sometimes you have to click the microphone switch button, especially when there are a few microphones connected to a PC or because of the state of the OS. \r\n→No.  The microphone might not be compatible, or it might be broken.  Try restarting you Mac.  If that does not work, contact the camera maker’s technical support.\r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('140', '4', '1', '108', 'What should I do if the volume is low? ', 'You might have muted or turned down the volume.  Uncheck the microphone mute and set the volume to high on the Sounds and Audio Devices section of the control panel. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('141', '4', '1', '109', 'There is an echo to my voice. How can I make it go away? ', 'Is the person you are speaking with using speakers?\r\n→Yes.  The microphone catches your voice through the speakers. The echo goes away if the person uses a headset. The person needs an echo cancellation microphone if the speakers are used.\r\nReference: Echo cancellation microphone \"ACCUMICII\" by ClearOne. \r\n→No.  If the headset is cheap, an echo sometimes happens.  \r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('142', '4', '1', '110', 'I can not hear the others’ voices with so much noise.  ', '1)Do they use a headset? \r\n→No.  Ask them to use a headset. \r\n→yes.  (Go to 2) \r\n2)Is their microphone a directional microphone?\r\n→Yes.  There might be some problems with their PCs.  Ask them to restart their PCs and login again. \r\n→No.  We recommend you use a better quality headset.  \r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('143', '4', '1', '111', 'A page cannot be shown.  (Pop-up window is blocked.) ', 'If you are using “Windows XP Service Pack 2”, pop-ups are blocked.  On your browser click ‘Tools‘> Go to ‘Pop-up Blocker’> ‘Pop-up Blocker Settings’.  In the box labeled \"Address of Web site to allow\", type \" www.nice2meet.us \" (without quotation marks)  Click the ‘Add’ button to add it to the allowed list.  Click the ‘Close’ button to save your settings. ', '1', '1', '1');
INSERT INTO `faq` VALUES ('144', '5', '1', '106', 'I cannot hear the other participants’ voices.', '1) Is there a speaker or a headphone connected to your Mac?\r\n→No.  Connect a speaker or a headphone to your Mac.  \r\n→Yes.  (Go to 2) \r\n2) Is the speaker or headphone mute set up? \r\n→Yes.  Click on the Apple Menu and select System Preferences>Sound (in Hardware)>Output.  Uncheck the mute box and turn on the volume. \r\n→No.  (Go to 3) \r\n3) Is the volume on the computer off?\r\n→Yes.  Turn up the volume. (Be sure to turn up the computer volume.) \r\n→No.  The set up of other users might not be normal.  Ask them to check. (Refer to above \"Other participants cannot hear my voices\")\r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('145', '5', '1', '107', 'What should I do if the volume is low? ', 'You might have muted or turned down the volume.\r\nClick on the Apple Menu and select System Preferences>Sound (in Hardware)>Output.  Uncheck the mute box and turn on the volume.  \r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('146', '5', '1', '108', 'There is an echo to my voice. How can I make it go away? ', 'Is the person you are speaking with using speakers?\r\n→Yes.  The microphone catches your voice through the speakers. The echo goes away if the person uses a headset. The person needs an echo cancellation microphone if the speakers are used.→No.  If the headset is cheap, an echo sometimes happens. \r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('147', '5', '1', '109', 'I can not hear the others’ voices with so much noise.', '1) Do they use a headset? \r\n→No.  Ask them to use a headset. \r\n→yes.  (Go to 2) \r\n2) Are their microphones a directional microphone?\r\n→Yes.  There might be some problems with their PCs.  Ask them to restart their PCs and login again.→No.  We recommend you use a better quality headset.\r\n', '1', '1', '1');
INSERT INTO `faq` VALUES ('149', '3', '1', '106', '推奨動作環境を教えてください。', '動作環境ページをご参照ください\r\nhttp://www.nice2meet.us/ja/requirements/', '1', '0', '1');
INSERT INTO `faq` VALUES ('150', '3', '1', '111', '必須動作環境を教えてください', '動作環境ページをご参照ください。\r\nhttp://www.nice2meet.us/ja/requirements/', '1', '0', '1');
INSERT INTO `faq` VALUES ('151', '3', '1', '112', '推奨ブロードバンド環境（上り128kbps、下り384kbps以上）とありますが、これ以下の回線速度の場合、どのような不都合が発生するのでしょうか？', '音声遅延や、映像遅延などが発生いたします。\r\nまた、「低速回線モード」、「音声優先モード」、「利用帯域設定」など、回線速度の低い環境に合わせた設定を使用した場合、画質が粗くなったり、映像が出なくなります。', '1', '0', '1');

-- ----------------------------
-- Table structure for faq_data
-- ----------------------------
DROP TABLE IF EXISTS `faq_data`;
CREATE TABLE `faq_data` (
  `faq_key` int(11) NOT NULL,
  `lang` varchar(10) NOT NULL,
  `question` text,
  `answer` text,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  UNIQUE KEY `faq_key` (`faq_key`,`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='FAQデータ';

-- ----------------------------
-- Records of faq_data
-- ----------------------------
INSERT INTO `faq_data` VALUES ('23', 'en', 'Is it possible to try the service before purchasing it?', 'Yes, you can sign up online for a 2 week trial account.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('33', 'en', 'What should I do if I forget my ID and password?', 'Contact us by phone (0570-00-2192) or e-mail. We can send you the information once we confirm your identity.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('37', 'en', 'Can I use nice to meet you on a Mac?', 'Yes, but you can not use the ‘Sharing’ and ‘Checker’ features on a Mac.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('38', 'en', 'Can I setup nice to meet you within an intranet?', 'Yes, you can purchase a server to run nice to meet you on your intranet for added security. Please contact our sales team for more details. (0570-00-2192)', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('40', 'en', 'Can I rent a web camera?', 'Normally we do not offer rental of web cameras, but we can make exceptions. Please ask our sales team for more information. (0570-00-2192)', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('53', 'en', 'How secure is V-cube?', 'V-cube application is highly secure.  V-cube uses the original protocol for video and audio, and SSL (Secured Socket Layer)for documents such as text chat and file sharing. It is optional to have SSL for all communication.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('60', 'en', 'Is there a minimum contract term?', 'No, there is no minimum contract term. It is on a month-to-month basis.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('61', 'en', '', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('148', 'en', '', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('15', 'en', '', '', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('16', 'en', 'How many IDs and passwords do I get per contract?', 'One ID and password is issued per contract. You may share the ID and password with as many users as you’d like. It may be easier for you to think of it this way; you are renting a ‘virtual room’ and you have the right to allow anyone to use it.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('24', 'en', 'Is it possible to connect more than 10 locations?', 'Yes, up to 49 locations can be connected simultaneously by using the optional “Audience” feature.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('25', 'en', '', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('26', 'en', 'How can I start my meeting without disclosing my ID & password?', 'By using the ‘INVITATION’ feature you can e-mail an invitation with an automatically generated hyperlink to all your guests. This will allow your guests to have ‘one-click access’ into the room. Not only can you secure your ID & password, but it’s hassle free for your guests.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('27', 'en', 'Is it possible to participate in a meeting without an ID?', 'Yes, it is possible to participate in a meeting using a hyperlink sent to you by ‘e-mail invitation’. *An e-mail address is required to receive the invitation.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('28', 'en', 'Can I have several meetings simultaneously with one account ID & password?', 'No, you can only have one meeting at a time per account ID & password. Basically, the concept is that you are renting one ‘room’, therefore, you can only have one meeting at a time. Please, contact the support center if you would like to create another ‘room’ or account ID & password.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('30', 'en', '', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('31', 'en', 'Can I share files and applications running on my PC?', 'Yes, you can activate the desktop sharing feature and show the documents and applications running on your desktop. This feature will also enable people to take control of your PC. N2MY Sharing is an optional feature.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('32', 'en', 'Can I copy the contents of the whiteboard to a clipboard or vice versa?', 'No. There is only a File/picture upload on the whiteboard feature.\nYou can display files such as Word, Excel, PowerPoint, PDF, BMP, PNG, JPEG, and GIF on the whiteboard.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('34', 'en', 'How many people can participate in a meeting at the same time?', 'You can have up to 10 people attend a meeting with video and voice. If you use the ‘Audience’ feature you can have up to 49 locations connected at the same time.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('35', 'en', 'Tell me the advantages of using V-cube meeting over other services.', 'There are 3 big differences: \n1. No need for installation\n2. Cross platform, you can use it on a Macintosh as well as on Windows PC. It can also connect to H.323 compliant terminals.\n3. Bypass proxy and firewall. No need for any changes in settings for company LAN.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('36', 'en', 'Do I need any special equipment for V-cube Meeting?', 'All you need is broadband internet connection, computer, web camera and a headset.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('39', 'en', 'What happens if a server goes down, how do you respond to it?', 'In the event that a server goes down, we have backup servers in multiple locations that will keep our service running 24/7.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('41', 'en', 'Can I try the conference room package before purchasing it?', 'Normally we do not offer a free trial for our conference room package, but we have made exceptions in the past with money back guarantee deposit. Please ask our sales team for more information. (0570-00-2192)', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('42', 'en', 'Can I connect V-cube Meeting to the web conferencing system I currently have?', 'If your current system uses an H.323 compliant terminal, the answer is yes. But you must sign up for the H.323 optional service. Please contact our support team for more details.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('43', 'en', 'Can I try the N2MY mobile service with a free trial account?', 'Normally we do not offer a free trial for N2MY mobile, but we can make exceptions. Please ask our sales team for more information. (0570-00-2192)', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('58', 'en', 'How much bandwidth do I need for a conference between 10 locations?', 'You need at least 256kbps upstream and 512kbps downstream.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('59', 'en', 'How many frame rates does nice to meet you get?', 'It automatically changes depending on the number of participants connected. But the maximum frame rate you can get is 15fps.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('29', 'en', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('78', 'en', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('44', 'en', 'Is there an initial cost?', 'Yes, there is a one-time activation fee of 45,000 Yen for any plan.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('45', 'en', 'Please tell me the details about your pricing.', 'We have several plans starting from 9,900 Yen (tax included) per month. It is a flat fee no matter how many participants there are. It is like renting a virtual conference room online with an account and you are free to use it as you like. See pricing details for more info.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('46', 'en', 'Can I change the service plan? Can I change the plan within one month?', 'You can change the service plan in the middle of the month. However, the change will be effective until the first day of the following month.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('47', 'en', 'Do I have to pay the initial cost again to re-start the service once I close an account?', 'Yes, we charge the initial setup cost of 45,000 Yen.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('48', 'en', 'Is there a limitation on the number of users in a room?', 'This will depend on the type of price plan you purchased. Please contact our support team to find out what the limitations are on each plan.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('49', 'en', 'Tell me about conferencing package.', 'Please ask our support team to get an estimate. (0570-00-2192)', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('75', 'en', 'Is there an initial cost?', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('77', 'en', 'Do I have to pay the initial cost again to re-start the service once I close an account?', 'Yes, we charge the initial setup cost of 45,000 Yen.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('149', 'en', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('150', 'en', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('151', 'en', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('50', 'en', 'Can I use V-cube’s service even if my PC is inside a firewall (reverse proxy or NAT)?', 'Yes, our service was designed to bypass firewalls. However, if you are connecting via a proxy server you may experience slower connection speeds. Such delay can be avoided by opening port 1935 outward. For more details please contact our support team.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('52', 'en', 'Can I use nice to meet you with PHS mobile? (AirH or P-in)', 'It is not guaranteed by company, but you can use it with PHS mobile. It would be difficult to use it because of the network speed is usually lower than 64kbps.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('54', 'en', 'What kind of video/voice codec is used?', 'Flash is used for video encode/decode. The codec used in the feature is the Sorenson Spark codec made by Sorenson Media.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('55', 'en', 'What kind of protocols and ports are used?', 'RTMP is Macromedia\'s original protocol used in Flash Communication Server. For more details, please refer to the Macromedia website.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('56', 'en', 'What kind of security is used for login-ID, video, voice and data communication?', 'SSL is used for all the communication except video and voice. Video and voice are secured by our original protocol, which is proprietary. N2MY SSL is an optional feature makes it possible to make all communication more secure.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('57', 'en', 'Which web cameras do you recommend?', 'Our system is easiest to use with a standard USB web cam.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('51', 'en', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('20', 'en', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('62', 'en', 'I can not go into the meeting room because the program stops where it measures the connection speed.', 'This may occur because the internet connection speed is too slow or the performance of the PC is slow. Please try logging in again. If it occurs again, the PC may not satisfy minimum system requirements (internet connection or specification). Please refer to our requirements for more details.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('63', 'en', 'I see an error message when the PC connection speed is measured. （Proxy）', 'You might not be able to use this service in your network environment. There are a few possible reasons:\n1. Your network’s proxy server is old.\n2. The security setting of the network is too high.\nTo resolve this please ask our support team. We can suggest the best way to accommodate your network environment.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('64', 'en', 'I am disconnected during the meeting each time I use it.', 'The system regularly reports to the server that there is a meeting taking place. If the server does not get the report because of the delay, it sometimes disconnects. The basic reason is delay.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('65', 'en', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('66', 'en', 'There is a huge delay.', '1) Check the environment of your PC using N2MY Checker. \n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker.\n2) How is your PC Connected?\n→ISDN　or　Dial up\nThe speed of your PC connection is less than what we recommend.\n→ADSL／DSL／T-1（Go to 3）\n3)Is your connection through a proxy?\n→Yes. It is through a proxy.\nIf you are connecting via a proxy server you may experience slower connection speeds. Such a delay can be avoided by opening port 1935 outward. Please talk to your IT department.\n→No. It is not through a proxy. （Go to 4）\n4)Does your PC have a processor newer than a Pentium III 533 MHz?\n→No.\nYour PC is not fast enough. Your must have a PC that meets or exceeds our minimum system requirements.\n→Yes.（Go to 5）\n5) Are other applications open?\n→Yes.\nClose other applications, the system may be out of resources. \n→No. \nIt seems that the video tip is the reason for the delay. Your PC cannot see video images because it is too old or it is a notebook. Please try using another PC.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('67', 'en', 'My picture cannot be seen.', '1) Check the environment of your PC using N2MY Checker. \n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker. \n2) Is there a web camera connected?\n→No. \nConnect USB camera or IEEE 1934 camera.\n→Yes. （Go to 3） \n3) Does your OS recognize the camera?\n→No.\nMake your OS recognize the camera by installing the driver.\n→Yes. （Go to 4） \n4) Can you see your picture when you click the “camera switch button” which is located under the frame where your picture is supposed to be?\n→Yes.\nSometimes you have to click the camera switch button to display your picture.\n→No. （Go to 5） \n5)Did you click “approve” on the small window that pops up after clicking the meeting start button?\n→No.\nClose the meeting window and start it again.\n→Yes. （Go to 6） \n6)Are other applications for the camera open?\n→Yes.\nClose the application for camera and click the meeting start button again.\n→No.\nThe camera might not be compatible, or it may be broken. Try restarting your PC, if that does not work, contact the camera maker’s technical support.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('68', 'en', 'I cannot see other participants’ pictures.', '1) Check the environment of other participants’ PCs using N2MY Checker. \n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker.\n2) Are they logged in? \n→No. Ask them to login. \n→Yes. （Go to 3） \n3) Can they see their own pictures on their PCs? \n→ No. they should set it up so they can see their pictures on their own PCs. (Refer to above “Self picture can not be seen”.) \n→ Yes. （Go to 4） \n4) Can you see their names? \n→No. There might be some systematic trouble. Try starting the meeting again. \n→Yes. （Go to 5） \n5) Can you see the text you type on the chat window?\n→No. You might not be connected. Try starting the meeting again.\n→Yes. Other participants might not be connected. Try starting the meeting again. (Refer to above “Self picture cannot be seen”.)', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('69', 'en', 'Other participants cannot hear my voice.', '1) Check the environment of your PC using N2MY Checker. \n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker. \n2) Is a microphone connected to your PC? \n→No. Connect a microphone to your PC. \n→Yes. （Go to 3） \n3) Did you click “approve” on the small window which pops up after clicking the meeting start button? \n→No. Close the meeting window and start it again. You have to click “approve” on the small window. \n→Yes. （Go to 4） \n4) Is the microphone mute is set up? \n→Yes. Uncheck the microphone mute and set the volume high on the Sounds and Audio Devices section of the control panel. \n→No. （Go to 5） \n5)Go to control panel.>Find the sounds and audio devices.> Audio>Recording>Click the Volume>Is the box of microphone checked? \n→No. Check the box of the microphone. \n→Yes. （Go to 6） \n6)Can others hear you speaking when you click the microphone switch button? \n→Yes. Sometimes you have to click the microphone switch button, especially when there are a few microphones connected to a PC or because of the state of the OS. \n→No. The microphone might not be compatible, or it might be broken. Try restarting you PC. If that does not work, contact the camera maker’s technical support.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('70', 'en', 'I cannot hear the other participants’ voices.', '1) Check the environment of your PC using N2MY Checker. \n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker. \n2) Is either a speaker or a headphone connected to your PC?\n→No. Connect either a speaker or a headphone to your PC. \n→Yes. (Go to 3)\n3) Is the speaker or headphone mute set up? \n→Yes. Uncheck the microphone mute and set the volume to high on the Sounds and Audio Devices section of control panel. \n→No. (Go to 4) \n4) Is the volume of the PC off?\n→Yes. Turn up the volume. (Be sure to turn up the PC volume.) \n→No. The set up of other users might not be normal. Ask them to check. (Refer to above \"Other participants cannot hear my voices\")', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('71', 'en', 'What should I do if the volume is low?', 'You might have muted or turned down the volume. Uncheck the microphone mute and set the volume to high on the Sounds and Audio Devices section of the control panel.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('72', 'en', 'There is an echo to my voice. How can I make it go away?', 'Is the person you are speaking with using speakers?\n→Yes. The microphone catches your voice through the speakers. The echo goes away if the person uses a headset. The person needs an echo cancellation microphone if the speakers are used.\nReference: Echo cancellation microphone \"ACCUMICII\" by ClearOne. \n→No. If the headset is cheap, an echo sometimes happens.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('73', 'en', 'I can not hear the others’ voices with so much noise.', '1)Do they use a headset? \n→No. Ask them to use a headset. \n→yes. (Go to 2) \n2)Is their microphone a directional microphone?\n→Yes. There might be some problems with their PCs. Ask them to restart their PCs and login again. \n→No. We recommend you use a better quality headset.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('74', 'en', 'A page cannot be shown. (Pop-up window is blocked.)', 'If you are using “Windows XP Service Pack 2”, pop-ups are blocked. On your browser click ‘Tools‘> Go to ‘Pop-up Blocker’> ‘Pop-up Blocker Settings’. In the box labeled \"Address of Web site to allow\", type \" www.nice2meet.us \" (without quotation marks) Click the ‘Add’ button to add it to the allowed list. Click the ‘Close’ button to save your settings.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('5', 'en', 'I can not hear the others’ voices with so much noise.', '1) Is there a speaker or a headphone connected to your Mac?\n→No. Connect a speaker or a headphone to your Mac. \n→Yes. (Go to 2) \n2) Is the speaker or headphone mute set up? \n→Yes. Click on the Apple Menu and select System Preferences>Sound (in Hardware)>Output. Uncheck the mute box and turn on the volume. \n→No. (Go to 3) \n3) Is the volume on the computer off?\n→Yes. Turn up the volume. (Be sure to turn up the computer volume.) \n→No. The set up of other users might not be normal. Ask them to check. (Refer to above \"Other participants cannot hear my voices\")', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('79', 'en', 'Which microphone can I use? Which do you recommend?', 'We recommend using USB microphones. (Normally there are no sound input terminals. If there are, they cannot be used in most cases.) You may experience howling or echo with iSight or computer microphone.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('80', 'en', 'We recommend using our system in the front. There may be delay when the window goes to the background since the processing order is from front to back on a Mac.', 'We recommend using our system in the front. There may be delay when the window goes to the background since the processing order is from front to back on a Mac.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('81', 'en', 'There is a huge delay.', '1) How is your PC Connected?\n→ISDN　or　Dial up\nThe speed of your PC connection is less than what we recommend.\n→ADSL／DSL／T-1（Go to 2）\n2)Is your connection through a proxy?\n→Yes. It is through a proxy. If you are connecting via a proxy server you may experience slower connection speeds. Such a delay can be avoided by opening port 1935 outward. Please talk to your IT department.\n→No. It is not through a proxy. （Go to 3）\n3)Does your Mac meet our system requirements? \n→No. Please use a computer that meets our system requirements. \n→Yes. (Go to 4) \n4) Are other applications open?\n→Yes. Close other applications, the system may be out of resources. \n→No. It seems that the video tip is the reason for the delay. Your PC cannot see video images because it is too old or it is a notebook. Please try using another PC. (Go to 5) \n5) The window is not in the front. There may be delay when the window goes to the background since the processing order is from front to back on a Mac.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('82', 'en', 'My picture cannot be seen.', '1) Is a web camera connected?\n→No. Connect USB camera or IEEE 1934 camera.\n→Yes. （Go to 2）\n2) Does your OS recognize the camera?\n→No.\nMake your OS recognize the camera by installing the driver.\n→Yes. （Go to 3） \n3) Can you see your picture when you click the “camera switch button” which is located under the frame where your picture is supposed to be?\n→Yes. Sometimes you have to click the camera switch button to display your picture. *You have to click the button on Mac with USB camera since Mac has its own camera driver.\n→No. （Go to 4） \n4)Did you click “approve” on the small window that pops up after clicking the meeting start button?\n→No. Close the meeting window and start it again.\n→Yes. （Go to 5） \n5)Are other applications for the camera open?\n→Yes. Close the application for camera and click the meeting start button again.\n→No.\nThe camera might not be compatible, or it may be broken. Try restarting your PC, if that does not work, contact the camera maker’s technical support.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('83', 'en', 'I cannot see other participants’ pictures.', '1) Are they logged in? \n→No. Ask them to login. \n→Yes. （Go to 2） \n2) Can they see their own pictures on their PCs? \n→ No. They should set it up so they can see their pictures on their own PCs. (Refer to above “Self picture can not be seen”.) \n→ Yes. （Go to 3） \n3) Can you see their names? \n→No. There might be some systematic trouble. Try starting the meeting again. \n→Yes. （Go to 4） \n4) Can you see the text you type on the chat window?\n→No. You might not be connected. Try starting the meeting again.\n→Yes. Other participants might not be connected. Try starting the meeting again. (Refer to above “Self picture cannot be seen”.)', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('84', 'en', 'Other participants cannot hear my voice.', '1) Is a microphone connected to your Mac? \n→No. Connect a microphone to your Mac. \n→Yes. （Go to 2） \n2) Did you click “approve” on the small window which pops up after clicking the meeting start button? →No. Close the meeting window and start it again. You have to click “approve” on the small window. \n→Yes. （Go to 3） \n3) Is the microphone mute is set up? \n→Yes. Click on the Apple Menu and select System Preferences>Sound (in Hardware)>Output. Uncheck the mute box and turn on the volume.\n→No. （Go to 4） \n4) Can others hear you speaking when you click the microphone switch button? \n→Yes. Sometimes you have to click the microphone switch button, especially when there are a few microphones connected to a PC or because of the state of the OS. \n→No. The microphone might not be compatible, or it might be broken. Try restarting you Mac. If that does not work, contact the camera maker’s technical support.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('85', 'en', 'I cannot hear the other participants’ voices.', '1) Is there a speaker or a headphone connected to your Mac?\n→No. Connect a speaker or a headphone to your Mac. \n→Yes. (Go to 2) \n2) Is the speaker or headphone mute set up? \n→Yes. Click on the Apple Menu and select System Preferences>Sound (in Hardware)>Output. Uncheck the mute box and turn on the volume. \n→No. (Go to 3) \n3) Is the volume on the computer off?\n→Yes. Turn up the volume. (Be sure to turn up the computer volume.) \n→No. The set up of other users might not be normal. Ask them to check. (Refer to above \"Other participants cannot hear my voices\")', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('86', 'en', 'What should I do if the volume is low?', 'You might have muted or turned down the volume.\nClick on the Apple Menu and select System Preferences>Sound (in Hardware)>Output. Uncheck the mute box and turn on the volume.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('87', 'en', 'There is an echo to my voice. How can I make it go away?', 'Is the person you are speaking with using speakers?\n→Yes. The microphone catches your voice through the speakers. The echo goes away if the person uses a headset. The person needs an echo cancellation microphone if the speakers are used.→No. If the headset is cheap, an echo sometimes happens.', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('23', 'ja', '申し込みの前に、無料で試用することはできますか？', '実際にご利用頂ける「無料トライアルアカウント」を貸し出させて頂きますので、ウェブサイトからお申し込みください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('33', 'ja', 'IDとパスワードをなくしてしまったのですが、どうすればいいでしょうか？', 'お手数ですが、サポートセンターにお問い合わせください。\r\nサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('37', 'ja', 'Macで利用できますか？', 'はい、可能です。 ただし、一部ツール、N2MY Sharing N2MY CheckerはWindows版しかございません。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('38', 'ja', 'インターネットは利用できないが、イントラネット上で利用したい。', '弊社サーバをお買いあげいただき、イントラネット上で運用する方法があります。費用やスケジュールなどについては、サポートセンターまでお問い合わせください。 \r\nサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('40', 'ja', 'カメラを貸し出して頂くことは可能でしょうか？', '原則、貸し出しはしておりませんが、お問い合せ頂ければ貸し出しをさせていただくことも可能となっております。 \r\nサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('53', 'ja', 'セキュリティは万全なのでしょうか？', 'はい。映像と音声は非公開の独自プロトコルで通信しており、テキストチャットやファイル共有などドキュメントの部分は標準でSSLで通信しておりますので、限りなく万全な設計になっております。また、オプションサービスであるSSL対応サービスをご利用頂くことで、全ての通信をSSL化することも可能です。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('60', 'ja', '最低利用期間など、利用期間に関する制限はありますか？', 'いいえ、制限は一切ございません。 \r\n契約期間の制限はございませんので、月単位のご利用が可能です。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('61', 'ja', '海外でも利用できますか？', 'はい、可能です。ウェブベースでのテレビ会議システムであるため、インターネットをご利用することが可能であれば、どなたでも参加して頂くことが可能です。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('148', 'ja', '無料トライアルアカウントでH.323TV会議システムへの接続（N2MY H.323）も利用できますか？', '原則、ご利用頂けませんが、お問い合せ頂ければH.323TV会議システムへの接続（N2MY H.323）をご利用頂ける無料トライアルアカウントを発行することも可能となっております。 カスタマーサポートセンターまでお問合せ下さい。\r\nカスタマーサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('15', 'ja', 'ホワイトボードには、どのような資料が貼り付けられますか？', 'BMP、JPEG、GIF、PNG、TIFF形式画像や、Word、Excel、PowerPoint、PDF　などの資料の貼り付けが可能です。\r\n※動画ファイル、PowerPointのアニメーション機能には対応しておりません。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('16', 'ja', 'IDの管理はユーザ毎で行うのでしょうか？', 'いいえ。ログインＩＤの発行は基本的に１契約＝１会議室あたり１つとなります。\r\nお使いいただくユーザ総数は100人でも1000人でも問題ありません。座席数が10人の会議室を、１室ご利用するイメージでお考えいただければと思います。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('24', 'ja', '1契約（１会議室）につき10拠点間での会議が可能ということですが、10拠点以上で行うことは可能でしょうか？', 'オーディエンス機能（閲覧のみの機能）を利用することにより最大50拠点までテレビ会議に参加して頂くことが可能となります。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('25', 'ja', 'オーディエンス機能とはなんでしょうか？', 'オーディエンス（傍聴者）とは、テレビ会議を傍聴することができる会議参加者です。オーディエンス参加者は、通常参加者が会議を行っている様子を閲覧することができます。オーディエンス参加者が、通常参加者となって、行われている会議に対して意見等を述べることもできます。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('26', 'ja', '社外の人間と会議をしたいのですが、ID・パスワードを教えたくありません。', 'はい、可能です。「招待メール機能」をご利用頂くことで、他の会議参加者にIDとパスワードを教えることなくテレビ会議を行うことが可能となります。\r\n※招待メールを受け取る為のアドレスが必要です。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('27', 'ja', 'IDを持っていないユーザも参加することはできますか？', 'はい、可能です。招待メール機能によって参加頂けます。\r\n※招待メールを受け取る為のアドレスが必要です。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('28', 'ja', '1契約で複数の会議を同時に開催することはできますか？', 'いいえ、出来ません。\r\n別途、追加で会議室の契約ができますので、お問い合わせフォームからお問い合わせいただくか、サポートセンターにお問い合わせください。\r\nサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('30', 'ja', 'ホワイトボードに貼り付ける資料の容量制限はありますか？', '1ファイルにつき、20Mまでアップロード可能です。また、1ファイルにつき20ページまでアップロード可能です。20Mまたは、20ページを超えるファイルは分割してアップロードしてください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('31', 'ja', '自分のPCで動いているアプリケーションの画面を表示・共有できますか？', 'はい、可能です。\r\nただし、N2MY Sharingへのオプション契約が必要となります。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('32', 'ja', 'ホワイトボードの内容をコピー＆ペーストすることはできますか？', 'いいえ、こちらの機能には現在対応しておりません。 ただし、「議事録を見る」機能で会議終了後でもホワイトボードの内容を見ることができます。 \r\nまた、会議中に「ファイルキャビネット」機能でお互いファイルの送受信を行うことができます。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('34', 'ja', '同時に何人、会議に参加できますか？', '通常サービスでは最大10人まで同時に会議に参加して頂くことが可能です。\r\nまた、オプション機能であるオーディエンス機能をご利用頂くことで最大50人まで同時に会議に参加して頂くことが可能となります。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('35', 'ja', '他社テレビ会議サービスとの違いについて教えてください。', 'nice to meet youが他社サービスと大きく異なる点は以下の5点となっております。\r\n\r\n・インストール不要。\r\n専用ソフトをインストールする必要がなくインターネットが使用出来ればご利用可能です。\r\n\r\n・環境を選ばないマルチプラットフォーム。\r\nWindows、Macでも利用できます。\r\n\r\n・H.323対応のテレビ会議システムにも対応しています。\r\n\r\n・ 携帯対応。テレビ電話機能を持つ携帯電話での参加が可能です。\r\n\r\n・プロキシ、Firewall対応。企業内ＬＡＮの設定にも自動的に対応するので、難しい設定は不要です。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('36', 'ja', 'nice to meet youを利用するために必要な機器などは何ですか？', 'ブロードバンド環境、パソコン、カメラ、ヘッドセットがあればすぐにnice to meet youをご利用頂けます。カメラがなくても参加可能です。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('39', 'ja', 'サーバが落ちた場合はどのような対応をして頂けるのでしょうか？', 'センターが分散されているため、万が一サーバがダウンした場合でも、サービスに影響を与えないように設計されております。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('41', 'ja', '会議室パックを導入前に試用してみたいのですが、貸し出してもらうことは可能でしょうか？', '会議室パッケージのお貸し出しにつきましては、一度カスタマーサポートセンターまでお問い合せください。\r\nカスタマーサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('42', 'ja', '既に以前購入したテレビ会議システムがあるのですが、それを利用することはできますか？', 'H.323対応のテレビ会議システムであればご利用頂けます。\r\nただし、オプションサービスであるH.323対応サービスをご契約して頂く必要がございます。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('43', 'ja', '無料トライアルアカウントで携帯電話接続（N2MY Mobile）も利用できますか？', '原則、ご利用頂けませんが、携帯電話接続（N2MY Mobile）のお試しをご希望のお客さまは、携帯電話接続機能の付いた無料トライアルアカウントを発行させていただきます。 \r\nカスタマーサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('58', 'ja', '10拠点で利用する場合の必要帯域は？', '上り256Kbps、下り512Kbpsとなります。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('59', 'ja', 'フレームレートはいくつですか？', '最大15fpsとなっております。\r\n会議参加人数に合わせて、自動的に最適なフレームレートになります。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('29', 'ja', '海外でも利用できますか？', 'はい、可能です。ウェブベースでのテレビ会議システムであるため、インターネットをご利用することが可能であれば、どなたでも参加して頂くことが可能です。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('78', 'ja', '1契約で複数のセミナーを同時に開催することはできますか？', 'いいえ、出来ません。別途、追加でセミナールームの契約ができますので、お問い合わせフォームからお問い合わせいただくか、カスタマーサポートセンターにお問い合わせください。\r\nカスタマーサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('44', 'ja', '初期費用はいくらですか？', 'いずれのプランも一律 47,250円（税込）となります。こちらの料金にはセッティング費用等の諸費用も含まれております。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('45', 'ja', '料金体系の詳細をおしえてください。', '月額10,395（税込）より各種プランをご用意しております。\r\nなお、課金はルームチャージ制となっておりますのでご利用人数に関係なく一律料金となっております。\r\n1つのアカウントで、オンライン上の会議室を１室お貸しするようなイメージになります。詳細は料金プラン一覧をご覧ください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('46', 'ja', '料金プランを入会後に変更する際に、禁則事項など何か規定はありますか？\r\n（入会後１ヶ月しないうちにプランを変更するなど）', '特に制限はございません。プランの変更につきましては月の途中でも変更は可能ですが、適用は翌月１日からとなります。（その月は現行のプランでのご利用となります）前払いのお客様は翌々月の変更となります。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('47', 'ja', '解約後に、再開する場合は、初期費用はかかるのでしょうか？', 'はい、再度設定いたしますので、初期費用が発生いたします。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('48', 'ja', '料金は「1室あたり」とありますが、利用ユーザー数に制限はあるのでしょうか？', 'ご利用ユーザー数に制限はございません。課金は会議室単位になりますので、何名様でご利用されても料金は変わりません。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('49', 'ja', '会議室パックの概算費用を教えてください。', '弊社担当者からお見積もりをさせて頂きますので、サポートセンターまでお問い合せください。\r\nサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('75', 'ja', '初期費用はいくらですか？', 'ご利用のプランによって異なりますので、カスタマーサポートセンターまでお問合せ下さい。\r\nカスタマーサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('77', 'ja', '解約後に、再開する場合は、初期費用はかかるのでしょうか？', 'はい、再度設定しますので、初期費用は発生します。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('149', 'ja', '推奨動作環境を教えてください。', '動作環境ページをご参照ください\r\nhttp://www.nice2meet.us/ja/requirements/', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('150', 'ja', '必須動作環境を教えてください', '動作環境ページをご参照ください。\r\nhttp://www.nice2meet.us/ja/requirements/', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('151', 'ja', '推奨ブロードバンド環境（上り128kbps、下り384kbps以上）とありますが、これ以下の回線速度の場合、どのような不都合が発生するのでしょうか？', '音声遅延や、映像遅延などが発生いたします。\r\nまた、「低速回線モード」、「音声優先モード」、「利用帯域設定」など、回線速度の低い環境に合わせた設定を使用した場合、画質が粗くなったり、映像が出なくなります。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('50', 'ja', 'ファイアウォール（リバースプロキシやNAT）からの接続に対応していますか？', 'はい。ただしプロキシ経由の接続の場合には、プロキシの種類によって、映像・音声の遅延が発生する場合がございます。　対応策がございますので、別途お問い合わせください。\r\nカスタマーサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('52', 'ja', 'PHS移動通信（AirH″などのモバイルカード）からの利用は可能ですか？', '弊社では、動作の保証をしておりませんが、利用する事は可能です。 \r\n「低速回線モード」、「音声優先モード」、「利用帯域設定」など、回線速度の低い環境に合わせた設定も可能です。\r\nただし、PHS環境（AirH\"など）の場合には、128kbps接続でもパケット処理などにより、実質的には64kbps以下の速度しか出ないケースが多い為、実質的な利用は困難なケースがございます。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('54', 'ja', '映像と音声のコーデックは何を使用していますか？', 'ビデオのエンコード・デコードは、Flashの機能により行われております。\r\nその機能で使用されているコーデックはSorenson Media社のSorenson Sparkコーデックです。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('55', 'ja', '使用しているプロトコルは何でしょうか？', 'Flash Media Serverで使用される、Adobe社独自のプロトコルRTMPを使用しております。\r\n※RTMPはFlash Media Serverで使用される、Adobe社独自のプロトコルです。\r\n詳細は、Adobe社のウェブページ（http://www.adobe.com/jp/products/flashmediaserver/）をご覧ください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('56', 'ja', '映像・音声・データのやりとりや、ログインＩＤなどは、どのようなセキュリティ対応をされているのか教えてください。', 'ビデオストリーム以外の全ての通信は、SSL通信を利用しております。\r\nビデオストリームは独自の通信を行っており、プロトコル仕様は公開されていないため現状でも安全ですが、オプション機能により、ビデオストリームのSSＬ化も可能です。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('57', 'ja', 'カメラは何が使えますか？推奨カメラなどはありますか？', 'USBカメラを利用することが可能です。\r\n詳細は、動作確認済みカメラをご覧ください。\r\nhttp://www.nice2meet.us/ja/tools/', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('51', 'ja', '推奨はブロードバンド環境（上り128kbps、下り384kbps以上）とありますが、これ以下の回線速度の場合、どのような不都合が発生するのでしょうか？', '音声遅延や、映像遅延などが発生いたします。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('20', 'ja', '3G対応の携帯電話を呼び出してもつながらない', '「N2MY Mobile」は非通知で発信を行なっています。\r\n携帯電話側が非通知着信を拒否する設定になっている場合は、許可する設定に変更を行なってください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('62', 'ja', '回線速度を測定中のまま進まない', '回線速度が非常に遅い、あるいは回線を確保出来ていない可能性がございます。\r\n再度、入室していただきますと回線を再取得いたしますので、問題を解決する場合がございます。\r\n上記の方法で解決しない場合は、当社の推奨環境（回線、PCスペック）を満たしていない場合がございますので、弊社推奨環境をご確認ください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('63', 'ja', '回線速度を測定する画面でエラーが表示される（プロキシ）', '回線速度が非常に遅い、あるいは回線を確保出来ていない可能性がございます。\r\n再度、入室していただきますと回線を再取得いたしますので、問題を解決する場合がございます。\r\n上記の方法で解決しない場合は、当社の推奨環境（回線、PCスペック）を満たしていない場合がございますので、弊社推奨環境をご確認ください。\r\nカスタマーサポートセンター：0570-00-2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('64', 'ja', '毎回、会議中に「切断されました」となってしまう', '当システムでは、会議サーバに対して一定時間毎に「正常に会議が行われている」ことを通信しております。そのため、遅延が著しく発生している場合、サーバ側が接続していない状況と誤認し切断する場合がございます。会議では遅延を引き起こさなくても根本的な原因は遅延と同じになります。\r\n※プロキシが存在する場合や回線環境、PCスペックが当社製品の動作環境を下回っている可能性があります。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('65', 'ja', '映像の縦横比がおかしい', 'ご使用中のビデオデバイスによっては、映像取得サイズを変更した際に映像の縦横比が変更されたり、上下に黒い帯が入る可能性があります。デバイスの性能や設定等をご確認ください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('66', 'ja', '遅延がひどい', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)回線は何をお使いですか？\r\n\r\n→ISDN　or　ダイヤルアップ\r\n推奨の回線速度が維持できません。快適で安価なBフレッツをお勧めいたします。\r\n\r\n→ADSL／Bフレッツ／その他、光やケーブルテレビなど　（3へ進む）\r\n\r\n\r\n3)プロキシ経由の接続ですか？\r\n\r\n→プロキシ経由です。\r\nプロキシ経由の接続の場合、プロキシサーバの特性上、遅延が発生する可能性があります。ただし、1935ポートを外向きに開けていただければプロキシサーバを介さずに通信ができますので、遅延の回避が可能となります。詳細はネットワーク管理者にその旨ご相談ください。\r\n\r\n→プロキシ経由ではない（4へ進む）\r\n\r\n\r\n4)お使いのパソコンは、PentiumIIIの533MHz以上ですか？\r\n\r\n→いいえ。\r\n推奨のスペックを満たしておりません。映像などを処理するに当たり、ある程度のスペックが要求されます。大変恐縮ではございますが、推奨環境以上のパソコンでご利用ください。\r\n\r\n→はい。（5へ進む）\r\n\r\n\r\n5)何かほかのアプリケーションが起動していませんか？\r\n\r\n→はい。\r\nシステムのリソース不足の可能性がございますので、他のアプリケーションを終了してください。\r\n\r\n→いいえ。\r\nご利用のパソコン自体に搭載されておりますビデオチップが遅延の原因かと思われます。少し古いタイプやノートパソコンなどでは映像を見るに適したスペックになっていないことがあります。お手数ではございますが再度、別のパソコンでご利用ください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('67', 'ja', '自分の映像が表示されない', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)カメラが接続されていますか？\r\n\r\n→接続されていない。\r\nUSBカメラもしくは、IEEE1394のカメラを接続してください。\r\n\r\n→接続されている（3へ進む）\r\n\r\n\r\n3)カメラはOSに認識されていますか？\r\n\r\n→認識されていない。\r\nカメラのマニュアルに従い、ドライバをインストールするなどして、カメラを認識させてください。\r\n\r\n→認識されている。（4へ進む）\r\n\r\n\r\n4)「カメラ切り替え」ボタンを何度かクリックして映像が表示されますか？\r\n\r\n→表示される。\r\nカメラが複数台つながっている場合や、OSの状態によってはカメラ切り替えボタンを押さないとカメラが利用できない場合がございます。\r\n\r\n→表示されない。（5へ進む）\r\n\r\n\r\n5)「会議室に入室」ボタンを押した後に表示される小さなウィンドウで、「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n会議ウィンドウを閉じて、再度、会議室に入室してください。\r\nその際に、必ず「許可」をクリックするようにしてください。\r\n\r\n→クリックした。（6へ進む）\r\n\r\n\r\n6)ほかにカメラを使うアプリケーションは起動していませんか？\r\n\r\n→起動している。\r\nカメラを使う他のアプリケーションを終了し、会議ウィンドウを閉じて、再度、会議室に入室してください。\r\n\r\n→起動していない。\r\n対応していないカメラである可能性か、カメラが壊れている可能性がございます。また、パソコンの再起動を一度お試しください。カメラの不具合につきましては各メーカーへお問い合わせください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('68', 'ja', '相手の映像が表示されない', '1)相手の方にN2MY Checker2でご利用環境の診断を行って頂いてください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)相手がログインしていますか？\r\n\r\n→ログインしていない。\r\n相手の方へ会議室への入室のお願いをしてください。\r\n\r\n→ログインしている。（3へ進む）\r\n\r\n\r\n3)相手は相手自身の映像を相手のパソコン上で見えていますか？\r\n\r\n→見えていない。\r\n相手の映像が相手のPC上で表示されるように設定してください。\r\n（『自分の映像が表示されない』を参照）\r\n\r\n→見えている。（4へ進む）\r\n\r\n\r\n4)相手の名前は表示されていますか？\r\n\r\n→表示されている。\r\n何らかのシステムトラブルの可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示されていない。（5へ進む）\r\n\r\n\r\n5)チャットウィンドウに文字を打ち込んで送信を押した後、チャットウィンドウにその内容が表示されますか？\r\n\r\n→表示されない。\r\nお客様が正常に接続できていない可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示される。\r\n相手が正常に接続できていない可能性がございます。\r\n相手に会議室への再入室のお願いをしてください。（『自分の映像が表示されない』を参照）', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('69', 'ja', '自分の音声が相手に届かない', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)パソコンにマイクは接続されていますか？\r\n\r\n→接続されていない。\r\nパソコンにマイクを接続し、会議室へ再入室してください。\r\n\r\n→接続されている。（3へ進む）\r\n\r\n\r\n3)会議開始後に表示される小さなウィンドウで「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n再度会議室に入室し直してください。その際に必ず「許可」をクリックしてください。\r\n\r\n→クリックした。（4へ進む）\r\n\r\n\r\n4)パソコンのマイクがミュートの設定になっていませんか？\r\n\r\n→なっている。\r\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。\r\n\r\n→なっていない。（5へ進む）\r\n\r\n\r\n5)[コントロールパネル]→[サウンドとオーディオデバイス]→[オーディオ]→[録音]→[音量]→[録音コントロール]の画面で「マイク」にチェックがはいっていますか？\r\n\r\n→入っていない。\r\n「マイク」にチェックをいれてください。\r\n\r\n→入っている。（6へ進む）\r\n\r\n\r\n6)マイク切り替えボタンをクリックして、相手に自分の音声が届くようになりますか？\r\n\r\n→届く。\r\nマイクが複数台つながっている場合や、OSの状態によってはマイク切り替えボタンを押さないとマイクが利用できない場合があります。\r\n\r\n→届かない。\r\nマイクが故障しているか、お使いのコンピュータでは使えないマイクの可能性があります。また、パソコンの再起動を一度お試しください。マイクの不具合につきましては各メーカーへお問い合わせください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('70', 'ja', '相手の音声が自分に届かない（音が出ない）', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)スピーカもしくはヘッドフォンはつながっていますか？\r\n\r\n→つながっていない。\r\nスピーカもしくはヘッドフォンを接続し、会議室へ再入室してください。\r\n\r\n→つながっている（3へ進む）\r\n\r\n\r\n3)パソコンのスピーカもしくはヘッドフォンがミュートになっていませんか？\r\n\r\n→ミュートになっている。\r\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。\r\n\r\n→ミュートになっていない（4へ進む）\r\n\r\n\r\n4)パソコン本体のボリュームがOFFになっていませんか？\r\n\r\n→なっている。\r\nボリュームをあげてください。（設定ではなく、パソコン本体のボリューム）\r\n\r\n→なっていない。\r\n相手の設定が正常でない可能性があります。\r\n相手側にFAQの『自分の音声が相手に届かない。』を確認してください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('71', 'ja', '音が小さいのですが、どうすればいいでしょうか？', 'ミュートもしくはボリュームが小さくなっていませんか？\r\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('72', 'ja', '自分の声がエコーしてしまいます。直し方はありますか？', '相手側がスピーカーを使っていませんか？\r\n\r\n→使用している。\r\nあなたの声を相手側のマイクが拾っています。ヘッドセットをご利用頂ければエコーは無くなります。相手側でスピーカーの使用がやむを得ない場合は、エコーキャンセラー付きのマイクが必要になります。\r\n参考：エコーキャンセラー付きマイク　ClearOne社　ACCUMICII\r\n\r\n→使用していない。\r\n相手のヘッドセットが安価な物だと、多少自分の声が戻ってくる可能性がございます。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('73', 'ja', '相手の声がノイズが多くてよく聞こえない', '1)相手はヘッドセットを使用していますか？\r\n\r\n→使用していない。\r\nヘッドセットをご利用頂いてください。\r\n\r\n→使用している（2へ進む）\r\n\r\n\r\n2)相手のヘッドセットのマイクが、音声集音タイプですか？\r\n\r\n→はい。\r\n相手のコンピュータに何か問題がある可能性がございます。PCの再起動のお願いをしてください。\r\n\r\n→いいえ。\r\n現在の物より高性能なヘッドセットをご利用頂けますようお勧めいたします。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('74', 'ja', 'クリックしてもページが表示されない（ポップアップ表示がブロックされてしまう）', '「Windows XP Service Pack 2」をご利用されている方は、ポップアップ表示がブロックされてしまいます。\r\nブラウザのツールバーより「ツール」を選択、メニューから「ポップアップブロック」→「ポップアップブロックの設定」を選択し、クリックします。\r\n「許可するWebサイトのアドレス」に『www.nice2meet.us』と入力し、「追加」ボタンをクリックします。\r\n「閉じる」ボタンをクリックし、設定ウインドウを閉じます。\r\n\r\n以上で設定は完了です。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('5', 'ja', '相手の声がノイズが多くてよく聞こえない', '1)相手はヘッドセットを使用していますか？\r\n\r\n→使用していない。\r\nヘッドセットをご利用頂いてください。\r\n\r\n→使用している（2へ進む）\r\n\r\n\r\n2)相手のヘッドセットのマイクが、音声集音タイプですか？\r\n\r\n→はい。\r\n相手のコンピュータに何か問題がある可能性がございます。PCの再起動のお願いをしてください。\r\n\r\n→いいえ。\r\n現在の物より高性能なヘッドセットをご利用頂けますようお勧めいたします。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('79', 'ja', '推奨のマイクなどありますか？', 'USBタイプの利用を推奨いたします。\r\nピンジャックタイプですとノイズが発生しやすくなります。\r\niSightや本体マイクでも会議可能ですが、ハウリングやエコーが発生しやすくなります。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('80', 'ja', '会議中に他の作業をしていると、会議の処理が遅いように感じるのですが？', '最前面での利用を推奨いたします。\r\nOSやブラウザの設定によっては、会議の画面がバックグランドに回った際の処理が遅くなるため、常に最前面で利用してください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('81', 'ja', '遅延がひどい', '1)回線は何をお使いですか？\r\n\r\n→ISDN　or　ダイヤルアップ\r\n推奨の回線速度が維持できません。快適で安価なBフレッツをお勧めいたします。\r\n\r\n→ADSL／Bフレッツ／その他、光やケーブルテレビなど（2へ進む）\r\n\r\n\r\n2)プロキシ経由の接続ですか？\r\n\r\n→プロキシ経由です。\r\nプロキシ経由の接続の場合、プロキシサーバの特性上、遅延が発生する可能性があります。ただし、1935ポートを外向きに開けていただければプロキシサーバを介さずに通信ができますので、遅延の回避が可能となります。詳細はネットワーク管理者にその旨ご相談ください。\r\n\r\n→プロキシ経由ではない（3へ進む）\r\n\r\n\r\n3)お使いのパソコンは推奨環境を満たしていますか？\r\n\r\n→いいえ。\r\n映像などを処理するに当たり、ある程度のスペックが要求されます。大変恐縮ではございますが、推奨環境以上のパソコンでご利用ください。\r\n\r\n→はい。（4へ進む）\r\n\r\n\r\n4)何かほかのアプリケーションが起動していませんか？\r\n\r\n→はい。\r\nシステムのリソース不足の可能性がございますので、他のアプリケーションを終了してください。\r\n\r\n→いいえ。\r\nご利用のパソコン自体に搭載されておりますビデオチップが遅延の原因かと思われます。少し古いタイプやノートパソコンなどでは映像を見るに適したスペックになっていないことがあります。お手数ではございますが再度、別のパソコンでご利用ください。(5へ進む)\r\n\r\n\r\n5)ウィンドウが最前面になっていない。\r\n\r\nMacは最前面のウィンドウの処理を優先にする為、ウィンドウがバックグラウンドに回ると極端に処理が遅くなる場合があります。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('82', 'ja', '自分の映像が表示されない', '1)カメラが接続されていますか？\r\n\r\n→接続されていない。\r\nUSBカメラもしくは、IEEE1394のカメラを接続してください。\r\n\r\n→接続されている（2へ進む）\r\n\r\n\r\n2)カメラはOSに認識されていますか？\r\n\r\n→認識されていない。\r\nカメラのマニュアルに従い、ドライバをインストールするなどして、カメラを認識させてください。\r\n\r\n→認識されている。（3へ進む）\r\n\r\n\r\n3)「カメラ切り替え」ボタンを何度かクリックして映像が表示されますか？　\r\n\r\n→表示される。\r\nカメラが複数台つながっている場合や、OSの状態によってはカメラ切り替えボタンを押さないとカメラが利用できない場合がございます。\r\n※Macでは標準でカメラドライバを持っている為、USBカメラ利用の場合は必ず切り替えを行う必要があります。\r\n\r\n→表示されない。（4へ進む）\r\n\r\n\r\n4)「会議室に入室」ボタンを押した後に表示される小さなウィンドウで、「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n会議ウィンドウを閉じて、再度、会議室に入室してください。\r\nその際に、必ず「許可」をクリックするようにしてください。\r\n\r\n→クリックした。（5へ進む）\r\n\r\n\r\n5)ほかにカメラを使うアプリケーションは起動していませんか？\r\n\r\n→起動している。\r\nカメラを使う他のアプリケーションを終了し、会議ウィンドウを閉じて、再度、会議室に入室してください。\r\n\r\n→起動していない。\r\n対応していないカメラである可能性か、カメラが壊れている可能性がございます。また、パソコンの再起動を一度お試しください。カメラの不具合につきましては各メーカーへお問い合わせください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('83', 'ja', '相手の映像が表示されない', '1)相手がログインしていますか？\r\n\r\n→ログインしていない。\r\n相手の方へ会議室への入室のお願いをしてください。\r\n\r\n→ログインしている。（2へ進む）\r\n\r\n\r\n2)相手は相手自身の映像を相手のパソコン上で見えていますか？\r\n\r\n→見えていない。\r\n相手の映像が相手のPC上で表示されるように設定してください。\r\n（『自分の映像が表示されない』を参照）\r\n\r\n→見えている。（3へ進む）\r\n\r\n\r\n3)相手の名前は表示されていますか？\r\n\r\n→表示されている。\r\n何らかのシステムトラブルの可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示されていない。（4へ進む）\r\n\r\n\r\n4)チャットウィンドウに文字を打ち込んで送信を押した後、チャットウィンドウにその内容が表示されますか？\r\n\r\n→表示されない。\r\nお客様が正常に接続できていない可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示される。\r\n相手が正常に接続できていない可能性がございます。\r\n相手に会議室への再入室のお願いをしてください。（『自分の映像が表示されない』を参照）', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('84', 'ja', '自分の音声が相手に届かない', '1)パソコンにマイクは接続されていますか？\r\n\r\n→接続されていない。\r\nパソコンにマイクを接続し、会議室へ再入室してください。\r\n\r\n→接続されている。（2へ進む）\r\n\r\n\r\n2)会議開始後に表示される小さなウィンドウで「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n再度、会議室に入室し直してください。その際に必ず「許可」をクリックしてください。\r\n\r\n→クリックした。（3へ進む）\r\n\r\n\r\n3)パソコンのマイクがミュートの設定になっていませんか？\r\n\r\n→なっている。\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[入力]→[主音量]→[消音]にチェックを外してください。\r\n\r\n→なっていない。（4へ進む）\r\n\r\n\r\n4)マイク切り替えボタンを何度かクリックして、相手に自分の音声が届くようになりますか？\r\n\r\n→届く。\r\nMacでは、マイク切り替えボタンを押さないとマイクが利用できない場合があります。\r\n\r\n→届かない。\r\nマイクが故障しているか、お使いのコンピュータでは使えない可能性があります。また、パソコンの再起動を一度お試しください。マイクの不具合につきましては各メーカーへお問い合わせください。\r\nMacの音声入力はほとんどの場合、音声入力端子が使えない場合があります。当社ではUSBアダプタを必ずご使用になる事を推奨しております。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('85', 'ja', '相手の音声が自分に届かない（音が出ない）', '1)スピーカもしくはヘッドフォンはつながっていますか？\r\n\r\n→つながっていない。\r\nスピーカもしくはヘッドフォンを接続し、会議室へ再入室してください。\r\n\r\n→つながっている（2へ進む）\r\n\r\n\r\n2)パソコンのスピーカもしくはヘッドフォンがミュートになっていませんか？\r\n\r\n→ミュートになっている。\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[出力]→[主音量]→[消音]のチェックを外して、ボリュームを上げてください。\r\n\r\n→ミュートになっていない（3へ進む）\r\n\r\n\r\n3)パソコン本体のボリュームがOFFになっていませんか？\r\n\r\n→なっている。\r\nボリュームをあげてください。（設定ではなく、パソコン本体のボリューム）\r\n\r\n→なっていない。\r\n相手の設定が正常でない可能性があります。\r\n相手側にFAQの『自分の音声が相手に届かない。』の確認をお願いをしてください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('86', 'ja', '音が小さいのですが、どうすればいいでしょうか？', 'ミュートもしくはボリュームが小さくなっていませんか？\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[入力]→[入力音量]→[消音]のチェックを外して、ボリュームを上げてください。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('87', 'ja', '自分の声がエコーしてしまいます。直し方はありますか？', '相手側がスピーカーを使っていませんか？\r\n\r\n→使用している。\r\nあなたの声を相手側のマイクが拾っています。ヘッドセットをご利用頂ければエコーは無くなります。相手側でスピーカーの使用がやむを得ない場合は、エコーキャンセラー付きのマイクが必要になります。\r\n\r\n→使用していない。\r\n相手のヘッドセットが安価な物だと、多少自分の声が戻ってくる可能性がございます。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('23', 'zh', '注册之前，或者您可以免费试用？', '其实用“免费试用账户”，所以我们租了一个顶Kimasu，请从网站申请。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('33', 'zh', '我已经失去了我的ID和密码，我该怎么办？', '请稍候，请与支持中心。支持中心:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('37', 'zh', '我可以使用Mac吗？', '是的，可以。然而，一些工具，N2MY分享N2MY检查器的Windows Shikagozaimasen版本。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('38', 'zh', '互联网是不可用，您要使用Intranet上。', '感谢您购买服务器给我们如何在Intranet上运行你。关于成本和进度，请与支持中心。支持中心:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('40', 'zh', '用户可以租相机，可以吗？', '的原则，但不租，我们可以有快乐的贷款应与顶解礼。支持中心:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('53', 'zh', '什么安全，我们准备好了吗？', '是。视频和音频通信协议有自己的文字聊天等文件，私营部分和文件共享是标准的SSL通信，因此，我们有我们的设计是无限万全。此外，可选服务用户可以使用启用SSL的服务，所有的交流可以启用了SSL。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('60', 'zh', '期间并就如何长时间使用最低限度？', '没有，一切Gozaimasen限制。没有时间限制，以便合同可以得到的每月。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('61', 'zh', '这样做可以在外国？', '是的，可以。由于基于网络的视频会议系统，并尽可能使用互联网，数据可以提供给任何人参与。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('148', 'zh', '免费试用帐户连接H.323TV会议（N2MY 323），你也可以使用？', '原则上可给我，如果连接到系统的顶解礼联系H.323TV会议（N2MY 323），我们可以发出免费试用帐户可使用的。请联系客户支持。客户服务:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('15', 'zh', '白板或类似的材料粘贴？', 'BMP和JPEG，GIF和PNG，TIFF图像格式，以及Word，Excel和PowerPoint中，PDF格式，可粘贴到其他文件。 ※视频文件，PowerPoint动画功能不支持。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('16', 'zh', '身份管理是做什么的每个用户？', '编号登录ID的问题本质上是一个合同=每会议室可能有一个。您的总人数在100人在1000年的用户是没有人的问题。会议室座位10人，一个图像我想你会用你的房间。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('24', 'zh', '一个合同（1会议室）是每个可能的10间基地，10个基地或可以做更多的会议，可以吗？', '受众特征（功能只读）达50个将利用这些数据可以提供给基地参加视频会议。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('25', 'zh', '哪些功能，哪些对象？', '观众（的观众），是会议的参与者可以收听电视电话会议。观众参与者可以查看有关情况，我们通常是次会议的与会者。观众参加，与会者通常可以提供关于这个问题的看法已会议。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('26', 'zh', '我想一个人守在门外，身份证资料编号我不想密码会议。', '是的，可以。 “邀请功能”，观众可使用ID的其他与会者将能够教，而不是做一个视频会议和密码。 ※地址是需要收到邀请。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('27', 'zh', '用户ID，您没有加入可以吗？', '是的，可以。提前邀请功能的参与。 ※地址是需要收到邀请。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('28', 'zh', '另一次会议举行的同时我可以处理？', '不，不是。另外，因为合同可以是一个额外的会议室，联系方式或联系我们，请联系支持中心。支持中心:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('30', 'zh', '能力有限的白板上粘贴材料可用？', '每个文件，20分你可以最多上传。同样的，一个可以上传最多20页的文件。 2000万或超过20页的文件，请上传和分裂。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('31', 'zh', '你能分享你的电脑屏幕上运行的应用程序？', '是的，可以。然而，N2MY共享选项是必需的合同。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('32', 'zh', '复制并粘贴白板的内容，可以吗？', '不，这里的功能目前不支持。然而，“记录显示，”你仍然可以看到在会后的白板功能的内容。此外，在这次会谈是“文件柜”，也可以是另一个功能，发送和接收文件。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('34', 'zh', '同时很多人，你可以加入会议？', '该服务通常高达10的数据可以提供给人们参加会议的同时进行。最大的受众用户可以使用该功能50是一种可选功能，数据可以提供给人们参加会议的同时进行。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('35', 'zh', '请解释一下，视频会议和其他服务的区别。', '很高兴认识你，其他服务是很大的不同个人与以下几点。 -没有安装。是出来重新利用现有的，而无需安装特殊的软件互联网。不可知，多平台环境。在Windows，Mac也可提供。 · 323支持视频会议系统也启用。 ，移动电话。您可以参与手机的可视电话功能。 ，代理，防火墙的支持。由于企业局域网，并自动作出反应的困难设置设置是必需的。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('36', 'zh', '很高兴认识你所需的设备使用是什么？', '宽带接入，电脑，相机，耳机，立即可高兴见到你。你可以不参加的相机。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('39', 'zh', '如果服务器已下降到不支持什么样？', '分配到中心，即使服务器出现故障的情况下，我们的目的是不影响服务。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('41', 'zh', '我想引进前试用包装的会议室可以得到贷款，可以吗？', '为了您的贷款配套的会议室，请联系客户支持一次。客户服务:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('42', 'zh', '以前我已经购买了视频会议系统，也可以使用它？', '如果可用的H.323标准的视频会议系统。然而，H.323协议是一个可选的服务合同，您需要指定您的客户支持服务。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('43', 'zh', '免费试用帐户，手机连接（N2MY移动），您也可以使用？', '原则上，但给我的，手机连接（N2MY手机）的客户将愿意尝试，胃口好免费试用帐户，将与移动电话连接印发。客户服务:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('58', 'zh', '如果您使用的10个碱基的带宽要求？', '上行至256Kbps，是512kbps的下降。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('59', 'zh', '什么是一些帧速率？', '我们正以15帧。根据会议的参加人数是最好的帧速率自动。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('29', 'zh', '这样做可以在外国？', '是的，可以。由于基于网络的视频会议系统，并尽可能使用互联网，数据可以提供给任何人参与。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('78', 'zh', '研讨会举行的同时多我可以处理？', '不，不是。另外，因为合同可以是一个额外的会议室的联系方式或与我们联系，请联系客户支持中心。客户服务:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('44', 'zh', '多少是初始成本？', '一个是四万七千二百五十零日元统一规划（含税），将。这里所有的房费还包括设置费和其他费用。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('45', 'zh', '请告诉我收费结构的细节。', '每月10395（含税），我们提供的各种计划。收费是统一费率，我们使用它，不论数目制度，我们的房费。一个账户，一个网上会议室将是您的客房租金的形象。欲了解更多信息，请看到率计划的清单。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('46', 'zh', '当您加入后的利率调整计划，规定哪些事项包装呢？ （入学后，与前一个月，他计划改变）', '有没有特别的限制。对于任何在本月计划中的变化是可能的变化，将适用于从下个月。 （本月将在目前的计划预付费用户）将改变我们的每月第二天。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('47', 'zh', '终止后，如果恢复的初始成本是需要什么？', '是的，它会重新设定，初始成本会发生。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('48', 'zh', '房价是“每间房”，没有对用户的数量限制你有呢？', '有没有可用的用户数量限制。由于单位的会议室收费率也对许多人可保持不变。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('49', 'zh', '请告诉我在拥挤的会议室大约费用。', '因此，我们将引用的代表，请联系支持中心。支持中心:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('75', 'zh', '多少是初始成本？', '它取决于你的计划，请联系客户支持。客户服务:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('77', 'zh', '终止后，如果恢复的初始成本是需要什么？', '是的，我重新设定，初始成本会发生。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('149', 'zh', '请告诉我推荐的工作环境。', '请参阅要求页http://www.nice2meet.us/ja/requirements/', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('150', 'zh', '请告诉的基本要求', '请参阅要求页。 http://www.nice2meet.us/ja/requirements/', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('151', 'zh', '推荐宽带（高达128kbps的，或384kbps的下降）那里，如果低于这个生产线的速度，可能会发生什么危害呢？', '和语音延迟，延迟将发生视频。此外，“低速模式”，“语音优先模式”，“设定带宽使用”，如在使用线速度较低的设置根据环境或质量粗糙，你走了视频。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('50', 'zh', '防火墙（反向代理，和NAT）支持连接到或从？', '是。如果通过代理服务器连接，但是，代理类型，视频和音频可能会发生延迟。这是可能的解决方法，请与我们联系。客户服务:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('52', 'zh', '小灵通移动通信（AirH“手机卡等）有什么可以用呢？', '我们不是一个工作，可用于事情的保障。 “慢模式”，“语音优先模式”，“设置带宽使用”，因为根据环境还可以设置一个较低的线路速度。不过，小灵通环境（AirH“等）的情况下，在包处理128kbps的连接，并在实际上，只有64kbps的无在许多情况下减少的速度，大量的使用受困难的个案。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('54', 'zh', '你有视频和音频编解码器使用？', '视频编码和解码，闪存是通过我们的能力。编解码器用于在其索伦森媒体的Sorenson Spark编码功能。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('55', 'zh', '你什么协议呢？', '在Flash中使用的媒体服务器，Adobe公司的专有协议，我们使用的RTMP。 ※使用的RTMP是由Flash媒体服务器，Adobe的专有协议。欲了解更多信息，Adobe公司的网页（http://www.adobe.com/jp/products/flashmediaserver/）请。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('56', 'zh', '语音数据和视频通信，如登录ID，请让我知道这是什么安全的措施。', '所有通信比其他的视频流，我们使用SSL通信。视频流是做自己的通讯，安全协议规范的存在，但并没有被公布的一项可选功能，SSL的视频流是可能的。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('57', 'zh', '我可以使用相机？是否有推荐的相机？', '它可以使用USB摄像头。有关详细信息，请参阅相机的测试。 http://www.nice2meet.us/ja/tools/', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('51', 'zh', '推荐宽带（高达128kbps的，或384kbps的下降）那里，如果低于这个生产线的速度，可能会发生什么危害呢？', '和语音延迟，延迟将发生视频。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('20', 'zh', '没有领导的3G功能的移动电话', '“N2MY手机”是不导电的出站的通知。如果传入通知被配置为拒绝非移动电话，请允许进行更改设置。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('62', 'zh', '不进行测量生产线的速度', '线速度很慢，可能不会作出主题行或安全的。再次，胃口好行，以便我们能够得到再度入院，它可能解决问题。如果上述没有解决的方式，我们首选的环境（线，电脑规格），所以可能不符合，请查看我们首选的环境。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('63', 'zh', '屏幕显示错误的测量线速度（代理）', '线速度很慢，可能不会作出主题行或安全的。再次，胃口好行，以便我们能够得到再度入院，它可能解决问题。如果上述没有解决的方式，我们首选的环境（线，电脑规格），所以可能不符合，请查看我们首选的环境。客户服务:0570 - 00 - 2192', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('64', 'zh', '在会议期间的时间“，已被断开”，成为', '在我们的系统中，在一定时间内每次会议“已成功举行”我们是通信服务器。因此，如果您遇到重大延误，并可能会断开和误解的情况没有连接到服务器。会议还引安起Kosanaku的延迟和延迟的根本原因是相同的。 ※如果有环境和代理线，PC可能不能满足我们产品的规格要求。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('65', 'zh', '视频宽高比错误', '在使用视频设备，或方面的图像比例改变当您更改的图像采集的大小，可升上下黑条。请检查这些设备的性能和设置。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('71', 'zh', '声音很低，我该怎么办？', '不要降低音量或静音呢？ [控制面板]→[声音和音频设备]→[音量麦克风静音消除屏幕上，请设置音量高。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('72', 'zh', '和我的声音也有同感。什么是你吗？', '不要使用扬声器对方？ →使用。拿起麦克风的另一端你的声音。回声如果耳机可顶解礼丢失。如果令人信服的发言，另一边的使用将需要一个回声消除器麦克风。参考：ClearOne的ACCUMICII→与回音消除不使用麦克风。耳机是便宜和其它东西，可以找到自己的声音有点回来。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('73', 'zh', '很多麻烦听取其他噪音的声音', '1）是人使用耳机？ →在不使用。顶我请使用耳机。 →使用（到2）2）耳机麦克风或其他声音收集器类型的声音？ →是。可能出现的问题可能有一些其他计算机。请要求电脑重新启动。 →编号建议作为高性能耳机从目前的可用。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('74', 'zh', '甚至没有在页面上点击（他们被阻止弹出）', '“视窗XP服务包2”，可给您的，会弹出被阻止。浏览器的“工具”，从菜单“阻止弹出”→“弹出窗口阻止程序设置，选择”工具栏被选中，单击它。在“网站地址，让”』到『www.nice2meet.us，然后点“Add”按钮。 “关闭”按钮，关闭配置窗口。这样就完成了设置。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('5', 'zh', '很多麻烦听取其他噪音的声音', '1）是人使用耳机？ →在不使用。顶我请使用耳机。 →使用（到2）2）耳机麦克风或其他声音收集器类型的声音？ →是。可能出现的问题可能有一些其他计算机。请要求电脑重新启动。 →编号建议作为高性能耳机从目前的可用。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('79', 'zh', '迈克，你对此有何建议？', '我们建议使用的USB接口类型。和噪声类型比较容易脚插座。会议还提供了iSight和麦克风机构，容易嚎叫和回声。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('80', 'zh', '和其他工作，在会议上，我喜欢在会议进程缓慢的感觉？', '建议在前台使用。操作系统和浏览器的设置放慢的过程当屏幕转向会议的背景，请始终使用前景。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('86', 'zh', '声音很低，我该怎么办？', '不要降低音量或静音呢？系统首选项]→[见→硬件[声音选择→[输入]→[声音输入]→[静音]取消检查，请提高音量。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('87', 'zh', '和我的声音也有同感。什么是你吗？', '不要使用扬声器对方？ →使用。拿起麦克风的另一端你的声音。回声如果耳机可顶解礼丢失。如果令人信服的发言，另一边的使用将需要一个回声消除器麦克风。 →在不使用。耳机是便宜和其它东西，可以找到自己的声音有点回来。', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('66', 'zh', '严重延误', '', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('67', 'zh', '没有出现在我的视频', '', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('68', 'zh', '谁看不到视频', '', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('69', 'zh', '他们会接受他们的声音', '', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('70', 'zh', '触角伸向其他音频（没有声音）', '', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('81', 'zh', '严重延误', '', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('82', 'zh', '没有出现在我的视频', '', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('83', 'zh', '谁看不到视频', '', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('84', 'zh', '他们会接受他们的声音', '', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('85', 'zh', '触角伸向其他音频（没有声音）', '', '2009-10-26 10:33:00', '2009-10-26 10:33:00');
INSERT INTO `faq_data` VALUES ('5', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('15', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('16', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('20', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('23', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('24', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('25', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('26', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('27', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('28', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('29', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('30', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('31', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('32', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('33', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('34', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('35', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('36', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('37', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('38', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('39', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('40', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('41', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('42', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('43', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('44', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('45', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('46', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('47', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('48', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('49', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('50', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('51', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('52', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('53', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('54', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('55', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('56', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('57', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('58', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('59', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('60', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('61', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('62', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('63', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('64', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('65', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('66', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('67', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('68', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('69', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('70', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('71', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('72', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('73', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('74', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('75', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('77', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('78', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('79', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('80', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('81', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('82', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('83', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('84', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('85', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('86', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('87', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('148', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('149', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('150', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('151', 'ko', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('5', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('15', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('16', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('20', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('23', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('24', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('25', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('26', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('27', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('28', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('29', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('30', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('31', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('32', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('33', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('34', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('35', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('36', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('37', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('38', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('39', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('40', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('41', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('42', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('43', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('44', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('45', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('46', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('47', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('48', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('49', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('50', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('51', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('52', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('53', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('54', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('55', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('56', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('57', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('58', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('59', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('60', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('61', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('62', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('63', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('64', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('65', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('66', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('67', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('68', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('69', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('70', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('71', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('72', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('73', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('74', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('75', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('77', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('78', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('79', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('80', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('81', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('82', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('83', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('84', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('85', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('86', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('87', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('148', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('149', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('150', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('151', 'th', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('5', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('15', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('16', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('20', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('23', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('24', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('25', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('26', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('27', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('28', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('29', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('30', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('31', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('32', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('33', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('34', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('35', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('36', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('37', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('38', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('39', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('40', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('41', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('42', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('43', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('44', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('45', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('46', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('47', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('48', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('49', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('50', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('51', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('52', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('53', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('54', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('55', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('56', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('57', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('58', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('59', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('60', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('61', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('62', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('63', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('64', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('65', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('66', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('67', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('68', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('69', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('70', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('71', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('72', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('73', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('74', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('75', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('77', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('78', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('79', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('80', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('81', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('82', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('83', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('84', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('85', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('86', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('87', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('148', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('149', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('150', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('151', 'tl', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('5', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('15', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('16', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('20', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('23', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('24', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('25', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('26', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('27', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('28', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('29', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('30', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('31', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('32', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('33', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('34', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('35', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('36', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('37', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('38', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('39', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('40', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('41', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('42', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('43', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('44', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('45', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('46', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('47', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('48', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('49', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('50', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('51', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('52', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('53', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('54', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('55', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('56', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('57', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('58', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('59', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('60', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('61', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('62', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('63', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('64', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('65', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('66', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('67', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('68', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('69', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('70', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('71', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('72', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('73', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('74', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('75', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('77', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('78', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('79', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('80', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('81', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('82', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('83', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('84', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('85', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('86', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('87', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('148', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('149', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('150', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('151', 'vi', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('5', 'zh-CN', '噪音太大，我听不到其他人的声音。', '1）是人使用耳机？ →在不使用。顶我请使用耳机。 →使用（到2）2）耳机麦克风或其他声音收集器类型的声音？ →是。可能出现的问题可能有一些其他计算机。请要求电脑重新启动。 →编号建议作为高性能耳机从目前的可用。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('15', 'zh-CN', '什么类型的文档可以上传到白板?', 'BMP和JPEG，GIF和PNG，TIFF图像格式，以及Word，Excel和PowerPoint中，PDF格式，可粘贴到其他文件。 ※视频文件，PowerPoint动画功能不支持。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('16', 'zh-CN', '一使用契约可以获得多少 ID 和密码?', '编号登录ID的问题本质上是一个合同=每会议室可能有一个。您的总人数在100人在1000年的用户是没有人的问题。会议室座位10人，一个图像我想你会用你的房间。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('20', 'zh-CN', '无法连接3G 对应手机', '“N2MY手机”是不导电的出站的通知。如果传入通知被配置为拒绝非移动电话，请允许进行更改设置。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('23', 'zh-CN', '在购买服务之前是否可以试用?', '是，您可以在线注册申请试用帐户。试用期限为2周。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('24', 'zh-CN', '能否同时连接10 个以上的地点?', '是，使用可选的“受众”功能，最多可以同时连接 49 个用户。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('25', 'zh-CN', '什么是受众功能?', '受众参与者是能以旁听身份参加视频会议的用户。当一般用户举行视频会议时，受众参与者可进行旁观。会议中，受众参与者也可以以一般用户的身份参加会议，例如，发表意见。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('26', 'zh-CN', '如何在不会泄露 ID 和密码的情况下，和非用户进行会议呢?', '是的，可以。 “邀请功能”，观众可使用ID的其他与会者将能够教，而不是做一个视频会议和密码。 ※地址是需要收到邀请。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('27', 'zh-CN', '没有 ID 也可以参加会议?', '是的，可以。提前邀请功能的参与。 ※地址是需要收到邀请。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('28', 'zh-CN', '能否使用一个帐户 ID 和密码同时举行多个会议吗?', '不，不是。另外，因为合同可以是一个额外的会议室，联系方式或联系我们，请联系支持中心。支持中心:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('29', 'zh-CN', '在国外可以使用吗?', '可以，因为是以网络为平台的视频会议系统，所以只要能上网，就能够使用。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('30', 'zh-CN', '白板上传文档的上限是多少?', '每个文件的上传上限为 20 MB或 20 页。超过 20 MB 或 20页以上的文件需在上传之前应进行分割。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('31', 'zh-CN', '能否与其他用户共享我的 PC 上运行的文件和应用程序?', '是的，可以。然而，N2MY共享选项是必需的合同。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('32', 'zh-CN', '能否将白板的内容复制到剪贴板或反之亦然?', '不，这里的功能目前不支持。然而，“记录显示，”你仍然可以看到在会后的白板功能的内容。此外，在这次会谈是“文件柜”，也可以是另一个功能，发送和接收文件。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('33', 'zh-CN', '忘记 ID 和密码应该怎么办?', '请稍候，请与支持中心。支持中心:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('34', 'zh-CN', '多少人可同时参加会议?', '该服务通常高达10的数据可以提供给人们参加会议的同时进行。最大的受众用户可以使用该功能50是一种可选功能，数据可以提供给人们参加会议的同时进行。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('35', 'zh-CN', '告诉我 V-cube 会议系统相对于其他服务的优势。', '很高兴认识你，其他服务是很大的不同个人与以下几点。 -没有安装。是出来重新利用现有的，而无需安装特殊的软件互联网。不可知，多平台环境。在Windows，Mac也可提供。 · 323支持视频会议系统也启用。 ，移动电话。您可以参与手机的可视电话功能。 ，代理，防火墙的支持。由于企业局域网，并自动作出反应的困难设置设置是必需的。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('36', 'zh-CN', '使用 V-cube 会议系统是否需要任何特殊设备?', '您只需要宽带环境、电脑、摄像头和耳机麦克风。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('37', 'zh-CN', '我能否在 Mac 上使用 nice to meet you 网络视频会议工具?', '可以，但是“桌面共享”和“Checker”功能不支持Mac。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('38', 'zh-CN', '我能否在 Intranet 中设置 nice to meet you 网络视频会议工具?', '感谢您购买服务器给我们如何在Intranet上运行你。关于成本和进度，请与支持中心。支持中心:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('39', 'zh-CN', '如果服务器停机会有什么影响? 你们会如何解决?', '我们的服务器分散在多个IDC，就算某个服务器停机也可以确保 24/7 服务，不受影响。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('40', 'zh-CN', '我能否租用摄像头?', '的原则，但不租，我们可以有快乐的贷款应与顶解礼。支持中心:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('41', 'zh-CN', '我能否在购买会议室配套系统之前试用?', '为了您的贷款配套的会议室，请联系客户支持一次。客户服务:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('42', 'zh-CN', '我能否将 V-cube 会议系统连接到我当前使用的网络视频会议系统?', '如果可用的H.323标准的视频会议系统。然而，H.323协议是一个可选的服务合同，您需要指定您的客户支持服务。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('43', 'zh-CN', '我能否通过免费试用帐户试用 N2MY 手机服务?', '原则上，但给我的，手机连接（N2MY手机）的客户将愿意尝试，胃口好免费试用帐户，将与移动电话连接印发。客户服务:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('44', 'zh-CN', '是否有初始费用?', '可以，不论哪个服务，激活费用一律为 45,000 日圆。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('45', 'zh-CN', '请告诉我定价详细信息。', '每月10395（含税），我们提供的各种计划。收费是统一费率，我们使用它，不论数目制度，我们的房费。一个账户，一个网上会议室将是您的客房租金的形象。欲了解更多信息，请看到率计划的清单。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('46', 'zh-CN', '当您加入后的利率调整计划，规定哪些事项包装呢？ （入学后，与前一个月，他计划改变）', '您可以在月中更改所签订的服务协议（套餐）。 但是，更改将在下个月的第一天生效。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('47', 'zh-CN', '在我关闭帐户之后，我是否需要再次支付初始费用才能重新启动该服务?', '初始设置费用为 45,000 日圆。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('48', 'zh-CN', '会议室中的用户数是否有限制?', '这将取决于您所签订的服务协议（套餐）。 请与我们的客户中心联系，了解每项服务协议计划的限制。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('49', 'zh-CN', '告诉我关于会议配套系统的信息。', '因此，我们将引用的代表，请联系支持中心。支持中心:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('50', 'zh-CN', '如果我的 PC 在防火墙内，我能否使用 V-cube 的服务(反向代理或 NAT)?', '是。如果通过代理服务器连接，但是，代理类型，视频和音频可能会发生延迟。这是可能的解决方法，请与我们联系。客户服务:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('51', 'zh-CN', '建议使用宽带(上传最少为 128 kbps，下载最少为 384 kbps)，但如果连接速度较慢，可能发生哪些问题?', '将会发生声音和图像延迟。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('52', 'zh-CN', '我能否通过 PHS 手机使用 nice to meet you 网络视频会议工具? (AirH 或 P-in)', '我们不是一个工作，可用于事情的保障。 “慢模式”，“语音优先模式”，“设置带宽使用”，因为根据环境还可以设置一个较低的线路速度。不过，小灵通环境（AirH“等）的情况下，在包处理128kbps的连接，并在实际上，只有64kbps的无在许多情况下减少的速度，大量的使用受困难的个案。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('53', 'zh-CN', '安全性完善吗？', '是的。视频和语音通过非公开的独立协议进行传输，文本聊天和文件共享等文档部分以标准SSL进行传输，因此在安全方面万无一失。此外，因为使用支持SSL的选项服务，所以全部传输也可以进行SSL化。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('54', 'zh-CN', '使用哪种视频/视频编解码器?', '视频编码和解码，闪存是通过我们的能力。编解码器用于在其索伦森媒体的Sorenson Spark编码功能。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('55', 'zh-CN', '使用哪种通信协议和端口?', '在Flash中使用的媒体服务器，Adobe公司的专有协议，我们使用的RTMP。 ※使用的RTMP是由Flash媒体服务器，Adobe的专有协议。欲了解更多信息，Adobe公司的网页（http://www.adobe.com/jp/products/flashmediaserver/）请。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('56', 'zh-CN', '登录 ID、视频、音频和数据通信使用哪种安全保护?', '所有通信比其他的视频流，我们使用SSL通信。视频流是做自己的通讯，安全协议规范的存在，但并没有被公布的一项可选功能，SSL的视频流是可能的。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('57', 'zh-CN', '您推荐哪种摄像头?', '它可以使用USB摄像头。有关详细信息，请参阅相机的测试。 http://www.nice2meet.us/ja/tools/', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('58', 'zh-CN', '同时连接10个地点举行会议时，我需要多少带宽?', '您至少需要 256kbps 上游速度和 512kbps 下游速度。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('59', 'zh-CN', 'nice to meet you 网络视频会议工具所需的帧速率是多少?', '我们正以15帧。根据会议的参加人数是最好的帧速率自动。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('60', 'zh-CN', '是否有最短使用契约期限?', '没有，一切Gozaimasen限制。没有时间限制，以便合同可以得到的每月。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('61', 'zh-CN', '在国外也可以使用吗?', '可以，因为是以网络为平台的视频会议系统，所以只要能上网，就能够使用。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('62', 'zh-CN', '由于会议系统在测量连接速度时停止，我无法进入会议室。', '线速度很慢，可能不会作出主题行或安全的。再次，胃口好行，以便我们能够得到再度入院，它可能解决问题。如果上述没有解决的方式，我们首选的环境（线，电脑规格），所以可能不符合，请查看我们首选的环境。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('63', 'zh-CN', '测量 PC 连接速度时出现错误消息。 (代理服务器)', '线速度很慢，可能不会作出主题行或安全的。再次，胃口好行，以便我们能够得到再度入院，它可能解决问题。如果上述没有解决的方式，我们首选的环境（线，电脑规格），所以可能不符合，请查看我们首选的环境。客户服务:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('64', 'zh-CN', '每次使用时，会议都会被中断。', '在我们的系统中，在一定时间内每次会议“已成功举行”我们是通信服务器。因此，如果您遇到重大延误，并可能会断开和误解的情况没有连接到服务器。会议还引安起Kosanaku的延迟和延迟的根本原因是相同的。 ※如果有环境和代理线，PC可能不能满足我们产品的规格要求。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('65', 'zh-CN', '视频的长宽比似乎错误。', '根据您使用的视频设备，更改视频捕获大小可能会导致长宽比发生变化，并可能导致在屏幕的顶部和底部出现黑边。请检查您的设备性能和设置。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('66', 'zh-CN', '延迟很长。', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('67', 'zh-CN', '看不到自己的视频。', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('68', 'zh-CN', '看不到对方的视频。', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('69', 'zh-CN', '其他参与者听不到我的声音。', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('70', 'zh-CN', '我听不到其他参与者的声音。', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('71', 'zh-CN', '如果音量很低，我应该怎么办?', '不要降低音量或静音呢？ [控制面板]→[声音和音频设备]→[音量麦克风静音消除屏幕上，请设置音量高。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('72', 'zh-CN', '我的声音有回音。 我如何消除回音?', '不要使用扬声器对方？ →使用。拿起麦克风的另一端你的声音。回声如果耳机可顶解礼丢失。如果令人信服的发言，另一边的使用将需要一个回声消除器麦克风。参考：ClearOne的ACCUMICII→与回音消除不使用麦克风。耳机是便宜和其它东西，可以找到自己的声音有点回来。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('73', 'zh-CN', '噪音太大，我听不到其他人的声音。', '1）是人使用耳机？ →在不使用。顶我请使用耳机。 →使用（到2）2）耳机麦克风或其他声音收集器类型的声音？ →是。可能出现的问题可能有一些其他计算机。请要求电脑重新启动。 →编号建议作为高性能耳机从目前的可用。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('74', 'zh-CN', '无法显示页面。 (已阻止弹出窗口。)', '“视窗XP服务包2”，可给您的，会弹出被阻止。浏览器的“工具”，从菜单“阻止弹出”→“弹出窗口阻止程序设置，选择”工具栏被选中，单击它。在“网站地址，让”』到『www.nice2meet.us，然后点“Add”按钮。 “关闭”按钮，关闭配置窗口。这样就完成了设置。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('75', 'zh-CN', '是否有初始费用?', '它取决于你的计划，请联系客户支持。客户服务:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('77', 'zh-CN', '在我关闭帐户之后，我是否需要再次支付初始费用才能重新启动该服务?', '是，初始设置费用为 45,000 日圆。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('78', 'zh-CN', '我能否在签订一使用契约的情况下同时举行多个研讨会?', '不，不是。另外，因为合同可以是一个额外的会议室的联系方式或与我们联系，请联系客户支持中心。客户服务:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('79', 'zh-CN', '我可以使用哪个麦克风? 您推荐哪一个?', '我们建议使用的USB接口类型。和噪声类型比较容易脚插座。会议还提供了iSight和麦克风机构，容易嚎叫和回声。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('80', 'zh-CN', '建议在前端使用我们的系统。 由于 Mac 上的处理顺序是从前到后，如果会议窗口不在最前端，可能出现延迟。', '建议在前台使用。操作系统和浏览器的设置放慢的过程当屏幕转向会议的背景，请始终使用前景。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('81', 'zh-CN', '延迟很长。', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('82', 'zh-CN', '看不到自己的视频。', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('83', 'zh-CN', '看不到对方的视频。', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('84', 'zh-CN', '其他参与者听不到我的声音。', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('85', 'zh-CN', '我听不到其他参与者的声音。', null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('86', 'zh-CN', '如果音量很低，我应该怎么办?', '不要降低音量或静音呢？系统首选项]→[见→硬件[声音选择→[输入]→[声音输入]→[静音]取消检查，请提高音量。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('87', 'zh-CN', '我的声音有回音。 我如何消除回音?', '不要使用扬声器对方？ →使用。拿起麦克风的另一端你的声音。回声如果耳机可顶解礼丢失。如果令人信服的发言，另一边的使用将需要一个回声消除器麦克风。 →在不使用。耳机是便宜和其它东西，可以找到自己的声音有点回来。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('148', 'zh-CN', '我能否通过免费试用帐户连接(N2MYH.323)到 H.323TV 会议系统?', '原则上可给我，如果连接到系统的顶解礼联系H.323TV会议（N2MY 323），我们可以发出免费试用帐户可使用的。请联系客户支持。客户服务:0570 - 00 - 2192', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('149', 'zh-CN', '推荐系统要求是什么?', '请参阅要求页http://www.nice2meet.us/ja/requirements/', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('150', 'zh-CN', '最低系统要求是什么?', '请参阅要求页。 http://www.nice2meet.us/ja/requirements/', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('151', 'zh-CN', '建议使用宽带(上传最少为 128 kbps，下载最少为 384 kbps)，但如果连接速度较慢，可能发生哪些问题?', '和语音延迟，延迟将发生视频。此外，“低速模式”，“语音优先模式”，“设定带宽使用”，如在使用线速度较低的设置根据环境或质量粗糙，你走了视频。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('5', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('15', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('16', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('20', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('23', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('24', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('25', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('26', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('27', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('28', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('29', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('30', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('31', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('32', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('33', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('34', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('35', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('36', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('37', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('38', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('39', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('40', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('41', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('42', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('43', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('44', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('45', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('46', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('47', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('48', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('49', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('50', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('51', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('52', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('53', 'zh-TW', '安全性完善嗎？', '是的。視頻和語音通過非公開的獨立協定進行傳輸，文本聊天和檔共用等文檔部分以標準SSL進行傳輸，因此在安全方面萬無一失。此外，因為使用支援SSL的選項服務，所以全部傳輸也可以進行SSL化。', '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('54', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('55', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('56', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('57', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('58', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('59', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('60', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('61', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('62', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('63', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('64', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('65', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('66', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('67', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('68', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('69', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('70', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('71', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('72', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('73', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('74', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('75', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('77', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('78', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('79', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('80', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('81', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('82', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('83', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('84', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('85', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('86', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('87', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('148', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('149', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('150', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('151', 'zh-TW', null, null, '2010-01-18 22:04:05', '2010-01-18 22:04:05');
INSERT INTO `faq_data` VALUES ('5', 'en_US', 'I can not hear the others’ voices with so much noise.', '1) Is there a speaker or a headphone connected to your Mac?\n→No. Connect a speaker or a headphone to your Mac. \n→Yes. (Go to 2) \n2) Is the speaker or headphone mute set up? \n→Yes. Click on the Apple Menu and select System Preferences>Sound (in Hardware)>Output. Uncheck the mute box and turn on the volume. \n→No. (Go to 3) \n3) Is the volume on the computer off?\n→Yes. Turn up the volume. (Be sure to turn up the computer volume.) \n→No. The set up of other users might not be normal. Ask them to check. (Refer to above \"Other participants cannot hear my voices\")', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('15', 'en_US', '', '', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('16', 'en_US', 'How many IDs and passwords do I get per contract?', 'One ID and password is issued per contract. You may share the ID and password with as many users as you’d like. It may be easier for you to think of it this way; you are renting a ‘virtual room’ and you have the right to allow anyone to use it.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('20', 'en_US', null, null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('23', 'en_US', 'Is it possible to try the service before purchasing it?', 'Yes, you can sign up online for a 2 week trial account.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('24', 'en_US', 'Is it possible to connect more than 10 locations?', 'Yes, up to 49 locations can be connected simultaneously by using the optional “Audience” feature.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('25', 'en_US', '', null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('26', 'en_US', 'How can I start my meeting without disclosing my ID & password?', 'By using the ‘INVITATION’ feature you can e-mail an invitation with an automatically generated hyperlink to all your guests. This will allow your guests to have ‘one-click access’ into the room. Not only can you secure your ID & password, but it’s hassle free for your guests.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('27', 'en_US', 'Is it possible to participate in a meeting without an ID?', 'Yes, it is possible to participate in a meeting using a hyperlink sent to you by ‘e-mail invitation’. *An e-mail address is required to receive the invitation.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('28', 'en_US', 'Can I have several meetings simultaneously with one account ID & password?', 'No, you can only have one meeting at a time per account ID & password. Basically, the concept is that you are renting one ‘room’, therefore, you can only have one meeting at a time. Please, contact the support center if you would like to create another ‘room’ or account ID & password.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('29', 'en_US', null, null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('30', 'en_US', '', null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('31', 'en_US', 'Can I share files and applications running on my PC?', 'Yes, you can activate the desktop sharing feature and show the documents and applications running on your desktop. This feature will also enable people to take control of your PC. Sharing is an optional feature.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('32', 'en_US', 'Can I copy the contents of the whiteboard to a clipboard or vice versa?', 'No. There is only a File/picture upload on the whiteboard feature.\nYou can display files such as Word, Excel, PowerPoint, PDF, BMP, PNG, JPEG, and GIF on the whiteboard.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('33', 'en_US', 'What should I do if I forget my ID and password?', 'Contact us by phone (0570-00-2192) or e-mail. We can send you the information once we confirm your identity.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('34', 'en_US', 'How many people can participate in a meeting at the same time?', 'You can have up to 10 people attend a meeting with video and voice. If you use the ‘Audience’ feature you can have up to 49 locations connected at the same time.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('35', 'en_US', 'Tell me the advantages of using V-CUBE meeting over other services.', 'There are 3 big differences: \n1. No need for installation\n2. Cross platform, you can use it on a Macintosh as well as on Windows PC. It can also connect to H.323 compliant terminals.\n3. Bypass proxy and firewall. No need for any changes in settings for company LAN.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('36', 'en_US', 'Do I need any special equipment for V-CUBE Meeting?', 'All you need is broadband internet connection, computer, web camera and a headset.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('37', 'en_US', 'Can I use V-CUBE Meeting on a Mac?', 'Yes, but you can not use the ‘Sharing’ and ‘Checker’ features on a Mac.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('38', 'en_US', 'Can I setup V-CUBE Meeting within an intranet?', 'Yes, you can purchase a server to run V-CUBE Meeting on your intranet for added security. Please contact our sales team for more details. (0570-00-2192)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('39', 'en_US', 'What happens if a server goes down, how do you respond to it?', 'In the event that a server goes down, we have backup servers in multiple locations that will keep our service running 24/7.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('40', 'en_US', 'Can I rent a web camera?', 'Normally we do not offer rental of web cameras, but we can make exceptions. Please ask our sales team for more information. (0570-00-2192)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('41', 'en_US', 'Can I try the conference room package before purchasing it?', 'Normally we do not offer a free trial for our conference room package, but we have made exceptions in the past with money back guarantee deposit. Please ask our sales team for more information. (0570-00-2192)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('42', 'en_US', 'Can I connect V-CUBE Meeting to the web conferencing system I currently have?', 'If your current system uses an H.323 compliant terminal, the answer is yes. But you must sign up for the H.323 optional service. Please contact our support team for more details.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('43', 'en_US', 'Can I try the V-CUBE mobile service with a free trial account?', 'Normally we do not offer a free trial for mobile, but we can make exceptions. Please ask our sales team for more information. (0570-00-2192)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('44', 'en_US', 'Is there an initial cost?', 'Yes, there is a one-time activation fee of 45,000 Yen for any plan.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('45', 'en_US', 'Please tell me the details about your pricing.', 'We have several plans starting from 9,900 Yen (tax included) per month. It is a flat fee no matter how many participants there are. It is like renting a virtual conference room online with an account and you are free to use it as you like. See pricing details for more info.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('46', 'en_US', 'Can I change the service plan? Can I change the plan within one month?', 'You can change the service plan in the middle of the month. However, the change will be effective until the first day of the following month.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('47', 'en_US', 'Do I have to pay the initial cost again to re-start the service once I close an account?', 'Yes, we charge the initial setup cost of 45,000 Yen.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('48', 'en_US', 'Is there a limitation on the number of users in a room?', 'This will depend on the type of price plan you purchased. Please contact our support team to find out what the limitations are on each plan.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('49', 'en_US', 'Tell me about conferencing package.', 'Please ask our support team to get an estimate. (0570-00-2192)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('50', 'en_US', 'Can I use V-cube’s service even if my PC is inside a firewall (reverse proxy or NAT)?', 'Yes, our service was designed to bypass firewalls. However, if you are connecting via a proxy server you may experience slower connection speeds. Such delay can be avoided by opening port 1935 outward. For more details please contact our support team.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('51', 'en_US', null, null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('52', 'en_US', 'Can I use V-CUBE Meeting with PHS mobile? (AirH or P-in)', 'It is not guaranteed by company, but you can use it with PHS mobile. It would be difficult to use it because of the network speed is usually lower than 64kbps.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('53', 'en_US', 'How secure is V-cube?', 'V-cube application is highly secure.  V-cube uses the original protocol for video and audio, and SSL (Secured Socket Layer)for documents such as text chat and file sharing. It is optional to have SSL for all communication.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('54', 'en_US', 'What kind of video/voice codec is used?', 'Flash is used for video encode/decode. The codec used in the feature is the Sorenson Spark codec made by Sorenson Media.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('55', 'en_US', 'What kind of protocols and ports are used?', 'RTMP is Macromedia\'s original protocol used in Flash Communication Server. For more details, please refer to the Macromedia website.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('56', 'en_US', 'What kind of security is used for login-ID, video, voice and data communication?', 'SSL is used for all the communication except video and voice. Video and voice are secured by our original protocol, which is proprietary. V-CUBE SSL is an optional feature makes it possible to make all communication more secure.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('57', 'en_US', 'Which web cameras do you recommend?', 'Our system is easiest to use with a standard USB web cam.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('58', 'en_US', 'How much bandwidth do I need for a conference between 10 locations?', 'You need at least 256kbps upstream and 512kbps downstream.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('59', 'en_US', 'How many frame rates does V-CUBE Meeting get?', 'It automatically changes depending on the number of participants connected. But the maximum frame rate you can get is 15fps.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('60', 'en_US', 'Is there a minimum contract term?', 'No, there is no minimum contract term. It is on a month-to-month basis.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('61', 'en_US', '', null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('62', 'en_US', 'I can not go into the meeting room because the program stops where it measures the connection speed.', 'This may occur because the internet connection speed is too slow or the performance of the PC is slow. Please try logging in again. If it occurs again, the PC may not satisfy minimum system requirements (internet connection or specification). Please refer to our requirements for more details.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('63', 'en_US', 'I see an error message when the PC connection speed is measured. （Proxy）', 'You might not be able to use this service in your network environment. There are a few possible reasons:\n1. Your network’s proxy server is old.\n2. The security setting of the network is too high.\nTo resolve this please ask our support team. We can suggest the best way to accommodate your network environment.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('64', 'en_US', 'I am disconnected during the meeting each time I use it.', 'The system regularly reports to the server that there is a meeting taking place. If the server does not get the report because of the delay, it sometimes disconnects. The basic reason is delay.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('65', 'en_US', null, null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('66', 'en_US', 'There is a huge delay.', '1) Check the environment of your PC using Checker. \n※Refer to Checker Manual for seeing how to use Checker.\n2) How is your PC Connected?\n→ISDN　or　Dial up\nThe speed of your PC connection is less than what we recommend.\n→ADSL／DSL／T-1（Go to 3）\n3)Is your connection through a proxy?\n→Yes. It is through a proxy.\nIf you are connecting via a proxy server you may experience slower connection speeds. Such a delay can be avoided by opening port 1935 outward. Please talk to your IT department.\n→No. It is not through a proxy. （Go to 4）\n4)Does your PC have a processor newer than a Pentium III 533 MHz?\n→No.\nYour PC is not fast enough. Your must have a PC that meets or exceeds our minimum system requirements.\n→Yes.（Go to 5）\n5) Are other applications open?\n→Yes.\nClose other applications, the system may be out of resources. \n→No. \nIt seems that the video tip is the reason for the delay. Your PC cannot see video images because it is too old or it is a notebook. Please try using another PC.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('67', 'en_US', 'My picture cannot be seen.', '1) Check the environment of your PC using Checker. \n※Refer to Checker Manual for seeing how to use Checker. \n2) Is there a web camera connected?\n→No. \nConnect USB camera or IEEE 1934 camera.\n→Yes. （Go to 3） \n3) Does your OS recognize the camera?\n→No.\nMake your OS recognize the camera by installing the driver.\n→Yes. （Go to 4） \n4) Can you see your picture when you click the “camera switch button” which is located under the frame where your picture is supposed to be?\n→Yes.\nSometimes you have to click the camera switch button to display your picture.\n→No. （Go to 5） \n5)Did you click “approve” on the small window that pops up after clicking the meeting start button?\n→No.\nClose the meeting window and start it again.\n→Yes. （Go to 6） \n6)Are other applications for the camera open?\n→Yes.\nClose the application for camera and click the meeting start button again.\n→No.\nThe camera might not be compatible, or it may be broken. Try restarting your PC, if that does not work, contact the camera maker’s technical support.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('68', 'en_US', 'I cannot see other participants’ pictures.', '1) Check the environment of other participants’ PCs using Checker. \n※Refer to Checker Manual for seeing how to use Checker.\n2) Are they logged in? \n→No. Ask them to login. \n→Yes. （Go to 3） \n3) Can they see their own pictures on their PCs? \n→ No. they should set it up so they can see their pictures on their own PCs. (Refer to above “Self picture can not be seen”.) \n→ Yes. （Go to 4） \n4) Can you see their names? \n→No. There might be some systematic trouble. Try starting the meeting again. \n→Yes. （Go to 5） \n5) Can you see the text you type on the chat window?\n→No. You might not be connected. Try starting the meeting again.\n→Yes. Other participants might not be connected. Try starting the meeting again. (Refer to above “Self picture cannot be seen”.)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('69', 'en_US', 'Other participants cannot hear my voice.', '1) Check the environment of your PC using Checker. \n※Refer to Checker Manual for seeing how to use Checker. \n2) Is a microphone connected to your PC? \n→No. Connect a microphone to your PC. \n→Yes. （Go to 3） \n3) Did you click “approve” on the small window which pops up after clicking the meeting start button? \n→No. Close the meeting window and start it again. You have to click “approve” on the small window. \n→Yes. （Go to 4） \n4) Is the microphone mute is set up? \n→Yes. Uncheck the microphone mute and set the volume high on the Sounds and Audio Devices section of the control panel. \n→No. （Go to 5） \n5)Go to control panel.>Find the sounds and audio devices.> Audio>Recording>Click the Volume>Is the box of microphone checked? \n→No. Check the box of the microphone. \n→Yes. （Go to 6） \n6)Can others hear you speaking when you click the microphone switch button? \n→Yes. Sometimes you have to click the microphone switch button, especially when there are a few microphones connected to a PC or because of the state of the OS. \n→No. The microphone might not be compatible, or it might be broken. Try restarting you PC. If that does not work, contact the camera maker’s technical support.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('70', 'en_US', 'I cannot hear the other participants’ voices.', '1) Check the environment of your PC using Checker. \n※Refer to Checker Manual for seeing how to use Checker. \n2) Is either a speaker or a headphone connected to your PC?\n→No. Connect either a speaker or a headphone to your PC. \n→Yes. (Go to 3)\n3) Is the speaker or headphone mute set up? \n→Yes. Uncheck the microphone mute and set the volume to high on the Sounds and Audio Devices section of control panel. \n→No. (Go to 4) \n4) Is the volume of the PC off?\n→Yes. Turn up the volume. (Be sure to turn up the PC volume.) \n→No. The set up of other users might not be normal. Ask them to check. (Refer to above \"Other participants cannot hear my voices\")', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('71', 'en_US', 'What should I do if the volume is low?', 'You might have muted or turned down the volume. Uncheck the microphone mute and set the volume to high on the Sounds and Audio Devices section of the control panel.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('72', 'en_US', 'There is an echo to my voice. How can I make it go away?', 'Is the person you are speaking with using speakers?\n→Yes. The microphone catches your voice through the speakers. The echo goes away if the person uses a headset. The person needs an echo cancellation microphone if the speakers are used.\nReference: Echo cancellation microphone \"ACCUMICII\" by ClearOne. \n→No. If the headset is cheap, an echo sometimes happens.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('73', 'en_US', 'I can not hear the others’ voices with so much noise.', '1)Do they use a headset? \n→No. Ask them to use a headset. \n→yes. (Go to 2) \n2)Is their microphone a directional microphone?\n→Yes. There might be some problems with their PCs. Ask them to restart their PCs and login again. \n→No. We recommend you use a better quality headset.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('74', 'en_US', 'A page cannot be shown. (Pop-up window is blocked.)', 'If you are using “Windows XP Service Pack 2”, pop-ups are blocked. On your browser click ‘Tools‘> Go to ‘Pop-up Blocker’> ‘Pop-up Blocker Settings’. In the box labeled \"Address of Web site to allow\", type \" www.nice2meet.us \" (without quotation marks) Click the ‘Add’ button to add it to the allowed list. Click the ‘Close’ button to save your settings.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('75', 'en_US', 'Is there an initial cost?', null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('77', 'en_US', 'Do I have to pay the initial cost again to re-start the service once I close an account?', 'Yes, we charge the initial setup cost of 45,000 Yen.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('78', 'en_US', null, null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('79', 'en_US', 'Which microphone can I use? Which do you recommend?', 'We recommend using USB microphones. (Normally there are no sound input terminals. If there are, they cannot be used in most cases.) You may experience howling or echo with iSight or computer microphone.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('80', 'en_US', 'We recommend using our system in the front. There may be delay when the window goes to the background since the processing order is from front to back on a Mac.', 'We recommend using our system in the front. There may be delay when the window goes to the background since the processing order is from front to back on a Mac.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('81', 'en_US', 'There is a huge delay.', '1) How is your PC Connected?\n→ISDN　or　Dial up\nThe speed of your PC connection is less than what we recommend.\n→ADSL／DSL／T-1（Go to 2）\n2)Is your connection through a proxy?\n→Yes. It is through a proxy. If you are connecting via a proxy server you may experience slower connection speeds. Such a delay can be avoided by opening port 1935 outward. Please talk to your IT department.\n→No. It is not through a proxy. （Go to 3）\n3)Does your Mac meet our system requirements? \n→No. Please use a computer that meets our system requirements. \n→Yes. (Go to 4) \n4) Are other applications open?\n→Yes. Close other applications, the system may be out of resources. \n→No. It seems that the video tip is the reason for the delay. Your PC cannot see video images because it is too old or it is a notebook. Please try using another PC. (Go to 5) \n5) The window is not in the front. There may be delay when the window goes to the background since the processing order is from front to back on a Mac.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('82', 'en_US', 'My picture cannot be seen.', '1) Is a web camera connected?\n→No. Connect USB camera or IEEE 1934 camera.\n→Yes. （Go to 2）\n2) Does your OS recognize the camera?\n→No.\nMake your OS recognize the camera by installing the driver.\n→Yes. （Go to 3） \n3) Can you see your picture when you click the “camera switch button” which is located under the frame where your picture is supposed to be?\n→Yes. Sometimes you have to click the camera switch button to display your picture. *You have to click the button on Mac with USB camera since Mac has its own camera driver.\n→No. （Go to 4） \n4)Did you click “approve” on the small window that pops up after clicking the meeting start button?\n→No. Close the meeting window and start it again.\n→Yes. （Go to 5） \n5)Are other applications for the camera open?\n→Yes. Close the application for camera and click the meeting start button again.\n→No.\nThe camera might not be compatible, or it may be broken. Try restarting your PC, if that does not work, contact the camera maker’s technical support.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('83', 'en_US', 'I cannot see other participants’ pictures.', '1) Are they logged in? \n→No. Ask them to login. \n→Yes. （Go to 2） \n2) Can they see their own pictures on their PCs? \n→ No. They should set it up so they can see their pictures on their own PCs. (Refer to above “Self picture can not be seen”.) \n→ Yes. （Go to 3） \n3) Can you see their names? \n→No. There might be some systematic trouble. Try starting the meeting again. \n→Yes. （Go to 4） \n4) Can you see the text you type on the chat window?\n→No. You might not be connected. Try starting the meeting again.\n→Yes. Other participants might not be connected. Try starting the meeting again. (Refer to above “Self picture cannot be seen”.)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('84', 'en_US', 'Other participants cannot hear my voice.', '1) Is a microphone connected to your Mac? \n→No. Connect a microphone to your Mac. \n→Yes. （Go to 2） \n2) Did you click “approve” on the small window which pops up after clicking the meeting start button? →No. Close the meeting window and start it again. You have to click “approve” on the small window. \n→Yes. （Go to 3） \n3) Is the microphone mute is set up? \n→Yes. Click on the Apple Menu and select System Preferences>Sound (in Hardware)>Output. Uncheck the mute box and turn on the volume.\n→No. （Go to 4） \n4) Can others hear you speaking when you click the microphone switch button? \n→Yes. Sometimes you have to click the microphone switch button, especially when there are a few microphones connected to a PC or because of the state of the OS. \n→No. The microphone might not be compatible, or it might be broken. Try restarting you Mac. If that does not work, contact the camera maker’s technical support.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('85', 'en_US', 'I cannot hear the other participants’ voices.', '1) Is there a speaker or a headphone connected to your Mac?\n→No. Connect a speaker or a headphone to your Mac. \n→Yes. (Go to 2) \n2) Is the speaker or headphone mute set up? \n→Yes. Click on the Apple Menu and select System Preferences>Sound (in Hardware)>Output. Uncheck the mute box and turn on the volume. \n→No. (Go to 3) \n3) Is the volume on the computer off?\n→Yes. Turn up the volume. (Be sure to turn up the computer volume.) \n→No. The set up of other users might not be normal. Ask them to check. (Refer to above \"Other participants cannot hear my voices\")', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('86', 'en_US', 'What should I do if the volume is low?', 'You might have muted or turned down the volume.\nClick on the Apple Menu and select System Preferences>Sound (in Hardware)>Output. Uncheck the mute box and turn on the volume.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('87', 'en_US', 'There is an echo to my voice. How can I make it go away?', 'Is the person you are speaking with using speakers?\n→Yes. The microphone catches your voice through the speakers. The echo goes away if the person uses a headset. The person needs an echo cancellation microphone if the speakers are used.→No. If the headset is cheap, an echo sometimes happens.', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('148', 'en_US', '', null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('149', 'en_US', null, null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('150', 'en_US', null, null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('151', 'en_US', null, null, '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('5', 'ja_JP', '相手の声がノイズが多くてよく聞こえない', '1)相手はヘッドセットを使用していますか？\r\n\r\n→使用していない。\r\nヘッドセットをご利用頂いてください。\r\n\r\n→使用している（2へ進む）\r\n\r\n\r\n2)相手のヘッドセットのマイクが、音声集音タイプですか？\r\n\r\n→はい。\r\n相手のコンピュータに何か問題がある可能性がございます。PCの再起動のお願いをしてください。\r\n\r\n→いいえ。\r\n現在の物より高性能なヘッドセットをご利用頂けますようお勧めいたします。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('15', 'ja_JP', 'ホワイトボードには、どのような資料が貼り付けられますか？', 'BMP、JPEG、GIF、PNG、TIFF形式画像や、Word、Excel、PowerPoint、PDF　などの資料の貼り付けが可能です。\r\n※動画ファイル、PowerPointのアニメーション機能には対応しておりません。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('16', 'ja_JP', 'IDの管理はユーザ毎で行うのでしょうか？', 'いいえ。ログインＩＤの発行は基本的に１契約＝１会議室あたり１つとなります。\r\nお使いいただくユーザ総数は100人でも1000人でも問題ありません。座席数が10人の会議室を、１室ご利用するイメージでお考えいただければと思います。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('20', 'ja_JP', '3G対応の携帯電話を呼び出してもつながらない', '「Mobile」は非通知で発信を行なっています。\n携帯電話側が非通知着信を拒否する設定になっている場合は、許可する設定に変更を行なってください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('23', 'ja_JP', '申し込みの前に、無料で試用することはできますか？', '実際にご利用頂ける「無料トライアルアカウント」を貸し出させて頂きますので、ウェブサイトからお申し込みください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('24', 'ja_JP', '1契約（１会議室）につき10拠点間での会議が可能ということですが、10拠点以上で行うことは可能でしょうか？', 'オーディエンス機能（閲覧のみの機能）を利用することにより最大50拠点までテレビ会議に参加して頂くことが可能となります。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('25', 'ja_JP', 'オーディエンス機能とはなんでしょうか？', 'オーディエンス（傍聴者）とは、テレビ会議を傍聴することができる会議参加者です。オーディエンス参加者は、通常参加者が会議を行っている様子を閲覧することができます。オーディエンス参加者が、通常参加者となって、行われている会議に対して意見等を述べることもできます。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('26', 'ja_JP', '社外の人間と会議をしたいのですが、ID・パスワードを教えたくありません。', 'はい、可能です。「招待メール機能」をご利用頂くことで、他の会議参加者にIDとパスワードを教えることなくテレビ会議を行うことが可能となります。\r\n※招待メールを受け取る為のアドレスが必要です。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('27', 'ja_JP', 'IDを持っていないユーザも参加することはできますか？', 'はい、可能です。招待メール機能によって参加頂けます。\r\n※招待メールを受け取る為のアドレスが必要です。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('28', 'ja_JP', '1契約で複数の会議を同時に開催することはできますか？', 'いいえ、出来ません。\r\n別途、追加で会議室の契約ができますので、お問い合わせフォームからお問い合わせいただくか、サポートセンターにお問い合わせください。\r\nサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('29', 'ja_JP', '海外でも利用できますか？', 'はい、可能です。ウェブベースでのテレビ会議システムであるため、インターネットをご利用することが可能であれば、どなたでも参加して頂くことが可能です。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('30', 'ja_JP', 'ホワイトボードに貼り付ける資料の容量制限はありますか？', '1ファイルにつき、20Mまでアップロード可能です。また、1ファイルにつき20ページまでアップロード可能です。20Mまたは、20ページを超えるファイルは分割してアップロードしてください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('31', 'ja_JP', '自分のPCで動いているアプリケーションの画面を表示・共有できますか？', 'はい、可能です。\nただし、Sharingへのオプション契約が必要となります。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('32', 'ja_JP', 'ホワイトボードの内容をコピー＆ペーストすることはできますか？', 'いいえ、こちらの機能には現在対応しておりません。 ただし、「議事録を見る」機能で会議終了後でもホワイトボードの内容を見ることができます。 \r\nまた、会議中に「ファイルキャビネット」機能でお互いファイルの送受信を行うことができます。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('33', 'ja_JP', 'IDとパスワードをなくしてしまったのですが、どうすればいいでしょうか？', 'お手数ですが、サポートセンターにお問い合わせください。\r\nサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('34', 'ja_JP', '同時に何人、会議に参加できますか？', '通常サービスでは最大10人まで同時に会議に参加して頂くことが可能です。\r\nまた、オプション機能であるオーディエンス機能をご利用頂くことで最大50人まで同時に会議に参加して頂くことが可能となります。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('35', 'ja_JP', '他社テレビ会議サービスとの違いについて教えてください。', 'V-CUBE Meetingが他社サービスと大きく異なる点は以下の5点となっております。\n\n・インストール不要。\n専用ソフトをインストールする必要がなくインターネットが使用出来ればご利用可能です。\n\n・環境を選ばないマルチプラットフォーム。\nWindows、Macでも利用できます。\n\n・H.323対応のテレビ会議システムにも対応しています。\n\n・ 携帯対応。テレビ電話機能を持つ携帯電話での参加が可能です。\n\n・プロキシ、Firewall対応。企業内ＬＡＮの設定にも自動的に対応するので、難しい設定は不要です。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('36', 'ja_JP', 'V-CUBE Meetingを利用するために必要な機器などは何ですか？', 'ブロードバンド環境、パソコン、カメラ、ヘッドセットがあればすぐにV-CUBE Meetingをご利用頂けます。カメラがなくても参加可能です。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('37', 'ja_JP', 'Macで利用できますか？', 'はい、可能です。 ただし、一部ツール、Sharing3、Checker2、Echo Cancelerβ、PresenceAppliはWindows版しかございません。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('38', 'ja_JP', 'インターネットは利用できないが、イントラネット上で利用したい。', '弊社サーバをお買いあげいただき、イントラネット上で運用する方法があります。費用やスケジュールなどについては、サポートセンターまでお問い合わせください。 \r\nサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('39', 'ja_JP', 'サーバが落ちた場合はどのような対応をして頂けるのでしょうか？', 'センターが分散されているため、万が一サーバがダウンした場合でも、サービスに影響を与えないように設計されております。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('40', 'ja_JP', 'カメラを貸し出して頂くことは可能でしょうか？', '原則、貸し出しはしておりませんが、お問い合せ頂ければ貸し出しをさせていただくことも可能となっております。 \r\nサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('41', 'ja_JP', '会議室パックを導入前に試用してみたいのですが、貸し出してもらうことは可能でしょうか？', '会議室パッケージのお貸し出しにつきましては、一度カスタマーサポートセンターまでお問い合せください。\r\nカスタマーサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('42', 'ja_JP', '既に以前購入したテレビ会議システムがあるのですが、それを利用することはできますか？', 'H.323対応のテレビ会議システムであればご利用頂けます。\r\nただし、オプションサービスであるH.323対応サービスをご契約して頂く必要がございます。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('43', 'ja_JP', '無料トライアルアカウントで携帯電話接続（V-CUBE Mobile）も利用できますか？', '原則、ご利用頂けませんが、携帯電話接続（Mobile）のお試しをご希望のお客さまは、携帯電話接続機能の付いた無料トライアルアカウントを発行させていただきます。 \nカスタマーサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('44', 'ja_JP', '初期費用はいくらですか？', 'いずれのプランも一律 47,250円（税込）となります。こちらの料金にはセッティング費用等の諸費用も含まれております。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('45', 'ja_JP', '料金体系の詳細をおしえてください。', '月額10,395（税込）より各種プランをご用意しております。\r\nなお、課金はルームチャージ制となっておりますのでご利用人数に関係なく一律料金となっております。\r\n1つのアカウントで、オンライン上の会議室を１室お貸しするようなイメージになります。詳細は料金プラン一覧をご覧ください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('46', 'ja_JP', '料金プランを入会後に変更する際に、禁則事項など何か規定はありますか？\r\n（入会後１ヶ月しないうちにプランを変更するなど）', '特に制限はございません。プランの変更につきましては月の途中でも変更は可能ですが、適用は翌月１日からとなります。（その月は現行のプランでのご利用となります）前払いのお客様は翌々月の変更となります。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('47', 'ja_JP', '解約後に、再開する場合は、初期費用はかかるのでしょうか？', 'はい、再度設定いたしますので、初期費用が発生いたします。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('48', 'ja_JP', '料金は「1室あたり」とありますが、利用ユーザー数に制限はあるのでしょうか？', 'ご利用ユーザー数に制限はございません。課金は会議室単位になりますので、何名様でご利用されても料金は変わりません。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('49', 'ja_JP', '会議室パックの概算費用を教えてください。', '弊社担当者からお見積もりをさせて頂きますので、サポートセンターまでお問い合せください。\r\nサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('50', 'ja_JP', 'ファイアウォール（リバースプロキシやNAT）からの接続に対応していますか？', 'はい。ただしプロキシ経由の接続の場合には、プロキシの種類によって、映像・音声の遅延が発生する場合がございます。　対応策がございますので、別途お問い合わせください。\r\nカスタマーサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('51', 'ja_JP', '推奨はブロードバンド環境（上り128kbps、下り384kbps以上）とありますが、これ以下の回線速度の場合、どのような不都合が発生するのでしょうか？', '音声遅延や、映像遅延などが発生いたします。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('52', 'ja_JP', 'PHS移動通信（AirH″などのモバイルカード）からの利用は可能ですか？', '弊社では、動作の保証をしておりませんが、利用する事は可能です。 \r\n「低速回線モード」、「音声優先モード」、「利用帯域設定」など、回線速度の低い環境に合わせた設定も可能です。\r\nただし、PHS環境（AirH\"など）の場合には、128kbps接続でもパケット処理などにより、実質的には64kbps以下の速度しか出ないケースが多い為、実質的な利用は困難なケースがございます。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('53', 'ja_JP', 'セキュリティは万全なのでしょうか？', 'はい。映像と音声は非公開の独自プロトコルで通信しており、テキストチャットやファイル共有などドキュメントの部分は標準でSSLで通信しておりますので、限りなく万全な設計になっております。また、オプションサービスであるSSL対応サービスをご利用頂くことで、全ての通信をSSL化することも可能です。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('54', 'ja_JP', '映像と音声のコーデックは何を使用していますか？', 'ビデオのエンコード・デコードは、Flashの機能により行われております。\r\nその機能で使用されているコーデックはSorenson Media社のSorenson Sparkコーデックです。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('55', 'ja_JP', '使用しているプロトコルは何でしょうか？', 'Flash Media Serverで使用される、Adobe社独自のプロトコルRTMPを使用しております。\r\n※RTMPはFlash Media Serverで使用される、Adobe社独自のプロトコルです。\r\n詳細は、Adobe社のウェブページ（http://www.adobe.com/jp/products/flashmediaserver/）をご覧ください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('56', 'ja_JP', '映像・音声・データのやりとりや、ログインＩＤなどは、どのようなセキュリティ対応をされているのか教えてください。', 'ビデオストリーム以外の全ての通信は、SSL通信を利用しております。\r\nビデオストリームは独自の通信を行っており、プロトコル仕様は公開されていないため現状でも安全ですが、オプション機能により、ビデオストリームのSSＬ化も可能です。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('57', 'ja_JP', 'カメラは何が使えますか？推奨カメラなどはありますか？', 'USBカメラを利用することが可能です。\r\n詳細は、動作確認済みカメラをご覧ください。\r\nhttp://www.nice2meet.us/ja/tools/', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('58', 'ja_JP', '10拠点で利用する場合の必要帯域は？', '上り256Kbps、下り512Kbpsとなります。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('59', 'ja_JP', 'フレームレートはいくつですか？', '最大15fpsとなっております。\r\n会議参加人数に合わせて、自動的に最適なフレームレートになります。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('60', 'ja_JP', '最低利用期間など、利用期間に関する制限はありますか？', 'いいえ、制限は一切ございません。 \r\n契約期間の制限はございませんので、月単位のご利用が可能です。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('61', 'ja_JP', '海外でも利用できますか？', 'はい、可能です。ウェブベースでのテレビ会議システムであるため、インターネットをご利用することが可能であれば、どなたでも参加して頂くことが可能です。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('62', 'ja_JP', '回線速度を測定中のまま進まない', '回線速度が非常に遅い、あるいは回線を確保出来ていない可能性がございます。\r\n再度、入室していただきますと回線を再取得いたしますので、問題を解決する場合がございます。\r\n上記の方法で解決しない場合は、当社の推奨環境（回線、PCスペック）を満たしていない場合がございますので、弊社推奨環境をご確認ください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('63', 'ja_JP', '回線速度を測定する画面でエラーが表示される（プロキシ）', '回線速度が非常に遅い、あるいは回線を確保出来ていない可能性がございます。\r\n再度、入室していただきますと回線を再取得いたしますので、問題を解決する場合がございます。\r\n上記の方法で解決しない場合は、当社の推奨環境（回線、PCスペック）を満たしていない場合がございますので、弊社推奨環境をご確認ください。\r\nカスタマーサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('64', 'ja_JP', '毎回、会議中に「切断されました」となってしまう', '当システムでは、会議サーバに対して一定時間毎に「正常に会議が行われている」ことを通信しております。そのため、遅延が著しく発生している場合、サーバ側が接続していない状況と誤認し切断する場合がございます。会議では遅延を引き起こさなくても根本的な原因は遅延と同じになります。\r\n※プロキシが存在する場合や回線環境、PCスペックが当社製品の動作環境を下回っている可能性があります。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('65', 'ja_JP', '映像の縦横比がおかしい', 'ご使用中のビデオデバイスによっては、映像取得サイズを変更した際に映像の縦横比が変更されたり、上下に黒い帯が入る可能性があります。デバイスの性能や設定等をご確認ください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('66', 'ja_JP', '遅延がひどい', '1)Checker2でご利用環境の診断を行ってください。\n※Checker2についてはChecker2ご利用マニュアルをご参照下さい。\n\n\n2)回線は何をお使いですか？\n\n→ISDN　or　ダイヤルアップ\n推奨の回線速度が維持できません。快適で安価なBフレッツをお勧めいたします。\n\n→ADSL／Bフレッツ／その他、光やケーブルテレビなど　（3へ進む）\n\n\n3)プロキシ経由の接続ですか？\n\n→プロキシ経由です。\nプロキシ経由の接続の場合、プロキシサーバの特性上、遅延が発生する可能性があります。ただし、1935ポートを外向きに開けていただければプロキシサーバを介さずに通信ができますので、遅延の回避が可能となります。詳細はネットワーク管理者にその旨ご相談ください。\n\n→プロキシ経由ではない（4へ進む）\n\n\n4)お使いのパソコンは、PentiumIIIの533MHz以上ですか？\n\n→いいえ。\n推奨のスペックを満たしておりません。映像などを処理するに当たり、ある程度のスペックが要求されます。大変恐縮ではございますが、推奨環境以上のパソコンでご利用ください。\n\n→はい。（5へ進む）\n\n\n5)何かほかのアプリケーションが起動していませんか？\n\n→はい。\nシステムのリソース不足の可能性がございますので、他のアプリケーションを終了してください。\n\n→いいえ。\nご利用のパソコン自体に搭載されておりますビデオチップが遅延の原因かと思われます。少し古いタイプやノートパソコンなどでは映像を見るに適したスペックになっていないことがあります。お手数ではございますが再度、別のパソコンでご利用ください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('67', 'ja_JP', '自分の映像が表示されない', '1)Checker2でご利用環境の診断を行ってください。\n※Checker2についてはChecker2ご利用マニュアルをご参照下さい。\n\n\n2)カメラが接続されていますか？\n\n→接続されていない。\nUSBカメラもしくは、IEEE1394のカメラを接続してください。\n\n→接続されている（3へ進む）\n\n\n3)カメラはOSに認識されていますか？\n\n→認識されていない。\nカメラのマニュアルに従い、ドライバをインストールするなどして、カメラを認識させてください。\n\n→認識されている。（4へ進む）\n\n\n4)「カメラ切り替え」ボタンを何度かクリックして映像が表示されますか？\n\n→表示される。\nカメラが複数台つながっている場合や、OSの状態によってはカメラ切り替えボタンを押さないとカメラが利用できない場合がございます。\n\n→表示されない。（5へ進む）\n\n\n5)「会議室に入室」ボタンを押した後に表示される小さなウィンドウで、「許可」をクリックしましたか？\n\n→クリックしていない。\n会議ウィンドウを閉じて、再度、会議室に入室してください。\nその際に、必ず「許可」をクリックするようにしてください。\n\n→クリックした。（6へ進む）\n\n\n6)ほかにカメラを使うアプリケーションは起動していませんか？\n\n→起動している。\nカメラを使う他のアプリケーションを終了し、会議ウィンドウを閉じて、再度、会議室に入室してください。\n\n→起動していない。\n対応していないカメラである可能性か、カメラが壊れている可能性がございます。また、パソコンの再起動を一度お試しください。カメラの不具合につきましては各メーカーへお問い合わせください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('68', 'ja_JP', '相手の映像が表示されない', '1)相手の方にChecker2でご利用環境の診断を行って頂いてください。\n※Checker2についてはChecker2ご利用マニュアルをご参照下さい。\n\n\n2)相手がログインしていますか？\n\n→ログインしていない。\n相手の方へ会議室への入室のお願いをしてください。\n\n→ログインしている。（3へ進む）\n\n\n3)相手は相手自身の映像を相手のパソコン上で見えていますか？\n\n→見えていない。\n相手の映像が相手のPC上で表示されるように設定してください。\n（『自分の映像が表示されない』を参照）\n\n→見えている。（4へ進む）\n\n\n4)相手の名前は表示されていますか？\n\n→表示されている。\n何らかのシステムトラブルの可能性がございます。\n再度、会議室に入室し直してください。\n\n→表示されていない。（5へ進む）\n\n\n5)チャットウィンドウに文字を打ち込んで送信を押した後、チャットウィンドウにその内容が表示されますか？\n\n→表示されない。\nお客様が正常に接続できていない可能性がございます。\n再度、会議室に入室し直してください。\n\n→表示される。\n相手が正常に接続できていない可能性がございます。\n相手に会議室への再入室のお願いをしてください。（『自分の映像が表示されない』を参照）', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('69', 'ja_JP', '自分の音声が相手に届かない', '1)Checker2でご利用環境の診断を行ってください。\n※Checker2についてはChecker2ご利用マニュアルをご参照下さい。\n\n\n2)パソコンにマイクは接続されていますか？\n\n→接続されていない。\nパソコンにマイクを接続し、会議室へ再入室してください。\n\n→接続されている。（3へ進む）\n\n\n3)会議開始後に表示される小さなウィンドウで「許可」をクリックしましたか？\n\n→クリックしていない。\n再度会議室に入室し直してください。その際に必ず「許可」をクリックしてください。\n\n→クリックした。（4へ進む）\n\n\n4)パソコンのマイクがミュートの設定になっていませんか？\n\n→なっている。\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。\n\n→なっていない。（5へ進む）\n\n\n5)[コントロールパネル]→[サウンドとオーディオデバイス]→[オーディオ]→[録音]→[音量]→[録音コントロール]の画面で「マイク」にチェックがはいっていますか？\n\n→入っていない。\n「マイク」にチェックをいれてください。\n\n→入っている。（6へ進む）\n\n\n6)マイク切り替えボタンをクリックして、相手に自分の音声が届くようになりますか？\n\n→届く。\nマイクが複数台つながっている場合や、OSの状態によってはマイク切り替えボタンを押さないとマイクが利用できない場合があります。\n\n→届かない。\nマイクが故障しているか、お使いのコンピュータでは使えないマイクの可能性があります。また、パソコンの再起動を一度お試しください。マイクの不具合につきましては各メーカーへお問い合わせください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('70', 'ja_JP', '相手の音声が自分に届かない（音が出ない）', '1)Checker2でご利用環境の診断を行ってください。\n※Checker2についてはChecker2ご利用マニュアルをご参照下さい。\n\n\n2)スピーカもしくはヘッドフォンはつながっていますか？\n\n→つながっていない。\nスピーカもしくはヘッドフォンを接続し、会議室へ再入室してください。\n\n→つながっている（3へ進む）\n\n\n3)パソコンのスピーカもしくはヘッドフォンがミュートになっていませんか？\n\n→ミュートになっている。\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。\n\n→ミュートになっていない（4へ進む）\n\n\n4)パソコン本体のボリュームがOFFになっていませんか？\n\n→なっている。\nボリュームをあげてください。（設定ではなく、パソコン本体のボリューム）\n\n→なっていない。\n相手の設定が正常でない可能性があります。\n相手側にFAQの『自分の音声が相手に届かない。』を確認してください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('71', 'ja_JP', '音が小さいのですが、どうすればいいでしょうか？', 'ミュートもしくはボリュームが小さくなっていませんか？\r\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('72', 'ja_JP', '自分の声がエコーしてしまいます。直し方はありますか？', '相手側がスピーカーを使っていませんか？\r\n\r\n→使用している。\r\nあなたの声を相手側のマイクが拾っています。ヘッドセットをご利用頂ければエコーは無くなります。相手側でスピーカーの使用がやむを得ない場合は、エコーキャンセラー付きのマイクが必要になります。\r\n参考：エコーキャンセラー付きマイク　ClearOne社　ACCUMICII\r\n\r\n→使用していない。\r\n相手のヘッドセットが安価な物だと、多少自分の声が戻ってくる可能性がございます。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('73', 'ja_JP', '相手の声がノイズが多くてよく聞こえない', '1)相手はヘッドセットを使用していますか？\r\n\r\n→使用していない。\r\nヘッドセットをご利用頂いてください。\r\n\r\n→使用している（2へ進む）\r\n\r\n\r\n2)相手のヘッドセットのマイクが、音声集音タイプですか？\r\n\r\n→はい。\r\n相手のコンピュータに何か問題がある可能性がございます。PCの再起動のお願いをしてください。\r\n\r\n→いいえ。\r\n現在の物より高性能なヘッドセットをご利用頂けますようお勧めいたします。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('74', 'ja_JP', 'クリックしてもページが表示されない（ポップアップ表示がブロックされてしまう）', '「Windows XP Service Pack 2」をご利用されている方は、ポップアップ表示がブロックされてしまいます。\r\nブラウザのツールバーより「ツール」を選択、メニューから「ポップアップブロック」→「ポップアップブロックの設定」を選択し、クリックします。\r\n「許可するWebサイトのアドレス」に『www.nice2meet.us』と入力し、「追加」ボタンをクリックします。\r\n「閉じる」ボタンをクリックし、設定ウインドウを閉じます。\r\n\r\n以上で設定は完了です。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('75', 'ja_JP', '初期費用はいくらですか？', 'ご利用のプランによって異なりますので、カスタマーサポートセンターまでお問合せ下さい。\r\nカスタマーサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('77', 'ja_JP', '解約後に、再開する場合は、初期費用はかかるのでしょうか？', 'はい、再度設定しますので、初期費用は発生します。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('78', 'ja_JP', '1契約で複数のセミナーを同時に開催することはできますか？', 'いいえ、出来ません。別途、追加でセミナールームの契約ができますので、お問い合わせフォームからお問い合わせいただくか、カスタマーサポートセンターにお問い合わせください。\r\nカスタマーサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('79', 'ja_JP', '推奨のマイクなどありますか？', 'USBタイプの利用を推奨いたします。\r\nピンジャックタイプですとノイズが発生しやすくなります。\r\niSightや本体マイクでも会議可能ですが、ハウリングやエコーが発生しやすくなります。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('80', 'ja_JP', '会議中に他の作業をしていると、会議の処理が遅いように感じるのですが？', '最前面での利用を推奨いたします。\r\nOSやブラウザの設定によっては、会議の画面がバックグランドに回った際の処理が遅くなるため、常に最前面で利用してください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('81', 'ja_JP', '遅延がひどい', '1)回線は何をお使いですか？\r\n\r\n→ISDN　or　ダイヤルアップ\r\n推奨の回線速度が維持できません。快適で安価なBフレッツをお勧めいたします。\r\n\r\n→ADSL／Bフレッツ／その他、光やケーブルテレビなど（2へ進む）\r\n\r\n\r\n2)プロキシ経由の接続ですか？\r\n\r\n→プロキシ経由です。\r\nプロキシ経由の接続の場合、プロキシサーバの特性上、遅延が発生する可能性があります。ただし、1935ポートを外向きに開けていただければプロキシサーバを介さずに通信ができますので、遅延の回避が可能となります。詳細はネットワーク管理者にその旨ご相談ください。\r\n\r\n→プロキシ経由ではない（3へ進む）\r\n\r\n\r\n3)お使いのパソコンは推奨環境を満たしていますか？\r\n\r\n→いいえ。\r\n映像などを処理するに当たり、ある程度のスペックが要求されます。大変恐縮ではございますが、推奨環境以上のパソコンでご利用ください。\r\n\r\n→はい。（4へ進む）\r\n\r\n\r\n4)何かほかのアプリケーションが起動していませんか？\r\n\r\n→はい。\r\nシステムのリソース不足の可能性がございますので、他のアプリケーションを終了してください。\r\n\r\n→いいえ。\r\nご利用のパソコン自体に搭載されておりますビデオチップが遅延の原因かと思われます。少し古いタイプやノートパソコンなどでは映像を見るに適したスペックになっていないことがあります。お手数ではございますが再度、別のパソコンでご利用ください。(5へ進む)\r\n\r\n\r\n5)ウィンドウが最前面になっていない。\r\n\r\nMacは最前面のウィンドウの処理を優先にする為、ウィンドウがバックグラウンドに回ると極端に処理が遅くなる場合があります。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('82', 'ja_JP', '自分の映像が表示されない', '1)カメラが接続されていますか？\r\n\r\n→接続されていない。\r\nUSBカメラもしくは、IEEE1394のカメラを接続してください。\r\n\r\n→接続されている（2へ進む）\r\n\r\n\r\n2)カメラはOSに認識されていますか？\r\n\r\n→認識されていない。\r\nカメラのマニュアルに従い、ドライバをインストールするなどして、カメラを認識させてください。\r\n\r\n→認識されている。（3へ進む）\r\n\r\n\r\n3)「カメラ切り替え」ボタンを何度かクリックして映像が表示されますか？　\r\n\r\n→表示される。\r\nカメラが複数台つながっている場合や、OSの状態によってはカメラ切り替えボタンを押さないとカメラが利用できない場合がございます。\r\n※Macでは標準でカメラドライバを持っている為、USBカメラ利用の場合は必ず切り替えを行う必要があります。\r\n\r\n→表示されない。（4へ進む）\r\n\r\n\r\n4)「会議室に入室」ボタンを押した後に表示される小さなウィンドウで、「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n会議ウィンドウを閉じて、再度、会議室に入室してください。\r\nその際に、必ず「許可」をクリックするようにしてください。\r\n\r\n→クリックした。（5へ進む）\r\n\r\n\r\n5)ほかにカメラを使うアプリケーションは起動していませんか？\r\n\r\n→起動している。\r\nカメラを使う他のアプリケーションを終了し、会議ウィンドウを閉じて、再度、会議室に入室してください。\r\n\r\n→起動していない。\r\n対応していないカメラである可能性か、カメラが壊れている可能性がございます。また、パソコンの再起動を一度お試しください。カメラの不具合につきましては各メーカーへお問い合わせください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('83', 'ja_JP', '相手の映像が表示されない', '1)相手がログインしていますか？\r\n\r\n→ログインしていない。\r\n相手の方へ会議室への入室のお願いをしてください。\r\n\r\n→ログインしている。（2へ進む）\r\n\r\n\r\n2)相手は相手自身の映像を相手のパソコン上で見えていますか？\r\n\r\n→見えていない。\r\n相手の映像が相手のPC上で表示されるように設定してください。\r\n（『自分の映像が表示されない』を参照）\r\n\r\n→見えている。（3へ進む）\r\n\r\n\r\n3)相手の名前は表示されていますか？\r\n\r\n→表示されている。\r\n何らかのシステムトラブルの可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示されていない。（4へ進む）\r\n\r\n\r\n4)チャットウィンドウに文字を打ち込んで送信を押した後、チャットウィンドウにその内容が表示されますか？\r\n\r\n→表示されない。\r\nお客様が正常に接続できていない可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示される。\r\n相手が正常に接続できていない可能性がございます。\r\n相手に会議室への再入室のお願いをしてください。（『自分の映像が表示されない』を参照）', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('84', 'ja_JP', '自分の音声が相手に届かない', '1)パソコンにマイクは接続されていますか？\r\n\r\n→接続されていない。\r\nパソコンにマイクを接続し、会議室へ再入室してください。\r\n\r\n→接続されている。（2へ進む）\r\n\r\n\r\n2)会議開始後に表示される小さなウィンドウで「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n再度、会議室に入室し直してください。その際に必ず「許可」をクリックしてください。\r\n\r\n→クリックした。（3へ進む）\r\n\r\n\r\n3)パソコンのマイクがミュートの設定になっていませんか？\r\n\r\n→なっている。\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[入力]→[主音量]→[消音]にチェックを外してください。\r\n\r\n→なっていない。（4へ進む）\r\n\r\n\r\n4)マイク切り替えボタンを何度かクリックして、相手に自分の音声が届くようになりますか？\r\n\r\n→届く。\r\nMacでは、マイク切り替えボタンを押さないとマイクが利用できない場合があります。\r\n\r\n→届かない。\r\nマイクが故障しているか、お使いのコンピュータでは使えない可能性があります。また、パソコンの再起動を一度お試しください。マイクの不具合につきましては各メーカーへお問い合わせください。\r\nMacの音声入力はほとんどの場合、音声入力端子が使えない場合があります。当社ではUSBアダプタを必ずご使用になる事を推奨しております。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('85', 'ja_JP', '相手の音声が自分に届かない（音が出ない）', '1)スピーカもしくはヘッドフォンはつながっていますか？\r\n\r\n→つながっていない。\r\nスピーカもしくはヘッドフォンを接続し、会議室へ再入室してください。\r\n\r\n→つながっている（2へ進む）\r\n\r\n\r\n2)パソコンのスピーカもしくはヘッドフォンがミュートになっていませんか？\r\n\r\n→ミュートになっている。\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[出力]→[主音量]→[消音]のチェックを外して、ボリュームを上げてください。\r\n\r\n→ミュートになっていない（3へ進む）\r\n\r\n\r\n3)パソコン本体のボリュームがOFFになっていませんか？\r\n\r\n→なっている。\r\nボリュームをあげてください。（設定ではなく、パソコン本体のボリューム）\r\n\r\n→なっていない。\r\n相手の設定が正常でない可能性があります。\r\n相手側にFAQの『自分の音声が相手に届かない。』の確認をお願いをしてください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('86', 'ja_JP', '音が小さいのですが、どうすればいいでしょうか？', 'ミュートもしくはボリュームが小さくなっていませんか？\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[入力]→[入力音量]→[消音]のチェックを外して、ボリュームを上げてください。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('87', 'ja_JP', '自分の声がエコーしてしまいます。直し方はありますか？', '相手側がスピーカーを使っていませんか？\r\n\r\n→使用している。\r\nあなたの声を相手側のマイクが拾っています。ヘッドセットをご利用頂ければエコーは無くなります。相手側でスピーカーの使用がやむを得ない場合は、エコーキャンセラー付きのマイクが必要になります。\r\n\r\n→使用していない。\r\n相手のヘッドセットが安価な物だと、多少自分の声が戻ってくる可能性がございます。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('148', 'ja_JP', '無料トライアルアカウントでH.323TV会議システムへの接続（V-CUBE H.323）も利用できますか？', '原則、ご利用頂けませんが、お問い合せ頂ければH.323TV会議システムへの接続（H.323）をご利用頂ける無料トライアルアカウントを発行することも可能となっております。 カスタマーサポートセンターまでお問合せ下さい。\nカスタマーサポートセンター：0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('149', 'ja_JP', '推奨動作環境を教えてください。', '動作環境ページをご参照ください\r\nhttp://www.nice2meet.us/ja/requirements/', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('150', 'ja_JP', '必須動作環境を教えてください', '動作環境ページをご参照ください。\r\nhttp://www.nice2meet.us/ja/requirements/', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('151', 'ja_JP', '推奨ブロードバンド環境（上り128kbps、下り384kbps以上）とありますが、これ以下の回線速度の場合、どのような不都合が発生するのでしょうか？', '音声遅延や、映像遅延などが発生いたします。\r\nまた、「低速回線モード」、「音声優先モード」、「利用帯域設定」など、回線速度の低い環境に合わせた設定を使用した場合、画質が粗くなったり、映像が出なくなります。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('5', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('15', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('16', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('20', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('23', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('24', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('25', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('26', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('27', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('28', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('29', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('30', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('31', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('32', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('33', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('34', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('35', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('36', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('37', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('38', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('39', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('40', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('41', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('42', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('43', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('44', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('45', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('46', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('47', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('48', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('49', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('50', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('51', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('52', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('53', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('54', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('55', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('56', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('57', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('58', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('59', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('60', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('61', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('62', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('63', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('64', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('65', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('66', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('67', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('68', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('69', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('70', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('71', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('72', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('73', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('74', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('75', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('77', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('78', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('79', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('80', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('81', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('82', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('83', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('84', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('85', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('86', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('87', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('148', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('149', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('150', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('151', '', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('5', 'zh_CN', '噪音太大，我听不到其他人的声音。', '1) 扬声器或耳机麦克风是否连接到您的 Mac?\n→否。 将扬声器或耳机麦克风连接到您的 Mac。 \n→是。 (转到 2) \n2) 扬声器或耳机麦克风静音是否设置? \n→是。 单击苹果菜单并选择“系统预置”>“声音”(在“硬件”中)>输出。 取消静音框选择并打开音量。 \n→否。 (转到 3) \n3) 电脑上的音量是否关闭?\n→是。 加大音量。 (请务必加大电脑音量。) \n→否。 其他用户的设置可能不正常。 请他们检查。 (请参阅上述“其他参与者听不到我的声音”)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('15', 'zh_CN', '什么类型的文档可以上传到白板?', '您可以上载 BMP、JPEG、GIF、PNG 和 TIFF 图像格式，以及格式为 Word、Excel、PowerPoint 和 PDF 的文档文件。\n*不支持视频文件和 PowerPoint 动画。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('16', 'zh_CN', '一份使用契约可以获得多少 ID 和密码?', '每份契约发放一个 ID 和密码。 只要您愿意，您可以与其他用户共享 ID 和密码。 也就是说，您租用一间虚拟会议室，您有权允许任何人使用。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('20', 'zh_CN', '无法连接3G 对应手机', 'V-CUBE Mobile 不显示来电号码。\n若您的手机设置为拒绝无来电显示电话的话，请更改设置。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('23', 'zh_CN', '在购买服务之前是否可以试用?', '是，您可以在线注册申请试用帐户。试用期限为2周。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('24', 'zh_CN', '能否同时连接10 个以上的地点?', '是，使用可选的“受众”功能，最多可以同时连接 49 个用户。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('25', 'zh_CN', '什么是受众功能?', '受众参与者是能以旁听身份参加视频会议的用户。当一般用户举行视频会议时，受众参与者可进行旁观。会议中，受众参与者也可以以一般用户的身份参加会议，例如，发表意见。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('26', 'zh_CN', '如何在不会泄露 ID 和密码的情况下，和非用户进行会议呢?', '使用“邀请”功能，您可以通过电子邮件发送邀请涵。受邀者可通过点击涵中自动生成的超链接进入会议室。 不仅可保护用户的ID 和密码不泄漏，而且也方便了受邀者。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('27', 'zh_CN', '没有 ID 也可以参加会议?', '可以利用电子邮件的邀请功能。通过邮件中的超链接直接参加会议。\n *受邀者需要电子邮件地址。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('28', 'zh_CN', '能否使用一个帐户 ID 和密码同时举行多个会议吗?', '不可以，每个 ID 和密码同时只能参加一个会议。比如说，您只租用一个会议室，同时只能召开一项会议。 如果您需要同时使用多个会议室的话，请与客服中心联系。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('29', 'zh_CN', '在国外可以使用吗?', '可以，因为是以网络为平台的视频会议系统，所以只要能上网，就能够使用。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('30', 'zh_CN', '白板上传文档的上限是多少?', '每个文件的上传上限为 20 MB 或 20 页。超过 20 MB 或 20 页以上的文件需在上传之前应进行分割。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('31', 'zh_CN', '能否与其他用户共享我的 PC 上运行的文件和应用程序?', '可以，您可以通过桌面共享功能显示您桌面上的文档和应用程序。 此功能还可让其他用户遥控您的 PC。 V-CUBE 桌面共享是一项可选功能。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('32', 'zh_CN', '能否将白板的内容复制到剪贴板或反之亦然?', '不可以。 电子白板功能只能上传文件/图片。但是，\n您可以在白板上显示 Word、Excel、PowerPoint、PDF、BMP、PNG、JPEG 和 GIF 等文件。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('33', 'zh_CN', '忘记 ID 和密码应该怎么办?', '您可直拨(0570-00-2193)或发电子邮件与我们联系。 确认您的身份之后，我们会和您联系。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('34', 'zh_CN', '多少人可同时参加会议?', '视频会议的参加人数最多为 10 人。但 如果您使用“受众”功能，最多可同时连接 49 个用户。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('35', 'zh_CN', '告诉我 V-CUBE 会议系统相对于其他服务的优势。', '有 3 个主要特点： \n1. 无需安装任何客户端软件\n2. 跨平台，支持 Macintosh 和 Windows ，还可以连接到 H.323 终端。\n3. 绕过代理服务器和防火墙。 不需要改变公司局域网的任何设置。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('36', 'zh_CN', '使用 V-CUBE 会议系统是否需要任何特殊设备?', '您只需要宽带环境、电脑、摄像头和耳机麦克风。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('37', 'zh_CN', '我能否在 Mac 上使用 V-CUBE 网络视频会议工具?', '可以，但是“桌面共享”和“Checker”功能不支持 Mac。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('38', 'zh_CN', '我能否在 Intranet 中设置 V-CUBE 网络视频会议工具?', '可以，您可在局域网内构建 V-CUBE 网络视频会议系统（含服务器）以增强安全性。 有关详细信息，请与我们的销售团队联系。 (0570-00-2193)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('39', 'zh_CN', '如果服务器停机会有什么影响? 你们会如何解决?', '我们的服务器分散在多个IDC，就算某个服务器停机也可以确保 24/7 服务，不受影响。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('40', 'zh_CN', '我能否租用摄像头?', '一般我们不出借摄像头，但您若有需要的话请向我们的销售团队咨询。(0570-00-2192)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('41', 'zh_CN', '我能否在购买会议室配套系统之前试用?', '通常我们不提供会议室配套系统的免费试用，但也有过在用户提供保证金的前提下出借的先例。有关详细信息，请向我们的销售团队咨询。(0570-00-2193)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('42', 'zh_CN', '我能否将 V-CUBE 会议系统连接到我当前使用的网络视频会议系统?', '如果您使用的系统是 H.323 终端，则可以使用本功能。但是您必须注册申请 H.323 可选服务。 有关详细信息，请与我们的客服中心联系。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('43', 'zh_CN', '我能否通过免费试用帐户试用 V-CUBE 手机服务?', '通常我们不提供 V-CUBE Mobile 功能的免费试用，但是您若有需求请向我们的销售团队咨询。(0570-00-2193)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('44', 'zh_CN', '是否有初始费用?', '可以，不论哪个服务，激活费用一律为 45,000 日圆。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('45', 'zh_CN', '请告诉我定价详细信息。', '月租 9,900 日圆起(含税)您就可以使用本系统。 无论参与者多少，费用不变。 就像是您租用了一个虚拟会议室，您当然可以自由使用。 有关详情，请参阅定价详细信息。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('46', 'zh_CN', '我能否更改所签订的服务协议（套餐）? 我能否在一个月内更改所签订的服务协议（套餐）?', '您可以在月中更改所签订的服务协议（套餐）。 但是，更改将在下个月的第一天生效。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('47', 'zh_CN', '在我关闭帐户之后，我是否需要再次支付初始费用才能重新启动该服务?', '初始设置费用为 45,000 日圆。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('48', 'zh_CN', '会议室中的用户数是否有限制?', '这将取决于您所签订的服务协议（套餐）。 请与我们的客户中心联系，了解每项服务协议计划的限制。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('49', 'zh_CN', '告诉我关于会议配套系统的信息。', '请向我们的支持团队咨询以获得估计。 (0570-00-2192)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('50', 'zh_CN', '如果我的 PC 在防火墙内，我能否使用 V-CUBE 的服务(反向代理或 NAT)?', '可以，我们的系统可绕过防火墙。但是，如果您通过代理服务器连接，连接速度可能较慢。此类延迟可以通过对外打开 1935 端口来避免。有关详情，请与我们的客服中心联系。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('51', 'zh_CN', '建议使用宽带(上传最少为 128 kbps，下载最少为 384 kbps)，但如果连接速度较慢，可能发生哪些问题?', '将会发生声音和图像延迟。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('52', 'zh_CN', '我能否通过 PHS 手机使用 V-CUBE 网络视频会议工具? (AirH 或 P-in)', '虽然无法保证通信质量，但您可以通过 PHS 手机使用该系统。由于通信速度通常低于 64kbps，事实上可能难以使用。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('53', 'zh_CN', '安全性完善吗？', '是的。视频和语音通过非公开的独立协议进行传输，文本聊天和文件共享等文档部分以标准SSL进行传输，因此在安全方面万无一失。此外，因为使用支持SSL的选项服务，所以全部传输也可以进行SSL化。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('54', 'zh_CN', '使用哪种视频/视频编解码器?', 'Flash 用于视频编码/解码。 该功能使用的编解码器是 Sorenson Media 的 Sorenson Spark 编解码器。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('55', 'zh_CN', '使用哪种通信协议和端口?', 'RTMP 是 Adobe 在 Flash Communication Server 中使用的通信协议。有关详情，请参阅 Adobe 网站。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('56', 'zh_CN', '登录 ID、视频、音频和数据通信使用哪种安全保护?', 'SSL 加密用于除视频音频之外的所有通信。 视频音频则由专用非公开通信协议保护。 V-CUBE SSL 是一项可选功能，可以使所有通信更安全。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('57', 'zh_CN', '您推荐哪种摄像头?', '我们的系统支持标准 USB 摄像头。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('58', 'zh_CN', '同时连接10个地点举行会议时，我需要多少带宽?', '您至少需要 256kbps 上游速度和 512kbps 下游速度。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('59', 'zh_CN', 'V-CUBE 网络视频会议工具所需的帧速率是多少?', '该系统根据连接的用户数自动调整。 您可以获得的最大帧速率为 15fps。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('60', 'zh_CN', '是否有最短使用契约期限?', '没有最短契约期限。 逐月计算。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('61', 'zh_CN', '在国外也可以使用吗?', '可以，因为是以网络为平台的视频会议系统，所以只要能上网，就能够使用。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('62', 'zh_CN', '由于会议系统在测量连接速度时停止，我无法进入会议室。', '发生这种情况可能是由于网络连接速度太慢或 电脑的性能较低。 请再次尝试登录。 如果再次出现这种情况，可能是 电脑不满足最低系统要求(网线连接或电脑规格）。 有关详情，请参阅推荐环境要求。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('63', 'zh_CN', '测量 PC 连接速度时出现错误消息。 (代理服务器)', '您可能无法在您的网络环境中使用此服务。可能的原因:\n1. 您的网络的代理服务器较旧。\n2. 网络的安全设置太高。\n要解决此问题，请咨询客户中心。我们可以建议适合您的网络环境的最佳方案。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('64', 'zh_CN', '每次使用时，会议都会被中断。', '系统定期向服务器报告会议发生状况。 如果服务器由于延迟而未获得报告，有可能断开连接。 基本原因是延迟。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('65', 'zh_CN', '视频的长宽比似乎错误。', '根据您使用的视频设备，更改视频捕获大小可能会导致长宽比发生变化，并可能导致在屏幕的顶部和底部出现黑边。请检查您的设备性能和设置。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('66', 'zh_CN', '延迟很长。', '1) 检查使用 V-CUBE Checker 的 PC 环境。 \n※有关如何使用 V-CUBE Checker，请参阅 V-CUBE Checker 操作手册。\n2) 您的 PC 如何上网?\n→ISDN　或拨号上线\n您的 PC 连接速度低于我们建议的速度。\n→ADSL／DSL／T-1 (转到 3)\n3) 是否通过代理服务器连接?\n→是。 通过代理服务器连接。\n如果您通过代理服务器连接，连接速度可能较慢。 此类延迟可以通过对外打开 1935 端口来避免。 请与您的 IT 部门联系。\n→否。 不通过代理服务器连接。 (转到 4)\n4) 您的 PC 的 CPU 是否高于 Pentium III 533 MHz?\n→否。\n您的 PC 不够快。 您必须使用符合或高于最低系统要求的 PC。\n→是。(转到 5)\n5) 是否有其他应用程序打开?\n→是。\n关闭其他应用程序，系统可能资源不足。 \n→否。 \n视频芯片有可能是延迟的原因，使您的 PC 无法显示视频影像。可能它太旧或是笔记本。 请尝试使用其他 PC。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('67', 'zh_CN', '看不到自己的视频。', '1) 检查使用 V-CUBE Checker 的 PC 的环境。 \n※有关如何使用 V-CUBE Checker，请参阅 V-CUBE Checker 操作手册。 \n2) 是否连接摄像头?\n→否。 \n连接 USB 摄像头或 IEEE 1934 摄像头。\n→是。 (转到 3) \n3) 您的操作系统是否识别摄像头?\n→否。\n通过安装驱动程序使您的操作系统能够识别摄像头。\n→是。 (转到 4) \n4) 当您单击摄像头开关按钮(位于视频所在画面下方)时，是否可以看到视频?\n→是。\n有时您必须单击摄像头开关按钮才能显示视频。\n→否。 (转到 5) \n5) 在单击会议开始按钮之后，您是否单击了弹出的小窗口中的“批准”?\n→否。\n关闭会议窗口，然后再次开始会议。\n→是。 (转到 6) \n6) 摄像头的其他应用程序是否打开?\n→是。\n关闭使用摄像头的应用程序，然后再次单击会议开始按钮。\n→否。\n摄像头可能不兼容，或者可能损坏。 尝试重新启动您的 PC，如果仍无法工作，请与摄像头制造商的技术支持部门联系。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('68', 'zh_CN', '看不到对方的视频。', '1) 检查对方的使用 V-CUBE Checker 的 PC 环境。 \n※有关如何使用 V-CUBE Checker，请参阅 V-CUBE Checker 操作手册。\n2) 对方是否已登录? \n→否。 请他们登录。 \n→是。 (转到 3) \n3) 对方能否在他们的 PC 上看到他们自己的视频? \n→ 否。 请对方进行设置，以便可以在自己的 PC 上看到自己的视频。 (请参阅上述“看不到自己的视频”。) \n→ 是。 (转到 4) \n4) 您能否看到他们的名称? \n→否。 可能存在系统问题。 再次尝试开始会议。 \n→是。 (转到 5) \n5) 您能否看到您在聊天窗口中输入的文本?\n→否。 您可能未连接。 再次尝试开始会议。\n→是。 对方可能未连接。 再次尝试开始会议。 (请参阅上述“看不到自己的视频”。)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('69', 'zh_CN', '其他参与者听不到我的声音。', '1) 检查使用 V-CUBE Checker 的 PC 的环境。 \n※有关如何使用 V-CUBE Checker，请参阅 V-CUBE Checker 操作手册。 \n2) 麦克风是否连接到您的 PC? \n→否。 将麦克风连接到您的 PC。 \n→是。 (转到 3) \n3) 在单击会议开始按钮之后，您是否单击了弹出的小窗口中的“批准”? \n→否。 关闭会议窗口，然后再次开始会议。 您必须单击小窗口中的“批准”。 \n→是。 (转到 4) \n4) 麦克风静音是否设置? \n→是。 取消麦克风静音选择，并在控制面板的“声音和音频设备”部分将音量设置为高。 \n→否。 (转到 5) \n5) 转到控制面板，依次单击“声音和音频设备”、“音频”、“录音”，然后单击“音量”。麦克风的框是否选中? \n→否。 选择麦克风。 \n→是。 (转到 6) \n6) 在您点击麦克风开关按钮之后，其他人能否听到您说话? \n→能。 请点击麦克风开关按钮。特别是多个麦克风同时连接到 PC 时，或者由于操作系统的状态，有可能发生此现象。\n→不能。 麦克风可能不兼容，或者可能损坏。 尝试重新启动您的 PC。 如果仍不能解决，请与摄像头制造商的技术支持部门联系。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('70', 'zh_CN', '我听不到其他参与者的声音。', '1) 检查使用 V-CUBE Checker 的 PC 的环境。 \n※有关如何使用 V-CUBE Checker，请参阅 V-CUBE Checker 操作手册。 \n2) 扬声器或耳机麦克风是否连接到您的 PC?\n→否。 将扬声器或耳机麦克风连接到您的 PC。 \n→是。 (转到 3)\n3) 扬声器或耳机麦克风静音是否设置? \n→是。 取消麦克风静音选择，并在控制面板的“声音和音频设备”部分将音量设置为高。 \n→否。 (转到 4) \n4) PC 的音量是否关闭?\n→是。 加大音量。 (请务必加大 PC 音量。) \n→否。 其他用户的设置可能不正常。 请他们检查。 (请参阅上述“其他参与者听不到我的声音”)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('71', 'zh_CN', '如果音量很低，我应该怎么办?', '您可能已静音或调低音量。 取消麦克风静音选择，并在控制面板的“声音和音频设备”部分将音量设置为高。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('72', 'zh_CN', '我的声音有回音。 我如何消除回音?', '对方是否使用扬声器?\n→是。 麦克风通过扬声器接收到您的声音。 如果对方使用耳机麦克风，回音将消失。 如果对方使用扬声器，需要使用可消除回音的麦克风。\n参考: ClearOne 的 ACCUMICII 是可消除回音的麦克风。 \n→否。 如果是价格低廉耳机麦克风，有时会发生回音。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('73', 'zh_CN', '噪音太大，我听不到其他人的声音。', '1) 对方是否使用耳机麦克风? \n→否。 请他们使用耳机麦克风。 \n→是。 (转到 2) \n2) 对方的麦克风是否为定向麦克风?\n→是。 对方的 PC 可能存在问题。 请他们重新启动 PC 并再次登录。 \n→否。 建议您使用质量更好的耳机麦克风。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('74', 'zh_CN', '无法显示页面。 (已阻止弹出窗口。)', '如果您使用 Windows XP Service Pack 2，会阻止弹出窗口。 在浏览器上，单击“工具”，转到“弹出窗口阻止程序”，单击“弹出窗口阻止程序设置”。 在“要允许的网站地址”框中，输入“www.nice2meet.us”(不带引号)，单击“添加”按钮将其添加到允许的站点列表。 单击“关闭”按钮保存您的设置。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('75', 'zh_CN', '是否有初始费用?', '根据您所签订的服务协议（套餐）而有所不同。 请咨询我们的客服中心。\n客服中心: 0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('77', 'zh_CN', '在我关闭帐户之后，我是否需要再次支付初始费用才能重新启动该服务?', '是，初始设置费用为 45,000 日圆。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('78', 'zh_CN', '我能否在签订一使用契约的情况下同时举行多个研讨会?', '不，您不能。您可以单独注册申请其他研讨室。 请通过查询表单提出请求，或者联系我们的客服中心。\n客服中心: 0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('79', 'zh_CN', '我可以使用哪个麦克风? 您推荐哪一个?', '建议使用 USB 麦克风。 (通常没有声音输入终端。 如果有，大多数情况下不能使用。)如果使用 iSight 或电脑麦克风，可能会出现啸叫声或回音。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('80', 'zh_CN', '建议在前端使用我们的系统。 由于 Mac 上的处理顺序是从前到后，如果会议窗口不在最前端，可能出现延迟。', '建议在前端使用我们的系统。 由于 Mac 上的处理顺序是从前到后，如果会议窗口不在最前端，可能出现延迟。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('81', 'zh_CN', '延迟很长。', '1) 您的 PC 如何上网?\n→ISDN　或拨号上线\n您的 PC 连接速度低于我们建议的速度。\n→ADSL／DSL／T-1 (转到 2)\n2) 是否通过代理服务器连接?\n→是。 通过代理服务器连接。 如果您通过代理服务器连接，连接速度可能较慢。 此类延迟可以通过对外打开 1935 端口来避免。 请与您的 IT 部门联系。\n→否。 不通过代理服务器连接。 (转到 3)\n3) 您的 Mac 是否满足系统要求? \n→否。 请使用满足系统要求的电脑。 \n→是。 (转到 4) \n4) 是否打开其他应用程序?\n→是。 关闭其他应用程序，系统可能资源不足。 \n→否。视频芯片有可能是延迟的原因，使您的 PC 无法显示视频影像。可能它太旧或是笔记本。 请尝试使用其他 PC。 (转到 5) \n5) 会议窗口未在前端。 由于 Mac 上的处理顺序是从前到后，如果会议窗口不在最前端，可能出现延迟。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('82', 'zh_CN', '看不到自己的视频。', '1) 摄像头是否连接?\n→否。 连接 USB 摄像头或 IEEE 1934 摄像头。\n→是。 (转到 2)\n2) 您的操作系统是否识别摄像头?\n→否。\n通过安装驱动程序使您的操作系统能够识别摄像头。\n→是。 (转到 3) \n3) 当您单击摄像头开关按钮(位于视频所在画面下方)时，是否可以看到视频?\n→是。 有时您必须单击摄像头开关按钮才能显示视频。 *对于 USB 摄像头，您必须在 Mac 上单击该按钮，因为 Mac 具有自己的摄像头驱动程序。\n→否。 (转到 4) \n4) 在单击会议开始按钮之后，您是否单击了弹出的小窗口中的“批准”?\n→否。 关闭会议窗口，然后再次开始会议。\n→是。 (转到 5) \n5) 摄像头的其他应用程序是否打开?\n→是。 关闭使用摄像头的应用程序，然后再次单击会议开始按钮。\n→否。\n摄像头可能不兼容，或者可能损坏。 尝试重新启动您的 PC，如果仍无法工作，请与摄像头制造商的技术支持部门联系。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('83', 'zh_CN', '看不到对方的视频。', '1) 对方是否已登录? \n→否。 请他们登录。 \n→是。 (转到 3) \n2) 对方能否在他们的 PC 上看到他们自己的视频? \n→ 否。 请对方进行设置，以便可以在自己的 PC 上看到自己的视频。 (请参阅上述“看不到自己的视频”。) \n→ 是。 (转到 4) \n4) 您能否看到他们的名称? \n→否。 可能存在系统问题。 再次尝试开始会议。 \n→是。 (转到 5) \n5) 您能否看到您在聊天窗口中输入的文本?\n→否。 您可能未连接。 再次尝试开始会议。\n→是。 对方可能未连接。 再次尝试开始会议。 (请参阅上述“看不到自己的视频”。)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('84', 'zh_CN', '其他参与者听不到我的声音。', '1) 麦克风是否连接到您的 Mac? \n→否。 将麦克风连接到您的 Mac。 \n→是。 (转到 2) \n2) 在单击会议开始按钮之后，您是否单击了弹出的小窗口中的“批准”?\n →否。 关闭会议窗口，然后再次开始会议。 您必须单击小窗口中的“批准”。 \n→是。 (转到 3) \n3) 麦克风静音是否设置? \n→是。 单击苹果菜单并选择“系统预置”>“声音”(在“硬件”中)>输出。 取消静音框选择并打开音量。\n→否。 (转到 4) \n4) 在您单击麦克风开关按钮之后，对方能否听到您说话? \n→能。 请点击麦克风开关按钮。特别是多个麦克风同时连接到 PC 时，或者由于操作系统的状态，有可能发生此现象。\n→不能。 麦克风可能不兼容，或者可能损坏。 尝试重新启动您的 PC。 如果仍不能解决，请与摄像头制造商的技术支持部门联系。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('85', 'zh_CN', '我听不到其他参与者的声音。', '1) 扬声器或耳机麦克风是否连接到您的 Mac?\n→否。 将扬声器或耳机麦克风连接到您的 Mac。 \n→是。 (转到 2) \n2) 扬声器或耳机麦克风静音是否设置? \n→是。 单击苹果菜单并选择“系统预置”>“声音”(在“硬件”中)>输出。 取消选中静音框并打开音量。 \n→否。 (转到 3) \n3) 电脑的音量是否关闭?\n→是。加大音量。 (请务必加大电脑音量。) \n→否。 其他用户的设置可能不正常。 请他们检查。 (请参阅上述“其他参与者听不到我的声音”)', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('86', 'zh_CN', '如果音量很低，我应该怎么办?', '您可能已静音或调低音量。\n单击苹果菜单并选择“系统预置”>“声音”(在“硬件”中)>输出。 取消静音框的选择并打开音量。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('87', 'zh_CN', '我的声音有回音。 我如何消除回音?', '对方是否使用扬声器?\n→是。 麦克风通过扬声器接收到您的声音。 如果该人员使用耳机麦克风，回音将消失。 如果该人员使用扬声器，需要可消除回音的麦克风。\n→否。 如果是价格低廉耳机麦克风，有时会发生回音。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('148', 'zh_CN', '我能否通过免费试用帐户连接(V-CUBE H.323)到 H.323TV 会议系统?', '一般情况下，不提供此项服务。但您可以申请一个免费的试用帐户，此帐户将使您可以通过连接(V-CUBE H.323)到 H.323TV 会议系统。 请联系我们的客服中心。\n客服中心: 0570-00-2192', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('149', 'zh_CN', '推荐系统要求是什么?', '请参阅系统要求页面。\nhttp://www.nice2meet.us/ja/requirements/', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('150', 'zh_CN', '最低系统要求是什么?', '请参阅系统要求页面。\nhttp://www.nice2meet.us/ja/requirements/', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('151', 'zh_CN', '建议使用宽带(上传最少为 128 kbps，下载最少为 384 kbps)，但如果连接速度较慢，可能发生哪些问题?', '将会发生视频延迟或音频延迟。\n如果您使用低速连接环境的设置(例如窄带模式、仅音频模式或优化带宽)，影像质量可能变成颗粒状，或者您可能无法获得影像。', '2010-05-16 20:47:36', '2010-05-16 20:47:36');
INSERT INTO `faq_data` VALUES ('5', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('15', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('16', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('20', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('23', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('24', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('25', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('26', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('27', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('28', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('29', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('30', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('31', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('32', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('33', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('34', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('35', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('36', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('37', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('38', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('39', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('40', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('41', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('42', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('43', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('44', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('45', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('46', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('47', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('48', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('49', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('50', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('51', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('52', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('53', 'zh_TW', '安全性完善嗎？', '是的。視頻和語音通過非公開的獨立協定進行傳輸，文本聊天和檔共用等文檔部分以標準SSL進行傳輸，因此在安全方面萬無一失。此外，因為使用支援SSL的選項服務，所以全部傳輸也可以進行SSL化。', '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('54', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('55', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('56', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('57', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('58', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('59', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('60', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('61', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('62', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('63', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('64', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('65', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('66', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('67', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('68', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('69', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('70', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('71', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('72', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('73', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('74', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('75', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('77', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('78', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('79', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('80', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('81', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('82', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('83', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('84', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('85', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('86', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('87', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('148', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('149', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('150', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');
INSERT INTO `faq_data` VALUES ('151', 'zh_TW', null, null, '2010-05-10 17:37:04', '2010-05-10 17:37:04');

-- ----------------------------
-- Table structure for faq_list
-- ----------------------------
DROP TABLE IF EXISTS `faq_list`;
CREATE TABLE `faq_list` (
  `faq_key` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category` int(11) NOT NULL DEFAULT '0',
  `faq_subcategory` int(11) NOT NULL DEFAULT '0',
  `faq_order` int(11) NOT NULL DEFAULT '0',
  `faq_question` text NOT NULL,
  `faq_answer` text NOT NULL,
  `faq_status` int(11) NOT NULL DEFAULT '0',
  `faq_lang` varchar(255) NOT NULL DEFAULT '0',
  `faq_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1: すべてのサービス, 2: 通常, 3: メンバー課金モデル',
  PRIMARY KEY (`faq_key`)
) ENGINE=MyISAM AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of faq_list
-- ----------------------------
INSERT INTO `faq_list` VALUES ('1', '1', '1', '1', '1契約（１会議室）につき10地点間での会議が可能ということですが、10地点以上で行うことは可能でしょうか？', 'オーディエンス機能（閲覧のみの機能）を利用することにより最大50拠点までテレビ会議に参加して頂くことが可能となります。', '2', '0', '1');
INSERT INTO `faq_list` VALUES ('2', '2', '1', '1', '料金の全般のしつもん', '料金の全般のこたえ', '2', '0', '1');
INSERT INTO `faq_list` VALUES ('3', '3', '2', '1', '環境のミーティングのしつもん', '環境のミーティングの答え', '2', '0', '1');
INSERT INTO `faq_list` VALUES ('4', '4', '3', '1', 'トラブルシューティングのセミナーしつもん', 'トラブルシューティングのセミナーのこたえ', '2', '0', '1');
INSERT INTO `faq_list` VALUES ('5', '5', '1', '10', '相手の声がノイズが多くてよく聞こえない', '1)相手はヘッドセットを使用していますか？\r\n\r\n→使用していない。\r\nヘッドセットをご利用頂いてください。\r\n\r\n→使用している（2へ進む）\r\n\r\n\r\n2)相手のヘッドセットのマイクが、音声集音タイプですか？\r\n\r\n→はい。\r\n相手のコンピュータに何か問題がある可能性がございます。PCの再起動のお願いをしてください。\r\n\r\n→いいえ。\r\n現在の物より高性能なヘッドセットをご利用頂けますようお勧めいたします。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('6', '5', '3', '1', 'まっくのせみなーのしつもん', 'まっくのせみなーのしつもん', '2', '0', '1');
INSERT INTO `faq_list` VALUES ('15', '1', '2', '9', 'ホワイトボードには、どのような資料が貼り付けられますか？', 'BMP、JPEG、GIF、PNG、TIFF形式画像や、Word、Excel、PowerPoint、PDF　などの資料の貼り付けが可能です。\r\n※動画ファイル、PowerPointのアニメーション機能には対応しておりません。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('16', '1', '2', '1', 'IDの管理はユーザ毎で行うのでしょうか？', 'いいえ。ログインＩＤの発行は基本的に１契約＝１会議室あたり１つとなります。\r\nお使いいただくユーザ総数は100人でも1000人でも問題ありません。座席数が10人の会議室を、１室ご利用するイメージでお考えいただければと思います。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('21', '1', '1', '2', '申し込みの前に、無料で試用することはできますか？', '実際にご利用頂けるテストアカウントを貸し出させて頂きますので、ウェブサイトからお申し込みください。', '2', '0', '1');
INSERT INTO `faq_list` VALUES ('18', '1', '3', '1', 'seminar questin', 'seminar answer', '2', '0', '1');
INSERT INTO `faq_list` VALUES ('19', '5', '2', '6', 'aaa', 'aaa', '2', '0', '1');
INSERT INTO `faq_list` VALUES ('20', '4', '1', '13', '3G対応の携帯電話を呼び出してもつながらない', '「N2MY Mobile」は非通知で発信を行なっています。\r\n携帯電話側が非通知着信を拒否する設定になっている場合は、許可する設定に変更を行なってください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('22', '2', '2', '1', 'test', 'test', '2', '0', '1');
INSERT INTO `faq_list` VALUES ('23', '1', '1', '3', '申し込みの前に、無料で試用することはできますか？', '実際にご利用頂ける「無料トライアルアカウント」を貸し出させて頂きますので、ウェブサイトからお申し込みください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('24', '1', '2', '2', '1契約（１会議室）につき10拠点間での会議が可能ということですが、10拠点以上で行うことは可能でしょうか？', 'オーディエンス機能（閲覧のみの機能）を利用することにより最大50拠点までテレビ会議に参加して頂くことが可能となります。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('25', '1', '2', '3', 'オーディエンス機能とはなんでしょうか？', 'オーディエンス（傍聴者）とは、テレビ会議を傍聴することができる会議参加者です。オーディエンス参加者は、通常参加者が会議を行っている様子を閲覧することができます。オーディエンス参加者が、通常参加者となって、行われている会議に対して意見等を述べることもできます。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('26', '1', '2', '4', '社外の人間と会議をしたいのですが、ID・パスワードを教えたくありません。', 'はい、可能です。「招待メール機能」をご利用頂くことで、他の会議参加者にIDとパスワードを教えることなくテレビ会議を行うことが可能となります。\r\n※招待メールを受け取る為のアドレスが必要です。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('27', '1', '2', '5', 'IDを持っていないユーザも参加することはできますか？', 'はい、可能です。招待メール機能によって参加頂けます。\r\n※招待メールを受け取る為のアドレスが必要です。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('28', '1', '2', '6', '1契約で複数の会議を同時に開催することはできますか？', 'いいえ、出来ません。\r\n別途、追加で会議室の契約ができますので、お問い合わせフォームからお問い合わせいただくか、サポートセンターにお問い合わせください。\r\nサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('29', '1', '3', '2', '海外でも利用できますか？', 'はい、可能です。ウェブベースでのテレビ会議システムであるため、インターネットをご利用することが可能であれば、どなたでも参加して頂くことが可能です。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('30', '1', '2', '10', 'ホワイトボードに貼り付ける資料の容量制限はありますか？', '1ファイルにつき、20Mまでアップロード可能です。また、1ファイルにつき20ページまでアップロード可能です。20Mまたは、20ページを超えるファイルは分割してアップロードしてください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('31', '1', '2', '7', '自分のPCで動いているアプリケーションの画面を表示・共有できますか？', 'はい、可能です。\r\nただし、N2MY Sharingへのオプション契約が必要となります。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('32', '1', '2', '8', 'ホワイトボードの内容をコピー＆ペーストすることはできますか？', 'いいえ、こちらの機能には現在対応しておりません。 ただし、「議事録を見る」機能で会議終了後でもホワイトボードの内容を見ることができます。 \r\nまた、会議中に「ファイルキャビネット」機能でお互いファイルの送受信を行うことができます。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('33', '1', '1', '4', 'IDとパスワードをなくしてしまったのですが、どうすればいいでしょうか？', 'お手数ですが、サポートセンターにお問い合わせください。\r\nサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('34', '1', '2', '11', '同時に何人、会議に参加できますか？', '通常サービスでは最大10人まで同時に会議に参加して頂くことが可能です。\r\nまた、オプション機能であるオーディエンス機能をご利用頂くことで最大50人まで同時に会議に参加して頂くことが可能となります。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('35', '1', '2', '12', '他社テレビ会議サービスとの違いについて教えてください。', 'nice to meet youが他社サービスと大きく異なる点は以下の5点となっております。\r\n\r\n・インストール不要。\r\n専用ソフトをインストールする必要がなくインターネットが使用出来ればご利用可能です。\r\n\r\n・環境を選ばないマルチプラットフォーム。\r\nWindows、Macでも利用できます。\r\n\r\n・H.323対応のテレビ会議システムにも対応しています。\r\n\r\n・ 携帯対応。テレビ電話機能を持つ携帯電話での参加が可能です。\r\n\r\n・プロキシ、Firewall対応。企業内ＬＡＮの設定にも自動的に対応するので、難しい設定は不要です。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('36', '1', '2', '13', 'nice to meet youを利用するために必要な機器などは何ですか？', 'ブロードバンド環境、パソコン、カメラ、ヘッドセットがあればすぐにnice to meet youをご利用頂けます。カメラがなくても参加可能です。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('37', '1', '1', '5', 'Macで利用できますか？', 'はい、可能です。 ただし、一部ツール、N2MY Sharing N2MY CheckerはWindows版しかございません。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('38', '1', '1', '6', 'インターネットは利用できないが、イントラネット上で利用したい。', '弊社サーバをお買いあげいただき、イントラネット上で運用する方法があります。費用やスケジュールなどについては、サポートセンターまでお問い合わせください。 \r\nサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('39', '1', '2', '14', 'サーバが落ちた場合はどのような対応をして頂けるのでしょうか？', 'センターが分散されているため、万が一サーバがダウンした場合でも、サービスに影響を与えないように設計されております。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('40', '1', '1', '7', 'カメラを貸し出して頂くことは可能でしょうか？', '原則、貸し出しはしておりませんが、お問い合せ頂ければ貸し出しをさせていただくことも可能となっております。 \r\nサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('41', '1', '2', '15', '会議室パックを導入前に試用してみたいのですが、貸し出してもらうことは可能でしょうか？', '会議室パッケージのお貸し出しにつきましては、一度カスタマーサポートセンターまでお問い合せください。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('42', '1', '2', '16', '既に以前購入したテレビ会議システムがあるのですが、それを利用することはできますか？', 'H.323対応のテレビ会議システムであればご利用頂けます。\r\nただし、オプションサービスであるH.323対応サービスをご契約して頂く必要がございます。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('43', '1', '2', '17', '無料トライアルアカウントで携帯電話接続（N2MY Mobile）も利用できますか？', '原則、ご利用頂けませんが、携帯電話接続（N2MY Mobile）のお試しをご希望のお客さまは、携帯電話接続機能の付いた無料トライアルアカウントを発行させていただきます。 \r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('44', '2', '2', '2', '初期費用はいくらですか？', 'いずれのプランも一律 47,250円（税込）となります。こちらの料金にはセッティング費用等の諸費用も含まれております。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('45', '2', '2', '3', '料金体系の詳細をおしえてください。', '月額10,395（税込）より各種プランをご用意しております。\r\nなお、課金はルームチャージ制となっておりますのでご利用人数に関係なく一律料金となっております。\r\n1つのアカウントで、オンライン上の会議室を１室お貸しするようなイメージになります。詳細は料金プラン一覧をご覧ください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('46', '2', '2', '4', '料金プランを入会後に変更する際に、禁則事項など何か規定はありますか？\r\n（入会後１ヶ月しないうちにプランを変更するなど）', '特に制限はございません。プランの変更につきましては月の途中でも変更は可能ですが、適用は翌月１日からとなります。（その月は現行のプランでのご利用となります）前払いのお客様は翌々月の変更となります。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('47', '2', '2', '5', '解約後に、再開する場合は、初期費用はかかるのでしょうか？', 'はい、再度設定いたしますので、初期費用が発生いたします。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('48', '2', '2', '6', '料金は「1室あたり」とありますが、利用ユーザー数に制限はあるのでしょうか？', 'ご利用ユーザー数に制限はございません。課金は会議室単位になりますので、何名様でご利用されても料金は変わりません。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('49', '2', '2', '2', '会議室パックの概算費用を教えてください。', '弊社担当者からお見積もりをさせて頂きますので、サポートセンターまでお問い合せください。\r\nサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('50', '3', '1', '2', 'ファイアウォール（リバースプロキシやNAT）からの接続に対応していますか？', 'はい。ただしプロキシ経由の接続の場合には、プロキシの種類によって、映像・音声の遅延が発生する場合がございます。　対応策がございますので、別途お問い合わせください。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('51', '3', '3', '1', '推奨はブロードバンド環境（上り128kbps、下り384kbps以上）とありますが、これ以下の回線速度の場合、どのような不都合が発生するのでしょうか？', '音声遅延や、映像遅延などが発生いたします。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('52', '3', '1', '1', 'PHS移動通信（AirH″などのモバイルカード）からの利用は可能ですか？', '弊社では、動作の保証をしておりませんが、利用する事は可能です。 \r\n「低速回線モード」、「音声優先モード」、「利用帯域設定」など、回線速度の低い環境に合わせた設定も可能です。\r\nただし、PHS環境（AirH\"など）の場合には、128kbps接続でもパケット処理などにより、実質的には64kbps以下の速度しか出ないケースが多い為、実質的な利用は困難なケースがございます。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('53', '1', '1', '8', 'セキュリティは万全なのでしょうか？', 'はい。映像と音声は非公開の独自プロトコルで通信しており、テキストチャットやファイル共有などドキュメントの部分は標準でSSLで通信しておりますので、限りなく万全な設計になっております。また、オプションサービスであるSSL対応サービスをご利用頂くことで、全ての通信をSSL化することも可能です。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('54', '3', '1', '2', '映像と音声のコーデックは何を使用していますか？', 'ビデオのエンコード・デコードは、Flashの機能により行われております。\r\nその機能で使用されているコーデックはSorenson Media社のSorenson Sparkコーデックです。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('55', '3', '1', '3', '使用しているプロトコルは何でしょうか？', 'Flash Media Serverで使用される、Adobe社独自のプロトコルRTMPを使用しております。\r\n※RTMPはFlash Media Serverで使用される、Adobe社独自のプロトコルです。\r\n詳細は、Adobe社のウェブページ（http://www.adobe.com/jp/products/flashmediaserver/）をご覧ください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('56', '3', '1', '4', '映像・音声・データのやりとりや、ログインＩＤなどは、どのようなセキュリティ対応をされているのか教えてください。', 'ビデオストリーム以外の全ての通信は、SSL通信を利用しております。\r\nビデオストリームは独自の通信を行っており、プロトコル仕様は公開されていないため現状でも安全ですが、オプション機能により、ビデオストリームのSSＬ化も可能です。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('57', '3', '1', '5', 'カメラは何が使えますか？推奨カメラなどはありますか？', 'USBカメラを利用することが可能です。\r\n詳細は、動作確認済みカメラをご覧ください。\r\nhttp://www.nice2meet.us/ja/tools/', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('58', '1', '2', '19', '10拠点で利用する場合の必要帯域は？', '上り256Kbps、下り512Kbpsとなります。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('59', '1', '2', '20', 'フレームレートはいくつですか？', '最大15fpsとなっております。\r\n会議参加人数に合わせて、自動的に最適なフレームレートになります。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('60', '1', '1', '20', '最低利用期間など、利用期間に関する制限はありますか？', 'いいえ、制限は一切ございません。 \r\n契約期間の制限はございませんので、月単位のご利用が可能です。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('61', '1', '1', '21', '海外でも利用できますか？', 'はい、可能です。ウェブベースでのテレビ会議システムであるため、インターネットをご利用することが可能であれば、どなたでも参加して頂くことが可能です。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('62', '4', '1', '1', '回線速度を測定中のまま進まない', '回線速度が非常に遅い、あるいは回線を確保出来ていない可能性がございます。\r\n再度、入室していただきますと回線を再取得いたしますので、問題を解決する場合がございます。\r\n上記の方法で解決しない場合は、当社の推奨環境（回線、PCスペック）を満たしていない場合がございますので、弊社推奨環境をご確認ください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('63', '4', '1', '2', '回線速度を測定する画面でエラーが表示される（プロキシ）', '回線速度が非常に遅い、あるいは回線を確保出来ていない可能性がございます。\r\n再度、入室していただきますと回線を再取得いたしますので、問題を解決する場合がございます。\r\n上記の方法で解決しない場合は、当社の推奨環境（回線、PCスペック）を満たしていない場合がございますので、弊社推奨環境をご確認ください。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('64', '4', '1', '3', '毎回、会議中に「切断されました」となってしまう', '当システムでは、会議サーバに対して一定時間毎に「正常に会議が行われている」ことを通信しております。そのため、遅延が著しく発生している場合、サーバ側が接続していない状況と誤認し切断する場合がございます。会議では遅延を引き起こさなくても根本的な原因は遅延と同じになります。\r\n※プロキシが存在する場合や回線環境、PCスペックが当社製品の動作環境を下回っている可能性があります。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('65', '4', '1', '14', '映像の縦横比がおかしい', 'ご使用中のビデオデバイスによっては、映像取得サイズを変更した際に映像の縦横比が変更されたり、上下に黒い帯が入る可能性があります。デバイスの性能や設定等をご確認ください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('66', '4', '1', '4', '遅延がひどい', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)回線は何をお使いですか？\r\n\r\n→ISDN　or　ダイヤルアップ\r\n推奨の回線速度が維持できません。快適で安価なBフレッツをお勧めいたします。\r\n\r\n→ADSL／Bフレッツ／その他、光やケーブルテレビなど　（3へ進む）\r\n\r\n\r\n3)プロキシ経由の接続ですか？\r\n\r\n→プロキシ経由です。\r\nプロキシ経由の接続の場合、プロキシサーバの特性上、遅延が発生する可能性があります。ただし、1935ポートを外向きに開けていただければプロキシサーバを介さずに通信ができますので、遅延の回避が可能となります。詳細はネットワーク管理者にその旨ご相談ください。\r\n\r\n→プロキシ経由ではない（4へ進む）\r\n\r\n\r\n4)お使いのパソコンは、PentiumIIIの533MHz以上ですか？\r\n\r\n→いいえ。\r\n推奨のスペックを満たしておりません。映像などを処理するに当たり、ある程度のスペックが要求されます。大変恐縮ではございますが、推奨環境以上のパソコンでご利用ください。\r\n\r\n→はい。（5へ進む）\r\n\r\n\r\n5)何かほかのアプリケーションが起動していませんか？\r\n\r\n→はい。\r\nシステムのリソース不足の可能性がございますので、他のアプリケーションを終了してください。\r\n\r\n→いいえ。\r\nご利用のパソコン自体に搭載されておりますビデオチップが遅延の原因かと思われます。少し古いタイプやノートパソコンなどでは映像を見るに適したスペックになっていないことがあります。お手数ではございますが再度、別のパソコンでご利用ください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('67', '4', '1', '5', '自分の映像が表示されない', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)カメラが接続されていますか？\r\n\r\n→接続されていない。\r\nUSBカメラもしくは、IEEE1394のカメラを接続してください。\r\n\r\n→接続されている（3へ進む）\r\n\r\n\r\n3)カメラはOSに認識されていますか？\r\n\r\n→認識されていない。\r\nカメラのマニュアルに従い、ドライバをインストールするなどして、カメラを認識させてください。\r\n\r\n→認識されている。（4へ進む）\r\n\r\n\r\n4)「カメラ切り替え」ボタンを何度かクリックして映像が表示されますか？\r\n\r\n→表示される。\r\nカメラが複数台つながっている場合や、OSの状態によってはカメラ切り替えボタンを押さないとカメラが利用できない場合がございます。\r\n\r\n→表示されない。（5へ進む）\r\n\r\n\r\n5)「会議室に入室」ボタンを押した後に表示される小さなウィンドウで、「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n会議ウィンドウを閉じて、再度、会議室に入室してください。\r\nその際に、必ず「許可」をクリックするようにしてください。\r\n\r\n→クリックした。（6へ進む）\r\n\r\n\r\n6)ほかにカメラを使うアプリケーションは起動していませんか？\r\n\r\n→起動している。\r\nカメラを使う他のアプリケーションを終了し、会議ウィンドウを閉じて、再度、会議室に入室してください。\r\n\r\n→起動していない。\r\n対応していないカメラである可能性か、カメラが壊れている可能性がございます。また、パソコンの再起動を一度お試しください。カメラの不具合につきましては各メーカーへお問い合わせください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('68', '4', '1', '6', '相手の映像が表示されない', '1)相手の方にN2MY Checker2でご利用環境の診断を行って頂いてください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)相手がログインしていますか？\r\n\r\n→ログインしていない。\r\n相手の方へ会議室への入室のお願いをしてください。\r\n\r\n→ログインしている。（3へ進む）\r\n\r\n\r\n3)相手は相手自身の映像を相手のパソコン上で見えていますか？\r\n\r\n→見えていない。\r\n相手の映像が相手のPC上で表示されるように設定してください。\r\n（『自分の映像が表示されない』を参照）\r\n\r\n→見えている。（4へ進む）\r\n\r\n\r\n4)相手の名前は表示されていますか？\r\n\r\n→表示されている。\r\n何らかのシステムトラブルの可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示されていない。（5へ進む）\r\n\r\n\r\n5)チャットウィンドウに文字を打ち込んで送信を押した後、チャットウィンドウにその内容が表示されますか？\r\n\r\n→表示されない。\r\nお客様が正常に接続できていない可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示される。\r\n相手が正常に接続できていない可能性がございます。\r\n相手に会議室への再入室のお願いをしてください。（『自分の映像が表示されない』を参照）', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('69', '4', '1', '7', '自分の音声が相手に届かない', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)パソコンにマイクは接続されていますか？\r\n\r\n→接続されていない。\r\nパソコンにマイクを接続し、会議室へ再入室してください。\r\n\r\n→接続されている。（3へ進む）\r\n\r\n\r\n3)会議開始後に表示される小さなウィンドウで「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n再度会議室に入室し直してください。その際に必ず「許可」をクリックしてください。\r\n\r\n→クリックした。（4へ進む）\r\n\r\n\r\n4)パソコンのマイクがミュートの設定になっていませんか？\r\n\r\n→なっている。\r\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。\r\n\r\n→なっていない。（5へ進む）\r\n\r\n\r\n5)[コントロールパネル]→[サウンドとオーディオデバイス]→[オーディオ]→[録音]→[音量]→[録音コントロール]の画面で「マイク」にチェックがはいっていますか？\r\n\r\n→入っていない。\r\n「マイク」にチェックをいれてください。\r\n\r\n→入っている。（6へ進む）\r\n\r\n\r\n6)マイク切り替えボタンをクリックして、相手に自分の音声が届くようになりますか？\r\n\r\n→届く。\r\nマイクが複数台つながっている場合や、OSの状態によってはマイク切り替えボタンを押さないとマイクが利用できない場合があります。\r\n\r\n→届かない。\r\nマイクが故障しているか、お使いのコンピュータでは使えないマイクの可能性があります。また、パソコンの再起動を一度お試しください。マイクの不具合につきましては各メーカーへお問い合わせください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('70', '4', '1', '8', '相手の音声が自分に届かない（音が出ない）', '1)N2MY Checker2でご利用環境の診断を行ってください。\r\n※N2MY Checker2についてはN2MY Checker2ご利用マニュアルをご参照下さい。\r\n\r\n\r\n2)スピーカもしくはヘッドフォンはつながっていますか？\r\n\r\n→つながっていない。\r\nスピーカもしくはヘッドフォンを接続し、会議室へ再入室してください。\r\n\r\n→つながっている（3へ進む）\r\n\r\n\r\n3)パソコンのスピーカもしくはヘッドフォンがミュートになっていませんか？\r\n\r\n→ミュートになっている。\r\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。\r\n\r\n→ミュートになっていない（4へ進む）\r\n\r\n\r\n4)パソコン本体のボリュームがOFFになっていませんか？\r\n\r\n→なっている。\r\nボリュームをあげてください。（設定ではなく、パソコン本体のボリューム）\r\n\r\n→なっていない。\r\n相手の設定が正常でない可能性があります。\r\n相手側にFAQの『自分の音声が相手に届かない。』を確認してください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('71', '4', '1', '9', '音が小さいのですが、どうすればいいでしょうか？', 'ミュートもしくはボリュームが小さくなっていませんか？\r\n[コントロールパネル]→[サウンドとオーディオデバイス]→[音量]の画面でマイクのミュートをはずし、ボリュームを高に設定してください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('72', '4', '1', '10', '自分の声がエコーしてしまいます。直し方はありますか？', '相手側がスピーカーを使っていませんか？\r\n\r\n→使用している。\r\nあなたの声を相手側のマイクが拾っています。ヘッドセットをご利用頂ければエコーは無くなります。相手側でスピーカーの使用がやむを得ない場合は、エコーキャンセラー付きのマイクが必要になります。\r\n参考：エコーキャンセラー付きマイク　ClearOne社　ACCUMICII\r\n\r\n→使用していない。\r\n相手のヘッドセットが安価な物だと、多少自分の声が戻ってくる可能性がございます。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('73', '4', '1', '11', '相手の声がノイズが多くてよく聞こえない', '1)相手はヘッドセットを使用していますか？\r\n\r\n→使用していない。\r\nヘッドセットをご利用頂いてください。\r\n\r\n→使用している（2へ進む）\r\n\r\n\r\n2)相手のヘッドセットのマイクが、音声集音タイプですか？\r\n\r\n→はい。\r\n相手のコンピュータに何か問題がある可能性がございます。PCの再起動のお願いをしてください。\r\n\r\n→いいえ。\r\n現在の物より高性能なヘッドセットをご利用頂けますようお勧めいたします。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('74', '4', '1', '12', 'クリックしてもページが表示されない（ポップアップ表示がブロックされてしまう）', '「Windows XP Service Pack 2」をご利用されている方は、ポップアップ表示がブロックされてしまいます。\r\nブラウザのツールバーより「ツール」を選択、メニューから「ポップアップブロック」→「ポップアップブロックの設定」を選択し、クリックします。\r\n「許可するWebサイトのアドレス」に『www.nice2meet.us』と入力し、「追加」ボタンをクリックします。\r\n「閉じる」ボタンをクリックし、設定ウインドウを閉じます。\r\n\r\n以上で設定は完了です。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('75', '2', '3', '1', '初期費用はいくらですか？', 'ご利用のプランによって異なりますので、カスタマーサポートセンターまでお問合せ下さい。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('76', '2', '3', '2', '料金体系の詳細をおしえてください。', '月額 29,900円（税別）から各種プランをご用意しております。\r\n1つのアカウントで、オンラインのセミナールームをを１つお貸しするようなイメージになるとお考えいただければと思います。\r\n詳細は料金プラン一覧をご覧ください。', '2', '0', '1');
INSERT INTO `faq_list` VALUES ('77', '2', '3', '3', '解約後に、再開する場合は、初期費用はかかるのでしょうか？', 'はい、再度設定しますので、初期費用は発生します。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('78', '1', '3', '3', '1契約で複数のセミナーを同時に開催することはできますか？', 'いいえ、出来ません。別途、追加でセミナールームの契約ができますので、お問い合わせフォームからお問い合わせいただくか、カスタマーサポートセンターにお問い合わせください。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('79', '5', '1', '1', '推奨のマイクなどありますか？', 'USBタイプの利用を推奨いたします。\r\nピンジャックタイプですとノイズが発生しやすくなります。\r\niSightや本体マイクでも会議可能ですが、ハウリングやエコーが発生しやすくなります。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('80', '5', '1', '2', '会議中に他の作業をしていると、会議の処理が遅いように感じるのですが？', '最前面での利用を推奨いたします。\r\nOSやブラウザの設定によっては、会議の画面がバックグランドに回った際の処理が遅くなるため、常に最前面で利用してください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('81', '5', '1', '3', '遅延がひどい', '1)回線は何をお使いですか？\r\n\r\n→ISDN　or　ダイヤルアップ\r\n推奨の回線速度が維持できません。快適で安価なBフレッツをお勧めいたします。\r\n\r\n→ADSL／Bフレッツ／その他、光やケーブルテレビなど（2へ進む）\r\n\r\n\r\n2)プロキシ経由の接続ですか？\r\n\r\n→プロキシ経由です。\r\nプロキシ経由の接続の場合、プロキシサーバの特性上、遅延が発生する可能性があります。ただし、1935ポートを外向きに開けていただければプロキシサーバを介さずに通信ができますので、遅延の回避が可能となります。詳細はネットワーク管理者にその旨ご相談ください。\r\n\r\n→プロキシ経由ではない（3へ進む）\r\n\r\n\r\n3)お使いのパソコンは推奨環境を満たしていますか？\r\n\r\n→いいえ。\r\n映像などを処理するに当たり、ある程度のスペックが要求されます。大変恐縮ではございますが、推奨環境以上のパソコンでご利用ください。\r\n\r\n→はい。（4へ進む）\r\n\r\n\r\n4)何かほかのアプリケーションが起動していませんか？\r\n\r\n→はい。\r\nシステムのリソース不足の可能性がございますので、他のアプリケーションを終了してください。\r\n\r\n→いいえ。\r\nご利用のパソコン自体に搭載されておりますビデオチップが遅延の原因かと思われます。少し古いタイプやノートパソコンなどでは映像を見るに適したスペックになっていないことがあります。お手数ではございますが再度、別のパソコンでご利用ください。(5へ進む)\r\n\r\n\r\n5)ウィンドウが最前面になっていない。\r\n\r\nMacは最前面のウィンドウの処理を優先にする為、ウィンドウがバックグラウンドに回ると極端に処理が遅くなる場合があります。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('82', '5', '1', '4', '自分の映像が表示されない', '1)カメラが接続されていますか？\r\n\r\n→接続されていない。\r\nUSBカメラもしくは、IEEE1394のカメラを接続してください。\r\n\r\n→接続されている（2へ進む）\r\n\r\n\r\n2)カメラはOSに認識されていますか？\r\n\r\n→認識されていない。\r\nカメラのマニュアルに従い、ドライバをインストールするなどして、カメラを認識させてください。\r\n\r\n→認識されている。（3へ進む）\r\n\r\n\r\n3)「カメラ切り替え」ボタンを何度かクリックして映像が表示されますか？　\r\n\r\n→表示される。\r\nカメラが複数台つながっている場合や、OSの状態によってはカメラ切り替えボタンを押さないとカメラが利用できない場合がございます。\r\n※Macでは標準でカメラドライバを持っている為、USBカメラ利用の場合は必ず切り替えを行う必要があります。\r\n\r\n→表示されない。（4へ進む）\r\n\r\n\r\n4)「会議室に入室」ボタンを押した後に表示される小さなウィンドウで、「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n会議ウィンドウを閉じて、再度、会議室に入室してください。\r\nその際に、必ず「許可」をクリックするようにしてください。\r\n\r\n→クリックした。（5へ進む）\r\n\r\n\r\n5)ほかにカメラを使うアプリケーションは起動していませんか？\r\n\r\n→起動している。\r\nカメラを使う他のアプリケーションを終了し、会議ウィンドウを閉じて、再度、会議室に入室してください。\r\n\r\n→起動していない。\r\n対応していないカメラである可能性か、カメラが壊れている可能性がございます。また、パソコンの再起動を一度お試しください。カメラの不具合につきましては各メーカーへお問い合わせください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('83', '5', '1', '5', '相手の映像が表示されない', '1)相手がログインしていますか？\r\n\r\n→ログインしていない。\r\n相手の方へ会議室への入室のお願いをしてください。\r\n\r\n→ログインしている。（2へ進む）\r\n\r\n\r\n2)相手は相手自身の映像を相手のパソコン上で見えていますか？\r\n\r\n→見えていない。\r\n相手の映像が相手のPC上で表示されるように設定してください。\r\n（『自分の映像が表示されない』を参照）\r\n\r\n→見えている。（3へ進む）\r\n\r\n\r\n3)相手の名前は表示されていますか？\r\n\r\n→表示されている。\r\n何らかのシステムトラブルの可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示されていない。（4へ進む）\r\n\r\n\r\n4)チャットウィンドウに文字を打ち込んで送信を押した後、チャットウィンドウにその内容が表示されますか？\r\n\r\n→表示されない。\r\nお客様が正常に接続できていない可能性がございます。\r\n再度、会議室に入室し直してください。\r\n\r\n→表示される。\r\n相手が正常に接続できていない可能性がございます。\r\n相手に会議室への再入室のお願いをしてください。（『自分の映像が表示されない』を参照）', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('84', '5', '1', '6', '自分の音声が相手に届かない', '1)パソコンにマイクは接続されていますか？\r\n\r\n→接続されていない。\r\nパソコンにマイクを接続し、会議室へ再入室してください。\r\n\r\n→接続されている。（2へ進む）\r\n\r\n\r\n2)会議開始後に表示される小さなウィンドウで「許可」をクリックしましたか？\r\n\r\n→クリックしていない。\r\n再度、会議室に入室し直してください。その際に必ず「許可」をクリックしてください。\r\n\r\n→クリックした。（3へ進む）\r\n\r\n\r\n3)パソコンのマイクがミュートの設定になっていませんか？\r\n\r\n→なっている。\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[入力]→[主音量]→[消音]にチェックを外してください。\r\n\r\n→なっていない。（4へ進む）\r\n\r\n\r\n4)マイク切り替えボタンを何度かクリックして、相手に自分の音声が届くようになりますか？\r\n\r\n→届く。\r\nMacでは、マイク切り替えボタンを押さないとマイクが利用できない場合があります。\r\n\r\n→届かない。\r\nマイクが故障しているか、お使いのコンピュータでは使えない可能性があります。また、パソコンの再起動を一度お試しください。マイクの不具合につきましては各メーカーへお問い合わせください。\r\nMacの音声入力はほとんどの場合、音声入力端子が使えない場合があります。当社ではUSBアダプタを必ずご使用になる事を推奨しております。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('85', '5', '1', '7', '相手の音声が自分に届かない（音が出ない）', '1)スピーカもしくはヘッドフォンはつながっていますか？\r\n\r\n→つながっていない。\r\nスピーカもしくはヘッドフォンを接続し、会議室へ再入室してください。\r\n\r\n→つながっている（2へ進む）\r\n\r\n\r\n2)パソコンのスピーカもしくはヘッドフォンがミュートになっていませんか？\r\n\r\n→ミュートになっている。\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[出力]→[主音量]→[消音]のチェックを外して、ボリュームを上げてください。\r\n\r\n→ミュートになっていない（3へ進む）\r\n\r\n\r\n3)パソコン本体のボリュームがOFFになっていませんか？\r\n\r\n→なっている。\r\nボリュームをあげてください。（設定ではなく、パソコン本体のボリューム）\r\n\r\n→なっていない。\r\n相手の設定が正常でない可能性があります。\r\n相手側にFAQの『自分の音声が相手に届かない。』の確認をお願いをしてください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('86', '5', '1', '8', '音が小さいのですが、どうすればいいでしょうか？', 'ミュートもしくはボリュームが小さくなっていませんか？\r\n[システム環境設定]→[ハードウェアの項目]内→[サウンド]を選択→[入力]→[入力音量]→[消音]のチェックを外して、ボリュームを上げてください。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('87', '5', '1', '9', '自分の声がエコーしてしまいます。直し方はありますか？', '相手側がスピーカーを使っていませんか？\r\n\r\n→使用している。\r\nあなたの声を相手側のマイクが拾っています。ヘッドセットをご利用頂ければエコーは無くなります。相手側でスピーカーの使用がやむを得ない場合は、エコーキャンセラー付きのマイクが必要になります。\r\n\r\n→使用していない。\r\n相手のヘッドセットが安価な物だと、多少自分の声が戻ってくる可能性がございます。', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('148', '1', '2', '18', '無料トライアルアカウントでH.323TV会議システムへの接続（N2MY H.323）も利用できますか？', '原則、ご利用頂けませんが、お問い合せ頂ければH.323TV会議システムへの接続（N2MY H.323）をご利用頂ける無料トライアルアカウントを発行することも可能となっております。 カスタマーサポートセンターまでお問合せ下さい。\r\nカスタマーサポートセンター：0570-00-2192', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('88', '1', '1', '100', 'Is it possible to connect more than 10 locations?', 'Yes, up to 49 locations can be connected simultaneously by using the optional “Audience” feature. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('89', '1', '1', '101', 'Is it possible to try the service before purchasing it?', 'Yes, you can sign up online for a 2 week trial account. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('90', '1', '1', '102', 'What should I do if I forget my ID and password?', 'Contact us by phone (0570-00-2192) or e-mail.  We can send you the information once we confirm your identity. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('91', '1', '1', '102', 'Can I use nice to meet you on a Mac?', 'Yes, but you can not use the ‘Sharing’ and ‘Checker’ features on a Mac. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('92', '1', '1', '103', 'Can I setup nice to meet you within an intranet?', 'Yes, you can purchase a server to run nice to meet you on your intranet for added security.  Please contact our sales team for more details. (0570-00-2192)', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('93', '1', '1', '104', 'Can I rent a web camera? ', 'Normally we do not offer rental of web cameras, but we can make exceptions.  Please ask our sales team for more information.  (0570-00-2192) ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('94', '1', '1', '105', 'What kind of security is used for login-ID, video, voice and data communication?', 'SSL is used for all the communication except video and voice.  Video and voice are secured by our original protocol, which is proprietary.  N2MY SSL is an optional feature makes it possible to make all communication more secure. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('95', '1', '1', '106', 'How can I start my meeting without disclosing my ID & password?', 'By using the ‘INVITATION’ feature you can e-mail an invitation with an automatically generated hyperlink to all your guests.  This will allow your guests to have ‘one-click access’ into the room.  Not only can you secure your ID & password, but it’s hassle free for your guests.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('96', '1', '1', '107', 'Is it possible to participate in a meeting without an ID? ', 'Yes, it is possible to participate in a meeting using a hyperlink sent to you by ‘e-mail invitation’.   *An e-mail address is required to receive the invitation. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('97', '1', '2', '100', 'How many IDs and passwords do I get per contract?', 'One ID and password is issued per contract.  You may share the ID and password with as many users as you’d like. It may be easier for you to think of it this way; you are renting a ‘virtual room’ and you have the right to allow anyone to use it. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('98', '1', '2', '101', 'How can I start my meeting without disclosing my ID & password?', 'By using the ‘INVITATION’ feature you can e-mail an invitation with an automatically generated hyperlink to all your guests.  This will allow your guests to have ‘one-click access’ into the room.  Not only can you secure your ID & password, but it’s hassle free for your guests.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('99', '1', '2', '102', 'Is it possible to connect more than 10 locations?', 'Yes, up to 49 locations can be connected simultaneously by using the optional “Audience” feature.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('100', '1', '2', '103', 'Is it possible to participate in a meeting without an ID? ', 'Yes, it is possible to participate in a meeting using a hyperlink sent to you by ‘e-mail invitation’.   *An e-mail address is required to receive the invitation. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('101', '1', '2', '104', 'Can I have several meetings simultaneously with one account ID & password?', 'No, you can only have one meeting at a time per account ID & password.  Basically, the concept is that you are renting one ‘room’, therefore, you can only have one meeting at a time.  Please, contact the support center if you would like to create another ‘room’ or account ID & password. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('102', '1', '2', '105', 'Is there a minimum contract term?', 'No, there is no minimum contract term.  It is on a month-to-month basis.  ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('103', '1', '2', '106', 'Can I share files and applications running on my PC?', 'Yes, you can activate the desktop sharing feature and show the documents and applications running on your desktop.  This feature will also enable people to take control of your PC. N2MY Sharing is an optional feature. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('104', '1', '2', '107', 'Can I copy the contents of the whiteboard to a clipboard or vice versa?', 'No. There is only a File/picture upload on the whiteboard feature.\r\nYou can display files such as Word, Excel, PowerPoint, PDF, BMP, PNG, JPEG, and GIF on the whiteboard. \r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('105', '1', '2', '108', 'How many people can participate in a meeting at the same time?  ', 'You can have up to 10 people attend a meeting with video and voice.  If you use the ‘Audience’ feature you can have up to 49 locations connected at the same time. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('106', '1', '2', '109', 'Tell me the advantages of using V-cube meeting over other services.', 'There are 3 big differences: \r\n1. No need for installation\r\n2. Cross platform, you can use it on a Macintosh as well as on Windows PC.  It can also connect to H.323 compliant terminals.\r\n3. Bypass proxy and firewall.  No need for any changes in settings for company LAN.  \r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('107', '1', '1', '110', 'Do I need any special equipment for V-cube Meeting?', 'All you need is broadband internet connection, computer, web camera and a headset. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('108', '1', '2', '112', 'What happens if a server goes down, how do you respond to it?', 'In the event that a server goes down, we have backup servers in multiple locations that will keep our service running 24/7.  ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('109', '1', '2', '113', 'Can I try the conference room package before purchasing it?', 'Normally we do not offer a free trial for our conference room package, but we have made exceptions in the past with money back guarantee deposit.  Please ask our sales team for more information. (0570-00-2192) ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('110', '1', '2', '114', 'Can I connect V-cube Meeting to the web conferencing system I currently have?', 'If your current system uses an H.323 compliant terminal, the answer is yes.  But you must sign up for the H.323 optional service. Please contact our support team for more details.  ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('111', '1', '2', '115', 'Can I try the N2MY mobile service with a free trial account? ', 'Normally we do not offer a free trial for N2MY mobile, but we can make exceptions.  Please ask our sales team for more information.  (0570-00-2192) ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('112', '1', '2', '116', 'How much bandwidth do I need for a conference between 10 locations?', 'You need at least 256kbps upstream and 512kbps downstream.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('113', '1', '2', '117', 'How many frame rates does nice to meet you get?', 'It automatically changes depending on the number of participants connected. But the maximum frame rate you can get is 15fps.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('114', '2', '2', '100', 'Is there an initial cost?', 'Yes, there is a one-time activation fee of 45,000 Yen for any plan. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('115', '2', '2', '101', 'Tell me about conferencing package.', 'Please ask our support team to get an estimate.  (0570-00-2192) ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('116', '2', '2', '101', 'Please tell me the details about your pricing. ', 'We have several plans starting from 9,900 Yen (tax included) per month.  It is a flat fee no matter how many participants there are.  It is like renting a virtual conference room online with an account and you are free to use it as you like. See pricing details for more info.  ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('117', '2', '2', '102', 'Can I change the service plan?  Can I change the plan within one month?', 'You can change the service plan in the middle of the month.  However, the change will be effective until the first day of the following month.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('118', '2', '2', '104', 'Do I have to pay the initial cost again to re-start the service once I close an account?', 'Yes, we charge the initial setup cost of 45,000 Yen. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('119', '2', '2', '105', 'Is there a limitation on the number of users in a room?', 'This will depend on the type of price plan you purchased.  Please contact our support team to find out what the limitations are on each plan.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('120', '3', '1', '100', 'Can I use nice to meet you with PHS mobile? (AirH or P-in)', 'It is not guaranteed by company, but you can use it with PHS mobile.  It would be difficult to use it because of the network speed is usually lower than 64kbps.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('121', '3', '1', '101', 'Can I use V-cube’s service even if my PC is inside a firewall (reverse proxy or NAT)?', 'Yes, our service was designed to bypass firewalls. However, if you are connecting via a proxy server you may experience slower connection speeds.  Such delay can be avoided by opening port 1935 outward.  For more details please contact our support team.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('122', '3', '1', '102', 'What kind of video/voice codec is used? ', 'Flash is used for video encode/decode.  The codec used in the feature is the Sorenson Spark codec made by Sorenson Media.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('123', '3', '1', '103', 'What kind of protocols and ports are used?', 'RTMP is Macromedia\'s original protocol used in Flash Communication Server.  For more details, please refer to the Macromedia website.', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('124', '3', '1', '104', 'What kind of security is used for login-ID, video, voice and data communication? ', 'SSL is used for all the communication except video and voice.  Video and voice are secured by our original protocol, which is proprietary.  N2MY SSL is an optional feature makes it possible to make all communication more secure. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('125', '3', '1', '105', 'Which web cameras do you recommend? ', 'Our system is easiest to use with a standard USB web cam. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('126', '4', '1', '100', 'I can not go into the meeting room because the program stops where it measures the connection speed.', 'This may occur because the internet connection speed is too slow or the performance of the PC is slow.  Please try logging in again.  If it occurs again, the PC may not satisfy minimum system requirements (internet connection or specification).  Please refer to our requirements for more details. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('127', '4', '1', '101', 'I see an error message when the PC connection speed is measured. （Proxy） ', 'You might not be able to use this service in your network environment.  There are a few possible reasons:\r\n1. Your network’s proxy server is old.\r\n2. The security setting of the network is too high.\r\nTo resolve this please ask our support team.  We can suggest the best way to accommodate your network environment.\r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('128', '4', '1', '102', 'I am disconnected during the meeting each time I use it. ', 'The system regularly reports to the server that there is a meeting taking place.  If the server does not get the report because of the delay, it sometimes disconnects.  The basic reason is delay. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('129', '4', '1', '103', 'There is a huge delay. ', '1) Check the environment of your PC using N2MY Checker.  \r\n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker.\r\n\r\n2) How is your PC Connected?\r\n→ISDN　or　Dial up\r\nThe speed of your PC connection is less than what we recommend.\r\n→ADSL／DSL／T-1（Go to 3）\r\n3)Is your connection through a proxy?\r\n→Yes.  It is through a proxy.\r\nIf you are connecting via a proxy server you may experience slower connection speeds. Such a delay can be avoided by opening port 1935 outward. Please talk to your IT department.\r\n→No.  It is not through a proxy. （Go to 4）\r\n4)Does your PC have a processor newer than a Pentium III 533 MHz?\r\n→No.\r\nYour PC is not fast enough.  Your must have a PC that meets or exceeds our minimum system requirements.\r\n→Yes.（Go to 5）\r\n5) Are other applications open?\r\n→Yes.\r\nClose other applications, the system may be out of resources. \r\n→No. \r\nIt seems that the video tip is the reason for the delay.  Your PC cannot see video images because it is too old or it is a notebook.  Please try using another PC.  \r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('130', '4', '1', '104', 'My picture cannot be seen. ', '1) Check the environment of your PC using N2MY Checker.  \r\n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker. \r\n2) Is there a web camera connected?\r\n→No.  \r\nConnect USB camera or IEEE 1934 camera.\r\n→Yes. （Go to 3） \r\n3) Does your OS recognize the camera?\r\n→No.\r\nMake your OS recognize the camera by installing the driver.\r\n→Yes.  （Go to 4） \r\n4) Can you see your picture when you click the “camera switch button” which is located under the frame where your picture is supposed to be?\r\n→Yes.\r\nSometimes you have to click the camera switch button to display your picture.\r\n→No.  （Go to 5） \r\n5)Did you click “approve” on the small window that pops up after clicking the meeting start button?\r\n→No.\r\nClose the meeting window and start it again.\r\n→Yes.  （Go to 6） \r\n6)Are other applications for the camera open?\r\n→Yes.\r\nClose the application for camera and click the meeting start button again.\r\n→No.\r\nThe camera might not be compatible, or it may be broken.  Try restarting your PC, if that does not work, contact the camera maker’s technical support. \r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('131', '4', '1', '105', 'I cannot see other participants’ pictures. ', '1) Check the environment of other participants’ PCs using N2MY Checker.  \r\n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker.\r\n2) Are they logged in? \r\n→No. Ask them to login. \r\n→Yes. （Go to 3） \r\n3) Can they see their own pictures on their PCs? \r\n→ No. they should set it up so they can see their pictures on their own PCs.  (Refer to above “Self picture can not be seen”.) \r\n→ Yes. （Go to 4） \r\n4) Can you see their names? \r\n→No. There might be some systematic trouble.  Try starting the meeting again. \r\n→Yes. （Go to 5） \r\n5) Can you see the text you type on the chat window?\r\n→No. You might not be connected.  Try starting the meeting again.\r\n→Yes.  Other participants might not be connected.  Try starting the meeting again.  (Refer to above “Self picture cannot be seen”.) \r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('132', '4', '1', '106', 'Other participants cannot hear my voice. ', '1) Check the environment of your PC using N2MY Checker.  \r\n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker.  \r\n2) Is a microphone connected to your PC? \r\n→No.  Connect a microphone to your PC. \r\n→Yes.  （Go to 3） \r\n3) Did you click “approve” on the small window which pops up after clicking the meeting start button? \r\n→No.  Close the meeting window and start it again.  You have to click “approve” on the small window. \r\n→Yes.  （Go to 4） \r\n4) Is the microphone mute is set up? \r\n→Yes. Uncheck the microphone mute and set the volume high on the Sounds and Audio Devices section of the control panel. \r\n→No.  （Go to 5） \r\n5)Go to control panel.>Find the sounds and audio devices.> Audio>Recording>Click the Volume>Is the box of microphone checked? \r\n→No.  Check the box of the microphone. \r\n→Yes.  （Go to 6） \r\n6)Can others hear you speaking when you click the microphone switch button? \r\n→Yes.  Sometimes you have to click the microphone switch button, especially when there are a few microphones connected to a PC or because of the state of the OS. \r\n→No.  The microphone might not be compatible, or it might be broken.  Try restarting you PC.  If that does not work, contact the camera maker’s technical support. \r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('133', '4', '1', '107', 'I cannot hear the other participants’ voices.   ', '1) Check the environment of your PC using N2MY Checker.  \r\n※Refer to N2MY Checker Manual for seeing how to use N2MY Checker. \r\n2) Is either a speaker or a headphone connected to your PC?\r\n→No.  Connect either a speaker or a headphone to your PC.  \r\n→Yes.  (Go to 3)\r\n 3) Is the speaker or headphone mute set up? \r\n→Yes.  Uncheck the microphone mute and set the volume to high on the Sounds and Audio Devices section of control panel. \r\n→No.  (Go to 4) \r\n4) Is the volume of the PC off?\r\n→Yes.  Turn up the volume. (Be sure to turn up the PC volume.) \r\n→No.  The set up of other users might not be normal.  Ask them to check. (Refer to above \"Other participants cannot hear my voices\")\r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('134', '5', '1', '100', 'Which microphone can I use? Which do you recommend? ', 'We recommend using USB microphones.  (Normally there are no sound input terminals.  If there are, they cannot be used in most cases.)  You may experience howling or echo with iSight or computer microphone. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('135', '5', '1', '101', 'I feel a delay when opening other applications in the front. ', 'We recommend using our system in the front.  There may be delay when the window goes to the background since the processing order is from front to back on a Mac. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('136', '5', '1', '102', 'There is a huge delay.  ', '1) How is your PC Connected?\r\n→ISDN　or　Dial up\r\nThe speed of your PC connection is less than what we recommend.\r\n→ADSL／DSL／T-1（Go to 2）\r\n2)Is your connection through a proxy?\r\n→Yes.  It is through a proxy.  If you are connecting via a proxy server you may experience slower connection speeds. Such a delay can be avoided by opening port 1935 outward. Please talk to your IT department.\r\n→No.  It is not through a proxy. （Go to 3）\r\n3)Does your Mac meet our system requirements? \r\n→No.  Please use a computer that meets our system requirements. \r\n→Yes.  (Go to 4) \r\n4) Are other applications open?\r\n→Yes.  Close other applications, the system may be out of resources. \r\n→No.  It seems that the video tip is the reason for the delay.  Your PC cannot see video images because it is too old or it is a notebook.  Please try using another PC.  (Go to 5) \r\n5) The window is not in the front.  There may be delay when the window goes to the background since the processing order is from front to back on a Mac.\r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('137', '5', '1', '103', 'My picture cannot be seen. ', '1) Is a web camera connected?\r\n→No.  Connect USB camera or IEEE 1934 camera.\r\n→Yes. （Go to 2）\r\n2) Does your OS recognize the camera?\r\n→No.\r\nMake your OS recognize the camera by installing the driver.\r\n→Yes.  （Go to 3） \r\n3) Can you see your picture when you click the “camera switch button” which is located under the frame where your picture is supposed to be?\r\n→Yes.  Sometimes you have to click the camera switch button to display your picture.  *You have to click the button on Mac with USB camera since Mac has its own camera driver.\r\n→No.  （Go to 4） \r\n4)Did you click “approve” on the small window that pops up after clicking the meeting start button?\r\n→No.  Close the meeting window and start it again.\r\n→Yes.  （Go to 5） \r\n5)Are other applications for the camera open?\r\n→Yes.  Close the application for camera and click the meeting start button again.\r\n→No.\r\nThe camera might not be compatible, or it may be broken.  Try restarting your PC, if that does not work, contact the camera maker’s technical support.\r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('138', '5', '1', '104', 'I cannot see other participants’ pictures.  ', '1) Are they logged in? \r\n→No. Ask them to login. \r\n→Yes. （Go to 2） \r\n2) Can they see their own pictures on their PCs? \r\n→ No. They should set it up so they can see their pictures on their own PCs.  (Refer to above “Self picture can not be seen”.) \r\n→ Yes. （Go to 3） \r\n3) Can you see their names? \r\n→No. There might be some systematic trouble.  Try starting the meeting again. \r\n→Yes. （Go to 4） \r\n4) Can you see the text you type on the chat window?\r\n→No. You might not be connected.  Try starting the meeting again.\r\n→Yes.  Other participants might not be connected.  Try starting the meeting again.  (Refer to above “Self picture cannot be seen”.)\r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('139', '5', '1', '105', 'Other participants cannot hear my voice. ', '1) Is a microphone connected to your Mac? \r\n→No.  Connect a microphone to your Mac. \r\n→Yes.  （Go to 2） \r\n2) Did you click “approve” on the small window which pops up after clicking the meeting start button? →No.  Close the meeting window and start it again.  You have to click “approve” on the small window. \r\n→Yes.  （Go to 3） \r\n3) Is the microphone mute is set up? \r\n→Yes. Click on the Apple Menu and select System Preferences>Sound (in Hardware)>Output.  Uncheck the mute box and turn on the volume.\r\n→No.  （Go to 4） \r\n4) Can others hear you speaking when you click the microphone switch button? \r\n→Yes.  Sometimes you have to click the microphone switch button, especially when there are a few microphones connected to a PC or because of the state of the OS. \r\n→No.  The microphone might not be compatible, or it might be broken.  Try restarting you Mac.  If that does not work, contact the camera maker’s technical support.\r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('140', '4', '1', '108', 'What should I do if the volume is low? ', 'You might have muted or turned down the volume.  Uncheck the microphone mute and set the volume to high on the Sounds and Audio Devices section of the control panel. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('141', '4', '1', '109', 'There is an echo to my voice. How can I make it go away? ', 'Is the person you are speaking with using speakers?\r\n→Yes.  The microphone catches your voice through the speakers. The echo goes away if the person uses a headset. The person needs an echo cancellation microphone if the speakers are used.\r\nReference: Echo cancellation microphone \"ACCUMICII\" by ClearOne. \r\n→No.  If the headset is cheap, an echo sometimes happens.  \r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('142', '4', '1', '110', 'I can not hear the others’ voices with so much noise.  ', '1)Do they use a headset? \r\n→No.  Ask them to use a headset. \r\n→yes.  (Go to 2) \r\n2)Is their microphone a directional microphone?\r\n→Yes.  There might be some problems with their PCs.  Ask them to restart their PCs and login again. \r\n→No.  We recommend you use a better quality headset.  \r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('143', '4', '1', '111', 'A page cannot be shown.  (Pop-up window is blocked.) ', 'If you are using “Windows XP Service Pack 2”, pop-ups are blocked.  On your browser click ‘Tools‘> Go to ‘Pop-up Blocker’> ‘Pop-up Blocker Settings’.  In the box labeled \"Address of Web site to allow\", type \" www.nice2meet.us \" (without quotation marks)  Click the ‘Add’ button to add it to the allowed list.  Click the ‘Close’ button to save your settings. ', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('144', '5', '1', '106', 'I cannot hear the other participants’ voices.', '1) Is there a speaker or a headphone connected to your Mac?\r\n→No.  Connect a speaker or a headphone to your Mac.  \r\n→Yes.  (Go to 2) \r\n2) Is the speaker or headphone mute set up? \r\n→Yes.  Click on the Apple Menu and select System Preferences>Sound (in Hardware)>Output.  Uncheck the mute box and turn on the volume. \r\n→No.  (Go to 3) \r\n3) Is the volume on the computer off?\r\n→Yes.  Turn up the volume. (Be sure to turn up the computer volume.) \r\n→No.  The set up of other users might not be normal.  Ask them to check. (Refer to above \"Other participants cannot hear my voices\")\r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('145', '5', '1', '107', 'What should I do if the volume is low? ', 'You might have muted or turned down the volume.\r\nClick on the Apple Menu and select System Preferences>Sound (in Hardware)>Output.  Uncheck the mute box and turn on the volume.  \r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('146', '5', '1', '108', 'There is an echo to my voice. How can I make it go away? ', 'Is the person you are speaking with using speakers?\r\n→Yes.  The microphone catches your voice through the speakers. The echo goes away if the person uses a headset. The person needs an echo cancellation microphone if the speakers are used.→No.  If the headset is cheap, an echo sometimes happens. \r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('147', '5', '1', '109', 'I can not hear the others’ voices with so much noise.', '1) Do they use a headset? \r\n→No.  Ask them to use a headset. \r\n→yes.  (Go to 2) \r\n2) Are their microphones a directional microphone?\r\n→Yes.  There might be some problems with their PCs.  Ask them to restart their PCs and login again.→No.  We recommend you use a better quality headset.\r\n', '1', '1', '1');
INSERT INTO `faq_list` VALUES ('149', '3', '1', '106', '推奨動作環境を教えてください。', '動作環境ページをご参照ください\r\nhttp://www.nice2meet.us/ja/requirements/', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('150', '3', '1', '111', '必須動作環境を教えてください', '動作環境ページをご参照ください。\r\nhttp://www.nice2meet.us/ja/requirements/', '1', '0', '1');
INSERT INTO `faq_list` VALUES ('151', '3', '1', '112', '推奨ブロードバンド環境（上り128kbps、下り384kbps以上）とありますが、これ以下の回線速度の場合、どのような不都合が発生するのでしょうか？', '音声遅延や、映像遅延などが発生いたします。\r\nまた、「低速回線モード」、「音声優先モード」、「利用帯域設定」など、回線速度の低い環境に合わせた設定を使用した場合、画質が粗くなったり、映像が出なくなります。', '1', '0', '1');

-- ----------------------------
-- Table structure for fms_server
-- ----------------------------
DROP TABLE IF EXISTS `fms_server`;
CREATE TABLE `fms_server` (
  `server_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datacenter_key` int(10) unsigned NOT NULL DEFAULT '0',
  `server_address` varchar(255) NOT NULL,
  `local_address` varchar(255) DEFAULT NULL COMMENT 'ローカルアドレス',
  `server_port` int(10) DEFAULT NULL,
  `server_country` varchar(32) DEFAULT 'jp',
  `server_count` int(10) unsigned DEFAULT '0',
  `server_priority` int(10) unsigned NOT NULL DEFAULT '0',
  `protocol_port` varchar(255) DEFAULT 'rtmp:1935,rtmp:80,rtmp:8080,rtmps:443,rtmps-tls:443' COMMENT 'FMS利用プロトコルポート',
  `server_version` varchar(20) NOT NULL COMMENT 'FMSバージョン',
  `is_ssl` tinyint(1) DEFAULT '0' COMMENT '暗号通信用',
  `use_global_link` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'GlobalLinkで利用するか',
  `maintenance_time` varchar(8) DEFAULT NULL COMMENT 'リブート時間',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  `is_available` tinyint(4) NOT NULL DEFAULT '1',
  `error_count` int(11) DEFAULT '0',
  PRIMARY KEY (`server_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fms_server
-- ----------------------------

-- ----------------------------
-- Table structure for ip_whitelist
-- ----------------------------
DROP TABLE IF EXISTS `ip_whitelist`;
CREATE TABLE `ip_whitelist` (
  `ip_whitelist_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'KEY',
  `user_key` int(11) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `comment` varchar(50) DEFAULT NULL COMMENT 'IP制限用コメント',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: 有効, 9: 削除',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`ip_whitelist_key`),
  KEY `user_key` (`user_key`),
  KEY `ip` (`ip`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ip_whitelist
-- ----------------------------

-- ----------------------------
-- Table structure for layout
-- ----------------------------
DROP TABLE IF EXISTS `layout`;
CREATE TABLE `layout` (
  `layout_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `layout_id` int(10) unsigned DEFAULT NULL,
  `layout_name` varchar(255) DEFAULT NULL,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`layout_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of layout
-- ----------------------------

-- ----------------------------
-- Table structure for mcu_server
-- ----------------------------
DROP TABLE IF EXISTS `mcu_server`;
CREATE TABLE `mcu_server` (
  `server_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server_country` varchar(32) DEFAULT 'jp',
  `server_address` varchar(255) NOT NULL,
  `number_domain` varchar(255) NOT NULL,
  `guest_domain` varchar(255) NOT NULL,
  `guest_number_domain` varchar(255) NOT NULL,
  `guest_h323_domain` varchar(255) NOT NULL,
  `guest_h323_number_domain` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `controller_port` int(10) NOT NULL DEFAULT '8080',
  `socket_port` int(10) NOT NULL DEFAULT '1500',
  `sip_proxy_port` int(10) NOT NULL DEFAULT '5060',
  `use_stb` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'STBでの利用フラグ',
  `target_web_api_version` varchar(128) NOT NULL,
  `is_available` tinyint(4) NOT NULL DEFAULT '1',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`server_key`),
  KEY `ip` (`ip`),
  KEY `ip_2` (`ip`),
  KEY `target_web_api_version` (`target_web_api_version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mcu_server
-- ----------------------------

-- ----------------------------
-- Table structure for media_mixer
-- ----------------------------
DROP TABLE IF EXISTS `media_mixer`;
CREATE TABLE `media_mixer` (
  `media_mixer_key` int(11) NOT NULL AUTO_INCREMENT,
  `mcu_server_key` int(10) NOT NULL,
  `media_name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `media_ip` varchar(255) NOT NULL,
  `local_net` varchar(255) NOT NULL,
  `public_ip` varchar(255) NOT NULL,
  `mobile_mix` int(1) NOT NULL,
  `stb_mix` int(1) NOT NULL DEFAULT '0' COMMENT 'STB用フラグ',
  `is_available` int(1) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`media_mixer_key`),
  KEY `mcu_server_key` (`mcu_server_key`,`is_available`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of media_mixer
-- ----------------------------

-- ----------------------------
-- Table structure for meeting_date_log
-- ----------------------------
DROP TABLE IF EXISTS `meeting_date_log`;
CREATE TABLE `meeting_date_log` (
  `log_no` int(11) NOT NULL AUTO_INCREMENT,
  `log_date` date NOT NULL DEFAULT '0000-00-00',
  `user_id` varchar(64) DEFAULT NULL COMMENT 'ユーザーID',
  `staff_id` varchar(64) DEFAULT NULL COMMENT '営業担当ID',
  `room_key` varchar(64) NOT NULL DEFAULT '',
  `use_time` int(11) NOT NULL DEFAULT '0',
  `use_time_mobile_normal` int(11) NOT NULL DEFAULT '0',
  `use_time_mobile_special` int(11) NOT NULL DEFAULT '0',
  `use_time_hispec` int(11) NOT NULL DEFAULT '0',
  `use_time_audience` int(11) NOT NULL DEFAULT '0',
  `use_time_h323` int(11) NOT NULL DEFAULT '0',
  `use_time_h323ins` int(11) NOT NULL DEFAULT '0',
  `account_time_mobile_normal` int(11) NOT NULL DEFAULT '0',
  `account_time_mobile_special` int(11) NOT NULL DEFAULT '0',
  `account_time_hispec` int(11) NOT NULL DEFAULT '0',
  `account_time_audience` int(11) NOT NULL DEFAULT '0',
  `account_time_h323` int(11) NOT NULL DEFAULT '0',
  `account_time_h323ins` int(11) NOT NULL DEFAULT '0',
  `meeting_cnt` int(11) NOT NULL DEFAULT '0',
  `participant_cnt` int(11) NOT NULL DEFAULT '0',
  `eco_move` double NOT NULL DEFAULT '0' COMMENT '移動量(km)',
  `eco_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '時間(分)',
  `eco_fare` bigint(20) NOT NULL DEFAULT '0' COMMENT '料金(円)',
  `eco_co2` double NOT NULL DEFAULT '0' COMMENT 'CO2(kg)',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`log_no`),
  KEY `log_date` (`log_date`),
  KEY `room_key` (`room_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of meeting_date_log
-- ----------------------------

-- ----------------------------
-- Table structure for member_status
-- ----------------------------
DROP TABLE IF EXISTS `member_status`;
CREATE TABLE `member_status` (
  `member_status_key` int(11) NOT NULL AUTO_INCREMENT,
  `member_status_name` text NOT NULL,
  PRIMARY KEY (`member_status_key`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_status
-- ----------------------------
INSERT INTO `member_status` VALUES ('1', 'Not Active');
INSERT INTO `member_status` VALUES ('2', 'Audience User');
INSERT INTO `member_status` VALUES ('3', 'Default User');
INSERT INTO `member_status` VALUES ('4', 'メンバー解除');
INSERT INTO `member_status` VALUES ('5', 'メンバー休止');
INSERT INTO `member_status` VALUES ('6', 'オーディエンス');
INSERT INTO `member_status` VALUES ('7', '通常ユーザー');
INSERT INTO `member_status` VALUES ('8', 'アドミンユーザー');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `news_key` int(11) NOT NULL AUTO_INCREMENT,
  `news_date` date NOT NULL DEFAULT '0000-00-00',
  `news_title` text NOT NULL,
  `news_contents` text NOT NULL,
  `news_show` int(11) NOT NULL DEFAULT '0',
  `news_delete` int(11) NOT NULL DEFAULT '0',
  `news_maintenance` int(11) NOT NULL DEFAULT '0',
  `news_lang` varchar(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`news_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------

-- ----------------------------
-- Table structure for notification
-- ----------------------------
DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'キー',
  `start_datetime` datetime NOT NULL COMMENT 'メンテナンス開始日時',
  `end_datetime` datetime NOT NULL COMMENT 'メンテナンス終了日時',
  `info` text NOT NULL COMMENT '情報',
  `url_ja` text NOT NULL COMMENT 'メンテナンスページ（日本語）',
  `url_en` text NOT NULL COMMENT 'メンテナンスページ（英語）',
  `url_zh` text NOT NULL COMMENT 'メンテナンスページ（中国語）',
  `default_lang` varchar(10) NOT NULL COMMENT '設定がない場合のデフォルト言語',
  `status` varchar(1) NOT NULL COMMENT 'ステータス',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='告知機能';

-- ----------------------------
-- Records of notification
-- ----------------------------

-- ----------------------------
-- Table structure for operation_log
-- ----------------------------
DROP TABLE IF EXISTS `operation_log`;
CREATE TABLE `operation_log` (
  `operation_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'キー',
  `staff_key` int(11) NOT NULL COMMENT 'スタッフキー',
  `action_name` varchar(255) NOT NULL COMMENT 'アクション',
  `operation_datetime` datetime NOT NULL COMMENT '操作日付',
  `table_name` varchar(255) NOT NULL COMMENT '操作テーブル',
  `keyword` varchar(255) NOT NULL COMMENT '検索用のインデックス',
  `info` text NOT NULL COMMENT '操作内容',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  PRIMARY KEY (`operation_key`),
  KEY `staff_key` (`staff_key`,`action_name`,`operation_datetime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作履歴';

-- ----------------------------
-- Records of operation_log
-- ----------------------------

-- ----------------------------
-- Table structure for options
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `option_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`option_key`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of options
-- ----------------------------
INSERT INTO `options` VALUES ('1', 'ssl', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('2', 'hispec', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('3', 'sharing', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('4', 'mobile', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('5', 'h323', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('6', 'audience', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('7', 'messenger', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('8', 'audience_max', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('9', 'seat_max', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('10', 'narrow', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('11', 'upload', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('12', 'rec', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('13', 'translator', '2006-06-19 17:35:32', null);
INSERT INTO `options` VALUES ('14', '', '0000-00-00 00:00:00', null);
INSERT INTO `options` VALUES ('15', 'intra_fms', '0000-00-00 00:00:00', null);
INSERT INTO `options` VALUES ('16', 'multicamera', '0000-00-00 00:00:00', null);
INSERT INTO `options` VALUES ('17', 'telephone', '0000-00-00 00:00:00', null);
INSERT INTO `options` VALUES ('18', 'record_gw', '0000-00-00 00:00:00', null);
INSERT INTO `options` VALUES ('19', 'smartphone', '0000-00-00 00:00:00', null);
INSERT INTO `options` VALUES ('20', 'whiteboard_video', '0000-00-00 00:00:00', null);
INSERT INTO `options` VALUES ('21', 'teleconference', '2012-05-16 04:46:10', null);
INSERT INTO `options` VALUES ('22', 'video_conference', '2012-05-16 04:46:10', null);
INSERT INTO `options` VALUES ('23', 'minimumBandwidth80', '2012-05-16 04:46:10', null);
INSERT INTO `options` VALUES ('24', 'h264', '2012-05-16 04:46:10', null);
INSERT INTO `options` VALUES ('25', 'tls', '2012-07-19 11:52:49', null);
INSERT INTO `options` VALUES ('26', 'ChinaFastLine', '2012-08-09 22:17:41', null);
INSERT INTO `options` VALUES ('27', 'ChinaFastLine2', '2012-08-27 23:01:49', null);
INSERT INTO `options` VALUES ('28', 'GlobalLink', '2013-03-04 10:58:31', null);

-- ----------------------------
-- Table structure for participant_mode
-- ----------------------------
DROP TABLE IF EXISTS `participant_mode`;
CREATE TABLE `participant_mode` (
  `participant_mode_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participant_mode_name` varchar(64) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`participant_mode_key`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of participant_mode
-- ----------------------------
INSERT INTO `participant_mode` VALUES ('1', 'general', '2006-06-19 17:35:32', null);
INSERT INTO `participant_mode` VALUES ('2', 'messenger', '2006-06-19 17:35:32', null);
INSERT INTO `participant_mode` VALUES ('3', 'mobile', '2006-06-19 17:35:32', null);
INSERT INTO `participant_mode` VALUES ('4', 'h323', '2006-06-19 17:35:32', null);
INSERT INTO `participant_mode` VALUES ('5', 'log_player', '2006-06-19 17:35:32', null);
INSERT INTO `participant_mode` VALUES ('8', 'test_user', '2006-06-19 17:35:32', null);
INSERT INTO `participant_mode` VALUES ('9', 'trial', '2006-11-05 16:50:09', null);
INSERT INTO `participant_mode` VALUES ('10', 'h323ins', '0000-00-00 00:00:00', null);
INSERT INTO `participant_mode` VALUES ('11', 'unknown', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for participant_role
-- ----------------------------
DROP TABLE IF EXISTS `participant_role`;
CREATE TABLE `participant_role` (
  `participant_role_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participant_role_name` varchar(64) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`participant_role_key`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of participant_role
-- ----------------------------
INSERT INTO `participant_role` VALUES ('1', 'default', '2006-11-05 16:09:50', '0000-00-00 00:00:00');
INSERT INTO `participant_role` VALUES ('2', 'invite', '2006-11-05 18:42:20', '0000-00-00 00:00:00');
INSERT INTO `participant_role` VALUES ('3', 'unknown', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for participant_type
-- ----------------------------
DROP TABLE IF EXISTS `participant_type`;
CREATE TABLE `participant_type` (
  `participant_type_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participant_type_name` varchar(64) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`participant_type_key`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of participant_type
-- ----------------------------
INSERT INTO `participant_type` VALUES ('1', 'normal', '2006-06-19 17:35:32', null);
INSERT INTO `participant_type` VALUES ('2', 'audience', '2006-06-19 17:35:32', null);
INSERT INTO `participant_type` VALUES ('3', 'document', '2006-06-19 17:35:32', null);
INSERT INTO `participant_type` VALUES ('4', 'compact', '2007-05-10 00:00:00', null);
INSERT INTO `participant_type` VALUES ('5', 'telepresence', '2007-05-10 00:00:00', null);
INSERT INTO `participant_type` VALUES ('6', 'whiteboard', '0000-00-00 00:00:00', null);
INSERT INTO `participant_type` VALUES ('7', 'whiteboard_audience', '0000-00-00 00:00:00', null);
INSERT INTO `participant_type` VALUES ('8', 'multicamera', '0000-00-00 00:00:00', null);
INSERT INTO `participant_type` VALUES ('9', 'multicamera_audience', '0000-00-00 00:00:00', null);
INSERT INTO `participant_type` VALUES ('10', 'h323', '0000-00-00 00:00:00', null);
INSERT INTO `participant_type` VALUES ('11', 'h323ins', '0000-00-00 00:00:00', null);
INSERT INTO `participant_type` VALUES ('12', 'phone', '0000-00-00 00:00:00', null);
INSERT INTO `participant_type` VALUES ('13', 'unknown', '0000-00-00 00:00:00', null);
INSERT INTO `participant_type` VALUES ('14', 'invisible_wb', '2010-08-30 22:39:28', null);
INSERT INTO `participant_type` VALUES ('15', 'invisible_wb_audience', '2010-09-21 22:04:05', null);
INSERT INTO `participant_type` VALUES ('16', 'staff', '2013-06-14 19:09:21', null);
INSERT INTO `participant_type` VALUES ('17', 'customer', '2013-06-14 19:09:21', null);
INSERT INTO `participant_type` VALUES ('18', 'whiteboard_staff', '2013-05-16 12:34:22', null);
INSERT INTO `participant_type` VALUES ('19', 'whiteboard_customer', '2013-05-16 12:34:22', null);
INSERT INTO `participant_type` VALUES ('20', 'invisible_wb_staff', '2013-05-16 12:34:22', null);
INSERT INTO `participant_type` VALUES ('21', 'invisible_wb_customer', '2013-05-16 12:34:22', null);
INSERT INTO `participant_type` VALUES ('22', 'teleconference', '2013-12-04 21:42:01', null);

-- ----------------------------
-- Table structure for pgi_session
-- ----------------------------
DROP TABLE IF EXISTS `pgi_session`;
CREATE TABLE `pgi_session` (
  `pgi_session_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `system_key` varchar(32) DEFAULT NULL COMMENT 'pgiのシステムキー',
  `company_id` varchar(64) DEFAULT NULL COMMENT 'CompanyID',
  `pgi_session_token` varchar(255) DEFAULT NULL COMMENT 'Token',
  `update_datetime` datetime NOT NULL COMMENT 'Login Time',
  PRIMARY KEY (`pgi_session_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pgi_session
-- ----------------------------

-- ----------------------------
-- Table structure for php_session
-- ----------------------------
DROP TABLE IF EXISTS `php_session`;
CREATE TABLE `php_session` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `data` blob NOT NULL,
  `t` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_session
-- ----------------------------

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) NOT NULL COMMENT 'questionnaireのID',
  `lang1` text CHARACTER SET utf8,
  `lang2` text CHARACTER SET utf8,
  `lang3` text CHARACTER SET utf8,
  `lang4` text CHARACTER SET utf8,
  `lang5` text CHARACTER SET utf8,
  `lang6` text CHARACTER SET utf8 COMMENT 'タイ語',
  `lang7` text CHARACTER SET utf8 COMMENT 'インドネシア語',
  `type` int(4) NOT NULL COMMENT '表示タイプ?1:radio,2:checkbox,3:radio+comment,4:checkbox+comment,5:comment',
  `sort` int(11) NOT NULL,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`question_id`),
  KEY `questionnaire_id` (`questionnaire_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of question
-- ----------------------------
INSERT INTO `question` VALUES ('1', '1', '今回のWeb会議利用に満足されていますか？', 'How satisfied are you with this Web Conferencing service?', '您对本次网络视频会议的使用满意吗？', null, null, null, null, '1', '1', '2011-09-27 17:24:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('2', '1', '映像について（複数選択可）', 'About Video (multiple selections)', '关于图像（可以多选）', null, null, null, null, '2', '2', '2011-09-28 12:35:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('3', '1', '音声について（複数選択可）', 'About Audio (multiple selections)', '关于语音（可以多选）', null, null, null, null, '2', '3', '2011-09-29 20:06:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('4', '1', 'デザインについて（複数選択可）', 'About Design  (multiple selections)', '关于设计（可以多选）', null, null, null, null, '4', '4', '2011-09-29 20:07:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('5', '1', 'その他（複数選択可）', 'Others  (multiple selections)', '其他（可以多选）', null, null, null, null, '4', '5', '2011-09-29 20:06:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('6', '1', 'Web会議の利用頻度をお教え下さい。', 'How often do you use this Web Conferencing service?', '请选择您使用网络视频会议的频率。', null, null, null, null, '1', '6', '2011-09-29 20:07:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('7', '1', '今回のWeb会議のタイプはどのようなものですか？', 'What type of meeting was this?', '您这次的网络视频会议属于什么类型？', null, null, null, null, '1', '7', '2011-10-03 15:41:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('8', '1', '今回の会議に参加した方はどのような方ですか？（複数選択可）', 'Who participated in this meeting? (multiple selections)', '这次会议的参会者身份是什么？（可以多选）', null, null, null, null, '1', '8', '2011-10-03 15:41:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('9', '1', 'その他ご意見・ご要望があればお願いいたします（自由記述）', 'Please give us your comments and suggestions (Free description)', '请您填写其他的意见或建议。（自由阐述）', null, null, null, null, '5', '9', '2011-10-03 15:42:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('10', '1', '今回はどの方法でWeb会議室に入室しましたか', 'How did you enter the meeting room?', '此次是通过什么方式进入会议室？', null, null, null, null, '1', '10', '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('11', '1', 'お名前', 'Name', '姓名', null, null, null, null, '5', '11', '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('12', '1', 'フリガナ', 'Name', '假名', null, null, null, null, '5', '12', '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('13', '1', '会社名・所属先', 'Company', '公司名・所属部门', null, null, null, null, '5', '13', '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('14', '1', 'メールアドレス', 'Email Address', '邮箱地址', null, null, null, null, '5', '14', '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('15', '1', '電話番号', 'Phone Number', '电话号码', null, null, null, null, '5', '15', '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('16', '2', '今回のWeb会議利用に満足されていますか？', 'How satisfied are you with this Web Conferencing service?', '您对本次网络视频会议的使用满意吗？', '您對本次網絡視频會議的使用滿意嗎？', 'Quel est votre degré de satisfaction concernant l\'utilisation de ce service de Web conférence?', 'คุณได้รับความพึงพอใจในการใช้การประชุมเว็บนี้?', 'Seberapa puas anda dengan web konferensi service ini?', '1', '1', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('17', '2', '映像について（複数選択可）', 'About Video (multiple selections)', '关于图像（可以多选）', '關於圖像（可以多選）', 'A propos de la vidéo (choix multiples)', 'สำหรับวิดีโอ (เลือกได้หลายแบบ)', 'tentang video ( beberapa seleksi)', '2', '2', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('18', '2', '音声について（複数選択可）', 'About Audio (multiple selections)', '关于语音（可以多选）', '關於語音（可以多選）', 'A propos de l\'audio (choix multiples)', 'สำหรับเสียง (เลือกได้หลายแบบ)', 'tentang audio (beberapa seleksi)', '2', '3', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('19', '2', 'その他問題があったと感じた点・ご意見・ご要望があればお願いいたします（自由記述）', 'Please give us your comments and suggestions (Free description)', '请您填写其他的意见或建议。（自由阐述）', '請您填寫其他的意見或建議。（自由闡述）', 'N\'hésitez pas à nous faire des suggestions et des commentaires', 'โปรดให้ความคิดเห็นและข้อเสนอแนะของคุณ (คำอธิบาย)', 'tolong berikan kepada kami masukan dan pendapat kamu (bebas deskripsi)', '5', '4', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('20', '2', 'お名前', 'Name', '姓名', '姓名', 'Nom', 'ชื่อ', 'nama', '5', '5', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('21', '2', 'フリガナ', 'Name', '假名', '假名', 'Nom', 'นามแฝง', 'nama', '5', '6', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('22', '2', '会社名・所属先', 'Company', '公司名・所属部门', '公司名·所屬部門', 'Société', 'ชื่อบริษัทหรือหน่วยงาน', 'company', '5', '7', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('23', '2', 'メールアドレス', 'Email Address', '邮箱地址', '郵箱地址', 'Email', 'ที่อยู่อีเมล์', 'alamat email', '5', '8', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES ('24', '2', '電話番号', 'Phone Number', '电话号码', '電話號碼', 'Numéro de téléphone', 'หมายเลขโทรศัพท์', 'nomor telepon', '5', '9', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for questionnaire
-- ----------------------------
DROP TABLE IF EXISTS `questionnaire`;
CREATE TABLE `questionnaire` (
  `questionnaire_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '表示箇所タイプ',
  `is_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '公開:1、非公開:0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`questionnaire_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of questionnaire
-- ----------------------------
INSERT INTO `questionnaire` VALUES ('1', 'meeting_end', '0', '0', '2011-09-27 17:20:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `questionnaire` VALUES ('2', 'meeting_end', '1', '0', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for question_branch
-- ----------------------------
DROP TABLE IF EXISTS `question_branch`;
CREATE TABLE `question_branch` (
  `question_branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL COMMENT '紐付くquestion_id',
  `lang1` text NOT NULL,
  `lang2` text NOT NULL,
  `lang3` text NOT NULL,
  `lang4` text NOT NULL,
  `lang5` text NOT NULL,
  `lang6` text COMMENT 'タイ語',
  `lang7` text COMMENT 'インドネシア語',
  `sort` int(11) NOT NULL,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`question_branch_id`),
  KEY `question_id` (`question_id`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of question_branch
-- ----------------------------
INSERT INTO `question_branch` VALUES ('1', '1', '大変満足', 'Very satisfied', '非常满意', '', '', null, null, '1', '2011-09-27 21:14:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('2', '1', 'やや満足', 'Somewhat satisfied', '比较满意', '', '', null, null, '3', '2011-09-27 21:15:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('3', '1', '満足', 'Satisfied', '满意', '', '', null, null, '2', '2011-09-27 21:15:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('4', '1', 'どちらとも言えない', 'Unsure', '都不是', '', '', null, null, '4', '2011-09-28 12:50:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('5', '1', 'やや不満', 'Somewhat dissatisfied', '比较不满意', '', '', null, null, '5', '2011-09-28 12:50:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('6', '1', '不満', 'Dissatisfied', '不满意', '', '', null, null, '6', '2011-09-28 12:51:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('7', '1', '非常に不満', 'Very dissatisfied', '非常不满意', '', '', null, null, '7', '2011-09-28 12:51:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('8', '2', '相手の映像が表示されない', 'Cannot see others\' video', '未显示对方图像', '', '', null, null, '1', '2011-09-29 19:46:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('9', '2', '自分の映像が表示されない', 'Cannot see your own video', '未显示我方图像', '', '', null, null, '2', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('10', '2', '映像の品質が悪い', 'Video quality is not good', '图像质量差', '', '', null, null, '3', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('11', '3', '自分の音声が取得できない', 'Cannot hear your own voice', '无法接通我方语音', '', '', null, null, '1', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('12', '3', '相手の音声が聞こえない', 'Cannot hear others\' voice', '无法听到对方语音', '', '', null, null, '2', '2011-09-28 12:52:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('13', '3', '音声が遅延する', 'Cause delay', '语音延迟', '', '', null, null, '4', '2011-09-28 21:24:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('14', '3', '音声がエコーする（やまびこのように聞こえる）', 'Cause echo', '语音回响（听起来有回音）', '', '', null, null, '5', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('15', '3', '音声がハウリングする（キーンという耳障りな大きな音がする）', 'Cause howling noise', '语音啸响（声音尖锐刺耳、大声作响）', '', '', null, null, '6', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('16', '4', '機能（ボタン）が多く操作に迷う', 'Too many features (buttons) and difficult to operate', '功能（按钮）太多令操作困惑', '', '', null, null, '1', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('17', '4', 'デザインがわかりにくい（使いにくい）箇所がある。（お手数ですが詳細をご記入ください）', 'Design is not user-friendly (please enter details)', '有些部分设计难懂（不易使用）。（麻烦您填写详细内容）', '', '', null, null, '2', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('18', '5', '突然切断される', 'Suddenly disconnect', '突然中断', '', '', null, null, '1', '2011-09-28 12:52:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('19', '5', 'デスクトップ共有が開始出来ない', 'Cannot start Desktop Sharing', '桌面共享无法开始', '', '', null, null, '2', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('20', '5', 'デスクトップ共有が閲覧出来ない', 'Cannot view Desktop Sharing', '桌面共享无法阅览', '', '', null, null, '3', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('21', '5', '全体的に動作が遅い', 'Slow in performance', '整体反应慢', '', '', null, null, '4', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('22', '5', 'ホワイトボード操作に不具合があった（機能が多くあるため、お手数ですが詳細をご記入ください）', 'Trouble with Whiteboard operation (Since there are many features, please enter details)', '白板操作有些不便（因为功能较多、麻烦您填写详细内容）', '', '', null, null, '5', '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('23', '6', '月1回', 'Monthly', '每月1次', '', '', null, null, '1', '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('24', '6', '月2～3回程度', '2-3 times per month', '每月2~3次', '', '', null, null, '2', '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('25', '6', '月4回程度（週1回）', '4 times per month (weekly)', '每月4次（每周一次）', '', '', null, null, '3', '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('26', '6', '月4回以上', 'more than 4 times per month', '每月4次以上', '', '', null, null, '4', '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('27', '6', '不定期', 'Irregular', '不定期', '', '', null, null, '5', '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('28', '7', '営業会議', 'Sales meeting', '营业会议', '', '', null, null, '1', '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('29', '7', '企画会議', 'Planning meeting', '企划会议', '', '', null, null, '2', '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('30', '7', '報告会議', 'Debrief meeting', '报告会议', '', '', null, null, '3', '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('31', '7', '商談', 'Business negotiation', '商业谈判', '', '', null, null, '4', '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('32', '7', '研修・教育', 'Training', '培训•教育', '', '', null, null, '5', '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('33', '7', '経営会議・取締役会議', 'Management meeting/Board meeting', '经营会议•董事会议', '', '', null, null, '6', '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('34', '7', 'その他', 'Others', '其他', '', '', null, null, '7', '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('35', '8', '経営者', 'Executive', '经营者', '', '', null, null, '1', '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('36', '8', '役職者', 'Manager', '管理者', '', '', null, null, '2', '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('37', '8', '担当者', 'Person in charge', '负责人', '', '', null, null, '3', '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('38', '8', '取引先', 'Business partner', '交易户', '', '', null, null, '4', '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('39', '8', '顧客', 'Customer', '顾客', '', '', null, null, '5', '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('40', '8', '在宅勤務者', 'Homeworker', '在家办公者', '', '', null, null, '6', '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('41', '8', 'その他', 'Others', '其他', '', '', null, null, '7', '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('42', '3', '音が頻繁に途切れる', 'Lose sound very often', '经常听不到声音', '', '', null, null, '3', '2011-10-11 21:30:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('44', '10', 'V-CUBEミーティングのID/パスワードを使ってログイン', 'With User ID and Password', '使用V-CUBE Meeting的ID/密码登陆 ', '', '', null, null, '1', '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('45', '10', '送られてきた招待メール内のURLをクリック', 'From Invitation URL', '点击收到的邀请邮件内的URL', '', '', null, null, '2', '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('46', '16', '満足', 'Satisfied', '满意', '滿意', 'Satisfait', 'พึงพอใจ?', 'Puas', '1', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('47', '16', '普通', 'So so', '一般', '一般', 'Couci-couça', 'ดังนั้น', 'Cukup Memuaskan', '2', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('48', '16', '不満', 'Dissatisfied', '不满意', '不滿意', 'Mécontent', 'ไม่พอใจ', 'Tidak Puas', '3', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('49', '17', '相手の映像が表示されない', 'Cannot see others\' video', '未显示对方图像', '未顯示對方圖像', 'Impossible de voir la vidéo des autres', 'ไม่ปรากฏภาพบุคคลอื่น', 'Tidak dapat melihat video yang lainnya', '1', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('50', '17', '自分の映像が表示されない', 'Cannot see your own video', '未显示我方图像', '未顯示我方圖像', 'Impossible de voir ma vidéo', 'ไม่ปรากฎภาพคุณเอง', 'Tidak dapa melihat video saya', '2', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('51', '17', '映像の品質が悪い', 'Video quality is not good', '图像质量差', '圖像品質差', 'La qualité vidéo n\'est pas bonne', 'คุณภาพของวิดีโอไม่ดี', 'Kualitas video tidak bagus', '3', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('52', '18', '自分の音声が取得できない', 'Cannot hear your own voice', '无法接通我方语音', '無法接通我方語音', 'Impossible d\'entendre ma voix', 'ไม่ได้ยินเสียงคุณเอง', 'Tidak dapat mendengar suara saya sendiri', '1', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('53', '18', '相手の音声が聞こえない', 'Cannot hear others\' voice', '无法听到对方语音', '無法聽到對方語音', 'Impossible d\'entendre la voix des autres', 'ไม่ได้ยินเสียงบุคคลอื่น', 'Tidak dapat mendengar suara lainnya', '2', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('54', '18', '音が頻繁に途切れる', 'Lose sound very often', '经常听不到声音', '經常聽不到聲音', 'Le son coupe régulièrement', 'เสียงถูกขัดจังหวะบ่อย', 'Suara sering hilang', '3', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('55', '18', '音声が遅延する', 'Cause delay', '语音延迟', '語音延遲', 'Il y a de la latence', 'เกิดความล่าช้า', 'Karena delay', '4', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('56', '18', '音声がエコーする（やまびこのように聞こえる）', 'Cause echo', '语音回响（听起来有回音）', '語音迴響（聽起來有回音）', 'Il y a de l\'echo', 'เกิดเสียงสะท้อน', 'Karena echo', '5', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('57', '18', '音声がハウリングする（キーンという耳障りな大きな音がする）', 'Cause howling noise', '语音啸响（声音尖锐刺耳、大声作响）', '語音嘯響（聲音尖銳刺耳、大聲作響）', 'Il y a du bruit', 'เกิดเสียงหอน', 'Karena lolongan', '6', '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('58', '3', '音が頻繁に途切れる', 'Lose sound very often', '经常听不到声音', '', '', null, null, '3', '2014-10-06 16:03:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('59', '10', 'V-CUBEミーティングのID/パスワードを使ってログイン', 'With User ID and Password', '使用V-CUBE Meeting的ID/密码登陆 ', '', '', null, null, '1', '2014-10-06 16:03:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES ('60', '10', '送られてきた招待メール内のURLをクリック', 'From Invitation URL', '点击收到的邀请邮件内的URL', '', '', null, null, '2', '2014-10-06 16:03:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for relation
-- ----------------------------
DROP TABLE IF EXISTS `relation`;
CREATE TABLE `relation` (
  `relation_key` varchar(64) NOT NULL,
  `relation_type` varchar(64) NOT NULL,
  `user_key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_datetime` datetime NOT NULL,
  KEY `relation_key` (`relation_key`),
  KEY `relation_key_2` (`relation_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of relation
-- ----------------------------

-- ----------------------------
-- Table structure for sastik_account_host
-- ----------------------------
DROP TABLE IF EXISTS `sastik_account_host`;
CREATE TABLE `sastik_account_host` (
  `account_host_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) NOT NULL,
  `account_host` varchar(64) NOT NULL COMMENT '顧客サーバIP',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日時',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日時',
  PRIMARY KEY (`account_host_key`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sastik_account_host
-- ----------------------------

-- ----------------------------
-- Table structure for sastik_account_translation
-- ----------------------------
DROP TABLE IF EXISTS `sastik_account_translation`;
CREATE TABLE `sastik_account_translation` (
  `account_translation_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_host_key` int(10) unsigned NOT NULL,
  `account_id` varchar(64) NOT NULL COMMENT 'アカウント名',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日時',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日時',
  PRIMARY KEY (`account_translation_key`),
  KEY `account_id` (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sastik_account_translation
-- ----------------------------

-- ----------------------------
-- Table structure for service
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service` (
  `service_key` int(11) NOT NULL AUTO_INCREMENT,
  `country_key` int(11) NOT NULL DEFAULT '0',
  `service_name` text NOT NULL,
  `max_seat` int(11) NOT NULL COMMENT '最大参加者数',
  `max_audience_seat` int(11) NOT NULL COMMENT '最大オーディエンス数',
  `max_whiteboard_seat` int(11) NOT NULL DEFAULT '0' COMMENT '資料ユーザー数',
  `max_guest_seat` int(11) NOT NULL DEFAULT '0',
  `max_guest_seat_flg` int(11) NOT NULL DEFAULT '0',
  `extend_max_seat` int(11) NOT NULL DEFAULT '0',
  `extend_max_audience_seat` int(11) NOT NULL DEFAULT '0',
  `extend_seat_flg` int(11) NOT NULL DEFAULT '0',
  `meeting_limit_time` int(11) NOT NULL DEFAULT '0' COMMENT 'デフォルト会議利用制限時間',
  `max_room_bandwidth` int(11) NOT NULL DEFAULT '0' COMMENT '部屋上限帯域',
  `max_user_bandwidth` int(11) NOT NULL DEFAULT '0' COMMENT '拠点上限帯域',
  `min_user_bandwidth` int(11) NOT NULL DEFAULT '0' COMMENT '拠点下限限帯域',
  `default_camera_size` varchar(64) DEFAULT NULL COMMENT 'デフォルトカメラサイズ',
  `disable_rec_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '録画不可フラグ',
  `active_speaker_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'アクティブスピーカー利用フラグ',
  `active_speaker_user_count` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'アクティブスピーカー最大映像表示数',
  `whiteboard_video` int(1) DEFAULT '0' COMMENT '資料共有ビデオ付き',
  `meeting_ssl` int(11) NOT NULL COMMENT '暗号化',
  `desktop_share` int(11) NOT NULL COMMENT 'デスクトップ共有',
  `hdd_extention` int(11) NOT NULL COMMENT '容量',
  `high_quality` int(11) NOT NULL COMMENT '高画質',
  `mobile_phone` int(11) NOT NULL COMMENT '携帯',
  `h323_client` int(11) NOT NULL COMMENT 'H323',
  `whiteboard` int(11) NOT NULL COMMENT '資料共有',
  `multicamera` int(11) NOT NULL COMMENT 'マルチカメラ',
  `telephone` int(11) NOT NULL COMMENT '電話連携',
  `smartphone` int(11) NOT NULL COMMENT 'スマートフォン連携',
  `record_gw` int(11) NOT NULL COMMENT '録画GW',
  `h264` int(11) NOT NULL DEFAULT '0' COMMENT 'H.264オプション',
  `global_link` int(11) DEFAULT '0' COMMENT 'グローバルリンク',
  `teleconference` int(11) NOT NULL DEFAULT '0' COMMENT '電話会議オプション',
  `pgi_setting_key` varchar(64) NOT NULL DEFAULT '' COMMENT 'PGiプランキー',
  `hd_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'HDプランフラグ',
  `bandwidth_up_flg` tinyint(4) DEFAULT '0' COMMENT '帯域追加フラグ',
  `sales_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'セールスオプション',
  `use_room_plan` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ルームプラン利用フラグ',
  `use_member_plan` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ID制利用フラグ',
  `use_sales_plan` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'セールスプラン利用フラグ',
  `use_one_plan` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'VCUBE ONE利用フラグ',
  `use_stb_plan` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'STB利用フラグ',
  `whiteboard_page` int(11) NOT NULL DEFAULT '0' COMMENT 'アップロード可能ページ数',
  `whiteboard_size` int(11) NOT NULL DEFAULT '0' COMMENT 'アップロード可能ファイルサイズ',
  `document_filetype` varchar(255) DEFAULT NULL COMMENT 'ップロード可能ドキュメント（拡張子をカンマ区切りで登録）',
  `image_filetype` varchar(255) DEFAULT NULL COMMENT 'ップロード可能画像（拡張子をカンマ区切り）',
  `cabinet_filetype` varchar(255) DEFAULT NULL COMMENT '利用キャビネットファイルタイプ（カンマ区切り）',
  `ignore_staff_reenter_alert` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'スタッフ再入室アラート非表示フラグ',
  `service_initial_charge` int(11) NOT NULL DEFAULT '0',
  `service_charge` int(11) NOT NULL DEFAULT '0',
  `service_additional_charge` double NOT NULL DEFAULT '0',
  `service_freetime` int(11) NOT NULL DEFAULT '0',
  `service_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `service_status` tinyint(4) NOT NULL,
  `service_expire_day` int(11) NOT NULL DEFAULT '0' COMMENT '自動利用停止日数',
  `member_storage_size` int(11) NOT NULL DEFAULT '0' COMMENT '1メンバーのストレージ容量(Mbyte)',
  `add_storage_size` int(11) DEFAULT '1000' COMMENT 'room制用、1部屋毎ストレージ容量',
  `max_storage_size` int(11) NOT NULL DEFAULT '0' COMMENT 'ユーザー最大容量',
  `lang_allow` varchar(100) DEFAULT NULL COMMENT '言語許可リスト(「,」区切りで指定nullの場合は全許可)',
  `max_member_count` tinyint(11) NOT NULL DEFAULT '0' COMMENT 'ユーザー最大メンバー数',
  `port` tinyint(11) NOT NULL DEFAULT '0' COMMENT 'ユーザー最大メンバー数',
  `user_service_name` varchar(64) DEFAULT NULL COMMENT 'custom_domain_serviceの提供サービス名',
  `external_user_invitation_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ID制の外部招待許可フラグ',
  `external_member_invitation_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ID制別ユーザーに紐ずくメンバーの招待フラグ',
  `active_speaker_mode_only_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '強制アクティブスピーカーモードフラグ',
  `guest_url_format` int(11) NOT NULL DEFAULT '0' COMMENT '招待URLの表示方法:0:<>あり 1:<>なし',
  `vcubeid_connect_service` varchar(64) DEFAULT NULL COMMENT 'VCUBE ID連携サービス名',
  PRIMARY KEY (`service_key`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of service
-- ----------------------------
INSERT INTO `service` VALUES ('1', '1', 'STOP', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('2', '2', 'STOP', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('3', '1', 'Vstaff', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('4', '1', 'Trial', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('5', '1', 'Temporary', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '100', '0', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('6', '1', 'Diamond', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '79900', '0', '60000', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('7', '2', 'Diamond', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '799', '0', '60000', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('8', '1', 'Platinum', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '39900', '20', '2000', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('9', '2', 'Platinum', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '399', '0.2', '2000', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('10', '1', 'Gold', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '29900', '25', '1200', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('11', '2', 'Gold', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '299', '0.25', '1200', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('12', '1', 'Silver', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '19900', '30', '600', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('13', '2', 'Silver', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '199', '0.3', '600', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('14', '1', 'Standard(旧)', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '9900', '35', '120', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('15', '2', 'Standard', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '99', '0.35', '120', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('17', '2', 'Personal', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '10', '0.02', '300', '2003-08-17 14:07:33', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('18', '2', 'Silver(Reseller)', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '199', '0', '40000', '2003-10-13 00:52:12', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('19', '2', 'Trial', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2003-11-26 17:23:43', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('20', '1', 'Personal', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '980', '2', '300', '2004-01-26 02:43:47', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('21', '3', 'Standard', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '99', '0.35', '120', '2004-01-31 07:54:50', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('22', '3', 'Silver', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '199', '0.3', '600', '2004-01-31 07:55:19', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('23', '3', 'Gold', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '299', '0.25', '1200', '2004-01-31 07:55:42', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('24', '3', 'Platinum', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '399', '0.2', '2000', '2004-01-31 07:56:30', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('25', '3', 'Platinum', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '299', '799', '0', '60000', '2004-01-31 07:56:59', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('26', '1', 'Lite（旧）', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '9900', '35', '120', '2004-10-20 14:52:32', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('27', '1', 'Basic', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '29900', '25', '1200', '0000-00-00 00:00:00', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('28', '1', 'Professional', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '79900', '0', '60000', '2004-10-20 14:58:47', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('29', '3', 'Trial', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2004-11-27 00:28:38', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('30', '1', 'VismeeProfessional', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '35000', '96000', '0', '60000', '2005-01-13 15:47:14', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('31', '1', 'VismeeBasic', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '35000', '36000', '30', '1200', '2005-01-13 15:52:53', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('32', '1', 'VismeeLite', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '35000', '12000', '42', '120', '2005-01-13 15:49:52', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('33', '1', 'Lite', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '9900', '95', '0', '2005-04-12 04:48:15', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('34', '1', 'FORSI', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '60000', '2005-08-02 11:17:37', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('35', '1', 'Seminar_Spot', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '10000', '0', '200', '0', '2005-11-15 05:29:57', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('36', '1', 'Seminar_Lite', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '29000', '120', '120', '2005-11-15 05:29:57', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('37', '1', 'Seminar_Basic', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '49900', '100', '120', '2005-11-15 05:29:57', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('38', '1', 'Seminar_Professional', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '29000', '99000', '80', '120', '2005-11-15 05:29:57', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('39', '1', 'Seminar_Event', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '190000', '179000', '60', '120', '2005-11-15 05:29:57', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('40', '1', 'Seminar_Trial', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2005-11-15 05:29:57', '0', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('41', '1', 'New_Trial', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '9900', '190', '0', '2006-03-28 19:05:47', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('42', '1', 'Entry', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '34900', '25', '1200', '2006-03-28 19:10:47', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('43', '1', 'Standard', '10', '0', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '79900', '0', '60000', '2006-03-28 19:10:47', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('44', '1', 'High_Quality', '10', '0', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '119900', '0', '60000', '2006-03-28 19:14:04', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('45', '1', 'Premium20', '9', '11', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '119900', '0', '60000', '2006-03-28 19:14:04', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('46', '1', 'Premium30', '9', '21', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '149900', '0', '60000', '2006-03-28 19:19:15', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('47', '1', 'Premium40', '9', '31', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '179900', '0', '60000', '2006-03-28 19:19:15', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('48', '1', 'おてがるプラン', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '10000', '2000', '10', '60', '2009-02-10 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('49', '1', '使い放題プラン', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '10000', '7000', '0', '60000', '2009-02-10 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('50', '1', 'Premium50', '9', '41', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '209900', '0', '60000', '2009-12-11 22:24:22', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('51', '1', 'Premium60', '9', '51', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '239900', '0', '60000', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('52', '1', 'Premium70', '9', '61', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '269900', '0', '60000', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('53', '1', 'Premium80', '9', '71', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '299900', '0', '60000', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('54', '1', 'Premium90', '9', '81', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '329900', '0', '60000', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('55', '1', 'Premium100', '9', '91', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '359900', '0', '60000', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('56', '1', 'ペーパーレス', '20', '0', '20', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '30000', '30000', '0', '60000', '2009-12-21 22:32:11', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('57', '1', 'Entry_', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '34900', '25', '1200', '2010-01-28 15:23:45', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('58', '1', '同時2拠点プラン', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '20000', '20000', '0', '60000', '2010-03-12 14:31:36', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('59', '1', '同時3拠点プラン', '3', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '30000', '30000', '0', '60000', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('60', '1', '同時5拠点プラン', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '50000', '50000', '0', '60000', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('61', '1', '同時2拠点（大塚商会）', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '20000', '20000', '0', '60000', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('62', '1', 'スマート（大塚商会）', '3', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '30000', '30000', '0', '60000', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('63', '1', 'スマートプラス（大塚商会', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '50000', '50000', '0', '60000', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('64', '1', '一括登録用', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '5000', '2010-04-01 15:34:14', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('65', '1', 'Premium20プラス', '20', '0', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '129900', '0', '10000', '2010-04-28 10:30:54', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('66', '1', 'Premium30プラス', '19', '11', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '159900', '0', '60000', '2010-09-14 11:57:44', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('68', '1', '20110312', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2011-03-12 13:29:37', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('67', '1', 'V-CUBEセンター', '9', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2010-11-25 23:08:14', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('69', '1', 'IT_Hinan', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2011-07-07 11:44:48', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('70', '1', 'V-CUBE ミーティング on cybozu.com', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '50000', '0', '10000', '2011-11-24 15:45:09', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('71', '1', 'Paperless_Free', '5', '0', '5', '0', '0', '0', '0', '0', '45', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('72', '1', 'Meeting_Free', '3', '0', '0', '0', '0', '0', '0', '0', '30', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2012-04-16 21:06:21', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('73', '1', 'HD_3-5-10', '10', '0', '10', '0', '0', '0', '0', '0', '0', '10240', '1024', '150', '640x480', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '100000', '100000', '0', '2012-05-08 11:11:24', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('74', '1', '新ハイクオリティ', '10', '0', '10', '0', '0', '0', '0', '0', '0', '6144', '512', '100', '640x480', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '100000', '100000', '0', '2012-05-08 11:51:30', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('75', '1', '新スタンダード', '10', '0', '10', '0', '0', '0', '0', '0', '0', '3072', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '100000', '100000', '0', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('76', '1', 'V-CUBE MTG ON cybozu.comトライアル', '5', '0', '0', '0', '0', '0', '0', '0', '45', '2048', '384', '30', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2012-05-22 19:53:26', '1', '35', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('77', '1', 'Panasonic Malaysia', '3', '0', '0', '0', '0', '0', '0', '0', '60', '2048', '384', '30', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '0000-00-00 00:00:00', '1', '90', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('78', '1', 'PGi_Paperless_Free', '5', '0', '5', '0', '0', '0', '0', '0', '45', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('79', '1', 'VCUBE_Doctor_Standard', '5', '0', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2012-08-20 16:07:02', '1', '0', '500', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('80', '1', '新エントリー', '10', '0', '10', '0', '0', '0', '0', '0', '0', '2048', '384', '80', '320x240', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '34900', '25', '1200', '2012-08-30 11:51:30', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('81', '1', '新Premium20', '9', '11', '10', '0', '0', '0', '0', '0', '0', '4096', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '119900', '0', '60000', '2012-08-30 11:51:30', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('82', '1', '新Premium30', '9', '21', '10', '0', '0', '0', '0', '0', '0', '6144', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '149900', '0', '60000', '2012-08-30 11:51:30', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('83', '1', '新Premium40', '9', '31', '10', '0', '0', '0', '0', '0', '0', '8192', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '179900', '0', '60000', '2012-08-30 11:51:30', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('84', '1', '新Premium50', '9', '41', '10', '0', '0', '0', '0', '0', '0', '10240', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '209900', '0', '60000', '2012-08-30 11:51:30', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('85', '1', '新同時2拠点（大塚商会）', '2', '0', '0', '0', '0', '0', '0', '0', '0', '2048', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '20000', '20000', '0', '60000', '2012-08-30 11:51:30', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('86', '1', '新スマート（大塚商会）', '3', '0', '0', '0', '0', '0', '0', '0', '0', '2048', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '30000', '30000', '0', '60000', '2012-08-30 11:51:30', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('87', '1', '新スマートプラス（大塚商会）', '5', '0', '0', '0', '0', '0', '0', '0', '0', '2048', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '50000', '50000', '0', '60000', '2012-08-30 11:51:30', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('88', '1', '新Premium20プラス', '20', '0', '10', '0', '0', '0', '0', '0', '0', '4096', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '129900', '0', '10000', '2012-08-30 11:51:30', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('89', '1', 'セールスプラン', '2', '0', '0', '0', '0', '0', '0', '0', '0', '2048', '384', '80', '320x240', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', null, null, null, '0', '45000', '34900', '25', '1200', '2012-10-25 21:21:38', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('91', '1', 'Trial_withH264', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '9900', '190', '0', '2013-03-18 11:13:40', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('92', '1', 'cybozu_new_trial', '3', '0', '0', '0', '0', '0', '0', '0', '30', '2048', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2013-03-19 13:12:39', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('93', '1', 'Cloud_Mcu', '10', '0', '10', '0', '0', '0', '0', '0', '0', '3072', '384', '80', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '30000', '0', '0', '0', '2013-04-26 17:55:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('94', '1', 'セールス資料共有', '2', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '320x240', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', '', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2013-05-15 10:25:36', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('95', '1', 'Docomo_Free', '3', '0', '0', '0', '0', '0', '0', '0', '60', '2048', '384', '80', '320x240', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2013-05-15 10:25:36', '1', '0', '0', '0', '0', null, '0', '0', null, '0', '0', '0', '0', 'docomo_free');
INSERT INTO `service` VALUES ('96', '1', 'ブルーIDプラン', '9', '16', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '69900', '3000', '0', '0', '2013-07-12 12:42:32', '1', '0', '200', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('97', '1', 'グリーンIDホストプラン', '4', '21', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '10000', '5000', '0', '0', '2013-07-12 12:42:32', '1', '0', '200', '200', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('98', '1', 'グリーンIDホストプランライト', '4', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '5000', '2500', '0', '0', '2013-07-12 12:42:32', '1', '0', '200', '200', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('99', '1', 'グレーID従量プラン', '4', '21', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '10000', '5000', '10', '60', '2013-07-12 12:42:32', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('100', '1', 'Docomo_Document_Free', '5', '0', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '320x240', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2013-07-12 12:42:32', '1', '0', '0', '0', '0', null, '0', '0', null, '0', '0', '0', '0', 'docomo_document_free');
INSERT INTO `service` VALUES ('101', '1', 'SFDCセールスプラン', '2', '0', '0', '0', '0', '0', '0', '0', '0', '2048', '384', '80', '320x240', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', null, null, null, '1', '45000', '34900', '25', '1200', '2013-07-23 11:01:01', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', 'sfdc_sales');
INSERT INTO `service` VALUES ('102', '1', 'SFDCセールス資料共有', '2', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '320x240', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', '', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', null, null, null, '1', '0', '0', '0', '0', '2013-07-23 11:01:01', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', 'sfdc_sales');
INSERT INTO `service` VALUES ('103', '1', 'SFDCセールス画面共有', '2', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '320x240', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', '', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', null, null, null, '1', '0', '0', '0', '0', '2013-07-23 11:01:01', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', 'sfdc_sales');
INSERT INTO `service` VALUES ('104', '1', 'イプロス2拠点プラン', '2', '0', '0', '0', '0', '0', '0', '0', '0', '3072', '384', '80', '', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '30000', '100000', '0', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('105', '1', 'イプロス3拠点プラン', '3', '0', '0', '0', '0', '0', '0', '0', '0', '3072', '384', '80', '', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '45000', '50000', '100000', '0', '0000-00-00 00:00:00', '1', '0', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('106', '1', 'イプロス2拠点トライアルプラン', '2', '0', '0', '0', '0', '0', '0', '0', '30', '3072', '384', '80', '', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '100000', '0', '0000-00-00 00:00:00', '1', '14', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('107', '1', 'イプロス3拠点トライアルプラン', '3', '0', '0', '0', '0', '0', '0', '0', '30', '3072', '384', '80', '', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '100000', '0', '0000-00-00 00:00:00', '1', '14', '0', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('108', '1', 'BIZ＋', '5', '0', '0', '0', '0', '0', '0', '0', '0', '3072', '384', '80', '320x240', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '0000-00-00 00:00:00', '1', '0', '50', '1000', '0', 'ja,en', '0', '0', 'biz', '0', '1', '0', '1', null);
INSERT INTO `service` VALUES ('109', '1', 'プロスパート(プロスパID)', '10', '0', '10', '0', '0', '0', '0', '0', '0', '3072', '384', '80', '320x240', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '0000-00-00 00:00:00', '1', '0', '200', '1000', '0', 'ja,en', '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('110', '1', 'プロスパート(プロスパルーム2)', '5', '0', '5', '5', '1', '0', '0', '0', '0', '3072', '384', '80', '320x240', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '0000-00-00 00:00:00', '1', '0', '0', '500', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('111', '1', 'グローバルプラン 電話連携あり', '10', '0', '10', '0', '0', '0', '0', '0', '0', '6144', '512', '100', '640x480', '0', '1', '4', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', 'VCUBE HYBRID PRODUCTION', '0', '0', '0', '0', '0', '0', '1', '0', '100', '20', '', '', '', '0', '45000', '100000', '100000', '0', '2014-08-30 00:00:00', '1', '0', '0', '0', '5000', null, '0', '50', null, '0', '0', '1', '0', null);
INSERT INTO `service` VALUES ('112', '1', 'グローバルプラン 電話連携あり(代理店)', '10', '0', '10', '0', '0', '0', '0', '0', '0', '6144', '512', '100', '640x480', '0', '1', '4', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', 'VCUBE HYBRID AGENCY PRODUCTION', '0', '0', '0', '0', '0', '0', '1', '0', '100', '20', '', '', '', '0', '45000', '100000', '100000', '0', '2014-08-30 00:00:00', '1', '0', '0', '0', '5000', null, '0', '50', null, '0', '0', '1', '0', null);
INSERT INTO `service` VALUES ('113', '1', 'グローバルプラン 電話連携なし', '10', '0', '10', '0', '0', '0', '0', '0', '0', '6144', '512', '100', '640x480', '0', '1', '4', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '', '0', '0', '0', '0', '0', '0', '1', '0', '100', '20', '', '', '', '0', '45000', '100000', '100000', '0', '2014-08-30 00:00:00', '1', '0', '0', '0', '5000', null, '0', '50', null, '0', '0', '1', '0', null);
INSERT INTO `service` VALUES ('114', '1', 'ローカルルプラン 電話連携あり', '10', '0', '10', '0', '0', '0', '0', '0', '0', '6144', '512', '100', '640x480', '0', '1', '4', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', 'VCUBE HYBRID PRODUCTION', '0', '0', '0', '0', '0', '0', '1', '0', '100', '20', '', '', '', '0', '45000', '100000', '100000', '0', '2014-08-30 00:00:00', '1', '0', '0', '0', '5000', null, '0', '50', null, '0', '0', '1', '0', null);
INSERT INTO `service` VALUES ('115', '1', 'ローカルルプラン 電話連携あり(代理店)', '10', '0', '10', '0', '0', '0', '0', '0', '0', '6144', '512', '100', '640x480', '0', '1', '4', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', 'VCUBE HYBRID AGENCY PRODUCTION', '0', '0', '0', '0', '0', '0', '1', '0', '100', '20', '', '', '', '0', '45000', '100000', '100000', '0', '2014-08-30 00:00:00', '1', '0', '0', '0', '5000', null, '0', '50', null, '0', '0', '1', '0', null);
INSERT INTO `service` VALUES ('116', '1', 'ローカルルプラン 電話連携なし', '10', '0', '10', '0', '0', '0', '0', '0', '0', '6144', '512', '100', '640x480', '0', '1', '4', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '0', '0', '0', '0', '0', '1', '0', '100', '20', '', '', '', '0', '45000', '100000', '100000', '0', '2014-08-30 00:00:00', '1', '0', '0', '0', '5000', null, '0', '50', null, '0', '0', '1', '0', null);
INSERT INTO `service` VALUES ('117', '1', 'STBプラン', '12', '0', '12', '1', '1', '0', '0', '0', '0', '10240', '1024', '150', '720p', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '1', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2014-08-18 17:53:20', '1', '0', '1000', '1000', '0', null, '0', '0', null, '0', '0', '0', '0', null);
INSERT INTO `service` VALUES ('118', '1', 'ONEセールスプラン', '2', '0', '0', '0', '0', '0', '0', '0', '0', '2048', '512', '100', '640x480', '0', '0', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', null, null, null, '0', '0', '0', '0', '0', '2014-09-12 00:00:00', '1', '0', '0', '0', '0', null, '0', '0', null, '0', '0', '0', '0', null);

-- ----------------------------
-- Table structure for service_option
-- ----------------------------
DROP TABLE IF EXISTS `service_option`;
CREATE TABLE `service_option` (
  `service_option_key` int(11) NOT NULL AUTO_INCREMENT,
  `country_key` int(11) NOT NULL DEFAULT '0',
  `service_option_name` text NOT NULL,
  `service_option_price` int(11) NOT NULL DEFAULT '0',
  `service_option_init_price` int(11) NOT NULL DEFAULT '0',
  `service_option_running_price` int(11) NOT NULL DEFAULT '0',
  `option_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'オプションステータス',
  `use_user_option` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ユーザーオプションフラグ',
  PRIMARY KEY (`service_option_key`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of service_option
-- ----------------------------
INSERT INTO `service_option` VALUES ('1', '1', 'messenger', '100', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('2', '1', 'meeting_ssl', '8000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('3', '1', 'desktop_share', '10000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('4', '1', 'hdd_extention', '10000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('5', '1', 'high_quality', '10000', '0', '30', '1', '0');
INSERT INTO `service_option` VALUES ('6', '1', 'mobile_phone', '5000', '0', '50', '1', '0');
INSERT INTO `service_option` VALUES ('7', '1', 'audience', '10000', '10000', '10', '1', '0');
INSERT INTO `service_option` VALUES ('8', '1', 'h323_client', '5000', '0', '10', '1', '0');
INSERT INTO `service_option` VALUES ('9', '1', 'on_demand', '30000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('10', '1', 'on_demand_hdd_extention', '10000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('11', '1', 'on_demand_user_extention', '30000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('12', '1', 'seminar_user_extention', '10000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('13', '1', 'compact', '10000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('14', '1', '導入サポート', '0', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('15', '1', 'intra_fms', '0', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('16', '1', 'whiteboard', '0', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('18', '1', 'multicamera', '10000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('19', '1', 'telephone', '10000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('20', '1', 'record_gw', '0', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('21', '1', 'smartphone', '5000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('22', '0', 'whiteboard_video', '5000', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('23', '0', 'teleconference', '0', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('24', '1', 'video_conference', '0', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('25', '1', 'minimumBandwidth80', '0', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('26', '1', 'h264', '0', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('27', '1', 'meeting_tls', '0', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('30', '1', 'GlobalLink', '0', '0', '0', '1', '0');
INSERT INTO `service_option` VALUES ('31', '1', 'user_max_storage_size_extention_1G', '10000', '0', '0', '1', '1');

-- ----------------------------
-- Table structure for sharing_server
-- ----------------------------
DROP TABLE IF EXISTS `sharing_server`;
CREATE TABLE `sharing_server` (
  `server_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datacenter_key` int(10) unsigned NOT NULL DEFAULT '0',
  `server_address` varchar(64) NOT NULL DEFAULT '',
  `server_country` varchar(32) DEFAULT 'jp',
  `server_count` int(10) unsigned DEFAULT '0',
  `server_priority` int(10) unsigned NOT NULL DEFAULT '0',
  `name_ja` text,
  `name_en` text,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`server_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sharing_server
-- ----------------------------

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `staff_key` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '担当キー',
  `login_id` varchar(64) DEFAULT NULL COMMENT 'ログインID',
  `login_password` varchar(64) DEFAULT NULL COMMENT 'ログインパスワード',
  `staff_id` varchar(4) DEFAULT NULL COMMENT '社員管理番号',
  `name` text COMMENT '担当者名',
  `section_id` varchar(256) DEFAULT NULL COMMENT '事業部ID',
  `status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ステータス',
  `authority` int(11) NOT NULL DEFAULT '0' COMMENT '権限',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日',
  PRIMARY KEY (`staff_key`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='担当者マスタ';

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('1', 'n2my_admin', 'df6f4c0750202b4485751787b3a6de1baa2b0aad', '1', 'システム管理者', '1', '1', '2', '2008-12-06 22:01:28', '2011-08-11 14:42:27');

-- ----------------------------
-- Table structure for trial
-- ----------------------------
DROP TABLE IF EXISTS `trial`;
CREATE TABLE `trial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(54) DEFAULT NULL,
  `session` varchar(200) NOT NULL,
  `q1` text,
  `q2` varchar(100) DEFAULT NULL,
  `q3` varchar(100) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trial
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` text NOT NULL,
  `server_key` int(10) NOT NULL,
  `user_registtime` datetime NOT NULL,
  `user_updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`user_key`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Table structure for user_agent
-- ----------------------------
DROP TABLE IF EXISTS `user_agent`;
CREATE TABLE `user_agent` (
  `user_agent_key` int(11) NOT NULL AUTO_INCREMENT,
  `db_key` varchar(100) NOT NULL COMMENT 'DB名',
  `user_id` varchar(64) DEFAULT NULL COMMENT 'ユーザーID',
  `invoice_flg` int(11) DEFAULT NULL COMMENT '請求フラグ',
  `meeting_key` int(11) NOT NULL COMMENT '会議キー',
  `participant_key` int(11) DEFAULT NULL COMMENT '参加者ID',
  `appli_type` varchar(64) DEFAULT NULL COMMENT 'タイプ(normal, audience...)',
  `appli_mode` varchar(64) DEFAULT NULL COMMENT 'モード(general, h323, mobile...)',
  `appli_role` varchar(64) DEFAULT NULL COMMENT 'ロール(default, invite...)',
  `appli_version` varchar(50) DEFAULT NULL COMMENT 'as2/as3',
  `user_agent` text COMMENT '生UserAgent',
  `os` varchar(100) DEFAULT NULL COMMENT 'OS',
  `browser` varchar(100) DEFAULT NULL COMMENT 'ブラウザ',
  `flash` varchar(50) DEFAULT NULL COMMENT 'Flashバージョン',
  `protcol` varchar(20) DEFAULT NULL COMMENT 'FMS接続プロトコル',
  `port` varchar(20) DEFAULT NULL COMMENT 'FMS接続ポート',
  `ip` varchar(100) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `monitor_size` varchar(100) DEFAULT NULL COMMENT 'モニターサイズ',
  `monitor_color` varchar(20) DEFAULT NULL COMMENT 'ビット',
  `camera` text COMMENT 'カメラデバイス名',
  `mic` text COMMENT 'マイクデバイス名',
  `speed` varchar(20) DEFAULT NULL COMMENT '回線速度(bps)',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`user_agent_key`),
  KEY `participant_key` (`participant_key`),
  KEY `user_id` (`user_id`),
  KEY `meeting_key` (`meeting_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_agent
-- ----------------------------
